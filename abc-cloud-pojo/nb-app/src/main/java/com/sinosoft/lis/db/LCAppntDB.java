/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.vschema.LCAppntSet;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LCAppntDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-09-07
 */
public class LCAppntDB extends LCAppntSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LCAppntDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LCAppnt" );
        mflag = true;
    }

    public LCAppntDB() {
        con = null;
        db = new DBOper( "LCAppnt" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LCAppntSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LCAppntSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LCAppnt WHERE  1=1  AND AppntID = ? AND ContNo = ?");
            pstmt.setLong(1, this.getAppntID());
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getContNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCAppnt");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LCAppnt SET  AppntID = ? , ContID = ? , PersonID = ? , ShardingID = ? , GrpContNo = ? , ContNo = ? , PrtNo = ? , AppntNo = ? , AppntGrade = ? , AppntName = ? , AppntSex = ? , AppntBirthday = ? , AppntType = ? , AddressNo = ? , IDType = ? , IDNo = ? , NativePlace = ? , Nationality = ? , RgtAddress = ? , Marriage = ? , MarriageDate = ? , Health = ? , Stature = ? , Avoirdupois = ? , Degree = ? , CreditGrade = ? , BankCode = ? , BankAccNo = ? , AccName = ? , JoinCompanyDate = ? , StartWorkDate = ? , Position = ? , Salary = ? , OccupationType = ? , OccupationCode = ? , WorkType = ? , PluralityType = ? , SmokeFlag = ? , Operator = ? , ManageCom = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , BMI = ? , License = ? , LicenseType = ? , RelatToInsu = ? , OccupationDesb = ? , IdValiDate = ? , HaveMotorcycleLicence = ? , PartTimeJob = ? , FirstName = ? , LastName = ? , CUSLevel = ? , AppRgtTpye = ? , TINNO = ? , TINFlag = ? , SocialInsuFlag = ? , LastNameEn = ? , FirstNameEn = ? , NameEn = ? , LastNamePY = ? , FirstNamePY = ? , Language = ? , SpecialPopFlag = ? , AffirmInputFlag = ? , YearIncome = ? , IncomeSource = ? , FamilyYearSalary = ? , DenType = ? , BudgetPrem = ? , TAXRESIDENTTYPE = ? , IDSTDATE = ? WHERE  1=1  AND AppntID = ? AND ContNo = ?");
            pstmt.setLong(1, this.getAppntID());
            pstmt.setLong(2, this.getContID());
            pstmt.setLong(3, this.getPersonID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getShardingID());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getContNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getPrtNo());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAppntNo());
            }
            if(this.getAppntGrade() == null || this.getAppntGrade().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAppntGrade());
            }
            if(this.getAppntName() == null || this.getAppntName().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAppntName());
            }
            if(this.getAppntSex() == null || this.getAppntSex().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAppntSex());
            }
            if(this.getAppntBirthday() == null || this.getAppntBirthday().equals("null")) {
            	pstmt.setNull(12, 93);
            } else {
            	pstmt.setDate(12, Date.valueOf(this.getAppntBirthday()));
            }
            if(this.getAppntType() == null || this.getAppntType().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAppntType());
            }
            if(this.getAddressNo() == null || this.getAddressNo().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAddressNo());
            }
            if(this.getIDType() == null || this.getIDType().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getIDType());
            }
            if(this.getIDNo() == null || this.getIDNo().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getIDNo());
            }
            if(this.getNativePlace() == null || this.getNativePlace().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getNativePlace());
            }
            if(this.getNationality() == null || this.getNationality().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getNationality());
            }
            if(this.getRgtAddress() == null || this.getRgtAddress().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getRgtAddress());
            }
            if(this.getMarriage() == null || this.getMarriage().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getMarriage());
            }
            if(this.getMarriageDate() == null || this.getMarriageDate().equals("null")) {
            	pstmt.setNull(21, 93);
            } else {
            	pstmt.setDate(21, Date.valueOf(this.getMarriageDate()));
            }
            if(this.getHealth() == null || this.getHealth().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getHealth());
            }
            pstmt.setDouble(23, this.getStature());
            pstmt.setDouble(24, this.getAvoirdupois());
            if(this.getDegree() == null || this.getDegree().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getDegree());
            }
            if(this.getCreditGrade() == null || this.getCreditGrade().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getCreditGrade());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getBankAccNo());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getAccName());
            }
            if(this.getJoinCompanyDate() == null || this.getJoinCompanyDate().equals("null")) {
            	pstmt.setNull(30, 93);
            } else {
            	pstmt.setDate(30, Date.valueOf(this.getJoinCompanyDate()));
            }
            if(this.getStartWorkDate() == null || this.getStartWorkDate().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getStartWorkDate()));
            }
            if(this.getPosition() == null || this.getPosition().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getPosition());
            }
            pstmt.setDouble(33, this.getSalary());
            if(this.getOccupationType() == null || this.getOccupationType().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getOccupationType());
            }
            if(this.getOccupationCode() == null || this.getOccupationCode().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getOccupationCode());
            }
            if(this.getWorkType() == null || this.getWorkType().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getWorkType());
            }
            if(this.getPluralityType() == null || this.getPluralityType().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getPluralityType());
            }
            if(this.getSmokeFlag() == null || this.getSmokeFlag().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getSmokeFlag());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getOperator());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getManageCom());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(43, 93);
            } else {
            	pstmt.setDate(43, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getModifyTime());
            }
            pstmt.setDouble(45, this.getBMI());
            if(this.getLicense() == null || this.getLicense().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getLicense());
            }
            if(this.getLicenseType() == null || this.getLicenseType().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getLicenseType());
            }
            if(this.getRelatToInsu() == null || this.getRelatToInsu().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getRelatToInsu());
            }
            if(this.getOccupationDesb() == null || this.getOccupationDesb().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getOccupationDesb());
            }
            if(this.getIdValiDate() == null || this.getIdValiDate().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getIdValiDate());
            }
            if(this.getHaveMotorcycleLicence() == null || this.getHaveMotorcycleLicence().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getHaveMotorcycleLicence());
            }
            if(this.getPartTimeJob() == null || this.getPartTimeJob().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getPartTimeJob());
            }
            if(this.getFirstName() == null || this.getFirstName().equals("null")) {
            	pstmt.setNull(53, 12);
            } else {
            	pstmt.setString(53, this.getFirstName());
            }
            if(this.getLastName() == null || this.getLastName().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getLastName());
            }
            if(this.getCUSLevel() == null || this.getCUSLevel().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getCUSLevel());
            }
            if(this.getAppRgtTpye() == null || this.getAppRgtTpye().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getAppRgtTpye());
            }
            if(this.getTINNO() == null || this.getTINNO().equals("null")) {
            	pstmt.setNull(57, 12);
            } else {
            	pstmt.setString(57, this.getTINNO());
            }
            if(this.getTINFlag() == null || this.getTINFlag().equals("null")) {
            	pstmt.setNull(58, 12);
            } else {
            	pstmt.setString(58, this.getTINFlag());
            }
            if(this.getSocialInsuFlag() == null || this.getSocialInsuFlag().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getSocialInsuFlag());
            }
            if(this.getLastNameEn() == null || this.getLastNameEn().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getLastNameEn());
            }
            if(this.getFirstNameEn() == null || this.getFirstNameEn().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getFirstNameEn());
            }
            if(this.getNameEn() == null || this.getNameEn().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getNameEn());
            }
            if(this.getLastNamePY() == null || this.getLastNamePY().equals("null")) {
            	pstmt.setNull(63, 12);
            } else {
            	pstmt.setString(63, this.getLastNamePY());
            }
            if(this.getFirstNamePY() == null || this.getFirstNamePY().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getFirstNamePY());
            }
            if(this.getLanguage() == null || this.getLanguage().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getLanguage());
            }
            if(this.getSpecialPopFlag() == null || this.getSpecialPopFlag().equals("null")) {
            	pstmt.setNull(66, 12);
            } else {
            	pstmt.setString(66, this.getSpecialPopFlag());
            }
            if(this.getAffirmInputFlag() == null || this.getAffirmInputFlag().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getAffirmInputFlag());
            }
            pstmt.setDouble(68, this.getYearIncome());
            if(this.getIncomeSource() == null || this.getIncomeSource().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getIncomeSource());
            }
            pstmt.setDouble(70, this.getFamilyYearSalary());
            if(this.getDenType() == null || this.getDenType().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getDenType());
            }
            if(this.getBudgetPrem() == null || this.getBudgetPrem().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getBudgetPrem());
            }
            if(this.getTAXRESIDENTTYPE() == null || this.getTAXRESIDENTTYPE().equals("null")) {
            	pstmt.setNull(73, 12);
            } else {
            	pstmt.setString(73, this.getTAXRESIDENTTYPE());
            }
            if(this.getIDSTDATE() == null || this.getIDSTDATE().equals("null")) {
            	pstmt.setNull(74, 12);
            } else {
            	pstmt.setString(74, this.getIDSTDATE());
            }
            // set where condition
            pstmt.setLong(75, this.getAppntID());
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(76, 12);
            } else {
            	pstmt.setString(76, this.getContNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCAppnt");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LCAppnt VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getAppntID());
            pstmt.setLong(2, this.getContID());
            pstmt.setLong(3, this.getPersonID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getShardingID());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getContNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getPrtNo());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAppntNo());
            }
            if(this.getAppntGrade() == null || this.getAppntGrade().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAppntGrade());
            }
            if(this.getAppntName() == null || this.getAppntName().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAppntName());
            }
            if(this.getAppntSex() == null || this.getAppntSex().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAppntSex());
            }
            if(this.getAppntBirthday() == null || this.getAppntBirthday().equals("null")) {
            	pstmt.setNull(12, 93);
            } else {
            	pstmt.setDate(12, Date.valueOf(this.getAppntBirthday()));
            }
            if(this.getAppntType() == null || this.getAppntType().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAppntType());
            }
            if(this.getAddressNo() == null || this.getAddressNo().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAddressNo());
            }
            if(this.getIDType() == null || this.getIDType().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getIDType());
            }
            if(this.getIDNo() == null || this.getIDNo().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getIDNo());
            }
            if(this.getNativePlace() == null || this.getNativePlace().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getNativePlace());
            }
            if(this.getNationality() == null || this.getNationality().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getNationality());
            }
            if(this.getRgtAddress() == null || this.getRgtAddress().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getRgtAddress());
            }
            if(this.getMarriage() == null || this.getMarriage().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getMarriage());
            }
            if(this.getMarriageDate() == null || this.getMarriageDate().equals("null")) {
            	pstmt.setNull(21, 93);
            } else {
            	pstmt.setDate(21, Date.valueOf(this.getMarriageDate()));
            }
            if(this.getHealth() == null || this.getHealth().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getHealth());
            }
            pstmt.setDouble(23, this.getStature());
            pstmt.setDouble(24, this.getAvoirdupois());
            if(this.getDegree() == null || this.getDegree().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getDegree());
            }
            if(this.getCreditGrade() == null || this.getCreditGrade().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getCreditGrade());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getBankAccNo());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getAccName());
            }
            if(this.getJoinCompanyDate() == null || this.getJoinCompanyDate().equals("null")) {
            	pstmt.setNull(30, 93);
            } else {
            	pstmt.setDate(30, Date.valueOf(this.getJoinCompanyDate()));
            }
            if(this.getStartWorkDate() == null || this.getStartWorkDate().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getStartWorkDate()));
            }
            if(this.getPosition() == null || this.getPosition().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getPosition());
            }
            pstmt.setDouble(33, this.getSalary());
            if(this.getOccupationType() == null || this.getOccupationType().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getOccupationType());
            }
            if(this.getOccupationCode() == null || this.getOccupationCode().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getOccupationCode());
            }
            if(this.getWorkType() == null || this.getWorkType().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getWorkType());
            }
            if(this.getPluralityType() == null || this.getPluralityType().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getPluralityType());
            }
            if(this.getSmokeFlag() == null || this.getSmokeFlag().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getSmokeFlag());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getOperator());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getManageCom());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(43, 93);
            } else {
            	pstmt.setDate(43, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getModifyTime());
            }
            pstmt.setDouble(45, this.getBMI());
            if(this.getLicense() == null || this.getLicense().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getLicense());
            }
            if(this.getLicenseType() == null || this.getLicenseType().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getLicenseType());
            }
            if(this.getRelatToInsu() == null || this.getRelatToInsu().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getRelatToInsu());
            }
            if(this.getOccupationDesb() == null || this.getOccupationDesb().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getOccupationDesb());
            }
            if(this.getIdValiDate() == null || this.getIdValiDate().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getIdValiDate());
            }
            if(this.getHaveMotorcycleLicence() == null || this.getHaveMotorcycleLicence().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getHaveMotorcycleLicence());
            }
            if(this.getPartTimeJob() == null || this.getPartTimeJob().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getPartTimeJob());
            }
            if(this.getFirstName() == null || this.getFirstName().equals("null")) {
            	pstmt.setNull(53, 12);
            } else {
            	pstmt.setString(53, this.getFirstName());
            }
            if(this.getLastName() == null || this.getLastName().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getLastName());
            }
            if(this.getCUSLevel() == null || this.getCUSLevel().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getCUSLevel());
            }
            if(this.getAppRgtTpye() == null || this.getAppRgtTpye().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getAppRgtTpye());
            }
            if(this.getTINNO() == null || this.getTINNO().equals("null")) {
            	pstmt.setNull(57, 12);
            } else {
            	pstmt.setString(57, this.getTINNO());
            }
            if(this.getTINFlag() == null || this.getTINFlag().equals("null")) {
            	pstmt.setNull(58, 12);
            } else {
            	pstmt.setString(58, this.getTINFlag());
            }
            if(this.getSocialInsuFlag() == null || this.getSocialInsuFlag().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getSocialInsuFlag());
            }
            if(this.getLastNameEn() == null || this.getLastNameEn().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getLastNameEn());
            }
            if(this.getFirstNameEn() == null || this.getFirstNameEn().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getFirstNameEn());
            }
            if(this.getNameEn() == null || this.getNameEn().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getNameEn());
            }
            if(this.getLastNamePY() == null || this.getLastNamePY().equals("null")) {
            	pstmt.setNull(63, 12);
            } else {
            	pstmt.setString(63, this.getLastNamePY());
            }
            if(this.getFirstNamePY() == null || this.getFirstNamePY().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getFirstNamePY());
            }
            if(this.getLanguage() == null || this.getLanguage().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getLanguage());
            }
            if(this.getSpecialPopFlag() == null || this.getSpecialPopFlag().equals("null")) {
            	pstmt.setNull(66, 12);
            } else {
            	pstmt.setString(66, this.getSpecialPopFlag());
            }
            if(this.getAffirmInputFlag() == null || this.getAffirmInputFlag().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getAffirmInputFlag());
            }
            pstmt.setDouble(68, this.getYearIncome());
            if(this.getIncomeSource() == null || this.getIncomeSource().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getIncomeSource());
            }
            pstmt.setDouble(70, this.getFamilyYearSalary());
            if(this.getDenType() == null || this.getDenType().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getDenType());
            }
            if(this.getBudgetPrem() == null || this.getBudgetPrem().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getBudgetPrem());
            }
            if(this.getTAXRESIDENTTYPE() == null || this.getTAXRESIDENTTYPE().equals("null")) {
            	pstmt.setNull(73, 12);
            } else {
            	pstmt.setString(73, this.getTAXRESIDENTTYPE());
            }
            if(this.getIDSTDATE() == null || this.getIDSTDATE().equals("null")) {
            	pstmt.setNull(74, 12);
            } else {
            	pstmt.setString(74, this.getIDSTDATE());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LCAppnt WHERE  1=1  AND AppntID = ? AND ContNo = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getAppntID());
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getContNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAppntDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LCAppntSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LCAppntSet aLCAppntSet = new LCAppntSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCAppnt");
            LCAppntSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAppntDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LCAppntSchema s1 = new LCAppntSchema();
                s1.setSchema(rs,i);
                aLCAppntSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLCAppntSet;
    }

    public LCAppntSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LCAppntSet aLCAppntSet = new LCAppntSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAppntDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LCAppntSchema s1 = new LCAppntSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAppntDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCAppntSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCAppntSet;
    }

    public LCAppntSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCAppntSet aLCAppntSet = new LCAppntSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCAppnt");
            LCAppntSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCAppntSchema s1 = new LCAppntSchema();
                s1.setSchema(rs,i);
                aLCAppntSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCAppntSet;
    }

    public LCAppntSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCAppntSet aLCAppntSet = new LCAppntSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCAppntSchema s1 = new LCAppntSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCAppntDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCAppntSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCAppntSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCAppnt");
            LCAppntSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LCAppnt " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCAppntDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LCAppntSet
     */
    public LCAppntSet getData() {
        int tCount = 0;
        LCAppntSet tLCAppntSet = new LCAppntSet();
        LCAppntSchema tLCAppntSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLCAppntSchema = new LCAppntSchema();
            tLCAppntSchema.setSchema(mResultSet, 1);
            tLCAppntSet.add(tLCAppntSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLCAppntSchema = new LCAppntSchema();
                    tLCAppntSchema.setSchema(mResultSet, 1);
                    tLCAppntSet.add(tLCAppntSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLCAppntSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LCAppntDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LCAppntDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LCAppntDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
