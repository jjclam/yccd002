/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBUWErrorDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBUWErrorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBUWErrorSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long UWErrorID;
    /** Shardingid */
    private String ShardingID;
    /** 批单号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 保单号码 */
    private String PolNo;
    /** 投保单号码 */
    private String ProposalNo;
    /** 核保次数 */
    private int UWNo;
    /** 流水号 */
    private String SerialNo;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 管理机构 */
    private String ManageCom;
    /** 核保规则编码 */
    private String UWRuleCode;
    /** 核保出错信息 */
    private String UWError;
    /** 当前值 */
    private String CurrValue;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 核保可通过标记（分保） */
    private String UWPassFlag;
    /** 核保级别 */
    private String UWGrade;
    /** 核保建议结论 */
    private String SugPassFlag;

    public static final int FIELDNUM = 23;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBUWErrorSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "UWErrorID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBUWErrorSchema cloned = (LBUWErrorSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getUWErrorID() {
        return UWErrorID;
    }
    public void setUWErrorID(long aUWErrorID) {
        UWErrorID = aUWErrorID;
    }
    public void setUWErrorID(String aUWErrorID) {
        if (aUWErrorID != null && !aUWErrorID.equals("")) {
            UWErrorID = new Long(aUWErrorID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public int getUWNo() {
        return UWNo;
    }
    public void setUWNo(int aUWNo) {
        UWNo = aUWNo;
    }
    public void setUWNo(String aUWNo) {
        if (aUWNo != null && !aUWNo.equals("")) {
            Integer tInteger = new Integer(aUWNo);
            int i = tInteger.intValue();
            UWNo = i;
        }
    }

    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getUWRuleCode() {
        return UWRuleCode;
    }
    public void setUWRuleCode(String aUWRuleCode) {
        UWRuleCode = aUWRuleCode;
    }
    public String getUWError() {
        return UWError;
    }
    public void setUWError(String aUWError) {
        UWError = aUWError;
    }
    public String getCurrValue() {
        return CurrValue;
    }
    public void setCurrValue(String aCurrValue) {
        CurrValue = aCurrValue;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getUWPassFlag() {
        return UWPassFlag;
    }
    public void setUWPassFlag(String aUWPassFlag) {
        UWPassFlag = aUWPassFlag;
    }
    public String getUWGrade() {
        return UWGrade;
    }
    public void setUWGrade(String aUWGrade) {
        UWGrade = aUWGrade;
    }
    public String getSugPassFlag() {
        return SugPassFlag;
    }
    public void setSugPassFlag(String aSugPassFlag) {
        SugPassFlag = aSugPassFlag;
    }

    /**
    * 使用另外一个 LBUWErrorSchema 对象给 Schema 赋值
    * @param: aLBUWErrorSchema LBUWErrorSchema
    **/
    public void setSchema(LBUWErrorSchema aLBUWErrorSchema) {
        this.UWErrorID = aLBUWErrorSchema.getUWErrorID();
        this.ShardingID = aLBUWErrorSchema.getShardingID();
        this.EdorNo = aLBUWErrorSchema.getEdorNo();
        this.GrpContNo = aLBUWErrorSchema.getGrpContNo();
        this.ContNo = aLBUWErrorSchema.getContNo();
        this.ProposalContNo = aLBUWErrorSchema.getProposalContNo();
        this.PolNo = aLBUWErrorSchema.getPolNo();
        this.ProposalNo = aLBUWErrorSchema.getProposalNo();
        this.UWNo = aLBUWErrorSchema.getUWNo();
        this.SerialNo = aLBUWErrorSchema.getSerialNo();
        this.InsuredNo = aLBUWErrorSchema.getInsuredNo();
        this.InsuredName = aLBUWErrorSchema.getInsuredName();
        this.AppntNo = aLBUWErrorSchema.getAppntNo();
        this.AppntName = aLBUWErrorSchema.getAppntName();
        this.ManageCom = aLBUWErrorSchema.getManageCom();
        this.UWRuleCode = aLBUWErrorSchema.getUWRuleCode();
        this.UWError = aLBUWErrorSchema.getUWError();
        this.CurrValue = aLBUWErrorSchema.getCurrValue();
        this.ModifyDate = fDate.getDate( aLBUWErrorSchema.getModifyDate());
        this.ModifyTime = aLBUWErrorSchema.getModifyTime();
        this.UWPassFlag = aLBUWErrorSchema.getUWPassFlag();
        this.UWGrade = aLBUWErrorSchema.getUWGrade();
        this.SugPassFlag = aLBUWErrorSchema.getSugPassFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.UWErrorID = rs.getLong("UWErrorID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("ProposalContNo") == null )
                this.ProposalContNo = null;
            else
                this.ProposalContNo = rs.getString("ProposalContNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("ProposalNo") == null )
                this.ProposalNo = null;
            else
                this.ProposalNo = rs.getString("ProposalNo").trim();

            this.UWNo = rs.getInt("UWNo");
            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("InsuredName") == null )
                this.InsuredName = null;
            else
                this.InsuredName = rs.getString("InsuredName").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("AppntName") == null )
                this.AppntName = null;
            else
                this.AppntName = rs.getString("AppntName").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("UWRuleCode") == null )
                this.UWRuleCode = null;
            else
                this.UWRuleCode = rs.getString("UWRuleCode").trim();

            if( rs.getString("UWError") == null )
                this.UWError = null;
            else
                this.UWError = rs.getString("UWError").trim();

            if( rs.getString("CurrValue") == null )
                this.CurrValue = null;
            else
                this.CurrValue = rs.getString("CurrValue").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("UWPassFlag") == null )
                this.UWPassFlag = null;
            else
                this.UWPassFlag = rs.getString("UWPassFlag").trim();

            if( rs.getString("UWGrade") == null )
                this.UWGrade = null;
            else
                this.UWGrade = rs.getString("UWGrade").trim();

            if( rs.getString("SugPassFlag") == null )
                this.SugPassFlag = null;
            else
                this.SugPassFlag = rs.getString("SugPassFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBUWErrorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBUWErrorSchema getSchema() {
        LBUWErrorSchema aLBUWErrorSchema = new LBUWErrorSchema();
        aLBUWErrorSchema.setSchema(this);
        return aLBUWErrorSchema;
    }

    public LBUWErrorDB getDB() {
        LBUWErrorDB aDBOper = new LBUWErrorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBUWError描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(UWErrorID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(UWNo));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWRuleCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWError)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CurrValue)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWPassFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SugPassFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBUWError>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            UWErrorID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            UWNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9, SysConst.PACKAGESPILTER))).intValue();
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            UWRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            UWError = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            CurrValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            UWPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            SugPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBUWErrorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("UWErrorID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWErrorID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("UWNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWNo));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("UWRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWRuleCode));
        }
        if (FCode.equalsIgnoreCase("UWError")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWError));
        }
        if (FCode.equalsIgnoreCase("CurrValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CurrValue));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("UWPassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWPassFlag));
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWGrade));
        }
        if (FCode.equalsIgnoreCase("SugPassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SugPassFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(UWErrorID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ProposalNo);
                break;
            case 8:
                strFieldValue = String.valueOf(UWNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(UWRuleCode);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(UWError);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(CurrValue);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(UWPassFlag);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(UWGrade);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(SugPassFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("UWErrorID")) {
            if( FValue != null && !FValue.equals("")) {
                UWErrorID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("UWNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                UWNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("UWRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWRuleCode = FValue.trim();
            }
            else
                UWRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("UWError")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWError = FValue.trim();
            }
            else
                UWError = null;
        }
        if (FCode.equalsIgnoreCase("CurrValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                CurrValue = FValue.trim();
            }
            else
                CurrValue = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("UWPassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWPassFlag = FValue.trim();
            }
            else
                UWPassFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
                UWGrade = null;
        }
        if (FCode.equalsIgnoreCase("SugPassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SugPassFlag = FValue.trim();
            }
            else
                SugPassFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBUWErrorSchema other = (LBUWErrorSchema)otherObject;
        return
            UWErrorID == other.getUWErrorID()
            && ShardingID.equals(other.getShardingID())
            && EdorNo.equals(other.getEdorNo())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && ProposalContNo.equals(other.getProposalContNo())
            && PolNo.equals(other.getPolNo())
            && ProposalNo.equals(other.getProposalNo())
            && UWNo == other.getUWNo()
            && SerialNo.equals(other.getSerialNo())
            && InsuredNo.equals(other.getInsuredNo())
            && InsuredName.equals(other.getInsuredName())
            && AppntNo.equals(other.getAppntNo())
            && AppntName.equals(other.getAppntName())
            && ManageCom.equals(other.getManageCom())
            && UWRuleCode.equals(other.getUWRuleCode())
            && UWError.equals(other.getUWError())
            && CurrValue.equals(other.getCurrValue())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && UWPassFlag.equals(other.getUWPassFlag())
            && UWGrade.equals(other.getUWGrade())
            && SugPassFlag.equals(other.getSugPassFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("UWErrorID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 5;
        }
        if( strFieldName.equals("PolNo") ) {
            return 6;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 7;
        }
        if( strFieldName.equals("UWNo") ) {
            return 8;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 9;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 10;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 11;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 12;
        }
        if( strFieldName.equals("AppntName") ) {
            return 13;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 14;
        }
        if( strFieldName.equals("UWRuleCode") ) {
            return 15;
        }
        if( strFieldName.equals("UWError") ) {
            return 16;
        }
        if( strFieldName.equals("CurrValue") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 19;
        }
        if( strFieldName.equals("UWPassFlag") ) {
            return 20;
        }
        if( strFieldName.equals("UWGrade") ) {
            return 21;
        }
        if( strFieldName.equals("SugPassFlag") ) {
            return 22;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "UWErrorID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "ProposalContNo";
                break;
            case 6:
                strFieldName = "PolNo";
                break;
            case 7:
                strFieldName = "ProposalNo";
                break;
            case 8:
                strFieldName = "UWNo";
                break;
            case 9:
                strFieldName = "SerialNo";
                break;
            case 10:
                strFieldName = "InsuredNo";
                break;
            case 11:
                strFieldName = "InsuredName";
                break;
            case 12:
                strFieldName = "AppntNo";
                break;
            case 13:
                strFieldName = "AppntName";
                break;
            case 14:
                strFieldName = "ManageCom";
                break;
            case 15:
                strFieldName = "UWRuleCode";
                break;
            case 16:
                strFieldName = "UWError";
                break;
            case 17:
                strFieldName = "CurrValue";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            case 20:
                strFieldName = "UWPassFlag";
                break;
            case 21:
                strFieldName = "UWGrade";
                break;
            case 22:
                strFieldName = "SugPassFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "UWERRORID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "UWNO":
                return Schema.TYPE_INT;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "UWRULECODE":
                return Schema.TYPE_STRING;
            case "UWERROR":
                return Schema.TYPE_STRING;
            case "CURRVALUE":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "UWPASSFLAG":
                return Schema.TYPE_STRING;
            case "UWGRADE":
                return Schema.TYPE_STRING;
            case "SUGPASSFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
