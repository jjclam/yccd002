/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LMRiskDutyPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-10-16
 */
public class LMRiskDutyPojo implements  Pojo,Serializable {

    // @Field
    /** 险种编码 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 责任代码 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String DutyCode; 
    /** 选择标记 */
    private String ChoFlag; 
    /** 个案标记 */
    private String SpecFlag; 
    /** 责任分类 */
    private String Dutytype; 
    /** 账单类型 */
    private String Accprop; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getChoFlag() {
        return ChoFlag;
    }
    public void setChoFlag(String aChoFlag) {
        ChoFlag = aChoFlag;
    }
    public String getSpecFlag() {
        return SpecFlag;
    }
    public void setSpecFlag(String aSpecFlag) {
        SpecFlag = aSpecFlag;
    }
    public String getDutytype() {
        return Dutytype;
    }
    public void setDutytype(String aDutytype) {
        Dutytype = aDutytype;
    }
    public String getAccprop() {
        return Accprop;
    }
    public void setAccprop(String aAccprop) {
        Accprop = aAccprop;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 2;
        }
        if( strFieldName.equals("ChoFlag") ) {
            return 3;
        }
        if( strFieldName.equals("SpecFlag") ) {
            return 4;
        }
        if( strFieldName.equals("Dutytype") ) {
            return 5;
        }
        if( strFieldName.equals("Accprop") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "ChoFlag";
                break;
            case 4:
                strFieldName = "SpecFlag";
                break;
            case 5:
                strFieldName = "Dutytype";
                break;
            case 6:
                strFieldName = "Accprop";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "CHOFLAG":
                return Schema.TYPE_STRING;
            case "SPECFLAG":
                return Schema.TYPE_STRING;
            case "DUTYTYPE":
                return Schema.TYPE_STRING;
            case "ACCPROP":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("ChoFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChoFlag));
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecFlag));
        }
        if (FCode.equalsIgnoreCase("Dutytype")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dutytype));
        }
        if (FCode.equalsIgnoreCase("Accprop")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Accprop));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 3:
                strFieldValue = String.valueOf(ChoFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(SpecFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(Dutytype);
                break;
            case 6:
                strFieldValue = String.valueOf(Accprop);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("ChoFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChoFlag = FValue.trim();
            }
            else
                ChoFlag = null;
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecFlag = FValue.trim();
            }
            else
                SpecFlag = null;
        }
        if (FCode.equalsIgnoreCase("Dutytype")) {
            if( FValue != null && !FValue.equals(""))
            {
                Dutytype = FValue.trim();
            }
            else
                Dutytype = null;
        }
        if (FCode.equalsIgnoreCase("Accprop")) {
            if( FValue != null && !FValue.equals(""))
            {
                Accprop = FValue.trim();
            }
            else
                Accprop = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskDutyPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", DutyCode="+DutyCode +
            ", ChoFlag="+ChoFlag +
            ", SpecFlag="+SpecFlag +
            ", Dutytype="+Dutytype +
            ", Accprop="+Accprop +"]";
    }
}
