/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LKNRTBizTransDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LKNRTBizTransSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-07
 */
public class LKNRTBizTransSchema implements Schema, Cloneable {
    // @Field
    /** Lknrtbizid */
    private long LKNRTBizID;
    /** Shardingid */
    private String ShardingID;
    /** Transcode */
    private String TransCode;
    /** Bankcode */
    private String BankCode;
    /** Trandate */
    private Date TranDate;
    /** Zoneno */
    private String ZoneNo;
    /** Banknode */
    private String BankNode;
    /** Tellerno */
    private String TellerNo;
    /** Transno */
    private String TransNo;
    /** Proposalno */
    private String ProposalNo;
    /** Banksalechnl */
    private String BanksaleChnl;
    /** Uwtype */
    private String UwType;
    /** Appntname */
    private String AppntName;
    /** Appntidtype */
    private String AppntidType;
    /** Appntidno */
    private String AppntidNo;
    /** Appntaccno */
    private String AppntaccNo;
    /** Bizstatus */
    private String BizStatus;
    /** Uwstatus */
    private String UwStatus;
    /** Respdate */
    private Date RespDate;
    /** Resptime */
    private String RespTime;
    /** Makedate */
    private Date MakeDate;
    /** Maketime */
    private String MakeTime;
    /** Modifydate */
    private Date ModifyDate;
    /** Modifytime */
    private String ModifyTime;
    /** Bak1 */
    private String Bak1;
    /** Bak2 */
    private String Bak2;

    public static final int FIELDNUM = 26;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LKNRTBizTransSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "LKNRTBizID";
        pk[1] = "TransCode";
        pk[2] = "BankCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LKNRTBizTransSchema cloned = (LKNRTBizTransSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getLKNRTBizID() {
        return LKNRTBizID;
    }
    public void setLKNRTBizID(long aLKNRTBizID) {
        LKNRTBizID = aLKNRTBizID;
    }
    public void setLKNRTBizID(String aLKNRTBizID) {
        if (aLKNRTBizID != null && !aLKNRTBizID.equals("")) {
            LKNRTBizID = new Long(aLKNRTBizID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getTransCode() {
        return TransCode;
    }
    public void setTransCode(String aTransCode) {
        TransCode = aTransCode;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getTranDate() {
        if(TranDate != null) {
            return fDate.getString(TranDate);
        } else {
            return null;
        }
    }
    public void setTranDate(Date aTranDate) {
        TranDate = aTranDate;
    }
    public void setTranDate(String aTranDate) {
        if (aTranDate != null && !aTranDate.equals("")) {
            TranDate = fDate.getDate(aTranDate);
        } else
            TranDate = null;
    }

    public String getZoneNo() {
        return ZoneNo;
    }
    public void setZoneNo(String aZoneNo) {
        ZoneNo = aZoneNo;
    }
    public String getBankNode() {
        return BankNode;
    }
    public void setBankNode(String aBankNode) {
        BankNode = aBankNode;
    }
    public String getTellerNo() {
        return TellerNo;
    }
    public void setTellerNo(String aTellerNo) {
        TellerNo = aTellerNo;
    }
    public String getTransNo() {
        return TransNo;
    }
    public void setTransNo(String aTransNo) {
        TransNo = aTransNo;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public String getBanksaleChnl() {
        return BanksaleChnl;
    }
    public void setBanksaleChnl(String aBanksaleChnl) {
        BanksaleChnl = aBanksaleChnl;
    }
    public String getUwType() {
        return UwType;
    }
    public void setUwType(String aUwType) {
        UwType = aUwType;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAppntidType() {
        return AppntidType;
    }
    public void setAppntidType(String aAppntidType) {
        AppntidType = aAppntidType;
    }
    public String getAppntidNo() {
        return AppntidNo;
    }
    public void setAppntidNo(String aAppntidNo) {
        AppntidNo = aAppntidNo;
    }
    public String getAppntaccNo() {
        return AppntaccNo;
    }
    public void setAppntaccNo(String aAppntaccNo) {
        AppntaccNo = aAppntaccNo;
    }
    public String getBizStatus() {
        return BizStatus;
    }
    public void setBizStatus(String aBizStatus) {
        BizStatus = aBizStatus;
    }
    public String getUwStatus() {
        return UwStatus;
    }
    public void setUwStatus(String aUwStatus) {
        UwStatus = aUwStatus;
    }
    public String getRespDate() {
        if(RespDate != null) {
            return fDate.getString(RespDate);
        } else {
            return null;
        }
    }
    public void setRespDate(Date aRespDate) {
        RespDate = aRespDate;
    }
    public void setRespDate(String aRespDate) {
        if (aRespDate != null && !aRespDate.equals("")) {
            RespDate = fDate.getDate(aRespDate);
        } else
            RespDate = null;
    }

    public String getRespTime() {
        return RespTime;
    }
    public void setRespTime(String aRespTime) {
        RespTime = aRespTime;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBak1() {
        return Bak1;
    }
    public void setBak1(String aBak1) {
        Bak1 = aBak1;
    }
    public String getBak2() {
        return Bak2;
    }
    public void setBak2(String aBak2) {
        Bak2 = aBak2;
    }

    /**
    * 使用另外一个 LKNRTBizTransSchema 对象给 Schema 赋值
    * @param: aLKNRTBizTransSchema LKNRTBizTransSchema
    **/
    public void setSchema(LKNRTBizTransSchema aLKNRTBizTransSchema) {
        this.LKNRTBizID = aLKNRTBizTransSchema.getLKNRTBizID();
        this.ShardingID = aLKNRTBizTransSchema.getShardingID();
        this.TransCode = aLKNRTBizTransSchema.getTransCode();
        this.BankCode = aLKNRTBizTransSchema.getBankCode();
        this.TranDate = fDate.getDate( aLKNRTBizTransSchema.getTranDate());
        this.ZoneNo = aLKNRTBizTransSchema.getZoneNo();
        this.BankNode = aLKNRTBizTransSchema.getBankNode();
        this.TellerNo = aLKNRTBizTransSchema.getTellerNo();
        this.TransNo = aLKNRTBizTransSchema.getTransNo();
        this.ProposalNo = aLKNRTBizTransSchema.getProposalNo();
        this.BanksaleChnl = aLKNRTBizTransSchema.getBanksaleChnl();
        this.UwType = aLKNRTBizTransSchema.getUwType();
        this.AppntName = aLKNRTBizTransSchema.getAppntName();
        this.AppntidType = aLKNRTBizTransSchema.getAppntidType();
        this.AppntidNo = aLKNRTBizTransSchema.getAppntidNo();
        this.AppntaccNo = aLKNRTBizTransSchema.getAppntaccNo();
        this.BizStatus = aLKNRTBizTransSchema.getBizStatus();
        this.UwStatus = aLKNRTBizTransSchema.getUwStatus();
        this.RespDate = fDate.getDate( aLKNRTBizTransSchema.getRespDate());
        this.RespTime = aLKNRTBizTransSchema.getRespTime();
        this.MakeDate = fDate.getDate( aLKNRTBizTransSchema.getMakeDate());
        this.MakeTime = aLKNRTBizTransSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLKNRTBizTransSchema.getModifyDate());
        this.ModifyTime = aLKNRTBizTransSchema.getModifyTime();
        this.Bak1 = aLKNRTBizTransSchema.getBak1();
        this.Bak2 = aLKNRTBizTransSchema.getBak2();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.LKNRTBizID = rs.getLong("LKNRTBizID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("TransCode") == null )
                this.TransCode = null;
            else
                this.TransCode = rs.getString("TransCode").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            this.TranDate = rs.getDate("TranDate");
            if( rs.getString("ZoneNo") == null )
                this.ZoneNo = null;
            else
                this.ZoneNo = rs.getString("ZoneNo").trim();

            if( rs.getString("BankNode") == null )
                this.BankNode = null;
            else
                this.BankNode = rs.getString("BankNode").trim();

            if( rs.getString("TellerNo") == null )
                this.TellerNo = null;
            else
                this.TellerNo = rs.getString("TellerNo").trim();

            if( rs.getString("TransNo") == null )
                this.TransNo = null;
            else
                this.TransNo = rs.getString("TransNo").trim();

            if( rs.getString("ProposalNo") == null )
                this.ProposalNo = null;
            else
                this.ProposalNo = rs.getString("ProposalNo").trim();

            if( rs.getString("BanksaleChnl") == null )
                this.BanksaleChnl = null;
            else
                this.BanksaleChnl = rs.getString("BanksaleChnl").trim();

            if( rs.getString("UwType") == null )
                this.UwType = null;
            else
                this.UwType = rs.getString("UwType").trim();

            if( rs.getString("AppntName") == null )
                this.AppntName = null;
            else
                this.AppntName = rs.getString("AppntName").trim();

            if( rs.getString("AppntidType") == null )
                this.AppntidType = null;
            else
                this.AppntidType = rs.getString("AppntidType").trim();

            if( rs.getString("AppntidNo") == null )
                this.AppntidNo = null;
            else
                this.AppntidNo = rs.getString("AppntidNo").trim();

            if( rs.getString("AppntaccNo") == null )
                this.AppntaccNo = null;
            else
                this.AppntaccNo = rs.getString("AppntaccNo").trim();

            if( rs.getString("BizStatus") == null )
                this.BizStatus = null;
            else
                this.BizStatus = rs.getString("BizStatus").trim();

            if( rs.getString("UwStatus") == null )
                this.UwStatus = null;
            else
                this.UwStatus = rs.getString("UwStatus").trim();

            this.RespDate = rs.getDate("RespDate");
            if( rs.getString("RespTime") == null )
                this.RespTime = null;
            else
                this.RespTime = rs.getString("RespTime").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("Bak1") == null )
                this.Bak1 = null;
            else
                this.Bak1 = rs.getString("Bak1").trim();

            if( rs.getString("Bak2") == null )
                this.Bak2 = null;
            else
                this.Bak2 = rs.getString("Bak2").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKNRTBizTransSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LKNRTBizTransSchema getSchema() {
        LKNRTBizTransSchema aLKNRTBizTransSchema = new LKNRTBizTransSchema();
        aLKNRTBizTransSchema.setSchema(this);
        return aLKNRTBizTransSchema;
    }

    public LKNRTBizTransDB getDB() {
        LKNRTBizTransDB aDBOper = new LKNRTBizTransDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKNRTBizTrans描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(LKNRTBizID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( TranDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZoneNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TellerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BanksaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UwType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntidType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntidNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntaccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BizStatus)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UwStatus)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( RespDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RespTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak2));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKNRTBizTrans>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            LKNRTBizID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            TransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            TranDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER));
            ZoneNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            TellerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            BanksaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            UwType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            AppntidType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            AppntidNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            AppntaccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            BizStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            UwStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            RespDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            RespTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKNRTBizTransSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("LKNRTBizID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LKNRTBizID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("TranDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTranDate()));
        }
        if (FCode.equalsIgnoreCase("ZoneNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZoneNo));
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
        }
        if (FCode.equalsIgnoreCase("TellerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TellerNo));
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("BanksaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BanksaleChnl));
        }
        if (FCode.equalsIgnoreCase("UwType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UwType));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AppntidType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntidType));
        }
        if (FCode.equalsIgnoreCase("AppntidNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntidNo));
        }
        if (FCode.equalsIgnoreCase("AppntaccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntaccNo));
        }
        if (FCode.equalsIgnoreCase("BizStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BizStatus));
        }
        if (FCode.equalsIgnoreCase("UwStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UwStatus));
        }
        if (FCode.equalsIgnoreCase("RespDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRespDate()));
        }
        if (FCode.equalsIgnoreCase("RespTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RespTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(LKNRTBizID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TransCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTranDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ZoneNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BankNode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(TellerNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(TransNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ProposalNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(BanksaleChnl);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(UwType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AppntidType);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AppntidNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AppntaccNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(BizStatus);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(UwStatus);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRespDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(RespTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Bak1);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Bak2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("LKNRTBizID")) {
            if( FValue != null && !FValue.equals("")) {
                LKNRTBizID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransCode = FValue.trim();
            }
            else
                TransCode = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("TranDate")) {
            if(FValue != null && !FValue.equals("")) {
                TranDate = fDate.getDate( FValue );
            }
            else
                TranDate = null;
        }
        if (FCode.equalsIgnoreCase("ZoneNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZoneNo = FValue.trim();
            }
            else
                ZoneNo = null;
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankNode = FValue.trim();
            }
            else
                BankNode = null;
        }
        if (FCode.equalsIgnoreCase("TellerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TellerNo = FValue.trim();
            }
            else
                TellerNo = null;
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransNo = FValue.trim();
            }
            else
                TransNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("BanksaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                BanksaleChnl = FValue.trim();
            }
            else
                BanksaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("UwType")) {
            if( FValue != null && !FValue.equals(""))
            {
                UwType = FValue.trim();
            }
            else
                UwType = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AppntidType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntidType = FValue.trim();
            }
            else
                AppntidType = null;
        }
        if (FCode.equalsIgnoreCase("AppntidNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntidNo = FValue.trim();
            }
            else
                AppntidNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntaccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntaccNo = FValue.trim();
            }
            else
                AppntaccNo = null;
        }
        if (FCode.equalsIgnoreCase("BizStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                BizStatus = FValue.trim();
            }
            else
                BizStatus = null;
        }
        if (FCode.equalsIgnoreCase("UwStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                UwStatus = FValue.trim();
            }
            else
                UwStatus = null;
        }
        if (FCode.equalsIgnoreCase("RespDate")) {
            if(FValue != null && !FValue.equals("")) {
                RespDate = fDate.getDate( FValue );
            }
            else
                RespDate = null;
        }
        if (FCode.equalsIgnoreCase("RespTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                RespTime = FValue.trim();
            }
            else
                RespTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak1 = FValue.trim();
            }
            else
                Bak1 = null;
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak2 = FValue.trim();
            }
            else
                Bak2 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LKNRTBizTransSchema other = (LKNRTBizTransSchema)otherObject;
        return
            LKNRTBizID == other.getLKNRTBizID()
            && ShardingID.equals(other.getShardingID())
            && TransCode.equals(other.getTransCode())
            && BankCode.equals(other.getBankCode())
            && fDate.getString(TranDate).equals(other.getTranDate())
            && ZoneNo.equals(other.getZoneNo())
            && BankNode.equals(other.getBankNode())
            && TellerNo.equals(other.getTellerNo())
            && TransNo.equals(other.getTransNo())
            && ProposalNo.equals(other.getProposalNo())
            && BanksaleChnl.equals(other.getBanksaleChnl())
            && UwType.equals(other.getUwType())
            && AppntName.equals(other.getAppntName())
            && AppntidType.equals(other.getAppntidType())
            && AppntidNo.equals(other.getAppntidNo())
            && AppntaccNo.equals(other.getAppntaccNo())
            && BizStatus.equals(other.getBizStatus())
            && UwStatus.equals(other.getUwStatus())
            && fDate.getString(RespDate).equals(other.getRespDate())
            && RespTime.equals(other.getRespTime())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Bak1.equals(other.getBak1())
            && Bak2.equals(other.getBak2());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("LKNRTBizID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("TransCode") ) {
            return 2;
        }
        if( strFieldName.equals("BankCode") ) {
            return 3;
        }
        if( strFieldName.equals("TranDate") ) {
            return 4;
        }
        if( strFieldName.equals("ZoneNo") ) {
            return 5;
        }
        if( strFieldName.equals("BankNode") ) {
            return 6;
        }
        if( strFieldName.equals("TellerNo") ) {
            return 7;
        }
        if( strFieldName.equals("TransNo") ) {
            return 8;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 9;
        }
        if( strFieldName.equals("BanksaleChnl") ) {
            return 10;
        }
        if( strFieldName.equals("UwType") ) {
            return 11;
        }
        if( strFieldName.equals("AppntName") ) {
            return 12;
        }
        if( strFieldName.equals("AppntidType") ) {
            return 13;
        }
        if( strFieldName.equals("AppntidNo") ) {
            return 14;
        }
        if( strFieldName.equals("AppntaccNo") ) {
            return 15;
        }
        if( strFieldName.equals("BizStatus") ) {
            return 16;
        }
        if( strFieldName.equals("UwStatus") ) {
            return 17;
        }
        if( strFieldName.equals("RespDate") ) {
            return 18;
        }
        if( strFieldName.equals("RespTime") ) {
            return 19;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 20;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 21;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 23;
        }
        if( strFieldName.equals("Bak1") ) {
            return 24;
        }
        if( strFieldName.equals("Bak2") ) {
            return 25;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "LKNRTBizID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "TransCode";
                break;
            case 3:
                strFieldName = "BankCode";
                break;
            case 4:
                strFieldName = "TranDate";
                break;
            case 5:
                strFieldName = "ZoneNo";
                break;
            case 6:
                strFieldName = "BankNode";
                break;
            case 7:
                strFieldName = "TellerNo";
                break;
            case 8:
                strFieldName = "TransNo";
                break;
            case 9:
                strFieldName = "ProposalNo";
                break;
            case 10:
                strFieldName = "BanksaleChnl";
                break;
            case 11:
                strFieldName = "UwType";
                break;
            case 12:
                strFieldName = "AppntName";
                break;
            case 13:
                strFieldName = "AppntidType";
                break;
            case 14:
                strFieldName = "AppntidNo";
                break;
            case 15:
                strFieldName = "AppntaccNo";
                break;
            case 16:
                strFieldName = "BizStatus";
                break;
            case 17:
                strFieldName = "UwStatus";
                break;
            case 18:
                strFieldName = "RespDate";
                break;
            case 19:
                strFieldName = "RespTime";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "Bak1";
                break;
            case 25:
                strFieldName = "Bak2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "LKNRTBIZID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "TRANSCODE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "TRANDATE":
                return Schema.TYPE_DATE;
            case "ZONENO":
                return Schema.TYPE_STRING;
            case "BANKNODE":
                return Schema.TYPE_STRING;
            case "TELLERNO":
                return Schema.TYPE_STRING;
            case "TRANSNO":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "BANKSALECHNL":
                return Schema.TYPE_STRING;
            case "UWTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "APPNTIDTYPE":
                return Schema.TYPE_STRING;
            case "APPNTIDNO":
                return Schema.TYPE_STRING;
            case "APPNTACCNO":
                return Schema.TYPE_STRING;
            case "BIZSTATUS":
                return Schema.TYPE_STRING;
            case "UWSTATUS":
                return Schema.TYPE_STRING;
            case "RESPDATE":
                return Schema.TYPE_DATE;
            case "RESPTIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DATE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
