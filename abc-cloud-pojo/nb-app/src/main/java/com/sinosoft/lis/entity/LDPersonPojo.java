/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDPersonPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LDPersonPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long PersonID; 
    /** Shardingid */
    private String ShardingID; 
    /** 客户号码 */
    private String CustomerNo; 
    /** 客户姓名 */
    private String Name; 
    /** 客户性别 */
    private String Sex; 
    /** 客户出生日期 */
    private String  Birthday;
    /** 证件类型 */
    private String IDType; 
    /** 证件号码 */
    private String IDNo; 
    /** 密码 */
    private String Password; 
    /** 国籍 */
    private String NativePlace; 
    /** 民族 */
    private String Nationality; 
    /** 户口所在地 */
    private String RgtAddress; 
    /** 婚姻状况 */
    private String Marriage; 
    /** 结婚日期 */
    private String  MarriageDate;
    /** 健康状况 */
    private String Health; 
    /** 身高 */
    private double Stature; 
    /** 体重 */
    private double Avoirdupois; 
    /** 学历 */
    private String Degree; 
    /** 信用等级 */
    private String CreditGrade; 
    /** 其它证件类型 */
    private String OthIDType; 
    /** 其它证件号码 */
    private String OthIDNo; 
    /** Ic卡号 */
    private String ICNo; 
    /** 单位编码 */
    private String GrpNo; 
    /** 入司日期 */
    private String  JoinCompanyDate;
    /** 参加工作日期 */
    private String  StartWorkDate;
    /** 职位 */
    private String Position; 
    /** 工资 */
    private double Salary; 
    /** 职业类别 */
    private String OccupationType; 
    /** 职业代码 */
    private String OccupationCode; 
    /** 职业（工种） */
    private String WorkType; 
    /** 兼职（工种） */
    private String PluralityType; 
    /** 死亡日期 */
    private String  DeathDate;
    /** 是否吸烟标志 */
    private String SmokeFlag; 
    /** 黑名单标记 */
    private String BlacklistFlag; 
    /** 属性 */
    private String Proterty; 
    /** 备注 */
    private String Remark; 
    /** 状态 */
    private String State; 
    /** Vip值 */
    private String VIPValue; 
    /** 操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 单位名称 */
    private String GrpName; 
    /** 驾照 */
    private String License; 
    /** 驾照类型 */
    private String LicenseType; 
    /** 社保登记号 */
    private String SocialInsuNo; 
    /** 证件有效期 */
    private String IdValiDate; 
    /** 是否有摩托车驾照 */
    private String HaveMotorcycleLicence; 
    /** 兼职 */
    private String PartTimeJob; 
    /** 医保标识 */
    private String HealthFlag; 
    /** 在职标识 */
    private String ServiceMark; 
    /** Firstname */
    private String FirstName; 
    /** Lastname */
    private String LastName; 
    /** 客户级别 */
    private String CUSLevel; 
    /** 社保标记 */
    private String SSFlag; 
    /** 居民类型 */
    private String RgtTpye; 
    /** 美国纳税人识别号 */
    private String TINNO; 
    /** 是否为美国纳税义务的个人 */
    private String TINFlag; 
    /** 新客户标识 */
    private String NewCustomerFlag; 


    public static final int FIELDNUM = 60;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getPersonID() {
        return PersonID;
    }
    public void setPersonID(long aPersonID) {
        PersonID = aPersonID;
    }
    public void setPersonID(String aPersonID) {
        if (aPersonID != null && !aPersonID.equals("")) {
            PersonID = new Long(aPersonID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        return Birthday;
    }
    public void setBirthday(String aBirthday) {
        Birthday = aBirthday;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getMarriageDate() {
        return MarriageDate;
    }
    public void setMarriageDate(String aMarriageDate) {
        MarriageDate = aMarriageDate;
    }
    public String getHealth() {
        return Health;
    }
    public void setHealth(String aHealth) {
        Health = aHealth;
    }
    public double getStature() {
        return Stature;
    }
    public void setStature(double aStature) {
        Stature = aStature;
    }
    public void setStature(String aStature) {
        if (aStature != null && !aStature.equals("")) {
            Double tDouble = new Double(aStature);
            double d = tDouble.doubleValue();
            Stature = d;
        }
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }
    public void setAvoirdupois(double aAvoirdupois) {
        Avoirdupois = aAvoirdupois;
    }
    public void setAvoirdupois(String aAvoirdupois) {
        if (aAvoirdupois != null && !aAvoirdupois.equals("")) {
            Double tDouble = new Double(aAvoirdupois);
            double d = tDouble.doubleValue();
            Avoirdupois = d;
        }
    }

    public String getDegree() {
        return Degree;
    }
    public void setDegree(String aDegree) {
        Degree = aDegree;
    }
    public String getCreditGrade() {
        return CreditGrade;
    }
    public void setCreditGrade(String aCreditGrade) {
        CreditGrade = aCreditGrade;
    }
    public String getOthIDType() {
        return OthIDType;
    }
    public void setOthIDType(String aOthIDType) {
        OthIDType = aOthIDType;
    }
    public String getOthIDNo() {
        return OthIDNo;
    }
    public void setOthIDNo(String aOthIDNo) {
        OthIDNo = aOthIDNo;
    }
    public String getICNo() {
        return ICNo;
    }
    public void setICNo(String aICNo) {
        ICNo = aICNo;
    }
    public String getGrpNo() {
        return GrpNo;
    }
    public void setGrpNo(String aGrpNo) {
        GrpNo = aGrpNo;
    }
    public String getJoinCompanyDate() {
        return JoinCompanyDate;
    }
    public void setJoinCompanyDate(String aJoinCompanyDate) {
        JoinCompanyDate = aJoinCompanyDate;
    }
    public String getStartWorkDate() {
        return StartWorkDate;
    }
    public void setStartWorkDate(String aStartWorkDate) {
        StartWorkDate = aStartWorkDate;
    }
    public String getPosition() {
        return Position;
    }
    public void setPosition(String aPosition) {
        Position = aPosition;
    }
    public double getSalary() {
        return Salary;
    }
    public void setSalary(double aSalary) {
        Salary = aSalary;
    }
    public void setSalary(String aSalary) {
        if (aSalary != null && !aSalary.equals("")) {
            Double tDouble = new Double(aSalary);
            double d = tDouble.doubleValue();
            Salary = d;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getWorkType() {
        return WorkType;
    }
    public void setWorkType(String aWorkType) {
        WorkType = aWorkType;
    }
    public String getPluralityType() {
        return PluralityType;
    }
    public void setPluralityType(String aPluralityType) {
        PluralityType = aPluralityType;
    }
    public String getDeathDate() {
        return DeathDate;
    }
    public void setDeathDate(String aDeathDate) {
        DeathDate = aDeathDate;
    }
    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getBlacklistFlag() {
        return BlacklistFlag;
    }
    public void setBlacklistFlag(String aBlacklistFlag) {
        BlacklistFlag = aBlacklistFlag;
    }
    public String getProterty() {
        return Proterty;
    }
    public void setProterty(String aProterty) {
        Proterty = aProterty;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getVIPValue() {
        return VIPValue;
    }
    public void setVIPValue(String aVIPValue) {
        VIPValue = aVIPValue;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getLicenseType() {
        return LicenseType;
    }
    public void setLicenseType(String aLicenseType) {
        LicenseType = aLicenseType;
    }
    public String getSocialInsuNo() {
        return SocialInsuNo;
    }
    public void setSocialInsuNo(String aSocialInsuNo) {
        SocialInsuNo = aSocialInsuNo;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }
    public void setHaveMotorcycleLicence(String aHaveMotorcycleLicence) {
        HaveMotorcycleLicence = aHaveMotorcycleLicence;
    }
    public String getPartTimeJob() {
        return PartTimeJob;
    }
    public void setPartTimeJob(String aPartTimeJob) {
        PartTimeJob = aPartTimeJob;
    }
    public String getHealthFlag() {
        return HealthFlag;
    }
    public void setHealthFlag(String aHealthFlag) {
        HealthFlag = aHealthFlag;
    }
    public String getServiceMark() {
        return ServiceMark;
    }
    public void setServiceMark(String aServiceMark) {
        ServiceMark = aServiceMark;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String aFirstName) {
        FirstName = aFirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String aLastName) {
        LastName = aLastName;
    }
    public String getCUSLevel() {
        return CUSLevel;
    }
    public void setCUSLevel(String aCUSLevel) {
        CUSLevel = aCUSLevel;
    }
    public String getSSFlag() {
        return SSFlag;
    }
    public void setSSFlag(String aSSFlag) {
        SSFlag = aSSFlag;
    }
    public String getRgtTpye() {
        return RgtTpye;
    }
    public void setRgtTpye(String aRgtTpye) {
        RgtTpye = aRgtTpye;
    }
    public String getTINNO() {
        return TINNO;
    }
    public void setTINNO(String aTINNO) {
        TINNO = aTINNO;
    }
    public String getTINFlag() {
        return TINFlag;
    }
    public void setTINFlag(String aTINFlag) {
        TINFlag = aTINFlag;
    }
    public String getNewCustomerFlag() {
        return NewCustomerFlag;
    }
    public void setNewCustomerFlag(String aNewCustomerFlag) {
        NewCustomerFlag = aNewCustomerFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PersonID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 2;
        }
        if( strFieldName.equals("Name") ) {
            return 3;
        }
        if( strFieldName.equals("Sex") ) {
            return 4;
        }
        if( strFieldName.equals("Birthday") ) {
            return 5;
        }
        if( strFieldName.equals("IDType") ) {
            return 6;
        }
        if( strFieldName.equals("IDNo") ) {
            return 7;
        }
        if( strFieldName.equals("Password") ) {
            return 8;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 9;
        }
        if( strFieldName.equals("Nationality") ) {
            return 10;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 11;
        }
        if( strFieldName.equals("Marriage") ) {
            return 12;
        }
        if( strFieldName.equals("MarriageDate") ) {
            return 13;
        }
        if( strFieldName.equals("Health") ) {
            return 14;
        }
        if( strFieldName.equals("Stature") ) {
            return 15;
        }
        if( strFieldName.equals("Avoirdupois") ) {
            return 16;
        }
        if( strFieldName.equals("Degree") ) {
            return 17;
        }
        if( strFieldName.equals("CreditGrade") ) {
            return 18;
        }
        if( strFieldName.equals("OthIDType") ) {
            return 19;
        }
        if( strFieldName.equals("OthIDNo") ) {
            return 20;
        }
        if( strFieldName.equals("ICNo") ) {
            return 21;
        }
        if( strFieldName.equals("GrpNo") ) {
            return 22;
        }
        if( strFieldName.equals("JoinCompanyDate") ) {
            return 23;
        }
        if( strFieldName.equals("StartWorkDate") ) {
            return 24;
        }
        if( strFieldName.equals("Position") ) {
            return 25;
        }
        if( strFieldName.equals("Salary") ) {
            return 26;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 27;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 28;
        }
        if( strFieldName.equals("WorkType") ) {
            return 29;
        }
        if( strFieldName.equals("PluralityType") ) {
            return 30;
        }
        if( strFieldName.equals("DeathDate") ) {
            return 31;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 32;
        }
        if( strFieldName.equals("BlacklistFlag") ) {
            return 33;
        }
        if( strFieldName.equals("Proterty") ) {
            return 34;
        }
        if( strFieldName.equals("Remark") ) {
            return 35;
        }
        if( strFieldName.equals("State") ) {
            return 36;
        }
        if( strFieldName.equals("VIPValue") ) {
            return 37;
        }
        if( strFieldName.equals("Operator") ) {
            return 38;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 39;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 40;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 41;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 42;
        }
        if( strFieldName.equals("GrpName") ) {
            return 43;
        }
        if( strFieldName.equals("License") ) {
            return 44;
        }
        if( strFieldName.equals("LicenseType") ) {
            return 45;
        }
        if( strFieldName.equals("SocialInsuNo") ) {
            return 46;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 47;
        }
        if( strFieldName.equals("HaveMotorcycleLicence") ) {
            return 48;
        }
        if( strFieldName.equals("PartTimeJob") ) {
            return 49;
        }
        if( strFieldName.equals("HealthFlag") ) {
            return 50;
        }
        if( strFieldName.equals("ServiceMark") ) {
            return 51;
        }
        if( strFieldName.equals("FirstName") ) {
            return 52;
        }
        if( strFieldName.equals("LastName") ) {
            return 53;
        }
        if( strFieldName.equals("CUSLevel") ) {
            return 54;
        }
        if( strFieldName.equals("SSFlag") ) {
            return 55;
        }
        if( strFieldName.equals("RgtTpye") ) {
            return 56;
        }
        if( strFieldName.equals("TINNO") ) {
            return 57;
        }
        if( strFieldName.equals("TINFlag") ) {
            return 58;
        }
        if( strFieldName.equals("NewCustomerFlag") ) {
            return 59;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PersonID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "CustomerNo";
                break;
            case 3:
                strFieldName = "Name";
                break;
            case 4:
                strFieldName = "Sex";
                break;
            case 5:
                strFieldName = "Birthday";
                break;
            case 6:
                strFieldName = "IDType";
                break;
            case 7:
                strFieldName = "IDNo";
                break;
            case 8:
                strFieldName = "Password";
                break;
            case 9:
                strFieldName = "NativePlace";
                break;
            case 10:
                strFieldName = "Nationality";
                break;
            case 11:
                strFieldName = "RgtAddress";
                break;
            case 12:
                strFieldName = "Marriage";
                break;
            case 13:
                strFieldName = "MarriageDate";
                break;
            case 14:
                strFieldName = "Health";
                break;
            case 15:
                strFieldName = "Stature";
                break;
            case 16:
                strFieldName = "Avoirdupois";
                break;
            case 17:
                strFieldName = "Degree";
                break;
            case 18:
                strFieldName = "CreditGrade";
                break;
            case 19:
                strFieldName = "OthIDType";
                break;
            case 20:
                strFieldName = "OthIDNo";
                break;
            case 21:
                strFieldName = "ICNo";
                break;
            case 22:
                strFieldName = "GrpNo";
                break;
            case 23:
                strFieldName = "JoinCompanyDate";
                break;
            case 24:
                strFieldName = "StartWorkDate";
                break;
            case 25:
                strFieldName = "Position";
                break;
            case 26:
                strFieldName = "Salary";
                break;
            case 27:
                strFieldName = "OccupationType";
                break;
            case 28:
                strFieldName = "OccupationCode";
                break;
            case 29:
                strFieldName = "WorkType";
                break;
            case 30:
                strFieldName = "PluralityType";
                break;
            case 31:
                strFieldName = "DeathDate";
                break;
            case 32:
                strFieldName = "SmokeFlag";
                break;
            case 33:
                strFieldName = "BlacklistFlag";
                break;
            case 34:
                strFieldName = "Proterty";
                break;
            case 35:
                strFieldName = "Remark";
                break;
            case 36:
                strFieldName = "State";
                break;
            case 37:
                strFieldName = "VIPValue";
                break;
            case 38:
                strFieldName = "Operator";
                break;
            case 39:
                strFieldName = "MakeDate";
                break;
            case 40:
                strFieldName = "MakeTime";
                break;
            case 41:
                strFieldName = "ModifyDate";
                break;
            case 42:
                strFieldName = "ModifyTime";
                break;
            case 43:
                strFieldName = "GrpName";
                break;
            case 44:
                strFieldName = "License";
                break;
            case 45:
                strFieldName = "LicenseType";
                break;
            case 46:
                strFieldName = "SocialInsuNo";
                break;
            case 47:
                strFieldName = "IdValiDate";
                break;
            case 48:
                strFieldName = "HaveMotorcycleLicence";
                break;
            case 49:
                strFieldName = "PartTimeJob";
                break;
            case 50:
                strFieldName = "HealthFlag";
                break;
            case 51:
                strFieldName = "ServiceMark";
                break;
            case 52:
                strFieldName = "FirstName";
                break;
            case 53:
                strFieldName = "LastName";
                break;
            case 54:
                strFieldName = "CUSLevel";
                break;
            case 55:
                strFieldName = "SSFlag";
                break;
            case 56:
                strFieldName = "RgtTpye";
                break;
            case 57:
                strFieldName = "TINNO";
                break;
            case 58:
                strFieldName = "TINFlag";
                break;
            case 59:
                strFieldName = "NewCustomerFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PERSONID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "MARRIAGEDATE":
                return Schema.TYPE_STRING;
            case "HEALTH":
                return Schema.TYPE_STRING;
            case "STATURE":
                return Schema.TYPE_DOUBLE;
            case "AVOIRDUPOIS":
                return Schema.TYPE_DOUBLE;
            case "DEGREE":
                return Schema.TYPE_STRING;
            case "CREDITGRADE":
                return Schema.TYPE_STRING;
            case "OTHIDTYPE":
                return Schema.TYPE_STRING;
            case "OTHIDNO":
                return Schema.TYPE_STRING;
            case "ICNO":
                return Schema.TYPE_STRING;
            case "GRPNO":
                return Schema.TYPE_STRING;
            case "JOINCOMPANYDATE":
                return Schema.TYPE_STRING;
            case "STARTWORKDATE":
                return Schema.TYPE_STRING;
            case "POSITION":
                return Schema.TYPE_STRING;
            case "SALARY":
                return Schema.TYPE_DOUBLE;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "WORKTYPE":
                return Schema.TYPE_STRING;
            case "PLURALITYTYPE":
                return Schema.TYPE_STRING;
            case "DEATHDATE":
                return Schema.TYPE_STRING;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "BLACKLISTFLAG":
                return Schema.TYPE_STRING;
            case "PROTERTY":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "VIPVALUE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "LICENSETYPE":
                return Schema.TYPE_STRING;
            case "SOCIALINSUNO":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "HAVEMOTORCYCLELICENCE":
                return Schema.TYPE_STRING;
            case "PARTTIMEJOB":
                return Schema.TYPE_STRING;
            case "HEALTHFLAG":
                return Schema.TYPE_STRING;
            case "SERVICEMARK":
                return Schema.TYPE_STRING;
            case "FIRSTNAME":
                return Schema.TYPE_STRING;
            case "LASTNAME":
                return Schema.TYPE_STRING;
            case "CUSLEVEL":
                return Schema.TYPE_STRING;
            case "SSFLAG":
                return Schema.TYPE_STRING;
            case "RGTTPYE":
                return Schema.TYPE_STRING;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            case "NEWCUSTOMERFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_DOUBLE;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_DOUBLE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PersonID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarriageDate));
        }
        if (FCode.equalsIgnoreCase("Health")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Health));
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stature));
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Avoirdupois));
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
        }
        if (FCode.equalsIgnoreCase("OthIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDType));
        }
        if (FCode.equalsIgnoreCase("OthIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDNo));
        }
        if (FCode.equalsIgnoreCase("ICNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ICNo));
        }
        if (FCode.equalsIgnoreCase("GrpNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JoinCompanyDate));
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartWorkDate));
        }
        if (FCode.equalsIgnoreCase("Position")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
        }
        if (FCode.equalsIgnoreCase("DeathDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeathDate));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistFlag));
        }
        if (FCode.equalsIgnoreCase("Proterty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Proterty));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIPValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseType));
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuNo));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HaveMotorcycleLicence));
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PartTimeJob));
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthFlag));
        }
        if (FCode.equalsIgnoreCase("ServiceMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceMark));
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstName));
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastName));
        }
        if (FCode.equalsIgnoreCase("CUSLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUSLevel));
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SSFlag));
        }
        if (FCode.equalsIgnoreCase("RgtTpye")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtTpye));
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINNO));
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINFlag));
        }
        if (FCode.equalsIgnoreCase("NewCustomerFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewCustomerFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PersonID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 3:
                strFieldValue = String.valueOf(Name);
                break;
            case 4:
                strFieldValue = String.valueOf(Sex);
                break;
            case 5:
                strFieldValue = String.valueOf(Birthday);
                break;
            case 6:
                strFieldValue = String.valueOf(IDType);
                break;
            case 7:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 8:
                strFieldValue = String.valueOf(Password);
                break;
            case 9:
                strFieldValue = String.valueOf(NativePlace);
                break;
            case 10:
                strFieldValue = String.valueOf(Nationality);
                break;
            case 11:
                strFieldValue = String.valueOf(RgtAddress);
                break;
            case 12:
                strFieldValue = String.valueOf(Marriage);
                break;
            case 13:
                strFieldValue = String.valueOf(MarriageDate);
                break;
            case 14:
                strFieldValue = String.valueOf(Health);
                break;
            case 15:
                strFieldValue = String.valueOf(Stature);
                break;
            case 16:
                strFieldValue = String.valueOf(Avoirdupois);
                break;
            case 17:
                strFieldValue = String.valueOf(Degree);
                break;
            case 18:
                strFieldValue = String.valueOf(CreditGrade);
                break;
            case 19:
                strFieldValue = String.valueOf(OthIDType);
                break;
            case 20:
                strFieldValue = String.valueOf(OthIDNo);
                break;
            case 21:
                strFieldValue = String.valueOf(ICNo);
                break;
            case 22:
                strFieldValue = String.valueOf(GrpNo);
                break;
            case 23:
                strFieldValue = String.valueOf(JoinCompanyDate);
                break;
            case 24:
                strFieldValue = String.valueOf(StartWorkDate);
                break;
            case 25:
                strFieldValue = String.valueOf(Position);
                break;
            case 26:
                strFieldValue = String.valueOf(Salary);
                break;
            case 27:
                strFieldValue = String.valueOf(OccupationType);
                break;
            case 28:
                strFieldValue = String.valueOf(OccupationCode);
                break;
            case 29:
                strFieldValue = String.valueOf(WorkType);
                break;
            case 30:
                strFieldValue = String.valueOf(PluralityType);
                break;
            case 31:
                strFieldValue = String.valueOf(DeathDate);
                break;
            case 32:
                strFieldValue = String.valueOf(SmokeFlag);
                break;
            case 33:
                strFieldValue = String.valueOf(BlacklistFlag);
                break;
            case 34:
                strFieldValue = String.valueOf(Proterty);
                break;
            case 35:
                strFieldValue = String.valueOf(Remark);
                break;
            case 36:
                strFieldValue = String.valueOf(State);
                break;
            case 37:
                strFieldValue = String.valueOf(VIPValue);
                break;
            case 38:
                strFieldValue = String.valueOf(Operator);
                break;
            case 39:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 40:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 41:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 42:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 43:
                strFieldValue = String.valueOf(GrpName);
                break;
            case 44:
                strFieldValue = String.valueOf(License);
                break;
            case 45:
                strFieldValue = String.valueOf(LicenseType);
                break;
            case 46:
                strFieldValue = String.valueOf(SocialInsuNo);
                break;
            case 47:
                strFieldValue = String.valueOf(IdValiDate);
                break;
            case 48:
                strFieldValue = String.valueOf(HaveMotorcycleLicence);
                break;
            case 49:
                strFieldValue = String.valueOf(PartTimeJob);
                break;
            case 50:
                strFieldValue = String.valueOf(HealthFlag);
                break;
            case 51:
                strFieldValue = String.valueOf(ServiceMark);
                break;
            case 52:
                strFieldValue = String.valueOf(FirstName);
                break;
            case 53:
                strFieldValue = String.valueOf(LastName);
                break;
            case 54:
                strFieldValue = String.valueOf(CUSLevel);
                break;
            case 55:
                strFieldValue = String.valueOf(SSFlag);
                break;
            case 56:
                strFieldValue = String.valueOf(RgtTpye);
                break;
            case 57:
                strFieldValue = String.valueOf(TINNO);
                break;
            case 58:
                strFieldValue = String.valueOf(TINFlag);
                break;
            case 59:
                strFieldValue = String.valueOf(NewCustomerFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PersonID")) {
            if( FValue != null && !FValue.equals("")) {
                PersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                Birthday = FValue.trim();
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MarriageDate = FValue.trim();
            }
            else
                MarriageDate = null;
        }
        if (FCode.equalsIgnoreCase("Health")) {
            if( FValue != null && !FValue.equals(""))
            {
                Health = FValue.trim();
            }
            else
                Health = null;
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Stature = d;
            }
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Avoirdupois = d;
            }
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            if( FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
                Degree = null;
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
                CreditGrade = null;
        }
        if (FCode.equalsIgnoreCase("OthIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthIDType = FValue.trim();
            }
            else
                OthIDType = null;
        }
        if (FCode.equalsIgnoreCase("OthIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthIDNo = FValue.trim();
            }
            else
                OthIDNo = null;
        }
        if (FCode.equalsIgnoreCase("ICNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ICNo = FValue.trim();
            }
            else
                ICNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNo = FValue.trim();
            }
            else
                GrpNo = null;
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JoinCompanyDate = FValue.trim();
            }
            else
                JoinCompanyDate = null;
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartWorkDate = FValue.trim();
            }
            else
                StartWorkDate = null;
        }
        if (FCode.equalsIgnoreCase("Position")) {
            if( FValue != null && !FValue.equals(""))
            {
                Position = FValue.trim();
            }
            else
                Position = null;
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Salary = d;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkType = FValue.trim();
            }
            else
                WorkType = null;
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
                PluralityType = null;
        }
        if (FCode.equalsIgnoreCase("DeathDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeathDate = FValue.trim();
            }
            else
                DeathDate = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistFlag = FValue.trim();
            }
            else
                BlacklistFlag = null;
        }
        if (FCode.equalsIgnoreCase("Proterty")) {
            if( FValue != null && !FValue.equals(""))
            {
                Proterty = FValue.trim();
            }
            else
                Proterty = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIPValue = FValue.trim();
            }
            else
                VIPValue = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseType = FValue.trim();
            }
            else
                LicenseType = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuNo = FValue.trim();
            }
            else
                SocialInsuNo = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            if( FValue != null && !FValue.equals(""))
            {
                HaveMotorcycleLicence = FValue.trim();
            }
            else
                HaveMotorcycleLicence = null;
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            if( FValue != null && !FValue.equals(""))
            {
                PartTimeJob = FValue.trim();
            }
            else
                PartTimeJob = null;
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthFlag = FValue.trim();
            }
            else
                HealthFlag = null;
        }
        if (FCode.equalsIgnoreCase("ServiceMark")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceMark = FValue.trim();
            }
            else
                ServiceMark = null;
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstName = FValue.trim();
            }
            else
                FirstName = null;
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastName = FValue.trim();
            }
            else
                LastName = null;
        }
        if (FCode.equalsIgnoreCase("CUSLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUSLevel = FValue.trim();
            }
            else
                CUSLevel = null;
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SSFlag = FValue.trim();
            }
            else
                SSFlag = null;
        }
        if (FCode.equalsIgnoreCase("RgtTpye")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtTpye = FValue.trim();
            }
            else
                RgtTpye = null;
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINNO = FValue.trim();
            }
            else
                TINNO = null;
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINFlag = FValue.trim();
            }
            else
                TINFlag = null;
        }
        if (FCode.equalsIgnoreCase("NewCustomerFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewCustomerFlag = FValue.trim();
            }
            else
                NewCustomerFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LDPersonPojo [" +
            "PersonID="+PersonID +
            ", ShardingID="+ShardingID +
            ", CustomerNo="+CustomerNo +
            ", Name="+Name +
            ", Sex="+Sex +
            ", Birthday="+Birthday +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", Password="+Password +
            ", NativePlace="+NativePlace +
            ", Nationality="+Nationality +
            ", RgtAddress="+RgtAddress +
            ", Marriage="+Marriage +
            ", MarriageDate="+MarriageDate +
            ", Health="+Health +
            ", Stature="+Stature +
            ", Avoirdupois="+Avoirdupois +
            ", Degree="+Degree +
            ", CreditGrade="+CreditGrade +
            ", OthIDType="+OthIDType +
            ", OthIDNo="+OthIDNo +
            ", ICNo="+ICNo +
            ", GrpNo="+GrpNo +
            ", JoinCompanyDate="+JoinCompanyDate +
            ", StartWorkDate="+StartWorkDate +
            ", Position="+Position +
            ", Salary="+Salary +
            ", OccupationType="+OccupationType +
            ", OccupationCode="+OccupationCode +
            ", WorkType="+WorkType +
            ", PluralityType="+PluralityType +
            ", DeathDate="+DeathDate +
            ", SmokeFlag="+SmokeFlag +
            ", BlacklistFlag="+BlacklistFlag +
            ", Proterty="+Proterty +
            ", Remark="+Remark +
            ", State="+State +
            ", VIPValue="+VIPValue +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", GrpName="+GrpName +
            ", License="+License +
            ", LicenseType="+LicenseType +
            ", SocialInsuNo="+SocialInsuNo +
            ", IdValiDate="+IdValiDate +
            ", HaveMotorcycleLicence="+HaveMotorcycleLicence +
            ", PartTimeJob="+PartTimeJob +
            ", HealthFlag="+HealthFlag +
            ", ServiceMark="+ServiceMark +
            ", FirstName="+FirstName +
            ", LastName="+LastName +
            ", CUSLevel="+CUSLevel +
            ", SSFlag="+SSFlag +
            ", RgtTpye="+RgtTpye +
            ", TINNO="+TINNO +
            ", TINFlag="+TINFlag +
            ", NewCustomerFlag="+NewCustomerFlag +"]";
    }
}
