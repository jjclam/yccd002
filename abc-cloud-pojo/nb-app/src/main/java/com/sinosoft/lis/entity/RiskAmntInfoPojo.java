package com.sinosoft.lis.entity;

import java.io.Serializable;

/**
 * @Author: zhuyiming
 * @Description: 用于风险保额累计
 * @Date: Created in 12:42 2017/9/28
 * @Modified By
 */
public class RiskAmntInfoPojo implements Serializable {
    /**
     * L：累计寿险风险保额 ，H：累计重疾风险保额 ，Y：累计意外险风险保额 CL:寿险保单保额 ，CH:累计重疾保单保额 ，CY:意外险保单保额
     * PL：寿险体检风险保额 ，LI：人身险保额 ，SR：累计风险保额 M：住院医疗风险保额 ，N：意外医疗风险保额 ，J：津贴险风险保额
     * ZJ:累计自驾车风险保额 ，HC:防癌险风险保额 ，ZB:累计再保风险保额
     */
    private String riskTypeL;
    private String riskTypeH;
    private String riskTypeY;
    private String riskTypeCL;
    private String riskTypeCH;
    private String riskTypeCY;
    private String riskTypePL;
    private String riskTypeLI;
    private String riskTypeSR;
    private String riskTypeM;
    private String riskTypeN;
    private String riskTypeJ;
    private String riskTypeZJ;
    private String riskTypeHC;
    private String riskTypeZB;

    /**
     * BL：累计寿险保单保额 ， BH：累计健康险保单保额 ， BY：累计意外险保单保额 ， BJ：累计自驾车保单保额 ， BC：累计防癌险保单保额
     */
    private String contRiskTypeBL;
    private String contRiskTypeBH;
    private String contRiskTypeBY;
    private String contRiskTypeBJ;
    private String contRiskTypeBC;

    /**
     * 1：寿险风险保额 ， 2：重疾险风险保额 ， 4：意外险风险保额
     */
    private String heathType1;
    private String heathType2;
    private String heathType4;

    /**
     * 身故累计
     */
    private String appRiskCodeDeath;

    public String getAppRiskCodeDeath() {
        return appRiskCodeDeath;
    }

    public void setAppRiskCodeDeath(String appRiskCodeDeath) {
        this.appRiskCodeDeath = appRiskCodeDeath;
    }

    public String getRiskTypeL() {
        return riskTypeL;
    }

    public void setRiskTypeL(String riskTypeL) {
        this.riskTypeL = riskTypeL;
    }

    public String getRiskTypeH() {
        return riskTypeH;
    }

    public void setRiskTypeH(String riskTypeH) {
        this.riskTypeH = riskTypeH;
    }

    public String getRiskTypeY() {
        return riskTypeY;
    }

    public void setRiskTypeY(String riskTypeY) {
        this.riskTypeY = riskTypeY;
    }

    public String getRiskTypeCL() {
        return riskTypeCL;
    }

    public void setRiskTypeCL(String riskTypeCL) {
        this.riskTypeCL = riskTypeCL;
    }

    public String getRiskTypeCH() {
        return riskTypeCH;
    }

    public void setRiskTypeCH(String riskTypeCH) {
        this.riskTypeCH = riskTypeCH;
    }

    public String getRiskTypeCY() {
        return riskTypeCY;
    }

    public void setRiskTypeCY(String riskTypeCY) {
        this.riskTypeCY = riskTypeCY;
    }

    public String getRiskTypePL() {
        return riskTypePL;
    }

    public void setRiskTypePL(String riskTypePL) {
        this.riskTypePL = riskTypePL;
    }

    public String getRiskTypeLI() {
        return riskTypeLI;
    }

    public void setRiskTypeLI(String riskTypeLI) {
        this.riskTypeLI = riskTypeLI;
    }

    public String getRiskTypeSR() {
        return riskTypeSR;
    }

    public void setRiskTypeSR(String riskTypeSR) {
        this.riskTypeSR = riskTypeSR;
    }

    public String getRiskTypeM() {
        return riskTypeM;
    }

    public void setRiskTypeM(String riskTypeM) {
        this.riskTypeM = riskTypeM;
    }

    public String getRiskTypeN() {
        return riskTypeN;
    }

    public void setRiskTypeN(String riskTypeN) {
        this.riskTypeN = riskTypeN;
    }

    public String getRiskTypeJ() {
        return riskTypeJ;
    }

    public void setRiskTypeJ(String riskTypeJ) {
        this.riskTypeJ = riskTypeJ;
    }

    public String getRiskTypeZJ() {
        return riskTypeZJ;
    }

    public void setRiskTypeZJ(String riskTypeZJ) {
        this.riskTypeZJ = riskTypeZJ;
    }

    public String getRiskTypeHC() {
        return riskTypeHC;
    }

    public void setRiskTypeHC(String riskTypeHC) {
        this.riskTypeHC = riskTypeHC;
    }

    public String getRiskTypeZB() {
        return riskTypeZB;
    }

    public void setRiskTypeZB(String riskTypeZB) {
        this.riskTypeZB = riskTypeZB;
    }

    public String getContRiskTypeBL() {
        return contRiskTypeBL;
    }

    public void setContRiskTypeBL(String contRiskTypeBL) {
        this.contRiskTypeBL = contRiskTypeBL;
    }

    public String getContRiskTypeBH() {
        return contRiskTypeBH;
    }

    public void setContRiskTypeBH(String contRiskTypeBH) {
        this.contRiskTypeBH = contRiskTypeBH;
    }

    public String getContRiskTypeBY() {
        return contRiskTypeBY;
    }

    public void setContRiskTypeBY(String contRiskTypeBY) {
        this.contRiskTypeBY = contRiskTypeBY;
    }

    public String getContRiskTypeBJ() {
        return contRiskTypeBJ;
    }

    public void setContRiskTypeBJ(String contRiskTypeBJ) {
        this.contRiskTypeBJ = contRiskTypeBJ;
    }

    public String getContRiskTypeBC() {
        return contRiskTypeBC;
    }

    public void setContRiskTypeBC(String contRiskTypeBC) {
        this.contRiskTypeBC = contRiskTypeBC;
    }

    public String getHeathType1() {
        return heathType1;
    }

    public void setHeathType1(String heathType1) {
        this.heathType1 = heathType1;
    }

    public String getHeathType2() {
        return heathType2;
    }

    public void setHeathType2(String heathType2) {
        this.heathType2 = heathType2;
    }

    public String getHeathType4() {
        return heathType4;
    }

    public void setHeathType4(String heathType4) {
        this.heathType4 = heathType4;
    }
}
