/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCGrpPolPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCGrpPolPojo implements Pojo,Serializable {
    // @Field
    /** 集体保单险种号码 */
    private String GrpPolNo; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体投保单险种号码 */
    private String GrpProposalNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 销售方式 */
    private String SalesWay; 
    /** 销售方式子类 */
    private String SalesWaySub; 
    /** 业务性质 */
    private String BizNature; 
    /** 出单平台 */
    private String PolIssuPlat; 
    /** 险类编码 */
    private String KindCode; 
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVersion; 
    /** 代理机构 */
    private String AgentCom; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 联合代理人代码 */
    private String AgentCode1; 
    /** 客户号码 */
    private String CustomerNo; 
    /** 单位名称 */
    private String GrpName; 
    /** 终交日期 */
    private String  PayEndDate;
    /** 交至日期 */
    private String  PaytoDate;
    /** 封顶线 */
    private double PeakLine; 
    /** 起付限 */
    private double GetLimit; 
    /** 赔付比例 */
    private double GetRate; 
    /** 分红比率 */
    private double BonusRate; 
    /** 医疗费用限额 */
    private double MaxMedFee; 
    /** 雇员自付比例 */
    private double EmployeeRate; 
    /** 家属自付比例 */
    private double FamilyRate; 
    /** 预计人数 */
    private int ExpPeoples; 
    /** 预计保费 */
    private double ExpPremium; 
    /** 预计保额 */
    private double ExpAmnt; 
    /** 管理费比例 */
    private double ManageFeeRate; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 险种生效日期 */
    private String  CValiDate;
    /** 投保总人数 */
    private int Peoples2; 
    /** 份数 */
    private double Mult; 
    /** 保费 */
    private double Prem; 
    /** 保额 */
    private double Amnt; 
    /** 累计保费 */
    private double SumPrem; 
    /** 累计交费 */
    private double SumPay; 
    /** 差额 */
    private double Dif; 
    /** 复核状态 */
    private String ApproveFlag; 
    /** 复核人编码 */
    private String ApproveCode; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 
    /** 核保状态 */
    private String UWFlag; 
    /** 核保人 */
    private String UWOperator; 
    /** 核保完成日期 */
    private String  UWDate;
    /** 核保完成时间 */
    private String UWTime; 
    /** 投保单/保单标志 */
    private String AppFlag; 
    /** 状态 */
    private String State; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 管理机构 */
    private String ManageCom; 
    /** 公司代码 */
    private String ComCode; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改操作员 */
    private String ModifyOperator; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 在职投保人数 */
    private int OnWorkPeoples; 
    /** 退休投保人数 */
    private int OffWorkPeoples; 
    /** 其它投保人数 */
    private int OtherPeoples; 
    /** 连带投保人数 */
    private int RelaPeoples; 
    /** 连带配偶投保人数 */
    private int RelaMatePeoples; 
    /** 连带子女投保人数 */
    private int RelaYoungPeoples; 
    /** 连带其它投保人数 */
    private int RelaOtherPeoples; 
    /** 等待期 */
    private int WaitPeriod; 
    /** 分红标志 */
    private String BonusFlag; 
    /** 初始费用比例 */
    private double InitRate; 
    /** 是否分保标记 */
    private String DistriFlag; 
    /** 摊回手续费比例 */
    private double FeeRate; 
    /** 续保标识 */
    private String RenewFlag; 
    /** 分保比例 */
    private double DistriRate; 
    /** 币别 */
    private String Currency; 
    /** 当前标准保费 */
    private double StandPrem; 
    /** 语言 */
    private String Lang; 
    /** 团险险种净折扣率 */
    private long CleanDisRate; 
    /** 是否计业绩 */
    private String AchvAccruFlag; 


    public static final int FIELDNUM = 80;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpProposalNo() {
        return GrpProposalNo;
    }
    public void setGrpProposalNo(String aGrpProposalNo) {
        GrpProposalNo = aGrpProposalNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getSalesWay() {
        return SalesWay;
    }
    public void setSalesWay(String aSalesWay) {
        SalesWay = aSalesWay;
    }
    public String getSalesWaySub() {
        return SalesWaySub;
    }
    public void setSalesWaySub(String aSalesWaySub) {
        SalesWaySub = aSalesWaySub;
    }
    public String getBizNature() {
        return BizNature;
    }
    public void setBizNature(String aBizNature) {
        BizNature = aBizNature;
    }
    public String getPolIssuPlat() {
        return PolIssuPlat;
    }
    public void setPolIssuPlat(String aPolIssuPlat) {
        PolIssuPlat = aPolIssuPlat;
    }
    public String getKindCode() {
        return KindCode;
    }
    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVersion() {
        return RiskVersion;
    }
    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode1() {
        return AgentCode1;
    }
    public void setAgentCode1(String aAgentCode1) {
        AgentCode1 = aAgentCode1;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getPayEndDate() {
        return PayEndDate;
    }
    public void setPayEndDate(String aPayEndDate) {
        PayEndDate = aPayEndDate;
    }
    public String getPaytoDate() {
        return PaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public double getPeakLine() {
        return PeakLine;
    }
    public void setPeakLine(double aPeakLine) {
        PeakLine = aPeakLine;
    }
    public void setPeakLine(String aPeakLine) {
        if (aPeakLine != null && !aPeakLine.equals("")) {
            Double tDouble = new Double(aPeakLine);
            double d = tDouble.doubleValue();
            PeakLine = d;
        }
    }

    public double getGetLimit() {
        return GetLimit;
    }
    public void setGetLimit(double aGetLimit) {
        GetLimit = aGetLimit;
    }
    public void setGetLimit(String aGetLimit) {
        if (aGetLimit != null && !aGetLimit.equals("")) {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = d;
        }
    }

    public double getGetRate() {
        return GetRate;
    }
    public void setGetRate(double aGetRate) {
        GetRate = aGetRate;
    }
    public void setGetRate(String aGetRate) {
        if (aGetRate != null && !aGetRate.equals("")) {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = d;
        }
    }

    public double getBonusRate() {
        return BonusRate;
    }
    public void setBonusRate(double aBonusRate) {
        BonusRate = aBonusRate;
    }
    public void setBonusRate(String aBonusRate) {
        if (aBonusRate != null && !aBonusRate.equals("")) {
            Double tDouble = new Double(aBonusRate);
            double d = tDouble.doubleValue();
            BonusRate = d;
        }
    }

    public double getMaxMedFee() {
        return MaxMedFee;
    }
    public void setMaxMedFee(double aMaxMedFee) {
        MaxMedFee = aMaxMedFee;
    }
    public void setMaxMedFee(String aMaxMedFee) {
        if (aMaxMedFee != null && !aMaxMedFee.equals("")) {
            Double tDouble = new Double(aMaxMedFee);
            double d = tDouble.doubleValue();
            MaxMedFee = d;
        }
    }

    public double getEmployeeRate() {
        return EmployeeRate;
    }
    public void setEmployeeRate(double aEmployeeRate) {
        EmployeeRate = aEmployeeRate;
    }
    public void setEmployeeRate(String aEmployeeRate) {
        if (aEmployeeRate != null && !aEmployeeRate.equals("")) {
            Double tDouble = new Double(aEmployeeRate);
            double d = tDouble.doubleValue();
            EmployeeRate = d;
        }
    }

    public double getFamilyRate() {
        return FamilyRate;
    }
    public void setFamilyRate(double aFamilyRate) {
        FamilyRate = aFamilyRate;
    }
    public void setFamilyRate(String aFamilyRate) {
        if (aFamilyRate != null && !aFamilyRate.equals("")) {
            Double tDouble = new Double(aFamilyRate);
            double d = tDouble.doubleValue();
            FamilyRate = d;
        }
    }

    public int getExpPeoples() {
        return ExpPeoples;
    }
    public void setExpPeoples(int aExpPeoples) {
        ExpPeoples = aExpPeoples;
    }
    public void setExpPeoples(String aExpPeoples) {
        if (aExpPeoples != null && !aExpPeoples.equals("")) {
            Integer tInteger = new Integer(aExpPeoples);
            int i = tInteger.intValue();
            ExpPeoples = i;
        }
    }

    public double getExpPremium() {
        return ExpPremium;
    }
    public void setExpPremium(double aExpPremium) {
        ExpPremium = aExpPremium;
    }
    public void setExpPremium(String aExpPremium) {
        if (aExpPremium != null && !aExpPremium.equals("")) {
            Double tDouble = new Double(aExpPremium);
            double d = tDouble.doubleValue();
            ExpPremium = d;
        }
    }

    public double getExpAmnt() {
        return ExpAmnt;
    }
    public void setExpAmnt(double aExpAmnt) {
        ExpAmnt = aExpAmnt;
    }
    public void setExpAmnt(String aExpAmnt) {
        if (aExpAmnt != null && !aExpAmnt.equals("")) {
            Double tDouble = new Double(aExpAmnt);
            double d = tDouble.doubleValue();
            ExpAmnt = d;
        }
    }

    public double getManageFeeRate() {
        return ManageFeeRate;
    }
    public void setManageFeeRate(double aManageFeeRate) {
        ManageFeeRate = aManageFeeRate;
    }
    public void setManageFeeRate(String aManageFeeRate) {
        if (aManageFeeRate != null && !aManageFeeRate.equals("")) {
            Double tDouble = new Double(aManageFeeRate);
            double d = tDouble.doubleValue();
            ManageFeeRate = d;
        }
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public int getPeoples2() {
        return Peoples2;
    }
    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }
    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getSumPay() {
        return SumPay;
    }
    public void setSumPay(double aSumPay) {
        SumPay = aSumPay;
    }
    public void setSumPay(String aSumPay) {
        if (aSumPay != null && !aSumPay.equals("")) {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = d;
        }
    }

    public double getDif() {
        return Dif;
    }
    public void setDif(double aDif) {
        Dif = aDif;
    }
    public void setDif(String aDif) {
        if (aDif != null && !aDif.equals("")) {
            Double tDouble = new Double(aDif);
            double d = tDouble.doubleValue();
            Dif = d;
        }
    }

    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWDate() {
        return UWDate;
    }
    public void setUWDate(String aUWDate) {
        UWDate = aUWDate;
    }
    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public int getOnWorkPeoples() {
        return OnWorkPeoples;
    }
    public void setOnWorkPeoples(int aOnWorkPeoples) {
        OnWorkPeoples = aOnWorkPeoples;
    }
    public void setOnWorkPeoples(String aOnWorkPeoples) {
        if (aOnWorkPeoples != null && !aOnWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOnWorkPeoples);
            int i = tInteger.intValue();
            OnWorkPeoples = i;
        }
    }

    public int getOffWorkPeoples() {
        return OffWorkPeoples;
    }
    public void setOffWorkPeoples(int aOffWorkPeoples) {
        OffWorkPeoples = aOffWorkPeoples;
    }
    public void setOffWorkPeoples(String aOffWorkPeoples) {
        if (aOffWorkPeoples != null && !aOffWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOffWorkPeoples);
            int i = tInteger.intValue();
            OffWorkPeoples = i;
        }
    }

    public int getOtherPeoples() {
        return OtherPeoples;
    }
    public void setOtherPeoples(int aOtherPeoples) {
        OtherPeoples = aOtherPeoples;
    }
    public void setOtherPeoples(String aOtherPeoples) {
        if (aOtherPeoples != null && !aOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aOtherPeoples);
            int i = tInteger.intValue();
            OtherPeoples = i;
        }
    }

    public int getRelaPeoples() {
        return RelaPeoples;
    }
    public void setRelaPeoples(int aRelaPeoples) {
        RelaPeoples = aRelaPeoples;
    }
    public void setRelaPeoples(String aRelaPeoples) {
        if (aRelaPeoples != null && !aRelaPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaPeoples);
            int i = tInteger.intValue();
            RelaPeoples = i;
        }
    }

    public int getRelaMatePeoples() {
        return RelaMatePeoples;
    }
    public void setRelaMatePeoples(int aRelaMatePeoples) {
        RelaMatePeoples = aRelaMatePeoples;
    }
    public void setRelaMatePeoples(String aRelaMatePeoples) {
        if (aRelaMatePeoples != null && !aRelaMatePeoples.equals("")) {
            Integer tInteger = new Integer(aRelaMatePeoples);
            int i = tInteger.intValue();
            RelaMatePeoples = i;
        }
    }

    public int getRelaYoungPeoples() {
        return RelaYoungPeoples;
    }
    public void setRelaYoungPeoples(int aRelaYoungPeoples) {
        RelaYoungPeoples = aRelaYoungPeoples;
    }
    public void setRelaYoungPeoples(String aRelaYoungPeoples) {
        if (aRelaYoungPeoples != null && !aRelaYoungPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaYoungPeoples);
            int i = tInteger.intValue();
            RelaYoungPeoples = i;
        }
    }

    public int getRelaOtherPeoples() {
        return RelaOtherPeoples;
    }
    public void setRelaOtherPeoples(int aRelaOtherPeoples) {
        RelaOtherPeoples = aRelaOtherPeoples;
    }
    public void setRelaOtherPeoples(String aRelaOtherPeoples) {
        if (aRelaOtherPeoples != null && !aRelaOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaOtherPeoples);
            int i = tInteger.intValue();
            RelaOtherPeoples = i;
        }
    }

    public int getWaitPeriod() {
        return WaitPeriod;
    }
    public void setWaitPeriod(int aWaitPeriod) {
        WaitPeriod = aWaitPeriod;
    }
    public void setWaitPeriod(String aWaitPeriod) {
        if (aWaitPeriod != null && !aWaitPeriod.equals("")) {
            Integer tInteger = new Integer(aWaitPeriod);
            int i = tInteger.intValue();
            WaitPeriod = i;
        }
    }

    public String getBonusFlag() {
        return BonusFlag;
    }
    public void setBonusFlag(String aBonusFlag) {
        BonusFlag = aBonusFlag;
    }
    public double getInitRate() {
        return InitRate;
    }
    public void setInitRate(double aInitRate) {
        InitRate = aInitRate;
    }
    public void setInitRate(String aInitRate) {
        if (aInitRate != null && !aInitRate.equals("")) {
            Double tDouble = new Double(aInitRate);
            double d = tDouble.doubleValue();
            InitRate = d;
        }
    }

    public String getDistriFlag() {
        return DistriFlag;
    }
    public void setDistriFlag(String aDistriFlag) {
        DistriFlag = aDistriFlag;
    }
    public double getFeeRate() {
        return FeeRate;
    }
    public void setFeeRate(double aFeeRate) {
        FeeRate = aFeeRate;
    }
    public void setFeeRate(String aFeeRate) {
        if (aFeeRate != null && !aFeeRate.equals("")) {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = d;
        }
    }

    public String getRenewFlag() {
        return RenewFlag;
    }
    public void setRenewFlag(String aRenewFlag) {
        RenewFlag = aRenewFlag;
    }
    public double getDistriRate() {
        return DistriRate;
    }
    public void setDistriRate(double aDistriRate) {
        DistriRate = aDistriRate;
    }
    public void setDistriRate(String aDistriRate) {
        if (aDistriRate != null && !aDistriRate.equals("")) {
            Double tDouble = new Double(aDistriRate);
            double d = tDouble.doubleValue();
            DistriRate = d;
        }
    }

    public String getCurrency() {
        return Currency;
    }
    public void setCurrency(String aCurrency) {
        Currency = aCurrency;
    }
    public double getStandPrem() {
        return StandPrem;
    }
    public void setStandPrem(double aStandPrem) {
        StandPrem = aStandPrem;
    }
    public void setStandPrem(String aStandPrem) {
        if (aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public String getLang() {
        return Lang;
    }
    public void setLang(String aLang) {
        Lang = aLang;
    }
    public long getCleanDisRate() {
        return CleanDisRate;
    }
    public void setCleanDisRate(long aCleanDisRate) {
        CleanDisRate = aCleanDisRate;
    }
    public void setCleanDisRate(String aCleanDisRate) {
        if (aCleanDisRate != null && !aCleanDisRate.equals("")) {
            CleanDisRate = new Long(aCleanDisRate).longValue();
        }
    }

    public String getAchvAccruFlag() {
        return AchvAccruFlag;
    }
    public void setAchvAccruFlag(String aAchvAccruFlag) {
        AchvAccruFlag = aAchvAccruFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpPolNo") ) {
            return 0;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 1;
        }
        if( strFieldName.equals("GrpProposalNo") ) {
            return 2;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 3;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 4;
        }
        if( strFieldName.equals("SalesWay") ) {
            return 5;
        }
        if( strFieldName.equals("SalesWaySub") ) {
            return 6;
        }
        if( strFieldName.equals("BizNature") ) {
            return 7;
        }
        if( strFieldName.equals("PolIssuPlat") ) {
            return 8;
        }
        if( strFieldName.equals("KindCode") ) {
            return 9;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 10;
        }
        if( strFieldName.equals("RiskVersion") ) {
            return 11;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 12;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 13;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 14;
        }
        if( strFieldName.equals("AgentCode1") ) {
            return 15;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 16;
        }
        if( strFieldName.equals("GrpName") ) {
            return 17;
        }
        if( strFieldName.equals("PayEndDate") ) {
            return 18;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 19;
        }
        if( strFieldName.equals("PeakLine") ) {
            return 20;
        }
        if( strFieldName.equals("GetLimit") ) {
            return 21;
        }
        if( strFieldName.equals("GetRate") ) {
            return 22;
        }
        if( strFieldName.equals("BonusRate") ) {
            return 23;
        }
        if( strFieldName.equals("MaxMedFee") ) {
            return 24;
        }
        if( strFieldName.equals("EmployeeRate") ) {
            return 25;
        }
        if( strFieldName.equals("FamilyRate") ) {
            return 26;
        }
        if( strFieldName.equals("ExpPeoples") ) {
            return 27;
        }
        if( strFieldName.equals("ExpPremium") ) {
            return 28;
        }
        if( strFieldName.equals("ExpAmnt") ) {
            return 29;
        }
        if( strFieldName.equals("ManageFeeRate") ) {
            return 30;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 31;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 32;
        }
        if( strFieldName.equals("Peoples2") ) {
            return 33;
        }
        if( strFieldName.equals("Mult") ) {
            return 34;
        }
        if( strFieldName.equals("Prem") ) {
            return 35;
        }
        if( strFieldName.equals("Amnt") ) {
            return 36;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 37;
        }
        if( strFieldName.equals("SumPay") ) {
            return 38;
        }
        if( strFieldName.equals("Dif") ) {
            return 39;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 40;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 41;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 42;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 43;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 44;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 45;
        }
        if( strFieldName.equals("UWDate") ) {
            return 46;
        }
        if( strFieldName.equals("UWTime") ) {
            return 47;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 48;
        }
        if( strFieldName.equals("State") ) {
            return 49;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 50;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 51;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 52;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 53;
        }
        if( strFieldName.equals("ComCode") ) {
            return 54;
        }
        if( strFieldName.equals("Operator") ) {
            return 55;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 56;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 57;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 58;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 59;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 60;
        }
        if( strFieldName.equals("OnWorkPeoples") ) {
            return 61;
        }
        if( strFieldName.equals("OffWorkPeoples") ) {
            return 62;
        }
        if( strFieldName.equals("OtherPeoples") ) {
            return 63;
        }
        if( strFieldName.equals("RelaPeoples") ) {
            return 64;
        }
        if( strFieldName.equals("RelaMatePeoples") ) {
            return 65;
        }
        if( strFieldName.equals("RelaYoungPeoples") ) {
            return 66;
        }
        if( strFieldName.equals("RelaOtherPeoples") ) {
            return 67;
        }
        if( strFieldName.equals("WaitPeriod") ) {
            return 68;
        }
        if( strFieldName.equals("BonusFlag") ) {
            return 69;
        }
        if( strFieldName.equals("InitRate") ) {
            return 70;
        }
        if( strFieldName.equals("DistriFlag") ) {
            return 71;
        }
        if( strFieldName.equals("FeeRate") ) {
            return 72;
        }
        if( strFieldName.equals("RenewFlag") ) {
            return 73;
        }
        if( strFieldName.equals("DistriRate") ) {
            return 74;
        }
        if( strFieldName.equals("Currency") ) {
            return 75;
        }
        if( strFieldName.equals("StandPrem") ) {
            return 76;
        }
        if( strFieldName.equals("Lang") ) {
            return 77;
        }
        if( strFieldName.equals("CleanDisRate") ) {
            return 78;
        }
        if( strFieldName.equals("AchvAccruFlag") ) {
            return 79;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpPolNo";
                break;
            case 1:
                strFieldName = "GrpContNo";
                break;
            case 2:
                strFieldName = "GrpProposalNo";
                break;
            case 3:
                strFieldName = "PrtNo";
                break;
            case 4:
                strFieldName = "SaleChnl";
                break;
            case 5:
                strFieldName = "SalesWay";
                break;
            case 6:
                strFieldName = "SalesWaySub";
                break;
            case 7:
                strFieldName = "BizNature";
                break;
            case 8:
                strFieldName = "PolIssuPlat";
                break;
            case 9:
                strFieldName = "KindCode";
                break;
            case 10:
                strFieldName = "RiskCode";
                break;
            case 11:
                strFieldName = "RiskVersion";
                break;
            case 12:
                strFieldName = "AgentCom";
                break;
            case 13:
                strFieldName = "AgentCode";
                break;
            case 14:
                strFieldName = "AgentGroup";
                break;
            case 15:
                strFieldName = "AgentCode1";
                break;
            case 16:
                strFieldName = "CustomerNo";
                break;
            case 17:
                strFieldName = "GrpName";
                break;
            case 18:
                strFieldName = "PayEndDate";
                break;
            case 19:
                strFieldName = "PaytoDate";
                break;
            case 20:
                strFieldName = "PeakLine";
                break;
            case 21:
                strFieldName = "GetLimit";
                break;
            case 22:
                strFieldName = "GetRate";
                break;
            case 23:
                strFieldName = "BonusRate";
                break;
            case 24:
                strFieldName = "MaxMedFee";
                break;
            case 25:
                strFieldName = "EmployeeRate";
                break;
            case 26:
                strFieldName = "FamilyRate";
                break;
            case 27:
                strFieldName = "ExpPeoples";
                break;
            case 28:
                strFieldName = "ExpPremium";
                break;
            case 29:
                strFieldName = "ExpAmnt";
                break;
            case 30:
                strFieldName = "ManageFeeRate";
                break;
            case 31:
                strFieldName = "PayIntv";
                break;
            case 32:
                strFieldName = "CValiDate";
                break;
            case 33:
                strFieldName = "Peoples2";
                break;
            case 34:
                strFieldName = "Mult";
                break;
            case 35:
                strFieldName = "Prem";
                break;
            case 36:
                strFieldName = "Amnt";
                break;
            case 37:
                strFieldName = "SumPrem";
                break;
            case 38:
                strFieldName = "SumPay";
                break;
            case 39:
                strFieldName = "Dif";
                break;
            case 40:
                strFieldName = "ApproveFlag";
                break;
            case 41:
                strFieldName = "ApproveCode";
                break;
            case 42:
                strFieldName = "ApproveDate";
                break;
            case 43:
                strFieldName = "ApproveTime";
                break;
            case 44:
                strFieldName = "UWFlag";
                break;
            case 45:
                strFieldName = "UWOperator";
                break;
            case 46:
                strFieldName = "UWDate";
                break;
            case 47:
                strFieldName = "UWTime";
                break;
            case 48:
                strFieldName = "AppFlag";
                break;
            case 49:
                strFieldName = "State";
                break;
            case 50:
                strFieldName = "StandbyFlag1";
                break;
            case 51:
                strFieldName = "StandbyFlag2";
                break;
            case 52:
                strFieldName = "StandbyFlag3";
                break;
            case 53:
                strFieldName = "ManageCom";
                break;
            case 54:
                strFieldName = "ComCode";
                break;
            case 55:
                strFieldName = "Operator";
                break;
            case 56:
                strFieldName = "MakeDate";
                break;
            case 57:
                strFieldName = "MakeTime";
                break;
            case 58:
                strFieldName = "ModifyOperator";
                break;
            case 59:
                strFieldName = "ModifyDate";
                break;
            case 60:
                strFieldName = "ModifyTime";
                break;
            case 61:
                strFieldName = "OnWorkPeoples";
                break;
            case 62:
                strFieldName = "OffWorkPeoples";
                break;
            case 63:
                strFieldName = "OtherPeoples";
                break;
            case 64:
                strFieldName = "RelaPeoples";
                break;
            case 65:
                strFieldName = "RelaMatePeoples";
                break;
            case 66:
                strFieldName = "RelaYoungPeoples";
                break;
            case 67:
                strFieldName = "RelaOtherPeoples";
                break;
            case 68:
                strFieldName = "WaitPeriod";
                break;
            case 69:
                strFieldName = "BonusFlag";
                break;
            case 70:
                strFieldName = "InitRate";
                break;
            case 71:
                strFieldName = "DistriFlag";
                break;
            case 72:
                strFieldName = "FeeRate";
                break;
            case 73:
                strFieldName = "RenewFlag";
                break;
            case 74:
                strFieldName = "DistriRate";
                break;
            case 75:
                strFieldName = "Currency";
                break;
            case 76:
                strFieldName = "StandPrem";
                break;
            case 77:
                strFieldName = "Lang";
                break;
            case 78:
                strFieldName = "CleanDisRate";
                break;
            case 79:
                strFieldName = "AchvAccruFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPROPOSALNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SALESWAY":
                return Schema.TYPE_STRING;
            case "SALESWAYSUB":
                return Schema.TYPE_STRING;
            case "BIZNATURE":
                return Schema.TYPE_STRING;
            case "POLISSUPLAT":
                return Schema.TYPE_STRING;
            case "KINDCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVERSION":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE1":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "PAYENDDATE":
                return Schema.TYPE_STRING;
            case "PAYTODATE":
                return Schema.TYPE_STRING;
            case "PEAKLINE":
                return Schema.TYPE_DOUBLE;
            case "GETLIMIT":
                return Schema.TYPE_DOUBLE;
            case "GETRATE":
                return Schema.TYPE_DOUBLE;
            case "BONUSRATE":
                return Schema.TYPE_DOUBLE;
            case "MAXMEDFEE":
                return Schema.TYPE_DOUBLE;
            case "EMPLOYEERATE":
                return Schema.TYPE_DOUBLE;
            case "FAMILYRATE":
                return Schema.TYPE_DOUBLE;
            case "EXPPEOPLES":
                return Schema.TYPE_INT;
            case "EXPPREMIUM":
                return Schema.TYPE_DOUBLE;
            case "EXPAMNT":
                return Schema.TYPE_DOUBLE;
            case "MANAGEFEERATE":
                return Schema.TYPE_DOUBLE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "PEOPLES2":
                return Schema.TYPE_INT;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPAY":
                return Schema.TYPE_DOUBLE;
            case "DIF":
                return Schema.TYPE_DOUBLE;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_STRING;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "ONWORKPEOPLES":
                return Schema.TYPE_INT;
            case "OFFWORKPEOPLES":
                return Schema.TYPE_INT;
            case "OTHERPEOPLES":
                return Schema.TYPE_INT;
            case "RELAPEOPLES":
                return Schema.TYPE_INT;
            case "RELAMATEPEOPLES":
                return Schema.TYPE_INT;
            case "RELAYOUNGPEOPLES":
                return Schema.TYPE_INT;
            case "RELAOTHERPEOPLES":
                return Schema.TYPE_INT;
            case "WAITPERIOD":
                return Schema.TYPE_INT;
            case "BONUSFLAG":
                return Schema.TYPE_STRING;
            case "INITRATE":
                return Schema.TYPE_DOUBLE;
            case "DISTRIFLAG":
                return Schema.TYPE_STRING;
            case "FEERATE":
                return Schema.TYPE_DOUBLE;
            case "RENEWFLAG":
                return Schema.TYPE_STRING;
            case "DISTRIRATE":
                return Schema.TYPE_DOUBLE;
            case "CURRENCY":
                return Schema.TYPE_STRING;
            case "STANDPREM":
                return Schema.TYPE_DOUBLE;
            case "LANG":
                return Schema.TYPE_STRING;
            case "CLEANDISRATE":
                return Schema.TYPE_LONG;
            case "ACHVACCRUFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_DOUBLE;
            case 25:
                return Schema.TYPE_DOUBLE;
            case 26:
                return Schema.TYPE_DOUBLE;
            case 27:
                return Schema.TYPE_INT;
            case 28:
                return Schema.TYPE_DOUBLE;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_DOUBLE;
            case 31:
                return Schema.TYPE_INT;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_INT;
            case 34:
                return Schema.TYPE_DOUBLE;
            case 35:
                return Schema.TYPE_DOUBLE;
            case 36:
                return Schema.TYPE_DOUBLE;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_DOUBLE;
            case 39:
                return Schema.TYPE_DOUBLE;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_INT;
            case 62:
                return Schema.TYPE_INT;
            case 63:
                return Schema.TYPE_INT;
            case 64:
                return Schema.TYPE_INT;
            case 65:
                return Schema.TYPE_INT;
            case 66:
                return Schema.TYPE_INT;
            case 67:
                return Schema.TYPE_INT;
            case 68:
                return Schema.TYPE_INT;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_DOUBLE;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_DOUBLE;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_DOUBLE;
            case 75:
                return Schema.TYPE_STRING;
            case 76:
                return Schema.TYPE_DOUBLE;
            case 77:
                return Schema.TYPE_STRING;
            case 78:
                return Schema.TYPE_LONG;
            case 79:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpProposalNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("SalesWay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalesWay));
        }
        if (FCode.equalsIgnoreCase("SalesWaySub")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalesWaySub));
        }
        if (FCode.equalsIgnoreCase("BizNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BizNature));
        }
        if (FCode.equalsIgnoreCase("PolIssuPlat")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolIssuPlat));
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDate));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaytoDate));
        }
        if (FCode.equalsIgnoreCase("PeakLine")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeakLine));
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
        }
        if (FCode.equalsIgnoreCase("BonusRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusRate));
        }
        if (FCode.equalsIgnoreCase("MaxMedFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxMedFee));
        }
        if (FCode.equalsIgnoreCase("EmployeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeRate));
        }
        if (FCode.equalsIgnoreCase("FamilyRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyRate));
        }
        if (FCode.equalsIgnoreCase("ExpPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPeoples));
        }
        if (FCode.equalsIgnoreCase("ExpPremium")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPremium));
        }
        if (FCode.equalsIgnoreCase("ExpAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpAmnt));
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFeeRate));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWDate));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
        }
        if (FCode.equalsIgnoreCase("RelaPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPeoples));
        }
        if (FCode.equalsIgnoreCase("RelaMatePeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaMatePeoples));
        }
        if (FCode.equalsIgnoreCase("RelaYoungPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaYoungPeoples));
        }
        if (FCode.equalsIgnoreCase("RelaOtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherPeoples));
        }
        if (FCode.equalsIgnoreCase("WaitPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WaitPeriod));
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusFlag));
        }
        if (FCode.equalsIgnoreCase("InitRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InitRate));
        }
        if (FCode.equalsIgnoreCase("DistriFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DistriFlag));
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (FCode.equalsIgnoreCase("RenewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewFlag));
        }
        if (FCode.equalsIgnoreCase("DistriRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DistriRate));
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Lang));
        }
        if (FCode.equalsIgnoreCase("CleanDisRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CleanDisRate));
        }
        if (FCode.equalsIgnoreCase("AchvAccruFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AchvAccruFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpPolNo);
                break;
            case 1:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(GrpProposalNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 4:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 5:
                strFieldValue = String.valueOf(SalesWay);
                break;
            case 6:
                strFieldValue = String.valueOf(SalesWaySub);
                break;
            case 7:
                strFieldValue = String.valueOf(BizNature);
                break;
            case 8:
                strFieldValue = String.valueOf(PolIssuPlat);
                break;
            case 9:
                strFieldValue = String.valueOf(KindCode);
                break;
            case 10:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 11:
                strFieldValue = String.valueOf(RiskVersion);
                break;
            case 12:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 13:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 14:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 15:
                strFieldValue = String.valueOf(AgentCode1);
                break;
            case 16:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 17:
                strFieldValue = String.valueOf(GrpName);
                break;
            case 18:
                strFieldValue = String.valueOf(PayEndDate);
                break;
            case 19:
                strFieldValue = String.valueOf(PaytoDate);
                break;
            case 20:
                strFieldValue = String.valueOf(PeakLine);
                break;
            case 21:
                strFieldValue = String.valueOf(GetLimit);
                break;
            case 22:
                strFieldValue = String.valueOf(GetRate);
                break;
            case 23:
                strFieldValue = String.valueOf(BonusRate);
                break;
            case 24:
                strFieldValue = String.valueOf(MaxMedFee);
                break;
            case 25:
                strFieldValue = String.valueOf(EmployeeRate);
                break;
            case 26:
                strFieldValue = String.valueOf(FamilyRate);
                break;
            case 27:
                strFieldValue = String.valueOf(ExpPeoples);
                break;
            case 28:
                strFieldValue = String.valueOf(ExpPremium);
                break;
            case 29:
                strFieldValue = String.valueOf(ExpAmnt);
                break;
            case 30:
                strFieldValue = String.valueOf(ManageFeeRate);
                break;
            case 31:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 32:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 33:
                strFieldValue = String.valueOf(Peoples2);
                break;
            case 34:
                strFieldValue = String.valueOf(Mult);
                break;
            case 35:
                strFieldValue = String.valueOf(Prem);
                break;
            case 36:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 37:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 38:
                strFieldValue = String.valueOf(SumPay);
                break;
            case 39:
                strFieldValue = String.valueOf(Dif);
                break;
            case 40:
                strFieldValue = String.valueOf(ApproveFlag);
                break;
            case 41:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 42:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 43:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            case 44:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 45:
                strFieldValue = String.valueOf(UWOperator);
                break;
            case 46:
                strFieldValue = String.valueOf(UWDate);
                break;
            case 47:
                strFieldValue = String.valueOf(UWTime);
                break;
            case 48:
                strFieldValue = String.valueOf(AppFlag);
                break;
            case 49:
                strFieldValue = String.valueOf(State);
                break;
            case 50:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 51:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 52:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 53:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 54:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 55:
                strFieldValue = String.valueOf(Operator);
                break;
            case 56:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 57:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 58:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 59:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 60:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 61:
                strFieldValue = String.valueOf(OnWorkPeoples);
                break;
            case 62:
                strFieldValue = String.valueOf(OffWorkPeoples);
                break;
            case 63:
                strFieldValue = String.valueOf(OtherPeoples);
                break;
            case 64:
                strFieldValue = String.valueOf(RelaPeoples);
                break;
            case 65:
                strFieldValue = String.valueOf(RelaMatePeoples);
                break;
            case 66:
                strFieldValue = String.valueOf(RelaYoungPeoples);
                break;
            case 67:
                strFieldValue = String.valueOf(RelaOtherPeoples);
                break;
            case 68:
                strFieldValue = String.valueOf(WaitPeriod);
                break;
            case 69:
                strFieldValue = String.valueOf(BonusFlag);
                break;
            case 70:
                strFieldValue = String.valueOf(InitRate);
                break;
            case 71:
                strFieldValue = String.valueOf(DistriFlag);
                break;
            case 72:
                strFieldValue = String.valueOf(FeeRate);
                break;
            case 73:
                strFieldValue = String.valueOf(RenewFlag);
                break;
            case 74:
                strFieldValue = String.valueOf(DistriRate);
                break;
            case 75:
                strFieldValue = String.valueOf(Currency);
                break;
            case 76:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 77:
                strFieldValue = String.valueOf(Lang);
                break;
            case 78:
                strFieldValue = String.valueOf(CleanDisRate);
                break;
            case 79:
                strFieldValue = String.valueOf(AchvAccruFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpProposalNo = FValue.trim();
            }
            else
                GrpProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("SalesWay")) {
            if( FValue != null && !FValue.equals(""))
            {
                SalesWay = FValue.trim();
            }
            else
                SalesWay = null;
        }
        if (FCode.equalsIgnoreCase("SalesWaySub")) {
            if( FValue != null && !FValue.equals(""))
            {
                SalesWaySub = FValue.trim();
            }
            else
                SalesWaySub = null;
        }
        if (FCode.equalsIgnoreCase("BizNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                BizNature = FValue.trim();
            }
            else
                BizNature = null;
        }
        if (FCode.equalsIgnoreCase("PolIssuPlat")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolIssuPlat = FValue.trim();
            }
            else
                PolIssuPlat = null;
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
                KindCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
                RiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode1 = FValue.trim();
            }
            else
                AgentCode1 = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDate = FValue.trim();
            }
            else
                PayEndDate = null;
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaytoDate = FValue.trim();
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("PeakLine")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PeakLine = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("BonusRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BonusRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxMedFee")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MaxMedFee = d;
            }
        }
        if (FCode.equalsIgnoreCase("EmployeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                EmployeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FamilyRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FamilyRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("ExpPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ExpPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("ExpPremium")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ExpPremium = d;
            }
        }
        if (FCode.equalsIgnoreCase("ExpAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ExpAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ManageFeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Dif = d;
            }
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWDate = FValue.trim();
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OnWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OffWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OtherPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaMatePeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaMatePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaYoungPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaYoungPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaOtherPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaOtherPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("WaitPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                WaitPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusFlag = FValue.trim();
            }
            else
                BonusFlag = null;
        }
        if (FCode.equalsIgnoreCase("InitRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                InitRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("DistriFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DistriFlag = FValue.trim();
            }
            else
                DistriFlag = null;
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("RenewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RenewFlag = FValue.trim();
            }
            else
                RenewFlag = null;
        }
        if (FCode.equalsIgnoreCase("DistriRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DistriRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            if( FValue != null && !FValue.equals(""))
            {
                Currency = FValue.trim();
            }
            else
                Currency = null;
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            if( FValue != null && !FValue.equals(""))
            {
                Lang = FValue.trim();
            }
            else
                Lang = null;
        }
        if (FCode.equalsIgnoreCase("CleanDisRate")) {
            if( FValue != null && !FValue.equals("")) {
                CleanDisRate = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("AchvAccruFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AchvAccruFlag = FValue.trim();
            }
            else
                AchvAccruFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LCGrpPolPojo [" +
            "GrpPolNo="+GrpPolNo +
            ", GrpContNo="+GrpContNo +
            ", GrpProposalNo="+GrpProposalNo +
            ", PrtNo="+PrtNo +
            ", SaleChnl="+SaleChnl +
            ", SalesWay="+SalesWay +
            ", SalesWaySub="+SalesWaySub +
            ", BizNature="+BizNature +
            ", PolIssuPlat="+PolIssuPlat +
            ", KindCode="+KindCode +
            ", RiskCode="+RiskCode +
            ", RiskVersion="+RiskVersion +
            ", AgentCom="+AgentCom +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", AgentCode1="+AgentCode1 +
            ", CustomerNo="+CustomerNo +
            ", GrpName="+GrpName +
            ", PayEndDate="+PayEndDate +
            ", PaytoDate="+PaytoDate +
            ", PeakLine="+PeakLine +
            ", GetLimit="+GetLimit +
            ", GetRate="+GetRate +
            ", BonusRate="+BonusRate +
            ", MaxMedFee="+MaxMedFee +
            ", EmployeeRate="+EmployeeRate +
            ", FamilyRate="+FamilyRate +
            ", ExpPeoples="+ExpPeoples +
            ", ExpPremium="+ExpPremium +
            ", ExpAmnt="+ExpAmnt +
            ", ManageFeeRate="+ManageFeeRate +
            ", PayIntv="+PayIntv +
            ", CValiDate="+CValiDate +
            ", Peoples2="+Peoples2 +
            ", Mult="+Mult +
            ", Prem="+Prem +
            ", Amnt="+Amnt +
            ", SumPrem="+SumPrem +
            ", SumPay="+SumPay +
            ", Dif="+Dif +
            ", ApproveFlag="+ApproveFlag +
            ", ApproveCode="+ApproveCode +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +
            ", UWFlag="+UWFlag +
            ", UWOperator="+UWOperator +
            ", UWDate="+UWDate +
            ", UWTime="+UWTime +
            ", AppFlag="+AppFlag +
            ", State="+State +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", StandbyFlag3="+StandbyFlag3 +
            ", ManageCom="+ManageCom +
            ", ComCode="+ComCode +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyOperator="+ModifyOperator +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", OnWorkPeoples="+OnWorkPeoples +
            ", OffWorkPeoples="+OffWorkPeoples +
            ", OtherPeoples="+OtherPeoples +
            ", RelaPeoples="+RelaPeoples +
            ", RelaMatePeoples="+RelaMatePeoples +
            ", RelaYoungPeoples="+RelaYoungPeoples +
            ", RelaOtherPeoples="+RelaOtherPeoples +
            ", WaitPeriod="+WaitPeriod +
            ", BonusFlag="+BonusFlag +
            ", InitRate="+InitRate +
            ", DistriFlag="+DistriFlag +
            ", FeeRate="+FeeRate +
            ", RenewFlag="+RenewFlag +
            ", DistriRate="+DistriRate +
            ", Currency="+Currency +
            ", StandPrem="+StandPrem +
            ", Lang="+Lang +
            ", CleanDisRate="+CleanDisRate +
            ", AchvAccruFlag="+AchvAccruFlag +"]";
    }
}
