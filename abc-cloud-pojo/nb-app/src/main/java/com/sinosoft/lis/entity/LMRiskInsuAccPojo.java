/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskInsuAccPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-19
 */
public class LMRiskInsuAccPojo implements Pojo,Serializable {
    // @Field
    /** 保险帐户号码 */
    @RedisPrimaryHKey
    private String InsuAccNo;
    /** 账号类型 */
    private String AccType; 
    /** 账户分类 */
    private String AccKind; 
    /** 保险帐户名称 */
    private String InsuAccName; 
    /** 账户产生位置 */
    private String AccCreatePos; 
    /** 账号产生规则 */
    private String AccCreateType; 
    /** 账户固定利率 */
    private double AccRate; 
    /** 账户对应利率表 */
    private String AccRateTable; 
    /** 账户结清计算公式 */
    private String AccCancelCode; 
    /** 账户结算方式 */
    private String AccComputeFlag; 
    /** 投资类型 */
    private String InvestType; 
    /** 基金公司代码 */
    private String FundCompanyCode; 
    /** 账户所有者 */
    private String Owner; 
    /** 账户是否参与分红 */
    private String AccBonusFlag; 
    /** 分红记入账户的方式 */
    private String AccBonusMode; 
    /** 分红记入账户代码 */
    private String BonusToInsuAccNo; 
    /** 分红时是否进行账户结算 */
    private String InsuAccCalBalaFlag; 
    /** 分红方式 */
    private String BonusMode; 
    /** 投资收益类型 */
    private String InvestFlag; 
    /** 计价周期 */
    private String CalValueFreq; 
    /** 计价价格获取规则 */
    private String CalValueRule; 
    /** 小数位数 */
    private String UnitDecimal; 
    /** 四舍五入标记 */
    private String RoundMethod; 
    /** 是否参与分红 */
    private String BonusFlag; 


    public static final int FIELDNUM = 24;    // 数据库表的字段个数
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getAccKind() {
        return AccKind;
    }
    public void setAccKind(String aAccKind) {
        AccKind = aAccKind;
    }
    public String getInsuAccName() {
        return InsuAccName;
    }
    public void setInsuAccName(String aInsuAccName) {
        InsuAccName = aInsuAccName;
    }
    public String getAccCreatePos() {
        return AccCreatePos;
    }
    public void setAccCreatePos(String aAccCreatePos) {
        AccCreatePos = aAccCreatePos;
    }
    public String getAccCreateType() {
        return AccCreateType;
    }
    public void setAccCreateType(String aAccCreateType) {
        AccCreateType = aAccCreateType;
    }
    public double getAccRate() {
        return AccRate;
    }
    public void setAccRate(double aAccRate) {
        AccRate = aAccRate;
    }
    public void setAccRate(String aAccRate) {
        if (aAccRate != null && !aAccRate.equals("")) {
            Double tDouble = new Double(aAccRate);
            double d = tDouble.doubleValue();
            AccRate = d;
        }
    }

    public String getAccRateTable() {
        return AccRateTable;
    }
    public void setAccRateTable(String aAccRateTable) {
        AccRateTable = aAccRateTable;
    }
    public String getAccCancelCode() {
        return AccCancelCode;
    }
    public void setAccCancelCode(String aAccCancelCode) {
        AccCancelCode = aAccCancelCode;
    }
    public String getAccComputeFlag() {
        return AccComputeFlag;
    }
    public void setAccComputeFlag(String aAccComputeFlag) {
        AccComputeFlag = aAccComputeFlag;
    }
    public String getInvestType() {
        return InvestType;
    }
    public void setInvestType(String aInvestType) {
        InvestType = aInvestType;
    }
    public String getFundCompanyCode() {
        return FundCompanyCode;
    }
    public void setFundCompanyCode(String aFundCompanyCode) {
        FundCompanyCode = aFundCompanyCode;
    }
    public String getOwner() {
        return Owner;
    }
    public void setOwner(String aOwner) {
        Owner = aOwner;
    }
    public String getAccBonusFlag() {
        return AccBonusFlag;
    }
    public void setAccBonusFlag(String aAccBonusFlag) {
        AccBonusFlag = aAccBonusFlag;
    }
    public String getAccBonusMode() {
        return AccBonusMode;
    }
    public void setAccBonusMode(String aAccBonusMode) {
        AccBonusMode = aAccBonusMode;
    }
    public String getBonusToInsuAccNo() {
        return BonusToInsuAccNo;
    }
    public void setBonusToInsuAccNo(String aBonusToInsuAccNo) {
        BonusToInsuAccNo = aBonusToInsuAccNo;
    }
    public String getInsuAccCalBalaFlag() {
        return InsuAccCalBalaFlag;
    }
    public void setInsuAccCalBalaFlag(String aInsuAccCalBalaFlag) {
        InsuAccCalBalaFlag = aInsuAccCalBalaFlag;
    }
    public String getBonusMode() {
        return BonusMode;
    }
    public void setBonusMode(String aBonusMode) {
        BonusMode = aBonusMode;
    }
    public String getInvestFlag() {
        return InvestFlag;
    }
    public void setInvestFlag(String aInvestFlag) {
        InvestFlag = aInvestFlag;
    }
    public String getCalValueFreq() {
        return CalValueFreq;
    }
    public void setCalValueFreq(String aCalValueFreq) {
        CalValueFreq = aCalValueFreq;
    }
    public String getCalValueRule() {
        return CalValueRule;
    }
    public void setCalValueRule(String aCalValueRule) {
        CalValueRule = aCalValueRule;
    }
    public String getUnitDecimal() {
        return UnitDecimal;
    }
    public void setUnitDecimal(String aUnitDecimal) {
        UnitDecimal = aUnitDecimal;
    }
    public String getRoundMethod() {
        return RoundMethod;
    }
    public void setRoundMethod(String aRoundMethod) {
        RoundMethod = aRoundMethod;
    }
    public String getBonusFlag() {
        return BonusFlag;
    }
    public void setBonusFlag(String aBonusFlag) {
        BonusFlag = aBonusFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsuAccNo") ) {
            return 0;
        }
        if( strFieldName.equals("AccType") ) {
            return 1;
        }
        if( strFieldName.equals("AccKind") ) {
            return 2;
        }
        if( strFieldName.equals("InsuAccName") ) {
            return 3;
        }
        if( strFieldName.equals("AccCreatePos") ) {
            return 4;
        }
        if( strFieldName.equals("AccCreateType") ) {
            return 5;
        }
        if( strFieldName.equals("AccRate") ) {
            return 6;
        }
        if( strFieldName.equals("AccRateTable") ) {
            return 7;
        }
        if( strFieldName.equals("AccCancelCode") ) {
            return 8;
        }
        if( strFieldName.equals("AccComputeFlag") ) {
            return 9;
        }
        if( strFieldName.equals("InvestType") ) {
            return 10;
        }
        if( strFieldName.equals("FundCompanyCode") ) {
            return 11;
        }
        if( strFieldName.equals("Owner") ) {
            return 12;
        }
        if( strFieldName.equals("AccBonusFlag") ) {
            return 13;
        }
        if( strFieldName.equals("AccBonusMode") ) {
            return 14;
        }
        if( strFieldName.equals("BonusToInsuAccNo") ) {
            return 15;
        }
        if( strFieldName.equals("InsuAccCalBalaFlag") ) {
            return 16;
        }
        if( strFieldName.equals("BonusMode") ) {
            return 17;
        }
        if( strFieldName.equals("InvestFlag") ) {
            return 18;
        }
        if( strFieldName.equals("CalValueFreq") ) {
            return 19;
        }
        if( strFieldName.equals("CalValueRule") ) {
            return 20;
        }
        if( strFieldName.equals("UnitDecimal") ) {
            return 21;
        }
        if( strFieldName.equals("RoundMethod") ) {
            return 22;
        }
        if( strFieldName.equals("BonusFlag") ) {
            return 23;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsuAccNo";
                break;
            case 1:
                strFieldName = "AccType";
                break;
            case 2:
                strFieldName = "AccKind";
                break;
            case 3:
                strFieldName = "InsuAccName";
                break;
            case 4:
                strFieldName = "AccCreatePos";
                break;
            case 5:
                strFieldName = "AccCreateType";
                break;
            case 6:
                strFieldName = "AccRate";
                break;
            case 7:
                strFieldName = "AccRateTable";
                break;
            case 8:
                strFieldName = "AccCancelCode";
                break;
            case 9:
                strFieldName = "AccComputeFlag";
                break;
            case 10:
                strFieldName = "InvestType";
                break;
            case 11:
                strFieldName = "FundCompanyCode";
                break;
            case 12:
                strFieldName = "Owner";
                break;
            case 13:
                strFieldName = "AccBonusFlag";
                break;
            case 14:
                strFieldName = "AccBonusMode";
                break;
            case 15:
                strFieldName = "BonusToInsuAccNo";
                break;
            case 16:
                strFieldName = "InsuAccCalBalaFlag";
                break;
            case 17:
                strFieldName = "BonusMode";
                break;
            case 18:
                strFieldName = "InvestFlag";
                break;
            case 19:
                strFieldName = "CalValueFreq";
                break;
            case 20:
                strFieldName = "CalValueRule";
                break;
            case 21:
                strFieldName = "UnitDecimal";
                break;
            case 22:
                strFieldName = "RoundMethod";
                break;
            case 23:
                strFieldName = "BonusFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "ACCKIND":
                return Schema.TYPE_STRING;
            case "INSUACCNAME":
                return Schema.TYPE_STRING;
            case "ACCCREATEPOS":
                return Schema.TYPE_STRING;
            case "ACCCREATETYPE":
                return Schema.TYPE_STRING;
            case "ACCRATE":
                return Schema.TYPE_DOUBLE;
            case "ACCRATETABLE":
                return Schema.TYPE_STRING;
            case "ACCCANCELCODE":
                return Schema.TYPE_STRING;
            case "ACCCOMPUTEFLAG":
                return Schema.TYPE_STRING;
            case "INVESTTYPE":
                return Schema.TYPE_STRING;
            case "FUNDCOMPANYCODE":
                return Schema.TYPE_STRING;
            case "OWNER":
                return Schema.TYPE_STRING;
            case "ACCBONUSFLAG":
                return Schema.TYPE_STRING;
            case "ACCBONUSMODE":
                return Schema.TYPE_STRING;
            case "BONUSTOINSUACCNO":
                return Schema.TYPE_STRING;
            case "INSUACCCALBALAFLAG":
                return Schema.TYPE_STRING;
            case "BONUSMODE":
                return Schema.TYPE_STRING;
            case "INVESTFLAG":
                return Schema.TYPE_STRING;
            case "CALVALUEFREQ":
                return Schema.TYPE_STRING;
            case "CALVALUERULE":
                return Schema.TYPE_STRING;
            case "UNITDECIMAL":
                return Schema.TYPE_STRING;
            case "ROUNDMETHOD":
                return Schema.TYPE_STRING;
            case "BONUSFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("AccKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccKind));
        }
        if (FCode.equalsIgnoreCase("InsuAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccName));
        }
        if (FCode.equalsIgnoreCase("AccCreatePos")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCreatePos));
        }
        if (FCode.equalsIgnoreCase("AccCreateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCreateType));
        }
        if (FCode.equalsIgnoreCase("AccRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccRate));
        }
        if (FCode.equalsIgnoreCase("AccRateTable")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccRateTable));
        }
        if (FCode.equalsIgnoreCase("AccCancelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCancelCode));
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccComputeFlag));
        }
        if (FCode.equalsIgnoreCase("InvestType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestType));
        }
        if (FCode.equalsIgnoreCase("FundCompanyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FundCompanyCode));
        }
        if (FCode.equalsIgnoreCase("Owner")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Owner));
        }
        if (FCode.equalsIgnoreCase("AccBonusFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccBonusFlag));
        }
        if (FCode.equalsIgnoreCase("AccBonusMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccBonusMode));
        }
        if (FCode.equalsIgnoreCase("BonusToInsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusToInsuAccNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccCalBalaFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccCalBalaFlag));
        }
        if (FCode.equalsIgnoreCase("BonusMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusMode));
        }
        if (FCode.equalsIgnoreCase("InvestFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestFlag));
        }
        if (FCode.equalsIgnoreCase("CalValueFreq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalValueFreq));
        }
        if (FCode.equalsIgnoreCase("CalValueRule")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalValueRule));
        }
        if (FCode.equalsIgnoreCase("UnitDecimal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitDecimal));
        }
        if (FCode.equalsIgnoreCase("RoundMethod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RoundMethod));
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 1:
                strFieldValue = String.valueOf(AccType);
                break;
            case 2:
                strFieldValue = String.valueOf(AccKind);
                break;
            case 3:
                strFieldValue = String.valueOf(InsuAccName);
                break;
            case 4:
                strFieldValue = String.valueOf(AccCreatePos);
                break;
            case 5:
                strFieldValue = String.valueOf(AccCreateType);
                break;
            case 6:
                strFieldValue = String.valueOf(AccRate);
                break;
            case 7:
                strFieldValue = String.valueOf(AccRateTable);
                break;
            case 8:
                strFieldValue = String.valueOf(AccCancelCode);
                break;
            case 9:
                strFieldValue = String.valueOf(AccComputeFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(InvestType);
                break;
            case 11:
                strFieldValue = String.valueOf(FundCompanyCode);
                break;
            case 12:
                strFieldValue = String.valueOf(Owner);
                break;
            case 13:
                strFieldValue = String.valueOf(AccBonusFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(AccBonusMode);
                break;
            case 15:
                strFieldValue = String.valueOf(BonusToInsuAccNo);
                break;
            case 16:
                strFieldValue = String.valueOf(InsuAccCalBalaFlag);
                break;
            case 17:
                strFieldValue = String.valueOf(BonusMode);
                break;
            case 18:
                strFieldValue = String.valueOf(InvestFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(CalValueFreq);
                break;
            case 20:
                strFieldValue = String.valueOf(CalValueRule);
                break;
            case 21:
                strFieldValue = String.valueOf(UnitDecimal);
                break;
            case 22:
                strFieldValue = String.valueOf(RoundMethod);
                break;
            case 23:
                strFieldValue = String.valueOf(BonusFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("AccKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccKind = FValue.trim();
            }
            else
                AccKind = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccName = FValue.trim();
            }
            else
                InsuAccName = null;
        }
        if (FCode.equalsIgnoreCase("AccCreatePos")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCreatePos = FValue.trim();
            }
            else
                AccCreatePos = null;
        }
        if (FCode.equalsIgnoreCase("AccCreateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCreateType = FValue.trim();
            }
            else
                AccCreateType = null;
        }
        if (FCode.equalsIgnoreCase("AccRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AccRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("AccRateTable")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccRateTable = FValue.trim();
            }
            else
                AccRateTable = null;
        }
        if (FCode.equalsIgnoreCase("AccCancelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCancelCode = FValue.trim();
            }
            else
                AccCancelCode = null;
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
                AccComputeFlag = null;
        }
        if (FCode.equalsIgnoreCase("InvestType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestType = FValue.trim();
            }
            else
                InvestType = null;
        }
        if (FCode.equalsIgnoreCase("FundCompanyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FundCompanyCode = FValue.trim();
            }
            else
                FundCompanyCode = null;
        }
        if (FCode.equalsIgnoreCase("Owner")) {
            if( FValue != null && !FValue.equals(""))
            {
                Owner = FValue.trim();
            }
            else
                Owner = null;
        }
        if (FCode.equalsIgnoreCase("AccBonusFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccBonusFlag = FValue.trim();
            }
            else
                AccBonusFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccBonusMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccBonusMode = FValue.trim();
            }
            else
                AccBonusMode = null;
        }
        if (FCode.equalsIgnoreCase("BonusToInsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusToInsuAccNo = FValue.trim();
            }
            else
                BonusToInsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccCalBalaFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccCalBalaFlag = FValue.trim();
            }
            else
                InsuAccCalBalaFlag = null;
        }
        if (FCode.equalsIgnoreCase("BonusMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusMode = FValue.trim();
            }
            else
                BonusMode = null;
        }
        if (FCode.equalsIgnoreCase("InvestFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestFlag = FValue.trim();
            }
            else
                InvestFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalValueFreq")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalValueFreq = FValue.trim();
            }
            else
                CalValueFreq = null;
        }
        if (FCode.equalsIgnoreCase("CalValueRule")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalValueRule = FValue.trim();
            }
            else
                CalValueRule = null;
        }
        if (FCode.equalsIgnoreCase("UnitDecimal")) {
            if( FValue != null && !FValue.equals(""))
            {
                UnitDecimal = FValue.trim();
            }
            else
                UnitDecimal = null;
        }
        if (FCode.equalsIgnoreCase("RoundMethod")) {
            if( FValue != null && !FValue.equals(""))
            {
                RoundMethod = FValue.trim();
            }
            else
                RoundMethod = null;
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusFlag = FValue.trim();
            }
            else
                BonusFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskInsuAccPojo [" +
            "InsuAccNo="+InsuAccNo +
            ", AccType="+AccType +
            ", AccKind="+AccKind +
            ", InsuAccName="+InsuAccName +
            ", AccCreatePos="+AccCreatePos +
            ", AccCreateType="+AccCreateType +
            ", AccRate="+AccRate +
            ", AccRateTable="+AccRateTable +
            ", AccCancelCode="+AccCancelCode +
            ", AccComputeFlag="+AccComputeFlag +
            ", InvestType="+InvestType +
            ", FundCompanyCode="+FundCompanyCode +
            ", Owner="+Owner +
            ", AccBonusFlag="+AccBonusFlag +
            ", AccBonusMode="+AccBonusMode +
            ", BonusToInsuAccNo="+BonusToInsuAccNo +
            ", InsuAccCalBalaFlag="+InsuAccCalBalaFlag +
            ", BonusMode="+BonusMode +
            ", InvestFlag="+InvestFlag +
            ", CalValueFreq="+CalValueFreq +
            ", CalValueRule="+CalValueRule +
            ", UnitDecimal="+UnitDecimal +
            ", RoundMethod="+RoundMethod +
            ", BonusFlag="+BonusFlag +"]";
    }
}
