/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCHangUpDB;

/**
 * <p>ClassName: LCHangUpSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-16
 */
public class LCHangUpSchema implements Schema, Cloneable {
    // @Field
    /** 请求报文id */
    private String reqMsgId;
    /** 蚂蚁保单号 */
    private String policyNo;
    /** 机构保单号 */
    private String contNo;
    /** 挂起保单业务单号 */
    private String hangUpBizNo;
    /** 原保单状态 */
    private String oldAppflag;
    /** 现保单状态 */
    private String newAppflag;
    /** 接口代码 */
    private String function;
    /** 结果码 */
    private String resultCode;
    /** 结果码描述 */
    private String resultDesc;
    /** 接口调用结果 */
    private String resultStatus;
    /** 日期 */
    private Date MakeDate;
    /** 时间 */
    private String MakeTime;

    public static final int FIELDNUM = 12;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCHangUpSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "reqMsgId";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCHangUpSchema cloned = (LCHangUpSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getReqMsgId() {
        return reqMsgId;
    }
    public void setReqMsgId(String areqMsgId) {
        reqMsgId = areqMsgId;
    }
    public String getPolicyNo() {
        return policyNo;
    }
    public void setPolicyNo(String apolicyNo) {
        policyNo = apolicyNo;
    }
    public String getContNo() {
        return contNo;
    }
    public void setContNo(String acontNo) {
        contNo = acontNo;
    }
    public String getHangUpBizNo() {
        return hangUpBizNo;
    }
    public void setHangUpBizNo(String ahangUpBizNo) {
        hangUpBizNo = ahangUpBizNo;
    }
    public String getOldAppflag() {
        return oldAppflag;
    }
    public void setOldAppflag(String aoldAppflag) {
        oldAppflag = aoldAppflag;
    }
    public String getNewAppflag() {
        return newAppflag;
    }
    public void setNewAppflag(String anewAppflag) {
        newAppflag = anewAppflag;
    }
    public String getFunction() {
        return function;
    }
    public void setFunction(String afunction) {
        function = afunction;
    }
    public String getResultCode() {
        return resultCode;
    }
    public void setResultCode(String aresultCode) {
        resultCode = aresultCode;
    }
    public String getResultDesc() {
        return resultDesc;
    }
    public void setResultDesc(String aresultDesc) {
        resultDesc = aresultDesc;
    }
    public String getResultStatus() {
        return resultStatus;
    }
    public void setResultStatus(String aresultStatus) {
        resultStatus = aresultStatus;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
    * 使用另外一个 LCHangUpSchema 对象给 Schema 赋值
    * @param: aLCHangUpSchema LCHangUpSchema
    **/
    public void setSchema(LCHangUpSchema aLCHangUpSchema) {
        this.reqMsgId = aLCHangUpSchema.getReqMsgId();
        this.policyNo = aLCHangUpSchema.getPolicyNo();
        this.contNo = aLCHangUpSchema.getContNo();
        this.hangUpBizNo = aLCHangUpSchema.getHangUpBizNo();
        this.oldAppflag = aLCHangUpSchema.getOldAppflag();
        this.newAppflag = aLCHangUpSchema.getNewAppflag();
        this.function = aLCHangUpSchema.getFunction();
        this.resultCode = aLCHangUpSchema.getResultCode();
        this.resultDesc = aLCHangUpSchema.getResultDesc();
        this.resultStatus = aLCHangUpSchema.getResultStatus();
        this.MakeDate = fDate.getDate( aLCHangUpSchema.getMakeDate());
        this.MakeTime = aLCHangUpSchema.getMakeTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("reqMsgId") == null )
                this.reqMsgId = null;
            else
                this.reqMsgId = rs.getString("reqMsgId").trim();

            if( rs.getString("policyNo") == null )
                this.policyNo = null;
            else
                this.policyNo = rs.getString("policyNo").trim();

            if( rs.getString("contNo") == null )
                this.contNo = null;
            else
                this.contNo = rs.getString("contNo").trim();

            if( rs.getString("hangUpBizNo") == null )
                this.hangUpBizNo = null;
            else
                this.hangUpBizNo = rs.getString("hangUpBizNo").trim();

            if( rs.getString("oldAppflag") == null )
                this.oldAppflag = null;
            else
                this.oldAppflag = rs.getString("oldAppflag").trim();

            if( rs.getString("newAppflag") == null )
                this.newAppflag = null;
            else
                this.newAppflag = rs.getString("newAppflag").trim();

            if( rs.getString("function") == null )
                this.function = null;
            else
                this.function = rs.getString("function").trim();

            if( rs.getString("resultCode") == null )
                this.resultCode = null;
            else
                this.resultCode = rs.getString("resultCode").trim();

            if( rs.getString("resultDesc") == null )
                this.resultDesc = null;
            else
                this.resultDesc = rs.getString("resultDesc").trim();

            if( rs.getString("resultStatus") == null )
                this.resultStatus = null;
            else
                this.resultStatus = rs.getString("resultStatus").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCHangUpSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCHangUpSchema getSchema() {
        LCHangUpSchema aLCHangUpSchema = new LCHangUpSchema();
        aLCHangUpSchema.setSchema(this);
        return aLCHangUpSchema;
    }

    public LCHangUpDB getDB() {
        LCHangUpDB aDBOper = new LCHangUpDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCHangUp描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(reqMsgId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(contNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(hangUpBizNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(oldAppflag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(newAppflag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(function)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(resultCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(resultDesc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(resultStatus)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCHangUp>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            reqMsgId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            contNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            hangUpBizNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            oldAppflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            newAppflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            function = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            resultCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            resultDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            resultStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCHangUpSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("reqMsgId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(reqMsgId));
        }
        if (FCode.equalsIgnoreCase("policyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
        }
        if (FCode.equalsIgnoreCase("contNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(contNo));
        }
        if (FCode.equalsIgnoreCase("hangUpBizNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(hangUpBizNo));
        }
        if (FCode.equalsIgnoreCase("oldAppflag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(oldAppflag));
        }
        if (FCode.equalsIgnoreCase("newAppflag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(newAppflag));
        }
        if (FCode.equalsIgnoreCase("function")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(function));
        }
        if (FCode.equalsIgnoreCase("resultCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(resultCode));
        }
        if (FCode.equalsIgnoreCase("resultDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(resultDesc));
        }
        if (FCode.equalsIgnoreCase("resultStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(resultStatus));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(reqMsgId);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(policyNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(contNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(hangUpBizNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(oldAppflag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(newAppflag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(function);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(resultCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(resultDesc);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(resultStatus);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("reqMsgId")) {
            if( FValue != null && !FValue.equals(""))
            {
                reqMsgId = FValue.trim();
            }
            else
                reqMsgId = null;
        }
        if (FCode.equalsIgnoreCase("policyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                policyNo = FValue.trim();
            }
            else
                policyNo = null;
        }
        if (FCode.equalsIgnoreCase("contNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                contNo = FValue.trim();
            }
            else
                contNo = null;
        }
        if (FCode.equalsIgnoreCase("hangUpBizNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                hangUpBizNo = FValue.trim();
            }
            else
                hangUpBizNo = null;
        }
        if (FCode.equalsIgnoreCase("oldAppflag")) {
            if( FValue != null && !FValue.equals(""))
            {
                oldAppflag = FValue.trim();
            }
            else
                oldAppflag = null;
        }
        if (FCode.equalsIgnoreCase("newAppflag")) {
            if( FValue != null && !FValue.equals(""))
            {
                newAppflag = FValue.trim();
            }
            else
                newAppflag = null;
        }
        if (FCode.equalsIgnoreCase("function")) {
            if( FValue != null && !FValue.equals(""))
            {
                function = FValue.trim();
            }
            else
                function = null;
        }
        if (FCode.equalsIgnoreCase("resultCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                resultCode = FValue.trim();
            }
            else
                resultCode = null;
        }
        if (FCode.equalsIgnoreCase("resultDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                resultDesc = FValue.trim();
            }
            else
                resultDesc = null;
        }
        if (FCode.equalsIgnoreCase("resultStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                resultStatus = FValue.trim();
            }
            else
                resultStatus = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCHangUpSchema other = (LCHangUpSchema)otherObject;
        return
            reqMsgId.equals(other.getReqMsgId())
            && policyNo.equals(other.getPolicyNo())
            && contNo.equals(other.getContNo())
            && hangUpBizNo.equals(other.getHangUpBizNo())
            && oldAppflag.equals(other.getOldAppflag())
            && newAppflag.equals(other.getNewAppflag())
            && function.equals(other.getFunction())
            && resultCode.equals(other.getResultCode())
            && resultDesc.equals(other.getResultDesc())
            && resultStatus.equals(other.getResultStatus())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("reqMsgId") ) {
            return 0;
        }
        if( strFieldName.equals("policyNo") ) {
            return 1;
        }
        if( strFieldName.equals("contNo") ) {
            return 2;
        }
        if( strFieldName.equals("hangUpBizNo") ) {
            return 3;
        }
        if( strFieldName.equals("oldAppflag") ) {
            return 4;
        }
        if( strFieldName.equals("newAppflag") ) {
            return 5;
        }
        if( strFieldName.equals("function") ) {
            return 6;
        }
        if( strFieldName.equals("resultCode") ) {
            return 7;
        }
        if( strFieldName.equals("resultDesc") ) {
            return 8;
        }
        if( strFieldName.equals("resultStatus") ) {
            return 9;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 10;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "reqMsgId";
                break;
            case 1:
                strFieldName = "policyNo";
                break;
            case 2:
                strFieldName = "contNo";
                break;
            case 3:
                strFieldName = "hangUpBizNo";
                break;
            case 4:
                strFieldName = "oldAppflag";
                break;
            case 5:
                strFieldName = "newAppflag";
                break;
            case 6:
                strFieldName = "function";
                break;
            case 7:
                strFieldName = "resultCode";
                break;
            case 8:
                strFieldName = "resultDesc";
                break;
            case 9:
                strFieldName = "resultStatus";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "REQMSGID":
                return Schema.TYPE_STRING;
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "HANGUPBIZNO":
                return Schema.TYPE_STRING;
            case "OLDAPPFLAG":
                return Schema.TYPE_STRING;
            case "NEWAPPFLAG":
                return Schema.TYPE_STRING;
            case "FUNCTION":
                return Schema.TYPE_STRING;
            case "RESULTCODE":
                return Schema.TYPE_STRING;
            case "RESULTDESC":
                return Schema.TYPE_STRING;
            case "RESULTSTATUS":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
