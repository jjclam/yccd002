/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMRiskProcessDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LMRiskProcessSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-06-08
 */
public class LMRiskProcessSchema implements Schema, Cloneable {
    // @Field
    /** 险种代码 */
    private String RiskCode;
    /** 险种种类 */
    private String RiskType;
    /** 是否校验销售信息 */
    private String ValiSaleFlag;
    /** 是否需要调用单证服务 */
    private String CallVchFlag;
    /** 是否获取费率 */
    private String GetRateFlag;
    /** 是否获取特殊审批信息 */
    private String SpecialApproveFlag;
    /** 备用字段1 */
    private String Bak1;
    /** 备用字段2 */
    private String Bak2;
    /** 备用字段3 */
    private String Bak3;

    public static final int FIELDNUM = 9;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMRiskProcessSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "RiskCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMRiskProcessSchema cloned = (LMRiskProcessSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskType() {
        return RiskType;
    }
    public void setRiskType(String aRiskType) {
        RiskType = aRiskType;
    }
    public String getValiSaleFlag() {
        return ValiSaleFlag;
    }
    public void setValiSaleFlag(String aValiSaleFlag) {
        ValiSaleFlag = aValiSaleFlag;
    }
    public String getCallVchFlag() {
        return CallVchFlag;
    }
    public void setCallVchFlag(String aCallVchFlag) {
        CallVchFlag = aCallVchFlag;
    }
    public String getGetRateFlag() {
        return GetRateFlag;
    }
    public void setGetRateFlag(String aGetRateFlag) {
        GetRateFlag = aGetRateFlag;
    }
    public String getSpecialApproveFlag() {
        return SpecialApproveFlag;
    }
    public void setSpecialApproveFlag(String aSpecialApproveFlag) {
        SpecialApproveFlag = aSpecialApproveFlag;
    }
    public String getBak1() {
        return Bak1;
    }
    public void setBak1(String aBak1) {
        Bak1 = aBak1;
    }
    public String getBak2() {
        return Bak2;
    }
    public void setBak2(String aBak2) {
        Bak2 = aBak2;
    }
    public String getBak3() {
        return Bak3;
    }
    public void setBak3(String aBak3) {
        Bak3 = aBak3;
    }

    /**
    * 使用另外一个 LMRiskProcessSchema 对象给 Schema 赋值
    * @param: aLMRiskProcessSchema LMRiskProcessSchema
    **/
    public void setSchema(LMRiskProcessSchema aLMRiskProcessSchema) {
        this.RiskCode = aLMRiskProcessSchema.getRiskCode();
        this.RiskType = aLMRiskProcessSchema.getRiskType();
        this.ValiSaleFlag = aLMRiskProcessSchema.getValiSaleFlag();
        this.CallVchFlag = aLMRiskProcessSchema.getCallVchFlag();
        this.GetRateFlag = aLMRiskProcessSchema.getGetRateFlag();
        this.SpecialApproveFlag = aLMRiskProcessSchema.getSpecialApproveFlag();
        this.Bak1 = aLMRiskProcessSchema.getBak1();
        this.Bak2 = aLMRiskProcessSchema.getBak2();
        this.Bak3 = aLMRiskProcessSchema.getBak3();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("RiskType") == null )
                this.RiskType = null;
            else
                this.RiskType = rs.getString("RiskType").trim();

            if( rs.getString("ValiSaleFlag") == null )
                this.ValiSaleFlag = null;
            else
                this.ValiSaleFlag = rs.getString("ValiSaleFlag").trim();

            if( rs.getString("CallVchFlag") == null )
                this.CallVchFlag = null;
            else
                this.CallVchFlag = rs.getString("CallVchFlag").trim();

            if( rs.getString("GetRateFlag") == null )
                this.GetRateFlag = null;
            else
                this.GetRateFlag = rs.getString("GetRateFlag").trim();

            if( rs.getString("SpecialApproveFlag") == null )
                this.SpecialApproveFlag = null;
            else
                this.SpecialApproveFlag = rs.getString("SpecialApproveFlag").trim();

            if( rs.getString("Bak1") == null )
                this.Bak1 = null;
            else
                this.Bak1 = rs.getString("Bak1").trim();

            if( rs.getString("Bak2") == null )
                this.Bak2 = null;
            else
                this.Bak2 = rs.getString("Bak2").trim();

            if( rs.getString("Bak3") == null )
                this.Bak3 = null;
            else
                this.Bak3 = rs.getString("Bak3").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskProcessSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMRiskProcessSchema getSchema() {
        LMRiskProcessSchema aLMRiskProcessSchema = new LMRiskProcessSchema();
        aLMRiskProcessSchema.setSchema(this);
        return aLMRiskProcessSchema;
    }

    public LMRiskProcessDB getDB() {
        LMRiskProcessDB aDBOper = new LMRiskProcessDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskProcess描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ValiSaleFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CallVchFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetRateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecialApproveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Bak3));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskProcess>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ValiSaleFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            CallVchFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            GetRateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            SpecialApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskProcessSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
        }
        if (FCode.equalsIgnoreCase("ValiSaleFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValiSaleFlag));
        }
        if (FCode.equalsIgnoreCase("CallVchFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CallVchFlag));
        }
        if (FCode.equalsIgnoreCase("GetRateFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRateFlag));
        }
        if (FCode.equalsIgnoreCase("SpecialApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialApproveFlag));
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ValiSaleFlag);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CallVchFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GetRateFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(SpecialApproveFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Bak1);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Bak2);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Bak3);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType = FValue.trim();
            }
            else
                RiskType = null;
        }
        if (FCode.equalsIgnoreCase("ValiSaleFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValiSaleFlag = FValue.trim();
            }
            else
                ValiSaleFlag = null;
        }
        if (FCode.equalsIgnoreCase("CallVchFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CallVchFlag = FValue.trim();
            }
            else
                CallVchFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetRateFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetRateFlag = FValue.trim();
            }
            else
                GetRateFlag = null;
        }
        if (FCode.equalsIgnoreCase("SpecialApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecialApproveFlag = FValue.trim();
            }
            else
                SpecialApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak1 = FValue.trim();
            }
            else
                Bak1 = null;
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak2 = FValue.trim();
            }
            else
                Bak2 = null;
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak3 = FValue.trim();
            }
            else
                Bak3 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMRiskProcessSchema other = (LMRiskProcessSchema)otherObject;
        return
            RiskCode.equals(other.getRiskCode())
            && RiskType.equals(other.getRiskType())
            && ValiSaleFlag.equals(other.getValiSaleFlag())
            && CallVchFlag.equals(other.getCallVchFlag())
            && GetRateFlag.equals(other.getGetRateFlag())
            && SpecialApproveFlag.equals(other.getSpecialApproveFlag())
            && Bak1.equals(other.getBak1())
            && Bak2.equals(other.getBak2())
            && Bak3.equals(other.getBak3());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskType") ) {
            return 1;
        }
        if( strFieldName.equals("ValiSaleFlag") ) {
            return 2;
        }
        if( strFieldName.equals("CallVchFlag") ) {
            return 3;
        }
        if( strFieldName.equals("GetRateFlag") ) {
            return 4;
        }
        if( strFieldName.equals("SpecialApproveFlag") ) {
            return 5;
        }
        if( strFieldName.equals("Bak1") ) {
            return 6;
        }
        if( strFieldName.equals("Bak2") ) {
            return 7;
        }
        if( strFieldName.equals("Bak3") ) {
            return 8;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskType";
                break;
            case 2:
                strFieldName = "ValiSaleFlag";
                break;
            case 3:
                strFieldName = "CallVchFlag";
                break;
            case 4:
                strFieldName = "GetRateFlag";
                break;
            case 5:
                strFieldName = "SpecialApproveFlag";
                break;
            case 6:
                strFieldName = "Bak1";
                break;
            case 7:
                strFieldName = "Bak2";
                break;
            case 8:
                strFieldName = "Bak3";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKTYPE":
                return Schema.TYPE_STRING;
            case "VALISALEFLAG":
                return Schema.TYPE_STRING;
            case "CALLVCHFLAG":
                return Schema.TYPE_STRING;
            case "GETRATEFLAG":
                return Schema.TYPE_STRING;
            case "SPECIALAPPROVEFLAG":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            case "BAK3":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
