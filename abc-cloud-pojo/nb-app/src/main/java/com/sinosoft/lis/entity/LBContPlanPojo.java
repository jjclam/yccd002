/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBContPlanPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBContPlanPojo implements Pojo,Serializable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体投保单号码 */
    private String ProposalGrpContNo; 
    /** 保险计划编码 */
    private String ContPlanCode; 
    /** 保险计划名称 */
    private String ContPlanName; 
    /** 计划类型 */
    private String PlanType; 
    /** 计划规则 */
    private String PlanRule; 
    /** 计划规则sql */
    private String PlanSql; 
    /** 备注 */
    private String Remark; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 可投保人数 */
    private int Peoples3; 
    /** 备注2 */
    private String Remark2; 
    /** 参保人数 */
    private int Peoples2; 
    /** 方案编码 */
    private String PlanCode; 
    /** 方案标识 */
    private String PlanFlag; 
    /** 保费计算方式 */
    private String PremCalType; 
    /** 保险期间 */
    private int InsuPeriod; 
    /** 保险期间单位 */
    private String InsuPeriodFlag; 
    /** 职业类别标记 */
    private String OccupTypeFlag; 
    /** 最低职业类别 */
    private String MinOccupType; 
    /** 最高职业类别 */
    private String MaxOccupType; 
    /** 职业类别 */
    private String OccupType; 
    /** 职业中分类 */
    private String OccupMidType; 
    /** 工种 */
    private String OccupCode; 
    /** 最低年龄 */
    private int MinAge; 
    /** 最高年龄 */
    private int MaxAge; 
    /** 平均年龄 */
    private int AvgAge; 
    /** 参加社保占比 */
    private double SocialInsuRate; 
    /** 男性比例 */
    private double MaleRate; 
    /** 女性比例 */
    private double FemaleRate; 
    /** 退休占比 */
    private double RetireRate; 
    /** 保费分摊方式 */
    private String PremMode; 
    /** 企业负担占比 */
    private double EnterpriseRate; 
    /** 最低月薪 */
    private double MinSalary; 
    /** 最高月薪 */
    private double MaxSalary; 
    /** 平均月薪 */
    private double AvgSalary; 
    /** 其他说明 */
    private String OtherDesc; 
    /** 最后一次修改操作员 */
    private String ModifyOperator; 
    /** 职业比例 */
    private String OccupRate; 
    /** 套餐编码 */
    private String CombiCode; 
    /** 套餐费率 */
    private String CombiRate; 
    /** 套餐份数 */
    private int CombiMult; 
    /** 计划类别 */
    private String RiskPlan; 


    public static final int FIELDNUM = 46;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getProposalGrpContNo() {
        return ProposalGrpContNo;
    }
    public void setProposalGrpContNo(String aProposalGrpContNo) {
        ProposalGrpContNo = aProposalGrpContNo;
    }
    public String getContPlanCode() {
        return ContPlanCode;
    }
    public void setContPlanCode(String aContPlanCode) {
        ContPlanCode = aContPlanCode;
    }
    public String getContPlanName() {
        return ContPlanName;
    }
    public void setContPlanName(String aContPlanName) {
        ContPlanName = aContPlanName;
    }
    public String getPlanType() {
        return PlanType;
    }
    public void setPlanType(String aPlanType) {
        PlanType = aPlanType;
    }
    public String getPlanRule() {
        return PlanRule;
    }
    public void setPlanRule(String aPlanRule) {
        PlanRule = aPlanRule;
    }
    public String getPlanSql() {
        return PlanSql;
    }
    public void setPlanSql(String aPlanSql) {
        PlanSql = aPlanSql;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public int getPeoples3() {
        return Peoples3;
    }
    public void setPeoples3(int aPeoples3) {
        Peoples3 = aPeoples3;
    }
    public void setPeoples3(String aPeoples3) {
        if (aPeoples3 != null && !aPeoples3.equals("")) {
            Integer tInteger = new Integer(aPeoples3);
            int i = tInteger.intValue();
            Peoples3 = i;
        }
    }

    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }
    public int getPeoples2() {
        return Peoples2;
    }
    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }
    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public String getPlanCode() {
        return PlanCode;
    }
    public void setPlanCode(String aPlanCode) {
        PlanCode = aPlanCode;
    }
    public String getPlanFlag() {
        return PlanFlag;
    }
    public void setPlanFlag(String aPlanFlag) {
        PlanFlag = aPlanFlag;
    }
    public String getPremCalType() {
        return PremCalType;
    }
    public void setPremCalType(String aPremCalType) {
        PremCalType = aPremCalType;
    }
    public int getInsuPeriod() {
        return InsuPeriod;
    }
    public void setInsuPeriod(int aInsuPeriod) {
        InsuPeriod = aInsuPeriod;
    }
    public void setInsuPeriod(String aInsuPeriod) {
        if (aInsuPeriod != null && !aInsuPeriod.equals("")) {
            Integer tInteger = new Integer(aInsuPeriod);
            int i = tInteger.intValue();
            InsuPeriod = i;
        }
    }

    public String getInsuPeriodFlag() {
        return InsuPeriodFlag;
    }
    public void setInsuPeriodFlag(String aInsuPeriodFlag) {
        InsuPeriodFlag = aInsuPeriodFlag;
    }
    public String getOccupTypeFlag() {
        return OccupTypeFlag;
    }
    public void setOccupTypeFlag(String aOccupTypeFlag) {
        OccupTypeFlag = aOccupTypeFlag;
    }
    public String getMinOccupType() {
        return MinOccupType;
    }
    public void setMinOccupType(String aMinOccupType) {
        MinOccupType = aMinOccupType;
    }
    public String getMaxOccupType() {
        return MaxOccupType;
    }
    public void setMaxOccupType(String aMaxOccupType) {
        MaxOccupType = aMaxOccupType;
    }
    public String getOccupType() {
        return OccupType;
    }
    public void setOccupType(String aOccupType) {
        OccupType = aOccupType;
    }
    public String getOccupMidType() {
        return OccupMidType;
    }
    public void setOccupMidType(String aOccupMidType) {
        OccupMidType = aOccupMidType;
    }
    public String getOccupCode() {
        return OccupCode;
    }
    public void setOccupCode(String aOccupCode) {
        OccupCode = aOccupCode;
    }
    public int getMinAge() {
        return MinAge;
    }
    public void setMinAge(int aMinAge) {
        MinAge = aMinAge;
    }
    public void setMinAge(String aMinAge) {
        if (aMinAge != null && !aMinAge.equals("")) {
            Integer tInteger = new Integer(aMinAge);
            int i = tInteger.intValue();
            MinAge = i;
        }
    }

    public int getMaxAge() {
        return MaxAge;
    }
    public void setMaxAge(int aMaxAge) {
        MaxAge = aMaxAge;
    }
    public void setMaxAge(String aMaxAge) {
        if (aMaxAge != null && !aMaxAge.equals("")) {
            Integer tInteger = new Integer(aMaxAge);
            int i = tInteger.intValue();
            MaxAge = i;
        }
    }

    public int getAvgAge() {
        return AvgAge;
    }
    public void setAvgAge(int aAvgAge) {
        AvgAge = aAvgAge;
    }
    public void setAvgAge(String aAvgAge) {
        if (aAvgAge != null && !aAvgAge.equals("")) {
            Integer tInteger = new Integer(aAvgAge);
            int i = tInteger.intValue();
            AvgAge = i;
        }
    }

    public double getSocialInsuRate() {
        return SocialInsuRate;
    }
    public void setSocialInsuRate(double aSocialInsuRate) {
        SocialInsuRate = aSocialInsuRate;
    }
    public void setSocialInsuRate(String aSocialInsuRate) {
        if (aSocialInsuRate != null && !aSocialInsuRate.equals("")) {
            Double tDouble = new Double(aSocialInsuRate);
            double d = tDouble.doubleValue();
            SocialInsuRate = d;
        }
    }

    public double getMaleRate() {
        return MaleRate;
    }
    public void setMaleRate(double aMaleRate) {
        MaleRate = aMaleRate;
    }
    public void setMaleRate(String aMaleRate) {
        if (aMaleRate != null && !aMaleRate.equals("")) {
            Double tDouble = new Double(aMaleRate);
            double d = tDouble.doubleValue();
            MaleRate = d;
        }
    }

    public double getFemaleRate() {
        return FemaleRate;
    }
    public void setFemaleRate(double aFemaleRate) {
        FemaleRate = aFemaleRate;
    }
    public void setFemaleRate(String aFemaleRate) {
        if (aFemaleRate != null && !aFemaleRate.equals("")) {
            Double tDouble = new Double(aFemaleRate);
            double d = tDouble.doubleValue();
            FemaleRate = d;
        }
    }

    public double getRetireRate() {
        return RetireRate;
    }
    public void setRetireRate(double aRetireRate) {
        RetireRate = aRetireRate;
    }
    public void setRetireRate(String aRetireRate) {
        if (aRetireRate != null && !aRetireRate.equals("")) {
            Double tDouble = new Double(aRetireRate);
            double d = tDouble.doubleValue();
            RetireRate = d;
        }
    }

    public String getPremMode() {
        return PremMode;
    }
    public void setPremMode(String aPremMode) {
        PremMode = aPremMode;
    }
    public double getEnterpriseRate() {
        return EnterpriseRate;
    }
    public void setEnterpriseRate(double aEnterpriseRate) {
        EnterpriseRate = aEnterpriseRate;
    }
    public void setEnterpriseRate(String aEnterpriseRate) {
        if (aEnterpriseRate != null && !aEnterpriseRate.equals("")) {
            Double tDouble = new Double(aEnterpriseRate);
            double d = tDouble.doubleValue();
            EnterpriseRate = d;
        }
    }

    public double getMinSalary() {
        return MinSalary;
    }
    public void setMinSalary(double aMinSalary) {
        MinSalary = aMinSalary;
    }
    public void setMinSalary(String aMinSalary) {
        if (aMinSalary != null && !aMinSalary.equals("")) {
            Double tDouble = new Double(aMinSalary);
            double d = tDouble.doubleValue();
            MinSalary = d;
        }
    }

    public double getMaxSalary() {
        return MaxSalary;
    }
    public void setMaxSalary(double aMaxSalary) {
        MaxSalary = aMaxSalary;
    }
    public void setMaxSalary(String aMaxSalary) {
        if (aMaxSalary != null && !aMaxSalary.equals("")) {
            Double tDouble = new Double(aMaxSalary);
            double d = tDouble.doubleValue();
            MaxSalary = d;
        }
    }

    public double getAvgSalary() {
        return AvgSalary;
    }
    public void setAvgSalary(double aAvgSalary) {
        AvgSalary = aAvgSalary;
    }
    public void setAvgSalary(String aAvgSalary) {
        if (aAvgSalary != null && !aAvgSalary.equals("")) {
            Double tDouble = new Double(aAvgSalary);
            double d = tDouble.doubleValue();
            AvgSalary = d;
        }
    }

    public String getOtherDesc() {
        return OtherDesc;
    }
    public void setOtherDesc(String aOtherDesc) {
        OtherDesc = aOtherDesc;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getOccupRate() {
        return OccupRate;
    }
    public void setOccupRate(String aOccupRate) {
        OccupRate = aOccupRate;
    }
    public String getCombiCode() {
        return CombiCode;
    }
    public void setCombiCode(String aCombiCode) {
        CombiCode = aCombiCode;
    }
    public String getCombiRate() {
        return CombiRate;
    }
    public void setCombiRate(String aCombiRate) {
        CombiRate = aCombiRate;
    }
    public int getCombiMult() {
        return CombiMult;
    }
    public void setCombiMult(int aCombiMult) {
        CombiMult = aCombiMult;
    }
    public void setCombiMult(String aCombiMult) {
        if (aCombiMult != null && !aCombiMult.equals("")) {
            Integer tInteger = new Integer(aCombiMult);
            int i = tInteger.intValue();
            CombiMult = i;
        }
    }

    public String getRiskPlan() {
        return RiskPlan;
    }
    public void setRiskPlan(String aRiskPlan) {
        RiskPlan = aRiskPlan;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ProposalGrpContNo") ) {
            return 1;
        }
        if( strFieldName.equals("ContPlanCode") ) {
            return 2;
        }
        if( strFieldName.equals("ContPlanName") ) {
            return 3;
        }
        if( strFieldName.equals("PlanType") ) {
            return 4;
        }
        if( strFieldName.equals("PlanRule") ) {
            return 5;
        }
        if( strFieldName.equals("PlanSql") ) {
            return 6;
        }
        if( strFieldName.equals("Remark") ) {
            return 7;
        }
        if( strFieldName.equals("Operator") ) {
            return 8;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 9;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 11;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 12;
        }
        if( strFieldName.equals("Peoples3") ) {
            return 13;
        }
        if( strFieldName.equals("Remark2") ) {
            return 14;
        }
        if( strFieldName.equals("Peoples2") ) {
            return 15;
        }
        if( strFieldName.equals("PlanCode") ) {
            return 16;
        }
        if( strFieldName.equals("PlanFlag") ) {
            return 17;
        }
        if( strFieldName.equals("PremCalType") ) {
            return 18;
        }
        if( strFieldName.equals("InsuPeriod") ) {
            return 19;
        }
        if( strFieldName.equals("InsuPeriodFlag") ) {
            return 20;
        }
        if( strFieldName.equals("OccupTypeFlag") ) {
            return 21;
        }
        if( strFieldName.equals("MinOccupType") ) {
            return 22;
        }
        if( strFieldName.equals("MaxOccupType") ) {
            return 23;
        }
        if( strFieldName.equals("OccupType") ) {
            return 24;
        }
        if( strFieldName.equals("OccupMidType") ) {
            return 25;
        }
        if( strFieldName.equals("OccupCode") ) {
            return 26;
        }
        if( strFieldName.equals("MinAge") ) {
            return 27;
        }
        if( strFieldName.equals("MaxAge") ) {
            return 28;
        }
        if( strFieldName.equals("AvgAge") ) {
            return 29;
        }
        if( strFieldName.equals("SocialInsuRate") ) {
            return 30;
        }
        if( strFieldName.equals("MaleRate") ) {
            return 31;
        }
        if( strFieldName.equals("FemaleRate") ) {
            return 32;
        }
        if( strFieldName.equals("RetireRate") ) {
            return 33;
        }
        if( strFieldName.equals("PremMode") ) {
            return 34;
        }
        if( strFieldName.equals("EnterpriseRate") ) {
            return 35;
        }
        if( strFieldName.equals("MinSalary") ) {
            return 36;
        }
        if( strFieldName.equals("MaxSalary") ) {
            return 37;
        }
        if( strFieldName.equals("AvgSalary") ) {
            return 38;
        }
        if( strFieldName.equals("OtherDesc") ) {
            return 39;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 40;
        }
        if( strFieldName.equals("OccupRate") ) {
            return 41;
        }
        if( strFieldName.equals("CombiCode") ) {
            return 42;
        }
        if( strFieldName.equals("CombiRate") ) {
            return 43;
        }
        if( strFieldName.equals("CombiMult") ) {
            return 44;
        }
        if( strFieldName.equals("RiskPlan") ) {
            return 45;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "ContPlanCode";
                break;
            case 3:
                strFieldName = "ContPlanName";
                break;
            case 4:
                strFieldName = "PlanType";
                break;
            case 5:
                strFieldName = "PlanRule";
                break;
            case 6:
                strFieldName = "PlanSql";
                break;
            case 7:
                strFieldName = "Remark";
                break;
            case 8:
                strFieldName = "Operator";
                break;
            case 9:
                strFieldName = "MakeDate";
                break;
            case 10:
                strFieldName = "MakeTime";
                break;
            case 11:
                strFieldName = "ModifyDate";
                break;
            case 12:
                strFieldName = "ModifyTime";
                break;
            case 13:
                strFieldName = "Peoples3";
                break;
            case 14:
                strFieldName = "Remark2";
                break;
            case 15:
                strFieldName = "Peoples2";
                break;
            case 16:
                strFieldName = "PlanCode";
                break;
            case 17:
                strFieldName = "PlanFlag";
                break;
            case 18:
                strFieldName = "PremCalType";
                break;
            case 19:
                strFieldName = "InsuPeriod";
                break;
            case 20:
                strFieldName = "InsuPeriodFlag";
                break;
            case 21:
                strFieldName = "OccupTypeFlag";
                break;
            case 22:
                strFieldName = "MinOccupType";
                break;
            case 23:
                strFieldName = "MaxOccupType";
                break;
            case 24:
                strFieldName = "OccupType";
                break;
            case 25:
                strFieldName = "OccupMidType";
                break;
            case 26:
                strFieldName = "OccupCode";
                break;
            case 27:
                strFieldName = "MinAge";
                break;
            case 28:
                strFieldName = "MaxAge";
                break;
            case 29:
                strFieldName = "AvgAge";
                break;
            case 30:
                strFieldName = "SocialInsuRate";
                break;
            case 31:
                strFieldName = "MaleRate";
                break;
            case 32:
                strFieldName = "FemaleRate";
                break;
            case 33:
                strFieldName = "RetireRate";
                break;
            case 34:
                strFieldName = "PremMode";
                break;
            case 35:
                strFieldName = "EnterpriseRate";
                break;
            case 36:
                strFieldName = "MinSalary";
                break;
            case 37:
                strFieldName = "MaxSalary";
                break;
            case 38:
                strFieldName = "AvgSalary";
                break;
            case 39:
                strFieldName = "OtherDesc";
                break;
            case 40:
                strFieldName = "ModifyOperator";
                break;
            case 41:
                strFieldName = "OccupRate";
                break;
            case 42:
                strFieldName = "CombiCode";
                break;
            case 43:
                strFieldName = "CombiRate";
                break;
            case 44:
                strFieldName = "CombiMult";
                break;
            case 45:
                strFieldName = "RiskPlan";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALGRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTPLANCODE":
                return Schema.TYPE_STRING;
            case "CONTPLANNAME":
                return Schema.TYPE_STRING;
            case "PLANTYPE":
                return Schema.TYPE_STRING;
            case "PLANRULE":
                return Schema.TYPE_STRING;
            case "PLANSQL":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "PEOPLES3":
                return Schema.TYPE_INT;
            case "REMARK2":
                return Schema.TYPE_STRING;
            case "PEOPLES2":
                return Schema.TYPE_INT;
            case "PLANCODE":
                return Schema.TYPE_STRING;
            case "PLANFLAG":
                return Schema.TYPE_STRING;
            case "PREMCALTYPE":
                return Schema.TYPE_STRING;
            case "INSUPERIOD":
                return Schema.TYPE_INT;
            case "INSUPERIODFLAG":
                return Schema.TYPE_STRING;
            case "OCCUPTYPEFLAG":
                return Schema.TYPE_STRING;
            case "MINOCCUPTYPE":
                return Schema.TYPE_STRING;
            case "MAXOCCUPTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPMIDTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPCODE":
                return Schema.TYPE_STRING;
            case "MINAGE":
                return Schema.TYPE_INT;
            case "MAXAGE":
                return Schema.TYPE_INT;
            case "AVGAGE":
                return Schema.TYPE_INT;
            case "SOCIALINSURATE":
                return Schema.TYPE_DOUBLE;
            case "MALERATE":
                return Schema.TYPE_DOUBLE;
            case "FEMALERATE":
                return Schema.TYPE_DOUBLE;
            case "RETIRERATE":
                return Schema.TYPE_DOUBLE;
            case "PREMMODE":
                return Schema.TYPE_STRING;
            case "ENTERPRISERATE":
                return Schema.TYPE_DOUBLE;
            case "MINSALARY":
                return Schema.TYPE_DOUBLE;
            case "MAXSALARY":
                return Schema.TYPE_DOUBLE;
            case "AVGSALARY":
                return Schema.TYPE_DOUBLE;
            case "OTHERDESC":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "OCCUPRATE":
                return Schema.TYPE_STRING;
            case "COMBICODE":
                return Schema.TYPE_STRING;
            case "COMBIRATE":
                return Schema.TYPE_STRING;
            case "COMBIMULT":
                return Schema.TYPE_INT;
            case "RISKPLAN":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_INT;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_INT;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_INT;
            case 28:
                return Schema.TYPE_INT;
            case 29:
                return Schema.TYPE_INT;
            case 30:
                return Schema.TYPE_DOUBLE;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_DOUBLE;
            case 33:
                return Schema.TYPE_DOUBLE;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_DOUBLE;
            case 36:
                return Schema.TYPE_DOUBLE;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_DOUBLE;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_INT;
            case 45:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
        }
        if (FCode.equalsIgnoreCase("ContPlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanName));
        }
        if (FCode.equalsIgnoreCase("PlanType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanType));
        }
        if (FCode.equalsIgnoreCase("PlanRule")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanRule));
        }
        if (FCode.equalsIgnoreCase("PlanSql")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanSql));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equalsIgnoreCase("PlanFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanFlag));
        }
        if (FCode.equalsIgnoreCase("PremCalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremCalType));
        }
        if (FCode.equalsIgnoreCase("InsuPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuPeriod));
        }
        if (FCode.equalsIgnoreCase("InsuPeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuPeriodFlag));
        }
        if (FCode.equalsIgnoreCase("OccupTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupTypeFlag));
        }
        if (FCode.equalsIgnoreCase("MinOccupType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinOccupType));
        }
        if (FCode.equalsIgnoreCase("MaxOccupType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxOccupType));
        }
        if (FCode.equalsIgnoreCase("OccupType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupType));
        }
        if (FCode.equalsIgnoreCase("OccupMidType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupMidType));
        }
        if (FCode.equalsIgnoreCase("OccupCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupCode));
        }
        if (FCode.equalsIgnoreCase("MinAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAge));
        }
        if (FCode.equalsIgnoreCase("MaxAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAge));
        }
        if (FCode.equalsIgnoreCase("AvgAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AvgAge));
        }
        if (FCode.equalsIgnoreCase("SocialInsuRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuRate));
        }
        if (FCode.equalsIgnoreCase("MaleRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaleRate));
        }
        if (FCode.equalsIgnoreCase("FemaleRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FemaleRate));
        }
        if (FCode.equalsIgnoreCase("RetireRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetireRate));
        }
        if (FCode.equalsIgnoreCase("PremMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremMode));
        }
        if (FCode.equalsIgnoreCase("EnterpriseRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterpriseRate));
        }
        if (FCode.equalsIgnoreCase("MinSalary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinSalary));
        }
        if (FCode.equalsIgnoreCase("MaxSalary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxSalary));
        }
        if (FCode.equalsIgnoreCase("AvgSalary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AvgSalary));
        }
        if (FCode.equalsIgnoreCase("OtherDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherDesc));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("OccupRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupRate));
        }
        if (FCode.equalsIgnoreCase("CombiCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CombiCode));
        }
        if (FCode.equalsIgnoreCase("CombiRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CombiRate));
        }
        if (FCode.equalsIgnoreCase("CombiMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CombiMult));
        }
        if (FCode.equalsIgnoreCase("RiskPlan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPlan));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ContPlanCode);
                break;
            case 3:
                strFieldValue = String.valueOf(ContPlanName);
                break;
            case 4:
                strFieldValue = String.valueOf(PlanType);
                break;
            case 5:
                strFieldValue = String.valueOf(PlanRule);
                break;
            case 6:
                strFieldValue = String.valueOf(PlanSql);
                break;
            case 7:
                strFieldValue = String.valueOf(Remark);
                break;
            case 8:
                strFieldValue = String.valueOf(Operator);
                break;
            case 9:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 10:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 12:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 13:
                strFieldValue = String.valueOf(Peoples3);
                break;
            case 14:
                strFieldValue = String.valueOf(Remark2);
                break;
            case 15:
                strFieldValue = String.valueOf(Peoples2);
                break;
            case 16:
                strFieldValue = String.valueOf(PlanCode);
                break;
            case 17:
                strFieldValue = String.valueOf(PlanFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(PremCalType);
                break;
            case 19:
                strFieldValue = String.valueOf(InsuPeriod);
                break;
            case 20:
                strFieldValue = String.valueOf(InsuPeriodFlag);
                break;
            case 21:
                strFieldValue = String.valueOf(OccupTypeFlag);
                break;
            case 22:
                strFieldValue = String.valueOf(MinOccupType);
                break;
            case 23:
                strFieldValue = String.valueOf(MaxOccupType);
                break;
            case 24:
                strFieldValue = String.valueOf(OccupType);
                break;
            case 25:
                strFieldValue = String.valueOf(OccupMidType);
                break;
            case 26:
                strFieldValue = String.valueOf(OccupCode);
                break;
            case 27:
                strFieldValue = String.valueOf(MinAge);
                break;
            case 28:
                strFieldValue = String.valueOf(MaxAge);
                break;
            case 29:
                strFieldValue = String.valueOf(AvgAge);
                break;
            case 30:
                strFieldValue = String.valueOf(SocialInsuRate);
                break;
            case 31:
                strFieldValue = String.valueOf(MaleRate);
                break;
            case 32:
                strFieldValue = String.valueOf(FemaleRate);
                break;
            case 33:
                strFieldValue = String.valueOf(RetireRate);
                break;
            case 34:
                strFieldValue = String.valueOf(PremMode);
                break;
            case 35:
                strFieldValue = String.valueOf(EnterpriseRate);
                break;
            case 36:
                strFieldValue = String.valueOf(MinSalary);
                break;
            case 37:
                strFieldValue = String.valueOf(MaxSalary);
                break;
            case 38:
                strFieldValue = String.valueOf(AvgSalary);
                break;
            case 39:
                strFieldValue = String.valueOf(OtherDesc);
                break;
            case 40:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 41:
                strFieldValue = String.valueOf(OccupRate);
                break;
            case 42:
                strFieldValue = String.valueOf(CombiCode);
                break;
            case 43:
                strFieldValue = String.valueOf(CombiRate);
                break;
            case 44:
                strFieldValue = String.valueOf(CombiMult);
                break;
            case 45:
                strFieldValue = String.valueOf(RiskPlan);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
                ProposalGrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
                ContPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("ContPlanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlanName = FValue.trim();
            }
            else
                ContPlanName = null;
        }
        if (FCode.equalsIgnoreCase("PlanType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanType = FValue.trim();
            }
            else
                PlanType = null;
        }
        if (FCode.equalsIgnoreCase("PlanRule")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanRule = FValue.trim();
            }
            else
                PlanRule = null;
        }
        if (FCode.equalsIgnoreCase("PlanSql")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanSql = FValue.trim();
            }
            else
                PlanSql = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples3 = i;
            }
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
                PlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PlanFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanFlag = FValue.trim();
            }
            else
                PlanFlag = null;
        }
        if (FCode.equalsIgnoreCase("PremCalType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PremCalType = FValue.trim();
            }
            else
                PremCalType = null;
        }
        if (FCode.equalsIgnoreCase("InsuPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuPeriodFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuPeriodFlag = FValue.trim();
            }
            else
                InsuPeriodFlag = null;
        }
        if (FCode.equalsIgnoreCase("OccupTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupTypeFlag = FValue.trim();
            }
            else
                OccupTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("MinOccupType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MinOccupType = FValue.trim();
            }
            else
                MinOccupType = null;
        }
        if (FCode.equalsIgnoreCase("MaxOccupType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MaxOccupType = FValue.trim();
            }
            else
                MaxOccupType = null;
        }
        if (FCode.equalsIgnoreCase("OccupType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupType = FValue.trim();
            }
            else
                OccupType = null;
        }
        if (FCode.equalsIgnoreCase("OccupMidType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupMidType = FValue.trim();
            }
            else
                OccupMidType = null;
        }
        if (FCode.equalsIgnoreCase("OccupCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupCode = FValue.trim();
            }
            else
                OccupCode = null;
        }
        if (FCode.equalsIgnoreCase("MinAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MinAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MaxAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("AvgAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AvgAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("SocialInsuRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SocialInsuRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaleRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MaleRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FemaleRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FemaleRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("RetireRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RetireRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PremMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PremMode = FValue.trim();
            }
            else
                PremMode = null;
        }
        if (FCode.equalsIgnoreCase("EnterpriseRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                EnterpriseRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("MinSalary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MinSalary = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxSalary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MaxSalary = d;
            }
        }
        if (FCode.equalsIgnoreCase("AvgSalary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AvgSalary = d;
            }
        }
        if (FCode.equalsIgnoreCase("OtherDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherDesc = FValue.trim();
            }
            else
                OtherDesc = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("OccupRate")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupRate = FValue.trim();
            }
            else
                OccupRate = null;
        }
        if (FCode.equalsIgnoreCase("CombiCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CombiCode = FValue.trim();
            }
            else
                CombiCode = null;
        }
        if (FCode.equalsIgnoreCase("CombiRate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CombiRate = FValue.trim();
            }
            else
                CombiRate = null;
        }
        if (FCode.equalsIgnoreCase("CombiMult")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                CombiMult = i;
            }
        }
        if (FCode.equalsIgnoreCase("RiskPlan")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskPlan = FValue.trim();
            }
            else
                RiskPlan = null;
        }
        return true;
    }


    public String toString() {
    return "LBContPlanPojo [" +
            "GrpContNo="+GrpContNo +
            ", ProposalGrpContNo="+ProposalGrpContNo +
            ", ContPlanCode="+ContPlanCode +
            ", ContPlanName="+ContPlanName +
            ", PlanType="+PlanType +
            ", PlanRule="+PlanRule +
            ", PlanSql="+PlanSql +
            ", Remark="+Remark +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Peoples3="+Peoples3 +
            ", Remark2="+Remark2 +
            ", Peoples2="+Peoples2 +
            ", PlanCode="+PlanCode +
            ", PlanFlag="+PlanFlag +
            ", PremCalType="+PremCalType +
            ", InsuPeriod="+InsuPeriod +
            ", InsuPeriodFlag="+InsuPeriodFlag +
            ", OccupTypeFlag="+OccupTypeFlag +
            ", MinOccupType="+MinOccupType +
            ", MaxOccupType="+MaxOccupType +
            ", OccupType="+OccupType +
            ", OccupMidType="+OccupMidType +
            ", OccupCode="+OccupCode +
            ", MinAge="+MinAge +
            ", MaxAge="+MaxAge +
            ", AvgAge="+AvgAge +
            ", SocialInsuRate="+SocialInsuRate +
            ", MaleRate="+MaleRate +
            ", FemaleRate="+FemaleRate +
            ", RetireRate="+RetireRate +
            ", PremMode="+PremMode +
            ", EnterpriseRate="+EnterpriseRate +
            ", MinSalary="+MinSalary +
            ", MaxSalary="+MaxSalary +
            ", AvgSalary="+AvgSalary +
            ", OtherDesc="+OtherDesc +
            ", ModifyOperator="+ModifyOperator +
            ", OccupRate="+OccupRate +
            ", CombiCode="+CombiCode +
            ", CombiRate="+CombiRate +
            ", CombiMult="+CombiMult +
            ", RiskPlan="+RiskPlan +"]";
    }
}
