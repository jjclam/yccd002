/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: ES_DOC_MAINPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class ES_DOC_MAINPojo implements Pojo,Serializable {
    // @Field
    /** 单证编号 */
    @RedisPrimaryHKey
    private int DocID; 
    /** 单证号码 */
    @RedisIndexHKey
    private String DocCode; 
    /** 业务类型 */
    private String BussType; 
    /** 单证细类 */
    private String SubType; 
    /** 单证页数 */
    private int NumPages; 
    /** 单证状态 */
    private String DocFlag; 
    /** 备注 */
    private String DocRemark; 
    /** 扫描操作员 */
    private String ScanOperator; 
    /** 管理机构 */
    private String ManageCom; 
    /** 录入状态 */
    private String InputState; 
    /** 操作员 */
    private String Operator; 
    /** 录入开始日期 */
    private String  InputStartDate;
    /** 录入开始时间 */
    private String InputStartTime; 
    /** 录入结束日期 */
    private String  InputEndDate;
    /** 录入结束时间 */
    private String InputEndTime; 
    /** 生成日期 */
    private String  MakeDate;
    /** 生成时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** 单证版本 */
    private String Version; 
    /** 批次号 */
    private String ScanNo; 
    /** 单证印刷号码 */
    private String PrintCode; 


    public static final int FIELDNUM = 22;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public int getDocID() {
        return DocID;
    }
    public void setDocID(int aDocID) {
        DocID = aDocID;
    }
    public void setDocID(String aDocID) {
        if (aDocID != null && !aDocID.equals("")) {
            Integer tInteger = new Integer(aDocID);
            int i = tInteger.intValue();
            DocID = i;
        }
    }

    public String getDocCode() {
        return DocCode;
    }
    public void setDocCode(String aDocCode) {
        DocCode = aDocCode;
    }
    public String getBussType() {
        return BussType;
    }
    public void setBussType(String aBussType) {
        BussType = aBussType;
    }
    public String getSubType() {
        return SubType;
    }
    public void setSubType(String aSubType) {
        SubType = aSubType;
    }
    public int getNumPages() {
        return NumPages;
    }
    public void setNumPages(int aNumPages) {
        NumPages = aNumPages;
    }
    public void setNumPages(String aNumPages) {
        if (aNumPages != null && !aNumPages.equals("")) {
            Integer tInteger = new Integer(aNumPages);
            int i = tInteger.intValue();
            NumPages = i;
        }
    }

    public String getDocFlag() {
        return DocFlag;
    }
    public void setDocFlag(String aDocFlag) {
        DocFlag = aDocFlag;
    }
    public String getDocRemark() {
        return DocRemark;
    }
    public void setDocRemark(String aDocRemark) {
        DocRemark = aDocRemark;
    }
    public String getScanOperator() {
        return ScanOperator;
    }
    public void setScanOperator(String aScanOperator) {
        ScanOperator = aScanOperator;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getInputState() {
        return InputState;
    }
    public void setInputState(String aInputState) {
        InputState = aInputState;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getInputStartDate() {
        return InputStartDate;
    }
    public void setInputStartDate(String aInputStartDate) {
        InputStartDate = aInputStartDate;
    }
    public String getInputStartTime() {
        return InputStartTime;
    }
    public void setInputStartTime(String aInputStartTime) {
        InputStartTime = aInputStartTime;
    }
    public String getInputEndDate() {
        return InputEndDate;
    }
    public void setInputEndDate(String aInputEndDate) {
        InputEndDate = aInputEndDate;
    }
    public String getInputEndTime() {
        return InputEndTime;
    }
    public void setInputEndTime(String aInputEndTime) {
        InputEndTime = aInputEndTime;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getVersion() {
        return Version;
    }
    public void setVersion(String aVersion) {
        Version = aVersion;
    }
    public String getScanNo() {
        return ScanNo;
    }
    public void setScanNo(String aScanNo) {
        ScanNo = aScanNo;
    }
    public String getPrintCode() {
        return PrintCode;
    }
    public void setPrintCode(String aPrintCode) {
        PrintCode = aPrintCode;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("DocID") ) {
            return 0;
        }
        if( strFieldName.equals("DocCode") ) {
            return 1;
        }
        if( strFieldName.equals("BussType") ) {
            return 2;
        }
        if( strFieldName.equals("SubType") ) {
            return 3;
        }
        if( strFieldName.equals("NumPages") ) {
            return 4;
        }
        if( strFieldName.equals("DocFlag") ) {
            return 5;
        }
        if( strFieldName.equals("DocRemark") ) {
            return 6;
        }
        if( strFieldName.equals("ScanOperator") ) {
            return 7;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 8;
        }
        if( strFieldName.equals("InputState") ) {
            return 9;
        }
        if( strFieldName.equals("Operator") ) {
            return 10;
        }
        if( strFieldName.equals("InputStartDate") ) {
            return 11;
        }
        if( strFieldName.equals("InputStartTime") ) {
            return 12;
        }
        if( strFieldName.equals("InputEndDate") ) {
            return 13;
        }
        if( strFieldName.equals("InputEndTime") ) {
            return 14;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 15;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 18;
        }
        if( strFieldName.equals("Version") ) {
            return 19;
        }
        if( strFieldName.equals("ScanNo") ) {
            return 20;
        }
        if( strFieldName.equals("PrintCode") ) {
            return 21;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "DocID";
                break;
            case 1:
                strFieldName = "DocCode";
                break;
            case 2:
                strFieldName = "BussType";
                break;
            case 3:
                strFieldName = "SubType";
                break;
            case 4:
                strFieldName = "NumPages";
                break;
            case 5:
                strFieldName = "DocFlag";
                break;
            case 6:
                strFieldName = "DocRemark";
                break;
            case 7:
                strFieldName = "ScanOperator";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            case 9:
                strFieldName = "InputState";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "InputStartDate";
                break;
            case 12:
                strFieldName = "InputStartTime";
                break;
            case 13:
                strFieldName = "InputEndDate";
                break;
            case 14:
                strFieldName = "InputEndTime";
                break;
            case 15:
                strFieldName = "MakeDate";
                break;
            case 16:
                strFieldName = "MakeTime";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            case 19:
                strFieldName = "Version";
                break;
            case 20:
                strFieldName = "ScanNo";
                break;
            case 21:
                strFieldName = "PrintCode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "DOCID":
                return Schema.TYPE_INT;
            case "DOCCODE":
                return Schema.TYPE_STRING;
            case "BUSSTYPE":
                return Schema.TYPE_STRING;
            case "SUBTYPE":
                return Schema.TYPE_STRING;
            case "NUMPAGES":
                return Schema.TYPE_INT;
            case "DOCFLAG":
                return Schema.TYPE_STRING;
            case "DOCREMARK":
                return Schema.TYPE_STRING;
            case "SCANOPERATOR":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "INPUTSTATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "INPUTSTARTDATE":
                return Schema.TYPE_STRING;
            case "INPUTSTARTTIME":
                return Schema.TYPE_STRING;
            case "INPUTENDDATE":
                return Schema.TYPE_STRING;
            case "INPUTENDTIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "VERSION":
                return Schema.TYPE_STRING;
            case "SCANNO":
                return Schema.TYPE_STRING;
            case "PRINTCODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_INT;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_INT;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("DocID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocID));
        }
        if (FCode.equalsIgnoreCase("DocCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocCode));
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussType));
        }
        if (FCode.equalsIgnoreCase("SubType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubType));
        }
        if (FCode.equalsIgnoreCase("NumPages")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NumPages));
        }
        if (FCode.equalsIgnoreCase("DocFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocFlag));
        }
        if (FCode.equalsIgnoreCase("DocRemark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocRemark));
        }
        if (FCode.equalsIgnoreCase("ScanOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanOperator));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("InputState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputState));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("InputStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputStartDate));
        }
        if (FCode.equalsIgnoreCase("InputStartTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputStartTime));
        }
        if (FCode.equalsIgnoreCase("InputEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputEndDate));
        }
        if (FCode.equalsIgnoreCase("InputEndTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputEndTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Version")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Version));
        }
        if (FCode.equalsIgnoreCase("ScanNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScanNo));
        }
        if (FCode.equalsIgnoreCase("PrintCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(DocID);
                break;
            case 1:
                strFieldValue = String.valueOf(DocCode);
                break;
            case 2:
                strFieldValue = String.valueOf(BussType);
                break;
            case 3:
                strFieldValue = String.valueOf(SubType);
                break;
            case 4:
                strFieldValue = String.valueOf(NumPages);
                break;
            case 5:
                strFieldValue = String.valueOf(DocFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(DocRemark);
                break;
            case 7:
                strFieldValue = String.valueOf(ScanOperator);
                break;
            case 8:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 9:
                strFieldValue = String.valueOf(InputState);
                break;
            case 10:
                strFieldValue = String.valueOf(Operator);
                break;
            case 11:
                strFieldValue = String.valueOf(InputStartDate);
                break;
            case 12:
                strFieldValue = String.valueOf(InputStartTime);
                break;
            case 13:
                strFieldValue = String.valueOf(InputEndDate);
                break;
            case 14:
                strFieldValue = String.valueOf(InputEndTime);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 16:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 17:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 19:
                strFieldValue = String.valueOf(Version);
                break;
            case 20:
                strFieldValue = String.valueOf(ScanNo);
                break;
            case 21:
                strFieldValue = String.valueOf(PrintCode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("DocID")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                DocID = i;
            }
        }
        if (FCode.equalsIgnoreCase("DocCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DocCode = FValue.trim();
            }
            else
                DocCode = null;
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BussType = FValue.trim();
            }
            else
                BussType = null;
        }
        if (FCode.equalsIgnoreCase("SubType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubType = FValue.trim();
            }
            else
                SubType = null;
        }
        if (FCode.equalsIgnoreCase("NumPages")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                NumPages = i;
            }
        }
        if (FCode.equalsIgnoreCase("DocFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DocFlag = FValue.trim();
            }
            else
                DocFlag = null;
        }
        if (FCode.equalsIgnoreCase("DocRemark")) {
            if( FValue != null && !FValue.equals(""))
            {
                DocRemark = FValue.trim();
            }
            else
                DocRemark = null;
        }
        if (FCode.equalsIgnoreCase("ScanOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ScanOperator = FValue.trim();
            }
            else
                ScanOperator = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("InputState")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputState = FValue.trim();
            }
            else
                InputState = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("InputStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputStartDate = FValue.trim();
            }
            else
                InputStartDate = null;
        }
        if (FCode.equalsIgnoreCase("InputStartTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputStartTime = FValue.trim();
            }
            else
                InputStartTime = null;
        }
        if (FCode.equalsIgnoreCase("InputEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputEndDate = FValue.trim();
            }
            else
                InputEndDate = null;
        }
        if (FCode.equalsIgnoreCase("InputEndTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputEndTime = FValue.trim();
            }
            else
                InputEndTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Version")) {
            if( FValue != null && !FValue.equals(""))
            {
                Version = FValue.trim();
            }
            else
                Version = null;
        }
        if (FCode.equalsIgnoreCase("ScanNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ScanNo = FValue.trim();
            }
            else
                ScanNo = null;
        }
        if (FCode.equalsIgnoreCase("PrintCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintCode = FValue.trim();
            }
            else
                PrintCode = null;
        }
        return true;
    }


    public String toString() {
    return "ES_DOC_MAINPojo [" +
            "DocID="+DocID +
            ", DocCode="+DocCode +
            ", BussType="+BussType +
            ", SubType="+SubType +
            ", NumPages="+NumPages +
            ", DocFlag="+DocFlag +
            ", DocRemark="+DocRemark +
            ", ScanOperator="+ScanOperator +
            ", ManageCom="+ManageCom +
            ", InputState="+InputState +
            ", Operator="+Operator +
            ", InputStartDate="+InputStartDate +
            ", InputStartTime="+InputStartTime +
            ", InputEndDate="+InputEndDate +
            ", InputEndTime="+InputEndTime +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Version="+Version +
            ", ScanNo="+ScanNo +
            ", PrintCode="+PrintCode +"]";
    }
}
