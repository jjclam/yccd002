/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-12-03
 */
public class LMRiskPojo implements  Pojo,Serializable {
    // @Field
    /** 险种编码 */
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 险种名称 */
    private String RiskName; 
    /** 险种简称 */
    private String RiskShortName; 
    /** 险种英文名称 */
    private String RiskEnName; 
    /** 险种英文简称 */
    private String RiskEnShortName; 
    /** 选择责任标记 */
    private String ChoDutyFlag; 
    /** 续期收费标记 */
    private String CPayFlag; 
    /** 生存给付标记 */
    private String GetFlag; 
    /** 保全标记 */
    private String EdorFlag; 
    /** 续保标记 */
    private String RnewFlag; 
    /** 核保标记 */
    private String UWFlag; 
    /** 分保标记 */
    private String RinsFlag; 
    /** 保险帐户标记 */
    private String InsuAccFlag; 
    /** 预定利率 */
    private double DestRate; 
    /** 原险种编码 */
    private String OrigRiskCode; 
    /** 子版本号 */
    private String SubRiskVer; 
    /** 险种统计名称 */
    private String RiskStatName; 
    /** 标准产品标记 */
    private String StandardFlag; 
    /** 产品上下架状态 */
    private String UpDownStatus; 


    public static final int FIELDNUM = 20;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getRiskShortName() {
        return RiskShortName;
    }
    public void setRiskShortName(String aRiskShortName) {
        RiskShortName = aRiskShortName;
    }
    public String getRiskEnName() {
        return RiskEnName;
    }
    public void setRiskEnName(String aRiskEnName) {
        RiskEnName = aRiskEnName;
    }
    public String getRiskEnShortName() {
        return RiskEnShortName;
    }
    public void setRiskEnShortName(String aRiskEnShortName) {
        RiskEnShortName = aRiskEnShortName;
    }
    public String getChoDutyFlag() {
        return ChoDutyFlag;
    }
    public void setChoDutyFlag(String aChoDutyFlag) {
        ChoDutyFlag = aChoDutyFlag;
    }
    public String getCPayFlag() {
        return CPayFlag;
    }
    public void setCPayFlag(String aCPayFlag) {
        CPayFlag = aCPayFlag;
    }
    public String getGetFlag() {
        return GetFlag;
    }
    public void setGetFlag(String aGetFlag) {
        GetFlag = aGetFlag;
    }
    public String getEdorFlag() {
        return EdorFlag;
    }
    public void setEdorFlag(String aEdorFlag) {
        EdorFlag = aEdorFlag;
    }
    public String getRnewFlag() {
        return RnewFlag;
    }
    public void setRnewFlag(String aRnewFlag) {
        RnewFlag = aRnewFlag;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getRinsFlag() {
        return RinsFlag;
    }
    public void setRinsFlag(String aRinsFlag) {
        RinsFlag = aRinsFlag;
    }
    public String getInsuAccFlag() {
        return InsuAccFlag;
    }
    public void setInsuAccFlag(String aInsuAccFlag) {
        InsuAccFlag = aInsuAccFlag;
    }
    public double getDestRate() {
        return DestRate;
    }
    public void setDestRate(double aDestRate) {
        DestRate = aDestRate;
    }
    public void setDestRate(String aDestRate) {
        if (aDestRate != null && !aDestRate.equals("")) {
            Double tDouble = new Double(aDestRate);
            double d = tDouble.doubleValue();
            DestRate = d;
        }
    }

    public String getOrigRiskCode() {
        return OrigRiskCode;
    }
    public void setOrigRiskCode(String aOrigRiskCode) {
        OrigRiskCode = aOrigRiskCode;
    }
    public String getSubRiskVer() {
        return SubRiskVer;
    }
    public void setSubRiskVer(String aSubRiskVer) {
        SubRiskVer = aSubRiskVer;
    }
    public String getRiskStatName() {
        return RiskStatName;
    }
    public void setRiskStatName(String aRiskStatName) {
        RiskStatName = aRiskStatName;
    }
    public String getStandardFlag() {
        return StandardFlag;
    }
    public void setStandardFlag(String aStandardFlag) {
        StandardFlag = aStandardFlag;
    }
    public String getUpDownStatus() {
        return UpDownStatus;
    }
    public void setUpDownStatus(String aUpDownStatus) {
        UpDownStatus = aUpDownStatus;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("RiskName") ) {
            return 2;
        }
        if( strFieldName.equals("RiskShortName") ) {
            return 3;
        }
        if( strFieldName.equals("RiskEnName") ) {
            return 4;
        }
        if( strFieldName.equals("RiskEnShortName") ) {
            return 5;
        }
        if( strFieldName.equals("ChoDutyFlag") ) {
            return 6;
        }
        if( strFieldName.equals("CPayFlag") ) {
            return 7;
        }
        if( strFieldName.equals("GetFlag") ) {
            return 8;
        }
        if( strFieldName.equals("EdorFlag") ) {
            return 9;
        }
        if( strFieldName.equals("RnewFlag") ) {
            return 10;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 11;
        }
        if( strFieldName.equals("RinsFlag") ) {
            return 12;
        }
        if( strFieldName.equals("InsuAccFlag") ) {
            return 13;
        }
        if( strFieldName.equals("DestRate") ) {
            return 14;
        }
        if( strFieldName.equals("OrigRiskCode") ) {
            return 15;
        }
        if( strFieldName.equals("SubRiskVer") ) {
            return 16;
        }
        if( strFieldName.equals("RiskStatName") ) {
            return 17;
        }
        if( strFieldName.equals("StandardFlag") ) {
            return 18;
        }
        if( strFieldName.equals("UpDownStatus") ) {
            return 19;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "RiskShortName";
                break;
            case 4:
                strFieldName = "RiskEnName";
                break;
            case 5:
                strFieldName = "RiskEnShortName";
                break;
            case 6:
                strFieldName = "ChoDutyFlag";
                break;
            case 7:
                strFieldName = "CPayFlag";
                break;
            case 8:
                strFieldName = "GetFlag";
                break;
            case 9:
                strFieldName = "EdorFlag";
                break;
            case 10:
                strFieldName = "RnewFlag";
                break;
            case 11:
                strFieldName = "UWFlag";
                break;
            case 12:
                strFieldName = "RinsFlag";
                break;
            case 13:
                strFieldName = "InsuAccFlag";
                break;
            case 14:
                strFieldName = "DestRate";
                break;
            case 15:
                strFieldName = "OrigRiskCode";
                break;
            case 16:
                strFieldName = "SubRiskVer";
                break;
            case 17:
                strFieldName = "RiskStatName";
                break;
            case 18:
                strFieldName = "StandardFlag";
                break;
            case 19:
                strFieldName = "UpDownStatus";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "RISKSHORTNAME":
                return Schema.TYPE_STRING;
            case "RISKENNAME":
                return Schema.TYPE_STRING;
            case "RISKENSHORTNAME":
                return Schema.TYPE_STRING;
            case "CHODUTYFLAG":
                return Schema.TYPE_STRING;
            case "CPAYFLAG":
                return Schema.TYPE_STRING;
            case "GETFLAG":
                return Schema.TYPE_STRING;
            case "EDORFLAG":
                return Schema.TYPE_STRING;
            case "RNEWFLAG":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "RINSFLAG":
                return Schema.TYPE_STRING;
            case "INSUACCFLAG":
                return Schema.TYPE_STRING;
            case "DESTRATE":
                return Schema.TYPE_DOUBLE;
            case "ORIGRISKCODE":
                return Schema.TYPE_STRING;
            case "SUBRISKVER":
                return Schema.TYPE_STRING;
            case "RISKSTATNAME":
                return Schema.TYPE_STRING;
            case "STANDARDFLAG":
                return Schema.TYPE_STRING;
            case "UPDOWNSTATUS":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("RiskShortName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskShortName));
        }
        if (FCode.equalsIgnoreCase("RiskEnName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskEnName));
        }
        if (FCode.equalsIgnoreCase("RiskEnShortName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskEnShortName));
        }
        if (FCode.equalsIgnoreCase("ChoDutyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChoDutyFlag));
        }
        if (FCode.equalsIgnoreCase("CPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CPayFlag));
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetFlag));
        }
        if (FCode.equalsIgnoreCase("EdorFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorFlag));
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewFlag));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("RinsFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RinsFlag));
        }
        if (FCode.equalsIgnoreCase("InsuAccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccFlag));
        }
        if (FCode.equalsIgnoreCase("DestRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestRate));
        }
        if (FCode.equalsIgnoreCase("OrigRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrigRiskCode));
        }
        if (FCode.equalsIgnoreCase("SubRiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubRiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskStatName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskStatName));
        }
        if (FCode.equalsIgnoreCase("StandardFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandardFlag));
        }
        if (FCode.equalsIgnoreCase("UpDownStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpDownStatus));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 3:
                strFieldValue = String.valueOf(RiskShortName);
                break;
            case 4:
                strFieldValue = String.valueOf(RiskEnName);
                break;
            case 5:
                strFieldValue = String.valueOf(RiskEnShortName);
                break;
            case 6:
                strFieldValue = String.valueOf(ChoDutyFlag);
                break;
            case 7:
                strFieldValue = String.valueOf(CPayFlag);
                break;
            case 8:
                strFieldValue = String.valueOf(GetFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(EdorFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(RnewFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(RinsFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(InsuAccFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(DestRate);
                break;
            case 15:
                strFieldValue = String.valueOf(OrigRiskCode);
                break;
            case 16:
                strFieldValue = String.valueOf(SubRiskVer);
                break;
            case 17:
                strFieldValue = String.valueOf(RiskStatName);
                break;
            case 18:
                strFieldValue = String.valueOf(StandardFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(UpDownStatus);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("RiskShortName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskShortName = FValue.trim();
            }
            else
                RiskShortName = null;
        }
        if (FCode.equalsIgnoreCase("RiskEnName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskEnName = FValue.trim();
            }
            else
                RiskEnName = null;
        }
        if (FCode.equalsIgnoreCase("RiskEnShortName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskEnShortName = FValue.trim();
            }
            else
                RiskEnShortName = null;
        }
        if (FCode.equalsIgnoreCase("ChoDutyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChoDutyFlag = FValue.trim();
            }
            else
                ChoDutyFlag = null;
        }
        if (FCode.equalsIgnoreCase("CPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CPayFlag = FValue.trim();
            }
            else
                CPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetFlag = FValue.trim();
            }
            else
                GetFlag = null;
        }
        if (FCode.equalsIgnoreCase("EdorFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorFlag = FValue.trim();
            }
            else
                EdorFlag = null;
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RnewFlag = FValue.trim();
            }
            else
                RnewFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("RinsFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RinsFlag = FValue.trim();
            }
            else
                RinsFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccFlag = FValue.trim();
            }
            else
                InsuAccFlag = null;
        }
        if (FCode.equalsIgnoreCase("DestRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DestRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("OrigRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrigRiskCode = FValue.trim();
            }
            else
                OrigRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("SubRiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubRiskVer = FValue.trim();
            }
            else
                SubRiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskStatName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskStatName = FValue.trim();
            }
            else
                RiskStatName = null;
        }
        if (FCode.equalsIgnoreCase("StandardFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandardFlag = FValue.trim();
            }
            else
                StandardFlag = null;
        }
        if (FCode.equalsIgnoreCase("UpDownStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpDownStatus = FValue.trim();
            }
            else
                UpDownStatus = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", RiskName="+RiskName +
            ", RiskShortName="+RiskShortName +
            ", RiskEnName="+RiskEnName +
            ", RiskEnShortName="+RiskEnShortName +
            ", ChoDutyFlag="+ChoDutyFlag +
            ", CPayFlag="+CPayFlag +
            ", GetFlag="+GetFlag +
            ", EdorFlag="+EdorFlag +
            ", RnewFlag="+RnewFlag +
            ", UWFlag="+UWFlag +
            ", RinsFlag="+RinsFlag +
            ", InsuAccFlag="+InsuAccFlag +
            ", DestRate="+DestRate +
            ", OrigRiskCode="+OrigRiskCode +
            ", SubRiskVer="+SubRiskVer +
            ", RiskStatName="+RiskStatName +
            ", StandardFlag="+StandardFlag +
            ", UpDownStatus="+UpDownStatus +"]";
    }
}
