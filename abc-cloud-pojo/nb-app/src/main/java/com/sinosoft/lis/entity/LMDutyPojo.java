/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyPojo implements Pojo,Serializable {
    // @Field
    /** 责任代码 */
    @RedisPrimaryHKey
    private String DutyCode; 
    /** 责任名称 */
    private String DutyName; 
    /** 缴费终止期间 */
    private int PayEndYear; 
    /** 缴费终止期间单位 */
    private String PayEndYearFlag; 
    /** 缴费终止日期计算参照 */
    private String PayEndDateCalRef; 
    /** 缴费终止日期计算方式 */
    private String PayEndDateCalMode; 
    /** 起领期间 */
    private int GetYear; 
    /** 起领期间单位 */
    private String GetYearFlag; 
    /** 保险期间 */
    private int InsuYear; 
    /** 保险期间单位 */
    private String InsuYearFlag; 
    /** 意外责任期间 */
    private int AcciYear; 
    /** 意外责任期间单位 */
    private String AcciYearFlag; 
    /** 计算方法 */
    private String CalMode; 
    /** 缴费终止期间关系 */
    private String PayEndYearRela; 
    /** 起领期间关系 */
    private String GetYearRela; 
    /** 保险期间关系 */
    private String InsuYearRela; 
    /** 基本保额算法 */
    private String BasicCalCode; 
    /** 风险保额算法 */
    private String RiskCalCode; 
    /** 保额标记 */
    private String AmntFlag; 
    /** 单位保额 */
    private double VPU; 
    /** 加费类型 */
    private String AddFeeFlag; 
    /** 保险终止日期计算方式 */
    private String EndDateCalMode; 
    /** 是否加入保额标记 */
    private String AddAmntFlag; 


    public static final int FIELDNUM = 23;    // 数据库表的字段个数
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getDutyName() {
        return DutyName;
    }
    public void setDutyName(String aDutyName) {
        DutyName = aDutyName;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }
    public void setPayEndYearFlag(String aPayEndYearFlag) {
        PayEndYearFlag = aPayEndYearFlag;
    }
    public String getPayEndDateCalRef() {
        return PayEndDateCalRef;
    }
    public void setPayEndDateCalRef(String aPayEndDateCalRef) {
        PayEndDateCalRef = aPayEndDateCalRef;
    }
    public String getPayEndDateCalMode() {
        return PayEndDateCalMode;
    }
    public void setPayEndDateCalMode(String aPayEndDateCalMode) {
        PayEndDateCalMode = aPayEndDateCalMode;
    }
    public int getGetYear() {
        return GetYear;
    }
    public void setGetYear(int aGetYear) {
        GetYear = aGetYear;
    }
    public void setGetYear(String aGetYear) {
        if (aGetYear != null && !aGetYear.equals("")) {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }
    public void setGetYearFlag(String aGetYearFlag) {
        GetYearFlag = aGetYearFlag;
    }
    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public int getAcciYear() {
        return AcciYear;
    }
    public void setAcciYear(int aAcciYear) {
        AcciYear = aAcciYear;
    }
    public void setAcciYear(String aAcciYear) {
        if (aAcciYear != null && !aAcciYear.equals("")) {
            Integer tInteger = new Integer(aAcciYear);
            int i = tInteger.intValue();
            AcciYear = i;
        }
    }

    public String getAcciYearFlag() {
        return AcciYearFlag;
    }
    public void setAcciYearFlag(String aAcciYearFlag) {
        AcciYearFlag = aAcciYearFlag;
    }
    public String getCalMode() {
        return CalMode;
    }
    public void setCalMode(String aCalMode) {
        CalMode = aCalMode;
    }
    public String getPayEndYearRela() {
        return PayEndYearRela;
    }
    public void setPayEndYearRela(String aPayEndYearRela) {
        PayEndYearRela = aPayEndYearRela;
    }
    public String getGetYearRela() {
        return GetYearRela;
    }
    public void setGetYearRela(String aGetYearRela) {
        GetYearRela = aGetYearRela;
    }
    public String getInsuYearRela() {
        return InsuYearRela;
    }
    public void setInsuYearRela(String aInsuYearRela) {
        InsuYearRela = aInsuYearRela;
    }
    public String getBasicCalCode() {
        return BasicCalCode;
    }
    public void setBasicCalCode(String aBasicCalCode) {
        BasicCalCode = aBasicCalCode;
    }
    public String getRiskCalCode() {
        return RiskCalCode;
    }
    public void setRiskCalCode(String aRiskCalCode) {
        RiskCalCode = aRiskCalCode;
    }
    public String getAmntFlag() {
        return AmntFlag;
    }
    public void setAmntFlag(String aAmntFlag) {
        AmntFlag = aAmntFlag;
    }
    public double getVPU() {
        return VPU;
    }
    public void setVPU(double aVPU) {
        VPU = aVPU;
    }
    public void setVPU(String aVPU) {
        if (aVPU != null && !aVPU.equals("")) {
            Double tDouble = new Double(aVPU);
            double d = tDouble.doubleValue();
            VPU = d;
        }
    }

    public String getAddFeeFlag() {
        return AddFeeFlag;
    }
    public void setAddFeeFlag(String aAddFeeFlag) {
        AddFeeFlag = aAddFeeFlag;
    }
    public String getEndDateCalMode() {
        return EndDateCalMode;
    }
    public void setEndDateCalMode(String aEndDateCalMode) {
        EndDateCalMode = aEndDateCalMode;
    }
    public String getAddAmntFlag() {
        return AddAmntFlag;
    }
    public void setAddAmntFlag(String aAddAmntFlag) {
        AddAmntFlag = aAddAmntFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("DutyCode") ) {
            return 0;
        }
        if( strFieldName.equals("DutyName") ) {
            return 1;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 2;
        }
        if( strFieldName.equals("PayEndYearFlag") ) {
            return 3;
        }
        if( strFieldName.equals("PayEndDateCalRef") ) {
            return 4;
        }
        if( strFieldName.equals("PayEndDateCalMode") ) {
            return 5;
        }
        if( strFieldName.equals("GetYear") ) {
            return 6;
        }
        if( strFieldName.equals("GetYearFlag") ) {
            return 7;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 8;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 9;
        }
        if( strFieldName.equals("AcciYear") ) {
            return 10;
        }
        if( strFieldName.equals("AcciYearFlag") ) {
            return 11;
        }
        if( strFieldName.equals("CalMode") ) {
            return 12;
        }
        if( strFieldName.equals("PayEndYearRela") ) {
            return 13;
        }
        if( strFieldName.equals("GetYearRela") ) {
            return 14;
        }
        if( strFieldName.equals("InsuYearRela") ) {
            return 15;
        }
        if( strFieldName.equals("BasicCalCode") ) {
            return 16;
        }
        if( strFieldName.equals("RiskCalCode") ) {
            return 17;
        }
        if( strFieldName.equals("AmntFlag") ) {
            return 18;
        }
        if( strFieldName.equals("VPU") ) {
            return 19;
        }
        if( strFieldName.equals("AddFeeFlag") ) {
            return 20;
        }
        if( strFieldName.equals("EndDateCalMode") ) {
            return 21;
        }
        if( strFieldName.equals("AddAmntFlag") ) {
            return 22;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "DutyCode";
                break;
            case 1:
                strFieldName = "DutyName";
                break;
            case 2:
                strFieldName = "PayEndYear";
                break;
            case 3:
                strFieldName = "PayEndYearFlag";
                break;
            case 4:
                strFieldName = "PayEndDateCalRef";
                break;
            case 5:
                strFieldName = "PayEndDateCalMode";
                break;
            case 6:
                strFieldName = "GetYear";
                break;
            case 7:
                strFieldName = "GetYearFlag";
                break;
            case 8:
                strFieldName = "InsuYear";
                break;
            case 9:
                strFieldName = "InsuYearFlag";
                break;
            case 10:
                strFieldName = "AcciYear";
                break;
            case 11:
                strFieldName = "AcciYearFlag";
                break;
            case 12:
                strFieldName = "CalMode";
                break;
            case 13:
                strFieldName = "PayEndYearRela";
                break;
            case 14:
                strFieldName = "GetYearRela";
                break;
            case 15:
                strFieldName = "InsuYearRela";
                break;
            case 16:
                strFieldName = "BasicCalCode";
                break;
            case 17:
                strFieldName = "RiskCalCode";
                break;
            case 18:
                strFieldName = "AmntFlag";
                break;
            case 19:
                strFieldName = "VPU";
                break;
            case 20:
                strFieldName = "AddFeeFlag";
                break;
            case 21:
                strFieldName = "EndDateCalMode";
                break;
            case 22:
                strFieldName = "AddAmntFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "DUTYNAME":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "PAYENDYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYENDDATECALREF":
                return Schema.TYPE_STRING;
            case "PAYENDDATECALMODE":
                return Schema.TYPE_STRING;
            case "GETYEAR":
                return Schema.TYPE_INT;
            case "GETYEARFLAG":
                return Schema.TYPE_STRING;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "ACCIYEAR":
                return Schema.TYPE_INT;
            case "ACCIYEARFLAG":
                return Schema.TYPE_STRING;
            case "CALMODE":
                return Schema.TYPE_STRING;
            case "PAYENDYEARRELA":
                return Schema.TYPE_STRING;
            case "GETYEARRELA":
                return Schema.TYPE_STRING;
            case "INSUYEARRELA":
                return Schema.TYPE_STRING;
            case "BASICCALCODE":
                return Schema.TYPE_STRING;
            case "RISKCALCODE":
                return Schema.TYPE_STRING;
            case "AMNTFLAG":
                return Schema.TYPE_STRING;
            case "VPU":
                return Schema.TYPE_DOUBLE;
            case "ADDFEEFLAG":
                return Schema.TYPE_STRING;
            case "ENDDATECALMODE":
                return Schema.TYPE_STRING;
            case "ADDAMNTFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_INT;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_INT;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_INT;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("DutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyName));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayEndDateCalRef")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDateCalRef));
        }
        if (FCode.equalsIgnoreCase("PayEndDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDateCalMode));
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("AcciYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciYear));
        }
        if (FCode.equalsIgnoreCase("AcciYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciYearFlag));
        }
        if (FCode.equalsIgnoreCase("CalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalMode));
        }
        if (FCode.equalsIgnoreCase("PayEndYearRela")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearRela));
        }
        if (FCode.equalsIgnoreCase("GetYearRela")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearRela));
        }
        if (FCode.equalsIgnoreCase("InsuYearRela")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearRela));
        }
        if (FCode.equalsIgnoreCase("BasicCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BasicCalCode));
        }
        if (FCode.equalsIgnoreCase("RiskCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCalCode));
        }
        if (FCode.equalsIgnoreCase("AmntFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntFlag));
        }
        if (FCode.equalsIgnoreCase("VPU")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VPU));
        }
        if (FCode.equalsIgnoreCase("AddFeeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddFeeFlag));
        }
        if (FCode.equalsIgnoreCase("EndDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDateCalMode));
        }
        if (FCode.equalsIgnoreCase("AddAmntFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddAmntFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 1:
                strFieldValue = String.valueOf(DutyName);
                break;
            case 2:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 3:
                strFieldValue = String.valueOf(PayEndYearFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(PayEndDateCalRef);
                break;
            case 5:
                strFieldValue = String.valueOf(PayEndDateCalMode);
                break;
            case 6:
                strFieldValue = String.valueOf(GetYear);
                break;
            case 7:
                strFieldValue = String.valueOf(GetYearFlag);
                break;
            case 8:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 9:
                strFieldValue = String.valueOf(InsuYearFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(AcciYear);
                break;
            case 11:
                strFieldValue = String.valueOf(AcciYearFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(CalMode);
                break;
            case 13:
                strFieldValue = String.valueOf(PayEndYearRela);
                break;
            case 14:
                strFieldValue = String.valueOf(GetYearRela);
                break;
            case 15:
                strFieldValue = String.valueOf(InsuYearRela);
                break;
            case 16:
                strFieldValue = String.valueOf(BasicCalCode);
                break;
            case 17:
                strFieldValue = String.valueOf(RiskCalCode);
                break;
            case 18:
                strFieldValue = String.valueOf(AmntFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(VPU);
                break;
            case 20:
                strFieldValue = String.valueOf(AddFeeFlag);
                break;
            case 21:
                strFieldValue = String.valueOf(EndDateCalMode);
                break;
            case 22:
                strFieldValue = String.valueOf(AddAmntFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyName = FValue.trim();
            }
            else
                DutyName = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
                PayEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDateCalRef")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDateCalRef = FValue.trim();
            }
            else
                PayEndDateCalRef = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDateCalMode = FValue.trim();
            }
            else
                PayEndDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
                GetYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("AcciYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AcciYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("AcciYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AcciYearFlag = FValue.trim();
            }
            else
                AcciYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalMode = FValue.trim();
            }
            else
                CalMode = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYearRela")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearRela = FValue.trim();
            }
            else
                PayEndYearRela = null;
        }
        if (FCode.equalsIgnoreCase("GetYearRela")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetYearRela = FValue.trim();
            }
            else
                GetYearRela = null;
        }
        if (FCode.equalsIgnoreCase("InsuYearRela")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearRela = FValue.trim();
            }
            else
                InsuYearRela = null;
        }
        if (FCode.equalsIgnoreCase("BasicCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BasicCalCode = FValue.trim();
            }
            else
                BasicCalCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCalCode = FValue.trim();
            }
            else
                RiskCalCode = null;
        }
        if (FCode.equalsIgnoreCase("AmntFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntFlag = FValue.trim();
            }
            else
                AmntFlag = null;
        }
        if (FCode.equalsIgnoreCase("VPU")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                VPU = d;
            }
        }
        if (FCode.equalsIgnoreCase("AddFeeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddFeeFlag = FValue.trim();
            }
            else
                AddFeeFlag = null;
        }
        if (FCode.equalsIgnoreCase("EndDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDateCalMode = FValue.trim();
            }
            else
                EndDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("AddAmntFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddAmntFlag = FValue.trim();
            }
            else
                AddAmntFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyPojo [" +
            "DutyCode="+DutyCode +
            ", DutyName="+DutyName +
            ", PayEndYear="+PayEndYear +
            ", PayEndYearFlag="+PayEndYearFlag +
            ", PayEndDateCalRef="+PayEndDateCalRef +
            ", PayEndDateCalMode="+PayEndDateCalMode +
            ", GetYear="+GetYear +
            ", GetYearFlag="+GetYearFlag +
            ", InsuYear="+InsuYear +
            ", InsuYearFlag="+InsuYearFlag +
            ", AcciYear="+AcciYear +
            ", AcciYearFlag="+AcciYearFlag +
            ", CalMode="+CalMode +
            ", PayEndYearRela="+PayEndYearRela +
            ", GetYearRela="+GetYearRela +
            ", InsuYearRela="+InsuYearRela +
            ", BasicCalCode="+BasicCalCode +
            ", RiskCalCode="+RiskCalCode +
            ", AmntFlag="+AmntFlag +
            ", VPU="+VPU +
            ", AddFeeFlag="+AddFeeFlag +
            ", EndDateCalMode="+EndDateCalMode +
            ", AddAmntFlag="+AddAmntFlag +"]";
    }
}
