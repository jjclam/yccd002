/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LLSubReportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LLSubReportDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LLSubReportDBSet extends LLSubReportSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LLSubReportDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LLSubReport");
        mflag = true;
    }

    public LLSubReportDBSet() {
        db = new DBOper( "LLSubReport" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSubReportDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LLSubReport WHERE  1=1  AND SubRptNo = ? AND CustomerNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSubRptNo() == null || this.get(i).getSubRptNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSubRptNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSubReportDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LLSubReport SET  SubRptNo = ? , CustomerNo = ? , CustomerName = ? , CustomerType = ? , AccSubject = ? , AccidentType = ? , AccDate = ? , AccEndDate = ? , AccDesc = ? , CustSituation = ? , AccPlace = ? , HospitalCode = ? , HospitalName = ? , InHospitalDate = ? , OutHospitalDate = ? , Remark = ? , SeriousGrade = ? , SurveyFlag = ? , Operator = ? , MngCom = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FirstDiaDate = ? , HosGrade = ? , LocalPlace = ? , HosTel = ? , DieDate = ? , AccCause = ? , VIPFlag = ? , AccidentDetail = ? , CureDesc = ? , SubmitFlag = ? , CondoleFlag = ? , DieFlag = ? , AccResult1 = ? , AccResult2 = ? , CaseNo = ? , SeqNo = ? WHERE  1=1  AND SubRptNo = ? AND CustomerNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSubRptNo() == null || this.get(i).getSubRptNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSubRptNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
            if(this.get(i).getCustomerName() == null || this.get(i).getCustomerName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getCustomerName());
            }
            if(this.get(i).getCustomerType() == null || this.get(i).getCustomerType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getCustomerType());
            }
            if(this.get(i).getAccSubject() == null || this.get(i).getAccSubject().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAccSubject());
            }
            if(this.get(i).getAccidentType() == null || this.get(i).getAccidentType().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getAccidentType());
            }
            if(this.get(i).getAccDate() == null || this.get(i).getAccDate().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getAccDate()));
            }
            if(this.get(i).getAccEndDate() == null || this.get(i).getAccEndDate().equals("null")) {
                pstmt.setDate(8,null);
            } else {
                pstmt.setDate(8, Date.valueOf(this.get(i).getAccEndDate()));
            }
            if(this.get(i).getAccDesc() == null || this.get(i).getAccDesc().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getAccDesc());
            }
            if(this.get(i).getCustSituation() == null || this.get(i).getCustSituation().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getCustSituation());
            }
            if(this.get(i).getAccPlace() == null || this.get(i).getAccPlace().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAccPlace());
            }
            if(this.get(i).getHospitalCode() == null || this.get(i).getHospitalCode().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getHospitalCode());
            }
            if(this.get(i).getHospitalName() == null || this.get(i).getHospitalName().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getHospitalName());
            }
            if(this.get(i).getInHospitalDate() == null || this.get(i).getInHospitalDate().equals("null")) {
                pstmt.setDate(14,null);
            } else {
                pstmt.setDate(14, Date.valueOf(this.get(i).getInHospitalDate()));
            }
            if(this.get(i).getOutHospitalDate() == null || this.get(i).getOutHospitalDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getOutHospitalDate()));
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getRemark());
            }
            if(this.get(i).getSeriousGrade() == null || this.get(i).getSeriousGrade().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getSeriousGrade());
            }
            if(this.get(i).getSurveyFlag() == null || this.get(i).getSurveyFlag().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getSurveyFlag());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getOperator());
            }
            if(this.get(i).getMngCom() == null || this.get(i).getMngCom().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getMngCom());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(21,null);
            } else {
                pstmt.setDate(21, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getModifyTime());
            }
            if(this.get(i).getFirstDiaDate() == null || this.get(i).getFirstDiaDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getFirstDiaDate()));
            }
            if(this.get(i).getHosGrade() == null || this.get(i).getHosGrade().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getHosGrade());
            }
            if(this.get(i).getLocalPlace() == null || this.get(i).getLocalPlace().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getLocalPlace());
            }
            if(this.get(i).getHosTel() == null || this.get(i).getHosTel().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getHosTel());
            }
            if(this.get(i).getDieDate() == null || this.get(i).getDieDate().equals("null")) {
                pstmt.setDate(29,null);
            } else {
                pstmt.setDate(29, Date.valueOf(this.get(i).getDieDate()));
            }
            if(this.get(i).getAccCause() == null || this.get(i).getAccCause().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getAccCause());
            }
            if(this.get(i).getVIPFlag() == null || this.get(i).getVIPFlag().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getVIPFlag());
            }
            if(this.get(i).getAccidentDetail() == null || this.get(i).getAccidentDetail().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getAccidentDetail());
            }
            if(this.get(i).getCureDesc() == null || this.get(i).getCureDesc().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getCureDesc());
            }
            if(this.get(i).getSubmitFlag() == null || this.get(i).getSubmitFlag().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getSubmitFlag());
            }
            if(this.get(i).getCondoleFlag() == null || this.get(i).getCondoleFlag().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getCondoleFlag());
            }
            if(this.get(i).getDieFlag() == null || this.get(i).getDieFlag().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getDieFlag());
            }
            if(this.get(i).getAccResult1() == null || this.get(i).getAccResult1().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getAccResult1());
            }
            if(this.get(i).getAccResult2() == null || this.get(i).getAccResult2().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getAccResult2());
            }
            if(this.get(i).getCaseNo() == null || this.get(i).getCaseNo().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getCaseNo());
            }
            pstmt.setInt(40, this.get(i).getSeqNo());
            // set where condition
            if(this.get(i).getSubRptNo() == null || this.get(i).getSubRptNo().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getSubRptNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getCustomerNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSubReportDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LLSubReport VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSubRptNo() == null || this.get(i).getSubRptNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSubRptNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
            if(this.get(i).getCustomerName() == null || this.get(i).getCustomerName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getCustomerName());
            }
            if(this.get(i).getCustomerType() == null || this.get(i).getCustomerType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getCustomerType());
            }
            if(this.get(i).getAccSubject() == null || this.get(i).getAccSubject().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAccSubject());
            }
            if(this.get(i).getAccidentType() == null || this.get(i).getAccidentType().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getAccidentType());
            }
            if(this.get(i).getAccDate() == null || this.get(i).getAccDate().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getAccDate()));
            }
            if(this.get(i).getAccEndDate() == null || this.get(i).getAccEndDate().equals("null")) {
                pstmt.setDate(8,null);
            } else {
                pstmt.setDate(8, Date.valueOf(this.get(i).getAccEndDate()));
            }
            if(this.get(i).getAccDesc() == null || this.get(i).getAccDesc().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getAccDesc());
            }
            if(this.get(i).getCustSituation() == null || this.get(i).getCustSituation().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getCustSituation());
            }
            if(this.get(i).getAccPlace() == null || this.get(i).getAccPlace().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAccPlace());
            }
            if(this.get(i).getHospitalCode() == null || this.get(i).getHospitalCode().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getHospitalCode());
            }
            if(this.get(i).getHospitalName() == null || this.get(i).getHospitalName().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getHospitalName());
            }
            if(this.get(i).getInHospitalDate() == null || this.get(i).getInHospitalDate().equals("null")) {
                pstmt.setDate(14,null);
            } else {
                pstmt.setDate(14, Date.valueOf(this.get(i).getInHospitalDate()));
            }
            if(this.get(i).getOutHospitalDate() == null || this.get(i).getOutHospitalDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getOutHospitalDate()));
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getRemark());
            }
            if(this.get(i).getSeriousGrade() == null || this.get(i).getSeriousGrade().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getSeriousGrade());
            }
            if(this.get(i).getSurveyFlag() == null || this.get(i).getSurveyFlag().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getSurveyFlag());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getOperator());
            }
            if(this.get(i).getMngCom() == null || this.get(i).getMngCom().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getMngCom());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(21,null);
            } else {
                pstmt.setDate(21, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getModifyTime());
            }
            if(this.get(i).getFirstDiaDate() == null || this.get(i).getFirstDiaDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getFirstDiaDate()));
            }
            if(this.get(i).getHosGrade() == null || this.get(i).getHosGrade().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getHosGrade());
            }
            if(this.get(i).getLocalPlace() == null || this.get(i).getLocalPlace().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getLocalPlace());
            }
            if(this.get(i).getHosTel() == null || this.get(i).getHosTel().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getHosTel());
            }
            if(this.get(i).getDieDate() == null || this.get(i).getDieDate().equals("null")) {
                pstmt.setDate(29,null);
            } else {
                pstmt.setDate(29, Date.valueOf(this.get(i).getDieDate()));
            }
            if(this.get(i).getAccCause() == null || this.get(i).getAccCause().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getAccCause());
            }
            if(this.get(i).getVIPFlag() == null || this.get(i).getVIPFlag().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getVIPFlag());
            }
            if(this.get(i).getAccidentDetail() == null || this.get(i).getAccidentDetail().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getAccidentDetail());
            }
            if(this.get(i).getCureDesc() == null || this.get(i).getCureDesc().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getCureDesc());
            }
            if(this.get(i).getSubmitFlag() == null || this.get(i).getSubmitFlag().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getSubmitFlag());
            }
            if(this.get(i).getCondoleFlag() == null || this.get(i).getCondoleFlag().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getCondoleFlag());
            }
            if(this.get(i).getDieFlag() == null || this.get(i).getDieFlag().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getDieFlag());
            }
            if(this.get(i).getAccResult1() == null || this.get(i).getAccResult1().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getAccResult1());
            }
            if(this.get(i).getAccResult2() == null || this.get(i).getAccResult2().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getAccResult2());
            }
            if(this.get(i).getCaseNo() == null || this.get(i).getCaseNo().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getCaseNo());
            }
            pstmt.setInt(40, this.get(i).getSeqNo());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLSubReportDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
