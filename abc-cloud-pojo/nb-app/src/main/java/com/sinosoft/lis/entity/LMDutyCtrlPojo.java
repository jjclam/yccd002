/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyCtrlPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyCtrlPojo implements Pojo,Serializable {
    // @Field
    /** 责任代码 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String DutyCode; 
    /** 其他编码 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String OtherCode; 
    /** 字段名称 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String FieldName; 
    /** 录入标志 */
    private String InpFlag; 
    /** 控制类型 */
    private String CtrlType; 
    /** 参数再计算方式 */
    private String ParamRecalType; 
    /** 参数再计算处理 */
    private String ParamRecal; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getOtherCode() {
        return OtherCode;
    }
    public void setOtherCode(String aOtherCode) {
        OtherCode = aOtherCode;
    }
    public String getFieldName() {
        return FieldName;
    }
    public void setFieldName(String aFieldName) {
        FieldName = aFieldName;
    }
    public String getInpFlag() {
        return InpFlag;
    }
    public void setInpFlag(String aInpFlag) {
        InpFlag = aInpFlag;
    }
    public String getCtrlType() {
        return CtrlType;
    }
    public void setCtrlType(String aCtrlType) {
        CtrlType = aCtrlType;
    }
    public String getParamRecalType() {
        return ParamRecalType;
    }
    public void setParamRecalType(String aParamRecalType) {
        ParamRecalType = aParamRecalType;
    }
    public String getParamRecal() {
        return ParamRecal;
    }
    public void setParamRecal(String aParamRecal) {
        ParamRecal = aParamRecal;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("DutyCode") ) {
            return 0;
        }
        if( strFieldName.equals("OtherCode") ) {
            return 1;
        }
        if( strFieldName.equals("FieldName") ) {
            return 2;
        }
        if( strFieldName.equals("InpFlag") ) {
            return 3;
        }
        if( strFieldName.equals("CtrlType") ) {
            return 4;
        }
        if( strFieldName.equals("ParamRecalType") ) {
            return 5;
        }
        if( strFieldName.equals("ParamRecal") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "DutyCode";
                break;
            case 1:
                strFieldName = "OtherCode";
                break;
            case 2:
                strFieldName = "FieldName";
                break;
            case 3:
                strFieldName = "InpFlag";
                break;
            case 4:
                strFieldName = "CtrlType";
                break;
            case 5:
                strFieldName = "ParamRecalType";
                break;
            case 6:
                strFieldName = "ParamRecal";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "OTHERCODE":
                return Schema.TYPE_STRING;
            case "FIELDNAME":
                return Schema.TYPE_STRING;
            case "INPFLAG":
                return Schema.TYPE_STRING;
            case "CTRLTYPE":
                return Schema.TYPE_STRING;
            case "PARAMRECALTYPE":
                return Schema.TYPE_STRING;
            case "PARAMRECAL":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("OtherCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherCode));
        }
        if (FCode.equalsIgnoreCase("FieldName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FieldName));
        }
        if (FCode.equalsIgnoreCase("InpFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InpFlag));
        }
        if (FCode.equalsIgnoreCase("CtrlType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CtrlType));
        }
        if (FCode.equalsIgnoreCase("ParamRecalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamRecalType));
        }
        if (FCode.equalsIgnoreCase("ParamRecal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamRecal));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 1:
                strFieldValue = String.valueOf(OtherCode);
                break;
            case 2:
                strFieldValue = String.valueOf(FieldName);
                break;
            case 3:
                strFieldValue = String.valueOf(InpFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(CtrlType);
                break;
            case 5:
                strFieldValue = String.valueOf(ParamRecalType);
                break;
            case 6:
                strFieldValue = String.valueOf(ParamRecal);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("OtherCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherCode = FValue.trim();
            }
            else
                OtherCode = null;
        }
        if (FCode.equalsIgnoreCase("FieldName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FieldName = FValue.trim();
            }
            else
                FieldName = null;
        }
        if (FCode.equalsIgnoreCase("InpFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InpFlag = FValue.trim();
            }
            else
                InpFlag = null;
        }
        if (FCode.equalsIgnoreCase("CtrlType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CtrlType = FValue.trim();
            }
            else
                CtrlType = null;
        }
        if (FCode.equalsIgnoreCase("ParamRecalType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ParamRecalType = FValue.trim();
            }
            else
                ParamRecalType = null;
        }
        if (FCode.equalsIgnoreCase("ParamRecal")) {
            if( FValue != null && !FValue.equals(""))
            {
                ParamRecal = FValue.trim();
            }
            else
                ParamRecal = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyCtrlPojo [" +
            "DutyCode="+DutyCode +
            ", OtherCode="+OtherCode +
            ", FieldName="+FieldName +
            ", InpFlag="+InpFlag +
            ", CtrlType="+CtrlType +
            ", ParamRecalType="+ParamRecalType +
            ", ParamRecal="+ParamRecal +"]";
    }
}
