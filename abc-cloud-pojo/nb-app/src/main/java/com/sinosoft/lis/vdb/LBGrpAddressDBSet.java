/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LBGrpAddressSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LBGrpAddressDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBGrpAddressDBSet extends LBGrpAddressSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LBGrpAddressDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LBGrpAddress");
        mflag = true;
    }

    public LBGrpAddressDBSet() {
        db = new DBOper( "LBGrpAddress" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpAddressDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LBGrpAddress WHERE  1=1  AND CustomerNo = ? AND AddressNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCustomerNo());
            }
            if(this.get(i).getAddressNo() == null || this.get(i).getAddressNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getAddressNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpAddressDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LBGrpAddress SET  CustomerNo = ? , AddressNo = ? , GrpAddress = ? , GrpZipCode = ? , LinkMan1 = ? , Department1 = ? , HeadShip1 = ? , Phone1 = ? , E_Mail1 = ? , Fax1 = ? , LinkMan2 = ? , Department2 = ? , HeadShip2 = ? , Phone2 = ? , E_Mail2 = ? , Fax2 = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , MobilePhone1 = ? , MobilePhone2 = ? , IDType = ? , IDNo = ? , IDNoValidate = ? , IDType2 = ? , IDNo2 = ? , IDNoValidate2 = ? , Province = ? , City = ? , County = ? , TinNo = ? , TinFlag = ? , USATinFlag = ? , USATinName = ? , USATinAddress = ? , USATinNo = ? WHERE  1=1  AND CustomerNo = ? AND AddressNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCustomerNo());
            }
            if(this.get(i).getAddressNo() == null || this.get(i).getAddressNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getAddressNo());
            }
            if(this.get(i).getGrpAddress() == null || this.get(i).getGrpAddress().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGrpAddress());
            }
            if(this.get(i).getGrpZipCode() == null || this.get(i).getGrpZipCode().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getGrpZipCode());
            }
            if(this.get(i).getLinkMan1() == null || this.get(i).getLinkMan1().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getLinkMan1());
            }
            if(this.get(i).getDepartment1() == null || this.get(i).getDepartment1().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getDepartment1());
            }
            if(this.get(i).getHeadShip1() == null || this.get(i).getHeadShip1().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getHeadShip1());
            }
            if(this.get(i).getPhone1() == null || this.get(i).getPhone1().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getPhone1());
            }
            if(this.get(i).getE_Mail1() == null || this.get(i).getE_Mail1().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getE_Mail1());
            }
            if(this.get(i).getFax1() == null || this.get(i).getFax1().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getFax1());
            }
            if(this.get(i).getLinkMan2() == null || this.get(i).getLinkMan2().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getLinkMan2());
            }
            if(this.get(i).getDepartment2() == null || this.get(i).getDepartment2().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getDepartment2());
            }
            if(this.get(i).getHeadShip2() == null || this.get(i).getHeadShip2().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getHeadShip2());
            }
            if(this.get(i).getPhone2() == null || this.get(i).getPhone2().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getPhone2());
            }
            if(this.get(i).getE_Mail2() == null || this.get(i).getE_Mail2().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getE_Mail2());
            }
            if(this.get(i).getFax2() == null || this.get(i).getFax2().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getFax2());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(18,null);
            } else {
                pstmt.setDate(18, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(20,null);
            } else {
                pstmt.setDate(20, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getModifyTime());
            }
            if(this.get(i).getMobilePhone1() == null || this.get(i).getMobilePhone1().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMobilePhone1());
            }
            if(this.get(i).getMobilePhone2() == null || this.get(i).getMobilePhone2().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getMobilePhone2());
            }
            if(this.get(i).getIDType() == null || this.get(i).getIDType().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getIDType());
            }
            if(this.get(i).getIDNo() == null || this.get(i).getIDNo().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getIDNo());
            }
            if(this.get(i).getIDNoValidate() == null || this.get(i).getIDNoValidate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getIDNoValidate()));
            }
            if(this.get(i).getIDType2() == null || this.get(i).getIDType2().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getIDType2());
            }
            if(this.get(i).getIDNo2() == null || this.get(i).getIDNo2().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getIDNo2());
            }
            if(this.get(i).getIDNoValidate2() == null || this.get(i).getIDNoValidate2().equals("null")) {
                pstmt.setDate(29,null);
            } else {
                pstmt.setDate(29, Date.valueOf(this.get(i).getIDNoValidate2()));
            }
            if(this.get(i).getProvince() == null || this.get(i).getProvince().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getProvince());
            }
            if(this.get(i).getCity() == null || this.get(i).getCity().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getCity());
            }
            if(this.get(i).getCounty() == null || this.get(i).getCounty().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getCounty());
            }
            if(this.get(i).getTinNo() == null || this.get(i).getTinNo().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getTinNo());
            }
            if(this.get(i).getTinFlag() == null || this.get(i).getTinFlag().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getTinFlag());
            }
            if(this.get(i).getUSATinFlag() == null || this.get(i).getUSATinFlag().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getUSATinFlag());
            }
            if(this.get(i).getUSATinName() == null || this.get(i).getUSATinName().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getUSATinName());
            }
            if(this.get(i).getUSATinAddress() == null || this.get(i).getUSATinAddress().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getUSATinAddress());
            }
            if(this.get(i).getUSATinNo() == null || this.get(i).getUSATinNo().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getUSATinNo());
            }
            // set where condition
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getCustomerNo());
            }
            if(this.get(i).getAddressNo() == null || this.get(i).getAddressNo().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getAddressNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpAddressDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LBGrpAddress VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCustomerNo());
            }
            if(this.get(i).getAddressNo() == null || this.get(i).getAddressNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getAddressNo());
            }
            if(this.get(i).getGrpAddress() == null || this.get(i).getGrpAddress().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGrpAddress());
            }
            if(this.get(i).getGrpZipCode() == null || this.get(i).getGrpZipCode().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getGrpZipCode());
            }
            if(this.get(i).getLinkMan1() == null || this.get(i).getLinkMan1().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getLinkMan1());
            }
            if(this.get(i).getDepartment1() == null || this.get(i).getDepartment1().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getDepartment1());
            }
            if(this.get(i).getHeadShip1() == null || this.get(i).getHeadShip1().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getHeadShip1());
            }
            if(this.get(i).getPhone1() == null || this.get(i).getPhone1().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getPhone1());
            }
            if(this.get(i).getE_Mail1() == null || this.get(i).getE_Mail1().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getE_Mail1());
            }
            if(this.get(i).getFax1() == null || this.get(i).getFax1().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getFax1());
            }
            if(this.get(i).getLinkMan2() == null || this.get(i).getLinkMan2().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getLinkMan2());
            }
            if(this.get(i).getDepartment2() == null || this.get(i).getDepartment2().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getDepartment2());
            }
            if(this.get(i).getHeadShip2() == null || this.get(i).getHeadShip2().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getHeadShip2());
            }
            if(this.get(i).getPhone2() == null || this.get(i).getPhone2().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getPhone2());
            }
            if(this.get(i).getE_Mail2() == null || this.get(i).getE_Mail2().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getE_Mail2());
            }
            if(this.get(i).getFax2() == null || this.get(i).getFax2().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getFax2());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(18,null);
            } else {
                pstmt.setDate(18, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(20,null);
            } else {
                pstmt.setDate(20, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getModifyTime());
            }
            if(this.get(i).getMobilePhone1() == null || this.get(i).getMobilePhone1().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMobilePhone1());
            }
            if(this.get(i).getMobilePhone2() == null || this.get(i).getMobilePhone2().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getMobilePhone2());
            }
            if(this.get(i).getIDType() == null || this.get(i).getIDType().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getIDType());
            }
            if(this.get(i).getIDNo() == null || this.get(i).getIDNo().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getIDNo());
            }
            if(this.get(i).getIDNoValidate() == null || this.get(i).getIDNoValidate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getIDNoValidate()));
            }
            if(this.get(i).getIDType2() == null || this.get(i).getIDType2().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getIDType2());
            }
            if(this.get(i).getIDNo2() == null || this.get(i).getIDNo2().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getIDNo2());
            }
            if(this.get(i).getIDNoValidate2() == null || this.get(i).getIDNoValidate2().equals("null")) {
                pstmt.setDate(29,null);
            } else {
                pstmt.setDate(29, Date.valueOf(this.get(i).getIDNoValidate2()));
            }
            if(this.get(i).getProvince() == null || this.get(i).getProvince().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getProvince());
            }
            if(this.get(i).getCity() == null || this.get(i).getCity().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getCity());
            }
            if(this.get(i).getCounty() == null || this.get(i).getCounty().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getCounty());
            }
            if(this.get(i).getTinNo() == null || this.get(i).getTinNo().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getTinNo());
            }
            if(this.get(i).getTinFlag() == null || this.get(i).getTinFlag().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getTinFlag());
            }
            if(this.get(i).getUSATinFlag() == null || this.get(i).getUSATinFlag().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getUSATinFlag());
            }
            if(this.get(i).getUSATinName() == null || this.get(i).getUSATinName().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getUSATinName());
            }
            if(this.get(i).getUSATinAddress() == null || this.get(i).getUSATinAddress().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getUSATinAddress());
            }
            if(this.get(i).getUSATinNo() == null || this.get(i).getUSATinNo().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getUSATinNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGrpAddressDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
