/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.RiskAmnt_DeathDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: RiskAmnt_DeathSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class RiskAmnt_DeathSchema implements Schema, Cloneable {
    // @Field
    /** Riskcode */
    private String Riskcode;
    /** Adultflag */
    private String AdultFlag;
    /** Calcode */
    private String CalCode;
    /** Remark */
    private String Remark;

    public static final int FIELDNUM = 4;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public RiskAmnt_DeathSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "Riskcode";
        pk[1] = "AdultFlag";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        RiskAmnt_DeathSchema cloned = (RiskAmnt_DeathSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskcode() {
        return Riskcode;
    }
    public void setRiskcode(String aRiskcode) {
        Riskcode = aRiskcode;
    }
    public String getAdultFlag() {
        return AdultFlag;
    }
    public void setAdultFlag(String aAdultFlag) {
        AdultFlag = aAdultFlag;
    }
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    /**
    * 使用另外一个 RiskAmnt_DeathSchema 对象给 Schema 赋值
    * @param: aRiskAmnt_DeathSchema RiskAmnt_DeathSchema
    **/
    public void setSchema(RiskAmnt_DeathSchema aRiskAmnt_DeathSchema) {
        this.Riskcode = aRiskAmnt_DeathSchema.getRiskcode();
        this.AdultFlag = aRiskAmnt_DeathSchema.getAdultFlag();
        this.CalCode = aRiskAmnt_DeathSchema.getCalCode();
        this.Remark = aRiskAmnt_DeathSchema.getRemark();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("Riskcode") == null )
                this.Riskcode = null;
            else
                this.Riskcode = rs.getString("Riskcode").trim();

            if( rs.getString("AdultFlag") == null )
                this.AdultFlag = null;
            else
                this.AdultFlag = rs.getString("AdultFlag").trim();

            if( rs.getString("CalCode") == null )
                this.CalCode = null;
            else
                this.CalCode = rs.getString("CalCode").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "RiskAmnt_DeathSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public RiskAmnt_DeathSchema getSchema() {
        RiskAmnt_DeathSchema aRiskAmnt_DeathSchema = new RiskAmnt_DeathSchema();
        aRiskAmnt_DeathSchema.setSchema(this);
        return aRiskAmnt_DeathSchema;
    }

    public RiskAmnt_DeathDB getDB() {
        RiskAmnt_DeathDB aDBOper = new RiskAmnt_DeathDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpRiskAmnt_Death描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(Riskcode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AdultFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpRiskAmnt_Death>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            Riskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            AdultFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "RiskAmnt_DeathSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("Riskcode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Riskcode));
        }
        if (FCode.equalsIgnoreCase("AdultFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AdultFlag));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(Riskcode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AdultFlag);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("Riskcode")) {
            if( FValue != null && !FValue.equals(""))
            {
                Riskcode = FValue.trim();
            }
            else
                Riskcode = null;
        }
        if (FCode.equalsIgnoreCase("AdultFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AdultFlag = FValue.trim();
            }
            else
                AdultFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        RiskAmnt_DeathSchema other = (RiskAmnt_DeathSchema)otherObject;
        return
            Riskcode.equals(other.getRiskcode())
            && AdultFlag.equals(other.getAdultFlag())
            && CalCode.equals(other.getCalCode())
            && Remark.equals(other.getRemark());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("Riskcode") ) {
            return 0;
        }
        if( strFieldName.equals("AdultFlag") ) {
            return 1;
        }
        if( strFieldName.equals("CalCode") ) {
            return 2;
        }
        if( strFieldName.equals("Remark") ) {
            return 3;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "Riskcode";
                break;
            case 1:
                strFieldName = "AdultFlag";
                break;
            case 2:
                strFieldName = "CalCode";
                break;
            case 3:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "ADULTFLAG":
                return Schema.TYPE_STRING;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
