/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCPolicyInfoPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-07-09
 */
public class LCPolicyInfoPojo implements Pojo,Serializable {
    // @Field
    /** 个人保单号 */
    private String ContNo;
    /** 个人客户号 */
    private String InsuredNo;
    /** 保险计划编码 */
    private String ContplanCode;
    /** 套餐名称 */
    private String ContplanName;
    /** 基本保费 */
    private double BasePrem;
    /** 加费 */
    private double AddPrem;
    /** 激活日期 */
    private String  ActiveDate;
    /** 保险期间 */
    private int InsuYear;
    /** 保险期间单位 */
    private String InsuYearFlag;
    /** 生效间隔 */
    private int Cvaliintv;
    /** 生效间隔单位 */
    private String CvaliintvFlag;
    /** 被保人中介平台客户号 */
    private String SisinSuredNo;
    /** 出访国家 */
    private String TraveLcountry;
    /** 接口网点ip地址 */
    private String AgentIp;
    /** 航班号 */
    private String FlightNo;
    /** 境外救援卡号 */
    private String RescuecardNo;
    /** 经办人 */
    private String HandlerName;
    /** 经办人电话 */
    private String HandlerPhone;
    /** 签单日期 */
    private String  SignDate;
    /** 签单时间 */
    private String SignTime;
    /** 网点地址编码 */
    private String agentcom;
    /** 险类 */
    private String RiskType;
    /** 出单方式 */
    private String OrderType;
    /** 借款合同编号 */
    private String JYContNo;
    /** 借款凭证编号 */
    private String JYCertNo;
    /** 借款（起始）日期 */
    private String JYStartDate;
    /** 借款（终止）到期 */
    private String JYEndDate;
    /** 借意险贷款金额 */
    private String LoanAmount;
    /** 借意险贷款发放机构 */
    private String LendCom;
    /** 借款人性质 */
    private String LoanerNature;
    /** 借款人性质名称 */
    private String LoanerNatureName;
    /** 借意险借款期限（yd套餐） */
    private String LendTerm;
    /** 境外救援出访国家代码 */
    private String CountryCode;
    /** 燃气公司 */
    private String GasCompany;
    /** 燃气用户地址 */
    private String GasUserAddress;
    /** 学平险约定赔付比例 */
    private String PayoutPro;
    /** 学平险学校名称 */
    private String School;
    /** 学平险保费收费确认时间 */
    private String ChargeDate;
    /** 溜冰标记为1 */
    private String Mark;


    public static final int FIELDNUM = 39;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getContplanCode() {
        return ContplanCode;
    }
    public void setContplanCode(String aContplanCode) {
        ContplanCode = aContplanCode;
    }
    public String getContplanName() {
        return ContplanName;
    }
    public void setContplanName(String aContplanName) {
        ContplanName = aContplanName;
    }
    public double getBasePrem() {
        return BasePrem;
    }
    public void setBasePrem(double aBasePrem) {
        BasePrem = aBasePrem;
    }
    public void setBasePrem(String aBasePrem) {
        if (aBasePrem != null && !aBasePrem.equals("")) {
            Double tDouble = new Double(aBasePrem);
            double d = tDouble.doubleValue();
            BasePrem = d;
        }
    }

    public double getAddPrem() {
        return AddPrem;
    }
    public void setAddPrem(double aAddPrem) {
        AddPrem = aAddPrem;
    }
    public void setAddPrem(String aAddPrem) {
        if (aAddPrem != null && !aAddPrem.equals("")) {
            Double tDouble = new Double(aAddPrem);
            double d = tDouble.doubleValue();
            AddPrem = d;
        }
    }

    public String getActiveDate() {
        return ActiveDate;
    }
    public void setActiveDate(String aActiveDate) {
        ActiveDate = aActiveDate;
    }
    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public int getCvaliintv() {
        return Cvaliintv;
    }
    public void setCvaliintv(int aCvaliintv) {
        Cvaliintv = aCvaliintv;
    }
    public void setCvaliintv(String aCvaliintv) {
        if (aCvaliintv != null && !aCvaliintv.equals("")) {
            Integer tInteger = new Integer(aCvaliintv);
            int i = tInteger.intValue();
            Cvaliintv = i;
        }
    }

    public String getCvaliintvFlag() {
        return CvaliintvFlag;
    }
    public void setCvaliintvFlag(String aCvaliintvFlag) {
        CvaliintvFlag = aCvaliintvFlag;
    }
    public String getSisinSuredNo() {
        return SisinSuredNo;
    }
    public void setSisinSuredNo(String aSisinSuredNo) {
        SisinSuredNo = aSisinSuredNo;
    }
    public String getTraveLcountry() {
        return TraveLcountry;
    }
    public void setTraveLcountry(String aTraveLcountry) {
        TraveLcountry = aTraveLcountry;
    }
    public String getAgentIp() {
        return AgentIp;
    }
    public void setAgentIp(String aAgentIp) {
        AgentIp = aAgentIp;
    }
    public String getFlightNo() {
        return FlightNo;
    }
    public void setFlightNo(String aFlightNo) {
        FlightNo = aFlightNo;
    }
    public String getRescuecardNo() {
        return RescuecardNo;
    }
    public void setRescuecardNo(String aRescuecardNo) {
        RescuecardNo = aRescuecardNo;
    }
    public String getHandlerName() {
        return HandlerName;
    }
    public void setHandlerName(String aHandlerName) {
        HandlerName = aHandlerName;
    }
    public String getHandlerPhone() {
        return HandlerPhone;
    }
    public void setHandlerPhone(String aHandlerPhone) {
        HandlerPhone = aHandlerPhone;
    }
    public String getSignDate() {
        return SignDate;
    }
    public void setSignDate(String aSignDate) {
        SignDate = aSignDate;
    }
    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getAgentcom() {
        return agentcom;
    }
    public void setAgentcom(String aagentcom) {
        agentcom = aagentcom;
    }
    public String getRiskType() {
        return RiskType;
    }
    public void setRiskType(String aRiskType) {
        RiskType = aRiskType;
    }
    public String getOrderType() {
        return OrderType;
    }
    public void setOrderType(String aOrderType) {
        OrderType = aOrderType;
    }
    public String getJYContNo() {
        return JYContNo;
    }
    public void setJYContNo(String aJYContNo) {
        JYContNo = aJYContNo;
    }
    public String getJYCertNo() {
        return JYCertNo;
    }
    public void setJYCertNo(String aJYCertNo) {
        JYCertNo = aJYCertNo;
    }
    public String getJYStartDate() {
        return JYStartDate;
    }
    public void setJYStartDate(String aJYStartDate) {
        JYStartDate = aJYStartDate;
    }
    public String getJYEndDate() {
        return JYEndDate;
    }
    public void setJYEndDate(String aJYEndDate) {
        JYEndDate = aJYEndDate;
    }
    public String getLoanAmount() {
        return LoanAmount;
    }
    public void setLoanAmount(String aLoanAmount) {
        LoanAmount = aLoanAmount;
    }
    public String getLendCom() {
        return LendCom;
    }
    public void setLendCom(String aLendCom) {
        LendCom = aLendCom;
    }
    public String getLoanerNature() {
        return LoanerNature;
    }
    public void setLoanerNature(String aLoanerNature) {
        LoanerNature = aLoanerNature;
    }
    public String getLoanerNatureName() {
        return LoanerNatureName;
    }
    public void setLoanerNatureName(String aLoanerNatureName) {
        LoanerNatureName = aLoanerNatureName;
    }
    public String getLendTerm() {
        return LendTerm;
    }
    public void setLendTerm(String aLendTerm) {
        LendTerm = aLendTerm;
    }
    public String getCountryCode() {
        return CountryCode;
    }
    public void setCountryCode(String aCountryCode) {
        CountryCode = aCountryCode;
    }
    public String getGasCompany() {
        return GasCompany;
    }
    public void setGasCompany(String aGasCompany) {
        GasCompany = aGasCompany;
    }
    public String getGasUserAddress() {
        return GasUserAddress;
    }
    public void setGasUserAddress(String aGasUserAddress) {
        GasUserAddress = aGasUserAddress;
    }
    public String getPayoutPro() {
        return PayoutPro;
    }
    public void setPayoutPro(String aPayoutPro) {
        PayoutPro = aPayoutPro;
    }
    public String getSchool() {
        return School;
    }
    public void setSchool(String aSchool) {
        School = aSchool;
    }
    public String getChargeDate() {
        return ChargeDate;
    }
    public void setChargeDate(String aChargeDate) {
        ChargeDate = aChargeDate;
    }
    public String getMark() {
        return Mark;
    }
    public void setMark(String aMark) {
        Mark = aMark;
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContNo") ) {
            return 0;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 1;
        }
        if( strFieldName.equals("ContplanCode") ) {
            return 2;
        }
        if( strFieldName.equals("ContplanName") ) {
            return 3;
        }
        if( strFieldName.equals("BasePrem") ) {
            return 4;
        }
        if( strFieldName.equals("AddPrem") ) {
            return 5;
        }
        if( strFieldName.equals("ActiveDate") ) {
            return 6;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 7;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 8;
        }
        if( strFieldName.equals("Cvaliintv") ) {
            return 9;
        }
        if( strFieldName.equals("CvaliintvFlag") ) {
            return 10;
        }
        if( strFieldName.equals("SisinSuredNo") ) {
            return 11;
        }
        if( strFieldName.equals("TraveLcountry") ) {
            return 12;
        }
        if( strFieldName.equals("AgentIp") ) {
            return 13;
        }
        if( strFieldName.equals("FlightNo") ) {
            return 14;
        }
        if( strFieldName.equals("RescuecardNo") ) {
            return 15;
        }
        if( strFieldName.equals("HandlerName") ) {
            return 16;
        }
        if( strFieldName.equals("HandlerPhone") ) {
            return 17;
        }
        if( strFieldName.equals("SignDate") ) {
            return 18;
        }
        if( strFieldName.equals("SignTime") ) {
            return 19;
        }
        if( strFieldName.equals("agentcom") ) {
            return 20;
        }
        if( strFieldName.equals("RiskType") ) {
            return 21;
        }
        if( strFieldName.equals("OrderType") ) {
            return 22;
        }
        if( strFieldName.equals("JYContNo") ) {
            return 23;
        }
        if( strFieldName.equals("JYCertNo") ) {
            return 24;
        }
        if( strFieldName.equals("JYStartDate") ) {
            return 25;
        }
        if( strFieldName.equals("JYEndDate") ) {
            return 26;
        }
        if( strFieldName.equals("LoanAmount") ) {
            return 27;
        }
        if( strFieldName.equals("LendCom") ) {
            return 28;
        }
        if( strFieldName.equals("LoanerNature") ) {
            return 29;
        }
        if( strFieldName.equals("LoanerNatureName") ) {
            return 30;
        }
        if( strFieldName.equals("LendTerm") ) {
            return 31;
        }
        if( strFieldName.equals("CountryCode") ) {
            return 32;
        }
        if( strFieldName.equals("GasCompany") ) {
            return 33;
        }
        if( strFieldName.equals("GasUserAddress") ) {
            return 34;
        }
        if( strFieldName.equals("PayoutPro") ) {
            return 35;
        }
        if( strFieldName.equals("School") ) {
            return 36;
        }
        if( strFieldName.equals("ChargeDate") ) {
            return 37;
        }
        if( strFieldName.equals("Mark") ) {
            return 38;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "InsuredNo";
                break;
            case 2:
                strFieldName = "ContplanCode";
                break;
            case 3:
                strFieldName = "ContplanName";
                break;
            case 4:
                strFieldName = "BasePrem";
                break;
            case 5:
                strFieldName = "AddPrem";
                break;
            case 6:
                strFieldName = "ActiveDate";
                break;
            case 7:
                strFieldName = "InsuYear";
                break;
            case 8:
                strFieldName = "InsuYearFlag";
                break;
            case 9:
                strFieldName = "Cvaliintv";
                break;
            case 10:
                strFieldName = "CvaliintvFlag";
                break;
            case 11:
                strFieldName = "SisinSuredNo";
                break;
            case 12:
                strFieldName = "TraveLcountry";
                break;
            case 13:
                strFieldName = "AgentIp";
                break;
            case 14:
                strFieldName = "FlightNo";
                break;
            case 15:
                strFieldName = "RescuecardNo";
                break;
            case 16:
                strFieldName = "HandlerName";
                break;
            case 17:
                strFieldName = "HandlerPhone";
                break;
            case 18:
                strFieldName = "SignDate";
                break;
            case 19:
                strFieldName = "SignTime";
                break;
            case 20:
                strFieldName = "agentcom";
                break;
            case 21:
                strFieldName = "RiskType";
                break;
            case 22:
                strFieldName = "OrderType";
                break;
            case 23:
                strFieldName = "JYContNo";
                break;
            case 24:
                strFieldName = "JYCertNo";
                break;
            case 25:
                strFieldName = "JYStartDate";
                break;
            case 26:
                strFieldName = "JYEndDate";
                break;
            case 27:
                strFieldName = "LoanAmount";
                break;
            case 28:
                strFieldName = "LendCom";
                break;
            case 29:
                strFieldName = "LoanerNature";
                break;
            case 30:
                strFieldName = "LoanerNatureName";
                break;
            case 31:
                strFieldName = "LendTerm";
                break;
            case 32:
                strFieldName = "CountryCode";
                break;
            case 33:
                strFieldName = "GasCompany";
                break;
            case 34:
                strFieldName = "GasUserAddress";
                break;
            case 35:
                strFieldName = "PayoutPro";
                break;
            case 36:
                strFieldName = "School";
                break;
            case 37:
                strFieldName = "ChargeDate";
                break;
            case 38:
                strFieldName = "Mark";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "CONTPLANCODE":
                return Schema.TYPE_STRING;
            case "CONTPLANNAME":
                return Schema.TYPE_STRING;
            case "BASEPREM":
                return Schema.TYPE_DOUBLE;
            case "ADDPREM":
                return Schema.TYPE_DOUBLE;
            case "ACTIVEDATE":
                return Schema.TYPE_STRING;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "CVALIINTV":
                return Schema.TYPE_INT;
            case "CVALIINTVFLAG":
                return Schema.TYPE_STRING;
            case "SISINSUREDNO":
                return Schema.TYPE_STRING;
            case "TRAVELCOUNTRY":
                return Schema.TYPE_STRING;
            case "AGENTIP":
                return Schema.TYPE_STRING;
            case "FLIGHTNO":
                return Schema.TYPE_STRING;
            case "RESCUECARDNO":
                return Schema.TYPE_STRING;
            case "HANDLERNAME":
                return Schema.TYPE_STRING;
            case "HANDLERPHONE":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_STRING;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "RISKTYPE":
                return Schema.TYPE_STRING;
            case "ORDERTYPE":
                return Schema.TYPE_STRING;
            case "JYCONTNO":
                return Schema.TYPE_STRING;
            case "JYCERTNO":
                return Schema.TYPE_STRING;
            case "JYSTARTDATE":
                return Schema.TYPE_STRING;
            case "JYENDDATE":
                return Schema.TYPE_STRING;
            case "LOANAMOUNT":
                return Schema.TYPE_STRING;
            case "LENDCOM":
                return Schema.TYPE_STRING;
            case "LOANERNATURE":
                return Schema.TYPE_STRING;
            case "LOANERNATURENAME":
                return Schema.TYPE_STRING;
            case "LENDTERM":
                return Schema.TYPE_STRING;
            case "COUNTRYCODE":
                return Schema.TYPE_STRING;
            case "GASCOMPANY":
                return Schema.TYPE_STRING;
            case "GASUSERADDRESS":
                return Schema.TYPE_STRING;
            case "PAYOUTPRO":
                return Schema.TYPE_STRING;
            case "SCHOOL":
                return Schema.TYPE_STRING;
            case "CHARGEDATE":
                return Schema.TYPE_STRING;
            case "MARK":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_INT;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_INT;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("ContplanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContplanCode));
        }
        if (FCode.equalsIgnoreCase("ContplanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContplanName));
        }
        if (FCode.equalsIgnoreCase("BasePrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BasePrem));
        }
        if (FCode.equalsIgnoreCase("AddPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddPrem));
        }
        if (FCode.equalsIgnoreCase("ActiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActiveDate));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("Cvaliintv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Cvaliintv));
        }
        if (FCode.equalsIgnoreCase("CvaliintvFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CvaliintvFlag));
        }
        if (FCode.equalsIgnoreCase("SisinSuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SisinSuredNo));
        }
        if (FCode.equalsIgnoreCase("TraveLcountry")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TraveLcountry));
        }
        if (FCode.equalsIgnoreCase("AgentIp")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentIp));
        }
        if (FCode.equalsIgnoreCase("FlightNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FlightNo));
        }
        if (FCode.equalsIgnoreCase("RescuecardNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RescuecardNo));
        }
        if (FCode.equalsIgnoreCase("HandlerName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerName));
        }
        if (FCode.equalsIgnoreCase("HandlerPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerPhone));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("agentcom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(agentcom));
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
        }
        if (FCode.equalsIgnoreCase("OrderType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderType));
        }
        if (FCode.equalsIgnoreCase("JYContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JYContNo));
        }
        if (FCode.equalsIgnoreCase("JYCertNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JYCertNo));
        }
        if (FCode.equalsIgnoreCase("JYStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JYStartDate));
        }
        if (FCode.equalsIgnoreCase("JYEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JYEndDate));
        }
        if (FCode.equalsIgnoreCase("LoanAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LoanAmount));
        }
        if (FCode.equalsIgnoreCase("LendCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LendCom));
        }
        if (FCode.equalsIgnoreCase("LoanerNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LoanerNature));
        }
        if (FCode.equalsIgnoreCase("LoanerNatureName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LoanerNatureName));
        }
        if (FCode.equalsIgnoreCase("LendTerm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LendTerm));
        }
        if (FCode.equalsIgnoreCase("CountryCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CountryCode));
        }
        if (FCode.equalsIgnoreCase("GasCompany")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GasCompany));
        }
        if (FCode.equalsIgnoreCase("GasUserAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GasUserAddress));
        }
        if (FCode.equalsIgnoreCase("PayoutPro")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayoutPro));
        }
        if (FCode.equalsIgnoreCase("School")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(School));
        }
        if (FCode.equalsIgnoreCase("ChargeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeDate));
        }
        if (FCode.equalsIgnoreCase("Mark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mark));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ContplanCode);
                break;
            case 3:
                strFieldValue = String.valueOf(ContplanName);
                break;
            case 4:
                strFieldValue = String.valueOf(BasePrem);
                break;
            case 5:
                strFieldValue = String.valueOf(AddPrem);
                break;
            case 6:
                strFieldValue = String.valueOf(ActiveDate);
                break;
            case 7:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 8:
                strFieldValue = String.valueOf(InsuYearFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(Cvaliintv);
                break;
            case 10:
                strFieldValue = String.valueOf(CvaliintvFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(SisinSuredNo);
                break;
            case 12:
                strFieldValue = String.valueOf(TraveLcountry);
                break;
            case 13:
                strFieldValue = String.valueOf(AgentIp);
                break;
            case 14:
                strFieldValue = String.valueOf(FlightNo);
                break;
            case 15:
                strFieldValue = String.valueOf(RescuecardNo);
                break;
            case 16:
                strFieldValue = String.valueOf(HandlerName);
                break;
            case 17:
                strFieldValue = String.valueOf(HandlerPhone);
                break;
            case 18:
                strFieldValue = String.valueOf(SignDate);
                break;
            case 19:
                strFieldValue = String.valueOf(SignTime);
                break;
            case 20:
                strFieldValue = String.valueOf(agentcom);
                break;
            case 21:
                strFieldValue = String.valueOf(RiskType);
                break;
            case 22:
                strFieldValue = String.valueOf(OrderType);
                break;
            case 23:
                strFieldValue = String.valueOf(JYContNo);
                break;
            case 24:
                strFieldValue = String.valueOf(JYCertNo);
                break;
            case 25:
                strFieldValue = String.valueOf(JYStartDate);
                break;
            case 26:
                strFieldValue = String.valueOf(JYEndDate);
                break;
            case 27:
                strFieldValue = String.valueOf(LoanAmount);
                break;
            case 28:
                strFieldValue = String.valueOf(LendCom);
                break;
            case 29:
                strFieldValue = String.valueOf(LoanerNature);
                break;
            case 30:
                strFieldValue = String.valueOf(LoanerNatureName);
                break;
            case 31:
                strFieldValue = String.valueOf(LendTerm);
                break;
            case 32:
                strFieldValue = String.valueOf(CountryCode);
                break;
            case 33:
                strFieldValue = String.valueOf(GasCompany);
                break;
            case 34:
                strFieldValue = String.valueOf(GasUserAddress);
                break;
            case 35:
                strFieldValue = String.valueOf(PayoutPro);
                break;
            case 36:
                strFieldValue = String.valueOf(School);
                break;
            case 37:
                strFieldValue = String.valueOf(ChargeDate);
                break;
            case 38:
                strFieldValue = String.valueOf(Mark);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("ContplanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContplanCode = FValue.trim();
            }
            else
                ContplanCode = null;
        }
        if (FCode.equalsIgnoreCase("ContplanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContplanName = FValue.trim();
            }
            else
                ContplanName = null;
        }
        if (FCode.equalsIgnoreCase("BasePrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BasePrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("AddPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AddPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("ActiveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActiveDate = FValue.trim();
            }
            else
                ActiveDate = null;
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("Cvaliintv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Cvaliintv = i;
            }
        }
        if (FCode.equalsIgnoreCase("CvaliintvFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CvaliintvFlag = FValue.trim();
            }
            else
                CvaliintvFlag = null;
        }
        if (FCode.equalsIgnoreCase("SisinSuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SisinSuredNo = FValue.trim();
            }
            else
                SisinSuredNo = null;
        }
        if (FCode.equalsIgnoreCase("TraveLcountry")) {
            if( FValue != null && !FValue.equals(""))
            {
                TraveLcountry = FValue.trim();
            }
            else
                TraveLcountry = null;
        }
        if (FCode.equalsIgnoreCase("AgentIp")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentIp = FValue.trim();
            }
            else
                AgentIp = null;
        }
        if (FCode.equalsIgnoreCase("FlightNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                FlightNo = FValue.trim();
            }
            else
                FlightNo = null;
        }
        if (FCode.equalsIgnoreCase("RescuecardNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RescuecardNo = FValue.trim();
            }
            else
                RescuecardNo = null;
        }
        if (FCode.equalsIgnoreCase("HandlerName")) {
            if( FValue != null && !FValue.equals(""))
            {
                HandlerName = FValue.trim();
            }
            else
                HandlerName = null;
        }
        if (FCode.equalsIgnoreCase("HandlerPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                HandlerPhone = FValue.trim();
            }
            else
                HandlerPhone = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignDate = FValue.trim();
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("agentcom")) {
            if( FValue != null && !FValue.equals(""))
            {
                agentcom = FValue.trim();
            }
            else
                agentcom = null;
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType = FValue.trim();
            }
            else
                RiskType = null;
        }
        if (FCode.equalsIgnoreCase("OrderType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrderType = FValue.trim();
            }
            else
                OrderType = null;
        }
        if (FCode.equalsIgnoreCase("JYContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                JYContNo = FValue.trim();
            }
            else
                JYContNo = null;
        }
        if (FCode.equalsIgnoreCase("JYCertNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                JYCertNo = FValue.trim();
            }
            else
                JYCertNo = null;
        }
        if (FCode.equalsIgnoreCase("JYStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JYStartDate = FValue.trim();
            }
            else
                JYStartDate = null;
        }
        if (FCode.equalsIgnoreCase("JYEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JYEndDate = FValue.trim();
            }
            else
                JYEndDate = null;
        }
        if (FCode.equalsIgnoreCase("LoanAmount")) {
            if( FValue != null && !FValue.equals(""))
            {
                LoanAmount = FValue.trim();
            }
            else
                LoanAmount = null;
        }
        if (FCode.equalsIgnoreCase("LendCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                LendCom = FValue.trim();
            }
            else
                LendCom = null;
        }
        if (FCode.equalsIgnoreCase("LoanerNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                LoanerNature = FValue.trim();
            }
            else
                LoanerNature = null;
        }
        if (FCode.equalsIgnoreCase("LoanerNatureName")) {
            if( FValue != null && !FValue.equals(""))
            {
                LoanerNatureName = FValue.trim();
            }
            else
                LoanerNatureName = null;
        }
        if (FCode.equalsIgnoreCase("LendTerm")) {
            if( FValue != null && !FValue.equals(""))
            {
                LendTerm = FValue.trim();
            }
            else
                LendTerm = null;
        }
        if (FCode.equalsIgnoreCase("CountryCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CountryCode = FValue.trim();
            }
            else
                CountryCode = null;
        }
        if (FCode.equalsIgnoreCase("GasCompany")) {
            if( FValue != null && !FValue.equals(""))
            {
                GasCompany = FValue.trim();
            }
            else
                GasCompany = null;
        }
        if (FCode.equalsIgnoreCase("GasUserAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                GasUserAddress = FValue.trim();
            }
            else
                GasUserAddress = null;
        }
        if (FCode.equalsIgnoreCase("PayoutPro")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayoutPro = FValue.trim();
            }
            else
                PayoutPro = null;
        }
        if (FCode.equalsIgnoreCase("School")) {
            if( FValue != null && !FValue.equals(""))
            {
                School = FValue.trim();
            }
            else
                School = null;
        }
        if (FCode.equalsIgnoreCase("ChargeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeDate = FValue.trim();
            }
            else
                ChargeDate = null;
        }
        if (FCode.equalsIgnoreCase("Mark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mark = FValue.trim();
            }
            else
                Mark = null;
        }
        return true;
    }


    public String toString() {
        return "LCPolicyInfoPojo [" +
                "ContNo="+ContNo +
                ", InsuredNo="+InsuredNo +
                ", ContplanCode="+ContplanCode +
                ", ContplanName="+ContplanName +
                ", BasePrem="+BasePrem +
                ", AddPrem="+AddPrem +
                ", ActiveDate="+ActiveDate +
                ", InsuYear="+InsuYear +
                ", InsuYearFlag="+InsuYearFlag +
                ", Cvaliintv="+Cvaliintv +
                ", CvaliintvFlag="+CvaliintvFlag +
                ", SisinSuredNo="+SisinSuredNo +
                ", TraveLcountry="+TraveLcountry +
                ", AgentIp="+AgentIp +
                ", FlightNo="+FlightNo +
                ", RescuecardNo="+RescuecardNo +
                ", HandlerName="+HandlerName +
                ", HandlerPhone="+HandlerPhone +
                ", SignDate="+SignDate +
                ", SignTime="+SignTime +
                ", agentcom="+agentcom +
                ", RiskType="+RiskType +
                ", OrderType="+OrderType +
                ", JYContNo="+JYContNo +
                ", JYCertNo="+JYCertNo +
                ", JYStartDate="+JYStartDate +
                ", JYEndDate="+JYEndDate +
                ", LoanAmount="+LoanAmount +
                ", LendCom="+LendCom +
                ", LoanerNature="+LoanerNature +
                ", LoanerNatureName="+LoanerNatureName +
                ", LendTerm="+LendTerm +
                ", CountryCode="+CountryCode +
                ", GasCompany="+GasCompany +
                ", GasUserAddress="+GasUserAddress +
                ", PayoutPro="+PayoutPro +
                ", School="+School +
                ", ChargeDate="+ChargeDate +
                ", Mark="+Mark +"]";
    }
}
