/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskFeePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskFeePojo implements Pojo,Serializable {
    // @Field
    /** 管理费编码 */
    private String FeeCode; 
    /** 管理费名称 */
    private String FeeName; 
    /** 管理费说明 */
    private String FeeNoti; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 交费项编码 */
    private String PayPlanCode; 
    /** 关系说明 */
    private String PayInsuAccName; 
    /** 管理费分类 */
    private String FeeKind; 
    /** 费用项目分类 */
    private String FeeItemType; 
    /** 费用收取位置 */
    private String FeeTakePlace; 
    /** 管理费计算方式 */
    private String FeeCalMode; 
    /** 计算类型 */
    private String FeeCalModeType; 
    /** 管理费计算公式 */
    private String FeeCalCode; 
    /** 固定值 */
    private double FeeValue; 
    /** 比较值 */
    private double CompareValue; 
    /** 扣除管理费周期 */
    private String FeePeriod; 
    /** 扣除管理费最大次数 */
    private int MaxTime; 
    /** 缺省标记 */
    private String DefaultFlag; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 扣除管理费起始时间 */
    private String  FeeStartDate;
    /** 管理费顺序 */
    private int FeeNum; 
    /** 计价基数类型 */
    private String FeeBaseType; 
    /** 后续处理类名 */
    private String InterfaceClassName; 
    /** 收取类型 */
    private String FeeType; 
    /** 收取时点 */
    private String PeriodType; 


    public static final int FIELDNUM = 28;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getFeeCode() {
        return FeeCode;
    }
    public void setFeeCode(String aFeeCode) {
        FeeCode = aFeeCode;
    }
    public String getFeeName() {
        return FeeName;
    }
    public void setFeeName(String aFeeName) {
        FeeName = aFeeName;
    }
    public String getFeeNoti() {
        return FeeNoti;
    }
    public void setFeeNoti(String aFeeNoti) {
        FeeNoti = aFeeNoti;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getPayInsuAccName() {
        return PayInsuAccName;
    }
    public void setPayInsuAccName(String aPayInsuAccName) {
        PayInsuAccName = aPayInsuAccName;
    }
    public String getFeeKind() {
        return FeeKind;
    }
    public void setFeeKind(String aFeeKind) {
        FeeKind = aFeeKind;
    }
    public String getFeeItemType() {
        return FeeItemType;
    }
    public void setFeeItemType(String aFeeItemType) {
        FeeItemType = aFeeItemType;
    }
    public String getFeeTakePlace() {
        return FeeTakePlace;
    }
    public void setFeeTakePlace(String aFeeTakePlace) {
        FeeTakePlace = aFeeTakePlace;
    }
    public String getFeeCalMode() {
        return FeeCalMode;
    }
    public void setFeeCalMode(String aFeeCalMode) {
        FeeCalMode = aFeeCalMode;
    }
    public String getFeeCalModeType() {
        return FeeCalModeType;
    }
    public void setFeeCalModeType(String aFeeCalModeType) {
        FeeCalModeType = aFeeCalModeType;
    }
    public String getFeeCalCode() {
        return FeeCalCode;
    }
    public void setFeeCalCode(String aFeeCalCode) {
        FeeCalCode = aFeeCalCode;
    }
    public double getFeeValue() {
        return FeeValue;
    }
    public void setFeeValue(double aFeeValue) {
        FeeValue = aFeeValue;
    }
    public void setFeeValue(String aFeeValue) {
        if (aFeeValue != null && !aFeeValue.equals("")) {
            Double tDouble = new Double(aFeeValue);
            double d = tDouble.doubleValue();
            FeeValue = d;
        }
    }

    public double getCompareValue() {
        return CompareValue;
    }
    public void setCompareValue(double aCompareValue) {
        CompareValue = aCompareValue;
    }
    public void setCompareValue(String aCompareValue) {
        if (aCompareValue != null && !aCompareValue.equals("")) {
            Double tDouble = new Double(aCompareValue);
            double d = tDouble.doubleValue();
            CompareValue = d;
        }
    }

    public String getFeePeriod() {
        return FeePeriod;
    }
    public void setFeePeriod(String aFeePeriod) {
        FeePeriod = aFeePeriod;
    }
    public int getMaxTime() {
        return MaxTime;
    }
    public void setMaxTime(int aMaxTime) {
        MaxTime = aMaxTime;
    }
    public void setMaxTime(String aMaxTime) {
        if (aMaxTime != null && !aMaxTime.equals("")) {
            Integer tInteger = new Integer(aMaxTime);
            int i = tInteger.intValue();
            MaxTime = i;
        }
    }

    public String getDefaultFlag() {
        return DefaultFlag;
    }
    public void setDefaultFlag(String aDefaultFlag) {
        DefaultFlag = aDefaultFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFeeStartDate() {
        return FeeStartDate;
    }
    public void setFeeStartDate(String aFeeStartDate) {
        FeeStartDate = aFeeStartDate;
    }
    public int getFeeNum() {
        return FeeNum;
    }
    public void setFeeNum(int aFeeNum) {
        FeeNum = aFeeNum;
    }
    public void setFeeNum(String aFeeNum) {
        if (aFeeNum != null && !aFeeNum.equals("")) {
            Integer tInteger = new Integer(aFeeNum);
            int i = tInteger.intValue();
            FeeNum = i;
        }
    }

    public String getFeeBaseType() {
        return FeeBaseType;
    }
    public void setFeeBaseType(String aFeeBaseType) {
        FeeBaseType = aFeeBaseType;
    }
    public String getInterfaceClassName() {
        return InterfaceClassName;
    }
    public void setInterfaceClassName(String aInterfaceClassName) {
        InterfaceClassName = aInterfaceClassName;
    }
    public String getFeeType() {
        return FeeType;
    }
    public void setFeeType(String aFeeType) {
        FeeType = aFeeType;
    }
    public String getPeriodType() {
        return PeriodType;
    }
    public void setPeriodType(String aPeriodType) {
        PeriodType = aPeriodType;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("FeeCode") ) {
            return 0;
        }
        if( strFieldName.equals("FeeName") ) {
            return 1;
        }
        if( strFieldName.equals("FeeNoti") ) {
            return 2;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 3;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 4;
        }
        if( strFieldName.equals("PayInsuAccName") ) {
            return 5;
        }
        if( strFieldName.equals("FeeKind") ) {
            return 6;
        }
        if( strFieldName.equals("FeeItemType") ) {
            return 7;
        }
        if( strFieldName.equals("FeeTakePlace") ) {
            return 8;
        }
        if( strFieldName.equals("FeeCalMode") ) {
            return 9;
        }
        if( strFieldName.equals("FeeCalModeType") ) {
            return 10;
        }
        if( strFieldName.equals("FeeCalCode") ) {
            return 11;
        }
        if( strFieldName.equals("FeeValue") ) {
            return 12;
        }
        if( strFieldName.equals("CompareValue") ) {
            return 13;
        }
        if( strFieldName.equals("FeePeriod") ) {
            return 14;
        }
        if( strFieldName.equals("MaxTime") ) {
            return 15;
        }
        if( strFieldName.equals("DefaultFlag") ) {
            return 16;
        }
        if( strFieldName.equals("Operator") ) {
            return 17;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 18;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 19;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 20;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 21;
        }
        if( strFieldName.equals("FeeStartDate") ) {
            return 22;
        }
        if( strFieldName.equals("FeeNum") ) {
            return 23;
        }
        if( strFieldName.equals("FeeBaseType") ) {
            return 24;
        }
        if( strFieldName.equals("InterfaceClassName") ) {
            return 25;
        }
        if( strFieldName.equals("FeeType") ) {
            return 26;
        }
        if( strFieldName.equals("PeriodType") ) {
            return 27;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "FeeCode";
                break;
            case 1:
                strFieldName = "FeeName";
                break;
            case 2:
                strFieldName = "FeeNoti";
                break;
            case 3:
                strFieldName = "InsuAccNo";
                break;
            case 4:
                strFieldName = "PayPlanCode";
                break;
            case 5:
                strFieldName = "PayInsuAccName";
                break;
            case 6:
                strFieldName = "FeeKind";
                break;
            case 7:
                strFieldName = "FeeItemType";
                break;
            case 8:
                strFieldName = "FeeTakePlace";
                break;
            case 9:
                strFieldName = "FeeCalMode";
                break;
            case 10:
                strFieldName = "FeeCalModeType";
                break;
            case 11:
                strFieldName = "FeeCalCode";
                break;
            case 12:
                strFieldName = "FeeValue";
                break;
            case 13:
                strFieldName = "CompareValue";
                break;
            case 14:
                strFieldName = "FeePeriod";
                break;
            case 15:
                strFieldName = "MaxTime";
                break;
            case 16:
                strFieldName = "DefaultFlag";
                break;
            case 17:
                strFieldName = "Operator";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            case 22:
                strFieldName = "FeeStartDate";
                break;
            case 23:
                strFieldName = "FeeNum";
                break;
            case 24:
                strFieldName = "FeeBaseType";
                break;
            case 25:
                strFieldName = "InterfaceClassName";
                break;
            case 26:
                strFieldName = "FeeType";
                break;
            case 27:
                strFieldName = "PeriodType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "FEECODE":
                return Schema.TYPE_STRING;
            case "FEENAME":
                return Schema.TYPE_STRING;
            case "FEENOTI":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "PAYINSUACCNAME":
                return Schema.TYPE_STRING;
            case "FEEKIND":
                return Schema.TYPE_STRING;
            case "FEEITEMTYPE":
                return Schema.TYPE_STRING;
            case "FEETAKEPLACE":
                return Schema.TYPE_STRING;
            case "FEECALMODE":
                return Schema.TYPE_STRING;
            case "FEECALMODETYPE":
                return Schema.TYPE_STRING;
            case "FEECALCODE":
                return Schema.TYPE_STRING;
            case "FEEVALUE":
                return Schema.TYPE_DOUBLE;
            case "COMPAREVALUE":
                return Schema.TYPE_DOUBLE;
            case "FEEPERIOD":
                return Schema.TYPE_STRING;
            case "MAXTIME":
                return Schema.TYPE_INT;
            case "DEFAULTFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FEESTARTDATE":
                return Schema.TYPE_STRING;
            case "FEENUM":
                return Schema.TYPE_INT;
            case "FEEBASETYPE":
                return Schema.TYPE_STRING;
            case "INTERFACECLASSNAME":
                return Schema.TYPE_STRING;
            case "FEETYPE":
                return Schema.TYPE_STRING;
            case "PERIODTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_DOUBLE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_INT;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_INT;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("FeeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCode));
        }
        if (FCode.equalsIgnoreCase("FeeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeName));
        }
        if (FCode.equalsIgnoreCase("FeeNoti")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeNoti));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("PayInsuAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayInsuAccName));
        }
        if (FCode.equalsIgnoreCase("FeeKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeKind));
        }
        if (FCode.equalsIgnoreCase("FeeItemType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeItemType));
        }
        if (FCode.equalsIgnoreCase("FeeTakePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeTakePlace));
        }
        if (FCode.equalsIgnoreCase("FeeCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCalMode));
        }
        if (FCode.equalsIgnoreCase("FeeCalModeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCalModeType));
        }
        if (FCode.equalsIgnoreCase("FeeCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCalCode));
        }
        if (FCode.equalsIgnoreCase("FeeValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeValue));
        }
        if (FCode.equalsIgnoreCase("CompareValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompareValue));
        }
        if (FCode.equalsIgnoreCase("FeePeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeePeriod));
        }
        if (FCode.equalsIgnoreCase("MaxTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxTime));
        }
        if (FCode.equalsIgnoreCase("DefaultFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FeeStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeStartDate));
        }
        if (FCode.equalsIgnoreCase("FeeNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeNum));
        }
        if (FCode.equalsIgnoreCase("FeeBaseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeBaseType));
        }
        if (FCode.equalsIgnoreCase("InterfaceClassName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterfaceClassName));
        }
        if (FCode.equalsIgnoreCase("FeeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
        }
        if (FCode.equalsIgnoreCase("PeriodType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeriodType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(FeeCode);
                break;
            case 1:
                strFieldValue = String.valueOf(FeeName);
                break;
            case 2:
                strFieldValue = String.valueOf(FeeNoti);
                break;
            case 3:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 4:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 5:
                strFieldValue = String.valueOf(PayInsuAccName);
                break;
            case 6:
                strFieldValue = String.valueOf(FeeKind);
                break;
            case 7:
                strFieldValue = String.valueOf(FeeItemType);
                break;
            case 8:
                strFieldValue = String.valueOf(FeeTakePlace);
                break;
            case 9:
                strFieldValue = String.valueOf(FeeCalMode);
                break;
            case 10:
                strFieldValue = String.valueOf(FeeCalModeType);
                break;
            case 11:
                strFieldValue = String.valueOf(FeeCalCode);
                break;
            case 12:
                strFieldValue = String.valueOf(FeeValue);
                break;
            case 13:
                strFieldValue = String.valueOf(CompareValue);
                break;
            case 14:
                strFieldValue = String.valueOf(FeePeriod);
                break;
            case 15:
                strFieldValue = String.valueOf(MaxTime);
                break;
            case 16:
                strFieldValue = String.valueOf(DefaultFlag);
                break;
            case 17:
                strFieldValue = String.valueOf(Operator);
                break;
            case 18:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 19:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 20:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 21:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 22:
                strFieldValue = String.valueOf(FeeStartDate);
                break;
            case 23:
                strFieldValue = String.valueOf(FeeNum);
                break;
            case 24:
                strFieldValue = String.valueOf(FeeBaseType);
                break;
            case 25:
                strFieldValue = String.valueOf(InterfaceClassName);
                break;
            case 26:
                strFieldValue = String.valueOf(FeeType);
                break;
            case 27:
                strFieldValue = String.valueOf(PeriodType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("FeeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCode = FValue.trim();
            }
            else
                FeeCode = null;
        }
        if (FCode.equalsIgnoreCase("FeeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeName = FValue.trim();
            }
            else
                FeeName = null;
        }
        if (FCode.equalsIgnoreCase("FeeNoti")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeNoti = FValue.trim();
            }
            else
                FeeNoti = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PayInsuAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayInsuAccName = FValue.trim();
            }
            else
                PayInsuAccName = null;
        }
        if (FCode.equalsIgnoreCase("FeeKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeKind = FValue.trim();
            }
            else
                FeeKind = null;
        }
        if (FCode.equalsIgnoreCase("FeeItemType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeItemType = FValue.trim();
            }
            else
                FeeItemType = null;
        }
        if (FCode.equalsIgnoreCase("FeeTakePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeTakePlace = FValue.trim();
            }
            else
                FeeTakePlace = null;
        }
        if (FCode.equalsIgnoreCase("FeeCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCalMode = FValue.trim();
            }
            else
                FeeCalMode = null;
        }
        if (FCode.equalsIgnoreCase("FeeCalModeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCalModeType = FValue.trim();
            }
            else
                FeeCalModeType = null;
        }
        if (FCode.equalsIgnoreCase("FeeCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCalCode = FValue.trim();
            }
            else
                FeeCalCode = null;
        }
        if (FCode.equalsIgnoreCase("FeeValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("CompareValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                CompareValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeePeriod")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeePeriod = FValue.trim();
            }
            else
                FeePeriod = null;
        }
        if (FCode.equalsIgnoreCase("MaxTime")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxTime = i;
            }
        }
        if (FCode.equalsIgnoreCase("DefaultFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DefaultFlag = FValue.trim();
            }
            else
                DefaultFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FeeStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeStartDate = FValue.trim();
            }
            else
                FeeStartDate = null;
        }
        if (FCode.equalsIgnoreCase("FeeNum")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                FeeNum = i;
            }
        }
        if (FCode.equalsIgnoreCase("FeeBaseType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeBaseType = FValue.trim();
            }
            else
                FeeBaseType = null;
        }
        if (FCode.equalsIgnoreCase("InterfaceClassName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InterfaceClassName = FValue.trim();
            }
            else
                InterfaceClassName = null;
        }
        if (FCode.equalsIgnoreCase("FeeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeType = FValue.trim();
            }
            else
                FeeType = null;
        }
        if (FCode.equalsIgnoreCase("PeriodType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PeriodType = FValue.trim();
            }
            else
                PeriodType = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskFeePojo [" +
            "FeeCode="+FeeCode +
            ", FeeName="+FeeName +
            ", FeeNoti="+FeeNoti +
            ", InsuAccNo="+InsuAccNo +
            ", PayPlanCode="+PayPlanCode +
            ", PayInsuAccName="+PayInsuAccName +
            ", FeeKind="+FeeKind +
            ", FeeItemType="+FeeItemType +
            ", FeeTakePlace="+FeeTakePlace +
            ", FeeCalMode="+FeeCalMode +
            ", FeeCalModeType="+FeeCalModeType +
            ", FeeCalCode="+FeeCalCode +
            ", FeeValue="+FeeValue +
            ", CompareValue="+CompareValue +
            ", FeePeriod="+FeePeriod +
            ", MaxTime="+MaxTime +
            ", DefaultFlag="+DefaultFlag +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", FeeStartDate="+FeeStartDate +
            ", FeeNum="+FeeNum +
            ", FeeBaseType="+FeeBaseType +
            ", InterfaceClassName="+InterfaceClassName +
            ", FeeType="+FeeType +
            ", PeriodType="+PeriodType +"]";
    }
}
