package com.sinosoft.lis.entity;

import java.io.Serializable;

/**
 * Created by zhangfei on 2018/6/14.
 */
public class LCApiPojo implements Serializable {
    /** 多年期标记 **/
    private String MultiYearMark;
    /** 首期保单标记 **/
    private String YearMark;
    /** 网点编码 **/
    private String OutLetsCode;
    /** 收费模式 **/
    private String ChargeMode;
    /** 收费类型 **/
    private String ChargeType;
    /** 渠道编码 **/
    private String ChannelCode;

    public LCApiPojo() {
    }

    public String getMultiYearMark() {
        return MultiYearMark;
    }

    public void setMultiYearMark(String multiYearMark) {
        MultiYearMark = multiYearMark;
    }

    public String getYearMark() {
        return YearMark;
    }

    public void setYearMark(String yearMark) {
        YearMark = yearMark;
    }

    public String getOutLetsCode() {
        return OutLetsCode;
    }

    public void setOutLetsCode(String outLetsCode) {
        OutLetsCode = outLetsCode;
    }

    public String getChargeMode() {
        return ChargeMode;
    }

    public void setChargeMode(String chargeMode) {
        ChargeMode = chargeMode;
    }

    public String getChargeType() {
        return ChargeType;
    }

    public void setChargeType(String chargeType) {
        ChargeType = chargeType;
    }

    public String getChannelCode() {
        return ChannelCode;
    }

    public void setChannelCode(String channelCode) {
        ChannelCode = channelCode;
    }

    @Override
    public String toString() {
        return "LCApiPojo{" +
                "MultiYearMark='" + MultiYearMark + '\'' +
                ", YearMark='" + YearMark + '\'' +
                ", OutLetsCode='" + OutLetsCode + '\'' +
                ", ChargeMode='" + ChargeMode + '\'' +
                ", ChargeType='" + ChargeType + '\'' +
                ", ChannelCode='" + ChannelCode + '\'' +
                '}';
    }
}
