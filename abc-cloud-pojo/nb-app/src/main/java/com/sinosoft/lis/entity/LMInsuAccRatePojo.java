/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMInsuAccRatePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMInsuAccRatePojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 会计年度 */
    private int FiscalYear; 
    /** 结算日期 */
    private String  BalaDate;
    /** 利率应公布日期 */
    private String  SRateDate;
    /** 利率实际公布日期 */
    private String  ARateDate;
    /** 结算/分红标志 */
    private String Flag; 
    /** 利率类型 */
    private String RateIntv; 
    /** 利率 */
    private double Rate; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 开始日期 */
    private String  StartDate;
    /** 结束日期 */
    private String  EndDate;
    /** 分红类型 */
    private String Bonustype; 


    public static final int FIELDNUM = 15;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public int getFiscalYear() {
        return FiscalYear;
    }
    public void setFiscalYear(int aFiscalYear) {
        FiscalYear = aFiscalYear;
    }
    public void setFiscalYear(String aFiscalYear) {
        if (aFiscalYear != null && !aFiscalYear.equals("")) {
            Integer tInteger = new Integer(aFiscalYear);
            int i = tInteger.intValue();
            FiscalYear = i;
        }
    }

    public String getBalaDate() {
        return BalaDate;
    }
    public void setBalaDate(String aBalaDate) {
        BalaDate = aBalaDate;
    }
    public String getSRateDate() {
        return SRateDate;
    }
    public void setSRateDate(String aSRateDate) {
        SRateDate = aSRateDate;
    }
    public String getARateDate() {
        return ARateDate;
    }
    public void setARateDate(String aARateDate) {
        ARateDate = aARateDate;
    }
    public String getFlag() {
        return Flag;
    }
    public void setFlag(String aFlag) {
        Flag = aFlag;
    }
    public String getRateIntv() {
        return RateIntv;
    }
    public void setRateIntv(String aRateIntv) {
        RateIntv = aRateIntv;
    }
    public double getRate() {
        return Rate;
    }
    public void setRate(double aRate) {
        Rate = aRate;
    }
    public void setRate(String aRate) {
        if (aRate != null && !aRate.equals("")) {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getBonustype() {
        return Bonustype;
    }
    public void setBonustype(String aBonustype) {
        Bonustype = aBonustype;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 1;
        }
        if( strFieldName.equals("FiscalYear") ) {
            return 2;
        }
        if( strFieldName.equals("BalaDate") ) {
            return 3;
        }
        if( strFieldName.equals("SRateDate") ) {
            return 4;
        }
        if( strFieldName.equals("ARateDate") ) {
            return 5;
        }
        if( strFieldName.equals("Flag") ) {
            return 6;
        }
        if( strFieldName.equals("RateIntv") ) {
            return 7;
        }
        if( strFieldName.equals("Rate") ) {
            return 8;
        }
        if( strFieldName.equals("Operator") ) {
            return 9;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 10;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 11;
        }
        if( strFieldName.equals("StartDate") ) {
            return 12;
        }
        if( strFieldName.equals("EndDate") ) {
            return 13;
        }
        if( strFieldName.equals("Bonustype") ) {
            return 14;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "InsuAccNo";
                break;
            case 2:
                strFieldName = "FiscalYear";
                break;
            case 3:
                strFieldName = "BalaDate";
                break;
            case 4:
                strFieldName = "SRateDate";
                break;
            case 5:
                strFieldName = "ARateDate";
                break;
            case 6:
                strFieldName = "Flag";
                break;
            case 7:
                strFieldName = "RateIntv";
                break;
            case 8:
                strFieldName = "Rate";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "StartDate";
                break;
            case 13:
                strFieldName = "EndDate";
                break;
            case 14:
                strFieldName = "Bonustype";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "FISCALYEAR":
                return Schema.TYPE_INT;
            case "BALADATE":
                return Schema.TYPE_STRING;
            case "SRATEDATE":
                return Schema.TYPE_STRING;
            case "ARATEDATE":
                return Schema.TYPE_STRING;
            case "FLAG":
                return Schema.TYPE_STRING;
            case "RATEINTV":
                return Schema.TYPE_STRING;
            case "RATE":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "BONUSTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_INT;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("FiscalYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FiscalYear));
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaDate));
        }
        if (FCode.equalsIgnoreCase("SRateDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SRateDate));
        }
        if (FCode.equalsIgnoreCase("ARateDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ARateDate));
        }
        if (FCode.equalsIgnoreCase("Flag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
        }
        if (FCode.equalsIgnoreCase("RateIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RateIntv));
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("Bonustype")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bonustype));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 2:
                strFieldValue = String.valueOf(FiscalYear);
                break;
            case 3:
                strFieldValue = String.valueOf(BalaDate);
                break;
            case 4:
                strFieldValue = String.valueOf(SRateDate);
                break;
            case 5:
                strFieldValue = String.valueOf(ARateDate);
                break;
            case 6:
                strFieldValue = String.valueOf(Flag);
                break;
            case 7:
                strFieldValue = String.valueOf(RateIntv);
                break;
            case 8:
                strFieldValue = String.valueOf(Rate);
                break;
            case 9:
                strFieldValue = String.valueOf(Operator);
                break;
            case 10:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 11:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 12:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 13:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 14:
                strFieldValue = String.valueOf(Bonustype);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("FiscalYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                FiscalYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaDate = FValue.trim();
            }
            else
                BalaDate = null;
        }
        if (FCode.equalsIgnoreCase("SRateDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SRateDate = FValue.trim();
            }
            else
                SRateDate = null;
        }
        if (FCode.equalsIgnoreCase("ARateDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ARateDate = FValue.trim();
            }
            else
                ARateDate = null;
        }
        if (FCode.equalsIgnoreCase("Flag")) {
            if( FValue != null && !FValue.equals(""))
            {
                Flag = FValue.trim();
            }
            else
                Flag = null;
        }
        if (FCode.equalsIgnoreCase("RateIntv")) {
            if( FValue != null && !FValue.equals(""))
            {
                RateIntv = FValue.trim();
            }
            else
                RateIntv = null;
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("Bonustype")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bonustype = FValue.trim();
            }
            else
                Bonustype = null;
        }
        return true;
    }


    public String toString() {
    return "LMInsuAccRatePojo [" +
            "RiskCode="+RiskCode +
            ", InsuAccNo="+InsuAccNo +
            ", FiscalYear="+FiscalYear +
            ", BalaDate="+BalaDate +
            ", SRateDate="+SRateDate +
            ", ARateDate="+ARateDate +
            ", Flag="+Flag +
            ", RateIntv="+RateIntv +
            ", Rate="+Rate +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", Bonustype="+Bonustype +"]";
    }
}
