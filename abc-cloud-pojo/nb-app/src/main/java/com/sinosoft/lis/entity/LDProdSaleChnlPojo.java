/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDProdSaleChnlPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LDProdSaleChnlPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码/计划编码 */
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 销售渠道 */
    @RedisPrimaryHKey
    private String SaleChnl; 
    /** 销售机构 */
    @RedisPrimaryHKey
    private String SaleMngCom; 
    /** 银行编码 */
    @RedisPrimaryHKey
    private String BankCode; 
    /** 销售起期 */
    private String  StartDate;
    /** 销售止期 */
    private String  EndDate;
    /** 操作员 */
    private String Operator; 
    /** 创建日期 */
    private String  MakeDate;
    /** 创建时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 11;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getSaleMngCom() {
        return SaleMngCom;
    }
    public void setSaleMngCom(String aSaleMngCom) {
        SaleMngCom = aSaleMngCom;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 1;
        }
        if( strFieldName.equals("SaleMngCom") ) {
            return 2;
        }
        if( strFieldName.equals("BankCode") ) {
            return 3;
        }
        if( strFieldName.equals("StartDate") ) {
            return 4;
        }
        if( strFieldName.equals("EndDate") ) {
            return 5;
        }
        if( strFieldName.equals("Operator") ) {
            return 6;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 7;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 8;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 10;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "SaleChnl";
                break;
            case 2:
                strFieldName = "SaleMngCom";
                break;
            case 3:
                strFieldName = "BankCode";
                break;
            case 4:
                strFieldName = "StartDate";
                break;
            case 5:
                strFieldName = "EndDate";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SALEMNGCOM":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("SaleMngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleMngCom));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 2:
                strFieldValue = String.valueOf(SaleMngCom);
                break;
            case 3:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 4:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 5:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 6:
                strFieldValue = String.valueOf(Operator);
                break;
            case 7:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 8:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 9:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 10:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("SaleMngCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleMngCom = FValue.trim();
            }
            else
                SaleMngCom = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LDProdSaleChnlPojo [" +
            "RiskCode="+RiskCode +
            ", SaleChnl="+SaleChnl +
            ", SaleMngCom="+SaleMngCom +
            ", BankCode="+BankCode +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
