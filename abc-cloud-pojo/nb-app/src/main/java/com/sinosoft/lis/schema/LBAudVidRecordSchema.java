/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBAudVidRecordDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBAudVidRecordSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-13
 */
public class LBAudVidRecordSchema implements Schema, Cloneable {
    // @Field
    /** Edorno */
    private String EdorNo;
    /** Prtno */
    private String PrtNo;
    /** Isrecord */
    private String IsRecord;
    /** Isquailty */
    private String IsQuailty;
    /** Donerecorddate */
    private Date DoneRecordDate;
    /** Donerecordtime */
    private String DoneRecordTime;
    /** Quailtyresult */
    private String QuailtyResult;
    /** Quailtydate */
    private Date QuailtyDate;
    /** Qualitytime */
    private String QualityTime;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** Remark */
    private String Remark;
    /** Reason */
    private String Reason;

    public static final int FIELDNUM = 16;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBAudVidRecordSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PrtNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBAudVidRecordSchema cloned = (LBAudVidRecordSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getIsRecord() {
        return IsRecord;
    }
    public void setIsRecord(String aIsRecord) {
        IsRecord = aIsRecord;
    }
    public String getIsQuailty() {
        return IsQuailty;
    }
    public void setIsQuailty(String aIsQuailty) {
        IsQuailty = aIsQuailty;
    }
    public String getDoneRecordDate() {
        if(DoneRecordDate != null) {
            return fDate.getString(DoneRecordDate);
        } else {
            return null;
        }
    }
    public void setDoneRecordDate(Date aDoneRecordDate) {
        DoneRecordDate = aDoneRecordDate;
    }
    public void setDoneRecordDate(String aDoneRecordDate) {
        if (aDoneRecordDate != null && !aDoneRecordDate.equals("")) {
            DoneRecordDate = fDate.getDate(aDoneRecordDate);
        } else
            DoneRecordDate = null;
    }

    public String getDoneRecordTime() {
        return DoneRecordTime;
    }
    public void setDoneRecordTime(String aDoneRecordTime) {
        DoneRecordTime = aDoneRecordTime;
    }
    public String getQuailtyResult() {
        return QuailtyResult;
    }
    public void setQuailtyResult(String aQuailtyResult) {
        QuailtyResult = aQuailtyResult;
    }
    public String getQuailtyDate() {
        if(QuailtyDate != null) {
            return fDate.getString(QuailtyDate);
        } else {
            return null;
        }
    }
    public void setQuailtyDate(Date aQuailtyDate) {
        QuailtyDate = aQuailtyDate;
    }
    public void setQuailtyDate(String aQuailtyDate) {
        if (aQuailtyDate != null && !aQuailtyDate.equals("")) {
            QuailtyDate = fDate.getDate(aQuailtyDate);
        } else
            QuailtyDate = null;
    }

    public String getQualityTime() {
        return QualityTime;
    }
    public void setQualityTime(String aQualityTime) {
        QualityTime = aQualityTime;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getReason() {
        return Reason;
    }
    public void setReason(String aReason) {
        Reason = aReason;
    }

    /**
    * 使用另外一个 LBAudVidRecordSchema 对象给 Schema 赋值
    * @param: aLBAudVidRecordSchema LBAudVidRecordSchema
    **/
    public void setSchema(LBAudVidRecordSchema aLBAudVidRecordSchema) {
        this.EdorNo = aLBAudVidRecordSchema.getEdorNo();
        this.PrtNo = aLBAudVidRecordSchema.getPrtNo();
        this.IsRecord = aLBAudVidRecordSchema.getIsRecord();
        this.IsQuailty = aLBAudVidRecordSchema.getIsQuailty();
        this.DoneRecordDate = fDate.getDate( aLBAudVidRecordSchema.getDoneRecordDate());
        this.DoneRecordTime = aLBAudVidRecordSchema.getDoneRecordTime();
        this.QuailtyResult = aLBAudVidRecordSchema.getQuailtyResult();
        this.QuailtyDate = fDate.getDate( aLBAudVidRecordSchema.getQuailtyDate());
        this.QualityTime = aLBAudVidRecordSchema.getQualityTime();
        this.Operator = aLBAudVidRecordSchema.getOperator();
        this.MakeDate = fDate.getDate( aLBAudVidRecordSchema.getMakeDate());
        this.MakeTime = aLBAudVidRecordSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLBAudVidRecordSchema.getModifyDate());
        this.ModifyTime = aLBAudVidRecordSchema.getModifyTime();
        this.Remark = aLBAudVidRecordSchema.getRemark();
        this.Reason = aLBAudVidRecordSchema.getReason();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("IsRecord") == null )
                this.IsRecord = null;
            else
                this.IsRecord = rs.getString("IsRecord").trim();

            if( rs.getString("IsQuailty") == null )
                this.IsQuailty = null;
            else
                this.IsQuailty = rs.getString("IsQuailty").trim();

            this.DoneRecordDate = rs.getDate("DoneRecordDate");
            if( rs.getString("DoneRecordTime") == null )
                this.DoneRecordTime = null;
            else
                this.DoneRecordTime = rs.getString("DoneRecordTime").trim();

            if( rs.getString("QuailtyResult") == null )
                this.QuailtyResult = null;
            else
                this.QuailtyResult = rs.getString("QuailtyResult").trim();

            this.QuailtyDate = rs.getDate("QuailtyDate");
            if( rs.getString("QualityTime") == null )
                this.QualityTime = null;
            else
                this.QualityTime = rs.getString("QualityTime").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("Reason") == null )
                this.Reason = null;
            else
                this.Reason = rs.getString("Reason").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAudVidRecordSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBAudVidRecordSchema getSchema() {
        LBAudVidRecordSchema aLBAudVidRecordSchema = new LBAudVidRecordSchema();
        aLBAudVidRecordSchema.setSchema(this);
        return aLBAudVidRecordSchema;
    }

    public LBAudVidRecordDB getDB() {
        LBAudVidRecordDB aDBOper = new LBAudVidRecordDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBAudVidRecord描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsRecord)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsQuailty)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( DoneRecordDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DoneRecordTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QuailtyResult)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( QuailtyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QualityTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Reason));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBAudVidRecord>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            IsRecord = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            IsQuailty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            DoneRecordDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER));
            DoneRecordTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            QuailtyResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            QuailtyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            QualityTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBAudVidRecordSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("IsRecord")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsRecord));
        }
        if (FCode.equalsIgnoreCase("IsQuailty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsQuailty));
        }
        if (FCode.equalsIgnoreCase("DoneRecordDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDoneRecordDate()));
        }
        if (FCode.equalsIgnoreCase("DoneRecordTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoneRecordTime));
        }
        if (FCode.equalsIgnoreCase("QuailtyResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuailtyResult));
        }
        if (FCode.equalsIgnoreCase("QuailtyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getQuailtyDate()));
        }
        if (FCode.equalsIgnoreCase("QualityTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualityTime));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Reason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(IsRecord);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(IsQuailty);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDoneRecordDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DoneRecordTime);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(QuailtyResult);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getQuailtyDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(QualityTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Reason);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("IsRecord")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsRecord = FValue.trim();
            }
            else
                IsRecord = null;
        }
        if (FCode.equalsIgnoreCase("IsQuailty")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsQuailty = FValue.trim();
            }
            else
                IsQuailty = null;
        }
        if (FCode.equalsIgnoreCase("DoneRecordDate")) {
            if(FValue != null && !FValue.equals("")) {
                DoneRecordDate = fDate.getDate( FValue );
            }
            else
                DoneRecordDate = null;
        }
        if (FCode.equalsIgnoreCase("DoneRecordTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                DoneRecordTime = FValue.trim();
            }
            else
                DoneRecordTime = null;
        }
        if (FCode.equalsIgnoreCase("QuailtyResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                QuailtyResult = FValue.trim();
            }
            else
                QuailtyResult = null;
        }
        if (FCode.equalsIgnoreCase("QuailtyDate")) {
            if(FValue != null && !FValue.equals("")) {
                QuailtyDate = fDate.getDate( FValue );
            }
            else
                QuailtyDate = null;
        }
        if (FCode.equalsIgnoreCase("QualityTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualityTime = FValue.trim();
            }
            else
                QualityTime = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Reason")) {
            if( FValue != null && !FValue.equals(""))
            {
                Reason = FValue.trim();
            }
            else
                Reason = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBAudVidRecordSchema other = (LBAudVidRecordSchema)otherObject;
        return
            EdorNo.equals(other.getEdorNo())
            && PrtNo.equals(other.getPrtNo())
            && IsRecord.equals(other.getIsRecord())
            && IsQuailty.equals(other.getIsQuailty())
            && fDate.getString(DoneRecordDate).equals(other.getDoneRecordDate())
            && DoneRecordTime.equals(other.getDoneRecordTime())
            && QuailtyResult.equals(other.getQuailtyResult())
            && fDate.getString(QuailtyDate).equals(other.getQuailtyDate())
            && QualityTime.equals(other.getQualityTime())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Remark.equals(other.getRemark())
            && Reason.equals(other.getReason());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("EdorNo") ) {
            return 0;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 1;
        }
        if( strFieldName.equals("IsRecord") ) {
            return 2;
        }
        if( strFieldName.equals("IsQuailty") ) {
            return 3;
        }
        if( strFieldName.equals("DoneRecordDate") ) {
            return 4;
        }
        if( strFieldName.equals("DoneRecordTime") ) {
            return 5;
        }
        if( strFieldName.equals("QuailtyResult") ) {
            return 6;
        }
        if( strFieldName.equals("QuailtyDate") ) {
            return 7;
        }
        if( strFieldName.equals("QualityTime") ) {
            return 8;
        }
        if( strFieldName.equals("Operator") ) {
            return 9;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 10;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 11;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 12;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 13;
        }
        if( strFieldName.equals("Remark") ) {
            return 14;
        }
        if( strFieldName.equals("Reason") ) {
            return 15;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "PrtNo";
                break;
            case 2:
                strFieldName = "IsRecord";
                break;
            case 3:
                strFieldName = "IsQuailty";
                break;
            case 4:
                strFieldName = "DoneRecordDate";
                break;
            case 5:
                strFieldName = "DoneRecordTime";
                break;
            case 6:
                strFieldName = "QuailtyResult";
                break;
            case 7:
                strFieldName = "QuailtyDate";
                break;
            case 8:
                strFieldName = "QualityTime";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "ModifyDate";
                break;
            case 13:
                strFieldName = "ModifyTime";
                break;
            case 14:
                strFieldName = "Remark";
                break;
            case 15:
                strFieldName = "Reason";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "ISRECORD":
                return Schema.TYPE_STRING;
            case "ISQUAILTY":
                return Schema.TYPE_STRING;
            case "DONERECORDDATE":
                return Schema.TYPE_DATE;
            case "DONERECORDTIME":
                return Schema.TYPE_STRING;
            case "QUAILTYRESULT":
                return Schema.TYPE_STRING;
            case "QUAILTYDATE":
                return Schema.TYPE_DATE;
            case "QUALITYTIME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "REASON":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DATE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DATE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DATE;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
