/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCInsuredPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCInsuredPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long InsuredID; 
    /** Fk_lccont */
    private long ContID; 
    /** Fk_ldperson */
    private long PersonID; 
    /** Shardingid */
    private String ShardingID; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 被保人客户号 */
    private String InsuredNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 管理机构 */
    private String ManageCom; 
    /** 处理机构 */
    private String ExecuteCom; 
    /** 家庭保障号 */
    private String FamilyID; 
    /** 与主被保人关系 */
    private String RelationToMainInsured; 
    /** 与投保人关系 */
    private String RelationToAppnt; 
    /** 客户地址号码 */
    private String AddressNo; 
    /** 客户内部号码 */
    private String SequenceNo; 
    /** 被保人名称 */
    private String Name; 
    /** 被保人性别 */
    private String Sex; 
    /** 被保人出生日期 */
    private String  Birthday;
    /** 证件类型 */
    private String IDType; 
    /** 证件号码 */
    private String IDNo; 
    /** 国籍 */
    private String NativePlace; 
    /** 民族 */
    private String Nationality; 
    /** 户口所在地 */
    private String RgtAddress; 
    /** 婚姻状况 */
    private String Marriage; 
    /** 结婚日期 */
    private String  MarriageDate;
    /** 健康状况 */
    private String Health; 
    /** 身高 */
    private double Stature; 
    /** 体重 */
    private double Avoirdupois; 
    /** 学历 */
    private String Degree; 
    /** 信用等级 */
    private String CreditGrade; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 银行帐户名 */
    private String AccName; 
    /** 入司日期 */
    private String  JoinCompanyDate;
    /** 参加工作日期 */
    private String  StartWorkDate;
    /** 职位 */
    private String Position; 
    /** 工资 */
    private double Salary; 
    /** 职业类别 */
    private String OccupationType; 
    /** 职业代码 */
    private String OccupationCode; 
    /** 职业（工种） */
    private String WorkType; 
    /** 兼职（工种） */
    private String PluralityType; 
    /** 是否吸烟标志 */
    private String SmokeFlag; 
    /** 保险计划编码 */
    private String ContPlanCode; 
    /** 操作员 */
    private String Operator; 
    /** 被保人状态 */
    private String InsuredStat; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 核保状态 */
    private String UWFlag; 
    /** 最终核保人编码 */
    private String UWCode; 
    /** 核保完成日期 */
    private String  UWDate;
    /** 核保完成时间 */
    private String UWTime; 
    /** 身体指标 */
    private double BMI; 
    /** 被保人数目 */
    private int InsuredPeoples; 
    /** 驾照 */
    private String License; 
    /** 驾照类型 */
    private String LicenseType; 
    /** 团体客户序号 */
    private int CustomerSeqNo; 
    /** 工作号 */
    private String WorkNo; 
    /** 社保登记号 */
    private String SocialInsuNo; 
    /** 职业描述 */
    private String OccupationDesb; 
    /** 证件有效期 */
    private String IdValiDate; 
    /** 是否有摩托车驾照 */
    private String HaveMotorcycleLicence; 
    /** 兼职 */
    private String PartTimeJob; 
    /** 医保标识 */
    private String HealthFlag; 
    /** 在职标识 */
    private String ServiceMark; 
    /** Firstname */
    private String FirstName; 
    /** Lastname */
    private String LastName; 
    /** 社保标记 */
    private String SSFlag; 
    /** 美国纳税人识别号 */
    private String TINNO; 
    /** 是否为美国纳税义务的个人 */
    private String TINFlag; 
    /** 被保险人类型 */
    private String InsuredType; 
    /** 证件是否长期 */
    private String IsLongValid; 
    /** 证件开始日期 */
    private String  IDStartDate;
    private String IsMainInsured;


    public static final int FIELDNUM = 76;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getInsuredID() {
        return InsuredID;
    }
    public void setInsuredID(long aInsuredID) {
        InsuredID = aInsuredID;
    }
    public void setInsuredID(String aInsuredID) {
        if (aInsuredID != null && !aInsuredID.equals("")) {
            InsuredID = new Long(aInsuredID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public long getPersonID() {
        return PersonID;
    }
    public void setPersonID(long aPersonID) {
        PersonID = aPersonID;
    }
    public void setPersonID(String aPersonID) {
        if (aPersonID != null && !aPersonID.equals("")) {
            PersonID = new Long(aPersonID).longValue();
        }
    }

    public String getIsMainInsured() {
        return IsMainInsured;
    }

    public void setIsMainInsured(String isMainInsured) {
        IsMainInsured = isMainInsured;
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getExecuteCom() {
        return ExecuteCom;
    }
    public void setExecuteCom(String aExecuteCom) {
        ExecuteCom = aExecuteCom;
    }
    public String getFamilyID() {
        return FamilyID;
    }
    public void setFamilyID(String aFamilyID) {
        FamilyID = aFamilyID;
    }
    public String getRelationToMainInsured() {
        return RelationToMainInsured;
    }
    public void setRelationToMainInsured(String aRelationToMainInsured) {
        RelationToMainInsured = aRelationToMainInsured;
    }
    public String getRelationToAppnt() {
        return RelationToAppnt;
    }
    public void setRelationToAppnt(String aRelationToAppnt) {
        RelationToAppnt = aRelationToAppnt;
    }
    public String getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }
    public String getSequenceNo() {
        return SequenceNo;
    }
    public void setSequenceNo(String aSequenceNo) {
        SequenceNo = aSequenceNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        return Birthday;
    }
    public void setBirthday(String aBirthday) {
        Birthday = aBirthday;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getMarriageDate() {
        return MarriageDate;
    }
    public void setMarriageDate(String aMarriageDate) {
        MarriageDate = aMarriageDate;
    }
    public String getHealth() {
        return Health;
    }
    public void setHealth(String aHealth) {
        Health = aHealth;
    }
    public double getStature() {
        return Stature;
    }
    public void setStature(double aStature) {
        Stature = aStature;
    }
    public void setStature(String aStature) {
        if (aStature != null && !aStature.equals("")) {
            Double tDouble = new Double(aStature);
            double d = tDouble.doubleValue();
            Stature = d;
        }
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }
    public void setAvoirdupois(double aAvoirdupois) {
        Avoirdupois = aAvoirdupois;
    }
    public void setAvoirdupois(String aAvoirdupois) {
        if (aAvoirdupois != null && !aAvoirdupois.equals("")) {
            Double tDouble = new Double(aAvoirdupois);
            double d = tDouble.doubleValue();
            Avoirdupois = d;
        }
    }

    public String getDegree() {
        return Degree;
    }
    public void setDegree(String aDegree) {
        Degree = aDegree;
    }
    public String getCreditGrade() {
        return CreditGrade;
    }
    public void setCreditGrade(String aCreditGrade) {
        CreditGrade = aCreditGrade;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getJoinCompanyDate() {
        return JoinCompanyDate;
    }
    public void setJoinCompanyDate(String aJoinCompanyDate) {
        JoinCompanyDate = aJoinCompanyDate;
    }
    public String getStartWorkDate() {
        return StartWorkDate;
    }
    public void setStartWorkDate(String aStartWorkDate) {
        StartWorkDate = aStartWorkDate;
    }
    public String getPosition() {
        return Position;
    }
    public void setPosition(String aPosition) {
        Position = aPosition;
    }
    public double getSalary() {
        return Salary;
    }
    public void setSalary(double aSalary) {
        Salary = aSalary;
    }
    public void setSalary(String aSalary) {
        if (aSalary != null && !aSalary.equals("")) {
            Double tDouble = new Double(aSalary);
            double d = tDouble.doubleValue();
            Salary = d;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getWorkType() {
        return WorkType;
    }
    public void setWorkType(String aWorkType) {
        WorkType = aWorkType;
    }
    public String getPluralityType() {
        return PluralityType;
    }
    public void setPluralityType(String aPluralityType) {
        PluralityType = aPluralityType;
    }
    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getContPlanCode() {
        return ContPlanCode;
    }
    public void setContPlanCode(String aContPlanCode) {
        ContPlanCode = aContPlanCode;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getInsuredStat() {
        return InsuredStat;
    }
    public void setInsuredStat(String aInsuredStat) {
        InsuredStat = aInsuredStat;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWCode() {
        return UWCode;
    }
    public void setUWCode(String aUWCode) {
        UWCode = aUWCode;
    }
    public String getUWDate() {
        return UWDate;
    }
    public void setUWDate(String aUWDate) {
        UWDate = aUWDate;
    }
    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public double getBMI() {
        return BMI;
    }
    public void setBMI(double aBMI) {
        BMI = aBMI;
    }
    public void setBMI(String aBMI) {
        if (aBMI != null && !aBMI.equals("")) {
            Double tDouble = new Double(aBMI);
            double d = tDouble.doubleValue();
            BMI = d;
        }
    }

    public int getInsuredPeoples() {
        return InsuredPeoples;
    }
    public void setInsuredPeoples(int aInsuredPeoples) {
        InsuredPeoples = aInsuredPeoples;
    }
    public void setInsuredPeoples(String aInsuredPeoples) {
        if (aInsuredPeoples != null && !aInsuredPeoples.equals("")) {
            Integer tInteger = new Integer(aInsuredPeoples);
            int i = tInteger.intValue();
            InsuredPeoples = i;
        }
    }

    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getLicenseType() {
        return LicenseType;
    }
    public void setLicenseType(String aLicenseType) {
        LicenseType = aLicenseType;
    }
    public int getCustomerSeqNo() {
        return CustomerSeqNo;
    }
    public void setCustomerSeqNo(int aCustomerSeqNo) {
        CustomerSeqNo = aCustomerSeqNo;
    }
    public void setCustomerSeqNo(String aCustomerSeqNo) {
        if (aCustomerSeqNo != null && !aCustomerSeqNo.equals("")) {
            Integer tInteger = new Integer(aCustomerSeqNo);
            int i = tInteger.intValue();
            CustomerSeqNo = i;
        }
    }

    public String getWorkNo() {
        return WorkNo;
    }
    public void setWorkNo(String aWorkNo) {
        WorkNo = aWorkNo;
    }
    public String getSocialInsuNo() {
        return SocialInsuNo;
    }
    public void setSocialInsuNo(String aSocialInsuNo) {
        SocialInsuNo = aSocialInsuNo;
    }
    public String getOccupationDesb() {
        return OccupationDesb;
    }
    public void setOccupationDesb(String aOccupationDesb) {
        OccupationDesb = aOccupationDesb;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }
    public void setHaveMotorcycleLicence(String aHaveMotorcycleLicence) {
        HaveMotorcycleLicence = aHaveMotorcycleLicence;
    }
    public String getPartTimeJob() {
        return PartTimeJob;
    }
    public void setPartTimeJob(String aPartTimeJob) {
        PartTimeJob = aPartTimeJob;
    }
    public String getHealthFlag() {
        return HealthFlag;
    }
    public void setHealthFlag(String aHealthFlag) {
        HealthFlag = aHealthFlag;
    }
    public String getServiceMark() {
        return ServiceMark;
    }
    public void setServiceMark(String aServiceMark) {
        ServiceMark = aServiceMark;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String aFirstName) {
        FirstName = aFirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String aLastName) {
        LastName = aLastName;
    }
    public String getSSFlag() {
        return SSFlag;
    }
    public void setSSFlag(String aSSFlag) {
        SSFlag = aSSFlag;
    }
    public String getTINNO() {
        return TINNO;
    }
    public void setTINNO(String aTINNO) {
        TINNO = aTINNO;
    }
    public String getTINFlag() {
        return TINFlag;
    }
    public void setTINFlag(String aTINFlag) {
        TINFlag = aTINFlag;
    }
    public String getInsuredType() {
        return InsuredType;
    }
    public void setInsuredType(String aInsuredType) {
        InsuredType = aInsuredType;
    }
    public String getIsLongValid() {
        return IsLongValid;
    }
    public void setIsLongValid(String aIsLongValid) {
        IsLongValid = aIsLongValid;
    }
    public String getIDStartDate() {
        return IDStartDate;
    }
    public void setIDStartDate(String aIDStartDate) {
        IDStartDate = aIDStartDate;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsuredID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("PersonID") ) {
            return 2;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 3;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 4;
        }
        if( strFieldName.equals("ContNo") ) {
            return 5;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 6;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 7;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 8;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 9;
        }
        if( strFieldName.equals("ExecuteCom") ) {
            return 10;
        }
        if( strFieldName.equals("FamilyID") ) {
            return 11;
        }
        if( strFieldName.equals("RelationToMainInsured") ) {
            return 12;
        }
        if( strFieldName.equals("RelationToAppnt") ) {
            return 13;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 14;
        }
        if( strFieldName.equals("SequenceNo") ) {
            return 15;
        }
        if( strFieldName.equals("Name") ) {
            return 16;
        }
        if( strFieldName.equals("Sex") ) {
            return 17;
        }
        if( strFieldName.equals("Birthday") ) {
            return 18;
        }
        if( strFieldName.equals("IDType") ) {
            return 19;
        }
        if( strFieldName.equals("IDNo") ) {
            return 20;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 21;
        }
        if( strFieldName.equals("Nationality") ) {
            return 22;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 23;
        }
        if( strFieldName.equals("Marriage") ) {
            return 24;
        }
        if( strFieldName.equals("MarriageDate") ) {
            return 25;
        }
        if( strFieldName.equals("Health") ) {
            return 26;
        }
        if( strFieldName.equals("Stature") ) {
            return 27;
        }
        if( strFieldName.equals("Avoirdupois") ) {
            return 28;
        }
        if( strFieldName.equals("Degree") ) {
            return 29;
        }
        if( strFieldName.equals("CreditGrade") ) {
            return 30;
        }
        if( strFieldName.equals("BankCode") ) {
            return 31;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 32;
        }
        if( strFieldName.equals("AccName") ) {
            return 33;
        }
        if( strFieldName.equals("JoinCompanyDate") ) {
            return 34;
        }
        if( strFieldName.equals("StartWorkDate") ) {
            return 35;
        }
        if( strFieldName.equals("Position") ) {
            return 36;
        }
        if( strFieldName.equals("Salary") ) {
            return 37;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 38;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 39;
        }
        if( strFieldName.equals("WorkType") ) {
            return 40;
        }
        if( strFieldName.equals("PluralityType") ) {
            return 41;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 42;
        }
        if( strFieldName.equals("ContPlanCode") ) {
            return 43;
        }
        if( strFieldName.equals("Operator") ) {
            return 44;
        }
        if( strFieldName.equals("InsuredStat") ) {
            return 45;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 46;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 47;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 48;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 49;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 50;
        }
        if( strFieldName.equals("UWCode") ) {
            return 51;
        }
        if( strFieldName.equals("UWDate") ) {
            return 52;
        }
        if( strFieldName.equals("UWTime") ) {
            return 53;
        }
        if( strFieldName.equals("BMI") ) {
            return 54;
        }
        if( strFieldName.equals("InsuredPeoples") ) {
            return 55;
        }
        if( strFieldName.equals("License") ) {
            return 56;
        }
        if( strFieldName.equals("LicenseType") ) {
            return 57;
        }
        if( strFieldName.equals("CustomerSeqNo") ) {
            return 58;
        }
        if( strFieldName.equals("WorkNo") ) {
            return 59;
        }
        if( strFieldName.equals("SocialInsuNo") ) {
            return 60;
        }
        if( strFieldName.equals("OccupationDesb") ) {
            return 61;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 62;
        }
        if( strFieldName.equals("HaveMotorcycleLicence") ) {
            return 63;
        }
        if( strFieldName.equals("PartTimeJob") ) {
            return 64;
        }
        if( strFieldName.equals("HealthFlag") ) {
            return 65;
        }
        if( strFieldName.equals("ServiceMark") ) {
            return 66;
        }
        if( strFieldName.equals("FirstName") ) {
            return 67;
        }
        if( strFieldName.equals("LastName") ) {
            return 68;
        }
        if( strFieldName.equals("SSFlag") ) {
            return 69;
        }
        if( strFieldName.equals("TINNO") ) {
            return 70;
        }
        if( strFieldName.equals("TINFlag") ) {
            return 71;
        }
        if( strFieldName.equals("InsuredType") ) {
            return 72;
        }
        if( strFieldName.equals("IsLongValid") ) {
            return 73;
        }
        if( strFieldName.equals("IDStartDate") ) {
            return 74;
        }
        if( strFieldName.equals("IsMainInsured") ) {
            return 74;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsuredID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "PersonID";
                break;
            case 3:
                strFieldName = "ShardingID";
                break;
            case 4:
                strFieldName = "GrpContNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "InsuredNo";
                break;
            case 7:
                strFieldName = "PrtNo";
                break;
            case 8:
                strFieldName = "AppntNo";
                break;
            case 9:
                strFieldName = "ManageCom";
                break;
            case 10:
                strFieldName = "ExecuteCom";
                break;
            case 11:
                strFieldName = "FamilyID";
                break;
            case 12:
                strFieldName = "RelationToMainInsured";
                break;
            case 13:
                strFieldName = "RelationToAppnt";
                break;
            case 14:
                strFieldName = "AddressNo";
                break;
            case 15:
                strFieldName = "SequenceNo";
                break;
            case 16:
                strFieldName = "Name";
                break;
            case 17:
                strFieldName = "Sex";
                break;
            case 18:
                strFieldName = "Birthday";
                break;
            case 19:
                strFieldName = "IDType";
                break;
            case 20:
                strFieldName = "IDNo";
                break;
            case 21:
                strFieldName = "NativePlace";
                break;
            case 22:
                strFieldName = "Nationality";
                break;
            case 23:
                strFieldName = "RgtAddress";
                break;
            case 24:
                strFieldName = "Marriage";
                break;
            case 25:
                strFieldName = "MarriageDate";
                break;
            case 26:
                strFieldName = "Health";
                break;
            case 27:
                strFieldName = "Stature";
                break;
            case 28:
                strFieldName = "Avoirdupois";
                break;
            case 29:
                strFieldName = "Degree";
                break;
            case 30:
                strFieldName = "CreditGrade";
                break;
            case 31:
                strFieldName = "BankCode";
                break;
            case 32:
                strFieldName = "BankAccNo";
                break;
            case 33:
                strFieldName = "AccName";
                break;
            case 34:
                strFieldName = "JoinCompanyDate";
                break;
            case 35:
                strFieldName = "StartWorkDate";
                break;
            case 36:
                strFieldName = "Position";
                break;
            case 37:
                strFieldName = "Salary";
                break;
            case 38:
                strFieldName = "OccupationType";
                break;
            case 39:
                strFieldName = "OccupationCode";
                break;
            case 40:
                strFieldName = "WorkType";
                break;
            case 41:
                strFieldName = "PluralityType";
                break;
            case 42:
                strFieldName = "SmokeFlag";
                break;
            case 43:
                strFieldName = "ContPlanCode";
                break;
            case 44:
                strFieldName = "Operator";
                break;
            case 45:
                strFieldName = "InsuredStat";
                break;
            case 46:
                strFieldName = "MakeDate";
                break;
            case 47:
                strFieldName = "MakeTime";
                break;
            case 48:
                strFieldName = "ModifyDate";
                break;
            case 49:
                strFieldName = "ModifyTime";
                break;
            case 50:
                strFieldName = "UWFlag";
                break;
            case 51:
                strFieldName = "UWCode";
                break;
            case 52:
                strFieldName = "UWDate";
                break;
            case 53:
                strFieldName = "UWTime";
                break;
            case 54:
                strFieldName = "BMI";
                break;
            case 55:
                strFieldName = "InsuredPeoples";
                break;
            case 56:
                strFieldName = "License";
                break;
            case 57:
                strFieldName = "LicenseType";
                break;
            case 58:
                strFieldName = "CustomerSeqNo";
                break;
            case 59:
                strFieldName = "WorkNo";
                break;
            case 60:
                strFieldName = "SocialInsuNo";
                break;
            case 61:
                strFieldName = "OccupationDesb";
                break;
            case 62:
                strFieldName = "IdValiDate";
                break;
            case 63:
                strFieldName = "HaveMotorcycleLicence";
                break;
            case 64:
                strFieldName = "PartTimeJob";
                break;
            case 65:
                strFieldName = "HealthFlag";
                break;
            case 66:
                strFieldName = "ServiceMark";
                break;
            case 67:
                strFieldName = "FirstName";
                break;
            case 68:
                strFieldName = "LastName";
                break;
            case 69:
                strFieldName = "SSFlag";
                break;
            case 70:
                strFieldName = "TINNO";
                break;
            case 71:
                strFieldName = "TINFlag";
                break;
            case 72:
                strFieldName = "InsuredType";
                break;
            case 73:
                strFieldName = "IsLongValid";
                break;
            case 74:
                strFieldName = "IDStartDate";
                break;
            case 75:
                strFieldName = "IsMainInsured";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUREDID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "PERSONID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "EXECUTECOM":
                return Schema.TYPE_STRING;
            case "FAMILYID":
                return Schema.TYPE_STRING;
            case "RELATIONTOMAININSURED":
                return Schema.TYPE_STRING;
            case "RELATIONTOAPPNT":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_STRING;
            case "SEQUENCENO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "MARRIAGEDATE":
                return Schema.TYPE_STRING;
            case "HEALTH":
                return Schema.TYPE_STRING;
            case "STATURE":
                return Schema.TYPE_DOUBLE;
            case "AVOIRDUPOIS":
                return Schema.TYPE_DOUBLE;
            case "DEGREE":
                return Schema.TYPE_STRING;
            case "CREDITGRADE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "JOINCOMPANYDATE":
                return Schema.TYPE_STRING;
            case "STARTWORKDATE":
                return Schema.TYPE_STRING;
            case "POSITION":
                return Schema.TYPE_STRING;
            case "SALARY":
                return Schema.TYPE_DOUBLE;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "WORKTYPE":
                return Schema.TYPE_STRING;
            case "PLURALITYTYPE":
                return Schema.TYPE_STRING;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "CONTPLANCODE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "INSUREDSTAT":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWCODE":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_STRING;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "BMI":
                return Schema.TYPE_DOUBLE;
            case "INSUREDPEOPLES":
                return Schema.TYPE_INT;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "LICENSETYPE":
                return Schema.TYPE_STRING;
            case "CUSTOMERSEQNO":
                return Schema.TYPE_INT;
            case "WORKNO":
                return Schema.TYPE_STRING;
            case "SOCIALINSUNO":
                return Schema.TYPE_STRING;
            case "OCCUPATIONDESB":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "HAVEMOTORCYCLELICENCE":
                return Schema.TYPE_STRING;
            case "PARTTIMEJOB":
                return Schema.TYPE_STRING;
            case "HEALTHFLAG":
                return Schema.TYPE_STRING;
            case "SERVICEMARK":
                return Schema.TYPE_STRING;
            case "FIRSTNAME":
                return Schema.TYPE_STRING;
            case "LASTNAME":
                return Schema.TYPE_STRING;
            case "SSFLAG":
                return Schema.TYPE_STRING;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            case "INSUREDTYPE":
                return Schema.TYPE_STRING;
            case "ISLONGVALID":
                return Schema.TYPE_STRING;
            case "IDSTARTDATE":
                return Schema.TYPE_STRING;
            case "IsMainInsured":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_LONG;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_DOUBLE;
            case 28:
                return Schema.TYPE_DOUBLE;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_DOUBLE;
            case 55:
                return Schema.TYPE_INT;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_INT;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsuredID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("PersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PersonID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ExecuteCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
        }
        if (FCode.equalsIgnoreCase("FamilyID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyID));
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToMainInsured));
        }
        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("SequenceNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MarriageDate));
        }
        if (FCode.equalsIgnoreCase("Health")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Health));
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stature));
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Avoirdupois));
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JoinCompanyDate));
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartWorkDate));
        }
        if (FCode.equalsIgnoreCase("Position")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("InsuredStat")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredStat));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWCode));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWDate));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("BMI")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BMI));
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredPeoples));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseType));
        }
        if (FCode.equalsIgnoreCase("CustomerSeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerSeqNo));
        }
        if (FCode.equalsIgnoreCase("WorkNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkNo));
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuNo));
        }
        if (FCode.equalsIgnoreCase("OccupationDesb")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationDesb));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HaveMotorcycleLicence));
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PartTimeJob));
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthFlag));
        }
        if (FCode.equalsIgnoreCase("ServiceMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceMark));
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstName));
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastName));
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SSFlag));
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINNO));
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINFlag));
        }
        if (FCode.equalsIgnoreCase("InsuredType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredType));
        }
        if (FCode.equalsIgnoreCase("IsLongValid")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsLongValid));
        }
        if (FCode.equalsIgnoreCase("IDStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDStartDate));
        }
        if (FCode.equalsIgnoreCase("IsMainInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDStartDate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsuredID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = String.valueOf(PersonID);
                break;
            case 3:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 4:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 6:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 7:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 8:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 9:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 10:
                strFieldValue = String.valueOf(ExecuteCom);
                break;
            case 11:
                strFieldValue = String.valueOf(FamilyID);
                break;
            case 12:
                strFieldValue = String.valueOf(RelationToMainInsured);
                break;
            case 13:
                strFieldValue = String.valueOf(RelationToAppnt);
                break;
            case 14:
                strFieldValue = String.valueOf(AddressNo);
                break;
            case 15:
                strFieldValue = String.valueOf(SequenceNo);
                break;
            case 16:
                strFieldValue = String.valueOf(Name);
                break;
            case 17:
                strFieldValue = String.valueOf(Sex);
                break;
            case 18:
                strFieldValue = String.valueOf(Birthday);
                break;
            case 19:
                strFieldValue = String.valueOf(IDType);
                break;
            case 20:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 21:
                strFieldValue = String.valueOf(NativePlace);
                break;
            case 22:
                strFieldValue = String.valueOf(Nationality);
                break;
            case 23:
                strFieldValue = String.valueOf(RgtAddress);
                break;
            case 24:
                strFieldValue = String.valueOf(Marriage);
                break;
            case 25:
                strFieldValue = String.valueOf(MarriageDate);
                break;
            case 26:
                strFieldValue = String.valueOf(Health);
                break;
            case 27:
                strFieldValue = String.valueOf(Stature);
                break;
            case 28:
                strFieldValue = String.valueOf(Avoirdupois);
                break;
            case 29:
                strFieldValue = String.valueOf(Degree);
                break;
            case 30:
                strFieldValue = String.valueOf(CreditGrade);
                break;
            case 31:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 32:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 33:
                strFieldValue = String.valueOf(AccName);
                break;
            case 34:
                strFieldValue = String.valueOf(JoinCompanyDate);
                break;
            case 35:
                strFieldValue = String.valueOf(StartWorkDate);
                break;
            case 36:
                strFieldValue = String.valueOf(Position);
                break;
            case 37:
                strFieldValue = String.valueOf(Salary);
                break;
            case 38:
                strFieldValue = String.valueOf(OccupationType);
                break;
            case 39:
                strFieldValue = String.valueOf(OccupationCode);
                break;
            case 40:
                strFieldValue = String.valueOf(WorkType);
                break;
            case 41:
                strFieldValue = String.valueOf(PluralityType);
                break;
            case 42:
                strFieldValue = String.valueOf(SmokeFlag);
                break;
            case 43:
                strFieldValue = String.valueOf(ContPlanCode);
                break;
            case 44:
                strFieldValue = String.valueOf(Operator);
                break;
            case 45:
                strFieldValue = String.valueOf(InsuredStat);
                break;
            case 46:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 47:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 48:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 49:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 50:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 51:
                strFieldValue = String.valueOf(UWCode);
                break;
            case 52:
                strFieldValue = String.valueOf(UWDate);
                break;
            case 53:
                strFieldValue = String.valueOf(UWTime);
                break;
            case 54:
                strFieldValue = String.valueOf(BMI);
                break;
            case 55:
                strFieldValue = String.valueOf(InsuredPeoples);
                break;
            case 56:
                strFieldValue = String.valueOf(License);
                break;
            case 57:
                strFieldValue = String.valueOf(LicenseType);
                break;
            case 58:
                strFieldValue = String.valueOf(CustomerSeqNo);
                break;
            case 59:
                strFieldValue = String.valueOf(WorkNo);
                break;
            case 60:
                strFieldValue = String.valueOf(SocialInsuNo);
                break;
            case 61:
                strFieldValue = String.valueOf(OccupationDesb);
                break;
            case 62:
                strFieldValue = String.valueOf(IdValiDate);
                break;
            case 63:
                strFieldValue = String.valueOf(HaveMotorcycleLicence);
                break;
            case 64:
                strFieldValue = String.valueOf(PartTimeJob);
                break;
            case 65:
                strFieldValue = String.valueOf(HealthFlag);
                break;
            case 66:
                strFieldValue = String.valueOf(ServiceMark);
                break;
            case 67:
                strFieldValue = String.valueOf(FirstName);
                break;
            case 68:
                strFieldValue = String.valueOf(LastName);
                break;
            case 69:
                strFieldValue = String.valueOf(SSFlag);
                break;
            case 70:
                strFieldValue = String.valueOf(TINNO);
                break;
            case 71:
                strFieldValue = String.valueOf(TINFlag);
                break;
            case 72:
                strFieldValue = String.valueOf(InsuredType);
                break;
            case 73:
                strFieldValue = String.valueOf(IsLongValid);
                break;
            case 74:
                strFieldValue = String.valueOf(IDStartDate);
                break;
            case 75:
                strFieldValue = String.valueOf(IsMainInsured);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsuredID")) {
            if( FValue != null && !FValue.equals("")) {
                InsuredID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("PersonID")) {
            if( FValue != null && !FValue.equals("")) {
                PersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteCom = FValue.trim();
            }
            else
                ExecuteCom = null;
        }
        if (FCode.equalsIgnoreCase("FamilyID")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyID = FValue.trim();
            }
            else
                FamilyID = null;
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToMainInsured = FValue.trim();
            }
            else
                RelationToMainInsured = null;
        }
        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToAppnt = FValue.trim();
            }
            else
                RelationToAppnt = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddressNo = FValue.trim();
            }
            else
                AddressNo = null;
        }
        if (FCode.equalsIgnoreCase("SequenceNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SequenceNo = FValue.trim();
            }
            else
                SequenceNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                Birthday = FValue.trim();
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MarriageDate = FValue.trim();
            }
            else
                MarriageDate = null;
        }
        if (FCode.equalsIgnoreCase("Health")) {
            if( FValue != null && !FValue.equals(""))
            {
                Health = FValue.trim();
            }
            else
                Health = null;
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Stature = d;
            }
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Avoirdupois = d;
            }
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            if( FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
                Degree = null;
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
                CreditGrade = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JoinCompanyDate = FValue.trim();
            }
            else
                JoinCompanyDate = null;
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartWorkDate = FValue.trim();
            }
            else
                StartWorkDate = null;
        }
        if (FCode.equalsIgnoreCase("Position")) {
            if( FValue != null && !FValue.equals(""))
            {
                Position = FValue.trim();
            }
            else
                Position = null;
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Salary = d;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkType = FValue.trim();
            }
            else
                WorkType = null;
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
                PluralityType = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
                ContPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("InsuredStat")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredStat = FValue.trim();
            }
            else
                InsuredStat = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWCode = FValue.trim();
            }
            else
                UWCode = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWDate = FValue.trim();
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("BMI")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BMI = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuredPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseType = FValue.trim();
            }
            else
                LicenseType = null;
        }
        if (FCode.equalsIgnoreCase("CustomerSeqNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                CustomerSeqNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("WorkNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkNo = FValue.trim();
            }
            else
                WorkNo = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuNo = FValue.trim();
            }
            else
                SocialInsuNo = null;
        }
        if (FCode.equalsIgnoreCase("OccupationDesb")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationDesb = FValue.trim();
            }
            else
                OccupationDesb = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            if( FValue != null && !FValue.equals(""))
            {
                HaveMotorcycleLicence = FValue.trim();
            }
            else
                HaveMotorcycleLicence = null;
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            if( FValue != null && !FValue.equals(""))
            {
                PartTimeJob = FValue.trim();
            }
            else
                PartTimeJob = null;
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthFlag = FValue.trim();
            }
            else
                HealthFlag = null;
        }
        if (FCode.equalsIgnoreCase("ServiceMark")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceMark = FValue.trim();
            }
            else
                ServiceMark = null;
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstName = FValue.trim();
            }
            else
                FirstName = null;
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastName = FValue.trim();
            }
            else
                LastName = null;
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SSFlag = FValue.trim();
            }
            else
                SSFlag = null;
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINNO = FValue.trim();
            }
            else
                TINNO = null;
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINFlag = FValue.trim();
            }
            else
                TINFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuredType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredType = FValue.trim();
            }
            else
                InsuredType = null;
        }
        if (FCode.equalsIgnoreCase("IsLongValid")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsLongValid = FValue.trim();
            }
            else
                IsLongValid = null;
        }
        if (FCode.equalsIgnoreCase("IDStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDStartDate = FValue.trim();
            }
            else
                IDStartDate = null;
        }
        if (FCode.equalsIgnoreCase("IsMainInsured")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsMainInsured = FValue.trim();
            }
            else
                IsMainInsured = null;
        }
        return true;
    }


    public String toString() {
    return "LCInsuredPojo [" +
            "InsuredID="+InsuredID +
            ", ContID="+ContID +
            ", PersonID="+PersonID +
            ", ShardingID="+ShardingID +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", InsuredNo="+InsuredNo +
            ", PrtNo="+PrtNo +
            ", AppntNo="+AppntNo +
            ", ManageCom="+ManageCom +
            ", ExecuteCom="+ExecuteCom +
            ", FamilyID="+FamilyID +
            ", RelationToMainInsured="+RelationToMainInsured +
            ", RelationToAppnt="+RelationToAppnt +
            ", AddressNo="+AddressNo +
            ", SequenceNo="+SequenceNo +
            ", Name="+Name +
            ", Sex="+Sex +
            ", Birthday="+Birthday +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", NativePlace="+NativePlace +
            ", Nationality="+Nationality +
            ", RgtAddress="+RgtAddress +
            ", Marriage="+Marriage +
            ", MarriageDate="+MarriageDate +
            ", Health="+Health +
            ", Stature="+Stature +
            ", Avoirdupois="+Avoirdupois +
            ", Degree="+Degree +
            ", CreditGrade="+CreditGrade +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", AccName="+AccName +
            ", JoinCompanyDate="+JoinCompanyDate +
            ", StartWorkDate="+StartWorkDate +
            ", Position="+Position +
            ", Salary="+Salary +
            ", OccupationType="+OccupationType +
            ", OccupationCode="+OccupationCode +
            ", WorkType="+WorkType +
            ", PluralityType="+PluralityType +
            ", SmokeFlag="+SmokeFlag +
            ", ContPlanCode="+ContPlanCode +
            ", Operator="+Operator +
            ", InsuredStat="+InsuredStat +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", UWFlag="+UWFlag +
            ", UWCode="+UWCode +
            ", UWDate="+UWDate +
            ", UWTime="+UWTime +
            ", BMI="+BMI +
            ", InsuredPeoples="+InsuredPeoples +
            ", License="+License +
            ", LicenseType="+LicenseType +
            ", CustomerSeqNo="+CustomerSeqNo +
            ", WorkNo="+WorkNo +
            ", SocialInsuNo="+SocialInsuNo +
            ", OccupationDesb="+OccupationDesb +
            ", IdValiDate="+IdValiDate +
            ", HaveMotorcycleLicence="+HaveMotorcycleLicence +
            ", PartTimeJob="+PartTimeJob +
            ", HealthFlag="+HealthFlag +
            ", ServiceMark="+ServiceMark +
            ", FirstName="+FirstName +
            ", LastName="+LastName +
            ", SSFlag="+SSFlag +
            ", TINNO="+TINNO +
            ", TINFlag="+TINFlag +
            ", InsuredType="+InsuredType +
            ", IsLongValid="+IsLongValid +
            ", IsMainInsured="+IsMainInsured +
            ", IDStartDate="+IDStartDate +"]";
    }
}
