/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBContPlanDetailDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBContPlanDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBContPlanDetailSchema implements Schema, Cloneable {
    // @Field
    /** 保单号码 */
    private String PolicyNo;
    /** 投保单号 */
    private String PropNo;
    /** 系统方案编码 */
    private String SysPlanCode;
    /** 方案编码 */
    private String PlanCode;
    /** 险种编码 */
    private String RiskCode;
    /** 责任编码 */
    private String DutyCode;
    /** 保额类型 */
    private String AmntType;
    /** 固定保额 */
    private String FixedAmnt;
    /** 月薪倍数 */
    private String SalaryMult;
    /** 最高保额 */
    private String MaxAmnt;
    /** 最低保额 */
    private String MinAmnt;
    /** 期望保费类型 */
    private String ExceptPremType;
    /** 期望保费/费率/折扣 */
    private String ExceptPrem;
    /** 参考保费/费率 */
    private String StandValue;
    /** 初始保费 */
    private String InitPrem;
    /** 预期收益率 */
    private String ExceptYield;
    /** 最终值 */
    private String FinalValue;
    /** 变更类型 */
    private String ChangeType;
    /** 主附共用标识 */
    private String RelaShareFlag;
    /** 备注 */
    private String Remark;
    /** 入机操作员 */
    private String MakeOperator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改操作员 */
    private String ModifyOperator;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 26;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBContPlanDetailSchema() {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "PolicyNo";
        pk[1] = "SysPlanCode";
        pk[2] = "PlanCode";
        pk[3] = "RiskCode";
        pk[4] = "DutyCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBContPlanDetailSchema cloned = (LBContPlanDetailSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }
    public void setPolicyNo(String aPolicyNo) {
        PolicyNo = aPolicyNo;
    }
    public String getPropNo() {
        return PropNo;
    }
    public void setPropNo(String aPropNo) {
        PropNo = aPropNo;
    }
    public String getSysPlanCode() {
        return SysPlanCode;
    }
    public void setSysPlanCode(String aSysPlanCode) {
        SysPlanCode = aSysPlanCode;
    }
    public String getPlanCode() {
        return PlanCode;
    }
    public void setPlanCode(String aPlanCode) {
        PlanCode = aPlanCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getAmntType() {
        return AmntType;
    }
    public void setAmntType(String aAmntType) {
        AmntType = aAmntType;
    }
    public String getFixedAmnt() {
        return FixedAmnt;
    }
    public void setFixedAmnt(String aFixedAmnt) {
        FixedAmnt = aFixedAmnt;
    }
    public String getSalaryMult() {
        return SalaryMult;
    }
    public void setSalaryMult(String aSalaryMult) {
        SalaryMult = aSalaryMult;
    }
    public String getMaxAmnt() {
        return MaxAmnt;
    }
    public void setMaxAmnt(String aMaxAmnt) {
        MaxAmnt = aMaxAmnt;
    }
    public String getMinAmnt() {
        return MinAmnt;
    }
    public void setMinAmnt(String aMinAmnt) {
        MinAmnt = aMinAmnt;
    }
    public String getExceptPremType() {
        return ExceptPremType;
    }
    public void setExceptPremType(String aExceptPremType) {
        ExceptPremType = aExceptPremType;
    }
    public String getExceptPrem() {
        return ExceptPrem;
    }
    public void setExceptPrem(String aExceptPrem) {
        ExceptPrem = aExceptPrem;
    }
    public String getStandValue() {
        return StandValue;
    }
    public void setStandValue(String aStandValue) {
        StandValue = aStandValue;
    }
    public String getInitPrem() {
        return InitPrem;
    }
    public void setInitPrem(String aInitPrem) {
        InitPrem = aInitPrem;
    }
    public String getExceptYield() {
        return ExceptYield;
    }
    public void setExceptYield(String aExceptYield) {
        ExceptYield = aExceptYield;
    }
    public String getFinalValue() {
        return FinalValue;
    }
    public void setFinalValue(String aFinalValue) {
        FinalValue = aFinalValue;
    }
    public String getChangeType() {
        return ChangeType;
    }
    public void setChangeType(String aChangeType) {
        ChangeType = aChangeType;
    }
    public String getRelaShareFlag() {
        return RelaShareFlag;
    }
    public void setRelaShareFlag(String aRelaShareFlag) {
        RelaShareFlag = aRelaShareFlag;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getMakeOperator() {
        return MakeOperator;
    }
    public void setMakeOperator(String aMakeOperator) {
        MakeOperator = aMakeOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 使用另外一个 LBContPlanDetailSchema 对象给 Schema 赋值
    * @param: aLBContPlanDetailSchema LBContPlanDetailSchema
    **/
    public void setSchema(LBContPlanDetailSchema aLBContPlanDetailSchema) {
        this.PolicyNo = aLBContPlanDetailSchema.getPolicyNo();
        this.PropNo = aLBContPlanDetailSchema.getPropNo();
        this.SysPlanCode = aLBContPlanDetailSchema.getSysPlanCode();
        this.PlanCode = aLBContPlanDetailSchema.getPlanCode();
        this.RiskCode = aLBContPlanDetailSchema.getRiskCode();
        this.DutyCode = aLBContPlanDetailSchema.getDutyCode();
        this.AmntType = aLBContPlanDetailSchema.getAmntType();
        this.FixedAmnt = aLBContPlanDetailSchema.getFixedAmnt();
        this.SalaryMult = aLBContPlanDetailSchema.getSalaryMult();
        this.MaxAmnt = aLBContPlanDetailSchema.getMaxAmnt();
        this.MinAmnt = aLBContPlanDetailSchema.getMinAmnt();
        this.ExceptPremType = aLBContPlanDetailSchema.getExceptPremType();
        this.ExceptPrem = aLBContPlanDetailSchema.getExceptPrem();
        this.StandValue = aLBContPlanDetailSchema.getStandValue();
        this.InitPrem = aLBContPlanDetailSchema.getInitPrem();
        this.ExceptYield = aLBContPlanDetailSchema.getExceptYield();
        this.FinalValue = aLBContPlanDetailSchema.getFinalValue();
        this.ChangeType = aLBContPlanDetailSchema.getChangeType();
        this.RelaShareFlag = aLBContPlanDetailSchema.getRelaShareFlag();
        this.Remark = aLBContPlanDetailSchema.getRemark();
        this.MakeOperator = aLBContPlanDetailSchema.getMakeOperator();
        this.MakeDate = fDate.getDate( aLBContPlanDetailSchema.getMakeDate());
        this.MakeTime = aLBContPlanDetailSchema.getMakeTime();
        this.ModifyOperator = aLBContPlanDetailSchema.getModifyOperator();
        this.ModifyDate = fDate.getDate( aLBContPlanDetailSchema.getModifyDate());
        this.ModifyTime = aLBContPlanDetailSchema.getModifyTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("PolicyNo") == null )
                this.PolicyNo = null;
            else
                this.PolicyNo = rs.getString("PolicyNo").trim();

            if( rs.getString("PropNo") == null )
                this.PropNo = null;
            else
                this.PropNo = rs.getString("PropNo").trim();

            if( rs.getString("SysPlanCode") == null )
                this.SysPlanCode = null;
            else
                this.SysPlanCode = rs.getString("SysPlanCode").trim();

            if( rs.getString("PlanCode") == null )
                this.PlanCode = null;
            else
                this.PlanCode = rs.getString("PlanCode").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("DutyCode") == null )
                this.DutyCode = null;
            else
                this.DutyCode = rs.getString("DutyCode").trim();

            if( rs.getString("AmntType") == null )
                this.AmntType = null;
            else
                this.AmntType = rs.getString("AmntType").trim();

            if( rs.getString("FixedAmnt") == null )
                this.FixedAmnt = null;
            else
                this.FixedAmnt = rs.getString("FixedAmnt").trim();

            if( rs.getString("SalaryMult") == null )
                this.SalaryMult = null;
            else
                this.SalaryMult = rs.getString("SalaryMult").trim();

            if( rs.getString("MaxAmnt") == null )
                this.MaxAmnt = null;
            else
                this.MaxAmnt = rs.getString("MaxAmnt").trim();

            if( rs.getString("MinAmnt") == null )
                this.MinAmnt = null;
            else
                this.MinAmnt = rs.getString("MinAmnt").trim();

            if( rs.getString("ExceptPremType") == null )
                this.ExceptPremType = null;
            else
                this.ExceptPremType = rs.getString("ExceptPremType").trim();

            if( rs.getString("ExceptPrem") == null )
                this.ExceptPrem = null;
            else
                this.ExceptPrem = rs.getString("ExceptPrem").trim();

            if( rs.getString("StandValue") == null )
                this.StandValue = null;
            else
                this.StandValue = rs.getString("StandValue").trim();

            if( rs.getString("InitPrem") == null )
                this.InitPrem = null;
            else
                this.InitPrem = rs.getString("InitPrem").trim();

            if( rs.getString("ExceptYield") == null )
                this.ExceptYield = null;
            else
                this.ExceptYield = rs.getString("ExceptYield").trim();

            if( rs.getString("FinalValue") == null )
                this.FinalValue = null;
            else
                this.FinalValue = rs.getString("FinalValue").trim();

            if( rs.getString("ChangeType") == null )
                this.ChangeType = null;
            else
                this.ChangeType = rs.getString("ChangeType").trim();

            if( rs.getString("RelaShareFlag") == null )
                this.RelaShareFlag = null;
            else
                this.RelaShareFlag = rs.getString("RelaShareFlag").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("MakeOperator") == null )
                this.MakeOperator = null;
            else
                this.MakeOperator = rs.getString("MakeOperator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            if( rs.getString("ModifyOperator") == null )
                this.ModifyOperator = null;
            else
                this.ModifyOperator = rs.getString("ModifyOperator").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBContPlanDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBContPlanDetailSchema getSchema() {
        LBContPlanDetailSchema aLBContPlanDetailSchema = new LBContPlanDetailSchema();
        aLBContPlanDetailSchema.setSchema(this);
        return aLBContPlanDetailSchema;
    }

    public LBContPlanDetailDB getDB() {
        LBContPlanDetailDB aDBOper = new LBContPlanDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBContPlanDetail描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PropNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SysPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AmntType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FixedAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SalaryMult)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MaxAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MinAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExceptPremType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExceptPrem)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandValue)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InitPrem)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExceptYield)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FinalValue)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChangeType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelaShareFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBContPlanDetail>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            PolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            PropNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            SysPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            PlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            AmntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            FixedAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            SalaryMult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            MaxAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            MinAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            ExceptPremType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ExceptPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            StandValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            InitPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            ExceptYield = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            FinalValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ChangeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            RelaShareFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            MakeOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            ModifyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBContPlanDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
        }
        if (FCode.equalsIgnoreCase("PropNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PropNo));
        }
        if (FCode.equalsIgnoreCase("SysPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SysPlanCode));
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("AmntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntType));
        }
        if (FCode.equalsIgnoreCase("FixedAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FixedAmnt));
        }
        if (FCode.equalsIgnoreCase("SalaryMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SalaryMult));
        }
        if (FCode.equalsIgnoreCase("MaxAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAmnt));
        }
        if (FCode.equalsIgnoreCase("MinAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAmnt));
        }
        if (FCode.equalsIgnoreCase("ExceptPremType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExceptPremType));
        }
        if (FCode.equalsIgnoreCase("ExceptPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExceptPrem));
        }
        if (FCode.equalsIgnoreCase("StandValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandValue));
        }
        if (FCode.equalsIgnoreCase("InitPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InitPrem));
        }
        if (FCode.equalsIgnoreCase("ExceptYield")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExceptYield));
        }
        if (FCode.equalsIgnoreCase("FinalValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FinalValue));
        }
        if (FCode.equalsIgnoreCase("ChangeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChangeType));
        }
        if (FCode.equalsIgnoreCase("RelaShareFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaShareFlag));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeOperator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PolicyNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PropNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SysPlanCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PlanCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AmntType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(FixedAmnt);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(SalaryMult);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MaxAmnt);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(MinAmnt);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ExceptPremType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ExceptPrem);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(StandValue);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(InitPrem);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ExceptYield);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(FinalValue);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ChangeType);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(RelaShareFlag);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(MakeOperator);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ModifyOperator);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyNo = FValue.trim();
            }
            else
                PolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("PropNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PropNo = FValue.trim();
            }
            else
                PropNo = null;
        }
        if (FCode.equalsIgnoreCase("SysPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SysPlanCode = FValue.trim();
            }
            else
                SysPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
                PlanCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("AmntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntType = FValue.trim();
            }
            else
                AmntType = null;
        }
        if (FCode.equalsIgnoreCase("FixedAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                FixedAmnt = FValue.trim();
            }
            else
                FixedAmnt = null;
        }
        if (FCode.equalsIgnoreCase("SalaryMult")) {
            if( FValue != null && !FValue.equals(""))
            {
                SalaryMult = FValue.trim();
            }
            else
                SalaryMult = null;
        }
        if (FCode.equalsIgnoreCase("MaxAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                MaxAmnt = FValue.trim();
            }
            else
                MaxAmnt = null;
        }
        if (FCode.equalsIgnoreCase("MinAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                MinAmnt = FValue.trim();
            }
            else
                MinAmnt = null;
        }
        if (FCode.equalsIgnoreCase("ExceptPremType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExceptPremType = FValue.trim();
            }
            else
                ExceptPremType = null;
        }
        if (FCode.equalsIgnoreCase("ExceptPrem")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExceptPrem = FValue.trim();
            }
            else
                ExceptPrem = null;
        }
        if (FCode.equalsIgnoreCase("StandValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandValue = FValue.trim();
            }
            else
                StandValue = null;
        }
        if (FCode.equalsIgnoreCase("InitPrem")) {
            if( FValue != null && !FValue.equals(""))
            {
                InitPrem = FValue.trim();
            }
            else
                InitPrem = null;
        }
        if (FCode.equalsIgnoreCase("ExceptYield")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExceptYield = FValue.trim();
            }
            else
                ExceptYield = null;
        }
        if (FCode.equalsIgnoreCase("FinalValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                FinalValue = FValue.trim();
            }
            else
                FinalValue = null;
        }
        if (FCode.equalsIgnoreCase("ChangeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChangeType = FValue.trim();
            }
            else
                ChangeType = null;
        }
        if (FCode.equalsIgnoreCase("RelaShareFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelaShareFlag = FValue.trim();
            }
            else
                RelaShareFlag = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeOperator = FValue.trim();
            }
            else
                MakeOperator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBContPlanDetailSchema other = (LBContPlanDetailSchema)otherObject;
        return
            PolicyNo.equals(other.getPolicyNo())
            && PropNo.equals(other.getPropNo())
            && SysPlanCode.equals(other.getSysPlanCode())
            && PlanCode.equals(other.getPlanCode())
            && RiskCode.equals(other.getRiskCode())
            && DutyCode.equals(other.getDutyCode())
            && AmntType.equals(other.getAmntType())
            && FixedAmnt.equals(other.getFixedAmnt())
            && SalaryMult.equals(other.getSalaryMult())
            && MaxAmnt.equals(other.getMaxAmnt())
            && MinAmnt.equals(other.getMinAmnt())
            && ExceptPremType.equals(other.getExceptPremType())
            && ExceptPrem.equals(other.getExceptPrem())
            && StandValue.equals(other.getStandValue())
            && InitPrem.equals(other.getInitPrem())
            && ExceptYield.equals(other.getExceptYield())
            && FinalValue.equals(other.getFinalValue())
            && ChangeType.equals(other.getChangeType())
            && RelaShareFlag.equals(other.getRelaShareFlag())
            && Remark.equals(other.getRemark())
            && MakeOperator.equals(other.getMakeOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && ModifyOperator.equals(other.getModifyOperator())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PolicyNo") ) {
            return 0;
        }
        if( strFieldName.equals("PropNo") ) {
            return 1;
        }
        if( strFieldName.equals("SysPlanCode") ) {
            return 2;
        }
        if( strFieldName.equals("PlanCode") ) {
            return 3;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 4;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 5;
        }
        if( strFieldName.equals("AmntType") ) {
            return 6;
        }
        if( strFieldName.equals("FixedAmnt") ) {
            return 7;
        }
        if( strFieldName.equals("SalaryMult") ) {
            return 8;
        }
        if( strFieldName.equals("MaxAmnt") ) {
            return 9;
        }
        if( strFieldName.equals("MinAmnt") ) {
            return 10;
        }
        if( strFieldName.equals("ExceptPremType") ) {
            return 11;
        }
        if( strFieldName.equals("ExceptPrem") ) {
            return 12;
        }
        if( strFieldName.equals("StandValue") ) {
            return 13;
        }
        if( strFieldName.equals("InitPrem") ) {
            return 14;
        }
        if( strFieldName.equals("ExceptYield") ) {
            return 15;
        }
        if( strFieldName.equals("FinalValue") ) {
            return 16;
        }
        if( strFieldName.equals("ChangeType") ) {
            return 17;
        }
        if( strFieldName.equals("RelaShareFlag") ) {
            return 18;
        }
        if( strFieldName.equals("Remark") ) {
            return 19;
        }
        if( strFieldName.equals("MakeOperator") ) {
            return 20;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 21;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PolicyNo";
                break;
            case 1:
                strFieldName = "PropNo";
                break;
            case 2:
                strFieldName = "SysPlanCode";
                break;
            case 3:
                strFieldName = "PlanCode";
                break;
            case 4:
                strFieldName = "RiskCode";
                break;
            case 5:
                strFieldName = "DutyCode";
                break;
            case 6:
                strFieldName = "AmntType";
                break;
            case 7:
                strFieldName = "FixedAmnt";
                break;
            case 8:
                strFieldName = "SalaryMult";
                break;
            case 9:
                strFieldName = "MaxAmnt";
                break;
            case 10:
                strFieldName = "MinAmnt";
                break;
            case 11:
                strFieldName = "ExceptPremType";
                break;
            case 12:
                strFieldName = "ExceptPrem";
                break;
            case 13:
                strFieldName = "StandValue";
                break;
            case 14:
                strFieldName = "InitPrem";
                break;
            case 15:
                strFieldName = "ExceptYield";
                break;
            case 16:
                strFieldName = "FinalValue";
                break;
            case 17:
                strFieldName = "ChangeType";
                break;
            case 18:
                strFieldName = "RelaShareFlag";
                break;
            case 19:
                strFieldName = "Remark";
                break;
            case 20:
                strFieldName = "MakeOperator";
                break;
            case 21:
                strFieldName = "MakeDate";
                break;
            case 22:
                strFieldName = "MakeTime";
                break;
            case 23:
                strFieldName = "ModifyOperator";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "PROPNO":
                return Schema.TYPE_STRING;
            case "SYSPLANCODE":
                return Schema.TYPE_STRING;
            case "PLANCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "AMNTTYPE":
                return Schema.TYPE_STRING;
            case "FIXEDAMNT":
                return Schema.TYPE_STRING;
            case "SALARYMULT":
                return Schema.TYPE_STRING;
            case "MAXAMNT":
                return Schema.TYPE_STRING;
            case "MINAMNT":
                return Schema.TYPE_STRING;
            case "EXCEPTPREMTYPE":
                return Schema.TYPE_STRING;
            case "EXCEPTPREM":
                return Schema.TYPE_STRING;
            case "STANDVALUE":
                return Schema.TYPE_STRING;
            case "INITPREM":
                return Schema.TYPE_STRING;
            case "EXCEPTYIELD":
                return Schema.TYPE_STRING;
            case "FINALVALUE":
                return Schema.TYPE_STRING;
            case "CHANGETYPE":
                return Schema.TYPE_STRING;
            case "RELASHAREFLAG":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "MAKEOPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_DATE;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
