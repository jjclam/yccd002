/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCProdSetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCProdSetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LCProdSetSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long ProdSetID;
    /** Shardingid */
    private String ShardingID;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 产品组合编码 */
    private String ProdSetCode;
    /** 产品组合名称 */
    private String ProdSetName;
    /** 产品组合份数 */
    private double ProdSetMult;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** Standbyflag1 */
    private String StandByFlag1;
    /** Standbyflag2 */
    private String StandByFlag2;
    /** Standbyflag3 */
    private Date StandByFlag3;
    /** Standbyflag4 */
    private Date StandByFlag4;
    /** Standbyflag5 */
    private double StandByFlag5;
    /** Standbyflag6 */
    private double StandByFlag6;
    /** 保险年龄年期 */
    private int InsuYear;
    /** 保险年龄年期标志 */
    private String InsuYearFlag;

    public static final int FIELDNUM = 22;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCProdSetSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ProdSetID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCProdSetSchema cloned = (LCProdSetSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getProdSetID() {
        return ProdSetID;
    }
    public void setProdSetID(long aProdSetID) {
        ProdSetID = aProdSetID;
    }
    public void setProdSetID(String aProdSetID) {
        if (aProdSetID != null && !aProdSetID.equals("")) {
            ProdSetID = new Long(aProdSetID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getProdSetName() {
        return ProdSetName;
    }
    public void setProdSetName(String aProdSetName) {
        ProdSetName = aProdSetName;
    }
    public double getProdSetMult() {
        return ProdSetMult;
    }
    public void setProdSetMult(double aProdSetMult) {
        ProdSetMult = aProdSetMult;
    }
    public void setProdSetMult(String aProdSetMult) {
        if (aProdSetMult != null && !aProdSetMult.equals("")) {
            Double tDouble = new Double(aProdSetMult);
            double d = tDouble.doubleValue();
            ProdSetMult = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getStandByFlag2() {
        return StandByFlag2;
    }
    public void setStandByFlag2(String aStandByFlag2) {
        StandByFlag2 = aStandByFlag2;
    }
    public String getStandByFlag3() {
        if(StandByFlag3 != null) {
            return fDate.getString(StandByFlag3);
        } else {
            return null;
        }
    }
    public void setStandByFlag3(Date aStandByFlag3) {
        StandByFlag3 = aStandByFlag3;
    }
    public void setStandByFlag3(String aStandByFlag3) {
        if (aStandByFlag3 != null && !aStandByFlag3.equals("")) {
            StandByFlag3 = fDate.getDate(aStandByFlag3);
        } else
            StandByFlag3 = null;
    }

    public String getStandByFlag4() {
        if(StandByFlag4 != null) {
            return fDate.getString(StandByFlag4);
        } else {
            return null;
        }
    }
    public void setStandByFlag4(Date aStandByFlag4) {
        StandByFlag4 = aStandByFlag4;
    }
    public void setStandByFlag4(String aStandByFlag4) {
        if (aStandByFlag4 != null && !aStandByFlag4.equals("")) {
            StandByFlag4 = fDate.getDate(aStandByFlag4);
        } else
            StandByFlag4 = null;
    }

    public double getStandByFlag5() {
        return StandByFlag5;
    }
    public void setStandByFlag5(double aStandByFlag5) {
        StandByFlag5 = aStandByFlag5;
    }
    public void setStandByFlag5(String aStandByFlag5) {
        if (aStandByFlag5 != null && !aStandByFlag5.equals("")) {
            Double tDouble = new Double(aStandByFlag5);
            double d = tDouble.doubleValue();
            StandByFlag5 = d;
        }
    }

    public double getStandByFlag6() {
        return StandByFlag6;
    }
    public void setStandByFlag6(double aStandByFlag6) {
        StandByFlag6 = aStandByFlag6;
    }
    public void setStandByFlag6(String aStandByFlag6) {
        if (aStandByFlag6 != null && !aStandByFlag6.equals("")) {
            Double tDouble = new Double(aStandByFlag6);
            double d = tDouble.doubleValue();
            StandByFlag6 = d;
        }
    }

    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }

    /**
    * 使用另外一个 LCProdSetSchema 对象给 Schema 赋值
    * @param: aLCProdSetSchema LCProdSetSchema
    **/
    public void setSchema(LCProdSetSchema aLCProdSetSchema) {
        this.ProdSetID = aLCProdSetSchema.getProdSetID();
        this.ShardingID = aLCProdSetSchema.getShardingID();
        this.GrpContNo = aLCProdSetSchema.getGrpContNo();
        this.ContNo = aLCProdSetSchema.getContNo();
        this.ProposalContNo = aLCProdSetSchema.getProposalContNo();
        this.PrtNo = aLCProdSetSchema.getPrtNo();
        this.ProdSetCode = aLCProdSetSchema.getProdSetCode();
        this.ProdSetName = aLCProdSetSchema.getProdSetName();
        this.ProdSetMult = aLCProdSetSchema.getProdSetMult();
        this.Operator = aLCProdSetSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCProdSetSchema.getMakeDate());
        this.MakeTime = aLCProdSetSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCProdSetSchema.getModifyDate());
        this.ModifyTime = aLCProdSetSchema.getModifyTime();
        this.StandByFlag1 = aLCProdSetSchema.getStandByFlag1();
        this.StandByFlag2 = aLCProdSetSchema.getStandByFlag2();
        this.StandByFlag3 = fDate.getDate( aLCProdSetSchema.getStandByFlag3());
        this.StandByFlag4 = fDate.getDate( aLCProdSetSchema.getStandByFlag4());
        this.StandByFlag5 = aLCProdSetSchema.getStandByFlag5();
        this.StandByFlag6 = aLCProdSetSchema.getStandByFlag6();
        this.InsuYear = aLCProdSetSchema.getInsuYear();
        this.InsuYearFlag = aLCProdSetSchema.getInsuYearFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.ProdSetID = rs.getLong("ProdSetID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("ProposalContNo") == null )
                this.ProposalContNo = null;
            else
                this.ProposalContNo = rs.getString("ProposalContNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("ProdSetCode") == null )
                this.ProdSetCode = null;
            else
                this.ProdSetCode = rs.getString("ProdSetCode").trim();

            if( rs.getString("ProdSetName") == null )
                this.ProdSetName = null;
            else
                this.ProdSetName = rs.getString("ProdSetName").trim();

            this.ProdSetMult = rs.getDouble("ProdSetMult");
            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("StandByFlag1") == null )
                this.StandByFlag1 = null;
            else
                this.StandByFlag1 = rs.getString("StandByFlag1").trim();

            if( rs.getString("StandByFlag2") == null )
                this.StandByFlag2 = null;
            else
                this.StandByFlag2 = rs.getString("StandByFlag2").trim();

            this.StandByFlag3 = rs.getDate("StandByFlag3");
            this.StandByFlag4 = rs.getDate("StandByFlag4");
            this.StandByFlag5 = rs.getDouble("StandByFlag5");
            this.StandByFlag6 = rs.getDouble("StandByFlag6");
            this.InsuYear = rs.getInt("InsuYear");
            if( rs.getString("InsuYearFlag") == null )
                this.InsuYearFlag = null;
            else
                this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCProdSetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCProdSetSchema getSchema() {
        LCProdSetSchema aLCProdSetSchema = new LCProdSetSchema();
        aLCProdSetSchema.setSchema(this);
        return aLCProdSetSchema;
    }

    public LCProdSetDB getDB() {
        LCProdSetDB aDBOper = new LCProdSetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCProdSet描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(ProdSetID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ProdSetMult));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StandByFlag3 ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StandByFlag4 ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandByFlag5));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandByFlag6));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuYearFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCProdSet>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ProdSetID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ProdSetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ProdSetName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ProdSetMult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            StandByFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            StandByFlag3 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER));
            StandByFlag4 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            StandByFlag5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19, SysConst.PACKAGESPILTER))).doubleValue();
            StandByFlag6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).doubleValue();
            InsuYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).intValue();
            InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCProdSetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetName));
        }
        if (FCode.equalsIgnoreCase("ProdSetMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetMult));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByFlag3()));
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByFlag4()));
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag5));
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag6));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ProdSetID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ProdSetCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ProdSetName);
                break;
            case 8:
                strFieldValue = String.valueOf(ProdSetMult);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag2);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByFlag3()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByFlag4()));
                break;
            case 18:
                strFieldValue = String.valueOf(StandByFlag5);
                break;
            case 19:
                strFieldValue = String.valueOf(StandByFlag6);
                break;
            case 20:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ProdSetID")) {
            if( FValue != null && !FValue.equals("")) {
                ProdSetID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetName = FValue.trim();
            }
            else
                ProdSetName = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetMult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ProdSetMult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag2 = FValue.trim();
            }
            else
                StandByFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            if(FValue != null && !FValue.equals("")) {
                StandByFlag3 = fDate.getDate( FValue );
            }
            else
                StandByFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            if(FValue != null && !FValue.equals("")) {
                StandByFlag4 = fDate.getDate( FValue );
            }
            else
                StandByFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandByFlag5 = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandByFlag6 = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCProdSetSchema other = (LCProdSetSchema)otherObject;
        return
            ProdSetID == other.getProdSetID()
            && ShardingID.equals(other.getShardingID())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && ProposalContNo.equals(other.getProposalContNo())
            && PrtNo.equals(other.getPrtNo())
            && ProdSetCode.equals(other.getProdSetCode())
            && ProdSetName.equals(other.getProdSetName())
            && ProdSetMult == other.getProdSetMult()
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && StandByFlag1.equals(other.getStandByFlag1())
            && StandByFlag2.equals(other.getStandByFlag2())
            && fDate.getString(StandByFlag3).equals(other.getStandByFlag3())
            && fDate.getString(StandByFlag4).equals(other.getStandByFlag4())
            && StandByFlag5 == other.getStandByFlag5()
            && StandByFlag6 == other.getStandByFlag6()
            && InsuYear == other.getInsuYear()
            && InsuYearFlag.equals(other.getInsuYearFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ProdSetID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 2;
        }
        if( strFieldName.equals("ContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 5;
        }
        if( strFieldName.equals("ProdSetCode") ) {
            return 6;
        }
        if( strFieldName.equals("ProdSetName") ) {
            return 7;
        }
        if( strFieldName.equals("ProdSetMult") ) {
            return 8;
        }
        if( strFieldName.equals("Operator") ) {
            return 9;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 10;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 11;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 12;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 13;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 14;
        }
        if( strFieldName.equals("StandByFlag2") ) {
            return 15;
        }
        if( strFieldName.equals("StandByFlag3") ) {
            return 16;
        }
        if( strFieldName.equals("StandByFlag4") ) {
            return 17;
        }
        if( strFieldName.equals("StandByFlag5") ) {
            return 18;
        }
        if( strFieldName.equals("StandByFlag6") ) {
            return 19;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 20;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 21;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ProdSetID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "ProposalContNo";
                break;
            case 5:
                strFieldName = "PrtNo";
                break;
            case 6:
                strFieldName = "ProdSetCode";
                break;
            case 7:
                strFieldName = "ProdSetName";
                break;
            case 8:
                strFieldName = "ProdSetMult";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "ModifyDate";
                break;
            case 13:
                strFieldName = "ModifyTime";
                break;
            case 14:
                strFieldName = "StandByFlag1";
                break;
            case 15:
                strFieldName = "StandByFlag2";
                break;
            case 16:
                strFieldName = "StandByFlag3";
                break;
            case 17:
                strFieldName = "StandByFlag4";
                break;
            case 18:
                strFieldName = "StandByFlag5";
                break;
            case 19:
                strFieldName = "StandByFlag6";
                break;
            case 20:
                strFieldName = "InsuYear";
                break;
            case 21:
                strFieldName = "InsuYearFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRODSETID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "PRODSETNAME":
                return Schema.TYPE_STRING;
            case "PRODSETMULT":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_DATE;
            case "STANDBYFLAG4":
                return Schema.TYPE_DATE;
            case "STANDBYFLAG5":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG6":
                return Schema.TYPE_DOUBLE;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DATE;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_DOUBLE;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_INT;
            case 21:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
