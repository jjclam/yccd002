/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LPEdorMainPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LPEdorMainPojo implements Pojo,Serializable {
    // @Field
    /** 保全受理号 */
    private String EdorAcceptNo; 
    /** 批单号 */
    private String EdorNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 批改申请号 */
    private String EdorAppNo; 
    /** 申请人名称 */
    private String EdorAppName; 
    /** 管理机构 */
    private String ManageCom; 
    /** 变动的保费 */
    private double ChgPrem; 
    /** 变动的保额 */
    private double ChgAmnt; 
    /** 补/退费金额 */
    private double GetMoney; 
    /** 补/退费利息 */
    private double GetInterest; 
    /** 批改申请日期 */
    private String  EdorAppDate;
    /** 批改生效日期 */
    private String  EdorValiDate;
    /** 批改状态 */
    private String EdorState; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 银行帐户名 */
    private String AccName; 
    /** 通讯地址 */
    private String PostalAddress; 
    /** 通讯邮编 */
    private String ZipCode; 
    /** 通讯电话 */
    private String Phone; 
    /** 打印标志 */
    private String PrintFlag; 
    /** 核保级别 */
    private String UWGrade; 
    /** 申请级别 */
    private String AppGrade; 
    /** 核保标志 */
    private String UWState; 
    /** 核保人 */
    private String UWOperator; 
    /** 核保日期 */
    private String  UWDate;
    /** 核保时间 */
    private String UWTime; 
    /** 确认人 */
    private String ConfOperator; 
    /** 确认日期 */
    private String  ConfDate;
    /** 确认时间 */
    private String ConfTime; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 复核状态 */
    private String ApproveFlag; 
    /** 复核人 */
    private String ApproveOperator; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 


    public static final int FIELDNUM = 38;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }
    public void setEdorAcceptNo(String aEdorAcceptNo) {
        EdorAcceptNo = aEdorAcceptNo;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getEdorAppNo() {
        return EdorAppNo;
    }
    public void setEdorAppNo(String aEdorAppNo) {
        EdorAppNo = aEdorAppNo;
    }
    public String getEdorAppName() {
        return EdorAppName;
    }
    public void setEdorAppName(String aEdorAppName) {
        EdorAppName = aEdorAppName;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public double getChgPrem() {
        return ChgPrem;
    }
    public void setChgPrem(double aChgPrem) {
        ChgPrem = aChgPrem;
    }
    public void setChgPrem(String aChgPrem) {
        if (aChgPrem != null && !aChgPrem.equals("")) {
            Double tDouble = new Double(aChgPrem);
            double d = tDouble.doubleValue();
            ChgPrem = d;
        }
    }

    public double getChgAmnt() {
        return ChgAmnt;
    }
    public void setChgAmnt(double aChgAmnt) {
        ChgAmnt = aChgAmnt;
    }
    public void setChgAmnt(String aChgAmnt) {
        if (aChgAmnt != null && !aChgAmnt.equals("")) {
            Double tDouble = new Double(aChgAmnt);
            double d = tDouble.doubleValue();
            ChgAmnt = d;
        }
    }

    public double getGetMoney() {
        return GetMoney;
    }
    public void setGetMoney(double aGetMoney) {
        GetMoney = aGetMoney;
    }
    public void setGetMoney(String aGetMoney) {
        if (aGetMoney != null && !aGetMoney.equals("")) {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public double getGetInterest() {
        return GetInterest;
    }
    public void setGetInterest(double aGetInterest) {
        GetInterest = aGetInterest;
    }
    public void setGetInterest(String aGetInterest) {
        if (aGetInterest != null && !aGetInterest.equals("")) {
            Double tDouble = new Double(aGetInterest);
            double d = tDouble.doubleValue();
            GetInterest = d;
        }
    }

    public String getEdorAppDate() {
        return EdorAppDate;
    }
    public void setEdorAppDate(String aEdorAppDate) {
        EdorAppDate = aEdorAppDate;
    }
    public String getEdorValiDate() {
        return EdorValiDate;
    }
    public void setEdorValiDate(String aEdorValiDate) {
        EdorValiDate = aEdorValiDate;
    }
    public String getEdorState() {
        return EdorState;
    }
    public void setEdorState(String aEdorState) {
        EdorState = aEdorState;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getPostalAddress() {
        return PostalAddress;
    }
    public void setPostalAddress(String aPostalAddress) {
        PostalAddress = aPostalAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getPrintFlag() {
        return PrintFlag;
    }
    public void setPrintFlag(String aPrintFlag) {
        PrintFlag = aPrintFlag;
    }
    public String getUWGrade() {
        return UWGrade;
    }
    public void setUWGrade(String aUWGrade) {
        UWGrade = aUWGrade;
    }
    public String getAppGrade() {
        return AppGrade;
    }
    public void setAppGrade(String aAppGrade) {
        AppGrade = aAppGrade;
    }
    public String getUWState() {
        return UWState;
    }
    public void setUWState(String aUWState) {
        UWState = aUWState;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWDate() {
        return UWDate;
    }
    public void setUWDate(String aUWDate) {
        UWDate = aUWDate;
    }
    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getConfOperator() {
        return ConfOperator;
    }
    public void setConfOperator(String aConfOperator) {
        ConfOperator = aConfOperator;
    }
    public String getConfDate() {
        return ConfDate;
    }
    public void setConfDate(String aConfDate) {
        ConfDate = aConfDate;
    }
    public String getConfTime() {
        return ConfTime;
    }
    public void setConfTime(String aConfTime) {
        ConfTime = aConfTime;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveOperator() {
        return ApproveOperator;
    }
    public void setApproveOperator(String aApproveOperator) {
        ApproveOperator = aApproveOperator;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("EdorAcceptNo") ) {
            return 0;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 1;
        }
        if( strFieldName.equals("ContNo") ) {
            return 2;
        }
        if( strFieldName.equals("EdorAppNo") ) {
            return 3;
        }
        if( strFieldName.equals("EdorAppName") ) {
            return 4;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 5;
        }
        if( strFieldName.equals("ChgPrem") ) {
            return 6;
        }
        if( strFieldName.equals("ChgAmnt") ) {
            return 7;
        }
        if( strFieldName.equals("GetMoney") ) {
            return 8;
        }
        if( strFieldName.equals("GetInterest") ) {
            return 9;
        }
        if( strFieldName.equals("EdorAppDate") ) {
            return 10;
        }
        if( strFieldName.equals("EdorValiDate") ) {
            return 11;
        }
        if( strFieldName.equals("EdorState") ) {
            return 12;
        }
        if( strFieldName.equals("BankCode") ) {
            return 13;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 14;
        }
        if( strFieldName.equals("AccName") ) {
            return 15;
        }
        if( strFieldName.equals("PostalAddress") ) {
            return 16;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 17;
        }
        if( strFieldName.equals("Phone") ) {
            return 18;
        }
        if( strFieldName.equals("PrintFlag") ) {
            return 19;
        }
        if( strFieldName.equals("UWGrade") ) {
            return 20;
        }
        if( strFieldName.equals("AppGrade") ) {
            return 21;
        }
        if( strFieldName.equals("UWState") ) {
            return 22;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 23;
        }
        if( strFieldName.equals("UWDate") ) {
            return 24;
        }
        if( strFieldName.equals("UWTime") ) {
            return 25;
        }
        if( strFieldName.equals("ConfOperator") ) {
            return 26;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 27;
        }
        if( strFieldName.equals("ConfTime") ) {
            return 28;
        }
        if( strFieldName.equals("Operator") ) {
            return 29;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 30;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 31;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 32;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 33;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 34;
        }
        if( strFieldName.equals("ApproveOperator") ) {
            return 35;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 36;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 37;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "EdorAcceptNo";
                break;
            case 1:
                strFieldName = "EdorNo";
                break;
            case 2:
                strFieldName = "ContNo";
                break;
            case 3:
                strFieldName = "EdorAppNo";
                break;
            case 4:
                strFieldName = "EdorAppName";
                break;
            case 5:
                strFieldName = "ManageCom";
                break;
            case 6:
                strFieldName = "ChgPrem";
                break;
            case 7:
                strFieldName = "ChgAmnt";
                break;
            case 8:
                strFieldName = "GetMoney";
                break;
            case 9:
                strFieldName = "GetInterest";
                break;
            case 10:
                strFieldName = "EdorAppDate";
                break;
            case 11:
                strFieldName = "EdorValiDate";
                break;
            case 12:
                strFieldName = "EdorState";
                break;
            case 13:
                strFieldName = "BankCode";
                break;
            case 14:
                strFieldName = "BankAccNo";
                break;
            case 15:
                strFieldName = "AccName";
                break;
            case 16:
                strFieldName = "PostalAddress";
                break;
            case 17:
                strFieldName = "ZipCode";
                break;
            case 18:
                strFieldName = "Phone";
                break;
            case 19:
                strFieldName = "PrintFlag";
                break;
            case 20:
                strFieldName = "UWGrade";
                break;
            case 21:
                strFieldName = "AppGrade";
                break;
            case 22:
                strFieldName = "UWState";
                break;
            case 23:
                strFieldName = "UWOperator";
                break;
            case 24:
                strFieldName = "UWDate";
                break;
            case 25:
                strFieldName = "UWTime";
                break;
            case 26:
                strFieldName = "ConfOperator";
                break;
            case 27:
                strFieldName = "ConfDate";
                break;
            case 28:
                strFieldName = "ConfTime";
                break;
            case 29:
                strFieldName = "Operator";
                break;
            case 30:
                strFieldName = "MakeDate";
                break;
            case 31:
                strFieldName = "MakeTime";
                break;
            case 32:
                strFieldName = "ModifyDate";
                break;
            case 33:
                strFieldName = "ModifyTime";
                break;
            case 34:
                strFieldName = "ApproveFlag";
                break;
            case 35:
                strFieldName = "ApproveOperator";
                break;
            case 36:
                strFieldName = "ApproveDate";
                break;
            case 37:
                strFieldName = "ApproveTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "EDORACCEPTNO":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "EDORAPPNO":
                return Schema.TYPE_STRING;
            case "EDORAPPNAME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "CHGPREM":
                return Schema.TYPE_DOUBLE;
            case "CHGAMNT":
                return Schema.TYPE_DOUBLE;
            case "GETMONEY":
                return Schema.TYPE_DOUBLE;
            case "GETINTEREST":
                return Schema.TYPE_DOUBLE;
            case "EDORAPPDATE":
                return Schema.TYPE_STRING;
            case "EDORVALIDATE":
                return Schema.TYPE_STRING;
            case "EDORSTATE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "POSTALADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "PRINTFLAG":
                return Schema.TYPE_STRING;
            case "UWGRADE":
                return Schema.TYPE_STRING;
            case "APPGRADE":
                return Schema.TYPE_STRING;
            case "UWSTATE":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_STRING;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "CONFOPERATOR":
                return Schema.TYPE_STRING;
            case "CONFDATE":
                return Schema.TYPE_STRING;
            case "CONFTIME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVEOPERATOR":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("EdorAppNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAppNo));
        }
        if (FCode.equalsIgnoreCase("EdorAppName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAppName));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ChgPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgPrem));
        }
        if (FCode.equalsIgnoreCase("ChgAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgAmnt));
        }
        if (FCode.equalsIgnoreCase("GetMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMoney));
        }
        if (FCode.equalsIgnoreCase("GetInterest")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetInterest));
        }
        if (FCode.equalsIgnoreCase("EdorAppDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAppDate));
        }
        if (FCode.equalsIgnoreCase("EdorValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorValiDate));
        }
        if (FCode.equalsIgnoreCase("EdorState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorState));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWGrade));
        }
        if (FCode.equalsIgnoreCase("AppGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppGrade));
        }
        if (FCode.equalsIgnoreCase("UWState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWState));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWDate));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("ConfOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfOperator));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfDate));
        }
        if (FCode.equalsIgnoreCase("ConfTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfTime));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOperator));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(EdorAcceptNo);
                break;
            case 1:
                strFieldValue = String.valueOf(EdorNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(EdorAppNo);
                break;
            case 4:
                strFieldValue = String.valueOf(EdorAppName);
                break;
            case 5:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 6:
                strFieldValue = String.valueOf(ChgPrem);
                break;
            case 7:
                strFieldValue = String.valueOf(ChgAmnt);
                break;
            case 8:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 9:
                strFieldValue = String.valueOf(GetInterest);
                break;
            case 10:
                strFieldValue = String.valueOf(EdorAppDate);
                break;
            case 11:
                strFieldValue = String.valueOf(EdorValiDate);
                break;
            case 12:
                strFieldValue = String.valueOf(EdorState);
                break;
            case 13:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 14:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 15:
                strFieldValue = String.valueOf(AccName);
                break;
            case 16:
                strFieldValue = String.valueOf(PostalAddress);
                break;
            case 17:
                strFieldValue = String.valueOf(ZipCode);
                break;
            case 18:
                strFieldValue = String.valueOf(Phone);
                break;
            case 19:
                strFieldValue = String.valueOf(PrintFlag);
                break;
            case 20:
                strFieldValue = String.valueOf(UWGrade);
                break;
            case 21:
                strFieldValue = String.valueOf(AppGrade);
                break;
            case 22:
                strFieldValue = String.valueOf(UWState);
                break;
            case 23:
                strFieldValue = String.valueOf(UWOperator);
                break;
            case 24:
                strFieldValue = String.valueOf(UWDate);
                break;
            case 25:
                strFieldValue = String.valueOf(UWTime);
                break;
            case 26:
                strFieldValue = String.valueOf(ConfOperator);
                break;
            case 27:
                strFieldValue = String.valueOf(ConfDate);
                break;
            case 28:
                strFieldValue = String.valueOf(ConfTime);
                break;
            case 29:
                strFieldValue = String.valueOf(Operator);
                break;
            case 30:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 31:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 32:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 33:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 34:
                strFieldValue = String.valueOf(ApproveFlag);
                break;
            case 35:
                strFieldValue = String.valueOf(ApproveOperator);
                break;
            case 36:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 37:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
                EdorAcceptNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorAppNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAppNo = FValue.trim();
            }
            else
                EdorAppNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorAppName")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAppName = FValue.trim();
            }
            else
                EdorAppName = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ChgPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ChgPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("ChgAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ChgAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetInterest")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetInterest = d;
            }
        }
        if (FCode.equalsIgnoreCase("EdorAppDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAppDate = FValue.trim();
            }
            else
                EdorAppDate = null;
        }
        if (FCode.equalsIgnoreCase("EdorValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorValiDate = FValue.trim();
            }
            else
                EdorValiDate = null;
        }
        if (FCode.equalsIgnoreCase("EdorState")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorState = FValue.trim();
            }
            else
                EdorState = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
                PostalAddress = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
                PrintFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
                UWGrade = null;
        }
        if (FCode.equalsIgnoreCase("AppGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppGrade = FValue.trim();
            }
            else
                AppGrade = null;
        }
        if (FCode.equalsIgnoreCase("UWState")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWState = FValue.trim();
            }
            else
                UWState = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWDate = FValue.trim();
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("ConfOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfOperator = FValue.trim();
            }
            else
                ConfOperator = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfDate = FValue.trim();
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfTime = FValue.trim();
            }
            else
                ConfTime = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveOperator = FValue.trim();
            }
            else
                ApproveOperator = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        return true;
    }


    public String toString() {
    return "LPEdorMainPojo [" +
            "EdorAcceptNo="+EdorAcceptNo +
            ", EdorNo="+EdorNo +
            ", ContNo="+ContNo +
            ", EdorAppNo="+EdorAppNo +
            ", EdorAppName="+EdorAppName +
            ", ManageCom="+ManageCom +
            ", ChgPrem="+ChgPrem +
            ", ChgAmnt="+ChgAmnt +
            ", GetMoney="+GetMoney +
            ", GetInterest="+GetInterest +
            ", EdorAppDate="+EdorAppDate +
            ", EdorValiDate="+EdorValiDate +
            ", EdorState="+EdorState +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", AccName="+AccName +
            ", PostalAddress="+PostalAddress +
            ", ZipCode="+ZipCode +
            ", Phone="+Phone +
            ", PrintFlag="+PrintFlag +
            ", UWGrade="+UWGrade +
            ", AppGrade="+AppGrade +
            ", UWState="+UWState +
            ", UWOperator="+UWOperator +
            ", UWDate="+UWDate +
            ", UWTime="+UWTime +
            ", ConfOperator="+ConfOperator +
            ", ConfDate="+ConfDate +
            ", ConfTime="+ConfTime +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", ApproveFlag="+ApproveFlag +
            ", ApproveOperator="+ApproveOperator +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +"]";
    }
}
