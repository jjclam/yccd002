/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCGetPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCGetPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long GetID; 
    /** Fk_lcduty */
    private long DutyID; 
    /** Shardingid */
    private String ShardingID; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 责任编码 */
    private String DutyCode; 
    /** 给付责任编码 */
    private String GetDutyCode; 
    /** 给付责任类型 */
    private String GetDutyKind; 
    /** 被保人客户号码 */
    private String InsuredNo; 
    /** 领取方式 */
    private String GetMode; 
    /** 起付限 */
    private double GetLimit; 
    /** 赔付比例 */
    private double GetRate; 
    /** 催付标记 */
    private String UrgeGetFlag; 
    /** 生存意外给付标志 */
    private String LiveGetType; 
    /** 递增率 */
    private double AddRate; 
    /** 默认申请标志 */
    private String CanGet; 
    /** 是否和账户相关 */
    private String NeedAcc; 
    /** 是否是账户结清后才能申请 */
    private String NeedCancelAcc; 
    /** 标准给付金额 */
    private double StandMoney; 
    /** 实际给付金额 */
    private double ActuGet; 
    /** 已领金额 */
    private double SumMoney; 
    /** 领取间隔 */
    private int GetIntv; 
    /** 领至日期 */
    private String  GettoDate;
    /** 起领日期 */
    private String  GetStartDate;
    /** 止领日期 */
    private String  GetEndDate;
    /** 结算日期 */
    private String  BalaDate;
    /** 状态 */
    private String State; 
    /** 管理机构 */
    private String ManageCom; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 止领标志 */
    private String GetEndState; 


    public static final int FIELDNUM = 35;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getGetID() {
        return GetID;
    }
    public void setGetID(long aGetID) {
        GetID = aGetID;
    }
    public void setGetID(String aGetID) {
        if (aGetID != null && !aGetID.equals("")) {
            GetID = new Long(aGetID).longValue();
        }
    }

    public long getDutyID() {
        return DutyID;
    }
    public void setDutyID(long aDutyID) {
        DutyID = aDutyID;
    }
    public void setDutyID(String aDutyID) {
        if (aDutyID != null && !aDutyID.equals("")) {
            DutyID = new Long(aDutyID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyKind() {
        return GetDutyKind;
    }
    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getGetMode() {
        return GetMode;
    }
    public void setGetMode(String aGetMode) {
        GetMode = aGetMode;
    }
    public double getGetLimit() {
        return GetLimit;
    }
    public void setGetLimit(double aGetLimit) {
        GetLimit = aGetLimit;
    }
    public void setGetLimit(String aGetLimit) {
        if (aGetLimit != null && !aGetLimit.equals("")) {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = d;
        }
    }

    public double getGetRate() {
        return GetRate;
    }
    public void setGetRate(double aGetRate) {
        GetRate = aGetRate;
    }
    public void setGetRate(String aGetRate) {
        if (aGetRate != null && !aGetRate.equals("")) {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = d;
        }
    }

    public String getUrgeGetFlag() {
        return UrgeGetFlag;
    }
    public void setUrgeGetFlag(String aUrgeGetFlag) {
        UrgeGetFlag = aUrgeGetFlag;
    }
    public String getLiveGetType() {
        return LiveGetType;
    }
    public void setLiveGetType(String aLiveGetType) {
        LiveGetType = aLiveGetType;
    }
    public double getAddRate() {
        return AddRate;
    }
    public void setAddRate(double aAddRate) {
        AddRate = aAddRate;
    }
    public void setAddRate(String aAddRate) {
        if (aAddRate != null && !aAddRate.equals("")) {
            Double tDouble = new Double(aAddRate);
            double d = tDouble.doubleValue();
            AddRate = d;
        }
    }

    public String getCanGet() {
        return CanGet;
    }
    public void setCanGet(String aCanGet) {
        CanGet = aCanGet;
    }
    public String getNeedAcc() {
        return NeedAcc;
    }
    public void setNeedAcc(String aNeedAcc) {
        NeedAcc = aNeedAcc;
    }
    public String getNeedCancelAcc() {
        return NeedCancelAcc;
    }
    public void setNeedCancelAcc(String aNeedCancelAcc) {
        NeedCancelAcc = aNeedCancelAcc;
    }
    public double getStandMoney() {
        return StandMoney;
    }
    public void setStandMoney(double aStandMoney) {
        StandMoney = aStandMoney;
    }
    public void setStandMoney(String aStandMoney) {
        if (aStandMoney != null && !aStandMoney.equals("")) {
            Double tDouble = new Double(aStandMoney);
            double d = tDouble.doubleValue();
            StandMoney = d;
        }
    }

    public double getActuGet() {
        return ActuGet;
    }
    public void setActuGet(double aActuGet) {
        ActuGet = aActuGet;
    }
    public void setActuGet(String aActuGet) {
        if (aActuGet != null && !aActuGet.equals("")) {
            Double tDouble = new Double(aActuGet);
            double d = tDouble.doubleValue();
            ActuGet = d;
        }
    }

    public double getSumMoney() {
        return SumMoney;
    }
    public void setSumMoney(double aSumMoney) {
        SumMoney = aSumMoney;
    }
    public void setSumMoney(String aSumMoney) {
        if (aSumMoney != null && !aSumMoney.equals("")) {
            Double tDouble = new Double(aSumMoney);
            double d = tDouble.doubleValue();
            SumMoney = d;
        }
    }

    public int getGetIntv() {
        return GetIntv;
    }
    public void setGetIntv(int aGetIntv) {
        GetIntv = aGetIntv;
    }
    public void setGetIntv(String aGetIntv) {
        if (aGetIntv != null && !aGetIntv.equals("")) {
            Integer tInteger = new Integer(aGetIntv);
            int i = tInteger.intValue();
            GetIntv = i;
        }
    }

    public String getGettoDate() {
        return GettoDate;
    }
    public void setGettoDate(String aGettoDate) {
        GettoDate = aGettoDate;
    }
    public String getGetStartDate() {
        return GetStartDate;
    }
    public void setGetStartDate(String aGetStartDate) {
        GetStartDate = aGetStartDate;
    }
    public String getGetEndDate() {
        return GetEndDate;
    }
    public void setGetEndDate(String aGetEndDate) {
        GetEndDate = aGetEndDate;
    }
    public String getBalaDate() {
        return BalaDate;
    }
    public void setBalaDate(String aBalaDate) {
        BalaDate = aBalaDate;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getGetEndState() {
        return GetEndState;
    }
    public void setGetEndState(String aGetEndState) {
        GetEndState = aGetEndState;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GetID") ) {
            return 0;
        }
        if( strFieldName.equals("DutyID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PolNo") ) {
            return 5;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 6;
        }
        if( strFieldName.equals("GetDutyCode") ) {
            return 7;
        }
        if( strFieldName.equals("GetDutyKind") ) {
            return 8;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 9;
        }
        if( strFieldName.equals("GetMode") ) {
            return 10;
        }
        if( strFieldName.equals("GetLimit") ) {
            return 11;
        }
        if( strFieldName.equals("GetRate") ) {
            return 12;
        }
        if( strFieldName.equals("UrgeGetFlag") ) {
            return 13;
        }
        if( strFieldName.equals("LiveGetType") ) {
            return 14;
        }
        if( strFieldName.equals("AddRate") ) {
            return 15;
        }
        if( strFieldName.equals("CanGet") ) {
            return 16;
        }
        if( strFieldName.equals("NeedAcc") ) {
            return 17;
        }
        if( strFieldName.equals("NeedCancelAcc") ) {
            return 18;
        }
        if( strFieldName.equals("StandMoney") ) {
            return 19;
        }
        if( strFieldName.equals("ActuGet") ) {
            return 20;
        }
        if( strFieldName.equals("SumMoney") ) {
            return 21;
        }
        if( strFieldName.equals("GetIntv") ) {
            return 22;
        }
        if( strFieldName.equals("GettoDate") ) {
            return 23;
        }
        if( strFieldName.equals("GetStartDate") ) {
            return 24;
        }
        if( strFieldName.equals("GetEndDate") ) {
            return 25;
        }
        if( strFieldName.equals("BalaDate") ) {
            return 26;
        }
        if( strFieldName.equals("State") ) {
            return 27;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 28;
        }
        if( strFieldName.equals("Operator") ) {
            return 29;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 30;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 31;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 32;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 33;
        }
        if( strFieldName.equals("GetEndState") ) {
            return 34;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GetID";
                break;
            case 1:
                strFieldName = "DutyID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "PolNo";
                break;
            case 6:
                strFieldName = "DutyCode";
                break;
            case 7:
                strFieldName = "GetDutyCode";
                break;
            case 8:
                strFieldName = "GetDutyKind";
                break;
            case 9:
                strFieldName = "InsuredNo";
                break;
            case 10:
                strFieldName = "GetMode";
                break;
            case 11:
                strFieldName = "GetLimit";
                break;
            case 12:
                strFieldName = "GetRate";
                break;
            case 13:
                strFieldName = "UrgeGetFlag";
                break;
            case 14:
                strFieldName = "LiveGetType";
                break;
            case 15:
                strFieldName = "AddRate";
                break;
            case 16:
                strFieldName = "CanGet";
                break;
            case 17:
                strFieldName = "NeedAcc";
                break;
            case 18:
                strFieldName = "NeedCancelAcc";
                break;
            case 19:
                strFieldName = "StandMoney";
                break;
            case 20:
                strFieldName = "ActuGet";
                break;
            case 21:
                strFieldName = "SumMoney";
                break;
            case 22:
                strFieldName = "GetIntv";
                break;
            case 23:
                strFieldName = "GettoDate";
                break;
            case 24:
                strFieldName = "GetStartDate";
                break;
            case 25:
                strFieldName = "GetEndDate";
                break;
            case 26:
                strFieldName = "BalaDate";
                break;
            case 27:
                strFieldName = "State";
                break;
            case 28:
                strFieldName = "ManageCom";
                break;
            case 29:
                strFieldName = "Operator";
                break;
            case 30:
                strFieldName = "MakeDate";
                break;
            case 31:
                strFieldName = "MakeTime";
                break;
            case 32:
                strFieldName = "ModifyTime";
                break;
            case 33:
                strFieldName = "ModifyDate";
                break;
            case 34:
                strFieldName = "GetEndState";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GETID":
                return Schema.TYPE_LONG;
            case "DUTYID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYKIND":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "GETMODE":
                return Schema.TYPE_STRING;
            case "GETLIMIT":
                return Schema.TYPE_DOUBLE;
            case "GETRATE":
                return Schema.TYPE_DOUBLE;
            case "URGEGETFLAG":
                return Schema.TYPE_STRING;
            case "LIVEGETTYPE":
                return Schema.TYPE_STRING;
            case "ADDRATE":
                return Schema.TYPE_DOUBLE;
            case "CANGET":
                return Schema.TYPE_STRING;
            case "NEEDACC":
                return Schema.TYPE_STRING;
            case "NEEDCANCELACC":
                return Schema.TYPE_STRING;
            case "STANDMONEY":
                return Schema.TYPE_DOUBLE;
            case "ACTUGET":
                return Schema.TYPE_DOUBLE;
            case "SUMMONEY":
                return Schema.TYPE_DOUBLE;
            case "GETINTV":
                return Schema.TYPE_INT;
            case "GETTODATE":
                return Schema.TYPE_STRING;
            case "GETSTARTDATE":
                return Schema.TYPE_STRING;
            case "GETENDDATE":
                return Schema.TYPE_STRING;
            case "BALADATE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "GETENDSTATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_INT;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GetID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetID));
        }
        if (FCode.equalsIgnoreCase("DutyID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("GetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMode));
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
        }
        if (FCode.equalsIgnoreCase("UrgeGetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UrgeGetFlag));
        }
        if (FCode.equalsIgnoreCase("LiveGetType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetType));
        }
        if (FCode.equalsIgnoreCase("AddRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddRate));
        }
        if (FCode.equalsIgnoreCase("CanGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CanGet));
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedAcc));
        }
        if (FCode.equalsIgnoreCase("NeedCancelAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedCancelAcc));
        }
        if (FCode.equalsIgnoreCase("StandMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandMoney));
        }
        if (FCode.equalsIgnoreCase("ActuGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGet));
        }
        if (FCode.equalsIgnoreCase("SumMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetIntv));
        }
        if (FCode.equalsIgnoreCase("GettoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GettoDate));
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartDate));
        }
        if (FCode.equalsIgnoreCase("GetEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetEndDate));
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaDate));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("GetEndState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetEndState));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GetID);
                break;
            case 1:
                strFieldValue = String.valueOf(DutyID);
                break;
            case 2:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 3:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 5:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 6:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 7:
                strFieldValue = String.valueOf(GetDutyCode);
                break;
            case 8:
                strFieldValue = String.valueOf(GetDutyKind);
                break;
            case 9:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 10:
                strFieldValue = String.valueOf(GetMode);
                break;
            case 11:
                strFieldValue = String.valueOf(GetLimit);
                break;
            case 12:
                strFieldValue = String.valueOf(GetRate);
                break;
            case 13:
                strFieldValue = String.valueOf(UrgeGetFlag);
                break;
            case 14:
                strFieldValue = String.valueOf(LiveGetType);
                break;
            case 15:
                strFieldValue = String.valueOf(AddRate);
                break;
            case 16:
                strFieldValue = String.valueOf(CanGet);
                break;
            case 17:
                strFieldValue = String.valueOf(NeedAcc);
                break;
            case 18:
                strFieldValue = String.valueOf(NeedCancelAcc);
                break;
            case 19:
                strFieldValue = String.valueOf(StandMoney);
                break;
            case 20:
                strFieldValue = String.valueOf(ActuGet);
                break;
            case 21:
                strFieldValue = String.valueOf(SumMoney);
                break;
            case 22:
                strFieldValue = String.valueOf(GetIntv);
                break;
            case 23:
                strFieldValue = String.valueOf(GettoDate);
                break;
            case 24:
                strFieldValue = String.valueOf(GetStartDate);
                break;
            case 25:
                strFieldValue = String.valueOf(GetEndDate);
                break;
            case 26:
                strFieldValue = String.valueOf(BalaDate);
                break;
            case 27:
                strFieldValue = String.valueOf(State);
                break;
            case 28:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 29:
                strFieldValue = String.valueOf(Operator);
                break;
            case 30:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 31:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 32:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 33:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 34:
                strFieldValue = String.valueOf(GetEndState);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GetID")) {
            if( FValue != null && !FValue.equals("")) {
                GetID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("DutyID")) {
            if( FValue != null && !FValue.equals("")) {
                DutyID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
                GetDutyKind = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("GetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetMode = FValue.trim();
            }
            else
                GetMode = null;
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("UrgeGetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UrgeGetFlag = FValue.trim();
            }
            else
                UrgeGetFlag = null;
        }
        if (FCode.equalsIgnoreCase("LiveGetType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveGetType = FValue.trim();
            }
            else
                LiveGetType = null;
        }
        if (FCode.equalsIgnoreCase("AddRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AddRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("CanGet")) {
            if( FValue != null && !FValue.equals(""))
            {
                CanGet = FValue.trim();
            }
            else
                CanGet = null;
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedAcc = FValue.trim();
            }
            else
                NeedAcc = null;
        }
        if (FCode.equalsIgnoreCase("NeedCancelAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedCancelAcc = FValue.trim();
            }
            else
                NeedCancelAcc = null;
        }
        if (FCode.equalsIgnoreCase("StandMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("ActuGet")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ActuGet = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("GettoDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GettoDate = FValue.trim();
            }
            else
                GettoDate = null;
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetStartDate = FValue.trim();
            }
            else
                GetStartDate = null;
        }
        if (FCode.equalsIgnoreCase("GetEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetEndDate = FValue.trim();
            }
            else
                GetEndDate = null;
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaDate = FValue.trim();
            }
            else
                BalaDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("GetEndState")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetEndState = FValue.trim();
            }
            else
                GetEndState = null;
        }
        return true;
    }


    public String toString() {
    return "LCGetPojo [" +
            "GetID="+GetID +
            ", DutyID="+DutyID +
            ", ShardingID="+ShardingID +
            ", GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", PolNo="+PolNo +
            ", DutyCode="+DutyCode +
            ", GetDutyCode="+GetDutyCode +
            ", GetDutyKind="+GetDutyKind +
            ", InsuredNo="+InsuredNo +
            ", GetMode="+GetMode +
            ", GetLimit="+GetLimit +
            ", GetRate="+GetRate +
            ", UrgeGetFlag="+UrgeGetFlag +
            ", LiveGetType="+LiveGetType +
            ", AddRate="+AddRate +
            ", CanGet="+CanGet +
            ", NeedAcc="+NeedAcc +
            ", NeedCancelAcc="+NeedCancelAcc +
            ", StandMoney="+StandMoney +
            ", ActuGet="+ActuGet +
            ", SumMoney="+SumMoney +
            ", GetIntv="+GetIntv +
            ", GettoDate="+GettoDate +
            ", GetStartDate="+GetStartDate +
            ", GetEndDate="+GetEndDate +
            ", BalaDate="+BalaDate +
            ", State="+State +
            ", ManageCom="+ManageCom +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyTime="+ModifyTime +
            ", ModifyDate="+ModifyDate +
            ", GetEndState="+GetEndState +"]";
    }
}
