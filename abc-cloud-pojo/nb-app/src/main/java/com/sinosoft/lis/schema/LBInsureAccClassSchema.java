/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBInsureAccClassDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBInsureAccClassSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBInsureAccClassSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long InsureAccClassID;
    /** Shardingid */
    private String ShardingID;
    /** 批单号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 合同号码 */
    private String ContNo;
    /** 管理机构 */
    private String ManageCom;
    /** 保单险种号码 */
    private String PolNo;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 对应其它号码 */
    private String OtherNo;
    /** 对应其它号码类型 */
    private String OtherType;
    /** 账户归属属性 */
    private String AccAscription;
    /** 险种编码 */
    private String RiskCode;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 账户类型 */
    private String AccType;
    /** 账户结算方式 */
    private String AccComputeFlag;
    /** 账户成立日期 */
    private Date AccFoundDate;
    /** 账户成立时间 */
    private String AccFoundTime;
    /** 结算日期 */
    private Date BalaDate;
    /** 结算时间 */
    private String BalaTime;
    /** 累计交费 */
    private double SumPay;
    /** 累计领取 */
    private double SumPaym;
    /** 期初账户现金余额 */
    private double LastAccBala;
    /** 期初账户单位数 */
    private double LastUnitCount;
    /** 期初账户单位价格 */
    private double LastUnitPrice;
    /** 保险帐户现金余额 */
    private double InsuAccBala;
    /** 保险帐户单位数 */
    private double UnitCount;
    /** 保险账户可领金额 */
    private double InsuAccGetMoney;
    /** 冻结金额 */
    private double FrozenMoney;
    /** 状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 37;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBInsureAccClassSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "InsureAccClassID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBInsureAccClassSchema cloned = (LBInsureAccClassSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getInsureAccClassID() {
        return InsureAccClassID;
    }
    public void setInsureAccClassID(long aInsureAccClassID) {
        InsureAccClassID = aInsureAccClassID;
    }
    public void setInsureAccClassID(String aInsureAccClassID) {
        if (aInsureAccClassID != null && !aInsureAccClassID.equals("")) {
            InsureAccClassID = new Long(aInsureAccClassID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherType() {
        return OtherType;
    }
    public void setOtherType(String aOtherType) {
        OtherType = aOtherType;
    }
    public String getAccAscription() {
        return AccAscription;
    }
    public void setAccAscription(String aAccAscription) {
        AccAscription = aAccAscription;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getAccComputeFlag() {
        return AccComputeFlag;
    }
    public void setAccComputeFlag(String aAccComputeFlag) {
        AccComputeFlag = aAccComputeFlag;
    }
    public String getAccFoundDate() {
        if(AccFoundDate != null) {
            return fDate.getString(AccFoundDate);
        } else {
            return null;
        }
    }
    public void setAccFoundDate(Date aAccFoundDate) {
        AccFoundDate = aAccFoundDate;
    }
    public void setAccFoundDate(String aAccFoundDate) {
        if (aAccFoundDate != null && !aAccFoundDate.equals("")) {
            AccFoundDate = fDate.getDate(aAccFoundDate);
        } else
            AccFoundDate = null;
    }

    public String getAccFoundTime() {
        return AccFoundTime;
    }
    public void setAccFoundTime(String aAccFoundTime) {
        AccFoundTime = aAccFoundTime;
    }
    public String getBalaDate() {
        if(BalaDate != null) {
            return fDate.getString(BalaDate);
        } else {
            return null;
        }
    }
    public void setBalaDate(Date aBalaDate) {
        BalaDate = aBalaDate;
    }
    public void setBalaDate(String aBalaDate) {
        if (aBalaDate != null && !aBalaDate.equals("")) {
            BalaDate = fDate.getDate(aBalaDate);
        } else
            BalaDate = null;
    }

    public String getBalaTime() {
        return BalaTime;
    }
    public void setBalaTime(String aBalaTime) {
        BalaTime = aBalaTime;
    }
    public double getSumPay() {
        return SumPay;
    }
    public void setSumPay(double aSumPay) {
        SumPay = aSumPay;
    }
    public void setSumPay(String aSumPay) {
        if (aSumPay != null && !aSumPay.equals("")) {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = d;
        }
    }

    public double getSumPaym() {
        return SumPaym;
    }
    public void setSumPaym(double aSumPaym) {
        SumPaym = aSumPaym;
    }
    public void setSumPaym(String aSumPaym) {
        if (aSumPaym != null && !aSumPaym.equals("")) {
            Double tDouble = new Double(aSumPaym);
            double d = tDouble.doubleValue();
            SumPaym = d;
        }
    }

    public double getLastAccBala() {
        return LastAccBala;
    }
    public void setLastAccBala(double aLastAccBala) {
        LastAccBala = aLastAccBala;
    }
    public void setLastAccBala(String aLastAccBala) {
        if (aLastAccBala != null && !aLastAccBala.equals("")) {
            Double tDouble = new Double(aLastAccBala);
            double d = tDouble.doubleValue();
            LastAccBala = d;
        }
    }

    public double getLastUnitCount() {
        return LastUnitCount;
    }
    public void setLastUnitCount(double aLastUnitCount) {
        LastUnitCount = aLastUnitCount;
    }
    public void setLastUnitCount(String aLastUnitCount) {
        if (aLastUnitCount != null && !aLastUnitCount.equals("")) {
            Double tDouble = new Double(aLastUnitCount);
            double d = tDouble.doubleValue();
            LastUnitCount = d;
        }
    }

    public double getLastUnitPrice() {
        return LastUnitPrice;
    }
    public void setLastUnitPrice(double aLastUnitPrice) {
        LastUnitPrice = aLastUnitPrice;
    }
    public void setLastUnitPrice(String aLastUnitPrice) {
        if (aLastUnitPrice != null && !aLastUnitPrice.equals("")) {
            Double tDouble = new Double(aLastUnitPrice);
            double d = tDouble.doubleValue();
            LastUnitPrice = d;
        }
    }

    public double getInsuAccBala() {
        return InsuAccBala;
    }
    public void setInsuAccBala(double aInsuAccBala) {
        InsuAccBala = aInsuAccBala;
    }
    public void setInsuAccBala(String aInsuAccBala) {
        if (aInsuAccBala != null && !aInsuAccBala.equals("")) {
            Double tDouble = new Double(aInsuAccBala);
            double d = tDouble.doubleValue();
            InsuAccBala = d;
        }
    }

    public double getUnitCount() {
        return UnitCount;
    }
    public void setUnitCount(double aUnitCount) {
        UnitCount = aUnitCount;
    }
    public void setUnitCount(String aUnitCount) {
        if (aUnitCount != null && !aUnitCount.equals("")) {
            Double tDouble = new Double(aUnitCount);
            double d = tDouble.doubleValue();
            UnitCount = d;
        }
    }

    public double getInsuAccGetMoney() {
        return InsuAccGetMoney;
    }
    public void setInsuAccGetMoney(double aInsuAccGetMoney) {
        InsuAccGetMoney = aInsuAccGetMoney;
    }
    public void setInsuAccGetMoney(String aInsuAccGetMoney) {
        if (aInsuAccGetMoney != null && !aInsuAccGetMoney.equals("")) {
            Double tDouble = new Double(aInsuAccGetMoney);
            double d = tDouble.doubleValue();
            InsuAccGetMoney = d;
        }
    }

    public double getFrozenMoney() {
        return FrozenMoney;
    }
    public void setFrozenMoney(double aFrozenMoney) {
        FrozenMoney = aFrozenMoney;
    }
    public void setFrozenMoney(String aFrozenMoney) {
        if (aFrozenMoney != null && !aFrozenMoney.equals("")) {
            Double tDouble = new Double(aFrozenMoney);
            double d = tDouble.doubleValue();
            FrozenMoney = d;
        }
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 使用另外一个 LBInsureAccClassSchema 对象给 Schema 赋值
    * @param: aLBInsureAccClassSchema LBInsureAccClassSchema
    **/
    public void setSchema(LBInsureAccClassSchema aLBInsureAccClassSchema) {
        this.InsureAccClassID = aLBInsureAccClassSchema.getInsureAccClassID();
        this.ShardingID = aLBInsureAccClassSchema.getShardingID();
        this.EdorNo = aLBInsureAccClassSchema.getEdorNo();
        this.GrpContNo = aLBInsureAccClassSchema.getGrpContNo();
        this.GrpPolNo = aLBInsureAccClassSchema.getGrpPolNo();
        this.ContNo = aLBInsureAccClassSchema.getContNo();
        this.ManageCom = aLBInsureAccClassSchema.getManageCom();
        this.PolNo = aLBInsureAccClassSchema.getPolNo();
        this.InsuAccNo = aLBInsureAccClassSchema.getInsuAccNo();
        this.PayPlanCode = aLBInsureAccClassSchema.getPayPlanCode();
        this.OtherNo = aLBInsureAccClassSchema.getOtherNo();
        this.OtherType = aLBInsureAccClassSchema.getOtherType();
        this.AccAscription = aLBInsureAccClassSchema.getAccAscription();
        this.RiskCode = aLBInsureAccClassSchema.getRiskCode();
        this.InsuredNo = aLBInsureAccClassSchema.getInsuredNo();
        this.AppntNo = aLBInsureAccClassSchema.getAppntNo();
        this.AccType = aLBInsureAccClassSchema.getAccType();
        this.AccComputeFlag = aLBInsureAccClassSchema.getAccComputeFlag();
        this.AccFoundDate = fDate.getDate( aLBInsureAccClassSchema.getAccFoundDate());
        this.AccFoundTime = aLBInsureAccClassSchema.getAccFoundTime();
        this.BalaDate = fDate.getDate( aLBInsureAccClassSchema.getBalaDate());
        this.BalaTime = aLBInsureAccClassSchema.getBalaTime();
        this.SumPay = aLBInsureAccClassSchema.getSumPay();
        this.SumPaym = aLBInsureAccClassSchema.getSumPaym();
        this.LastAccBala = aLBInsureAccClassSchema.getLastAccBala();
        this.LastUnitCount = aLBInsureAccClassSchema.getLastUnitCount();
        this.LastUnitPrice = aLBInsureAccClassSchema.getLastUnitPrice();
        this.InsuAccBala = aLBInsureAccClassSchema.getInsuAccBala();
        this.UnitCount = aLBInsureAccClassSchema.getUnitCount();
        this.InsuAccGetMoney = aLBInsureAccClassSchema.getInsuAccGetMoney();
        this.FrozenMoney = aLBInsureAccClassSchema.getFrozenMoney();
        this.State = aLBInsureAccClassSchema.getState();
        this.Operator = aLBInsureAccClassSchema.getOperator();
        this.MakeDate = fDate.getDate( aLBInsureAccClassSchema.getMakeDate());
        this.MakeTime = aLBInsureAccClassSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLBInsureAccClassSchema.getModifyDate());
        this.ModifyTime = aLBInsureAccClassSchema.getModifyTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.InsureAccClassID = rs.getLong("InsureAccClassID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("GrpPolNo") == null )
                this.GrpPolNo = null;
            else
                this.GrpPolNo = rs.getString("GrpPolNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("InsuAccNo") == null )
                this.InsuAccNo = null;
            else
                this.InsuAccNo = rs.getString("InsuAccNo").trim();

            if( rs.getString("PayPlanCode") == null )
                this.PayPlanCode = null;
            else
                this.PayPlanCode = rs.getString("PayPlanCode").trim();

            if( rs.getString("OtherNo") == null )
                this.OtherNo = null;
            else
                this.OtherNo = rs.getString("OtherNo").trim();

            if( rs.getString("OtherType") == null )
                this.OtherType = null;
            else
                this.OtherType = rs.getString("OtherType").trim();

            if( rs.getString("AccAscription") == null )
                this.AccAscription = null;
            else
                this.AccAscription = rs.getString("AccAscription").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("AccType") == null )
                this.AccType = null;
            else
                this.AccType = rs.getString("AccType").trim();

            if( rs.getString("AccComputeFlag") == null )
                this.AccComputeFlag = null;
            else
                this.AccComputeFlag = rs.getString("AccComputeFlag").trim();

            this.AccFoundDate = rs.getDate("AccFoundDate");
            if( rs.getString("AccFoundTime") == null )
                this.AccFoundTime = null;
            else
                this.AccFoundTime = rs.getString("AccFoundTime").trim();

            this.BalaDate = rs.getDate("BalaDate");
            if( rs.getString("BalaTime") == null )
                this.BalaTime = null;
            else
                this.BalaTime = rs.getString("BalaTime").trim();

            this.SumPay = rs.getDouble("SumPay");
            this.SumPaym = rs.getDouble("SumPaym");
            this.LastAccBala = rs.getDouble("LastAccBala");
            this.LastUnitCount = rs.getDouble("LastUnitCount");
            this.LastUnitPrice = rs.getDouble("LastUnitPrice");
            this.InsuAccBala = rs.getDouble("InsuAccBala");
            this.UnitCount = rs.getDouble("UnitCount");
            this.InsuAccGetMoney = rs.getDouble("InsuAccGetMoney");
            this.FrozenMoney = rs.getDouble("FrozenMoney");
            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsureAccClassSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBInsureAccClassSchema getSchema() {
        LBInsureAccClassSchema aLBInsureAccClassSchema = new LBInsureAccClassSchema();
        aLBInsureAccClassSchema.setSchema(this);
        return aLBInsureAccClassSchema;
    }

    public LBInsureAccClassDB getDB() {
        LBInsureAccClassDB aDBOper = new LBInsureAccClassDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBInsureAccClass描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(InsureAccClassID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccAscription)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccComputeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AccFoundDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccFoundTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( BalaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalaTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPay));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPaym));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LastAccBala));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LastUnitCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LastUnitPrice));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuAccBala));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(UnitCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuAccGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FrozenMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBInsureAccClass>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            InsureAccClassID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            OtherType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            AccAscription = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            AccComputeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            AccFoundDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            AccFoundTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            BalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            BalaTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            SumPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23, SysConst.PACKAGESPILTER))).doubleValue();
            SumPaym = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24, SysConst.PACKAGESPILTER))).doubleValue();
            LastAccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25, SysConst.PACKAGESPILTER))).doubleValue();
            LastUnitCount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26, SysConst.PACKAGESPILTER))).doubleValue();
            LastUnitPrice = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27, SysConst.PACKAGESPILTER))).doubleValue();
            InsuAccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28, SysConst.PACKAGESPILTER))).doubleValue();
            UnitCount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29, SysConst.PACKAGESPILTER))).doubleValue();
            InsuAccGetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30, SysConst.PACKAGESPILTER))).doubleValue();
            FrozenMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31, SysConst.PACKAGESPILTER))).doubleValue();
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsureAccClassSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsureAccClassID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureAccClassID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherType));
        }
        if (FCode.equalsIgnoreCase("AccAscription")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccAscription));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccComputeFlag));
        }
        if (FCode.equalsIgnoreCase("AccFoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccFoundDate()));
        }
        if (FCode.equalsIgnoreCase("AccFoundTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccFoundTime));
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
        }
        if (FCode.equalsIgnoreCase("BalaTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalaTime));
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equalsIgnoreCase("SumPaym")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPaym));
        }
        if (FCode.equalsIgnoreCase("LastAccBala")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastAccBala));
        }
        if (FCode.equalsIgnoreCase("LastUnitCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastUnitCount));
        }
        if (FCode.equalsIgnoreCase("LastUnitPrice")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastUnitPrice));
        }
        if (FCode.equalsIgnoreCase("InsuAccBala")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccBala));
        }
        if (FCode.equalsIgnoreCase("UnitCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitCount));
        }
        if (FCode.equalsIgnoreCase("InsuAccGetMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccGetMoney));
        }
        if (FCode.equalsIgnoreCase("FrozenMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FrozenMoney));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsureAccClassID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(OtherType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AccAscription);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AccComputeFlag);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccFoundDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(AccFoundTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(BalaTime);
                break;
            case 22:
                strFieldValue = String.valueOf(SumPay);
                break;
            case 23:
                strFieldValue = String.valueOf(SumPaym);
                break;
            case 24:
                strFieldValue = String.valueOf(LastAccBala);
                break;
            case 25:
                strFieldValue = String.valueOf(LastUnitCount);
                break;
            case 26:
                strFieldValue = String.valueOf(LastUnitPrice);
                break;
            case 27:
                strFieldValue = String.valueOf(InsuAccBala);
                break;
            case 28:
                strFieldValue = String.valueOf(UnitCount);
                break;
            case 29:
                strFieldValue = String.valueOf(InsuAccGetMoney);
                break;
            case 30:
                strFieldValue = String.valueOf(FrozenMoney);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsureAccClassID")) {
            if( FValue != null && !FValue.equals("")) {
                InsureAccClassID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherType = FValue.trim();
            }
            else
                OtherType = null;
        }
        if (FCode.equalsIgnoreCase("AccAscription")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccAscription = FValue.trim();
            }
            else
                AccAscription = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
                AccComputeFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccFoundDate")) {
            if(FValue != null && !FValue.equals("")) {
                AccFoundDate = fDate.getDate( FValue );
            }
            else
                AccFoundDate = null;
        }
        if (FCode.equalsIgnoreCase("AccFoundTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccFoundTime = FValue.trim();
            }
            else
                AccFoundTime = null;
        }
        if (FCode.equalsIgnoreCase("BalaDate")) {
            if(FValue != null && !FValue.equals("")) {
                BalaDate = fDate.getDate( FValue );
            }
            else
                BalaDate = null;
        }
        if (FCode.equalsIgnoreCase("BalaTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BalaTime = FValue.trim();
            }
            else
                BalaTime = null;
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPaym")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPaym = d;
            }
        }
        if (FCode.equalsIgnoreCase("LastAccBala")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                LastAccBala = d;
            }
        }
        if (FCode.equalsIgnoreCase("LastUnitCount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                LastUnitCount = d;
            }
        }
        if (FCode.equalsIgnoreCase("LastUnitPrice")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                LastUnitPrice = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuAccBala")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                InsuAccBala = d;
            }
        }
        if (FCode.equalsIgnoreCase("UnitCount")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                UnitCount = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuAccGetMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                InsuAccGetMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("FrozenMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FrozenMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBInsureAccClassSchema other = (LBInsureAccClassSchema)otherObject;
        return
            InsureAccClassID == other.getInsureAccClassID()
            && ShardingID.equals(other.getShardingID())
            && EdorNo.equals(other.getEdorNo())
            && GrpContNo.equals(other.getGrpContNo())
            && GrpPolNo.equals(other.getGrpPolNo())
            && ContNo.equals(other.getContNo())
            && ManageCom.equals(other.getManageCom())
            && PolNo.equals(other.getPolNo())
            && InsuAccNo.equals(other.getInsuAccNo())
            && PayPlanCode.equals(other.getPayPlanCode())
            && OtherNo.equals(other.getOtherNo())
            && OtherType.equals(other.getOtherType())
            && AccAscription.equals(other.getAccAscription())
            && RiskCode.equals(other.getRiskCode())
            && InsuredNo.equals(other.getInsuredNo())
            && AppntNo.equals(other.getAppntNo())
            && AccType.equals(other.getAccType())
            && AccComputeFlag.equals(other.getAccComputeFlag())
            && fDate.getString(AccFoundDate).equals(other.getAccFoundDate())
            && AccFoundTime.equals(other.getAccFoundTime())
            && fDate.getString(BalaDate).equals(other.getBalaDate())
            && BalaTime.equals(other.getBalaTime())
            && SumPay == other.getSumPay()
            && SumPaym == other.getSumPaym()
            && LastAccBala == other.getLastAccBala()
            && LastUnitCount == other.getLastUnitCount()
            && LastUnitPrice == other.getLastUnitPrice()
            && InsuAccBala == other.getInsuAccBala()
            && UnitCount == other.getUnitCount()
            && InsuAccGetMoney == other.getInsuAccGetMoney()
            && FrozenMoney == other.getFrozenMoney()
            && State.equals(other.getState())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsureAccClassID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 4;
        }
        if( strFieldName.equals("ContNo") ) {
            return 5;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 6;
        }
        if( strFieldName.equals("PolNo") ) {
            return 7;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 8;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 9;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 10;
        }
        if( strFieldName.equals("OtherType") ) {
            return 11;
        }
        if( strFieldName.equals("AccAscription") ) {
            return 12;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 13;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 14;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 15;
        }
        if( strFieldName.equals("AccType") ) {
            return 16;
        }
        if( strFieldName.equals("AccComputeFlag") ) {
            return 17;
        }
        if( strFieldName.equals("AccFoundDate") ) {
            return 18;
        }
        if( strFieldName.equals("AccFoundTime") ) {
            return 19;
        }
        if( strFieldName.equals("BalaDate") ) {
            return 20;
        }
        if( strFieldName.equals("BalaTime") ) {
            return 21;
        }
        if( strFieldName.equals("SumPay") ) {
            return 22;
        }
        if( strFieldName.equals("SumPaym") ) {
            return 23;
        }
        if( strFieldName.equals("LastAccBala") ) {
            return 24;
        }
        if( strFieldName.equals("LastUnitCount") ) {
            return 25;
        }
        if( strFieldName.equals("LastUnitPrice") ) {
            return 26;
        }
        if( strFieldName.equals("InsuAccBala") ) {
            return 27;
        }
        if( strFieldName.equals("UnitCount") ) {
            return 28;
        }
        if( strFieldName.equals("InsuAccGetMoney") ) {
            return 29;
        }
        if( strFieldName.equals("FrozenMoney") ) {
            return 30;
        }
        if( strFieldName.equals("State") ) {
            return 31;
        }
        if( strFieldName.equals("Operator") ) {
            return 32;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 33;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 34;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 35;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 36;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsureAccClassID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "GrpPolNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "ManageCom";
                break;
            case 7:
                strFieldName = "PolNo";
                break;
            case 8:
                strFieldName = "InsuAccNo";
                break;
            case 9:
                strFieldName = "PayPlanCode";
                break;
            case 10:
                strFieldName = "OtherNo";
                break;
            case 11:
                strFieldName = "OtherType";
                break;
            case 12:
                strFieldName = "AccAscription";
                break;
            case 13:
                strFieldName = "RiskCode";
                break;
            case 14:
                strFieldName = "InsuredNo";
                break;
            case 15:
                strFieldName = "AppntNo";
                break;
            case 16:
                strFieldName = "AccType";
                break;
            case 17:
                strFieldName = "AccComputeFlag";
                break;
            case 18:
                strFieldName = "AccFoundDate";
                break;
            case 19:
                strFieldName = "AccFoundTime";
                break;
            case 20:
                strFieldName = "BalaDate";
                break;
            case 21:
                strFieldName = "BalaTime";
                break;
            case 22:
                strFieldName = "SumPay";
                break;
            case 23:
                strFieldName = "SumPaym";
                break;
            case 24:
                strFieldName = "LastAccBala";
                break;
            case 25:
                strFieldName = "LastUnitCount";
                break;
            case 26:
                strFieldName = "LastUnitPrice";
                break;
            case 27:
                strFieldName = "InsuAccBala";
                break;
            case 28:
                strFieldName = "UnitCount";
                break;
            case 29:
                strFieldName = "InsuAccGetMoney";
                break;
            case 30:
                strFieldName = "FrozenMoney";
                break;
            case 31:
                strFieldName = "State";
                break;
            case 32:
                strFieldName = "Operator";
                break;
            case 33:
                strFieldName = "MakeDate";
                break;
            case 34:
                strFieldName = "MakeTime";
                break;
            case 35:
                strFieldName = "ModifyDate";
                break;
            case 36:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUREACCCLASSID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERTYPE":
                return Schema.TYPE_STRING;
            case "ACCASCRIPTION":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "ACCCOMPUTEFLAG":
                return Schema.TYPE_STRING;
            case "ACCFOUNDDATE":
                return Schema.TYPE_DATE;
            case "ACCFOUNDTIME":
                return Schema.TYPE_STRING;
            case "BALADATE":
                return Schema.TYPE_DATE;
            case "BALATIME":
                return Schema.TYPE_STRING;
            case "SUMPAY":
                return Schema.TYPE_DOUBLE;
            case "SUMPAYM":
                return Schema.TYPE_DOUBLE;
            case "LASTACCBALA":
                return Schema.TYPE_DOUBLE;
            case "LASTUNITCOUNT":
                return Schema.TYPE_DOUBLE;
            case "LASTUNITPRICE":
                return Schema.TYPE_DOUBLE;
            case "INSUACCBALA":
                return Schema.TYPE_DOUBLE;
            case "UNITCOUNT":
                return Schema.TYPE_DOUBLE;
            case "INSUACCGETMONEY":
                return Schema.TYPE_DOUBLE;
            case "FROZENMONEY":
                return Schema.TYPE_DOUBLE;
            case "STATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_DOUBLE;
            case 25:
                return Schema.TYPE_DOUBLE;
            case 26:
                return Schema.TYPE_DOUBLE;
            case 27:
                return Schema.TYPE_DOUBLE;
            case 28:
                return Schema.TYPE_DOUBLE;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_DOUBLE;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_DATE;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_DATE;
            case 36:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
