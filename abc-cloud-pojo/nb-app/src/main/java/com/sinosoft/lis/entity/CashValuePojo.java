package com.sinosoft.lis.entity;/**
 * Created by fei on 2017/9/25.
 */

import java.io.Serializable;
import java.util.List;

/**
 * @author malili
 * @create 2017-09-25 16:21
 * @desc 现金价值表所对应的实体类
 **/

public class CashValuePojo implements Serializable {
    //险种编码
    private String riskCode;

    private List<String> cashValue;

    public CashValuePojo(String riskCode, List<String> cashValue) {
        this.riskCode = riskCode;
        this.cashValue = cashValue;
    }

    public CashValuePojo() {
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public List<String> getCashValue() {
        return cashValue;
    }

    public void setCashValue(List<String> cashValue) {
        this.cashValue = cashValue;
    }

    @Override
    public String toString() {
        return "CashValuePojo{" +
                "riskCode='" + riskCode + '\'' +
                ", cashValue=" + cashValue +
                '}';
    }
}
