/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyGetClmPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyGetClmPojo implements Pojo,Serializable {
    // @Field
    /** 给付代码 */
    @RedisIndexHKey
    @RedisPrimaryHKey
    private String GetDutyCode; 
    /** 给付名称 */
    private String GetDutyName; 
    /** 给付责任类型 */
    @RedisPrimaryHKey
    private String GetDutyKind; 
    /** 默认值 */
    private double DefaultVal; 
    /** 算法 */
    private String CalCode; 
    /** 反算算法 */
    private String CnterCalCode; 
    /** 其他算法 */
    private String OthCalCode; 
    /** 录入标记 */
    private String InpFlag; 
    /** 统计类别 */
    private String StatType; 
    /** 起付限 */
    private double MinGet; 
    /** 给付后动作 */
    private String AfterGet; 
    /** 赔付限额 */
    private double MaxGet; 
    /** 赔付比例 */
    private double ClaimRate; 
    /** 赔付天数限额 */
    private int ClmDayLmt; 
    /** 累计赔付天数限额 */
    private int SumClmDayLmt; 
    /** 免赔额 */
    private double Deductible; 
    /** 免赔天数 */
    private int DeDuctDay; 
    /** 观察期 */
    private int ObsPeriod; 
    /** 被保人死亡后有效标记 */
    private String DeadValiFlag; 
    /** 死亡给付与现值关系 */
    private String DeadToPValueFlag; 
    /** 领取时是否需要重新计算 */
    private String NeedReCompute; 
    /** 给付类型 */
    private String CasePolType; 
    /** 伤残级别 */
    private String DeformityGrade; 
    /** 责任匹配算法 */
    private String FilterCalCode; 
    /** 赔付影响主险类型 */
    private String EffectOnMainRisk; 


    public static final int FIELDNUM = 25;    // 数据库表的字段个数
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyName() {
        return GetDutyName;
    }
    public void setGetDutyName(String aGetDutyName) {
        GetDutyName = aGetDutyName;
    }
    public String getGetDutyKind() {
        return GetDutyKind;
    }
    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }
    public double getDefaultVal() {
        return DefaultVal;
    }
    public void setDefaultVal(double aDefaultVal) {
        DefaultVal = aDefaultVal;
    }
    public void setDefaultVal(String aDefaultVal) {
        if (aDefaultVal != null && !aDefaultVal.equals("")) {
            Double tDouble = new Double(aDefaultVal);
            double d = tDouble.doubleValue();
            DefaultVal = d;
        }
    }

    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getCnterCalCode() {
        return CnterCalCode;
    }
    public void setCnterCalCode(String aCnterCalCode) {
        CnterCalCode = aCnterCalCode;
    }
    public String getOthCalCode() {
        return OthCalCode;
    }
    public void setOthCalCode(String aOthCalCode) {
        OthCalCode = aOthCalCode;
    }
    public String getInpFlag() {
        return InpFlag;
    }
    public void setInpFlag(String aInpFlag) {
        InpFlag = aInpFlag;
    }
    public String getStatType() {
        return StatType;
    }
    public void setStatType(String aStatType) {
        StatType = aStatType;
    }
    public double getMinGet() {
        return MinGet;
    }
    public void setMinGet(double aMinGet) {
        MinGet = aMinGet;
    }
    public void setMinGet(String aMinGet) {
        if (aMinGet != null && !aMinGet.equals("")) {
            Double tDouble = new Double(aMinGet);
            double d = tDouble.doubleValue();
            MinGet = d;
        }
    }

    public String getAfterGet() {
        return AfterGet;
    }
    public void setAfterGet(String aAfterGet) {
        AfterGet = aAfterGet;
    }
    public double getMaxGet() {
        return MaxGet;
    }
    public void setMaxGet(double aMaxGet) {
        MaxGet = aMaxGet;
    }
    public void setMaxGet(String aMaxGet) {
        if (aMaxGet != null && !aMaxGet.equals("")) {
            Double tDouble = new Double(aMaxGet);
            double d = tDouble.doubleValue();
            MaxGet = d;
        }
    }

    public double getClaimRate() {
        return ClaimRate;
    }
    public void setClaimRate(double aClaimRate) {
        ClaimRate = aClaimRate;
    }
    public void setClaimRate(String aClaimRate) {
        if (aClaimRate != null && !aClaimRate.equals("")) {
            Double tDouble = new Double(aClaimRate);
            double d = tDouble.doubleValue();
            ClaimRate = d;
        }
    }

    public int getClmDayLmt() {
        return ClmDayLmt;
    }
    public void setClmDayLmt(int aClmDayLmt) {
        ClmDayLmt = aClmDayLmt;
    }
    public void setClmDayLmt(String aClmDayLmt) {
        if (aClmDayLmt != null && !aClmDayLmt.equals("")) {
            Integer tInteger = new Integer(aClmDayLmt);
            int i = tInteger.intValue();
            ClmDayLmt = i;
        }
    }

    public int getSumClmDayLmt() {
        return SumClmDayLmt;
    }
    public void setSumClmDayLmt(int aSumClmDayLmt) {
        SumClmDayLmt = aSumClmDayLmt;
    }
    public void setSumClmDayLmt(String aSumClmDayLmt) {
        if (aSumClmDayLmt != null && !aSumClmDayLmt.equals("")) {
            Integer tInteger = new Integer(aSumClmDayLmt);
            int i = tInteger.intValue();
            SumClmDayLmt = i;
        }
    }

    public double getDeductible() {
        return Deductible;
    }
    public void setDeductible(double aDeductible) {
        Deductible = aDeductible;
    }
    public void setDeductible(String aDeductible) {
        if (aDeductible != null && !aDeductible.equals("")) {
            Double tDouble = new Double(aDeductible);
            double d = tDouble.doubleValue();
            Deductible = d;
        }
    }

    public int getDeDuctDay() {
        return DeDuctDay;
    }
    public void setDeDuctDay(int aDeDuctDay) {
        DeDuctDay = aDeDuctDay;
    }
    public void setDeDuctDay(String aDeDuctDay) {
        if (aDeDuctDay != null && !aDeDuctDay.equals("")) {
            Integer tInteger = new Integer(aDeDuctDay);
            int i = tInteger.intValue();
            DeDuctDay = i;
        }
    }

    public int getObsPeriod() {
        return ObsPeriod;
    }
    public void setObsPeriod(int aObsPeriod) {
        ObsPeriod = aObsPeriod;
    }
    public void setObsPeriod(String aObsPeriod) {
        if (aObsPeriod != null && !aObsPeriod.equals("")) {
            Integer tInteger = new Integer(aObsPeriod);
            int i = tInteger.intValue();
            ObsPeriod = i;
        }
    }

    public String getDeadValiFlag() {
        return DeadValiFlag;
    }
    public void setDeadValiFlag(String aDeadValiFlag) {
        DeadValiFlag = aDeadValiFlag;
    }
    public String getDeadToPValueFlag() {
        return DeadToPValueFlag;
    }
    public void setDeadToPValueFlag(String aDeadToPValueFlag) {
        DeadToPValueFlag = aDeadToPValueFlag;
    }
    public String getNeedReCompute() {
        return NeedReCompute;
    }
    public void setNeedReCompute(String aNeedReCompute) {
        NeedReCompute = aNeedReCompute;
    }
    public String getCasePolType() {
        return CasePolType;
    }
    public void setCasePolType(String aCasePolType) {
        CasePolType = aCasePolType;
    }
    public String getDeformityGrade() {
        return DeformityGrade;
    }
    public void setDeformityGrade(String aDeformityGrade) {
        DeformityGrade = aDeformityGrade;
    }
    public String getFilterCalCode() {
        return FilterCalCode;
    }
    public void setFilterCalCode(String aFilterCalCode) {
        FilterCalCode = aFilterCalCode;
    }
    public String getEffectOnMainRisk() {
        return EffectOnMainRisk;
    }
    public void setEffectOnMainRisk(String aEffectOnMainRisk) {
        EffectOnMainRisk = aEffectOnMainRisk;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GetDutyCode") ) {
            return 0;
        }
        if( strFieldName.equals("GetDutyName") ) {
            return 1;
        }
        if( strFieldName.equals("GetDutyKind") ) {
            return 2;
        }
        if( strFieldName.equals("DefaultVal") ) {
            return 3;
        }
        if( strFieldName.equals("CalCode") ) {
            return 4;
        }
        if( strFieldName.equals("CnterCalCode") ) {
            return 5;
        }
        if( strFieldName.equals("OthCalCode") ) {
            return 6;
        }
        if( strFieldName.equals("InpFlag") ) {
            return 7;
        }
        if( strFieldName.equals("StatType") ) {
            return 8;
        }
        if( strFieldName.equals("MinGet") ) {
            return 9;
        }
        if( strFieldName.equals("AfterGet") ) {
            return 10;
        }
        if( strFieldName.equals("MaxGet") ) {
            return 11;
        }
        if( strFieldName.equals("ClaimRate") ) {
            return 12;
        }
        if( strFieldName.equals("ClmDayLmt") ) {
            return 13;
        }
        if( strFieldName.equals("SumClmDayLmt") ) {
            return 14;
        }
        if( strFieldName.equals("Deductible") ) {
            return 15;
        }
        if( strFieldName.equals("DeDuctDay") ) {
            return 16;
        }
        if( strFieldName.equals("ObsPeriod") ) {
            return 17;
        }
        if( strFieldName.equals("DeadValiFlag") ) {
            return 18;
        }
        if( strFieldName.equals("DeadToPValueFlag") ) {
            return 19;
        }
        if( strFieldName.equals("NeedReCompute") ) {
            return 20;
        }
        if( strFieldName.equals("CasePolType") ) {
            return 21;
        }
        if( strFieldName.equals("DeformityGrade") ) {
            return 22;
        }
        if( strFieldName.equals("FilterCalCode") ) {
            return 23;
        }
        if( strFieldName.equals("EffectOnMainRisk") ) {
            return 24;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GetDutyCode";
                break;
            case 1:
                strFieldName = "GetDutyName";
                break;
            case 2:
                strFieldName = "GetDutyKind";
                break;
            case 3:
                strFieldName = "DefaultVal";
                break;
            case 4:
                strFieldName = "CalCode";
                break;
            case 5:
                strFieldName = "CnterCalCode";
                break;
            case 6:
                strFieldName = "OthCalCode";
                break;
            case 7:
                strFieldName = "InpFlag";
                break;
            case 8:
                strFieldName = "StatType";
                break;
            case 9:
                strFieldName = "MinGet";
                break;
            case 10:
                strFieldName = "AfterGet";
                break;
            case 11:
                strFieldName = "MaxGet";
                break;
            case 12:
                strFieldName = "ClaimRate";
                break;
            case 13:
                strFieldName = "ClmDayLmt";
                break;
            case 14:
                strFieldName = "SumClmDayLmt";
                break;
            case 15:
                strFieldName = "Deductible";
                break;
            case 16:
                strFieldName = "DeDuctDay";
                break;
            case 17:
                strFieldName = "ObsPeriod";
                break;
            case 18:
                strFieldName = "DeadValiFlag";
                break;
            case 19:
                strFieldName = "DeadToPValueFlag";
                break;
            case 20:
                strFieldName = "NeedReCompute";
                break;
            case 21:
                strFieldName = "CasePolType";
                break;
            case 22:
                strFieldName = "DeformityGrade";
                break;
            case 23:
                strFieldName = "FilterCalCode";
                break;
            case 24:
                strFieldName = "EffectOnMainRisk";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYNAME":
                return Schema.TYPE_STRING;
            case "GETDUTYKIND":
                return Schema.TYPE_STRING;
            case "DEFAULTVAL":
                return Schema.TYPE_DOUBLE;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "CNTERCALCODE":
                return Schema.TYPE_STRING;
            case "OTHCALCODE":
                return Schema.TYPE_STRING;
            case "INPFLAG":
                return Schema.TYPE_STRING;
            case "STATTYPE":
                return Schema.TYPE_STRING;
            case "MINGET":
                return Schema.TYPE_DOUBLE;
            case "AFTERGET":
                return Schema.TYPE_STRING;
            case "MAXGET":
                return Schema.TYPE_DOUBLE;
            case "CLAIMRATE":
                return Schema.TYPE_DOUBLE;
            case "CLMDAYLMT":
                return Schema.TYPE_INT;
            case "SUMCLMDAYLMT":
                return Schema.TYPE_INT;
            case "DEDUCTIBLE":
                return Schema.TYPE_DOUBLE;
            case "DEDUCTDAY":
                return Schema.TYPE_INT;
            case "OBSPERIOD":
                return Schema.TYPE_INT;
            case "DEADVALIFLAG":
                return Schema.TYPE_STRING;
            case "DEADTOPVALUEFLAG":
                return Schema.TYPE_STRING;
            case "NEEDRECOMPUTE":
                return Schema.TYPE_STRING;
            case "CASEPOLTYPE":
                return Schema.TYPE_STRING;
            case "DEFORMITYGRADE":
                return Schema.TYPE_STRING;
            case "FILTERCALCODE":
                return Schema.TYPE_STRING;
            case "EFFECTONMAINRISK":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_DOUBLE;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_INT;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_INT;
            case 17:
                return Schema.TYPE_INT;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equalsIgnoreCase("DefaultVal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultVal));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("CnterCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CnterCalCode));
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthCalCode));
        }
        if (FCode.equalsIgnoreCase("InpFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InpFlag));
        }
        if (FCode.equalsIgnoreCase("StatType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StatType));
        }
        if (FCode.equalsIgnoreCase("MinGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinGet));
        }
        if (FCode.equalsIgnoreCase("AfterGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AfterGet));
        }
        if (FCode.equalsIgnoreCase("MaxGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxGet));
        }
        if (FCode.equalsIgnoreCase("ClaimRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimRate));
        }
        if (FCode.equalsIgnoreCase("ClmDayLmt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmDayLmt));
        }
        if (FCode.equalsIgnoreCase("SumClmDayLmt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumClmDayLmt));
        }
        if (FCode.equalsIgnoreCase("Deductible")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Deductible));
        }
        if (FCode.equalsIgnoreCase("DeDuctDay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeDuctDay));
        }
        if (FCode.equalsIgnoreCase("ObsPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObsPeriod));
        }
        if (FCode.equalsIgnoreCase("DeadValiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadValiFlag));
        }
        if (FCode.equalsIgnoreCase("DeadToPValueFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadToPValueFlag));
        }
        if (FCode.equalsIgnoreCase("NeedReCompute")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedReCompute));
        }
        if (FCode.equalsIgnoreCase("CasePolType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CasePolType));
        }
        if (FCode.equalsIgnoreCase("DeformityGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeformityGrade));
        }
        if (FCode.equalsIgnoreCase("FilterCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FilterCalCode));
        }
        if (FCode.equalsIgnoreCase("EffectOnMainRisk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EffectOnMainRisk));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GetDutyCode);
                break;
            case 1:
                strFieldValue = String.valueOf(GetDutyName);
                break;
            case 2:
                strFieldValue = String.valueOf(GetDutyKind);
                break;
            case 3:
                strFieldValue = String.valueOf(DefaultVal);
                break;
            case 4:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 5:
                strFieldValue = String.valueOf(CnterCalCode);
                break;
            case 6:
                strFieldValue = String.valueOf(OthCalCode);
                break;
            case 7:
                strFieldValue = String.valueOf(InpFlag);
                break;
            case 8:
                strFieldValue = String.valueOf(StatType);
                break;
            case 9:
                strFieldValue = String.valueOf(MinGet);
                break;
            case 10:
                strFieldValue = String.valueOf(AfterGet);
                break;
            case 11:
                strFieldValue = String.valueOf(MaxGet);
                break;
            case 12:
                strFieldValue = String.valueOf(ClaimRate);
                break;
            case 13:
                strFieldValue = String.valueOf(ClmDayLmt);
                break;
            case 14:
                strFieldValue = String.valueOf(SumClmDayLmt);
                break;
            case 15:
                strFieldValue = String.valueOf(Deductible);
                break;
            case 16:
                strFieldValue = String.valueOf(DeDuctDay);
                break;
            case 17:
                strFieldValue = String.valueOf(ObsPeriod);
                break;
            case 18:
                strFieldValue = String.valueOf(DeadValiFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(DeadToPValueFlag);
                break;
            case 20:
                strFieldValue = String.valueOf(NeedReCompute);
                break;
            case 21:
                strFieldValue = String.valueOf(CasePolType);
                break;
            case 22:
                strFieldValue = String.valueOf(DeformityGrade);
                break;
            case 23:
                strFieldValue = String.valueOf(FilterCalCode);
                break;
            case 24:
                strFieldValue = String.valueOf(EffectOnMainRisk);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
                GetDutyName = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
                GetDutyKind = null;
        }
        if (FCode.equalsIgnoreCase("DefaultVal")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultVal = d;
            }
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("CnterCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CnterCalCode = FValue.trim();
            }
            else
                CnterCalCode = null;
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthCalCode = FValue.trim();
            }
            else
                OthCalCode = null;
        }
        if (FCode.equalsIgnoreCase("InpFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InpFlag = FValue.trim();
            }
            else
                InpFlag = null;
        }
        if (FCode.equalsIgnoreCase("StatType")) {
            if( FValue != null && !FValue.equals(""))
            {
                StatType = FValue.trim();
            }
            else
                StatType = null;
        }
        if (FCode.equalsIgnoreCase("MinGet")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MinGet = d;
            }
        }
        if (FCode.equalsIgnoreCase("AfterGet")) {
            if( FValue != null && !FValue.equals(""))
            {
                AfterGet = FValue.trim();
            }
            else
                AfterGet = null;
        }
        if (FCode.equalsIgnoreCase("MaxGet")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MaxGet = d;
            }
        }
        if (FCode.equalsIgnoreCase("ClaimRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ClaimRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("ClmDayLmt")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ClmDayLmt = i;
            }
        }
        if (FCode.equalsIgnoreCase("SumClmDayLmt")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SumClmDayLmt = i;
            }
        }
        if (FCode.equalsIgnoreCase("Deductible")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Deductible = d;
            }
        }
        if (FCode.equalsIgnoreCase("DeDuctDay")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                DeDuctDay = i;
            }
        }
        if (FCode.equalsIgnoreCase("ObsPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ObsPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("DeadValiFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadValiFlag = FValue.trim();
            }
            else
                DeadValiFlag = null;
        }
        if (FCode.equalsIgnoreCase("DeadToPValueFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadToPValueFlag = FValue.trim();
            }
            else
                DeadToPValueFlag = null;
        }
        if (FCode.equalsIgnoreCase("NeedReCompute")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedReCompute = FValue.trim();
            }
            else
                NeedReCompute = null;
        }
        if (FCode.equalsIgnoreCase("CasePolType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CasePolType = FValue.trim();
            }
            else
                CasePolType = null;
        }
        if (FCode.equalsIgnoreCase("DeformityGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeformityGrade = FValue.trim();
            }
            else
                DeformityGrade = null;
        }
        if (FCode.equalsIgnoreCase("FilterCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FilterCalCode = FValue.trim();
            }
            else
                FilterCalCode = null;
        }
        if (FCode.equalsIgnoreCase("EffectOnMainRisk")) {
            if( FValue != null && !FValue.equals(""))
            {
                EffectOnMainRisk = FValue.trim();
            }
            else
                EffectOnMainRisk = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyGetClmPojo [" +
            "GetDutyCode="+GetDutyCode +
            ", GetDutyName="+GetDutyName +
            ", GetDutyKind="+GetDutyKind +
            ", DefaultVal="+DefaultVal +
            ", CalCode="+CalCode +
            ", CnterCalCode="+CnterCalCode +
            ", OthCalCode="+OthCalCode +
            ", InpFlag="+InpFlag +
            ", StatType="+StatType +
            ", MinGet="+MinGet +
            ", AfterGet="+AfterGet +
            ", MaxGet="+MaxGet +
            ", ClaimRate="+ClaimRate +
            ", ClmDayLmt="+ClmDayLmt +
            ", SumClmDayLmt="+SumClmDayLmt +
            ", Deductible="+Deductible +
            ", DeDuctDay="+DeDuctDay +
            ", ObsPeriod="+ObsPeriod +
            ", DeadValiFlag="+DeadValiFlag +
            ", DeadToPValueFlag="+DeadToPValueFlag +
            ", NeedReCompute="+NeedReCompute +
            ", CasePolType="+CasePolType +
            ", DeformityGrade="+DeformityGrade +
            ", FilterCalCode="+FilterCalCode +
            ", EffectOnMainRisk="+EffectOnMainRisk +"]";
    }
}
