/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCAgentToContDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCAgentToContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCAgentToContSchema implements Schema, Cloneable {
    // @Field
    /** 保单号码 */
    private String PolicyNo;
    /** 销售渠道 */
    private String SaleChnl;
    /** 客户经理编码 */
    private String AgentCode;
    /** 客户经理姓名 */
    private String AgentName;
    /** 展业机构编码 */
    private String AgentGroup;
    /** 所属机构 */
    private String AgentManageCom;
    /** 分佣比例 */
    private double CommBusiRate;
    /** 管理机构 */
    private String ManageCom;
    /** 公司代码 */
    private String ComCode;
    /** 入机操作员 */
    private String MakeOperator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改操作员 */
    private String ModifyOperator;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCAgentToContSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "PolicyNo";
        pk[1] = "SaleChnl";
        pk[2] = "AgentCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCAgentToContSchema cloned = (LCAgentToContSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }
    public void setPolicyNo(String aPolicyNo) {
        PolicyNo = aPolicyNo;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentManageCom() {
        return AgentManageCom;
    }
    public void setAgentManageCom(String aAgentManageCom) {
        AgentManageCom = aAgentManageCom;
    }
    public double getCommBusiRate() {
        return CommBusiRate;
    }
    public void setCommBusiRate(double aCommBusiRate) {
        CommBusiRate = aCommBusiRate;
    }
    public void setCommBusiRate(String aCommBusiRate) {
        if (aCommBusiRate != null && !aCommBusiRate.equals("")) {
            Double tDouble = new Double(aCommBusiRate);
            double d = tDouble.doubleValue();
            CommBusiRate = d;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getMakeOperator() {
        return MakeOperator;
    }
    public void setMakeOperator(String aMakeOperator) {
        MakeOperator = aMakeOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 使用另外一个 LCAgentToContSchema 对象给 Schema 赋值
    * @param: aLCAgentToContSchema LCAgentToContSchema
    **/
    public void setSchema(LCAgentToContSchema aLCAgentToContSchema) {
        this.PolicyNo = aLCAgentToContSchema.getPolicyNo();
        this.SaleChnl = aLCAgentToContSchema.getSaleChnl();
        this.AgentCode = aLCAgentToContSchema.getAgentCode();
        this.AgentName = aLCAgentToContSchema.getAgentName();
        this.AgentGroup = aLCAgentToContSchema.getAgentGroup();
        this.AgentManageCom = aLCAgentToContSchema.getAgentManageCom();
        this.CommBusiRate = aLCAgentToContSchema.getCommBusiRate();
        this.ManageCom = aLCAgentToContSchema.getManageCom();
        this.ComCode = aLCAgentToContSchema.getComCode();
        this.MakeOperator = aLCAgentToContSchema.getMakeOperator();
        this.MakeDate = fDate.getDate( aLCAgentToContSchema.getMakeDate());
        this.MakeTime = aLCAgentToContSchema.getMakeTime();
        this.ModifyOperator = aLCAgentToContSchema.getModifyOperator();
        this.ModifyDate = fDate.getDate( aLCAgentToContSchema.getModifyDate());
        this.ModifyTime = aLCAgentToContSchema.getModifyTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("PolicyNo") == null )
                this.PolicyNo = null;
            else
                this.PolicyNo = rs.getString("PolicyNo").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentName") == null )
                this.AgentName = null;
            else
                this.AgentName = rs.getString("AgentName").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("AgentManageCom") == null )
                this.AgentManageCom = null;
            else
                this.AgentManageCom = rs.getString("AgentManageCom").trim();

            this.CommBusiRate = rs.getDouble("CommBusiRate");
            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("ComCode") == null )
                this.ComCode = null;
            else
                this.ComCode = rs.getString("ComCode").trim();

            if( rs.getString("MakeOperator") == null )
                this.MakeOperator = null;
            else
                this.MakeOperator = rs.getString("MakeOperator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            if( rs.getString("ModifyOperator") == null )
                this.ModifyOperator = null;
            else
                this.ModifyOperator = rs.getString("ModifyOperator").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAgentToContSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCAgentToContSchema getSchema() {
        LCAgentToContSchema aLCAgentToContSchema = new LCAgentToContSchema();
        aLCAgentToContSchema.setSchema(this);
        return aLCAgentToContSchema;
    }

    public LCAgentToContDB getDB() {
        LCAgentToContDB aDBOper = new LCAgentToContDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAgentToCont描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CommBusiRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAgentToCont>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            PolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AgentManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            CommBusiRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7, SysConst.PACKAGESPILTER))).doubleValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            MakeOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ModifyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAgentToContSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentManageCom));
        }
        if (FCode.equalsIgnoreCase("CommBusiRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CommBusiRate));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeOperator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PolicyNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AgentManageCom);
                break;
            case 6:
                strFieldValue = String.valueOf(CommBusiRate);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeOperator);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ModifyOperator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyNo = FValue.trim();
            }
            else
                PolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentManageCom = FValue.trim();
            }
            else
                AgentManageCom = null;
        }
        if (FCode.equalsIgnoreCase("CommBusiRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                CommBusiRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeOperator = FValue.trim();
            }
            else
                MakeOperator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCAgentToContSchema other = (LCAgentToContSchema)otherObject;
        return
            PolicyNo.equals(other.getPolicyNo())
            && SaleChnl.equals(other.getSaleChnl())
            && AgentCode.equals(other.getAgentCode())
            && AgentName.equals(other.getAgentName())
            && AgentGroup.equals(other.getAgentGroup())
            && AgentManageCom.equals(other.getAgentManageCom())
            && CommBusiRate == other.getCommBusiRate()
            && ManageCom.equals(other.getManageCom())
            && ComCode.equals(other.getComCode())
            && MakeOperator.equals(other.getMakeOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && ModifyOperator.equals(other.getModifyOperator())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PolicyNo") ) {
            return 0;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 1;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 2;
        }
        if( strFieldName.equals("AgentName") ) {
            return 3;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 4;
        }
        if( strFieldName.equals("AgentManageCom") ) {
            return 5;
        }
        if( strFieldName.equals("CommBusiRate") ) {
            return 6;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 7;
        }
        if( strFieldName.equals("ComCode") ) {
            return 8;
        }
        if( strFieldName.equals("MakeOperator") ) {
            return 9;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 10;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 11;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 12;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 13;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 14;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PolicyNo";
                break;
            case 1:
                strFieldName = "SaleChnl";
                break;
            case 2:
                strFieldName = "AgentCode";
                break;
            case 3:
                strFieldName = "AgentName";
                break;
            case 4:
                strFieldName = "AgentGroup";
                break;
            case 5:
                strFieldName = "AgentManageCom";
                break;
            case 6:
                strFieldName = "CommBusiRate";
                break;
            case 7:
                strFieldName = "ManageCom";
                break;
            case 8:
                strFieldName = "ComCode";
                break;
            case 9:
                strFieldName = "MakeOperator";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "ModifyOperator";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTMANAGECOM":
                return Schema.TYPE_STRING;
            case "COMMBUSIRATE":
                return Schema.TYPE_DOUBLE;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "MAKEOPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
