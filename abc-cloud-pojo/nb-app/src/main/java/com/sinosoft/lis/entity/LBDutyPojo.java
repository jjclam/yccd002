/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBDutyPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBDutyPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long DutyID; 
    /** Shardingid */
    private String ShardingID; 
    /** 批单号 */
    private String EdorNo; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 责任编码 */
    private String DutyCode; 
    /** 合同号码 */
    private String ContNo; 
    /** 份数 */
    private double Mult; 
    /** 标准保费 */
    private double StandPrem; 
    /** 实际保费 */
    private double Prem; 
    /** 累计保费 */
    private double SumPrem; 
    /** 基本保额 */
    private double Amnt; 
    /** 风险保额 */
    private double RiskAmnt; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 交费年期 */
    private int PayYears; 
    /** 保险年期 */
    private int Years; 
    /** 浮动费率 */
    private double FloatRate; 
    /** 首期交费日期 */
    private String  FirstPayDate;
    /** 首期交费月数 */
    private int FirstMonth; 
    /** 交至日期 */
    private String  PaytoDate;
    /** 终交日期 */
    private String  PayEndDate;
    /** 终交年龄年期标志 */
    private String PayEndYearFlag; 
    /** 终交年龄年期 */
    private int PayEndYear; 
    /** 领取年龄年期标志 */
    private String GetYearFlag; 
    /** 领取年龄年期 */
    private int GetYear; 
    /** 保险年龄年期标志 */
    private String InsuYearFlag; 
    /** 保险年龄年期 */
    private int InsuYear; 
    /** 意外年龄年期标志 */
    private String AcciYearFlag; 
    /** 意外年龄年期 */
    private int AcciYear; 
    /** 保险责任终止日期 */
    private String  EndDate;
    /** 意外责任终止日期 */
    private String  AcciEndDate;
    /** 免交标志 */
    private String FreeFlag; 
    /** 免交比率 */
    private double FreeRate; 
    /** 免交起期 */
    private String  FreeStartDate;
    /** 免交止期 */
    private String  FreeEndDate;
    /** 起领日期 */
    private String  GetStartDate;
    /** 起领日期计算类型 */
    private String GetStartType; 
    /** 生存金领取方式 */
    private String LiveGetMode; 
    /** 身故金领取方式 */
    private String DeadGetMode; 
    /** 红利金领取方式 */
    private String BonusGetMode; 
    /** 社保标记 */
    private String SSFlag; 
    /** 封顶线 */
    private double PeakLine; 
    /** 起付限 */
    private double GetLimit; 
    /** 赔付比例 */
    private double GetRate; 
    /** 保费计算规则 */
    private String CalRule; 
    /** 保费算保额标志 */
    private String PremToAmnt; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 险种责任生效日期 */
    private String  CValiDate;
    /** 领取间隔 */
    private int GetIntv; 
    /** 归属规则 */
    private String AscriptionRuleCode; 
    /** 缴费规则 */
    private String PayRuleCode; 
    /** 团险标准费率保费 */
    private double StandRatePrem; 
    /** 期交保费 */
    private double PeriodPrem; 


    public static final int FIELDNUM = 59;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getDutyID() {
        return DutyID;
    }
    public void setDutyID(long aDutyID) {
        DutyID = aDutyID;
    }
    public void setDutyID(String aDutyID) {
        if (aDutyID != null && !aDutyID.equals("")) {
            DutyID = new Long(aDutyID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getStandPrem() {
        return StandPrem;
    }
    public void setStandPrem(double aStandPrem) {
        StandPrem = aStandPrem;
    }
    public void setStandPrem(String aStandPrem) {
        if (aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getRiskAmnt() {
        return RiskAmnt;
    }
    public void setRiskAmnt(double aRiskAmnt) {
        RiskAmnt = aRiskAmnt;
    }
    public void setRiskAmnt(String aRiskAmnt) {
        if (aRiskAmnt != null && !aRiskAmnt.equals("")) {
            Double tDouble = new Double(aRiskAmnt);
            double d = tDouble.doubleValue();
            RiskAmnt = d;
        }
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public int getPayYears() {
        return PayYears;
    }
    public void setPayYears(int aPayYears) {
        PayYears = aPayYears;
    }
    public void setPayYears(String aPayYears) {
        if (aPayYears != null && !aPayYears.equals("")) {
            Integer tInteger = new Integer(aPayYears);
            int i = tInteger.intValue();
            PayYears = i;
        }
    }

    public int getYears() {
        return Years;
    }
    public void setYears(int aYears) {
        Years = aYears;
    }
    public void setYears(String aYears) {
        if (aYears != null && !aYears.equals("")) {
            Integer tInteger = new Integer(aYears);
            int i = tInteger.intValue();
            Years = i;
        }
    }

    public double getFloatRate() {
        return FloatRate;
    }
    public void setFloatRate(double aFloatRate) {
        FloatRate = aFloatRate;
    }
    public void setFloatRate(String aFloatRate) {
        if (aFloatRate != null && !aFloatRate.equals("")) {
            Double tDouble = new Double(aFloatRate);
            double d = tDouble.doubleValue();
            FloatRate = d;
        }
    }

    public String getFirstPayDate() {
        return FirstPayDate;
    }
    public void setFirstPayDate(String aFirstPayDate) {
        FirstPayDate = aFirstPayDate;
    }
    public int getFirstMonth() {
        return FirstMonth;
    }
    public void setFirstMonth(int aFirstMonth) {
        FirstMonth = aFirstMonth;
    }
    public void setFirstMonth(String aFirstMonth) {
        if (aFirstMonth != null && !aFirstMonth.equals("")) {
            Integer tInteger = new Integer(aFirstMonth);
            int i = tInteger.intValue();
            FirstMonth = i;
        }
    }

    public String getPaytoDate() {
        return PaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public String getPayEndDate() {
        return PayEndDate;
    }
    public void setPayEndDate(String aPayEndDate) {
        PayEndDate = aPayEndDate;
    }
    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }
    public void setPayEndYearFlag(String aPayEndYearFlag) {
        PayEndYearFlag = aPayEndYearFlag;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }
    public void setGetYearFlag(String aGetYearFlag) {
        GetYearFlag = aGetYearFlag;
    }
    public int getGetYear() {
        return GetYear;
    }
    public void setGetYear(int aGetYear) {
        GetYear = aGetYear;
    }
    public void setGetYear(String aGetYear) {
        if (aGetYear != null && !aGetYear.equals("")) {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getAcciYearFlag() {
        return AcciYearFlag;
    }
    public void setAcciYearFlag(String aAcciYearFlag) {
        AcciYearFlag = aAcciYearFlag;
    }
    public int getAcciYear() {
        return AcciYear;
    }
    public void setAcciYear(int aAcciYear) {
        AcciYear = aAcciYear;
    }
    public void setAcciYear(String aAcciYear) {
        if (aAcciYear != null && !aAcciYear.equals("")) {
            Integer tInteger = new Integer(aAcciYear);
            int i = tInteger.intValue();
            AcciYear = i;
        }
    }

    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getAcciEndDate() {
        return AcciEndDate;
    }
    public void setAcciEndDate(String aAcciEndDate) {
        AcciEndDate = aAcciEndDate;
    }
    public String getFreeFlag() {
        return FreeFlag;
    }
    public void setFreeFlag(String aFreeFlag) {
        FreeFlag = aFreeFlag;
    }
    public double getFreeRate() {
        return FreeRate;
    }
    public void setFreeRate(double aFreeRate) {
        FreeRate = aFreeRate;
    }
    public void setFreeRate(String aFreeRate) {
        if (aFreeRate != null && !aFreeRate.equals("")) {
            Double tDouble = new Double(aFreeRate);
            double d = tDouble.doubleValue();
            FreeRate = d;
        }
    }

    public String getFreeStartDate() {
        return FreeStartDate;
    }
    public void setFreeStartDate(String aFreeStartDate) {
        FreeStartDate = aFreeStartDate;
    }
    public String getFreeEndDate() {
        return FreeEndDate;
    }
    public void setFreeEndDate(String aFreeEndDate) {
        FreeEndDate = aFreeEndDate;
    }
    public String getGetStartDate() {
        return GetStartDate;
    }
    public void setGetStartDate(String aGetStartDate) {
        GetStartDate = aGetStartDate;
    }
    public String getGetStartType() {
        return GetStartType;
    }
    public void setGetStartType(String aGetStartType) {
        GetStartType = aGetStartType;
    }
    public String getLiveGetMode() {
        return LiveGetMode;
    }
    public void setLiveGetMode(String aLiveGetMode) {
        LiveGetMode = aLiveGetMode;
    }
    public String getDeadGetMode() {
        return DeadGetMode;
    }
    public void setDeadGetMode(String aDeadGetMode) {
        DeadGetMode = aDeadGetMode;
    }
    public String getBonusGetMode() {
        return BonusGetMode;
    }
    public void setBonusGetMode(String aBonusGetMode) {
        BonusGetMode = aBonusGetMode;
    }
    public String getSSFlag() {
        return SSFlag;
    }
    public void setSSFlag(String aSSFlag) {
        SSFlag = aSSFlag;
    }
    public double getPeakLine() {
        return PeakLine;
    }
    public void setPeakLine(double aPeakLine) {
        PeakLine = aPeakLine;
    }
    public void setPeakLine(String aPeakLine) {
        if (aPeakLine != null && !aPeakLine.equals("")) {
            Double tDouble = new Double(aPeakLine);
            double d = tDouble.doubleValue();
            PeakLine = d;
        }
    }

    public double getGetLimit() {
        return GetLimit;
    }
    public void setGetLimit(double aGetLimit) {
        GetLimit = aGetLimit;
    }
    public void setGetLimit(String aGetLimit) {
        if (aGetLimit != null && !aGetLimit.equals("")) {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = d;
        }
    }

    public double getGetRate() {
        return GetRate;
    }
    public void setGetRate(double aGetRate) {
        GetRate = aGetRate;
    }
    public void setGetRate(String aGetRate) {
        if (aGetRate != null && !aGetRate.equals("")) {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = d;
        }
    }

    public String getCalRule() {
        return CalRule;
    }
    public void setCalRule(String aCalRule) {
        CalRule = aCalRule;
    }
    public String getPremToAmnt() {
        return PremToAmnt;
    }
    public void setPremToAmnt(String aPremToAmnt) {
        PremToAmnt = aPremToAmnt;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public int getGetIntv() {
        return GetIntv;
    }
    public void setGetIntv(int aGetIntv) {
        GetIntv = aGetIntv;
    }
    public void setGetIntv(String aGetIntv) {
        if (aGetIntv != null && !aGetIntv.equals("")) {
            Integer tInteger = new Integer(aGetIntv);
            int i = tInteger.intValue();
            GetIntv = i;
        }
    }

    public String getAscriptionRuleCode() {
        return AscriptionRuleCode;
    }
    public void setAscriptionRuleCode(String aAscriptionRuleCode) {
        AscriptionRuleCode = aAscriptionRuleCode;
    }
    public String getPayRuleCode() {
        return PayRuleCode;
    }
    public void setPayRuleCode(String aPayRuleCode) {
        PayRuleCode = aPayRuleCode;
    }
    public double getStandRatePrem() {
        return StandRatePrem;
    }
    public void setStandRatePrem(double aStandRatePrem) {
        StandRatePrem = aStandRatePrem;
    }
    public void setStandRatePrem(String aStandRatePrem) {
        if (aStandRatePrem != null && !aStandRatePrem.equals("")) {
            Double tDouble = new Double(aStandRatePrem);
            double d = tDouble.doubleValue();
            StandRatePrem = d;
        }
    }

    public double getPeriodPrem() {
        return PeriodPrem;
    }
    public void setPeriodPrem(double aPeriodPrem) {
        PeriodPrem = aPeriodPrem;
    }
    public void setPeriodPrem(String aPeriodPrem) {
        if (aPeriodPrem != null && !aPeriodPrem.equals("")) {
            Double tDouble = new Double(aPeriodPrem);
            double d = tDouble.doubleValue();
            PeriodPrem = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("DutyID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("PolNo") ) {
            return 3;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 4;
        }
        if( strFieldName.equals("ContNo") ) {
            return 5;
        }
        if( strFieldName.equals("Mult") ) {
            return 6;
        }
        if( strFieldName.equals("StandPrem") ) {
            return 7;
        }
        if( strFieldName.equals("Prem") ) {
            return 8;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 9;
        }
        if( strFieldName.equals("Amnt") ) {
            return 10;
        }
        if( strFieldName.equals("RiskAmnt") ) {
            return 11;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 12;
        }
        if( strFieldName.equals("PayYears") ) {
            return 13;
        }
        if( strFieldName.equals("Years") ) {
            return 14;
        }
        if( strFieldName.equals("FloatRate") ) {
            return 15;
        }
        if( strFieldName.equals("FirstPayDate") ) {
            return 16;
        }
        if( strFieldName.equals("FirstMonth") ) {
            return 17;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 18;
        }
        if( strFieldName.equals("PayEndDate") ) {
            return 19;
        }
        if( strFieldName.equals("PayEndYearFlag") ) {
            return 20;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 21;
        }
        if( strFieldName.equals("GetYearFlag") ) {
            return 22;
        }
        if( strFieldName.equals("GetYear") ) {
            return 23;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 24;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 25;
        }
        if( strFieldName.equals("AcciYearFlag") ) {
            return 26;
        }
        if( strFieldName.equals("AcciYear") ) {
            return 27;
        }
        if( strFieldName.equals("EndDate") ) {
            return 28;
        }
        if( strFieldName.equals("AcciEndDate") ) {
            return 29;
        }
        if( strFieldName.equals("FreeFlag") ) {
            return 30;
        }
        if( strFieldName.equals("FreeRate") ) {
            return 31;
        }
        if( strFieldName.equals("FreeStartDate") ) {
            return 32;
        }
        if( strFieldName.equals("FreeEndDate") ) {
            return 33;
        }
        if( strFieldName.equals("GetStartDate") ) {
            return 34;
        }
        if( strFieldName.equals("GetStartType") ) {
            return 35;
        }
        if( strFieldName.equals("LiveGetMode") ) {
            return 36;
        }
        if( strFieldName.equals("DeadGetMode") ) {
            return 37;
        }
        if( strFieldName.equals("BonusGetMode") ) {
            return 38;
        }
        if( strFieldName.equals("SSFlag") ) {
            return 39;
        }
        if( strFieldName.equals("PeakLine") ) {
            return 40;
        }
        if( strFieldName.equals("GetLimit") ) {
            return 41;
        }
        if( strFieldName.equals("GetRate") ) {
            return 42;
        }
        if( strFieldName.equals("CalRule") ) {
            return 43;
        }
        if( strFieldName.equals("PremToAmnt") ) {
            return 44;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 45;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 46;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 47;
        }
        if( strFieldName.equals("Operator") ) {
            return 48;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 49;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 50;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 51;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 52;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 53;
        }
        if( strFieldName.equals("GetIntv") ) {
            return 54;
        }
        if( strFieldName.equals("AscriptionRuleCode") ) {
            return 55;
        }
        if( strFieldName.equals("PayRuleCode") ) {
            return 56;
        }
        if( strFieldName.equals("StandRatePrem") ) {
            return 57;
        }
        if( strFieldName.equals("PeriodPrem") ) {
            return 58;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "DutyID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "PolNo";
                break;
            case 4:
                strFieldName = "DutyCode";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "Mult";
                break;
            case 7:
                strFieldName = "StandPrem";
                break;
            case 8:
                strFieldName = "Prem";
                break;
            case 9:
                strFieldName = "SumPrem";
                break;
            case 10:
                strFieldName = "Amnt";
                break;
            case 11:
                strFieldName = "RiskAmnt";
                break;
            case 12:
                strFieldName = "PayIntv";
                break;
            case 13:
                strFieldName = "PayYears";
                break;
            case 14:
                strFieldName = "Years";
                break;
            case 15:
                strFieldName = "FloatRate";
                break;
            case 16:
                strFieldName = "FirstPayDate";
                break;
            case 17:
                strFieldName = "FirstMonth";
                break;
            case 18:
                strFieldName = "PaytoDate";
                break;
            case 19:
                strFieldName = "PayEndDate";
                break;
            case 20:
                strFieldName = "PayEndYearFlag";
                break;
            case 21:
                strFieldName = "PayEndYear";
                break;
            case 22:
                strFieldName = "GetYearFlag";
                break;
            case 23:
                strFieldName = "GetYear";
                break;
            case 24:
                strFieldName = "InsuYearFlag";
                break;
            case 25:
                strFieldName = "InsuYear";
                break;
            case 26:
                strFieldName = "AcciYearFlag";
                break;
            case 27:
                strFieldName = "AcciYear";
                break;
            case 28:
                strFieldName = "EndDate";
                break;
            case 29:
                strFieldName = "AcciEndDate";
                break;
            case 30:
                strFieldName = "FreeFlag";
                break;
            case 31:
                strFieldName = "FreeRate";
                break;
            case 32:
                strFieldName = "FreeStartDate";
                break;
            case 33:
                strFieldName = "FreeEndDate";
                break;
            case 34:
                strFieldName = "GetStartDate";
                break;
            case 35:
                strFieldName = "GetStartType";
                break;
            case 36:
                strFieldName = "LiveGetMode";
                break;
            case 37:
                strFieldName = "DeadGetMode";
                break;
            case 38:
                strFieldName = "BonusGetMode";
                break;
            case 39:
                strFieldName = "SSFlag";
                break;
            case 40:
                strFieldName = "PeakLine";
                break;
            case 41:
                strFieldName = "GetLimit";
                break;
            case 42:
                strFieldName = "GetRate";
                break;
            case 43:
                strFieldName = "CalRule";
                break;
            case 44:
                strFieldName = "PremToAmnt";
                break;
            case 45:
                strFieldName = "StandbyFlag1";
                break;
            case 46:
                strFieldName = "StandbyFlag2";
                break;
            case 47:
                strFieldName = "StandbyFlag3";
                break;
            case 48:
                strFieldName = "Operator";
                break;
            case 49:
                strFieldName = "MakeDate";
                break;
            case 50:
                strFieldName = "MakeTime";
                break;
            case 51:
                strFieldName = "ModifyDate";
                break;
            case 52:
                strFieldName = "ModifyTime";
                break;
            case 53:
                strFieldName = "CValiDate";
                break;
            case 54:
                strFieldName = "GetIntv";
                break;
            case 55:
                strFieldName = "AscriptionRuleCode";
                break;
            case 56:
                strFieldName = "PayRuleCode";
                break;
            case 57:
                strFieldName = "StandRatePrem";
                break;
            case 58:
                strFieldName = "PeriodPrem";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "DUTYID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "STANDPREM":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "RISKAMNT":
                return Schema.TYPE_DOUBLE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYYEARS":
                return Schema.TYPE_INT;
            case "YEARS":
                return Schema.TYPE_INT;
            case "FLOATRATE":
                return Schema.TYPE_DOUBLE;
            case "FIRSTPAYDATE":
                return Schema.TYPE_STRING;
            case "FIRSTMONTH":
                return Schema.TYPE_INT;
            case "PAYTODATE":
                return Schema.TYPE_STRING;
            case "PAYENDDATE":
                return Schema.TYPE_STRING;
            case "PAYENDYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "GETYEARFLAG":
                return Schema.TYPE_STRING;
            case "GETYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "ACCIYEARFLAG":
                return Schema.TYPE_STRING;
            case "ACCIYEAR":
                return Schema.TYPE_INT;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "ACCIENDDATE":
                return Schema.TYPE_STRING;
            case "FREEFLAG":
                return Schema.TYPE_STRING;
            case "FREERATE":
                return Schema.TYPE_DOUBLE;
            case "FREESTARTDATE":
                return Schema.TYPE_STRING;
            case "FREEENDDATE":
                return Schema.TYPE_STRING;
            case "GETSTARTDATE":
                return Schema.TYPE_STRING;
            case "GETSTARTTYPE":
                return Schema.TYPE_STRING;
            case "LIVEGETMODE":
                return Schema.TYPE_STRING;
            case "DEADGETMODE":
                return Schema.TYPE_STRING;
            case "BONUSGETMODE":
                return Schema.TYPE_STRING;
            case "SSFLAG":
                return Schema.TYPE_STRING;
            case "PEAKLINE":
                return Schema.TYPE_DOUBLE;
            case "GETLIMIT":
                return Schema.TYPE_DOUBLE;
            case "GETRATE":
                return Schema.TYPE_DOUBLE;
            case "CALRULE":
                return Schema.TYPE_STRING;
            case "PREMTOAMNT":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "GETINTV":
                return Schema.TYPE_INT;
            case "ASCRIPTIONRULECODE":
                return Schema.TYPE_STRING;
            case "PAYRULECODE":
                return Schema.TYPE_STRING;
            case "STANDRATEPREM":
                return Schema.TYPE_DOUBLE;
            case "PERIODPREM":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_INT;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_INT;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_INT;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_INT;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_INT;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_INT;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_DOUBLE;
            case 41:
                return Schema.TYPE_DOUBLE;
            case 42:
                return Schema.TYPE_DOUBLE;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_INT;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_DOUBLE;
            case 58:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("DutyID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("RiskAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskAmnt));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
        }
        if (FCode.equalsIgnoreCase("Years")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
        }
        if (FCode.equalsIgnoreCase("FloatRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FloatRate));
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstPayDate));
        }
        if (FCode.equalsIgnoreCase("FirstMonth")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstMonth));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaytoDate));
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDate));
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("AcciYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciYearFlag));
        }
        if (FCode.equalsIgnoreCase("AcciYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciYear));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("AcciEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciEndDate));
        }
        if (FCode.equalsIgnoreCase("FreeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeFlag));
        }
        if (FCode.equalsIgnoreCase("FreeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeRate));
        }
        if (FCode.equalsIgnoreCase("FreeStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeStartDate));
        }
        if (FCode.equalsIgnoreCase("FreeEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeEndDate));
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartDate));
        }
        if (FCode.equalsIgnoreCase("GetStartType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartType));
        }
        if (FCode.equalsIgnoreCase("LiveGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetMode));
        }
        if (FCode.equalsIgnoreCase("DeadGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadGetMode));
        }
        if (FCode.equalsIgnoreCase("BonusGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusGetMode));
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SSFlag));
        }
        if (FCode.equalsIgnoreCase("PeakLine")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeakLine));
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
        }
        if (FCode.equalsIgnoreCase("CalRule")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalRule));
        }
        if (FCode.equalsIgnoreCase("PremToAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremToAmnt));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetIntv));
        }
        if (FCode.equalsIgnoreCase("AscriptionRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptionRuleCode));
        }
        if (FCode.equalsIgnoreCase("PayRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayRuleCode));
        }
        if (FCode.equalsIgnoreCase("StandRatePrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandRatePrem));
        }
        if (FCode.equalsIgnoreCase("PeriodPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeriodPrem));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(DutyID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(EdorNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 4:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 5:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 6:
                strFieldValue = String.valueOf(Mult);
                break;
            case 7:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 8:
                strFieldValue = String.valueOf(Prem);
                break;
            case 9:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 10:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 11:
                strFieldValue = String.valueOf(RiskAmnt);
                break;
            case 12:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 13:
                strFieldValue = String.valueOf(PayYears);
                break;
            case 14:
                strFieldValue = String.valueOf(Years);
                break;
            case 15:
                strFieldValue = String.valueOf(FloatRate);
                break;
            case 16:
                strFieldValue = String.valueOf(FirstPayDate);
                break;
            case 17:
                strFieldValue = String.valueOf(FirstMonth);
                break;
            case 18:
                strFieldValue = String.valueOf(PaytoDate);
                break;
            case 19:
                strFieldValue = String.valueOf(PayEndDate);
                break;
            case 20:
                strFieldValue = String.valueOf(PayEndYearFlag);
                break;
            case 21:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 22:
                strFieldValue = String.valueOf(GetYearFlag);
                break;
            case 23:
                strFieldValue = String.valueOf(GetYear);
                break;
            case 24:
                strFieldValue = String.valueOf(InsuYearFlag);
                break;
            case 25:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 26:
                strFieldValue = String.valueOf(AcciYearFlag);
                break;
            case 27:
                strFieldValue = String.valueOf(AcciYear);
                break;
            case 28:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 29:
                strFieldValue = String.valueOf(AcciEndDate);
                break;
            case 30:
                strFieldValue = String.valueOf(FreeFlag);
                break;
            case 31:
                strFieldValue = String.valueOf(FreeRate);
                break;
            case 32:
                strFieldValue = String.valueOf(FreeStartDate);
                break;
            case 33:
                strFieldValue = String.valueOf(FreeEndDate);
                break;
            case 34:
                strFieldValue = String.valueOf(GetStartDate);
                break;
            case 35:
                strFieldValue = String.valueOf(GetStartType);
                break;
            case 36:
                strFieldValue = String.valueOf(LiveGetMode);
                break;
            case 37:
                strFieldValue = String.valueOf(DeadGetMode);
                break;
            case 38:
                strFieldValue = String.valueOf(BonusGetMode);
                break;
            case 39:
                strFieldValue = String.valueOf(SSFlag);
                break;
            case 40:
                strFieldValue = String.valueOf(PeakLine);
                break;
            case 41:
                strFieldValue = String.valueOf(GetLimit);
                break;
            case 42:
                strFieldValue = String.valueOf(GetRate);
                break;
            case 43:
                strFieldValue = String.valueOf(CalRule);
                break;
            case 44:
                strFieldValue = String.valueOf(PremToAmnt);
                break;
            case 45:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 46:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 47:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 48:
                strFieldValue = String.valueOf(Operator);
                break;
            case 49:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 50:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 51:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 52:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 53:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 54:
                strFieldValue = String.valueOf(GetIntv);
                break;
            case 55:
                strFieldValue = String.valueOf(AscriptionRuleCode);
                break;
            case 56:
                strFieldValue = String.valueOf(PayRuleCode);
                break;
            case 57:
                strFieldValue = String.valueOf(StandRatePrem);
                break;
            case 58:
                strFieldValue = String.valueOf(PeriodPrem);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("DutyID")) {
            if( FValue != null && !FValue.equals("")) {
                DutyID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("RiskAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RiskAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayYears = i;
            }
        }
        if (FCode.equalsIgnoreCase("Years")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Years = i;
            }
        }
        if (FCode.equalsIgnoreCase("FloatRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FloatRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstPayDate = FValue.trim();
            }
            else
                FirstPayDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstMonth")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                FirstMonth = i;
            }
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaytoDate = FValue.trim();
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDate = FValue.trim();
            }
            else
                PayEndDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
                PayEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
                GetYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("AcciYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AcciYearFlag = FValue.trim();
            }
            else
                AcciYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("AcciYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AcciYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("AcciEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AcciEndDate = FValue.trim();
            }
            else
                AcciEndDate = null;
        }
        if (FCode.equalsIgnoreCase("FreeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FreeFlag = FValue.trim();
            }
            else
                FreeFlag = null;
        }
        if (FCode.equalsIgnoreCase("FreeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FreeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FreeStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FreeStartDate = FValue.trim();
            }
            else
                FreeStartDate = null;
        }
        if (FCode.equalsIgnoreCase("FreeEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FreeEndDate = FValue.trim();
            }
            else
                FreeEndDate = null;
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetStartDate = FValue.trim();
            }
            else
                GetStartDate = null;
        }
        if (FCode.equalsIgnoreCase("GetStartType")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetStartType = FValue.trim();
            }
            else
                GetStartType = null;
        }
        if (FCode.equalsIgnoreCase("LiveGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveGetMode = FValue.trim();
            }
            else
                LiveGetMode = null;
        }
        if (FCode.equalsIgnoreCase("DeadGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadGetMode = FValue.trim();
            }
            else
                DeadGetMode = null;
        }
        if (FCode.equalsIgnoreCase("BonusGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusGetMode = FValue.trim();
            }
            else
                BonusGetMode = null;
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SSFlag = FValue.trim();
            }
            else
                SSFlag = null;
        }
        if (FCode.equalsIgnoreCase("PeakLine")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PeakLine = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("CalRule")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalRule = FValue.trim();
            }
            else
                CalRule = null;
        }
        if (FCode.equalsIgnoreCase("PremToAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                PremToAmnt = FValue.trim();
            }
            else
                PremToAmnt = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("AscriptionRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AscriptionRuleCode = FValue.trim();
            }
            else
                AscriptionRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("PayRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayRuleCode = FValue.trim();
            }
            else
                PayRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("StandRatePrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandRatePrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("PeriodPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PeriodPrem = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LBDutyPojo [" +
            "DutyID="+DutyID +
            ", ShardingID="+ShardingID +
            ", EdorNo="+EdorNo +
            ", PolNo="+PolNo +
            ", DutyCode="+DutyCode +
            ", ContNo="+ContNo +
            ", Mult="+Mult +
            ", StandPrem="+StandPrem +
            ", Prem="+Prem +
            ", SumPrem="+SumPrem +
            ", Amnt="+Amnt +
            ", RiskAmnt="+RiskAmnt +
            ", PayIntv="+PayIntv +
            ", PayYears="+PayYears +
            ", Years="+Years +
            ", FloatRate="+FloatRate +
            ", FirstPayDate="+FirstPayDate +
            ", FirstMonth="+FirstMonth +
            ", PaytoDate="+PaytoDate +
            ", PayEndDate="+PayEndDate +
            ", PayEndYearFlag="+PayEndYearFlag +
            ", PayEndYear="+PayEndYear +
            ", GetYearFlag="+GetYearFlag +
            ", GetYear="+GetYear +
            ", InsuYearFlag="+InsuYearFlag +
            ", InsuYear="+InsuYear +
            ", AcciYearFlag="+AcciYearFlag +
            ", AcciYear="+AcciYear +
            ", EndDate="+EndDate +
            ", AcciEndDate="+AcciEndDate +
            ", FreeFlag="+FreeFlag +
            ", FreeRate="+FreeRate +
            ", FreeStartDate="+FreeStartDate +
            ", FreeEndDate="+FreeEndDate +
            ", GetStartDate="+GetStartDate +
            ", GetStartType="+GetStartType +
            ", LiveGetMode="+LiveGetMode +
            ", DeadGetMode="+DeadGetMode +
            ", BonusGetMode="+BonusGetMode +
            ", SSFlag="+SSFlag +
            ", PeakLine="+PeakLine +
            ", GetLimit="+GetLimit +
            ", GetRate="+GetRate +
            ", CalRule="+CalRule +
            ", PremToAmnt="+PremToAmnt +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", StandbyFlag3="+StandbyFlag3 +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", CValiDate="+CValiDate +
            ", GetIntv="+GetIntv +
            ", AscriptionRuleCode="+AscriptionRuleCode +
            ", PayRuleCode="+PayRuleCode +
            ", StandRatePrem="+StandRatePrem +
            ", PeriodPrem="+PeriodPrem +"]";
    }
}
