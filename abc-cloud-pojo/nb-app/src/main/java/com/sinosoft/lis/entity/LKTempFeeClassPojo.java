/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LKTempFeeClassPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LKTempFeeClassPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private String TempFeeClassID; 
    /** Shardingid */
    private String ShardingID; 
    /** 暂交费收据号码 */
    private String TempFeeNo; 
    /** 交费方式 */
    private String PayMode; 
    /** 票据号 */
    private String ChequeNo; 
    /** 交费金额 */
    private double PayMoney; 
    /** 投保人名称 */
    private String AppntName; 
    /** 交费日期 */
    private String  PayDate;
    /** 确认日期 */
    private String  ConfDate;
    /** 复核日期 */
    private String  ApproveDate;
    /** 到帐日期 */
    private String  EnterAccDate;
    /** 是否核销标志 */
    private String ConfFlag; 
    /** 流水号 */
    private String SerialNo; 
    /** 交费机构 */
    private String ManageCom; 
    /** 管理机构 */
    private String PolicyCom; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 银行帐户名 */
    private String AccName; 
    /** 财务确认操作日期 */
    private String  ConfMakeDate;
    /** 财务确认操作时间 */
    private String ConfMakeTime; 
    /** 支票日期 */
    private String  ChequeDate;
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 收费银行编码 */
    private String InBankCode; 
    /** 收费银行帐号 */
    private String InBankAccNo; 
    /** 收费银行帐户名 */
    private String InAccName; 
    /** 保单所属机构 */
    private String ContCom; 
    /** 对应其它号码 */
    private String OtherNo; 
    /** 其它号码类型 */
    private String OtherNoType; 
    /** 收据类型 */
    private String TempFeeNoType; 
    /** 证件类型 */
    private String IDType; 
    /** 证件号码 */
    private String IDNo; 


    public static final int FIELDNUM = 35;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getTempFeeClassID() {
        return TempFeeClassID;
    }
    public void setTempFeeClassID(String aTempFeeClassID) {
        TempFeeClassID = aTempFeeClassID;
    }
    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getTempFeeNo() {
        return TempFeeNo;
    }
    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getChequeNo() {
        return ChequeNo;
    }
    public void setChequeNo(String aChequeNo) {
        ChequeNo = aChequeNo;
    }
    public double getPayMoney() {
        return PayMoney;
    }
    public void setPayMoney(double aPayMoney) {
        PayMoney = aPayMoney;
    }
    public void setPayMoney(String aPayMoney) {
        if (aPayMoney != null && !aPayMoney.equals("")) {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }

    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getPayDate() {
        return PayDate;
    }
    public void setPayDate(String aPayDate) {
        PayDate = aPayDate;
    }
    public String getConfDate() {
        return ConfDate;
    }
    public void setConfDate(String aConfDate) {
        ConfDate = aConfDate;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getEnterAccDate() {
        return EnterAccDate;
    }
    public void setEnterAccDate(String aEnterAccDate) {
        EnterAccDate = aEnterAccDate;
    }
    public String getConfFlag() {
        return ConfFlag;
    }
    public void setConfFlag(String aConfFlag) {
        ConfFlag = aConfFlag;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPolicyCom() {
        return PolicyCom;
    }
    public void setPolicyCom(String aPolicyCom) {
        PolicyCom = aPolicyCom;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getConfMakeDate() {
        return ConfMakeDate;
    }
    public void setConfMakeDate(String aConfMakeDate) {
        ConfMakeDate = aConfMakeDate;
    }
    public String getConfMakeTime() {
        return ConfMakeTime;
    }
    public void setConfMakeTime(String aConfMakeTime) {
        ConfMakeTime = aConfMakeTime;
    }
    public String getChequeDate() {
        return ChequeDate;
    }
    public void setChequeDate(String aChequeDate) {
        ChequeDate = aChequeDate;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getInBankCode() {
        return InBankCode;
    }
    public void setInBankCode(String aInBankCode) {
        InBankCode = aInBankCode;
    }
    public String getInBankAccNo() {
        return InBankAccNo;
    }
    public void setInBankAccNo(String aInBankAccNo) {
        InBankAccNo = aInBankAccNo;
    }
    public String getInAccName() {
        return InAccName;
    }
    public void setInAccName(String aInAccName) {
        InAccName = aInAccName;
    }
    public String getContCom() {
        return ContCom;
    }
    public void setContCom(String aContCom) {
        ContCom = aContCom;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getTempFeeNoType() {
        return TempFeeNoType;
    }
    public void setTempFeeNoType(String aTempFeeNoType) {
        TempFeeNoType = aTempFeeNoType;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TempFeeClassID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("TempFeeNo") ) {
            return 2;
        }
        if( strFieldName.equals("PayMode") ) {
            return 3;
        }
        if( strFieldName.equals("ChequeNo") ) {
            return 4;
        }
        if( strFieldName.equals("PayMoney") ) {
            return 5;
        }
        if( strFieldName.equals("AppntName") ) {
            return 6;
        }
        if( strFieldName.equals("PayDate") ) {
            return 7;
        }
        if( strFieldName.equals("ConfDate") ) {
            return 8;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 9;
        }
        if( strFieldName.equals("EnterAccDate") ) {
            return 10;
        }
        if( strFieldName.equals("ConfFlag") ) {
            return 11;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 12;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 13;
        }
        if( strFieldName.equals("PolicyCom") ) {
            return 14;
        }
        if( strFieldName.equals("BankCode") ) {
            return 15;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 16;
        }
        if( strFieldName.equals("AccName") ) {
            return 17;
        }
        if( strFieldName.equals("ConfMakeDate") ) {
            return 18;
        }
        if( strFieldName.equals("ConfMakeTime") ) {
            return 19;
        }
        if( strFieldName.equals("ChequeDate") ) {
            return 20;
        }
        if( strFieldName.equals("Operator") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        if( strFieldName.equals("InBankCode") ) {
            return 26;
        }
        if( strFieldName.equals("InBankAccNo") ) {
            return 27;
        }
        if( strFieldName.equals("InAccName") ) {
            return 28;
        }
        if( strFieldName.equals("ContCom") ) {
            return 29;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 30;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 31;
        }
        if( strFieldName.equals("TempFeeNoType") ) {
            return 32;
        }
        if( strFieldName.equals("IDType") ) {
            return 33;
        }
        if( strFieldName.equals("IDNo") ) {
            return 34;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TempFeeClassID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "TempFeeNo";
                break;
            case 3:
                strFieldName = "PayMode";
                break;
            case 4:
                strFieldName = "ChequeNo";
                break;
            case 5:
                strFieldName = "PayMoney";
                break;
            case 6:
                strFieldName = "AppntName";
                break;
            case 7:
                strFieldName = "PayDate";
                break;
            case 8:
                strFieldName = "ConfDate";
                break;
            case 9:
                strFieldName = "ApproveDate";
                break;
            case 10:
                strFieldName = "EnterAccDate";
                break;
            case 11:
                strFieldName = "ConfFlag";
                break;
            case 12:
                strFieldName = "SerialNo";
                break;
            case 13:
                strFieldName = "ManageCom";
                break;
            case 14:
                strFieldName = "PolicyCom";
                break;
            case 15:
                strFieldName = "BankCode";
                break;
            case 16:
                strFieldName = "BankAccNo";
                break;
            case 17:
                strFieldName = "AccName";
                break;
            case 18:
                strFieldName = "ConfMakeDate";
                break;
            case 19:
                strFieldName = "ConfMakeTime";
                break;
            case 20:
                strFieldName = "ChequeDate";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "InBankCode";
                break;
            case 27:
                strFieldName = "InBankAccNo";
                break;
            case 28:
                strFieldName = "InAccName";
                break;
            case 29:
                strFieldName = "ContCom";
                break;
            case 30:
                strFieldName = "OtherNo";
                break;
            case 31:
                strFieldName = "OtherNoType";
                break;
            case 32:
                strFieldName = "TempFeeNoType";
                break;
            case 33:
                strFieldName = "IDType";
                break;
            case 34:
                strFieldName = "IDNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TEMPFEECLASSID":
                return Schema.TYPE_STRING;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "TEMPFEENO":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "CHEQUENO":
                return Schema.TYPE_STRING;
            case "PAYMONEY":
                return Schema.TYPE_DOUBLE;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "PAYDATE":
                return Schema.TYPE_STRING;
            case "CONFDATE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "ENTERACCDATE":
                return Schema.TYPE_STRING;
            case "CONFFLAG":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "POLICYCOM":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "CONFMAKEDATE":
                return Schema.TYPE_STRING;
            case "CONFMAKETIME":
                return Schema.TYPE_STRING;
            case "CHEQUEDATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "INBANKCODE":
                return Schema.TYPE_STRING;
            case "INBANKACCNO":
                return Schema.TYPE_STRING;
            case "INACCNAME":
                return Schema.TYPE_STRING;
            case "CONTCOM":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "TEMPFEENOTYPE":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TempFeeClassID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeClassID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("ChequeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChequeNo));
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayDate));
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfDate));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterAccDate));
        }
        if (FCode.equalsIgnoreCase("ConfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfFlag));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyCom));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfMakeDate));
        }
        if (FCode.equalsIgnoreCase("ConfMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConfMakeTime));
        }
        if (FCode.equalsIgnoreCase("ChequeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChequeDate));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankCode));
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankAccNo));
        }
        if (FCode.equalsIgnoreCase("InAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InAccName));
        }
        if (FCode.equalsIgnoreCase("ContCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContCom));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("TempFeeNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNoType));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TempFeeClassID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(TempFeeNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PayMode);
                break;
            case 4:
                strFieldValue = String.valueOf(ChequeNo);
                break;
            case 5:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 6:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 7:
                strFieldValue = String.valueOf(PayDate);
                break;
            case 8:
                strFieldValue = String.valueOf(ConfDate);
                break;
            case 9:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 10:
                strFieldValue = String.valueOf(EnterAccDate);
                break;
            case 11:
                strFieldValue = String.valueOf(ConfFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 13:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 14:
                strFieldValue = String.valueOf(PolicyCom);
                break;
            case 15:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 16:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 17:
                strFieldValue = String.valueOf(AccName);
                break;
            case 18:
                strFieldValue = String.valueOf(ConfMakeDate);
                break;
            case 19:
                strFieldValue = String.valueOf(ConfMakeTime);
                break;
            case 20:
                strFieldValue = String.valueOf(ChequeDate);
                break;
            case 21:
                strFieldValue = String.valueOf(Operator);
                break;
            case 22:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 23:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 24:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 26:
                strFieldValue = String.valueOf(InBankCode);
                break;
            case 27:
                strFieldValue = String.valueOf(InBankAccNo);
                break;
            case 28:
                strFieldValue = String.valueOf(InAccName);
                break;
            case 29:
                strFieldValue = String.valueOf(ContCom);
                break;
            case 30:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 31:
                strFieldValue = String.valueOf(OtherNoType);
                break;
            case 32:
                strFieldValue = String.valueOf(TempFeeNoType);
                break;
            case 33:
                strFieldValue = String.valueOf(IDType);
                break;
            case 34:
                strFieldValue = String.valueOf(IDNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TempFeeClassID")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeClassID = FValue.trim();
            }
            else
                TempFeeClassID = null;
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
                TempFeeNo = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("ChequeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChequeNo = FValue.trim();
            }
            else
                ChequeNo = null;
        }
        if (FCode.equalsIgnoreCase("PayMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayDate = FValue.trim();
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfDate = FValue.trim();
            }
            else
                ConfDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("EnterAccDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EnterAccDate = FValue.trim();
            }
            else
                EnterAccDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfFlag = FValue.trim();
            }
            else
                ConfFlag = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("PolicyCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyCom = FValue.trim();
            }
            else
                PolicyCom = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfMakeDate = FValue.trim();
            }
            else
                ConfMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("ConfMakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConfMakeTime = FValue.trim();
            }
            else
                ConfMakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ChequeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChequeDate = FValue.trim();
            }
            else
                ChequeDate = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankCode = FValue.trim();
            }
            else
                InBankCode = null;
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankAccNo = FValue.trim();
            }
            else
                InBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InAccName = FValue.trim();
            }
            else
                InAccName = null;
        }
        if (FCode.equalsIgnoreCase("ContCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContCom = FValue.trim();
            }
            else
                ContCom = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNoType = FValue.trim();
            }
            else
                TempFeeNoType = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        return true;
    }


    public String toString() {
    return "LKTempFeeClassPojo [" +
            "TempFeeClassID="+TempFeeClassID +
            ", ShardingID="+ShardingID +
            ", TempFeeNo="+TempFeeNo +
            ", PayMode="+PayMode +
            ", ChequeNo="+ChequeNo +
            ", PayMoney="+PayMoney +
            ", AppntName="+AppntName +
            ", PayDate="+PayDate +
            ", ConfDate="+ConfDate +
            ", ApproveDate="+ApproveDate +
            ", EnterAccDate="+EnterAccDate +
            ", ConfFlag="+ConfFlag +
            ", SerialNo="+SerialNo +
            ", ManageCom="+ManageCom +
            ", PolicyCom="+PolicyCom +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", AccName="+AccName +
            ", ConfMakeDate="+ConfMakeDate +
            ", ConfMakeTime="+ConfMakeTime +
            ", ChequeDate="+ChequeDate +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", InBankCode="+InBankCode +
            ", InBankAccNo="+InBankAccNo +
            ", InAccName="+InAccName +
            ", ContCom="+ContCom +
            ", OtherNo="+OtherNo +
            ", OtherNoType="+OtherNoType +
            ", TempFeeNoType="+TempFeeNoType +
            ", IDType="+IDType +
            ", IDNo="+IDNo +"]";
    }
}
