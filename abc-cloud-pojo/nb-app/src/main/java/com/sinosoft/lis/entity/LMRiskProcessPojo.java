/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskProcessPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-06-08
 */
public class LMRiskProcessPojo implements Pojo,Serializable {
    // @Field
    /** 险种代码 */
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 险种种类 */
    private String RiskType; 
    /** 是否校验销售信息 */
    private String ValiSaleFlag; 
    /** 是否需要调用单证服务 */
    private String CallVchFlag; 
    /** 是否获取费率 */
    private String GetRateFlag; 
    /** 是否获取特殊审批信息 */
    private String SpecialApproveFlag; 
    /** 备用字段1 */
    private String Bak1; 
    /** 备用字段2 */
    private String Bak2; 
    /** 备用字段3 */
    private String Bak3; 


    public static final int FIELDNUM = 9;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskType() {
        return RiskType;
    }
    public void setRiskType(String aRiskType) {
        RiskType = aRiskType;
    }
    public String getValiSaleFlag() {
        return ValiSaleFlag;
    }
    public void setValiSaleFlag(String aValiSaleFlag) {
        ValiSaleFlag = aValiSaleFlag;
    }
    public String getCallVchFlag() {
        return CallVchFlag;
    }
    public void setCallVchFlag(String aCallVchFlag) {
        CallVchFlag = aCallVchFlag;
    }
    public String getGetRateFlag() {
        return GetRateFlag;
    }
    public void setGetRateFlag(String aGetRateFlag) {
        GetRateFlag = aGetRateFlag;
    }
    public String getSpecialApproveFlag() {
        return SpecialApproveFlag;
    }
    public void setSpecialApproveFlag(String aSpecialApproveFlag) {
        SpecialApproveFlag = aSpecialApproveFlag;
    }
    public String getBak1() {
        return Bak1;
    }
    public void setBak1(String aBak1) {
        Bak1 = aBak1;
    }
    public String getBak2() {
        return Bak2;
    }
    public void setBak2(String aBak2) {
        Bak2 = aBak2;
    }
    public String getBak3() {
        return Bak3;
    }
    public void setBak3(String aBak3) {
        Bak3 = aBak3;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskType") ) {
            return 1;
        }
        if( strFieldName.equals("ValiSaleFlag") ) {
            return 2;
        }
        if( strFieldName.equals("CallVchFlag") ) {
            return 3;
        }
        if( strFieldName.equals("GetRateFlag") ) {
            return 4;
        }
        if( strFieldName.equals("SpecialApproveFlag") ) {
            return 5;
        }
        if( strFieldName.equals("Bak1") ) {
            return 6;
        }
        if( strFieldName.equals("Bak2") ) {
            return 7;
        }
        if( strFieldName.equals("Bak3") ) {
            return 8;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskType";
                break;
            case 2:
                strFieldName = "ValiSaleFlag";
                break;
            case 3:
                strFieldName = "CallVchFlag";
                break;
            case 4:
                strFieldName = "GetRateFlag";
                break;
            case 5:
                strFieldName = "SpecialApproveFlag";
                break;
            case 6:
                strFieldName = "Bak1";
                break;
            case 7:
                strFieldName = "Bak2";
                break;
            case 8:
                strFieldName = "Bak3";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKTYPE":
                return Schema.TYPE_STRING;
            case "VALISALEFLAG":
                return Schema.TYPE_STRING;
            case "CALLVCHFLAG":
                return Schema.TYPE_STRING;
            case "GETRATEFLAG":
                return Schema.TYPE_STRING;
            case "SPECIALAPPROVEFLAG":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            case "BAK3":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
        }
        if (FCode.equalsIgnoreCase("ValiSaleFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValiSaleFlag));
        }
        if (FCode.equalsIgnoreCase("CallVchFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CallVchFlag));
        }
        if (FCode.equalsIgnoreCase("GetRateFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRateFlag));
        }
        if (FCode.equalsIgnoreCase("SpecialApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialApproveFlag));
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskType);
                break;
            case 2:
                strFieldValue = String.valueOf(ValiSaleFlag);
                break;
            case 3:
                strFieldValue = String.valueOf(CallVchFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(GetRateFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(SpecialApproveFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(Bak1);
                break;
            case 7:
                strFieldValue = String.valueOf(Bak2);
                break;
            case 8:
                strFieldValue = String.valueOf(Bak3);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskType = FValue.trim();
            }
            else
                RiskType = null;
        }
        if (FCode.equalsIgnoreCase("ValiSaleFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValiSaleFlag = FValue.trim();
            }
            else
                ValiSaleFlag = null;
        }
        if (FCode.equalsIgnoreCase("CallVchFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CallVchFlag = FValue.trim();
            }
            else
                CallVchFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetRateFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetRateFlag = FValue.trim();
            }
            else
                GetRateFlag = null;
        }
        if (FCode.equalsIgnoreCase("SpecialApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecialApproveFlag = FValue.trim();
            }
            else
                SpecialApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak1 = FValue.trim();
            }
            else
                Bak1 = null;
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak2 = FValue.trim();
            }
            else
                Bak2 = null;
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak3 = FValue.trim();
            }
            else
                Bak3 = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskProcessPojo [" +
            "RiskCode="+RiskCode +
            ", RiskType="+RiskType +
            ", ValiSaleFlag="+ValiSaleFlag +
            ", CallVchFlag="+CallVchFlag +
            ", GetRateFlag="+GetRateFlag +
            ", SpecialApproveFlag="+SpecialApproveFlag +
            ", Bak1="+Bak1 +
            ", Bak2="+Bak2 +
            ", Bak3="+Bak3 +"]";
    }
}
