/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LMRiskAddInfoPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-01-15
 */
public class LMRiskAddInfoPojo implements  Pojo,Serializable {
    // @Field
    /** 主键id */
    @RedisPrimaryHKey
    private String AddInfoId; 
    /** 险种编码 */
    @RedisIndexHKey
    private String RiskCode; 
    /** 销售类型 */
    private String SellType; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 交费年期 */
    private int PayYears; 
    /** 保险年龄年期 */
    private int InsuYear; 
    /** 保险年龄年期标志 */
    private String InsuYearFlag; 
    /** 终交年龄年期 */
    private int PayEndYear; 
    /** 终交年龄年期标志 */
    private String PayEndYearFlag; 
    /** 领取年龄年期标志 */
    private String GetYearFlag; 
    /** 领取年龄年期 */
    private int GetYear; 
    /** 续保标记 */
    private String RnewFlag; 
    /** 红利领取方式 */
    private String BonusGetMode; 
    /** 生存金领取方式 */
    private String LiveGetMode; 
    /** 保费转化基数 */
    private long PremPerUnit; 


    public static final int FIELDNUM = 15;    // 数据库表的字段个数
    public String getAddInfoId() {
        return AddInfoId;
    }
    public void setAddInfoId(String aAddInfoId) {
        AddInfoId = aAddInfoId;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getSellType() {
        return SellType;
    }
    public void setSellType(String aSellType) {
        SellType = aSellType;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public int getPayYears() {
        return PayYears;
    }
    public void setPayYears(int aPayYears) {
        PayYears = aPayYears;
    }
    public void setPayYears(String aPayYears) {
        if (aPayYears != null && !aPayYears.equals("")) {
            Integer tInteger = new Integer(aPayYears);
            int i = tInteger.intValue();
            PayYears = i;
        }
    }

    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }
    public void setPayEndYearFlag(String aPayEndYearFlag) {
        PayEndYearFlag = aPayEndYearFlag;
    }
    public String getGetYearFlag() {
        return GetYearFlag;
    }
    public void setGetYearFlag(String aGetYearFlag) {
        GetYearFlag = aGetYearFlag;
    }
    public int getGetYear() {
        return GetYear;
    }
    public void setGetYear(int aGetYear) {
        GetYear = aGetYear;
    }
    public void setGetYear(String aGetYear) {
        if (aGetYear != null && !aGetYear.equals("")) {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }

    public String getRnewFlag() {
        return RnewFlag;
    }
    public void setRnewFlag(String aRnewFlag) {
        RnewFlag = aRnewFlag;
    }
    public String getBonusGetMode() {
        return BonusGetMode;
    }
    public void setBonusGetMode(String aBonusGetMode) {
        BonusGetMode = aBonusGetMode;
    }
    public String getLiveGetMode() {
        return LiveGetMode;
    }
    public void setLiveGetMode(String aLiveGetMode) {
        LiveGetMode = aLiveGetMode;
    }
    public long getPremPerUnit() {
        return PremPerUnit;
    }
    public void setPremPerUnit(long aPremPerUnit) {
        PremPerUnit = aPremPerUnit;
    }
    public void setPremPerUnit(String aPremPerUnit) {
        if (aPremPerUnit != null && !aPremPerUnit.equals("")) {
            PremPerUnit = new Long(aPremPerUnit).longValue();
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AddInfoId") ) {
            return 0;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 1;
        }
        if( strFieldName.equals("SellType") ) {
            return 2;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 3;
        }
        if( strFieldName.equals("PayYears") ) {
            return 4;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 5;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 6;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 7;
        }
        if( strFieldName.equals("PayEndYearFlag") ) {
            return 8;
        }
        if( strFieldName.equals("GetYearFlag") ) {
            return 9;
        }
        if( strFieldName.equals("GetYear") ) {
            return 10;
        }
        if( strFieldName.equals("RnewFlag") ) {
            return 11;
        }
        if( strFieldName.equals("BonusGetMode") ) {
            return 12;
        }
        if( strFieldName.equals("LiveGetMode") ) {
            return 13;
        }
        if( strFieldName.equals("PremPerUnit") ) {
            return 14;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AddInfoId";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "SellType";
                break;
            case 3:
                strFieldName = "PayIntv";
                break;
            case 4:
                strFieldName = "PayYears";
                break;
            case 5:
                strFieldName = "InsuYear";
                break;
            case 6:
                strFieldName = "InsuYearFlag";
                break;
            case 7:
                strFieldName = "PayEndYear";
                break;
            case 8:
                strFieldName = "PayEndYearFlag";
                break;
            case 9:
                strFieldName = "GetYearFlag";
                break;
            case 10:
                strFieldName = "GetYear";
                break;
            case 11:
                strFieldName = "RnewFlag";
                break;
            case 12:
                strFieldName = "BonusGetMode";
                break;
            case 13:
                strFieldName = "LiveGetMode";
                break;
            case 14:
                strFieldName = "PremPerUnit";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "ADDINFOID":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYYEARS":
                return Schema.TYPE_INT;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "PAYENDYEARFLAG":
                return Schema.TYPE_STRING;
            case "GETYEARFLAG":
                return Schema.TYPE_STRING;
            case "GETYEAR":
                return Schema.TYPE_INT;
            case "RNEWFLAG":
                return Schema.TYPE_STRING;
            case "BONUSGETMODE":
                return Schema.TYPE_STRING;
            case "LIVEGETMODE":
                return Schema.TYPE_STRING;
            case "PREMPERUNIT":
                return Schema.TYPE_LONG;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_INT;
            case 5:
                return Schema.TYPE_INT;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_INT;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_INT;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_LONG;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AddInfoId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddInfoId));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SellType));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewFlag));
        }
        if (FCode.equalsIgnoreCase("BonusGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusGetMode));
        }
        if (FCode.equalsIgnoreCase("LiveGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetMode));
        }
        if (FCode.equalsIgnoreCase("PremPerUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremPerUnit));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AddInfoId);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 2:
                strFieldValue = String.valueOf(SellType);
                break;
            case 3:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 4:
                strFieldValue = String.valueOf(PayYears);
                break;
            case 5:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 6:
                strFieldValue = String.valueOf(InsuYearFlag);
                break;
            case 7:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 8:
                strFieldValue = String.valueOf(PayEndYearFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(GetYearFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(GetYear);
                break;
            case 11:
                strFieldValue = String.valueOf(RnewFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(BonusGetMode);
                break;
            case 13:
                strFieldValue = String.valueOf(LiveGetMode);
                break;
            case 14:
                strFieldValue = String.valueOf(PremPerUnit);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AddInfoId")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddInfoId = FValue.trim();
            }
            else
                AddInfoId = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SellType = FValue.trim();
            }
            else
                SellType = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayYears = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
                PayEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
                GetYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RnewFlag = FValue.trim();
            }
            else
                RnewFlag = null;
        }
        if (FCode.equalsIgnoreCase("BonusGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusGetMode = FValue.trim();
            }
            else
                BonusGetMode = null;
        }
        if (FCode.equalsIgnoreCase("LiveGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveGetMode = FValue.trim();
            }
            else
                LiveGetMode = null;
        }
        if (FCode.equalsIgnoreCase("PremPerUnit")) {
            if( FValue != null && !FValue.equals("")) {
                PremPerUnit = new Long(FValue).longValue();
            }
        }
        return true;
    }


    public String toString() {
    return "LMRiskAddInfoPojo [" +
            "AddInfoId="+AddInfoId +
            ", RiskCode="+RiskCode +
            ", SellType="+SellType +
            ", PayIntv="+PayIntv +
            ", PayYears="+PayYears +
            ", InsuYear="+InsuYear +
            ", InsuYearFlag="+InsuYearFlag +
            ", PayEndYear="+PayEndYear +
            ", PayEndYearFlag="+PayEndYearFlag +
            ", GetYearFlag="+GetYearFlag +
            ", GetYear="+GetYear +
            ", RnewFlag="+RnewFlag +
            ", BonusGetMode="+BonusGetMode +
            ", LiveGetMode="+LiveGetMode +
            ", PremPerUnit="+PremPerUnit +"]";
    }
}
