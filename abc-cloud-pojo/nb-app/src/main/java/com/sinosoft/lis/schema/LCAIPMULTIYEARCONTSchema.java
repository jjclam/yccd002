/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCAIPMULTIYEARCONTDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCAIPMULTIYEARCONTSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-06-11
 */
public class LCAIPMULTIYEARCONTSchema implements Schema, Cloneable {
    // @Field
    /** 保单号 */
    private String ContNo;
    /** 保单印刷号 */
    private String PrtNo;
    /** 保单生效日 */
    private Date CValiDate;
    /** 单证类型 */
    private String CardType;
    /** 单证号 */
    private String CardNo;
    /** 投保单/保单标志 */
    private String AppFlag;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 备用字段1 */
    private String StandbyFalg1;
    /** 备用字段2 */
    private String StandbyFalg2;
    /** 备用字段3 */
    private String StandbyFalg3;
    /** 备用字段4 */
    private String StandbyFalg4;
    /** 备用字段5 */
    private String StandbyFalg5;

    public static final int FIELDNUM = 15;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCAIPMULTIYEARCONTSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ContNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCAIPMULTIYEARCONTSchema cloned = (LCAIPMULTIYEARCONTSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getCValiDate() {
        if(CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }
    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else
            CValiDate = null;
    }

    public String getCardType() {
        return CardType;
    }
    public void setCardType(String aCardType) {
        CardType = aCardType;
    }
    public String getCardNo() {
        return CardNo;
    }
    public void setCardNo(String aCardNo) {
        CardNo = aCardNo;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandbyFalg1() {
        return StandbyFalg1;
    }
    public void setStandbyFalg1(String aStandbyFalg1) {
        StandbyFalg1 = aStandbyFalg1;
    }
    public String getStandbyFalg2() {
        return StandbyFalg2;
    }
    public void setStandbyFalg2(String aStandbyFalg2) {
        StandbyFalg2 = aStandbyFalg2;
    }
    public String getStandbyFalg3() {
        return StandbyFalg3;
    }
    public void setStandbyFalg3(String aStandbyFalg3) {
        StandbyFalg3 = aStandbyFalg3;
    }
    public String getStandbyFalg4() {
        return StandbyFalg4;
    }
    public void setStandbyFalg4(String aStandbyFalg4) {
        StandbyFalg4 = aStandbyFalg4;
    }
    public String getStandbyFalg5() {
        return StandbyFalg5;
    }
    public void setStandbyFalg5(String aStandbyFalg5) {
        StandbyFalg5 = aStandbyFalg5;
    }

    /**
    * 使用另外一个 LCAIPMULTIYEARCONTSchema 对象给 Schema 赋值
    * @param: aLCAIPMULTIYEARCONTSchema LCAIPMULTIYEARCONTSchema
    **/
    public void setSchema(LCAIPMULTIYEARCONTSchema aLCAIPMULTIYEARCONTSchema) {
        this.ContNo = aLCAIPMULTIYEARCONTSchema.getContNo();
        this.PrtNo = aLCAIPMULTIYEARCONTSchema.getPrtNo();
        this.CValiDate = fDate.getDate( aLCAIPMULTIYEARCONTSchema.getCValiDate());
        this.CardType = aLCAIPMULTIYEARCONTSchema.getCardType();
        this.CardNo = aLCAIPMULTIYEARCONTSchema.getCardNo();
        this.AppFlag = aLCAIPMULTIYEARCONTSchema.getAppFlag();
        this.MakeDate = fDate.getDate( aLCAIPMULTIYEARCONTSchema.getMakeDate());
        this.MakeTime = aLCAIPMULTIYEARCONTSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCAIPMULTIYEARCONTSchema.getModifyDate());
        this.ModifyTime = aLCAIPMULTIYEARCONTSchema.getModifyTime();
        this.StandbyFalg1 = aLCAIPMULTIYEARCONTSchema.getStandbyFalg1();
        this.StandbyFalg2 = aLCAIPMULTIYEARCONTSchema.getStandbyFalg2();
        this.StandbyFalg3 = aLCAIPMULTIYEARCONTSchema.getStandbyFalg3();
        this.StandbyFalg4 = aLCAIPMULTIYEARCONTSchema.getStandbyFalg4();
        this.StandbyFalg5 = aLCAIPMULTIYEARCONTSchema.getStandbyFalg5();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            this.CValiDate = rs.getDate("CValiDate");
            if( rs.getString("CardType") == null )
                this.CardType = null;
            else
                this.CardType = rs.getString("CardType").trim();

            if( rs.getString("CardNo") == null )
                this.CardNo = null;
            else
                this.CardNo = rs.getString("CardNo").trim();

            if( rs.getString("AppFlag") == null )
                this.AppFlag = null;
            else
                this.AppFlag = rs.getString("AppFlag").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("StandbyFalg1") == null )
                this.StandbyFalg1 = null;
            else
                this.StandbyFalg1 = rs.getString("StandbyFalg1").trim();

            if( rs.getString("StandbyFalg2") == null )
                this.StandbyFalg2 = null;
            else
                this.StandbyFalg2 = rs.getString("StandbyFalg2").trim();

            if( rs.getString("StandbyFalg3") == null )
                this.StandbyFalg3 = null;
            else
                this.StandbyFalg3 = rs.getString("StandbyFalg3").trim();

            if( rs.getString("StandbyFalg4") == null )
                this.StandbyFalg4 = null;
            else
                this.StandbyFalg4 = rs.getString("StandbyFalg4").trim();

            if( rs.getString("StandbyFalg5") == null )
                this.StandbyFalg5 = null;
            else
                this.StandbyFalg5 = rs.getString("StandbyFalg5").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAIPMULTIYEARCONTSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCAIPMULTIYEARCONTSchema getSchema() {
        LCAIPMULTIYEARCONTSchema aLCAIPMULTIYEARCONTSchema = new LCAIPMULTIYEARCONTSchema();
        aLCAIPMULTIYEARCONTSchema.setSchema(this);
        return aLCAIPMULTIYEARCONTSchema;
    }

    public LCAIPMULTIYEARCONTDB getDB() {
        LCAIPMULTIYEARCONTDB aDBOper = new LCAIPMULTIYEARCONTDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAIPMULTIYEARCONT描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFalg1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFalg2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFalg3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFalg4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFalg5));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAIPMULTIYEARCONT>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER));
            CardType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            CardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            StandbyFalg1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            StandbyFalg2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            StandbyFalg3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            StandbyFalg4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            StandbyFalg5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAIPMULTIYEARCONTSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
        }
        if (FCode.equalsIgnoreCase("CardType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardType));
        }
        if (FCode.equalsIgnoreCase("CardNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg1));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg2));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg3));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg4));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CardType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CardNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AppFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(StandbyFalg1);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(StandbyFalg2);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(StandbyFalg3);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(StandbyFalg4);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(StandbyFalg5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if(FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate( FValue );
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("CardType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardType = FValue.trim();
            }
            else
                CardType = null;
        }
        if (FCode.equalsIgnoreCase("CardNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardNo = FValue.trim();
            }
            else
                CardNo = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg1 = FValue.trim();
            }
            else
                StandbyFalg1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg2 = FValue.trim();
            }
            else
                StandbyFalg2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg3 = FValue.trim();
            }
            else
                StandbyFalg3 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg4 = FValue.trim();
            }
            else
                StandbyFalg4 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg5 = FValue.trim();
            }
            else
                StandbyFalg5 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCAIPMULTIYEARCONTSchema other = (LCAIPMULTIYEARCONTSchema)otherObject;
        return
            ContNo.equals(other.getContNo())
            && PrtNo.equals(other.getPrtNo())
            && fDate.getString(CValiDate).equals(other.getCValiDate())
            && CardType.equals(other.getCardType())
            && CardNo.equals(other.getCardNo())
            && AppFlag.equals(other.getAppFlag())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && StandbyFalg1.equals(other.getStandbyFalg1())
            && StandbyFalg2.equals(other.getStandbyFalg2())
            && StandbyFalg3.equals(other.getStandbyFalg3())
            && StandbyFalg4.equals(other.getStandbyFalg4())
            && StandbyFalg5.equals(other.getStandbyFalg5());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContNo") ) {
            return 0;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 1;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 2;
        }
        if( strFieldName.equals("CardType") ) {
            return 3;
        }
        if( strFieldName.equals("CardNo") ) {
            return 4;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 5;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 6;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 7;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 8;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 9;
        }
        if( strFieldName.equals("StandbyFalg1") ) {
            return 10;
        }
        if( strFieldName.equals("StandbyFalg2") ) {
            return 11;
        }
        if( strFieldName.equals("StandbyFalg3") ) {
            return 12;
        }
        if( strFieldName.equals("StandbyFalg4") ) {
            return 13;
        }
        if( strFieldName.equals("StandbyFalg5") ) {
            return 14;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "PrtNo";
                break;
            case 2:
                strFieldName = "CValiDate";
                break;
            case 3:
                strFieldName = "CardType";
                break;
            case 4:
                strFieldName = "CardNo";
                break;
            case 5:
                strFieldName = "AppFlag";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "StandbyFalg1";
                break;
            case 11:
                strFieldName = "StandbyFalg2";
                break;
            case 12:
                strFieldName = "StandbyFalg3";
                break;
            case 13:
                strFieldName = "StandbyFalg4";
                break;
            case 14:
                strFieldName = "StandbyFalg5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_DATE;
            case "CARDTYPE":
                return Schema.TYPE_STRING;
            case "CARDNO":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFALG1":
                return Schema.TYPE_STRING;
            case "STANDBYFALG2":
                return Schema.TYPE_STRING;
            case "STANDBYFALG3":
                return Schema.TYPE_STRING;
            case "STANDBYFALG4":
                return Schema.TYPE_STRING;
            case "STANDBYFALG5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_DATE;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
