/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCIndUWSubPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-08-30
 */
public class LCIndUWSubPojo implements  Pojo,Serializable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 总单投保单号码 */
    private String ProposalContNo; 
    /** 核保顺序号 */
    private int UWNo; 
    /** 被保人客户号码 */
    private String InsuredNo; 
    /** 被保人名称 */
    private String InsuredName; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 投保人名称 */
    private String AppntName; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 核保通过标志 */
    private String PassFlag; 
    /** 核保级别 */
    private String UWGrade; 
    /** 申请级别 */
    private String AppGrade; 
    /** 延至日 */
    private String PostponeDay; 
    /** 延至日期 */
    private String  PostponeDate;
    /** 是否自动核保 */
    private String AutoUWFlag; 
    /** 状态 */
    private String State; 
    /** 管理机构 */
    private String ManageCom; 
    /** 核保意见 */
    private String UWIdea; 
    /** 上报内容 */
    private String UpReportContent; 
    /** 操作员 */
    private String Operator; 
    /** 是否体检件 */
    private String HealthFlag; 
    /** 是否问题件 */
    private String QuesFlag; 
    /** 特约标志 */
    private String SpecFlag; 
    /** 加费标志 */
    private String AddPremFlag; 
    /** 加费原因 */
    private String AddPremReason; 
    /** 生调标志 */
    private String ReportFlag; 
    /** 核保通知书打印标记 */
    private String PrintFlag; 
    /** 业务员通知书打印标记 */
    private String PrintFlag2; 
    /** 保险计划变更标记 */
    private String ChangePolFlag; 
    /** 保险计划变更原因 */
    private String ChangePolReason; 
    /** 特约原因 */
    private String SpecReason; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 建议核保结论 */
    private String SugPassFlag; 
    /** 建议核保意见 */
    private String SugUWIdea; 
    /** 上报标志 */
    private String UpReport; 
    /** 核保类型 */
    private String UWType; 


    public static final int FIELDNUM = 40;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public int getUWNo() {
        return UWNo;
    }
    public void setUWNo(int aUWNo) {
        UWNo = aUWNo;
    }
    public void setUWNo(String aUWNo) {
        if (aUWNo != null && !aUWNo.equals("")) {
            Integer tInteger = new Integer(aUWNo);
            int i = tInteger.intValue();
            UWNo = i;
        }
    }

    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getPassFlag() {
        return PassFlag;
    }
    public void setPassFlag(String aPassFlag) {
        PassFlag = aPassFlag;
    }
    public String getUWGrade() {
        return UWGrade;
    }
    public void setUWGrade(String aUWGrade) {
        UWGrade = aUWGrade;
    }
    public String getAppGrade() {
        return AppGrade;
    }
    public void setAppGrade(String aAppGrade) {
        AppGrade = aAppGrade;
    }
    public String getPostponeDay() {
        return PostponeDay;
    }
    public void setPostponeDay(String aPostponeDay) {
        PostponeDay = aPostponeDay;
    }
    public String getPostponeDate() {
        return PostponeDate;
    }
    public void setPostponeDate(String aPostponeDate) {
        PostponeDate = aPostponeDate;
    }
    public String getAutoUWFlag() {
        return AutoUWFlag;
    }
    public void setAutoUWFlag(String aAutoUWFlag) {
        AutoUWFlag = aAutoUWFlag;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getUWIdea() {
        return UWIdea;
    }
    public void setUWIdea(String aUWIdea) {
        UWIdea = aUWIdea;
    }
    public String getUpReportContent() {
        return UpReportContent;
    }
    public void setUpReportContent(String aUpReportContent) {
        UpReportContent = aUpReportContent;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getHealthFlag() {
        return HealthFlag;
    }
    public void setHealthFlag(String aHealthFlag) {
        HealthFlag = aHealthFlag;
    }
    public String getQuesFlag() {
        return QuesFlag;
    }
    public void setQuesFlag(String aQuesFlag) {
        QuesFlag = aQuesFlag;
    }
    public String getSpecFlag() {
        return SpecFlag;
    }
    public void setSpecFlag(String aSpecFlag) {
        SpecFlag = aSpecFlag;
    }
    public String getAddPremFlag() {
        return AddPremFlag;
    }
    public void setAddPremFlag(String aAddPremFlag) {
        AddPremFlag = aAddPremFlag;
    }
    public String getAddPremReason() {
        return AddPremReason;
    }
    public void setAddPremReason(String aAddPremReason) {
        AddPremReason = aAddPremReason;
    }
    public String getReportFlag() {
        return ReportFlag;
    }
    public void setReportFlag(String aReportFlag) {
        ReportFlag = aReportFlag;
    }
    public String getPrintFlag() {
        return PrintFlag;
    }
    public void setPrintFlag(String aPrintFlag) {
        PrintFlag = aPrintFlag;
    }
    public String getPrintFlag2() {
        return PrintFlag2;
    }
    public void setPrintFlag2(String aPrintFlag2) {
        PrintFlag2 = aPrintFlag2;
    }
    public String getChangePolFlag() {
        return ChangePolFlag;
    }
    public void setChangePolFlag(String aChangePolFlag) {
        ChangePolFlag = aChangePolFlag;
    }
    public String getChangePolReason() {
        return ChangePolReason;
    }
    public void setChangePolReason(String aChangePolReason) {
        ChangePolReason = aChangePolReason;
    }
    public String getSpecReason() {
        return SpecReason;
    }
    public void setSpecReason(String aSpecReason) {
        SpecReason = aSpecReason;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getSugPassFlag() {
        return SugPassFlag;
    }
    public void setSugPassFlag(String aSugPassFlag) {
        SugPassFlag = aSugPassFlag;
    }
    public String getSugUWIdea() {
        return SugUWIdea;
    }
    public void setSugUWIdea(String aSugUWIdea) {
        SugUWIdea = aSugUWIdea;
    }
    public String getUpReport() {
        return UpReport;
    }
    public void setUpReport(String aUpReport) {
        UpReport = aUpReport;
    }
    public String getUWType() {
        return UWType;
    }
    public void setUWType(String aUWType) {
        UWType = aUWType;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ContNo") ) {
            return 1;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 2;
        }
        if( strFieldName.equals("UWNo") ) {
            return 3;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 4;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 5;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 6;
        }
        if( strFieldName.equals("AppntName") ) {
            return 7;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 8;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 9;
        }
        if( strFieldName.equals("PassFlag") ) {
            return 10;
        }
        if( strFieldName.equals("UWGrade") ) {
            return 11;
        }
        if( strFieldName.equals("AppGrade") ) {
            return 12;
        }
        if( strFieldName.equals("PostponeDay") ) {
            return 13;
        }
        if( strFieldName.equals("PostponeDate") ) {
            return 14;
        }
        if( strFieldName.equals("AutoUWFlag") ) {
            return 15;
        }
        if( strFieldName.equals("State") ) {
            return 16;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 17;
        }
        if( strFieldName.equals("UWIdea") ) {
            return 18;
        }
        if( strFieldName.equals("UpReportContent") ) {
            return 19;
        }
        if( strFieldName.equals("Operator") ) {
            return 20;
        }
        if( strFieldName.equals("HealthFlag") ) {
            return 21;
        }
        if( strFieldName.equals("QuesFlag") ) {
            return 22;
        }
        if( strFieldName.equals("SpecFlag") ) {
            return 23;
        }
        if( strFieldName.equals("AddPremFlag") ) {
            return 24;
        }
        if( strFieldName.equals("AddPremReason") ) {
            return 25;
        }
        if( strFieldName.equals("ReportFlag") ) {
            return 26;
        }
        if( strFieldName.equals("PrintFlag") ) {
            return 27;
        }
        if( strFieldName.equals("PrintFlag2") ) {
            return 28;
        }
        if( strFieldName.equals("ChangePolFlag") ) {
            return 29;
        }
        if( strFieldName.equals("ChangePolReason") ) {
            return 30;
        }
        if( strFieldName.equals("SpecReason") ) {
            return 31;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 32;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 33;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 34;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 35;
        }
        if( strFieldName.equals("SugPassFlag") ) {
            return 36;
        }
        if( strFieldName.equals("SugUWIdea") ) {
            return 37;
        }
        if( strFieldName.equals("UpReport") ) {
            return 38;
        }
        if( strFieldName.equals("UWType") ) {
            return 39;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "ProposalContNo";
                break;
            case 3:
                strFieldName = "UWNo";
                break;
            case 4:
                strFieldName = "InsuredNo";
                break;
            case 5:
                strFieldName = "InsuredName";
                break;
            case 6:
                strFieldName = "AppntNo";
                break;
            case 7:
                strFieldName = "AppntName";
                break;
            case 8:
                strFieldName = "AgentCode";
                break;
            case 9:
                strFieldName = "AgentGroup";
                break;
            case 10:
                strFieldName = "PassFlag";
                break;
            case 11:
                strFieldName = "UWGrade";
                break;
            case 12:
                strFieldName = "AppGrade";
                break;
            case 13:
                strFieldName = "PostponeDay";
                break;
            case 14:
                strFieldName = "PostponeDate";
                break;
            case 15:
                strFieldName = "AutoUWFlag";
                break;
            case 16:
                strFieldName = "State";
                break;
            case 17:
                strFieldName = "ManageCom";
                break;
            case 18:
                strFieldName = "UWIdea";
                break;
            case 19:
                strFieldName = "UpReportContent";
                break;
            case 20:
                strFieldName = "Operator";
                break;
            case 21:
                strFieldName = "HealthFlag";
                break;
            case 22:
                strFieldName = "QuesFlag";
                break;
            case 23:
                strFieldName = "SpecFlag";
                break;
            case 24:
                strFieldName = "AddPremFlag";
                break;
            case 25:
                strFieldName = "AddPremReason";
                break;
            case 26:
                strFieldName = "ReportFlag";
                break;
            case 27:
                strFieldName = "PrintFlag";
                break;
            case 28:
                strFieldName = "PrintFlag2";
                break;
            case 29:
                strFieldName = "ChangePolFlag";
                break;
            case 30:
                strFieldName = "ChangePolReason";
                break;
            case 31:
                strFieldName = "SpecReason";
                break;
            case 32:
                strFieldName = "MakeDate";
                break;
            case 33:
                strFieldName = "MakeTime";
                break;
            case 34:
                strFieldName = "ModifyDate";
                break;
            case 35:
                strFieldName = "ModifyTime";
                break;
            case 36:
                strFieldName = "SugPassFlag";
                break;
            case 37:
                strFieldName = "SugUWIdea";
                break;
            case 38:
                strFieldName = "UpReport";
                break;
            case 39:
                strFieldName = "UWType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "UWNO":
                return Schema.TYPE_INT;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "PASSFLAG":
                return Schema.TYPE_STRING;
            case "UWGRADE":
                return Schema.TYPE_STRING;
            case "APPGRADE":
                return Schema.TYPE_STRING;
            case "POSTPONEDAY":
                return Schema.TYPE_STRING;
            case "POSTPONEDATE":
                return Schema.TYPE_STRING;
            case "AUTOUWFLAG":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "UWIDEA":
                return Schema.TYPE_STRING;
            case "UPREPORTCONTENT":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "HEALTHFLAG":
                return Schema.TYPE_STRING;
            case "QUESFLAG":
                return Schema.TYPE_STRING;
            case "SPECFLAG":
                return Schema.TYPE_STRING;
            case "ADDPREMFLAG":
                return Schema.TYPE_STRING;
            case "ADDPREMREASON":
                return Schema.TYPE_STRING;
            case "REPORTFLAG":
                return Schema.TYPE_STRING;
            case "PRINTFLAG":
                return Schema.TYPE_STRING;
            case "PRINTFLAG2":
                return Schema.TYPE_STRING;
            case "CHANGEPOLFLAG":
                return Schema.TYPE_STRING;
            case "CHANGEPOLREASON":
                return Schema.TYPE_STRING;
            case "SPECREASON":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "SUGPASSFLAG":
                return Schema.TYPE_STRING;
            case "SUGUWIDEA":
                return Schema.TYPE_STRING;
            case "UPREPORT":
                return Schema.TYPE_STRING;
            case "UWTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("UWNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("PassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PassFlag));
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWGrade));
        }
        if (FCode.equalsIgnoreCase("AppGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppGrade));
        }
        if (FCode.equalsIgnoreCase("PostponeDay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostponeDay));
        }
        if (FCode.equalsIgnoreCase("PostponeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostponeDate));
        }
        if (FCode.equalsIgnoreCase("AutoUWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoUWFlag));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("UWIdea")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWIdea));
        }
        if (FCode.equalsIgnoreCase("UpReportContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpReportContent));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthFlag));
        }
        if (FCode.equalsIgnoreCase("QuesFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuesFlag));
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecFlag));
        }
        if (FCode.equalsIgnoreCase("AddPremFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddPremFlag));
        }
        if (FCode.equalsIgnoreCase("AddPremReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddPremReason));
        }
        if (FCode.equalsIgnoreCase("ReportFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportFlag));
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
        }
        if (FCode.equalsIgnoreCase("PrintFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag2));
        }
        if (FCode.equalsIgnoreCase("ChangePolFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChangePolFlag));
        }
        if (FCode.equalsIgnoreCase("ChangePolReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChangePolReason));
        }
        if (FCode.equalsIgnoreCase("SpecReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecReason));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("SugPassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SugPassFlag));
        }
        if (FCode.equalsIgnoreCase("SugUWIdea")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SugUWIdea));
        }
        if (FCode.equalsIgnoreCase("UpReport")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpReport));
        }
        if (FCode.equalsIgnoreCase("UWType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ProposalContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(UWNo);
                break;
            case 4:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 5:
                strFieldValue = String.valueOf(InsuredName);
                break;
            case 6:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 7:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 8:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 9:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 10:
                strFieldValue = String.valueOf(PassFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(UWGrade);
                break;
            case 12:
                strFieldValue = String.valueOf(AppGrade);
                break;
            case 13:
                strFieldValue = String.valueOf(PostponeDay);
                break;
            case 14:
                strFieldValue = String.valueOf(PostponeDate);
                break;
            case 15:
                strFieldValue = String.valueOf(AutoUWFlag);
                break;
            case 16:
                strFieldValue = String.valueOf(State);
                break;
            case 17:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 18:
                strFieldValue = String.valueOf(UWIdea);
                break;
            case 19:
                strFieldValue = String.valueOf(UpReportContent);
                break;
            case 20:
                strFieldValue = String.valueOf(Operator);
                break;
            case 21:
                strFieldValue = String.valueOf(HealthFlag);
                break;
            case 22:
                strFieldValue = String.valueOf(QuesFlag);
                break;
            case 23:
                strFieldValue = String.valueOf(SpecFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(AddPremFlag);
                break;
            case 25:
                strFieldValue = String.valueOf(AddPremReason);
                break;
            case 26:
                strFieldValue = String.valueOf(ReportFlag);
                break;
            case 27:
                strFieldValue = String.valueOf(PrintFlag);
                break;
            case 28:
                strFieldValue = String.valueOf(PrintFlag2);
                break;
            case 29:
                strFieldValue = String.valueOf(ChangePolFlag);
                break;
            case 30:
                strFieldValue = String.valueOf(ChangePolReason);
                break;
            case 31:
                strFieldValue = String.valueOf(SpecReason);
                break;
            case 32:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 33:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 34:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 35:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 36:
                strFieldValue = String.valueOf(SugPassFlag);
                break;
            case 37:
                strFieldValue = String.valueOf(SugUWIdea);
                break;
            case 38:
                strFieldValue = String.valueOf(UpReport);
                break;
            case 39:
                strFieldValue = String.valueOf(UWType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("UWNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                UWNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("PassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PassFlag = FValue.trim();
            }
            else
                PassFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
                UWGrade = null;
        }
        if (FCode.equalsIgnoreCase("AppGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppGrade = FValue.trim();
            }
            else
                AppGrade = null;
        }
        if (FCode.equalsIgnoreCase("PostponeDay")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostponeDay = FValue.trim();
            }
            else
                PostponeDay = null;
        }
        if (FCode.equalsIgnoreCase("PostponeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostponeDate = FValue.trim();
            }
            else
                PostponeDate = null;
        }
        if (FCode.equalsIgnoreCase("AutoUWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoUWFlag = FValue.trim();
            }
            else
                AutoUWFlag = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("UWIdea")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWIdea = FValue.trim();
            }
            else
                UWIdea = null;
        }
        if (FCode.equalsIgnoreCase("UpReportContent")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpReportContent = FValue.trim();
            }
            else
                UpReportContent = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthFlag = FValue.trim();
            }
            else
                HealthFlag = null;
        }
        if (FCode.equalsIgnoreCase("QuesFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                QuesFlag = FValue.trim();
            }
            else
                QuesFlag = null;
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecFlag = FValue.trim();
            }
            else
                SpecFlag = null;
        }
        if (FCode.equalsIgnoreCase("AddPremFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddPremFlag = FValue.trim();
            }
            else
                AddPremFlag = null;
        }
        if (FCode.equalsIgnoreCase("AddPremReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddPremReason = FValue.trim();
            }
            else
                AddPremReason = null;
        }
        if (FCode.equalsIgnoreCase("ReportFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportFlag = FValue.trim();
            }
            else
                ReportFlag = null;
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
                PrintFlag = null;
        }
        if (FCode.equalsIgnoreCase("PrintFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintFlag2 = FValue.trim();
            }
            else
                PrintFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("ChangePolFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChangePolFlag = FValue.trim();
            }
            else
                ChangePolFlag = null;
        }
        if (FCode.equalsIgnoreCase("ChangePolReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChangePolReason = FValue.trim();
            }
            else
                ChangePolReason = null;
        }
        if (FCode.equalsIgnoreCase("SpecReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecReason = FValue.trim();
            }
            else
                SpecReason = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("SugPassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SugPassFlag = FValue.trim();
            }
            else
                SugPassFlag = null;
        }
        if (FCode.equalsIgnoreCase("SugUWIdea")) {
            if( FValue != null && !FValue.equals(""))
            {
                SugUWIdea = FValue.trim();
            }
            else
                SugUWIdea = null;
        }
        if (FCode.equalsIgnoreCase("UpReport")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpReport = FValue.trim();
            }
            else
                UpReport = null;
        }
        if (FCode.equalsIgnoreCase("UWType")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWType = FValue.trim();
            }
            else
                UWType = null;
        }
        return true;
    }


    public String toString() {
    return "LCIndUWSubPojo [" +
            "GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", ProposalContNo="+ProposalContNo +
            ", UWNo="+UWNo +
            ", InsuredNo="+InsuredNo +
            ", InsuredName="+InsuredName +
            ", AppntNo="+AppntNo +
            ", AppntName="+AppntName +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", PassFlag="+PassFlag +
            ", UWGrade="+UWGrade +
            ", AppGrade="+AppGrade +
            ", PostponeDay="+PostponeDay +
            ", PostponeDate="+PostponeDate +
            ", AutoUWFlag="+AutoUWFlag +
            ", State="+State +
            ", ManageCom="+ManageCom +
            ", UWIdea="+UWIdea +
            ", UpReportContent="+UpReportContent +
            ", Operator="+Operator +
            ", HealthFlag="+HealthFlag +
            ", QuesFlag="+QuesFlag +
            ", SpecFlag="+SpecFlag +
            ", AddPremFlag="+AddPremFlag +
            ", AddPremReason="+AddPremReason +
            ", ReportFlag="+ReportFlag +
            ", PrintFlag="+PrintFlag +
            ", PrintFlag2="+PrintFlag2 +
            ", ChangePolFlag="+ChangePolFlag +
            ", ChangePolReason="+ChangePolReason +
            ", SpecReason="+SpecReason +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", SugPassFlag="+SugPassFlag +
            ", SugUWIdea="+SugUWIdea +
            ", UpReport="+UpReport +
            ", UWType="+UWType +"]";
    }
}
