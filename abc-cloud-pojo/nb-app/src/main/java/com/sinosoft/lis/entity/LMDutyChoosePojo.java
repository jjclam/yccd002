/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyChoosePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyChoosePojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 责任代码 */
    private String DutyCode; 
    /** 责任名称 */
    private String DutyName; 
    /** 关联责任 */
    private String RelaDutyCode; 
    /** 关联责任名称 */
    private String RelaDutyName; 
    /** 关联关系 */
    private String Relation; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getDutyName() {
        return DutyName;
    }
    public void setDutyName(String aDutyName) {
        DutyName = aDutyName;
    }
    public String getRelaDutyCode() {
        return RelaDutyCode;
    }
    public void setRelaDutyCode(String aRelaDutyCode) {
        RelaDutyCode = aRelaDutyCode;
    }
    public String getRelaDutyName() {
        return RelaDutyName;
    }
    public void setRelaDutyName(String aRelaDutyName) {
        RelaDutyName = aRelaDutyName;
    }
    public String getRelation() {
        return Relation;
    }
    public void setRelation(String aRelation) {
        Relation = aRelation;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 2;
        }
        if( strFieldName.equals("DutyName") ) {
            return 3;
        }
        if( strFieldName.equals("RelaDutyCode") ) {
            return 4;
        }
        if( strFieldName.equals("RelaDutyName") ) {
            return 5;
        }
        if( strFieldName.equals("Relation") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "DutyName";
                break;
            case 4:
                strFieldName = "RelaDutyCode";
                break;
            case 5:
                strFieldName = "RelaDutyName";
                break;
            case 6:
                strFieldName = "Relation";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "DUTYNAME":
                return Schema.TYPE_STRING;
            case "RELADUTYCODE":
                return Schema.TYPE_STRING;
            case "RELADUTYNAME":
                return Schema.TYPE_STRING;
            case "RELATION":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("DutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyName));
        }
        if (FCode.equalsIgnoreCase("RelaDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaDutyCode));
        }
        if (FCode.equalsIgnoreCase("RelaDutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaDutyName));
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 3:
                strFieldValue = String.valueOf(DutyName);
                break;
            case 4:
                strFieldValue = String.valueOf(RelaDutyCode);
                break;
            case 5:
                strFieldValue = String.valueOf(RelaDutyName);
                break;
            case 6:
                strFieldValue = String.valueOf(Relation);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyName = FValue.trim();
            }
            else
                DutyName = null;
        }
        if (FCode.equalsIgnoreCase("RelaDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelaDutyCode = FValue.trim();
            }
            else
                RelaDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("RelaDutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelaDutyName = FValue.trim();
            }
            else
                RelaDutyName = null;
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Relation = FValue.trim();
            }
            else
                Relation = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyChoosePojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", DutyCode="+DutyCode +
            ", DutyName="+DutyName +
            ", RelaDutyCode="+RelaDutyCode +
            ", RelaDutyName="+RelaDutyName +
            ", Relation="+Relation +"]";
    }
}
