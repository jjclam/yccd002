/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBContPlanDetailSubPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBContPlanDetailSubPojo implements Pojo,Serializable {
    // @Field
    /** 保单号码 */
    private String PolicyNo; 
    /** 投保单号 */
    private String PropNo; 
    /** 系统方案编码 */
    private String SysPlanCode; 
    /** 方案编码 */
    private String PlanCode; 
    /** 险种编码 */
    private String RiskCode; 
    /** 责任编码 */
    private String DutyCode; 
    /** P1 */
    private String P1; 
    /** P2 */
    private String P2; 
    /** P3 */
    private String P3; 
    /** P4 */
    private String P4; 
    /** P5 */
    private String P5; 
    /** P6 */
    private String P6; 
    /** P7 */
    private String P7; 
    /** P8 */
    private String P8; 
    /** P9 */
    private String P9; 
    /** P10 */
    private String P10; 
    /** P11 */
    private String P11; 
    /** P12 */
    private String P12; 
    /** P13 */
    private String P13; 
    /** P14 */
    private String P14; 
    /** P15 */
    private String P15; 
    /** P16 */
    private String P16; 
    /** P17 */
    private String P17; 
    /** P18 */
    private String P18; 
    /** P19 */
    private String P19; 
    /** P20 */
    private String P20; 
    /** P21 */
    private String P21; 
    /** P22 */
    private String P22; 
    /** P23 */
    private String P23; 
    /** P24 */
    private String P24; 
    /** P25 */
    private String P25; 
    /** P26 */
    private String P26; 
    /** P27 */
    private String P27; 
    /** P28 */
    private String P28; 
    /** P29 */
    private String P29; 
    /** P30 */
    private String P30; 


    public static final int FIELDNUM = 36;    // 数据库表的字段个数
    public String getPolicyNo() {
        return PolicyNo;
    }
    public void setPolicyNo(String aPolicyNo) {
        PolicyNo = aPolicyNo;
    }
    public String getPropNo() {
        return PropNo;
    }
    public void setPropNo(String aPropNo) {
        PropNo = aPropNo;
    }
    public String getSysPlanCode() {
        return SysPlanCode;
    }
    public void setSysPlanCode(String aSysPlanCode) {
        SysPlanCode = aSysPlanCode;
    }
    public String getPlanCode() {
        return PlanCode;
    }
    public void setPlanCode(String aPlanCode) {
        PlanCode = aPlanCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getP1() {
        return P1;
    }
    public void setP1(String aP1) {
        P1 = aP1;
    }
    public String getP2() {
        return P2;
    }
    public void setP2(String aP2) {
        P2 = aP2;
    }
    public String getP3() {
        return P3;
    }
    public void setP3(String aP3) {
        P3 = aP3;
    }
    public String getP4() {
        return P4;
    }
    public void setP4(String aP4) {
        P4 = aP4;
    }
    public String getP5() {
        return P5;
    }
    public void setP5(String aP5) {
        P5 = aP5;
    }
    public String getP6() {
        return P6;
    }
    public void setP6(String aP6) {
        P6 = aP6;
    }
    public String getP7() {
        return P7;
    }
    public void setP7(String aP7) {
        P7 = aP7;
    }
    public String getP8() {
        return P8;
    }
    public void setP8(String aP8) {
        P8 = aP8;
    }
    public String getP9() {
        return P9;
    }
    public void setP9(String aP9) {
        P9 = aP9;
    }
    public String getP10() {
        return P10;
    }
    public void setP10(String aP10) {
        P10 = aP10;
    }
    public String getP11() {
        return P11;
    }
    public void setP11(String aP11) {
        P11 = aP11;
    }
    public String getP12() {
        return P12;
    }
    public void setP12(String aP12) {
        P12 = aP12;
    }
    public String getP13() {
        return P13;
    }
    public void setP13(String aP13) {
        P13 = aP13;
    }
    public String getP14() {
        return P14;
    }
    public void setP14(String aP14) {
        P14 = aP14;
    }
    public String getP15() {
        return P15;
    }
    public void setP15(String aP15) {
        P15 = aP15;
    }
    public String getP16() {
        return P16;
    }
    public void setP16(String aP16) {
        P16 = aP16;
    }
    public String getP17() {
        return P17;
    }
    public void setP17(String aP17) {
        P17 = aP17;
    }
    public String getP18() {
        return P18;
    }
    public void setP18(String aP18) {
        P18 = aP18;
    }
    public String getP19() {
        return P19;
    }
    public void setP19(String aP19) {
        P19 = aP19;
    }
    public String getP20() {
        return P20;
    }
    public void setP20(String aP20) {
        P20 = aP20;
    }
    public String getP21() {
        return P21;
    }
    public void setP21(String aP21) {
        P21 = aP21;
    }
    public String getP22() {
        return P22;
    }
    public void setP22(String aP22) {
        P22 = aP22;
    }
    public String getP23() {
        return P23;
    }
    public void setP23(String aP23) {
        P23 = aP23;
    }
    public String getP24() {
        return P24;
    }
    public void setP24(String aP24) {
        P24 = aP24;
    }
    public String getP25() {
        return P25;
    }
    public void setP25(String aP25) {
        P25 = aP25;
    }
    public String getP26() {
        return P26;
    }
    public void setP26(String aP26) {
        P26 = aP26;
    }
    public String getP27() {
        return P27;
    }
    public void setP27(String aP27) {
        P27 = aP27;
    }
    public String getP28() {
        return P28;
    }
    public void setP28(String aP28) {
        P28 = aP28;
    }
    public String getP29() {
        return P29;
    }
    public void setP29(String aP29) {
        P29 = aP29;
    }
    public String getP30() {
        return P30;
    }
    public void setP30(String aP30) {
        P30 = aP30;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PolicyNo") ) {
            return 0;
        }
        if( strFieldName.equals("PropNo") ) {
            return 1;
        }
        if( strFieldName.equals("SysPlanCode") ) {
            return 2;
        }
        if( strFieldName.equals("PlanCode") ) {
            return 3;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 4;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 5;
        }
        if( strFieldName.equals("P1") ) {
            return 6;
        }
        if( strFieldName.equals("P2") ) {
            return 7;
        }
        if( strFieldName.equals("P3") ) {
            return 8;
        }
        if( strFieldName.equals("P4") ) {
            return 9;
        }
        if( strFieldName.equals("P5") ) {
            return 10;
        }
        if( strFieldName.equals("P6") ) {
            return 11;
        }
        if( strFieldName.equals("P7") ) {
            return 12;
        }
        if( strFieldName.equals("P8") ) {
            return 13;
        }
        if( strFieldName.equals("P9") ) {
            return 14;
        }
        if( strFieldName.equals("P10") ) {
            return 15;
        }
        if( strFieldName.equals("P11") ) {
            return 16;
        }
        if( strFieldName.equals("P12") ) {
            return 17;
        }
        if( strFieldName.equals("P13") ) {
            return 18;
        }
        if( strFieldName.equals("P14") ) {
            return 19;
        }
        if( strFieldName.equals("P15") ) {
            return 20;
        }
        if( strFieldName.equals("P16") ) {
            return 21;
        }
        if( strFieldName.equals("P17") ) {
            return 22;
        }
        if( strFieldName.equals("P18") ) {
            return 23;
        }
        if( strFieldName.equals("P19") ) {
            return 24;
        }
        if( strFieldName.equals("P20") ) {
            return 25;
        }
        if( strFieldName.equals("P21") ) {
            return 26;
        }
        if( strFieldName.equals("P22") ) {
            return 27;
        }
        if( strFieldName.equals("P23") ) {
            return 28;
        }
        if( strFieldName.equals("P24") ) {
            return 29;
        }
        if( strFieldName.equals("P25") ) {
            return 30;
        }
        if( strFieldName.equals("P26") ) {
            return 31;
        }
        if( strFieldName.equals("P27") ) {
            return 32;
        }
        if( strFieldName.equals("P28") ) {
            return 33;
        }
        if( strFieldName.equals("P29") ) {
            return 34;
        }
        if( strFieldName.equals("P30") ) {
            return 35;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PolicyNo";
                break;
            case 1:
                strFieldName = "PropNo";
                break;
            case 2:
                strFieldName = "SysPlanCode";
                break;
            case 3:
                strFieldName = "PlanCode";
                break;
            case 4:
                strFieldName = "RiskCode";
                break;
            case 5:
                strFieldName = "DutyCode";
                break;
            case 6:
                strFieldName = "P1";
                break;
            case 7:
                strFieldName = "P2";
                break;
            case 8:
                strFieldName = "P3";
                break;
            case 9:
                strFieldName = "P4";
                break;
            case 10:
                strFieldName = "P5";
                break;
            case 11:
                strFieldName = "P6";
                break;
            case 12:
                strFieldName = "P7";
                break;
            case 13:
                strFieldName = "P8";
                break;
            case 14:
                strFieldName = "P9";
                break;
            case 15:
                strFieldName = "P10";
                break;
            case 16:
                strFieldName = "P11";
                break;
            case 17:
                strFieldName = "P12";
                break;
            case 18:
                strFieldName = "P13";
                break;
            case 19:
                strFieldName = "P14";
                break;
            case 20:
                strFieldName = "P15";
                break;
            case 21:
                strFieldName = "P16";
                break;
            case 22:
                strFieldName = "P17";
                break;
            case 23:
                strFieldName = "P18";
                break;
            case 24:
                strFieldName = "P19";
                break;
            case 25:
                strFieldName = "P20";
                break;
            case 26:
                strFieldName = "P21";
                break;
            case 27:
                strFieldName = "P22";
                break;
            case 28:
                strFieldName = "P23";
                break;
            case 29:
                strFieldName = "P24";
                break;
            case 30:
                strFieldName = "P25";
                break;
            case 31:
                strFieldName = "P26";
                break;
            case 32:
                strFieldName = "P27";
                break;
            case 33:
                strFieldName = "P28";
                break;
            case 34:
                strFieldName = "P29";
                break;
            case 35:
                strFieldName = "P30";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "PROPNO":
                return Schema.TYPE_STRING;
            case "SYSPLANCODE":
                return Schema.TYPE_STRING;
            case "PLANCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "P1":
                return Schema.TYPE_STRING;
            case "P2":
                return Schema.TYPE_STRING;
            case "P3":
                return Schema.TYPE_STRING;
            case "P4":
                return Schema.TYPE_STRING;
            case "P5":
                return Schema.TYPE_STRING;
            case "P6":
                return Schema.TYPE_STRING;
            case "P7":
                return Schema.TYPE_STRING;
            case "P8":
                return Schema.TYPE_STRING;
            case "P9":
                return Schema.TYPE_STRING;
            case "P10":
                return Schema.TYPE_STRING;
            case "P11":
                return Schema.TYPE_STRING;
            case "P12":
                return Schema.TYPE_STRING;
            case "P13":
                return Schema.TYPE_STRING;
            case "P14":
                return Schema.TYPE_STRING;
            case "P15":
                return Schema.TYPE_STRING;
            case "P16":
                return Schema.TYPE_STRING;
            case "P17":
                return Schema.TYPE_STRING;
            case "P18":
                return Schema.TYPE_STRING;
            case "P19":
                return Schema.TYPE_STRING;
            case "P20":
                return Schema.TYPE_STRING;
            case "P21":
                return Schema.TYPE_STRING;
            case "P22":
                return Schema.TYPE_STRING;
            case "P23":
                return Schema.TYPE_STRING;
            case "P24":
                return Schema.TYPE_STRING;
            case "P25":
                return Schema.TYPE_STRING;
            case "P26":
                return Schema.TYPE_STRING;
            case "P27":
                return Schema.TYPE_STRING;
            case "P28":
                return Schema.TYPE_STRING;
            case "P29":
                return Schema.TYPE_STRING;
            case "P30":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
        }
        if (FCode.equalsIgnoreCase("PropNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PropNo));
        }
        if (FCode.equalsIgnoreCase("SysPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SysPlanCode));
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("P1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P1));
        }
        if (FCode.equalsIgnoreCase("P2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P2));
        }
        if (FCode.equalsIgnoreCase("P3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P3));
        }
        if (FCode.equalsIgnoreCase("P4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P4));
        }
        if (FCode.equalsIgnoreCase("P5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P5));
        }
        if (FCode.equalsIgnoreCase("P6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P6));
        }
        if (FCode.equalsIgnoreCase("P7")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P7));
        }
        if (FCode.equalsIgnoreCase("P8")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P8));
        }
        if (FCode.equalsIgnoreCase("P9")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P9));
        }
        if (FCode.equalsIgnoreCase("P10")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P10));
        }
        if (FCode.equalsIgnoreCase("P11")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P11));
        }
        if (FCode.equalsIgnoreCase("P12")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P12));
        }
        if (FCode.equalsIgnoreCase("P13")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P13));
        }
        if (FCode.equalsIgnoreCase("P14")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P14));
        }
        if (FCode.equalsIgnoreCase("P15")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P15));
        }
        if (FCode.equalsIgnoreCase("P16")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P16));
        }
        if (FCode.equalsIgnoreCase("P17")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P17));
        }
        if (FCode.equalsIgnoreCase("P18")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P18));
        }
        if (FCode.equalsIgnoreCase("P19")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P19));
        }
        if (FCode.equalsIgnoreCase("P20")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P20));
        }
        if (FCode.equalsIgnoreCase("P21")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P21));
        }
        if (FCode.equalsIgnoreCase("P22")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P22));
        }
        if (FCode.equalsIgnoreCase("P23")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P23));
        }
        if (FCode.equalsIgnoreCase("P24")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P24));
        }
        if (FCode.equalsIgnoreCase("P25")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P25));
        }
        if (FCode.equalsIgnoreCase("P26")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P26));
        }
        if (FCode.equalsIgnoreCase("P27")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P27));
        }
        if (FCode.equalsIgnoreCase("P28")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P28));
        }
        if (FCode.equalsIgnoreCase("P29")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P29));
        }
        if (FCode.equalsIgnoreCase("P30")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P30));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PolicyNo);
                break;
            case 1:
                strFieldValue = String.valueOf(PropNo);
                break;
            case 2:
                strFieldValue = String.valueOf(SysPlanCode);
                break;
            case 3:
                strFieldValue = String.valueOf(PlanCode);
                break;
            case 4:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 5:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 6:
                strFieldValue = String.valueOf(P1);
                break;
            case 7:
                strFieldValue = String.valueOf(P2);
                break;
            case 8:
                strFieldValue = String.valueOf(P3);
                break;
            case 9:
                strFieldValue = String.valueOf(P4);
                break;
            case 10:
                strFieldValue = String.valueOf(P5);
                break;
            case 11:
                strFieldValue = String.valueOf(P6);
                break;
            case 12:
                strFieldValue = String.valueOf(P7);
                break;
            case 13:
                strFieldValue = String.valueOf(P8);
                break;
            case 14:
                strFieldValue = String.valueOf(P9);
                break;
            case 15:
                strFieldValue = String.valueOf(P10);
                break;
            case 16:
                strFieldValue = String.valueOf(P11);
                break;
            case 17:
                strFieldValue = String.valueOf(P12);
                break;
            case 18:
                strFieldValue = String.valueOf(P13);
                break;
            case 19:
                strFieldValue = String.valueOf(P14);
                break;
            case 20:
                strFieldValue = String.valueOf(P15);
                break;
            case 21:
                strFieldValue = String.valueOf(P16);
                break;
            case 22:
                strFieldValue = String.valueOf(P17);
                break;
            case 23:
                strFieldValue = String.valueOf(P18);
                break;
            case 24:
                strFieldValue = String.valueOf(P19);
                break;
            case 25:
                strFieldValue = String.valueOf(P20);
                break;
            case 26:
                strFieldValue = String.valueOf(P21);
                break;
            case 27:
                strFieldValue = String.valueOf(P22);
                break;
            case 28:
                strFieldValue = String.valueOf(P23);
                break;
            case 29:
                strFieldValue = String.valueOf(P24);
                break;
            case 30:
                strFieldValue = String.valueOf(P25);
                break;
            case 31:
                strFieldValue = String.valueOf(P26);
                break;
            case 32:
                strFieldValue = String.valueOf(P27);
                break;
            case 33:
                strFieldValue = String.valueOf(P28);
                break;
            case 34:
                strFieldValue = String.valueOf(P29);
                break;
            case 35:
                strFieldValue = String.valueOf(P30);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyNo = FValue.trim();
            }
            else
                PolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("PropNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PropNo = FValue.trim();
            }
            else
                PropNo = null;
        }
        if (FCode.equalsIgnoreCase("SysPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SysPlanCode = FValue.trim();
            }
            else
                SysPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
                PlanCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("P1")) {
            if( FValue != null && !FValue.equals(""))
            {
                P1 = FValue.trim();
            }
            else
                P1 = null;
        }
        if (FCode.equalsIgnoreCase("P2")) {
            if( FValue != null && !FValue.equals(""))
            {
                P2 = FValue.trim();
            }
            else
                P2 = null;
        }
        if (FCode.equalsIgnoreCase("P3")) {
            if( FValue != null && !FValue.equals(""))
            {
                P3 = FValue.trim();
            }
            else
                P3 = null;
        }
        if (FCode.equalsIgnoreCase("P4")) {
            if( FValue != null && !FValue.equals(""))
            {
                P4 = FValue.trim();
            }
            else
                P4 = null;
        }
        if (FCode.equalsIgnoreCase("P5")) {
            if( FValue != null && !FValue.equals(""))
            {
                P5 = FValue.trim();
            }
            else
                P5 = null;
        }
        if (FCode.equalsIgnoreCase("P6")) {
            if( FValue != null && !FValue.equals(""))
            {
                P6 = FValue.trim();
            }
            else
                P6 = null;
        }
        if (FCode.equalsIgnoreCase("P7")) {
            if( FValue != null && !FValue.equals(""))
            {
                P7 = FValue.trim();
            }
            else
                P7 = null;
        }
        if (FCode.equalsIgnoreCase("P8")) {
            if( FValue != null && !FValue.equals(""))
            {
                P8 = FValue.trim();
            }
            else
                P8 = null;
        }
        if (FCode.equalsIgnoreCase("P9")) {
            if( FValue != null && !FValue.equals(""))
            {
                P9 = FValue.trim();
            }
            else
                P9 = null;
        }
        if (FCode.equalsIgnoreCase("P10")) {
            if( FValue != null && !FValue.equals(""))
            {
                P10 = FValue.trim();
            }
            else
                P10 = null;
        }
        if (FCode.equalsIgnoreCase("P11")) {
            if( FValue != null && !FValue.equals(""))
            {
                P11 = FValue.trim();
            }
            else
                P11 = null;
        }
        if (FCode.equalsIgnoreCase("P12")) {
            if( FValue != null && !FValue.equals(""))
            {
                P12 = FValue.trim();
            }
            else
                P12 = null;
        }
        if (FCode.equalsIgnoreCase("P13")) {
            if( FValue != null && !FValue.equals(""))
            {
                P13 = FValue.trim();
            }
            else
                P13 = null;
        }
        if (FCode.equalsIgnoreCase("P14")) {
            if( FValue != null && !FValue.equals(""))
            {
                P14 = FValue.trim();
            }
            else
                P14 = null;
        }
        if (FCode.equalsIgnoreCase("P15")) {
            if( FValue != null && !FValue.equals(""))
            {
                P15 = FValue.trim();
            }
            else
                P15 = null;
        }
        if (FCode.equalsIgnoreCase("P16")) {
            if( FValue != null && !FValue.equals(""))
            {
                P16 = FValue.trim();
            }
            else
                P16 = null;
        }
        if (FCode.equalsIgnoreCase("P17")) {
            if( FValue != null && !FValue.equals(""))
            {
                P17 = FValue.trim();
            }
            else
                P17 = null;
        }
        if (FCode.equalsIgnoreCase("P18")) {
            if( FValue != null && !FValue.equals(""))
            {
                P18 = FValue.trim();
            }
            else
                P18 = null;
        }
        if (FCode.equalsIgnoreCase("P19")) {
            if( FValue != null && !FValue.equals(""))
            {
                P19 = FValue.trim();
            }
            else
                P19 = null;
        }
        if (FCode.equalsIgnoreCase("P20")) {
            if( FValue != null && !FValue.equals(""))
            {
                P20 = FValue.trim();
            }
            else
                P20 = null;
        }
        if (FCode.equalsIgnoreCase("P21")) {
            if( FValue != null && !FValue.equals(""))
            {
                P21 = FValue.trim();
            }
            else
                P21 = null;
        }
        if (FCode.equalsIgnoreCase("P22")) {
            if( FValue != null && !FValue.equals(""))
            {
                P22 = FValue.trim();
            }
            else
                P22 = null;
        }
        if (FCode.equalsIgnoreCase("P23")) {
            if( FValue != null && !FValue.equals(""))
            {
                P23 = FValue.trim();
            }
            else
                P23 = null;
        }
        if (FCode.equalsIgnoreCase("P24")) {
            if( FValue != null && !FValue.equals(""))
            {
                P24 = FValue.trim();
            }
            else
                P24 = null;
        }
        if (FCode.equalsIgnoreCase("P25")) {
            if( FValue != null && !FValue.equals(""))
            {
                P25 = FValue.trim();
            }
            else
                P25 = null;
        }
        if (FCode.equalsIgnoreCase("P26")) {
            if( FValue != null && !FValue.equals(""))
            {
                P26 = FValue.trim();
            }
            else
                P26 = null;
        }
        if (FCode.equalsIgnoreCase("P27")) {
            if( FValue != null && !FValue.equals(""))
            {
                P27 = FValue.trim();
            }
            else
                P27 = null;
        }
        if (FCode.equalsIgnoreCase("P28")) {
            if( FValue != null && !FValue.equals(""))
            {
                P28 = FValue.trim();
            }
            else
                P28 = null;
        }
        if (FCode.equalsIgnoreCase("P29")) {
            if( FValue != null && !FValue.equals(""))
            {
                P29 = FValue.trim();
            }
            else
                P29 = null;
        }
        if (FCode.equalsIgnoreCase("P30")) {
            if( FValue != null && !FValue.equals(""))
            {
                P30 = FValue.trim();
            }
            else
                P30 = null;
        }
        return true;
    }


    public String toString() {
    return "LBContPlanDetailSubPojo [" +
            "PolicyNo="+PolicyNo +
            ", PropNo="+PropNo +
            ", SysPlanCode="+SysPlanCode +
            ", PlanCode="+PlanCode +
            ", RiskCode="+RiskCode +
            ", DutyCode="+DutyCode +
            ", P1="+P1 +
            ", P2="+P2 +
            ", P3="+P3 +
            ", P4="+P4 +
            ", P5="+P5 +
            ", P6="+P6 +
            ", P7="+P7 +
            ", P8="+P8 +
            ", P9="+P9 +
            ", P10="+P10 +
            ", P11="+P11 +
            ", P12="+P12 +
            ", P13="+P13 +
            ", P14="+P14 +
            ", P15="+P15 +
            ", P16="+P16 +
            ", P17="+P17 +
            ", P18="+P18 +
            ", P19="+P19 +
            ", P20="+P20 +
            ", P21="+P21 +
            ", P22="+P22 +
            ", P23="+P23 +
            ", P24="+P24 +
            ", P25="+P25 +
            ", P26="+P26 +
            ", P27="+P27 +
            ", P28="+P28 +
            ", P29="+P29 +
            ", P30="+P30 +"]";
    }
}
