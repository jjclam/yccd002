/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMProdRulesSuitChnlPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMProdRulesSuitChnlPojo implements Pojo,Serializable {
    // @Field
    /** 险种销售渠道 */
    private String SaleChnl; 
    /** 规则适用销售渠道 */
    private String RulesChnl; 
    /** 规则适用销售渠道名称 */
    private String RulesChnlName; 


    public static final int FIELDNUM = 3;    // 数据库表的字段个数
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getRulesChnl() {
        return RulesChnl;
    }
    public void setRulesChnl(String aRulesChnl) {
        RulesChnl = aRulesChnl;
    }
    public String getRulesChnlName() {
        return RulesChnlName;
    }
    public void setRulesChnlName(String aRulesChnlName) {
        RulesChnlName = aRulesChnlName;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SaleChnl") ) {
            return 0;
        }
        if( strFieldName.equals("RulesChnl") ) {
            return 1;
        }
        if( strFieldName.equals("RulesChnlName") ) {
            return 2;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SaleChnl";
                break;
            case 1:
                strFieldName = "RulesChnl";
                break;
            case 2:
                strFieldName = "RulesChnlName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "RULESCHNL":
                return Schema.TYPE_STRING;
            case "RULESCHNLNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("RulesChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesChnl));
        }
        if (FCode.equalsIgnoreCase("RulesChnlName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesChnlName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 1:
                strFieldValue = String.valueOf(RulesChnl);
                break;
            case 2:
                strFieldValue = String.valueOf(RulesChnlName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("RulesChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesChnl = FValue.trim();
            }
            else
                RulesChnl = null;
        }
        if (FCode.equalsIgnoreCase("RulesChnlName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesChnlName = FValue.trim();
            }
            else
                RulesChnlName = null;
        }
        return true;
    }


    public String toString() {
    return "LMProdRulesSuitChnlPojo [" +
            "SaleChnl="+SaleChnl +
            ", RulesChnl="+RulesChnl +
            ", RulesChnlName="+RulesChnlName +"]";
    }
}
