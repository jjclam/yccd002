/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBGrpAddressDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBGrpAddressSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBGrpAddressSchema implements Schema, Cloneable {
    // @Field
    /** 客户号码 */
    private String CustomerNo;
    /** 地址号码 */
    private String AddressNo;
    /** 单位地址 */
    private String GrpAddress;
    /** 单位邮编 */
    private String GrpZipCode;
    /** 联系人1 */
    private String LinkMan1;
    /** 部门1 */
    private String Department1;
    /** 职务1 */
    private String HeadShip1;
    /** 联系电话1 */
    private String Phone1;
    /** E_mail1 */
    private String E_Mail1;
    /** 传真1 */
    private String Fax1;
    /** 联系人2 */
    private String LinkMan2;
    /** 部门2 */
    private String Department2;
    /** 职务2 */
    private String HeadShip2;
    /** 联系电话2 */
    private String Phone2;
    /** E_mail2 */
    private String E_Mail2;
    /** 传真2 */
    private String Fax2;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 手机1 */
    private String MobilePhone1;
    /** 手机2 */
    private String MobilePhone2;
    /** 证件类型1 */
    private String IDType;
    /** 证件号1 */
    private String IDNo;
    /** 证件有效日期 */
    private Date IDNoValidate;
    /** 证件类型2 */
    private String IDType2;
    /** 证件号2 */
    private String IDNo2;
    /** 证件有效日期2 */
    private Date IDNoValidate2;
    /** 省 */
    private String Province;
    /** 市 */
    private String City;
    /** 县 */
    private String County;
    /** 投保单位是否为美国企业 */
    private String TinNo;
    /** 投保单位美国纳税人识别号 */
    private String TinFlag;
    /** 投保单位是否有美国控制人 */
    private String USATinFlag;
    /** 美国控制人姓名 */
    private String USATinName;
    /** 美国控制人地址 */
    private String USATinAddress;
    /** 美国控制人纳税人识别号 */
    private String USATinNo;

    public static final int FIELDNUM = 38;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBGrpAddressSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "CustomerNo";
        pk[1] = "AddressNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBGrpAddressSchema cloned = (LBGrpAddressSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }
    public String getGrpAddress() {
        return GrpAddress;
    }
    public void setGrpAddress(String aGrpAddress) {
        GrpAddress = aGrpAddress;
    }
    public String getGrpZipCode() {
        return GrpZipCode;
    }
    public void setGrpZipCode(String aGrpZipCode) {
        GrpZipCode = aGrpZipCode;
    }
    public String getLinkMan1() {
        return LinkMan1;
    }
    public void setLinkMan1(String aLinkMan1) {
        LinkMan1 = aLinkMan1;
    }
    public String getDepartment1() {
        return Department1;
    }
    public void setDepartment1(String aDepartment1) {
        Department1 = aDepartment1;
    }
    public String getHeadShip1() {
        return HeadShip1;
    }
    public void setHeadShip1(String aHeadShip1) {
        HeadShip1 = aHeadShip1;
    }
    public String getPhone1() {
        return Phone1;
    }
    public void setPhone1(String aPhone1) {
        Phone1 = aPhone1;
    }
    public String getE_Mail1() {
        return E_Mail1;
    }
    public void setE_Mail1(String aE_Mail1) {
        E_Mail1 = aE_Mail1;
    }
    public String getFax1() {
        return Fax1;
    }
    public void setFax1(String aFax1) {
        Fax1 = aFax1;
    }
    public String getLinkMan2() {
        return LinkMan2;
    }
    public void setLinkMan2(String aLinkMan2) {
        LinkMan2 = aLinkMan2;
    }
    public String getDepartment2() {
        return Department2;
    }
    public void setDepartment2(String aDepartment2) {
        Department2 = aDepartment2;
    }
    public String getHeadShip2() {
        return HeadShip2;
    }
    public void setHeadShip2(String aHeadShip2) {
        HeadShip2 = aHeadShip2;
    }
    public String getPhone2() {
        return Phone2;
    }
    public void setPhone2(String aPhone2) {
        Phone2 = aPhone2;
    }
    public String getE_Mail2() {
        return E_Mail2;
    }
    public void setE_Mail2(String aE_Mail2) {
        E_Mail2 = aE_Mail2;
    }
    public String getFax2() {
        return Fax2;
    }
    public void setFax2(String aFax2) {
        Fax2 = aFax2;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getMobilePhone1() {
        return MobilePhone1;
    }
    public void setMobilePhone1(String aMobilePhone1) {
        MobilePhone1 = aMobilePhone1;
    }
    public String getMobilePhone2() {
        return MobilePhone2;
    }
    public void setMobilePhone2(String aMobilePhone2) {
        MobilePhone2 = aMobilePhone2;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getIDNoValidate() {
        if(IDNoValidate != null) {
            return fDate.getString(IDNoValidate);
        } else {
            return null;
        }
    }
    public void setIDNoValidate(Date aIDNoValidate) {
        IDNoValidate = aIDNoValidate;
    }
    public void setIDNoValidate(String aIDNoValidate) {
        if (aIDNoValidate != null && !aIDNoValidate.equals("")) {
            IDNoValidate = fDate.getDate(aIDNoValidate);
        } else
            IDNoValidate = null;
    }

    public String getIDType2() {
        return IDType2;
    }
    public void setIDType2(String aIDType2) {
        IDType2 = aIDType2;
    }
    public String getIDNo2() {
        return IDNo2;
    }
    public void setIDNo2(String aIDNo2) {
        IDNo2 = aIDNo2;
    }
    public String getIDNoValidate2() {
        if(IDNoValidate2 != null) {
            return fDate.getString(IDNoValidate2);
        } else {
            return null;
        }
    }
    public void setIDNoValidate2(Date aIDNoValidate2) {
        IDNoValidate2 = aIDNoValidate2;
    }
    public void setIDNoValidate2(String aIDNoValidate2) {
        if (aIDNoValidate2 != null && !aIDNoValidate2.equals("")) {
            IDNoValidate2 = fDate.getDate(aIDNoValidate2);
        } else
            IDNoValidate2 = null;
    }

    public String getProvince() {
        return Province;
    }
    public void setProvince(String aProvince) {
        Province = aProvince;
    }
    public String getCity() {
        return City;
    }
    public void setCity(String aCity) {
        City = aCity;
    }
    public String getCounty() {
        return County;
    }
    public void setCounty(String aCounty) {
        County = aCounty;
    }
    public String getTinNo() {
        return TinNo;
    }
    public void setTinNo(String aTinNo) {
        TinNo = aTinNo;
    }
    public String getTinFlag() {
        return TinFlag;
    }
    public void setTinFlag(String aTinFlag) {
        TinFlag = aTinFlag;
    }
    public String getUSATinFlag() {
        return USATinFlag;
    }
    public void setUSATinFlag(String aUSATinFlag) {
        USATinFlag = aUSATinFlag;
    }
    public String getUSATinName() {
        return USATinName;
    }
    public void setUSATinName(String aUSATinName) {
        USATinName = aUSATinName;
    }
    public String getUSATinAddress() {
        return USATinAddress;
    }
    public void setUSATinAddress(String aUSATinAddress) {
        USATinAddress = aUSATinAddress;
    }
    public String getUSATinNo() {
        return USATinNo;
    }
    public void setUSATinNo(String aUSATinNo) {
        USATinNo = aUSATinNo;
    }

    /**
    * 使用另外一个 LBGrpAddressSchema 对象给 Schema 赋值
    * @param: aLBGrpAddressSchema LBGrpAddressSchema
    **/
    public void setSchema(LBGrpAddressSchema aLBGrpAddressSchema) {
        this.CustomerNo = aLBGrpAddressSchema.getCustomerNo();
        this.AddressNo = aLBGrpAddressSchema.getAddressNo();
        this.GrpAddress = aLBGrpAddressSchema.getGrpAddress();
        this.GrpZipCode = aLBGrpAddressSchema.getGrpZipCode();
        this.LinkMan1 = aLBGrpAddressSchema.getLinkMan1();
        this.Department1 = aLBGrpAddressSchema.getDepartment1();
        this.HeadShip1 = aLBGrpAddressSchema.getHeadShip1();
        this.Phone1 = aLBGrpAddressSchema.getPhone1();
        this.E_Mail1 = aLBGrpAddressSchema.getE_Mail1();
        this.Fax1 = aLBGrpAddressSchema.getFax1();
        this.LinkMan2 = aLBGrpAddressSchema.getLinkMan2();
        this.Department2 = aLBGrpAddressSchema.getDepartment2();
        this.HeadShip2 = aLBGrpAddressSchema.getHeadShip2();
        this.Phone2 = aLBGrpAddressSchema.getPhone2();
        this.E_Mail2 = aLBGrpAddressSchema.getE_Mail2();
        this.Fax2 = aLBGrpAddressSchema.getFax2();
        this.Operator = aLBGrpAddressSchema.getOperator();
        this.MakeDate = fDate.getDate( aLBGrpAddressSchema.getMakeDate());
        this.MakeTime = aLBGrpAddressSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLBGrpAddressSchema.getModifyDate());
        this.ModifyTime = aLBGrpAddressSchema.getModifyTime();
        this.MobilePhone1 = aLBGrpAddressSchema.getMobilePhone1();
        this.MobilePhone2 = aLBGrpAddressSchema.getMobilePhone2();
        this.IDType = aLBGrpAddressSchema.getIDType();
        this.IDNo = aLBGrpAddressSchema.getIDNo();
        this.IDNoValidate = fDate.getDate( aLBGrpAddressSchema.getIDNoValidate());
        this.IDType2 = aLBGrpAddressSchema.getIDType2();
        this.IDNo2 = aLBGrpAddressSchema.getIDNo2();
        this.IDNoValidate2 = fDate.getDate( aLBGrpAddressSchema.getIDNoValidate2());
        this.Province = aLBGrpAddressSchema.getProvince();
        this.City = aLBGrpAddressSchema.getCity();
        this.County = aLBGrpAddressSchema.getCounty();
        this.TinNo = aLBGrpAddressSchema.getTinNo();
        this.TinFlag = aLBGrpAddressSchema.getTinFlag();
        this.USATinFlag = aLBGrpAddressSchema.getUSATinFlag();
        this.USATinName = aLBGrpAddressSchema.getUSATinName();
        this.USATinAddress = aLBGrpAddressSchema.getUSATinAddress();
        this.USATinNo = aLBGrpAddressSchema.getUSATinNo();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("AddressNo") == null )
                this.AddressNo = null;
            else
                this.AddressNo = rs.getString("AddressNo").trim();

            if( rs.getString("GrpAddress") == null )
                this.GrpAddress = null;
            else
                this.GrpAddress = rs.getString("GrpAddress").trim();

            if( rs.getString("GrpZipCode") == null )
                this.GrpZipCode = null;
            else
                this.GrpZipCode = rs.getString("GrpZipCode").trim();

            if( rs.getString("LinkMan1") == null )
                this.LinkMan1 = null;
            else
                this.LinkMan1 = rs.getString("LinkMan1").trim();

            if( rs.getString("Department1") == null )
                this.Department1 = null;
            else
                this.Department1 = rs.getString("Department1").trim();

            if( rs.getString("HeadShip1") == null )
                this.HeadShip1 = null;
            else
                this.HeadShip1 = rs.getString("HeadShip1").trim();

            if( rs.getString("Phone1") == null )
                this.Phone1 = null;
            else
                this.Phone1 = rs.getString("Phone1").trim();

            if( rs.getString("E_Mail1") == null )
                this.E_Mail1 = null;
            else
                this.E_Mail1 = rs.getString("E_Mail1").trim();

            if( rs.getString("Fax1") == null )
                this.Fax1 = null;
            else
                this.Fax1 = rs.getString("Fax1").trim();

            if( rs.getString("LinkMan2") == null )
                this.LinkMan2 = null;
            else
                this.LinkMan2 = rs.getString("LinkMan2").trim();

            if( rs.getString("Department2") == null )
                this.Department2 = null;
            else
                this.Department2 = rs.getString("Department2").trim();

            if( rs.getString("HeadShip2") == null )
                this.HeadShip2 = null;
            else
                this.HeadShip2 = rs.getString("HeadShip2").trim();

            if( rs.getString("Phone2") == null )
                this.Phone2 = null;
            else
                this.Phone2 = rs.getString("Phone2").trim();

            if( rs.getString("E_Mail2") == null )
                this.E_Mail2 = null;
            else
                this.E_Mail2 = rs.getString("E_Mail2").trim();

            if( rs.getString("Fax2") == null )
                this.Fax2 = null;
            else
                this.Fax2 = rs.getString("Fax2").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("MobilePhone1") == null )
                this.MobilePhone1 = null;
            else
                this.MobilePhone1 = rs.getString("MobilePhone1").trim();

            if( rs.getString("MobilePhone2") == null )
                this.MobilePhone2 = null;
            else
                this.MobilePhone2 = rs.getString("MobilePhone2").trim();

            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            this.IDNoValidate = rs.getDate("IDNoValidate");
            if( rs.getString("IDType2") == null )
                this.IDType2 = null;
            else
                this.IDType2 = rs.getString("IDType2").trim();

            if( rs.getString("IDNo2") == null )
                this.IDNo2 = null;
            else
                this.IDNo2 = rs.getString("IDNo2").trim();

            this.IDNoValidate2 = rs.getDate("IDNoValidate2");
            if( rs.getString("Province") == null )
                this.Province = null;
            else
                this.Province = rs.getString("Province").trim();

            if( rs.getString("City") == null )
                this.City = null;
            else
                this.City = rs.getString("City").trim();

            if( rs.getString("County") == null )
                this.County = null;
            else
                this.County = rs.getString("County").trim();

            if( rs.getString("TinNo") == null )
                this.TinNo = null;
            else
                this.TinNo = rs.getString("TinNo").trim();

            if( rs.getString("TinFlag") == null )
                this.TinFlag = null;
            else
                this.TinFlag = rs.getString("TinFlag").trim();

            if( rs.getString("USATinFlag") == null )
                this.USATinFlag = null;
            else
                this.USATinFlag = rs.getString("USATinFlag").trim();

            if( rs.getString("USATinName") == null )
                this.USATinName = null;
            else
                this.USATinName = rs.getString("USATinName").trim();

            if( rs.getString("USATinAddress") == null )
                this.USATinAddress = null;
            else
                this.USATinAddress = rs.getString("USATinAddress").trim();

            if( rs.getString("USATinNo") == null )
                this.USATinNo = null;
            else
                this.USATinNo = rs.getString("USATinNo").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpAddressSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBGrpAddressSchema getSchema() {
        LBGrpAddressSchema aLBGrpAddressSchema = new LBGrpAddressSchema();
        aLBGrpAddressSchema.setSchema(this);
        return aLBGrpAddressSchema;
    }

    public LBGrpAddressDB getDB() {
        LBGrpAddressDB aDBOper = new LBGrpAddressDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBGrpAddress描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LinkMan1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Department1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HeadShip1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(E_Mail1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LinkMan2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Department2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HeadShip2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(E_Mail2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MobilePhone1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MobilePhone2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( IDNoValidate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( IDNoValidate2 ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Province)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(City)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(County)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TinNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TinFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(USATinFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(USATinName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(USATinAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(USATinNo));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBGrpAddress>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            GrpAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GrpZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            LinkMan1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Department1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            HeadShip1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Phone1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            E_Mail1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            Fax1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            LinkMan2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            Department2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            HeadShip2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            Phone2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            E_Mail2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            Fax2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            MobilePhone1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            MobilePhone2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            IDNoValidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            IDType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            IDNo2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            IDNoValidate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER));
            Province = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            City = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            County = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            TinNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            TinFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            USATinFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            USATinName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            USATinAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            USATinNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGrpAddressSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("GrpAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAddress));
        }
        if (FCode.equalsIgnoreCase("GrpZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpZipCode));
        }
        if (FCode.equalsIgnoreCase("LinkMan1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan1));
        }
        if (FCode.equalsIgnoreCase("Department1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Department1));
        }
        if (FCode.equalsIgnoreCase("HeadShip1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadShip1));
        }
        if (FCode.equalsIgnoreCase("Phone1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone1));
        }
        if (FCode.equalsIgnoreCase("E_Mail1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_Mail1));
        }
        if (FCode.equalsIgnoreCase("Fax1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax1));
        }
        if (FCode.equalsIgnoreCase("LinkMan2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan2));
        }
        if (FCode.equalsIgnoreCase("Department2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Department2));
        }
        if (FCode.equalsIgnoreCase("HeadShip2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadShip2));
        }
        if (FCode.equalsIgnoreCase("Phone2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone2));
        }
        if (FCode.equalsIgnoreCase("E_Mail2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_Mail2));
        }
        if (FCode.equalsIgnoreCase("Fax2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax2));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("MobilePhone1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MobilePhone1));
        }
        if (FCode.equalsIgnoreCase("MobilePhone2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MobilePhone2));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("IDNoValidate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDNoValidate()));
        }
        if (FCode.equalsIgnoreCase("IDType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType2));
        }
        if (FCode.equalsIgnoreCase("IDNo2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo2));
        }
        if (FCode.equalsIgnoreCase("IDNoValidate2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDNoValidate2()));
        }
        if (FCode.equalsIgnoreCase("Province")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Province));
        }
        if (FCode.equalsIgnoreCase("City")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(City));
        }
        if (FCode.equalsIgnoreCase("County")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(County));
        }
        if (FCode.equalsIgnoreCase("TinNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TinNo));
        }
        if (FCode.equalsIgnoreCase("TinFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TinFlag));
        }
        if (FCode.equalsIgnoreCase("USATinFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USATinFlag));
        }
        if (FCode.equalsIgnoreCase("USATinName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USATinName));
        }
        if (FCode.equalsIgnoreCase("USATinAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USATinAddress));
        }
        if (FCode.equalsIgnoreCase("USATinNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USATinNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AddressNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpAddress);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpZipCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(LinkMan1);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Department1);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(HeadShip1);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Phone1);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(E_Mail1);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Fax1);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(LinkMan2);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Department2);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(HeadShip2);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Phone2);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(E_Mail2);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Fax2);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MobilePhone1);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MobilePhone2);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDNoValidate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(IDType2);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(IDNo2);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDNoValidate2()));
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(Province);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(City);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(County);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(TinNo);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(TinFlag);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(USATinFlag);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(USATinName);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(USATinAddress);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(USATinNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddressNo = FValue.trim();
            }
            else
                AddressNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAddress = FValue.trim();
            }
            else
                GrpAddress = null;
        }
        if (FCode.equalsIgnoreCase("GrpZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpZipCode = FValue.trim();
            }
            else
                GrpZipCode = null;
        }
        if (FCode.equalsIgnoreCase("LinkMan1")) {
            if( FValue != null && !FValue.equals(""))
            {
                LinkMan1 = FValue.trim();
            }
            else
                LinkMan1 = null;
        }
        if (FCode.equalsIgnoreCase("Department1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Department1 = FValue.trim();
            }
            else
                Department1 = null;
        }
        if (FCode.equalsIgnoreCase("HeadShip1")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadShip1 = FValue.trim();
            }
            else
                HeadShip1 = null;
        }
        if (FCode.equalsIgnoreCase("Phone1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone1 = FValue.trim();
            }
            else
                Phone1 = null;
        }
        if (FCode.equalsIgnoreCase("E_Mail1")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_Mail1 = FValue.trim();
            }
            else
                E_Mail1 = null;
        }
        if (FCode.equalsIgnoreCase("Fax1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax1 = FValue.trim();
            }
            else
                Fax1 = null;
        }
        if (FCode.equalsIgnoreCase("LinkMan2")) {
            if( FValue != null && !FValue.equals(""))
            {
                LinkMan2 = FValue.trim();
            }
            else
                LinkMan2 = null;
        }
        if (FCode.equalsIgnoreCase("Department2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Department2 = FValue.trim();
            }
            else
                Department2 = null;
        }
        if (FCode.equalsIgnoreCase("HeadShip2")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadShip2 = FValue.trim();
            }
            else
                HeadShip2 = null;
        }
        if (FCode.equalsIgnoreCase("Phone2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone2 = FValue.trim();
            }
            else
                Phone2 = null;
        }
        if (FCode.equalsIgnoreCase("E_Mail2")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_Mail2 = FValue.trim();
            }
            else
                E_Mail2 = null;
        }
        if (FCode.equalsIgnoreCase("Fax2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax2 = FValue.trim();
            }
            else
                Fax2 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("MobilePhone1")) {
            if( FValue != null && !FValue.equals(""))
            {
                MobilePhone1 = FValue.trim();
            }
            else
                MobilePhone1 = null;
        }
        if (FCode.equalsIgnoreCase("MobilePhone2")) {
            if( FValue != null && !FValue.equals(""))
            {
                MobilePhone2 = FValue.trim();
            }
            else
                MobilePhone2 = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("IDNoValidate")) {
            if(FValue != null && !FValue.equals("")) {
                IDNoValidate = fDate.getDate( FValue );
            }
            else
                IDNoValidate = null;
        }
        if (FCode.equalsIgnoreCase("IDType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType2 = FValue.trim();
            }
            else
                IDType2 = null;
        }
        if (FCode.equalsIgnoreCase("IDNo2")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo2 = FValue.trim();
            }
            else
                IDNo2 = null;
        }
        if (FCode.equalsIgnoreCase("IDNoValidate2")) {
            if(FValue != null && !FValue.equals("")) {
                IDNoValidate2 = fDate.getDate( FValue );
            }
            else
                IDNoValidate2 = null;
        }
        if (FCode.equalsIgnoreCase("Province")) {
            if( FValue != null && !FValue.equals(""))
            {
                Province = FValue.trim();
            }
            else
                Province = null;
        }
        if (FCode.equalsIgnoreCase("City")) {
            if( FValue != null && !FValue.equals(""))
            {
                City = FValue.trim();
            }
            else
                City = null;
        }
        if (FCode.equalsIgnoreCase("County")) {
            if( FValue != null && !FValue.equals(""))
            {
                County = FValue.trim();
            }
            else
                County = null;
        }
        if (FCode.equalsIgnoreCase("TinNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TinNo = FValue.trim();
            }
            else
                TinNo = null;
        }
        if (FCode.equalsIgnoreCase("TinFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TinFlag = FValue.trim();
            }
            else
                TinFlag = null;
        }
        if (FCode.equalsIgnoreCase("USATinFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                USATinFlag = FValue.trim();
            }
            else
                USATinFlag = null;
        }
        if (FCode.equalsIgnoreCase("USATinName")) {
            if( FValue != null && !FValue.equals(""))
            {
                USATinName = FValue.trim();
            }
            else
                USATinName = null;
        }
        if (FCode.equalsIgnoreCase("USATinAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                USATinAddress = FValue.trim();
            }
            else
                USATinAddress = null;
        }
        if (FCode.equalsIgnoreCase("USATinNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                USATinNo = FValue.trim();
            }
            else
                USATinNo = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBGrpAddressSchema other = (LBGrpAddressSchema)otherObject;
        return
            CustomerNo.equals(other.getCustomerNo())
            && AddressNo.equals(other.getAddressNo())
            && GrpAddress.equals(other.getGrpAddress())
            && GrpZipCode.equals(other.getGrpZipCode())
            && LinkMan1.equals(other.getLinkMan1())
            && Department1.equals(other.getDepartment1())
            && HeadShip1.equals(other.getHeadShip1())
            && Phone1.equals(other.getPhone1())
            && E_Mail1.equals(other.getE_Mail1())
            && Fax1.equals(other.getFax1())
            && LinkMan2.equals(other.getLinkMan2())
            && Department2.equals(other.getDepartment2())
            && HeadShip2.equals(other.getHeadShip2())
            && Phone2.equals(other.getPhone2())
            && E_Mail2.equals(other.getE_Mail2())
            && Fax2.equals(other.getFax2())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && MobilePhone1.equals(other.getMobilePhone1())
            && MobilePhone2.equals(other.getMobilePhone2())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && fDate.getString(IDNoValidate).equals(other.getIDNoValidate())
            && IDType2.equals(other.getIDType2())
            && IDNo2.equals(other.getIDNo2())
            && fDate.getString(IDNoValidate2).equals(other.getIDNoValidate2())
            && Province.equals(other.getProvince())
            && City.equals(other.getCity())
            && County.equals(other.getCounty())
            && TinNo.equals(other.getTinNo())
            && TinFlag.equals(other.getTinFlag())
            && USATinFlag.equals(other.getUSATinFlag())
            && USATinName.equals(other.getUSATinName())
            && USATinAddress.equals(other.getUSATinAddress())
            && USATinNo.equals(other.getUSATinNo());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CustomerNo") ) {
            return 0;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 1;
        }
        if( strFieldName.equals("GrpAddress") ) {
            return 2;
        }
        if( strFieldName.equals("GrpZipCode") ) {
            return 3;
        }
        if( strFieldName.equals("LinkMan1") ) {
            return 4;
        }
        if( strFieldName.equals("Department1") ) {
            return 5;
        }
        if( strFieldName.equals("HeadShip1") ) {
            return 6;
        }
        if( strFieldName.equals("Phone1") ) {
            return 7;
        }
        if( strFieldName.equals("E_Mail1") ) {
            return 8;
        }
        if( strFieldName.equals("Fax1") ) {
            return 9;
        }
        if( strFieldName.equals("LinkMan2") ) {
            return 10;
        }
        if( strFieldName.equals("Department2") ) {
            return 11;
        }
        if( strFieldName.equals("HeadShip2") ) {
            return 12;
        }
        if( strFieldName.equals("Phone2") ) {
            return 13;
        }
        if( strFieldName.equals("E_Mail2") ) {
            return 14;
        }
        if( strFieldName.equals("Fax2") ) {
            return 15;
        }
        if( strFieldName.equals("Operator") ) {
            return 16;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 17;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 19;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 20;
        }
        if( strFieldName.equals("MobilePhone1") ) {
            return 21;
        }
        if( strFieldName.equals("MobilePhone2") ) {
            return 22;
        }
        if( strFieldName.equals("IDType") ) {
            return 23;
        }
        if( strFieldName.equals("IDNo") ) {
            return 24;
        }
        if( strFieldName.equals("IDNoValidate") ) {
            return 25;
        }
        if( strFieldName.equals("IDType2") ) {
            return 26;
        }
        if( strFieldName.equals("IDNo2") ) {
            return 27;
        }
        if( strFieldName.equals("IDNoValidate2") ) {
            return 28;
        }
        if( strFieldName.equals("Province") ) {
            return 29;
        }
        if( strFieldName.equals("City") ) {
            return 30;
        }
        if( strFieldName.equals("County") ) {
            return 31;
        }
        if( strFieldName.equals("TinNo") ) {
            return 32;
        }
        if( strFieldName.equals("TinFlag") ) {
            return 33;
        }
        if( strFieldName.equals("USATinFlag") ) {
            return 34;
        }
        if( strFieldName.equals("USATinName") ) {
            return 35;
        }
        if( strFieldName.equals("USATinAddress") ) {
            return 36;
        }
        if( strFieldName.equals("USATinNo") ) {
            return 37;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CustomerNo";
                break;
            case 1:
                strFieldName = "AddressNo";
                break;
            case 2:
                strFieldName = "GrpAddress";
                break;
            case 3:
                strFieldName = "GrpZipCode";
                break;
            case 4:
                strFieldName = "LinkMan1";
                break;
            case 5:
                strFieldName = "Department1";
                break;
            case 6:
                strFieldName = "HeadShip1";
                break;
            case 7:
                strFieldName = "Phone1";
                break;
            case 8:
                strFieldName = "E_Mail1";
                break;
            case 9:
                strFieldName = "Fax1";
                break;
            case 10:
                strFieldName = "LinkMan2";
                break;
            case 11:
                strFieldName = "Department2";
                break;
            case 12:
                strFieldName = "HeadShip2";
                break;
            case 13:
                strFieldName = "Phone2";
                break;
            case 14:
                strFieldName = "E_Mail2";
                break;
            case 15:
                strFieldName = "Fax2";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "ModifyDate";
                break;
            case 20:
                strFieldName = "ModifyTime";
                break;
            case 21:
                strFieldName = "MobilePhone1";
                break;
            case 22:
                strFieldName = "MobilePhone2";
                break;
            case 23:
                strFieldName = "IDType";
                break;
            case 24:
                strFieldName = "IDNo";
                break;
            case 25:
                strFieldName = "IDNoValidate";
                break;
            case 26:
                strFieldName = "IDType2";
                break;
            case 27:
                strFieldName = "IDNo2";
                break;
            case 28:
                strFieldName = "IDNoValidate2";
                break;
            case 29:
                strFieldName = "Province";
                break;
            case 30:
                strFieldName = "City";
                break;
            case 31:
                strFieldName = "County";
                break;
            case 32:
                strFieldName = "TinNo";
                break;
            case 33:
                strFieldName = "TinFlag";
                break;
            case 34:
                strFieldName = "USATinFlag";
                break;
            case 35:
                strFieldName = "USATinName";
                break;
            case 36:
                strFieldName = "USATinAddress";
                break;
            case 37:
                strFieldName = "USATinNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_STRING;
            case "GRPADDRESS":
                return Schema.TYPE_STRING;
            case "GRPZIPCODE":
                return Schema.TYPE_STRING;
            case "LINKMAN1":
                return Schema.TYPE_STRING;
            case "DEPARTMENT1":
                return Schema.TYPE_STRING;
            case "HEADSHIP1":
                return Schema.TYPE_STRING;
            case "PHONE1":
                return Schema.TYPE_STRING;
            case "E_MAIL1":
                return Schema.TYPE_STRING;
            case "FAX1":
                return Schema.TYPE_STRING;
            case "LINKMAN2":
                return Schema.TYPE_STRING;
            case "DEPARTMENT2":
                return Schema.TYPE_STRING;
            case "HEADSHIP2":
                return Schema.TYPE_STRING;
            case "PHONE2":
                return Schema.TYPE_STRING;
            case "E_MAIL2":
                return Schema.TYPE_STRING;
            case "FAX2":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MOBILEPHONE1":
                return Schema.TYPE_STRING;
            case "MOBILEPHONE2":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "IDNOVALIDATE":
                return Schema.TYPE_DATE;
            case "IDTYPE2":
                return Schema.TYPE_STRING;
            case "IDNO2":
                return Schema.TYPE_STRING;
            case "IDNOVALIDATE2":
                return Schema.TYPE_DATE;
            case "PROVINCE":
                return Schema.TYPE_STRING;
            case "CITY":
                return Schema.TYPE_STRING;
            case "COUNTY":
                return Schema.TYPE_STRING;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            case "USATINFLAG":
                return Schema.TYPE_STRING;
            case "USATINNAME":
                return Schema.TYPE_STRING;
            case "USATINADDRESS":
                return Schema.TYPE_STRING;
            case "USATINNO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DATE;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_DATE;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
