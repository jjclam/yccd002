package com.sinosoft.lis.entity;

import java.io.Serializable;

/**
 * Created by grs on 2017/9/18.
 */
public class GlobalPojo implements Serializable {

    String BankCode;
    String ZoneNo;
    String BrNo;
    String TransNo;
    String MainRiskCode;
    String AutoRenew;
    /** 交易流水号 **/
    String SerialNo;
    /** 交易日期 **/
    String TransDate;
    /** 交易时间 **/
    String TransTime;
    /** 子渠道 **/
    String EntrustWay ;
    /** 单证号码 **/
    String ContPrtNo;
    /** 人员资格证编号 **/
    String AgentPersonCode;
    /** 1:是，0：否，若为空表示非转保**/
    String TransferFlag;
    /** 原保单号**/
    String OldPolicy;
    /** 	原保单密码**/
    String OldPolicyPwd;
    /** 原保单印刷号**/
    String OldVchNo;
    /** 柜员代码**/
    String TellerNo;
    /** 公司编码**/
    String CorpNo;
    /** 处理标志**/
    String FunctionFlag;
    /** 保险公司交易流水号**/
    String InsuSerial;

    String InterActNode;
    /**团险 录入的附加费**/
    double addPrem;
    /**指定生效日*/
    String PoliValidDate;
    /**指定险种生效日*/
    String RiskBeginDate;
    /**指定险种终止*/
    String RiskEndDate;
    //s3 回传的银行
    String InBankCode;
    //s3 回传的账号
    String InBankAccNo;
    /**微信指定银行标记*/
    String BankFlag;

    @Override
    public String toString() {
        return "GlobalPojo{" +
                "BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BrNo='" + BrNo + '\'' +
                ", TransNo='" + TransNo + '\'' +
                ", MainRiskCode='" + MainRiskCode + '\'' +
                ", AutoRenew='" + AutoRenew + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", EntrustWay='" + EntrustWay + '\'' +
                ", ContPrtNo='" + ContPrtNo + '\'' +
                ", AgentPersonCode='" + AgentPersonCode + '\'' +
                ", TransferFlag='" + TransferFlag + '\'' +
                ", OldPolicy='" + OldPolicy + '\'' +
                ", OldPolicyPwd='" + OldPolicyPwd + '\'' +
                ", OldVchNo='" + OldVchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", CorpNo='" + CorpNo + '\'' +
                ", FunctionFlag='" + FunctionFlag + '\'' +
                ", InsuSerial='" + InsuSerial + '\'' +
                ", InterActNode='" + InterActNode + '\'' +
                ", addPrem=" + addPrem +
                ", PoliValidDate='" + PoliValidDate + '\'' +
                ", RiskBeginDate='" + RiskBeginDate + '\'' +
                ", RiskEndDate='" + RiskEndDate + '\'' +
                ", InBankCode='" + InBankCode + '\'' +
                ", InBankAccNo='" + InBankAccNo + '\'' +
                ", BankFlag='" + BankFlag + '\'' +
                '}';
    }

    public String getBankFlag() {
        return BankFlag;
    }

    public void setBankFlag(String bankFlag) {
        BankFlag = bankFlag;
    }

    public String getInBankCode() {
        return InBankCode;
    }

    public void setInBankCode(String inBankCode) {
        InBankCode = inBankCode;
    }

    public String getInBankAccNo() {
        return InBankAccNo;
    }

    public void setInBankAccNo(String inBankAccNo) {
        InBankAccNo = inBankAccNo;
    }

    public String getRiskBeginDate() {
        return RiskBeginDate;
    }

    public void setRiskBeginDate(String riskBeginDate) {
        RiskBeginDate = riskBeginDate;
    }

    public String getRiskEndDate() {
        return RiskEndDate;
    }

    public void setRiskEndDate(String riskEndDate) {
        RiskEndDate = riskEndDate;
    }

    public String getPoliValidDate() {
        return PoliValidDate;
    }

    public void setPoliValidDate(String poliValidDate) {
        PoliValidDate = poliValidDate;
    }

    public double getAddPrem() {
        return addPrem;
    }

    public void setAddPrem(double addPrem) {
        this.addPrem = addPrem;
    }
    public void setAddPrem(String addPrem) {
        this.addPrem = Double.parseDouble(addPrem);
    }

    public String getAutoRenew() {
        return AutoRenew;
    }

    public void setAutoRenew(String autoRenew) {
        AutoRenew = autoRenew;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBrNo() {
        return BrNo;
    }

    public void setBrNo(String brNo) {
        BrNo = brNo;
    }

    public String getTransNo() {
        return TransNo;
    }

    public void setTransNo(String transNo) {
        TransNo = transNo;
    }

    public String getOldPolicy() {
        return OldPolicy;
    }

    public void setOldPolicy(String oldPolicy) {
        OldPolicy = oldPolicy;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getEntrustWay() {
        return EntrustWay;
    }

    public void setEntrustWay(String entrustWay) {
        EntrustWay = entrustWay;
    }
    public String getContPrtNo() {
        return ContPrtNo;
    }

    public void setContPrtNo(String contPrtNo) {
        ContPrtNo = contPrtNo;
    }

    public String getAgentPersonCode() {
        return AgentPersonCode;
    }

    public void setAgentPersonCode(String agentPersonCode) {
        AgentPersonCode = agentPersonCode;
    }

    public String getTransferFlag() {
        return TransferFlag;
    }

    public void setTransferFlag(String transferFlag) {
        TransferFlag = transferFlag;
    }

    public String getOldPolicyPwd() {
        return OldPolicyPwd;
    }

    public void setOldPolicyPwd(String oldPolicyPwd) {
        OldPolicyPwd = oldPolicyPwd;
    }

    public String getOldVchNo() {
        return OldVchNo;
    }

    public void setOldVchNo(String oldVchNo) {
        OldVchNo = oldVchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getCorpNo() {
        return CorpNo;
    }

    public void setCorpNo(String corpNo) {
        CorpNo = corpNo;
    }

    public String getFunctionFlag() {
        return FunctionFlag;
    }

    public void setFunctionFlag(String functionFlag) {
        FunctionFlag = functionFlag;
    }

    public String getInsuSerial() {
        return InsuSerial;
    }

    public void setInsuSerial(String insuSerial) {
        InsuSerial = insuSerial;
    }

    public String getInterActNode() {
        return InterActNode;
    }

    public void setInterActNode(String interActNode) {
        InterActNode = interActNode;
    }

}