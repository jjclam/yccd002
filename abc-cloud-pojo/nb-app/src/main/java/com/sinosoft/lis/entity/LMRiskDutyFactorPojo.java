/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskDutyFactorPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskDutyFactorPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 责任代码 */
    private String DutyCode; 
    /** 计算要素 */
    private String CalFactor; 
    /** 要素名称 */
    private String FactorName; 
    /** 计划要素类型 */
    private String CalFactorType; 
    /** 计算sql */
    private String CalSql; 
    /** 可选属性 */
    private String ChooseFlag; 
    /** 要素描述 */
    private String FactorNoti; 
    /** 要素顺序 */
    private int FactorOrder; 


    public static final int FIELDNUM = 9;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getCalFactor() {
        return CalFactor;
    }
    public void setCalFactor(String aCalFactor) {
        CalFactor = aCalFactor;
    }
    public String getFactorName() {
        return FactorName;
    }
    public void setFactorName(String aFactorName) {
        FactorName = aFactorName;
    }
    public String getCalFactorType() {
        return CalFactorType;
    }
    public void setCalFactorType(String aCalFactorType) {
        CalFactorType = aCalFactorType;
    }
    public String getCalSql() {
        return CalSql;
    }
    public void setCalSql(String aCalSql) {
        CalSql = aCalSql;
    }
    public String getChooseFlag() {
        return ChooseFlag;
    }
    public void setChooseFlag(String aChooseFlag) {
        ChooseFlag = aChooseFlag;
    }
    public String getFactorNoti() {
        return FactorNoti;
    }
    public void setFactorNoti(String aFactorNoti) {
        FactorNoti = aFactorNoti;
    }
    public int getFactorOrder() {
        return FactorOrder;
    }
    public void setFactorOrder(int aFactorOrder) {
        FactorOrder = aFactorOrder;
    }
    public void setFactorOrder(String aFactorOrder) {
        if (aFactorOrder != null && !aFactorOrder.equals("")) {
            Integer tInteger = new Integer(aFactorOrder);
            int i = tInteger.intValue();
            FactorOrder = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 1;
        }
        if( strFieldName.equals("CalFactor") ) {
            return 2;
        }
        if( strFieldName.equals("FactorName") ) {
            return 3;
        }
        if( strFieldName.equals("CalFactorType") ) {
            return 4;
        }
        if( strFieldName.equals("CalSql") ) {
            return 5;
        }
        if( strFieldName.equals("ChooseFlag") ) {
            return 6;
        }
        if( strFieldName.equals("FactorNoti") ) {
            return 7;
        }
        if( strFieldName.equals("FactorOrder") ) {
            return 8;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "DutyCode";
                break;
            case 2:
                strFieldName = "CalFactor";
                break;
            case 3:
                strFieldName = "FactorName";
                break;
            case 4:
                strFieldName = "CalFactorType";
                break;
            case 5:
                strFieldName = "CalSql";
                break;
            case 6:
                strFieldName = "ChooseFlag";
                break;
            case 7:
                strFieldName = "FactorNoti";
                break;
            case 8:
                strFieldName = "FactorOrder";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "CALFACTOR":
                return Schema.TYPE_STRING;
            case "FACTORNAME":
                return Schema.TYPE_STRING;
            case "CALFACTORTYPE":
                return Schema.TYPE_STRING;
            case "CALSQL":
                return Schema.TYPE_STRING;
            case "CHOOSEFLAG":
                return Schema.TYPE_STRING;
            case "FACTORNOTI":
                return Schema.TYPE_STRING;
            case "FACTORORDER":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("CalFactor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactor));
        }
        if (FCode.equalsIgnoreCase("FactorName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorName));
        }
        if (FCode.equalsIgnoreCase("CalFactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactorType));
        }
        if (FCode.equalsIgnoreCase("CalSql")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalSql));
        }
        if (FCode.equalsIgnoreCase("ChooseFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChooseFlag));
        }
        if (FCode.equalsIgnoreCase("FactorNoti")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorNoti));
        }
        if (FCode.equalsIgnoreCase("FactorOrder")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorOrder));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 2:
                strFieldValue = String.valueOf(CalFactor);
                break;
            case 3:
                strFieldValue = String.valueOf(FactorName);
                break;
            case 4:
                strFieldValue = String.valueOf(CalFactorType);
                break;
            case 5:
                strFieldValue = String.valueOf(CalSql);
                break;
            case 6:
                strFieldValue = String.valueOf(ChooseFlag);
                break;
            case 7:
                strFieldValue = String.valueOf(FactorNoti);
                break;
            case 8:
                strFieldValue = String.valueOf(FactorOrder);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("CalFactor")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFactor = FValue.trim();
            }
            else
                CalFactor = null;
        }
        if (FCode.equalsIgnoreCase("FactorName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorName = FValue.trim();
            }
            else
                FactorName = null;
        }
        if (FCode.equalsIgnoreCase("CalFactorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFactorType = FValue.trim();
            }
            else
                CalFactorType = null;
        }
        if (FCode.equalsIgnoreCase("CalSql")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalSql = FValue.trim();
            }
            else
                CalSql = null;
        }
        if (FCode.equalsIgnoreCase("ChooseFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChooseFlag = FValue.trim();
            }
            else
                ChooseFlag = null;
        }
        if (FCode.equalsIgnoreCase("FactorNoti")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorNoti = FValue.trim();
            }
            else
                FactorNoti = null;
        }
        if (FCode.equalsIgnoreCase("FactorOrder")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                FactorOrder = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LMRiskDutyFactorPojo [" +
            "RiskCode="+RiskCode +
            ", DutyCode="+DutyCode +
            ", CalFactor="+CalFactor +
            ", FactorName="+FactorName +
            ", CalFactorType="+CalFactorType +
            ", CalSql="+CalSql +
            ", ChooseFlag="+ChooseFlag +
            ", FactorNoti="+FactorNoti +
            ", FactorOrder="+FactorOrder +"]";
    }
}
