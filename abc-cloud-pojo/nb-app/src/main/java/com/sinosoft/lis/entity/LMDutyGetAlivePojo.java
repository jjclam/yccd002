/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyGetAlivePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyGetAlivePojo implements Pojo,Serializable {
    // @Field
    /** 给付代码 */
    @RedisIndexHKey
    @RedisPrimaryHKey
    private String GetDutyCode; 
    /** 给付名称 */
    private String GetDutyName; 
    /** 给付责任类型 */
    @RedisPrimaryHKey
    private String GetDutyKind; 
    /** 给付间隔 */
    private int GetIntv; 
    /** 默认值 */
    private double DefaultVal; 
    /** 算法 */
    private String CalCode; 
    /** 反算算法 */
    private String CnterCalCode; 
    /** 其他算法 */
    private String OthCalCode; 
    /** 起领期间 */
    private int GetStartPeriod; 
    /** 起领期间单位 */
    private String GetStartUnit; 
    /** 起领日期计算参照 */
    private String StartDateCalRef; 
    /** 起领日期计算方式 */
    private String StartDateCalMode; 
    /** 起领期间上限 */
    private int MinGetStartPeriod; 
    /** 止领期间 */
    private int GetEndPeriod; 
    /** 止领期间单位 */
    private String GetEndUnit; 
    /** 止领日期计算参照 */
    private String EndDateCalRef; 
    /** 止领日期计算方式 */
    private String EndDateCalMode; 
    /** 止领期间下限 */
    private int MaxGetEndPeriod; 
    /** 递增标记 */
    private String AddFlag; 
    /** 递增间隔 */
    private int AddIntv; 
    /** 递增开始期间 */
    private int AddStartPeriod; 
    /** 递增开始期间单位 */
    private String AddStartUnit; 
    /** 递增终止期间 */
    private int AddEndPeriod; 
    /** 递增终止期间单位 */
    private String AddEndUnit; 
    /** 递增类型 */
    private String AddType; 
    /** 递增值 */
    private double AddValue; 
    /** 给付最大次数 */
    private int MaxGetCount; 
    /** 给付后动作 */
    private String AfterGet; 
    /** 领取动作类型 */
    private String GetActionType; 
    /** 催付标记 */
    private String UrgeGetFlag; 
    /** 现金领取标记 */
    private String DiscntFlag; 
    /** 领取条件 */
    private String GetCond; 
    /** 给付最大次数类型 */
    private String MaxGetCountType; 
    /** 领取时是否需要重新计算 */
    private String NeedReCompute; 


    public static final int FIELDNUM = 34;    // 数据库表的字段个数
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyName() {
        return GetDutyName;
    }
    public void setGetDutyName(String aGetDutyName) {
        GetDutyName = aGetDutyName;
    }
    public String getGetDutyKind() {
        return GetDutyKind;
    }
    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }
    public int getGetIntv() {
        return GetIntv;
    }
    public void setGetIntv(int aGetIntv) {
        GetIntv = aGetIntv;
    }
    public void setGetIntv(String aGetIntv) {
        if (aGetIntv != null && !aGetIntv.equals("")) {
            Integer tInteger = new Integer(aGetIntv);
            int i = tInteger.intValue();
            GetIntv = i;
        }
    }

    public double getDefaultVal() {
        return DefaultVal;
    }
    public void setDefaultVal(double aDefaultVal) {
        DefaultVal = aDefaultVal;
    }
    public void setDefaultVal(String aDefaultVal) {
        if (aDefaultVal != null && !aDefaultVal.equals("")) {
            Double tDouble = new Double(aDefaultVal);
            double d = tDouble.doubleValue();
            DefaultVal = d;
        }
    }

    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getCnterCalCode() {
        return CnterCalCode;
    }
    public void setCnterCalCode(String aCnterCalCode) {
        CnterCalCode = aCnterCalCode;
    }
    public String getOthCalCode() {
        return OthCalCode;
    }
    public void setOthCalCode(String aOthCalCode) {
        OthCalCode = aOthCalCode;
    }
    public int getGetStartPeriod() {
        return GetStartPeriod;
    }
    public void setGetStartPeriod(int aGetStartPeriod) {
        GetStartPeriod = aGetStartPeriod;
    }
    public void setGetStartPeriod(String aGetStartPeriod) {
        if (aGetStartPeriod != null && !aGetStartPeriod.equals("")) {
            Integer tInteger = new Integer(aGetStartPeriod);
            int i = tInteger.intValue();
            GetStartPeriod = i;
        }
    }

    public String getGetStartUnit() {
        return GetStartUnit;
    }
    public void setGetStartUnit(String aGetStartUnit) {
        GetStartUnit = aGetStartUnit;
    }
    public String getStartDateCalRef() {
        return StartDateCalRef;
    }
    public void setStartDateCalRef(String aStartDateCalRef) {
        StartDateCalRef = aStartDateCalRef;
    }
    public String getStartDateCalMode() {
        return StartDateCalMode;
    }
    public void setStartDateCalMode(String aStartDateCalMode) {
        StartDateCalMode = aStartDateCalMode;
    }
    public int getMinGetStartPeriod() {
        return MinGetStartPeriod;
    }
    public void setMinGetStartPeriod(int aMinGetStartPeriod) {
        MinGetStartPeriod = aMinGetStartPeriod;
    }
    public void setMinGetStartPeriod(String aMinGetStartPeriod) {
        if (aMinGetStartPeriod != null && !aMinGetStartPeriod.equals("")) {
            Integer tInteger = new Integer(aMinGetStartPeriod);
            int i = tInteger.intValue();
            MinGetStartPeriod = i;
        }
    }

    public int getGetEndPeriod() {
        return GetEndPeriod;
    }
    public void setGetEndPeriod(int aGetEndPeriod) {
        GetEndPeriod = aGetEndPeriod;
    }
    public void setGetEndPeriod(String aGetEndPeriod) {
        if (aGetEndPeriod != null && !aGetEndPeriod.equals("")) {
            Integer tInteger = new Integer(aGetEndPeriod);
            int i = tInteger.intValue();
            GetEndPeriod = i;
        }
    }

    public String getGetEndUnit() {
        return GetEndUnit;
    }
    public void setGetEndUnit(String aGetEndUnit) {
        GetEndUnit = aGetEndUnit;
    }
    public String getEndDateCalRef() {
        return EndDateCalRef;
    }
    public void setEndDateCalRef(String aEndDateCalRef) {
        EndDateCalRef = aEndDateCalRef;
    }
    public String getEndDateCalMode() {
        return EndDateCalMode;
    }
    public void setEndDateCalMode(String aEndDateCalMode) {
        EndDateCalMode = aEndDateCalMode;
    }
    public int getMaxGetEndPeriod() {
        return MaxGetEndPeriod;
    }
    public void setMaxGetEndPeriod(int aMaxGetEndPeriod) {
        MaxGetEndPeriod = aMaxGetEndPeriod;
    }
    public void setMaxGetEndPeriod(String aMaxGetEndPeriod) {
        if (aMaxGetEndPeriod != null && !aMaxGetEndPeriod.equals("")) {
            Integer tInteger = new Integer(aMaxGetEndPeriod);
            int i = tInteger.intValue();
            MaxGetEndPeriod = i;
        }
    }

    public String getAddFlag() {
        return AddFlag;
    }
    public void setAddFlag(String aAddFlag) {
        AddFlag = aAddFlag;
    }
    public int getAddIntv() {
        return AddIntv;
    }
    public void setAddIntv(int aAddIntv) {
        AddIntv = aAddIntv;
    }
    public void setAddIntv(String aAddIntv) {
        if (aAddIntv != null && !aAddIntv.equals("")) {
            Integer tInteger = new Integer(aAddIntv);
            int i = tInteger.intValue();
            AddIntv = i;
        }
    }

    public int getAddStartPeriod() {
        return AddStartPeriod;
    }
    public void setAddStartPeriod(int aAddStartPeriod) {
        AddStartPeriod = aAddStartPeriod;
    }
    public void setAddStartPeriod(String aAddStartPeriod) {
        if (aAddStartPeriod != null && !aAddStartPeriod.equals("")) {
            Integer tInteger = new Integer(aAddStartPeriod);
            int i = tInteger.intValue();
            AddStartPeriod = i;
        }
    }

    public String getAddStartUnit() {
        return AddStartUnit;
    }
    public void setAddStartUnit(String aAddStartUnit) {
        AddStartUnit = aAddStartUnit;
    }
    public int getAddEndPeriod() {
        return AddEndPeriod;
    }
    public void setAddEndPeriod(int aAddEndPeriod) {
        AddEndPeriod = aAddEndPeriod;
    }
    public void setAddEndPeriod(String aAddEndPeriod) {
        if (aAddEndPeriod != null && !aAddEndPeriod.equals("")) {
            Integer tInteger = new Integer(aAddEndPeriod);
            int i = tInteger.intValue();
            AddEndPeriod = i;
        }
    }

    public String getAddEndUnit() {
        return AddEndUnit;
    }
    public void setAddEndUnit(String aAddEndUnit) {
        AddEndUnit = aAddEndUnit;
    }
    public String getAddType() {
        return AddType;
    }
    public void setAddType(String aAddType) {
        AddType = aAddType;
    }
    public double getAddValue() {
        return AddValue;
    }
    public void setAddValue(double aAddValue) {
        AddValue = aAddValue;
    }
    public void setAddValue(String aAddValue) {
        if (aAddValue != null && !aAddValue.equals("")) {
            Double tDouble = new Double(aAddValue);
            double d = tDouble.doubleValue();
            AddValue = d;
        }
    }

    public int getMaxGetCount() {
        return MaxGetCount;
    }
    public void setMaxGetCount(int aMaxGetCount) {
        MaxGetCount = aMaxGetCount;
    }
    public void setMaxGetCount(String aMaxGetCount) {
        if (aMaxGetCount != null && !aMaxGetCount.equals("")) {
            Integer tInteger = new Integer(aMaxGetCount);
            int i = tInteger.intValue();
            MaxGetCount = i;
        }
    }

    public String getAfterGet() {
        return AfterGet;
    }
    public void setAfterGet(String aAfterGet) {
        AfterGet = aAfterGet;
    }
    public String getGetActionType() {
        return GetActionType;
    }
    public void setGetActionType(String aGetActionType) {
        GetActionType = aGetActionType;
    }
    public String getUrgeGetFlag() {
        return UrgeGetFlag;
    }
    public void setUrgeGetFlag(String aUrgeGetFlag) {
        UrgeGetFlag = aUrgeGetFlag;
    }
    public String getDiscntFlag() {
        return DiscntFlag;
    }
    public void setDiscntFlag(String aDiscntFlag) {
        DiscntFlag = aDiscntFlag;
    }
    public String getGetCond() {
        return GetCond;
    }
    public void setGetCond(String aGetCond) {
        GetCond = aGetCond;
    }
    public String getMaxGetCountType() {
        return MaxGetCountType;
    }
    public void setMaxGetCountType(String aMaxGetCountType) {
        MaxGetCountType = aMaxGetCountType;
    }
    public String getNeedReCompute() {
        return NeedReCompute;
    }
    public void setNeedReCompute(String aNeedReCompute) {
        NeedReCompute = aNeedReCompute;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GetDutyCode") ) {
            return 0;
        }
        if( strFieldName.equals("GetDutyName") ) {
            return 1;
        }
        if( strFieldName.equals("GetDutyKind") ) {
            return 2;
        }
        if( strFieldName.equals("GetIntv") ) {
            return 3;
        }
        if( strFieldName.equals("DefaultVal") ) {
            return 4;
        }
        if( strFieldName.equals("CalCode") ) {
            return 5;
        }
        if( strFieldName.equals("CnterCalCode") ) {
            return 6;
        }
        if( strFieldName.equals("OthCalCode") ) {
            return 7;
        }
        if( strFieldName.equals("GetStartPeriod") ) {
            return 8;
        }
        if( strFieldName.equals("GetStartUnit") ) {
            return 9;
        }
        if( strFieldName.equals("StartDateCalRef") ) {
            return 10;
        }
        if( strFieldName.equals("StartDateCalMode") ) {
            return 11;
        }
        if( strFieldName.equals("MinGetStartPeriod") ) {
            return 12;
        }
        if( strFieldName.equals("GetEndPeriod") ) {
            return 13;
        }
        if( strFieldName.equals("GetEndUnit") ) {
            return 14;
        }
        if( strFieldName.equals("EndDateCalRef") ) {
            return 15;
        }
        if( strFieldName.equals("EndDateCalMode") ) {
            return 16;
        }
        if( strFieldName.equals("MaxGetEndPeriod") ) {
            return 17;
        }
        if( strFieldName.equals("AddFlag") ) {
            return 18;
        }
        if( strFieldName.equals("AddIntv") ) {
            return 19;
        }
        if( strFieldName.equals("AddStartPeriod") ) {
            return 20;
        }
        if( strFieldName.equals("AddStartUnit") ) {
            return 21;
        }
        if( strFieldName.equals("AddEndPeriod") ) {
            return 22;
        }
        if( strFieldName.equals("AddEndUnit") ) {
            return 23;
        }
        if( strFieldName.equals("AddType") ) {
            return 24;
        }
        if( strFieldName.equals("AddValue") ) {
            return 25;
        }
        if( strFieldName.equals("MaxGetCount") ) {
            return 26;
        }
        if( strFieldName.equals("AfterGet") ) {
            return 27;
        }
        if( strFieldName.equals("GetActionType") ) {
            return 28;
        }
        if( strFieldName.equals("UrgeGetFlag") ) {
            return 29;
        }
        if( strFieldName.equals("DiscntFlag") ) {
            return 30;
        }
        if( strFieldName.equals("GetCond") ) {
            return 31;
        }
        if( strFieldName.equals("MaxGetCountType") ) {
            return 32;
        }
        if( strFieldName.equals("NeedReCompute") ) {
            return 33;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GetDutyCode";
                break;
            case 1:
                strFieldName = "GetDutyName";
                break;
            case 2:
                strFieldName = "GetDutyKind";
                break;
            case 3:
                strFieldName = "GetIntv";
                break;
            case 4:
                strFieldName = "DefaultVal";
                break;
            case 5:
                strFieldName = "CalCode";
                break;
            case 6:
                strFieldName = "CnterCalCode";
                break;
            case 7:
                strFieldName = "OthCalCode";
                break;
            case 8:
                strFieldName = "GetStartPeriod";
                break;
            case 9:
                strFieldName = "GetStartUnit";
                break;
            case 10:
                strFieldName = "StartDateCalRef";
                break;
            case 11:
                strFieldName = "StartDateCalMode";
                break;
            case 12:
                strFieldName = "MinGetStartPeriod";
                break;
            case 13:
                strFieldName = "GetEndPeriod";
                break;
            case 14:
                strFieldName = "GetEndUnit";
                break;
            case 15:
                strFieldName = "EndDateCalRef";
                break;
            case 16:
                strFieldName = "EndDateCalMode";
                break;
            case 17:
                strFieldName = "MaxGetEndPeriod";
                break;
            case 18:
                strFieldName = "AddFlag";
                break;
            case 19:
                strFieldName = "AddIntv";
                break;
            case 20:
                strFieldName = "AddStartPeriod";
                break;
            case 21:
                strFieldName = "AddStartUnit";
                break;
            case 22:
                strFieldName = "AddEndPeriod";
                break;
            case 23:
                strFieldName = "AddEndUnit";
                break;
            case 24:
                strFieldName = "AddType";
                break;
            case 25:
                strFieldName = "AddValue";
                break;
            case 26:
                strFieldName = "MaxGetCount";
                break;
            case 27:
                strFieldName = "AfterGet";
                break;
            case 28:
                strFieldName = "GetActionType";
                break;
            case 29:
                strFieldName = "UrgeGetFlag";
                break;
            case 30:
                strFieldName = "DiscntFlag";
                break;
            case 31:
                strFieldName = "GetCond";
                break;
            case 32:
                strFieldName = "MaxGetCountType";
                break;
            case 33:
                strFieldName = "NeedReCompute";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYNAME":
                return Schema.TYPE_STRING;
            case "GETDUTYKIND":
                return Schema.TYPE_STRING;
            case "GETINTV":
                return Schema.TYPE_INT;
            case "DEFAULTVAL":
                return Schema.TYPE_DOUBLE;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "CNTERCALCODE":
                return Schema.TYPE_STRING;
            case "OTHCALCODE":
                return Schema.TYPE_STRING;
            case "GETSTARTPERIOD":
                return Schema.TYPE_INT;
            case "GETSTARTUNIT":
                return Schema.TYPE_STRING;
            case "STARTDATECALREF":
                return Schema.TYPE_STRING;
            case "STARTDATECALMODE":
                return Schema.TYPE_STRING;
            case "MINGETSTARTPERIOD":
                return Schema.TYPE_INT;
            case "GETENDPERIOD":
                return Schema.TYPE_INT;
            case "GETENDUNIT":
                return Schema.TYPE_STRING;
            case "ENDDATECALREF":
                return Schema.TYPE_STRING;
            case "ENDDATECALMODE":
                return Schema.TYPE_STRING;
            case "MAXGETENDPERIOD":
                return Schema.TYPE_INT;
            case "ADDFLAG":
                return Schema.TYPE_STRING;
            case "ADDINTV":
                return Schema.TYPE_INT;
            case "ADDSTARTPERIOD":
                return Schema.TYPE_INT;
            case "ADDSTARTUNIT":
                return Schema.TYPE_STRING;
            case "ADDENDPERIOD":
                return Schema.TYPE_INT;
            case "ADDENDUNIT":
                return Schema.TYPE_STRING;
            case "ADDTYPE":
                return Schema.TYPE_STRING;
            case "ADDVALUE":
                return Schema.TYPE_DOUBLE;
            case "MAXGETCOUNT":
                return Schema.TYPE_INT;
            case "AFTERGET":
                return Schema.TYPE_STRING;
            case "GETACTIONTYPE":
                return Schema.TYPE_STRING;
            case "URGEGETFLAG":
                return Schema.TYPE_STRING;
            case "DISCNTFLAG":
                return Schema.TYPE_STRING;
            case "GETCOND":
                return Schema.TYPE_STRING;
            case "MAXGETCOUNTTYPE":
                return Schema.TYPE_STRING;
            case "NEEDRECOMPUTE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_INT;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_INT;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_INT;
            case 20:
                return Schema.TYPE_INT;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_INT;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_DOUBLE;
            case 26:
                return Schema.TYPE_INT;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetIntv));
        }
        if (FCode.equalsIgnoreCase("DefaultVal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultVal));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("CnterCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CnterCalCode));
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthCalCode));
        }
        if (FCode.equalsIgnoreCase("GetStartPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartPeriod));
        }
        if (FCode.equalsIgnoreCase("GetStartUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartUnit));
        }
        if (FCode.equalsIgnoreCase("StartDateCalRef")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDateCalRef));
        }
        if (FCode.equalsIgnoreCase("StartDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDateCalMode));
        }
        if (FCode.equalsIgnoreCase("MinGetStartPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinGetStartPeriod));
        }
        if (FCode.equalsIgnoreCase("GetEndPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetEndPeriod));
        }
        if (FCode.equalsIgnoreCase("GetEndUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetEndUnit));
        }
        if (FCode.equalsIgnoreCase("EndDateCalRef")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDateCalRef));
        }
        if (FCode.equalsIgnoreCase("EndDateCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDateCalMode));
        }
        if (FCode.equalsIgnoreCase("MaxGetEndPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxGetEndPeriod));
        }
        if (FCode.equalsIgnoreCase("AddFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddFlag));
        }
        if (FCode.equalsIgnoreCase("AddIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddIntv));
        }
        if (FCode.equalsIgnoreCase("AddStartPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddStartPeriod));
        }
        if (FCode.equalsIgnoreCase("AddStartUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddStartUnit));
        }
        if (FCode.equalsIgnoreCase("AddEndPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddEndPeriod));
        }
        if (FCode.equalsIgnoreCase("AddEndUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddEndUnit));
        }
        if (FCode.equalsIgnoreCase("AddType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddType));
        }
        if (FCode.equalsIgnoreCase("AddValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddValue));
        }
        if (FCode.equalsIgnoreCase("MaxGetCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxGetCount));
        }
        if (FCode.equalsIgnoreCase("AfterGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AfterGet));
        }
        if (FCode.equalsIgnoreCase("GetActionType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetActionType));
        }
        if (FCode.equalsIgnoreCase("UrgeGetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UrgeGetFlag));
        }
        if (FCode.equalsIgnoreCase("DiscntFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiscntFlag));
        }
        if (FCode.equalsIgnoreCase("GetCond")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetCond));
        }
        if (FCode.equalsIgnoreCase("MaxGetCountType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxGetCountType));
        }
        if (FCode.equalsIgnoreCase("NeedReCompute")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedReCompute));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GetDutyCode);
                break;
            case 1:
                strFieldValue = String.valueOf(GetDutyName);
                break;
            case 2:
                strFieldValue = String.valueOf(GetDutyKind);
                break;
            case 3:
                strFieldValue = String.valueOf(GetIntv);
                break;
            case 4:
                strFieldValue = String.valueOf(DefaultVal);
                break;
            case 5:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 6:
                strFieldValue = String.valueOf(CnterCalCode);
                break;
            case 7:
                strFieldValue = String.valueOf(OthCalCode);
                break;
            case 8:
                strFieldValue = String.valueOf(GetStartPeriod);
                break;
            case 9:
                strFieldValue = String.valueOf(GetStartUnit);
                break;
            case 10:
                strFieldValue = String.valueOf(StartDateCalRef);
                break;
            case 11:
                strFieldValue = String.valueOf(StartDateCalMode);
                break;
            case 12:
                strFieldValue = String.valueOf(MinGetStartPeriod);
                break;
            case 13:
                strFieldValue = String.valueOf(GetEndPeriod);
                break;
            case 14:
                strFieldValue = String.valueOf(GetEndUnit);
                break;
            case 15:
                strFieldValue = String.valueOf(EndDateCalRef);
                break;
            case 16:
                strFieldValue = String.valueOf(EndDateCalMode);
                break;
            case 17:
                strFieldValue = String.valueOf(MaxGetEndPeriod);
                break;
            case 18:
                strFieldValue = String.valueOf(AddFlag);
                break;
            case 19:
                strFieldValue = String.valueOf(AddIntv);
                break;
            case 20:
                strFieldValue = String.valueOf(AddStartPeriod);
                break;
            case 21:
                strFieldValue = String.valueOf(AddStartUnit);
                break;
            case 22:
                strFieldValue = String.valueOf(AddEndPeriod);
                break;
            case 23:
                strFieldValue = String.valueOf(AddEndUnit);
                break;
            case 24:
                strFieldValue = String.valueOf(AddType);
                break;
            case 25:
                strFieldValue = String.valueOf(AddValue);
                break;
            case 26:
                strFieldValue = String.valueOf(MaxGetCount);
                break;
            case 27:
                strFieldValue = String.valueOf(AfterGet);
                break;
            case 28:
                strFieldValue = String.valueOf(GetActionType);
                break;
            case 29:
                strFieldValue = String.valueOf(UrgeGetFlag);
                break;
            case 30:
                strFieldValue = String.valueOf(DiscntFlag);
                break;
            case 31:
                strFieldValue = String.valueOf(GetCond);
                break;
            case 32:
                strFieldValue = String.valueOf(MaxGetCountType);
                break;
            case 33:
                strFieldValue = String.valueOf(NeedReCompute);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
                GetDutyName = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
                GetDutyKind = null;
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("DefaultVal")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultVal = d;
            }
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("CnterCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CnterCalCode = FValue.trim();
            }
            else
                CnterCalCode = null;
        }
        if (FCode.equalsIgnoreCase("OthCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthCalCode = FValue.trim();
            }
            else
                OthCalCode = null;
        }
        if (FCode.equalsIgnoreCase("GetStartPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetStartPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetStartUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetStartUnit = FValue.trim();
            }
            else
                GetStartUnit = null;
        }
        if (FCode.equalsIgnoreCase("StartDateCalRef")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDateCalRef = FValue.trim();
            }
            else
                StartDateCalRef = null;
        }
        if (FCode.equalsIgnoreCase("StartDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDateCalMode = FValue.trim();
            }
            else
                StartDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("MinGetStartPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MinGetStartPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetEndPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetEndPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetEndUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetEndUnit = FValue.trim();
            }
            else
                GetEndUnit = null;
        }
        if (FCode.equalsIgnoreCase("EndDateCalRef")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDateCalRef = FValue.trim();
            }
            else
                EndDateCalRef = null;
        }
        if (FCode.equalsIgnoreCase("EndDateCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDateCalMode = FValue.trim();
            }
            else
                EndDateCalMode = null;
        }
        if (FCode.equalsIgnoreCase("MaxGetEndPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxGetEndPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("AddFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddFlag = FValue.trim();
            }
            else
                AddFlag = null;
        }
        if (FCode.equalsIgnoreCase("AddIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AddIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("AddStartPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AddStartPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("AddStartUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddStartUnit = FValue.trim();
            }
            else
                AddStartUnit = null;
        }
        if (FCode.equalsIgnoreCase("AddEndPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AddEndPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("AddEndUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddEndUnit = FValue.trim();
            }
            else
                AddEndUnit = null;
        }
        if (FCode.equalsIgnoreCase("AddType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddType = FValue.trim();
            }
            else
                AddType = null;
        }
        if (FCode.equalsIgnoreCase("AddValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AddValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxGetCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxGetCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("AfterGet")) {
            if( FValue != null && !FValue.equals(""))
            {
                AfterGet = FValue.trim();
            }
            else
                AfterGet = null;
        }
        if (FCode.equalsIgnoreCase("GetActionType")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetActionType = FValue.trim();
            }
            else
                GetActionType = null;
        }
        if (FCode.equalsIgnoreCase("UrgeGetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UrgeGetFlag = FValue.trim();
            }
            else
                UrgeGetFlag = null;
        }
        if (FCode.equalsIgnoreCase("DiscntFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DiscntFlag = FValue.trim();
            }
            else
                DiscntFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetCond")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetCond = FValue.trim();
            }
            else
                GetCond = null;
        }
        if (FCode.equalsIgnoreCase("MaxGetCountType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MaxGetCountType = FValue.trim();
            }
            else
                MaxGetCountType = null;
        }
        if (FCode.equalsIgnoreCase("NeedReCompute")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedReCompute = FValue.trim();
            }
            else
                NeedReCompute = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyGetAlivePojo [" +
            "GetDutyCode="+GetDutyCode +
            ", GetDutyName="+GetDutyName +
            ", GetDutyKind="+GetDutyKind +
            ", GetIntv="+GetIntv +
            ", DefaultVal="+DefaultVal +
            ", CalCode="+CalCode +
            ", CnterCalCode="+CnterCalCode +
            ", OthCalCode="+OthCalCode +
            ", GetStartPeriod="+GetStartPeriod +
            ", GetStartUnit="+GetStartUnit +
            ", StartDateCalRef="+StartDateCalRef +
            ", StartDateCalMode="+StartDateCalMode +
            ", MinGetStartPeriod="+MinGetStartPeriod +
            ", GetEndPeriod="+GetEndPeriod +
            ", GetEndUnit="+GetEndUnit +
            ", EndDateCalRef="+EndDateCalRef +
            ", EndDateCalMode="+EndDateCalMode +
            ", MaxGetEndPeriod="+MaxGetEndPeriod +
            ", AddFlag="+AddFlag +
            ", AddIntv="+AddIntv +
            ", AddStartPeriod="+AddStartPeriod +
            ", AddStartUnit="+AddStartUnit +
            ", AddEndPeriod="+AddEndPeriod +
            ", AddEndUnit="+AddEndUnit +
            ", AddType="+AddType +
            ", AddValue="+AddValue +
            ", MaxGetCount="+MaxGetCount +
            ", AfterGet="+AfterGet +
            ", GetActionType="+GetActionType +
            ", UrgeGetFlag="+UrgeGetFlag +
            ", DiscntFlag="+DiscntFlag +
            ", GetCond="+GetCond +
            ", MaxGetCountType="+MaxGetCountType +
            ", NeedReCompute="+NeedReCompute +"]";
    }
}
