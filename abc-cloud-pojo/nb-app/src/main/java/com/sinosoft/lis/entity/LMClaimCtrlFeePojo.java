/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMClaimCtrlFeePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMClaimCtrlFeePojo implements Pojo,Serializable {
    // @Field
    /** 理赔控制编号 */
    private String ClaimCtrlCode; 
    /** 起始费用 */
    private double ClmFeeMIN; 
    /** 起始费用单位 */
    private String ClmFeeMINFlag; 
    /** 费用期间间隔 */
    private double ClmFeeInterval; 
    /** 费用期间间隔单位 */
    private String ClmFeeFlag; 
    /** 赔付金额计算sql */
    private String CalCode2; 
    /** 赔付金额类型 */
    private String CalResultType; 
    /** 赔付金额默认值 */
    private double DefaultValue; 
    /** 赔付金额计算方式 */
    private String CalCtrlFlag; 


    public static final int FIELDNUM = 9;    // 数据库表的字段个数
    public String getClaimCtrlCode() {
        return ClaimCtrlCode;
    }
    public void setClaimCtrlCode(String aClaimCtrlCode) {
        ClaimCtrlCode = aClaimCtrlCode;
    }
    public double getClmFeeMIN() {
        return ClmFeeMIN;
    }
    public void setClmFeeMIN(double aClmFeeMIN) {
        ClmFeeMIN = aClmFeeMIN;
    }
    public void setClmFeeMIN(String aClmFeeMIN) {
        if (aClmFeeMIN != null && !aClmFeeMIN.equals("")) {
            Double tDouble = new Double(aClmFeeMIN);
            double d = tDouble.doubleValue();
            ClmFeeMIN = d;
        }
    }

    public String getClmFeeMINFlag() {
        return ClmFeeMINFlag;
    }
    public void setClmFeeMINFlag(String aClmFeeMINFlag) {
        ClmFeeMINFlag = aClmFeeMINFlag;
    }
    public double getClmFeeInterval() {
        return ClmFeeInterval;
    }
    public void setClmFeeInterval(double aClmFeeInterval) {
        ClmFeeInterval = aClmFeeInterval;
    }
    public void setClmFeeInterval(String aClmFeeInterval) {
        if (aClmFeeInterval != null && !aClmFeeInterval.equals("")) {
            Double tDouble = new Double(aClmFeeInterval);
            double d = tDouble.doubleValue();
            ClmFeeInterval = d;
        }
    }

    public String getClmFeeFlag() {
        return ClmFeeFlag;
    }
    public void setClmFeeFlag(String aClmFeeFlag) {
        ClmFeeFlag = aClmFeeFlag;
    }
    public String getCalCode2() {
        return CalCode2;
    }
    public void setCalCode2(String aCalCode2) {
        CalCode2 = aCalCode2;
    }
    public String getCalResultType() {
        return CalResultType;
    }
    public void setCalResultType(String aCalResultType) {
        CalResultType = aCalResultType;
    }
    public double getDefaultValue() {
        return DefaultValue;
    }
    public void setDefaultValue(double aDefaultValue) {
        DefaultValue = aDefaultValue;
    }
    public void setDefaultValue(String aDefaultValue) {
        if (aDefaultValue != null && !aDefaultValue.equals("")) {
            Double tDouble = new Double(aDefaultValue);
            double d = tDouble.doubleValue();
            DefaultValue = d;
        }
    }

    public String getCalCtrlFlag() {
        return CalCtrlFlag;
    }
    public void setCalCtrlFlag(String aCalCtrlFlag) {
        CalCtrlFlag = aCalCtrlFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ClaimCtrlCode") ) {
            return 0;
        }
        if( strFieldName.equals("ClmFeeMIN") ) {
            return 1;
        }
        if( strFieldName.equals("ClmFeeMINFlag") ) {
            return 2;
        }
        if( strFieldName.equals("ClmFeeInterval") ) {
            return 3;
        }
        if( strFieldName.equals("ClmFeeFlag") ) {
            return 4;
        }
        if( strFieldName.equals("CalCode2") ) {
            return 5;
        }
        if( strFieldName.equals("CalResultType") ) {
            return 6;
        }
        if( strFieldName.equals("DefaultValue") ) {
            return 7;
        }
        if( strFieldName.equals("CalCtrlFlag") ) {
            return 8;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ClaimCtrlCode";
                break;
            case 1:
                strFieldName = "ClmFeeMIN";
                break;
            case 2:
                strFieldName = "ClmFeeMINFlag";
                break;
            case 3:
                strFieldName = "ClmFeeInterval";
                break;
            case 4:
                strFieldName = "ClmFeeFlag";
                break;
            case 5:
                strFieldName = "CalCode2";
                break;
            case 6:
                strFieldName = "CalResultType";
                break;
            case 7:
                strFieldName = "DefaultValue";
                break;
            case 8:
                strFieldName = "CalCtrlFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CLAIMCTRLCODE":
                return Schema.TYPE_STRING;
            case "CLMFEEMIN":
                return Schema.TYPE_DOUBLE;
            case "CLMFEEMINFLAG":
                return Schema.TYPE_STRING;
            case "CLMFEEINTERVAL":
                return Schema.TYPE_DOUBLE;
            case "CLMFEEFLAG":
                return Schema.TYPE_STRING;
            case "CALCODE2":
                return Schema.TYPE_STRING;
            case "CALRESULTTYPE":
                return Schema.TYPE_STRING;
            case "DEFAULTVALUE":
                return Schema.TYPE_DOUBLE;
            case "CALCTRLFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_DOUBLE;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_DOUBLE;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ClaimCtrlCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimCtrlCode));
        }
        if (FCode.equalsIgnoreCase("ClmFeeMIN")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeMIN));
        }
        if (FCode.equalsIgnoreCase("ClmFeeMINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeMINFlag));
        }
        if (FCode.equalsIgnoreCase("ClmFeeInterval")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeInterval));
        }
        if (FCode.equalsIgnoreCase("ClmFeeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeFlag));
        }
        if (FCode.equalsIgnoreCase("CalCode2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode2));
        }
        if (FCode.equalsIgnoreCase("CalResultType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalResultType));
        }
        if (FCode.equalsIgnoreCase("DefaultValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultValue));
        }
        if (FCode.equalsIgnoreCase("CalCtrlFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCtrlFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ClaimCtrlCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ClmFeeMIN);
                break;
            case 2:
                strFieldValue = String.valueOf(ClmFeeMINFlag);
                break;
            case 3:
                strFieldValue = String.valueOf(ClmFeeInterval);
                break;
            case 4:
                strFieldValue = String.valueOf(ClmFeeFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(CalCode2);
                break;
            case 6:
                strFieldValue = String.valueOf(CalResultType);
                break;
            case 7:
                strFieldValue = String.valueOf(DefaultValue);
                break;
            case 8:
                strFieldValue = String.valueOf(CalCtrlFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ClaimCtrlCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClaimCtrlCode = FValue.trim();
            }
            else
                ClaimCtrlCode = null;
        }
        if (FCode.equalsIgnoreCase("ClmFeeMIN")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ClmFeeMIN = d;
            }
        }
        if (FCode.equalsIgnoreCase("ClmFeeMINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmFeeMINFlag = FValue.trim();
            }
            else
                ClmFeeMINFlag = null;
        }
        if (FCode.equalsIgnoreCase("ClmFeeInterval")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ClmFeeInterval = d;
            }
        }
        if (FCode.equalsIgnoreCase("ClmFeeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmFeeFlag = FValue.trim();
            }
            else
                ClmFeeFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalCode2")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode2 = FValue.trim();
            }
            else
                CalCode2 = null;
        }
        if (FCode.equalsIgnoreCase("CalResultType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalResultType = FValue.trim();
            }
            else
                CalResultType = null;
        }
        if (FCode.equalsIgnoreCase("DefaultValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("CalCtrlFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCtrlFlag = FValue.trim();
            }
            else
                CalCtrlFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LMClaimCtrlFeePojo [" +
            "ClaimCtrlCode="+ClaimCtrlCode +
            ", ClmFeeMIN="+ClmFeeMIN +
            ", ClmFeeMINFlag="+ClmFeeMINFlag +
            ", ClmFeeInterval="+ClmFeeInterval +
            ", ClmFeeFlag="+ClmFeeFlag +
            ", CalCode2="+CalCode2 +
            ", CalResultType="+CalResultType +
            ", DefaultValue="+DefaultValue +
            ", CalCtrlFlag="+CalCtrlFlag +"]";
    }
}
