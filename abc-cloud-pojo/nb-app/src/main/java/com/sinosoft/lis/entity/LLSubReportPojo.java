/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LLSubReportPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LLSubReportPojo implements Pojo,Serializable {
    // @Field
    /** 分报案号 */
    private String SubRptNo; 
    /** 关联客户号码 */
    private String CustomerNo; 
    /** 关联客户的名称 */
    private String CustomerName; 
    /** 关联客户类型 */
    private String CustomerType; 
    /** 事件主题 */
    private String AccSubject; 
    /** 事故类型 */
    private String AccidentType; 
    /** 出险日期 */
    private String  AccDate;
    /** 终止日期 */
    private String  AccEndDate;
    /** 事故描述 */
    private String AccDesc; 
    /** 事故者状况 */
    private String CustSituation; 
    /** 事故地点 */
    private String AccPlace; 
    /** 医院代码 */
    private String HospitalCode; 
    /** 医院名称 */
    private String HospitalName; 
    /** 入院日期 */
    private String  InHospitalDate;
    /** 出院日期 */
    private String  OutHospitalDate;
    /** 备注 */
    private String Remark; 
    /** 重大事件标志 */
    private String SeriousGrade; 
    /** 调查报告标志 */
    private String SurveyFlag; 
    /** 操作员 */
    private String Operator; 
    /** 管理机构 */
    private String MngCom; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 首诊日 */
    private String  FirstDiaDate;
    /** 医院级别 */
    private String HosGrade; 
    /** 所在地 */
    private String LocalPlace; 
    /** 医院联系电话 */
    private String HosTel; 
    /** 死亡日期 */
    private String  DieDate;
    /** 事件原因 */
    private String AccCause; 
    /** Vip标志 */
    private String VIPFlag; 
    /** 出险细节 */
    private String AccidentDetail; 
    /** 治疗情况 */
    private String CureDesc; 
    /** 发起呈报标志 */
    private String SubmitFlag; 
    /** 提起慰问标志 */
    private String CondoleFlag; 
    /** 死亡标志 */
    private String DieFlag; 
    /** 出险结果1 */
    private String AccResult1; 
    /** 出险结果2 */
    private String AccResult2; 
    /** 案件号 */
    private String CaseNo; 
    /** 客户序号 */
    private int SeqNo; 


    public static final int FIELDNUM = 40;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSubRptNo() {
        return SubRptNo;
    }
    public void setSubRptNo(String aSubRptNo) {
        SubRptNo = aSubRptNo;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getCustomerName() {
        return CustomerName;
    }
    public void setCustomerName(String aCustomerName) {
        CustomerName = aCustomerName;
    }
    public String getCustomerType() {
        return CustomerType;
    }
    public void setCustomerType(String aCustomerType) {
        CustomerType = aCustomerType;
    }
    public String getAccSubject() {
        return AccSubject;
    }
    public void setAccSubject(String aAccSubject) {
        AccSubject = aAccSubject;
    }
    public String getAccidentType() {
        return AccidentType;
    }
    public void setAccidentType(String aAccidentType) {
        AccidentType = aAccidentType;
    }
    public String getAccDate() {
        return AccDate;
    }
    public void setAccDate(String aAccDate) {
        AccDate = aAccDate;
    }
    public String getAccEndDate() {
        return AccEndDate;
    }
    public void setAccEndDate(String aAccEndDate) {
        AccEndDate = aAccEndDate;
    }
    public String getAccDesc() {
        return AccDesc;
    }
    public void setAccDesc(String aAccDesc) {
        AccDesc = aAccDesc;
    }
    public String getCustSituation() {
        return CustSituation;
    }
    public void setCustSituation(String aCustSituation) {
        CustSituation = aCustSituation;
    }
    public String getAccPlace() {
        return AccPlace;
    }
    public void setAccPlace(String aAccPlace) {
        AccPlace = aAccPlace;
    }
    public String getHospitalCode() {
        return HospitalCode;
    }
    public void setHospitalCode(String aHospitalCode) {
        HospitalCode = aHospitalCode;
    }
    public String getHospitalName() {
        return HospitalName;
    }
    public void setHospitalName(String aHospitalName) {
        HospitalName = aHospitalName;
    }
    public String getInHospitalDate() {
        return InHospitalDate;
    }
    public void setInHospitalDate(String aInHospitalDate) {
        InHospitalDate = aInHospitalDate;
    }
    public String getOutHospitalDate() {
        return OutHospitalDate;
    }
    public void setOutHospitalDate(String aOutHospitalDate) {
        OutHospitalDate = aOutHospitalDate;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getSeriousGrade() {
        return SeriousGrade;
    }
    public void setSeriousGrade(String aSeriousGrade) {
        SeriousGrade = aSeriousGrade;
    }
    public String getSurveyFlag() {
        return SurveyFlag;
    }
    public void setSurveyFlag(String aSurveyFlag) {
        SurveyFlag = aSurveyFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMngCom() {
        return MngCom;
    }
    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFirstDiaDate() {
        return FirstDiaDate;
    }
    public void setFirstDiaDate(String aFirstDiaDate) {
        FirstDiaDate = aFirstDiaDate;
    }
    public String getHosGrade() {
        return HosGrade;
    }
    public void setHosGrade(String aHosGrade) {
        HosGrade = aHosGrade;
    }
    public String getLocalPlace() {
        return LocalPlace;
    }
    public void setLocalPlace(String aLocalPlace) {
        LocalPlace = aLocalPlace;
    }
    public String getHosTel() {
        return HosTel;
    }
    public void setHosTel(String aHosTel) {
        HosTel = aHosTel;
    }
    public String getDieDate() {
        return DieDate;
    }
    public void setDieDate(String aDieDate) {
        DieDate = aDieDate;
    }
    public String getAccCause() {
        return AccCause;
    }
    public void setAccCause(String aAccCause) {
        AccCause = aAccCause;
    }
    public String getVIPFlag() {
        return VIPFlag;
    }
    public void setVIPFlag(String aVIPFlag) {
        VIPFlag = aVIPFlag;
    }
    public String getAccidentDetail() {
        return AccidentDetail;
    }
    public void setAccidentDetail(String aAccidentDetail) {
        AccidentDetail = aAccidentDetail;
    }
    public String getCureDesc() {
        return CureDesc;
    }
    public void setCureDesc(String aCureDesc) {
        CureDesc = aCureDesc;
    }
    public String getSubmitFlag() {
        return SubmitFlag;
    }
    public void setSubmitFlag(String aSubmitFlag) {
        SubmitFlag = aSubmitFlag;
    }
    public String getCondoleFlag() {
        return CondoleFlag;
    }
    public void setCondoleFlag(String aCondoleFlag) {
        CondoleFlag = aCondoleFlag;
    }
    public String getDieFlag() {
        return DieFlag;
    }
    public void setDieFlag(String aDieFlag) {
        DieFlag = aDieFlag;
    }
    public String getAccResult1() {
        return AccResult1;
    }
    public void setAccResult1(String aAccResult1) {
        AccResult1 = aAccResult1;
    }
    public String getAccResult2() {
        return AccResult2;
    }
    public void setAccResult2(String aAccResult2) {
        AccResult2 = aAccResult2;
    }
    public String getCaseNo() {
        return CaseNo;
    }
    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }
    public int getSeqNo() {
        return SeqNo;
    }
    public void setSeqNo(int aSeqNo) {
        SeqNo = aSeqNo;
    }
    public void setSeqNo(String aSeqNo) {
        if (aSeqNo != null && !aSeqNo.equals("")) {
            Integer tInteger = new Integer(aSeqNo);
            int i = tInteger.intValue();
            SeqNo = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SubRptNo") ) {
            return 0;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 1;
        }
        if( strFieldName.equals("CustomerName") ) {
            return 2;
        }
        if( strFieldName.equals("CustomerType") ) {
            return 3;
        }
        if( strFieldName.equals("AccSubject") ) {
            return 4;
        }
        if( strFieldName.equals("AccidentType") ) {
            return 5;
        }
        if( strFieldName.equals("AccDate") ) {
            return 6;
        }
        if( strFieldName.equals("AccEndDate") ) {
            return 7;
        }
        if( strFieldName.equals("AccDesc") ) {
            return 8;
        }
        if( strFieldName.equals("CustSituation") ) {
            return 9;
        }
        if( strFieldName.equals("AccPlace") ) {
            return 10;
        }
        if( strFieldName.equals("HospitalCode") ) {
            return 11;
        }
        if( strFieldName.equals("HospitalName") ) {
            return 12;
        }
        if( strFieldName.equals("InHospitalDate") ) {
            return 13;
        }
        if( strFieldName.equals("OutHospitalDate") ) {
            return 14;
        }
        if( strFieldName.equals("Remark") ) {
            return 15;
        }
        if( strFieldName.equals("SeriousGrade") ) {
            return 16;
        }
        if( strFieldName.equals("SurveyFlag") ) {
            return 17;
        }
        if( strFieldName.equals("Operator") ) {
            return 18;
        }
        if( strFieldName.equals("MngCom") ) {
            return 19;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 20;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 21;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 23;
        }
        if( strFieldName.equals("FirstDiaDate") ) {
            return 24;
        }
        if( strFieldName.equals("HosGrade") ) {
            return 25;
        }
        if( strFieldName.equals("LocalPlace") ) {
            return 26;
        }
        if( strFieldName.equals("HosTel") ) {
            return 27;
        }
        if( strFieldName.equals("DieDate") ) {
            return 28;
        }
        if( strFieldName.equals("AccCause") ) {
            return 29;
        }
        if( strFieldName.equals("VIPFlag") ) {
            return 30;
        }
        if( strFieldName.equals("AccidentDetail") ) {
            return 31;
        }
        if( strFieldName.equals("CureDesc") ) {
            return 32;
        }
        if( strFieldName.equals("SubmitFlag") ) {
            return 33;
        }
        if( strFieldName.equals("CondoleFlag") ) {
            return 34;
        }
        if( strFieldName.equals("DieFlag") ) {
            return 35;
        }
        if( strFieldName.equals("AccResult1") ) {
            return 36;
        }
        if( strFieldName.equals("AccResult2") ) {
            return 37;
        }
        if( strFieldName.equals("CaseNo") ) {
            return 38;
        }
        if( strFieldName.equals("SeqNo") ) {
            return 39;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SubRptNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "CustomerName";
                break;
            case 3:
                strFieldName = "CustomerType";
                break;
            case 4:
                strFieldName = "AccSubject";
                break;
            case 5:
                strFieldName = "AccidentType";
                break;
            case 6:
                strFieldName = "AccDate";
                break;
            case 7:
                strFieldName = "AccEndDate";
                break;
            case 8:
                strFieldName = "AccDesc";
                break;
            case 9:
                strFieldName = "CustSituation";
                break;
            case 10:
                strFieldName = "AccPlace";
                break;
            case 11:
                strFieldName = "HospitalCode";
                break;
            case 12:
                strFieldName = "HospitalName";
                break;
            case 13:
                strFieldName = "InHospitalDate";
                break;
            case 14:
                strFieldName = "OutHospitalDate";
                break;
            case 15:
                strFieldName = "Remark";
                break;
            case 16:
                strFieldName = "SeriousGrade";
                break;
            case 17:
                strFieldName = "SurveyFlag";
                break;
            case 18:
                strFieldName = "Operator";
                break;
            case 19:
                strFieldName = "MngCom";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "FirstDiaDate";
                break;
            case 25:
                strFieldName = "HosGrade";
                break;
            case 26:
                strFieldName = "LocalPlace";
                break;
            case 27:
                strFieldName = "HosTel";
                break;
            case 28:
                strFieldName = "DieDate";
                break;
            case 29:
                strFieldName = "AccCause";
                break;
            case 30:
                strFieldName = "VIPFlag";
                break;
            case 31:
                strFieldName = "AccidentDetail";
                break;
            case 32:
                strFieldName = "CureDesc";
                break;
            case 33:
                strFieldName = "SubmitFlag";
                break;
            case 34:
                strFieldName = "CondoleFlag";
                break;
            case 35:
                strFieldName = "DieFlag";
                break;
            case 36:
                strFieldName = "AccResult1";
                break;
            case 37:
                strFieldName = "AccResult2";
                break;
            case 38:
                strFieldName = "CaseNo";
                break;
            case 39:
                strFieldName = "SeqNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SUBRPTNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNAME":
                return Schema.TYPE_STRING;
            case "CUSTOMERTYPE":
                return Schema.TYPE_STRING;
            case "ACCSUBJECT":
                return Schema.TYPE_STRING;
            case "ACCIDENTTYPE":
                return Schema.TYPE_STRING;
            case "ACCDATE":
                return Schema.TYPE_STRING;
            case "ACCENDDATE":
                return Schema.TYPE_STRING;
            case "ACCDESC":
                return Schema.TYPE_STRING;
            case "CUSTSITUATION":
                return Schema.TYPE_STRING;
            case "ACCPLACE":
                return Schema.TYPE_STRING;
            case "HOSPITALCODE":
                return Schema.TYPE_STRING;
            case "HOSPITALNAME":
                return Schema.TYPE_STRING;
            case "INHOSPITALDATE":
                return Schema.TYPE_STRING;
            case "OUTHOSPITALDATE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "SERIOUSGRADE":
                return Schema.TYPE_STRING;
            case "SURVEYFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MNGCOM":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FIRSTDIADATE":
                return Schema.TYPE_STRING;
            case "HOSGRADE":
                return Schema.TYPE_STRING;
            case "LOCALPLACE":
                return Schema.TYPE_STRING;
            case "HOSTEL":
                return Schema.TYPE_STRING;
            case "DIEDATE":
                return Schema.TYPE_STRING;
            case "ACCCAUSE":
                return Schema.TYPE_STRING;
            case "VIPFLAG":
                return Schema.TYPE_STRING;
            case "ACCIDENTDETAIL":
                return Schema.TYPE_STRING;
            case "CUREDESC":
                return Schema.TYPE_STRING;
            case "SUBMITFLAG":
                return Schema.TYPE_STRING;
            case "CONDOLEFLAG":
                return Schema.TYPE_STRING;
            case "DIEFLAG":
                return Schema.TYPE_STRING;
            case "ACCRESULT1":
                return Schema.TYPE_STRING;
            case "ACCRESULT2":
                return Schema.TYPE_STRING;
            case "CASENO":
                return Schema.TYPE_STRING;
            case "SEQNO":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SubRptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubRptNo));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("CustomerName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
        }
        if (FCode.equalsIgnoreCase("CustomerType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerType));
        }
        if (FCode.equalsIgnoreCase("AccSubject")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccSubject));
        }
        if (FCode.equalsIgnoreCase("AccidentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentType));
        }
        if (FCode.equalsIgnoreCase("AccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccDate));
        }
        if (FCode.equalsIgnoreCase("AccEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccEndDate));
        }
        if (FCode.equalsIgnoreCase("AccDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccDesc));
        }
        if (FCode.equalsIgnoreCase("CustSituation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustSituation));
        }
        if (FCode.equalsIgnoreCase("AccPlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccPlace));
        }
        if (FCode.equalsIgnoreCase("HospitalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
        }
        if (FCode.equalsIgnoreCase("HospitalName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
        }
        if (FCode.equalsIgnoreCase("InHospitalDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InHospitalDate));
        }
        if (FCode.equalsIgnoreCase("OutHospitalDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutHospitalDate));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("SeriousGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeriousGrade));
        }
        if (FCode.equalsIgnoreCase("SurveyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FirstDiaDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstDiaDate));
        }
        if (FCode.equalsIgnoreCase("HosGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HosGrade));
        }
        if (FCode.equalsIgnoreCase("LocalPlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LocalPlace));
        }
        if (FCode.equalsIgnoreCase("HosTel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HosTel));
        }
        if (FCode.equalsIgnoreCase("DieDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DieDate));
        }
        if (FCode.equalsIgnoreCase("AccCause")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCause));
        }
        if (FCode.equalsIgnoreCase("VIPFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIPFlag));
        }
        if (FCode.equalsIgnoreCase("AccidentDetail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentDetail));
        }
        if (FCode.equalsIgnoreCase("CureDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CureDesc));
        }
        if (FCode.equalsIgnoreCase("SubmitFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubmitFlag));
        }
        if (FCode.equalsIgnoreCase("CondoleFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CondoleFlag));
        }
        if (FCode.equalsIgnoreCase("DieFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DieFlag));
        }
        if (FCode.equalsIgnoreCase("AccResult1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccResult1));
        }
        if (FCode.equalsIgnoreCase("AccResult2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccResult2));
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equalsIgnoreCase("SeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeqNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SubRptNo);
                break;
            case 1:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 2:
                strFieldValue = String.valueOf(CustomerName);
                break;
            case 3:
                strFieldValue = String.valueOf(CustomerType);
                break;
            case 4:
                strFieldValue = String.valueOf(AccSubject);
                break;
            case 5:
                strFieldValue = String.valueOf(AccidentType);
                break;
            case 6:
                strFieldValue = String.valueOf(AccDate);
                break;
            case 7:
                strFieldValue = String.valueOf(AccEndDate);
                break;
            case 8:
                strFieldValue = String.valueOf(AccDesc);
                break;
            case 9:
                strFieldValue = String.valueOf(CustSituation);
                break;
            case 10:
                strFieldValue = String.valueOf(AccPlace);
                break;
            case 11:
                strFieldValue = String.valueOf(HospitalCode);
                break;
            case 12:
                strFieldValue = String.valueOf(HospitalName);
                break;
            case 13:
                strFieldValue = String.valueOf(InHospitalDate);
                break;
            case 14:
                strFieldValue = String.valueOf(OutHospitalDate);
                break;
            case 15:
                strFieldValue = String.valueOf(Remark);
                break;
            case 16:
                strFieldValue = String.valueOf(SeriousGrade);
                break;
            case 17:
                strFieldValue = String.valueOf(SurveyFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(Operator);
                break;
            case 19:
                strFieldValue = String.valueOf(MngCom);
                break;
            case 20:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 21:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 22:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 23:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 24:
                strFieldValue = String.valueOf(FirstDiaDate);
                break;
            case 25:
                strFieldValue = String.valueOf(HosGrade);
                break;
            case 26:
                strFieldValue = String.valueOf(LocalPlace);
                break;
            case 27:
                strFieldValue = String.valueOf(HosTel);
                break;
            case 28:
                strFieldValue = String.valueOf(DieDate);
                break;
            case 29:
                strFieldValue = String.valueOf(AccCause);
                break;
            case 30:
                strFieldValue = String.valueOf(VIPFlag);
                break;
            case 31:
                strFieldValue = String.valueOf(AccidentDetail);
                break;
            case 32:
                strFieldValue = String.valueOf(CureDesc);
                break;
            case 33:
                strFieldValue = String.valueOf(SubmitFlag);
                break;
            case 34:
                strFieldValue = String.valueOf(CondoleFlag);
                break;
            case 35:
                strFieldValue = String.valueOf(DieFlag);
                break;
            case 36:
                strFieldValue = String.valueOf(AccResult1);
                break;
            case 37:
                strFieldValue = String.valueOf(AccResult2);
                break;
            case 38:
                strFieldValue = String.valueOf(CaseNo);
                break;
            case 39:
                strFieldValue = String.valueOf(SeqNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SubRptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubRptNo = FValue.trim();
            }
            else
                SubRptNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerName")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerName = FValue.trim();
            }
            else
                CustomerName = null;
        }
        if (FCode.equalsIgnoreCase("CustomerType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerType = FValue.trim();
            }
            else
                CustomerType = null;
        }
        if (FCode.equalsIgnoreCase("AccSubject")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccSubject = FValue.trim();
            }
            else
                AccSubject = null;
        }
        if (FCode.equalsIgnoreCase("AccidentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentType = FValue.trim();
            }
            else
                AccidentType = null;
        }
        if (FCode.equalsIgnoreCase("AccDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccDate = FValue.trim();
            }
            else
                AccDate = null;
        }
        if (FCode.equalsIgnoreCase("AccEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccEndDate = FValue.trim();
            }
            else
                AccEndDate = null;
        }
        if (FCode.equalsIgnoreCase("AccDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccDesc = FValue.trim();
            }
            else
                AccDesc = null;
        }
        if (FCode.equalsIgnoreCase("CustSituation")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustSituation = FValue.trim();
            }
            else
                CustSituation = null;
        }
        if (FCode.equalsIgnoreCase("AccPlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccPlace = FValue.trim();
            }
            else
                AccPlace = null;
        }
        if (FCode.equalsIgnoreCase("HospitalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HospitalCode = FValue.trim();
            }
            else
                HospitalCode = null;
        }
        if (FCode.equalsIgnoreCase("HospitalName")) {
            if( FValue != null && !FValue.equals(""))
            {
                HospitalName = FValue.trim();
            }
            else
                HospitalName = null;
        }
        if (FCode.equalsIgnoreCase("InHospitalDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                InHospitalDate = FValue.trim();
            }
            else
                InHospitalDate = null;
        }
        if (FCode.equalsIgnoreCase("OutHospitalDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutHospitalDate = FValue.trim();
            }
            else
                OutHospitalDate = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("SeriousGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                SeriousGrade = FValue.trim();
            }
            else
                SeriousGrade = null;
        }
        if (FCode.equalsIgnoreCase("SurveyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurveyFlag = FValue.trim();
            }
            else
                SurveyFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
                MngCom = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FirstDiaDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstDiaDate = FValue.trim();
            }
            else
                FirstDiaDate = null;
        }
        if (FCode.equalsIgnoreCase("HosGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                HosGrade = FValue.trim();
            }
            else
                HosGrade = null;
        }
        if (FCode.equalsIgnoreCase("LocalPlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                LocalPlace = FValue.trim();
            }
            else
                LocalPlace = null;
        }
        if (FCode.equalsIgnoreCase("HosTel")) {
            if( FValue != null && !FValue.equals(""))
            {
                HosTel = FValue.trim();
            }
            else
                HosTel = null;
        }
        if (FCode.equalsIgnoreCase("DieDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                DieDate = FValue.trim();
            }
            else
                DieDate = null;
        }
        if (FCode.equalsIgnoreCase("AccCause")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCause = FValue.trim();
            }
            else
                AccCause = null;
        }
        if (FCode.equalsIgnoreCase("VIPFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIPFlag = FValue.trim();
            }
            else
                VIPFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccidentDetail")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentDetail = FValue.trim();
            }
            else
                AccidentDetail = null;
        }
        if (FCode.equalsIgnoreCase("CureDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                CureDesc = FValue.trim();
            }
            else
                CureDesc = null;
        }
        if (FCode.equalsIgnoreCase("SubmitFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubmitFlag = FValue.trim();
            }
            else
                SubmitFlag = null;
        }
        if (FCode.equalsIgnoreCase("CondoleFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CondoleFlag = FValue.trim();
            }
            else
                CondoleFlag = null;
        }
        if (FCode.equalsIgnoreCase("DieFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DieFlag = FValue.trim();
            }
            else
                DieFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccResult1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccResult1 = FValue.trim();
            }
            else
                AccResult1 = null;
        }
        if (FCode.equalsIgnoreCase("AccResult2")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccResult2 = FValue.trim();
            }
            else
                AccResult2 = null;
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
                CaseNo = null;
        }
        if (FCode.equalsIgnoreCase("SeqNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SeqNo = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LLSubReportPojo [" +
            "SubRptNo="+SubRptNo +
            ", CustomerNo="+CustomerNo +
            ", CustomerName="+CustomerName +
            ", CustomerType="+CustomerType +
            ", AccSubject="+AccSubject +
            ", AccidentType="+AccidentType +
            ", AccDate="+AccDate +
            ", AccEndDate="+AccEndDate +
            ", AccDesc="+AccDesc +
            ", CustSituation="+CustSituation +
            ", AccPlace="+AccPlace +
            ", HospitalCode="+HospitalCode +
            ", HospitalName="+HospitalName +
            ", InHospitalDate="+InHospitalDate +
            ", OutHospitalDate="+OutHospitalDate +
            ", Remark="+Remark +
            ", SeriousGrade="+SeriousGrade +
            ", SurveyFlag="+SurveyFlag +
            ", Operator="+Operator +
            ", MngCom="+MngCom +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", FirstDiaDate="+FirstDiaDate +
            ", HosGrade="+HosGrade +
            ", LocalPlace="+LocalPlace +
            ", HosTel="+HosTel +
            ", DieDate="+DieDate +
            ", AccCause="+AccCause +
            ", VIPFlag="+VIPFlag +
            ", AccidentDetail="+AccidentDetail +
            ", CureDesc="+CureDesc +
            ", SubmitFlag="+SubmitFlag +
            ", CondoleFlag="+CondoleFlag +
            ", DieFlag="+DieFlag +
            ", AccResult1="+AccResult1 +
            ", AccResult2="+AccResult2 +
            ", CaseNo="+CaseNo +
            ", SeqNo="+SeqNo +"]";
    }
}
