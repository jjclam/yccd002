/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCContHangUpStatePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCContHangUpStatePojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long ContHUpStateID; 
    /** Shardingid */
    private String ShardingID; 
    /** 合同号 */
    private String ContNo; 
    /** 被保险人号码 */
    private String InsuredNo; 
    /** 保单险种号 */
    private String PolNo; 
    /** 挂起类型 */
    private String HangUpType; 
    /** 挂起号码 */
    private String HangUpNo; 
    /** 承保挂起标志 */
    private String NBFlag; 
    /** 保全挂起标志 */
    private String PosFlag; 
    /** 理赔挂起标志 */
    private String ClaimFlag; 
    /** 渠道挂起标志 */
    private String AgentFlag; 
    /** 续期挂起标志 */
    private String RNFlag; 
    /** 备用标志1 */
    private String StandFlag1; 
    /** 备用标志2 */
    private String StandFlag2; 
    /** 备用标志3 */
    private String StandFlag3; 
    /** 备注 */
    private String Remark; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** Standflag4 */
    private String StandFlag4; 
    /** Standflag5 */
    private String StandFlag5; 


    public static final int FIELDNUM = 23;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getContHUpStateID() {
        return ContHUpStateID;
    }
    public void setContHUpStateID(long aContHUpStateID) {
        ContHUpStateID = aContHUpStateID;
    }
    public void setContHUpStateID(String aContHUpStateID) {
        if (aContHUpStateID != null && !aContHUpStateID.equals("")) {
            ContHUpStateID = new Long(aContHUpStateID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getHangUpType() {
        return HangUpType;
    }
    public void setHangUpType(String aHangUpType) {
        HangUpType = aHangUpType;
    }
    public String getHangUpNo() {
        return HangUpNo;
    }
    public void setHangUpNo(String aHangUpNo) {
        HangUpNo = aHangUpNo;
    }
    public String getNBFlag() {
        return NBFlag;
    }
    public void setNBFlag(String aNBFlag) {
        NBFlag = aNBFlag;
    }
    public String getPosFlag() {
        return PosFlag;
    }
    public void setPosFlag(String aPosFlag) {
        PosFlag = aPosFlag;
    }
    public String getClaimFlag() {
        return ClaimFlag;
    }
    public void setClaimFlag(String aClaimFlag) {
        ClaimFlag = aClaimFlag;
    }
    public String getAgentFlag() {
        return AgentFlag;
    }
    public void setAgentFlag(String aAgentFlag) {
        AgentFlag = aAgentFlag;
    }
    public String getRNFlag() {
        return RNFlag;
    }
    public void setRNFlag(String aRNFlag) {
        RNFlag = aRNFlag;
    }
    public String getStandFlag1() {
        return StandFlag1;
    }
    public void setStandFlag1(String aStandFlag1) {
        StandFlag1 = aStandFlag1;
    }
    public String getStandFlag2() {
        return StandFlag2;
    }
    public void setStandFlag2(String aStandFlag2) {
        StandFlag2 = aStandFlag2;
    }
    public String getStandFlag3() {
        return StandFlag3;
    }
    public void setStandFlag3(String aStandFlag3) {
        StandFlag3 = aStandFlag3;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandFlag4() {
        return StandFlag4;
    }
    public void setStandFlag4(String aStandFlag4) {
        StandFlag4 = aStandFlag4;
    }
    public String getStandFlag5() {
        return StandFlag5;
    }
    public void setStandFlag5(String aStandFlag5) {
        StandFlag5 = aStandFlag5;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContHUpStateID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("ContNo") ) {
            return 2;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 3;
        }
        if( strFieldName.equals("PolNo") ) {
            return 4;
        }
        if( strFieldName.equals("HangUpType") ) {
            return 5;
        }
        if( strFieldName.equals("HangUpNo") ) {
            return 6;
        }
        if( strFieldName.equals("NBFlag") ) {
            return 7;
        }
        if( strFieldName.equals("PosFlag") ) {
            return 8;
        }
        if( strFieldName.equals("ClaimFlag") ) {
            return 9;
        }
        if( strFieldName.equals("AgentFlag") ) {
            return 10;
        }
        if( strFieldName.equals("RNFlag") ) {
            return 11;
        }
        if( strFieldName.equals("StandFlag1") ) {
            return 12;
        }
        if( strFieldName.equals("StandFlag2") ) {
            return 13;
        }
        if( strFieldName.equals("StandFlag3") ) {
            return 14;
        }
        if( strFieldName.equals("Remark") ) {
            return 15;
        }
        if( strFieldName.equals("Operator") ) {
            return 16;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 17;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 19;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 20;
        }
        if( strFieldName.equals("StandFlag4") ) {
            return 21;
        }
        if( strFieldName.equals("StandFlag5") ) {
            return 22;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContHUpStateID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "ContNo";
                break;
            case 3:
                strFieldName = "InsuredNo";
                break;
            case 4:
                strFieldName = "PolNo";
                break;
            case 5:
                strFieldName = "HangUpType";
                break;
            case 6:
                strFieldName = "HangUpNo";
                break;
            case 7:
                strFieldName = "NBFlag";
                break;
            case 8:
                strFieldName = "PosFlag";
                break;
            case 9:
                strFieldName = "ClaimFlag";
                break;
            case 10:
                strFieldName = "AgentFlag";
                break;
            case 11:
                strFieldName = "RNFlag";
                break;
            case 12:
                strFieldName = "StandFlag1";
                break;
            case 13:
                strFieldName = "StandFlag2";
                break;
            case 14:
                strFieldName = "StandFlag3";
                break;
            case 15:
                strFieldName = "Remark";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "ModifyDate";
                break;
            case 20:
                strFieldName = "ModifyTime";
                break;
            case 21:
                strFieldName = "StandFlag4";
                break;
            case 22:
                strFieldName = "StandFlag5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTHUPSTATEID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "HANGUPTYPE":
                return Schema.TYPE_STRING;
            case "HANGUPNO":
                return Schema.TYPE_STRING;
            case "NBFLAG":
                return Schema.TYPE_STRING;
            case "POSFLAG":
                return Schema.TYPE_STRING;
            case "CLAIMFLAG":
                return Schema.TYPE_STRING;
            case "AGENTFLAG":
                return Schema.TYPE_STRING;
            case "RNFLAG":
                return Schema.TYPE_STRING;
            case "STANDFLAG1":
                return Schema.TYPE_STRING;
            case "STANDFLAG2":
                return Schema.TYPE_STRING;
            case "STANDFLAG3":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDFLAG4":
                return Schema.TYPE_STRING;
            case "STANDFLAG5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContHUpStateID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContHUpStateID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("HangUpType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HangUpType));
        }
        if (FCode.equalsIgnoreCase("HangUpNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HangUpNo));
        }
        if (FCode.equalsIgnoreCase("NBFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NBFlag));
        }
        if (FCode.equalsIgnoreCase("PosFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PosFlag));
        }
        if (FCode.equalsIgnoreCase("ClaimFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimFlag));
        }
        if (FCode.equalsIgnoreCase("AgentFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentFlag));
        }
        if (FCode.equalsIgnoreCase("RNFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RNFlag));
        }
        if (FCode.equalsIgnoreCase("StandFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandFlag1));
        }
        if (FCode.equalsIgnoreCase("StandFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandFlag2));
        }
        if (FCode.equalsIgnoreCase("StandFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandFlag3));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandFlag4));
        }
        if (FCode.equalsIgnoreCase("StandFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandFlag5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ContHUpStateID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 4:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 5:
                strFieldValue = String.valueOf(HangUpType);
                break;
            case 6:
                strFieldValue = String.valueOf(HangUpNo);
                break;
            case 7:
                strFieldValue = String.valueOf(NBFlag);
                break;
            case 8:
                strFieldValue = String.valueOf(PosFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(ClaimFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(AgentFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(RNFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(StandFlag1);
                break;
            case 13:
                strFieldValue = String.valueOf(StandFlag2);
                break;
            case 14:
                strFieldValue = String.valueOf(StandFlag3);
                break;
            case 15:
                strFieldValue = String.valueOf(Remark);
                break;
            case 16:
                strFieldValue = String.valueOf(Operator);
                break;
            case 17:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 18:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 19:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 20:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 21:
                strFieldValue = String.valueOf(StandFlag4);
                break;
            case 22:
                strFieldValue = String.valueOf(StandFlag5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContHUpStateID")) {
            if( FValue != null && !FValue.equals("")) {
                ContHUpStateID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("HangUpType")) {
            if( FValue != null && !FValue.equals(""))
            {
                HangUpType = FValue.trim();
            }
            else
                HangUpType = null;
        }
        if (FCode.equalsIgnoreCase("HangUpNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                HangUpNo = FValue.trim();
            }
            else
                HangUpNo = null;
        }
        if (FCode.equalsIgnoreCase("NBFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NBFlag = FValue.trim();
            }
            else
                NBFlag = null;
        }
        if (FCode.equalsIgnoreCase("PosFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PosFlag = FValue.trim();
            }
            else
                PosFlag = null;
        }
        if (FCode.equalsIgnoreCase("ClaimFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClaimFlag = FValue.trim();
            }
            else
                ClaimFlag = null;
        }
        if (FCode.equalsIgnoreCase("AgentFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentFlag = FValue.trim();
            }
            else
                AgentFlag = null;
        }
        if (FCode.equalsIgnoreCase("RNFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RNFlag = FValue.trim();
            }
            else
                RNFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandFlag1 = FValue.trim();
            }
            else
                StandFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandFlag2 = FValue.trim();
            }
            else
                StandFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandFlag3 = FValue.trim();
            }
            else
                StandFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandFlag4 = FValue.trim();
            }
            else
                StandFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandFlag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandFlag5 = FValue.trim();
            }
            else
                StandFlag5 = null;
        }
        return true;
    }


    public String toString() {
    return "LCContHangUpStatePojo [" +
            "ContHUpStateID="+ContHUpStateID +
            ", ShardingID="+ShardingID +
            ", ContNo="+ContNo +
            ", InsuredNo="+InsuredNo +
            ", PolNo="+PolNo +
            ", HangUpType="+HangUpType +
            ", HangUpNo="+HangUpNo +
            ", NBFlag="+NBFlag +
            ", PosFlag="+PosFlag +
            ", ClaimFlag="+ClaimFlag +
            ", AgentFlag="+AgentFlag +
            ", RNFlag="+RNFlag +
            ", StandFlag1="+StandFlag1 +
            ", StandFlag2="+StandFlag2 +
            ", StandFlag3="+StandFlag3 +
            ", Remark="+Remark +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", StandFlag4="+StandFlag4 +
            ", StandFlag5="+StandFlag5 +"]";
    }
}
