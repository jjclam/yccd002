/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCHealthCheckInfoPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-12-11
 */
public class LCHealthCheckInfoPojo implements  Pojo,Serializable {
    // @Field
    /** 业务主键 */
    private String Serialno; 
    /** 保单号 */
    private String ContNo; 
    /** 被保人客户号 */
    private String Insuredno; 
    /** 被保人姓名 */
    private String InsuredName; 
    /** 证件类型 */
    private String IDType; 
    /** 证件号 */
    private String IDNo; 
    /** 保单处理状态 */
    private String ContDealSatet; 
    /** 保单处理结果信息 */
    private String ContDealResultInfo; 
    /** 健康体检状态 */
    private String HealthCheckState; 
    /** 体检结论信息 */
    private String HealthCheckResultInfo; 
    /** 备用字段1 */
    private String standby1; 
    /** 备用字段2 */
    private String standby2; 
    /** 备用字段3 */
    private String standby3; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后修改日期 */
    private String  ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 17;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSerialno() {
        return Serialno;
    }
    public void setSerialno(String aSerialno) {
        Serialno = aSerialno;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredno() {
        return Insuredno;
    }
    public void setInsuredno(String aInsuredno) {
        Insuredno = aInsuredno;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getContDealSatet() {
        return ContDealSatet;
    }
    public void setContDealSatet(String aContDealSatet) {
        ContDealSatet = aContDealSatet;
    }
    public String getContDealResultInfo() {
        return ContDealResultInfo;
    }
    public void setContDealResultInfo(String aContDealResultInfo) {
        ContDealResultInfo = aContDealResultInfo;
    }
    public String getHealthCheckState() {
        return HealthCheckState;
    }
    public void setHealthCheckState(String aHealthCheckState) {
        HealthCheckState = aHealthCheckState;
    }
    public String getHealthCheckResultInfo() {
        return HealthCheckResultInfo;
    }
    public void setHealthCheckResultInfo(String aHealthCheckResultInfo) {
        HealthCheckResultInfo = aHealthCheckResultInfo;
    }
    public String getStandby1() {
        return standby1;
    }
    public void setStandby1(String astandby1) {
        standby1 = astandby1;
    }
    public String getStandby2() {
        return standby2;
    }
    public void setStandby2(String astandby2) {
        standby2 = astandby2;
    }
    public String getStandby3() {
        return standby3;
    }
    public void setStandby3(String astandby3) {
        standby3 = astandby3;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("Serialno") ) {
            return 0;
        }
        if( strFieldName.equals("ContNo") ) {
            return 1;
        }
        if( strFieldName.equals("Insuredno") ) {
            return 2;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 3;
        }
        if( strFieldName.equals("IDType") ) {
            return 4;
        }
        if( strFieldName.equals("IDNo") ) {
            return 5;
        }
        if( strFieldName.equals("ContDealSatet") ) {
            return 6;
        }
        if( strFieldName.equals("ContDealResultInfo") ) {
            return 7;
        }
        if( strFieldName.equals("HealthCheckState") ) {
            return 8;
        }
        if( strFieldName.equals("HealthCheckResultInfo") ) {
            return 9;
        }
        if( strFieldName.equals("standby1") ) {
            return 10;
        }
        if( strFieldName.equals("standby2") ) {
            return 11;
        }
        if( strFieldName.equals("standby3") ) {
            return 12;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 13;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 16;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "Serialno";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "Insuredno";
                break;
            case 3:
                strFieldName = "InsuredName";
                break;
            case 4:
                strFieldName = "IDType";
                break;
            case 5:
                strFieldName = "IDNo";
                break;
            case 6:
                strFieldName = "ContDealSatet";
                break;
            case 7:
                strFieldName = "ContDealResultInfo";
                break;
            case 8:
                strFieldName = "HealthCheckState";
                break;
            case 9:
                strFieldName = "HealthCheckResultInfo";
                break;
            case 10:
                strFieldName = "standby1";
                break;
            case 11:
                strFieldName = "standby2";
                break;
            case 12:
                strFieldName = "standby3";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "CONTDEALSATET":
                return Schema.TYPE_STRING;
            case "CONTDEALRESULTINFO":
                return Schema.TYPE_STRING;
            case "HEALTHCHECKSTATE":
                return Schema.TYPE_STRING;
            case "HEALTHCHECKRESULTINFO":
                return Schema.TYPE_STRING;
            case "STANDBY1":
                return Schema.TYPE_STRING;
            case "STANDBY2":
                return Schema.TYPE_STRING;
            case "STANDBY3":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("Serialno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Serialno));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("Insuredno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Insuredno));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("ContDealSatet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContDealSatet));
        }
        if (FCode.equalsIgnoreCase("ContDealResultInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContDealResultInfo));
        }
        if (FCode.equalsIgnoreCase("HealthCheckState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthCheckState));
        }
        if (FCode.equalsIgnoreCase("HealthCheckResultInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthCheckResultInfo));
        }
        if (FCode.equalsIgnoreCase("standby1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(standby1));
        }
        if (FCode.equalsIgnoreCase("standby2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(standby2));
        }
        if (FCode.equalsIgnoreCase("standby3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(standby3));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(Serialno);
                break;
            case 1:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(Insuredno);
                break;
            case 3:
                strFieldValue = String.valueOf(InsuredName);
                break;
            case 4:
                strFieldValue = String.valueOf(IDType);
                break;
            case 5:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 6:
                strFieldValue = String.valueOf(ContDealSatet);
                break;
            case 7:
                strFieldValue = String.valueOf(ContDealResultInfo);
                break;
            case 8:
                strFieldValue = String.valueOf(HealthCheckState);
                break;
            case 9:
                strFieldValue = String.valueOf(HealthCheckResultInfo);
                break;
            case 10:
                strFieldValue = String.valueOf(standby1);
                break;
            case 11:
                strFieldValue = String.valueOf(standby2);
                break;
            case 12:
                strFieldValue = String.valueOf(standby3);
                break;
            case 13:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 15:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 16:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("Serialno")) {
            if( FValue != null && !FValue.equals(""))
            {
                Serialno = FValue.trim();
            }
            else
                Serialno = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("Insuredno")) {
            if( FValue != null && !FValue.equals(""))
            {
                Insuredno = FValue.trim();
            }
            else
                Insuredno = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("ContDealSatet")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContDealSatet = FValue.trim();
            }
            else
                ContDealSatet = null;
        }
        if (FCode.equalsIgnoreCase("ContDealResultInfo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContDealResultInfo = FValue.trim();
            }
            else
                ContDealResultInfo = null;
        }
        if (FCode.equalsIgnoreCase("HealthCheckState")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthCheckState = FValue.trim();
            }
            else
                HealthCheckState = null;
        }
        if (FCode.equalsIgnoreCase("HealthCheckResultInfo")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthCheckResultInfo = FValue.trim();
            }
            else
                HealthCheckResultInfo = null;
        }
        if (FCode.equalsIgnoreCase("standby1")) {
            if( FValue != null && !FValue.equals(""))
            {
                standby1 = FValue.trim();
            }
            else
                standby1 = null;
        }
        if (FCode.equalsIgnoreCase("standby2")) {
            if( FValue != null && !FValue.equals(""))
            {
                standby2 = FValue.trim();
            }
            else
                standby2 = null;
        }
        if (FCode.equalsIgnoreCase("standby3")) {
            if( FValue != null && !FValue.equals(""))
            {
                standby3 = FValue.trim();
            }
            else
                standby3 = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LCHealthCheckInfoPojo [" +
            "Serialno="+Serialno +
            ", ContNo="+ContNo +
            ", Insuredno="+Insuredno +
            ", InsuredName="+InsuredName +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", ContDealSatet="+ContDealSatet +
            ", ContDealResultInfo="+ContDealResultInfo +
            ", HealthCheckState="+HealthCheckState +
            ", HealthCheckResultInfo="+HealthCheckResultInfo +
            ", standby1="+standby1 +
            ", standby2="+standby2 +
            ", standby3="+standby3 +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
