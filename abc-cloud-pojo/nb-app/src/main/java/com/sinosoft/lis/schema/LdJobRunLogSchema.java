/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LdJobRunLogDB;

/**
 * <p>ClassName: LdJobRunLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-11-23
 */
public class LdJobRunLogSchema implements Schema, Cloneable {
    // @Field
    /** 序号 */
    private long SerialNo;
    /** 基本任务名称 */
    private String TaskName;
    /** 基本任务编码 */
    private String TaskCode;
    /** 执行日期 */
    private Date ExecuteDate;
    /** 执行时间 */
    private String ExecuteTime;
    /** 完成日期 */
    private Date FinishDate;
    /** 完成时间 */
    private String FinishTime;
    /** 执行次数 */
    private long ExecuteCount;
    /** 执行状态 */
    private String ExecuteState;
    /** 执行结果 */
    private String ExecuteResult;
    /** 备用字段1 */
    private String Remark1;
    /** 备用字段2 */
    private String Remark2;

    public static final int FIELDNUM = 12;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LdJobRunLogSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LdJobRunLogSchema cloned = (LdJobRunLogSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(long aSerialNo) {
        SerialNo = aSerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        if (aSerialNo != null && !aSerialNo.equals("")) {
            SerialNo = new Long(aSerialNo).longValue();
        }
    }

    public String getTaskName() {
        return TaskName;
    }
    public void setTaskName(String aTaskName) {
        TaskName = aTaskName;
    }
    public String getTaskCode() {
        return TaskCode;
    }
    public void setTaskCode(String aTaskCode) {
        TaskCode = aTaskCode;
    }
    public String getExecuteDate() {
        if(ExecuteDate != null) {
            return fDate.getString(ExecuteDate);
        } else {
            return null;
        }
    }
    public void setExecuteDate(Date aExecuteDate) {
        ExecuteDate = aExecuteDate;
    }
    public void setExecuteDate(String aExecuteDate) {
        if (aExecuteDate != null && !aExecuteDate.equals("")) {
            ExecuteDate = fDate.getDate(aExecuteDate);
        } else
            ExecuteDate = null;
    }

    public String getExecuteTime() {
        return ExecuteTime;
    }
    public void setExecuteTime(String aExecuteTime) {
        ExecuteTime = aExecuteTime;
    }
    public String getFinishDate() {
        if(FinishDate != null) {
            return fDate.getString(FinishDate);
        } else {
            return null;
        }
    }
    public void setFinishDate(Date aFinishDate) {
        FinishDate = aFinishDate;
    }
    public void setFinishDate(String aFinishDate) {
        if (aFinishDate != null && !aFinishDate.equals("")) {
            FinishDate = fDate.getDate(aFinishDate);
        } else
            FinishDate = null;
    }

    public String getFinishTime() {
        return FinishTime;
    }
    public void setFinishTime(String aFinishTime) {
        FinishTime = aFinishTime;
    }
    public long getExecuteCount() {
        return ExecuteCount;
    }
    public void setExecuteCount(long aExecuteCount) {
        ExecuteCount = aExecuteCount;
    }
    public void setExecuteCount(String aExecuteCount) {
        if (aExecuteCount != null && !aExecuteCount.equals("")) {
            ExecuteCount = new Long(aExecuteCount).longValue();
        }
    }

    public String getExecuteState() {
        return ExecuteState;
    }
    public void setExecuteState(String aExecuteState) {
        ExecuteState = aExecuteState;
    }
    public String getExecuteResult() {
        return ExecuteResult;
    }
    public void setExecuteResult(String aExecuteResult) {
        ExecuteResult = aExecuteResult;
    }
    public String getRemark1() {
        return Remark1;
    }
    public void setRemark1(String aRemark1) {
        Remark1 = aRemark1;
    }
    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }

    /**
    * 使用另外一个 LdJobRunLogSchema 对象给 Schema 赋值
    * @param: aLdJobRunLogSchema LdJobRunLogSchema
    **/
    public void setSchema(LdJobRunLogSchema aLdJobRunLogSchema) {
        this.SerialNo = aLdJobRunLogSchema.getSerialNo();
        this.TaskName = aLdJobRunLogSchema.getTaskName();
        this.TaskCode = aLdJobRunLogSchema.getTaskCode();
        this.ExecuteDate = fDate.getDate( aLdJobRunLogSchema.getExecuteDate());
        this.ExecuteTime = aLdJobRunLogSchema.getExecuteTime();
        this.FinishDate = fDate.getDate( aLdJobRunLogSchema.getFinishDate());
        this.FinishTime = aLdJobRunLogSchema.getFinishTime();
        this.ExecuteCount = aLdJobRunLogSchema.getExecuteCount();
        this.ExecuteState = aLdJobRunLogSchema.getExecuteState();
        this.ExecuteResult = aLdJobRunLogSchema.getExecuteResult();
        this.Remark1 = aLdJobRunLogSchema.getRemark1();
        this.Remark2 = aLdJobRunLogSchema.getRemark2();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.SerialNo = rs.getLong("SerialNo");
            if( rs.getString("TaskName") == null )
                this.TaskName = null;
            else
                this.TaskName = rs.getString("TaskName").trim();

            if( rs.getString("TaskCode") == null )
                this.TaskCode = null;
            else
                this.TaskCode = rs.getString("TaskCode").trim();

            this.ExecuteDate = rs.getDate("ExecuteDate");
            if( rs.getString("ExecuteTime") == null )
                this.ExecuteTime = null;
            else
                this.ExecuteTime = rs.getString("ExecuteTime").trim();

            this.FinishDate = rs.getDate("FinishDate");
            if( rs.getString("FinishTime") == null )
                this.FinishTime = null;
            else
                this.FinishTime = rs.getString("FinishTime").trim();

            this.ExecuteCount = rs.getLong("ExecuteCount");
            if( rs.getString("ExecuteState") == null )
                this.ExecuteState = null;
            else
                this.ExecuteState = rs.getString("ExecuteState").trim();

            if( rs.getString("ExecuteResult") == null )
                this.ExecuteResult = null;
            else
                this.ExecuteResult = rs.getString("ExecuteResult").trim();

            if( rs.getString("Remark1") == null )
                this.Remark1 = null;
            else
                this.Remark1 = rs.getString("Remark1").trim();

            if( rs.getString("Remark2") == null )
                this.Remark2 = null;
            else
                this.Remark2 = rs.getString("Remark2").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LdJobRunLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LdJobRunLogSchema getSchema() {
        LdJobRunLogSchema aLdJobRunLogSchema = new LdJobRunLogSchema();
        aLdJobRunLogSchema.setSchema(this);
        return aLdJobRunLogSchema;
    }

    public LdJobRunLogDB getDB() {
        LdJobRunLogDB aDBOper = new LdJobRunLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLdJobRunLog描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(SerialNo));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaskName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ExecuteDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExecuteTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FinishDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FinishTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ExecuteCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExecuteState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExecuteResult)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark2));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLdJobRunLog>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).longValue();
            TaskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            TaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ExecuteDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
            ExecuteTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            FinishDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
            FinishTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ExecuteCount = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).longValue();
            ExecuteState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ExecuteResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LdJobRunLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("TaskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskName));
        }
        if (FCode.equalsIgnoreCase("TaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskCode));
        }
        if (FCode.equalsIgnoreCase("ExecuteDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getExecuteDate()));
        }
        if (FCode.equalsIgnoreCase("ExecuteTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteTime));
        }
        if (FCode.equalsIgnoreCase("FinishDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFinishDate()));
        }
        if (FCode.equalsIgnoreCase("FinishTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FinishTime));
        }
        if (FCode.equalsIgnoreCase("ExecuteCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCount));
        }
        if (FCode.equalsIgnoreCase("ExecuteState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteState));
        }
        if (FCode.equalsIgnoreCase("ExecuteResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteResult));
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TaskName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TaskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getExecuteDate()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ExecuteTime);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFinishDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(FinishTime);
                break;
            case 7:
                strFieldValue = String.valueOf(ExecuteCount);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ExecuteState);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ExecuteResult);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Remark1);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Remark2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals("")) {
                SerialNo = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("TaskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaskName = FValue.trim();
            }
            else
                TaskName = null;
        }
        if (FCode.equalsIgnoreCase("TaskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaskCode = FValue.trim();
            }
            else
                TaskCode = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteDate")) {
            if(FValue != null && !FValue.equals("")) {
                ExecuteDate = fDate.getDate( FValue );
            }
            else
                ExecuteDate = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteTime = FValue.trim();
            }
            else
                ExecuteTime = null;
        }
        if (FCode.equalsIgnoreCase("FinishDate")) {
            if(FValue != null && !FValue.equals("")) {
                FinishDate = fDate.getDate( FValue );
            }
            else
                FinishDate = null;
        }
        if (FCode.equalsIgnoreCase("FinishTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                FinishTime = FValue.trim();
            }
            else
                FinishTime = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteCount")) {
            if( FValue != null && !FValue.equals("")) {
                ExecuteCount = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ExecuteState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteState = FValue.trim();
            }
            else
                ExecuteState = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteResult = FValue.trim();
            }
            else
                ExecuteResult = null;
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark1 = FValue.trim();
            }
            else
                Remark1 = null;
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LdJobRunLogSchema other = (LdJobRunLogSchema)otherObject;
        return
            SerialNo == other.getSerialNo()
            && TaskName.equals(other.getTaskName())
            && TaskCode.equals(other.getTaskCode())
            && fDate.getString(ExecuteDate).equals(other.getExecuteDate())
            && ExecuteTime.equals(other.getExecuteTime())
            && fDate.getString(FinishDate).equals(other.getFinishDate())
            && FinishTime.equals(other.getFinishTime())
            && ExecuteCount == other.getExecuteCount()
            && ExecuteState.equals(other.getExecuteState())
            && ExecuteResult.equals(other.getExecuteResult())
            && Remark1.equals(other.getRemark1())
            && Remark2.equals(other.getRemark2());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("TaskName") ) {
            return 1;
        }
        if( strFieldName.equals("TaskCode") ) {
            return 2;
        }
        if( strFieldName.equals("ExecuteDate") ) {
            return 3;
        }
        if( strFieldName.equals("ExecuteTime") ) {
            return 4;
        }
        if( strFieldName.equals("FinishDate") ) {
            return 5;
        }
        if( strFieldName.equals("FinishTime") ) {
            return 6;
        }
        if( strFieldName.equals("ExecuteCount") ) {
            return 7;
        }
        if( strFieldName.equals("ExecuteState") ) {
            return 8;
        }
        if( strFieldName.equals("ExecuteResult") ) {
            return 9;
        }
        if( strFieldName.equals("Remark1") ) {
            return 10;
        }
        if( strFieldName.equals("Remark2") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "TaskName";
                break;
            case 2:
                strFieldName = "TaskCode";
                break;
            case 3:
                strFieldName = "ExecuteDate";
                break;
            case 4:
                strFieldName = "ExecuteTime";
                break;
            case 5:
                strFieldName = "FinishDate";
                break;
            case 6:
                strFieldName = "FinishTime";
                break;
            case 7:
                strFieldName = "ExecuteCount";
                break;
            case 8:
                strFieldName = "ExecuteState";
                break;
            case 9:
                strFieldName = "ExecuteResult";
                break;
            case 10:
                strFieldName = "Remark1";
                break;
            case 11:
                strFieldName = "Remark2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_LONG;
            case "TASKNAME":
                return Schema.TYPE_STRING;
            case "TASKCODE":
                return Schema.TYPE_STRING;
            case "EXECUTEDATE":
                return Schema.TYPE_DATE;
            case "EXECUTETIME":
                return Schema.TYPE_STRING;
            case "FINISHDATE":
                return Schema.TYPE_DATE;
            case "FINISHTIME":
                return Schema.TYPE_STRING;
            case "EXECUTECOUNT":
                return Schema.TYPE_LONG;
            case "EXECUTESTATE":
                return Schema.TYPE_STRING;
            case "EXECUTERESULT":
                return Schema.TYPE_STRING;
            case "REMARK1":
                return Schema.TYPE_STRING;
            case "REMARK2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_DATE;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DATE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_LONG;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
