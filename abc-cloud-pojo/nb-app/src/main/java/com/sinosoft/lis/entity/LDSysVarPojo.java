/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDSysVarPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LDSysVarPojo implements Pojo,Serializable {
    // @Field
    /** 系统变量名 */
    @RedisPrimaryHKey
    private String SysVar; 
    /** 系统变量类型 */
    private String SysVarType; 
    /** 系统变量值 */
    private String SysVarValue; 


    public static final int FIELDNUM = 3;    // 数据库表的字段个数
    public String getSysVar() {
        return SysVar;
    }
    public void setSysVar(String aSysVar) {
        SysVar = aSysVar;
    }
    public String getSysVarType() {
        return SysVarType;
    }
    public void setSysVarType(String aSysVarType) {
        SysVarType = aSysVarType;
    }
    public String getSysVarValue() {
        return SysVarValue;
    }
    public void setSysVarValue(String aSysVarValue) {
        SysVarValue = aSysVarValue;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SysVar") ) {
            return 0;
        }
        if( strFieldName.equals("SysVarType") ) {
            return 1;
        }
        if( strFieldName.equals("SysVarValue") ) {
            return 2;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SysVar";
                break;
            case 1:
                strFieldName = "SysVarType";
                break;
            case 2:
                strFieldName = "SysVarValue";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SYSVAR":
                return Schema.TYPE_STRING;
            case "SYSVARTYPE":
                return Schema.TYPE_STRING;
            case "SYSVARVALUE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SysVar")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SysVar));
        }
        if (FCode.equalsIgnoreCase("SysVarType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SysVarType));
        }
        if (FCode.equalsIgnoreCase("SysVarValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SysVarValue));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SysVar);
                break;
            case 1:
                strFieldValue = String.valueOf(SysVarType);
                break;
            case 2:
                strFieldValue = String.valueOf(SysVarValue);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SysVar")) {
            if( FValue != null && !FValue.equals(""))
            {
                SysVar = FValue.trim();
            }
            else
                SysVar = null;
        }
        if (FCode.equalsIgnoreCase("SysVarType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SysVarType = FValue.trim();
            }
            else
                SysVarType = null;
        }
        if (FCode.equalsIgnoreCase("SysVarValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                SysVarValue = FValue.trim();
            }
            else
                SysVarValue = null;
        }
        return true;
    }


    public String toString() {
    return "LDSysVarPojo [" +
            "SysVar="+SysVar +
            ", SysVarType="+SysVarType +
            ", SysVarValue="+SysVarValue +"]";
    }
}
