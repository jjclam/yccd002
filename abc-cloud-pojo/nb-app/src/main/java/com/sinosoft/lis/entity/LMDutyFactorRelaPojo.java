/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyFactorRelaPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-24
 */
public class LMDutyFactorRelaPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 责任编码 */
    private String DutyCode; 
    /** 要素编码 */
    private String FactorCode; 
    /** 强制标志 */
    private String MandatoryFlag; 
    /** 默认值 */
    private String DefaultValue; 
    /** 要素顺序 */
    private int FactorOrder; 


    public static final int FIELDNUM = 6;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getFactorCode() {
        return FactorCode;
    }
    public void setFactorCode(String aFactorCode) {
        FactorCode = aFactorCode;
    }
    public String getMandatoryFlag() {
        return MandatoryFlag;
    }
    public void setMandatoryFlag(String aMandatoryFlag) {
        MandatoryFlag = aMandatoryFlag;
    }
    public String getDefaultValue() {
        return DefaultValue;
    }
    public void setDefaultValue(String aDefaultValue) {
        DefaultValue = aDefaultValue;
    }
    public int getFactorOrder() {
        return FactorOrder;
    }
    public void setFactorOrder(int aFactorOrder) {
        FactorOrder = aFactorOrder;
    }
    public void setFactorOrder(String aFactorOrder) {
        if (aFactorOrder != null && !aFactorOrder.equals("")) {
            Integer tInteger = new Integer(aFactorOrder);
            int i = tInteger.intValue();
            FactorOrder = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 1;
        }
        if( strFieldName.equals("FactorCode") ) {
            return 2;
        }
        if( strFieldName.equals("MandatoryFlag") ) {
            return 3;
        }
        if( strFieldName.equals("DefaultValue") ) {
            return 4;
        }
        if( strFieldName.equals("FactorOrder") ) {
            return 5;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "DutyCode";
                break;
            case 2:
                strFieldName = "FactorCode";
                break;
            case 3:
                strFieldName = "MandatoryFlag";
                break;
            case 4:
                strFieldName = "DefaultValue";
                break;
            case 5:
                strFieldName = "FactorOrder";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "FACTORCODE":
                return Schema.TYPE_STRING;
            case "MANDATORYFLAG":
                return Schema.TYPE_STRING;
            case "DEFAULTVALUE":
                return Schema.TYPE_STRING;
            case "FACTORORDER":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("FactorCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorCode));
        }
        if (FCode.equalsIgnoreCase("MandatoryFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MandatoryFlag));
        }
        if (FCode.equalsIgnoreCase("DefaultValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultValue));
        }
        if (FCode.equalsIgnoreCase("FactorOrder")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorOrder));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 2:
                strFieldValue = String.valueOf(FactorCode);
                break;
            case 3:
                strFieldValue = String.valueOf(MandatoryFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(DefaultValue);
                break;
            case 5:
                strFieldValue = String.valueOf(FactorOrder);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("FactorCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorCode = FValue.trim();
            }
            else
                FactorCode = null;
        }
        if (FCode.equalsIgnoreCase("MandatoryFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MandatoryFlag = FValue.trim();
            }
            else
                MandatoryFlag = null;
        }
        if (FCode.equalsIgnoreCase("DefaultValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                DefaultValue = FValue.trim();
            }
            else
                DefaultValue = null;
        }
        if (FCode.equalsIgnoreCase("FactorOrder")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                FactorOrder = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LMDutyFactorRelaPojo [" +
            "RiskCode="+RiskCode +
            ", DutyCode="+DutyCode +
            ", FactorCode="+FactorCode +
            ", MandatoryFlag="+MandatoryFlag +
            ", DefaultValue="+DefaultValue +
            ", FactorOrder="+FactorOrder +"]";
    }
}
