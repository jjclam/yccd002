/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCImageUploadInfoDB;

/**
 * <p>ClassName: LCImageUploadInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-10-12
 */
public class LCImageUploadInfoSchema implements Schema, Cloneable {
    // @Field
    /** 请求流水号 */
    private String TransNo;
    /** 保单号 */
    private String ContNo;
    /** 业务类型编码 */
    private String AppCode;
    /** 图片数据类型 */
    private String DataType;
    /** 上传路径 */
    private String UploadUrl;
    /** 上传状态 */
    private String UploadState;
    /** 结果描述 */
    private String ResultRemark;
    /** 上传次数 */
    private String UploadCount;
    /** 备用字段1 */
    private String StandByfFag1;
    /** 备用字段2 */
    private String StandByfFag2;
    /** 备用字段3 */
    private String StandByfFag3;
    /** 备用字段4 */
    private String StandByfFag4;
    /** 备用字段5 */
    private String StandByfFag5;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 17;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCImageUploadInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TransNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCImageUploadInfoSchema cloned = (LCImageUploadInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getTransNo() {
        return TransNo;
    }
    public void setTransNo(String aTransNo) {
        TransNo = aTransNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getAppCode() {
        return AppCode;
    }
    public void setAppCode(String aAppCode) {
        AppCode = aAppCode;
    }
    public String getDataType() {
        return DataType;
    }
    public void setDataType(String aDataType) {
        DataType = aDataType;
    }
    public String getUploadUrl() {
        return UploadUrl;
    }
    public void setUploadUrl(String aUploadUrl) {
        UploadUrl = aUploadUrl;
    }
    public String getUploadState() {
        return UploadState;
    }
    public void setUploadState(String aUploadState) {
        UploadState = aUploadState;
    }
    public String getResultRemark() {
        return ResultRemark;
    }
    public void setResultRemark(String aResultRemark) {
        ResultRemark = aResultRemark;
    }
    public String getUploadCount() {
        return UploadCount;
    }
    public void setUploadCount(String aUploadCount) {
        UploadCount = aUploadCount;
    }
    public String getStandByfFag1() {
        return StandByfFag1;
    }
    public void setStandByfFag1(String aStandByfFag1) {
        StandByfFag1 = aStandByfFag1;
    }
    public String getStandByfFag2() {
        return StandByfFag2;
    }
    public void setStandByfFag2(String aStandByfFag2) {
        StandByfFag2 = aStandByfFag2;
    }
    public String getStandByfFag3() {
        return StandByfFag3;
    }
    public void setStandByfFag3(String aStandByfFag3) {
        StandByfFag3 = aStandByfFag3;
    }
    public String getStandByfFag4() {
        return StandByfFag4;
    }
    public void setStandByfFag4(String aStandByfFag4) {
        StandByfFag4 = aStandByfFag4;
    }
    public String getStandByfFag5() {
        return StandByfFag5;
    }
    public void setStandByfFag5(String aStandByfFag5) {
        StandByfFag5 = aStandByfFag5;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 使用另外一个 LCImageUploadInfoSchema 对象给 Schema 赋值
    * @param: aLCImageUploadInfoSchema LCImageUploadInfoSchema
    **/
    public void setSchema(LCImageUploadInfoSchema aLCImageUploadInfoSchema) {
        this.TransNo = aLCImageUploadInfoSchema.getTransNo();
        this.ContNo = aLCImageUploadInfoSchema.getContNo();
        this.AppCode = aLCImageUploadInfoSchema.getAppCode();
        this.DataType = aLCImageUploadInfoSchema.getDataType();
        this.UploadUrl = aLCImageUploadInfoSchema.getUploadUrl();
        this.UploadState = aLCImageUploadInfoSchema.getUploadState();
        this.ResultRemark = aLCImageUploadInfoSchema.getResultRemark();
        this.UploadCount = aLCImageUploadInfoSchema.getUploadCount();
        this.StandByfFag1 = aLCImageUploadInfoSchema.getStandByfFag1();
        this.StandByfFag2 = aLCImageUploadInfoSchema.getStandByfFag2();
        this.StandByfFag3 = aLCImageUploadInfoSchema.getStandByfFag3();
        this.StandByfFag4 = aLCImageUploadInfoSchema.getStandByfFag4();
        this.StandByfFag5 = aLCImageUploadInfoSchema.getStandByfFag5();
        this.MakeDate = fDate.getDate( aLCImageUploadInfoSchema.getMakeDate());
        this.MakeTime = aLCImageUploadInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCImageUploadInfoSchema.getModifyDate());
        this.ModifyTime = aLCImageUploadInfoSchema.getModifyTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("TransNo") == null )
                this.TransNo = null;
            else
                this.TransNo = rs.getString("TransNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("AppCode") == null )
                this.AppCode = null;
            else
                this.AppCode = rs.getString("AppCode").trim();

            if( rs.getString("DataType") == null )
                this.DataType = null;
            else
                this.DataType = rs.getString("DataType").trim();

            if( rs.getString("UploadUrl") == null )
                this.UploadUrl = null;
            else
                this.UploadUrl = rs.getString("UploadUrl").trim();

            if( rs.getString("UploadState") == null )
                this.UploadState = null;
            else
                this.UploadState = rs.getString("UploadState").trim();

            if( rs.getString("ResultRemark") == null )
                this.ResultRemark = null;
            else
                this.ResultRemark = rs.getString("ResultRemark").trim();

            if( rs.getString("UploadCount") == null )
                this.UploadCount = null;
            else
                this.UploadCount = rs.getString("UploadCount").trim();

            if( rs.getString("StandByfFag1") == null )
                this.StandByfFag1 = null;
            else
                this.StandByfFag1 = rs.getString("StandByfFag1").trim();

            if( rs.getString("StandByfFag2") == null )
                this.StandByfFag2 = null;
            else
                this.StandByfFag2 = rs.getString("StandByfFag2").trim();

            if( rs.getString("StandByfFag3") == null )
                this.StandByfFag3 = null;
            else
                this.StandByfFag3 = rs.getString("StandByfFag3").trim();

            if( rs.getString("StandByfFag4") == null )
                this.StandByfFag4 = null;
            else
                this.StandByfFag4 = rs.getString("StandByfFag4").trim();

            if( rs.getString("StandByfFag5") == null )
                this.StandByfFag5 = null;
            else
                this.StandByfFag5 = rs.getString("StandByfFag5").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCImageUploadInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCImageUploadInfoSchema getSchema() {
        LCImageUploadInfoSchema aLCImageUploadInfoSchema = new LCImageUploadInfoSchema();
        aLCImageUploadInfoSchema.setSchema(this);
        return aLCImageUploadInfoSchema;
    }

    public LCImageUploadInfoDB getDB() {
        LCImageUploadInfoDB aDBOper = new LCImageUploadInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCImageUploadInfo描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DataType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UploadUrl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UploadState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ResultRemark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UploadCount)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByfFag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByfFag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByfFag3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByfFag4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByfFag5)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCImageUploadInfo>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            AppCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            DataType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            UploadUrl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            UploadState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ResultRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            UploadCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            StandByfFag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            StandByfFag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            StandByfFag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            StandByfFag4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            StandByfFag5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCImageUploadInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("AppCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppCode));
        }
        if (FCode.equalsIgnoreCase("DataType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DataType));
        }
        if (FCode.equalsIgnoreCase("UploadUrl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UploadUrl));
        }
        if (FCode.equalsIgnoreCase("UploadState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UploadState));
        }
        if (FCode.equalsIgnoreCase("ResultRemark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultRemark));
        }
        if (FCode.equalsIgnoreCase("UploadCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UploadCount));
        }
        if (FCode.equalsIgnoreCase("StandByfFag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag1));
        }
        if (FCode.equalsIgnoreCase("StandByfFag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag2));
        }
        if (FCode.equalsIgnoreCase("StandByfFag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag3));
        }
        if (FCode.equalsIgnoreCase("StandByfFag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag4));
        }
        if (FCode.equalsIgnoreCase("StandByfFag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByfFag5));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TransNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AppCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DataType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(UploadUrl);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(UploadState);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ResultRemark);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(UploadCount);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(StandByfFag1);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(StandByfFag2);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(StandByfFag3);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(StandByfFag4);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(StandByfFag5);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TransNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransNo = FValue.trim();
            }
            else
                TransNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("AppCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppCode = FValue.trim();
            }
            else
                AppCode = null;
        }
        if (FCode.equalsIgnoreCase("DataType")) {
            if( FValue != null && !FValue.equals(""))
            {
                DataType = FValue.trim();
            }
            else
                DataType = null;
        }
        if (FCode.equalsIgnoreCase("UploadUrl")) {
            if( FValue != null && !FValue.equals(""))
            {
                UploadUrl = FValue.trim();
            }
            else
                UploadUrl = null;
        }
        if (FCode.equalsIgnoreCase("UploadState")) {
            if( FValue != null && !FValue.equals(""))
            {
                UploadState = FValue.trim();
            }
            else
                UploadState = null;
        }
        if (FCode.equalsIgnoreCase("ResultRemark")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultRemark = FValue.trim();
            }
            else
                ResultRemark = null;
        }
        if (FCode.equalsIgnoreCase("UploadCount")) {
            if( FValue != null && !FValue.equals(""))
            {
                UploadCount = FValue.trim();
            }
            else
                UploadCount = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag1 = FValue.trim();
            }
            else
                StandByfFag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag2 = FValue.trim();
            }
            else
                StandByfFag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag3 = FValue.trim();
            }
            else
                StandByfFag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag4 = FValue.trim();
            }
            else
                StandByfFag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByfFag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByfFag5 = FValue.trim();
            }
            else
                StandByfFag5 = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCImageUploadInfoSchema other = (LCImageUploadInfoSchema)otherObject;
        return
            TransNo.equals(other.getTransNo())
            && ContNo.equals(other.getContNo())
            && AppCode.equals(other.getAppCode())
            && DataType.equals(other.getDataType())
            && UploadUrl.equals(other.getUploadUrl())
            && UploadState.equals(other.getUploadState())
            && ResultRemark.equals(other.getResultRemark())
            && UploadCount.equals(other.getUploadCount())
            && StandByfFag1.equals(other.getStandByfFag1())
            && StandByfFag2.equals(other.getStandByfFag2())
            && StandByfFag3.equals(other.getStandByfFag3())
            && StandByfFag4.equals(other.getStandByfFag4())
            && StandByfFag5.equals(other.getStandByfFag5())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TransNo") ) {
            return 0;
        }
        if( strFieldName.equals("ContNo") ) {
            return 1;
        }
        if( strFieldName.equals("AppCode") ) {
            return 2;
        }
        if( strFieldName.equals("DataType") ) {
            return 3;
        }
        if( strFieldName.equals("UploadUrl") ) {
            return 4;
        }
        if( strFieldName.equals("UploadState") ) {
            return 5;
        }
        if( strFieldName.equals("ResultRemark") ) {
            return 6;
        }
        if( strFieldName.equals("UploadCount") ) {
            return 7;
        }
        if( strFieldName.equals("StandByfFag1") ) {
            return 8;
        }
        if( strFieldName.equals("StandByfFag2") ) {
            return 9;
        }
        if( strFieldName.equals("StandByfFag3") ) {
            return 10;
        }
        if( strFieldName.equals("StandByfFag4") ) {
            return 11;
        }
        if( strFieldName.equals("StandByfFag5") ) {
            return 12;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 13;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 16;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TransNo";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "AppCode";
                break;
            case 3:
                strFieldName = "DataType";
                break;
            case 4:
                strFieldName = "UploadUrl";
                break;
            case 5:
                strFieldName = "UploadState";
                break;
            case 6:
                strFieldName = "ResultRemark";
                break;
            case 7:
                strFieldName = "UploadCount";
                break;
            case 8:
                strFieldName = "StandByfFag1";
                break;
            case 9:
                strFieldName = "StandByfFag2";
                break;
            case 10:
                strFieldName = "StandByfFag3";
                break;
            case 11:
                strFieldName = "StandByfFag4";
                break;
            case 12:
                strFieldName = "StandByfFag5";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "APPCODE":
                return Schema.TYPE_STRING;
            case "DATATYPE":
                return Schema.TYPE_STRING;
            case "UPLOADURL":
                return Schema.TYPE_STRING;
            case "UPLOADSTATE":
                return Schema.TYPE_STRING;
            case "RESULTREMARK":
                return Schema.TYPE_STRING;
            case "UPLOADCOUNT":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFFAG5":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
