/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: VMS_OUTPUT_LOGPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class VMS_OUTPUT_LOGPojo implements Pojo,Serializable {
    // @Field
    /** 序列号 */
    private String SERIALNO; 
    /** 服务类型 */
    private String SERVICETYPE; 
    /** 批次号 */
    private String BATCHNO; 
    /** 交易编码 */
    private String BUSSNO; 
    /** 创建日期 */
    private String MAKEDATE; 
    /** 创建时间 */
    private String MAKETIME; 


    public static final int FIELDNUM = 6;    // 数据库表的字段个数
    public String getSERIALNO() {
        return SERIALNO;
    }
    public void setSERIALNO(String aSERIALNO) {
        SERIALNO = aSERIALNO;
    }
    public String getSERVICETYPE() {
        return SERVICETYPE;
    }
    public void setSERVICETYPE(String aSERVICETYPE) {
        SERVICETYPE = aSERVICETYPE;
    }
    public String getBATCHNO() {
        return BATCHNO;
    }
    public void setBATCHNO(String aBATCHNO) {
        BATCHNO = aBATCHNO;
    }
    public String getBUSSNO() {
        return BUSSNO;
    }
    public void setBUSSNO(String aBUSSNO) {
        BUSSNO = aBUSSNO;
    }
    public String getMAKEDATE() {
        return MAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SERIALNO") ) {
            return 0;
        }
        if( strFieldName.equals("SERVICETYPE") ) {
            return 1;
        }
        if( strFieldName.equals("BATCHNO") ) {
            return 2;
        }
        if( strFieldName.equals("BUSSNO") ) {
            return 3;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 4;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 5;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SERIALNO";
                break;
            case 1:
                strFieldName = "SERVICETYPE";
                break;
            case 2:
                strFieldName = "BATCHNO";
                break;
            case 3:
                strFieldName = "BUSSNO";
                break;
            case 4:
                strFieldName = "MAKEDATE";
                break;
            case 5:
                strFieldName = "MAKETIME";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "SERVICETYPE":
                return Schema.TYPE_STRING;
            case "BATCHNO":
                return Schema.TYPE_STRING;
            case "BUSSNO":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SERIALNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SERIALNO));
        }
        if (FCode.equalsIgnoreCase("SERVICETYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SERVICETYPE));
        }
        if (FCode.equalsIgnoreCase("BATCHNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BATCHNO));
        }
        if (FCode.equalsIgnoreCase("BUSSNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BUSSNO));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKEDATE));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SERIALNO);
                break;
            case 1:
                strFieldValue = String.valueOf(SERVICETYPE);
                break;
            case 2:
                strFieldValue = String.valueOf(BATCHNO);
                break;
            case 3:
                strFieldValue = String.valueOf(BUSSNO);
                break;
            case 4:
                strFieldValue = String.valueOf(MAKEDATE);
                break;
            case 5:
                strFieldValue = String.valueOf(MAKETIME);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SERIALNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SERIALNO = FValue.trim();
            }
            else
                SERIALNO = null;
        }
        if (FCode.equalsIgnoreCase("SERVICETYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                SERVICETYPE = FValue.trim();
            }
            else
                SERVICETYPE = null;
        }
        if (FCode.equalsIgnoreCase("BATCHNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                BATCHNO = FValue.trim();
            }
            else
                BATCHNO = null;
        }
        if (FCode.equalsIgnoreCase("BUSSNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                BUSSNO = FValue.trim();
            }
            else
                BUSSNO = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKEDATE = FValue.trim();
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        return true;
    }


    public String toString() {
    return "VMS_OUTPUT_LOGPojo [" +
            "SERIALNO="+SERIALNO +
            ", SERVICETYPE="+SERVICETYPE +
            ", BATCHNO="+BATCHNO +
            ", BUSSNO="+BUSSNO +
            ", MAKEDATE="+MAKEDATE +
            ", MAKETIME="+MAKETIME +"]";
    }
}
