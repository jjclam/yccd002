/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMFactorConfigPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-24
 */
public class LMFactorConfigPojo implements Pojo,Serializable {
    // @Field
    /** 要素编码 */
    @RedisPrimaryHKey
    private String FactorCode; 
    /** 要素名称 */
    private String FactorName; 
    /** 字段编码 */
    private String FieldCode; 
    /** 字段类型 */
    private String FieldType; 
    /** 值类型 */
    private String ValueType; 
    /** 值长度 */
    private String ValueLength; 
    /** 值范围 */
    private String ValueScope; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getFactorCode() {
        return FactorCode;
    }
    public void setFactorCode(String aFactorCode) {
        FactorCode = aFactorCode;
    }
    public String getFactorName() {
        return FactorName;
    }
    public void setFactorName(String aFactorName) {
        FactorName = aFactorName;
    }
    public String getFieldCode() {
        return FieldCode;
    }
    public void setFieldCode(String aFieldCode) {
        FieldCode = aFieldCode;
    }
    public String getFieldType() {
        return FieldType;
    }
    public void setFieldType(String aFieldType) {
        FieldType = aFieldType;
    }
    public String getValueType() {
        return ValueType;
    }
    public void setValueType(String aValueType) {
        ValueType = aValueType;
    }
    public String getValueLength() {
        return ValueLength;
    }
    public void setValueLength(String aValueLength) {
        ValueLength = aValueLength;
    }
    public String getValueScope() {
        return ValueScope;
    }
    public void setValueScope(String aValueScope) {
        ValueScope = aValueScope;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("FactorCode") ) {
            return 0;
        }
        if( strFieldName.equals("FactorName") ) {
            return 1;
        }
        if( strFieldName.equals("FieldCode") ) {
            return 2;
        }
        if( strFieldName.equals("FieldType") ) {
            return 3;
        }
        if( strFieldName.equals("ValueType") ) {
            return 4;
        }
        if( strFieldName.equals("ValueLength") ) {
            return 5;
        }
        if( strFieldName.equals("ValueScope") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "FactorCode";
                break;
            case 1:
                strFieldName = "FactorName";
                break;
            case 2:
                strFieldName = "FieldCode";
                break;
            case 3:
                strFieldName = "FieldType";
                break;
            case 4:
                strFieldName = "ValueType";
                break;
            case 5:
                strFieldName = "ValueLength";
                break;
            case 6:
                strFieldName = "ValueScope";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "FACTORCODE":
                return Schema.TYPE_STRING;
            case "FACTORNAME":
                return Schema.TYPE_STRING;
            case "FIELDCODE":
                return Schema.TYPE_STRING;
            case "FIELDTYPE":
                return Schema.TYPE_STRING;
            case "VALUETYPE":
                return Schema.TYPE_STRING;
            case "VALUELENGTH":
                return Schema.TYPE_STRING;
            case "VALUESCOPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("FactorCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorCode));
        }
        if (FCode.equalsIgnoreCase("FactorName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorName));
        }
        if (FCode.equalsIgnoreCase("FieldCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FieldCode));
        }
        if (FCode.equalsIgnoreCase("FieldType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FieldType));
        }
        if (FCode.equalsIgnoreCase("ValueType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValueType));
        }
        if (FCode.equalsIgnoreCase("ValueLength")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValueLength));
        }
        if (FCode.equalsIgnoreCase("ValueScope")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValueScope));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(FactorCode);
                break;
            case 1:
                strFieldValue = String.valueOf(FactorName);
                break;
            case 2:
                strFieldValue = String.valueOf(FieldCode);
                break;
            case 3:
                strFieldValue = String.valueOf(FieldType);
                break;
            case 4:
                strFieldValue = String.valueOf(ValueType);
                break;
            case 5:
                strFieldValue = String.valueOf(ValueLength);
                break;
            case 6:
                strFieldValue = String.valueOf(ValueScope);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("FactorCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorCode = FValue.trim();
            }
            else
                FactorCode = null;
        }
        if (FCode.equalsIgnoreCase("FactorName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorName = FValue.trim();
            }
            else
                FactorName = null;
        }
        if (FCode.equalsIgnoreCase("FieldCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FieldCode = FValue.trim();
            }
            else
                FieldCode = null;
        }
        if (FCode.equalsIgnoreCase("FieldType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FieldType = FValue.trim();
            }
            else
                FieldType = null;
        }
        if (FCode.equalsIgnoreCase("ValueType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValueType = FValue.trim();
            }
            else
                ValueType = null;
        }
        if (FCode.equalsIgnoreCase("ValueLength")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValueLength = FValue.trim();
            }
            else
                ValueLength = null;
        }
        if (FCode.equalsIgnoreCase("ValueScope")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValueScope = FValue.trim();
            }
            else
                ValueScope = null;
        }
        return true;
    }


    public String toString() {
    return "LMFactorConfigPojo [" +
            "FactorCode="+FactorCode +
            ", FactorName="+FactorName +
            ", FieldCode="+FieldCode +
            ", FieldType="+FieldType +
            ", ValueType="+ValueType +
            ", ValueLength="+ValueLength +
            ", ValueScope="+ValueScope +"]";
    }
}
