/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAAgentStatePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LAAgentStatePojo implements Pojo,Serializable {
    // @Field
    /** 代理人号 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String AgentCode; 
    /** 代理人职级 */
    private String AgentGrade; 
    /** 管理机构 */
    private String ManageCom; 
    /** 展业类型 */
    private String BranchType; 
    /** 渠道类型 */
    private String BranchType2; 
    /** 状态类型 */
    @RedisPrimaryHKey
    private String StateType; 
    /** 状态值 */
    private String StateValue; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 备注1 */
    private String Note1; 
    /** 备注2 */
    private String Note2; 
    /** 备注3 */
    private String Note3; 


    public static final int FIELDNUM = 15;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGrade() {
        return AgentGrade;
    }
    public void setAgentGrade(String aAgentGrade) {
        AgentGrade = aAgentGrade;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }
    public String getStateType() {
        return StateType;
    }
    public void setStateType(String aStateType) {
        StateType = aStateType;
    }
    public String getStateValue() {
        return StateValue;
    }
    public void setStateValue(String aStateValue) {
        StateValue = aStateValue;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getNote1() {
        return Note1;
    }
    public void setNote1(String aNote1) {
        Note1 = aNote1;
    }
    public String getNote2() {
        return Note2;
    }
    public void setNote2(String aNote2) {
        Note2 = aNote2;
    }
    public String getNote3() {
        return Note3;
    }
    public void setNote3(String aNote3) {
        Note3 = aNote3;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCode") ) {
            return 0;
        }
        if( strFieldName.equals("AgentGrade") ) {
            return 1;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 2;
        }
        if( strFieldName.equals("BranchType") ) {
            return 3;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 4;
        }
        if( strFieldName.equals("StateType") ) {
            return 5;
        }
        if( strFieldName.equals("StateValue") ) {
            return 6;
        }
        if( strFieldName.equals("Operator") ) {
            return 7;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 8;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 11;
        }
        if( strFieldName.equals("Note1") ) {
            return 12;
        }
        if( strFieldName.equals("Note2") ) {
            return 13;
        }
        if( strFieldName.equals("Note3") ) {
            return 14;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "AgentGrade";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "BranchType";
                break;
            case 4:
                strFieldName = "BranchType2";
                break;
            case 5:
                strFieldName = "StateType";
                break;
            case 6:
                strFieldName = "StateValue";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "Note1";
                break;
            case 13:
                strFieldName = "Note2";
                break;
            case 14:
                strFieldName = "Note3";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGRADE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "STATETYPE":
                return Schema.TYPE_STRING;
            case "STATEVALUE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "NOTE1":
                return Schema.TYPE_STRING;
            case "NOTE2":
                return Schema.TYPE_STRING;
            case "NOTE3":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateType));
        }
        if (FCode.equalsIgnoreCase("StateValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Note1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Note1));
        }
        if (FCode.equalsIgnoreCase("Note2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Note2));
        }
        if (FCode.equalsIgnoreCase("Note3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Note3));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 1:
                strFieldValue = String.valueOf(AgentGrade);
                break;
            case 2:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 3:
                strFieldValue = String.valueOf(BranchType);
                break;
            case 4:
                strFieldValue = String.valueOf(BranchType2);
                break;
            case 5:
                strFieldValue = String.valueOf(StateType);
                break;
            case 6:
                strFieldValue = String.valueOf(StateValue);
                break;
            case 7:
                strFieldValue = String.valueOf(Operator);
                break;
            case 8:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 9:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 10:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 12:
                strFieldValue = String.valueOf(Note1);
                break;
            case 13:
                strFieldValue = String.valueOf(Note2);
                break;
            case 14:
                strFieldValue = String.valueOf(Note3);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
                AgentGrade = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateType = FValue.trim();
            }
            else
                StateType = null;
        }
        if (FCode.equalsIgnoreCase("StateValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateValue = FValue.trim();
            }
            else
                StateValue = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Note1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Note1 = FValue.trim();
            }
            else
                Note1 = null;
        }
        if (FCode.equalsIgnoreCase("Note2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Note2 = FValue.trim();
            }
            else
                Note2 = null;
        }
        if (FCode.equalsIgnoreCase("Note3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Note3 = FValue.trim();
            }
            else
                Note3 = null;
        }
        return true;
    }


    public String toString() {
    return "LAAgentStatePojo [" +
            "AgentCode="+AgentCode +
            ", AgentGrade="+AgentGrade +
            ", ManageCom="+ManageCom +
            ", BranchType="+BranchType +
            ", BranchType2="+BranchType2 +
            ", StateType="+StateType +
            ", StateValue="+StateValue +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Note1="+Note1 +
            ", Note2="+Note2 +
            ", Note3="+Note3 +"]";
    }
}
