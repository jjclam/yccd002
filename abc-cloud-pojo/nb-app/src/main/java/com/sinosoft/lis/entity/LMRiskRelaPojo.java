/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskRelaPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskRelaPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 关联险种编码 */
    @RedisIndexHKey
    private String RelaRiskCode; 
    /** 险种之间的关系 */
    private String RelaCode; 
    /** 管理机构 */
    private String ManageComGrp; 
    /** 代扣附加险保费标志 */
    private String DKSubPrem; 


    public static final int FIELDNUM = 5;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRelaRiskCode() {
        return RelaRiskCode;
    }
    public void setRelaRiskCode(String aRelaRiskCode) {
        RelaRiskCode = aRelaRiskCode;
    }
    public String getRelaCode() {
        return RelaCode;
    }
    public void setRelaCode(String aRelaCode) {
        RelaCode = aRelaCode;
    }
    public String getManageComGrp() {
        return ManageComGrp;
    }
    public void setManageComGrp(String aManageComGrp) {
        ManageComGrp = aManageComGrp;
    }
    public String getDKSubPrem() {
        return DKSubPrem;
    }
    public void setDKSubPrem(String aDKSubPrem) {
        DKSubPrem = aDKSubPrem;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RelaRiskCode") ) {
            return 1;
        }
        if( strFieldName.equals("RelaCode") ) {
            return 2;
        }
        if( strFieldName.equals("ManageComGrp") ) {
            return 3;
        }
        if( strFieldName.equals("DKSubPrem") ) {
            return 4;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RelaRiskCode";
                break;
            case 2:
                strFieldName = "RelaCode";
                break;
            case 3:
                strFieldName = "ManageComGrp";
                break;
            case 4:
                strFieldName = "DKSubPrem";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RELARISKCODE":
                return Schema.TYPE_STRING;
            case "RELACODE":
                return Schema.TYPE_STRING;
            case "MANAGECOMGRP":
                return Schema.TYPE_STRING;
            case "DKSUBPREM":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RelaRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaRiskCode));
        }
        if (FCode.equalsIgnoreCase("RelaCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaCode));
        }
        if (FCode.equalsIgnoreCase("ManageComGrp")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageComGrp));
        }
        if (FCode.equalsIgnoreCase("DKSubPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DKSubPrem));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RelaRiskCode);
                break;
            case 2:
                strFieldValue = String.valueOf(RelaCode);
                break;
            case 3:
                strFieldValue = String.valueOf(ManageComGrp);
                break;
            case 4:
                strFieldValue = String.valueOf(DKSubPrem);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RelaRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelaRiskCode = FValue.trim();
            }
            else
                RelaRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RelaCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelaCode = FValue.trim();
            }
            else
                RelaCode = null;
        }
        if (FCode.equalsIgnoreCase("ManageComGrp")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageComGrp = FValue.trim();
            }
            else
                ManageComGrp = null;
        }
        if (FCode.equalsIgnoreCase("DKSubPrem")) {
            if( FValue != null && !FValue.equals(""))
            {
                DKSubPrem = FValue.trim();
            }
            else
                DKSubPrem = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskRelaPojo [" +
            "RiskCode="+RiskCode +
            ", RelaRiskCode="+RelaRiskCode +
            ", RelaCode="+RelaCode +
            ", ManageComGrp="+ManageComGrp +
            ", DKSubPrem="+DKSubPrem +"]";
    }
}
