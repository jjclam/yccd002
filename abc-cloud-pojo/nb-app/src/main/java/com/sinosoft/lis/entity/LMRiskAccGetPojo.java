/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskAccGetPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskAccGetPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 给付责任编码 */
    @RedisPrimaryHKey
    private String GetDutyCode; 
    /** 默认比例 */
    private double DefaultRate; 
    /** 是否需要录入 */
    private String NeedInput; 
    /** 转出账户时的算法编码(现金) */
    private String CalCodeMoney; 
    /** 处理方向 */
    private String DealDirection; 
    /** 账户转出计算标志 */
    private String CalFlag; 
    /** 账户产生位置 */
    private String AccCreatePos; 
    /** 给付名称 */
    private String GetDutyName; 


    public static final int FIELDNUM = 11;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public double getDefaultRate() {
        return DefaultRate;
    }
    public void setDefaultRate(double aDefaultRate) {
        DefaultRate = aDefaultRate;
    }
    public void setDefaultRate(String aDefaultRate) {
        if (aDefaultRate != null && !aDefaultRate.equals("")) {
            Double tDouble = new Double(aDefaultRate);
            double d = tDouble.doubleValue();
            DefaultRate = d;
        }
    }

    public String getNeedInput() {
        return NeedInput;
    }
    public void setNeedInput(String aNeedInput) {
        NeedInput = aNeedInput;
    }
    public String getCalCodeMoney() {
        return CalCodeMoney;
    }
    public void setCalCodeMoney(String aCalCodeMoney) {
        CalCodeMoney = aCalCodeMoney;
    }
    public String getDealDirection() {
        return DealDirection;
    }
    public void setDealDirection(String aDealDirection) {
        DealDirection = aDealDirection;
    }
    public String getCalFlag() {
        return CalFlag;
    }
    public void setCalFlag(String aCalFlag) {
        CalFlag = aCalFlag;
    }
    public String getAccCreatePos() {
        return AccCreatePos;
    }
    public void setAccCreatePos(String aAccCreatePos) {
        AccCreatePos = aAccCreatePos;
    }
    public String getGetDutyName() {
        return GetDutyName;
    }
    public void setGetDutyName(String aGetDutyName) {
        GetDutyName = aGetDutyName;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 2;
        }
        if( strFieldName.equals("GetDutyCode") ) {
            return 3;
        }
        if( strFieldName.equals("DefaultRate") ) {
            return 4;
        }
        if( strFieldName.equals("NeedInput") ) {
            return 5;
        }
        if( strFieldName.equals("CalCodeMoney") ) {
            return 6;
        }
        if( strFieldName.equals("DealDirection") ) {
            return 7;
        }
        if( strFieldName.equals("CalFlag") ) {
            return 8;
        }
        if( strFieldName.equals("AccCreatePos") ) {
            return 9;
        }
        if( strFieldName.equals("GetDutyName") ) {
            return 10;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "InsuAccNo";
                break;
            case 3:
                strFieldName = "GetDutyCode";
                break;
            case 4:
                strFieldName = "DefaultRate";
                break;
            case 5:
                strFieldName = "NeedInput";
                break;
            case 6:
                strFieldName = "CalCodeMoney";
                break;
            case 7:
                strFieldName = "DealDirection";
                break;
            case 8:
                strFieldName = "CalFlag";
                break;
            case 9:
                strFieldName = "AccCreatePos";
                break;
            case 10:
                strFieldName = "GetDutyName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "DEFAULTRATE":
                return Schema.TYPE_DOUBLE;
            case "NEEDINPUT":
                return Schema.TYPE_STRING;
            case "CALCODEMONEY":
                return Schema.TYPE_STRING;
            case "DEALDIRECTION":
                return Schema.TYPE_STRING;
            case "CALFLAG":
                return Schema.TYPE_STRING;
            case "ACCCREATEPOS":
                return Schema.TYPE_STRING;
            case "GETDUTYNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("DefaultRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultRate));
        }
        if (FCode.equalsIgnoreCase("NeedInput")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedInput));
        }
        if (FCode.equalsIgnoreCase("CalCodeMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCodeMoney));
        }
        if (FCode.equalsIgnoreCase("DealDirection")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DealDirection));
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
        }
        if (FCode.equalsIgnoreCase("AccCreatePos")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCreatePos));
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 3:
                strFieldValue = String.valueOf(GetDutyCode);
                break;
            case 4:
                strFieldValue = String.valueOf(DefaultRate);
                break;
            case 5:
                strFieldValue = String.valueOf(NeedInput);
                break;
            case 6:
                strFieldValue = String.valueOf(CalCodeMoney);
                break;
            case 7:
                strFieldValue = String.valueOf(DealDirection);
                break;
            case 8:
                strFieldValue = String.valueOf(CalFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(AccCreatePos);
                break;
            case 10:
                strFieldValue = String.valueOf(GetDutyName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("DefaultRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                DefaultRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("NeedInput")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedInput = FValue.trim();
            }
            else
                NeedInput = null;
        }
        if (FCode.equalsIgnoreCase("CalCodeMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCodeMoney = FValue.trim();
            }
            else
                CalCodeMoney = null;
        }
        if (FCode.equalsIgnoreCase("DealDirection")) {
            if( FValue != null && !FValue.equals(""))
            {
                DealDirection = FValue.trim();
            }
            else
                DealDirection = null;
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFlag = FValue.trim();
            }
            else
                CalFlag = null;
        }
        if (FCode.equalsIgnoreCase("AccCreatePos")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCreatePos = FValue.trim();
            }
            else
                AccCreatePos = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
                GetDutyName = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskAccGetPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", InsuAccNo="+InsuAccNo +
            ", GetDutyCode="+GetDutyCode +
            ", DefaultRate="+DefaultRate +
            ", NeedInput="+NeedInput +
            ", CalCodeMoney="+CalCodeMoney +
            ", DealDirection="+DealDirection +
            ", CalFlag="+CalFlag +
            ", AccCreatePos="+AccCreatePos +
            ", GetDutyName="+GetDutyName +"]";
    }
}
