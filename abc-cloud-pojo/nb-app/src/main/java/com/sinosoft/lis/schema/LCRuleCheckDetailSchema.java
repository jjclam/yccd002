/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCRuleCheckDetailDB;

/**
 * <p>ClassName: LCRuleCheckDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-05-16
 */
public class LCRuleCheckDetailSchema implements Schema, Cloneable {
    // @Field
    /** 规则代码 */
    private String RuleCode;
    /** 规则描述 */
    private String RuleDes;
    /** 操作员 */
    private String Operator;

    public static final int FIELDNUM = 3;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCRuleCheckDetailSchema() {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCRuleCheckDetailSchema cloned = (LCRuleCheckDetailSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRuleCode() {
        return RuleCode;
    }
    public void setRuleCode(String aRuleCode) {
        RuleCode = aRuleCode;
    }
    public String getRuleDes() {
        return RuleDes;
    }
    public void setRuleDes(String aRuleDes) {
        RuleDes = aRuleDes;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    /**
    * 使用另外一个 LCRuleCheckDetailSchema 对象给 Schema 赋值
    * @param: aLCRuleCheckDetailSchema LCRuleCheckDetailSchema
    **/
    public void setSchema(LCRuleCheckDetailSchema aLCRuleCheckDetailSchema) {
        this.RuleCode = aLCRuleCheckDetailSchema.getRuleCode();
        this.RuleDes = aLCRuleCheckDetailSchema.getRuleDes();
        this.Operator = aLCRuleCheckDetailSchema.getOperator();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RuleCode") == null )
                this.RuleCode = null;
            else
                this.RuleCode = rs.getString("RuleCode").trim();

            if( rs.getString("RuleDes") == null )
                this.RuleDes = null;
            else
                this.RuleDes = rs.getString("RuleDes").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCRuleCheckDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCRuleCheckDetailSchema getSchema() {
        LCRuleCheckDetailSchema aLCRuleCheckDetailSchema = new LCRuleCheckDetailSchema();
        aLCRuleCheckDetailSchema.setSchema(this);
        return aLCRuleCheckDetailSchema;
    }

    public LCRuleCheckDetailDB getDB() {
        LCRuleCheckDetailDB aDBOper = new LCRuleCheckDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCRuleCheckDetail描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RuleCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RuleDes)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCRuleCheckDetail>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RuleDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCRuleCheckDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RuleCode));
        }
        if (FCode.equalsIgnoreCase("RuleDes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RuleDes));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RuleCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RuleDes);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RuleCode = FValue.trim();
            }
            else
                RuleCode = null;
        }
        if (FCode.equalsIgnoreCase("RuleDes")) {
            if( FValue != null && !FValue.equals(""))
            {
                RuleDes = FValue.trim();
            }
            else
                RuleDes = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCRuleCheckDetailSchema other = (LCRuleCheckDetailSchema)otherObject;
        return
            RuleCode.equals(other.getRuleCode())
            && RuleDes.equals(other.getRuleDes())
            && Operator.equals(other.getOperator());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RuleCode") ) {
            return 0;
        }
        if( strFieldName.equals("RuleDes") ) {
            return 1;
        }
        if( strFieldName.equals("Operator") ) {
            return 2;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RuleCode";
                break;
            case 1:
                strFieldName = "RuleDes";
                break;
            case 2:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RULECODE":
                return Schema.TYPE_STRING;
            case "RULEDES":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "LCRuleCheckDetailSchema {" +
            "RuleCode="+RuleCode +
            ", RuleDes="+RuleDes +
            ", Operator="+Operator +"}";
    }
}
