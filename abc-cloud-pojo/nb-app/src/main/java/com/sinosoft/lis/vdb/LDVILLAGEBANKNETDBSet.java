/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LDVILLAGEBANKNETSchema;
import com.sinosoft.lis.vschema.LDVILLAGEBANKNETSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LDVILLAGEBANKNETDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-01-09
 */
public class LDVILLAGEBANKNETDBSet extends LDVILLAGEBANKNETSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LDVILLAGEBANKNETDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LDVILLAGEBANKNET");
        mflag = true;
    }

    public LDVILLAGEBANKNETDBSet() {
        db = new DBOper( "LDVILLAGEBANKNET" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDVILLAGEBANKNETDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LDVILLAGEBANKNET WHERE  1=1  AND BANK_BRCHNET_CODE = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBANK_BRCHNET_CODE() == null || this.get(i).getBANK_BRCHNET_CODE().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBANK_BRCHNET_CODE());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDVILLAGEBANKNETDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LDVILLAGEBANKNET SET  BANK_BRCHNET_CODE = ? , BANK_BRCHNET_NAME = ? , BANK_INSIDE_CODE = ? , AGENTCODE = ? , AGENCY_BRCHNET_CODE = ? , AGENCYCODE = ? , MAKEDATE = ? , MAKETIME = ? , MODIFYDATE = ? , MODIFYTIME = ? WHERE  1=1  AND BANK_BRCHNET_CODE = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBANK_BRCHNET_CODE() == null || this.get(i).getBANK_BRCHNET_CODE().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBANK_BRCHNET_CODE());
            }
            if(this.get(i).getBANK_BRCHNET_NAME() == null || this.get(i).getBANK_BRCHNET_NAME().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getBANK_BRCHNET_NAME());
            }
            if(this.get(i).getBANK_INSIDE_CODE() == null || this.get(i).getBANK_INSIDE_CODE().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBANK_INSIDE_CODE());
            }
            if(this.get(i).getAGENTCODE() == null || this.get(i).getAGENTCODE().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAGENTCODE());
            }
            if(this.get(i).getAGENCY_BRCHNET_CODE() == null || this.get(i).getAGENCY_BRCHNET_CODE().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAGENCY_BRCHNET_CODE());
            }
            if(this.get(i).getAGENCYCODE() == null || this.get(i).getAGENCYCODE().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getAGENCYCODE());
            }
            if(this.get(i).getMAKEDATE() == null || this.get(i).getMAKEDATE().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getMAKEDATE()));
            }
            if(this.get(i).getMAKETIME() == null || this.get(i).getMAKETIME().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getMAKETIME());
            }
            if(this.get(i).getMODIFYDATE() == null || this.get(i).getMODIFYDATE().equals("null")) {
                pstmt.setDate(9,null);
            } else {
                pstmt.setDate(9, Date.valueOf(this.get(i).getMODIFYDATE()));
            }
            if(this.get(i).getMODIFYTIME() == null || this.get(i).getMODIFYTIME().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getMODIFYTIME());
            }
            // set where condition
            if(this.get(i).getBANK_BRCHNET_CODE() == null || this.get(i).getBANK_BRCHNET_CODE().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getBANK_BRCHNET_CODE());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDVILLAGEBANKNETDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LDVILLAGEBANKNET VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBANK_BRCHNET_CODE() == null || this.get(i).getBANK_BRCHNET_CODE().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBANK_BRCHNET_CODE());
            }
            if(this.get(i).getBANK_BRCHNET_NAME() == null || this.get(i).getBANK_BRCHNET_NAME().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getBANK_BRCHNET_NAME());
            }
            if(this.get(i).getBANK_INSIDE_CODE() == null || this.get(i).getBANK_INSIDE_CODE().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBANK_INSIDE_CODE());
            }
            if(this.get(i).getAGENTCODE() == null || this.get(i).getAGENTCODE().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAGENTCODE());
            }
            if(this.get(i).getAGENCY_BRCHNET_CODE() == null || this.get(i).getAGENCY_BRCHNET_CODE().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAGENCY_BRCHNET_CODE());
            }
            if(this.get(i).getAGENCYCODE() == null || this.get(i).getAGENCYCODE().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getAGENCYCODE());
            }
            if(this.get(i).getMAKEDATE() == null || this.get(i).getMAKEDATE().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getMAKEDATE()));
            }
            if(this.get(i).getMAKETIME() == null || this.get(i).getMAKETIME().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getMAKETIME());
            }
            if(this.get(i).getMODIFYDATE() == null || this.get(i).getMODIFYDATE().equals("null")) {
                pstmt.setDate(9,null);
            } else {
                pstmt.setDate(9, Date.valueOf(this.get(i).getMODIFYDATE()));
            }
            if(this.get(i).getMODIFYTIME() == null || this.get(i).getMODIFYTIME().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getMODIFYTIME());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDVILLAGEBANKNETDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
