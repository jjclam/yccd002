/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMRiskPAChkDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LMRiskPAChkSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-06
 */
public class LMRiskPAChkSchema implements Schema, Cloneable {
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 销售渠道 */
    private String Salechnl;
    /** 销售类型 */
    private String Selltype;
    /** 保费校验标志 */
    private String PremFlag;
    /** 保额校验标志 */
    private String AmntFlag;
    /** 备用字段1 */
    private String StandByString1;
    /** 备用字段2 */
    private String StandByString2;
    /** 备用字段3 */
    private String StandByString3;
    /** 备用字段4 */
    private String StandByString4;
    /** 备用字段5 */
    private String StandByString5;

    public static final int FIELDNUM = 10;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMRiskPAChkSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "RiskCode";
        pk[1] = "Salechnl";
        pk[2] = "Selltype";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMRiskPAChkSchema cloned = (LMRiskPAChkSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getSalechnl() {
        return Salechnl;
    }
    public void setSalechnl(String aSalechnl) {
        Salechnl = aSalechnl;
    }
    public String getSelltype() {
        return Selltype;
    }
    public void setSelltype(String aSelltype) {
        Selltype = aSelltype;
    }
    public String getPremFlag() {
        return PremFlag;
    }
    public void setPremFlag(String aPremFlag) {
        PremFlag = aPremFlag;
    }
    public String getAmntFlag() {
        return AmntFlag;
    }
    public void setAmntFlag(String aAmntFlag) {
        AmntFlag = aAmntFlag;
    }
    public String getStandByString1() {
        return StandByString1;
    }
    public void setStandByString1(String aStandByString1) {
        StandByString1 = aStandByString1;
    }
    public String getStandByString2() {
        return StandByString2;
    }
    public void setStandByString2(String aStandByString2) {
        StandByString2 = aStandByString2;
    }
    public String getStandByString3() {
        return StandByString3;
    }
    public void setStandByString3(String aStandByString3) {
        StandByString3 = aStandByString3;
    }
    public String getStandByString4() {
        return StandByString4;
    }
    public void setStandByString4(String aStandByString4) {
        StandByString4 = aStandByString4;
    }
    public String getStandByString5() {
        return StandByString5;
    }
    public void setStandByString5(String aStandByString5) {
        StandByString5 = aStandByString5;
    }

    /**
    * 使用另外一个 LMRiskPAChkSchema 对象给 Schema 赋值
    * @param: aLMRiskPAChkSchema LMRiskPAChkSchema
    **/
    public void setSchema(LMRiskPAChkSchema aLMRiskPAChkSchema) {
        this.RiskCode = aLMRiskPAChkSchema.getRiskCode();
        this.Salechnl = aLMRiskPAChkSchema.getSalechnl();
        this.Selltype = aLMRiskPAChkSchema.getSelltype();
        this.PremFlag = aLMRiskPAChkSchema.getPremFlag();
        this.AmntFlag = aLMRiskPAChkSchema.getAmntFlag();
        this.StandByString1 = aLMRiskPAChkSchema.getStandByString1();
        this.StandByString2 = aLMRiskPAChkSchema.getStandByString2();
        this.StandByString3 = aLMRiskPAChkSchema.getStandByString3();
        this.StandByString4 = aLMRiskPAChkSchema.getStandByString4();
        this.StandByString5 = aLMRiskPAChkSchema.getStandByString5();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("Salechnl") == null )
                this.Salechnl = null;
            else
                this.Salechnl = rs.getString("Salechnl").trim();

            if( rs.getString("Selltype") == null )
                this.Selltype = null;
            else
                this.Selltype = rs.getString("Selltype").trim();

            if( rs.getString("PremFlag") == null )
                this.PremFlag = null;
            else
                this.PremFlag = rs.getString("PremFlag").trim();

            if( rs.getString("AmntFlag") == null )
                this.AmntFlag = null;
            else
                this.AmntFlag = rs.getString("AmntFlag").trim();

            if( rs.getString("StandByString1") == null )
                this.StandByString1 = null;
            else
                this.StandByString1 = rs.getString("StandByString1").trim();

            if( rs.getString("StandByString2") == null )
                this.StandByString2 = null;
            else
                this.StandByString2 = rs.getString("StandByString2").trim();

            if( rs.getString("StandByString3") == null )
                this.StandByString3 = null;
            else
                this.StandByString3 = rs.getString("StandByString3").trim();

            if( rs.getString("StandByString4") == null )
                this.StandByString4 = null;
            else
                this.StandByString4 = rs.getString("StandByString4").trim();

            if( rs.getString("StandByString5") == null )
                this.StandByString5 = null;
            else
                this.StandByString5 = rs.getString("StandByString5").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskPAChkSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMRiskPAChkSchema getSchema() {
        LMRiskPAChkSchema aLMRiskPAChkSchema = new LMRiskPAChkSchema();
        aLMRiskPAChkSchema.setSchema(this);
        return aLMRiskPAChkSchema;
    }

    public LMRiskPAChkDB getDB() {
        LMRiskPAChkDB aDBOper = new LMRiskPAChkDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskPAChk描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Salechnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Selltype)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PremFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AmntFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByString1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByString2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByString3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByString4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByString5));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskPAChk>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            Salechnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            Selltype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            PremFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AmntFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            StandByString1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            StandByString2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            StandByString3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            StandByString4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            StandByString5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskPAChkSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("Salechnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salechnl));
        }
        if (FCode.equalsIgnoreCase("Selltype")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Selltype));
        }
        if (FCode.equalsIgnoreCase("PremFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremFlag));
        }
        if (FCode.equalsIgnoreCase("AmntFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntFlag));
        }
        if (FCode.equalsIgnoreCase("StandByString1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString1));
        }
        if (FCode.equalsIgnoreCase("StandByString2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString2));
        }
        if (FCode.equalsIgnoreCase("StandByString3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString3));
        }
        if (FCode.equalsIgnoreCase("StandByString4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString4));
        }
        if (FCode.equalsIgnoreCase("StandByString5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(Salechnl);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Selltype);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PremFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AmntFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(StandByString1);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(StandByString2);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(StandByString3);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(StandByString4);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(StandByString5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("Salechnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                Salechnl = FValue.trim();
            }
            else
                Salechnl = null;
        }
        if (FCode.equalsIgnoreCase("Selltype")) {
            if( FValue != null && !FValue.equals(""))
            {
                Selltype = FValue.trim();
            }
            else
                Selltype = null;
        }
        if (FCode.equalsIgnoreCase("PremFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PremFlag = FValue.trim();
            }
            else
                PremFlag = null;
        }
        if (FCode.equalsIgnoreCase("AmntFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntFlag = FValue.trim();
            }
            else
                AmntFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandByString1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString1 = FValue.trim();
            }
            else
                StandByString1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByString2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString2 = FValue.trim();
            }
            else
                StandByString2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByString3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString3 = FValue.trim();
            }
            else
                StandByString3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByString4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString4 = FValue.trim();
            }
            else
                StandByString4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByString5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByString5 = FValue.trim();
            }
            else
                StandByString5 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMRiskPAChkSchema other = (LMRiskPAChkSchema)otherObject;
        return
            RiskCode.equals(other.getRiskCode())
            && Salechnl.equals(other.getSalechnl())
            && Selltype.equals(other.getSelltype())
            && PremFlag.equals(other.getPremFlag())
            && AmntFlag.equals(other.getAmntFlag())
            && StandByString1.equals(other.getStandByString1())
            && StandByString2.equals(other.getStandByString2())
            && StandByString3.equals(other.getStandByString3())
            && StandByString4.equals(other.getStandByString4())
            && StandByString5.equals(other.getStandByString5());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("Salechnl") ) {
            return 1;
        }
        if( strFieldName.equals("Selltype") ) {
            return 2;
        }
        if( strFieldName.equals("PremFlag") ) {
            return 3;
        }
        if( strFieldName.equals("AmntFlag") ) {
            return 4;
        }
        if( strFieldName.equals("StandByString1") ) {
            return 5;
        }
        if( strFieldName.equals("StandByString2") ) {
            return 6;
        }
        if( strFieldName.equals("StandByString3") ) {
            return 7;
        }
        if( strFieldName.equals("StandByString4") ) {
            return 8;
        }
        if( strFieldName.equals("StandByString5") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "Salechnl";
                break;
            case 2:
                strFieldName = "Selltype";
                break;
            case 3:
                strFieldName = "PremFlag";
                break;
            case 4:
                strFieldName = "AmntFlag";
                break;
            case 5:
                strFieldName = "StandByString1";
                break;
            case 6:
                strFieldName = "StandByString2";
                break;
            case 7:
                strFieldName = "StandByString3";
                break;
            case 8:
                strFieldName = "StandByString4";
                break;
            case 9:
                strFieldName = "StandByString5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "PREMFLAG":
                return Schema.TYPE_STRING;
            case "AMNTFLAG":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING1":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING2":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING3":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING4":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
