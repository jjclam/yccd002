/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LBCUWSubSchema;
import com.sinosoft.lis.vschema.LBCUWSubSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LBCUWSubDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBCUWSubDB extends LBCUWSubSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LBCUWSubDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LBCUWSub" );
        mflag = true;
    }

    public LBCUWSubDB() {
        con = null;
        db = new DBOper( "LBCUWSub" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LBCUWSubSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LBCUWSubSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LBCUWSub WHERE  1=1  AND CUWSubID = ?");
            pstmt.setLong(1, this.getCUWSubID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBCUWSub");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LBCUWSub SET  CUWSubID = ? , ShardingID = ? , EdorNo = ? , GrpContNo = ? , ContNo = ? , ProposalContNo = ? , UWNo = ? , InsuredNo = ? , InsuredName = ? , AppntNo = ? , AppntName = ? , AgentCode = ? , AgentGroup = ? , PassFlag = ? , UWGrade = ? , AppGrade = ? , PostponeDay = ? , PostponeDate = ? , AutoUWFlag = ? , State = ? , ManageCom = ? , UWIdea = ? , UpReportContent = ? , Operator = ? , HealthFlag = ? , QuesFlag = ? , SpecFlag = ? , AddPremFlag = ? , AddPremReason = ? , ReportFlag = ? , PrintFlag = ? , PrintFlag2 = ? , ChangePolFlag = ? , ChangePolReason = ? , SpecReason = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , SugPassFlag = ? , SugUWIdea = ? , UpReport = ? , HesiChgDate = ? , HesiChgCount = ? WHERE  1=1  AND CUWSubID = ?");
            pstmt.setLong(1, this.getCUWSubID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getShardingID());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getEdorNo());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getContNo());
            }
            if(this.getProposalContNo() == null || this.getProposalContNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getProposalContNo());
            }
            pstmt.setInt(7, this.getUWNo());
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getInsuredNo());
            }
            if(this.getInsuredName() == null || this.getInsuredName().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getInsuredName());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAppntNo());
            }
            if(this.getAppntName() == null || this.getAppntName().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAppntName());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentGroup());
            }
            if(this.getPassFlag() == null || this.getPassFlag().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getPassFlag());
            }
            if(this.getUWGrade() == null || this.getUWGrade().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getUWGrade());
            }
            if(this.getAppGrade() == null || this.getAppGrade().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAppGrade());
            }
            if(this.getPostponeDay() == null || this.getPostponeDay().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getPostponeDay());
            }
            if(this.getPostponeDate() == null || this.getPostponeDate().equals("null")) {
            	pstmt.setNull(18, 93);
            } else {
            	pstmt.setDate(18, Date.valueOf(this.getPostponeDate()));
            }
            if(this.getAutoUWFlag() == null || this.getAutoUWFlag().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getAutoUWFlag());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getState());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getManageCom());
            }
            if(this.getUWIdea() == null || this.getUWIdea().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getUWIdea());
            }
            if(this.getUpReportContent() == null || this.getUpReportContent().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getUpReportContent());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getOperator());
            }
            if(this.getHealthFlag() == null || this.getHealthFlag().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getHealthFlag());
            }
            if(this.getQuesFlag() == null || this.getQuesFlag().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getQuesFlag());
            }
            if(this.getSpecFlag() == null || this.getSpecFlag().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getSpecFlag());
            }
            if(this.getAddPremFlag() == null || this.getAddPremFlag().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getAddPremFlag());
            }
            if(this.getAddPremReason() == null || this.getAddPremReason().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getAddPremReason());
            }
            if(this.getReportFlag() == null || this.getReportFlag().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getReportFlag());
            }
            if(this.getPrintFlag() == null || this.getPrintFlag().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getPrintFlag());
            }
            if(this.getPrintFlag2() == null || this.getPrintFlag2().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getPrintFlag2());
            }
            if(this.getChangePolFlag() == null || this.getChangePolFlag().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getChangePolFlag());
            }
            if(this.getChangePolReason() == null || this.getChangePolReason().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getChangePolReason());
            }
            if(this.getSpecReason() == null || this.getSpecReason().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getSpecReason());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(36, 93);
            } else {
            	pstmt.setDate(36, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(38, 93);
            } else {
            	pstmt.setDate(38, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getModifyTime());
            }
            if(this.getSugPassFlag() == null || this.getSugPassFlag().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getSugPassFlag());
            }
            if(this.getSugUWIdea() == null || this.getSugUWIdea().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getSugUWIdea());
            }
            if(this.getUpReport() == null || this.getUpReport().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getUpReport());
            }
            if(this.getHesiChgDate() == null || this.getHesiChgDate().equals("null")) {
            	pstmt.setNull(43, 93);
            } else {
            	pstmt.setDate(43, Date.valueOf(this.getHesiChgDate()));
            }
            pstmt.setInt(44, this.getHesiChgCount());
            // set where condition
            pstmt.setLong(45, this.getCUWSubID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBCUWSub");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LBCUWSub VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getCUWSubID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getShardingID());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getEdorNo());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getContNo());
            }
            if(this.getProposalContNo() == null || this.getProposalContNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getProposalContNo());
            }
            pstmt.setInt(7, this.getUWNo());
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getInsuredNo());
            }
            if(this.getInsuredName() == null || this.getInsuredName().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getInsuredName());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAppntNo());
            }
            if(this.getAppntName() == null || this.getAppntName().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAppntName());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentGroup());
            }
            if(this.getPassFlag() == null || this.getPassFlag().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getPassFlag());
            }
            if(this.getUWGrade() == null || this.getUWGrade().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getUWGrade());
            }
            if(this.getAppGrade() == null || this.getAppGrade().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAppGrade());
            }
            if(this.getPostponeDay() == null || this.getPostponeDay().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getPostponeDay());
            }
            if(this.getPostponeDate() == null || this.getPostponeDate().equals("null")) {
            	pstmt.setNull(18, 93);
            } else {
            	pstmt.setDate(18, Date.valueOf(this.getPostponeDate()));
            }
            if(this.getAutoUWFlag() == null || this.getAutoUWFlag().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getAutoUWFlag());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getState());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getManageCom());
            }
            if(this.getUWIdea() == null || this.getUWIdea().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getUWIdea());
            }
            if(this.getUpReportContent() == null || this.getUpReportContent().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getUpReportContent());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getOperator());
            }
            if(this.getHealthFlag() == null || this.getHealthFlag().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getHealthFlag());
            }
            if(this.getQuesFlag() == null || this.getQuesFlag().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getQuesFlag());
            }
            if(this.getSpecFlag() == null || this.getSpecFlag().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getSpecFlag());
            }
            if(this.getAddPremFlag() == null || this.getAddPremFlag().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getAddPremFlag());
            }
            if(this.getAddPremReason() == null || this.getAddPremReason().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getAddPremReason());
            }
            if(this.getReportFlag() == null || this.getReportFlag().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getReportFlag());
            }
            if(this.getPrintFlag() == null || this.getPrintFlag().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getPrintFlag());
            }
            if(this.getPrintFlag2() == null || this.getPrintFlag2().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getPrintFlag2());
            }
            if(this.getChangePolFlag() == null || this.getChangePolFlag().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getChangePolFlag());
            }
            if(this.getChangePolReason() == null || this.getChangePolReason().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getChangePolReason());
            }
            if(this.getSpecReason() == null || this.getSpecReason().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getSpecReason());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(36, 93);
            } else {
            	pstmt.setDate(36, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(38, 93);
            } else {
            	pstmt.setDate(38, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getModifyTime());
            }
            if(this.getSugPassFlag() == null || this.getSugPassFlag().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getSugPassFlag());
            }
            if(this.getSugUWIdea() == null || this.getSugUWIdea().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getSugUWIdea());
            }
            if(this.getUpReport() == null || this.getUpReport().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getUpReport());
            }
            if(this.getHesiChgDate() == null || this.getHesiChgDate().equals("null")) {
            	pstmt.setNull(43, 93);
            } else {
            	pstmt.setDate(43, Date.valueOf(this.getHesiChgDate()));
            }
            pstmt.setInt(44, this.getHesiChgCount());
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LBCUWSub WHERE  1=1  AND CUWSubID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getCUWSubID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBCUWSubDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LBCUWSubSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LBCUWSubSet aLBCUWSubSet = new LBCUWSubSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBCUWSub");
            LBCUWSubSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBCUWSubDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBCUWSubSchema s1 = new LBCUWSubSchema();
                s1.setSchema(rs,i);
                aLBCUWSubSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLBCUWSubSet;
    }

    public LBCUWSubSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LBCUWSubSet aLBCUWSubSet = new LBCUWSubSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBCUWSubDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBCUWSubSchema s1 = new LBCUWSubSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBCUWSubDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBCUWSubSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBCUWSubSet;
    }

    public LBCUWSubSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBCUWSubSet aLBCUWSubSet = new LBCUWSubSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBCUWSub");
            LBCUWSubSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBCUWSubSchema s1 = new LBCUWSubSchema();
                s1.setSchema(rs,i);
                aLBCUWSubSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBCUWSubSet;
    }

    public LBCUWSubSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBCUWSubSet aLBCUWSubSet = new LBCUWSubSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBCUWSubSchema s1 = new LBCUWSubSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBCUWSubDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBCUWSubSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBCUWSubSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBCUWSub");
            LBCUWSubSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LBCUWSub " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LBCUWSubDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LBCUWSubSet
     */
    public LBCUWSubSet getData() {
        int tCount = 0;
        LBCUWSubSet tLBCUWSubSet = new LBCUWSubSet();
        LBCUWSubSchema tLBCUWSubSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLBCUWSubSchema = new LBCUWSubSchema();
            tLBCUWSubSchema.setSchema(mResultSet, 1);
            tLBCUWSubSet.add(tLBCUWSubSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLBCUWSubSchema = new LBCUWSubSchema();
                    tLBCUWSubSchema.setSchema(mResultSet, 1);
                    tLBCUWSubSet.add(tLBCUWSubSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLBCUWSubSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LBCUWSubDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LBCUWSubDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LBCUWSubDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
