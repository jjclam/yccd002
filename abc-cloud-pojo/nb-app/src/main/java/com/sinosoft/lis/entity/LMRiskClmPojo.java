/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskClmPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskClmPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 险种名称 */
    private String RiskName; 
    /** 调查标记 */
    private String SurveyFlag; 
    /** 调查开始位置 */
    private String SurveyStartFlag; 
    /** 控制赔付比例标记 */
    private String ClmRateCtlFlag; 
    /** 退预缴保费标记 */
    private String PrePremFlag; 


    public static final int FIELDNUM = 7;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getSurveyFlag() {
        return SurveyFlag;
    }
    public void setSurveyFlag(String aSurveyFlag) {
        SurveyFlag = aSurveyFlag;
    }
    public String getSurveyStartFlag() {
        return SurveyStartFlag;
    }
    public void setSurveyStartFlag(String aSurveyStartFlag) {
        SurveyStartFlag = aSurveyStartFlag;
    }
    public String getClmRateCtlFlag() {
        return ClmRateCtlFlag;
    }
    public void setClmRateCtlFlag(String aClmRateCtlFlag) {
        ClmRateCtlFlag = aClmRateCtlFlag;
    }
    public String getPrePremFlag() {
        return PrePremFlag;
    }
    public void setPrePremFlag(String aPrePremFlag) {
        PrePremFlag = aPrePremFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("RiskName") ) {
            return 2;
        }
        if( strFieldName.equals("SurveyFlag") ) {
            return 3;
        }
        if( strFieldName.equals("SurveyStartFlag") ) {
            return 4;
        }
        if( strFieldName.equals("ClmRateCtlFlag") ) {
            return 5;
        }
        if( strFieldName.equals("PrePremFlag") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "SurveyFlag";
                break;
            case 4:
                strFieldName = "SurveyStartFlag";
                break;
            case 5:
                strFieldName = "ClmRateCtlFlag";
                break;
            case 6:
                strFieldName = "PrePremFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "SURVEYFLAG":
                return Schema.TYPE_STRING;
            case "SURVEYSTARTFLAG":
                return Schema.TYPE_STRING;
            case "CLMRATECTLFLAG":
                return Schema.TYPE_STRING;
            case "PREPREMFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("SurveyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
        }
        if (FCode.equalsIgnoreCase("SurveyStartFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyStartFlag));
        }
        if (FCode.equalsIgnoreCase("ClmRateCtlFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmRateCtlFlag));
        }
        if (FCode.equalsIgnoreCase("PrePremFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrePremFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 3:
                strFieldValue = String.valueOf(SurveyFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(SurveyStartFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(ClmRateCtlFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(PrePremFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("SurveyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurveyFlag = FValue.trim();
            }
            else
                SurveyFlag = null;
        }
        if (FCode.equalsIgnoreCase("SurveyStartFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurveyStartFlag = FValue.trim();
            }
            else
                SurveyStartFlag = null;
        }
        if (FCode.equalsIgnoreCase("ClmRateCtlFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmRateCtlFlag = FValue.trim();
            }
            else
                ClmRateCtlFlag = null;
        }
        if (FCode.equalsIgnoreCase("PrePremFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrePremFlag = FValue.trim();
            }
            else
                PrePremFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskClmPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", RiskName="+RiskName +
            ", SurveyFlag="+SurveyFlag +
            ", SurveyStartFlag="+SurveyStartFlag +
            ", ClmRateCtlFlag="+ClmRateCtlFlag +
            ", PrePremFlag="+PrePremFlag +"]";
    }
}
