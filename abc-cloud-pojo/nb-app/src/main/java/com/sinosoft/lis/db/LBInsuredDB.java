/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LBInsuredSchema;
import com.sinosoft.lis.vschema.LBInsuredSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LBInsuredDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBInsuredDB extends LBInsuredSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LBInsuredDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LBInsured" );
        mflag = true;
    }

    public LBInsuredDB() {
        con = null;
        db = new DBOper( "LBInsured" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LBInsuredSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LBInsuredSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LBInsured WHERE  1=1  AND InsuredID = ?");
            pstmt.setLong(1, this.getInsuredID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBInsured");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LBInsured SET  InsuredID = ? , ShardingID = ? , EdorNo = ? , GrpContNo = ? , ContNo = ? , InsuredNo = ? , PrtNo = ? , AppntNo = ? , ManageCom = ? , ExecuteCom = ? , FamilyID = ? , RelationToMainInsured = ? , RelationToAppnt = ? , AddressNo = ? , SequenceNo = ? , Name = ? , Sex = ? , Birthday = ? , IDType = ? , IDNo = ? , NativePlace = ? , Nationality = ? , RgtAddress = ? , Marriage = ? , MarriageDate = ? , Health = ? , Stature = ? , Avoirdupois = ? , Degree = ? , CreditGrade = ? , BankCode = ? , BankAccNo = ? , AccName = ? , JoinCompanyDate = ? , StartWorkDate = ? , Position = ? , Salary = ? , OccupationType = ? , OccupationCode = ? , WorkType = ? , PluralityType = ? , SmokeFlag = ? , ContPlanCode = ? , Operator = ? , InsuredStat = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , UWFlag = ? , UWCode = ? , UWDate = ? , UWTime = ? , BMI = ? , InsuredPeoples = ? , License = ? , LicenseType = ? , CustomerSeqNo = ? , WorkNo = ? , SocialInsuNo = ? , OccupationDesb = ? , IdValiDate = ? , HaveMotorcycleLicence = ? , PartTimeJob = ? , HealthFlag = ? , ServiceMark = ? , FirstName = ? , LastName = ? , SSFlag = ? , TINNO = ? , TINFlag = ? , InsuredType = ? , IsLongValid = ? , IDStartDate = ? WHERE  1=1  AND InsuredID = ?");
            pstmt.setLong(1, this.getInsuredID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getShardingID());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getEdorNo());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getContNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getInsuredNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getPrtNo());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAppntNo());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getManageCom());
            }
            if(this.getExecuteCom() == null || this.getExecuteCom().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getExecuteCom());
            }
            if(this.getFamilyID() == null || this.getFamilyID().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getFamilyID());
            }
            if(this.getRelationToMainInsured() == null || this.getRelationToMainInsured().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getRelationToMainInsured());
            }
            if(this.getRelationToAppnt() == null || this.getRelationToAppnt().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getRelationToAppnt());
            }
            if(this.getAddressNo() == null || this.getAddressNo().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAddressNo());
            }
            if(this.getSequenceNo() == null || this.getSequenceNo().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getSequenceNo());
            }
            if(this.getName() == null || this.getName().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getName());
            }
            if(this.getSex() == null || this.getSex().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getSex());
            }
            if(this.getBirthday() == null || this.getBirthday().equals("null")) {
            	pstmt.setNull(18, 93);
            } else {
            	pstmt.setDate(18, Date.valueOf(this.getBirthday()));
            }
            if(this.getIDType() == null || this.getIDType().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getIDType());
            }
            if(this.getIDNo() == null || this.getIDNo().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getIDNo());
            }
            if(this.getNativePlace() == null || this.getNativePlace().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getNativePlace());
            }
            if(this.getNationality() == null || this.getNationality().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getNationality());
            }
            if(this.getRgtAddress() == null || this.getRgtAddress().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getRgtAddress());
            }
            if(this.getMarriage() == null || this.getMarriage().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getMarriage());
            }
            if(this.getMarriageDate() == null || this.getMarriageDate().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getMarriageDate()));
            }
            if(this.getHealth() == null || this.getHealth().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getHealth());
            }
            pstmt.setDouble(27, this.getStature());
            pstmt.setDouble(28, this.getAvoirdupois());
            if(this.getDegree() == null || this.getDegree().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getDegree());
            }
            if(this.getCreditGrade() == null || this.getCreditGrade().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getCreditGrade());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getBankAccNo());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getAccName());
            }
            if(this.getJoinCompanyDate() == null || this.getJoinCompanyDate().equals("null")) {
            	pstmt.setNull(34, 93);
            } else {
            	pstmt.setDate(34, Date.valueOf(this.getJoinCompanyDate()));
            }
            if(this.getStartWorkDate() == null || this.getStartWorkDate().equals("null")) {
            	pstmt.setNull(35, 93);
            } else {
            	pstmt.setDate(35, Date.valueOf(this.getStartWorkDate()));
            }
            if(this.getPosition() == null || this.getPosition().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getPosition());
            }
            pstmt.setDouble(37, this.getSalary());
            if(this.getOccupationType() == null || this.getOccupationType().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getOccupationType());
            }
            if(this.getOccupationCode() == null || this.getOccupationCode().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getOccupationCode());
            }
            if(this.getWorkType() == null || this.getWorkType().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getWorkType());
            }
            if(this.getPluralityType() == null || this.getPluralityType().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getPluralityType());
            }
            if(this.getSmokeFlag() == null || this.getSmokeFlag().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getSmokeFlag());
            }
            if(this.getContPlanCode() == null || this.getContPlanCode().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getContPlanCode());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getOperator());
            }
            if(this.getInsuredStat() == null || this.getInsuredStat().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getInsuredStat());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(46, 93);
            } else {
            	pstmt.setDate(46, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(48, 93);
            } else {
            	pstmt.setDate(48, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getModifyTime());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getUWFlag());
            }
            if(this.getUWCode() == null || this.getUWCode().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getUWCode());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(52, 93);
            } else {
            	pstmt.setDate(52, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(53, 12);
            } else {
            	pstmt.setString(53, this.getUWTime());
            }
            pstmt.setDouble(54, this.getBMI());
            pstmt.setInt(55, this.getInsuredPeoples());
            if(this.getLicense() == null || this.getLicense().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getLicense());
            }
            if(this.getLicenseType() == null || this.getLicenseType().equals("null")) {
            	pstmt.setNull(57, 12);
            } else {
            	pstmt.setString(57, this.getLicenseType());
            }
            pstmt.setInt(58, this.getCustomerSeqNo());
            if(this.getWorkNo() == null || this.getWorkNo().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getWorkNo());
            }
            if(this.getSocialInsuNo() == null || this.getSocialInsuNo().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getSocialInsuNo());
            }
            if(this.getOccupationDesb() == null || this.getOccupationDesb().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getOccupationDesb());
            }
            if(this.getIdValiDate() == null || this.getIdValiDate().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getIdValiDate());
            }
            if(this.getHaveMotorcycleLicence() == null || this.getHaveMotorcycleLicence().equals("null")) {
            	pstmt.setNull(63, 12);
            } else {
            	pstmt.setString(63, this.getHaveMotorcycleLicence());
            }
            if(this.getPartTimeJob() == null || this.getPartTimeJob().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getPartTimeJob());
            }
            if(this.getHealthFlag() == null || this.getHealthFlag().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getHealthFlag());
            }
            if(this.getServiceMark() == null || this.getServiceMark().equals("null")) {
            	pstmt.setNull(66, 12);
            } else {
            	pstmt.setString(66, this.getServiceMark());
            }
            if(this.getFirstName() == null || this.getFirstName().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getFirstName());
            }
            if(this.getLastName() == null || this.getLastName().equals("null")) {
            	pstmt.setNull(68, 12);
            } else {
            	pstmt.setString(68, this.getLastName());
            }
            if(this.getSSFlag() == null || this.getSSFlag().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getSSFlag());
            }
            if(this.getTINNO() == null || this.getTINNO().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getTINNO());
            }
            if(this.getTINFlag() == null || this.getTINFlag().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getTINFlag());
            }
            if(this.getInsuredType() == null || this.getInsuredType().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getInsuredType());
            }
            if(this.getIsLongValid() == null || this.getIsLongValid().equals("null")) {
            	pstmt.setNull(73, 12);
            } else {
            	pstmt.setString(73, this.getIsLongValid());
            }
            if(this.getIDStartDate() == null || this.getIDStartDate().equals("null")) {
            	pstmt.setNull(74, 93);
            } else {
            	pstmt.setDate(74, Date.valueOf(this.getIDStartDate()));
            }
            // set where condition
            pstmt.setLong(75, this.getInsuredID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBInsured");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LBInsured VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getInsuredID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getShardingID());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getEdorNo());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getContNo());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getInsuredNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getPrtNo());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAppntNo());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getManageCom());
            }
            if(this.getExecuteCom() == null || this.getExecuteCom().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getExecuteCom());
            }
            if(this.getFamilyID() == null || this.getFamilyID().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getFamilyID());
            }
            if(this.getRelationToMainInsured() == null || this.getRelationToMainInsured().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getRelationToMainInsured());
            }
            if(this.getRelationToAppnt() == null || this.getRelationToAppnt().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getRelationToAppnt());
            }
            if(this.getAddressNo() == null || this.getAddressNo().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAddressNo());
            }
            if(this.getSequenceNo() == null || this.getSequenceNo().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getSequenceNo());
            }
            if(this.getName() == null || this.getName().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getName());
            }
            if(this.getSex() == null || this.getSex().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getSex());
            }
            if(this.getBirthday() == null || this.getBirthday().equals("null")) {
            	pstmt.setNull(18, 93);
            } else {
            	pstmt.setDate(18, Date.valueOf(this.getBirthday()));
            }
            if(this.getIDType() == null || this.getIDType().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getIDType());
            }
            if(this.getIDNo() == null || this.getIDNo().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getIDNo());
            }
            if(this.getNativePlace() == null || this.getNativePlace().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getNativePlace());
            }
            if(this.getNationality() == null || this.getNationality().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getNationality());
            }
            if(this.getRgtAddress() == null || this.getRgtAddress().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getRgtAddress());
            }
            if(this.getMarriage() == null || this.getMarriage().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getMarriage());
            }
            if(this.getMarriageDate() == null || this.getMarriageDate().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getMarriageDate()));
            }
            if(this.getHealth() == null || this.getHealth().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getHealth());
            }
            pstmt.setDouble(27, this.getStature());
            pstmt.setDouble(28, this.getAvoirdupois());
            if(this.getDegree() == null || this.getDegree().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getDegree());
            }
            if(this.getCreditGrade() == null || this.getCreditGrade().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getCreditGrade());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getBankAccNo());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getAccName());
            }
            if(this.getJoinCompanyDate() == null || this.getJoinCompanyDate().equals("null")) {
            	pstmt.setNull(34, 93);
            } else {
            	pstmt.setDate(34, Date.valueOf(this.getJoinCompanyDate()));
            }
            if(this.getStartWorkDate() == null || this.getStartWorkDate().equals("null")) {
            	pstmt.setNull(35, 93);
            } else {
            	pstmt.setDate(35, Date.valueOf(this.getStartWorkDate()));
            }
            if(this.getPosition() == null || this.getPosition().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getPosition());
            }
            pstmt.setDouble(37, this.getSalary());
            if(this.getOccupationType() == null || this.getOccupationType().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getOccupationType());
            }
            if(this.getOccupationCode() == null || this.getOccupationCode().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getOccupationCode());
            }
            if(this.getWorkType() == null || this.getWorkType().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getWorkType());
            }
            if(this.getPluralityType() == null || this.getPluralityType().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getPluralityType());
            }
            if(this.getSmokeFlag() == null || this.getSmokeFlag().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getSmokeFlag());
            }
            if(this.getContPlanCode() == null || this.getContPlanCode().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getContPlanCode());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getOperator());
            }
            if(this.getInsuredStat() == null || this.getInsuredStat().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getInsuredStat());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(46, 93);
            } else {
            	pstmt.setDate(46, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(48, 93);
            } else {
            	pstmt.setDate(48, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getModifyTime());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getUWFlag());
            }
            if(this.getUWCode() == null || this.getUWCode().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getUWCode());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(52, 93);
            } else {
            	pstmt.setDate(52, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(53, 12);
            } else {
            	pstmt.setString(53, this.getUWTime());
            }
            pstmt.setDouble(54, this.getBMI());
            pstmt.setInt(55, this.getInsuredPeoples());
            if(this.getLicense() == null || this.getLicense().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getLicense());
            }
            if(this.getLicenseType() == null || this.getLicenseType().equals("null")) {
            	pstmt.setNull(57, 12);
            } else {
            	pstmt.setString(57, this.getLicenseType());
            }
            pstmt.setInt(58, this.getCustomerSeqNo());
            if(this.getWorkNo() == null || this.getWorkNo().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getWorkNo());
            }
            if(this.getSocialInsuNo() == null || this.getSocialInsuNo().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getSocialInsuNo());
            }
            if(this.getOccupationDesb() == null || this.getOccupationDesb().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getOccupationDesb());
            }
            if(this.getIdValiDate() == null || this.getIdValiDate().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getIdValiDate());
            }
            if(this.getHaveMotorcycleLicence() == null || this.getHaveMotorcycleLicence().equals("null")) {
            	pstmt.setNull(63, 12);
            } else {
            	pstmt.setString(63, this.getHaveMotorcycleLicence());
            }
            if(this.getPartTimeJob() == null || this.getPartTimeJob().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getPartTimeJob());
            }
            if(this.getHealthFlag() == null || this.getHealthFlag().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getHealthFlag());
            }
            if(this.getServiceMark() == null || this.getServiceMark().equals("null")) {
            	pstmt.setNull(66, 12);
            } else {
            	pstmt.setString(66, this.getServiceMark());
            }
            if(this.getFirstName() == null || this.getFirstName().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getFirstName());
            }
            if(this.getLastName() == null || this.getLastName().equals("null")) {
            	pstmt.setNull(68, 12);
            } else {
            	pstmt.setString(68, this.getLastName());
            }
            if(this.getSSFlag() == null || this.getSSFlag().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getSSFlag());
            }
            if(this.getTINNO() == null || this.getTINNO().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getTINNO());
            }
            if(this.getTINFlag() == null || this.getTINFlag().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getTINFlag());
            }
            if(this.getInsuredType() == null || this.getInsuredType().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getInsuredType());
            }
            if(this.getIsLongValid() == null || this.getIsLongValid().equals("null")) {
            	pstmt.setNull(73, 12);
            } else {
            	pstmt.setString(73, this.getIsLongValid());
            }
            if(this.getIDStartDate() == null || this.getIDStartDate().equals("null")) {
            	pstmt.setNull(74, 93);
            } else {
            	pstmt.setDate(74, Date.valueOf(this.getIDStartDate()));
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LBInsured WHERE  1=1  AND InsuredID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getInsuredID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBInsuredDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LBInsuredSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LBInsuredSet aLBInsuredSet = new LBInsuredSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBInsured");
            LBInsuredSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBInsuredDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBInsuredSchema s1 = new LBInsuredSchema();
                s1.setSchema(rs,i);
                aLBInsuredSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLBInsuredSet;
    }

    public LBInsuredSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LBInsuredSet aLBInsuredSet = new LBInsuredSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBInsuredDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBInsuredSchema s1 = new LBInsuredSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBInsuredDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBInsuredSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBInsuredSet;
    }

    public LBInsuredSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBInsuredSet aLBInsuredSet = new LBInsuredSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBInsured");
            LBInsuredSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBInsuredSchema s1 = new LBInsuredSchema();
                s1.setSchema(rs,i);
                aLBInsuredSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBInsuredSet;
    }

    public LBInsuredSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBInsuredSet aLBInsuredSet = new LBInsuredSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBInsuredSchema s1 = new LBInsuredSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBInsuredDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBInsuredSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBInsuredSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBInsured");
            LBInsuredSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LBInsured " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LBInsuredDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LBInsuredSet
     */
    public LBInsuredSet getData() {
        int tCount = 0;
        LBInsuredSet tLBInsuredSet = new LBInsuredSet();
        LBInsuredSchema tLBInsuredSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLBInsuredSchema = new LBInsuredSchema();
            tLBInsuredSchema.setSchema(mResultSet, 1);
            tLBInsuredSet.add(tLBInsuredSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLBInsuredSchema = new LBInsuredSchema();
                    tLBInsuredSchema.setSchema(mResultSet, 1);
                    tLBInsuredSet.add(tLBInsuredSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLBInsuredSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LBInsuredDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LBInsuredDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LBInsuredDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
