/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskFeeParamPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskFeeParamPojo implements Pojo,Serializable {
    // @Field
    /** 管理费编码 */
    private String FeeCode; 
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 交费项编码 */
    private String PayPlanCode; 
    /** 管理费计算方式 */
    private String FeeCalMode; 
    /** Feeid */
    private int FeeID; 
    /** 费用下限 */
    private double FeeMin; 
    /** 费用上限 */
    private double FeeMax; 
    /** 管理费比例 */
    private double FeeRate; 


    public static final int FIELDNUM = 8;    // 数据库表的字段个数
    public String getFeeCode() {
        return FeeCode;
    }
    public void setFeeCode(String aFeeCode) {
        FeeCode = aFeeCode;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getFeeCalMode() {
        return FeeCalMode;
    }
    public void setFeeCalMode(String aFeeCalMode) {
        FeeCalMode = aFeeCalMode;
    }
    public int getFeeID() {
        return FeeID;
    }
    public void setFeeID(int aFeeID) {
        FeeID = aFeeID;
    }
    public void setFeeID(String aFeeID) {
        if (aFeeID != null && !aFeeID.equals("")) {
            Integer tInteger = new Integer(aFeeID);
            int i = tInteger.intValue();
            FeeID = i;
        }
    }

    public double getFeeMin() {
        return FeeMin;
    }
    public void setFeeMin(double aFeeMin) {
        FeeMin = aFeeMin;
    }
    public void setFeeMin(String aFeeMin) {
        if (aFeeMin != null && !aFeeMin.equals("")) {
            Double tDouble = new Double(aFeeMin);
            double d = tDouble.doubleValue();
            FeeMin = d;
        }
    }

    public double getFeeMax() {
        return FeeMax;
    }
    public void setFeeMax(double aFeeMax) {
        FeeMax = aFeeMax;
    }
    public void setFeeMax(String aFeeMax) {
        if (aFeeMax != null && !aFeeMax.equals("")) {
            Double tDouble = new Double(aFeeMax);
            double d = tDouble.doubleValue();
            FeeMax = d;
        }
    }

    public double getFeeRate() {
        return FeeRate;
    }
    public void setFeeRate(double aFeeRate) {
        FeeRate = aFeeRate;
    }
    public void setFeeRate(String aFeeRate) {
        if (aFeeRate != null && !aFeeRate.equals("")) {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("FeeCode") ) {
            return 0;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 1;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 2;
        }
        if( strFieldName.equals("FeeCalMode") ) {
            return 3;
        }
        if( strFieldName.equals("FeeID") ) {
            return 4;
        }
        if( strFieldName.equals("FeeMin") ) {
            return 5;
        }
        if( strFieldName.equals("FeeMax") ) {
            return 6;
        }
        if( strFieldName.equals("FeeRate") ) {
            return 7;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "FeeCode";
                break;
            case 1:
                strFieldName = "InsuAccNo";
                break;
            case 2:
                strFieldName = "PayPlanCode";
                break;
            case 3:
                strFieldName = "FeeCalMode";
                break;
            case 4:
                strFieldName = "FeeID";
                break;
            case 5:
                strFieldName = "FeeMin";
                break;
            case 6:
                strFieldName = "FeeMax";
                break;
            case 7:
                strFieldName = "FeeRate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "FEECODE":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "FEECALMODE":
                return Schema.TYPE_STRING;
            case "FEEID":
                return Schema.TYPE_INT;
            case "FEEMIN":
                return Schema.TYPE_DOUBLE;
            case "FEEMAX":
                return Schema.TYPE_DOUBLE;
            case "FEERATE":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_INT;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("FeeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCode));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("FeeCalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCalMode));
        }
        if (FCode.equalsIgnoreCase("FeeID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeID));
        }
        if (FCode.equalsIgnoreCase("FeeMin")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeMin));
        }
        if (FCode.equalsIgnoreCase("FeeMax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeMax));
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(FeeCode);
                break;
            case 1:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 2:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 3:
                strFieldValue = String.valueOf(FeeCalMode);
                break;
            case 4:
                strFieldValue = String.valueOf(FeeID);
                break;
            case 5:
                strFieldValue = String.valueOf(FeeMin);
                break;
            case 6:
                strFieldValue = String.valueOf(FeeMax);
                break;
            case 7:
                strFieldValue = String.valueOf(FeeRate);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("FeeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCode = FValue.trim();
            }
            else
                FeeCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("FeeCalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeCalMode = FValue.trim();
            }
            else
                FeeCalMode = null;
        }
        if (FCode.equalsIgnoreCase("FeeID")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                FeeID = i;
            }
        }
        if (FCode.equalsIgnoreCase("FeeMin")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeMin = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeMax")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeMax = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LMRiskFeeParamPojo [" +
            "FeeCode="+FeeCode +
            ", InsuAccNo="+InsuAccNo +
            ", PayPlanCode="+PayPlanCode +
            ", FeeCalMode="+FeeCalMode +
            ", FeeID="+FeeID +
            ", FeeMin="+FeeMin +
            ", FeeMax="+FeeMax +
            ", FeeRate="+FeeRate +"]";
    }
}
