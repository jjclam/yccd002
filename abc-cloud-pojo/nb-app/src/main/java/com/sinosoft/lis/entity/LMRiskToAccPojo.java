/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskToAccPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskToAccPojo implements Pojo,Serializable {
    // @Field
    /** 险种版本 */
    private String RiskVer; 
    /** 保险帐户号码 */
    @RedisPrimaryHKey
    private String InsuAccNo; 
    /** 保险帐户名称 */
    private String InsuAccName; 
    /** 险种编码 */
    @RedisIndexHKey
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 保险帐户是否录入 */
    private String InsuAccInpflag; 
    /** 默认的补偿顺序 */
    private String DefaultOrder; 


    public static final int FIELDNUM = 6;    // 数据库表的字段个数
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getInsuAccName() {
        return InsuAccName;
    }
    public void setInsuAccName(String aInsuAccName) {
        InsuAccName = aInsuAccName;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getInsuAccInpflag() {
        return InsuAccInpflag;
    }
    public void setInsuAccInpflag(String aInsuAccInpflag) {
        InsuAccInpflag = aInsuAccInpflag;
    }
    public String getDefaultOrder() {
        return DefaultOrder;
    }
    public void setDefaultOrder(String aDefaultOrder) {
        DefaultOrder = aDefaultOrder;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskVer") ) {
            return 0;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 1;
        }
        if( strFieldName.equals("InsuAccName") ) {
            return 2;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 3;
        }
        if( strFieldName.equals("InsuAccInpflag") ) {
            return 4;
        }
        if( strFieldName.equals("DefaultOrder") ) {
            return 5;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskVer";
                break;
            case 1:
                strFieldName = "InsuAccNo";
                break;
            case 2:
                strFieldName = "InsuAccName";
                break;
            case 3:
                strFieldName = "RiskCode";
                break;
            case 4:
                strFieldName = "InsuAccInpflag";
                break;
            case 5:
                strFieldName = "DefaultOrder";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "INSUACCNAME":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "INSUACCINPFLAG":
                return Schema.TYPE_STRING;
            case "DEFAULTORDER":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccName));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("InsuAccInpflag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccInpflag));
        }
        if (FCode.equalsIgnoreCase("DefaultOrder")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultOrder));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 1:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 2:
                strFieldValue = String.valueOf(InsuAccName);
                break;
            case 3:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 4:
                strFieldValue = String.valueOf(InsuAccInpflag);
                break;
            case 5:
                strFieldValue = String.valueOf(DefaultOrder);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccName = FValue.trim();
            }
            else
                InsuAccName = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccInpflag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccInpflag = FValue.trim();
            }
            else
                InsuAccInpflag = null;
        }
        if (FCode.equalsIgnoreCase("DefaultOrder")) {
            if( FValue != null && !FValue.equals(""))
            {
                DefaultOrder = FValue.trim();
            }
            else
                DefaultOrder = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskToAccPojo [" +
            "RiskVer="+RiskVer +
            ", InsuAccNo="+InsuAccNo +
            ", InsuAccName="+InsuAccName +
            ", RiskCode="+RiskCode +
            ", InsuAccInpflag="+InsuAccInpflag +
            ", DefaultOrder="+DefaultOrder +"]";
    }
}
