/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCAIPMULTIYEARCONTPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-06-11
 */
public class LCAIPMULTIYEARCONTPojo implements Pojo,Serializable {
    // @Field
    /** 保单号 */
    private String ContNo; 
    /** 保单印刷号 */
    private String PrtNo; 
    /** 保单生效日 */
    private String  CValiDate;
    /** 单证类型 */
    private String CardType; 
    /** 单证号 */
    private String CardNo; 
    /** 投保单/保单标志 */
    private String AppFlag; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 备用字段1 */
    private String StandbyFalg1; 
    /** 备用字段2 */
    private String StandbyFalg2; 
    /** 备用字段3 */
    private String StandbyFalg3; 
    /** 备用字段4 */
    private String StandbyFalg4; 
    /** 备用字段5 */
    private String StandbyFalg5; 


    public static final int FIELDNUM = 15;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public String getCardType() {
        return CardType;
    }
    public void setCardType(String aCardType) {
        CardType = aCardType;
    }
    public String getCardNo() {
        return CardNo;
    }
    public void setCardNo(String aCardNo) {
        CardNo = aCardNo;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandbyFalg1() {
        return StandbyFalg1;
    }
    public void setStandbyFalg1(String aStandbyFalg1) {
        StandbyFalg1 = aStandbyFalg1;
    }
    public String getStandbyFalg2() {
        return StandbyFalg2;
    }
    public void setStandbyFalg2(String aStandbyFalg2) {
        StandbyFalg2 = aStandbyFalg2;
    }
    public String getStandbyFalg3() {
        return StandbyFalg3;
    }
    public void setStandbyFalg3(String aStandbyFalg3) {
        StandbyFalg3 = aStandbyFalg3;
    }
    public String getStandbyFalg4() {
        return StandbyFalg4;
    }
    public void setStandbyFalg4(String aStandbyFalg4) {
        StandbyFalg4 = aStandbyFalg4;
    }
    public String getStandbyFalg5() {
        return StandbyFalg5;
    }
    public void setStandbyFalg5(String aStandbyFalg5) {
        StandbyFalg5 = aStandbyFalg5;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContNo") ) {
            return 0;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 1;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 2;
        }
        if( strFieldName.equals("CardType") ) {
            return 3;
        }
        if( strFieldName.equals("CardNo") ) {
            return 4;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 5;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 6;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 7;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 8;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 9;
        }
        if( strFieldName.equals("StandbyFalg1") ) {
            return 10;
        }
        if( strFieldName.equals("StandbyFalg2") ) {
            return 11;
        }
        if( strFieldName.equals("StandbyFalg3") ) {
            return 12;
        }
        if( strFieldName.equals("StandbyFalg4") ) {
            return 13;
        }
        if( strFieldName.equals("StandbyFalg5") ) {
            return 14;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "PrtNo";
                break;
            case 2:
                strFieldName = "CValiDate";
                break;
            case 3:
                strFieldName = "CardType";
                break;
            case 4:
                strFieldName = "CardNo";
                break;
            case 5:
                strFieldName = "AppFlag";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "StandbyFalg1";
                break;
            case 11:
                strFieldName = "StandbyFalg2";
                break;
            case 12:
                strFieldName = "StandbyFalg3";
                break;
            case 13:
                strFieldName = "StandbyFalg4";
                break;
            case 14:
                strFieldName = "StandbyFalg5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "CARDTYPE":
                return Schema.TYPE_STRING;
            case "CARDNO":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFALG1":
                return Schema.TYPE_STRING;
            case "STANDBYFALG2":
                return Schema.TYPE_STRING;
            case "STANDBYFALG3":
                return Schema.TYPE_STRING;
            case "STANDBYFALG4":
                return Schema.TYPE_STRING;
            case "STANDBYFALG5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("CardType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardType));
        }
        if (FCode.equalsIgnoreCase("CardNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg1));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg2));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg3));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg4));
        }
        if (FCode.equalsIgnoreCase("StandbyFalg5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFalg5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 2:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 3:
                strFieldValue = String.valueOf(CardType);
                break;
            case 4:
                strFieldValue = String.valueOf(CardNo);
                break;
            case 5:
                strFieldValue = String.valueOf(AppFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 7:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 8:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 9:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 10:
                strFieldValue = String.valueOf(StandbyFalg1);
                break;
            case 11:
                strFieldValue = String.valueOf(StandbyFalg2);
                break;
            case 12:
                strFieldValue = String.valueOf(StandbyFalg3);
                break;
            case 13:
                strFieldValue = String.valueOf(StandbyFalg4);
                break;
            case 14:
                strFieldValue = String.valueOf(StandbyFalg5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("CardType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardType = FValue.trim();
            }
            else
                CardType = null;
        }
        if (FCode.equalsIgnoreCase("CardNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardNo = FValue.trim();
            }
            else
                CardNo = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg1 = FValue.trim();
            }
            else
                StandbyFalg1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg2 = FValue.trim();
            }
            else
                StandbyFalg2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg3 = FValue.trim();
            }
            else
                StandbyFalg3 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg4 = FValue.trim();
            }
            else
                StandbyFalg4 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFalg5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFalg5 = FValue.trim();
            }
            else
                StandbyFalg5 = null;
        }
        return true;
    }


    public String toString() {
    return "LCAIPMULTIYEARCONTPojo [" +
            "ContNo="+ContNo +
            ", PrtNo="+PrtNo +
            ", CValiDate="+CValiDate +
            ", CardType="+CardType +
            ", CardNo="+CardNo +
            ", AppFlag="+AppFlag +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", StandbyFalg1="+StandbyFalg1 +
            ", StandbyFalg2="+StandbyFalg2 +
            ", StandbyFalg3="+StandbyFalg3 +
            ", StandbyFalg4="+StandbyFalg4 +
            ", StandbyFalg5="+StandbyFalg5 +"]";
    }
}
