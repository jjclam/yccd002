/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMPolDutyEdorCalPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMPolDutyEdorCalPojo implements Pojo,Serializable {
    // @Field
    /** 险种代码 */
    private String RiskCode; 
    /** 责任代码 */
    private String DutyCode; 
    /** 保全项目 */
    private String EdorType; 
    /** 计算方向 */
    private String CalMode; 
    /** 补退保费计算代码 */
    private String ChgPremCalCode; 
    /** 利息计算算法 */
    private String InterestCalCode; 


    public static final int FIELDNUM = 6;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getEdorType() {
        return EdorType;
    }
    public void setEdorType(String aEdorType) {
        EdorType = aEdorType;
    }
    public String getCalMode() {
        return CalMode;
    }
    public void setCalMode(String aCalMode) {
        CalMode = aCalMode;
    }
    public String getChgPremCalCode() {
        return ChgPremCalCode;
    }
    public void setChgPremCalCode(String aChgPremCalCode) {
        ChgPremCalCode = aChgPremCalCode;
    }
    public String getInterestCalCode() {
        return InterestCalCode;
    }
    public void setInterestCalCode(String aInterestCalCode) {
        InterestCalCode = aInterestCalCode;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 1;
        }
        if( strFieldName.equals("EdorType") ) {
            return 2;
        }
        if( strFieldName.equals("CalMode") ) {
            return 3;
        }
        if( strFieldName.equals("ChgPremCalCode") ) {
            return 4;
        }
        if( strFieldName.equals("InterestCalCode") ) {
            return 5;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "DutyCode";
                break;
            case 2:
                strFieldName = "EdorType";
                break;
            case 3:
                strFieldName = "CalMode";
                break;
            case 4:
                strFieldName = "ChgPremCalCode";
                break;
            case 5:
                strFieldName = "InterestCalCode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "EDORTYPE":
                return Schema.TYPE_STRING;
            case "CALMODE":
                return Schema.TYPE_STRING;
            case "CHGPREMCALCODE":
                return Schema.TYPE_STRING;
            case "INTERESTCALCODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equalsIgnoreCase("CalMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalMode));
        }
        if (FCode.equalsIgnoreCase("ChgPremCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgPremCalCode));
        }
        if (FCode.equalsIgnoreCase("InterestCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestCalCode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 2:
                strFieldValue = String.valueOf(EdorType);
                break;
            case 3:
                strFieldValue = String.valueOf(CalMode);
                break;
            case 4:
                strFieldValue = String.valueOf(ChgPremCalCode);
                break;
            case 5:
                strFieldValue = String.valueOf(InterestCalCode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
                EdorType = null;
        }
        if (FCode.equalsIgnoreCase("CalMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalMode = FValue.trim();
            }
            else
                CalMode = null;
        }
        if (FCode.equalsIgnoreCase("ChgPremCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChgPremCalCode = FValue.trim();
            }
            else
                ChgPremCalCode = null;
        }
        if (FCode.equalsIgnoreCase("InterestCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InterestCalCode = FValue.trim();
            }
            else
                InterestCalCode = null;
        }
        return true;
    }


    public String toString() {
    return "LMPolDutyEdorCalPojo [" +
            "RiskCode="+RiskCode +
            ", DutyCode="+DutyCode +
            ", EdorType="+EdorType +
            ", CalMode="+CalMode +
            ", ChgPremCalCode="+ChgPremCalCode +
            ", InterestCalCode="+InterestCalCode +"]";
    }
}
