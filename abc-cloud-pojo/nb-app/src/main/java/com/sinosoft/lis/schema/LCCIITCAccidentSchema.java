/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCCIITCAccidentDB;

/**
 * <p>ClassName: LCCIITCAccidentSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-06-25
 */
public class LCCIITCAccidentSchema implements Schema, Cloneable {
    // @Field
    /** 投保单号 */
    private String PrtNo;
    /** 校验次数 */
    private int CheckNum;
    /** 超时标记 */
    private String OutTimeFlag;
    /** 超时原因详述 */
    private String OutTimeDescribe;
    /** 响应报文 */
    private String ResponseMsg;
    /** Http状态码 */
    private String HttpCode;
    /** Body响应码 */
    private String RetCode;
    /** Body响应信息 */
    private String RetMessage;
    /** 被保险人客户号 */
    private String InsuredNo;
    /** 被保险人姓名 */
    private String InsuredName;
    /** 被保险人证件类型 */
    private String InsuredIDType;
    /** 被保险人证件号码 */
    private String InsuredIDNO;
    /** 产品编码 */
    private String ProductCode;
    /** 多家公司承保提示 */
    private String MultiCompany;
    /** Ci */
    private String MajorDiseasePayment;
    /** Pd */
    private String Disability;
    /** 是否密集投保 */
    private String Dense;
    /** 累计保额提示 */
    private String AccumulativeMoney;
    /** 网页查询码（25位） */
    private String PageQueryCode;
    /** 数据截止日期 */
    private String TagDate;
    /** 是否有网页数据 */
    private String DisplayPage;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 备用属性字段1 */
    private String StandByFlag1;
    /** 请求流水号 */
    private String InsurerUuid;

    public static final int FIELDNUM = 28;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCCIITCAccidentSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "PrtNo";
        pk[1] = "CheckNum";
        pk[2] = "InsuredNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCCIITCAccidentSchema cloned = (LCCIITCAccidentSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public int getCheckNum() {
        return CheckNum;
    }
    public void setCheckNum(int aCheckNum) {
        CheckNum = aCheckNum;
    }
    public void setCheckNum(String aCheckNum) {
        if (aCheckNum != null && !aCheckNum.equals("")) {
            Integer tInteger = new Integer(aCheckNum);
            int i = tInteger.intValue();
            CheckNum = i;
        }
    }

    public String getOutTimeFlag() {
        return OutTimeFlag;
    }
    public void setOutTimeFlag(String aOutTimeFlag) {
        OutTimeFlag = aOutTimeFlag;
    }
    public String getOutTimeDescribe() {
        return OutTimeDescribe;
    }
    public void setOutTimeDescribe(String aOutTimeDescribe) {
        OutTimeDescribe = aOutTimeDescribe;
    }
    public String getResponseMsg() {
        return ResponseMsg;
    }
    public void setResponseMsg(String aResponseMsg) {
        ResponseMsg = aResponseMsg;
    }
    public String getHttpCode() {
        return HttpCode;
    }
    public void setHttpCode(String aHttpCode) {
        HttpCode = aHttpCode;
    }
    public String getRetCode() {
        return RetCode;
    }
    public void setRetCode(String aRetCode) {
        RetCode = aRetCode;
    }
    public String getRetMessage() {
        return RetMessage;
    }
    public void setRetMessage(String aRetMessage) {
        RetMessage = aRetMessage;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getInsuredIDType() {
        return InsuredIDType;
    }
    public void setInsuredIDType(String aInsuredIDType) {
        InsuredIDType = aInsuredIDType;
    }
    public String getInsuredIDNO() {
        return InsuredIDNO;
    }
    public void setInsuredIDNO(String aInsuredIDNO) {
        InsuredIDNO = aInsuredIDNO;
    }
    public String getProductCode() {
        return ProductCode;
    }
    public void setProductCode(String aProductCode) {
        ProductCode = aProductCode;
    }
    public String getMultiCompany() {
        return MultiCompany;
    }
    public void setMultiCompany(String aMultiCompany) {
        MultiCompany = aMultiCompany;
    }
    public String getMajorDiseasePayment() {
        return MajorDiseasePayment;
    }
    public void setMajorDiseasePayment(String aMajorDiseasePayment) {
        MajorDiseasePayment = aMajorDiseasePayment;
    }
    public String getDisability() {
        return Disability;
    }
    public void setDisability(String aDisability) {
        Disability = aDisability;
    }
    public String getDense() {
        return Dense;
    }
    public void setDense(String aDense) {
        Dense = aDense;
    }
    public String getAccumulativeMoney() {
        return AccumulativeMoney;
    }
    public void setAccumulativeMoney(String aAccumulativeMoney) {
        AccumulativeMoney = aAccumulativeMoney;
    }
    public String getPageQueryCode() {
        return PageQueryCode;
    }
    public void setPageQueryCode(String aPageQueryCode) {
        PageQueryCode = aPageQueryCode;
    }
    public String getTagDate() {
        return TagDate;
    }
    public void setTagDate(String aTagDate) {
        TagDate = aTagDate;
    }
    public String getDisplayPage() {
        return DisplayPage;
    }
    public void setDisplayPage(String aDisplayPage) {
        DisplayPage = aDisplayPage;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getInsurerUuid() {
        return InsurerUuid;
    }
    public void setInsurerUuid(String aInsurerUuid) {
        InsurerUuid = aInsurerUuid;
    }

    /**
    * 使用另外一个 LCCIITCAccidentSchema 对象给 Schema 赋值
    * @param: aLCCIITCAccidentSchema LCCIITCAccidentSchema
    **/
    public void setSchema(LCCIITCAccidentSchema aLCCIITCAccidentSchema) {
        this.PrtNo = aLCCIITCAccidentSchema.getPrtNo();
        this.CheckNum = aLCCIITCAccidentSchema.getCheckNum();
        this.OutTimeFlag = aLCCIITCAccidentSchema.getOutTimeFlag();
        this.OutTimeDescribe = aLCCIITCAccidentSchema.getOutTimeDescribe();
        this.ResponseMsg = aLCCIITCAccidentSchema.getResponseMsg();
        this.HttpCode = aLCCIITCAccidentSchema.getHttpCode();
        this.RetCode = aLCCIITCAccidentSchema.getRetCode();
        this.RetMessage = aLCCIITCAccidentSchema.getRetMessage();
        this.InsuredNo = aLCCIITCAccidentSchema.getInsuredNo();
        this.InsuredName = aLCCIITCAccidentSchema.getInsuredName();
        this.InsuredIDType = aLCCIITCAccidentSchema.getInsuredIDType();
        this.InsuredIDNO = aLCCIITCAccidentSchema.getInsuredIDNO();
        this.ProductCode = aLCCIITCAccidentSchema.getProductCode();
        this.MultiCompany = aLCCIITCAccidentSchema.getMultiCompany();
        this.MajorDiseasePayment = aLCCIITCAccidentSchema.getMajorDiseasePayment();
        this.Disability = aLCCIITCAccidentSchema.getDisability();
        this.Dense = aLCCIITCAccidentSchema.getDense();
        this.AccumulativeMoney = aLCCIITCAccidentSchema.getAccumulativeMoney();
        this.PageQueryCode = aLCCIITCAccidentSchema.getPageQueryCode();
        this.TagDate = aLCCIITCAccidentSchema.getTagDate();
        this.DisplayPage = aLCCIITCAccidentSchema.getDisplayPage();
        this.Operator = aLCCIITCAccidentSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCCIITCAccidentSchema.getMakeDate());
        this.MakeTime = aLCCIITCAccidentSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCCIITCAccidentSchema.getModifyDate());
        this.ModifyTime = aLCCIITCAccidentSchema.getModifyTime();
        this.StandByFlag1 = aLCCIITCAccidentSchema.getStandByFlag1();
        this.InsurerUuid = aLCCIITCAccidentSchema.getInsurerUuid();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            this.CheckNum = rs.getInt("CheckNum");
            if( rs.getString("OutTimeFlag") == null )
                this.OutTimeFlag = null;
            else
                this.OutTimeFlag = rs.getString("OutTimeFlag").trim();

            if( rs.getString("OutTimeDescribe") == null )
                this.OutTimeDescribe = null;
            else
                this.OutTimeDescribe = rs.getString("OutTimeDescribe").trim();

            if( rs.getString("ResponseMsg") == null )
                this.ResponseMsg = null;
            else
                this.ResponseMsg = rs.getString("ResponseMsg").trim();

            if( rs.getString("HttpCode") == null )
                this.HttpCode = null;
            else
                this.HttpCode = rs.getString("HttpCode").trim();

            if( rs.getString("RetCode") == null )
                this.RetCode = null;
            else
                this.RetCode = rs.getString("RetCode").trim();

            if( rs.getString("RetMessage") == null )
                this.RetMessage = null;
            else
                this.RetMessage = rs.getString("RetMessage").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("InsuredName") == null )
                this.InsuredName = null;
            else
                this.InsuredName = rs.getString("InsuredName").trim();

            if( rs.getString("InsuredIDType") == null )
                this.InsuredIDType = null;
            else
                this.InsuredIDType = rs.getString("InsuredIDType").trim();

            if( rs.getString("InsuredIDNO") == null )
                this.InsuredIDNO = null;
            else
                this.InsuredIDNO = rs.getString("InsuredIDNO").trim();

            if( rs.getString("ProductCode") == null )
                this.ProductCode = null;
            else
                this.ProductCode = rs.getString("ProductCode").trim();

            if( rs.getString("MultiCompany") == null )
                this.MultiCompany = null;
            else
                this.MultiCompany = rs.getString("MultiCompany").trim();

            if( rs.getString("MajorDiseasePayment") == null )
                this.MajorDiseasePayment = null;
            else
                this.MajorDiseasePayment = rs.getString("MajorDiseasePayment").trim();

            if( rs.getString("Disability") == null )
                this.Disability = null;
            else
                this.Disability = rs.getString("Disability").trim();

            if( rs.getString("Dense") == null )
                this.Dense = null;
            else
                this.Dense = rs.getString("Dense").trim();

            if( rs.getString("AccumulativeMoney") == null )
                this.AccumulativeMoney = null;
            else
                this.AccumulativeMoney = rs.getString("AccumulativeMoney").trim();

            if( rs.getString("PageQueryCode") == null )
                this.PageQueryCode = null;
            else
                this.PageQueryCode = rs.getString("PageQueryCode").trim();

            if( rs.getString("TagDate") == null )
                this.TagDate = null;
            else
                this.TagDate = rs.getString("TagDate").trim();

            if( rs.getString("DisplayPage") == null )
                this.DisplayPage = null;
            else
                this.DisplayPage = rs.getString("DisplayPage").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("StandByFlag1") == null )
                this.StandByFlag1 = null;
            else
                this.StandByFlag1 = rs.getString("StandByFlag1").trim();

            if( rs.getString("InsurerUuid") == null )
                this.InsurerUuid = null;
            else
                this.InsurerUuid = rs.getString("InsurerUuid").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCCIITCAccidentSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCCIITCAccidentSchema getSchema() {
        LCCIITCAccidentSchema aLCCIITCAccidentSchema = new LCCIITCAccidentSchema();
        aLCCIITCAccidentSchema.setSchema(this);
        return aLCCIITCAccidentSchema;
    }

    public LCCIITCAccidentDB getDB() {
        LCCIITCAccidentDB aDBOper = new LCCIITCAccidentDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCCIITCAccident描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CheckNum));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OutTimeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OutTimeDescribe)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ResponseMsg)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HttpCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RetCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RetMessage)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredIDNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProductCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MultiCompany)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MajorDiseasePayment)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Disability)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Dense)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccumulativeMoney)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PageQueryCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TagDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisplayPage)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsurerUuid));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCCIITCAccident>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            CheckNum = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
            OutTimeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            OutTimeDescribe = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ResponseMsg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            HttpCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            RetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            RetMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            InsuredIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            InsuredIDNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ProductCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            MultiCompany = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            MajorDiseasePayment = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            Disability = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            Dense = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            AccumulativeMoney = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            PageQueryCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            TagDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            DisplayPage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            InsurerUuid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCCIITCAccidentSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("CheckNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckNum));
        }
        if (FCode.equalsIgnoreCase("OutTimeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutTimeFlag));
        }
        if (FCode.equalsIgnoreCase("OutTimeDescribe")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutTimeDescribe));
        }
        if (FCode.equalsIgnoreCase("ResponseMsg")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResponseMsg));
        }
        if (FCode.equalsIgnoreCase("HttpCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HttpCode));
        }
        if (FCode.equalsIgnoreCase("RetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetCode));
        }
        if (FCode.equalsIgnoreCase("RetMessage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetMessage));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
        }
        if (FCode.equalsIgnoreCase("InsuredIDNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNO));
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProductCode));
        }
        if (FCode.equalsIgnoreCase("MultiCompany")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MultiCompany));
        }
        if (FCode.equalsIgnoreCase("MajorDiseasePayment")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MajorDiseasePayment));
        }
        if (FCode.equalsIgnoreCase("Disability")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Disability));
        }
        if (FCode.equalsIgnoreCase("Dense")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dense));
        }
        if (FCode.equalsIgnoreCase("AccumulativeMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccumulativeMoney));
        }
        if (FCode.equalsIgnoreCase("PageQueryCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageQueryCode));
        }
        if (FCode.equalsIgnoreCase("TagDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TagDate));
        }
        if (FCode.equalsIgnoreCase("DisplayPage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisplayPage));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("InsurerUuid")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurerUuid));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 1:
                strFieldValue = String.valueOf(CheckNum);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OutTimeFlag);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OutTimeDescribe);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ResponseMsg);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(HttpCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RetCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RetMessage);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InsuredIDType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(InsuredIDNO);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ProductCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(MultiCompany);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MajorDiseasePayment);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Disability);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Dense);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AccumulativeMoney);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(PageQueryCode);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(TagDate);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(DisplayPage);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(InsurerUuid);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("CheckNum")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                CheckNum = i;
            }
        }
        if (FCode.equalsIgnoreCase("OutTimeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutTimeFlag = FValue.trim();
            }
            else
                OutTimeFlag = null;
        }
        if (FCode.equalsIgnoreCase("OutTimeDescribe")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutTimeDescribe = FValue.trim();
            }
            else
                OutTimeDescribe = null;
        }
        if (FCode.equalsIgnoreCase("ResponseMsg")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResponseMsg = FValue.trim();
            }
            else
                ResponseMsg = null;
        }
        if (FCode.equalsIgnoreCase("HttpCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HttpCode = FValue.trim();
            }
            else
                HttpCode = null;
        }
        if (FCode.equalsIgnoreCase("RetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RetCode = FValue.trim();
            }
            else
                RetCode = null;
        }
        if (FCode.equalsIgnoreCase("RetMessage")) {
            if( FValue != null && !FValue.equals(""))
            {
                RetMessage = FValue.trim();
            }
            else
                RetMessage = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDType = FValue.trim();
            }
            else
                InsuredIDType = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDNO = FValue.trim();
            }
            else
                InsuredIDNO = null;
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProductCode = FValue.trim();
            }
            else
                ProductCode = null;
        }
        if (FCode.equalsIgnoreCase("MultiCompany")) {
            if( FValue != null && !FValue.equals(""))
            {
                MultiCompany = FValue.trim();
            }
            else
                MultiCompany = null;
        }
        if (FCode.equalsIgnoreCase("MajorDiseasePayment")) {
            if( FValue != null && !FValue.equals(""))
            {
                MajorDiseasePayment = FValue.trim();
            }
            else
                MajorDiseasePayment = null;
        }
        if (FCode.equalsIgnoreCase("Disability")) {
            if( FValue != null && !FValue.equals(""))
            {
                Disability = FValue.trim();
            }
            else
                Disability = null;
        }
        if (FCode.equalsIgnoreCase("Dense")) {
            if( FValue != null && !FValue.equals(""))
            {
                Dense = FValue.trim();
            }
            else
                Dense = null;
        }
        if (FCode.equalsIgnoreCase("AccumulativeMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccumulativeMoney = FValue.trim();
            }
            else
                AccumulativeMoney = null;
        }
        if (FCode.equalsIgnoreCase("PageQueryCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PageQueryCode = FValue.trim();
            }
            else
                PageQueryCode = null;
        }
        if (FCode.equalsIgnoreCase("TagDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                TagDate = FValue.trim();
            }
            else
                TagDate = null;
        }
        if (FCode.equalsIgnoreCase("DisplayPage")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisplayPage = FValue.trim();
            }
            else
                DisplayPage = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("InsurerUuid")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsurerUuid = FValue.trim();
            }
            else
                InsurerUuid = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCCIITCAccidentSchema other = (LCCIITCAccidentSchema)otherObject;
        return
            PrtNo.equals(other.getPrtNo())
            && CheckNum == other.getCheckNum()
            && OutTimeFlag.equals(other.getOutTimeFlag())
            && OutTimeDescribe.equals(other.getOutTimeDescribe())
            && ResponseMsg.equals(other.getResponseMsg())
            && HttpCode.equals(other.getHttpCode())
            && RetCode.equals(other.getRetCode())
            && RetMessage.equals(other.getRetMessage())
            && InsuredNo.equals(other.getInsuredNo())
            && InsuredName.equals(other.getInsuredName())
            && InsuredIDType.equals(other.getInsuredIDType())
            && InsuredIDNO.equals(other.getInsuredIDNO())
            && ProductCode.equals(other.getProductCode())
            && MultiCompany.equals(other.getMultiCompany())
            && MajorDiseasePayment.equals(other.getMajorDiseasePayment())
            && Disability.equals(other.getDisability())
            && Dense.equals(other.getDense())
            && AccumulativeMoney.equals(other.getAccumulativeMoney())
            && PageQueryCode.equals(other.getPageQueryCode())
            && TagDate.equals(other.getTagDate())
            && DisplayPage.equals(other.getDisplayPage())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && StandByFlag1.equals(other.getStandByFlag1())
            && InsurerUuid.equals(other.getInsurerUuid());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PrtNo") ) {
            return 0;
        }
        if( strFieldName.equals("CheckNum") ) {
            return 1;
        }
        if( strFieldName.equals("OutTimeFlag") ) {
            return 2;
        }
        if( strFieldName.equals("OutTimeDescribe") ) {
            return 3;
        }
        if( strFieldName.equals("ResponseMsg") ) {
            return 4;
        }
        if( strFieldName.equals("HttpCode") ) {
            return 5;
        }
        if( strFieldName.equals("RetCode") ) {
            return 6;
        }
        if( strFieldName.equals("RetMessage") ) {
            return 7;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 8;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 9;
        }
        if( strFieldName.equals("InsuredIDType") ) {
            return 10;
        }
        if( strFieldName.equals("InsuredIDNO") ) {
            return 11;
        }
        if( strFieldName.equals("ProductCode") ) {
            return 12;
        }
        if( strFieldName.equals("MultiCompany") ) {
            return 13;
        }
        if( strFieldName.equals("MajorDiseasePayment") ) {
            return 14;
        }
        if( strFieldName.equals("Disability") ) {
            return 15;
        }
        if( strFieldName.equals("Dense") ) {
            return 16;
        }
        if( strFieldName.equals("AccumulativeMoney") ) {
            return 17;
        }
        if( strFieldName.equals("PageQueryCode") ) {
            return 18;
        }
        if( strFieldName.equals("TagDate") ) {
            return 19;
        }
        if( strFieldName.equals("DisplayPage") ) {
            return 20;
        }
        if( strFieldName.equals("Operator") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 26;
        }
        if( strFieldName.equals("InsurerUuid") ) {
            return 27;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PrtNo";
                break;
            case 1:
                strFieldName = "CheckNum";
                break;
            case 2:
                strFieldName = "OutTimeFlag";
                break;
            case 3:
                strFieldName = "OutTimeDescribe";
                break;
            case 4:
                strFieldName = "ResponseMsg";
                break;
            case 5:
                strFieldName = "HttpCode";
                break;
            case 6:
                strFieldName = "RetCode";
                break;
            case 7:
                strFieldName = "RetMessage";
                break;
            case 8:
                strFieldName = "InsuredNo";
                break;
            case 9:
                strFieldName = "InsuredName";
                break;
            case 10:
                strFieldName = "InsuredIDType";
                break;
            case 11:
                strFieldName = "InsuredIDNO";
                break;
            case 12:
                strFieldName = "ProductCode";
                break;
            case 13:
                strFieldName = "MultiCompany";
                break;
            case 14:
                strFieldName = "MajorDiseasePayment";
                break;
            case 15:
                strFieldName = "Disability";
                break;
            case 16:
                strFieldName = "Dense";
                break;
            case 17:
                strFieldName = "AccumulativeMoney";
                break;
            case 18:
                strFieldName = "PageQueryCode";
                break;
            case 19:
                strFieldName = "TagDate";
                break;
            case 20:
                strFieldName = "DisplayPage";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "StandByFlag1";
                break;
            case 27:
                strFieldName = "InsurerUuid";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CHECKNUM":
                return Schema.TYPE_INT;
            case "OUTTIMEFLAG":
                return Schema.TYPE_STRING;
            case "OUTTIMEDESCRIBE":
                return Schema.TYPE_STRING;
            case "RESPONSEMSG":
                return Schema.TYPE_STRING;
            case "HTTPCODE":
                return Schema.TYPE_STRING;
            case "RETCODE":
                return Schema.TYPE_STRING;
            case "RETMESSAGE":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "INSUREDIDTYPE":
                return Schema.TYPE_STRING;
            case "INSUREDIDNO":
                return Schema.TYPE_STRING;
            case "PRODUCTCODE":
                return Schema.TYPE_STRING;
            case "MULTICOMPANY":
                return Schema.TYPE_STRING;
            case "MAJORDISEASEPAYMENT":
                return Schema.TYPE_STRING;
            case "DISABILITY":
                return Schema.TYPE_STRING;
            case "DENSE":
                return Schema.TYPE_STRING;
            case "ACCUMULATIVEMONEY":
                return Schema.TYPE_STRING;
            case "PAGEQUERYCODE":
                return Schema.TYPE_STRING;
            case "TAGDATE":
                return Schema.TYPE_STRING;
            case "DISPLAYPAGE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "INSURERUUID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_INT;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "LCCIITCAccidentSchema {" +
            "PrtNo="+PrtNo +
            ", CheckNum="+CheckNum +
            ", OutTimeFlag="+OutTimeFlag +
            ", OutTimeDescribe="+OutTimeDescribe +
            ", ResponseMsg="+ResponseMsg +
            ", HttpCode="+HttpCode +
            ", RetCode="+RetCode +
            ", RetMessage="+RetMessage +
            ", InsuredNo="+InsuredNo +
            ", InsuredName="+InsuredName +
            ", InsuredIDType="+InsuredIDType +
            ", InsuredIDNO="+InsuredIDNO +
            ", ProductCode="+ProductCode +
            ", MultiCompany="+MultiCompany +
            ", MajorDiseasePayment="+MajorDiseasePayment +
            ", Disability="+Disability +
            ", Dense="+Dense +
            ", AccumulativeMoney="+AccumulativeMoney +
            ", PageQueryCode="+PageQueryCode +
            ", TagDate="+TagDate +
            ", DisplayPage="+DisplayPage +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", StandByFlag1="+StandByFlag1 +
            ", InsurerUuid="+InsurerUuid +"}";
    }
}
