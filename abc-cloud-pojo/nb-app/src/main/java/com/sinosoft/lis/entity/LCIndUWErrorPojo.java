/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCIndUWErrorPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-08-30
 */
public class LCIndUWErrorPojo implements  Pojo,Serializable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 总单投保单号码 */
    private String ProposalContNo; 
    /** 核保次数 */
    private int UWNo; 
    /** 流水号 */
    private String SerialNo; 
    /** 被保人客户号码 */
    private String InsuredNo; 
    /** 被保人名称 */
    private String InsuredName; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 投保人名称 */
    private String AppntName; 
    /** 管理机构 */
    private String ManageCom; 
    /** 核保规则编码 */
    private String UWRuleCode; 
    /** 核保出错信息 */
    private String UWError; 
    /** 当前值 */
    private String CurrValue; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 核保可通过标记（分保） */
    private String UWPassFlag; 
    /** 核保级别 */
    private String UWGrade; 
    /** 核保建议结论 */
    private String SugPassFlag; 
    /** 核保类型 */
    private String UWType; 


    public static final int FIELDNUM = 19;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public int getUWNo() {
        return UWNo;
    }
    public void setUWNo(int aUWNo) {
        UWNo = aUWNo;
    }
    public void setUWNo(String aUWNo) {
        if (aUWNo != null && !aUWNo.equals("")) {
            Integer tInteger = new Integer(aUWNo);
            int i = tInteger.intValue();
            UWNo = i;
        }
    }

    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getUWRuleCode() {
        return UWRuleCode;
    }
    public void setUWRuleCode(String aUWRuleCode) {
        UWRuleCode = aUWRuleCode;
    }
    public String getUWError() {
        return UWError;
    }
    public void setUWError(String aUWError) {
        UWError = aUWError;
    }
    public String getCurrValue() {
        return CurrValue;
    }
    public void setCurrValue(String aCurrValue) {
        CurrValue = aCurrValue;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getUWPassFlag() {
        return UWPassFlag;
    }
    public void setUWPassFlag(String aUWPassFlag) {
        UWPassFlag = aUWPassFlag;
    }
    public String getUWGrade() {
        return UWGrade;
    }
    public void setUWGrade(String aUWGrade) {
        UWGrade = aUWGrade;
    }
    public String getSugPassFlag() {
        return SugPassFlag;
    }
    public void setSugPassFlag(String aSugPassFlag) {
        SugPassFlag = aSugPassFlag;
    }
    public String getUWType() {
        return UWType;
    }
    public void setUWType(String aUWType) {
        UWType = aUWType;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ContNo") ) {
            return 1;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 2;
        }
        if( strFieldName.equals("UWNo") ) {
            return 3;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 4;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 5;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 6;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 7;
        }
        if( strFieldName.equals("AppntName") ) {
            return 8;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 9;
        }
        if( strFieldName.equals("UWRuleCode") ) {
            return 10;
        }
        if( strFieldName.equals("UWError") ) {
            return 11;
        }
        if( strFieldName.equals("CurrValue") ) {
            return 12;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 13;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 14;
        }
        if( strFieldName.equals("UWPassFlag") ) {
            return 15;
        }
        if( strFieldName.equals("UWGrade") ) {
            return 16;
        }
        if( strFieldName.equals("SugPassFlag") ) {
            return 17;
        }
        if( strFieldName.equals("UWType") ) {
            return 18;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ContNo";
                break;
            case 2:
                strFieldName = "ProposalContNo";
                break;
            case 3:
                strFieldName = "UWNo";
                break;
            case 4:
                strFieldName = "SerialNo";
                break;
            case 5:
                strFieldName = "InsuredNo";
                break;
            case 6:
                strFieldName = "InsuredName";
                break;
            case 7:
                strFieldName = "AppntNo";
                break;
            case 8:
                strFieldName = "AppntName";
                break;
            case 9:
                strFieldName = "ManageCom";
                break;
            case 10:
                strFieldName = "UWRuleCode";
                break;
            case 11:
                strFieldName = "UWError";
                break;
            case 12:
                strFieldName = "CurrValue";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            case 15:
                strFieldName = "UWPassFlag";
                break;
            case 16:
                strFieldName = "UWGrade";
                break;
            case 17:
                strFieldName = "SugPassFlag";
                break;
            case 18:
                strFieldName = "UWType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "UWNO":
                return Schema.TYPE_INT;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "UWRULECODE":
                return Schema.TYPE_STRING;
            case "UWERROR":
                return Schema.TYPE_STRING;
            case "CURRVALUE":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "UWPASSFLAG":
                return Schema.TYPE_STRING;
            case "UWGRADE":
                return Schema.TYPE_STRING;
            case "SUGPASSFLAG":
                return Schema.TYPE_STRING;
            case "UWTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_INT;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("UWNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWNo));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("UWRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWRuleCode));
        }
        if (FCode.equalsIgnoreCase("UWError")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWError));
        }
        if (FCode.equalsIgnoreCase("CurrValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CurrValue));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("UWPassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWPassFlag));
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWGrade));
        }
        if (FCode.equalsIgnoreCase("SugPassFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SugPassFlag));
        }
        if (FCode.equalsIgnoreCase("UWType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ProposalContNo);
                break;
            case 3:
                strFieldValue = String.valueOf(UWNo);
                break;
            case 4:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 5:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 6:
                strFieldValue = String.valueOf(InsuredName);
                break;
            case 7:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 8:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 9:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 10:
                strFieldValue = String.valueOf(UWRuleCode);
                break;
            case 11:
                strFieldValue = String.valueOf(UWError);
                break;
            case 12:
                strFieldValue = String.valueOf(CurrValue);
                break;
            case 13:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 14:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 15:
                strFieldValue = String.valueOf(UWPassFlag);
                break;
            case 16:
                strFieldValue = String.valueOf(UWGrade);
                break;
            case 17:
                strFieldValue = String.valueOf(SugPassFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(UWType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("UWNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                UWNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("UWRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWRuleCode = FValue.trim();
            }
            else
                UWRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("UWError")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWError = FValue.trim();
            }
            else
                UWError = null;
        }
        if (FCode.equalsIgnoreCase("CurrValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                CurrValue = FValue.trim();
            }
            else
                CurrValue = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("UWPassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWPassFlag = FValue.trim();
            }
            else
                UWPassFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
                UWGrade = null;
        }
        if (FCode.equalsIgnoreCase("SugPassFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SugPassFlag = FValue.trim();
            }
            else
                SugPassFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWType")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWType = FValue.trim();
            }
            else
                UWType = null;
        }
        return true;
    }


    public String toString() {
    return "LCIndUWErrorPojo [" +
            "GrpContNo="+GrpContNo +
            ", ContNo="+ContNo +
            ", ProposalContNo="+ProposalContNo +
            ", UWNo="+UWNo +
            ", SerialNo="+SerialNo +
            ", InsuredNo="+InsuredNo +
            ", InsuredName="+InsuredName +
            ", AppntNo="+AppntNo +
            ", AppntName="+AppntName +
            ", ManageCom="+ManageCom +
            ", UWRuleCode="+UWRuleCode +
            ", UWError="+UWError +
            ", CurrValue="+CurrValue +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", UWPassFlag="+UWPassFlag +
            ", UWGrade="+UWGrade +
            ", SugPassFlag="+SugPassFlag +
            ", UWType="+UWType +"]";
    }
}
