/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCTransTrackPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-20
 */
public class LCTransTrackPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long TransStatusID; 
    /** Shardingid */
    private String ShardingID; 
    /** 业务流水号 */
    private String TransCode; 
    /** 报文编号 */
    private String ReportNo; 
    /** 银行代码 */
    private String BankCode; 
    /** 银行机构代码 */
    private String BankBranch; 
    /** 银行网点代码 */
    private String BankNode; 
    /** 银行操作员代码 */
    private String BankOperator; 
    /** 交易流水号(银行) */
    private String TransNo; 
    /** 处理标志 */
    private String FuncFlag; 
    /** 交易日期 */
    private String  TransDate;
    /** 交易时间 */
    private String TransTime; 
    /** 区站(管理机构) */
    private String ManageCom; 
    /** 险种代码 */
    private String RiskCode; 
    /** 投保单号 */
    private String ProposalNo; 
    /** 印刷号 */
    private String PrtNo; 
    /** 保单号 */
    private String PolNo; 
    /** 批单号 */
    private String EdorNo; 
    /** 收据号 */
    private String TempFeeNo; 
    /** 交易金额 */
    private double TransAmnt; 
    /** 银行信用卡号 */
    private String BankAcc; 
    /** 返还码 */
    private String RCode; 
    /** 交易状态 */
    private String TransStatus; 
    /** 业务状态 */
    private String Status; 
    /** 描述 */
    private String Descr; 
    /** 备用字段 */
    private String Temp; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后修改日期 */
    private String  ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime; 
    /** 状态代码 */
    private String State_Code; 
    /** 内部服务流水号 */
    private String RequestId; 
    /** 外部的服务代码 */
    private String OutServiceCode; 
    /** 客户端ip */
    private String ClientIP; 
    /** 客户端port */
    private String ClientPort; 
    /** 渠道类型id */
    private String IssueWay; 
    /** 服务处理开始时间 */
    private String  ServiceStartTime;
    /** 服务处理结束时间 */
    private String  ServiceEndTime;
    /** 银行与midplat的对账比对结果 */
    private String RBankVSMP; 
    /** 银行与midplat的对账比对结果描述 */
    private String DesBankVSMP; 
    /** Midplat与核心的对账比对结果 */
    private String RMPVSKernel; 
    /** Midplat与核心的对账比对结果描述 */
    private String DesMPVSKernel; 
    /** 对账后续处理结果 */
    private String ResultBalance; 
    /** 对账后续处理结果描述 */
    private String DesBalance; 
    /** 备用字段2 */
    private String bak1; 
    /** 备用字段3 */
    private String bak2; 
    /** 备用字段4 */
    private String bak3; 
    /** 备用字段5 */
    private String bak4; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 销售方式 */
    private String SaleTyoe; 


    public static final int FIELDNUM = 50;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getTransStatusID() {
        return TransStatusID;
    }
    public void setTransStatusID(long aTransStatusID) {
        TransStatusID = aTransStatusID;
    }
    public void setTransStatusID(String aTransStatusID) {
        if (aTransStatusID != null && !aTransStatusID.equals("")) {
            TransStatusID = new Long(aTransStatusID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getTransCode() {
        return TransCode;
    }
    public void setTransCode(String aTransCode) {
        TransCode = aTransCode;
    }
    public String getReportNo() {
        return ReportNo;
    }
    public void setReportNo(String aReportNo) {
        ReportNo = aReportNo;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankBranch() {
        return BankBranch;
    }
    public void setBankBranch(String aBankBranch) {
        BankBranch = aBankBranch;
    }
    public String getBankNode() {
        return BankNode;
    }
    public void setBankNode(String aBankNode) {
        BankNode = aBankNode;
    }
    public String getBankOperator() {
        return BankOperator;
    }
    public void setBankOperator(String aBankOperator) {
        BankOperator = aBankOperator;
    }
    public String getTransNo() {
        return TransNo;
    }
    public void setTransNo(String aTransNo) {
        TransNo = aTransNo;
    }
    public String getFuncFlag() {
        return FuncFlag;
    }
    public void setFuncFlag(String aFuncFlag) {
        FuncFlag = aFuncFlag;
    }
    public String getTransDate() {
        return TransDate;
    }
    public void setTransDate(String aTransDate) {
        TransDate = aTransDate;
    }
    public String getTransTime() {
        return TransTime;
    }
    public void setTransTime(String aTransTime) {
        TransTime = aTransTime;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getTempFeeNo() {
        return TempFeeNo;
    }
    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }
    public double getTransAmnt() {
        return TransAmnt;
    }
    public void setTransAmnt(double aTransAmnt) {
        TransAmnt = aTransAmnt;
    }
    public void setTransAmnt(String aTransAmnt) {
        if (aTransAmnt != null && !aTransAmnt.equals("")) {
            Double tDouble = new Double(aTransAmnt);
            double d = tDouble.doubleValue();
            TransAmnt = d;
        }
    }

    public String getBankAcc() {
        return BankAcc;
    }
    public void setBankAcc(String aBankAcc) {
        BankAcc = aBankAcc;
    }
    public String getRCode() {
        return RCode;
    }
    public void setRCode(String aRCode) {
        RCode = aRCode;
    }
    public String getTransStatus() {
        return TransStatus;
    }
    public void setTransStatus(String aTransStatus) {
        TransStatus = aTransStatus;
    }
    public String getStatus() {
        return Status;
    }
    public void setStatus(String aStatus) {
        Status = aStatus;
    }
    public String getDescr() {
        return Descr;
    }
    public void setDescr(String aDescr) {
        Descr = aDescr;
    }
    public String getTemp() {
        return Temp;
    }
    public void setTemp(String aTemp) {
        Temp = aTemp;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getState_Code() {
        return State_Code;
    }
    public void setState_Code(String aState_Code) {
        State_Code = aState_Code;
    }
    public String getRequestId() {
        return RequestId;
    }
    public void setRequestId(String aRequestId) {
        RequestId = aRequestId;
    }
    public String getOutServiceCode() {
        return OutServiceCode;
    }
    public void setOutServiceCode(String aOutServiceCode) {
        OutServiceCode = aOutServiceCode;
    }
    public String getClientIP() {
        return ClientIP;
    }
    public void setClientIP(String aClientIP) {
        ClientIP = aClientIP;
    }
    public String getClientPort() {
        return ClientPort;
    }
    public void setClientPort(String aClientPort) {
        ClientPort = aClientPort;
    }
    public String getIssueWay() {
        return IssueWay;
    }
    public void setIssueWay(String aIssueWay) {
        IssueWay = aIssueWay;
    }
    public String getServiceStartTime() {
        return ServiceStartTime;
    }
    public void setServiceStartTime(String aServiceStartTime) {
        ServiceStartTime = aServiceStartTime;
    }
    public String getServiceEndTime() {
        return ServiceEndTime;
    }
    public void setServiceEndTime(String aServiceEndTime) {
        ServiceEndTime = aServiceEndTime;
    }
    public String getRBankVSMP() {
        return RBankVSMP;
    }
    public void setRBankVSMP(String aRBankVSMP) {
        RBankVSMP = aRBankVSMP;
    }
    public String getDesBankVSMP() {
        return DesBankVSMP;
    }
    public void setDesBankVSMP(String aDesBankVSMP) {
        DesBankVSMP = aDesBankVSMP;
    }
    public String getRMPVSKernel() {
        return RMPVSKernel;
    }
    public void setRMPVSKernel(String aRMPVSKernel) {
        RMPVSKernel = aRMPVSKernel;
    }
    public String getDesMPVSKernel() {
        return DesMPVSKernel;
    }
    public void setDesMPVSKernel(String aDesMPVSKernel) {
        DesMPVSKernel = aDesMPVSKernel;
    }
    public String getResultBalance() {
        return ResultBalance;
    }
    public void setResultBalance(String aResultBalance) {
        ResultBalance = aResultBalance;
    }
    public String getDesBalance() {
        return DesBalance;
    }
    public void setDesBalance(String aDesBalance) {
        DesBalance = aDesBalance;
    }
    public String getBak1() {
        return bak1;
    }
    public void setBak1(String abak1) {
        bak1 = abak1;
    }
    public String getBak2() {
        return bak2;
    }
    public void setBak2(String abak2) {
        bak2 = abak2;
    }
    public String getBak3() {
        return bak3;
    }
    public void setBak3(String abak3) {
        bak3 = abak3;
    }
    public String getBak4() {
        return bak4;
    }
    public void setBak4(String abak4) {
        bak4 = abak4;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getSaleTyoe() {
        return SaleTyoe;
    }
    public void setSaleTyoe(String aSaleTyoe) {
        SaleTyoe = aSaleTyoe;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TransStatusID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("TransCode") ) {
            return 2;
        }
        if( strFieldName.equals("ReportNo") ) {
            return 3;
        }
        if( strFieldName.equals("BankCode") ) {
            return 4;
        }
        if( strFieldName.equals("BankBranch") ) {
            return 5;
        }
        if( strFieldName.equals("BankNode") ) {
            return 6;
        }
        if( strFieldName.equals("BankOperator") ) {
            return 7;
        }
        if( strFieldName.equals("TransNo") ) {
            return 8;
        }
        if( strFieldName.equals("FuncFlag") ) {
            return 9;
        }
        if( strFieldName.equals("TransDate") ) {
            return 10;
        }
        if( strFieldName.equals("TransTime") ) {
            return 11;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 12;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 13;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 14;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 15;
        }
        if( strFieldName.equals("PolNo") ) {
            return 16;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 17;
        }
        if( strFieldName.equals("TempFeeNo") ) {
            return 18;
        }
        if( strFieldName.equals("TransAmnt") ) {
            return 19;
        }
        if( strFieldName.equals("BankAcc") ) {
            return 20;
        }
        if( strFieldName.equals("RCode") ) {
            return 21;
        }
        if( strFieldName.equals("TransStatus") ) {
            return 22;
        }
        if( strFieldName.equals("Status") ) {
            return 23;
        }
        if( strFieldName.equals("Descr") ) {
            return 24;
        }
        if( strFieldName.equals("Temp") ) {
            return 25;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 26;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 27;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 28;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 29;
        }
        if( strFieldName.equals("State_Code") ) {
            return 30;
        }
        if( strFieldName.equals("RequestId") ) {
            return 31;
        }
        if( strFieldName.equals("OutServiceCode") ) {
            return 32;
        }
        if( strFieldName.equals("ClientIP") ) {
            return 33;
        }
        if( strFieldName.equals("ClientPort") ) {
            return 34;
        }
        if( strFieldName.equals("IssueWay") ) {
            return 35;
        }
        if( strFieldName.equals("ServiceStartTime") ) {
            return 36;
        }
        if( strFieldName.equals("ServiceEndTime") ) {
            return 37;
        }
        if( strFieldName.equals("RBankVSMP") ) {
            return 38;
        }
        if( strFieldName.equals("DesBankVSMP") ) {
            return 39;
        }
        if( strFieldName.equals("RMPVSKernel") ) {
            return 40;
        }
        if( strFieldName.equals("DesMPVSKernel") ) {
            return 41;
        }
        if( strFieldName.equals("ResultBalance") ) {
            return 42;
        }
        if( strFieldName.equals("DesBalance") ) {
            return 43;
        }
        if( strFieldName.equals("bak1") ) {
            return 44;
        }
        if( strFieldName.equals("bak2") ) {
            return 45;
        }
        if( strFieldName.equals("bak3") ) {
            return 46;
        }
        if( strFieldName.equals("bak4") ) {
            return 47;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 48;
        }
        if( strFieldName.equals("SaleTyoe") ) {
            return 49;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TransStatusID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "TransCode";
                break;
            case 3:
                strFieldName = "ReportNo";
                break;
            case 4:
                strFieldName = "BankCode";
                break;
            case 5:
                strFieldName = "BankBranch";
                break;
            case 6:
                strFieldName = "BankNode";
                break;
            case 7:
                strFieldName = "BankOperator";
                break;
            case 8:
                strFieldName = "TransNo";
                break;
            case 9:
                strFieldName = "FuncFlag";
                break;
            case 10:
                strFieldName = "TransDate";
                break;
            case 11:
                strFieldName = "TransTime";
                break;
            case 12:
                strFieldName = "ManageCom";
                break;
            case 13:
                strFieldName = "RiskCode";
                break;
            case 14:
                strFieldName = "ProposalNo";
                break;
            case 15:
                strFieldName = "PrtNo";
                break;
            case 16:
                strFieldName = "PolNo";
                break;
            case 17:
                strFieldName = "EdorNo";
                break;
            case 18:
                strFieldName = "TempFeeNo";
                break;
            case 19:
                strFieldName = "TransAmnt";
                break;
            case 20:
                strFieldName = "BankAcc";
                break;
            case 21:
                strFieldName = "RCode";
                break;
            case 22:
                strFieldName = "TransStatus";
                break;
            case 23:
                strFieldName = "Status";
                break;
            case 24:
                strFieldName = "Descr";
                break;
            case 25:
                strFieldName = "Temp";
                break;
            case 26:
                strFieldName = "MakeDate";
                break;
            case 27:
                strFieldName = "MakeTime";
                break;
            case 28:
                strFieldName = "ModifyDate";
                break;
            case 29:
                strFieldName = "ModifyTime";
                break;
            case 30:
                strFieldName = "State_Code";
                break;
            case 31:
                strFieldName = "RequestId";
                break;
            case 32:
                strFieldName = "OutServiceCode";
                break;
            case 33:
                strFieldName = "ClientIP";
                break;
            case 34:
                strFieldName = "ClientPort";
                break;
            case 35:
                strFieldName = "IssueWay";
                break;
            case 36:
                strFieldName = "ServiceStartTime";
                break;
            case 37:
                strFieldName = "ServiceEndTime";
                break;
            case 38:
                strFieldName = "RBankVSMP";
                break;
            case 39:
                strFieldName = "DesBankVSMP";
                break;
            case 40:
                strFieldName = "RMPVSKernel";
                break;
            case 41:
                strFieldName = "DesMPVSKernel";
                break;
            case 42:
                strFieldName = "ResultBalance";
                break;
            case 43:
                strFieldName = "DesBalance";
                break;
            case 44:
                strFieldName = "bak1";
                break;
            case 45:
                strFieldName = "bak2";
                break;
            case 46:
                strFieldName = "bak3";
                break;
            case 47:
                strFieldName = "bak4";
                break;
            case 48:
                strFieldName = "SaleChnl";
                break;
            case 49:
                strFieldName = "SaleTyoe";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSSTATUSID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "TRANSCODE":
                return Schema.TYPE_STRING;
            case "REPORTNO":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKBRANCH":
                return Schema.TYPE_STRING;
            case "BANKNODE":
                return Schema.TYPE_STRING;
            case "BANKOPERATOR":
                return Schema.TYPE_STRING;
            case "TRANSNO":
                return Schema.TYPE_STRING;
            case "FUNCFLAG":
                return Schema.TYPE_STRING;
            case "TRANSDATE":
                return Schema.TYPE_STRING;
            case "TRANSTIME":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "TEMPFEENO":
                return Schema.TYPE_STRING;
            case "TRANSAMNT":
                return Schema.TYPE_DOUBLE;
            case "BANKACC":
                return Schema.TYPE_STRING;
            case "RCODE":
                return Schema.TYPE_STRING;
            case "TRANSSTATUS":
                return Schema.TYPE_STRING;
            case "STATUS":
                return Schema.TYPE_STRING;
            case "DESCR":
                return Schema.TYPE_STRING;
            case "TEMP":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STATE_CODE":
                return Schema.TYPE_STRING;
            case "REQUESTID":
                return Schema.TYPE_STRING;
            case "OUTSERVICECODE":
                return Schema.TYPE_STRING;
            case "CLIENTIP":
                return Schema.TYPE_STRING;
            case "CLIENTPORT":
                return Schema.TYPE_STRING;
            case "ISSUEWAY":
                return Schema.TYPE_STRING;
            case "SERVICESTARTTIME":
                return Schema.TYPE_STRING;
            case "SERVICEENDTIME":
                return Schema.TYPE_STRING;
            case "RBANKVSMP":
                return Schema.TYPE_STRING;
            case "DESBANKVSMP":
                return Schema.TYPE_STRING;
            case "RMPVSKERNEL":
                return Schema.TYPE_STRING;
            case "DESMPVSKERNEL":
                return Schema.TYPE_STRING;
            case "RESULTBALANCE":
                return Schema.TYPE_STRING;
            case "DESBALANCE":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            case "BAK3":
                return Schema.TYPE_STRING;
            case "BAK4":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "SALETYOE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TransStatusID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransStatusID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
        }
        if (FCode.equalsIgnoreCase("ReportNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportNo));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankBranch));
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
        }
        if (FCode.equalsIgnoreCase("BankOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankOperator));
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FuncFlag));
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransDate));
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransTime));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equalsIgnoreCase("TransAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransAmnt));
        }
        if (FCode.equalsIgnoreCase("BankAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAcc));
        }
        if (FCode.equalsIgnoreCase("RCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RCode));
        }
        if (FCode.equalsIgnoreCase("TransStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransStatus));
        }
        if (FCode.equalsIgnoreCase("Status")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
        }
        if (FCode.equalsIgnoreCase("Descr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Descr));
        }
        if (FCode.equalsIgnoreCase("Temp")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Temp));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("State_Code")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State_Code));
        }
        if (FCode.equalsIgnoreCase("RequestId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RequestId));
        }
        if (FCode.equalsIgnoreCase("OutServiceCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutServiceCode));
        }
        if (FCode.equalsIgnoreCase("ClientIP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClientIP));
        }
        if (FCode.equalsIgnoreCase("ClientPort")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClientPort));
        }
        if (FCode.equalsIgnoreCase("IssueWay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IssueWay));
        }
        if (FCode.equalsIgnoreCase("ServiceStartTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceStartTime));
        }
        if (FCode.equalsIgnoreCase("ServiceEndTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceEndTime));
        }
        if (FCode.equalsIgnoreCase("RBankVSMP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RBankVSMP));
        }
        if (FCode.equalsIgnoreCase("DesBankVSMP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DesBankVSMP));
        }
        if (FCode.equalsIgnoreCase("RMPVSKernel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RMPVSKernel));
        }
        if (FCode.equalsIgnoreCase("DesMPVSKernel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DesMPVSKernel));
        }
        if (FCode.equalsIgnoreCase("ResultBalance")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultBalance));
        }
        if (FCode.equalsIgnoreCase("DesBalance")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DesBalance));
        }
        if (FCode.equalsIgnoreCase("bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
        }
        if (FCode.equalsIgnoreCase("bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
        }
        if (FCode.equalsIgnoreCase("bak3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
        }
        if (FCode.equalsIgnoreCase("bak4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("SaleTyoe")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleTyoe));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TransStatusID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(TransCode);
                break;
            case 3:
                strFieldValue = String.valueOf(ReportNo);
                break;
            case 4:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 5:
                strFieldValue = String.valueOf(BankBranch);
                break;
            case 6:
                strFieldValue = String.valueOf(BankNode);
                break;
            case 7:
                strFieldValue = String.valueOf(BankOperator);
                break;
            case 8:
                strFieldValue = String.valueOf(TransNo);
                break;
            case 9:
                strFieldValue = String.valueOf(FuncFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(TransDate);
                break;
            case 11:
                strFieldValue = String.valueOf(TransTime);
                break;
            case 12:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 13:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 14:
                strFieldValue = String.valueOf(ProposalNo);
                break;
            case 15:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 16:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 17:
                strFieldValue = String.valueOf(EdorNo);
                break;
            case 18:
                strFieldValue = String.valueOf(TempFeeNo);
                break;
            case 19:
                strFieldValue = String.valueOf(TransAmnt);
                break;
            case 20:
                strFieldValue = String.valueOf(BankAcc);
                break;
            case 21:
                strFieldValue = String.valueOf(RCode);
                break;
            case 22:
                strFieldValue = String.valueOf(TransStatus);
                break;
            case 23:
                strFieldValue = String.valueOf(Status);
                break;
            case 24:
                strFieldValue = String.valueOf(Descr);
                break;
            case 25:
                strFieldValue = String.valueOf(Temp);
                break;
            case 26:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 27:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 28:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 29:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 30:
                strFieldValue = String.valueOf(State_Code);
                break;
            case 31:
                strFieldValue = String.valueOf(RequestId);
                break;
            case 32:
                strFieldValue = String.valueOf(OutServiceCode);
                break;
            case 33:
                strFieldValue = String.valueOf(ClientIP);
                break;
            case 34:
                strFieldValue = String.valueOf(ClientPort);
                break;
            case 35:
                strFieldValue = String.valueOf(IssueWay);
                break;
            case 36:
                strFieldValue = String.valueOf(ServiceStartTime);
                break;
            case 37:
                strFieldValue = String.valueOf(ServiceEndTime);
                break;
            case 38:
                strFieldValue = String.valueOf(RBankVSMP);
                break;
            case 39:
                strFieldValue = String.valueOf(DesBankVSMP);
                break;
            case 40:
                strFieldValue = String.valueOf(RMPVSKernel);
                break;
            case 41:
                strFieldValue = String.valueOf(DesMPVSKernel);
                break;
            case 42:
                strFieldValue = String.valueOf(ResultBalance);
                break;
            case 43:
                strFieldValue = String.valueOf(DesBalance);
                break;
            case 44:
                strFieldValue = String.valueOf(bak1);
                break;
            case 45:
                strFieldValue = String.valueOf(bak2);
                break;
            case 46:
                strFieldValue = String.valueOf(bak3);
                break;
            case 47:
                strFieldValue = String.valueOf(bak4);
                break;
            case 48:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 49:
                strFieldValue = String.valueOf(SaleTyoe);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TransStatusID")) {
            if( FValue != null && !FValue.equals("")) {
                TransStatusID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransCode = FValue.trim();
            }
            else
                TransCode = null;
        }
        if (FCode.equalsIgnoreCase("ReportNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportNo = FValue.trim();
            }
            else
                ReportNo = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankBranch = FValue.trim();
            }
            else
                BankBranch = null;
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankNode = FValue.trim();
            }
            else
                BankNode = null;
        }
        if (FCode.equalsIgnoreCase("BankOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankOperator = FValue.trim();
            }
            else
                BankOperator = null;
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransNo = FValue.trim();
            }
            else
                TransNo = null;
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FuncFlag = FValue.trim();
            }
            else
                FuncFlag = null;
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransDate = FValue.trim();
            }
            else
                TransDate = null;
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransTime = FValue.trim();
            }
            else
                TransTime = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
                TempFeeNo = null;
        }
        if (FCode.equalsIgnoreCase("TransAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TransAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("BankAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAcc = FValue.trim();
            }
            else
                BankAcc = null;
        }
        if (FCode.equalsIgnoreCase("RCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RCode = FValue.trim();
            }
            else
                RCode = null;
        }
        if (FCode.equalsIgnoreCase("TransStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransStatus = FValue.trim();
            }
            else
                TransStatus = null;
        }
        if (FCode.equalsIgnoreCase("Status")) {
            if( FValue != null && !FValue.equals(""))
            {
                Status = FValue.trim();
            }
            else
                Status = null;
        }
        if (FCode.equalsIgnoreCase("Descr")) {
            if( FValue != null && !FValue.equals(""))
            {
                Descr = FValue.trim();
            }
            else
                Descr = null;
        }
        if (FCode.equalsIgnoreCase("Temp")) {
            if( FValue != null && !FValue.equals(""))
            {
                Temp = FValue.trim();
            }
            else
                Temp = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("State_Code")) {
            if( FValue != null && !FValue.equals(""))
            {
                State_Code = FValue.trim();
            }
            else
                State_Code = null;
        }
        if (FCode.equalsIgnoreCase("RequestId")) {
            if( FValue != null && !FValue.equals(""))
            {
                RequestId = FValue.trim();
            }
            else
                RequestId = null;
        }
        if (FCode.equalsIgnoreCase("OutServiceCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutServiceCode = FValue.trim();
            }
            else
                OutServiceCode = null;
        }
        if (FCode.equalsIgnoreCase("ClientIP")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClientIP = FValue.trim();
            }
            else
                ClientIP = null;
        }
        if (FCode.equalsIgnoreCase("ClientPort")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClientPort = FValue.trim();
            }
            else
                ClientPort = null;
        }
        if (FCode.equalsIgnoreCase("IssueWay")) {
            if( FValue != null && !FValue.equals(""))
            {
                IssueWay = FValue.trim();
            }
            else
                IssueWay = null;
        }
        if (FCode.equalsIgnoreCase("ServiceStartTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceStartTime = FValue.trim();
            }
            else
                ServiceStartTime = null;
        }
        if (FCode.equalsIgnoreCase("ServiceEndTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceEndTime = FValue.trim();
            }
            else
                ServiceEndTime = null;
        }
        if (FCode.equalsIgnoreCase("RBankVSMP")) {
            if( FValue != null && !FValue.equals(""))
            {
                RBankVSMP = FValue.trim();
            }
            else
                RBankVSMP = null;
        }
        if (FCode.equalsIgnoreCase("DesBankVSMP")) {
            if( FValue != null && !FValue.equals(""))
            {
                DesBankVSMP = FValue.trim();
            }
            else
                DesBankVSMP = null;
        }
        if (FCode.equalsIgnoreCase("RMPVSKernel")) {
            if( FValue != null && !FValue.equals(""))
            {
                RMPVSKernel = FValue.trim();
            }
            else
                RMPVSKernel = null;
        }
        if (FCode.equalsIgnoreCase("DesMPVSKernel")) {
            if( FValue != null && !FValue.equals(""))
            {
                DesMPVSKernel = FValue.trim();
            }
            else
                DesMPVSKernel = null;
        }
        if (FCode.equalsIgnoreCase("ResultBalance")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultBalance = FValue.trim();
            }
            else
                ResultBalance = null;
        }
        if (FCode.equalsIgnoreCase("DesBalance")) {
            if( FValue != null && !FValue.equals(""))
            {
                DesBalance = FValue.trim();
            }
            else
                DesBalance = null;
        }
        if (FCode.equalsIgnoreCase("bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                bak1 = FValue.trim();
            }
            else
                bak1 = null;
        }
        if (FCode.equalsIgnoreCase("bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                bak2 = FValue.trim();
            }
            else
                bak2 = null;
        }
        if (FCode.equalsIgnoreCase("bak3")) {
            if( FValue != null && !FValue.equals(""))
            {
                bak3 = FValue.trim();
            }
            else
                bak3 = null;
        }
        if (FCode.equalsIgnoreCase("bak4")) {
            if( FValue != null && !FValue.equals(""))
            {
                bak4 = FValue.trim();
            }
            else
                bak4 = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("SaleTyoe")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleTyoe = FValue.trim();
            }
            else
                SaleTyoe = null;
        }
        return true;
    }


    public String toString() {
    return "LCTransTrackPojo [" +
            "TransStatusID="+TransStatusID +
            ", ShardingID="+ShardingID +
            ", TransCode="+TransCode +
            ", ReportNo="+ReportNo +
            ", BankCode="+BankCode +
            ", BankBranch="+BankBranch +
            ", BankNode="+BankNode +
            ", BankOperator="+BankOperator +
            ", TransNo="+TransNo +
            ", FuncFlag="+FuncFlag +
            ", TransDate="+TransDate +
            ", TransTime="+TransTime +
            ", ManageCom="+ManageCom +
            ", RiskCode="+RiskCode +
            ", ProposalNo="+ProposalNo +
            ", PrtNo="+PrtNo +
            ", PolNo="+PolNo +
            ", EdorNo="+EdorNo +
            ", TempFeeNo="+TempFeeNo +
            ", TransAmnt="+TransAmnt +
            ", BankAcc="+BankAcc +
            ", RCode="+RCode +
            ", TransStatus="+TransStatus +
            ", Status="+Status +
            ", Descr="+Descr +
            ", Temp="+Temp +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", State_Code="+State_Code +
            ", RequestId="+RequestId +
            ", OutServiceCode="+OutServiceCode +
            ", ClientIP="+ClientIP +
            ", ClientPort="+ClientPort +
            ", IssueWay="+IssueWay +
            ", ServiceStartTime="+ServiceStartTime +
            ", ServiceEndTime="+ServiceEndTime +
            ", RBankVSMP="+RBankVSMP +
            ", DesBankVSMP="+DesBankVSMP +
            ", RMPVSKernel="+RMPVSKernel +
            ", DesMPVSKernel="+DesMPVSKernel +
            ", ResultBalance="+ResultBalance +
            ", DesBalance="+DesBalance +
            ", bak1="+bak1 +
            ", bak2="+bak2 +
            ", bak3="+bak3 +
            ", bak4="+bak4 +
            ", SaleChnl="+SaleChnl +
            ", SaleTyoe="+SaleTyoe +"]";
    }
}
