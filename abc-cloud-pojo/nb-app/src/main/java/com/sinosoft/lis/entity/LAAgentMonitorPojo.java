/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAAgentMonitorPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LAAgentMonitorPojo implements Pojo,Serializable {
    // @Field
    /** 代理人编码 */
    @RedisPrimaryHKey
    private String AgentCode; 
    /** 监控类型 */
    @RedisPrimaryHKey
    private String MonitorType; 
    /** 监控号 */
    @RedisPrimaryHKey
    private int MonitorNo; 
    /** 管理机构 */
    private String ManageCom; 
    /** 代理人展业机构代码 */
    private String AgentGroup; 
    /** 代理人姓名 */
    private String AgentName; 
    /** 代理人性别 */
    private String AgentSex; 
    /** 出生日期 */
    private String  Birthday;
    /** 证件类型 */
    private String IDNoType; 
    /** 证件号码 */
    private String IDNo; 
    /** 展业机构外部编码 */
    private String BranchAttr; 
    /** 展业机构管理人员 */
    private String BranchManager; 
    /** 展业机构名称 */
    private String BranchGroupName; 
    /** 监控状态 */
    private String MonitorState; 
    /** 监控日期 */
    private String  MonitorDate;
    /** 解除日期 */
    private String  CancelDate;
    /** 上报部门 */
    private String ReportCom; 
    /** 上报原因 */
    private String ReportReason; 
    /** 解除部门 */
    private String CancelCom; 
    /** 解除原因 */
    private String CancelReason; 
    /** 操作员 */
    private String Operator; 
    /** 操作日期 */
    private String  MakeDate;
    /** 操作时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** 备用1 */
    private String StandByFlag1; 
    /** 备用2 */
    private String StandByFlag2; 
    /** 备用3 */
    private String StandByFlag3; 
    /** 备用4 */
    private String StandByFlag4; 
    /** 备用5 */
    private String StandByFlag5; 
    /** 备用6 */
    private String StandByFlag6; 
    /** 备用7 */
    private String StandByFlag7; 
    /** 备用8 */
    private String StandByFlag8; 
    /** 备用9 */
    private String StandByFlag9; 
    /** 备用10 */
    private String StandByFlag10; 
    /** 备用11 */
    private String StandByFlag11; 
    /** 备用12 */
    private String StandByFlag12; 
    /** 备用13 */
    private String StandByFlag13; 
    /** 备用14 */
    private String StandByFlag14; 
    /** 备用15 */
    private String StandByFlag15; 


    public static final int FIELDNUM = 40;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getMonitorType() {
        return MonitorType;
    }
    public void setMonitorType(String aMonitorType) {
        MonitorType = aMonitorType;
    }
    public int getMonitorNo() {
        return MonitorNo;
    }
    public void setMonitorNo(int aMonitorNo) {
        MonitorNo = aMonitorNo;
    }
    public void setMonitorNo(String aMonitorNo) {
        if (aMonitorNo != null && !aMonitorNo.equals("")) {
            Integer tInteger = new Integer(aMonitorNo);
            int i = tInteger.intValue();
            MonitorNo = i;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getAgentSex() {
        return AgentSex;
    }
    public void setAgentSex(String aAgentSex) {
        AgentSex = aAgentSex;
    }
    public String getBirthday() {
        return Birthday;
    }
    public void setBirthday(String aBirthday) {
        Birthday = aBirthday;
    }
    public String getIDNoType() {
        return IDNoType;
    }
    public void setIDNoType(String aIDNoType) {
        IDNoType = aIDNoType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getBranchAttr() {
        return BranchAttr;
    }
    public void setBranchAttr(String aBranchAttr) {
        BranchAttr = aBranchAttr;
    }
    public String getBranchManager() {
        return BranchManager;
    }
    public void setBranchManager(String aBranchManager) {
        BranchManager = aBranchManager;
    }
    public String getBranchGroupName() {
        return BranchGroupName;
    }
    public void setBranchGroupName(String aBranchGroupName) {
        BranchGroupName = aBranchGroupName;
    }
    public String getMonitorState() {
        return MonitorState;
    }
    public void setMonitorState(String aMonitorState) {
        MonitorState = aMonitorState;
    }
    public String getMonitorDate() {
        return MonitorDate;
    }
    public void setMonitorDate(String aMonitorDate) {
        MonitorDate = aMonitorDate;
    }
    public String getCancelDate() {
        return CancelDate;
    }
    public void setCancelDate(String aCancelDate) {
        CancelDate = aCancelDate;
    }
    public String getReportCom() {
        return ReportCom;
    }
    public void setReportCom(String aReportCom) {
        ReportCom = aReportCom;
    }
    public String getReportReason() {
        return ReportReason;
    }
    public void setReportReason(String aReportReason) {
        ReportReason = aReportReason;
    }
    public String getCancelCom() {
        return CancelCom;
    }
    public void setCancelCom(String aCancelCom) {
        CancelCom = aCancelCom;
    }
    public String getCancelReason() {
        return CancelReason;
    }
    public void setCancelReason(String aCancelReason) {
        CancelReason = aCancelReason;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getStandByFlag2() {
        return StandByFlag2;
    }
    public void setStandByFlag2(String aStandByFlag2) {
        StandByFlag2 = aStandByFlag2;
    }
    public String getStandByFlag3() {
        return StandByFlag3;
    }
    public void setStandByFlag3(String aStandByFlag3) {
        StandByFlag3 = aStandByFlag3;
    }
    public String getStandByFlag4() {
        return StandByFlag4;
    }
    public void setStandByFlag4(String aStandByFlag4) {
        StandByFlag4 = aStandByFlag4;
    }
    public String getStandByFlag5() {
        return StandByFlag5;
    }
    public void setStandByFlag5(String aStandByFlag5) {
        StandByFlag5 = aStandByFlag5;
    }
    public String getStandByFlag6() {
        return StandByFlag6;
    }
    public void setStandByFlag6(String aStandByFlag6) {
        StandByFlag6 = aStandByFlag6;
    }
    public String getStandByFlag7() {
        return StandByFlag7;
    }
    public void setStandByFlag7(String aStandByFlag7) {
        StandByFlag7 = aStandByFlag7;
    }
    public String getStandByFlag8() {
        return StandByFlag8;
    }
    public void setStandByFlag8(String aStandByFlag8) {
        StandByFlag8 = aStandByFlag8;
    }
    public String getStandByFlag9() {
        return StandByFlag9;
    }
    public void setStandByFlag9(String aStandByFlag9) {
        StandByFlag9 = aStandByFlag9;
    }
    public String getStandByFlag10() {
        return StandByFlag10;
    }
    public void setStandByFlag10(String aStandByFlag10) {
        StandByFlag10 = aStandByFlag10;
    }
    public String getStandByFlag11() {
        return StandByFlag11;
    }
    public void setStandByFlag11(String aStandByFlag11) {
        StandByFlag11 = aStandByFlag11;
    }
    public String getStandByFlag12() {
        return StandByFlag12;
    }
    public void setStandByFlag12(String aStandByFlag12) {
        StandByFlag12 = aStandByFlag12;
    }
    public String getStandByFlag13() {
        return StandByFlag13;
    }
    public void setStandByFlag13(String aStandByFlag13) {
        StandByFlag13 = aStandByFlag13;
    }
    public String getStandByFlag14() {
        return StandByFlag14;
    }
    public void setStandByFlag14(String aStandByFlag14) {
        StandByFlag14 = aStandByFlag14;
    }
    public String getStandByFlag15() {
        return StandByFlag15;
    }
    public void setStandByFlag15(String aStandByFlag15) {
        StandByFlag15 = aStandByFlag15;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCode") ) {
            return 0;
        }
        if( strFieldName.equals("MonitorType") ) {
            return 1;
        }
        if( strFieldName.equals("MonitorNo") ) {
            return 2;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 3;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 4;
        }
        if( strFieldName.equals("AgentName") ) {
            return 5;
        }
        if( strFieldName.equals("AgentSex") ) {
            return 6;
        }
        if( strFieldName.equals("Birthday") ) {
            return 7;
        }
        if( strFieldName.equals("IDNoType") ) {
            return 8;
        }
        if( strFieldName.equals("IDNo") ) {
            return 9;
        }
        if( strFieldName.equals("BranchAttr") ) {
            return 10;
        }
        if( strFieldName.equals("BranchManager") ) {
            return 11;
        }
        if( strFieldName.equals("BranchGroupName") ) {
            return 12;
        }
        if( strFieldName.equals("MonitorState") ) {
            return 13;
        }
        if( strFieldName.equals("MonitorDate") ) {
            return 14;
        }
        if( strFieldName.equals("CancelDate") ) {
            return 15;
        }
        if( strFieldName.equals("ReportCom") ) {
            return 16;
        }
        if( strFieldName.equals("ReportReason") ) {
            return 17;
        }
        if( strFieldName.equals("CancelCom") ) {
            return 18;
        }
        if( strFieldName.equals("CancelReason") ) {
            return 19;
        }
        if( strFieldName.equals("Operator") ) {
            return 20;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 21;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 24;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 25;
        }
        if( strFieldName.equals("StandByFlag2") ) {
            return 26;
        }
        if( strFieldName.equals("StandByFlag3") ) {
            return 27;
        }
        if( strFieldName.equals("StandByFlag4") ) {
            return 28;
        }
        if( strFieldName.equals("StandByFlag5") ) {
            return 29;
        }
        if( strFieldName.equals("StandByFlag6") ) {
            return 30;
        }
        if( strFieldName.equals("StandByFlag7") ) {
            return 31;
        }
        if( strFieldName.equals("StandByFlag8") ) {
            return 32;
        }
        if( strFieldName.equals("StandByFlag9") ) {
            return 33;
        }
        if( strFieldName.equals("StandByFlag10") ) {
            return 34;
        }
        if( strFieldName.equals("StandByFlag11") ) {
            return 35;
        }
        if( strFieldName.equals("StandByFlag12") ) {
            return 36;
        }
        if( strFieldName.equals("StandByFlag13") ) {
            return 37;
        }
        if( strFieldName.equals("StandByFlag14") ) {
            return 38;
        }
        if( strFieldName.equals("StandByFlag15") ) {
            return 39;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "MonitorType";
                break;
            case 2:
                strFieldName = "MonitorNo";
                break;
            case 3:
                strFieldName = "ManageCom";
                break;
            case 4:
                strFieldName = "AgentGroup";
                break;
            case 5:
                strFieldName = "AgentName";
                break;
            case 6:
                strFieldName = "AgentSex";
                break;
            case 7:
                strFieldName = "Birthday";
                break;
            case 8:
                strFieldName = "IDNoType";
                break;
            case 9:
                strFieldName = "IDNo";
                break;
            case 10:
                strFieldName = "BranchAttr";
                break;
            case 11:
                strFieldName = "BranchManager";
                break;
            case 12:
                strFieldName = "BranchGroupName";
                break;
            case 13:
                strFieldName = "MonitorState";
                break;
            case 14:
                strFieldName = "MonitorDate";
                break;
            case 15:
                strFieldName = "CancelDate";
                break;
            case 16:
                strFieldName = "ReportCom";
                break;
            case 17:
                strFieldName = "ReportReason";
                break;
            case 18:
                strFieldName = "CancelCom";
                break;
            case 19:
                strFieldName = "CancelReason";
                break;
            case 20:
                strFieldName = "Operator";
                break;
            case 21:
                strFieldName = "MakeDate";
                break;
            case 22:
                strFieldName = "MakeTime";
                break;
            case 23:
                strFieldName = "ModifyDate";
                break;
            case 24:
                strFieldName = "ModifyTime";
                break;
            case 25:
                strFieldName = "StandByFlag1";
                break;
            case 26:
                strFieldName = "StandByFlag2";
                break;
            case 27:
                strFieldName = "StandByFlag3";
                break;
            case 28:
                strFieldName = "StandByFlag4";
                break;
            case 29:
                strFieldName = "StandByFlag5";
                break;
            case 30:
                strFieldName = "StandByFlag6";
                break;
            case 31:
                strFieldName = "StandByFlag7";
                break;
            case 32:
                strFieldName = "StandByFlag8";
                break;
            case 33:
                strFieldName = "StandByFlag9";
                break;
            case 34:
                strFieldName = "StandByFlag10";
                break;
            case 35:
                strFieldName = "StandByFlag11";
                break;
            case 36:
                strFieldName = "StandByFlag12";
                break;
            case 37:
                strFieldName = "StandByFlag13";
                break;
            case 38:
                strFieldName = "StandByFlag14";
                break;
            case 39:
                strFieldName = "StandByFlag15";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "MONITORTYPE":
                return Schema.TYPE_STRING;
            case "MONITORNO":
                return Schema.TYPE_INT;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "AGENTSEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_STRING;
            case "IDNOTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "BRANCHATTR":
                return Schema.TYPE_STRING;
            case "BRANCHMANAGER":
                return Schema.TYPE_STRING;
            case "BRANCHGROUPNAME":
                return Schema.TYPE_STRING;
            case "MONITORSTATE":
                return Schema.TYPE_STRING;
            case "MONITORDATE":
                return Schema.TYPE_STRING;
            case "CANCELDATE":
                return Schema.TYPE_STRING;
            case "REPORTCOM":
                return Schema.TYPE_STRING;
            case "REPORTREASON":
                return Schema.TYPE_STRING;
            case "CANCELCOM":
                return Schema.TYPE_STRING;
            case "CANCELREASON":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG6":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG7":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG8":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG9":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG10":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG11":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG12":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG13":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG14":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG15":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_INT;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("MonitorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonitorType));
        }
        if (FCode.equalsIgnoreCase("MonitorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonitorNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("AgentSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
        }
        if (FCode.equalsIgnoreCase("IDNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equalsIgnoreCase("BranchManager")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchManager));
        }
        if (FCode.equalsIgnoreCase("BranchGroupName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchGroupName));
        }
        if (FCode.equalsIgnoreCase("MonitorState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonitorState));
        }
        if (FCode.equalsIgnoreCase("MonitorDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonitorDate));
        }
        if (FCode.equalsIgnoreCase("CancelDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CancelDate));
        }
        if (FCode.equalsIgnoreCase("ReportCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportCom));
        }
        if (FCode.equalsIgnoreCase("ReportReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportReason));
        }
        if (FCode.equalsIgnoreCase("CancelCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CancelCom));
        }
        if (FCode.equalsIgnoreCase("CancelReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CancelReason));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag3));
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag4));
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag5));
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag6));
        }
        if (FCode.equalsIgnoreCase("StandByFlag7")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag7));
        }
        if (FCode.equalsIgnoreCase("StandByFlag8")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag8));
        }
        if (FCode.equalsIgnoreCase("StandByFlag9")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag9));
        }
        if (FCode.equalsIgnoreCase("StandByFlag10")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag10));
        }
        if (FCode.equalsIgnoreCase("StandByFlag11")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag11));
        }
        if (FCode.equalsIgnoreCase("StandByFlag12")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag12));
        }
        if (FCode.equalsIgnoreCase("StandByFlag13")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag13));
        }
        if (FCode.equalsIgnoreCase("StandByFlag14")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag14));
        }
        if (FCode.equalsIgnoreCase("StandByFlag15")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag15));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 1:
                strFieldValue = String.valueOf(MonitorType);
                break;
            case 2:
                strFieldValue = String.valueOf(MonitorNo);
                break;
            case 3:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 4:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 5:
                strFieldValue = String.valueOf(AgentName);
                break;
            case 6:
                strFieldValue = String.valueOf(AgentSex);
                break;
            case 7:
                strFieldValue = String.valueOf(Birthday);
                break;
            case 8:
                strFieldValue = String.valueOf(IDNoType);
                break;
            case 9:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 10:
                strFieldValue = String.valueOf(BranchAttr);
                break;
            case 11:
                strFieldValue = String.valueOf(BranchManager);
                break;
            case 12:
                strFieldValue = String.valueOf(BranchGroupName);
                break;
            case 13:
                strFieldValue = String.valueOf(MonitorState);
                break;
            case 14:
                strFieldValue = String.valueOf(MonitorDate);
                break;
            case 15:
                strFieldValue = String.valueOf(CancelDate);
                break;
            case 16:
                strFieldValue = String.valueOf(ReportCom);
                break;
            case 17:
                strFieldValue = String.valueOf(ReportReason);
                break;
            case 18:
                strFieldValue = String.valueOf(CancelCom);
                break;
            case 19:
                strFieldValue = String.valueOf(CancelReason);
                break;
            case 20:
                strFieldValue = String.valueOf(Operator);
                break;
            case 21:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 22:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 23:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 24:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 25:
                strFieldValue = String.valueOf(StandByFlag1);
                break;
            case 26:
                strFieldValue = String.valueOf(StandByFlag2);
                break;
            case 27:
                strFieldValue = String.valueOf(StandByFlag3);
                break;
            case 28:
                strFieldValue = String.valueOf(StandByFlag4);
                break;
            case 29:
                strFieldValue = String.valueOf(StandByFlag5);
                break;
            case 30:
                strFieldValue = String.valueOf(StandByFlag6);
                break;
            case 31:
                strFieldValue = String.valueOf(StandByFlag7);
                break;
            case 32:
                strFieldValue = String.valueOf(StandByFlag8);
                break;
            case 33:
                strFieldValue = String.valueOf(StandByFlag9);
                break;
            case 34:
                strFieldValue = String.valueOf(StandByFlag10);
                break;
            case 35:
                strFieldValue = String.valueOf(StandByFlag11);
                break;
            case 36:
                strFieldValue = String.valueOf(StandByFlag12);
                break;
            case 37:
                strFieldValue = String.valueOf(StandByFlag13);
                break;
            case 38:
                strFieldValue = String.valueOf(StandByFlag14);
                break;
            case 39:
                strFieldValue = String.valueOf(StandByFlag15);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("MonitorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MonitorType = FValue.trim();
            }
            else
                MonitorType = null;
        }
        if (FCode.equalsIgnoreCase("MonitorNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MonitorNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("AgentSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentSex = FValue.trim();
            }
            else
                AgentSex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                Birthday = FValue.trim();
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IDNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNoType = FValue.trim();
            }
            else
                IDNoType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
                BranchAttr = null;
        }
        if (FCode.equalsIgnoreCase("BranchManager")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchManager = FValue.trim();
            }
            else
                BranchManager = null;
        }
        if (FCode.equalsIgnoreCase("BranchGroupName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchGroupName = FValue.trim();
            }
            else
                BranchGroupName = null;
        }
        if (FCode.equalsIgnoreCase("MonitorState")) {
            if( FValue != null && !FValue.equals(""))
            {
                MonitorState = FValue.trim();
            }
            else
                MonitorState = null;
        }
        if (FCode.equalsIgnoreCase("MonitorDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MonitorDate = FValue.trim();
            }
            else
                MonitorDate = null;
        }
        if (FCode.equalsIgnoreCase("CancelDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CancelDate = FValue.trim();
            }
            else
                CancelDate = null;
        }
        if (FCode.equalsIgnoreCase("ReportCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportCom = FValue.trim();
            }
            else
                ReportCom = null;
        }
        if (FCode.equalsIgnoreCase("ReportReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportReason = FValue.trim();
            }
            else
                ReportReason = null;
        }
        if (FCode.equalsIgnoreCase("CancelCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                CancelCom = FValue.trim();
            }
            else
                CancelCom = null;
        }
        if (FCode.equalsIgnoreCase("CancelReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                CancelReason = FValue.trim();
            }
            else
                CancelReason = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag2 = FValue.trim();
            }
            else
                StandByFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag3 = FValue.trim();
            }
            else
                StandByFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag4 = FValue.trim();
            }
            else
                StandByFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag5 = FValue.trim();
            }
            else
                StandByFlag5 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag6 = FValue.trim();
            }
            else
                StandByFlag6 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag7")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag7 = FValue.trim();
            }
            else
                StandByFlag7 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag8")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag8 = FValue.trim();
            }
            else
                StandByFlag8 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag9")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag9 = FValue.trim();
            }
            else
                StandByFlag9 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag10")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag10 = FValue.trim();
            }
            else
                StandByFlag10 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag11")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag11 = FValue.trim();
            }
            else
                StandByFlag11 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag12")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag12 = FValue.trim();
            }
            else
                StandByFlag12 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag13")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag13 = FValue.trim();
            }
            else
                StandByFlag13 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag14")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag14 = FValue.trim();
            }
            else
                StandByFlag14 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag15")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag15 = FValue.trim();
            }
            else
                StandByFlag15 = null;
        }
        return true;
    }


    public String toString() {
    return "LAAgentMonitorPojo [" +
            "AgentCode="+AgentCode +
            ", MonitorType="+MonitorType +
            ", MonitorNo="+MonitorNo +
            ", ManageCom="+ManageCom +
            ", AgentGroup="+AgentGroup +
            ", AgentName="+AgentName +
            ", AgentSex="+AgentSex +
            ", Birthday="+Birthday +
            ", IDNoType="+IDNoType +
            ", IDNo="+IDNo +
            ", BranchAttr="+BranchAttr +
            ", BranchManager="+BranchManager +
            ", BranchGroupName="+BranchGroupName +
            ", MonitorState="+MonitorState +
            ", MonitorDate="+MonitorDate +
            ", CancelDate="+CancelDate +
            ", ReportCom="+ReportCom +
            ", ReportReason="+ReportReason +
            ", CancelCom="+CancelCom +
            ", CancelReason="+CancelReason +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", StandByFlag1="+StandByFlag1 +
            ", StandByFlag2="+StandByFlag2 +
            ", StandByFlag3="+StandByFlag3 +
            ", StandByFlag4="+StandByFlag4 +
            ", StandByFlag5="+StandByFlag5 +
            ", StandByFlag6="+StandByFlag6 +
            ", StandByFlag7="+StandByFlag7 +
            ", StandByFlag8="+StandByFlag8 +
            ", StandByFlag9="+StandByFlag9 +
            ", StandByFlag10="+StandByFlag10 +
            ", StandByFlag11="+StandByFlag11 +
            ", StandByFlag12="+StandByFlag12 +
            ", StandByFlag13="+StandByFlag13 +
            ", StandByFlag14="+StandByFlag14 +
            ", StandByFlag15="+StandByFlag15 +"]";
    }
}
