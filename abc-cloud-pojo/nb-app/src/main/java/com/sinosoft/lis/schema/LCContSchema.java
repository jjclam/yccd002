/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCContDB;

/**
 * <p>ClassName: LCContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-03-20
 */
public class LCContSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long ContID;
    /** Shardingid */
    private String ShardingID;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 总单类型 */
    private String ContType;
    /** 家庭单类型 */
    private String FamilyType;
    /** 家庭保障号 */
    private String FamilyID;
    /** 保单类型标记 */
    private String PolType;
    /** 卡单标志 */
    private String CardFlag;
    /** 管理机构 */
    private String ManageCom;
    /** 处理机构 */
    private String ExecuteCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 联合代理人代码 */
    private String AgentCode1;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 销售渠道 */
    private String SaleChnl;
    /** 经办人 */
    private String Handler;
    /** 保单口令 */
    private String Password;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 投保人性别 */
    private String AppntSex;
    /** 投保人出生日期 */
    private Date AppntBirthday;
    /** 投保人证件类型 */
    private String AppntIDType;
    /** 投保人证件号码 */
    private String AppntIDNo;
    /** 被保人客户号 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 被保人性别 */
    private String InsuredSex;
    /** 被保人出生日期 */
    private Date InsuredBirthday;
    /** 证件类型 */
    private String InsuredIDType;
    /** 证件号码 */
    private String InsuredIDNo;
    /** 交费间隔 */
    private int PayIntv;
    /** 交费方式 */
    private String PayMode;
    /** 交费位置 */
    private String PayLocation;
    /** 合同争议处理方式 */
    private String DisputedFlag;
    /** 溢交处理方式 */
    private String OutPayFlag;
    /** 保单送达方式 */
    private String GetPolMode;
    /** 签单机构 */
    private String SignCom;
    /** 签单日期 */
    private Date SignDate;
    /** 签单时间 */
    private String SignTime;
    /** 银行委托书号码 */
    private String ConsignNo;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 银行帐户名 */
    private String AccName;
    /** 保单打印次数 */
    private int PrintCount;
    /** 遗失补发次数 */
    private int LostTimes;
    /** 语种标记 */
    private String Lang;
    /** 币别 */
    private String Currency;
    /** 备注 */
    private String Remark;
    /** 人数 */
    private int Peoples;
    /** 份数 */
    private double Mult;
    /** 保费 */
    private double Prem;
    /** 保额 */
    private double Amnt;
    /** 累计保费 */
    private double SumPrem;
    /** 余额 */
    private double Dif;
    /** 交至日期 */
    private Date PaytoDate;
    /** 首期交费日期 */
    private Date FirstPayDate;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 录单人 */
    private String InputOperator;
    /** 录单完成日期 */
    private Date InputDate;
    /** 录单完成时间 */
    private String InputTime;
    /** 复核状态 */
    private String ApproveFlag;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 核保状态 */
    private String UWFlag;
    /** 核保人 */
    private String UWOperator;
    /** 核保完成日期 */
    private Date UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 投保单/保单标志 */
    private String AppFlag;
    /** 投保单申请日期 */
    private Date PolApplyDate;
    /** 保单送达日期 */
    private Date GetPolDate;
    /** 保单送达时间 */
    private String GetPolTime;
    /** 保单回执客户签收日期 */
    private Date CustomGetPolDate;
    /** 状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 初审人 */
    private String FirstTrialOperator;
    /** 初审日期 */
    private Date FirstTrialDate;
    /** 初审时间 */
    private String FirstTrialTime;
    /** 收单人 */
    private String ReceiveOperator;
    /** 收单日期 */
    private Date ReceiveDate;
    /** 收单时间 */
    private String ReceiveTime;
    /** 暂收据号 */
    private String TempFeeNo;
    /** 销售方式 */
    private String SellType;
    /** 强制人工核保标志 */
    private String ForceUWFlag;
    /** 强制人工核保原因 */
    private String ForceUWReason;
    /** 首期银行编码 */
    private String NewBankCode;
    /** 首期银行帐号 */
    private String NewBankAccNo;
    /** 首期银行帐户名 */
    private String NewAccName;
    /** 首期交费方式 */
    private String NewPayMode;
    /** 银代银行代码 */
    private String AgentBankCode;
    /** 银代柜员 */
    private String BankAgent;
    /** 银行柜员姓名 */
    private String BankAgentName;
    /** 银行柜员电话 */
    private String BankAgentTel;
    /** 组合险代码 */
    private String ProdSetCode;
    /** 中介平台保单号码 */
    private String PolicyNo;
    /** 单证印刷号码 */
    private String BillPressNo;
    /** 单证类型码 */
    private String CardTypeCode;
    /** 回访日期 */
    private Date VisitDate;
    /** 回访时间 */
    private String VisitTime;
    /** 销售机构 */
    private String SaleCom;
    /** 打印标志 */
    private String PrintFlag;
    /** 发票标志 */
    private String InvoicePrtFlag;
    /** 临分标记 */
    private String NewReinsureFlag;
    /** 续期交费提示 */
    private String RenewPayFlag;
    /** Appntfirstname */
    private String AppntFirstName;
    /** Appntlastname */
    private String AppntLastName;
    /** Insuredfirstname */
    private String InsuredFirstName;
    /** Insuredlastname */
    private String InsuredLastName;
    /** 授权标记 */
    private String AuthorFlag;
    /** 绿色通道 */
    private int GreenChnl;
    /** 出单方式 */
    private String TBType;
    /** 电子签名 */
    private String EAuto;
    /** 保单形式 */
    private String SlipForm;
    /** 自动垫交标志 */
    private String AutoPayFlag;
    /** 续保标志 */
    private int RnewFlag;
    /** 家庭保单号码 */
    private String FamilyContNo;
    /** 商业因素标准体承保标志 */
    private String BussFlag;
    /** 初审员签名 */
    private String SignName;
    /** 合同成立日期 */
    private Date OrganizeDate;
    /** 合同成立时间 */
    private String OrganizeTime;
    /** 首期自动发盘标志 */
    private String NewAutoSendBankFlag;
    /** 综拓专员编码 */
    private String AgentCodeOper;
    /** 综拓助理编码 */
    private String AgentCodeAssi;
    /** 延迟送达  因代码 */
    private String DelayReasonCode;
    /** 延迟送达原因 */
    private String DelayReasonDesc;
    /** 续期缴费提示 */
    private String XQremindflag;
    /** 组织机构代码 */
    private String OrganComCode;
    /** 开户行所在省 */
    private String BankProivnce;
    /** 首期开户行所在省 */
    private String NewBankProivnce;
    /** 仲裁机构 */
    private String ArbitrationCom;
    /** 印刷单证号 */
    private String OtherPrtno;
    /** 指定转账日期 */
    private Date DestAccountDate;
    /** 缴费次数 */
    private String PrePayCount;
    /** 首期开户行所在城市 */
    private String NewBankCity;
    /** 被保人转账银行 */
    private String InsuredBankCode;
    /** 被保险人银行账户号 */
    private String InsuredBankAccNo;
    /** 被保人转账银行账户名 */
    private String InsuredAccName;
    /** 被保人转账银行所在省 */
    private String InsuredBankProvince;
    /** 被保人转账银行所在城市 */
    private String InsuredBankCity;
    /** 首期卡折标志 */
    private String NewAccType;
    /** 续期卡折标志 */
    private String AccType;
    /** 续期开户城市 */
    private String BankCity;
    /** 预算金额 */
    private double EstimateMoney;
    /** 被保人卡折类型 */
    private String InsuredAccType;
    /** 是否允许逾越高保额收费 */
    private String IsAllowedCharges;
    /** 退保保全受理号 */
    private String WTEdorAcceptNo;
    /** 双录标识 */
    private String Recording;
    /** 销售渠道编码 */
    private String SaleChannels;
    /** 代理机构编码 */
    private String ZJAgentCom;
    /** 代理机构名称 */
    private String ZJAgentComName;
    /** 是否重录 */
    private String RecordFlag;
    /** 双主险编码 */
    private String PlanCode;
    /** 回执单证 */
    private String ReceiptNo;
    /** 保单补发 */
    private String ReissueCont;
    /** 职域代码 */
    private String DomainCode;
    /** 职域名称 */
    private String DomainName;
    /** 银行推荐人编码 */
    private String ReferralNo;
    /** 第三方订单号 */
    private String ThirdPartyOrderId;
    /** 用户id */
    private String UserId;
    /** 结算号码 */
    private String SettlementNumber;
    /** 结算日期 */
    private Date SettlementDate;
    /** 结算状态 */
    private String SettlementStatus;
    /** 产品名称 */
    private String ProductCode;
    /** 保单终止日期 */
    private Date EndDate;

    public static final int FIELDNUM = 171;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCContSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ContID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCContSchema cloned = (LCContSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getProposalContNo() {
        return ProposalContNo;
    }
    public void setProposalContNo(String aProposalContNo) {
        ProposalContNo = aProposalContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getFamilyType() {
        return FamilyType;
    }
    public void setFamilyType(String aFamilyType) {
        FamilyType = aFamilyType;
    }
    public String getFamilyID() {
        return FamilyID;
    }
    public void setFamilyID(String aFamilyID) {
        FamilyID = aFamilyID;
    }
    public String getPolType() {
        return PolType;
    }
    public void setPolType(String aPolType) {
        PolType = aPolType;
    }
    public String getCardFlag() {
        return CardFlag;
    }
    public void setCardFlag(String aCardFlag) {
        CardFlag = aCardFlag;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getExecuteCom() {
        return ExecuteCom;
    }
    public void setExecuteCom(String aExecuteCom) {
        ExecuteCom = aExecuteCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode1() {
        return AgentCode1;
    }
    public void setAgentCode1(String aAgentCode1) {
        AgentCode1 = aAgentCode1;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getHandler() {
        return Handler;
    }
    public void setHandler(String aHandler) {
        Handler = aHandler;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAppntSex() {
        return AppntSex;
    }
    public void setAppntSex(String aAppntSex) {
        AppntSex = aAppntSex;
    }
    public String getAppntBirthday() {
        if(AppntBirthday != null) {
            return fDate.getString(AppntBirthday);
        } else {
            return null;
        }
    }
    public void setAppntBirthday(Date aAppntBirthday) {
        AppntBirthday = aAppntBirthday;
    }
    public void setAppntBirthday(String aAppntBirthday) {
        if (aAppntBirthday != null && !aAppntBirthday.equals("")) {
            AppntBirthday = fDate.getDate(aAppntBirthday);
        } else
            AppntBirthday = null;
    }

    public String getAppntIDType() {
        return AppntIDType;
    }
    public void setAppntIDType(String aAppntIDType) {
        AppntIDType = aAppntIDType;
    }
    public String getAppntIDNo() {
        return AppntIDNo;
    }
    public void setAppntIDNo(String aAppntIDNo) {
        AppntIDNo = aAppntIDNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getInsuredSex() {
        return InsuredSex;
    }
    public void setInsuredSex(String aInsuredSex) {
        InsuredSex = aInsuredSex;
    }
    public String getInsuredBirthday() {
        if(InsuredBirthday != null) {
            return fDate.getString(InsuredBirthday);
        } else {
            return null;
        }
    }
    public void setInsuredBirthday(Date aInsuredBirthday) {
        InsuredBirthday = aInsuredBirthday;
    }
    public void setInsuredBirthday(String aInsuredBirthday) {
        if (aInsuredBirthday != null && !aInsuredBirthday.equals("")) {
            InsuredBirthday = fDate.getDate(aInsuredBirthday);
        } else
            InsuredBirthday = null;
    }

    public String getInsuredIDType() {
        return InsuredIDType;
    }
    public void setInsuredIDType(String aInsuredIDType) {
        InsuredIDType = aInsuredIDType;
    }
    public String getInsuredIDNo() {
        return InsuredIDNo;
    }
    public void setInsuredIDNo(String aInsuredIDNo) {
        InsuredIDNo = aInsuredIDNo;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getPayLocation() {
        return PayLocation;
    }
    public void setPayLocation(String aPayLocation) {
        PayLocation = aPayLocation;
    }
    public String getDisputedFlag() {
        return DisputedFlag;
    }
    public void setDisputedFlag(String aDisputedFlag) {
        DisputedFlag = aDisputedFlag;
    }
    public String getOutPayFlag() {
        return OutPayFlag;
    }
    public void setOutPayFlag(String aOutPayFlag) {
        OutPayFlag = aOutPayFlag;
    }
    public String getGetPolMode() {
        return GetPolMode;
    }
    public void setGetPolMode(String aGetPolMode) {
        GetPolMode = aGetPolMode;
    }
    public String getSignCom() {
        return SignCom;
    }
    public void setSignCom(String aSignCom) {
        SignCom = aSignCom;
    }
    public String getSignDate() {
        if(SignDate != null) {
            return fDate.getString(SignDate);
        } else {
            return null;
        }
    }
    public void setSignDate(Date aSignDate) {
        SignDate = aSignDate;
    }
    public void setSignDate(String aSignDate) {
        if (aSignDate != null && !aSignDate.equals("")) {
            SignDate = fDate.getDate(aSignDate);
        } else
            SignDate = null;
    }

    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getConsignNo() {
        return ConsignNo;
    }
    public void setConsignNo(String aConsignNo) {
        ConsignNo = aConsignNo;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public int getPrintCount() {
        return PrintCount;
    }
    public void setPrintCount(int aPrintCount) {
        PrintCount = aPrintCount;
    }
    public void setPrintCount(String aPrintCount) {
        if (aPrintCount != null && !aPrintCount.equals("")) {
            Integer tInteger = new Integer(aPrintCount);
            int i = tInteger.intValue();
            PrintCount = i;
        }
    }

    public int getLostTimes() {
        return LostTimes;
    }
    public void setLostTimes(int aLostTimes) {
        LostTimes = aLostTimes;
    }
    public void setLostTimes(String aLostTimes) {
        if (aLostTimes != null && !aLostTimes.equals("")) {
            Integer tInteger = new Integer(aLostTimes);
            int i = tInteger.intValue();
            LostTimes = i;
        }
    }

    public String getLang() {
        return Lang;
    }
    public void setLang(String aLang) {
        Lang = aLang;
    }
    public String getCurrency() {
        return Currency;
    }
    public void setCurrency(String aCurrency) {
        Currency = aCurrency;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getDif() {
        return Dif;
    }
    public void setDif(double aDif) {
        Dif = aDif;
    }
    public void setDif(String aDif) {
        if (aDif != null && !aDif.equals("")) {
            Double tDouble = new Double(aDif);
            double d = tDouble.doubleValue();
            Dif = d;
        }
    }

    public String getPaytoDate() {
        if(PaytoDate != null) {
            return fDate.getString(PaytoDate);
        } else {
            return null;
        }
    }
    public void setPaytoDate(Date aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        if (aPaytoDate != null && !aPaytoDate.equals("")) {
            PaytoDate = fDate.getDate(aPaytoDate);
        } else
            PaytoDate = null;
    }

    public String getFirstPayDate() {
        if(FirstPayDate != null) {
            return fDate.getString(FirstPayDate);
        } else {
            return null;
        }
    }
    public void setFirstPayDate(Date aFirstPayDate) {
        FirstPayDate = aFirstPayDate;
    }
    public void setFirstPayDate(String aFirstPayDate) {
        if (aFirstPayDate != null && !aFirstPayDate.equals("")) {
            FirstPayDate = fDate.getDate(aFirstPayDate);
        } else
            FirstPayDate = null;
    }

    public String getCValiDate() {
        if(CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }
    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else
            CValiDate = null;
    }

    public String getInputOperator() {
        return InputOperator;
    }
    public void setInputOperator(String aInputOperator) {
        InputOperator = aInputOperator;
    }
    public String getInputDate() {
        if(InputDate != null) {
            return fDate.getString(InputDate);
        } else {
            return null;
        }
    }
    public void setInputDate(Date aInputDate) {
        InputDate = aInputDate;
    }
    public void setInputDate(String aInputDate) {
        if (aInputDate != null && !aInputDate.equals("")) {
            InputDate = fDate.getDate(aInputDate);
        } else
            InputDate = null;
    }

    public String getInputTime() {
        return InputTime;
    }
    public void setInputTime(String aInputTime) {
        InputTime = aInputTime;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWDate() {
        if(UWDate != null) {
            return fDate.getString(UWDate);
        } else {
            return null;
        }
    }
    public void setUWDate(Date aUWDate) {
        UWDate = aUWDate;
    }
    public void setUWDate(String aUWDate) {
        if (aUWDate != null && !aUWDate.equals("")) {
            UWDate = fDate.getDate(aUWDate);
        } else
            UWDate = null;
    }

    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getPolApplyDate() {
        if(PolApplyDate != null) {
            return fDate.getString(PolApplyDate);
        } else {
            return null;
        }
    }
    public void setPolApplyDate(Date aPolApplyDate) {
        PolApplyDate = aPolApplyDate;
    }
    public void setPolApplyDate(String aPolApplyDate) {
        if (aPolApplyDate != null && !aPolApplyDate.equals("")) {
            PolApplyDate = fDate.getDate(aPolApplyDate);
        } else
            PolApplyDate = null;
    }

    public String getGetPolDate() {
        if(GetPolDate != null) {
            return fDate.getString(GetPolDate);
        } else {
            return null;
        }
    }
    public void setGetPolDate(Date aGetPolDate) {
        GetPolDate = aGetPolDate;
    }
    public void setGetPolDate(String aGetPolDate) {
        if (aGetPolDate != null && !aGetPolDate.equals("")) {
            GetPolDate = fDate.getDate(aGetPolDate);
        } else
            GetPolDate = null;
    }

    public String getGetPolTime() {
        return GetPolTime;
    }
    public void setGetPolTime(String aGetPolTime) {
        GetPolTime = aGetPolTime;
    }
    public String getCustomGetPolDate() {
        if(CustomGetPolDate != null) {
            return fDate.getString(CustomGetPolDate);
        } else {
            return null;
        }
    }
    public void setCustomGetPolDate(Date aCustomGetPolDate) {
        CustomGetPolDate = aCustomGetPolDate;
    }
    public void setCustomGetPolDate(String aCustomGetPolDate) {
        if (aCustomGetPolDate != null && !aCustomGetPolDate.equals("")) {
            CustomGetPolDate = fDate.getDate(aCustomGetPolDate);
        } else
            CustomGetPolDate = null;
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFirstTrialOperator() {
        return FirstTrialOperator;
    }
    public void setFirstTrialOperator(String aFirstTrialOperator) {
        FirstTrialOperator = aFirstTrialOperator;
    }
    public String getFirstTrialDate() {
        if(FirstTrialDate != null) {
            return fDate.getString(FirstTrialDate);
        } else {
            return null;
        }
    }
    public void setFirstTrialDate(Date aFirstTrialDate) {
        FirstTrialDate = aFirstTrialDate;
    }
    public void setFirstTrialDate(String aFirstTrialDate) {
        if (aFirstTrialDate != null && !aFirstTrialDate.equals("")) {
            FirstTrialDate = fDate.getDate(aFirstTrialDate);
        } else
            FirstTrialDate = null;
    }

    public String getFirstTrialTime() {
        return FirstTrialTime;
    }
    public void setFirstTrialTime(String aFirstTrialTime) {
        FirstTrialTime = aFirstTrialTime;
    }
    public String getReceiveOperator() {
        return ReceiveOperator;
    }
    public void setReceiveOperator(String aReceiveOperator) {
        ReceiveOperator = aReceiveOperator;
    }
    public String getReceiveDate() {
        if(ReceiveDate != null) {
            return fDate.getString(ReceiveDate);
        } else {
            return null;
        }
    }
    public void setReceiveDate(Date aReceiveDate) {
        ReceiveDate = aReceiveDate;
    }
    public void setReceiveDate(String aReceiveDate) {
        if (aReceiveDate != null && !aReceiveDate.equals("")) {
            ReceiveDate = fDate.getDate(aReceiveDate);
        } else
            ReceiveDate = null;
    }

    public String getReceiveTime() {
        return ReceiveTime;
    }
    public void setReceiveTime(String aReceiveTime) {
        ReceiveTime = aReceiveTime;
    }
    public String getTempFeeNo() {
        return TempFeeNo;
    }
    public void setTempFeeNo(String aTempFeeNo) {
        TempFeeNo = aTempFeeNo;
    }
    public String getSellType() {
        return SellType;
    }
    public void setSellType(String aSellType) {
        SellType = aSellType;
    }
    public String getForceUWFlag() {
        return ForceUWFlag;
    }
    public void setForceUWFlag(String aForceUWFlag) {
        ForceUWFlag = aForceUWFlag;
    }
    public String getForceUWReason() {
        return ForceUWReason;
    }
    public void setForceUWReason(String aForceUWReason) {
        ForceUWReason = aForceUWReason;
    }
    public String getNewBankCode() {
        return NewBankCode;
    }
    public void setNewBankCode(String aNewBankCode) {
        NewBankCode = aNewBankCode;
    }
    public String getNewBankAccNo() {
        return NewBankAccNo;
    }
    public void setNewBankAccNo(String aNewBankAccNo) {
        NewBankAccNo = aNewBankAccNo;
    }
    public String getNewAccName() {
        return NewAccName;
    }
    public void setNewAccName(String aNewAccName) {
        NewAccName = aNewAccName;
    }
    public String getNewPayMode() {
        return NewPayMode;
    }
    public void setNewPayMode(String aNewPayMode) {
        NewPayMode = aNewPayMode;
    }
    public String getAgentBankCode() {
        return AgentBankCode;
    }
    public void setAgentBankCode(String aAgentBankCode) {
        AgentBankCode = aAgentBankCode;
    }
    public String getBankAgent() {
        return BankAgent;
    }
    public void setBankAgent(String aBankAgent) {
        BankAgent = aBankAgent;
    }
    public String getBankAgentName() {
        return BankAgentName;
    }
    public void setBankAgentName(String aBankAgentName) {
        BankAgentName = aBankAgentName;
    }
    public String getBankAgentTel() {
        return BankAgentTel;
    }
    public void setBankAgentTel(String aBankAgentTel) {
        BankAgentTel = aBankAgentTel;
    }
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getPolicyNo() {
        return PolicyNo;
    }
    public void setPolicyNo(String aPolicyNo) {
        PolicyNo = aPolicyNo;
    }
    public String getBillPressNo() {
        return BillPressNo;
    }
    public void setBillPressNo(String aBillPressNo) {
        BillPressNo = aBillPressNo;
    }
    public String getCardTypeCode() {
        return CardTypeCode;
    }
    public void setCardTypeCode(String aCardTypeCode) {
        CardTypeCode = aCardTypeCode;
    }
    public String getVisitDate() {
        if(VisitDate != null) {
            return fDate.getString(VisitDate);
        } else {
            return null;
        }
    }
    public void setVisitDate(Date aVisitDate) {
        VisitDate = aVisitDate;
    }
    public void setVisitDate(String aVisitDate) {
        if (aVisitDate != null && !aVisitDate.equals("")) {
            VisitDate = fDate.getDate(aVisitDate);
        } else
            VisitDate = null;
    }

    public String getVisitTime() {
        return VisitTime;
    }
    public void setVisitTime(String aVisitTime) {
        VisitTime = aVisitTime;
    }
    public String getSaleCom() {
        return SaleCom;
    }
    public void setSaleCom(String aSaleCom) {
        SaleCom = aSaleCom;
    }
    public String getPrintFlag() {
        return PrintFlag;
    }
    public void setPrintFlag(String aPrintFlag) {
        PrintFlag = aPrintFlag;
    }
    public String getInvoicePrtFlag() {
        return InvoicePrtFlag;
    }
    public void setInvoicePrtFlag(String aInvoicePrtFlag) {
        InvoicePrtFlag = aInvoicePrtFlag;
    }
    public String getNewReinsureFlag() {
        return NewReinsureFlag;
    }
    public void setNewReinsureFlag(String aNewReinsureFlag) {
        NewReinsureFlag = aNewReinsureFlag;
    }
    public String getRenewPayFlag() {
        return RenewPayFlag;
    }
    public void setRenewPayFlag(String aRenewPayFlag) {
        RenewPayFlag = aRenewPayFlag;
    }
    public String getAppntFirstName() {
        return AppntFirstName;
    }
    public void setAppntFirstName(String aAppntFirstName) {
        AppntFirstName = aAppntFirstName;
    }
    public String getAppntLastName() {
        return AppntLastName;
    }
    public void setAppntLastName(String aAppntLastName) {
        AppntLastName = aAppntLastName;
    }
    public String getInsuredFirstName() {
        return InsuredFirstName;
    }
    public void setInsuredFirstName(String aInsuredFirstName) {
        InsuredFirstName = aInsuredFirstName;
    }
    public String getInsuredLastName() {
        return InsuredLastName;
    }
    public void setInsuredLastName(String aInsuredLastName) {
        InsuredLastName = aInsuredLastName;
    }
    public String getAuthorFlag() {
        return AuthorFlag;
    }
    public void setAuthorFlag(String aAuthorFlag) {
        AuthorFlag = aAuthorFlag;
    }
    public int getGreenChnl() {
        return GreenChnl;
    }
    public void setGreenChnl(int aGreenChnl) {
        GreenChnl = aGreenChnl;
    }
    public void setGreenChnl(String aGreenChnl) {
        if (aGreenChnl != null && !aGreenChnl.equals("")) {
            Integer tInteger = new Integer(aGreenChnl);
            int i = tInteger.intValue();
            GreenChnl = i;
        }
    }

    public String getTBType() {
        return TBType;
    }
    public void setTBType(String aTBType) {
        TBType = aTBType;
    }
    public String getEAuto() {
        return EAuto;
    }
    public void setEAuto(String aEAuto) {
        EAuto = aEAuto;
    }
    public String getSlipForm() {
        return SlipForm;
    }
    public void setSlipForm(String aSlipForm) {
        SlipForm = aSlipForm;
    }
    public String getAutoPayFlag() {
        return AutoPayFlag;
    }
    public void setAutoPayFlag(String aAutoPayFlag) {
        AutoPayFlag = aAutoPayFlag;
    }
    public int getRnewFlag() {
        return RnewFlag;
    }
    public void setRnewFlag(int aRnewFlag) {
        RnewFlag = aRnewFlag;
    }
    public void setRnewFlag(String aRnewFlag) {
        if (aRnewFlag != null && !aRnewFlag.equals("")) {
            Integer tInteger = new Integer(aRnewFlag);
            int i = tInteger.intValue();
            RnewFlag = i;
        }
    }

    public String getFamilyContNo() {
        return FamilyContNo;
    }
    public void setFamilyContNo(String aFamilyContNo) {
        FamilyContNo = aFamilyContNo;
    }
    public String getBussFlag() {
        return BussFlag;
    }
    public void setBussFlag(String aBussFlag) {
        BussFlag = aBussFlag;
    }
    public String getSignName() {
        return SignName;
    }
    public void setSignName(String aSignName) {
        SignName = aSignName;
    }
    public String getOrganizeDate() {
        if(OrganizeDate != null) {
            return fDate.getString(OrganizeDate);
        } else {
            return null;
        }
    }
    public void setOrganizeDate(Date aOrganizeDate) {
        OrganizeDate = aOrganizeDate;
    }
    public void setOrganizeDate(String aOrganizeDate) {
        if (aOrganizeDate != null && !aOrganizeDate.equals("")) {
            OrganizeDate = fDate.getDate(aOrganizeDate);
        } else
            OrganizeDate = null;
    }

    public String getOrganizeTime() {
        return OrganizeTime;
    }
    public void setOrganizeTime(String aOrganizeTime) {
        OrganizeTime = aOrganizeTime;
    }
    public String getNewAutoSendBankFlag() {
        return NewAutoSendBankFlag;
    }
    public void setNewAutoSendBankFlag(String aNewAutoSendBankFlag) {
        NewAutoSendBankFlag = aNewAutoSendBankFlag;
    }
    public String getAgentCodeOper() {
        return AgentCodeOper;
    }
    public void setAgentCodeOper(String aAgentCodeOper) {
        AgentCodeOper = aAgentCodeOper;
    }
    public String getAgentCodeAssi() {
        return AgentCodeAssi;
    }
    public void setAgentCodeAssi(String aAgentCodeAssi) {
        AgentCodeAssi = aAgentCodeAssi;
    }
    public String getDelayReasonCode() {
        return DelayReasonCode;
    }
    public void setDelayReasonCode(String aDelayReasonCode) {
        DelayReasonCode = aDelayReasonCode;
    }
    public String getDelayReasonDesc() {
        return DelayReasonDesc;
    }
    public void setDelayReasonDesc(String aDelayReasonDesc) {
        DelayReasonDesc = aDelayReasonDesc;
    }
    public String getXQremindflag() {
        return XQremindflag;
    }
    public void setXQremindflag(String aXQremindflag) {
        XQremindflag = aXQremindflag;
    }
    public String getOrganComCode() {
        return OrganComCode;
    }
    public void setOrganComCode(String aOrganComCode) {
        OrganComCode = aOrganComCode;
    }
    public String getBankProivnce() {
        return BankProivnce;
    }
    public void setBankProivnce(String aBankProivnce) {
        BankProivnce = aBankProivnce;
    }
    public String getNewBankProivnce() {
        return NewBankProivnce;
    }
    public void setNewBankProivnce(String aNewBankProivnce) {
        NewBankProivnce = aNewBankProivnce;
    }
    public String getArbitrationCom() {
        return ArbitrationCom;
    }
    public void setArbitrationCom(String aArbitrationCom) {
        ArbitrationCom = aArbitrationCom;
    }
    public String getOtherPrtno() {
        return OtherPrtno;
    }
    public void setOtherPrtno(String aOtherPrtno) {
        OtherPrtno = aOtherPrtno;
    }
    public String getDestAccountDate() {
        if(DestAccountDate != null) {
            return fDate.getString(DestAccountDate);
        } else {
            return null;
        }
    }
    public void setDestAccountDate(Date aDestAccountDate) {
        DestAccountDate = aDestAccountDate;
    }
    public void setDestAccountDate(String aDestAccountDate) {
        if (aDestAccountDate != null && !aDestAccountDate.equals("")) {
            DestAccountDate = fDate.getDate(aDestAccountDate);
        } else
            DestAccountDate = null;
    }

    public String getPrePayCount() {
        return PrePayCount;
    }
    public void setPrePayCount(String aPrePayCount) {
        PrePayCount = aPrePayCount;
    }
    public String getNewBankCity() {
        return NewBankCity;
    }
    public void setNewBankCity(String aNewBankCity) {
        NewBankCity = aNewBankCity;
    }
    public String getInsuredBankCode() {
        return InsuredBankCode;
    }
    public void setInsuredBankCode(String aInsuredBankCode) {
        InsuredBankCode = aInsuredBankCode;
    }
    public String getInsuredBankAccNo() {
        return InsuredBankAccNo;
    }
    public void setInsuredBankAccNo(String aInsuredBankAccNo) {
        InsuredBankAccNo = aInsuredBankAccNo;
    }
    public String getInsuredAccName() {
        return InsuredAccName;
    }
    public void setInsuredAccName(String aInsuredAccName) {
        InsuredAccName = aInsuredAccName;
    }
    public String getInsuredBankProvince() {
        return InsuredBankProvince;
    }
    public void setInsuredBankProvince(String aInsuredBankProvince) {
        InsuredBankProvince = aInsuredBankProvince;
    }
    public String getInsuredBankCity() {
        return InsuredBankCity;
    }
    public void setInsuredBankCity(String aInsuredBankCity) {
        InsuredBankCity = aInsuredBankCity;
    }
    public String getNewAccType() {
        return NewAccType;
    }
    public void setNewAccType(String aNewAccType) {
        NewAccType = aNewAccType;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getBankCity() {
        return BankCity;
    }
    public void setBankCity(String aBankCity) {
        BankCity = aBankCity;
    }
    public double getEstimateMoney() {
        return EstimateMoney;
    }
    public void setEstimateMoney(double aEstimateMoney) {
        EstimateMoney = aEstimateMoney;
    }
    public void setEstimateMoney(String aEstimateMoney) {
        if (aEstimateMoney != null && !aEstimateMoney.equals("")) {
            Double tDouble = new Double(aEstimateMoney);
            double d = tDouble.doubleValue();
            EstimateMoney = d;
        }
    }

    public String getInsuredAccType() {
        return InsuredAccType;
    }
    public void setInsuredAccType(String aInsuredAccType) {
        InsuredAccType = aInsuredAccType;
    }
    public String getIsAllowedCharges() {
        return IsAllowedCharges;
    }
    public void setIsAllowedCharges(String aIsAllowedCharges) {
        IsAllowedCharges = aIsAllowedCharges;
    }
    public String getWTEdorAcceptNo() {
        return WTEdorAcceptNo;
    }
    public void setWTEdorAcceptNo(String aWTEdorAcceptNo) {
        WTEdorAcceptNo = aWTEdorAcceptNo;
    }
    public String getRecording() {
        return Recording;
    }
    public void setRecording(String aRecording) {
        Recording = aRecording;
    }
    public String getSaleChannels() {
        return SaleChannels;
    }
    public void setSaleChannels(String aSaleChannels) {
        SaleChannels = aSaleChannels;
    }
    public String getZJAgentCom() {
        return ZJAgentCom;
    }
    public void setZJAgentCom(String aZJAgentCom) {
        ZJAgentCom = aZJAgentCom;
    }
    public String getZJAgentComName() {
        return ZJAgentComName;
    }
    public void setZJAgentComName(String aZJAgentComName) {
        ZJAgentComName = aZJAgentComName;
    }
    public String getRecordFlag() {
        return RecordFlag;
    }
    public void setRecordFlag(String aRecordFlag) {
        RecordFlag = aRecordFlag;
    }
    public String getPlanCode() {
        return PlanCode;
    }
    public void setPlanCode(String aPlanCode) {
        PlanCode = aPlanCode;
    }
    public String getReceiptNo() {
        return ReceiptNo;
    }
    public void setReceiptNo(String aReceiptNo) {
        ReceiptNo = aReceiptNo;
    }
    public String getReissueCont() {
        return ReissueCont;
    }
    public void setReissueCont(String aReissueCont) {
        ReissueCont = aReissueCont;
    }
    public String getDomainCode() {
        return DomainCode;
    }
    public void setDomainCode(String aDomainCode) {
        DomainCode = aDomainCode;
    }
    public String getDomainName() {
        return DomainName;
    }
    public void setDomainName(String aDomainName) {
        DomainName = aDomainName;
    }
    public String getReferralNo() {
        return ReferralNo;
    }
    public void setReferralNo(String aReferralNo) {
        ReferralNo = aReferralNo;
    }
    public String getThirdPartyOrderId() {
        return ThirdPartyOrderId;
    }
    public void setThirdPartyOrderId(String aThirdPartyOrderId) {
        ThirdPartyOrderId = aThirdPartyOrderId;
    }
    public String getUserId() {
        return UserId;
    }
    public void setUserId(String aUserId) {
        UserId = aUserId;
    }
    public String getSettlementNumber() {
        return SettlementNumber;
    }
    public void setSettlementNumber(String aSettlementNumber) {
        SettlementNumber = aSettlementNumber;
    }
    public String getSettlementDate() {
        if(SettlementDate != null) {
            return fDate.getString(SettlementDate);
        } else {
            return null;
        }
    }
    public void setSettlementDate(Date aSettlementDate) {
        SettlementDate = aSettlementDate;
    }
    public void setSettlementDate(String aSettlementDate) {
        if (aSettlementDate != null && !aSettlementDate.equals("")) {
            SettlementDate = fDate.getDate(aSettlementDate);
        } else
            SettlementDate = null;
    }

    public String getSettlementStatus() {
        return SettlementStatus;
    }
    public void setSettlementStatus(String aSettlementStatus) {
        SettlementStatus = aSettlementStatus;
    }
    public String getProductCode() {
        return ProductCode;
    }
    public void setProductCode(String aProductCode) {
        ProductCode = aProductCode;
    }
    public String getEndDate() {
        if(EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }
    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }
    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else
            EndDate = null;
    }


    /**
    * 使用另外一个 LCContSchema 对象给 Schema 赋值
    * @param: aLCContSchema LCContSchema
    **/
    public void setSchema(LCContSchema aLCContSchema) {
        this.ContID = aLCContSchema.getContID();
        this.ShardingID = aLCContSchema.getShardingID();
        this.GrpContNo = aLCContSchema.getGrpContNo();
        this.ContNo = aLCContSchema.getContNo();
        this.ProposalContNo = aLCContSchema.getProposalContNo();
        this.PrtNo = aLCContSchema.getPrtNo();
        this.ContType = aLCContSchema.getContType();
        this.FamilyType = aLCContSchema.getFamilyType();
        this.FamilyID = aLCContSchema.getFamilyID();
        this.PolType = aLCContSchema.getPolType();
        this.CardFlag = aLCContSchema.getCardFlag();
        this.ManageCom = aLCContSchema.getManageCom();
        this.ExecuteCom = aLCContSchema.getExecuteCom();
        this.AgentCom = aLCContSchema.getAgentCom();
        this.AgentCode = aLCContSchema.getAgentCode();
        this.AgentGroup = aLCContSchema.getAgentGroup();
        this.AgentCode1 = aLCContSchema.getAgentCode1();
        this.AgentType = aLCContSchema.getAgentType();
        this.SaleChnl = aLCContSchema.getSaleChnl();
        this.Handler = aLCContSchema.getHandler();
        this.Password = aLCContSchema.getPassword();
        this.AppntNo = aLCContSchema.getAppntNo();
        this.AppntName = aLCContSchema.getAppntName();
        this.AppntSex = aLCContSchema.getAppntSex();
        this.AppntBirthday = fDate.getDate( aLCContSchema.getAppntBirthday());
        this.AppntIDType = aLCContSchema.getAppntIDType();
        this.AppntIDNo = aLCContSchema.getAppntIDNo();
        this.InsuredNo = aLCContSchema.getInsuredNo();
        this.InsuredName = aLCContSchema.getInsuredName();
        this.InsuredSex = aLCContSchema.getInsuredSex();
        this.InsuredBirthday = fDate.getDate( aLCContSchema.getInsuredBirthday());
        this.InsuredIDType = aLCContSchema.getInsuredIDType();
        this.InsuredIDNo = aLCContSchema.getInsuredIDNo();
        this.PayIntv = aLCContSchema.getPayIntv();
        this.PayMode = aLCContSchema.getPayMode();
        this.PayLocation = aLCContSchema.getPayLocation();
        this.DisputedFlag = aLCContSchema.getDisputedFlag();
        this.OutPayFlag = aLCContSchema.getOutPayFlag();
        this.GetPolMode = aLCContSchema.getGetPolMode();
        this.SignCom = aLCContSchema.getSignCom();
        this.SignDate = fDate.getDate( aLCContSchema.getSignDate());
        this.SignTime = aLCContSchema.getSignTime();
        this.ConsignNo = aLCContSchema.getConsignNo();
        this.BankCode = aLCContSchema.getBankCode();
        this.BankAccNo = aLCContSchema.getBankAccNo();
        this.AccName = aLCContSchema.getAccName();
        this.PrintCount = aLCContSchema.getPrintCount();
        this.LostTimes = aLCContSchema.getLostTimes();
        this.Lang = aLCContSchema.getLang();
        this.Currency = aLCContSchema.getCurrency();
        this.Remark = aLCContSchema.getRemark();
        this.Peoples = aLCContSchema.getPeoples();
        this.Mult = aLCContSchema.getMult();
        this.Prem = aLCContSchema.getPrem();
        this.Amnt = aLCContSchema.getAmnt();
        this.SumPrem = aLCContSchema.getSumPrem();
        this.Dif = aLCContSchema.getDif();
        this.PaytoDate = fDate.getDate( aLCContSchema.getPaytoDate());
        this.FirstPayDate = fDate.getDate( aLCContSchema.getFirstPayDate());
        this.CValiDate = fDate.getDate( aLCContSchema.getCValiDate());
        this.InputOperator = aLCContSchema.getInputOperator();
        this.InputDate = fDate.getDate( aLCContSchema.getInputDate());
        this.InputTime = aLCContSchema.getInputTime();
        this.ApproveFlag = aLCContSchema.getApproveFlag();
        this.ApproveCode = aLCContSchema.getApproveCode();
        this.ApproveDate = fDate.getDate( aLCContSchema.getApproveDate());
        this.ApproveTime = aLCContSchema.getApproveTime();
        this.UWFlag = aLCContSchema.getUWFlag();
        this.UWOperator = aLCContSchema.getUWOperator();
        this.UWDate = fDate.getDate( aLCContSchema.getUWDate());
        this.UWTime = aLCContSchema.getUWTime();
        this.AppFlag = aLCContSchema.getAppFlag();
        this.PolApplyDate = fDate.getDate( aLCContSchema.getPolApplyDate());
        this.GetPolDate = fDate.getDate( aLCContSchema.getGetPolDate());
        this.GetPolTime = aLCContSchema.getGetPolTime();
        this.CustomGetPolDate = fDate.getDate( aLCContSchema.getCustomGetPolDate());
        this.State = aLCContSchema.getState();
        this.Operator = aLCContSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCContSchema.getMakeDate());
        this.MakeTime = aLCContSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCContSchema.getModifyDate());
        this.ModifyTime = aLCContSchema.getModifyTime();
        this.FirstTrialOperator = aLCContSchema.getFirstTrialOperator();
        this.FirstTrialDate = fDate.getDate( aLCContSchema.getFirstTrialDate());
        this.FirstTrialTime = aLCContSchema.getFirstTrialTime();
        this.ReceiveOperator = aLCContSchema.getReceiveOperator();
        this.ReceiveDate = fDate.getDate( aLCContSchema.getReceiveDate());
        this.ReceiveTime = aLCContSchema.getReceiveTime();
        this.TempFeeNo = aLCContSchema.getTempFeeNo();
        this.SellType = aLCContSchema.getSellType();
        this.ForceUWFlag = aLCContSchema.getForceUWFlag();
        this.ForceUWReason = aLCContSchema.getForceUWReason();
        this.NewBankCode = aLCContSchema.getNewBankCode();
        this.NewBankAccNo = aLCContSchema.getNewBankAccNo();
        this.NewAccName = aLCContSchema.getNewAccName();
        this.NewPayMode = aLCContSchema.getNewPayMode();
        this.AgentBankCode = aLCContSchema.getAgentBankCode();
        this.BankAgent = aLCContSchema.getBankAgent();
        this.BankAgentName = aLCContSchema.getBankAgentName();
        this.BankAgentTel = aLCContSchema.getBankAgentTel();
        this.ProdSetCode = aLCContSchema.getProdSetCode();
        this.PolicyNo = aLCContSchema.getPolicyNo();
        this.BillPressNo = aLCContSchema.getBillPressNo();
        this.CardTypeCode = aLCContSchema.getCardTypeCode();
        this.VisitDate = fDate.getDate( aLCContSchema.getVisitDate());
        this.VisitTime = aLCContSchema.getVisitTime();
        this.SaleCom = aLCContSchema.getSaleCom();
        this.PrintFlag = aLCContSchema.getPrintFlag();
        this.InvoicePrtFlag = aLCContSchema.getInvoicePrtFlag();
        this.NewReinsureFlag = aLCContSchema.getNewReinsureFlag();
        this.RenewPayFlag = aLCContSchema.getRenewPayFlag();
        this.AppntFirstName = aLCContSchema.getAppntFirstName();
        this.AppntLastName = aLCContSchema.getAppntLastName();
        this.InsuredFirstName = aLCContSchema.getInsuredFirstName();
        this.InsuredLastName = aLCContSchema.getInsuredLastName();
        this.AuthorFlag = aLCContSchema.getAuthorFlag();
        this.GreenChnl = aLCContSchema.getGreenChnl();
        this.TBType = aLCContSchema.getTBType();
        this.EAuto = aLCContSchema.getEAuto();
        this.SlipForm = aLCContSchema.getSlipForm();
        this.AutoPayFlag = aLCContSchema.getAutoPayFlag();
        this.RnewFlag = aLCContSchema.getRnewFlag();
        this.FamilyContNo = aLCContSchema.getFamilyContNo();
        this.BussFlag = aLCContSchema.getBussFlag();
        this.SignName = aLCContSchema.getSignName();
        this.OrganizeDate = fDate.getDate( aLCContSchema.getOrganizeDate());
        this.OrganizeTime = aLCContSchema.getOrganizeTime();
        this.NewAutoSendBankFlag = aLCContSchema.getNewAutoSendBankFlag();
        this.AgentCodeOper = aLCContSchema.getAgentCodeOper();
        this.AgentCodeAssi = aLCContSchema.getAgentCodeAssi();
        this.DelayReasonCode = aLCContSchema.getDelayReasonCode();
        this.DelayReasonDesc = aLCContSchema.getDelayReasonDesc();
        this.XQremindflag = aLCContSchema.getXQremindflag();
        this.OrganComCode = aLCContSchema.getOrganComCode();
        this.BankProivnce = aLCContSchema.getBankProivnce();
        this.NewBankProivnce = aLCContSchema.getNewBankProivnce();
        this.ArbitrationCom = aLCContSchema.getArbitrationCom();
        this.OtherPrtno = aLCContSchema.getOtherPrtno();
        this.DestAccountDate = fDate.getDate( aLCContSchema.getDestAccountDate());
        this.PrePayCount = aLCContSchema.getPrePayCount();
        this.NewBankCity = aLCContSchema.getNewBankCity();
        this.InsuredBankCode = aLCContSchema.getInsuredBankCode();
        this.InsuredBankAccNo = aLCContSchema.getInsuredBankAccNo();
        this.InsuredAccName = aLCContSchema.getInsuredAccName();
        this.InsuredBankProvince = aLCContSchema.getInsuredBankProvince();
        this.InsuredBankCity = aLCContSchema.getInsuredBankCity();
        this.NewAccType = aLCContSchema.getNewAccType();
        this.AccType = aLCContSchema.getAccType();
        this.BankCity = aLCContSchema.getBankCity();
        this.EstimateMoney = aLCContSchema.getEstimateMoney();
        this.InsuredAccType = aLCContSchema.getInsuredAccType();
        this.IsAllowedCharges = aLCContSchema.getIsAllowedCharges();
        this.WTEdorAcceptNo = aLCContSchema.getWTEdorAcceptNo();
        this.Recording = aLCContSchema.getRecording();
        this.SaleChannels = aLCContSchema.getSaleChannels();
        this.ZJAgentCom = aLCContSchema.getZJAgentCom();
        this.ZJAgentComName = aLCContSchema.getZJAgentComName();
        this.RecordFlag = aLCContSchema.getRecordFlag();
        this.PlanCode = aLCContSchema.getPlanCode();
        this.ReceiptNo = aLCContSchema.getReceiptNo();
        this.ReissueCont = aLCContSchema.getReissueCont();
        this.DomainCode = aLCContSchema.getDomainCode();
        this.DomainName = aLCContSchema.getDomainName();
        this.ReferralNo = aLCContSchema.getReferralNo();
        this.ThirdPartyOrderId = aLCContSchema.getThirdPartyOrderId();
        this.UserId = aLCContSchema.getUserId();
        this.SettlementNumber = aLCContSchema.getSettlementNumber();
        this.SettlementDate = fDate.getDate( aLCContSchema.getSettlementDate());
        this.SettlementStatus = aLCContSchema.getSettlementStatus();
        this.ProductCode = aLCContSchema.getProductCode();
        this.EndDate = fDate.getDate( aLCContSchema.getEndDate());
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.ContID = rs.getLong("ContID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("ProposalContNo") == null )
                this.ProposalContNo = null;
            else
                this.ProposalContNo = rs.getString("ProposalContNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("ContType") == null )
                this.ContType = null;
            else
                this.ContType = rs.getString("ContType").trim();

            if( rs.getString("FamilyType") == null )
                this.FamilyType = null;
            else
                this.FamilyType = rs.getString("FamilyType").trim();

            if( rs.getString("FamilyID") == null )
                this.FamilyID = null;
            else
                this.FamilyID = rs.getString("FamilyID").trim();

            if( rs.getString("PolType") == null )
                this.PolType = null;
            else
                this.PolType = rs.getString("PolType").trim();

            if( rs.getString("CardFlag") == null )
                this.CardFlag = null;
            else
                this.CardFlag = rs.getString("CardFlag").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("ExecuteCom") == null )
                this.ExecuteCom = null;
            else
                this.ExecuteCom = rs.getString("ExecuteCom").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("AgentCode1") == null )
                this.AgentCode1 = null;
            else
                this.AgentCode1 = rs.getString("AgentCode1").trim();

            if( rs.getString("AgentType") == null )
                this.AgentType = null;
            else
                this.AgentType = rs.getString("AgentType").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("Handler") == null )
                this.Handler = null;
            else
                this.Handler = rs.getString("Handler").trim();

            if( rs.getString("Password") == null )
                this.Password = null;
            else
                this.Password = rs.getString("Password").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("AppntName") == null )
                this.AppntName = null;
            else
                this.AppntName = rs.getString("AppntName").trim();

            if( rs.getString("AppntSex") == null )
                this.AppntSex = null;
            else
                this.AppntSex = rs.getString("AppntSex").trim();

            this.AppntBirthday = rs.getDate("AppntBirthday");
            if( rs.getString("AppntIDType") == null )
                this.AppntIDType = null;
            else
                this.AppntIDType = rs.getString("AppntIDType").trim();

            if( rs.getString("AppntIDNo") == null )
                this.AppntIDNo = null;
            else
                this.AppntIDNo = rs.getString("AppntIDNo").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("InsuredName") == null )
                this.InsuredName = null;
            else
                this.InsuredName = rs.getString("InsuredName").trim();

            if( rs.getString("InsuredSex") == null )
                this.InsuredSex = null;
            else
                this.InsuredSex = rs.getString("InsuredSex").trim();

            this.InsuredBirthday = rs.getDate("InsuredBirthday");
            if( rs.getString("InsuredIDType") == null )
                this.InsuredIDType = null;
            else
                this.InsuredIDType = rs.getString("InsuredIDType").trim();

            if( rs.getString("InsuredIDNo") == null )
                this.InsuredIDNo = null;
            else
                this.InsuredIDNo = rs.getString("InsuredIDNo").trim();

            this.PayIntv = rs.getInt("PayIntv");
            if( rs.getString("PayMode") == null )
                this.PayMode = null;
            else
                this.PayMode = rs.getString("PayMode").trim();

            if( rs.getString("PayLocation") == null )
                this.PayLocation = null;
            else
                this.PayLocation = rs.getString("PayLocation").trim();

            if( rs.getString("DisputedFlag") == null )
                this.DisputedFlag = null;
            else
                this.DisputedFlag = rs.getString("DisputedFlag").trim();

            if( rs.getString("OutPayFlag") == null )
                this.OutPayFlag = null;
            else
                this.OutPayFlag = rs.getString("OutPayFlag").trim();

            if( rs.getString("GetPolMode") == null )
                this.GetPolMode = null;
            else
                this.GetPolMode = rs.getString("GetPolMode").trim();

            if( rs.getString("SignCom") == null )
                this.SignCom = null;
            else
                this.SignCom = rs.getString("SignCom").trim();

            this.SignDate = rs.getDate("SignDate");
            if( rs.getString("SignTime") == null )
                this.SignTime = null;
            else
                this.SignTime = rs.getString("SignTime").trim();

            if( rs.getString("ConsignNo") == null )
                this.ConsignNo = null;
            else
                this.ConsignNo = rs.getString("ConsignNo").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

            this.PrintCount = rs.getInt("PrintCount");
            this.LostTimes = rs.getInt("LostTimes");
            if( rs.getString("Lang") == null )
                this.Lang = null;
            else
                this.Lang = rs.getString("Lang").trim();

            if( rs.getString("Currency") == null )
                this.Currency = null;
            else
                this.Currency = rs.getString("Currency").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            this.Peoples = rs.getInt("Peoples");
            this.Mult = rs.getDouble("Mult");
            this.Prem = rs.getDouble("Prem");
            this.Amnt = rs.getDouble("Amnt");
            this.SumPrem = rs.getDouble("SumPrem");
            this.Dif = rs.getDouble("Dif");
            this.PaytoDate = rs.getDate("PaytoDate");
            this.FirstPayDate = rs.getDate("FirstPayDate");
            this.CValiDate = rs.getDate("CValiDate");
            if( rs.getString("InputOperator") == null )
                this.InputOperator = null;
            else
                this.InputOperator = rs.getString("InputOperator").trim();

            this.InputDate = rs.getDate("InputDate");
            if( rs.getString("InputTime") == null )
                this.InputTime = null;
            else
                this.InputTime = rs.getString("InputTime").trim();

            if( rs.getString("ApproveFlag") == null )
                this.ApproveFlag = null;
            else
                this.ApproveFlag = rs.getString("ApproveFlag").trim();

            if( rs.getString("ApproveCode") == null )
                this.ApproveCode = null;
            else
                this.ApproveCode = rs.getString("ApproveCode").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("ApproveTime") == null )
                this.ApproveTime = null;
            else
                this.ApproveTime = rs.getString("ApproveTime").trim();

            if( rs.getString("UWFlag") == null )
                this.UWFlag = null;
            else
                this.UWFlag = rs.getString("UWFlag").trim();

            if( rs.getString("UWOperator") == null )
                this.UWOperator = null;
            else
                this.UWOperator = rs.getString("UWOperator").trim();

            this.UWDate = rs.getDate("UWDate");
            if( rs.getString("UWTime") == null )
                this.UWTime = null;
            else
                this.UWTime = rs.getString("UWTime").trim();

            if( rs.getString("AppFlag") == null )
                this.AppFlag = null;
            else
                this.AppFlag = rs.getString("AppFlag").trim();

            this.PolApplyDate = rs.getDate("PolApplyDate");
            this.GetPolDate = rs.getDate("GetPolDate");
            if( rs.getString("GetPolTime") == null )
                this.GetPolTime = null;
            else
                this.GetPolTime = rs.getString("GetPolTime").trim();

            this.CustomGetPolDate = rs.getDate("CustomGetPolDate");
            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("FirstTrialOperator") == null )
                this.FirstTrialOperator = null;
            else
                this.FirstTrialOperator = rs.getString("FirstTrialOperator").trim();

            this.FirstTrialDate = rs.getDate("FirstTrialDate");
            if( rs.getString("FirstTrialTime") == null )
                this.FirstTrialTime = null;
            else
                this.FirstTrialTime = rs.getString("FirstTrialTime").trim();

            if( rs.getString("ReceiveOperator") == null )
                this.ReceiveOperator = null;
            else
                this.ReceiveOperator = rs.getString("ReceiveOperator").trim();

            this.ReceiveDate = rs.getDate("ReceiveDate");
            if( rs.getString("ReceiveTime") == null )
                this.ReceiveTime = null;
            else
                this.ReceiveTime = rs.getString("ReceiveTime").trim();

            if( rs.getString("TempFeeNo") == null )
                this.TempFeeNo = null;
            else
                this.TempFeeNo = rs.getString("TempFeeNo").trim();

            if( rs.getString("SellType") == null )
                this.SellType = null;
            else
                this.SellType = rs.getString("SellType").trim();

            if( rs.getString("ForceUWFlag") == null )
                this.ForceUWFlag = null;
            else
                this.ForceUWFlag = rs.getString("ForceUWFlag").trim();

            if( rs.getString("ForceUWReason") == null )
                this.ForceUWReason = null;
            else
                this.ForceUWReason = rs.getString("ForceUWReason").trim();

            if( rs.getString("NewBankCode") == null )
                this.NewBankCode = null;
            else
                this.NewBankCode = rs.getString("NewBankCode").trim();

            if( rs.getString("NewBankAccNo") == null )
                this.NewBankAccNo = null;
            else
                this.NewBankAccNo = rs.getString("NewBankAccNo").trim();

            if( rs.getString("NewAccName") == null )
                this.NewAccName = null;
            else
                this.NewAccName = rs.getString("NewAccName").trim();

            if( rs.getString("NewPayMode") == null )
                this.NewPayMode = null;
            else
                this.NewPayMode = rs.getString("NewPayMode").trim();

            if( rs.getString("AgentBankCode") == null )
                this.AgentBankCode = null;
            else
                this.AgentBankCode = rs.getString("AgentBankCode").trim();

            if( rs.getString("BankAgent") == null )
                this.BankAgent = null;
            else
                this.BankAgent = rs.getString("BankAgent").trim();

            if( rs.getString("BankAgentName") == null )
                this.BankAgentName = null;
            else
                this.BankAgentName = rs.getString("BankAgentName").trim();

            if( rs.getString("BankAgentTel") == null )
                this.BankAgentTel = null;
            else
                this.BankAgentTel = rs.getString("BankAgentTel").trim();

            if( rs.getString("ProdSetCode") == null )
                this.ProdSetCode = null;
            else
                this.ProdSetCode = rs.getString("ProdSetCode").trim();

            if( rs.getString("PolicyNo") == null )
                this.PolicyNo = null;
            else
                this.PolicyNo = rs.getString("PolicyNo").trim();

            if( rs.getString("BillPressNo") == null )
                this.BillPressNo = null;
            else
                this.BillPressNo = rs.getString("BillPressNo").trim();

            if( rs.getString("CardTypeCode") == null )
                this.CardTypeCode = null;
            else
                this.CardTypeCode = rs.getString("CardTypeCode").trim();

            this.VisitDate = rs.getDate("VisitDate");
            if( rs.getString("VisitTime") == null )
                this.VisitTime = null;
            else
                this.VisitTime = rs.getString("VisitTime").trim();

            if( rs.getString("SaleCom") == null )
                this.SaleCom = null;
            else
                this.SaleCom = rs.getString("SaleCom").trim();

            if( rs.getString("PrintFlag") == null )
                this.PrintFlag = null;
            else
                this.PrintFlag = rs.getString("PrintFlag").trim();

            if( rs.getString("InvoicePrtFlag") == null )
                this.InvoicePrtFlag = null;
            else
                this.InvoicePrtFlag = rs.getString("InvoicePrtFlag").trim();

            if( rs.getString("NewReinsureFlag") == null )
                this.NewReinsureFlag = null;
            else
                this.NewReinsureFlag = rs.getString("NewReinsureFlag").trim();

            if( rs.getString("RenewPayFlag") == null )
                this.RenewPayFlag = null;
            else
                this.RenewPayFlag = rs.getString("RenewPayFlag").trim();

            if( rs.getString("AppntFirstName") == null )
                this.AppntFirstName = null;
            else
                this.AppntFirstName = rs.getString("AppntFirstName").trim();

            if( rs.getString("AppntLastName") == null )
                this.AppntLastName = null;
            else
                this.AppntLastName = rs.getString("AppntLastName").trim();

            if( rs.getString("InsuredFirstName") == null )
                this.InsuredFirstName = null;
            else
                this.InsuredFirstName = rs.getString("InsuredFirstName").trim();

            if( rs.getString("InsuredLastName") == null )
                this.InsuredLastName = null;
            else
                this.InsuredLastName = rs.getString("InsuredLastName").trim();

            if( rs.getString("AuthorFlag") == null )
                this.AuthorFlag = null;
            else
                this.AuthorFlag = rs.getString("AuthorFlag").trim();

            this.GreenChnl = rs.getInt("GreenChnl");
            if( rs.getString("TBType") == null )
                this.TBType = null;
            else
                this.TBType = rs.getString("TBType").trim();

            if( rs.getString("EAuto") == null )
                this.EAuto = null;
            else
                this.EAuto = rs.getString("EAuto").trim();

            if( rs.getString("SlipForm") == null )
                this.SlipForm = null;
            else
                this.SlipForm = rs.getString("SlipForm").trim();

            if( rs.getString("AutoPayFlag") == null )
                this.AutoPayFlag = null;
            else
                this.AutoPayFlag = rs.getString("AutoPayFlag").trim();

            this.RnewFlag = rs.getInt("RnewFlag");
            if( rs.getString("FamilyContNo") == null )
                this.FamilyContNo = null;
            else
                this.FamilyContNo = rs.getString("FamilyContNo").trim();

            if( rs.getString("BussFlag") == null )
                this.BussFlag = null;
            else
                this.BussFlag = rs.getString("BussFlag").trim();

            if( rs.getString("SignName") == null )
                this.SignName = null;
            else
                this.SignName = rs.getString("SignName").trim();

            this.OrganizeDate = rs.getDate("OrganizeDate");
            if( rs.getString("OrganizeTime") == null )
                this.OrganizeTime = null;
            else
                this.OrganizeTime = rs.getString("OrganizeTime").trim();

            if( rs.getString("NewAutoSendBankFlag") == null )
                this.NewAutoSendBankFlag = null;
            else
                this.NewAutoSendBankFlag = rs.getString("NewAutoSendBankFlag").trim();

            if( rs.getString("AgentCodeOper") == null )
                this.AgentCodeOper = null;
            else
                this.AgentCodeOper = rs.getString("AgentCodeOper").trim();

            if( rs.getString("AgentCodeAssi") == null )
                this.AgentCodeAssi = null;
            else
                this.AgentCodeAssi = rs.getString("AgentCodeAssi").trim();

            if( rs.getString("DelayReasonCode") == null )
                this.DelayReasonCode = null;
            else
                this.DelayReasonCode = rs.getString("DelayReasonCode").trim();

            if( rs.getString("DelayReasonDesc") == null )
                this.DelayReasonDesc = null;
            else
                this.DelayReasonDesc = rs.getString("DelayReasonDesc").trim();

            if( rs.getString("XQremindflag") == null )
                this.XQremindflag = null;
            else
                this.XQremindflag = rs.getString("XQremindflag").trim();

            if( rs.getString("OrganComCode") == null )
                this.OrganComCode = null;
            else
                this.OrganComCode = rs.getString("OrganComCode").trim();

            if( rs.getString("BankProivnce") == null )
                this.BankProivnce = null;
            else
                this.BankProivnce = rs.getString("BankProivnce").trim();

            if( rs.getString("NewBankProivnce") == null )
                this.NewBankProivnce = null;
            else
                this.NewBankProivnce = rs.getString("NewBankProivnce").trim();

            if( rs.getString("ArbitrationCom") == null )
                this.ArbitrationCom = null;
            else
                this.ArbitrationCom = rs.getString("ArbitrationCom").trim();

            if( rs.getString("OtherPrtno") == null )
                this.OtherPrtno = null;
            else
                this.OtherPrtno = rs.getString("OtherPrtno").trim();

            this.DestAccountDate = rs.getDate("DestAccountDate");
            if( rs.getString("PrePayCount") == null )
                this.PrePayCount = null;
            else
                this.PrePayCount = rs.getString("PrePayCount").trim();

            if( rs.getString("NewBankCity") == null )
                this.NewBankCity = null;
            else
                this.NewBankCity = rs.getString("NewBankCity").trim();

            if( rs.getString("InsuredBankCode") == null )
                this.InsuredBankCode = null;
            else
                this.InsuredBankCode = rs.getString("InsuredBankCode").trim();

            if( rs.getString("InsuredBankAccNo") == null )
                this.InsuredBankAccNo = null;
            else
                this.InsuredBankAccNo = rs.getString("InsuredBankAccNo").trim();

            if( rs.getString("InsuredAccName") == null )
                this.InsuredAccName = null;
            else
                this.InsuredAccName = rs.getString("InsuredAccName").trim();

            if( rs.getString("InsuredBankProvince") == null )
                this.InsuredBankProvince = null;
            else
                this.InsuredBankProvince = rs.getString("InsuredBankProvince").trim();

            if( rs.getString("InsuredBankCity") == null )
                this.InsuredBankCity = null;
            else
                this.InsuredBankCity = rs.getString("InsuredBankCity").trim();

            if( rs.getString("NewAccType") == null )
                this.NewAccType = null;
            else
                this.NewAccType = rs.getString("NewAccType").trim();

            if( rs.getString("AccType") == null )
                this.AccType = null;
            else
                this.AccType = rs.getString("AccType").trim();

            if( rs.getString("BankCity") == null )
                this.BankCity = null;
            else
                this.BankCity = rs.getString("BankCity").trim();

            this.EstimateMoney = rs.getDouble("EstimateMoney");
            if( rs.getString("InsuredAccType") == null )
                this.InsuredAccType = null;
            else
                this.InsuredAccType = rs.getString("InsuredAccType").trim();

            if( rs.getString("IsAllowedCharges") == null )
                this.IsAllowedCharges = null;
            else
                this.IsAllowedCharges = rs.getString("IsAllowedCharges").trim();

            if( rs.getString("WTEdorAcceptNo") == null )
                this.WTEdorAcceptNo = null;
            else
                this.WTEdorAcceptNo = rs.getString("WTEdorAcceptNo").trim();

            if( rs.getString("Recording") == null )
                this.Recording = null;
            else
                this.Recording = rs.getString("Recording").trim();

            if( rs.getString("SaleChannels") == null )
                this.SaleChannels = null;
            else
                this.SaleChannels = rs.getString("SaleChannels").trim();

            if( rs.getString("ZJAgentCom") == null )
                this.ZJAgentCom = null;
            else
                this.ZJAgentCom = rs.getString("ZJAgentCom").trim();

            if( rs.getString("ZJAgentComName") == null )
                this.ZJAgentComName = null;
            else
                this.ZJAgentComName = rs.getString("ZJAgentComName").trim();

            if( rs.getString("RecordFlag") == null )
                this.RecordFlag = null;
            else
                this.RecordFlag = rs.getString("RecordFlag").trim();

            if( rs.getString("PlanCode") == null )
                this.PlanCode = null;
            else
                this.PlanCode = rs.getString("PlanCode").trim();

            if( rs.getString("ReceiptNo") == null )
                this.ReceiptNo = null;
            else
                this.ReceiptNo = rs.getString("ReceiptNo").trim();

            if( rs.getString("ReissueCont") == null )
                this.ReissueCont = null;
            else
                this.ReissueCont = rs.getString("ReissueCont").trim();

            if( rs.getString("DomainCode") == null )
                this.DomainCode = null;
            else
                this.DomainCode = rs.getString("DomainCode").trim();

            if( rs.getString("DomainName") == null )
                this.DomainName = null;
            else
                this.DomainName = rs.getString("DomainName").trim();

            if( rs.getString("ReferralNo") == null )
                this.ReferralNo = null;
            else
                this.ReferralNo = rs.getString("ReferralNo").trim();

            if( rs.getString("ThirdPartyOrderId") == null )
                this.ThirdPartyOrderId = null;
            else
                this.ThirdPartyOrderId = rs.getString("ThirdPartyOrderId").trim();

            if( rs.getString("UserId") == null )
                this.UserId = null;
            else
                this.UserId = rs.getString("UserId").trim();

            if( rs.getString("SettlementNumber") == null )
                this.SettlementNumber = null;
            else
                this.SettlementNumber = rs.getString("SettlementNumber").trim();

            this.SettlementDate = rs.getDate("SettlementDate");
            if( rs.getString("SettlementStatus") == null )
                this.SettlementStatus = null;
            else
                this.SettlementStatus = rs.getString("SettlementStatus").trim();

            if( rs.getString("ProductCode") == null )
                this.ProductCode = null;
            else
                this.ProductCode = rs.getString("ProductCode").trim();

            this.EndDate = rs.getDate("EndDate");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCContSchema getSchema() {
        LCContSchema aLCContSchema = new LCContSchema();
        aLCContSchema.setSchema(this);
        return aLCContSchema;
    }

    public LCContDB getDB() {
        LCContDB aDBOper = new LCContDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCCont描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(ContID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FamilyType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FamilyID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntSex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AppntBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredSex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( InsuredBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayLocation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisputedFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OutPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetPolMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConsignNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PrintCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LostTimes));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Lang)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Dif));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PaytoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FirstPayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InputOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( InputDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InputTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetPolTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CustomGetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstTrialOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FirstTrialDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstTrialTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SellType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ForceUWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ForceUWReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewAccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewPayMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAgent)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAgentName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAgentTel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BillPressNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardTypeCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( VisitDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VisitTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrintFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InvoicePrtFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewReinsureFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RenewPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntFirstName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntLastName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredFirstName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredLastName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AuthorFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GreenChnl));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TBType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EAuto)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SlipForm)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AutoPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RnewFlag));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FamilyContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BussFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( OrganizeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OrganizeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewAutoSendBankFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCodeOper)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCodeAssi)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DelayReasonCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DelayReasonDesc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(XQremindflag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OrganComCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankProivnce)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewBankProivnce)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ArbitrationCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherPrtno)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( DestAccountDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrePayCount)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewBankCity)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredAccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredBankProvince)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredBankCity)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewAccType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCity)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(EstimateMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredAccType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsAllowedCharges)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WTEdorAcceptNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Recording)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChannels)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZJAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZJAgentComName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RecordFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReissueCont)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DomainCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DomainName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReferralNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ThirdPartyOrderId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UserId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SettlementNumber)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SettlementDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SettlementStatus)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProductCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EndDate )));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCCont>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ContID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            FamilyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            FamilyID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            CardFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            AgentCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            AppntSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            AppntBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
            AppntIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            AppntIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            InsuredBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
            InsuredIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            InsuredIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).intValue();
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            PayLocation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            DisputedFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            OutPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            GetPolMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            SignCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
            SignTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            ConsignNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            PrintCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).intValue();
            LostTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).intValue();
            Lang = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            Peoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,52,SysConst.PACKAGESPILTER))).intValue();
            Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,53,SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,54,SysConst.PACKAGESPILTER))).doubleValue();
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,55,SysConst.PACKAGESPILTER))).doubleValue();
            SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).doubleValue();
            Dif = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,57,SysConst.PACKAGESPILTER))).doubleValue();
            PaytoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58,SysConst.PACKAGESPILTER));
            FirstPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59,SysConst.PACKAGESPILTER));
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60,SysConst.PACKAGESPILTER));
            InputOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
            InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62,SysConst.PACKAGESPILTER));
            InputTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
            ApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66,SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70,SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
            AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
            PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73,SysConst.PACKAGESPILTER));
            GetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74,SysConst.PACKAGESPILTER));
            GetPolTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
            CustomGetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76,SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 82, SysConst.PACKAGESPILTER );
            FirstTrialOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 83, SysConst.PACKAGESPILTER );
            FirstTrialDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 84,SysConst.PACKAGESPILTER));
            FirstTrialTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 85, SysConst.PACKAGESPILTER );
            ReceiveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 86, SysConst.PACKAGESPILTER );
            ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 87,SysConst.PACKAGESPILTER));
            ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 88, SysConst.PACKAGESPILTER );
            TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 89, SysConst.PACKAGESPILTER );
            SellType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 90, SysConst.PACKAGESPILTER );
            ForceUWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 91, SysConst.PACKAGESPILTER );
            ForceUWReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 92, SysConst.PACKAGESPILTER );
            NewBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 93, SysConst.PACKAGESPILTER );
            NewBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 94, SysConst.PACKAGESPILTER );
            NewAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 95, SysConst.PACKAGESPILTER );
            NewPayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 96, SysConst.PACKAGESPILTER );
            AgentBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 97, SysConst.PACKAGESPILTER );
            BankAgent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 98, SysConst.PACKAGESPILTER );
            BankAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 99, SysConst.PACKAGESPILTER );
            BankAgentTel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 100, SysConst.PACKAGESPILTER );
            ProdSetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 101, SysConst.PACKAGESPILTER );
            PolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 102, SysConst.PACKAGESPILTER );
            BillPressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 103, SysConst.PACKAGESPILTER );
            CardTypeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 104, SysConst.PACKAGESPILTER );
            VisitDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 105,SysConst.PACKAGESPILTER));
            VisitTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 106, SysConst.PACKAGESPILTER );
            SaleCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 107, SysConst.PACKAGESPILTER );
            PrintFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 108, SysConst.PACKAGESPILTER );
            InvoicePrtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 109, SysConst.PACKAGESPILTER );
            NewReinsureFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 110, SysConst.PACKAGESPILTER );
            RenewPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 111, SysConst.PACKAGESPILTER );
            AppntFirstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 112, SysConst.PACKAGESPILTER );
            AppntLastName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 113, SysConst.PACKAGESPILTER );
            InsuredFirstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 114, SysConst.PACKAGESPILTER );
            InsuredLastName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 115, SysConst.PACKAGESPILTER );
            AuthorFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 116, SysConst.PACKAGESPILTER );
            GreenChnl = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,117,SysConst.PACKAGESPILTER))).intValue();
            TBType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 118, SysConst.PACKAGESPILTER );
            EAuto = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 119, SysConst.PACKAGESPILTER );
            SlipForm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 120, SysConst.PACKAGESPILTER );
            AutoPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 121, SysConst.PACKAGESPILTER );
            RnewFlag = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,122,SysConst.PACKAGESPILTER))).intValue();
            FamilyContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 123, SysConst.PACKAGESPILTER );
            BussFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 124, SysConst.PACKAGESPILTER );
            SignName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 125, SysConst.PACKAGESPILTER );
            OrganizeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 126,SysConst.PACKAGESPILTER));
            OrganizeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 127, SysConst.PACKAGESPILTER );
            NewAutoSendBankFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 128, SysConst.PACKAGESPILTER );
            AgentCodeOper = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 129, SysConst.PACKAGESPILTER );
            AgentCodeAssi = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 130, SysConst.PACKAGESPILTER );
            DelayReasonCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 131, SysConst.PACKAGESPILTER );
            DelayReasonDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 132, SysConst.PACKAGESPILTER );
            XQremindflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 133, SysConst.PACKAGESPILTER );
            OrganComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 134, SysConst.PACKAGESPILTER );
            BankProivnce = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 135, SysConst.PACKAGESPILTER );
            NewBankProivnce = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 136, SysConst.PACKAGESPILTER );
            ArbitrationCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 137, SysConst.PACKAGESPILTER );
            OtherPrtno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 138, SysConst.PACKAGESPILTER );
            DestAccountDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 139,SysConst.PACKAGESPILTER));
            PrePayCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 140, SysConst.PACKAGESPILTER );
            NewBankCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 141, SysConst.PACKAGESPILTER );
            InsuredBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 142, SysConst.PACKAGESPILTER );
            InsuredBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 143, SysConst.PACKAGESPILTER );
            InsuredAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 144, SysConst.PACKAGESPILTER );
            InsuredBankProvince = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 145, SysConst.PACKAGESPILTER );
            InsuredBankCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 146, SysConst.PACKAGESPILTER );
            NewAccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 147, SysConst.PACKAGESPILTER );
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 148, SysConst.PACKAGESPILTER );
            BankCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 149, SysConst.PACKAGESPILTER );
            EstimateMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,150,SysConst.PACKAGESPILTER))).doubleValue();
            InsuredAccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 151, SysConst.PACKAGESPILTER );
            IsAllowedCharges = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 152, SysConst.PACKAGESPILTER );
            WTEdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 153, SysConst.PACKAGESPILTER );
            Recording = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 154, SysConst.PACKAGESPILTER );
            SaleChannels = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 155, SysConst.PACKAGESPILTER );
            ZJAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 156, SysConst.PACKAGESPILTER );
            ZJAgentComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 157, SysConst.PACKAGESPILTER );
            RecordFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 158, SysConst.PACKAGESPILTER );
            PlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 159, SysConst.PACKAGESPILTER );
            ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 160, SysConst.PACKAGESPILTER );
            ReissueCont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 161, SysConst.PACKAGESPILTER );
            DomainCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 162, SysConst.PACKAGESPILTER );
            DomainName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 163, SysConst.PACKAGESPILTER );
            ReferralNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 164, SysConst.PACKAGESPILTER );
            ThirdPartyOrderId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 165, SysConst.PACKAGESPILTER );
            UserId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 166, SysConst.PACKAGESPILTER );
            SettlementNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 167, SysConst.PACKAGESPILTER );
            SettlementDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 168,SysConst.PACKAGESPILTER));
            SettlementStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 169, SysConst.PACKAGESPILTER );
            ProductCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 170, SysConst.PACKAGESPILTER );
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 171,SysConst.PACKAGESPILTER));
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("FamilyType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyType));
        }
        if (FCode.equalsIgnoreCase("FamilyID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyID));
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
        }
        if (FCode.equalsIgnoreCase("CardFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardFlag));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ExecuteCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
        }
        if (FCode.equalsIgnoreCase("AppntIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDType));
        }
        if (FCode.equalsIgnoreCase("AppntIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntIDNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNo));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("PayLocation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayLocation));
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
        }
        if (FCode.equalsIgnoreCase("GetPolMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolMode));
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("ConsignNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConsignNo));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
        }
        if (FCode.equalsIgnoreCase("LostTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LostTimes));
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Lang));
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstPayDate()));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputTime));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
        }
        if (FCode.equalsIgnoreCase("GetPolTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolTime));
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SellType));
        }
        if (FCode.equalsIgnoreCase("ForceUWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ForceUWFlag));
        }
        if (FCode.equalsIgnoreCase("ForceUWReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ForceUWReason));
        }
        if (FCode.equalsIgnoreCase("NewBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewBankCode));
        }
        if (FCode.equalsIgnoreCase("NewBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewBankAccNo));
        }
        if (FCode.equalsIgnoreCase("NewAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewAccName));
        }
        if (FCode.equalsIgnoreCase("NewPayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewPayMode));
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentBankCode));
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgent));
        }
        if (FCode.equalsIgnoreCase("BankAgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgentName));
        }
        if (FCode.equalsIgnoreCase("BankAgentTel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgentTel));
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
        }
        if (FCode.equalsIgnoreCase("BillPressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BillPressNo));
        }
        if (FCode.equalsIgnoreCase("CardTypeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardTypeCode));
        }
        if (FCode.equalsIgnoreCase("VisitDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getVisitDate()));
        }
        if (FCode.equalsIgnoreCase("VisitTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VisitTime));
        }
        if (FCode.equalsIgnoreCase("SaleCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleCom));
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintFlag));
        }
        if (FCode.equalsIgnoreCase("InvoicePrtFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvoicePrtFlag));
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewReinsureFlag));
        }
        if (FCode.equalsIgnoreCase("RenewPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewPayFlag));
        }
        if (FCode.equalsIgnoreCase("AppntFirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntFirstName));
        }
        if (FCode.equalsIgnoreCase("AppntLastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntLastName));
        }
        if (FCode.equalsIgnoreCase("InsuredFirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredFirstName));
        }
        if (FCode.equalsIgnoreCase("InsuredLastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredLastName));
        }
        if (FCode.equalsIgnoreCase("AuthorFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorFlag));
        }
        if (FCode.equalsIgnoreCase("GreenChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GreenChnl));
        }
        if (FCode.equalsIgnoreCase("TBType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TBType));
        }
        if (FCode.equalsIgnoreCase("EAuto")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EAuto));
        }
        if (FCode.equalsIgnoreCase("SlipForm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SlipForm));
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoPayFlag));
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewFlag));
        }
        if (FCode.equalsIgnoreCase("FamilyContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyContNo));
        }
        if (FCode.equalsIgnoreCase("BussFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussFlag));
        }
        if (FCode.equalsIgnoreCase("SignName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignName));
        }
        if (FCode.equalsIgnoreCase("OrganizeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOrganizeDate()));
        }
        if (FCode.equalsIgnoreCase("OrganizeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrganizeTime));
        }
        if (FCode.equalsIgnoreCase("NewAutoSendBankFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewAutoSendBankFlag));
        }
        if (FCode.equalsIgnoreCase("AgentCodeOper")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCodeOper));
        }
        if (FCode.equalsIgnoreCase("AgentCodeAssi")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCodeAssi));
        }
        if (FCode.equalsIgnoreCase("DelayReasonCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DelayReasonCode));
        }
        if (FCode.equalsIgnoreCase("DelayReasonDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DelayReasonDesc));
        }
        if (FCode.equalsIgnoreCase("XQremindflag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(XQremindflag));
        }
        if (FCode.equalsIgnoreCase("OrganComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrganComCode));
        }
        if (FCode.equalsIgnoreCase("BankProivnce")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankProivnce));
        }
        if (FCode.equalsIgnoreCase("NewBankProivnce")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewBankProivnce));
        }
        if (FCode.equalsIgnoreCase("ArbitrationCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArbitrationCom));
        }
        if (FCode.equalsIgnoreCase("OtherPrtno")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPrtno));
        }
        if (FCode.equalsIgnoreCase("DestAccountDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDestAccountDate()));
        }
        if (FCode.equalsIgnoreCase("PrePayCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrePayCount));
        }
        if (FCode.equalsIgnoreCase("NewBankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewBankCity));
        }
        if (FCode.equalsIgnoreCase("InsuredBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBankCode));
        }
        if (FCode.equalsIgnoreCase("InsuredBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBankAccNo));
        }
        if (FCode.equalsIgnoreCase("InsuredAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAccName));
        }
        if (FCode.equalsIgnoreCase("InsuredBankProvince")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBankProvince));
        }
        if (FCode.equalsIgnoreCase("InsuredBankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBankCity));
        }
        if (FCode.equalsIgnoreCase("NewAccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewAccType));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCity));
        }
        if (FCode.equalsIgnoreCase("EstimateMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EstimateMoney));
        }
        if (FCode.equalsIgnoreCase("InsuredAccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAccType));
        }
        if (FCode.equalsIgnoreCase("IsAllowedCharges")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsAllowedCharges));
        }
        if (FCode.equalsIgnoreCase("WTEdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WTEdorAcceptNo));
        }
        if (FCode.equalsIgnoreCase("Recording")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Recording));
        }
        if (FCode.equalsIgnoreCase("SaleChannels")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChannels));
        }
        if (FCode.equalsIgnoreCase("ZJAgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZJAgentCom));
        }
        if (FCode.equalsIgnoreCase("ZJAgentComName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZJAgentComName));
        }
        if (FCode.equalsIgnoreCase("RecordFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecordFlag));
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equalsIgnoreCase("ReceiptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
        }
        if (FCode.equalsIgnoreCase("ReissueCont")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReissueCont));
        }
        if (FCode.equalsIgnoreCase("DomainCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DomainCode));
        }
        if (FCode.equalsIgnoreCase("DomainName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DomainName));
        }
        if (FCode.equalsIgnoreCase("ReferralNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReferralNo));
        }
        if (FCode.equalsIgnoreCase("ThirdPartyOrderId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ThirdPartyOrderId));
        }
        if (FCode.equalsIgnoreCase("UserId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UserId));
        }
        if (FCode.equalsIgnoreCase("SettlementNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SettlementNumber));
        }
        if (FCode.equalsIgnoreCase("SettlementDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSettlementDate()));
        }
        if (FCode.equalsIgnoreCase("SettlementStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SettlementStatus));
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProductCode));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ContID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ContType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(FamilyType);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(FamilyID);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(PolType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(CardFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(AgentCode1);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Handler);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(AppntSex);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppntBirthday()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(AppntIDType);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(AppntIDNo);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(InsuredSex);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(InsuredIDType);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(InsuredIDNo);
                break;
            case 33:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(PayLocation);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(DisputedFlag);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(OutPayFlag);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(GetPolMode);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(SignCom);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(SignTime);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(ConsignNo);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 46:
                strFieldValue = String.valueOf(PrintCount);
                break;
            case 47:
                strFieldValue = String.valueOf(LostTimes);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(Lang);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(Currency);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 51:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 52:
                strFieldValue = String.valueOf(Mult);
                break;
            case 53:
                strFieldValue = String.valueOf(Prem);
                break;
            case 54:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 55:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 56:
                strFieldValue = String.valueOf(Dif);
                break;
            case 57:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstPayDate()));
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(InputOperator);
                break;
            case 61:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
                break;
            case 62:
                strFieldValue = StrTool.GBKToUnicode(InputTime);
                break;
            case 63:
                strFieldValue = StrTool.GBKToUnicode(ApproveFlag);
                break;
            case 64:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 65:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 66:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            case 67:
                strFieldValue = StrTool.GBKToUnicode(UWFlag);
                break;
            case 68:
                strFieldValue = StrTool.GBKToUnicode(UWOperator);
                break;
            case 69:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
                break;
            case 70:
                strFieldValue = StrTool.GBKToUnicode(UWTime);
                break;
            case 71:
                strFieldValue = StrTool.GBKToUnicode(AppFlag);
                break;
            case 72:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
                break;
            case 73:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
                break;
            case 74:
                strFieldValue = StrTool.GBKToUnicode(GetPolTime);
                break;
            case 75:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
                break;
            case 76:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 77:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 78:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 79:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 80:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 81:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 82:
                strFieldValue = StrTool.GBKToUnicode(FirstTrialOperator);
                break;
            case 83:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
                break;
            case 84:
                strFieldValue = StrTool.GBKToUnicode(FirstTrialTime);
                break;
            case 85:
                strFieldValue = StrTool.GBKToUnicode(ReceiveOperator);
                break;
            case 86:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
                break;
            case 87:
                strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
                break;
            case 88:
                strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
                break;
            case 89:
                strFieldValue = StrTool.GBKToUnicode(SellType);
                break;
            case 90:
                strFieldValue = StrTool.GBKToUnicode(ForceUWFlag);
                break;
            case 91:
                strFieldValue = StrTool.GBKToUnicode(ForceUWReason);
                break;
            case 92:
                strFieldValue = StrTool.GBKToUnicode(NewBankCode);
                break;
            case 93:
                strFieldValue = StrTool.GBKToUnicode(NewBankAccNo);
                break;
            case 94:
                strFieldValue = StrTool.GBKToUnicode(NewAccName);
                break;
            case 95:
                strFieldValue = StrTool.GBKToUnicode(NewPayMode);
                break;
            case 96:
                strFieldValue = StrTool.GBKToUnicode(AgentBankCode);
                break;
            case 97:
                strFieldValue = StrTool.GBKToUnicode(BankAgent);
                break;
            case 98:
                strFieldValue = StrTool.GBKToUnicode(BankAgentName);
                break;
            case 99:
                strFieldValue = StrTool.GBKToUnicode(BankAgentTel);
                break;
            case 100:
                strFieldValue = StrTool.GBKToUnicode(ProdSetCode);
                break;
            case 101:
                strFieldValue = StrTool.GBKToUnicode(PolicyNo);
                break;
            case 102:
                strFieldValue = StrTool.GBKToUnicode(BillPressNo);
                break;
            case 103:
                strFieldValue = StrTool.GBKToUnicode(CardTypeCode);
                break;
            case 104:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getVisitDate()));
                break;
            case 105:
                strFieldValue = StrTool.GBKToUnicode(VisitTime);
                break;
            case 106:
                strFieldValue = StrTool.GBKToUnicode(SaleCom);
                break;
            case 107:
                strFieldValue = StrTool.GBKToUnicode(PrintFlag);
                break;
            case 108:
                strFieldValue = StrTool.GBKToUnicode(InvoicePrtFlag);
                break;
            case 109:
                strFieldValue = StrTool.GBKToUnicode(NewReinsureFlag);
                break;
            case 110:
                strFieldValue = StrTool.GBKToUnicode(RenewPayFlag);
                break;
            case 111:
                strFieldValue = StrTool.GBKToUnicode(AppntFirstName);
                break;
            case 112:
                strFieldValue = StrTool.GBKToUnicode(AppntLastName);
                break;
            case 113:
                strFieldValue = StrTool.GBKToUnicode(InsuredFirstName);
                break;
            case 114:
                strFieldValue = StrTool.GBKToUnicode(InsuredLastName);
                break;
            case 115:
                strFieldValue = StrTool.GBKToUnicode(AuthorFlag);
                break;
            case 116:
                strFieldValue = String.valueOf(GreenChnl);
                break;
            case 117:
                strFieldValue = StrTool.GBKToUnicode(TBType);
                break;
            case 118:
                strFieldValue = StrTool.GBKToUnicode(EAuto);
                break;
            case 119:
                strFieldValue = StrTool.GBKToUnicode(SlipForm);
                break;
            case 120:
                strFieldValue = StrTool.GBKToUnicode(AutoPayFlag);
                break;
            case 121:
                strFieldValue = String.valueOf(RnewFlag);
                break;
            case 122:
                strFieldValue = StrTool.GBKToUnicode(FamilyContNo);
                break;
            case 123:
                strFieldValue = StrTool.GBKToUnicode(BussFlag);
                break;
            case 124:
                strFieldValue = StrTool.GBKToUnicode(SignName);
                break;
            case 125:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOrganizeDate()));
                break;
            case 126:
                strFieldValue = StrTool.GBKToUnicode(OrganizeTime);
                break;
            case 127:
                strFieldValue = StrTool.GBKToUnicode(NewAutoSendBankFlag);
                break;
            case 128:
                strFieldValue = StrTool.GBKToUnicode(AgentCodeOper);
                break;
            case 129:
                strFieldValue = StrTool.GBKToUnicode(AgentCodeAssi);
                break;
            case 130:
                strFieldValue = StrTool.GBKToUnicode(DelayReasonCode);
                break;
            case 131:
                strFieldValue = StrTool.GBKToUnicode(DelayReasonDesc);
                break;
            case 132:
                strFieldValue = StrTool.GBKToUnicode(XQremindflag);
                break;
            case 133:
                strFieldValue = StrTool.GBKToUnicode(OrganComCode);
                break;
            case 134:
                strFieldValue = StrTool.GBKToUnicode(BankProivnce);
                break;
            case 135:
                strFieldValue = StrTool.GBKToUnicode(NewBankProivnce);
                break;
            case 136:
                strFieldValue = StrTool.GBKToUnicode(ArbitrationCom);
                break;
            case 137:
                strFieldValue = StrTool.GBKToUnicode(OtherPrtno);
                break;
            case 138:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDestAccountDate()));
                break;
            case 139:
                strFieldValue = StrTool.GBKToUnicode(PrePayCount);
                break;
            case 140:
                strFieldValue = StrTool.GBKToUnicode(NewBankCity);
                break;
            case 141:
                strFieldValue = StrTool.GBKToUnicode(InsuredBankCode);
                break;
            case 142:
                strFieldValue = StrTool.GBKToUnicode(InsuredBankAccNo);
                break;
            case 143:
                strFieldValue = StrTool.GBKToUnicode(InsuredAccName);
                break;
            case 144:
                strFieldValue = StrTool.GBKToUnicode(InsuredBankProvince);
                break;
            case 145:
                strFieldValue = StrTool.GBKToUnicode(InsuredBankCity);
                break;
            case 146:
                strFieldValue = StrTool.GBKToUnicode(NewAccType);
                break;
            case 147:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 148:
                strFieldValue = StrTool.GBKToUnicode(BankCity);
                break;
            case 149:
                strFieldValue = String.valueOf(EstimateMoney);
                break;
            case 150:
                strFieldValue = StrTool.GBKToUnicode(InsuredAccType);
                break;
            case 151:
                strFieldValue = StrTool.GBKToUnicode(IsAllowedCharges);
                break;
            case 152:
                strFieldValue = StrTool.GBKToUnicode(WTEdorAcceptNo);
                break;
            case 153:
                strFieldValue = StrTool.GBKToUnicode(Recording);
                break;
            case 154:
                strFieldValue = StrTool.GBKToUnicode(SaleChannels);
                break;
            case 155:
                strFieldValue = StrTool.GBKToUnicode(ZJAgentCom);
                break;
            case 156:
                strFieldValue = StrTool.GBKToUnicode(ZJAgentComName);
                break;
            case 157:
                strFieldValue = StrTool.GBKToUnicode(RecordFlag);
                break;
            case 158:
                strFieldValue = StrTool.GBKToUnicode(PlanCode);
                break;
            case 159:
                strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
                break;
            case 160:
                strFieldValue = StrTool.GBKToUnicode(ReissueCont);
                break;
            case 161:
                strFieldValue = StrTool.GBKToUnicode(DomainCode);
                break;
            case 162:
                strFieldValue = StrTool.GBKToUnicode(DomainName);
                break;
            case 163:
                strFieldValue = StrTool.GBKToUnicode(ReferralNo);
                break;
            case 164:
                strFieldValue = StrTool.GBKToUnicode(ThirdPartyOrderId);
                break;
            case 165:
                strFieldValue = StrTool.GBKToUnicode(UserId);
                break;
            case 166:
                strFieldValue = StrTool.GBKToUnicode(SettlementNumber);
                break;
            case 167:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSettlementDate()));
                break;
            case 168:
                strFieldValue = StrTool.GBKToUnicode(SettlementStatus);
                break;
            case 169:
                strFieldValue = StrTool.GBKToUnicode(ProductCode);
                break;
            case 170:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
                ProposalContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("FamilyType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyType = FValue.trim();
            }
            else
                FamilyType = null;
        }
        if (FCode.equalsIgnoreCase("FamilyID")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyID = FValue.trim();
            }
            else
                FamilyID = null;
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolType = FValue.trim();
            }
            else
                PolType = null;
        }
        if (FCode.equalsIgnoreCase("CardFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardFlag = FValue.trim();
            }
            else
                CardFlag = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteCom = FValue.trim();
            }
            else
                ExecuteCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode1 = FValue.trim();
            }
            else
                AgentCode1 = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            if( FValue != null && !FValue.equals(""))
            {
                Handler = FValue.trim();
            }
            else
                Handler = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntSex = FValue.trim();
            }
            else
                AppntSex = null;
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            if(FValue != null && !FValue.equals("")) {
                AppntBirthday = fDate.getDate( FValue );
            }
            else
                AppntBirthday = null;
        }
        if (FCode.equalsIgnoreCase("AppntIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntIDType = FValue.trim();
            }
            else
                AppntIDType = null;
        }
        if (FCode.equalsIgnoreCase("AppntIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntIDNo = FValue.trim();
            }
            else
                AppntIDNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredSex = FValue.trim();
            }
            else
                InsuredSex = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            if(FValue != null && !FValue.equals("")) {
                InsuredBirthday = fDate.getDate( FValue );
            }
            else
                InsuredBirthday = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDType = FValue.trim();
            }
            else
                InsuredIDType = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDNo = FValue.trim();
            }
            else
                InsuredIDNo = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("PayLocation")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayLocation = FValue.trim();
            }
            else
                PayLocation = null;
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisputedFlag = FValue.trim();
            }
            else
                DisputedFlag = null;
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutPayFlag = FValue.trim();
            }
            else
                OutPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetPolMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolMode = FValue.trim();
            }
            else
                GetPolMode = null;
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignCom = FValue.trim();
            }
            else
                SignCom = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if(FValue != null && !FValue.equals("")) {
                SignDate = fDate.getDate( FValue );
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("ConsignNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConsignNo = FValue.trim();
            }
            else
                ConsignNo = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PrintCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("LostTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                LostTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("Lang")) {
            if( FValue != null && !FValue.equals(""))
            {
                Lang = FValue.trim();
            }
            else
                Lang = null;
        }
        if (FCode.equalsIgnoreCase("Currency")) {
            if( FValue != null && !FValue.equals(""))
            {
                Currency = FValue.trim();
            }
            else
                Currency = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Dif = d;
            }
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if(FValue != null && !FValue.equals("")) {
                PaytoDate = fDate.getDate( FValue );
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            if(FValue != null && !FValue.equals("")) {
                FirstPayDate = fDate.getDate( FValue );
            }
            else
                FirstPayDate = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if(FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate( FValue );
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputOperator = FValue.trim();
            }
            else
                InputOperator = null;
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            if(FValue != null && !FValue.equals("")) {
                InputDate = fDate.getDate( FValue );
            }
            else
                InputDate = null;
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                InputTime = FValue.trim();
            }
            else
                InputTime = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if(FValue != null && !FValue.equals("")) {
                UWDate = fDate.getDate( FValue );
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            if(FValue != null && !FValue.equals("")) {
                PolApplyDate = fDate.getDate( FValue );
            }
            else
                PolApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            if(FValue != null && !FValue.equals("")) {
                GetPolDate = fDate.getDate( FValue );
            }
            else
                GetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolTime = FValue.trim();
            }
            else
                GetPolTime = null;
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            if(FValue != null && !FValue.equals("")) {
                CustomGetPolDate = fDate.getDate( FValue );
            }
            else
                CustomGetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialOperator = FValue.trim();
            }
            else
                FirstTrialOperator = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            if(FValue != null && !FValue.equals("")) {
                FirstTrialDate = fDate.getDate( FValue );
            }
            else
                FirstTrialDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialTime = FValue.trim();
            }
            else
                FirstTrialTime = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveOperator = FValue.trim();
            }
            else
                ReceiveOperator = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ReceiveDate = fDate.getDate( FValue );
            }
            else
                ReceiveDate = null;
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiveTime = FValue.trim();
            }
            else
                ReceiveTime = null;
        }
        if (FCode.equalsIgnoreCase("TempFeeNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
                TempFeeNo = null;
        }
        if (FCode.equalsIgnoreCase("SellType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SellType = FValue.trim();
            }
            else
                SellType = null;
        }
        if (FCode.equalsIgnoreCase("ForceUWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ForceUWFlag = FValue.trim();
            }
            else
                ForceUWFlag = null;
        }
        if (FCode.equalsIgnoreCase("ForceUWReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                ForceUWReason = FValue.trim();
            }
            else
                ForceUWReason = null;
        }
        if (FCode.equalsIgnoreCase("NewBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewBankCode = FValue.trim();
            }
            else
                NewBankCode = null;
        }
        if (FCode.equalsIgnoreCase("NewBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewBankAccNo = FValue.trim();
            }
            else
                NewBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("NewAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewAccName = FValue.trim();
            }
            else
                NewAccName = null;
        }
        if (FCode.equalsIgnoreCase("NewPayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewPayMode = FValue.trim();
            }
            else
                NewPayMode = null;
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentBankCode = FValue.trim();
            }
            else
                AgentBankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgent = FValue.trim();
            }
            else
                BankAgent = null;
        }
        if (FCode.equalsIgnoreCase("BankAgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgentName = FValue.trim();
            }
            else
                BankAgentName = null;
        }
        if (FCode.equalsIgnoreCase("BankAgentTel")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgentTel = FValue.trim();
            }
            else
                BankAgentTel = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("PolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicyNo = FValue.trim();
            }
            else
                PolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("BillPressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BillPressNo = FValue.trim();
            }
            else
                BillPressNo = null;
        }
        if (FCode.equalsIgnoreCase("CardTypeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardTypeCode = FValue.trim();
            }
            else
                CardTypeCode = null;
        }
        if (FCode.equalsIgnoreCase("VisitDate")) {
            if(FValue != null && !FValue.equals("")) {
                VisitDate = fDate.getDate( FValue );
            }
            else
                VisitDate = null;
        }
        if (FCode.equalsIgnoreCase("VisitTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                VisitTime = FValue.trim();
            }
            else
                VisitTime = null;
        }
        if (FCode.equalsIgnoreCase("SaleCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleCom = FValue.trim();
            }
            else
                SaleCom = null;
        }
        if (FCode.equalsIgnoreCase("PrintFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
                PrintFlag = null;
        }
        if (FCode.equalsIgnoreCase("InvoicePrtFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvoicePrtFlag = FValue.trim();
            }
            else
                InvoicePrtFlag = null;
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewReinsureFlag = FValue.trim();
            }
            else
                NewReinsureFlag = null;
        }
        if (FCode.equalsIgnoreCase("RenewPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RenewPayFlag = FValue.trim();
            }
            else
                RenewPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("AppntFirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntFirstName = FValue.trim();
            }
            else
                AppntFirstName = null;
        }
        if (FCode.equalsIgnoreCase("AppntLastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntLastName = FValue.trim();
            }
            else
                AppntLastName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredFirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredFirstName = FValue.trim();
            }
            else
                InsuredFirstName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredLastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredLastName = FValue.trim();
            }
            else
                InsuredLastName = null;
        }
        if (FCode.equalsIgnoreCase("AuthorFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AuthorFlag = FValue.trim();
            }
            else
                AuthorFlag = null;
        }
        if (FCode.equalsIgnoreCase("GreenChnl")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GreenChnl = i;
            }
        }
        if (FCode.equalsIgnoreCase("TBType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TBType = FValue.trim();
            }
            else
                TBType = null;
        }
        if (FCode.equalsIgnoreCase("EAuto")) {
            if( FValue != null && !FValue.equals(""))
            {
                EAuto = FValue.trim();
            }
            else
                EAuto = null;
        }
        if (FCode.equalsIgnoreCase("SlipForm")) {
            if( FValue != null && !FValue.equals(""))
            {
                SlipForm = FValue.trim();
            }
            else
                SlipForm = null;
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoPayFlag = FValue.trim();
            }
            else
                AutoPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RnewFlag = i;
            }
        }
        if (FCode.equalsIgnoreCase("FamilyContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyContNo = FValue.trim();
            }
            else
                FamilyContNo = null;
        }
        if (FCode.equalsIgnoreCase("BussFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BussFlag = FValue.trim();
            }
            else
                BussFlag = null;
        }
        if (FCode.equalsIgnoreCase("SignName")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignName = FValue.trim();
            }
            else
                SignName = null;
        }
        if (FCode.equalsIgnoreCase("OrganizeDate")) {
            if(FValue != null && !FValue.equals("")) {
                OrganizeDate = fDate.getDate( FValue );
            }
            else
                OrganizeDate = null;
        }
        if (FCode.equalsIgnoreCase("OrganizeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrganizeTime = FValue.trim();
            }
            else
                OrganizeTime = null;
        }
        if (FCode.equalsIgnoreCase("NewAutoSendBankFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewAutoSendBankFlag = FValue.trim();
            }
            else
                NewAutoSendBankFlag = null;
        }
        if (FCode.equalsIgnoreCase("AgentCodeOper")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCodeOper = FValue.trim();
            }
            else
                AgentCodeOper = null;
        }
        if (FCode.equalsIgnoreCase("AgentCodeAssi")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCodeAssi = FValue.trim();
            }
            else
                AgentCodeAssi = null;
        }
        if (FCode.equalsIgnoreCase("DelayReasonCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DelayReasonCode = FValue.trim();
            }
            else
                DelayReasonCode = null;
        }
        if (FCode.equalsIgnoreCase("DelayReasonDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                DelayReasonDesc = FValue.trim();
            }
            else
                DelayReasonDesc = null;
        }
        if (FCode.equalsIgnoreCase("XQremindflag")) {
            if( FValue != null && !FValue.equals(""))
            {
                XQremindflag = FValue.trim();
            }
            else
                XQremindflag = null;
        }
        if (FCode.equalsIgnoreCase("OrganComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrganComCode = FValue.trim();
            }
            else
                OrganComCode = null;
        }
        if (FCode.equalsIgnoreCase("BankProivnce")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankProivnce = FValue.trim();
            }
            else
                BankProivnce = null;
        }
        if (FCode.equalsIgnoreCase("NewBankProivnce")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewBankProivnce = FValue.trim();
            }
            else
                NewBankProivnce = null;
        }
        if (FCode.equalsIgnoreCase("ArbitrationCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ArbitrationCom = FValue.trim();
            }
            else
                ArbitrationCom = null;
        }
        if (FCode.equalsIgnoreCase("OtherPrtno")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherPrtno = FValue.trim();
            }
            else
                OtherPrtno = null;
        }
        if (FCode.equalsIgnoreCase("DestAccountDate")) {
            if(FValue != null && !FValue.equals("")) {
                DestAccountDate = fDate.getDate( FValue );
            }
            else
                DestAccountDate = null;
        }
        if (FCode.equalsIgnoreCase("PrePayCount")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrePayCount = FValue.trim();
            }
            else
                PrePayCount = null;
        }
        if (FCode.equalsIgnoreCase("NewBankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewBankCity = FValue.trim();
            }
            else
                NewBankCity = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBankCode = FValue.trim();
            }
            else
                InsuredBankCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBankAccNo = FValue.trim();
            }
            else
                InsuredBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredAccName = FValue.trim();
            }
            else
                InsuredAccName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBankProvince")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBankProvince = FValue.trim();
            }
            else
                InsuredBankProvince = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBankCity = FValue.trim();
            }
            else
                InsuredBankCity = null;
        }
        if (FCode.equalsIgnoreCase("NewAccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewAccType = FValue.trim();
            }
            else
                NewAccType = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCity = FValue.trim();
            }
            else
                BankCity = null;
        }
        if (FCode.equalsIgnoreCase("EstimateMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                EstimateMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredAccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredAccType = FValue.trim();
            }
            else
                InsuredAccType = null;
        }
        if (FCode.equalsIgnoreCase("IsAllowedCharges")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsAllowedCharges = FValue.trim();
            }
            else
                IsAllowedCharges = null;
        }
        if (FCode.equalsIgnoreCase("WTEdorAcceptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                WTEdorAcceptNo = FValue.trim();
            }
            else
                WTEdorAcceptNo = null;
        }
        if (FCode.equalsIgnoreCase("Recording")) {
            if( FValue != null && !FValue.equals(""))
            {
                Recording = FValue.trim();
            }
            else
                Recording = null;
        }
        if (FCode.equalsIgnoreCase("SaleChannels")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChannels = FValue.trim();
            }
            else
                SaleChannels = null;
        }
        if (FCode.equalsIgnoreCase("ZJAgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZJAgentCom = FValue.trim();
            }
            else
                ZJAgentCom = null;
        }
        if (FCode.equalsIgnoreCase("ZJAgentComName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZJAgentComName = FValue.trim();
            }
            else
                ZJAgentComName = null;
        }
        if (FCode.equalsIgnoreCase("RecordFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RecordFlag = FValue.trim();
            }
            else
                RecordFlag = null;
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
                PlanCode = null;
        }
        if (FCode.equalsIgnoreCase("ReceiptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReceiptNo = FValue.trim();
            }
            else
                ReceiptNo = null;
        }
        if (FCode.equalsIgnoreCase("ReissueCont")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReissueCont = FValue.trim();
            }
            else
                ReissueCont = null;
        }
        if (FCode.equalsIgnoreCase("DomainCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DomainCode = FValue.trim();
            }
            else
                DomainCode = null;
        }
        if (FCode.equalsIgnoreCase("DomainName")) {
            if( FValue != null && !FValue.equals(""))
            {
                DomainName = FValue.trim();
            }
            else
                DomainName = null;
        }
        if (FCode.equalsIgnoreCase("ReferralNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReferralNo = FValue.trim();
            }
            else
                ReferralNo = null;
        }
        if (FCode.equalsIgnoreCase("ThirdPartyOrderId")) {
            if( FValue != null && !FValue.equals(""))
            {
                ThirdPartyOrderId = FValue.trim();
            }
            else
                ThirdPartyOrderId = null;
        }
        if (FCode.equalsIgnoreCase("UserId")) {
            if( FValue != null && !FValue.equals(""))
            {
                UserId = FValue.trim();
            }
            else
                UserId = null;
        }
        if (FCode.equalsIgnoreCase("SettlementNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                SettlementNumber = FValue.trim();
            }
            else
                SettlementNumber = null;
        }
        if (FCode.equalsIgnoreCase("SettlementDate")) {
            if(FValue != null && !FValue.equals("")) {
                SettlementDate = fDate.getDate( FValue );
            }
            else
                SettlementDate = null;
        }
        if (FCode.equalsIgnoreCase("SettlementStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                SettlementStatus = FValue.trim();
            }
            else
                SettlementStatus = null;
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProductCode = FValue.trim();
            }
            else
                ProductCode = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if(FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate( FValue );
            }
            else
                EndDate = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCContSchema other = (LCContSchema)otherObject;
        return
            ContID == other.getContID()
            && ShardingID.equals(other.getShardingID())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && ProposalContNo.equals(other.getProposalContNo())
            && PrtNo.equals(other.getPrtNo())
            && ContType.equals(other.getContType())
            && FamilyType.equals(other.getFamilyType())
            && FamilyID.equals(other.getFamilyID())
            && PolType.equals(other.getPolType())
            && CardFlag.equals(other.getCardFlag())
            && ManageCom.equals(other.getManageCom())
            && ExecuteCom.equals(other.getExecuteCom())
            && AgentCom.equals(other.getAgentCom())
            && AgentCode.equals(other.getAgentCode())
            && AgentGroup.equals(other.getAgentGroup())
            && AgentCode1.equals(other.getAgentCode1())
            && AgentType.equals(other.getAgentType())
            && SaleChnl.equals(other.getSaleChnl())
            && Handler.equals(other.getHandler())
            && Password.equals(other.getPassword())
            && AppntNo.equals(other.getAppntNo())
            && AppntName.equals(other.getAppntName())
            && AppntSex.equals(other.getAppntSex())
            && fDate.getString(AppntBirthday).equals(other.getAppntBirthday())
            && AppntIDType.equals(other.getAppntIDType())
            && AppntIDNo.equals(other.getAppntIDNo())
            && InsuredNo.equals(other.getInsuredNo())
            && InsuredName.equals(other.getInsuredName())
            && InsuredSex.equals(other.getInsuredSex())
            && fDate.getString(InsuredBirthday).equals(other.getInsuredBirthday())
            && InsuredIDType.equals(other.getInsuredIDType())
            && InsuredIDNo.equals(other.getInsuredIDNo())
            && PayIntv == other.getPayIntv()
            && PayMode.equals(other.getPayMode())
            && PayLocation.equals(other.getPayLocation())
            && DisputedFlag.equals(other.getDisputedFlag())
            && OutPayFlag.equals(other.getOutPayFlag())
            && GetPolMode.equals(other.getGetPolMode())
            && SignCom.equals(other.getSignCom())
            && fDate.getString(SignDate).equals(other.getSignDate())
            && SignTime.equals(other.getSignTime())
            && ConsignNo.equals(other.getConsignNo())
            && BankCode.equals(other.getBankCode())
            && BankAccNo.equals(other.getBankAccNo())
            && AccName.equals(other.getAccName())
            && PrintCount == other.getPrintCount()
            && LostTimes == other.getLostTimes()
            && Lang.equals(other.getLang())
            && Currency.equals(other.getCurrency())
            && Remark.equals(other.getRemark())
            && Peoples == other.getPeoples()
            && Mult == other.getMult()
            && Prem == other.getPrem()
            && Amnt == other.getAmnt()
            && SumPrem == other.getSumPrem()
            && Dif == other.getDif()
            && fDate.getString(PaytoDate).equals(other.getPaytoDate())
            && fDate.getString(FirstPayDate).equals(other.getFirstPayDate())
            && fDate.getString(CValiDate).equals(other.getCValiDate())
            && InputOperator.equals(other.getInputOperator())
            && fDate.getString(InputDate).equals(other.getInputDate())
            && InputTime.equals(other.getInputTime())
            && ApproveFlag.equals(other.getApproveFlag())
            && ApproveCode.equals(other.getApproveCode())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && ApproveTime.equals(other.getApproveTime())
            && UWFlag.equals(other.getUWFlag())
            && UWOperator.equals(other.getUWOperator())
            && fDate.getString(UWDate).equals(other.getUWDate())
            && UWTime.equals(other.getUWTime())
            && AppFlag.equals(other.getAppFlag())
            && fDate.getString(PolApplyDate).equals(other.getPolApplyDate())
            && fDate.getString(GetPolDate).equals(other.getGetPolDate())
            && GetPolTime.equals(other.getGetPolTime())
            && fDate.getString(CustomGetPolDate).equals(other.getCustomGetPolDate())
            && State.equals(other.getState())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && FirstTrialOperator.equals(other.getFirstTrialOperator())
            && fDate.getString(FirstTrialDate).equals(other.getFirstTrialDate())
            && FirstTrialTime.equals(other.getFirstTrialTime())
            && ReceiveOperator.equals(other.getReceiveOperator())
            && fDate.getString(ReceiveDate).equals(other.getReceiveDate())
            && ReceiveTime.equals(other.getReceiveTime())
            && TempFeeNo.equals(other.getTempFeeNo())
            && SellType.equals(other.getSellType())
            && ForceUWFlag.equals(other.getForceUWFlag())
            && ForceUWReason.equals(other.getForceUWReason())
            && NewBankCode.equals(other.getNewBankCode())
            && NewBankAccNo.equals(other.getNewBankAccNo())
            && NewAccName.equals(other.getNewAccName())
            && NewPayMode.equals(other.getNewPayMode())
            && AgentBankCode.equals(other.getAgentBankCode())
            && BankAgent.equals(other.getBankAgent())
            && BankAgentName.equals(other.getBankAgentName())
            && BankAgentTel.equals(other.getBankAgentTel())
            && ProdSetCode.equals(other.getProdSetCode())
            && PolicyNo.equals(other.getPolicyNo())
            && BillPressNo.equals(other.getBillPressNo())
            && CardTypeCode.equals(other.getCardTypeCode())
            && fDate.getString(VisitDate).equals(other.getVisitDate())
            && VisitTime.equals(other.getVisitTime())
            && SaleCom.equals(other.getSaleCom())
            && PrintFlag.equals(other.getPrintFlag())
            && InvoicePrtFlag.equals(other.getInvoicePrtFlag())
            && NewReinsureFlag.equals(other.getNewReinsureFlag())
            && RenewPayFlag.equals(other.getRenewPayFlag())
            && AppntFirstName.equals(other.getAppntFirstName())
            && AppntLastName.equals(other.getAppntLastName())
            && InsuredFirstName.equals(other.getInsuredFirstName())
            && InsuredLastName.equals(other.getInsuredLastName())
            && AuthorFlag.equals(other.getAuthorFlag())
            && GreenChnl == other.getGreenChnl()
            && TBType.equals(other.getTBType())
            && EAuto.equals(other.getEAuto())
            && SlipForm.equals(other.getSlipForm())
            && AutoPayFlag.equals(other.getAutoPayFlag())
            && RnewFlag == other.getRnewFlag()
            && FamilyContNo.equals(other.getFamilyContNo())
            && BussFlag.equals(other.getBussFlag())
            && SignName.equals(other.getSignName())
            && fDate.getString(OrganizeDate).equals(other.getOrganizeDate())
            && OrganizeTime.equals(other.getOrganizeTime())
            && NewAutoSendBankFlag.equals(other.getNewAutoSendBankFlag())
            && AgentCodeOper.equals(other.getAgentCodeOper())
            && AgentCodeAssi.equals(other.getAgentCodeAssi())
            && DelayReasonCode.equals(other.getDelayReasonCode())
            && DelayReasonDesc.equals(other.getDelayReasonDesc())
            && XQremindflag.equals(other.getXQremindflag())
            && OrganComCode.equals(other.getOrganComCode())
            && BankProivnce.equals(other.getBankProivnce())
            && NewBankProivnce.equals(other.getNewBankProivnce())
            && ArbitrationCom.equals(other.getArbitrationCom())
            && OtherPrtno.equals(other.getOtherPrtno())
            && fDate.getString(DestAccountDate).equals(other.getDestAccountDate())
            && PrePayCount.equals(other.getPrePayCount())
            && NewBankCity.equals(other.getNewBankCity())
            && InsuredBankCode.equals(other.getInsuredBankCode())
            && InsuredBankAccNo.equals(other.getInsuredBankAccNo())
            && InsuredAccName.equals(other.getInsuredAccName())
            && InsuredBankProvince.equals(other.getInsuredBankProvince())
            && InsuredBankCity.equals(other.getInsuredBankCity())
            && NewAccType.equals(other.getNewAccType())
            && AccType.equals(other.getAccType())
            && BankCity.equals(other.getBankCity())
            && EstimateMoney == other.getEstimateMoney()
            && InsuredAccType.equals(other.getInsuredAccType())
            && IsAllowedCharges.equals(other.getIsAllowedCharges())
            && WTEdorAcceptNo.equals(other.getWTEdorAcceptNo())
            && Recording.equals(other.getRecording())
            && SaleChannels.equals(other.getSaleChannels())
            && ZJAgentCom.equals(other.getZJAgentCom())
            && ZJAgentComName.equals(other.getZJAgentComName())
            && RecordFlag.equals(other.getRecordFlag())
            && PlanCode.equals(other.getPlanCode())
            && ReceiptNo.equals(other.getReceiptNo())
            && ReissueCont.equals(other.getReissueCont())
            && DomainCode.equals(other.getDomainCode())
            && DomainName.equals(other.getDomainName())
            && ReferralNo.equals(other.getReferralNo())
            && ThirdPartyOrderId.equals(other.getThirdPartyOrderId())
            && UserId.equals(other.getUserId())
            && SettlementNumber.equals(other.getSettlementNumber())
            && fDate.getString(SettlementDate).equals(other.getSettlementDate())
            && SettlementStatus.equals(other.getSettlementStatus())
            && ProductCode.equals(other.getProductCode())
            && fDate.getString(EndDate).equals(other.getEndDate());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 2;
        }
        if( strFieldName.equals("ContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ProposalContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 5;
        }
        if( strFieldName.equals("ContType") ) {
            return 6;
        }
        if( strFieldName.equals("FamilyType") ) {
            return 7;
        }
        if( strFieldName.equals("FamilyID") ) {
            return 8;
        }
        if( strFieldName.equals("PolType") ) {
            return 9;
        }
        if( strFieldName.equals("CardFlag") ) {
            return 10;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 11;
        }
        if( strFieldName.equals("ExecuteCom") ) {
            return 12;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 13;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 14;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 15;
        }
        if( strFieldName.equals("AgentCode1") ) {
            return 16;
        }
        if( strFieldName.equals("AgentType") ) {
            return 17;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 18;
        }
        if( strFieldName.equals("Handler") ) {
            return 19;
        }
        if( strFieldName.equals("Password") ) {
            return 20;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 21;
        }
        if( strFieldName.equals("AppntName") ) {
            return 22;
        }
        if( strFieldName.equals("AppntSex") ) {
            return 23;
        }
        if( strFieldName.equals("AppntBirthday") ) {
            return 24;
        }
        if( strFieldName.equals("AppntIDType") ) {
            return 25;
        }
        if( strFieldName.equals("AppntIDNo") ) {
            return 26;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 27;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 28;
        }
        if( strFieldName.equals("InsuredSex") ) {
            return 29;
        }
        if( strFieldName.equals("InsuredBirthday") ) {
            return 30;
        }
        if( strFieldName.equals("InsuredIDType") ) {
            return 31;
        }
        if( strFieldName.equals("InsuredIDNo") ) {
            return 32;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 33;
        }
        if( strFieldName.equals("PayMode") ) {
            return 34;
        }
        if( strFieldName.equals("PayLocation") ) {
            return 35;
        }
        if( strFieldName.equals("DisputedFlag") ) {
            return 36;
        }
        if( strFieldName.equals("OutPayFlag") ) {
            return 37;
        }
        if( strFieldName.equals("GetPolMode") ) {
            return 38;
        }
        if( strFieldName.equals("SignCom") ) {
            return 39;
        }
        if( strFieldName.equals("SignDate") ) {
            return 40;
        }
        if( strFieldName.equals("SignTime") ) {
            return 41;
        }
        if( strFieldName.equals("ConsignNo") ) {
            return 42;
        }
        if( strFieldName.equals("BankCode") ) {
            return 43;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 44;
        }
        if( strFieldName.equals("AccName") ) {
            return 45;
        }
        if( strFieldName.equals("PrintCount") ) {
            return 46;
        }
        if( strFieldName.equals("LostTimes") ) {
            return 47;
        }
        if( strFieldName.equals("Lang") ) {
            return 48;
        }
        if( strFieldName.equals("Currency") ) {
            return 49;
        }
        if( strFieldName.equals("Remark") ) {
            return 50;
        }
        if( strFieldName.equals("Peoples") ) {
            return 51;
        }
        if( strFieldName.equals("Mult") ) {
            return 52;
        }
        if( strFieldName.equals("Prem") ) {
            return 53;
        }
        if( strFieldName.equals("Amnt") ) {
            return 54;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 55;
        }
        if( strFieldName.equals("Dif") ) {
            return 56;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 57;
        }
        if( strFieldName.equals("FirstPayDate") ) {
            return 58;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 59;
        }
        if( strFieldName.equals("InputOperator") ) {
            return 60;
        }
        if( strFieldName.equals("InputDate") ) {
            return 61;
        }
        if( strFieldName.equals("InputTime") ) {
            return 62;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 63;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 64;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 65;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 66;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 67;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 68;
        }
        if( strFieldName.equals("UWDate") ) {
            return 69;
        }
        if( strFieldName.equals("UWTime") ) {
            return 70;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 71;
        }
        if( strFieldName.equals("PolApplyDate") ) {
            return 72;
        }
        if( strFieldName.equals("GetPolDate") ) {
            return 73;
        }
        if( strFieldName.equals("GetPolTime") ) {
            return 74;
        }
        if( strFieldName.equals("CustomGetPolDate") ) {
            return 75;
        }
        if( strFieldName.equals("State") ) {
            return 76;
        }
        if( strFieldName.equals("Operator") ) {
            return 77;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 78;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 79;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 80;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 81;
        }
        if( strFieldName.equals("FirstTrialOperator") ) {
            return 82;
        }
        if( strFieldName.equals("FirstTrialDate") ) {
            return 83;
        }
        if( strFieldName.equals("FirstTrialTime") ) {
            return 84;
        }
        if( strFieldName.equals("ReceiveOperator") ) {
            return 85;
        }
        if( strFieldName.equals("ReceiveDate") ) {
            return 86;
        }
        if( strFieldName.equals("ReceiveTime") ) {
            return 87;
        }
        if( strFieldName.equals("TempFeeNo") ) {
            return 88;
        }
        if( strFieldName.equals("SellType") ) {
            return 89;
        }
        if( strFieldName.equals("ForceUWFlag") ) {
            return 90;
        }
        if( strFieldName.equals("ForceUWReason") ) {
            return 91;
        }
        if( strFieldName.equals("NewBankCode") ) {
            return 92;
        }
        if( strFieldName.equals("NewBankAccNo") ) {
            return 93;
        }
        if( strFieldName.equals("NewAccName") ) {
            return 94;
        }
        if( strFieldName.equals("NewPayMode") ) {
            return 95;
        }
        if( strFieldName.equals("AgentBankCode") ) {
            return 96;
        }
        if( strFieldName.equals("BankAgent") ) {
            return 97;
        }
        if( strFieldName.equals("BankAgentName") ) {
            return 98;
        }
        if( strFieldName.equals("BankAgentTel") ) {
            return 99;
        }
        if( strFieldName.equals("ProdSetCode") ) {
            return 100;
        }
        if( strFieldName.equals("PolicyNo") ) {
            return 101;
        }
        if( strFieldName.equals("BillPressNo") ) {
            return 102;
        }
        if( strFieldName.equals("CardTypeCode") ) {
            return 103;
        }
        if( strFieldName.equals("VisitDate") ) {
            return 104;
        }
        if( strFieldName.equals("VisitTime") ) {
            return 105;
        }
        if( strFieldName.equals("SaleCom") ) {
            return 106;
        }
        if( strFieldName.equals("PrintFlag") ) {
            return 107;
        }
        if( strFieldName.equals("InvoicePrtFlag") ) {
            return 108;
        }
        if( strFieldName.equals("NewReinsureFlag") ) {
            return 109;
        }
        if( strFieldName.equals("RenewPayFlag") ) {
            return 110;
        }
        if( strFieldName.equals("AppntFirstName") ) {
            return 111;
        }
        if( strFieldName.equals("AppntLastName") ) {
            return 112;
        }
        if( strFieldName.equals("InsuredFirstName") ) {
            return 113;
        }
        if( strFieldName.equals("InsuredLastName") ) {
            return 114;
        }
        if( strFieldName.equals("AuthorFlag") ) {
            return 115;
        }
        if( strFieldName.equals("GreenChnl") ) {
            return 116;
        }
        if( strFieldName.equals("TBType") ) {
            return 117;
        }
        if( strFieldName.equals("EAuto") ) {
            return 118;
        }
        if( strFieldName.equals("SlipForm") ) {
            return 119;
        }
        if( strFieldName.equals("AutoPayFlag") ) {
            return 120;
        }
        if( strFieldName.equals("RnewFlag") ) {
            return 121;
        }
        if( strFieldName.equals("FamilyContNo") ) {
            return 122;
        }
        if( strFieldName.equals("BussFlag") ) {
            return 123;
        }
        if( strFieldName.equals("SignName") ) {
            return 124;
        }
        if( strFieldName.equals("OrganizeDate") ) {
            return 125;
        }
        if( strFieldName.equals("OrganizeTime") ) {
            return 126;
        }
        if( strFieldName.equals("NewAutoSendBankFlag") ) {
            return 127;
        }
        if( strFieldName.equals("AgentCodeOper") ) {
            return 128;
        }
        if( strFieldName.equals("AgentCodeAssi") ) {
            return 129;
        }
        if( strFieldName.equals("DelayReasonCode") ) {
            return 130;
        }
        if( strFieldName.equals("DelayReasonDesc") ) {
            return 131;
        }
        if( strFieldName.equals("XQremindflag") ) {
            return 132;
        }
        if( strFieldName.equals("OrganComCode") ) {
            return 133;
        }
        if( strFieldName.equals("BankProivnce") ) {
            return 134;
        }
        if( strFieldName.equals("NewBankProivnce") ) {
            return 135;
        }
        if( strFieldName.equals("ArbitrationCom") ) {
            return 136;
        }
        if( strFieldName.equals("OtherPrtno") ) {
            return 137;
        }
        if( strFieldName.equals("DestAccountDate") ) {
            return 138;
        }
        if( strFieldName.equals("PrePayCount") ) {
            return 139;
        }
        if( strFieldName.equals("NewBankCity") ) {
            return 140;
        }
        if( strFieldName.equals("InsuredBankCode") ) {
            return 141;
        }
        if( strFieldName.equals("InsuredBankAccNo") ) {
            return 142;
        }
        if( strFieldName.equals("InsuredAccName") ) {
            return 143;
        }
        if( strFieldName.equals("InsuredBankProvince") ) {
            return 144;
        }
        if( strFieldName.equals("InsuredBankCity") ) {
            return 145;
        }
        if( strFieldName.equals("NewAccType") ) {
            return 146;
        }
        if( strFieldName.equals("AccType") ) {
            return 147;
        }
        if( strFieldName.equals("BankCity") ) {
            return 148;
        }
        if( strFieldName.equals("EstimateMoney") ) {
            return 149;
        }
        if( strFieldName.equals("InsuredAccType") ) {
            return 150;
        }
        if( strFieldName.equals("IsAllowedCharges") ) {
            return 151;
        }
        if( strFieldName.equals("WTEdorAcceptNo") ) {
            return 152;
        }
        if( strFieldName.equals("Recording") ) {
            return 153;
        }
        if( strFieldName.equals("SaleChannels") ) {
            return 154;
        }
        if( strFieldName.equals("ZJAgentCom") ) {
            return 155;
        }
        if( strFieldName.equals("ZJAgentComName") ) {
            return 156;
        }
        if( strFieldName.equals("RecordFlag") ) {
            return 157;
        }
        if( strFieldName.equals("PlanCode") ) {
            return 158;
        }
        if( strFieldName.equals("ReceiptNo") ) {
            return 159;
        }
        if( strFieldName.equals("ReissueCont") ) {
            return 160;
        }
        if( strFieldName.equals("DomainCode") ) {
            return 161;
        }
        if( strFieldName.equals("DomainName") ) {
            return 162;
        }
        if( strFieldName.equals("ReferralNo") ) {
            return 163;
        }
        if( strFieldName.equals("ThirdPartyOrderId") ) {
            return 164;
        }
        if( strFieldName.equals("UserId") ) {
            return 165;
        }
        if( strFieldName.equals("SettlementNumber") ) {
            return 166;
        }
        if( strFieldName.equals("SettlementDate") ) {
            return 167;
        }
        if( strFieldName.equals("SettlementStatus") ) {
            return 168;
        }
        if( strFieldName.equals("ProductCode") ) {
            return 169;
        }
        if( strFieldName.equals("EndDate") ) {
            return 170;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "GrpContNo";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "ProposalContNo";
                break;
            case 5:
                strFieldName = "PrtNo";
                break;
            case 6:
                strFieldName = "ContType";
                break;
            case 7:
                strFieldName = "FamilyType";
                break;
            case 8:
                strFieldName = "FamilyID";
                break;
            case 9:
                strFieldName = "PolType";
                break;
            case 10:
                strFieldName = "CardFlag";
                break;
            case 11:
                strFieldName = "ManageCom";
                break;
            case 12:
                strFieldName = "ExecuteCom";
                break;
            case 13:
                strFieldName = "AgentCom";
                break;
            case 14:
                strFieldName = "AgentCode";
                break;
            case 15:
                strFieldName = "AgentGroup";
                break;
            case 16:
                strFieldName = "AgentCode1";
                break;
            case 17:
                strFieldName = "AgentType";
                break;
            case 18:
                strFieldName = "SaleChnl";
                break;
            case 19:
                strFieldName = "Handler";
                break;
            case 20:
                strFieldName = "Password";
                break;
            case 21:
                strFieldName = "AppntNo";
                break;
            case 22:
                strFieldName = "AppntName";
                break;
            case 23:
                strFieldName = "AppntSex";
                break;
            case 24:
                strFieldName = "AppntBirthday";
                break;
            case 25:
                strFieldName = "AppntIDType";
                break;
            case 26:
                strFieldName = "AppntIDNo";
                break;
            case 27:
                strFieldName = "InsuredNo";
                break;
            case 28:
                strFieldName = "InsuredName";
                break;
            case 29:
                strFieldName = "InsuredSex";
                break;
            case 30:
                strFieldName = "InsuredBirthday";
                break;
            case 31:
                strFieldName = "InsuredIDType";
                break;
            case 32:
                strFieldName = "InsuredIDNo";
                break;
            case 33:
                strFieldName = "PayIntv";
                break;
            case 34:
                strFieldName = "PayMode";
                break;
            case 35:
                strFieldName = "PayLocation";
                break;
            case 36:
                strFieldName = "DisputedFlag";
                break;
            case 37:
                strFieldName = "OutPayFlag";
                break;
            case 38:
                strFieldName = "GetPolMode";
                break;
            case 39:
                strFieldName = "SignCom";
                break;
            case 40:
                strFieldName = "SignDate";
                break;
            case 41:
                strFieldName = "SignTime";
                break;
            case 42:
                strFieldName = "ConsignNo";
                break;
            case 43:
                strFieldName = "BankCode";
                break;
            case 44:
                strFieldName = "BankAccNo";
                break;
            case 45:
                strFieldName = "AccName";
                break;
            case 46:
                strFieldName = "PrintCount";
                break;
            case 47:
                strFieldName = "LostTimes";
                break;
            case 48:
                strFieldName = "Lang";
                break;
            case 49:
                strFieldName = "Currency";
                break;
            case 50:
                strFieldName = "Remark";
                break;
            case 51:
                strFieldName = "Peoples";
                break;
            case 52:
                strFieldName = "Mult";
                break;
            case 53:
                strFieldName = "Prem";
                break;
            case 54:
                strFieldName = "Amnt";
                break;
            case 55:
                strFieldName = "SumPrem";
                break;
            case 56:
                strFieldName = "Dif";
                break;
            case 57:
                strFieldName = "PaytoDate";
                break;
            case 58:
                strFieldName = "FirstPayDate";
                break;
            case 59:
                strFieldName = "CValiDate";
                break;
            case 60:
                strFieldName = "InputOperator";
                break;
            case 61:
                strFieldName = "InputDate";
                break;
            case 62:
                strFieldName = "InputTime";
                break;
            case 63:
                strFieldName = "ApproveFlag";
                break;
            case 64:
                strFieldName = "ApproveCode";
                break;
            case 65:
                strFieldName = "ApproveDate";
                break;
            case 66:
                strFieldName = "ApproveTime";
                break;
            case 67:
                strFieldName = "UWFlag";
                break;
            case 68:
                strFieldName = "UWOperator";
                break;
            case 69:
                strFieldName = "UWDate";
                break;
            case 70:
                strFieldName = "UWTime";
                break;
            case 71:
                strFieldName = "AppFlag";
                break;
            case 72:
                strFieldName = "PolApplyDate";
                break;
            case 73:
                strFieldName = "GetPolDate";
                break;
            case 74:
                strFieldName = "GetPolTime";
                break;
            case 75:
                strFieldName = "CustomGetPolDate";
                break;
            case 76:
                strFieldName = "State";
                break;
            case 77:
                strFieldName = "Operator";
                break;
            case 78:
                strFieldName = "MakeDate";
                break;
            case 79:
                strFieldName = "MakeTime";
                break;
            case 80:
                strFieldName = "ModifyDate";
                break;
            case 81:
                strFieldName = "ModifyTime";
                break;
            case 82:
                strFieldName = "FirstTrialOperator";
                break;
            case 83:
                strFieldName = "FirstTrialDate";
                break;
            case 84:
                strFieldName = "FirstTrialTime";
                break;
            case 85:
                strFieldName = "ReceiveOperator";
                break;
            case 86:
                strFieldName = "ReceiveDate";
                break;
            case 87:
                strFieldName = "ReceiveTime";
                break;
            case 88:
                strFieldName = "TempFeeNo";
                break;
            case 89:
                strFieldName = "SellType";
                break;
            case 90:
                strFieldName = "ForceUWFlag";
                break;
            case 91:
                strFieldName = "ForceUWReason";
                break;
            case 92:
                strFieldName = "NewBankCode";
                break;
            case 93:
                strFieldName = "NewBankAccNo";
                break;
            case 94:
                strFieldName = "NewAccName";
                break;
            case 95:
                strFieldName = "NewPayMode";
                break;
            case 96:
                strFieldName = "AgentBankCode";
                break;
            case 97:
                strFieldName = "BankAgent";
                break;
            case 98:
                strFieldName = "BankAgentName";
                break;
            case 99:
                strFieldName = "BankAgentTel";
                break;
            case 100:
                strFieldName = "ProdSetCode";
                break;
            case 101:
                strFieldName = "PolicyNo";
                break;
            case 102:
                strFieldName = "BillPressNo";
                break;
            case 103:
                strFieldName = "CardTypeCode";
                break;
            case 104:
                strFieldName = "VisitDate";
                break;
            case 105:
                strFieldName = "VisitTime";
                break;
            case 106:
                strFieldName = "SaleCom";
                break;
            case 107:
                strFieldName = "PrintFlag";
                break;
            case 108:
                strFieldName = "InvoicePrtFlag";
                break;
            case 109:
                strFieldName = "NewReinsureFlag";
                break;
            case 110:
                strFieldName = "RenewPayFlag";
                break;
            case 111:
                strFieldName = "AppntFirstName";
                break;
            case 112:
                strFieldName = "AppntLastName";
                break;
            case 113:
                strFieldName = "InsuredFirstName";
                break;
            case 114:
                strFieldName = "InsuredLastName";
                break;
            case 115:
                strFieldName = "AuthorFlag";
                break;
            case 116:
                strFieldName = "GreenChnl";
                break;
            case 117:
                strFieldName = "TBType";
                break;
            case 118:
                strFieldName = "EAuto";
                break;
            case 119:
                strFieldName = "SlipForm";
                break;
            case 120:
                strFieldName = "AutoPayFlag";
                break;
            case 121:
                strFieldName = "RnewFlag";
                break;
            case 122:
                strFieldName = "FamilyContNo";
                break;
            case 123:
                strFieldName = "BussFlag";
                break;
            case 124:
                strFieldName = "SignName";
                break;
            case 125:
                strFieldName = "OrganizeDate";
                break;
            case 126:
                strFieldName = "OrganizeTime";
                break;
            case 127:
                strFieldName = "NewAutoSendBankFlag";
                break;
            case 128:
                strFieldName = "AgentCodeOper";
                break;
            case 129:
                strFieldName = "AgentCodeAssi";
                break;
            case 130:
                strFieldName = "DelayReasonCode";
                break;
            case 131:
                strFieldName = "DelayReasonDesc";
                break;
            case 132:
                strFieldName = "XQremindflag";
                break;
            case 133:
                strFieldName = "OrganComCode";
                break;
            case 134:
                strFieldName = "BankProivnce";
                break;
            case 135:
                strFieldName = "NewBankProivnce";
                break;
            case 136:
                strFieldName = "ArbitrationCom";
                break;
            case 137:
                strFieldName = "OtherPrtno";
                break;
            case 138:
                strFieldName = "DestAccountDate";
                break;
            case 139:
                strFieldName = "PrePayCount";
                break;
            case 140:
                strFieldName = "NewBankCity";
                break;
            case 141:
                strFieldName = "InsuredBankCode";
                break;
            case 142:
                strFieldName = "InsuredBankAccNo";
                break;
            case 143:
                strFieldName = "InsuredAccName";
                break;
            case 144:
                strFieldName = "InsuredBankProvince";
                break;
            case 145:
                strFieldName = "InsuredBankCity";
                break;
            case 146:
                strFieldName = "NewAccType";
                break;
            case 147:
                strFieldName = "AccType";
                break;
            case 148:
                strFieldName = "BankCity";
                break;
            case 149:
                strFieldName = "EstimateMoney";
                break;
            case 150:
                strFieldName = "InsuredAccType";
                break;
            case 151:
                strFieldName = "IsAllowedCharges";
                break;
            case 152:
                strFieldName = "WTEdorAcceptNo";
                break;
            case 153:
                strFieldName = "Recording";
                break;
            case 154:
                strFieldName = "SaleChannels";
                break;
            case 155:
                strFieldName = "ZJAgentCom";
                break;
            case 156:
                strFieldName = "ZJAgentComName";
                break;
            case 157:
                strFieldName = "RecordFlag";
                break;
            case 158:
                strFieldName = "PlanCode";
                break;
            case 159:
                strFieldName = "ReceiptNo";
                break;
            case 160:
                strFieldName = "ReissueCont";
                break;
            case 161:
                strFieldName = "DomainCode";
                break;
            case 162:
                strFieldName = "DomainName";
                break;
            case 163:
                strFieldName = "ReferralNo";
                break;
            case 164:
                strFieldName = "ThirdPartyOrderId";
                break;
            case 165:
                strFieldName = "UserId";
                break;
            case 166:
                strFieldName = "SettlementNumber";
                break;
            case 167:
                strFieldName = "SettlementDate";
                break;
            case 168:
                strFieldName = "SettlementStatus";
                break;
            case 169:
                strFieldName = "ProductCode";
                break;
            case 170:
                strFieldName = "EndDate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALCONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "FAMILYTYPE":
                return Schema.TYPE_STRING;
            case "FAMILYID":
                return Schema.TYPE_STRING;
            case "POLTYPE":
                return Schema.TYPE_STRING;
            case "CARDFLAG":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "EXECUTECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE1":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "HANDLER":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "APPNTSEX":
                return Schema.TYPE_STRING;
            case "APPNTBIRTHDAY":
                return Schema.TYPE_DATE;
            case "APPNTIDTYPE":
                return Schema.TYPE_STRING;
            case "APPNTIDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "INSUREDSEX":
                return Schema.TYPE_STRING;
            case "INSUREDBIRTHDAY":
                return Schema.TYPE_DATE;
            case "INSUREDIDTYPE":
                return Schema.TYPE_STRING;
            case "INSUREDIDNO":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "PAYLOCATION":
                return Schema.TYPE_STRING;
            case "DISPUTEDFLAG":
                return Schema.TYPE_STRING;
            case "OUTPAYFLAG":
                return Schema.TYPE_STRING;
            case "GETPOLMODE":
                return Schema.TYPE_STRING;
            case "SIGNCOM":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_DATE;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "CONSIGNNO":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "PRINTCOUNT":
                return Schema.TYPE_INT;
            case "LOSTTIMES":
                return Schema.TYPE_INT;
            case "LANG":
                return Schema.TYPE_STRING;
            case "CURRENCY":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "DIF":
                return Schema.TYPE_DOUBLE;
            case "PAYTODATE":
                return Schema.TYPE_DATE;
            case "FIRSTPAYDATE":
                return Schema.TYPE_DATE;
            case "CVALIDATE":
                return Schema.TYPE_DATE;
            case "INPUTOPERATOR":
                return Schema.TYPE_STRING;
            case "INPUTDATE":
                return Schema.TYPE_DATE;
            case "INPUTTIME":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_DATE;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "POLAPPLYDATE":
                return Schema.TYPE_DATE;
            case "GETPOLDATE":
                return Schema.TYPE_DATE;
            case "GETPOLTIME":
                return Schema.TYPE_STRING;
            case "CUSTOMGETPOLDATE":
                return Schema.TYPE_DATE;
            case "STATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALOPERATOR":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALDATE":
                return Schema.TYPE_DATE;
            case "FIRSTTRIALTIME":
                return Schema.TYPE_STRING;
            case "RECEIVEOPERATOR":
                return Schema.TYPE_STRING;
            case "RECEIVEDATE":
                return Schema.TYPE_DATE;
            case "RECEIVETIME":
                return Schema.TYPE_STRING;
            case "TEMPFEENO":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "FORCEUWFLAG":
                return Schema.TYPE_STRING;
            case "FORCEUWREASON":
                return Schema.TYPE_STRING;
            case "NEWBANKCODE":
                return Schema.TYPE_STRING;
            case "NEWBANKACCNO":
                return Schema.TYPE_STRING;
            case "NEWACCNAME":
                return Schema.TYPE_STRING;
            case "NEWPAYMODE":
                return Schema.TYPE_STRING;
            case "AGENTBANKCODE":
                return Schema.TYPE_STRING;
            case "BANKAGENT":
                return Schema.TYPE_STRING;
            case "BANKAGENTNAME":
                return Schema.TYPE_STRING;
            case "BANKAGENTTEL":
                return Schema.TYPE_STRING;
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "BILLPRESSNO":
                return Schema.TYPE_STRING;
            case "CARDTYPECODE":
                return Schema.TYPE_STRING;
            case "VISITDATE":
                return Schema.TYPE_DATE;
            case "VISITTIME":
                return Schema.TYPE_STRING;
            case "SALECOM":
                return Schema.TYPE_STRING;
            case "PRINTFLAG":
                return Schema.TYPE_STRING;
            case "INVOICEPRTFLAG":
                return Schema.TYPE_STRING;
            case "NEWREINSUREFLAG":
                return Schema.TYPE_STRING;
            case "RENEWPAYFLAG":
                return Schema.TYPE_STRING;
            case "APPNTFIRSTNAME":
                return Schema.TYPE_STRING;
            case "APPNTLASTNAME":
                return Schema.TYPE_STRING;
            case "INSUREDFIRSTNAME":
                return Schema.TYPE_STRING;
            case "INSUREDLASTNAME":
                return Schema.TYPE_STRING;
            case "AUTHORFLAG":
                return Schema.TYPE_STRING;
            case "GREENCHNL":
                return Schema.TYPE_INT;
            case "TBTYPE":
                return Schema.TYPE_STRING;
            case "EAUTO":
                return Schema.TYPE_STRING;
            case "SLIPFORM":
                return Schema.TYPE_STRING;
            case "AUTOPAYFLAG":
                return Schema.TYPE_STRING;
            case "RNEWFLAG":
                return Schema.TYPE_INT;
            case "FAMILYCONTNO":
                return Schema.TYPE_STRING;
            case "BUSSFLAG":
                return Schema.TYPE_STRING;
            case "SIGNNAME":
                return Schema.TYPE_STRING;
            case "ORGANIZEDATE":
                return Schema.TYPE_DATE;
            case "ORGANIZETIME":
                return Schema.TYPE_STRING;
            case "NEWAUTOSENDBANKFLAG":
                return Schema.TYPE_STRING;
            case "AGENTCODEOPER":
                return Schema.TYPE_STRING;
            case "AGENTCODEASSI":
                return Schema.TYPE_STRING;
            case "DELAYREASONCODE":
                return Schema.TYPE_STRING;
            case "DELAYREASONDESC":
                return Schema.TYPE_STRING;
            case "XQREMINDFLAG":
                return Schema.TYPE_STRING;
            case "ORGANCOMCODE":
                return Schema.TYPE_STRING;
            case "BANKPROIVNCE":
                return Schema.TYPE_STRING;
            case "NEWBANKPROIVNCE":
                return Schema.TYPE_STRING;
            case "ARBITRATIONCOM":
                return Schema.TYPE_STRING;
            case "OTHERPRTNO":
                return Schema.TYPE_STRING;
            case "DESTACCOUNTDATE":
                return Schema.TYPE_DATE;
            case "PREPAYCOUNT":
                return Schema.TYPE_STRING;
            case "NEWBANKCITY":
                return Schema.TYPE_STRING;
            case "INSUREDBANKCODE":
                return Schema.TYPE_STRING;
            case "INSUREDBANKACCNO":
                return Schema.TYPE_STRING;
            case "INSUREDACCNAME":
                return Schema.TYPE_STRING;
            case "INSUREDBANKPROVINCE":
                return Schema.TYPE_STRING;
            case "INSUREDBANKCITY":
                return Schema.TYPE_STRING;
            case "NEWACCTYPE":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "BANKCITY":
                return Schema.TYPE_STRING;
            case "ESTIMATEMONEY":
                return Schema.TYPE_DOUBLE;
            case "INSUREDACCTYPE":
                return Schema.TYPE_STRING;
            case "ISALLOWEDCHARGES":
                return Schema.TYPE_STRING;
            case "WTEDORACCEPTNO":
                return Schema.TYPE_STRING;
            case "RECORDING":
                return Schema.TYPE_STRING;
            case "SALECHANNELS":
                return Schema.TYPE_STRING;
            case "ZJAGENTCOM":
                return Schema.TYPE_STRING;
            case "ZJAGENTCOMNAME":
                return Schema.TYPE_STRING;
            case "RECORDFLAG":
                return Schema.TYPE_STRING;
            case "PLANCODE":
                return Schema.TYPE_STRING;
            case "RECEIPTNO":
                return Schema.TYPE_STRING;
            case "REISSUECONT":
                return Schema.TYPE_STRING;
            case "DOMAINCODE":
                return Schema.TYPE_STRING;
            case "DOMAINNAME":
                return Schema.TYPE_STRING;
            case "REFERRALNO":
                return Schema.TYPE_STRING;
            case "THIRDPARTYORDERID":
                return Schema.TYPE_STRING;
            case "USERID":
                return Schema.TYPE_STRING;
            case "SETTLEMENTNUMBER":
                return Schema.TYPE_STRING;
            case "SETTLEMENTDATE":
                return Schema.TYPE_DATE;
            case "SETTLEMENTSTATUS":
                return Schema.TYPE_STRING;
            case "PRODUCTCODE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_DATE;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_INT;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_DATE;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_INT;
            case 47:
                return Schema.TYPE_INT;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_INT;
            case 52:
                return Schema.TYPE_DOUBLE;
            case 53:
                return Schema.TYPE_DOUBLE;
            case 54:
                return Schema.TYPE_DOUBLE;
            case 55:
                return Schema.TYPE_DOUBLE;
            case 56:
                return Schema.TYPE_DOUBLE;
            case 57:
                return Schema.TYPE_DATE;
            case 58:
                return Schema.TYPE_DATE;
            case 59:
                return Schema.TYPE_DATE;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_DATE;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_DATE;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_DATE;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_DATE;
            case 73:
                return Schema.TYPE_DATE;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_DATE;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_STRING;
            case 78:
                return Schema.TYPE_DATE;
            case 79:
                return Schema.TYPE_STRING;
            case 80:
                return Schema.TYPE_DATE;
            case 81:
                return Schema.TYPE_STRING;
            case 82:
                return Schema.TYPE_STRING;
            case 83:
                return Schema.TYPE_DATE;
            case 84:
                return Schema.TYPE_STRING;
            case 85:
                return Schema.TYPE_STRING;
            case 86:
                return Schema.TYPE_DATE;
            case 87:
                return Schema.TYPE_STRING;
            case 88:
                return Schema.TYPE_STRING;
            case 89:
                return Schema.TYPE_STRING;
            case 90:
                return Schema.TYPE_STRING;
            case 91:
                return Schema.TYPE_STRING;
            case 92:
                return Schema.TYPE_STRING;
            case 93:
                return Schema.TYPE_STRING;
            case 94:
                return Schema.TYPE_STRING;
            case 95:
                return Schema.TYPE_STRING;
            case 96:
                return Schema.TYPE_STRING;
            case 97:
                return Schema.TYPE_STRING;
            case 98:
                return Schema.TYPE_STRING;
            case 99:
                return Schema.TYPE_STRING;
            case 100:
                return Schema.TYPE_STRING;
            case 101:
                return Schema.TYPE_STRING;
            case 102:
                return Schema.TYPE_STRING;
            case 103:
                return Schema.TYPE_STRING;
            case 104:
                return Schema.TYPE_DATE;
            case 105:
                return Schema.TYPE_STRING;
            case 106:
                return Schema.TYPE_STRING;
            case 107:
                return Schema.TYPE_STRING;
            case 108:
                return Schema.TYPE_STRING;
            case 109:
                return Schema.TYPE_STRING;
            case 110:
                return Schema.TYPE_STRING;
            case 111:
                return Schema.TYPE_STRING;
            case 112:
                return Schema.TYPE_STRING;
            case 113:
                return Schema.TYPE_STRING;
            case 114:
                return Schema.TYPE_STRING;
            case 115:
                return Schema.TYPE_STRING;
            case 116:
                return Schema.TYPE_INT;
            case 117:
                return Schema.TYPE_STRING;
            case 118:
                return Schema.TYPE_STRING;
            case 119:
                return Schema.TYPE_STRING;
            case 120:
                return Schema.TYPE_STRING;
            case 121:
                return Schema.TYPE_INT;
            case 122:
                return Schema.TYPE_STRING;
            case 123:
                return Schema.TYPE_STRING;
            case 124:
                return Schema.TYPE_STRING;
            case 125:
                return Schema.TYPE_DATE;
            case 126:
                return Schema.TYPE_STRING;
            case 127:
                return Schema.TYPE_STRING;
            case 128:
                return Schema.TYPE_STRING;
            case 129:
                return Schema.TYPE_STRING;
            case 130:
                return Schema.TYPE_STRING;
            case 131:
                return Schema.TYPE_STRING;
            case 132:
                return Schema.TYPE_STRING;
            case 133:
                return Schema.TYPE_STRING;
            case 134:
                return Schema.TYPE_STRING;
            case 135:
                return Schema.TYPE_STRING;
            case 136:
                return Schema.TYPE_STRING;
            case 137:
                return Schema.TYPE_STRING;
            case 138:
                return Schema.TYPE_DATE;
            case 139:
                return Schema.TYPE_STRING;
            case 140:
                return Schema.TYPE_STRING;
            case 141:
                return Schema.TYPE_STRING;
            case 142:
                return Schema.TYPE_STRING;
            case 143:
                return Schema.TYPE_STRING;
            case 144:
                return Schema.TYPE_STRING;
            case 145:
                return Schema.TYPE_STRING;
            case 146:
                return Schema.TYPE_STRING;
            case 147:
                return Schema.TYPE_STRING;
            case 148:
                return Schema.TYPE_STRING;
            case 149:
                return Schema.TYPE_DOUBLE;
            case 150:
                return Schema.TYPE_STRING;
            case 151:
                return Schema.TYPE_STRING;
            case 152:
                return Schema.TYPE_STRING;
            case 153:
                return Schema.TYPE_STRING;
            case 154:
                return Schema.TYPE_STRING;
            case 155:
                return Schema.TYPE_STRING;
            case 156:
                return Schema.TYPE_STRING;
            case 157:
                return Schema.TYPE_STRING;
            case 158:
                return Schema.TYPE_STRING;
            case 159:
                return Schema.TYPE_STRING;
            case 160:
                return Schema.TYPE_STRING;
            case 161:
                return Schema.TYPE_STRING;
            case 162:
                return Schema.TYPE_STRING;
            case 163:
                return Schema.TYPE_STRING;
            case 164:
                return Schema.TYPE_STRING;
            case 165:
                return Schema.TYPE_STRING;
            case 166:
                return Schema.TYPE_STRING;
            case 167:
                return Schema.TYPE_DATE;
            case 168:
                return Schema.TYPE_STRING;
            case 169:
                return Schema.TYPE_STRING;
            case 170:
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
