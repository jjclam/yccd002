/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMDutyGetFeeRelaPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMDutyGetFeeRelaPojo implements Pojo,Serializable {
    // @Field
    /** 给付代码 */
    private String GetDutyCode; 
    /** 给付名称 */
    private String GetDutyName; 
    /** 给付责任类型 */
    private String GetDutyKind; 
    /** 账单项目编码 */
    private String ClmFeeCode; 
    /** 账单项目名称 */
    private String ClmFeeName; 
    /** 费用计算方式 */
    private String ClmFeeCalType; 
    /** 费用明细计算公式 */
    private String ClmFeeCalCode; 
    /** 费用默认值 */
    private String ClmFeeDefValue; 


    public static final int FIELDNUM = 8;    // 数据库表的字段个数
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyName() {
        return GetDutyName;
    }
    public void setGetDutyName(String aGetDutyName) {
        GetDutyName = aGetDutyName;
    }
    public String getGetDutyKind() {
        return GetDutyKind;
    }
    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }
    public String getClmFeeCode() {
        return ClmFeeCode;
    }
    public void setClmFeeCode(String aClmFeeCode) {
        ClmFeeCode = aClmFeeCode;
    }
    public String getClmFeeName() {
        return ClmFeeName;
    }
    public void setClmFeeName(String aClmFeeName) {
        ClmFeeName = aClmFeeName;
    }
    public String getClmFeeCalType() {
        return ClmFeeCalType;
    }
    public void setClmFeeCalType(String aClmFeeCalType) {
        ClmFeeCalType = aClmFeeCalType;
    }
    public String getClmFeeCalCode() {
        return ClmFeeCalCode;
    }
    public void setClmFeeCalCode(String aClmFeeCalCode) {
        ClmFeeCalCode = aClmFeeCalCode;
    }
    public String getClmFeeDefValue() {
        return ClmFeeDefValue;
    }
    public void setClmFeeDefValue(String aClmFeeDefValue) {
        ClmFeeDefValue = aClmFeeDefValue;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GetDutyCode") ) {
            return 0;
        }
        if( strFieldName.equals("GetDutyName") ) {
            return 1;
        }
        if( strFieldName.equals("GetDutyKind") ) {
            return 2;
        }
        if( strFieldName.equals("ClmFeeCode") ) {
            return 3;
        }
        if( strFieldName.equals("ClmFeeName") ) {
            return 4;
        }
        if( strFieldName.equals("ClmFeeCalType") ) {
            return 5;
        }
        if( strFieldName.equals("ClmFeeCalCode") ) {
            return 6;
        }
        if( strFieldName.equals("ClmFeeDefValue") ) {
            return 7;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GetDutyCode";
                break;
            case 1:
                strFieldName = "GetDutyName";
                break;
            case 2:
                strFieldName = "GetDutyKind";
                break;
            case 3:
                strFieldName = "ClmFeeCode";
                break;
            case 4:
                strFieldName = "ClmFeeName";
                break;
            case 5:
                strFieldName = "ClmFeeCalType";
                break;
            case 6:
                strFieldName = "ClmFeeCalCode";
                break;
            case 7:
                strFieldName = "ClmFeeDefValue";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYNAME":
                return Schema.TYPE_STRING;
            case "GETDUTYKIND":
                return Schema.TYPE_STRING;
            case "CLMFEECODE":
                return Schema.TYPE_STRING;
            case "CLMFEENAME":
                return Schema.TYPE_STRING;
            case "CLMFEECALTYPE":
                return Schema.TYPE_STRING;
            case "CLMFEECALCODE":
                return Schema.TYPE_STRING;
            case "CLMFEEDEFVALUE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equalsIgnoreCase("ClmFeeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeCode));
        }
        if (FCode.equalsIgnoreCase("ClmFeeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeName));
        }
        if (FCode.equalsIgnoreCase("ClmFeeCalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeCalType));
        }
        if (FCode.equalsIgnoreCase("ClmFeeCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeCalCode));
        }
        if (FCode.equalsIgnoreCase("ClmFeeDefValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFeeDefValue));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GetDutyCode);
                break;
            case 1:
                strFieldValue = String.valueOf(GetDutyName);
                break;
            case 2:
                strFieldValue = String.valueOf(GetDutyKind);
                break;
            case 3:
                strFieldValue = String.valueOf(ClmFeeCode);
                break;
            case 4:
                strFieldValue = String.valueOf(ClmFeeName);
                break;
            case 5:
                strFieldValue = String.valueOf(ClmFeeCalType);
                break;
            case 6:
                strFieldValue = String.valueOf(ClmFeeCalCode);
                break;
            case 7:
                strFieldValue = String.valueOf(ClmFeeDefValue);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
                GetDutyName = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
                GetDutyKind = null;
        }
        if (FCode.equalsIgnoreCase("ClmFeeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmFeeCode = FValue.trim();
            }
            else
                ClmFeeCode = null;
        }
        if (FCode.equalsIgnoreCase("ClmFeeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmFeeName = FValue.trim();
            }
            else
                ClmFeeName = null;
        }
        if (FCode.equalsIgnoreCase("ClmFeeCalType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmFeeCalType = FValue.trim();
            }
            else
                ClmFeeCalType = null;
        }
        if (FCode.equalsIgnoreCase("ClmFeeCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmFeeCalCode = FValue.trim();
            }
            else
                ClmFeeCalCode = null;
        }
        if (FCode.equalsIgnoreCase("ClmFeeDefValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmFeeDefValue = FValue.trim();
            }
            else
                ClmFeeDefValue = null;
        }
        return true;
    }


    public String toString() {
    return "LMDutyGetFeeRelaPojo [" +
            "GetDutyCode="+GetDutyCode +
            ", GetDutyName="+GetDutyName +
            ", GetDutyKind="+GetDutyKind +
            ", ClmFeeCode="+ClmFeeCode +
            ", ClmFeeName="+ClmFeeName +
            ", ClmFeeCalType="+ClmFeeCalType +
            ", ClmFeeCalCode="+ClmFeeCalCode +
            ", ClmFeeDefValue="+ClmFeeDefValue +"]";
    }
}
