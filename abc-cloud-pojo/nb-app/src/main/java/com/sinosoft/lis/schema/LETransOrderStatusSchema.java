/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */
 

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LETransOrderStatusDB;

/**
 * <p>ClassName: LETransOrderStatusSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-12-03
 */
public class LETransOrderStatusSchema implements Schema, Cloneable {
    // @Field
    /** 订单号 */
    private String SerialNo;
    /** 交易类型 */
    private String TransType;
    /** 交易名称 */
    private String TradeName;
    /** 合作伙伴代码 */
    private String InstitutionCode;
    /** Orderserialnumber */
    private String OrderSerialNumber;
    /** 业务流水号 */
    private String TransRefGUID;
    /** 产品代码 */
    private String ProductCode;
    /** 产品名称 */
    private String ProductName;
    /** 保单号 */
    private String PolicySerialNumber;
    /** 投保单号 */
    private String ApplicationNumber;
    /** 对账单号 */
    private String CheckPayNo;
    /** 缴费订单号 */
    private String PayOrderSerialNumber;
    /** 流程状态 */
    private String ProcessState;
    /** 结果状态 */
    private String ResultStatus;
    /** 结果描述 */
    private String ResultInfoDesc;
    /** 结果信息 */
    private String InfoMessage;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 20;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LETransOrderStatusSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LETransOrderStatusSchema cloned = (LETransOrderStatusSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getTransType() {
        return TransType;
    }
    public void setTransType(String aTransType) {
        TransType = aTransType;
    }
    public String getTradeName() {
        return TradeName;
    }
    public void setTradeName(String aTradeName) {
        TradeName = aTradeName;
    }
    public String getInstitutionCode() {
        return InstitutionCode;
    }
    public void setInstitutionCode(String aInstitutionCode) {
        InstitutionCode = aInstitutionCode;
    }
    public String getOrderSerialNumber() {
        return OrderSerialNumber;
    }
    public void setOrderSerialNumber(String aOrderSerialNumber) {
        OrderSerialNumber = aOrderSerialNumber;
    }
    public String getTransRefGUID() {
        return TransRefGUID;
    }
    public void setTransRefGUID(String aTransRefGUID) {
        TransRefGUID = aTransRefGUID;
    }
    public String getProductCode() {
        return ProductCode;
    }
    public void setProductCode(String aProductCode) {
        ProductCode = aProductCode;
    }
    public String getProductName() {
        return ProductName;
    }
    public void setProductName(String aProductName) {
        ProductName = aProductName;
    }
    public String getPolicySerialNumber() {
        return PolicySerialNumber;
    }
    public void setPolicySerialNumber(String aPolicySerialNumber) {
        PolicySerialNumber = aPolicySerialNumber;
    }
    public String getApplicationNumber() {
        return ApplicationNumber;
    }
    public void setApplicationNumber(String aApplicationNumber) {
        ApplicationNumber = aApplicationNumber;
    }
    public String getCheckPayNo() {
        return CheckPayNo;
    }
    public void setCheckPayNo(String aCheckPayNo) {
        CheckPayNo = aCheckPayNo;
    }
    public String getPayOrderSerialNumber() {
        return PayOrderSerialNumber;
    }
    public void setPayOrderSerialNumber(String aPayOrderSerialNumber) {
        PayOrderSerialNumber = aPayOrderSerialNumber;
    }
    public String getProcessState() {
        return ProcessState;
    }
    public void setProcessState(String aProcessState) {
        ProcessState = aProcessState;
    }
    public String getResultStatus() {
        return ResultStatus;
    }
    public void setResultStatus(String aResultStatus) {
        ResultStatus = aResultStatus;
    }
    public String getResultInfoDesc() {
        return ResultInfoDesc;
    }
    public void setResultInfoDesc(String aResultInfoDesc) {
        ResultInfoDesc = aResultInfoDesc;
    }
    public String getInfoMessage() {
        return InfoMessage;
    }
    public void setInfoMessage(String aInfoMessage) {
        InfoMessage = aInfoMessage;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 使用另外一个 LETransOrderStatusSchema 对象给 Schema 赋值
    * @param: aLETransOrderStatusSchema LETransOrderStatusSchema
    **/
    public void setSchema(LETransOrderStatusSchema aLETransOrderStatusSchema) {
        this.SerialNo = aLETransOrderStatusSchema.getSerialNo();
        this.TransType = aLETransOrderStatusSchema.getTransType();
        this.TradeName = aLETransOrderStatusSchema.getTradeName();
        this.InstitutionCode = aLETransOrderStatusSchema.getInstitutionCode();
        this.OrderSerialNumber = aLETransOrderStatusSchema.getOrderSerialNumber();
        this.TransRefGUID = aLETransOrderStatusSchema.getTransRefGUID();
        this.ProductCode = aLETransOrderStatusSchema.getProductCode();
        this.ProductName = aLETransOrderStatusSchema.getProductName();
        this.PolicySerialNumber = aLETransOrderStatusSchema.getPolicySerialNumber();
        this.ApplicationNumber = aLETransOrderStatusSchema.getApplicationNumber();
        this.CheckPayNo = aLETransOrderStatusSchema.getCheckPayNo();
        this.PayOrderSerialNumber = aLETransOrderStatusSchema.getPayOrderSerialNumber();
        this.ProcessState = aLETransOrderStatusSchema.getProcessState();
        this.ResultStatus = aLETransOrderStatusSchema.getResultStatus();
        this.ResultInfoDesc = aLETransOrderStatusSchema.getResultInfoDesc();
        this.InfoMessage = aLETransOrderStatusSchema.getInfoMessage();
        this.MakeDate = fDate.getDate( aLETransOrderStatusSchema.getMakeDate());
        this.MakeTime = aLETransOrderStatusSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLETransOrderStatusSchema.getModifyDate());
        this.ModifyTime = aLETransOrderStatusSchema.getModifyTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("TransType") == null )
                this.TransType = null;
            else
                this.TransType = rs.getString("TransType").trim();

            if( rs.getString("TradeName") == null )
                this.TradeName = null;
            else
                this.TradeName = rs.getString("TradeName").trim();

            if( rs.getString("InstitutionCode") == null )
                this.InstitutionCode = null;
            else
                this.InstitutionCode = rs.getString("InstitutionCode").trim();

            if( rs.getString("OrderSerialNumber") == null )
                this.OrderSerialNumber = null;
            else
                this.OrderSerialNumber = rs.getString("OrderSerialNumber").trim();

            if( rs.getString("TransRefGUID") == null )
                this.TransRefGUID = null;
            else
                this.TransRefGUID = rs.getString("TransRefGUID").trim();

            if( rs.getString("ProductCode") == null )
                this.ProductCode = null;
            else
                this.ProductCode = rs.getString("ProductCode").trim();

            if( rs.getString("ProductName") == null )
                this.ProductName = null;
            else
                this.ProductName = rs.getString("ProductName").trim();

            if( rs.getString("PolicySerialNumber") == null )
                this.PolicySerialNumber = null;
            else
                this.PolicySerialNumber = rs.getString("PolicySerialNumber").trim();

            if( rs.getString("ApplicationNumber") == null )
                this.ApplicationNumber = null;
            else
                this.ApplicationNumber = rs.getString("ApplicationNumber").trim();

            if( rs.getString("CheckPayNo") == null )
                this.CheckPayNo = null;
            else
                this.CheckPayNo = rs.getString("CheckPayNo").trim();

            if( rs.getString("PayOrderSerialNumber") == null )
                this.PayOrderSerialNumber = null;
            else
                this.PayOrderSerialNumber = rs.getString("PayOrderSerialNumber").trim();

            if( rs.getString("ProcessState") == null )
                this.ProcessState = null;
            else
                this.ProcessState = rs.getString("ProcessState").trim();

            if( rs.getString("ResultStatus") == null )
                this.ResultStatus = null;
            else
                this.ResultStatus = rs.getString("ResultStatus").trim();

            if( rs.getString("ResultInfoDesc") == null )
                this.ResultInfoDesc = null;
            else
                this.ResultInfoDesc = rs.getString("ResultInfoDesc").trim();

            if( rs.getString("InfoMessage") == null )
                this.InfoMessage = null;
            else
                this.InfoMessage = rs.getString("InfoMessage").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LETransOrderStatusSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LETransOrderStatusSchema getSchema() {
        LETransOrderStatusSchema aLETransOrderStatusSchema = new LETransOrderStatusSchema();
        aLETransOrderStatusSchema.setSchema(this);
        return aLETransOrderStatusSchema;
    }

    public LETransOrderStatusDB getDB() {
        LETransOrderStatusDB aDBOper = new LETransOrderStatusDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLETransOrderStatus描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TradeName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InstitutionCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OrderSerialNumber)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TransRefGUID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProductCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProductName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolicySerialNumber)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApplicationNumber)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CheckPayNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayOrderSerialNumber)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProcessState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ResultStatus)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ResultInfoDesc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InfoMessage)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLETransOrderStatus>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            TradeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            InstitutionCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            OrderSerialNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            TransRefGUID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ProductCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ProductName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            PolicySerialNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ApplicationNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            CheckPayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            PayOrderSerialNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            ProcessState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            ResultStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            ResultInfoDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            InfoMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LETransOrderStatusSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("TransType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
        }
        if (FCode.equalsIgnoreCase("TradeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TradeName));
        }
        if (FCode.equalsIgnoreCase("InstitutionCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InstitutionCode));
        }
        if (FCode.equalsIgnoreCase("OrderSerialNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderSerialNumber));
        }
        if (FCode.equalsIgnoreCase("TransRefGUID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransRefGUID));
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProductCode));
        }
        if (FCode.equalsIgnoreCase("ProductName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProductName));
        }
        if (FCode.equalsIgnoreCase("PolicySerialNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicySerialNumber));
        }
        if (FCode.equalsIgnoreCase("ApplicationNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplicationNumber));
        }
        if (FCode.equalsIgnoreCase("CheckPayNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckPayNo));
        }
        if (FCode.equalsIgnoreCase("PayOrderSerialNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayOrderSerialNumber));
        }
        if (FCode.equalsIgnoreCase("ProcessState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProcessState));
        }
        if (FCode.equalsIgnoreCase("ResultStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultStatus));
        }
        if (FCode.equalsIgnoreCase("ResultInfoDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultInfoDesc));
        }
        if (FCode.equalsIgnoreCase("InfoMessage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InfoMessage));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TransType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TradeName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InstitutionCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(OrderSerialNumber);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(TransRefGUID);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ProductCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ProductName);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PolicySerialNumber);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ApplicationNumber);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(CheckPayNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PayOrderSerialNumber);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ProcessState);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ResultStatus);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ResultInfoDesc);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InfoMessage);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("TransType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransType = FValue.trim();
            }
            else
                TransType = null;
        }
        if (FCode.equalsIgnoreCase("TradeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                TradeName = FValue.trim();
            }
            else
                TradeName = null;
        }
        if (FCode.equalsIgnoreCase("InstitutionCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InstitutionCode = FValue.trim();
            }
            else
                InstitutionCode = null;
        }
        if (FCode.equalsIgnoreCase("OrderSerialNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrderSerialNumber = FValue.trim();
            }
            else
                OrderSerialNumber = null;
        }
        if (FCode.equalsIgnoreCase("TransRefGUID")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransRefGUID = FValue.trim();
            }
            else
                TransRefGUID = null;
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProductCode = FValue.trim();
            }
            else
                ProductCode = null;
        }
        if (FCode.equalsIgnoreCase("ProductName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProductName = FValue.trim();
            }
            else
                ProductName = null;
        }
        if (FCode.equalsIgnoreCase("PolicySerialNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicySerialNumber = FValue.trim();
            }
            else
                PolicySerialNumber = null;
        }
        if (FCode.equalsIgnoreCase("ApplicationNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplicationNumber = FValue.trim();
            }
            else
                ApplicationNumber = null;
        }
        if (FCode.equalsIgnoreCase("CheckPayNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CheckPayNo = FValue.trim();
            }
            else
                CheckPayNo = null;
        }
        if (FCode.equalsIgnoreCase("PayOrderSerialNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayOrderSerialNumber = FValue.trim();
            }
            else
                PayOrderSerialNumber = null;
        }
        if (FCode.equalsIgnoreCase("ProcessState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProcessState = FValue.trim();
            }
            else
                ProcessState = null;
        }
        if (FCode.equalsIgnoreCase("ResultStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultStatus = FValue.trim();
            }
            else
                ResultStatus = null;
        }
        if (FCode.equalsIgnoreCase("ResultInfoDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultInfoDesc = FValue.trim();
            }
            else
                ResultInfoDesc = null;
        }
        if (FCode.equalsIgnoreCase("InfoMessage")) {
            if( FValue != null && !FValue.equals(""))
            {
                InfoMessage = FValue.trim();
            }
            else
                InfoMessage = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LETransOrderStatusSchema other = (LETransOrderStatusSchema)otherObject;
        return
            SerialNo.equals(other.getSerialNo())
            && TransType.equals(other.getTransType())
            && TradeName.equals(other.getTradeName())
            && InstitutionCode.equals(other.getInstitutionCode())
            && OrderSerialNumber.equals(other.getOrderSerialNumber())
            && TransRefGUID.equals(other.getTransRefGUID())
            && ProductCode.equals(other.getProductCode())
            && ProductName.equals(other.getProductName())
            && PolicySerialNumber.equals(other.getPolicySerialNumber())
            && ApplicationNumber.equals(other.getApplicationNumber())
            && CheckPayNo.equals(other.getCheckPayNo())
            && PayOrderSerialNumber.equals(other.getPayOrderSerialNumber())
            && ProcessState.equals(other.getProcessState())
            && ResultStatus.equals(other.getResultStatus())
            && ResultInfoDesc.equals(other.getResultInfoDesc())
            && InfoMessage.equals(other.getInfoMessage())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("TransType") ) {
            return 1;
        }
        if( strFieldName.equals("TradeName") ) {
            return 2;
        }
        if( strFieldName.equals("InstitutionCode") ) {
            return 3;
        }
        if( strFieldName.equals("OrderSerialNumber") ) {
            return 4;
        }
        if( strFieldName.equals("TransRefGUID") ) {
            return 5;
        }
        if( strFieldName.equals("ProductCode") ) {
            return 6;
        }
        if( strFieldName.equals("ProductName") ) {
            return 7;
        }
        if( strFieldName.equals("PolicySerialNumber") ) {
            return 8;
        }
        if( strFieldName.equals("ApplicationNumber") ) {
            return 9;
        }
        if( strFieldName.equals("CheckPayNo") ) {
            return 10;
        }
        if( strFieldName.equals("PayOrderSerialNumber") ) {
            return 11;
        }
        if( strFieldName.equals("ProcessState") ) {
            return 12;
        }
        if( strFieldName.equals("ResultStatus") ) {
            return 13;
        }
        if( strFieldName.equals("ResultInfoDesc") ) {
            return 14;
        }
        if( strFieldName.equals("InfoMessage") ) {
            return 15;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 16;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 19;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "TransType";
                break;
            case 2:
                strFieldName = "TradeName";
                break;
            case 3:
                strFieldName = "InstitutionCode";
                break;
            case 4:
                strFieldName = "OrderSerialNumber";
                break;
            case 5:
                strFieldName = "TransRefGUID";
                break;
            case 6:
                strFieldName = "ProductCode";
                break;
            case 7:
                strFieldName = "ProductName";
                break;
            case 8:
                strFieldName = "PolicySerialNumber";
                break;
            case 9:
                strFieldName = "ApplicationNumber";
                break;
            case 10:
                strFieldName = "CheckPayNo";
                break;
            case 11:
                strFieldName = "PayOrderSerialNumber";
                break;
            case 12:
                strFieldName = "ProcessState";
                break;
            case 13:
                strFieldName = "ResultStatus";
                break;
            case 14:
                strFieldName = "ResultInfoDesc";
                break;
            case 15:
                strFieldName = "InfoMessage";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "TRANSTYPE":
                return Schema.TYPE_STRING;
            case "TRADENAME":
                return Schema.TYPE_STRING;
            case "INSTITUTIONCODE":
                return Schema.TYPE_STRING;
            case "ORDERSERIALNUMBER":
                return Schema.TYPE_STRING;
            case "TRANSREFGUID":
                return Schema.TYPE_STRING;
            case "PRODUCTCODE":
                return Schema.TYPE_STRING;
            case "PRODUCTNAME":
                return Schema.TYPE_STRING;
            case "POLICYSERIALNUMBER":
                return Schema.TYPE_STRING;
            case "APPLICATIONNUMBER":
                return Schema.TYPE_STRING;
            case "CHECKPAYNO":
                return Schema.TYPE_STRING;
            case "PAYORDERSERIALNUMBER":
                return Schema.TYPE_STRING;
            case "PROCESSSTATE":
                return Schema.TYPE_STRING;
            case "RESULTSTATUS":
                return Schema.TYPE_STRING;
            case "RESULTINFODESC":
                return Schema.TYPE_STRING;
            case "INFOMESSAGE":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
