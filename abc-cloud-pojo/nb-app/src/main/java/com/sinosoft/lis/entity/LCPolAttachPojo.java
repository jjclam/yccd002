/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCPolPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LCPolAttachPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long PolID; 
    /** Fk_lccont */
    private long ContID; 
    /** Shardingid */
    private String ShardingID; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体保单险种号码 */
    private String GrpPolNo; 
    /** 合同号码 */
    private String ContNo; 
    /** 保单险种号码 */
    private String PolNo; 
    /** 投保单险种号码 */
    private String ProposalNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 总单类型 */
    private String ContType; 
    /** 保单类型标记 */
    private String PolTypeFlag; 
    /** 主险保单号码 */
    private String MainPolNo; 
    /** 主被保人保单号码 */
    private String MasterPolNo; 
    /** 险类编码 */
    private String KindCode; 
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVersion; 
    /** 管理机构 */
    private String ManageCom; 
    /** 代理机构 */
    private String AgentCom; 
    /** 代理机构内部分类 */
    private String AgentType; 
    /** 代理人编码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 联合代理人代码 */
    private String AgentCode1; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 经办人 */
    private String Handler; 
    /** 被保人客户号码 */
    private String InsuredNo; 
    /** 被保人名称 */
    private String InsuredName; 
    /** 被保人性别 */
    private String InsuredSex; 
    /** 被保人生日 */
    private String  InsuredBirthday;
    /** 被保人投保年龄 */
    private int InsuredAppAge; 
    /** 被保人数目 */
    private int InsuredPeoples; 
    /** 被保人职业类别/工种编码 */
    private String OccupationType; 
    /** 投保人客户号码 */
    private String AppntNo; 
    /** 投保人名称 */
    private String AppntName; 
    /** 险种生效日期 */
    private String  CValiDate;
    /** 签单机构 */
    private String SignCom; 
    /** 签单日期 */
    private String  SignDate;
    /** 签单时间 */
    private String SignTime; 
    /** 首期交费日期 */
    private String  FirstPayDate;
    /** 终交日期 */
    private String  PayEndDate;
    /** 交至日期 */
    private String  PaytoDate;
    /** 起领日期 */
    private String  GetStartDate;
    /** 保险责任终止日期 */
    private String  EndDate;
    /** 意外责任终止日期 */
    private String  AcciEndDate;
    /** 领取年龄年期标志 */
    private String GetYearFlag; 
    /** 领取年龄年期 */
    private int GetYear; 
    /** 终交年龄年期标志 */
    private String PayEndYearFlag; 
    /** 终交年龄年期 */
    private int PayEndYear; 
    /** 保险年龄年期标志 */
    private String InsuYearFlag; 
    /** 保险年龄年期 */
    private int InsuYear; 
    /** 意外年龄年期标志 */
    private String AcciYearFlag; 
    /** 意外年龄年期 */
    private int AcciYear; 
    /** 起领日期计算类型 */
    private String GetStartType; 
    /** 是否指定生效日期 */
    private String SpecifyValiDate; 
    /** 交费方式 */
    private String PayMode; 
    /** 交费位置 */
    private String PayLocation; 
    /** 交费间隔 */
    private int PayIntv; 
    /** 交费年期 */
    private int PayYears; 
    /** 保险年期 */
    private int Years; 
    /** 管理费比例 */
    private double ManageFeeRate; 
    /** 浮动费率 */
    private double FloatRate; 
    /** 保费算保额标志 */
    private String PremToAmnt; 
    /** 总份数 */
    private double Mult; 
    /** 总标准保费 */
    private double StandPrem; 
    /** 总保费 */
    private double Prem; 
    /** 总累计保费 */
    private double SumPrem; 
    /** 总基本保额 */
    private double Amnt; 
    /** 总风险保额 */
    private double RiskAmnt; 
    /** 余额 */
    private double LeavingMoney; 
    /** 批改次数 */
    private int EndorseTimes; 
    /** 理赔次数 */
    private int ClaimTimes; 
    /** 生存领取次数 */
    private int LiveTimes; 
    /** 续保次数 */
    private int RenewCount; 
    /** 最后一次给付日期 */
    private String  LastGetDate;
    /** 最后一次借款日期 */
    private String  LastLoanDate;
    /** 最后一次催收日期 */
    private String  LastRegetDate;
    /** 最后一次保全日期 */
    private String  LastEdorDate;
    /** 最近复效日期 */
    private String  LastRevDate;
    /** 续保标志 */
    private int RnewFlag; 
    /** 停交标志 */
    private String StopFlag; 
    /** 满期标志 */
    private String ExpiryFlag; 
    /** 自动垫交标志 */
    private String AutoPayFlag; 
    /** 利差返还方式 */
    private String InterestDifFlag; 
    /** 减额交清标志 */
    private String SubFlag; 
    /** 受益人标记 */
    private String BnfFlag; 
    /** 是否体检件标志 */
    private String HealthCheckFlag; 
    /** 告知标志 */
    private String ImpartFlag; 
    /** 商业分保标记 */
    private String ReinsureFlag; 
    /** 代收标志 */
    private String AgentPayFlag; 
    /** 代付标志 */
    private String AgentGetFlag; 
    /** 生存金领取方式 */
    private String LiveGetMode; 
    /** 身故金领取方式 */
    private String DeadGetMode; 
    /** 红利金领取方式 */
    private String BonusGetMode; 
    /** 红利金领取人 */
    private String BonusMan; 
    /** 被保人、投保人死亡标志 */
    private String DeadFlag; 
    /** 是否吸烟标志 */
    private String SmokeFlag; 
    /** 备注 */
    private String Remark; 
    /** 复核状态 */
    private String ApproveFlag; 
    /** 复核人编码 */
    private String ApproveCode; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 
    /** 核保状态 */
    private String UWFlag; 
    /** 最终核保人编码 */
    private String UWCode; 
    /** 核保完成日期 */
    private String  UWDate;
    /** 核保完成时间 */
    private String UWTime; 
    /** 投保单申请日期 */
    private String  PolApplyDate;
    /** 投保单/保单标志 */
    private String AppFlag; 
    /** 其它保单状态 */
    private String PolState; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 等待期 */
    private int WaitPeriod; 
    /** 领取形式 */
    private String GetForm; 
    /** 领取银行编码 */
    private String GetBankCode; 
    /** 领取银行账户 */
    private String GetBankAccNo; 
    /** 领取银行户名 */
    private String GetAccName; 
    /** 不丧失价值选择 */
    private String KeepValueOpt; 
    /** 归属规则编码 */
    private String AscriptionRuleCode; 
    /** 缴费规则编码 */
    private String PayRuleCode; 
    /** 归属标记 */
    private String AscriptionFlag; 
    /** 自动应用团体帐户标记 */
    private String AutoPubAccFlag; 
    /** 组合标记 */
    private String CombiFlag; 
    /** 投资规则编码 */
    private String InvestRuleCode; 
    /** 投连账户生效日标志 */
    private String UintLinkValiFlag; 
    /** 产品组合编码 */
    private String ProdSetCode; 
    /** 是否通过风险测试 */
    private String InsurPolFlag; 
    /** 临分标记 */
    private String NewReinsureFlag; 
    /** 生存金是否累计生息标记 */
    private String LiveAccFlag; 
    /** 团险标准费率保费 */
    private double StandRatePrem; 
    /** 投保人firstname */
    private String AppntFirstName; 
    /** 投保人lastname */
    private String AppntLastName; 
    /** 被保人firstname */
    private String InsuredFirstName; 
    /** 被保人lastname */
    private String InsuredLastName; 
    /** 分期给付方式 */
    private String FQGetMode; 
    /** 给付期间 */
    private String GetPeriod; 
    /** 给付期间单位 */
    private String GetPeriodFlag; 
    /** 递延期间 */
    private String DelayPeriod; 
    /** 告知其他公司未成年人身故保额 */
    private double OtherAmnt; 
    /** 关联客户关系 */
    private String Relation; 
    /** 关联客户号 */
    private String ReCusNo; 
    /** 自动续保标记1 */
    private String AutoRnewAge; 
    /** 自动续保标记2 */
    private String AutoRnewYear; 


    public static final int FIELDNUM = 146;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getPolID() {
        return PolID;
    }
    public void setPolID(long aPolID) {
        PolID = aPolID;
    }
    public void setPolID(String aPolID) {
        if (aPolID != null && !aPolID.equals("")) {
            PolID = new Long(aPolID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getPolTypeFlag() {
        return PolTypeFlag;
    }
    public void setPolTypeFlag(String aPolTypeFlag) {
        PolTypeFlag = aPolTypeFlag;
    }
    public String getMainPolNo() {
        return MainPolNo;
    }
    public void setMainPolNo(String aMainPolNo) {
        MainPolNo = aMainPolNo;
    }
    public String getMasterPolNo() {
        return MasterPolNo;
    }
    public void setMasterPolNo(String aMasterPolNo) {
        MasterPolNo = aMasterPolNo;
    }
    public String getKindCode() {
        return KindCode;
    }
    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVersion() {
        return RiskVersion;
    }
    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode1() {
        return AgentCode1;
    }
    public void setAgentCode1(String aAgentCode1) {
        AgentCode1 = aAgentCode1;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getHandler() {
        return Handler;
    }
    public void setHandler(String aHandler) {
        Handler = aHandler;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getInsuredSex() {
        return InsuredSex;
    }
    public void setInsuredSex(String aInsuredSex) {
        InsuredSex = aInsuredSex;
    }
    public String getInsuredBirthday() {
        return InsuredBirthday;
    }
    public void setInsuredBirthday(String aInsuredBirthday) {
        InsuredBirthday = aInsuredBirthday;
    }
    public int getInsuredAppAge() {
        return InsuredAppAge;
    }
    public void setInsuredAppAge(int aInsuredAppAge) {
        InsuredAppAge = aInsuredAppAge;
    }
    public void setInsuredAppAge(String aInsuredAppAge) {
        if (aInsuredAppAge != null && !aInsuredAppAge.equals("")) {
            Integer tInteger = new Integer(aInsuredAppAge);
            int i = tInteger.intValue();
            InsuredAppAge = i;
        }
    }

    public int getInsuredPeoples() {
        return InsuredPeoples;
    }
    public void setInsuredPeoples(int aInsuredPeoples) {
        InsuredPeoples = aInsuredPeoples;
    }
    public void setInsuredPeoples(String aInsuredPeoples) {
        if (aInsuredPeoples != null && !aInsuredPeoples.equals("")) {
            Integer tInteger = new Integer(aInsuredPeoples);
            int i = tInteger.intValue();
            InsuredPeoples = i;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public String getSignCom() {
        return SignCom;
    }
    public void setSignCom(String aSignCom) {
        SignCom = aSignCom;
    }
    public String getSignDate() {
        return SignDate;
    }
    public void setSignDate(String aSignDate) {
        SignDate = aSignDate;
    }
    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getFirstPayDate() {
        return FirstPayDate;
    }
    public void setFirstPayDate(String aFirstPayDate) {
        FirstPayDate = aFirstPayDate;
    }
    public String getPayEndDate() {
        return PayEndDate;
    }
    public void setPayEndDate(String aPayEndDate) {
        PayEndDate = aPayEndDate;
    }
    public String getPaytoDate() {
        return PaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public String getGetStartDate() {
        return GetStartDate;
    }
    public void setGetStartDate(String aGetStartDate) {
        GetStartDate = aGetStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getAcciEndDate() {
        return AcciEndDate;
    }
    public void setAcciEndDate(String aAcciEndDate) {
        AcciEndDate = aAcciEndDate;
    }
    public String getGetYearFlag() {
        return GetYearFlag;
    }
    public void setGetYearFlag(String aGetYearFlag) {
        GetYearFlag = aGetYearFlag;
    }
    public int getGetYear() {
        return GetYear;
    }
    public void setGetYear(int aGetYear) {
        GetYear = aGetYear;
    }
    public void setGetYear(String aGetYear) {
        if (aGetYear != null && !aGetYear.equals("")) {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }
    public void setPayEndYearFlag(String aPayEndYearFlag) {
        PayEndYearFlag = aPayEndYearFlag;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getAcciYearFlag() {
        return AcciYearFlag;
    }
    public void setAcciYearFlag(String aAcciYearFlag) {
        AcciYearFlag = aAcciYearFlag;
    }
    public int getAcciYear() {
        return AcciYear;
    }
    public void setAcciYear(int aAcciYear) {
        AcciYear = aAcciYear;
    }
    public void setAcciYear(String aAcciYear) {
        if (aAcciYear != null && !aAcciYear.equals("")) {
            Integer tInteger = new Integer(aAcciYear);
            int i = tInteger.intValue();
            AcciYear = i;
        }
    }

    public String getGetStartType() {
        return GetStartType;
    }
    public void setGetStartType(String aGetStartType) {
        GetStartType = aGetStartType;
    }
    public String getSpecifyValiDate() {
        return SpecifyValiDate;
    }
    public void setSpecifyValiDate(String aSpecifyValiDate) {
        SpecifyValiDate = aSpecifyValiDate;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getPayLocation() {
        return PayLocation;
    }
    public void setPayLocation(String aPayLocation) {
        PayLocation = aPayLocation;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public int getPayYears() {
        return PayYears;
    }
    public void setPayYears(int aPayYears) {
        PayYears = aPayYears;
    }
    public void setPayYears(String aPayYears) {
        if (aPayYears != null && !aPayYears.equals("")) {
            Integer tInteger = new Integer(aPayYears);
            int i = tInteger.intValue();
            PayYears = i;
        }
    }

    public int getYears() {
        return Years;
    }
    public void setYears(int aYears) {
        Years = aYears;
    }
    public void setYears(String aYears) {
        if (aYears != null && !aYears.equals("")) {
            Integer tInteger = new Integer(aYears);
            int i = tInteger.intValue();
            Years = i;
        }
    }

    public double getManageFeeRate() {
        return ManageFeeRate;
    }
    public void setManageFeeRate(double aManageFeeRate) {
        ManageFeeRate = aManageFeeRate;
    }
    public void setManageFeeRate(String aManageFeeRate) {
        if (aManageFeeRate != null && !aManageFeeRate.equals("")) {
            Double tDouble = new Double(aManageFeeRate);
            double d = tDouble.doubleValue();
            ManageFeeRate = d;
        }
    }

    public double getFloatRate() {
        return FloatRate;
    }
    public void setFloatRate(double aFloatRate) {
        FloatRate = aFloatRate;
    }
    public void setFloatRate(String aFloatRate) {
        if (aFloatRate != null && !aFloatRate.equals("")) {
            Double tDouble = new Double(aFloatRate);
            double d = tDouble.doubleValue();
            FloatRate = d;
        }
    }

    public String getPremToAmnt() {
        return PremToAmnt;
    }
    public void setPremToAmnt(String aPremToAmnt) {
        PremToAmnt = aPremToAmnt;
    }
    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getStandPrem() {
        return StandPrem;
    }
    public void setStandPrem(double aStandPrem) {
        StandPrem = aStandPrem;
    }
    public void setStandPrem(String aStandPrem) {
        if (aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getRiskAmnt() {
        return RiskAmnt;
    }
    public void setRiskAmnt(double aRiskAmnt) {
        RiskAmnt = aRiskAmnt;
    }
    public void setRiskAmnt(String aRiskAmnt) {
        if (aRiskAmnt != null && !aRiskAmnt.equals("")) {
            Double tDouble = new Double(aRiskAmnt);
            double d = tDouble.doubleValue();
            RiskAmnt = d;
        }
    }

    public double getLeavingMoney() {
        return LeavingMoney;
    }
    public void setLeavingMoney(double aLeavingMoney) {
        LeavingMoney = aLeavingMoney;
    }
    public void setLeavingMoney(String aLeavingMoney) {
        if (aLeavingMoney != null && !aLeavingMoney.equals("")) {
            Double tDouble = new Double(aLeavingMoney);
            double d = tDouble.doubleValue();
            LeavingMoney = d;
        }
    }

    public int getEndorseTimes() {
        return EndorseTimes;
    }
    public void setEndorseTimes(int aEndorseTimes) {
        EndorseTimes = aEndorseTimes;
    }
    public void setEndorseTimes(String aEndorseTimes) {
        if (aEndorseTimes != null && !aEndorseTimes.equals("")) {
            Integer tInteger = new Integer(aEndorseTimes);
            int i = tInteger.intValue();
            EndorseTimes = i;
        }
    }

    public int getClaimTimes() {
        return ClaimTimes;
    }
    public void setClaimTimes(int aClaimTimes) {
        ClaimTimes = aClaimTimes;
    }
    public void setClaimTimes(String aClaimTimes) {
        if (aClaimTimes != null && !aClaimTimes.equals("")) {
            Integer tInteger = new Integer(aClaimTimes);
            int i = tInteger.intValue();
            ClaimTimes = i;
        }
    }

    public int getLiveTimes() {
        return LiveTimes;
    }
    public void setLiveTimes(int aLiveTimes) {
        LiveTimes = aLiveTimes;
    }
    public void setLiveTimes(String aLiveTimes) {
        if (aLiveTimes != null && !aLiveTimes.equals("")) {
            Integer tInteger = new Integer(aLiveTimes);
            int i = tInteger.intValue();
            LiveTimes = i;
        }
    }

    public int getRenewCount() {
        return RenewCount;
    }
    public void setRenewCount(int aRenewCount) {
        RenewCount = aRenewCount;
    }
    public void setRenewCount(String aRenewCount) {
        if (aRenewCount != null && !aRenewCount.equals("")) {
            Integer tInteger = new Integer(aRenewCount);
            int i = tInteger.intValue();
            RenewCount = i;
        }
    }

    public String getLastGetDate() {
        return LastGetDate;
    }
    public void setLastGetDate(String aLastGetDate) {
        LastGetDate = aLastGetDate;
    }
    public String getLastLoanDate() {
        return LastLoanDate;
    }
    public void setLastLoanDate(String aLastLoanDate) {
        LastLoanDate = aLastLoanDate;
    }
    public String getLastRegetDate() {
        return LastRegetDate;
    }
    public void setLastRegetDate(String aLastRegetDate) {
        LastRegetDate = aLastRegetDate;
    }
    public String getLastEdorDate() {
        return LastEdorDate;
    }
    public void setLastEdorDate(String aLastEdorDate) {
        LastEdorDate = aLastEdorDate;
    }
    public String getLastRevDate() {
        return LastRevDate;
    }
    public void setLastRevDate(String aLastRevDate) {
        LastRevDate = aLastRevDate;
    }
    public int getRnewFlag() {
        return RnewFlag;
    }
    public void setRnewFlag(int aRnewFlag) {
        RnewFlag = aRnewFlag;
    }
    public void setRnewFlag(String aRnewFlag) {
        if (aRnewFlag != null && !aRnewFlag.equals("")) {
            Integer tInteger = new Integer(aRnewFlag);
            int i = tInteger.intValue();
            RnewFlag = i;
        }
    }

    public String getStopFlag() {
        return StopFlag;
    }
    public void setStopFlag(String aStopFlag) {
        StopFlag = aStopFlag;
    }
    public String getExpiryFlag() {
        return ExpiryFlag;
    }
    public void setExpiryFlag(String aExpiryFlag) {
        ExpiryFlag = aExpiryFlag;
    }
    public String getAutoPayFlag() {
        return AutoPayFlag;
    }
    public void setAutoPayFlag(String aAutoPayFlag) {
        AutoPayFlag = aAutoPayFlag;
    }
    public String getInterestDifFlag() {
        return InterestDifFlag;
    }
    public void setInterestDifFlag(String aInterestDifFlag) {
        InterestDifFlag = aInterestDifFlag;
    }
    public String getSubFlag() {
        return SubFlag;
    }
    public void setSubFlag(String aSubFlag) {
        SubFlag = aSubFlag;
    }
    public String getBnfFlag() {
        return BnfFlag;
    }
    public void setBnfFlag(String aBnfFlag) {
        BnfFlag = aBnfFlag;
    }
    public String getHealthCheckFlag() {
        return HealthCheckFlag;
    }
    public void setHealthCheckFlag(String aHealthCheckFlag) {
        HealthCheckFlag = aHealthCheckFlag;
    }
    public String getImpartFlag() {
        return ImpartFlag;
    }
    public void setImpartFlag(String aImpartFlag) {
        ImpartFlag = aImpartFlag;
    }
    public String getReinsureFlag() {
        return ReinsureFlag;
    }
    public void setReinsureFlag(String aReinsureFlag) {
        ReinsureFlag = aReinsureFlag;
    }
    public String getAgentPayFlag() {
        return AgentPayFlag;
    }
    public void setAgentPayFlag(String aAgentPayFlag) {
        AgentPayFlag = aAgentPayFlag;
    }
    public String getAgentGetFlag() {
        return AgentGetFlag;
    }
    public void setAgentGetFlag(String aAgentGetFlag) {
        AgentGetFlag = aAgentGetFlag;
    }
    public String getLiveGetMode() {
        return LiveGetMode;
    }
    public void setLiveGetMode(String aLiveGetMode) {
        LiveGetMode = aLiveGetMode;
    }
    public String getDeadGetMode() {
        return DeadGetMode;
    }
    public void setDeadGetMode(String aDeadGetMode) {
        DeadGetMode = aDeadGetMode;
    }
    public String getBonusGetMode() {
        return BonusGetMode;
    }
    public void setBonusGetMode(String aBonusGetMode) {
        BonusGetMode = aBonusGetMode;
    }
    public String getBonusMan() {
        return BonusMan;
    }
    public void setBonusMan(String aBonusMan) {
        BonusMan = aBonusMan;
    }
    public String getDeadFlag() {
        return DeadFlag;
    }
    public void setDeadFlag(String aDeadFlag) {
        DeadFlag = aDeadFlag;
    }
    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWCode() {
        return UWCode;
    }
    public void setUWCode(String aUWCode) {
        UWCode = aUWCode;
    }
    public String getUWDate() {
        return UWDate;
    }
    public void setUWDate(String aUWDate) {
        UWDate = aUWDate;
    }
    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getPolApplyDate() {
        return PolApplyDate;
    }
    public void setPolApplyDate(String aPolApplyDate) {
        PolApplyDate = aPolApplyDate;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getPolState() {
        return PolState;
    }
    public void setPolState(String aPolState) {
        PolState = aPolState;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public int getWaitPeriod() {
        return WaitPeriod;
    }
    public void setWaitPeriod(int aWaitPeriod) {
        WaitPeriod = aWaitPeriod;
    }
    public void setWaitPeriod(String aWaitPeriod) {
        if (aWaitPeriod != null && !aWaitPeriod.equals("")) {
            Integer tInteger = new Integer(aWaitPeriod);
            int i = tInteger.intValue();
            WaitPeriod = i;
        }
    }

    public String getGetForm() {
        return GetForm;
    }
    public void setGetForm(String aGetForm) {
        GetForm = aGetForm;
    }
    public String getGetBankCode() {
        return GetBankCode;
    }
    public void setGetBankCode(String aGetBankCode) {
        GetBankCode = aGetBankCode;
    }
    public String getGetBankAccNo() {
        return GetBankAccNo;
    }
    public void setGetBankAccNo(String aGetBankAccNo) {
        GetBankAccNo = aGetBankAccNo;
    }
    public String getGetAccName() {
        return GetAccName;
    }
    public void setGetAccName(String aGetAccName) {
        GetAccName = aGetAccName;
    }
    public String getKeepValueOpt() {
        return KeepValueOpt;
    }
    public void setKeepValueOpt(String aKeepValueOpt) {
        KeepValueOpt = aKeepValueOpt;
    }
    public String getAscriptionRuleCode() {
        return AscriptionRuleCode;
    }
    public void setAscriptionRuleCode(String aAscriptionRuleCode) {
        AscriptionRuleCode = aAscriptionRuleCode;
    }
    public String getPayRuleCode() {
        return PayRuleCode;
    }
    public void setPayRuleCode(String aPayRuleCode) {
        PayRuleCode = aPayRuleCode;
    }
    public String getAscriptionFlag() {
        return AscriptionFlag;
    }
    public void setAscriptionFlag(String aAscriptionFlag) {
        AscriptionFlag = aAscriptionFlag;
    }
    public String getAutoPubAccFlag() {
        return AutoPubAccFlag;
    }
    public void setAutoPubAccFlag(String aAutoPubAccFlag) {
        AutoPubAccFlag = aAutoPubAccFlag;
    }
    public String getCombiFlag() {
        return CombiFlag;
    }
    public void setCombiFlag(String aCombiFlag) {
        CombiFlag = aCombiFlag;
    }
    public String getInvestRuleCode() {
        return InvestRuleCode;
    }
    public void setInvestRuleCode(String aInvestRuleCode) {
        InvestRuleCode = aInvestRuleCode;
    }
    public String getUintLinkValiFlag() {
        return UintLinkValiFlag;
    }
    public void setUintLinkValiFlag(String aUintLinkValiFlag) {
        UintLinkValiFlag = aUintLinkValiFlag;
    }
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getInsurPolFlag() {
        return InsurPolFlag;
    }
    public void setInsurPolFlag(String aInsurPolFlag) {
        InsurPolFlag = aInsurPolFlag;
    }
    public String getNewReinsureFlag() {
        return NewReinsureFlag;
    }
    public void setNewReinsureFlag(String aNewReinsureFlag) {
        NewReinsureFlag = aNewReinsureFlag;
    }
    public String getLiveAccFlag() {
        return LiveAccFlag;
    }
    public void setLiveAccFlag(String aLiveAccFlag) {
        LiveAccFlag = aLiveAccFlag;
    }
    public double getStandRatePrem() {
        return StandRatePrem;
    }
    public void setStandRatePrem(double aStandRatePrem) {
        StandRatePrem = aStandRatePrem;
    }
    public void setStandRatePrem(String aStandRatePrem) {
        if (aStandRatePrem != null && !aStandRatePrem.equals("")) {
            Double tDouble = new Double(aStandRatePrem);
            double d = tDouble.doubleValue();
            StandRatePrem = d;
        }
    }

    public String getAppntFirstName() {
        return AppntFirstName;
    }
    public void setAppntFirstName(String aAppntFirstName) {
        AppntFirstName = aAppntFirstName;
    }
    public String getAppntLastName() {
        return AppntLastName;
    }
    public void setAppntLastName(String aAppntLastName) {
        AppntLastName = aAppntLastName;
    }
    public String getInsuredFirstName() {
        return InsuredFirstName;
    }
    public void setInsuredFirstName(String aInsuredFirstName) {
        InsuredFirstName = aInsuredFirstName;
    }
    public String getInsuredLastName() {
        return InsuredLastName;
    }
    public void setInsuredLastName(String aInsuredLastName) {
        InsuredLastName = aInsuredLastName;
    }
    public String getFQGetMode() {
        return FQGetMode;
    }
    public void setFQGetMode(String aFQGetMode) {
        FQGetMode = aFQGetMode;
    }
    public String getGetPeriod() {
        return GetPeriod;
    }
    public void setGetPeriod(String aGetPeriod) {
        GetPeriod = aGetPeriod;
    }
    public String getGetPeriodFlag() {
        return GetPeriodFlag;
    }
    public void setGetPeriodFlag(String aGetPeriodFlag) {
        GetPeriodFlag = aGetPeriodFlag;
    }
    public String getDelayPeriod() {
        return DelayPeriod;
    }
    public void setDelayPeriod(String aDelayPeriod) {
        DelayPeriod = aDelayPeriod;
    }
    public double getOtherAmnt() {
        return OtherAmnt;
    }
    public void setOtherAmnt(double aOtherAmnt) {
        OtherAmnt = aOtherAmnt;
    }
    public void setOtherAmnt(String aOtherAmnt) {
        if (aOtherAmnt != null && !aOtherAmnt.equals("")) {
            Double tDouble = new Double(aOtherAmnt);
            double d = tDouble.doubleValue();
            OtherAmnt = d;
        }
    }

    public String getRelation() {
        return Relation;
    }
    public void setRelation(String aRelation) {
        Relation = aRelation;
    }
    public String getReCusNo() {
        return ReCusNo;
    }
    public void setReCusNo(String aReCusNo) {
        ReCusNo = aReCusNo;
    }
    public String getAutoRnewAge() {
        return AutoRnewAge;
    }
    public void setAutoRnewAge(String aAutoRnewAge) {
        AutoRnewAge = aAutoRnewAge;
    }
    public String getAutoRnewYear() {
        return AutoRnewYear;
    }
    public void setAutoRnewYear(String aAutoRnewYear) {
        AutoRnewYear = aAutoRnewYear;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PolID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 4;
        }
        if( strFieldName.equals("ContNo") ) {
            return 5;
        }
        if( strFieldName.equals("PolNo") ) {
            return 6;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 7;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 8;
        }
        if( strFieldName.equals("ContType") ) {
            return 9;
        }
        if( strFieldName.equals("PolTypeFlag") ) {
            return 10;
        }
        if( strFieldName.equals("MainPolNo") ) {
            return 11;
        }
        if( strFieldName.equals("MasterPolNo") ) {
            return 12;
        }
        if( strFieldName.equals("KindCode") ) {
            return 13;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 14;
        }
        if( strFieldName.equals("RiskVersion") ) {
            return 15;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 16;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 17;
        }
        if( strFieldName.equals("AgentType") ) {
            return 18;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 19;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 20;
        }
        if( strFieldName.equals("AgentCode1") ) {
            return 21;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 22;
        }
        if( strFieldName.equals("Handler") ) {
            return 23;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 24;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 25;
        }
        if( strFieldName.equals("InsuredSex") ) {
            return 26;
        }
        if( strFieldName.equals("InsuredBirthday") ) {
            return 27;
        }
        if( strFieldName.equals("InsuredAppAge") ) {
            return 28;
        }
        if( strFieldName.equals("InsuredPeoples") ) {
            return 29;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 30;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 31;
        }
        if( strFieldName.equals("AppntName") ) {
            return 32;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 33;
        }
        if( strFieldName.equals("SignCom") ) {
            return 34;
        }
        if( strFieldName.equals("SignDate") ) {
            return 35;
        }
        if( strFieldName.equals("SignTime") ) {
            return 36;
        }
        if( strFieldName.equals("FirstPayDate") ) {
            return 37;
        }
        if( strFieldName.equals("PayEndDate") ) {
            return 38;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 39;
        }
        if( strFieldName.equals("GetStartDate") ) {
            return 40;
        }
        if( strFieldName.equals("EndDate") ) {
            return 41;
        }
        if( strFieldName.equals("AcciEndDate") ) {
            return 42;
        }
        if( strFieldName.equals("GetYearFlag") ) {
            return 43;
        }
        if( strFieldName.equals("GetYear") ) {
            return 44;
        }
        if( strFieldName.equals("PayEndYearFlag") ) {
            return 45;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 46;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 47;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 48;
        }
        if( strFieldName.equals("AcciYearFlag") ) {
            return 49;
        }
        if( strFieldName.equals("AcciYear") ) {
            return 50;
        }
        if( strFieldName.equals("GetStartType") ) {
            return 51;
        }
        if( strFieldName.equals("SpecifyValiDate") ) {
            return 52;
        }
        if( strFieldName.equals("PayMode") ) {
            return 53;
        }
        if( strFieldName.equals("PayLocation") ) {
            return 54;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 55;
        }
        if( strFieldName.equals("PayYears") ) {
            return 56;
        }
        if( strFieldName.equals("Years") ) {
            return 57;
        }
        if( strFieldName.equals("ManageFeeRate") ) {
            return 58;
        }
        if( strFieldName.equals("FloatRate") ) {
            return 59;
        }
        if( strFieldName.equals("PremToAmnt") ) {
            return 60;
        }
        if( strFieldName.equals("Mult") ) {
            return 61;
        }
        if( strFieldName.equals("StandPrem") ) {
            return 62;
        }
        if( strFieldName.equals("Prem") ) {
            return 63;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 64;
        }
        if( strFieldName.equals("Amnt") ) {
            return 65;
        }
        if( strFieldName.equals("RiskAmnt") ) {
            return 66;
        }
        if( strFieldName.equals("LeavingMoney") ) {
            return 67;
        }
        if( strFieldName.equals("EndorseTimes") ) {
            return 68;
        }
        if( strFieldName.equals("ClaimTimes") ) {
            return 69;
        }
        if( strFieldName.equals("LiveTimes") ) {
            return 70;
        }
        if( strFieldName.equals("RenewCount") ) {
            return 71;
        }
        if( strFieldName.equals("LastGetDate") ) {
            return 72;
        }
        if( strFieldName.equals("LastLoanDate") ) {
            return 73;
        }
        if( strFieldName.equals("LastRegetDate") ) {
            return 74;
        }
        if( strFieldName.equals("LastEdorDate") ) {
            return 75;
        }
        if( strFieldName.equals("LastRevDate") ) {
            return 76;
        }
        if( strFieldName.equals("RnewFlag") ) {
            return 77;
        }
        if( strFieldName.equals("StopFlag") ) {
            return 78;
        }
        if( strFieldName.equals("ExpiryFlag") ) {
            return 79;
        }
        if( strFieldName.equals("AutoPayFlag") ) {
            return 80;
        }
        if( strFieldName.equals("InterestDifFlag") ) {
            return 81;
        }
        if( strFieldName.equals("SubFlag") ) {
            return 82;
        }
        if( strFieldName.equals("BnfFlag") ) {
            return 83;
        }
        if( strFieldName.equals("HealthCheckFlag") ) {
            return 84;
        }
        if( strFieldName.equals("ImpartFlag") ) {
            return 85;
        }
        if( strFieldName.equals("ReinsureFlag") ) {
            return 86;
        }
        if( strFieldName.equals("AgentPayFlag") ) {
            return 87;
        }
        if( strFieldName.equals("AgentGetFlag") ) {
            return 88;
        }
        if( strFieldName.equals("LiveGetMode") ) {
            return 89;
        }
        if( strFieldName.equals("DeadGetMode") ) {
            return 90;
        }
        if( strFieldName.equals("BonusGetMode") ) {
            return 91;
        }
        if( strFieldName.equals("BonusMan") ) {
            return 92;
        }
        if( strFieldName.equals("DeadFlag") ) {
            return 93;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 94;
        }
        if( strFieldName.equals("Remark") ) {
            return 95;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 96;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 97;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 98;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 99;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 100;
        }
        if( strFieldName.equals("UWCode") ) {
            return 101;
        }
        if( strFieldName.equals("UWDate") ) {
            return 102;
        }
        if( strFieldName.equals("UWTime") ) {
            return 103;
        }
        if( strFieldName.equals("PolApplyDate") ) {
            return 104;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 105;
        }
        if( strFieldName.equals("PolState") ) {
            return 106;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 107;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 108;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 109;
        }
        if( strFieldName.equals("Operator") ) {
            return 110;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 111;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 112;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 113;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 114;
        }
        if( strFieldName.equals("WaitPeriod") ) {
            return 115;
        }
        if( strFieldName.equals("GetForm") ) {
            return 116;
        }
        if( strFieldName.equals("GetBankCode") ) {
            return 117;
        }
        if( strFieldName.equals("GetBankAccNo") ) {
            return 118;
        }
        if( strFieldName.equals("GetAccName") ) {
            return 119;
        }
        if( strFieldName.equals("KeepValueOpt") ) {
            return 120;
        }
        if( strFieldName.equals("AscriptionRuleCode") ) {
            return 121;
        }
        if( strFieldName.equals("PayRuleCode") ) {
            return 122;
        }
        if( strFieldName.equals("AscriptionFlag") ) {
            return 123;
        }
        if( strFieldName.equals("AutoPubAccFlag") ) {
            return 124;
        }
        if( strFieldName.equals("CombiFlag") ) {
            return 125;
        }
        if( strFieldName.equals("InvestRuleCode") ) {
            return 126;
        }
        if( strFieldName.equals("UintLinkValiFlag") ) {
            return 127;
        }
        if( strFieldName.equals("ProdSetCode") ) {
            return 128;
        }
        if( strFieldName.equals("InsurPolFlag") ) {
            return 129;
        }
        if( strFieldName.equals("NewReinsureFlag") ) {
            return 130;
        }
        if( strFieldName.equals("LiveAccFlag") ) {
            return 131;
        }
        if( strFieldName.equals("StandRatePrem") ) {
            return 132;
        }
        if( strFieldName.equals("AppntFirstName") ) {
            return 133;
        }
        if( strFieldName.equals("AppntLastName") ) {
            return 134;
        }
        if( strFieldName.equals("InsuredFirstName") ) {
            return 135;
        }
        if( strFieldName.equals("InsuredLastName") ) {
            return 136;
        }
        if( strFieldName.equals("FQGetMode") ) {
            return 137;
        }
        if( strFieldName.equals("GetPeriod") ) {
            return 138;
        }
        if( strFieldName.equals("GetPeriodFlag") ) {
            return 139;
        }
        if( strFieldName.equals("DelayPeriod") ) {
            return 140;
        }
        if( strFieldName.equals("OtherAmnt") ) {
            return 141;
        }
        if( strFieldName.equals("Relation") ) {
            return 142;
        }
        if( strFieldName.equals("ReCusNo") ) {
            return 143;
        }
        if( strFieldName.equals("AutoRnewAge") ) {
            return 144;
        }
        if( strFieldName.equals("AutoRnewYear") ) {
            return 145;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PolID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "GrpPolNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "PolNo";
                break;
            case 7:
                strFieldName = "ProposalNo";
                break;
            case 8:
                strFieldName = "PrtNo";
                break;
            case 9:
                strFieldName = "ContType";
                break;
            case 10:
                strFieldName = "PolTypeFlag";
                break;
            case 11:
                strFieldName = "MainPolNo";
                break;
            case 12:
                strFieldName = "MasterPolNo";
                break;
            case 13:
                strFieldName = "KindCode";
                break;
            case 14:
                strFieldName = "RiskCode";
                break;
            case 15:
                strFieldName = "RiskVersion";
                break;
            case 16:
                strFieldName = "ManageCom";
                break;
            case 17:
                strFieldName = "AgentCom";
                break;
            case 18:
                strFieldName = "AgentType";
                break;
            case 19:
                strFieldName = "AgentCode";
                break;
            case 20:
                strFieldName = "AgentGroup";
                break;
            case 21:
                strFieldName = "AgentCode1";
                break;
            case 22:
                strFieldName = "SaleChnl";
                break;
            case 23:
                strFieldName = "Handler";
                break;
            case 24:
                strFieldName = "InsuredNo";
                break;
            case 25:
                strFieldName = "InsuredName";
                break;
            case 26:
                strFieldName = "InsuredSex";
                break;
            case 27:
                strFieldName = "InsuredBirthday";
                break;
            case 28:
                strFieldName = "InsuredAppAge";
                break;
            case 29:
                strFieldName = "InsuredPeoples";
                break;
            case 30:
                strFieldName = "OccupationType";
                break;
            case 31:
                strFieldName = "AppntNo";
                break;
            case 32:
                strFieldName = "AppntName";
                break;
            case 33:
                strFieldName = "CValiDate";
                break;
            case 34:
                strFieldName = "SignCom";
                break;
            case 35:
                strFieldName = "SignDate";
                break;
            case 36:
                strFieldName = "SignTime";
                break;
            case 37:
                strFieldName = "FirstPayDate";
                break;
            case 38:
                strFieldName = "PayEndDate";
                break;
            case 39:
                strFieldName = "PaytoDate";
                break;
            case 40:
                strFieldName = "GetStartDate";
                break;
            case 41:
                strFieldName = "EndDate";
                break;
            case 42:
                strFieldName = "AcciEndDate";
                break;
            case 43:
                strFieldName = "GetYearFlag";
                break;
            case 44:
                strFieldName = "GetYear";
                break;
            case 45:
                strFieldName = "PayEndYearFlag";
                break;
            case 46:
                strFieldName = "PayEndYear";
                break;
            case 47:
                strFieldName = "InsuYearFlag";
                break;
            case 48:
                strFieldName = "InsuYear";
                break;
            case 49:
                strFieldName = "AcciYearFlag";
                break;
            case 50:
                strFieldName = "AcciYear";
                break;
            case 51:
                strFieldName = "GetStartType";
                break;
            case 52:
                strFieldName = "SpecifyValiDate";
                break;
            case 53:
                strFieldName = "PayMode";
                break;
            case 54:
                strFieldName = "PayLocation";
                break;
            case 55:
                strFieldName = "PayIntv";
                break;
            case 56:
                strFieldName = "PayYears";
                break;
            case 57:
                strFieldName = "Years";
                break;
            case 58:
                strFieldName = "ManageFeeRate";
                break;
            case 59:
                strFieldName = "FloatRate";
                break;
            case 60:
                strFieldName = "PremToAmnt";
                break;
            case 61:
                strFieldName = "Mult";
                break;
            case 62:
                strFieldName = "StandPrem";
                break;
            case 63:
                strFieldName = "Prem";
                break;
            case 64:
                strFieldName = "SumPrem";
                break;
            case 65:
                strFieldName = "Amnt";
                break;
            case 66:
                strFieldName = "RiskAmnt";
                break;
            case 67:
                strFieldName = "LeavingMoney";
                break;
            case 68:
                strFieldName = "EndorseTimes";
                break;
            case 69:
                strFieldName = "ClaimTimes";
                break;
            case 70:
                strFieldName = "LiveTimes";
                break;
            case 71:
                strFieldName = "RenewCount";
                break;
            case 72:
                strFieldName = "LastGetDate";
                break;
            case 73:
                strFieldName = "LastLoanDate";
                break;
            case 74:
                strFieldName = "LastRegetDate";
                break;
            case 75:
                strFieldName = "LastEdorDate";
                break;
            case 76:
                strFieldName = "LastRevDate";
                break;
            case 77:
                strFieldName = "RnewFlag";
                break;
            case 78:
                strFieldName = "StopFlag";
                break;
            case 79:
                strFieldName = "ExpiryFlag";
                break;
            case 80:
                strFieldName = "AutoPayFlag";
                break;
            case 81:
                strFieldName = "InterestDifFlag";
                break;
            case 82:
                strFieldName = "SubFlag";
                break;
            case 83:
                strFieldName = "BnfFlag";
                break;
            case 84:
                strFieldName = "HealthCheckFlag";
                break;
            case 85:
                strFieldName = "ImpartFlag";
                break;
            case 86:
                strFieldName = "ReinsureFlag";
                break;
            case 87:
                strFieldName = "AgentPayFlag";
                break;
            case 88:
                strFieldName = "AgentGetFlag";
                break;
            case 89:
                strFieldName = "LiveGetMode";
                break;
            case 90:
                strFieldName = "DeadGetMode";
                break;
            case 91:
                strFieldName = "BonusGetMode";
                break;
            case 92:
                strFieldName = "BonusMan";
                break;
            case 93:
                strFieldName = "DeadFlag";
                break;
            case 94:
                strFieldName = "SmokeFlag";
                break;
            case 95:
                strFieldName = "Remark";
                break;
            case 96:
                strFieldName = "ApproveFlag";
                break;
            case 97:
                strFieldName = "ApproveCode";
                break;
            case 98:
                strFieldName = "ApproveDate";
                break;
            case 99:
                strFieldName = "ApproveTime";
                break;
            case 100:
                strFieldName = "UWFlag";
                break;
            case 101:
                strFieldName = "UWCode";
                break;
            case 102:
                strFieldName = "UWDate";
                break;
            case 103:
                strFieldName = "UWTime";
                break;
            case 104:
                strFieldName = "PolApplyDate";
                break;
            case 105:
                strFieldName = "AppFlag";
                break;
            case 106:
                strFieldName = "PolState";
                break;
            case 107:
                strFieldName = "StandbyFlag1";
                break;
            case 108:
                strFieldName = "StandbyFlag2";
                break;
            case 109:
                strFieldName = "StandbyFlag3";
                break;
            case 110:
                strFieldName = "Operator";
                break;
            case 111:
                strFieldName = "MakeDate";
                break;
            case 112:
                strFieldName = "MakeTime";
                break;
            case 113:
                strFieldName = "ModifyDate";
                break;
            case 114:
                strFieldName = "ModifyTime";
                break;
            case 115:
                strFieldName = "WaitPeriod";
                break;
            case 116:
                strFieldName = "GetForm";
                break;
            case 117:
                strFieldName = "GetBankCode";
                break;
            case 118:
                strFieldName = "GetBankAccNo";
                break;
            case 119:
                strFieldName = "GetAccName";
                break;
            case 120:
                strFieldName = "KeepValueOpt";
                break;
            case 121:
                strFieldName = "AscriptionRuleCode";
                break;
            case 122:
                strFieldName = "PayRuleCode";
                break;
            case 123:
                strFieldName = "AscriptionFlag";
                break;
            case 124:
                strFieldName = "AutoPubAccFlag";
                break;
            case 125:
                strFieldName = "CombiFlag";
                break;
            case 126:
                strFieldName = "InvestRuleCode";
                break;
            case 127:
                strFieldName = "UintLinkValiFlag";
                break;
            case 128:
                strFieldName = "ProdSetCode";
                break;
            case 129:
                strFieldName = "InsurPolFlag";
                break;
            case 130:
                strFieldName = "NewReinsureFlag";
                break;
            case 131:
                strFieldName = "LiveAccFlag";
                break;
            case 132:
                strFieldName = "StandRatePrem";
                break;
            case 133:
                strFieldName = "AppntFirstName";
                break;
            case 134:
                strFieldName = "AppntLastName";
                break;
            case 135:
                strFieldName = "InsuredFirstName";
                break;
            case 136:
                strFieldName = "InsuredLastName";
                break;
            case 137:
                strFieldName = "FQGetMode";
                break;
            case 138:
                strFieldName = "GetPeriod";
                break;
            case 139:
                strFieldName = "GetPeriodFlag";
                break;
            case 140:
                strFieldName = "DelayPeriod";
                break;
            case 141:
                strFieldName = "OtherAmnt";
                break;
            case 142:
                strFieldName = "Relation";
                break;
            case 143:
                strFieldName = "ReCusNo";
                break;
            case 144:
                strFieldName = "AutoRnewAge";
                break;
            case 145:
                strFieldName = "AutoRnewYear";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "POLID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "POLTYPEFLAG":
                return Schema.TYPE_STRING;
            case "MAINPOLNO":
                return Schema.TYPE_STRING;
            case "MASTERPOLNO":
                return Schema.TYPE_STRING;
            case "KINDCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVERSION":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE1":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "HANDLER":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "INSUREDSEX":
                return Schema.TYPE_STRING;
            case "INSUREDBIRTHDAY":
                return Schema.TYPE_STRING;
            case "INSUREDAPPAGE":
                return Schema.TYPE_INT;
            case "INSUREDPEOPLES":
                return Schema.TYPE_INT;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "SIGNCOM":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_STRING;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "FIRSTPAYDATE":
                return Schema.TYPE_STRING;
            case "PAYENDDATE":
                return Schema.TYPE_STRING;
            case "PAYTODATE":
                return Schema.TYPE_STRING;
            case "GETSTARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "ACCIENDDATE":
                return Schema.TYPE_STRING;
            case "GETYEARFLAG":
                return Schema.TYPE_STRING;
            case "GETYEAR":
                return Schema.TYPE_INT;
            case "PAYENDYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "ACCIYEARFLAG":
                return Schema.TYPE_STRING;
            case "ACCIYEAR":
                return Schema.TYPE_INT;
            case "GETSTARTTYPE":
                return Schema.TYPE_STRING;
            case "SPECIFYVALIDATE":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "PAYLOCATION":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYYEARS":
                return Schema.TYPE_INT;
            case "YEARS":
                return Schema.TYPE_INT;
            case "MANAGEFEERATE":
                return Schema.TYPE_DOUBLE;
            case "FLOATRATE":
                return Schema.TYPE_DOUBLE;
            case "PREMTOAMNT":
                return Schema.TYPE_STRING;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "STANDPREM":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "RISKAMNT":
                return Schema.TYPE_DOUBLE;
            case "LEAVINGMONEY":
                return Schema.TYPE_DOUBLE;
            case "ENDORSETIMES":
                return Schema.TYPE_INT;
            case "CLAIMTIMES":
                return Schema.TYPE_INT;
            case "LIVETIMES":
                return Schema.TYPE_INT;
            case "RENEWCOUNT":
                return Schema.TYPE_INT;
            case "LASTGETDATE":
                return Schema.TYPE_STRING;
            case "LASTLOANDATE":
                return Schema.TYPE_STRING;
            case "LASTREGETDATE":
                return Schema.TYPE_STRING;
            case "LASTEDORDATE":
                return Schema.TYPE_STRING;
            case "LASTREVDATE":
                return Schema.TYPE_STRING;
            case "RNEWFLAG":
                return Schema.TYPE_INT;
            case "STOPFLAG":
                return Schema.TYPE_STRING;
            case "EXPIRYFLAG":
                return Schema.TYPE_STRING;
            case "AUTOPAYFLAG":
                return Schema.TYPE_STRING;
            case "INTERESTDIFFLAG":
                return Schema.TYPE_STRING;
            case "SUBFLAG":
                return Schema.TYPE_STRING;
            case "BNFFLAG":
                return Schema.TYPE_STRING;
            case "HEALTHCHECKFLAG":
                return Schema.TYPE_STRING;
            case "IMPARTFLAG":
                return Schema.TYPE_STRING;
            case "REINSUREFLAG":
                return Schema.TYPE_STRING;
            case "AGENTPAYFLAG":
                return Schema.TYPE_STRING;
            case "AGENTGETFLAG":
                return Schema.TYPE_STRING;
            case "LIVEGETMODE":
                return Schema.TYPE_STRING;
            case "DEADGETMODE":
                return Schema.TYPE_STRING;
            case "BONUSGETMODE":
                return Schema.TYPE_STRING;
            case "BONUSMAN":
                return Schema.TYPE_STRING;
            case "DEADFLAG":
                return Schema.TYPE_STRING;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWCODE":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_STRING;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "POLAPPLYDATE":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "POLSTATE":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "WAITPERIOD":
                return Schema.TYPE_INT;
            case "GETFORM":
                return Schema.TYPE_STRING;
            case "GETBANKCODE":
                return Schema.TYPE_STRING;
            case "GETBANKACCNO":
                return Schema.TYPE_STRING;
            case "GETACCNAME":
                return Schema.TYPE_STRING;
            case "KEEPVALUEOPT":
                return Schema.TYPE_STRING;
            case "ASCRIPTIONRULECODE":
                return Schema.TYPE_STRING;
            case "PAYRULECODE":
                return Schema.TYPE_STRING;
            case "ASCRIPTIONFLAG":
                return Schema.TYPE_STRING;
            case "AUTOPUBACCFLAG":
                return Schema.TYPE_STRING;
            case "COMBIFLAG":
                return Schema.TYPE_STRING;
            case "INVESTRULECODE":
                return Schema.TYPE_STRING;
            case "UINTLINKVALIFLAG":
                return Schema.TYPE_STRING;
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "INSURPOLFLAG":
                return Schema.TYPE_STRING;
            case "NEWREINSUREFLAG":
                return Schema.TYPE_STRING;
            case "LIVEACCFLAG":
                return Schema.TYPE_STRING;
            case "STANDRATEPREM":
                return Schema.TYPE_DOUBLE;
            case "APPNTFIRSTNAME":
                return Schema.TYPE_STRING;
            case "APPNTLASTNAME":
                return Schema.TYPE_STRING;
            case "INSUREDFIRSTNAME":
                return Schema.TYPE_STRING;
            case "INSUREDLASTNAME":
                return Schema.TYPE_STRING;
            case "FQGETMODE":
                return Schema.TYPE_STRING;
            case "GETPERIOD":
                return Schema.TYPE_STRING;
            case "GETPERIODFLAG":
                return Schema.TYPE_STRING;
            case "DELAYPERIOD":
                return Schema.TYPE_STRING;
            case "OTHERAMNT":
                return Schema.TYPE_DOUBLE;
            case "RELATION":
                return Schema.TYPE_STRING;
            case "RECUSNO":
                return Schema.TYPE_STRING;
            case "AUTORNEWAGE":
                return Schema.TYPE_STRING;
            case "AUTORNEWYEAR":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_INT;
            case 29:
                return Schema.TYPE_INT;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_INT;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_INT;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_INT;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_INT;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_INT;
            case 56:
                return Schema.TYPE_INT;
            case 57:
                return Schema.TYPE_INT;
            case 58:
                return Schema.TYPE_DOUBLE;
            case 59:
                return Schema.TYPE_DOUBLE;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_DOUBLE;
            case 62:
                return Schema.TYPE_DOUBLE;
            case 63:
                return Schema.TYPE_DOUBLE;
            case 64:
                return Schema.TYPE_DOUBLE;
            case 65:
                return Schema.TYPE_DOUBLE;
            case 66:
                return Schema.TYPE_DOUBLE;
            case 67:
                return Schema.TYPE_DOUBLE;
            case 68:
                return Schema.TYPE_INT;
            case 69:
                return Schema.TYPE_INT;
            case 70:
                return Schema.TYPE_INT;
            case 71:
                return Schema.TYPE_INT;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_STRING;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_INT;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_STRING;
            case 80:
                return Schema.TYPE_STRING;
            case 81:
                return Schema.TYPE_STRING;
            case 82:
                return Schema.TYPE_STRING;
            case 83:
                return Schema.TYPE_STRING;
            case 84:
                return Schema.TYPE_STRING;
            case 85:
                return Schema.TYPE_STRING;
            case 86:
                return Schema.TYPE_STRING;
            case 87:
                return Schema.TYPE_STRING;
            case 88:
                return Schema.TYPE_STRING;
            case 89:
                return Schema.TYPE_STRING;
            case 90:
                return Schema.TYPE_STRING;
            case 91:
                return Schema.TYPE_STRING;
            case 92:
                return Schema.TYPE_STRING;
            case 93:
                return Schema.TYPE_STRING;
            case 94:
                return Schema.TYPE_STRING;
            case 95:
                return Schema.TYPE_STRING;
            case 96:
                return Schema.TYPE_STRING;
            case 97:
                return Schema.TYPE_STRING;
            case 98:
                return Schema.TYPE_STRING;
            case 99:
                return Schema.TYPE_STRING;
            case 100:
                return Schema.TYPE_STRING;
            case 101:
                return Schema.TYPE_STRING;
            case 102:
                return Schema.TYPE_STRING;
            case 103:
                return Schema.TYPE_STRING;
            case 104:
                return Schema.TYPE_STRING;
            case 105:
                return Schema.TYPE_STRING;
            case 106:
                return Schema.TYPE_STRING;
            case 107:
                return Schema.TYPE_STRING;
            case 108:
                return Schema.TYPE_STRING;
            case 109:
                return Schema.TYPE_STRING;
            case 110:
                return Schema.TYPE_STRING;
            case 111:
                return Schema.TYPE_STRING;
            case 112:
                return Schema.TYPE_STRING;
            case 113:
                return Schema.TYPE_STRING;
            case 114:
                return Schema.TYPE_STRING;
            case 115:
                return Schema.TYPE_INT;
            case 116:
                return Schema.TYPE_STRING;
            case 117:
                return Schema.TYPE_STRING;
            case 118:
                return Schema.TYPE_STRING;
            case 119:
                return Schema.TYPE_STRING;
            case 120:
                return Schema.TYPE_STRING;
            case 121:
                return Schema.TYPE_STRING;
            case 122:
                return Schema.TYPE_STRING;
            case 123:
                return Schema.TYPE_STRING;
            case 124:
                return Schema.TYPE_STRING;
            case 125:
                return Schema.TYPE_STRING;
            case 126:
                return Schema.TYPE_STRING;
            case 127:
                return Schema.TYPE_STRING;
            case 128:
                return Schema.TYPE_STRING;
            case 129:
                return Schema.TYPE_STRING;
            case 130:
                return Schema.TYPE_STRING;
            case 131:
                return Schema.TYPE_STRING;
            case 132:
                return Schema.TYPE_DOUBLE;
            case 133:
                return Schema.TYPE_STRING;
            case 134:
                return Schema.TYPE_STRING;
            case 135:
                return Schema.TYPE_STRING;
            case 136:
                return Schema.TYPE_STRING;
            case 137:
                return Schema.TYPE_STRING;
            case 138:
                return Schema.TYPE_STRING;
            case 139:
                return Schema.TYPE_STRING;
            case 140:
                return Schema.TYPE_STRING;
            case 141:
                return Schema.TYPE_DOUBLE;
            case 142:
                return Schema.TYPE_STRING;
            case 143:
                return Schema.TYPE_STRING;
            case 144:
                return Schema.TYPE_STRING;
            case 145:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PolID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("PolTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolTypeFlag));
        }
        if (FCode.equalsIgnoreCase("MainPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolNo));
        }
        if (FCode.equalsIgnoreCase("MasterPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MasterPolNo));
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBirthday));
        }
        if (FCode.equalsIgnoreCase("InsuredAppAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAppAge));
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredPeoples));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstPayDate));
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndDate));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PaytoDate));
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("AcciEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciEndDate));
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("AcciYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciYearFlag));
        }
        if (FCode.equalsIgnoreCase("AcciYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciYear));
        }
        if (FCode.equalsIgnoreCase("GetStartType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartType));
        }
        if (FCode.equalsIgnoreCase("SpecifyValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecifyValiDate));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("PayLocation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayLocation));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
        }
        if (FCode.equalsIgnoreCase("Years")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFeeRate));
        }
        if (FCode.equalsIgnoreCase("FloatRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FloatRate));
        }
        if (FCode.equalsIgnoreCase("PremToAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremToAmnt));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("RiskAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskAmnt));
        }
        if (FCode.equalsIgnoreCase("LeavingMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LeavingMoney));
        }
        if (FCode.equalsIgnoreCase("EndorseTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndorseTimes));
        }
        if (FCode.equalsIgnoreCase("ClaimTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimTimes));
        }
        if (FCode.equalsIgnoreCase("LiveTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveTimes));
        }
        if (FCode.equalsIgnoreCase("RenewCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewCount));
        }
        if (FCode.equalsIgnoreCase("LastGetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastGetDate));
        }
        if (FCode.equalsIgnoreCase("LastLoanDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastLoanDate));
        }
        if (FCode.equalsIgnoreCase("LastRegetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastRegetDate));
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastEdorDate));
        }
        if (FCode.equalsIgnoreCase("LastRevDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastRevDate));
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewFlag));
        }
        if (FCode.equalsIgnoreCase("StopFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StopFlag));
        }
        if (FCode.equalsIgnoreCase("ExpiryFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpiryFlag));
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoPayFlag));
        }
        if (FCode.equalsIgnoreCase("InterestDifFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestDifFlag));
        }
        if (FCode.equalsIgnoreCase("SubFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubFlag));
        }
        if (FCode.equalsIgnoreCase("BnfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfFlag));
        }
        if (FCode.equalsIgnoreCase("HealthCheckFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthCheckFlag));
        }
        if (FCode.equalsIgnoreCase("ImpartFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartFlag));
        }
        if (FCode.equalsIgnoreCase("ReinsureFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsureFlag));
        }
        if (FCode.equalsIgnoreCase("AgentPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPayFlag));
        }
        if (FCode.equalsIgnoreCase("AgentGetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetFlag));
        }
        if (FCode.equalsIgnoreCase("LiveGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetMode));
        }
        if (FCode.equalsIgnoreCase("DeadGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadGetMode));
        }
        if (FCode.equalsIgnoreCase("BonusGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusGetMode));
        }
        if (FCode.equalsIgnoreCase("BonusMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusMan));
        }
        if (FCode.equalsIgnoreCase("DeadFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadFlag));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWCode));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWDate));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolApplyDate));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("PolState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolState));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("WaitPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WaitPeriod));
        }
        if (FCode.equalsIgnoreCase("GetForm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetForm));
        }
        if (FCode.equalsIgnoreCase("GetBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetBankCode));
        }
        if (FCode.equalsIgnoreCase("GetBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetBankAccNo));
        }
        if (FCode.equalsIgnoreCase("GetAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetAccName));
        }
        if (FCode.equalsIgnoreCase("KeepValueOpt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KeepValueOpt));
        }
        if (FCode.equalsIgnoreCase("AscriptionRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptionRuleCode));
        }
        if (FCode.equalsIgnoreCase("PayRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayRuleCode));
        }
        if (FCode.equalsIgnoreCase("AscriptionFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptionFlag));
        }
        if (FCode.equalsIgnoreCase("AutoPubAccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoPubAccFlag));
        }
        if (FCode.equalsIgnoreCase("CombiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CombiFlag));
        }
        if (FCode.equalsIgnoreCase("InvestRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestRuleCode));
        }
        if (FCode.equalsIgnoreCase("UintLinkValiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UintLinkValiFlag));
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("InsurPolFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurPolFlag));
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewReinsureFlag));
        }
        if (FCode.equalsIgnoreCase("LiveAccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveAccFlag));
        }
        if (FCode.equalsIgnoreCase("StandRatePrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandRatePrem));
        }
        if (FCode.equalsIgnoreCase("AppntFirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntFirstName));
        }
        if (FCode.equalsIgnoreCase("AppntLastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntLastName));
        }
        if (FCode.equalsIgnoreCase("InsuredFirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredFirstName));
        }
        if (FCode.equalsIgnoreCase("InsuredLastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredLastName));
        }
        if (FCode.equalsIgnoreCase("FQGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FQGetMode));
        }
        if (FCode.equalsIgnoreCase("GetPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPeriod));
        }
        if (FCode.equalsIgnoreCase("GetPeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPeriodFlag));
        }
        if (FCode.equalsIgnoreCase("DelayPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DelayPeriod));
        }
        if (FCode.equalsIgnoreCase("OtherAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherAmnt));
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
        }
        if (FCode.equalsIgnoreCase("ReCusNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReCusNo));
        }
        if (FCode.equalsIgnoreCase("AutoRnewAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoRnewAge));
        }
        if (FCode.equalsIgnoreCase("AutoRnewYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoRnewYear));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PolID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 3:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(GrpPolNo);
                break;
            case 5:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 6:
                strFieldValue = String.valueOf(PolNo);
                break;
            case 7:
                strFieldValue = String.valueOf(ProposalNo);
                break;
            case 8:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 9:
                strFieldValue = String.valueOf(ContType);
                break;
            case 10:
                strFieldValue = String.valueOf(PolTypeFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(MainPolNo);
                break;
            case 12:
                strFieldValue = String.valueOf(MasterPolNo);
                break;
            case 13:
                strFieldValue = String.valueOf(KindCode);
                break;
            case 14:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 15:
                strFieldValue = String.valueOf(RiskVersion);
                break;
            case 16:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 17:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 18:
                strFieldValue = String.valueOf(AgentType);
                break;
            case 19:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 20:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 21:
                strFieldValue = String.valueOf(AgentCode1);
                break;
            case 22:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 23:
                strFieldValue = String.valueOf(Handler);
                break;
            case 24:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 25:
                strFieldValue = String.valueOf(InsuredName);
                break;
            case 26:
                strFieldValue = String.valueOf(InsuredSex);
                break;
            case 27:
                strFieldValue = String.valueOf(InsuredBirthday);
                break;
            case 28:
                strFieldValue = String.valueOf(InsuredAppAge);
                break;
            case 29:
                strFieldValue = String.valueOf(InsuredPeoples);
                break;
            case 30:
                strFieldValue = String.valueOf(OccupationType);
                break;
            case 31:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 32:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 33:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 34:
                strFieldValue = String.valueOf(SignCom);
                break;
            case 35:
                strFieldValue = String.valueOf(SignDate);
                break;
            case 36:
                strFieldValue = String.valueOf(SignTime);
                break;
            case 37:
                strFieldValue = String.valueOf(FirstPayDate);
                break;
            case 38:
                strFieldValue = String.valueOf(PayEndDate);
                break;
            case 39:
                strFieldValue = String.valueOf(PaytoDate);
                break;
            case 40:
                strFieldValue = String.valueOf(GetStartDate);
                break;
            case 41:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 42:
                strFieldValue = String.valueOf(AcciEndDate);
                break;
            case 43:
                strFieldValue = String.valueOf(GetYearFlag);
                break;
            case 44:
                strFieldValue = String.valueOf(GetYear);
                break;
            case 45:
                strFieldValue = String.valueOf(PayEndYearFlag);
                break;
            case 46:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 47:
                strFieldValue = String.valueOf(InsuYearFlag);
                break;
            case 48:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 49:
                strFieldValue = String.valueOf(AcciYearFlag);
                break;
            case 50:
                strFieldValue = String.valueOf(AcciYear);
                break;
            case 51:
                strFieldValue = String.valueOf(GetStartType);
                break;
            case 52:
                strFieldValue = String.valueOf(SpecifyValiDate);
                break;
            case 53:
                strFieldValue = String.valueOf(PayMode);
                break;
            case 54:
                strFieldValue = String.valueOf(PayLocation);
                break;
            case 55:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 56:
                strFieldValue = String.valueOf(PayYears);
                break;
            case 57:
                strFieldValue = String.valueOf(Years);
                break;
            case 58:
                strFieldValue = String.valueOf(ManageFeeRate);
                break;
            case 59:
                strFieldValue = String.valueOf(FloatRate);
                break;
            case 60:
                strFieldValue = String.valueOf(PremToAmnt);
                break;
            case 61:
                strFieldValue = String.valueOf(Mult);
                break;
            case 62:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 63:
                strFieldValue = String.valueOf(Prem);
                break;
            case 64:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 65:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 66:
                strFieldValue = String.valueOf(RiskAmnt);
                break;
            case 67:
                strFieldValue = String.valueOf(LeavingMoney);
                break;
            case 68:
                strFieldValue = String.valueOf(EndorseTimes);
                break;
            case 69:
                strFieldValue = String.valueOf(ClaimTimes);
                break;
            case 70:
                strFieldValue = String.valueOf(LiveTimes);
                break;
            case 71:
                strFieldValue = String.valueOf(RenewCount);
                break;
            case 72:
                strFieldValue = String.valueOf(LastGetDate);
                break;
            case 73:
                strFieldValue = String.valueOf(LastLoanDate);
                break;
            case 74:
                strFieldValue = String.valueOf(LastRegetDate);
                break;
            case 75:
                strFieldValue = String.valueOf(LastEdorDate);
                break;
            case 76:
                strFieldValue = String.valueOf(LastRevDate);
                break;
            case 77:
                strFieldValue = String.valueOf(RnewFlag);
                break;
            case 78:
                strFieldValue = String.valueOf(StopFlag);
                break;
            case 79:
                strFieldValue = String.valueOf(ExpiryFlag);
                break;
            case 80:
                strFieldValue = String.valueOf(AutoPayFlag);
                break;
            case 81:
                strFieldValue = String.valueOf(InterestDifFlag);
                break;
            case 82:
                strFieldValue = String.valueOf(SubFlag);
                break;
            case 83:
                strFieldValue = String.valueOf(BnfFlag);
                break;
            case 84:
                strFieldValue = String.valueOf(HealthCheckFlag);
                break;
            case 85:
                strFieldValue = String.valueOf(ImpartFlag);
                break;
            case 86:
                strFieldValue = String.valueOf(ReinsureFlag);
                break;
            case 87:
                strFieldValue = String.valueOf(AgentPayFlag);
                break;
            case 88:
                strFieldValue = String.valueOf(AgentGetFlag);
                break;
            case 89:
                strFieldValue = String.valueOf(LiveGetMode);
                break;
            case 90:
                strFieldValue = String.valueOf(DeadGetMode);
                break;
            case 91:
                strFieldValue = String.valueOf(BonusGetMode);
                break;
            case 92:
                strFieldValue = String.valueOf(BonusMan);
                break;
            case 93:
                strFieldValue = String.valueOf(DeadFlag);
                break;
            case 94:
                strFieldValue = String.valueOf(SmokeFlag);
                break;
            case 95:
                strFieldValue = String.valueOf(Remark);
                break;
            case 96:
                strFieldValue = String.valueOf(ApproveFlag);
                break;
            case 97:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 98:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 99:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            case 100:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 101:
                strFieldValue = String.valueOf(UWCode);
                break;
            case 102:
                strFieldValue = String.valueOf(UWDate);
                break;
            case 103:
                strFieldValue = String.valueOf(UWTime);
                break;
            case 104:
                strFieldValue = String.valueOf(PolApplyDate);
                break;
            case 105:
                strFieldValue = String.valueOf(AppFlag);
                break;
            case 106:
                strFieldValue = String.valueOf(PolState);
                break;
            case 107:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 108:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 109:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 110:
                strFieldValue = String.valueOf(Operator);
                break;
            case 111:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 112:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 113:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 114:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 115:
                strFieldValue = String.valueOf(WaitPeriod);
                break;
            case 116:
                strFieldValue = String.valueOf(GetForm);
                break;
            case 117:
                strFieldValue = String.valueOf(GetBankCode);
                break;
            case 118:
                strFieldValue = String.valueOf(GetBankAccNo);
                break;
            case 119:
                strFieldValue = String.valueOf(GetAccName);
                break;
            case 120:
                strFieldValue = String.valueOf(KeepValueOpt);
                break;
            case 121:
                strFieldValue = String.valueOf(AscriptionRuleCode);
                break;
            case 122:
                strFieldValue = String.valueOf(PayRuleCode);
                break;
            case 123:
                strFieldValue = String.valueOf(AscriptionFlag);
                break;
            case 124:
                strFieldValue = String.valueOf(AutoPubAccFlag);
                break;
            case 125:
                strFieldValue = String.valueOf(CombiFlag);
                break;
            case 126:
                strFieldValue = String.valueOf(InvestRuleCode);
                break;
            case 127:
                strFieldValue = String.valueOf(UintLinkValiFlag);
                break;
            case 128:
                strFieldValue = String.valueOf(ProdSetCode);
                break;
            case 129:
                strFieldValue = String.valueOf(InsurPolFlag);
                break;
            case 130:
                strFieldValue = String.valueOf(NewReinsureFlag);
                break;
            case 131:
                strFieldValue = String.valueOf(LiveAccFlag);
                break;
            case 132:
                strFieldValue = String.valueOf(StandRatePrem);
                break;
            case 133:
                strFieldValue = String.valueOf(AppntFirstName);
                break;
            case 134:
                strFieldValue = String.valueOf(AppntLastName);
                break;
            case 135:
                strFieldValue = String.valueOf(InsuredFirstName);
                break;
            case 136:
                strFieldValue = String.valueOf(InsuredLastName);
                break;
            case 137:
                strFieldValue = String.valueOf(FQGetMode);
                break;
            case 138:
                strFieldValue = String.valueOf(GetPeriod);
                break;
            case 139:
                strFieldValue = String.valueOf(GetPeriodFlag);
                break;
            case 140:
                strFieldValue = String.valueOf(DelayPeriod);
                break;
            case 141:
                strFieldValue = String.valueOf(OtherAmnt);
                break;
            case 142:
                strFieldValue = String.valueOf(Relation);
                break;
            case 143:
                strFieldValue = String.valueOf(ReCusNo);
                break;
            case 144:
                strFieldValue = String.valueOf(AutoRnewAge);
                break;
            case 145:
                strFieldValue = String.valueOf(AutoRnewYear);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PolID")) {
            if( FValue != null && !FValue.equals("")) {
                PolID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("PolTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolTypeFlag = FValue.trim();
            }
            else
                PolTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("MainPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainPolNo = FValue.trim();
            }
            else
                MainPolNo = null;
        }
        if (FCode.equalsIgnoreCase("MasterPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                MasterPolNo = FValue.trim();
            }
            else
                MasterPolNo = null;
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
                KindCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
                RiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode1 = FValue.trim();
            }
            else
                AgentCode1 = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            if( FValue != null && !FValue.equals(""))
            {
                Handler = FValue.trim();
            }
            else
                Handler = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredSex = FValue.trim();
            }
            else
                InsuredSex = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBirthday = FValue.trim();
            }
            else
                InsuredBirthday = null;
        }
        if (FCode.equalsIgnoreCase("InsuredAppAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuredAppAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuredPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignCom = FValue.trim();
            }
            else
                SignCom = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignDate = FValue.trim();
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstPayDate = FValue.trim();
            }
            else
                FirstPayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndDate = FValue.trim();
            }
            else
                PayEndDate = null;
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PaytoDate = FValue.trim();
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetStartDate = FValue.trim();
            }
            else
                GetStartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("AcciEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AcciEndDate = FValue.trim();
            }
            else
                AcciEndDate = null;
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
                GetYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
                PayEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("AcciYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AcciYearFlag = FValue.trim();
            }
            else
                AcciYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("AcciYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AcciYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetStartType")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetStartType = FValue.trim();
            }
            else
                GetStartType = null;
        }
        if (FCode.equalsIgnoreCase("SpecifyValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecifyValiDate = FValue.trim();
            }
            else
                SpecifyValiDate = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("PayLocation")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayLocation = FValue.trim();
            }
            else
                PayLocation = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayYears = i;
            }
        }
        if (FCode.equalsIgnoreCase("Years")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Years = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ManageFeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FloatRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FloatRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PremToAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                PremToAmnt = FValue.trim();
            }
            else
                PremToAmnt = null;
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("RiskAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RiskAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("LeavingMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                LeavingMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("EndorseTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                EndorseTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("ClaimTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ClaimTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("LiveTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                LiveTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("RenewCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RenewCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("LastGetDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastGetDate = FValue.trim();
            }
            else
                LastGetDate = null;
        }
        if (FCode.equalsIgnoreCase("LastLoanDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastLoanDate = FValue.trim();
            }
            else
                LastLoanDate = null;
        }
        if (FCode.equalsIgnoreCase("LastRegetDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastRegetDate = FValue.trim();
            }
            else
                LastRegetDate = null;
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastEdorDate = FValue.trim();
            }
            else
                LastEdorDate = null;
        }
        if (FCode.equalsIgnoreCase("LastRevDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastRevDate = FValue.trim();
            }
            else
                LastRevDate = null;
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RnewFlag = i;
            }
        }
        if (FCode.equalsIgnoreCase("StopFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                StopFlag = FValue.trim();
            }
            else
                StopFlag = null;
        }
        if (FCode.equalsIgnoreCase("ExpiryFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExpiryFlag = FValue.trim();
            }
            else
                ExpiryFlag = null;
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoPayFlag = FValue.trim();
            }
            else
                AutoPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("InterestDifFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InterestDifFlag = FValue.trim();
            }
            else
                InterestDifFlag = null;
        }
        if (FCode.equalsIgnoreCase("SubFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubFlag = FValue.trim();
            }
            else
                SubFlag = null;
        }
        if (FCode.equalsIgnoreCase("BnfFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BnfFlag = FValue.trim();
            }
            else
                BnfFlag = null;
        }
        if (FCode.equalsIgnoreCase("HealthCheckFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthCheckFlag = FValue.trim();
            }
            else
                HealthCheckFlag = null;
        }
        if (FCode.equalsIgnoreCase("ImpartFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartFlag = FValue.trim();
            }
            else
                ImpartFlag = null;
        }
        if (FCode.equalsIgnoreCase("ReinsureFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReinsureFlag = FValue.trim();
            }
            else
                ReinsureFlag = null;
        }
        if (FCode.equalsIgnoreCase("AgentPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentPayFlag = FValue.trim();
            }
            else
                AgentPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("AgentGetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGetFlag = FValue.trim();
            }
            else
                AgentGetFlag = null;
        }
        if (FCode.equalsIgnoreCase("LiveGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveGetMode = FValue.trim();
            }
            else
                LiveGetMode = null;
        }
        if (FCode.equalsIgnoreCase("DeadGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadGetMode = FValue.trim();
            }
            else
                DeadGetMode = null;
        }
        if (FCode.equalsIgnoreCase("BonusGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusGetMode = FValue.trim();
            }
            else
                BonusGetMode = null;
        }
        if (FCode.equalsIgnoreCase("BonusMan")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusMan = FValue.trim();
            }
            else
                BonusMan = null;
        }
        if (FCode.equalsIgnoreCase("DeadFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadFlag = FValue.trim();
            }
            else
                DeadFlag = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWCode = FValue.trim();
            }
            else
                UWCode = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWDate = FValue.trim();
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolApplyDate = FValue.trim();
            }
            else
                PolApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolState")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolState = FValue.trim();
            }
            else
                PolState = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("WaitPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                WaitPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetForm")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetForm = FValue.trim();
            }
            else
                GetForm = null;
        }
        if (FCode.equalsIgnoreCase("GetBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetBankCode = FValue.trim();
            }
            else
                GetBankCode = null;
        }
        if (FCode.equalsIgnoreCase("GetBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetBankAccNo = FValue.trim();
            }
            else
                GetBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("GetAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetAccName = FValue.trim();
            }
            else
                GetAccName = null;
        }
        if (FCode.equalsIgnoreCase("KeepValueOpt")) {
            if( FValue != null && !FValue.equals(""))
            {
                KeepValueOpt = FValue.trim();
            }
            else
                KeepValueOpt = null;
        }
        if (FCode.equalsIgnoreCase("AscriptionRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AscriptionRuleCode = FValue.trim();
            }
            else
                AscriptionRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("PayRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayRuleCode = FValue.trim();
            }
            else
                PayRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("AscriptionFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AscriptionFlag = FValue.trim();
            }
            else
                AscriptionFlag = null;
        }
        if (FCode.equalsIgnoreCase("AutoPubAccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoPubAccFlag = FValue.trim();
            }
            else
                AutoPubAccFlag = null;
        }
        if (FCode.equalsIgnoreCase("CombiFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CombiFlag = FValue.trim();
            }
            else
                CombiFlag = null;
        }
        if (FCode.equalsIgnoreCase("InvestRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestRuleCode = FValue.trim();
            }
            else
                InvestRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("UintLinkValiFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UintLinkValiFlag = FValue.trim();
            }
            else
                UintLinkValiFlag = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("InsurPolFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsurPolFlag = FValue.trim();
            }
            else
                InsurPolFlag = null;
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewReinsureFlag = FValue.trim();
            }
            else
                NewReinsureFlag = null;
        }
        if (FCode.equalsIgnoreCase("LiveAccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveAccFlag = FValue.trim();
            }
            else
                LiveAccFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandRatePrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandRatePrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("AppntFirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntFirstName = FValue.trim();
            }
            else
                AppntFirstName = null;
        }
        if (FCode.equalsIgnoreCase("AppntLastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntLastName = FValue.trim();
            }
            else
                AppntLastName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredFirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredFirstName = FValue.trim();
            }
            else
                InsuredFirstName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredLastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredLastName = FValue.trim();
            }
            else
                InsuredLastName = null;
        }
        if (FCode.equalsIgnoreCase("FQGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FQGetMode = FValue.trim();
            }
            else
                FQGetMode = null;
        }
        if (FCode.equalsIgnoreCase("GetPeriod")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPeriod = FValue.trim();
            }
            else
                GetPeriod = null;
        }
        if (FCode.equalsIgnoreCase("GetPeriodFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPeriodFlag = FValue.trim();
            }
            else
                GetPeriodFlag = null;
        }
        if (FCode.equalsIgnoreCase("DelayPeriod")) {
            if( FValue != null && !FValue.equals(""))
            {
                DelayPeriod = FValue.trim();
            }
            else
                DelayPeriod = null;
        }
        if (FCode.equalsIgnoreCase("OtherAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                OtherAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Relation = FValue.trim();
            }
            else
                Relation = null;
        }
        if (FCode.equalsIgnoreCase("ReCusNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReCusNo = FValue.trim();
            }
            else
                ReCusNo = null;
        }
        if (FCode.equalsIgnoreCase("AutoRnewAge")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoRnewAge = FValue.trim();
            }
            else
                AutoRnewAge = null;
        }
        if (FCode.equalsIgnoreCase("AutoRnewYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoRnewYear = FValue.trim();
            }
            else
                AutoRnewYear = null;
        }
        return true;
    }


    public String toString() {
    return "LCPolPojo [" +
            "PolID="+PolID +
            ", ContID="+ContID +
            ", ShardingID="+ShardingID +
            ", GrpContNo="+GrpContNo +
            ", GrpPolNo="+GrpPolNo +
            ", ContNo="+ContNo +
            ", PolNo="+PolNo +
            ", ProposalNo="+ProposalNo +
            ", PrtNo="+PrtNo +
            ", ContType="+ContType +
            ", PolTypeFlag="+PolTypeFlag +
            ", MainPolNo="+MainPolNo +
            ", MasterPolNo="+MasterPolNo +
            ", KindCode="+KindCode +
            ", RiskCode="+RiskCode +
            ", RiskVersion="+RiskVersion +
            ", ManageCom="+ManageCom +
            ", AgentCom="+AgentCom +
            ", AgentType="+AgentType +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", AgentCode1="+AgentCode1 +
            ", SaleChnl="+SaleChnl +
            ", Handler="+Handler +
            ", InsuredNo="+InsuredNo +
            ", InsuredName="+InsuredName +
            ", InsuredSex="+InsuredSex +
            ", InsuredBirthday="+InsuredBirthday +
            ", InsuredAppAge="+InsuredAppAge +
            ", InsuredPeoples="+InsuredPeoples +
            ", OccupationType="+OccupationType +
            ", AppntNo="+AppntNo +
            ", AppntName="+AppntName +
            ", CValiDate="+CValiDate +
            ", SignCom="+SignCom +
            ", SignDate="+SignDate +
            ", SignTime="+SignTime +
            ", FirstPayDate="+FirstPayDate +
            ", PayEndDate="+PayEndDate +
            ", PaytoDate="+PaytoDate +
            ", GetStartDate="+GetStartDate +
            ", EndDate="+EndDate +
            ", AcciEndDate="+AcciEndDate +
            ", GetYearFlag="+GetYearFlag +
            ", GetYear="+GetYear +
            ", PayEndYearFlag="+PayEndYearFlag +
            ", PayEndYear="+PayEndYear +
            ", InsuYearFlag="+InsuYearFlag +
            ", InsuYear="+InsuYear +
            ", AcciYearFlag="+AcciYearFlag +
            ", AcciYear="+AcciYear +
            ", GetStartType="+GetStartType +
            ", SpecifyValiDate="+SpecifyValiDate +
            ", PayMode="+PayMode +
            ", PayLocation="+PayLocation +
            ", PayIntv="+PayIntv +
            ", PayYears="+PayYears +
            ", Years="+Years +
            ", ManageFeeRate="+ManageFeeRate +
            ", FloatRate="+FloatRate +
            ", PremToAmnt="+PremToAmnt +
            ", Mult="+Mult +
            ", StandPrem="+StandPrem +
            ", Prem="+Prem +
            ", SumPrem="+SumPrem +
            ", Amnt="+Amnt +
            ", RiskAmnt="+RiskAmnt +
            ", LeavingMoney="+LeavingMoney +
            ", EndorseTimes="+EndorseTimes +
            ", ClaimTimes="+ClaimTimes +
            ", LiveTimes="+LiveTimes +
            ", RenewCount="+RenewCount +
            ", LastGetDate="+LastGetDate +
            ", LastLoanDate="+LastLoanDate +
            ", LastRegetDate="+LastRegetDate +
            ", LastEdorDate="+LastEdorDate +
            ", LastRevDate="+LastRevDate +
            ", RnewFlag="+RnewFlag +
            ", StopFlag="+StopFlag +
            ", ExpiryFlag="+ExpiryFlag +
            ", AutoPayFlag="+AutoPayFlag +
            ", InterestDifFlag="+InterestDifFlag +
            ", SubFlag="+SubFlag +
            ", BnfFlag="+BnfFlag +
            ", HealthCheckFlag="+HealthCheckFlag +
            ", ImpartFlag="+ImpartFlag +
            ", ReinsureFlag="+ReinsureFlag +
            ", AgentPayFlag="+AgentPayFlag +
            ", AgentGetFlag="+AgentGetFlag +
            ", LiveGetMode="+LiveGetMode +
            ", DeadGetMode="+DeadGetMode +
            ", BonusGetMode="+BonusGetMode +
            ", BonusMan="+BonusMan +
            ", DeadFlag="+DeadFlag +
            ", SmokeFlag="+SmokeFlag +
            ", Remark="+Remark +
            ", ApproveFlag="+ApproveFlag +
            ", ApproveCode="+ApproveCode +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +
            ", UWFlag="+UWFlag +
            ", UWCode="+UWCode +
            ", UWDate="+UWDate +
            ", UWTime="+UWTime +
            ", PolApplyDate="+PolApplyDate +
            ", AppFlag="+AppFlag +
            ", PolState="+PolState +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", StandbyFlag3="+StandbyFlag3 +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", WaitPeriod="+WaitPeriod +
            ", GetForm="+GetForm +
            ", GetBankCode="+GetBankCode +
            ", GetBankAccNo="+GetBankAccNo +
            ", GetAccName="+GetAccName +
            ", KeepValueOpt="+KeepValueOpt +
            ", AscriptionRuleCode="+AscriptionRuleCode +
            ", PayRuleCode="+PayRuleCode +
            ", AscriptionFlag="+AscriptionFlag +
            ", AutoPubAccFlag="+AutoPubAccFlag +
            ", CombiFlag="+CombiFlag +
            ", InvestRuleCode="+InvestRuleCode +
            ", UintLinkValiFlag="+UintLinkValiFlag +
            ", ProdSetCode="+ProdSetCode +
            ", InsurPolFlag="+InsurPolFlag +
            ", NewReinsureFlag="+NewReinsureFlag +
            ", LiveAccFlag="+LiveAccFlag +
            ", StandRatePrem="+StandRatePrem +
            ", AppntFirstName="+AppntFirstName +
            ", AppntLastName="+AppntLastName +
            ", InsuredFirstName="+InsuredFirstName +
            ", InsuredLastName="+InsuredLastName +
            ", FQGetMode="+FQGetMode +
            ", GetPeriod="+GetPeriod +
            ", GetPeriodFlag="+GetPeriodFlag +
            ", DelayPeriod="+DelayPeriod +
            ", OtherAmnt="+OtherAmnt +
            ", Relation="+Relation +
            ", ReCusNo="+ReCusNo +
            ", AutoRnewAge="+AutoRnewAge +
            ", AutoRnewYear="+AutoRnewYear +"]";
    }
}
