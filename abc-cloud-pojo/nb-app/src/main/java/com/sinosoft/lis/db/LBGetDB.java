/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LBGetSchema;
import com.sinosoft.lis.vschema.LBGetSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LBGetDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LBGetDB extends LBGetSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LBGetDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LBGet" );
        mflag = true;
    }

    public LBGetDB() {
        con = null;
        db = new DBOper( "LBGet" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LBGetSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LBGetSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LBGet WHERE  1=1  AND GetID = ?");
            pstmt.setLong(1, this.getGetID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBGet");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LBGet SET  GetID = ? , ShardingID = ? , EdorNo = ? , GrpContNo = ? , ContNo = ? , PolNo = ? , DutyCode = ? , GetDutyCode = ? , GetDutyKind = ? , InsuredNo = ? , GetMode = ? , GetLimit = ? , GetRate = ? , UrgeGetFlag = ? , LiveGetType = ? , AddRate = ? , CanGet = ? , NeedAcc = ? , NeedCancelAcc = ? , StandMoney = ? , ActuGet = ? , SumMoney = ? , GetIntv = ? , GettoDate = ? , GetStartDate = ? , GetEndDate = ? , BalaDate = ? , State = ? , ManageCom = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyTime = ? , ModifyDate = ? , GetEndState = ? WHERE  1=1  AND GetID = ?");
            pstmt.setLong(1, this.getGetID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getShardingID());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getEdorNo());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getContNo());
            }
            if(this.getPolNo() == null || this.getPolNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getPolNo());
            }
            if(this.getDutyCode() == null || this.getDutyCode().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getDutyCode());
            }
            if(this.getGetDutyCode() == null || this.getGetDutyCode().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getGetDutyCode());
            }
            if(this.getGetDutyKind() == null || this.getGetDutyKind().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getGetDutyKind());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getInsuredNo());
            }
            if(this.getGetMode() == null || this.getGetMode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getGetMode());
            }
            pstmt.setDouble(12, this.getGetLimit());
            pstmt.setDouble(13, this.getGetRate());
            if(this.getUrgeGetFlag() == null || this.getUrgeGetFlag().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getUrgeGetFlag());
            }
            if(this.getLiveGetType() == null || this.getLiveGetType().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getLiveGetType());
            }
            pstmt.setDouble(16, this.getAddRate());
            if(this.getCanGet() == null || this.getCanGet().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getCanGet());
            }
            if(this.getNeedAcc() == null || this.getNeedAcc().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getNeedAcc());
            }
            if(this.getNeedCancelAcc() == null || this.getNeedCancelAcc().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getNeedCancelAcc());
            }
            pstmt.setDouble(20, this.getStandMoney());
            pstmt.setDouble(21, this.getActuGet());
            pstmt.setDouble(22, this.getSumMoney());
            pstmt.setInt(23, this.getGetIntv());
            if(this.getGettoDate() == null || this.getGettoDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getGettoDate()));
            }
            if(this.getGetStartDate() == null || this.getGetStartDate().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getGetStartDate()));
            }
            if(this.getGetEndDate() == null || this.getGetEndDate().equals("null")) {
            	pstmt.setNull(26, 93);
            } else {
            	pstmt.setDate(26, Date.valueOf(this.getGetEndDate()));
            }
            if(this.getBalaDate() == null || this.getBalaDate().equals("null")) {
            	pstmt.setNull(27, 93);
            } else {
            	pstmt.setDate(27, Date.valueOf(this.getBalaDate()));
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getState());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getManageCom());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getMakeTime());
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getModifyTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(34, 93);
            } else {
            	pstmt.setDate(34, Date.valueOf(this.getModifyDate()));
            }
            if(this.getGetEndState() == null || this.getGetEndState().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getGetEndState());
            }
            // set where condition
            pstmt.setLong(36, this.getGetID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LBGet");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LBGet VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getGetID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getShardingID());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getEdorNo());
            }
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getGrpContNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getContNo());
            }
            if(this.getPolNo() == null || this.getPolNo().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getPolNo());
            }
            if(this.getDutyCode() == null || this.getDutyCode().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getDutyCode());
            }
            if(this.getGetDutyCode() == null || this.getGetDutyCode().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getGetDutyCode());
            }
            if(this.getGetDutyKind() == null || this.getGetDutyKind().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getGetDutyKind());
            }
            if(this.getInsuredNo() == null || this.getInsuredNo().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getInsuredNo());
            }
            if(this.getGetMode() == null || this.getGetMode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getGetMode());
            }
            pstmt.setDouble(12, this.getGetLimit());
            pstmt.setDouble(13, this.getGetRate());
            if(this.getUrgeGetFlag() == null || this.getUrgeGetFlag().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getUrgeGetFlag());
            }
            if(this.getLiveGetType() == null || this.getLiveGetType().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getLiveGetType());
            }
            pstmt.setDouble(16, this.getAddRate());
            if(this.getCanGet() == null || this.getCanGet().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getCanGet());
            }
            if(this.getNeedAcc() == null || this.getNeedAcc().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getNeedAcc());
            }
            if(this.getNeedCancelAcc() == null || this.getNeedCancelAcc().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getNeedCancelAcc());
            }
            pstmt.setDouble(20, this.getStandMoney());
            pstmt.setDouble(21, this.getActuGet());
            pstmt.setDouble(22, this.getSumMoney());
            pstmt.setInt(23, this.getGetIntv());
            if(this.getGettoDate() == null || this.getGettoDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getGettoDate()));
            }
            if(this.getGetStartDate() == null || this.getGetStartDate().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getGetStartDate()));
            }
            if(this.getGetEndDate() == null || this.getGetEndDate().equals("null")) {
            	pstmt.setNull(26, 93);
            } else {
            	pstmt.setDate(26, Date.valueOf(this.getGetEndDate()));
            }
            if(this.getBalaDate() == null || this.getBalaDate().equals("null")) {
            	pstmt.setNull(27, 93);
            } else {
            	pstmt.setDate(27, Date.valueOf(this.getBalaDate()));
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getState());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getManageCom());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(31, 93);
            } else {
            	pstmt.setDate(31, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getMakeTime());
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getModifyTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(34, 93);
            } else {
            	pstmt.setDate(34, Date.valueOf(this.getModifyDate()));
            }
            if(this.getGetEndState() == null || this.getGetEndState().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getGetEndState());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LBGet WHERE  1=1  AND GetID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getGetID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGetDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LBGetSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LBGetSet aLBGetSet = new LBGetSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBGet");
            LBGetSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGetDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBGetSchema s1 = new LBGetSchema();
                s1.setSchema(rs,i);
                aLBGetSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLBGetSet;
    }

    public LBGetSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LBGetSet aLBGetSet = new LBGetSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGetDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LBGetSchema s1 = new LBGetSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGetDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBGetSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBGetSet;
    }

    public LBGetSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBGetSet aLBGetSet = new LBGetSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBGet");
            LBGetSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBGetSchema s1 = new LBGetSchema();
                s1.setSchema(rs,i);
                aLBGetSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBGetSet;
    }

    public LBGetSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LBGetSet aLBGetSet = new LBGetSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LBGetSchema s1 = new LBGetSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LBGetDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLBGetSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLBGetSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LBGet");
            LBGetSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LBGet " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LBGetDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LBGetSet
     */
    public LBGetSet getData() {
        int tCount = 0;
        LBGetSet tLBGetSet = new LBGetSet();
        LBGetSchema tLBGetSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLBGetSchema = new LBGetSchema();
            tLBGetSchema.setSchema(mResultSet, 1);
            tLBGetSet.add(tLBGetSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLBGetSchema = new LBGetSchema();
                    tLBGetSchema.setSchema(mResultSet, 1);
                    tLBGetSet.add(tLBGetSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLBGetSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LBGetDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LBGetDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LBGetDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
