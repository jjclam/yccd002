/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LCGrpAddressPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCGrpAddressPojo implements Pojo,Serializable {
    // @Field
    /** 客户号码 */
    private String CustomerNo; 
    /** 地址号码 */
    private String AddressNo; 
    /** 单位地址 */
    private String GrpAddress; 
    /** 单位邮编 */
    private String GrpZipCode; 
    /** 联系人1 */
    private String LinkMan1; 
    /** 部门1 */
    private String Department1; 
    /** 职务1 */
    private String HeadShip1; 
    /** 联系电话1 */
    private String Phone1; 
    /** E_mail1 */
    private String E_Mail1; 
    /** 传真1 */
    private String Fax1; 
    /** 联系人2 */
    private String LinkMan2; 
    /** 部门2 */
    private String Department2; 
    /** 职务2 */
    private String HeadShip2; 
    /** 联系电话2 */
    private String Phone2; 
    /** E_mail2 */
    private String E_Mail2; 
    /** 传真2 */
    private String Fax2; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 手机1 */
    private String MobilePhone1; 
    /** 手机2 */
    private String MobilePhone2; 
    /** 证件类型1 */
    private String IDType; 
    /** 证件号1 */
    private String IDNo; 
    /** 证件有效日期 */
    private String  IDNoValidate;
    /** 证件类型2 */
    private String IDType2; 
    /** 证件号2 */
    private String IDNo2; 
    /** 证件有效日期2 */
    private String  IDNoValidate2;
    /** 省 */
    private String Province; 
    /** 市 */
    private String City; 
    /** 县 */
    private String County; 
    /** 投保单位是否为美国企业 */
    private String TinNo; 
    /** 投保单位美国纳税人识别号 */
    private String TinFlag; 
    /** 投保单位是否有美国控制人 */
    private String USATinFlag; 
    /** 美国控制人姓名 */
    private String USATinName; 
    /** 美国控制人地址 */
    private String USATinAddress; 
    /** 美国控制人纳税人识别号 */
    private String USATinNo; 


    public static final int FIELDNUM = 38;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }
    public String getGrpAddress() {
        return GrpAddress;
    }
    public void setGrpAddress(String aGrpAddress) {
        GrpAddress = aGrpAddress;
    }
    public String getGrpZipCode() {
        return GrpZipCode;
    }
    public void setGrpZipCode(String aGrpZipCode) {
        GrpZipCode = aGrpZipCode;
    }
    public String getLinkMan1() {
        return LinkMan1;
    }
    public void setLinkMan1(String aLinkMan1) {
        LinkMan1 = aLinkMan1;
    }
    public String getDepartment1() {
        return Department1;
    }
    public void setDepartment1(String aDepartment1) {
        Department1 = aDepartment1;
    }
    public String getHeadShip1() {
        return HeadShip1;
    }
    public void setHeadShip1(String aHeadShip1) {
        HeadShip1 = aHeadShip1;
    }
    public String getPhone1() {
        return Phone1;
    }
    public void setPhone1(String aPhone1) {
        Phone1 = aPhone1;
    }
    public String getE_Mail1() {
        return E_Mail1;
    }
    public void setE_Mail1(String aE_Mail1) {
        E_Mail1 = aE_Mail1;
    }
    public String getFax1() {
        return Fax1;
    }
    public void setFax1(String aFax1) {
        Fax1 = aFax1;
    }
    public String getLinkMan2() {
        return LinkMan2;
    }
    public void setLinkMan2(String aLinkMan2) {
        LinkMan2 = aLinkMan2;
    }
    public String getDepartment2() {
        return Department2;
    }
    public void setDepartment2(String aDepartment2) {
        Department2 = aDepartment2;
    }
    public String getHeadShip2() {
        return HeadShip2;
    }
    public void setHeadShip2(String aHeadShip2) {
        HeadShip2 = aHeadShip2;
    }
    public String getPhone2() {
        return Phone2;
    }
    public void setPhone2(String aPhone2) {
        Phone2 = aPhone2;
    }
    public String getE_Mail2() {
        return E_Mail2;
    }
    public void setE_Mail2(String aE_Mail2) {
        E_Mail2 = aE_Mail2;
    }
    public String getFax2() {
        return Fax2;
    }
    public void setFax2(String aFax2) {
        Fax2 = aFax2;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getMobilePhone1() {
        return MobilePhone1;
    }
    public void setMobilePhone1(String aMobilePhone1) {
        MobilePhone1 = aMobilePhone1;
    }
    public String getMobilePhone2() {
        return MobilePhone2;
    }
    public void setMobilePhone2(String aMobilePhone2) {
        MobilePhone2 = aMobilePhone2;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getIDNoValidate() {
        return IDNoValidate;
    }
    public void setIDNoValidate(String aIDNoValidate) {
        IDNoValidate = aIDNoValidate;
    }
    public String getIDType2() {
        return IDType2;
    }
    public void setIDType2(String aIDType2) {
        IDType2 = aIDType2;
    }
    public String getIDNo2() {
        return IDNo2;
    }
    public void setIDNo2(String aIDNo2) {
        IDNo2 = aIDNo2;
    }
    public String getIDNoValidate2() {
        return IDNoValidate2;
    }
    public void setIDNoValidate2(String aIDNoValidate2) {
        IDNoValidate2 = aIDNoValidate2;
    }
    public String getProvince() {
        return Province;
    }
    public void setProvince(String aProvince) {
        Province = aProvince;
    }
    public String getCity() {
        return City;
    }
    public void setCity(String aCity) {
        City = aCity;
    }
    public String getCounty() {
        return County;
    }
    public void setCounty(String aCounty) {
        County = aCounty;
    }
    public String getTinNo() {
        return TinNo;
    }
    public void setTinNo(String aTinNo) {
        TinNo = aTinNo;
    }
    public String getTinFlag() {
        return TinFlag;
    }
    public void setTinFlag(String aTinFlag) {
        TinFlag = aTinFlag;
    }
    public String getUSATinFlag() {
        return USATinFlag;
    }
    public void setUSATinFlag(String aUSATinFlag) {
        USATinFlag = aUSATinFlag;
    }
    public String getUSATinName() {
        return USATinName;
    }
    public void setUSATinName(String aUSATinName) {
        USATinName = aUSATinName;
    }
    public String getUSATinAddress() {
        return USATinAddress;
    }
    public void setUSATinAddress(String aUSATinAddress) {
        USATinAddress = aUSATinAddress;
    }
    public String getUSATinNo() {
        return USATinNo;
    }
    public void setUSATinNo(String aUSATinNo) {
        USATinNo = aUSATinNo;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CustomerNo") ) {
            return 0;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 1;
        }
        if( strFieldName.equals("GrpAddress") ) {
            return 2;
        }
        if( strFieldName.equals("GrpZipCode") ) {
            return 3;
        }
        if( strFieldName.equals("LinkMan1") ) {
            return 4;
        }
        if( strFieldName.equals("Department1") ) {
            return 5;
        }
        if( strFieldName.equals("HeadShip1") ) {
            return 6;
        }
        if( strFieldName.equals("Phone1") ) {
            return 7;
        }
        if( strFieldName.equals("E_Mail1") ) {
            return 8;
        }
        if( strFieldName.equals("Fax1") ) {
            return 9;
        }
        if( strFieldName.equals("LinkMan2") ) {
            return 10;
        }
        if( strFieldName.equals("Department2") ) {
            return 11;
        }
        if( strFieldName.equals("HeadShip2") ) {
            return 12;
        }
        if( strFieldName.equals("Phone2") ) {
            return 13;
        }
        if( strFieldName.equals("E_Mail2") ) {
            return 14;
        }
        if( strFieldName.equals("Fax2") ) {
            return 15;
        }
        if( strFieldName.equals("Operator") ) {
            return 16;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 17;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 19;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 20;
        }
        if( strFieldName.equals("MobilePhone1") ) {
            return 21;
        }
        if( strFieldName.equals("MobilePhone2") ) {
            return 22;
        }
        if( strFieldName.equals("IDType") ) {
            return 23;
        }
        if( strFieldName.equals("IDNo") ) {
            return 24;
        }
        if( strFieldName.equals("IDNoValidate") ) {
            return 25;
        }
        if( strFieldName.equals("IDType2") ) {
            return 26;
        }
        if( strFieldName.equals("IDNo2") ) {
            return 27;
        }
        if( strFieldName.equals("IDNoValidate2") ) {
            return 28;
        }
        if( strFieldName.equals("Province") ) {
            return 29;
        }
        if( strFieldName.equals("City") ) {
            return 30;
        }
        if( strFieldName.equals("County") ) {
            return 31;
        }
        if( strFieldName.equals("TinNo") ) {
            return 32;
        }
        if( strFieldName.equals("TinFlag") ) {
            return 33;
        }
        if( strFieldName.equals("USATinFlag") ) {
            return 34;
        }
        if( strFieldName.equals("USATinName") ) {
            return 35;
        }
        if( strFieldName.equals("USATinAddress") ) {
            return 36;
        }
        if( strFieldName.equals("USATinNo") ) {
            return 37;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CustomerNo";
                break;
            case 1:
                strFieldName = "AddressNo";
                break;
            case 2:
                strFieldName = "GrpAddress";
                break;
            case 3:
                strFieldName = "GrpZipCode";
                break;
            case 4:
                strFieldName = "LinkMan1";
                break;
            case 5:
                strFieldName = "Department1";
                break;
            case 6:
                strFieldName = "HeadShip1";
                break;
            case 7:
                strFieldName = "Phone1";
                break;
            case 8:
                strFieldName = "E_Mail1";
                break;
            case 9:
                strFieldName = "Fax1";
                break;
            case 10:
                strFieldName = "LinkMan2";
                break;
            case 11:
                strFieldName = "Department2";
                break;
            case 12:
                strFieldName = "HeadShip2";
                break;
            case 13:
                strFieldName = "Phone2";
                break;
            case 14:
                strFieldName = "E_Mail2";
                break;
            case 15:
                strFieldName = "Fax2";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "ModifyDate";
                break;
            case 20:
                strFieldName = "ModifyTime";
                break;
            case 21:
                strFieldName = "MobilePhone1";
                break;
            case 22:
                strFieldName = "MobilePhone2";
                break;
            case 23:
                strFieldName = "IDType";
                break;
            case 24:
                strFieldName = "IDNo";
                break;
            case 25:
                strFieldName = "IDNoValidate";
                break;
            case 26:
                strFieldName = "IDType2";
                break;
            case 27:
                strFieldName = "IDNo2";
                break;
            case 28:
                strFieldName = "IDNoValidate2";
                break;
            case 29:
                strFieldName = "Province";
                break;
            case 30:
                strFieldName = "City";
                break;
            case 31:
                strFieldName = "County";
                break;
            case 32:
                strFieldName = "TinNo";
                break;
            case 33:
                strFieldName = "TinFlag";
                break;
            case 34:
                strFieldName = "USATinFlag";
                break;
            case 35:
                strFieldName = "USATinName";
                break;
            case 36:
                strFieldName = "USATinAddress";
                break;
            case 37:
                strFieldName = "USATinNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_STRING;
            case "GRPADDRESS":
                return Schema.TYPE_STRING;
            case "GRPZIPCODE":
                return Schema.TYPE_STRING;
            case "LINKMAN1":
                return Schema.TYPE_STRING;
            case "DEPARTMENT1":
                return Schema.TYPE_STRING;
            case "HEADSHIP1":
                return Schema.TYPE_STRING;
            case "PHONE1":
                return Schema.TYPE_STRING;
            case "E_MAIL1":
                return Schema.TYPE_STRING;
            case "FAX1":
                return Schema.TYPE_STRING;
            case "LINKMAN2":
                return Schema.TYPE_STRING;
            case "DEPARTMENT2":
                return Schema.TYPE_STRING;
            case "HEADSHIP2":
                return Schema.TYPE_STRING;
            case "PHONE2":
                return Schema.TYPE_STRING;
            case "E_MAIL2":
                return Schema.TYPE_STRING;
            case "FAX2":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MOBILEPHONE1":
                return Schema.TYPE_STRING;
            case "MOBILEPHONE2":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "IDNOVALIDATE":
                return Schema.TYPE_STRING;
            case "IDTYPE2":
                return Schema.TYPE_STRING;
            case "IDNO2":
                return Schema.TYPE_STRING;
            case "IDNOVALIDATE2":
                return Schema.TYPE_STRING;
            case "PROVINCE":
                return Schema.TYPE_STRING;
            case "CITY":
                return Schema.TYPE_STRING;
            case "COUNTY":
                return Schema.TYPE_STRING;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            case "USATINFLAG":
                return Schema.TYPE_STRING;
            case "USATINNAME":
                return Schema.TYPE_STRING;
            case "USATINADDRESS":
                return Schema.TYPE_STRING;
            case "USATINNO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("GrpAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAddress));
        }
        if (FCode.equalsIgnoreCase("GrpZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpZipCode));
        }
        if (FCode.equalsIgnoreCase("LinkMan1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan1));
        }
        if (FCode.equalsIgnoreCase("Department1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Department1));
        }
        if (FCode.equalsIgnoreCase("HeadShip1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadShip1));
        }
        if (FCode.equalsIgnoreCase("Phone1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone1));
        }
        if (FCode.equalsIgnoreCase("E_Mail1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_Mail1));
        }
        if (FCode.equalsIgnoreCase("Fax1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax1));
        }
        if (FCode.equalsIgnoreCase("LinkMan2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan2));
        }
        if (FCode.equalsIgnoreCase("Department2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Department2));
        }
        if (FCode.equalsIgnoreCase("HeadShip2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadShip2));
        }
        if (FCode.equalsIgnoreCase("Phone2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone2));
        }
        if (FCode.equalsIgnoreCase("E_Mail2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_Mail2));
        }
        if (FCode.equalsIgnoreCase("Fax2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax2));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("MobilePhone1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MobilePhone1));
        }
        if (FCode.equalsIgnoreCase("MobilePhone2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MobilePhone2));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("IDNoValidate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoValidate));
        }
        if (FCode.equalsIgnoreCase("IDType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType2));
        }
        if (FCode.equalsIgnoreCase("IDNo2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo2));
        }
        if (FCode.equalsIgnoreCase("IDNoValidate2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoValidate2));
        }
        if (FCode.equalsIgnoreCase("Province")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Province));
        }
        if (FCode.equalsIgnoreCase("City")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(City));
        }
        if (FCode.equalsIgnoreCase("County")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(County));
        }
        if (FCode.equalsIgnoreCase("TinNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TinNo));
        }
        if (FCode.equalsIgnoreCase("TinFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TinFlag));
        }
        if (FCode.equalsIgnoreCase("USATinFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USATinFlag));
        }
        if (FCode.equalsIgnoreCase("USATinName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USATinName));
        }
        if (FCode.equalsIgnoreCase("USATinAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USATinAddress));
        }
        if (FCode.equalsIgnoreCase("USATinNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USATinNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 1:
                strFieldValue = String.valueOf(AddressNo);
                break;
            case 2:
                strFieldValue = String.valueOf(GrpAddress);
                break;
            case 3:
                strFieldValue = String.valueOf(GrpZipCode);
                break;
            case 4:
                strFieldValue = String.valueOf(LinkMan1);
                break;
            case 5:
                strFieldValue = String.valueOf(Department1);
                break;
            case 6:
                strFieldValue = String.valueOf(HeadShip1);
                break;
            case 7:
                strFieldValue = String.valueOf(Phone1);
                break;
            case 8:
                strFieldValue = String.valueOf(E_Mail1);
                break;
            case 9:
                strFieldValue = String.valueOf(Fax1);
                break;
            case 10:
                strFieldValue = String.valueOf(LinkMan2);
                break;
            case 11:
                strFieldValue = String.valueOf(Department2);
                break;
            case 12:
                strFieldValue = String.valueOf(HeadShip2);
                break;
            case 13:
                strFieldValue = String.valueOf(Phone2);
                break;
            case 14:
                strFieldValue = String.valueOf(E_Mail2);
                break;
            case 15:
                strFieldValue = String.valueOf(Fax2);
                break;
            case 16:
                strFieldValue = String.valueOf(Operator);
                break;
            case 17:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 18:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 19:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 20:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 21:
                strFieldValue = String.valueOf(MobilePhone1);
                break;
            case 22:
                strFieldValue = String.valueOf(MobilePhone2);
                break;
            case 23:
                strFieldValue = String.valueOf(IDType);
                break;
            case 24:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 25:
                strFieldValue = String.valueOf(IDNoValidate);
                break;
            case 26:
                strFieldValue = String.valueOf(IDType2);
                break;
            case 27:
                strFieldValue = String.valueOf(IDNo2);
                break;
            case 28:
                strFieldValue = String.valueOf(IDNoValidate2);
                break;
            case 29:
                strFieldValue = String.valueOf(Province);
                break;
            case 30:
                strFieldValue = String.valueOf(City);
                break;
            case 31:
                strFieldValue = String.valueOf(County);
                break;
            case 32:
                strFieldValue = String.valueOf(TinNo);
                break;
            case 33:
                strFieldValue = String.valueOf(TinFlag);
                break;
            case 34:
                strFieldValue = String.valueOf(USATinFlag);
                break;
            case 35:
                strFieldValue = String.valueOf(USATinName);
                break;
            case 36:
                strFieldValue = String.valueOf(USATinAddress);
                break;
            case 37:
                strFieldValue = String.valueOf(USATinNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddressNo = FValue.trim();
            }
            else
                AddressNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAddress = FValue.trim();
            }
            else
                GrpAddress = null;
        }
        if (FCode.equalsIgnoreCase("GrpZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpZipCode = FValue.trim();
            }
            else
                GrpZipCode = null;
        }
        if (FCode.equalsIgnoreCase("LinkMan1")) {
            if( FValue != null && !FValue.equals(""))
            {
                LinkMan1 = FValue.trim();
            }
            else
                LinkMan1 = null;
        }
        if (FCode.equalsIgnoreCase("Department1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Department1 = FValue.trim();
            }
            else
                Department1 = null;
        }
        if (FCode.equalsIgnoreCase("HeadShip1")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadShip1 = FValue.trim();
            }
            else
                HeadShip1 = null;
        }
        if (FCode.equalsIgnoreCase("Phone1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone1 = FValue.trim();
            }
            else
                Phone1 = null;
        }
        if (FCode.equalsIgnoreCase("E_Mail1")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_Mail1 = FValue.trim();
            }
            else
                E_Mail1 = null;
        }
        if (FCode.equalsIgnoreCase("Fax1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax1 = FValue.trim();
            }
            else
                Fax1 = null;
        }
        if (FCode.equalsIgnoreCase("LinkMan2")) {
            if( FValue != null && !FValue.equals(""))
            {
                LinkMan2 = FValue.trim();
            }
            else
                LinkMan2 = null;
        }
        if (FCode.equalsIgnoreCase("Department2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Department2 = FValue.trim();
            }
            else
                Department2 = null;
        }
        if (FCode.equalsIgnoreCase("HeadShip2")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadShip2 = FValue.trim();
            }
            else
                HeadShip2 = null;
        }
        if (FCode.equalsIgnoreCase("Phone2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone2 = FValue.trim();
            }
            else
                Phone2 = null;
        }
        if (FCode.equalsIgnoreCase("E_Mail2")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_Mail2 = FValue.trim();
            }
            else
                E_Mail2 = null;
        }
        if (FCode.equalsIgnoreCase("Fax2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax2 = FValue.trim();
            }
            else
                Fax2 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("MobilePhone1")) {
            if( FValue != null && !FValue.equals(""))
            {
                MobilePhone1 = FValue.trim();
            }
            else
                MobilePhone1 = null;
        }
        if (FCode.equalsIgnoreCase("MobilePhone2")) {
            if( FValue != null && !FValue.equals(""))
            {
                MobilePhone2 = FValue.trim();
            }
            else
                MobilePhone2 = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("IDNoValidate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNoValidate = FValue.trim();
            }
            else
                IDNoValidate = null;
        }
        if (FCode.equalsIgnoreCase("IDType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType2 = FValue.trim();
            }
            else
                IDType2 = null;
        }
        if (FCode.equalsIgnoreCase("IDNo2")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo2 = FValue.trim();
            }
            else
                IDNo2 = null;
        }
        if (FCode.equalsIgnoreCase("IDNoValidate2")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNoValidate2 = FValue.trim();
            }
            else
                IDNoValidate2 = null;
        }
        if (FCode.equalsIgnoreCase("Province")) {
            if( FValue != null && !FValue.equals(""))
            {
                Province = FValue.trim();
            }
            else
                Province = null;
        }
        if (FCode.equalsIgnoreCase("City")) {
            if( FValue != null && !FValue.equals(""))
            {
                City = FValue.trim();
            }
            else
                City = null;
        }
        if (FCode.equalsIgnoreCase("County")) {
            if( FValue != null && !FValue.equals(""))
            {
                County = FValue.trim();
            }
            else
                County = null;
        }
        if (FCode.equalsIgnoreCase("TinNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TinNo = FValue.trim();
            }
            else
                TinNo = null;
        }
        if (FCode.equalsIgnoreCase("TinFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TinFlag = FValue.trim();
            }
            else
                TinFlag = null;
        }
        if (FCode.equalsIgnoreCase("USATinFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                USATinFlag = FValue.trim();
            }
            else
                USATinFlag = null;
        }
        if (FCode.equalsIgnoreCase("USATinName")) {
            if( FValue != null && !FValue.equals(""))
            {
                USATinName = FValue.trim();
            }
            else
                USATinName = null;
        }
        if (FCode.equalsIgnoreCase("USATinAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                USATinAddress = FValue.trim();
            }
            else
                USATinAddress = null;
        }
        if (FCode.equalsIgnoreCase("USATinNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                USATinNo = FValue.trim();
            }
            else
                USATinNo = null;
        }
        return true;
    }


    public String toString() {
    return "LCGrpAddressPojo [" +
            "CustomerNo="+CustomerNo +
            ", AddressNo="+AddressNo +
            ", GrpAddress="+GrpAddress +
            ", GrpZipCode="+GrpZipCode +
            ", LinkMan1="+LinkMan1 +
            ", Department1="+Department1 +
            ", HeadShip1="+HeadShip1 +
            ", Phone1="+Phone1 +
            ", E_Mail1="+E_Mail1 +
            ", Fax1="+Fax1 +
            ", LinkMan2="+LinkMan2 +
            ", Department2="+Department2 +
            ", HeadShip2="+HeadShip2 +
            ", Phone2="+Phone2 +
            ", E_Mail2="+E_Mail2 +
            ", Fax2="+Fax2 +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", MobilePhone1="+MobilePhone1 +
            ", MobilePhone2="+MobilePhone2 +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", IDNoValidate="+IDNoValidate +
            ", IDType2="+IDType2 +
            ", IDNo2="+IDNo2 +
            ", IDNoValidate2="+IDNoValidate2 +
            ", Province="+Province +
            ", City="+City +
            ", County="+County +
            ", TinNo="+TinNo +
            ", TinFlag="+TinFlag +
            ", USATinFlag="+USATinFlag +
            ", USATinName="+USATinName +
            ", USATinAddress="+USATinAddress +
            ", USATinNo="+USATinNo +"]";
    }
}
