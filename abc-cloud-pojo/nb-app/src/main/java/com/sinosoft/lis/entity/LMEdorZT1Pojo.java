/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMEdorZT1Pojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMEdorZT1Pojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 责任编码 */
    private String DutyCode; 
    /** 交费计划编码 */
    private String PayPlanCode; 
    /** 退保计算类型 */
    private String SurrCalType; 
    /** 期缴时间间隔 */
    private String CycPayIntvType; 
    /** 算法编码 */
    private String CycPayCalCode; 
    /** 退保生存金计算类型 */
    private String LiveGetType; 
    /** 死亡退保金计算类型 */
    private String DeadGetType; 
    /** 计算方式参考 */
    private String CalCodeType; 
    /** 退保年度计算类型 */
    private String ZTYearType; 


    public static final int FIELDNUM = 10;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getSurrCalType() {
        return SurrCalType;
    }
    public void setSurrCalType(String aSurrCalType) {
        SurrCalType = aSurrCalType;
    }
    public String getCycPayIntvType() {
        return CycPayIntvType;
    }
    public void setCycPayIntvType(String aCycPayIntvType) {
        CycPayIntvType = aCycPayIntvType;
    }
    public String getCycPayCalCode() {
        return CycPayCalCode;
    }
    public void setCycPayCalCode(String aCycPayCalCode) {
        CycPayCalCode = aCycPayCalCode;
    }
    public String getLiveGetType() {
        return LiveGetType;
    }
    public void setLiveGetType(String aLiveGetType) {
        LiveGetType = aLiveGetType;
    }
    public String getDeadGetType() {
        return DeadGetType;
    }
    public void setDeadGetType(String aDeadGetType) {
        DeadGetType = aDeadGetType;
    }
    public String getCalCodeType() {
        return CalCodeType;
    }
    public void setCalCodeType(String aCalCodeType) {
        CalCodeType = aCalCodeType;
    }
    public String getZTYearType() {
        return ZTYearType;
    }
    public void setZTYearType(String aZTYearType) {
        ZTYearType = aZTYearType;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 1;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 2;
        }
        if( strFieldName.equals("SurrCalType") ) {
            return 3;
        }
        if( strFieldName.equals("CycPayIntvType") ) {
            return 4;
        }
        if( strFieldName.equals("CycPayCalCode") ) {
            return 5;
        }
        if( strFieldName.equals("LiveGetType") ) {
            return 6;
        }
        if( strFieldName.equals("DeadGetType") ) {
            return 7;
        }
        if( strFieldName.equals("CalCodeType") ) {
            return 8;
        }
        if( strFieldName.equals("ZTYearType") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "DutyCode";
                break;
            case 2:
                strFieldName = "PayPlanCode";
                break;
            case 3:
                strFieldName = "SurrCalType";
                break;
            case 4:
                strFieldName = "CycPayIntvType";
                break;
            case 5:
                strFieldName = "CycPayCalCode";
                break;
            case 6:
                strFieldName = "LiveGetType";
                break;
            case 7:
                strFieldName = "DeadGetType";
                break;
            case 8:
                strFieldName = "CalCodeType";
                break;
            case 9:
                strFieldName = "ZTYearType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "SURRCALTYPE":
                return Schema.TYPE_STRING;
            case "CYCPAYINTVTYPE":
                return Schema.TYPE_STRING;
            case "CYCPAYCALCODE":
                return Schema.TYPE_STRING;
            case "LIVEGETTYPE":
                return Schema.TYPE_STRING;
            case "DEADGETTYPE":
                return Schema.TYPE_STRING;
            case "CALCODETYPE":
                return Schema.TYPE_STRING;
            case "ZTYEARTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("SurrCalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurrCalType));
        }
        if (FCode.equalsIgnoreCase("CycPayIntvType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CycPayIntvType));
        }
        if (FCode.equalsIgnoreCase("CycPayCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CycPayCalCode));
        }
        if (FCode.equalsIgnoreCase("LiveGetType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetType));
        }
        if (FCode.equalsIgnoreCase("DeadGetType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadGetType));
        }
        if (FCode.equalsIgnoreCase("CalCodeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCodeType));
        }
        if (FCode.equalsIgnoreCase("ZTYearType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZTYearType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 2:
                strFieldValue = String.valueOf(PayPlanCode);
                break;
            case 3:
                strFieldValue = String.valueOf(SurrCalType);
                break;
            case 4:
                strFieldValue = String.valueOf(CycPayIntvType);
                break;
            case 5:
                strFieldValue = String.valueOf(CycPayCalCode);
                break;
            case 6:
                strFieldValue = String.valueOf(LiveGetType);
                break;
            case 7:
                strFieldValue = String.valueOf(DeadGetType);
                break;
            case 8:
                strFieldValue = String.valueOf(CalCodeType);
                break;
            case 9:
                strFieldValue = String.valueOf(ZTYearType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("SurrCalType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurrCalType = FValue.trim();
            }
            else
                SurrCalType = null;
        }
        if (FCode.equalsIgnoreCase("CycPayIntvType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CycPayIntvType = FValue.trim();
            }
            else
                CycPayIntvType = null;
        }
        if (FCode.equalsIgnoreCase("CycPayCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CycPayCalCode = FValue.trim();
            }
            else
                CycPayCalCode = null;
        }
        if (FCode.equalsIgnoreCase("LiveGetType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveGetType = FValue.trim();
            }
            else
                LiveGetType = null;
        }
        if (FCode.equalsIgnoreCase("DeadGetType")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadGetType = FValue.trim();
            }
            else
                DeadGetType = null;
        }
        if (FCode.equalsIgnoreCase("CalCodeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCodeType = FValue.trim();
            }
            else
                CalCodeType = null;
        }
        if (FCode.equalsIgnoreCase("ZTYearType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZTYearType = FValue.trim();
            }
            else
                ZTYearType = null;
        }
        return true;
    }


    public String toString() {
    return "LMEdorZT1Pojo [" +
            "RiskCode="+RiskCode +
            ", DutyCode="+DutyCode +
            ", PayPlanCode="+PayPlanCode +
            ", SurrCalType="+SurrCalType +
            ", CycPayIntvType="+CycPayIntvType +
            ", CycPayCalCode="+CycPayCalCode +
            ", LiveGetType="+LiveGetType +
            ", DeadGetType="+DeadGetType +
            ", CalCodeType="+CalCodeType +
            ", ZTYearType="+ZTYearType +"]";
    }
}
