/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDAvgInComPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-03-21
 */
public class LDAvgInComPojo implements  Pojo,Serializable {
    // @Field
    /** Seqno */
    private String SEQNO; 
    /** Comcode */
    @RedisPrimaryHKey
    private String COMCODE; 
    /** Cityincome */
    private double CITYINCOME; 
    /** Villageincome */
    private double VILLAGEINCOME; 
    /** Year */
    private String YEAR; 
    /** Operator */
    private String OPERATOR; 
    /** Makedate */
    private String  MAKEDATE;
    /** Maketime */
    private String MAKETIME; 
    /** Modifydate */
    private String  MODIFYDATE;
    /** Modifytime */
    private String MODIFYTIME; 
    /** Standbyflag1 */
    private String STANDBYFLAG1; 


    public static final int FIELDNUM = 11;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSEQNO() {
        return SEQNO;
    }
    public void setSEQNO(String aSEQNO) {
        SEQNO = aSEQNO;
    }
    public String getCOMCODE() {
        return COMCODE;
    }
    public void setCOMCODE(String aCOMCODE) {
        COMCODE = aCOMCODE;
    }
    public double getCITYINCOME() {
        return CITYINCOME;
    }
    public void setCITYINCOME(double aCITYINCOME) {
        CITYINCOME = aCITYINCOME;
    }
    public void setCITYINCOME(String aCITYINCOME) {
        if (aCITYINCOME != null && !aCITYINCOME.equals("")) {
            Double tDouble = new Double(aCITYINCOME);
            double d = tDouble.doubleValue();
            CITYINCOME = d;
        }
    }

    public double getVILLAGEINCOME() {
        return VILLAGEINCOME;
    }
    public void setVILLAGEINCOME(double aVILLAGEINCOME) {
        VILLAGEINCOME = aVILLAGEINCOME;
    }
    public void setVILLAGEINCOME(String aVILLAGEINCOME) {
        if (aVILLAGEINCOME != null && !aVILLAGEINCOME.equals("")) {
            Double tDouble = new Double(aVILLAGEINCOME);
            double d = tDouble.doubleValue();
            VILLAGEINCOME = d;
        }
    }

    public String getYEAR() {
        return YEAR;
    }
    public void setYEAR(String aYEAR) {
        YEAR = aYEAR;
    }
    public String getOPERATOR() {
        return OPERATOR;
    }
    public void setOPERATOR(String aOPERATOR) {
        OPERATOR = aOPERATOR;
    }
    public String getMAKEDATE() {
        return MAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }
    public String getMODIFYDATE() {
        return MODIFYDATE;
    }
    public void setMODIFYDATE(String aMODIFYDATE) {
        MODIFYDATE = aMODIFYDATE;
    }
    public String getMODIFYTIME() {
        return MODIFYTIME;
    }
    public void setMODIFYTIME(String aMODIFYTIME) {
        MODIFYTIME = aMODIFYTIME;
    }
    public String getSTANDBYFLAG1() {
        return STANDBYFLAG1;
    }
    public void setSTANDBYFLAG1(String aSTANDBYFLAG1) {
        STANDBYFLAG1 = aSTANDBYFLAG1;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SEQNO") ) {
            return 0;
        }
        if( strFieldName.equals("COMCODE") ) {
            return 1;
        }
        if( strFieldName.equals("CITYINCOME") ) {
            return 2;
        }
        if( strFieldName.equals("VILLAGEINCOME") ) {
            return 3;
        }
        if( strFieldName.equals("YEAR") ) {
            return 4;
        }
        if( strFieldName.equals("OPERATOR") ) {
            return 5;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 6;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 7;
        }
        if( strFieldName.equals("MODIFYDATE") ) {
            return 8;
        }
        if( strFieldName.equals("MODIFYTIME") ) {
            return 9;
        }
        if( strFieldName.equals("STANDBYFLAG1") ) {
            return 10;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SEQNO";
                break;
            case 1:
                strFieldName = "COMCODE";
                break;
            case 2:
                strFieldName = "CITYINCOME";
                break;
            case 3:
                strFieldName = "VILLAGEINCOME";
                break;
            case 4:
                strFieldName = "YEAR";
                break;
            case 5:
                strFieldName = "OPERATOR";
                break;
            case 6:
                strFieldName = "MAKEDATE";
                break;
            case 7:
                strFieldName = "MAKETIME";
                break;
            case 8:
                strFieldName = "MODIFYDATE";
                break;
            case 9:
                strFieldName = "MODIFYTIME";
                break;
            case 10:
                strFieldName = "STANDBYFLAG1";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SEQNO":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "CITYINCOME":
                return Schema.TYPE_DOUBLE;
            case "VILLAGEINCOME":
                return Schema.TYPE_DOUBLE;
            case "YEAR":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_DOUBLE;
            case 3:
                return Schema.TYPE_DOUBLE;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SEQNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SEQNO));
        }
        if (FCode.equalsIgnoreCase("COMCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMCODE));
        }
        if (FCode.equalsIgnoreCase("CITYINCOME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CITYINCOME));
        }
        if (FCode.equalsIgnoreCase("VILLAGEINCOME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VILLAGEINCOME));
        }
        if (FCode.equalsIgnoreCase("YEAR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(YEAR));
        }
        if (FCode.equalsIgnoreCase("OPERATOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OPERATOR));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKEDATE));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYDATE));
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG1));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SEQNO);
                break;
            case 1:
                strFieldValue = String.valueOf(COMCODE);
                break;
            case 2:
                strFieldValue = String.valueOf(CITYINCOME);
                break;
            case 3:
                strFieldValue = String.valueOf(VILLAGEINCOME);
                break;
            case 4:
                strFieldValue = String.valueOf(YEAR);
                break;
            case 5:
                strFieldValue = String.valueOf(OPERATOR);
                break;
            case 6:
                strFieldValue = String.valueOf(MAKEDATE);
                break;
            case 7:
                strFieldValue = String.valueOf(MAKETIME);
                break;
            case 8:
                strFieldValue = String.valueOf(MODIFYDATE);
                break;
            case 9:
                strFieldValue = String.valueOf(MODIFYTIME);
                break;
            case 10:
                strFieldValue = String.valueOf(STANDBYFLAG1);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SEQNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SEQNO = FValue.trim();
            }
            else
                SEQNO = null;
        }
        if (FCode.equalsIgnoreCase("COMCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMCODE = FValue.trim();
            }
            else
                COMCODE = null;
        }
        if (FCode.equalsIgnoreCase("CITYINCOME")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                CITYINCOME = d;
            }
        }
        if (FCode.equalsIgnoreCase("VILLAGEINCOME")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                VILLAGEINCOME = d;
            }
        }
        if (FCode.equalsIgnoreCase("YEAR")) {
            if( FValue != null && !FValue.equals(""))
            {
                YEAR = FValue.trim();
            }
            else
                YEAR = null;
        }
        if (FCode.equalsIgnoreCase("OPERATOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                OPERATOR = FValue.trim();
            }
            else
                OPERATOR = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKEDATE = FValue.trim();
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MODIFYDATE = FValue.trim();
            }
            else
                MODIFYDATE = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MODIFYTIME = FValue.trim();
            }
            else
                MODIFYTIME = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG1 = FValue.trim();
            }
            else
                STANDBYFLAG1 = null;
        }
        return true;
    }


    public String toString() {
    return "LDAvgInComPojo [" +
            "SEQNO="+SEQNO +
            ", COMCODE="+COMCODE +
            ", CITYINCOME="+CITYINCOME +
            ", VILLAGEINCOME="+VILLAGEINCOME +
            ", YEAR="+YEAR +
            ", OPERATOR="+OPERATOR +
            ", MAKEDATE="+MAKEDATE +
            ", MAKETIME="+MAKETIME +
            ", MODIFYDATE="+MODIFYDATE +
            ", MODIFYTIME="+MODIFYTIME +
            ", STANDBYFLAG1="+STANDBYFLAG1 +"]";
    }
}
