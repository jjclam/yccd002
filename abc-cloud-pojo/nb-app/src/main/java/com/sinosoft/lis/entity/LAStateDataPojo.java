/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAStateDataPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-27
 */
public class LAStateDataPojo implements Pojo,Serializable {
    // @Field
    /** Objectid */
    @RedisPrimaryHKey
    private String ObjectId; 
    /** Startdate */
    @RedisPrimaryHKey
    private String StartDate; 
    /** Enddate */
    @RedisPrimaryHKey
    private String EndDate; 
    /** Branchtype */
    @RedisPrimaryHKey
    private String BranchType; 
    /** Statetype */
    @RedisPrimaryHKey
    private String StateType; 
    /** T1 */
    private double T1; 
    /** T2 */
    private double T2; 
    /** T3 */
    private double T3; 
    /** T4 */
    private double T4; 
    /** T5 */
    private double T5; 
    /** T6 */
    private double T6; 
    /** T7 */
    private double T7; 
    /** T8 */
    private double T8; 
    /** T9 */
    private double T9; 
    /** T10 */
    private double T10; 
    /** T11 */
    private double T11; 
    /** T12 */
    private double T12; 
    /** T13 */
    private double T13; 
    /** T14 */
    private double T14; 
    /** T15 */
    private double T15; 
    /** T16 */
    private double T16; 
    /** T17 */
    private double T17; 
    /** T18 */
    private double T18; 
    /** T19 */
    private double T19; 
    /** T20 */
    private double T20; 


    public static final int FIELDNUM = 25;    // 数据库表的字段个数
    public String getObjectId() {
        return ObjectId;
    }
    public void setObjectId(String aObjectId) {
        ObjectId = aObjectId;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getStateType() {
        return StateType;
    }
    public void setStateType(String aStateType) {
        StateType = aStateType;
    }
    public double getT1() {
        return T1;
    }
    public void setT1(double aT1) {
        T1 = aT1;
    }
    public void setT1(String aT1) {
        if (aT1 != null && !aT1.equals("")) {
            Double tDouble = new Double(aT1);
            double d = tDouble.doubleValue();
            T1 = d;
        }
    }

    public double getT2() {
        return T2;
    }
    public void setT2(double aT2) {
        T2 = aT2;
    }
    public void setT2(String aT2) {
        if (aT2 != null && !aT2.equals("")) {
            Double tDouble = new Double(aT2);
            double d = tDouble.doubleValue();
            T2 = d;
        }
    }

    public double getT3() {
        return T3;
    }
    public void setT3(double aT3) {
        T3 = aT3;
    }
    public void setT3(String aT3) {
        if (aT3 != null && !aT3.equals("")) {
            Double tDouble = new Double(aT3);
            double d = tDouble.doubleValue();
            T3 = d;
        }
    }

    public double getT4() {
        return T4;
    }
    public void setT4(double aT4) {
        T4 = aT4;
    }
    public void setT4(String aT4) {
        if (aT4 != null && !aT4.equals("")) {
            Double tDouble = new Double(aT4);
            double d = tDouble.doubleValue();
            T4 = d;
        }
    }

    public double getT5() {
        return T5;
    }
    public void setT5(double aT5) {
        T5 = aT5;
    }
    public void setT5(String aT5) {
        if (aT5 != null && !aT5.equals("")) {
            Double tDouble = new Double(aT5);
            double d = tDouble.doubleValue();
            T5 = d;
        }
    }

    public double getT6() {
        return T6;
    }
    public void setT6(double aT6) {
        T6 = aT6;
    }
    public void setT6(String aT6) {
        if (aT6 != null && !aT6.equals("")) {
            Double tDouble = new Double(aT6);
            double d = tDouble.doubleValue();
            T6 = d;
        }
    }

    public double getT7() {
        return T7;
    }
    public void setT7(double aT7) {
        T7 = aT7;
    }
    public void setT7(String aT7) {
        if (aT7 != null && !aT7.equals("")) {
            Double tDouble = new Double(aT7);
            double d = tDouble.doubleValue();
            T7 = d;
        }
    }

    public double getT8() {
        return T8;
    }
    public void setT8(double aT8) {
        T8 = aT8;
    }
    public void setT8(String aT8) {
        if (aT8 != null && !aT8.equals("")) {
            Double tDouble = new Double(aT8);
            double d = tDouble.doubleValue();
            T8 = d;
        }
    }

    public double getT9() {
        return T9;
    }
    public void setT9(double aT9) {
        T9 = aT9;
    }
    public void setT9(String aT9) {
        if (aT9 != null && !aT9.equals("")) {
            Double tDouble = new Double(aT9);
            double d = tDouble.doubleValue();
            T9 = d;
        }
    }

    public double getT10() {
        return T10;
    }
    public void setT10(double aT10) {
        T10 = aT10;
    }
    public void setT10(String aT10) {
        if (aT10 != null && !aT10.equals("")) {
            Double tDouble = new Double(aT10);
            double d = tDouble.doubleValue();
            T10 = d;
        }
    }

    public double getT11() {
        return T11;
    }
    public void setT11(double aT11) {
        T11 = aT11;
    }
    public void setT11(String aT11) {
        if (aT11 != null && !aT11.equals("")) {
            Double tDouble = new Double(aT11);
            double d = tDouble.doubleValue();
            T11 = d;
        }
    }

    public double getT12() {
        return T12;
    }
    public void setT12(double aT12) {
        T12 = aT12;
    }
    public void setT12(String aT12) {
        if (aT12 != null && !aT12.equals("")) {
            Double tDouble = new Double(aT12);
            double d = tDouble.doubleValue();
            T12 = d;
        }
    }

    public double getT13() {
        return T13;
    }
    public void setT13(double aT13) {
        T13 = aT13;
    }
    public void setT13(String aT13) {
        if (aT13 != null && !aT13.equals("")) {
            Double tDouble = new Double(aT13);
            double d = tDouble.doubleValue();
            T13 = d;
        }
    }

    public double getT14() {
        return T14;
    }
    public void setT14(double aT14) {
        T14 = aT14;
    }
    public void setT14(String aT14) {
        if (aT14 != null && !aT14.equals("")) {
            Double tDouble = new Double(aT14);
            double d = tDouble.doubleValue();
            T14 = d;
        }
    }

    public double getT15() {
        return T15;
    }
    public void setT15(double aT15) {
        T15 = aT15;
    }
    public void setT15(String aT15) {
        if (aT15 != null && !aT15.equals("")) {
            Double tDouble = new Double(aT15);
            double d = tDouble.doubleValue();
            T15 = d;
        }
    }

    public double getT16() {
        return T16;
    }
    public void setT16(double aT16) {
        T16 = aT16;
    }
    public void setT16(String aT16) {
        if (aT16 != null && !aT16.equals("")) {
            Double tDouble = new Double(aT16);
            double d = tDouble.doubleValue();
            T16 = d;
        }
    }

    public double getT17() {
        return T17;
    }
    public void setT17(double aT17) {
        T17 = aT17;
    }
    public void setT17(String aT17) {
        if (aT17 != null && !aT17.equals("")) {
            Double tDouble = new Double(aT17);
            double d = tDouble.doubleValue();
            T17 = d;
        }
    }

    public double getT18() {
        return T18;
    }
    public void setT18(double aT18) {
        T18 = aT18;
    }
    public void setT18(String aT18) {
        if (aT18 != null && !aT18.equals("")) {
            Double tDouble = new Double(aT18);
            double d = tDouble.doubleValue();
            T18 = d;
        }
    }

    public double getT19() {
        return T19;
    }
    public void setT19(double aT19) {
        T19 = aT19;
    }
    public void setT19(String aT19) {
        if (aT19 != null && !aT19.equals("")) {
            Double tDouble = new Double(aT19);
            double d = tDouble.doubleValue();
            T19 = d;
        }
    }

    public double getT20() {
        return T20;
    }
    public void setT20(double aT20) {
        T20 = aT20;
    }
    public void setT20(String aT20) {
        if (aT20 != null && !aT20.equals("")) {
            Double tDouble = new Double(aT20);
            double d = tDouble.doubleValue();
            T20 = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ObjectId") ) {
            return 0;
        }
        if( strFieldName.equals("StartDate") ) {
            return 1;
        }
        if( strFieldName.equals("EndDate") ) {
            return 2;
        }
        if( strFieldName.equals("BranchType") ) {
            return 3;
        }
        if( strFieldName.equals("StateType") ) {
            return 4;
        }
        if( strFieldName.equals("T1") ) {
            return 5;
        }
        if( strFieldName.equals("T2") ) {
            return 6;
        }
        if( strFieldName.equals("T3") ) {
            return 7;
        }
        if( strFieldName.equals("T4") ) {
            return 8;
        }
        if( strFieldName.equals("T5") ) {
            return 9;
        }
        if( strFieldName.equals("T6") ) {
            return 10;
        }
        if( strFieldName.equals("T7") ) {
            return 11;
        }
        if( strFieldName.equals("T8") ) {
            return 12;
        }
        if( strFieldName.equals("T9") ) {
            return 13;
        }
        if( strFieldName.equals("T10") ) {
            return 14;
        }
        if( strFieldName.equals("T11") ) {
            return 15;
        }
        if( strFieldName.equals("T12") ) {
            return 16;
        }
        if( strFieldName.equals("T13") ) {
            return 17;
        }
        if( strFieldName.equals("T14") ) {
            return 18;
        }
        if( strFieldName.equals("T15") ) {
            return 19;
        }
        if( strFieldName.equals("T16") ) {
            return 20;
        }
        if( strFieldName.equals("T17") ) {
            return 21;
        }
        if( strFieldName.equals("T18") ) {
            return 22;
        }
        if( strFieldName.equals("T19") ) {
            return 23;
        }
        if( strFieldName.equals("T20") ) {
            return 24;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ObjectId";
                break;
            case 1:
                strFieldName = "StartDate";
                break;
            case 2:
                strFieldName = "EndDate";
                break;
            case 3:
                strFieldName = "BranchType";
                break;
            case 4:
                strFieldName = "StateType";
                break;
            case 5:
                strFieldName = "T1";
                break;
            case 6:
                strFieldName = "T2";
                break;
            case 7:
                strFieldName = "T3";
                break;
            case 8:
                strFieldName = "T4";
                break;
            case 9:
                strFieldName = "T5";
                break;
            case 10:
                strFieldName = "T6";
                break;
            case 11:
                strFieldName = "T7";
                break;
            case 12:
                strFieldName = "T8";
                break;
            case 13:
                strFieldName = "T9";
                break;
            case 14:
                strFieldName = "T10";
                break;
            case 15:
                strFieldName = "T11";
                break;
            case 16:
                strFieldName = "T12";
                break;
            case 17:
                strFieldName = "T13";
                break;
            case 18:
                strFieldName = "T14";
                break;
            case 19:
                strFieldName = "T15";
                break;
            case 20:
                strFieldName = "T16";
                break;
            case 21:
                strFieldName = "T17";
                break;
            case 22:
                strFieldName = "T18";
                break;
            case 23:
                strFieldName = "T19";
                break;
            case 24:
                strFieldName = "T20";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "OBJECTID":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "STATETYPE":
                return Schema.TYPE_STRING;
            case "T1":
                return Schema.TYPE_DOUBLE;
            case "T2":
                return Schema.TYPE_DOUBLE;
            case "T3":
                return Schema.TYPE_DOUBLE;
            case "T4":
                return Schema.TYPE_DOUBLE;
            case "T5":
                return Schema.TYPE_DOUBLE;
            case "T6":
                return Schema.TYPE_DOUBLE;
            case "T7":
                return Schema.TYPE_DOUBLE;
            case "T8":
                return Schema.TYPE_DOUBLE;
            case "T9":
                return Schema.TYPE_DOUBLE;
            case "T10":
                return Schema.TYPE_DOUBLE;
            case "T11":
                return Schema.TYPE_DOUBLE;
            case "T12":
                return Schema.TYPE_DOUBLE;
            case "T13":
                return Schema.TYPE_DOUBLE;
            case "T14":
                return Schema.TYPE_DOUBLE;
            case "T15":
                return Schema.TYPE_DOUBLE;
            case "T16":
                return Schema.TYPE_DOUBLE;
            case "T17":
                return Schema.TYPE_DOUBLE;
            case "T18":
                return Schema.TYPE_DOUBLE;
            case "T19":
                return Schema.TYPE_DOUBLE;
            case "T20":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_DOUBLE;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_DOUBLE;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_DOUBLE;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ObjectId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectId));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateType));
        }
        if (FCode.equalsIgnoreCase("T1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T1));
        }
        if (FCode.equalsIgnoreCase("T2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T2));
        }
        if (FCode.equalsIgnoreCase("T3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T3));
        }
        if (FCode.equalsIgnoreCase("T4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T4));
        }
        if (FCode.equalsIgnoreCase("T5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T5));
        }
        if (FCode.equalsIgnoreCase("T6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T6));
        }
        if (FCode.equalsIgnoreCase("T7")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T7));
        }
        if (FCode.equalsIgnoreCase("T8")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T8));
        }
        if (FCode.equalsIgnoreCase("T9")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T9));
        }
        if (FCode.equalsIgnoreCase("T10")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T10));
        }
        if (FCode.equalsIgnoreCase("T11")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T11));
        }
        if (FCode.equalsIgnoreCase("T12")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T12));
        }
        if (FCode.equalsIgnoreCase("T13")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T13));
        }
        if (FCode.equalsIgnoreCase("T14")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T14));
        }
        if (FCode.equalsIgnoreCase("T15")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T15));
        }
        if (FCode.equalsIgnoreCase("T16")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T16));
        }
        if (FCode.equalsIgnoreCase("T17")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T17));
        }
        if (FCode.equalsIgnoreCase("T18")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T18));
        }
        if (FCode.equalsIgnoreCase("T19")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T19));
        }
        if (FCode.equalsIgnoreCase("T20")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T20));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ObjectId);
                break;
            case 1:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 2:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 3:
                strFieldValue = String.valueOf(BranchType);
                break;
            case 4:
                strFieldValue = String.valueOf(StateType);
                break;
            case 5:
                strFieldValue = String.valueOf(T1);
                break;
            case 6:
                strFieldValue = String.valueOf(T2);
                break;
            case 7:
                strFieldValue = String.valueOf(T3);
                break;
            case 8:
                strFieldValue = String.valueOf(T4);
                break;
            case 9:
                strFieldValue = String.valueOf(T5);
                break;
            case 10:
                strFieldValue = String.valueOf(T6);
                break;
            case 11:
                strFieldValue = String.valueOf(T7);
                break;
            case 12:
                strFieldValue = String.valueOf(T8);
                break;
            case 13:
                strFieldValue = String.valueOf(T9);
                break;
            case 14:
                strFieldValue = String.valueOf(T10);
                break;
            case 15:
                strFieldValue = String.valueOf(T11);
                break;
            case 16:
                strFieldValue = String.valueOf(T12);
                break;
            case 17:
                strFieldValue = String.valueOf(T13);
                break;
            case 18:
                strFieldValue = String.valueOf(T14);
                break;
            case 19:
                strFieldValue = String.valueOf(T15);
                break;
            case 20:
                strFieldValue = String.valueOf(T16);
                break;
            case 21:
                strFieldValue = String.valueOf(T17);
                break;
            case 22:
                strFieldValue = String.valueOf(T18);
                break;
            case 23:
                strFieldValue = String.valueOf(T19);
                break;
            case 24:
                strFieldValue = String.valueOf(T20);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ObjectId")) {
            if( FValue != null && !FValue.equals(""))
            {
                ObjectId = FValue.trim();
            }
            else
                ObjectId = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateType = FValue.trim();
            }
            else
                StateType = null;
        }
        if (FCode.equalsIgnoreCase("T1")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T1 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T2")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T2 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T3")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T3 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T4")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T4 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T5")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T5 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T6")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T6 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T7")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T7 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T8")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T8 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T9")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T9 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T10")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T10 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T11")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T11 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T12")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T12 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T13")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T13 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T14")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T14 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T15")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T15 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T16")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T16 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T17")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T17 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T18")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T18 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T19")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T19 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T20")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T20 = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LAStateDataPojo [" +
            "ObjectId="+ObjectId +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", BranchType="+BranchType +
            ", StateType="+StateType +
            ", T1="+T1 +
            ", T2="+T2 +
            ", T3="+T3 +
            ", T4="+T4 +
            ", T5="+T5 +
            ", T6="+T6 +
            ", T7="+T7 +
            ", T8="+T8 +
            ", T9="+T9 +
            ", T10="+T10 +
            ", T11="+T11 +
            ", T12="+T12 +
            ", T13="+T13 +
            ", T14="+T14 +
            ", T15="+T15 +
            ", T16="+T16 +
            ", T17="+T17 +
            ", T18="+T18 +
            ", T19="+T19 +
            ", T20="+T20 +"]";
    }
}
