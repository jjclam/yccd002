/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCPolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LCPolSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long PolID;
    /** Fk_lccont */
    private long ContID;
    /** Shardingid */
    private String ShardingID;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单险种号码 */
    private String GrpPolNo;
    /** 合同号码 */
    private String ContNo;
    /** 保单险种号码 */
    private String PolNo;
    /** 投保单险种号码 */
    private String ProposalNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 总单类型 */
    private String ContType;
    /** 保单类型标记 */
    private String PolTypeFlag;
    /** 主险保单号码 */
    private String MainPolNo;
    /** 主被保人保单号码 */
    private String MasterPolNo;
    /** 险类编码 */
    private String KindCode;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 联合代理人代码 */
    private String AgentCode1;
    /** 销售渠道 */
    private String SaleChnl;
    /** 经办人 */
    private String Handler;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 被保人性别 */
    private String InsuredSex;
    /** 被保人生日 */
    private Date InsuredBirthday;
    /** 被保人投保年龄 */
    private int InsuredAppAge;
    /** 被保人数目 */
    private int InsuredPeoples;
    /** 被保人职业类别/工种编码 */
    private String OccupationType;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 险种生效日期 */
    private Date CValiDate;
    /** 签单机构 */
    private String SignCom;
    /** 签单日期 */
    private Date SignDate;
    /** 签单时间 */
    private String SignTime;
    /** 首期交费日期 */
    private Date FirstPayDate;
    /** 终交日期 */
    private Date PayEndDate;
    /** 交至日期 */
    private Date PaytoDate;
    /** 起领日期 */
    private Date GetStartDate;
    /** 保险责任终止日期 */
    private Date EndDate;
    /** 意外责任终止日期 */
    private Date AcciEndDate;
    /** 领取年龄年期标志 */
    private String GetYearFlag;
    /** 领取年龄年期 */
    private int GetYear;
    /** 终交年龄年期标志 */
    private String PayEndYearFlag;
    /** 终交年龄年期 */
    private int PayEndYear;
    /** 保险年龄年期标志 */
    private String InsuYearFlag;
    /** 保险年龄年期 */
    private int InsuYear;
    /** 意外年龄年期标志 */
    private String AcciYearFlag;
    /** 意外年龄年期 */
    private int AcciYear;
    /** 起领日期计算类型 */
    private String GetStartType;
    /** 是否指定生效日期 */
    private String SpecifyValiDate;
    /** 交费方式 */
    private String PayMode;
    /** 交费位置 */
    private String PayLocation;
    /** 交费间隔 */
    private int PayIntv;
    /** 交费年期 */
    private int PayYears;
    /** 保险年期 */
    private int Years;
    /** 管理费比例 */
    private double ManageFeeRate;
    /** 浮动费率 */
    private double FloatRate;
    /** 保费算保额标志 */
    private String PremToAmnt;
    /** 总份数 */
    private double Mult;
    /** 总标准保费 */
    private double StandPrem;
    /** 总保费 */
    private double Prem;
    /** 总累计保费 */
    private double SumPrem;
    /** 总基本保额 */
    private double Amnt;
    /** 总风险保额 */
    private double RiskAmnt;
    /** 余额 */
    private double LeavingMoney;
    /** 批改次数 */
    private int EndorseTimes;
    /** 理赔次数 */
    private int ClaimTimes;
    /** 生存领取次数 */
    private int LiveTimes;
    /** 续保次数 */
    private int RenewCount;
    /** 最后一次给付日期 */
    private Date LastGetDate;
    /** 最后一次借款日期 */
    private Date LastLoanDate;
    /** 最后一次催收日期 */
    private Date LastRegetDate;
    /** 最后一次保全日期 */
    private Date LastEdorDate;
    /** 最近复效日期 */
    private Date LastRevDate;
    /** 续保标志 */
    private int RnewFlag;
    /** 停交标志 */
    private String StopFlag;
    /** 满期标志 */
    private String ExpiryFlag;
    /** 自动垫交标志 */
    private String AutoPayFlag;
    /** 利差返还方式 */
    private String InterestDifFlag;
    /** 减额交清标志 */
    private String SubFlag;
    /** 受益人标记 */
    private String BnfFlag;
    /** 是否体检件标志 */
    private String HealthCheckFlag;
    /** 告知标志 */
    private String ImpartFlag;
    /** 商业分保标记 */
    private String ReinsureFlag;
    /** 代收标志 */
    private String AgentPayFlag;
    /** 代付标志 */
    private String AgentGetFlag;
    /** 生存金领取方式 */
    private String LiveGetMode;
    /** 身故金领取方式 */
    private String DeadGetMode;
    /** 红利金领取方式 */
    private String BonusGetMode;
    /** 红利金领取人 */
    private String BonusMan;
    /** 被保人、投保人死亡标志 */
    private String DeadFlag;
    /** 是否吸烟标志 */
    private String SmokeFlag;
    /** 备注 */
    private String Remark;
    /** 复核状态 */
    private String ApproveFlag;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 核保状态 */
    private String UWFlag;
    /** 最终核保人编码 */
    private String UWCode;
    /** 核保完成日期 */
    private Date UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 投保单申请日期 */
    private Date PolApplyDate;
    /** 投保单/保单标志 */
    private String AppFlag;
    /** 其它保单状态 */
    private String PolState;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 等待期 */
    private int WaitPeriod;
    /** 领取形式 */
    private String GetForm;
    /** 领取银行编码 */
    private String GetBankCode;
    /** 领取银行账户 */
    private String GetBankAccNo;
    /** 领取银行户名 */
    private String GetAccName;
    /** 不丧失价值选择 */
    private String KeepValueOpt;
    /** 归属规则编码 */
    private String AscriptionRuleCode;
    /** 缴费规则编码 */
    private String PayRuleCode;
    /** 归属标记 */
    private String AscriptionFlag;
    /** 自动应用团体帐户标记 */
    private String AutoPubAccFlag;
    /** 组合标记 */
    private String CombiFlag;
    /** 投资规则编码 */
    private String InvestRuleCode;
    /** 投连账户生效日标志 */
    private String UintLinkValiFlag;
    /** 产品组合编码 */
    private String ProdSetCode;
    /** 是否通过风险测试 */
    private String InsurPolFlag;
    /** 临分标记 */
    private String NewReinsureFlag;
    /** 生存金是否累计生息标记 */
    private String LiveAccFlag;
    /** 团险标准费率保费 */
    private double StandRatePrem;
    /** 投保人firstname */
    private String AppntFirstName;
    /** 投保人lastname */
    private String AppntLastName;
    /** 被保人firstname */
    private String InsuredFirstName;
    /** 被保人lastname */
    private String InsuredLastName;
    /** 分期给付方式 */
    private String FQGetMode;
    /** 给付期间 */
    private String GetPeriod;
    /** 给付期间单位 */
    private String GetPeriodFlag;
    /** 递延期间 */
    private String DelayPeriod;
    /** 告知其他公司未成年人身故保额 */
    private double OtherAmnt;
    /** 关联客户关系 */
    private String Relation;
    /** 关联客户号 */
    private String ReCusNo;
    /** 自动续保标记1 */
    private String AutoRnewAge;
    /** 自动续保标记2 */
    private String AutoRnewYear;

    public static final int FIELDNUM = 146;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCPolSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PolID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCPolSchema cloned = (LCPolSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getPolID() {
        return PolID;
    }
    public void setPolID(long aPolID) {
        PolID = aPolID;
    }
    public void setPolID(String aPolID) {
        if (aPolID != null && !aPolID.equals("")) {
            PolID = new Long(aPolID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getGrpPolNo() {
        return GrpPolNo;
    }
    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getPolTypeFlag() {
        return PolTypeFlag;
    }
    public void setPolTypeFlag(String aPolTypeFlag) {
        PolTypeFlag = aPolTypeFlag;
    }
    public String getMainPolNo() {
        return MainPolNo;
    }
    public void setMainPolNo(String aMainPolNo) {
        MainPolNo = aMainPolNo;
    }
    public String getMasterPolNo() {
        return MasterPolNo;
    }
    public void setMasterPolNo(String aMasterPolNo) {
        MasterPolNo = aMasterPolNo;
    }
    public String getKindCode() {
        return KindCode;
    }
    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVersion() {
        return RiskVersion;
    }
    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getAgentType() {
        return AgentType;
    }
    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentCode1() {
        return AgentCode1;
    }
    public void setAgentCode1(String aAgentCode1) {
        AgentCode1 = aAgentCode1;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getHandler() {
        return Handler;
    }
    public void setHandler(String aHandler) {
        Handler = aHandler;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getInsuredSex() {
        return InsuredSex;
    }
    public void setInsuredSex(String aInsuredSex) {
        InsuredSex = aInsuredSex;
    }
    public String getInsuredBirthday() {
        if(InsuredBirthday != null) {
            return fDate.getString(InsuredBirthday);
        } else {
            return null;
        }
    }
    public void setInsuredBirthday(Date aInsuredBirthday) {
        InsuredBirthday = aInsuredBirthday;
    }
    public void setInsuredBirthday(String aInsuredBirthday) {
        if (aInsuredBirthday != null && !aInsuredBirthday.equals("")) {
            InsuredBirthday = fDate.getDate(aInsuredBirthday);
        } else
            InsuredBirthday = null;
    }

    public int getInsuredAppAge() {
        return InsuredAppAge;
    }
    public void setInsuredAppAge(int aInsuredAppAge) {
        InsuredAppAge = aInsuredAppAge;
    }
    public void setInsuredAppAge(String aInsuredAppAge) {
        if (aInsuredAppAge != null && !aInsuredAppAge.equals("")) {
            Integer tInteger = new Integer(aInsuredAppAge);
            int i = tInteger.intValue();
            InsuredAppAge = i;
        }
    }

    public int getInsuredPeoples() {
        return InsuredPeoples;
    }
    public void setInsuredPeoples(int aInsuredPeoples) {
        InsuredPeoples = aInsuredPeoples;
    }
    public void setInsuredPeoples(String aInsuredPeoples) {
        if (aInsuredPeoples != null && !aInsuredPeoples.equals("")) {
            Integer tInteger = new Integer(aInsuredPeoples);
            int i = tInteger.intValue();
            InsuredPeoples = i;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getCValiDate() {
        if(CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }
    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else
            CValiDate = null;
    }

    public String getSignCom() {
        return SignCom;
    }
    public void setSignCom(String aSignCom) {
        SignCom = aSignCom;
    }
    public String getSignDate() {
        if(SignDate != null) {
            return fDate.getString(SignDate);
        } else {
            return null;
        }
    }
    public void setSignDate(Date aSignDate) {
        SignDate = aSignDate;
    }
    public void setSignDate(String aSignDate) {
        if (aSignDate != null && !aSignDate.equals("")) {
            SignDate = fDate.getDate(aSignDate);
        } else
            SignDate = null;
    }

    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getFirstPayDate() {
        if(FirstPayDate != null) {
            return fDate.getString(FirstPayDate);
        } else {
            return null;
        }
    }
    public void setFirstPayDate(Date aFirstPayDate) {
        FirstPayDate = aFirstPayDate;
    }
    public void setFirstPayDate(String aFirstPayDate) {
        if (aFirstPayDate != null && !aFirstPayDate.equals("")) {
            FirstPayDate = fDate.getDate(aFirstPayDate);
        } else
            FirstPayDate = null;
    }

    public String getPayEndDate() {
        if(PayEndDate != null) {
            return fDate.getString(PayEndDate);
        } else {
            return null;
        }
    }
    public void setPayEndDate(Date aPayEndDate) {
        PayEndDate = aPayEndDate;
    }
    public void setPayEndDate(String aPayEndDate) {
        if (aPayEndDate != null && !aPayEndDate.equals("")) {
            PayEndDate = fDate.getDate(aPayEndDate);
        } else
            PayEndDate = null;
    }

    public String getPaytoDate() {
        if(PaytoDate != null) {
            return fDate.getString(PaytoDate);
        } else {
            return null;
        }
    }
    public void setPaytoDate(Date aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        if (aPaytoDate != null && !aPaytoDate.equals("")) {
            PaytoDate = fDate.getDate(aPaytoDate);
        } else
            PaytoDate = null;
    }

    public String getGetStartDate() {
        if(GetStartDate != null) {
            return fDate.getString(GetStartDate);
        } else {
            return null;
        }
    }
    public void setGetStartDate(Date aGetStartDate) {
        GetStartDate = aGetStartDate;
    }
    public void setGetStartDate(String aGetStartDate) {
        if (aGetStartDate != null && !aGetStartDate.equals("")) {
            GetStartDate = fDate.getDate(aGetStartDate);
        } else
            GetStartDate = null;
    }

    public String getEndDate() {
        if(EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }
    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }
    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else
            EndDate = null;
    }

    public String getAcciEndDate() {
        if(AcciEndDate != null) {
            return fDate.getString(AcciEndDate);
        } else {
            return null;
        }
    }
    public void setAcciEndDate(Date aAcciEndDate) {
        AcciEndDate = aAcciEndDate;
    }
    public void setAcciEndDate(String aAcciEndDate) {
        if (aAcciEndDate != null && !aAcciEndDate.equals("")) {
            AcciEndDate = fDate.getDate(aAcciEndDate);
        } else
            AcciEndDate = null;
    }

    public String getGetYearFlag() {
        return GetYearFlag;
    }
    public void setGetYearFlag(String aGetYearFlag) {
        GetYearFlag = aGetYearFlag;
    }
    public int getGetYear() {
        return GetYear;
    }
    public void setGetYear(int aGetYear) {
        GetYear = aGetYear;
    }
    public void setGetYear(String aGetYear) {
        if (aGetYear != null && !aGetYear.equals("")) {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }
    public void setPayEndYearFlag(String aPayEndYearFlag) {
        PayEndYearFlag = aPayEndYearFlag;
    }
    public int getPayEndYear() {
        return PayEndYear;
    }
    public void setPayEndYear(int aPayEndYear) {
        PayEndYear = aPayEndYear;
    }
    public void setPayEndYear(String aPayEndYear) {
        if (aPayEndYear != null && !aPayEndYear.equals("")) {
            Integer tInteger = new Integer(aPayEndYear);
            int i = tInteger.intValue();
            PayEndYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getAcciYearFlag() {
        return AcciYearFlag;
    }
    public void setAcciYearFlag(String aAcciYearFlag) {
        AcciYearFlag = aAcciYearFlag;
    }
    public int getAcciYear() {
        return AcciYear;
    }
    public void setAcciYear(int aAcciYear) {
        AcciYear = aAcciYear;
    }
    public void setAcciYear(String aAcciYear) {
        if (aAcciYear != null && !aAcciYear.equals("")) {
            Integer tInteger = new Integer(aAcciYear);
            int i = tInteger.intValue();
            AcciYear = i;
        }
    }

    public String getGetStartType() {
        return GetStartType;
    }
    public void setGetStartType(String aGetStartType) {
        GetStartType = aGetStartType;
    }
    public String getSpecifyValiDate() {
        return SpecifyValiDate;
    }
    public void setSpecifyValiDate(String aSpecifyValiDate) {
        SpecifyValiDate = aSpecifyValiDate;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getPayLocation() {
        return PayLocation;
    }
    public void setPayLocation(String aPayLocation) {
        PayLocation = aPayLocation;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public int getPayYears() {
        return PayYears;
    }
    public void setPayYears(int aPayYears) {
        PayYears = aPayYears;
    }
    public void setPayYears(String aPayYears) {
        if (aPayYears != null && !aPayYears.equals("")) {
            Integer tInteger = new Integer(aPayYears);
            int i = tInteger.intValue();
            PayYears = i;
        }
    }

    public int getYears() {
        return Years;
    }
    public void setYears(int aYears) {
        Years = aYears;
    }
    public void setYears(String aYears) {
        if (aYears != null && !aYears.equals("")) {
            Integer tInteger = new Integer(aYears);
            int i = tInteger.intValue();
            Years = i;
        }
    }

    public double getManageFeeRate() {
        return ManageFeeRate;
    }
    public void setManageFeeRate(double aManageFeeRate) {
        ManageFeeRate = aManageFeeRate;
    }
    public void setManageFeeRate(String aManageFeeRate) {
        if (aManageFeeRate != null && !aManageFeeRate.equals("")) {
            Double tDouble = new Double(aManageFeeRate);
            double d = tDouble.doubleValue();
            ManageFeeRate = d;
        }
    }

    public double getFloatRate() {
        return FloatRate;
    }
    public void setFloatRate(double aFloatRate) {
        FloatRate = aFloatRate;
    }
    public void setFloatRate(String aFloatRate) {
        if (aFloatRate != null && !aFloatRate.equals("")) {
            Double tDouble = new Double(aFloatRate);
            double d = tDouble.doubleValue();
            FloatRate = d;
        }
    }

    public String getPremToAmnt() {
        return PremToAmnt;
    }
    public void setPremToAmnt(String aPremToAmnt) {
        PremToAmnt = aPremToAmnt;
    }
    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getStandPrem() {
        return StandPrem;
    }
    public void setStandPrem(double aStandPrem) {
        StandPrem = aStandPrem;
    }
    public void setStandPrem(String aStandPrem) {
        if (aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getRiskAmnt() {
        return RiskAmnt;
    }
    public void setRiskAmnt(double aRiskAmnt) {
        RiskAmnt = aRiskAmnt;
    }
    public void setRiskAmnt(String aRiskAmnt) {
        if (aRiskAmnt != null && !aRiskAmnt.equals("")) {
            Double tDouble = new Double(aRiskAmnt);
            double d = tDouble.doubleValue();
            RiskAmnt = d;
        }
    }

    public double getLeavingMoney() {
        return LeavingMoney;
    }
    public void setLeavingMoney(double aLeavingMoney) {
        LeavingMoney = aLeavingMoney;
    }
    public void setLeavingMoney(String aLeavingMoney) {
        if (aLeavingMoney != null && !aLeavingMoney.equals("")) {
            Double tDouble = new Double(aLeavingMoney);
            double d = tDouble.doubleValue();
            LeavingMoney = d;
        }
    }

    public int getEndorseTimes() {
        return EndorseTimes;
    }
    public void setEndorseTimes(int aEndorseTimes) {
        EndorseTimes = aEndorseTimes;
    }
    public void setEndorseTimes(String aEndorseTimes) {
        if (aEndorseTimes != null && !aEndorseTimes.equals("")) {
            Integer tInteger = new Integer(aEndorseTimes);
            int i = tInteger.intValue();
            EndorseTimes = i;
        }
    }

    public int getClaimTimes() {
        return ClaimTimes;
    }
    public void setClaimTimes(int aClaimTimes) {
        ClaimTimes = aClaimTimes;
    }
    public void setClaimTimes(String aClaimTimes) {
        if (aClaimTimes != null && !aClaimTimes.equals("")) {
            Integer tInteger = new Integer(aClaimTimes);
            int i = tInteger.intValue();
            ClaimTimes = i;
        }
    }

    public int getLiveTimes() {
        return LiveTimes;
    }
    public void setLiveTimes(int aLiveTimes) {
        LiveTimes = aLiveTimes;
    }
    public void setLiveTimes(String aLiveTimes) {
        if (aLiveTimes != null && !aLiveTimes.equals("")) {
            Integer tInteger = new Integer(aLiveTimes);
            int i = tInteger.intValue();
            LiveTimes = i;
        }
    }

    public int getRenewCount() {
        return RenewCount;
    }
    public void setRenewCount(int aRenewCount) {
        RenewCount = aRenewCount;
    }
    public void setRenewCount(String aRenewCount) {
        if (aRenewCount != null && !aRenewCount.equals("")) {
            Integer tInteger = new Integer(aRenewCount);
            int i = tInteger.intValue();
            RenewCount = i;
        }
    }

    public String getLastGetDate() {
        if(LastGetDate != null) {
            return fDate.getString(LastGetDate);
        } else {
            return null;
        }
    }
    public void setLastGetDate(Date aLastGetDate) {
        LastGetDate = aLastGetDate;
    }
    public void setLastGetDate(String aLastGetDate) {
        if (aLastGetDate != null && !aLastGetDate.equals("")) {
            LastGetDate = fDate.getDate(aLastGetDate);
        } else
            LastGetDate = null;
    }

    public String getLastLoanDate() {
        if(LastLoanDate != null) {
            return fDate.getString(LastLoanDate);
        } else {
            return null;
        }
    }
    public void setLastLoanDate(Date aLastLoanDate) {
        LastLoanDate = aLastLoanDate;
    }
    public void setLastLoanDate(String aLastLoanDate) {
        if (aLastLoanDate != null && !aLastLoanDate.equals("")) {
            LastLoanDate = fDate.getDate(aLastLoanDate);
        } else
            LastLoanDate = null;
    }

    public String getLastRegetDate() {
        if(LastRegetDate != null) {
            return fDate.getString(LastRegetDate);
        } else {
            return null;
        }
    }
    public void setLastRegetDate(Date aLastRegetDate) {
        LastRegetDate = aLastRegetDate;
    }
    public void setLastRegetDate(String aLastRegetDate) {
        if (aLastRegetDate != null && !aLastRegetDate.equals("")) {
            LastRegetDate = fDate.getDate(aLastRegetDate);
        } else
            LastRegetDate = null;
    }

    public String getLastEdorDate() {
        if(LastEdorDate != null) {
            return fDate.getString(LastEdorDate);
        } else {
            return null;
        }
    }
    public void setLastEdorDate(Date aLastEdorDate) {
        LastEdorDate = aLastEdorDate;
    }
    public void setLastEdorDate(String aLastEdorDate) {
        if (aLastEdorDate != null && !aLastEdorDate.equals("")) {
            LastEdorDate = fDate.getDate(aLastEdorDate);
        } else
            LastEdorDate = null;
    }

    public String getLastRevDate() {
        if(LastRevDate != null) {
            return fDate.getString(LastRevDate);
        } else {
            return null;
        }
    }
    public void setLastRevDate(Date aLastRevDate) {
        LastRevDate = aLastRevDate;
    }
    public void setLastRevDate(String aLastRevDate) {
        if (aLastRevDate != null && !aLastRevDate.equals("")) {
            LastRevDate = fDate.getDate(aLastRevDate);
        } else
            LastRevDate = null;
    }

    public int getRnewFlag() {
        return RnewFlag;
    }
    public void setRnewFlag(int aRnewFlag) {
        RnewFlag = aRnewFlag;
    }
    public void setRnewFlag(String aRnewFlag) {
        if (aRnewFlag != null && !aRnewFlag.equals("")) {
            Integer tInteger = new Integer(aRnewFlag);
            int i = tInteger.intValue();
            RnewFlag = i;
        }
    }

    public String getStopFlag() {
        return StopFlag;
    }
    public void setStopFlag(String aStopFlag) {
        StopFlag = aStopFlag;
    }
    public String getExpiryFlag() {
        return ExpiryFlag;
    }
    public void setExpiryFlag(String aExpiryFlag) {
        ExpiryFlag = aExpiryFlag;
    }
    public String getAutoPayFlag() {
        return AutoPayFlag;
    }
    public void setAutoPayFlag(String aAutoPayFlag) {
        AutoPayFlag = aAutoPayFlag;
    }
    public String getInterestDifFlag() {
        return InterestDifFlag;
    }
    public void setInterestDifFlag(String aInterestDifFlag) {
        InterestDifFlag = aInterestDifFlag;
    }
    public String getSubFlag() {
        return SubFlag;
    }
    public void setSubFlag(String aSubFlag) {
        SubFlag = aSubFlag;
    }
    public String getBnfFlag() {
        return BnfFlag;
    }
    public void setBnfFlag(String aBnfFlag) {
        BnfFlag = aBnfFlag;
    }
    public String getHealthCheckFlag() {
        return HealthCheckFlag;
    }
    public void setHealthCheckFlag(String aHealthCheckFlag) {
        HealthCheckFlag = aHealthCheckFlag;
    }
    public String getImpartFlag() {
        return ImpartFlag;
    }
    public void setImpartFlag(String aImpartFlag) {
        ImpartFlag = aImpartFlag;
    }
    public String getReinsureFlag() {
        return ReinsureFlag;
    }
    public void setReinsureFlag(String aReinsureFlag) {
        ReinsureFlag = aReinsureFlag;
    }
    public String getAgentPayFlag() {
        return AgentPayFlag;
    }
    public void setAgentPayFlag(String aAgentPayFlag) {
        AgentPayFlag = aAgentPayFlag;
    }
    public String getAgentGetFlag() {
        return AgentGetFlag;
    }
    public void setAgentGetFlag(String aAgentGetFlag) {
        AgentGetFlag = aAgentGetFlag;
    }
    public String getLiveGetMode() {
        return LiveGetMode;
    }
    public void setLiveGetMode(String aLiveGetMode) {
        LiveGetMode = aLiveGetMode;
    }
    public String getDeadGetMode() {
        return DeadGetMode;
    }
    public void setDeadGetMode(String aDeadGetMode) {
        DeadGetMode = aDeadGetMode;
    }
    public String getBonusGetMode() {
        return BonusGetMode;
    }
    public void setBonusGetMode(String aBonusGetMode) {
        BonusGetMode = aBonusGetMode;
    }
    public String getBonusMan() {
        return BonusMan;
    }
    public void setBonusMan(String aBonusMan) {
        BonusMan = aBonusMan;
    }
    public String getDeadFlag() {
        return DeadFlag;
    }
    public void setDeadFlag(String aDeadFlag) {
        DeadFlag = aDeadFlag;
    }
    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWCode() {
        return UWCode;
    }
    public void setUWCode(String aUWCode) {
        UWCode = aUWCode;
    }
    public String getUWDate() {
        if(UWDate != null) {
            return fDate.getString(UWDate);
        } else {
            return null;
        }
    }
    public void setUWDate(Date aUWDate) {
        UWDate = aUWDate;
    }
    public void setUWDate(String aUWDate) {
        if (aUWDate != null && !aUWDate.equals("")) {
            UWDate = fDate.getDate(aUWDate);
        } else
            UWDate = null;
    }

    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getPolApplyDate() {
        if(PolApplyDate != null) {
            return fDate.getString(PolApplyDate);
        } else {
            return null;
        }
    }
    public void setPolApplyDate(Date aPolApplyDate) {
        PolApplyDate = aPolApplyDate;
    }
    public void setPolApplyDate(String aPolApplyDate) {
        if (aPolApplyDate != null && !aPolApplyDate.equals("")) {
            PolApplyDate = fDate.getDate(aPolApplyDate);
        } else
            PolApplyDate = null;
    }

    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getPolState() {
        return PolState;
    }
    public void setPolState(String aPolState) {
        PolState = aPolState;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public int getWaitPeriod() {
        return WaitPeriod;
    }
    public void setWaitPeriod(int aWaitPeriod) {
        WaitPeriod = aWaitPeriod;
    }
    public void setWaitPeriod(String aWaitPeriod) {
        if (aWaitPeriod != null && !aWaitPeriod.equals("")) {
            Integer tInteger = new Integer(aWaitPeriod);
            int i = tInteger.intValue();
            WaitPeriod = i;
        }
    }

    public String getGetForm() {
        return GetForm;
    }
    public void setGetForm(String aGetForm) {
        GetForm = aGetForm;
    }
    public String getGetBankCode() {
        return GetBankCode;
    }
    public void setGetBankCode(String aGetBankCode) {
        GetBankCode = aGetBankCode;
    }
    public String getGetBankAccNo() {
        return GetBankAccNo;
    }
    public void setGetBankAccNo(String aGetBankAccNo) {
        GetBankAccNo = aGetBankAccNo;
    }
    public String getGetAccName() {
        return GetAccName;
    }
    public void setGetAccName(String aGetAccName) {
        GetAccName = aGetAccName;
    }
    public String getKeepValueOpt() {
        return KeepValueOpt;
    }
    public void setKeepValueOpt(String aKeepValueOpt) {
        KeepValueOpt = aKeepValueOpt;
    }
    public String getAscriptionRuleCode() {
        return AscriptionRuleCode;
    }
    public void setAscriptionRuleCode(String aAscriptionRuleCode) {
        AscriptionRuleCode = aAscriptionRuleCode;
    }
    public String getPayRuleCode() {
        return PayRuleCode;
    }
    public void setPayRuleCode(String aPayRuleCode) {
        PayRuleCode = aPayRuleCode;
    }
    public String getAscriptionFlag() {
        return AscriptionFlag;
    }
    public void setAscriptionFlag(String aAscriptionFlag) {
        AscriptionFlag = aAscriptionFlag;
    }
    public String getAutoPubAccFlag() {
        return AutoPubAccFlag;
    }
    public void setAutoPubAccFlag(String aAutoPubAccFlag) {
        AutoPubAccFlag = aAutoPubAccFlag;
    }
    public String getCombiFlag() {
        return CombiFlag;
    }
    public void setCombiFlag(String aCombiFlag) {
        CombiFlag = aCombiFlag;
    }
    public String getInvestRuleCode() {
        return InvestRuleCode;
    }
    public void setInvestRuleCode(String aInvestRuleCode) {
        InvestRuleCode = aInvestRuleCode;
    }
    public String getUintLinkValiFlag() {
        return UintLinkValiFlag;
    }
    public void setUintLinkValiFlag(String aUintLinkValiFlag) {
        UintLinkValiFlag = aUintLinkValiFlag;
    }
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getInsurPolFlag() {
        return InsurPolFlag;
    }
    public void setInsurPolFlag(String aInsurPolFlag) {
        InsurPolFlag = aInsurPolFlag;
    }
    public String getNewReinsureFlag() {
        return NewReinsureFlag;
    }
    public void setNewReinsureFlag(String aNewReinsureFlag) {
        NewReinsureFlag = aNewReinsureFlag;
    }
    public String getLiveAccFlag() {
        return LiveAccFlag;
    }
    public void setLiveAccFlag(String aLiveAccFlag) {
        LiveAccFlag = aLiveAccFlag;
    }
    public double getStandRatePrem() {
        return StandRatePrem;
    }
    public void setStandRatePrem(double aStandRatePrem) {
        StandRatePrem = aStandRatePrem;
    }
    public void setStandRatePrem(String aStandRatePrem) {
        if (aStandRatePrem != null && !aStandRatePrem.equals("")) {
            Double tDouble = new Double(aStandRatePrem);
            double d = tDouble.doubleValue();
            StandRatePrem = d;
        }
    }

    public String getAppntFirstName() {
        return AppntFirstName;
    }
    public void setAppntFirstName(String aAppntFirstName) {
        AppntFirstName = aAppntFirstName;
    }
    public String getAppntLastName() {
        return AppntLastName;
    }
    public void setAppntLastName(String aAppntLastName) {
        AppntLastName = aAppntLastName;
    }
    public String getInsuredFirstName() {
        return InsuredFirstName;
    }
    public void setInsuredFirstName(String aInsuredFirstName) {
        InsuredFirstName = aInsuredFirstName;
    }
    public String getInsuredLastName() {
        return InsuredLastName;
    }
    public void setInsuredLastName(String aInsuredLastName) {
        InsuredLastName = aInsuredLastName;
    }
    public String getFQGetMode() {
        return FQGetMode;
    }
    public void setFQGetMode(String aFQGetMode) {
        FQGetMode = aFQGetMode;
    }
    public String getGetPeriod() {
        return GetPeriod;
    }
    public void setGetPeriod(String aGetPeriod) {
        GetPeriod = aGetPeriod;
    }
    public String getGetPeriodFlag() {
        return GetPeriodFlag;
    }
    public void setGetPeriodFlag(String aGetPeriodFlag) {
        GetPeriodFlag = aGetPeriodFlag;
    }
    public String getDelayPeriod() {
        return DelayPeriod;
    }
    public void setDelayPeriod(String aDelayPeriod) {
        DelayPeriod = aDelayPeriod;
    }
    public double getOtherAmnt() {
        return OtherAmnt;
    }
    public void setOtherAmnt(double aOtherAmnt) {
        OtherAmnt = aOtherAmnt;
    }
    public void setOtherAmnt(String aOtherAmnt) {
        if (aOtherAmnt != null && !aOtherAmnt.equals("")) {
            Double tDouble = new Double(aOtherAmnt);
            double d = tDouble.doubleValue();
            OtherAmnt = d;
        }
    }

    public String getRelation() {
        return Relation;
    }
    public void setRelation(String aRelation) {
        Relation = aRelation;
    }
    public String getReCusNo() {
        return ReCusNo;
    }
    public void setReCusNo(String aReCusNo) {
        ReCusNo = aReCusNo;
    }
    public String getAutoRnewAge() {
        return AutoRnewAge;
    }
    public void setAutoRnewAge(String aAutoRnewAge) {
        AutoRnewAge = aAutoRnewAge;
    }
    public String getAutoRnewYear() {
        return AutoRnewYear;
    }
    public void setAutoRnewYear(String aAutoRnewYear) {
        AutoRnewYear = aAutoRnewYear;
    }

    /**
    * 使用另外一个 LCPolSchema 对象给 Schema 赋值
    * @param: aLCPolSchema LCPolSchema
    **/
    public void setSchema(LCPolSchema aLCPolSchema) {
        this.PolID = aLCPolSchema.getPolID();
        this.ContID = aLCPolSchema.getContID();
        this.ShardingID = aLCPolSchema.getShardingID();
        this.GrpContNo = aLCPolSchema.getGrpContNo();
        this.GrpPolNo = aLCPolSchema.getGrpPolNo();
        this.ContNo = aLCPolSchema.getContNo();
        this.PolNo = aLCPolSchema.getPolNo();
        this.ProposalNo = aLCPolSchema.getProposalNo();
        this.PrtNo = aLCPolSchema.getPrtNo();
        this.ContType = aLCPolSchema.getContType();
        this.PolTypeFlag = aLCPolSchema.getPolTypeFlag();
        this.MainPolNo = aLCPolSchema.getMainPolNo();
        this.MasterPolNo = aLCPolSchema.getMasterPolNo();
        this.KindCode = aLCPolSchema.getKindCode();
        this.RiskCode = aLCPolSchema.getRiskCode();
        this.RiskVersion = aLCPolSchema.getRiskVersion();
        this.ManageCom = aLCPolSchema.getManageCom();
        this.AgentCom = aLCPolSchema.getAgentCom();
        this.AgentType = aLCPolSchema.getAgentType();
        this.AgentCode = aLCPolSchema.getAgentCode();
        this.AgentGroup = aLCPolSchema.getAgentGroup();
        this.AgentCode1 = aLCPolSchema.getAgentCode1();
        this.SaleChnl = aLCPolSchema.getSaleChnl();
        this.Handler = aLCPolSchema.getHandler();
        this.InsuredNo = aLCPolSchema.getInsuredNo();
        this.InsuredName = aLCPolSchema.getInsuredName();
        this.InsuredSex = aLCPolSchema.getInsuredSex();
        this.InsuredBirthday = fDate.getDate( aLCPolSchema.getInsuredBirthday());
        this.InsuredAppAge = aLCPolSchema.getInsuredAppAge();
        this.InsuredPeoples = aLCPolSchema.getInsuredPeoples();
        this.OccupationType = aLCPolSchema.getOccupationType();
        this.AppntNo = aLCPolSchema.getAppntNo();
        this.AppntName = aLCPolSchema.getAppntName();
        this.CValiDate = fDate.getDate( aLCPolSchema.getCValiDate());
        this.SignCom = aLCPolSchema.getSignCom();
        this.SignDate = fDate.getDate( aLCPolSchema.getSignDate());
        this.SignTime = aLCPolSchema.getSignTime();
        this.FirstPayDate = fDate.getDate( aLCPolSchema.getFirstPayDate());
        this.PayEndDate = fDate.getDate( aLCPolSchema.getPayEndDate());
        this.PaytoDate = fDate.getDate( aLCPolSchema.getPaytoDate());
        this.GetStartDate = fDate.getDate( aLCPolSchema.getGetStartDate());
        this.EndDate = fDate.getDate( aLCPolSchema.getEndDate());
        this.AcciEndDate = fDate.getDate( aLCPolSchema.getAcciEndDate());
        this.GetYearFlag = aLCPolSchema.getGetYearFlag();
        this.GetYear = aLCPolSchema.getGetYear();
        this.PayEndYearFlag = aLCPolSchema.getPayEndYearFlag();
        this.PayEndYear = aLCPolSchema.getPayEndYear();
        this.InsuYearFlag = aLCPolSchema.getInsuYearFlag();
        this.InsuYear = aLCPolSchema.getInsuYear();
        this.AcciYearFlag = aLCPolSchema.getAcciYearFlag();
        this.AcciYear = aLCPolSchema.getAcciYear();
        this.GetStartType = aLCPolSchema.getGetStartType();
        this.SpecifyValiDate = aLCPolSchema.getSpecifyValiDate();
        this.PayMode = aLCPolSchema.getPayMode();
        this.PayLocation = aLCPolSchema.getPayLocation();
        this.PayIntv = aLCPolSchema.getPayIntv();
        this.PayYears = aLCPolSchema.getPayYears();
        this.Years = aLCPolSchema.getYears();
        this.ManageFeeRate = aLCPolSchema.getManageFeeRate();
        this.FloatRate = aLCPolSchema.getFloatRate();
        this.PremToAmnt = aLCPolSchema.getPremToAmnt();
        this.Mult = aLCPolSchema.getMult();
        this.StandPrem = aLCPolSchema.getStandPrem();
        this.Prem = aLCPolSchema.getPrem();
        this.SumPrem = aLCPolSchema.getSumPrem();
        this.Amnt = aLCPolSchema.getAmnt();
        this.RiskAmnt = aLCPolSchema.getRiskAmnt();
        this.LeavingMoney = aLCPolSchema.getLeavingMoney();
        this.EndorseTimes = aLCPolSchema.getEndorseTimes();
        this.ClaimTimes = aLCPolSchema.getClaimTimes();
        this.LiveTimes = aLCPolSchema.getLiveTimes();
        this.RenewCount = aLCPolSchema.getRenewCount();
        this.LastGetDate = fDate.getDate( aLCPolSchema.getLastGetDate());
        this.LastLoanDate = fDate.getDate( aLCPolSchema.getLastLoanDate());
        this.LastRegetDate = fDate.getDate( aLCPolSchema.getLastRegetDate());
        this.LastEdorDate = fDate.getDate( aLCPolSchema.getLastEdorDate());
        this.LastRevDate = fDate.getDate( aLCPolSchema.getLastRevDate());
        this.RnewFlag = aLCPolSchema.getRnewFlag();
        this.StopFlag = aLCPolSchema.getStopFlag();
        this.ExpiryFlag = aLCPolSchema.getExpiryFlag();
        this.AutoPayFlag = aLCPolSchema.getAutoPayFlag();
        this.InterestDifFlag = aLCPolSchema.getInterestDifFlag();
        this.SubFlag = aLCPolSchema.getSubFlag();
        this.BnfFlag = aLCPolSchema.getBnfFlag();
        this.HealthCheckFlag = aLCPolSchema.getHealthCheckFlag();
        this.ImpartFlag = aLCPolSchema.getImpartFlag();
        this.ReinsureFlag = aLCPolSchema.getReinsureFlag();
        this.AgentPayFlag = aLCPolSchema.getAgentPayFlag();
        this.AgentGetFlag = aLCPolSchema.getAgentGetFlag();
        this.LiveGetMode = aLCPolSchema.getLiveGetMode();
        this.DeadGetMode = aLCPolSchema.getDeadGetMode();
        this.BonusGetMode = aLCPolSchema.getBonusGetMode();
        this.BonusMan = aLCPolSchema.getBonusMan();
        this.DeadFlag = aLCPolSchema.getDeadFlag();
        this.SmokeFlag = aLCPolSchema.getSmokeFlag();
        this.Remark = aLCPolSchema.getRemark();
        this.ApproveFlag = aLCPolSchema.getApproveFlag();
        this.ApproveCode = aLCPolSchema.getApproveCode();
        this.ApproveDate = fDate.getDate( aLCPolSchema.getApproveDate());
        this.ApproveTime = aLCPolSchema.getApproveTime();
        this.UWFlag = aLCPolSchema.getUWFlag();
        this.UWCode = aLCPolSchema.getUWCode();
        this.UWDate = fDate.getDate( aLCPolSchema.getUWDate());
        this.UWTime = aLCPolSchema.getUWTime();
        this.PolApplyDate = fDate.getDate( aLCPolSchema.getPolApplyDate());
        this.AppFlag = aLCPolSchema.getAppFlag();
        this.PolState = aLCPolSchema.getPolState();
        this.StandbyFlag1 = aLCPolSchema.getStandbyFlag1();
        this.StandbyFlag2 = aLCPolSchema.getStandbyFlag2();
        this.StandbyFlag3 = aLCPolSchema.getStandbyFlag3();
        this.Operator = aLCPolSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCPolSchema.getMakeDate());
        this.MakeTime = aLCPolSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCPolSchema.getModifyDate());
        this.ModifyTime = aLCPolSchema.getModifyTime();
        this.WaitPeriod = aLCPolSchema.getWaitPeriod();
        this.GetForm = aLCPolSchema.getGetForm();
        this.GetBankCode = aLCPolSchema.getGetBankCode();
        this.GetBankAccNo = aLCPolSchema.getGetBankAccNo();
        this.GetAccName = aLCPolSchema.getGetAccName();
        this.KeepValueOpt = aLCPolSchema.getKeepValueOpt();
        this.AscriptionRuleCode = aLCPolSchema.getAscriptionRuleCode();
        this.PayRuleCode = aLCPolSchema.getPayRuleCode();
        this.AscriptionFlag = aLCPolSchema.getAscriptionFlag();
        this.AutoPubAccFlag = aLCPolSchema.getAutoPubAccFlag();
        this.CombiFlag = aLCPolSchema.getCombiFlag();
        this.InvestRuleCode = aLCPolSchema.getInvestRuleCode();
        this.UintLinkValiFlag = aLCPolSchema.getUintLinkValiFlag();
        this.ProdSetCode = aLCPolSchema.getProdSetCode();
        this.InsurPolFlag = aLCPolSchema.getInsurPolFlag();
        this.NewReinsureFlag = aLCPolSchema.getNewReinsureFlag();
        this.LiveAccFlag = aLCPolSchema.getLiveAccFlag();
        this.StandRatePrem = aLCPolSchema.getStandRatePrem();
        this.AppntFirstName = aLCPolSchema.getAppntFirstName();
        this.AppntLastName = aLCPolSchema.getAppntLastName();
        this.InsuredFirstName = aLCPolSchema.getInsuredFirstName();
        this.InsuredLastName = aLCPolSchema.getInsuredLastName();
        this.FQGetMode = aLCPolSchema.getFQGetMode();
        this.GetPeriod = aLCPolSchema.getGetPeriod();
        this.GetPeriodFlag = aLCPolSchema.getGetPeriodFlag();
        this.DelayPeriod = aLCPolSchema.getDelayPeriod();
        this.OtherAmnt = aLCPolSchema.getOtherAmnt();
        this.Relation = aLCPolSchema.getRelation();
        this.ReCusNo = aLCPolSchema.getReCusNo();
        this.AutoRnewAge = aLCPolSchema.getAutoRnewAge();
        this.AutoRnewYear = aLCPolSchema.getAutoRnewYear();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.PolID = rs.getLong("PolID");
            this.ContID = rs.getLong("ContID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("GrpPolNo") == null )
                this.GrpPolNo = null;
            else
                this.GrpPolNo = rs.getString("GrpPolNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("ProposalNo") == null )
                this.ProposalNo = null;
            else
                this.ProposalNo = rs.getString("ProposalNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("ContType") == null )
                this.ContType = null;
            else
                this.ContType = rs.getString("ContType").trim();

            if( rs.getString("PolTypeFlag") == null )
                this.PolTypeFlag = null;
            else
                this.PolTypeFlag = rs.getString("PolTypeFlag").trim();

            if( rs.getString("MainPolNo") == null )
                this.MainPolNo = null;
            else
                this.MainPolNo = rs.getString("MainPolNo").trim();

            if( rs.getString("MasterPolNo") == null )
                this.MasterPolNo = null;
            else
                this.MasterPolNo = rs.getString("MasterPolNo").trim();

            if( rs.getString("KindCode") == null )
                this.KindCode = null;
            else
                this.KindCode = rs.getString("KindCode").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("RiskVersion") == null )
                this.RiskVersion = null;
            else
                this.RiskVersion = rs.getString("RiskVersion").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("AgentType") == null )
                this.AgentType = null;
            else
                this.AgentType = rs.getString("AgentType").trim();

            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("AgentCode1") == null )
                this.AgentCode1 = null;
            else
                this.AgentCode1 = rs.getString("AgentCode1").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("Handler") == null )
                this.Handler = null;
            else
                this.Handler = rs.getString("Handler").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("InsuredName") == null )
                this.InsuredName = null;
            else
                this.InsuredName = rs.getString("InsuredName").trim();

            if( rs.getString("InsuredSex") == null )
                this.InsuredSex = null;
            else
                this.InsuredSex = rs.getString("InsuredSex").trim();

            this.InsuredBirthday = rs.getDate("InsuredBirthday");
            this.InsuredAppAge = rs.getInt("InsuredAppAge");
            this.InsuredPeoples = rs.getInt("InsuredPeoples");
            if( rs.getString("OccupationType") == null )
                this.OccupationType = null;
            else
                this.OccupationType = rs.getString("OccupationType").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("AppntName") == null )
                this.AppntName = null;
            else
                this.AppntName = rs.getString("AppntName").trim();

            this.CValiDate = rs.getDate("CValiDate");
            if( rs.getString("SignCom") == null )
                this.SignCom = null;
            else
                this.SignCom = rs.getString("SignCom").trim();

            this.SignDate = rs.getDate("SignDate");
            if( rs.getString("SignTime") == null )
                this.SignTime = null;
            else
                this.SignTime = rs.getString("SignTime").trim();

            this.FirstPayDate = rs.getDate("FirstPayDate");
            this.PayEndDate = rs.getDate("PayEndDate");
            this.PaytoDate = rs.getDate("PaytoDate");
            this.GetStartDate = rs.getDate("GetStartDate");
            this.EndDate = rs.getDate("EndDate");
            this.AcciEndDate = rs.getDate("AcciEndDate");
            if( rs.getString("GetYearFlag") == null )
                this.GetYearFlag = null;
            else
                this.GetYearFlag = rs.getString("GetYearFlag").trim();

            this.GetYear = rs.getInt("GetYear");
            if( rs.getString("PayEndYearFlag") == null )
                this.PayEndYearFlag = null;
            else
                this.PayEndYearFlag = rs.getString("PayEndYearFlag").trim();

            this.PayEndYear = rs.getInt("PayEndYear");
            if( rs.getString("InsuYearFlag") == null )
                this.InsuYearFlag = null;
            else
                this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

            this.InsuYear = rs.getInt("InsuYear");
            if( rs.getString("AcciYearFlag") == null )
                this.AcciYearFlag = null;
            else
                this.AcciYearFlag = rs.getString("AcciYearFlag").trim();

            this.AcciYear = rs.getInt("AcciYear");
            if( rs.getString("GetStartType") == null )
                this.GetStartType = null;
            else
                this.GetStartType = rs.getString("GetStartType").trim();

            if( rs.getString("SpecifyValiDate") == null )
                this.SpecifyValiDate = null;
            else
                this.SpecifyValiDate = rs.getString("SpecifyValiDate").trim();

            if( rs.getString("PayMode") == null )
                this.PayMode = null;
            else
                this.PayMode = rs.getString("PayMode").trim();

            if( rs.getString("PayLocation") == null )
                this.PayLocation = null;
            else
                this.PayLocation = rs.getString("PayLocation").trim();

            this.PayIntv = rs.getInt("PayIntv");
            this.PayYears = rs.getInt("PayYears");
            this.Years = rs.getInt("Years");
            this.ManageFeeRate = rs.getDouble("ManageFeeRate");
            this.FloatRate = rs.getDouble("FloatRate");
            if( rs.getString("PremToAmnt") == null )
                this.PremToAmnt = null;
            else
                this.PremToAmnt = rs.getString("PremToAmnt").trim();

            this.Mult = rs.getDouble("Mult");
            this.StandPrem = rs.getDouble("StandPrem");
            this.Prem = rs.getDouble("Prem");
            this.SumPrem = rs.getDouble("SumPrem");
            this.Amnt = rs.getDouble("Amnt");
            this.RiskAmnt = rs.getDouble("RiskAmnt");
            this.LeavingMoney = rs.getDouble("LeavingMoney");
            this.EndorseTimes = rs.getInt("EndorseTimes");
            this.ClaimTimes = rs.getInt("ClaimTimes");
            this.LiveTimes = rs.getInt("LiveTimes");
            this.RenewCount = rs.getInt("RenewCount");
            this.LastGetDate = rs.getDate("LastGetDate");
            this.LastLoanDate = rs.getDate("LastLoanDate");
            this.LastRegetDate = rs.getDate("LastRegetDate");
            this.LastEdorDate = rs.getDate("LastEdorDate");
            this.LastRevDate = rs.getDate("LastRevDate");
            this.RnewFlag = rs.getInt("RnewFlag");
            if( rs.getString("StopFlag") == null )
                this.StopFlag = null;
            else
                this.StopFlag = rs.getString("StopFlag").trim();

            if( rs.getString("ExpiryFlag") == null )
                this.ExpiryFlag = null;
            else
                this.ExpiryFlag = rs.getString("ExpiryFlag").trim();

            if( rs.getString("AutoPayFlag") == null )
                this.AutoPayFlag = null;
            else
                this.AutoPayFlag = rs.getString("AutoPayFlag").trim();

            if( rs.getString("InterestDifFlag") == null )
                this.InterestDifFlag = null;
            else
                this.InterestDifFlag = rs.getString("InterestDifFlag").trim();

            if( rs.getString("SubFlag") == null )
                this.SubFlag = null;
            else
                this.SubFlag = rs.getString("SubFlag").trim();

            if( rs.getString("BnfFlag") == null )
                this.BnfFlag = null;
            else
                this.BnfFlag = rs.getString("BnfFlag").trim();

            if( rs.getString("HealthCheckFlag") == null )
                this.HealthCheckFlag = null;
            else
                this.HealthCheckFlag = rs.getString("HealthCheckFlag").trim();

            if( rs.getString("ImpartFlag") == null )
                this.ImpartFlag = null;
            else
                this.ImpartFlag = rs.getString("ImpartFlag").trim();

            if( rs.getString("ReinsureFlag") == null )
                this.ReinsureFlag = null;
            else
                this.ReinsureFlag = rs.getString("ReinsureFlag").trim();

            if( rs.getString("AgentPayFlag") == null )
                this.AgentPayFlag = null;
            else
                this.AgentPayFlag = rs.getString("AgentPayFlag").trim();

            if( rs.getString("AgentGetFlag") == null )
                this.AgentGetFlag = null;
            else
                this.AgentGetFlag = rs.getString("AgentGetFlag").trim();

            if( rs.getString("LiveGetMode") == null )
                this.LiveGetMode = null;
            else
                this.LiveGetMode = rs.getString("LiveGetMode").trim();

            if( rs.getString("DeadGetMode") == null )
                this.DeadGetMode = null;
            else
                this.DeadGetMode = rs.getString("DeadGetMode").trim();

            if( rs.getString("BonusGetMode") == null )
                this.BonusGetMode = null;
            else
                this.BonusGetMode = rs.getString("BonusGetMode").trim();

            if( rs.getString("BonusMan") == null )
                this.BonusMan = null;
            else
                this.BonusMan = rs.getString("BonusMan").trim();

            if( rs.getString("DeadFlag") == null )
                this.DeadFlag = null;
            else
                this.DeadFlag = rs.getString("DeadFlag").trim();

            if( rs.getString("SmokeFlag") == null )
                this.SmokeFlag = null;
            else
                this.SmokeFlag = rs.getString("SmokeFlag").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("ApproveFlag") == null )
                this.ApproveFlag = null;
            else
                this.ApproveFlag = rs.getString("ApproveFlag").trim();

            if( rs.getString("ApproveCode") == null )
                this.ApproveCode = null;
            else
                this.ApproveCode = rs.getString("ApproveCode").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("ApproveTime") == null )
                this.ApproveTime = null;
            else
                this.ApproveTime = rs.getString("ApproveTime").trim();

            if( rs.getString("UWFlag") == null )
                this.UWFlag = null;
            else
                this.UWFlag = rs.getString("UWFlag").trim();

            if( rs.getString("UWCode") == null )
                this.UWCode = null;
            else
                this.UWCode = rs.getString("UWCode").trim();

            this.UWDate = rs.getDate("UWDate");
            if( rs.getString("UWTime") == null )
                this.UWTime = null;
            else
                this.UWTime = rs.getString("UWTime").trim();

            this.PolApplyDate = rs.getDate("PolApplyDate");
            if( rs.getString("AppFlag") == null )
                this.AppFlag = null;
            else
                this.AppFlag = rs.getString("AppFlag").trim();

            if( rs.getString("PolState") == null )
                this.PolState = null;
            else
                this.PolState = rs.getString("PolState").trim();

            if( rs.getString("StandbyFlag1") == null )
                this.StandbyFlag1 = null;
            else
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

            if( rs.getString("StandbyFlag2") == null )
                this.StandbyFlag2 = null;
            else
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

            if( rs.getString("StandbyFlag3") == null )
                this.StandbyFlag3 = null;
            else
                this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            this.WaitPeriod = rs.getInt("WaitPeriod");
            if( rs.getString("GetForm") == null )
                this.GetForm = null;
            else
                this.GetForm = rs.getString("GetForm").trim();

            if( rs.getString("GetBankCode") == null )
                this.GetBankCode = null;
            else
                this.GetBankCode = rs.getString("GetBankCode").trim();

            if( rs.getString("GetBankAccNo") == null )
                this.GetBankAccNo = null;
            else
                this.GetBankAccNo = rs.getString("GetBankAccNo").trim();

            if( rs.getString("GetAccName") == null )
                this.GetAccName = null;
            else
                this.GetAccName = rs.getString("GetAccName").trim();

            if( rs.getString("KeepValueOpt") == null )
                this.KeepValueOpt = null;
            else
                this.KeepValueOpt = rs.getString("KeepValueOpt").trim();

            if( rs.getString("AscriptionRuleCode") == null )
                this.AscriptionRuleCode = null;
            else
                this.AscriptionRuleCode = rs.getString("AscriptionRuleCode").trim();

            if( rs.getString("PayRuleCode") == null )
                this.PayRuleCode = null;
            else
                this.PayRuleCode = rs.getString("PayRuleCode").trim();

            if( rs.getString("AscriptionFlag") == null )
                this.AscriptionFlag = null;
            else
                this.AscriptionFlag = rs.getString("AscriptionFlag").trim();

            if( rs.getString("AutoPubAccFlag") == null )
                this.AutoPubAccFlag = null;
            else
                this.AutoPubAccFlag = rs.getString("AutoPubAccFlag").trim();

            if( rs.getString("CombiFlag") == null )
                this.CombiFlag = null;
            else
                this.CombiFlag = rs.getString("CombiFlag").trim();

            if( rs.getString("InvestRuleCode") == null )
                this.InvestRuleCode = null;
            else
                this.InvestRuleCode = rs.getString("InvestRuleCode").trim();

            if( rs.getString("UintLinkValiFlag") == null )
                this.UintLinkValiFlag = null;
            else
                this.UintLinkValiFlag = rs.getString("UintLinkValiFlag").trim();

            if( rs.getString("ProdSetCode") == null )
                this.ProdSetCode = null;
            else
                this.ProdSetCode = rs.getString("ProdSetCode").trim();

            if( rs.getString("InsurPolFlag") == null )
                this.InsurPolFlag = null;
            else
                this.InsurPolFlag = rs.getString("InsurPolFlag").trim();

            if( rs.getString("NewReinsureFlag") == null )
                this.NewReinsureFlag = null;
            else
                this.NewReinsureFlag = rs.getString("NewReinsureFlag").trim();

            if( rs.getString("LiveAccFlag") == null )
                this.LiveAccFlag = null;
            else
                this.LiveAccFlag = rs.getString("LiveAccFlag").trim();

            this.StandRatePrem = rs.getDouble("StandRatePrem");
            if( rs.getString("AppntFirstName") == null )
                this.AppntFirstName = null;
            else
                this.AppntFirstName = rs.getString("AppntFirstName").trim();

            if( rs.getString("AppntLastName") == null )
                this.AppntLastName = null;
            else
                this.AppntLastName = rs.getString("AppntLastName").trim();

            if( rs.getString("InsuredFirstName") == null )
                this.InsuredFirstName = null;
            else
                this.InsuredFirstName = rs.getString("InsuredFirstName").trim();

            if( rs.getString("InsuredLastName") == null )
                this.InsuredLastName = null;
            else
                this.InsuredLastName = rs.getString("InsuredLastName").trim();

            if( rs.getString("FQGetMode") == null )
                this.FQGetMode = null;
            else
                this.FQGetMode = rs.getString("FQGetMode").trim();

            if( rs.getString("GetPeriod") == null )
                this.GetPeriod = null;
            else
                this.GetPeriod = rs.getString("GetPeriod").trim();

            if( rs.getString("GetPeriodFlag") == null )
                this.GetPeriodFlag = null;
            else
                this.GetPeriodFlag = rs.getString("GetPeriodFlag").trim();

            if( rs.getString("DelayPeriod") == null )
                this.DelayPeriod = null;
            else
                this.DelayPeriod = rs.getString("DelayPeriod").trim();

            this.OtherAmnt = rs.getDouble("OtherAmnt");
            if( rs.getString("Relation") == null )
                this.Relation = null;
            else
                this.Relation = rs.getString("Relation").trim();

            if( rs.getString("ReCusNo") == null )
                this.ReCusNo = null;
            else
                this.ReCusNo = rs.getString("ReCusNo").trim();

            if( rs.getString("AutoRnewAge") == null )
                this.AutoRnewAge = null;
            else
                this.AutoRnewAge = rs.getString("AutoRnewAge").trim();

            if( rs.getString("AutoRnewYear") == null )
                this.AutoRnewYear = null;
            else
                this.AutoRnewYear = rs.getString("AutoRnewYear").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCPolSchema getSchema() {
        LCPolSchema aLCPolSchema = new LCPolSchema();
        aLCPolSchema.setSchema(this);
        return aLCPolSchema;
    }

    public LCPolDB getDB() {
        LCPolDB aDBOper = new LCPolDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPol描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(PolID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ContID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolTypeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MasterPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(KindCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredSex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( InsuredBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuredAppAge));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuredPeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FirstPayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PaytoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GetStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AcciEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayEndYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayEndYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AcciYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AcciYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetStartType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecifyValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayLocation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayYears));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ManageFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FloatRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PremToAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RiskAmnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LeavingMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(EndorseTimes));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ClaimTimes));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LiveTimes));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RenewCount));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LastGetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LastLoanDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LastRegetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LastEdorDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( LastRevDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RnewFlag));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StopFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExpiryFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AutoPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InterestDifFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BnfFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HealthCheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ImpartFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReinsureFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGetFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LiveGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DeadGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BonusGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BonusMan)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DeadFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SmokeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(WaitPeriod));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetForm)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetAccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(KeepValueOpt)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AscriptionRuleCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayRuleCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AscriptionFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AutoPubAccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CombiFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InvestRuleCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UintLinkValiFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsurPolFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewReinsureFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LiveAccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandRatePrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntFirstName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntLastName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredFirstName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredLastName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FQGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetPeriod)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetPeriodFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DelayPeriod)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OtherAmnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Relation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReCusNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AutoRnewAge)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AutoRnewYear));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCPol>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            PolID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ContID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            PolTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            MainPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            MasterPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            AgentCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            InsuredBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER));
            InsuredAppAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,29, SysConst.PACKAGESPILTER))).intValue();
            InsuredPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,30, SysConst.PACKAGESPILTER))).intValue();
            OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER));
            SignCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER));
            SignTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            FirstPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER));
            PayEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER));
            PaytoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER));
            GetStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER));
            AcciEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER));
            GetYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            GetYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,45, SysConst.PACKAGESPILTER))).intValue();
            PayEndYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            PayEndYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,47, SysConst.PACKAGESPILTER))).intValue();
            InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            InsuYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,49, SysConst.PACKAGESPILTER))).intValue();
            AcciYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            AcciYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,51, SysConst.PACKAGESPILTER))).intValue();
            GetStartType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            SpecifyValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
            PayLocation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,56, SysConst.PACKAGESPILTER))).intValue();
            PayYears = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,57, SysConst.PACKAGESPILTER))).intValue();
            Years = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,58, SysConst.PACKAGESPILTER))).intValue();
            ManageFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,59, SysConst.PACKAGESPILTER))).doubleValue();
            FloatRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,60, SysConst.PACKAGESPILTER))).doubleValue();
            PremToAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
            Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,62, SysConst.PACKAGESPILTER))).doubleValue();
            StandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,63, SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,64, SysConst.PACKAGESPILTER))).doubleValue();
            SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,65, SysConst.PACKAGESPILTER))).doubleValue();
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,66, SysConst.PACKAGESPILTER))).doubleValue();
            RiskAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,67, SysConst.PACKAGESPILTER))).doubleValue();
            LeavingMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,68, SysConst.PACKAGESPILTER))).doubleValue();
            EndorseTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,69, SysConst.PACKAGESPILTER))).intValue();
            ClaimTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,70, SysConst.PACKAGESPILTER))).intValue();
            LiveTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,71, SysConst.PACKAGESPILTER))).intValue();
            RenewCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,72, SysConst.PACKAGESPILTER))).intValue();
            LastGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER));
            LastLoanDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74, SysConst.PACKAGESPILTER));
            LastRegetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER));
            LastEdorDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER));
            LastRevDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER));
            RnewFlag = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,78, SysConst.PACKAGESPILTER))).intValue();
            StopFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79, SysConst.PACKAGESPILTER );
            ExpiryFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80, SysConst.PACKAGESPILTER );
            AutoPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81, SysConst.PACKAGESPILTER );
            InterestDifFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 82, SysConst.PACKAGESPILTER );
            SubFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 83, SysConst.PACKAGESPILTER );
            BnfFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 84, SysConst.PACKAGESPILTER );
            HealthCheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 85, SysConst.PACKAGESPILTER );
            ImpartFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 86, SysConst.PACKAGESPILTER );
            ReinsureFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 87, SysConst.PACKAGESPILTER );
            AgentPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 88, SysConst.PACKAGESPILTER );
            AgentGetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 89, SysConst.PACKAGESPILTER );
            LiveGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 90, SysConst.PACKAGESPILTER );
            DeadGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 91, SysConst.PACKAGESPILTER );
            BonusGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 92, SysConst.PACKAGESPILTER );
            BonusMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 93, SysConst.PACKAGESPILTER );
            DeadFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 94, SysConst.PACKAGESPILTER );
            SmokeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 95, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 96, SysConst.PACKAGESPILTER );
            ApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 97, SysConst.PACKAGESPILTER );
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 98, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 99, SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 100, SysConst.PACKAGESPILTER );
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 101, SysConst.PACKAGESPILTER );
            UWCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 102, SysConst.PACKAGESPILTER );
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 103, SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 104, SysConst.PACKAGESPILTER );
            PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 105, SysConst.PACKAGESPILTER));
            AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 106, SysConst.PACKAGESPILTER );
            PolState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 107, SysConst.PACKAGESPILTER );
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 108, SysConst.PACKAGESPILTER );
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 109, SysConst.PACKAGESPILTER );
            StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 110, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 111, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 112, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 113, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 114, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 115, SysConst.PACKAGESPILTER );
            WaitPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,116, SysConst.PACKAGESPILTER))).intValue();
            GetForm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 117, SysConst.PACKAGESPILTER );
            GetBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 118, SysConst.PACKAGESPILTER );
            GetBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 119, SysConst.PACKAGESPILTER );
            GetAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 120, SysConst.PACKAGESPILTER );
            KeepValueOpt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 121, SysConst.PACKAGESPILTER );
            AscriptionRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 122, SysConst.PACKAGESPILTER );
            PayRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 123, SysConst.PACKAGESPILTER );
            AscriptionFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 124, SysConst.PACKAGESPILTER );
            AutoPubAccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 125, SysConst.PACKAGESPILTER );
            CombiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 126, SysConst.PACKAGESPILTER );
            InvestRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 127, SysConst.PACKAGESPILTER );
            UintLinkValiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 128, SysConst.PACKAGESPILTER );
            ProdSetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 129, SysConst.PACKAGESPILTER );
            InsurPolFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 130, SysConst.PACKAGESPILTER );
            NewReinsureFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 131, SysConst.PACKAGESPILTER );
            LiveAccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 132, SysConst.PACKAGESPILTER );
            StandRatePrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,133, SysConst.PACKAGESPILTER))).doubleValue();
            AppntFirstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 134, SysConst.PACKAGESPILTER );
            AppntLastName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 135, SysConst.PACKAGESPILTER );
            InsuredFirstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 136, SysConst.PACKAGESPILTER );
            InsuredLastName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 137, SysConst.PACKAGESPILTER );
            FQGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 138, SysConst.PACKAGESPILTER );
            GetPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 139, SysConst.PACKAGESPILTER );
            GetPeriodFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 140, SysConst.PACKAGESPILTER );
            DelayPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 141, SysConst.PACKAGESPILTER );
            OtherAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,142, SysConst.PACKAGESPILTER))).doubleValue();
            Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 143, SysConst.PACKAGESPILTER );
            ReCusNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 144, SysConst.PACKAGESPILTER );
            AutoRnewAge = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 145, SysConst.PACKAGESPILTER );
            AutoRnewYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 146, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCPolSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PolID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("PolTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolTypeFlag));
        }
        if (FCode.equalsIgnoreCase("MainPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolNo));
        }
        if (FCode.equalsIgnoreCase("MasterPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MasterPolNo));
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
        }
        if (FCode.equalsIgnoreCase("InsuredAppAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAppAge));
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredPeoples));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstPayDate()));
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayEndDate()));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetStartDate()));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
        }
        if (FCode.equalsIgnoreCase("AcciEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAcciEndDate()));
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("AcciYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciYearFlag));
        }
        if (FCode.equalsIgnoreCase("AcciYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AcciYear));
        }
        if (FCode.equalsIgnoreCase("GetStartType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartType));
        }
        if (FCode.equalsIgnoreCase("SpecifyValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecifyValiDate));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("PayLocation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayLocation));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
        }
        if (FCode.equalsIgnoreCase("Years")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFeeRate));
        }
        if (FCode.equalsIgnoreCase("FloatRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FloatRate));
        }
        if (FCode.equalsIgnoreCase("PremToAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremToAmnt));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("RiskAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskAmnt));
        }
        if (FCode.equalsIgnoreCase("LeavingMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LeavingMoney));
        }
        if (FCode.equalsIgnoreCase("EndorseTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndorseTimes));
        }
        if (FCode.equalsIgnoreCase("ClaimTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimTimes));
        }
        if (FCode.equalsIgnoreCase("LiveTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveTimes));
        }
        if (FCode.equalsIgnoreCase("RenewCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RenewCount));
        }
        if (FCode.equalsIgnoreCase("LastGetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastGetDate()));
        }
        if (FCode.equalsIgnoreCase("LastLoanDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastLoanDate()));
        }
        if (FCode.equalsIgnoreCase("LastRegetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastRegetDate()));
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastEdorDate()));
        }
        if (FCode.equalsIgnoreCase("LastRevDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastRevDate()));
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RnewFlag));
        }
        if (FCode.equalsIgnoreCase("StopFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StopFlag));
        }
        if (FCode.equalsIgnoreCase("ExpiryFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpiryFlag));
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoPayFlag));
        }
        if (FCode.equalsIgnoreCase("InterestDifFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterestDifFlag));
        }
        if (FCode.equalsIgnoreCase("SubFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubFlag));
        }
        if (FCode.equalsIgnoreCase("BnfFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BnfFlag));
        }
        if (FCode.equalsIgnoreCase("HealthCheckFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthCheckFlag));
        }
        if (FCode.equalsIgnoreCase("ImpartFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartFlag));
        }
        if (FCode.equalsIgnoreCase("ReinsureFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsureFlag));
        }
        if (FCode.equalsIgnoreCase("AgentPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPayFlag));
        }
        if (FCode.equalsIgnoreCase("AgentGetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetFlag));
        }
        if (FCode.equalsIgnoreCase("LiveGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetMode));
        }
        if (FCode.equalsIgnoreCase("DeadGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadGetMode));
        }
        if (FCode.equalsIgnoreCase("BonusGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusGetMode));
        }
        if (FCode.equalsIgnoreCase("BonusMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusMan));
        }
        if (FCode.equalsIgnoreCase("DeadFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeadFlag));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWCode));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("PolState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolState));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("WaitPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WaitPeriod));
        }
        if (FCode.equalsIgnoreCase("GetForm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetForm));
        }
        if (FCode.equalsIgnoreCase("GetBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetBankCode));
        }
        if (FCode.equalsIgnoreCase("GetBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetBankAccNo));
        }
        if (FCode.equalsIgnoreCase("GetAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetAccName));
        }
        if (FCode.equalsIgnoreCase("KeepValueOpt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KeepValueOpt));
        }
        if (FCode.equalsIgnoreCase("AscriptionRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptionRuleCode));
        }
        if (FCode.equalsIgnoreCase("PayRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayRuleCode));
        }
        if (FCode.equalsIgnoreCase("AscriptionFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptionFlag));
        }
        if (FCode.equalsIgnoreCase("AutoPubAccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoPubAccFlag));
        }
        if (FCode.equalsIgnoreCase("CombiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CombiFlag));
        }
        if (FCode.equalsIgnoreCase("InvestRuleCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestRuleCode));
        }
        if (FCode.equalsIgnoreCase("UintLinkValiFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UintLinkValiFlag));
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("InsurPolFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurPolFlag));
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewReinsureFlag));
        }
        if (FCode.equalsIgnoreCase("LiveAccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LiveAccFlag));
        }
        if (FCode.equalsIgnoreCase("StandRatePrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandRatePrem));
        }
        if (FCode.equalsIgnoreCase("AppntFirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntFirstName));
        }
        if (FCode.equalsIgnoreCase("AppntLastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntLastName));
        }
        if (FCode.equalsIgnoreCase("InsuredFirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredFirstName));
        }
        if (FCode.equalsIgnoreCase("InsuredLastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredLastName));
        }
        if (FCode.equalsIgnoreCase("FQGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FQGetMode));
        }
        if (FCode.equalsIgnoreCase("GetPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPeriod));
        }
        if (FCode.equalsIgnoreCase("GetPeriodFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPeriodFlag));
        }
        if (FCode.equalsIgnoreCase("DelayPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DelayPeriod));
        }
        if (FCode.equalsIgnoreCase("OtherAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherAmnt));
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
        }
        if (FCode.equalsIgnoreCase("ReCusNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReCusNo));
        }
        if (FCode.equalsIgnoreCase("AutoRnewAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoRnewAge));
        }
        if (FCode.equalsIgnoreCase("AutoRnewYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AutoRnewYear));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PolID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ProposalNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ContType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(PolTypeFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MainPolNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MasterPolNo);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AgentCode1);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Handler);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(InsuredSex);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
                break;
            case 28:
                strFieldValue = String.valueOf(InsuredAppAge);
                break;
            case 29:
                strFieldValue = String.valueOf(InsuredPeoples);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(OccupationType);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(SignCom);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(SignTime);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstPayDate()));
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayEndDate()));
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetStartDate()));
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAcciEndDate()));
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(GetYearFlag);
                break;
            case 44:
                strFieldValue = String.valueOf(GetYear);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(PayEndYearFlag);
                break;
            case 46:
                strFieldValue = String.valueOf(PayEndYear);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
                break;
            case 48:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(AcciYearFlag);
                break;
            case 50:
                strFieldValue = String.valueOf(AcciYear);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(GetStartType);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(SpecifyValiDate);
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(PayLocation);
                break;
            case 55:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 56:
                strFieldValue = String.valueOf(PayYears);
                break;
            case 57:
                strFieldValue = String.valueOf(Years);
                break;
            case 58:
                strFieldValue = String.valueOf(ManageFeeRate);
                break;
            case 59:
                strFieldValue = String.valueOf(FloatRate);
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(PremToAmnt);
                break;
            case 61:
                strFieldValue = String.valueOf(Mult);
                break;
            case 62:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 63:
                strFieldValue = String.valueOf(Prem);
                break;
            case 64:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 65:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 66:
                strFieldValue = String.valueOf(RiskAmnt);
                break;
            case 67:
                strFieldValue = String.valueOf(LeavingMoney);
                break;
            case 68:
                strFieldValue = String.valueOf(EndorseTimes);
                break;
            case 69:
                strFieldValue = String.valueOf(ClaimTimes);
                break;
            case 70:
                strFieldValue = String.valueOf(LiveTimes);
                break;
            case 71:
                strFieldValue = String.valueOf(RenewCount);
                break;
            case 72:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastGetDate()));
                break;
            case 73:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastLoanDate()));
                break;
            case 74:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastRegetDate()));
                break;
            case 75:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastEdorDate()));
                break;
            case 76:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastRevDate()));
                break;
            case 77:
                strFieldValue = String.valueOf(RnewFlag);
                break;
            case 78:
                strFieldValue = StrTool.GBKToUnicode(StopFlag);
                break;
            case 79:
                strFieldValue = StrTool.GBKToUnicode(ExpiryFlag);
                break;
            case 80:
                strFieldValue = StrTool.GBKToUnicode(AutoPayFlag);
                break;
            case 81:
                strFieldValue = StrTool.GBKToUnicode(InterestDifFlag);
                break;
            case 82:
                strFieldValue = StrTool.GBKToUnicode(SubFlag);
                break;
            case 83:
                strFieldValue = StrTool.GBKToUnicode(BnfFlag);
                break;
            case 84:
                strFieldValue = StrTool.GBKToUnicode(HealthCheckFlag);
                break;
            case 85:
                strFieldValue = StrTool.GBKToUnicode(ImpartFlag);
                break;
            case 86:
                strFieldValue = StrTool.GBKToUnicode(ReinsureFlag);
                break;
            case 87:
                strFieldValue = StrTool.GBKToUnicode(AgentPayFlag);
                break;
            case 88:
                strFieldValue = StrTool.GBKToUnicode(AgentGetFlag);
                break;
            case 89:
                strFieldValue = StrTool.GBKToUnicode(LiveGetMode);
                break;
            case 90:
                strFieldValue = StrTool.GBKToUnicode(DeadGetMode);
                break;
            case 91:
                strFieldValue = StrTool.GBKToUnicode(BonusGetMode);
                break;
            case 92:
                strFieldValue = StrTool.GBKToUnicode(BonusMan);
                break;
            case 93:
                strFieldValue = StrTool.GBKToUnicode(DeadFlag);
                break;
            case 94:
                strFieldValue = StrTool.GBKToUnicode(SmokeFlag);
                break;
            case 95:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 96:
                strFieldValue = StrTool.GBKToUnicode(ApproveFlag);
                break;
            case 97:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 98:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 99:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            case 100:
                strFieldValue = StrTool.GBKToUnicode(UWFlag);
                break;
            case 101:
                strFieldValue = StrTool.GBKToUnicode(UWCode);
                break;
            case 102:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
                break;
            case 103:
                strFieldValue = StrTool.GBKToUnicode(UWTime);
                break;
            case 104:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
                break;
            case 105:
                strFieldValue = StrTool.GBKToUnicode(AppFlag);
                break;
            case 106:
                strFieldValue = StrTool.GBKToUnicode(PolState);
                break;
            case 107:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
                break;
            case 108:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
                break;
            case 109:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
                break;
            case 110:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 111:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 112:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 113:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 114:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 115:
                strFieldValue = String.valueOf(WaitPeriod);
                break;
            case 116:
                strFieldValue = StrTool.GBKToUnicode(GetForm);
                break;
            case 117:
                strFieldValue = StrTool.GBKToUnicode(GetBankCode);
                break;
            case 118:
                strFieldValue = StrTool.GBKToUnicode(GetBankAccNo);
                break;
            case 119:
                strFieldValue = StrTool.GBKToUnicode(GetAccName);
                break;
            case 120:
                strFieldValue = StrTool.GBKToUnicode(KeepValueOpt);
                break;
            case 121:
                strFieldValue = StrTool.GBKToUnicode(AscriptionRuleCode);
                break;
            case 122:
                strFieldValue = StrTool.GBKToUnicode(PayRuleCode);
                break;
            case 123:
                strFieldValue = StrTool.GBKToUnicode(AscriptionFlag);
                break;
            case 124:
                strFieldValue = StrTool.GBKToUnicode(AutoPubAccFlag);
                break;
            case 125:
                strFieldValue = StrTool.GBKToUnicode(CombiFlag);
                break;
            case 126:
                strFieldValue = StrTool.GBKToUnicode(InvestRuleCode);
                break;
            case 127:
                strFieldValue = StrTool.GBKToUnicode(UintLinkValiFlag);
                break;
            case 128:
                strFieldValue = StrTool.GBKToUnicode(ProdSetCode);
                break;
            case 129:
                strFieldValue = StrTool.GBKToUnicode(InsurPolFlag);
                break;
            case 130:
                strFieldValue = StrTool.GBKToUnicode(NewReinsureFlag);
                break;
            case 131:
                strFieldValue = StrTool.GBKToUnicode(LiveAccFlag);
                break;
            case 132:
                strFieldValue = String.valueOf(StandRatePrem);
                break;
            case 133:
                strFieldValue = StrTool.GBKToUnicode(AppntFirstName);
                break;
            case 134:
                strFieldValue = StrTool.GBKToUnicode(AppntLastName);
                break;
            case 135:
                strFieldValue = StrTool.GBKToUnicode(InsuredFirstName);
                break;
            case 136:
                strFieldValue = StrTool.GBKToUnicode(InsuredLastName);
                break;
            case 137:
                strFieldValue = StrTool.GBKToUnicode(FQGetMode);
                break;
            case 138:
                strFieldValue = StrTool.GBKToUnicode(GetPeriod);
                break;
            case 139:
                strFieldValue = StrTool.GBKToUnicode(GetPeriodFlag);
                break;
            case 140:
                strFieldValue = StrTool.GBKToUnicode(DelayPeriod);
                break;
            case 141:
                strFieldValue = String.valueOf(OtherAmnt);
                break;
            case 142:
                strFieldValue = StrTool.GBKToUnicode(Relation);
                break;
            case 143:
                strFieldValue = StrTool.GBKToUnicode(ReCusNo);
                break;
            case 144:
                strFieldValue = StrTool.GBKToUnicode(AutoRnewAge);
                break;
            case 145:
                strFieldValue = StrTool.GBKToUnicode(AutoRnewYear);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PolID")) {
            if( FValue != null && !FValue.equals("")) {
                PolID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
                GrpPolNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("PolTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolTypeFlag = FValue.trim();
            }
            else
                PolTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("MainPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainPolNo = FValue.trim();
            }
            else
                MainPolNo = null;
        }
        if (FCode.equalsIgnoreCase("MasterPolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                MasterPolNo = FValue.trim();
            }
            else
                MasterPolNo = null;
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
                KindCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
                RiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
                AgentType = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode1 = FValue.trim();
            }
            else
                AgentCode1 = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            if( FValue != null && !FValue.equals(""))
            {
                Handler = FValue.trim();
            }
            else
                Handler = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredSex = FValue.trim();
            }
            else
                InsuredSex = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            if(FValue != null && !FValue.equals("")) {
                InsuredBirthday = fDate.getDate( FValue );
            }
            else
                InsuredBirthday = null;
        }
        if (FCode.equalsIgnoreCase("InsuredAppAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuredAppAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuredPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if(FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate( FValue );
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignCom = FValue.trim();
            }
            else
                SignCom = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if(FValue != null && !FValue.equals("")) {
                SignDate = fDate.getDate( FValue );
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            if(FValue != null && !FValue.equals("")) {
                FirstPayDate = fDate.getDate( FValue );
            }
            else
                FirstPayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayEndDate = fDate.getDate( FValue );
            }
            else
                PayEndDate = null;
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if(FValue != null && !FValue.equals("")) {
                PaytoDate = fDate.getDate( FValue );
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("GetStartDate")) {
            if(FValue != null && !FValue.equals("")) {
                GetStartDate = fDate.getDate( FValue );
            }
            else
                GetStartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if(FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate( FValue );
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("AcciEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                AcciEndDate = fDate.getDate( FValue );
            }
            else
                AcciEndDate = null;
        }
        if (FCode.equalsIgnoreCase("GetYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
                GetYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("GetYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayEndYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayEndYearFlag = FValue.trim();
            }
            else
                PayEndYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayEndYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayEndYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("AcciYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AcciYearFlag = FValue.trim();
            }
            else
                AcciYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("AcciYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AcciYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetStartType")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetStartType = FValue.trim();
            }
            else
                GetStartType = null;
        }
        if (FCode.equalsIgnoreCase("SpecifyValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecifyValiDate = FValue.trim();
            }
            else
                SpecifyValiDate = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("PayLocation")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayLocation = FValue.trim();
            }
            else
                PayLocation = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("PayYears")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayYears = i;
            }
        }
        if (FCode.equalsIgnoreCase("Years")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Years = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ManageFeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FloatRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FloatRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PremToAmnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                PremToAmnt = FValue.trim();
            }
            else
                PremToAmnt = null;
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("RiskAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RiskAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("LeavingMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                LeavingMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("EndorseTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                EndorseTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("ClaimTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ClaimTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("LiveTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                LiveTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("RenewCount")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RenewCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("LastGetDate")) {
            if(FValue != null && !FValue.equals("")) {
                LastGetDate = fDate.getDate( FValue );
            }
            else
                LastGetDate = null;
        }
        if (FCode.equalsIgnoreCase("LastLoanDate")) {
            if(FValue != null && !FValue.equals("")) {
                LastLoanDate = fDate.getDate( FValue );
            }
            else
                LastLoanDate = null;
        }
        if (FCode.equalsIgnoreCase("LastRegetDate")) {
            if(FValue != null && !FValue.equals("")) {
                LastRegetDate = fDate.getDate( FValue );
            }
            else
                LastRegetDate = null;
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            if(FValue != null && !FValue.equals("")) {
                LastEdorDate = fDate.getDate( FValue );
            }
            else
                LastEdorDate = null;
        }
        if (FCode.equalsIgnoreCase("LastRevDate")) {
            if(FValue != null && !FValue.equals("")) {
                LastRevDate = fDate.getDate( FValue );
            }
            else
                LastRevDate = null;
        }
        if (FCode.equalsIgnoreCase("RnewFlag")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RnewFlag = i;
            }
        }
        if (FCode.equalsIgnoreCase("StopFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                StopFlag = FValue.trim();
            }
            else
                StopFlag = null;
        }
        if (FCode.equalsIgnoreCase("ExpiryFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExpiryFlag = FValue.trim();
            }
            else
                ExpiryFlag = null;
        }
        if (FCode.equalsIgnoreCase("AutoPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoPayFlag = FValue.trim();
            }
            else
                AutoPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("InterestDifFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InterestDifFlag = FValue.trim();
            }
            else
                InterestDifFlag = null;
        }
        if (FCode.equalsIgnoreCase("SubFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubFlag = FValue.trim();
            }
            else
                SubFlag = null;
        }
        if (FCode.equalsIgnoreCase("BnfFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BnfFlag = FValue.trim();
            }
            else
                BnfFlag = null;
        }
        if (FCode.equalsIgnoreCase("HealthCheckFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthCheckFlag = FValue.trim();
            }
            else
                HealthCheckFlag = null;
        }
        if (FCode.equalsIgnoreCase("ImpartFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartFlag = FValue.trim();
            }
            else
                ImpartFlag = null;
        }
        if (FCode.equalsIgnoreCase("ReinsureFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReinsureFlag = FValue.trim();
            }
            else
                ReinsureFlag = null;
        }
        if (FCode.equalsIgnoreCase("AgentPayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentPayFlag = FValue.trim();
            }
            else
                AgentPayFlag = null;
        }
        if (FCode.equalsIgnoreCase("AgentGetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGetFlag = FValue.trim();
            }
            else
                AgentGetFlag = null;
        }
        if (FCode.equalsIgnoreCase("LiveGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveGetMode = FValue.trim();
            }
            else
                LiveGetMode = null;
        }
        if (FCode.equalsIgnoreCase("DeadGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadGetMode = FValue.trim();
            }
            else
                DeadGetMode = null;
        }
        if (FCode.equalsIgnoreCase("BonusGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusGetMode = FValue.trim();
            }
            else
                BonusGetMode = null;
        }
        if (FCode.equalsIgnoreCase("BonusMan")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusMan = FValue.trim();
            }
            else
                BonusMan = null;
        }
        if (FCode.equalsIgnoreCase("DeadFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeadFlag = FValue.trim();
            }
            else
                DeadFlag = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWCode = FValue.trim();
            }
            else
                UWCode = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if(FValue != null && !FValue.equals("")) {
                UWDate = fDate.getDate( FValue );
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            if(FValue != null && !FValue.equals("")) {
                PolApplyDate = fDate.getDate( FValue );
            }
            else
                PolApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolState")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolState = FValue.trim();
            }
            else
                PolState = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("WaitPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                WaitPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetForm")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetForm = FValue.trim();
            }
            else
                GetForm = null;
        }
        if (FCode.equalsIgnoreCase("GetBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetBankCode = FValue.trim();
            }
            else
                GetBankCode = null;
        }
        if (FCode.equalsIgnoreCase("GetBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetBankAccNo = FValue.trim();
            }
            else
                GetBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("GetAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetAccName = FValue.trim();
            }
            else
                GetAccName = null;
        }
        if (FCode.equalsIgnoreCase("KeepValueOpt")) {
            if( FValue != null && !FValue.equals(""))
            {
                KeepValueOpt = FValue.trim();
            }
            else
                KeepValueOpt = null;
        }
        if (FCode.equalsIgnoreCase("AscriptionRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AscriptionRuleCode = FValue.trim();
            }
            else
                AscriptionRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("PayRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayRuleCode = FValue.trim();
            }
            else
                PayRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("AscriptionFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AscriptionFlag = FValue.trim();
            }
            else
                AscriptionFlag = null;
        }
        if (FCode.equalsIgnoreCase("AutoPubAccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoPubAccFlag = FValue.trim();
            }
            else
                AutoPubAccFlag = null;
        }
        if (FCode.equalsIgnoreCase("CombiFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CombiFlag = FValue.trim();
            }
            else
                CombiFlag = null;
        }
        if (FCode.equalsIgnoreCase("InvestRuleCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestRuleCode = FValue.trim();
            }
            else
                InvestRuleCode = null;
        }
        if (FCode.equalsIgnoreCase("UintLinkValiFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UintLinkValiFlag = FValue.trim();
            }
            else
                UintLinkValiFlag = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("InsurPolFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsurPolFlag = FValue.trim();
            }
            else
                InsurPolFlag = null;
        }
        if (FCode.equalsIgnoreCase("NewReinsureFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewReinsureFlag = FValue.trim();
            }
            else
                NewReinsureFlag = null;
        }
        if (FCode.equalsIgnoreCase("LiveAccFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                LiveAccFlag = FValue.trim();
            }
            else
                LiveAccFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandRatePrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandRatePrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("AppntFirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntFirstName = FValue.trim();
            }
            else
                AppntFirstName = null;
        }
        if (FCode.equalsIgnoreCase("AppntLastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntLastName = FValue.trim();
            }
            else
                AppntLastName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredFirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredFirstName = FValue.trim();
            }
            else
                InsuredFirstName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredLastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredLastName = FValue.trim();
            }
            else
                InsuredLastName = null;
        }
        if (FCode.equalsIgnoreCase("FQGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FQGetMode = FValue.trim();
            }
            else
                FQGetMode = null;
        }
        if (FCode.equalsIgnoreCase("GetPeriod")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPeriod = FValue.trim();
            }
            else
                GetPeriod = null;
        }
        if (FCode.equalsIgnoreCase("GetPeriodFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPeriodFlag = FValue.trim();
            }
            else
                GetPeriodFlag = null;
        }
        if (FCode.equalsIgnoreCase("DelayPeriod")) {
            if( FValue != null && !FValue.equals(""))
            {
                DelayPeriod = FValue.trim();
            }
            else
                DelayPeriod = null;
        }
        if (FCode.equalsIgnoreCase("OtherAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                OtherAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Relation = FValue.trim();
            }
            else
                Relation = null;
        }
        if (FCode.equalsIgnoreCase("ReCusNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReCusNo = FValue.trim();
            }
            else
                ReCusNo = null;
        }
        if (FCode.equalsIgnoreCase("AutoRnewAge")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoRnewAge = FValue.trim();
            }
            else
                AutoRnewAge = null;
        }
        if (FCode.equalsIgnoreCase("AutoRnewYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                AutoRnewYear = FValue.trim();
            }
            else
                AutoRnewYear = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCPolSchema other = (LCPolSchema)otherObject;
        return
            PolID == other.getPolID()
            && ContID == other.getContID()
            && ShardingID.equals(other.getShardingID())
            && GrpContNo.equals(other.getGrpContNo())
            && GrpPolNo.equals(other.getGrpPolNo())
            && ContNo.equals(other.getContNo())
            && PolNo.equals(other.getPolNo())
            && ProposalNo.equals(other.getProposalNo())
            && PrtNo.equals(other.getPrtNo())
            && ContType.equals(other.getContType())
            && PolTypeFlag.equals(other.getPolTypeFlag())
            && MainPolNo.equals(other.getMainPolNo())
            && MasterPolNo.equals(other.getMasterPolNo())
            && KindCode.equals(other.getKindCode())
            && RiskCode.equals(other.getRiskCode())
            && RiskVersion.equals(other.getRiskVersion())
            && ManageCom.equals(other.getManageCom())
            && AgentCom.equals(other.getAgentCom())
            && AgentType.equals(other.getAgentType())
            && AgentCode.equals(other.getAgentCode())
            && AgentGroup.equals(other.getAgentGroup())
            && AgentCode1.equals(other.getAgentCode1())
            && SaleChnl.equals(other.getSaleChnl())
            && Handler.equals(other.getHandler())
            && InsuredNo.equals(other.getInsuredNo())
            && InsuredName.equals(other.getInsuredName())
            && InsuredSex.equals(other.getInsuredSex())
            && fDate.getString(InsuredBirthday).equals(other.getInsuredBirthday())
            && InsuredAppAge == other.getInsuredAppAge()
            && InsuredPeoples == other.getInsuredPeoples()
            && OccupationType.equals(other.getOccupationType())
            && AppntNo.equals(other.getAppntNo())
            && AppntName.equals(other.getAppntName())
            && fDate.getString(CValiDate).equals(other.getCValiDate())
            && SignCom.equals(other.getSignCom())
            && fDate.getString(SignDate).equals(other.getSignDate())
            && SignTime.equals(other.getSignTime())
            && fDate.getString(FirstPayDate).equals(other.getFirstPayDate())
            && fDate.getString(PayEndDate).equals(other.getPayEndDate())
            && fDate.getString(PaytoDate).equals(other.getPaytoDate())
            && fDate.getString(GetStartDate).equals(other.getGetStartDate())
            && fDate.getString(EndDate).equals(other.getEndDate())
            && fDate.getString(AcciEndDate).equals(other.getAcciEndDate())
            && GetYearFlag.equals(other.getGetYearFlag())
            && GetYear == other.getGetYear()
            && PayEndYearFlag.equals(other.getPayEndYearFlag())
            && PayEndYear == other.getPayEndYear()
            && InsuYearFlag.equals(other.getInsuYearFlag())
            && InsuYear == other.getInsuYear()
            && AcciYearFlag.equals(other.getAcciYearFlag())
            && AcciYear == other.getAcciYear()
            && GetStartType.equals(other.getGetStartType())
            && SpecifyValiDate.equals(other.getSpecifyValiDate())
            && PayMode.equals(other.getPayMode())
            && PayLocation.equals(other.getPayLocation())
            && PayIntv == other.getPayIntv()
            && PayYears == other.getPayYears()
            && Years == other.getYears()
            && ManageFeeRate == other.getManageFeeRate()
            && FloatRate == other.getFloatRate()
            && PremToAmnt.equals(other.getPremToAmnt())
            && Mult == other.getMult()
            && StandPrem == other.getStandPrem()
            && Prem == other.getPrem()
            && SumPrem == other.getSumPrem()
            && Amnt == other.getAmnt()
            && RiskAmnt == other.getRiskAmnt()
            && LeavingMoney == other.getLeavingMoney()
            && EndorseTimes == other.getEndorseTimes()
            && ClaimTimes == other.getClaimTimes()
            && LiveTimes == other.getLiveTimes()
            && RenewCount == other.getRenewCount()
            && fDate.getString(LastGetDate).equals(other.getLastGetDate())
            && fDate.getString(LastLoanDate).equals(other.getLastLoanDate())
            && fDate.getString(LastRegetDate).equals(other.getLastRegetDate())
            && fDate.getString(LastEdorDate).equals(other.getLastEdorDate())
            && fDate.getString(LastRevDate).equals(other.getLastRevDate())
            && RnewFlag == other.getRnewFlag()
            && StopFlag.equals(other.getStopFlag())
            && ExpiryFlag.equals(other.getExpiryFlag())
            && AutoPayFlag.equals(other.getAutoPayFlag())
            && InterestDifFlag.equals(other.getInterestDifFlag())
            && SubFlag.equals(other.getSubFlag())
            && BnfFlag.equals(other.getBnfFlag())
            && HealthCheckFlag.equals(other.getHealthCheckFlag())
            && ImpartFlag.equals(other.getImpartFlag())
            && ReinsureFlag.equals(other.getReinsureFlag())
            && AgentPayFlag.equals(other.getAgentPayFlag())
            && AgentGetFlag.equals(other.getAgentGetFlag())
            && LiveGetMode.equals(other.getLiveGetMode())
            && DeadGetMode.equals(other.getDeadGetMode())
            && BonusGetMode.equals(other.getBonusGetMode())
            && BonusMan.equals(other.getBonusMan())
            && DeadFlag.equals(other.getDeadFlag())
            && SmokeFlag.equals(other.getSmokeFlag())
            && Remark.equals(other.getRemark())
            && ApproveFlag.equals(other.getApproveFlag())
            && ApproveCode.equals(other.getApproveCode())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && ApproveTime.equals(other.getApproveTime())
            && UWFlag.equals(other.getUWFlag())
            && UWCode.equals(other.getUWCode())
            && fDate.getString(UWDate).equals(other.getUWDate())
            && UWTime.equals(other.getUWTime())
            && fDate.getString(PolApplyDate).equals(other.getPolApplyDate())
            && AppFlag.equals(other.getAppFlag())
            && PolState.equals(other.getPolState())
            && StandbyFlag1.equals(other.getStandbyFlag1())
            && StandbyFlag2.equals(other.getStandbyFlag2())
            && StandbyFlag3.equals(other.getStandbyFlag3())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && WaitPeriod == other.getWaitPeriod()
            && GetForm.equals(other.getGetForm())
            && GetBankCode.equals(other.getGetBankCode())
            && GetBankAccNo.equals(other.getGetBankAccNo())
            && GetAccName.equals(other.getGetAccName())
            && KeepValueOpt.equals(other.getKeepValueOpt())
            && AscriptionRuleCode.equals(other.getAscriptionRuleCode())
            && PayRuleCode.equals(other.getPayRuleCode())
            && AscriptionFlag.equals(other.getAscriptionFlag())
            && AutoPubAccFlag.equals(other.getAutoPubAccFlag())
            && CombiFlag.equals(other.getCombiFlag())
            && InvestRuleCode.equals(other.getInvestRuleCode())
            && UintLinkValiFlag.equals(other.getUintLinkValiFlag())
            && ProdSetCode.equals(other.getProdSetCode())
            && InsurPolFlag.equals(other.getInsurPolFlag())
            && NewReinsureFlag.equals(other.getNewReinsureFlag())
            && LiveAccFlag.equals(other.getLiveAccFlag())
            && StandRatePrem == other.getStandRatePrem()
            && AppntFirstName.equals(other.getAppntFirstName())
            && AppntLastName.equals(other.getAppntLastName())
            && InsuredFirstName.equals(other.getInsuredFirstName())
            && InsuredLastName.equals(other.getInsuredLastName())
            && FQGetMode.equals(other.getFQGetMode())
            && GetPeriod.equals(other.getGetPeriod())
            && GetPeriodFlag.equals(other.getGetPeriodFlag())
            && DelayPeriod.equals(other.getDelayPeriod())
            && OtherAmnt == other.getOtherAmnt()
            && Relation.equals(other.getRelation())
            && ReCusNo.equals(other.getReCusNo())
            && AutoRnewAge.equals(other.getAutoRnewAge())
            && AutoRnewYear.equals(other.getAutoRnewYear());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PolID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("GrpPolNo") ) {
            return 4;
        }
        if( strFieldName.equals("ContNo") ) {
            return 5;
        }
        if( strFieldName.equals("PolNo") ) {
            return 6;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 7;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 8;
        }
        if( strFieldName.equals("ContType") ) {
            return 9;
        }
        if( strFieldName.equals("PolTypeFlag") ) {
            return 10;
        }
        if( strFieldName.equals("MainPolNo") ) {
            return 11;
        }
        if( strFieldName.equals("MasterPolNo") ) {
            return 12;
        }
        if( strFieldName.equals("KindCode") ) {
            return 13;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 14;
        }
        if( strFieldName.equals("RiskVersion") ) {
            return 15;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 16;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 17;
        }
        if( strFieldName.equals("AgentType") ) {
            return 18;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 19;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 20;
        }
        if( strFieldName.equals("AgentCode1") ) {
            return 21;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 22;
        }
        if( strFieldName.equals("Handler") ) {
            return 23;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 24;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 25;
        }
        if( strFieldName.equals("InsuredSex") ) {
            return 26;
        }
        if( strFieldName.equals("InsuredBirthday") ) {
            return 27;
        }
        if( strFieldName.equals("InsuredAppAge") ) {
            return 28;
        }
        if( strFieldName.equals("InsuredPeoples") ) {
            return 29;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 30;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 31;
        }
        if( strFieldName.equals("AppntName") ) {
            return 32;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 33;
        }
        if( strFieldName.equals("SignCom") ) {
            return 34;
        }
        if( strFieldName.equals("SignDate") ) {
            return 35;
        }
        if( strFieldName.equals("SignTime") ) {
            return 36;
        }
        if( strFieldName.equals("FirstPayDate") ) {
            return 37;
        }
        if( strFieldName.equals("PayEndDate") ) {
            return 38;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 39;
        }
        if( strFieldName.equals("GetStartDate") ) {
            return 40;
        }
        if( strFieldName.equals("EndDate") ) {
            return 41;
        }
        if( strFieldName.equals("AcciEndDate") ) {
            return 42;
        }
        if( strFieldName.equals("GetYearFlag") ) {
            return 43;
        }
        if( strFieldName.equals("GetYear") ) {
            return 44;
        }
        if( strFieldName.equals("PayEndYearFlag") ) {
            return 45;
        }
        if( strFieldName.equals("PayEndYear") ) {
            return 46;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 47;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 48;
        }
        if( strFieldName.equals("AcciYearFlag") ) {
            return 49;
        }
        if( strFieldName.equals("AcciYear") ) {
            return 50;
        }
        if( strFieldName.equals("GetStartType") ) {
            return 51;
        }
        if( strFieldName.equals("SpecifyValiDate") ) {
            return 52;
        }
        if( strFieldName.equals("PayMode") ) {
            return 53;
        }
        if( strFieldName.equals("PayLocation") ) {
            return 54;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 55;
        }
        if( strFieldName.equals("PayYears") ) {
            return 56;
        }
        if( strFieldName.equals("Years") ) {
            return 57;
        }
        if( strFieldName.equals("ManageFeeRate") ) {
            return 58;
        }
        if( strFieldName.equals("FloatRate") ) {
            return 59;
        }
        if( strFieldName.equals("PremToAmnt") ) {
            return 60;
        }
        if( strFieldName.equals("Mult") ) {
            return 61;
        }
        if( strFieldName.equals("StandPrem") ) {
            return 62;
        }
        if( strFieldName.equals("Prem") ) {
            return 63;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 64;
        }
        if( strFieldName.equals("Amnt") ) {
            return 65;
        }
        if( strFieldName.equals("RiskAmnt") ) {
            return 66;
        }
        if( strFieldName.equals("LeavingMoney") ) {
            return 67;
        }
        if( strFieldName.equals("EndorseTimes") ) {
            return 68;
        }
        if( strFieldName.equals("ClaimTimes") ) {
            return 69;
        }
        if( strFieldName.equals("LiveTimes") ) {
            return 70;
        }
        if( strFieldName.equals("RenewCount") ) {
            return 71;
        }
        if( strFieldName.equals("LastGetDate") ) {
            return 72;
        }
        if( strFieldName.equals("LastLoanDate") ) {
            return 73;
        }
        if( strFieldName.equals("LastRegetDate") ) {
            return 74;
        }
        if( strFieldName.equals("LastEdorDate") ) {
            return 75;
        }
        if( strFieldName.equals("LastRevDate") ) {
            return 76;
        }
        if( strFieldName.equals("RnewFlag") ) {
            return 77;
        }
        if( strFieldName.equals("StopFlag") ) {
            return 78;
        }
        if( strFieldName.equals("ExpiryFlag") ) {
            return 79;
        }
        if( strFieldName.equals("AutoPayFlag") ) {
            return 80;
        }
        if( strFieldName.equals("InterestDifFlag") ) {
            return 81;
        }
        if( strFieldName.equals("SubFlag") ) {
            return 82;
        }
        if( strFieldName.equals("BnfFlag") ) {
            return 83;
        }
        if( strFieldName.equals("HealthCheckFlag") ) {
            return 84;
        }
        if( strFieldName.equals("ImpartFlag") ) {
            return 85;
        }
        if( strFieldName.equals("ReinsureFlag") ) {
            return 86;
        }
        if( strFieldName.equals("AgentPayFlag") ) {
            return 87;
        }
        if( strFieldName.equals("AgentGetFlag") ) {
            return 88;
        }
        if( strFieldName.equals("LiveGetMode") ) {
            return 89;
        }
        if( strFieldName.equals("DeadGetMode") ) {
            return 90;
        }
        if( strFieldName.equals("BonusGetMode") ) {
            return 91;
        }
        if( strFieldName.equals("BonusMan") ) {
            return 92;
        }
        if( strFieldName.equals("DeadFlag") ) {
            return 93;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 94;
        }
        if( strFieldName.equals("Remark") ) {
            return 95;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 96;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 97;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 98;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 99;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 100;
        }
        if( strFieldName.equals("UWCode") ) {
            return 101;
        }
        if( strFieldName.equals("UWDate") ) {
            return 102;
        }
        if( strFieldName.equals("UWTime") ) {
            return 103;
        }
        if( strFieldName.equals("PolApplyDate") ) {
            return 104;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 105;
        }
        if( strFieldName.equals("PolState") ) {
            return 106;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 107;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 108;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 109;
        }
        if( strFieldName.equals("Operator") ) {
            return 110;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 111;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 112;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 113;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 114;
        }
        if( strFieldName.equals("WaitPeriod") ) {
            return 115;
        }
        if( strFieldName.equals("GetForm") ) {
            return 116;
        }
        if( strFieldName.equals("GetBankCode") ) {
            return 117;
        }
        if( strFieldName.equals("GetBankAccNo") ) {
            return 118;
        }
        if( strFieldName.equals("GetAccName") ) {
            return 119;
        }
        if( strFieldName.equals("KeepValueOpt") ) {
            return 120;
        }
        if( strFieldName.equals("AscriptionRuleCode") ) {
            return 121;
        }
        if( strFieldName.equals("PayRuleCode") ) {
            return 122;
        }
        if( strFieldName.equals("AscriptionFlag") ) {
            return 123;
        }
        if( strFieldName.equals("AutoPubAccFlag") ) {
            return 124;
        }
        if( strFieldName.equals("CombiFlag") ) {
            return 125;
        }
        if( strFieldName.equals("InvestRuleCode") ) {
            return 126;
        }
        if( strFieldName.equals("UintLinkValiFlag") ) {
            return 127;
        }
        if( strFieldName.equals("ProdSetCode") ) {
            return 128;
        }
        if( strFieldName.equals("InsurPolFlag") ) {
            return 129;
        }
        if( strFieldName.equals("NewReinsureFlag") ) {
            return 130;
        }
        if( strFieldName.equals("LiveAccFlag") ) {
            return 131;
        }
        if( strFieldName.equals("StandRatePrem") ) {
            return 132;
        }
        if( strFieldName.equals("AppntFirstName") ) {
            return 133;
        }
        if( strFieldName.equals("AppntLastName") ) {
            return 134;
        }
        if( strFieldName.equals("InsuredFirstName") ) {
            return 135;
        }
        if( strFieldName.equals("InsuredLastName") ) {
            return 136;
        }
        if( strFieldName.equals("FQGetMode") ) {
            return 137;
        }
        if( strFieldName.equals("GetPeriod") ) {
            return 138;
        }
        if( strFieldName.equals("GetPeriodFlag") ) {
            return 139;
        }
        if( strFieldName.equals("DelayPeriod") ) {
            return 140;
        }
        if( strFieldName.equals("OtherAmnt") ) {
            return 141;
        }
        if( strFieldName.equals("Relation") ) {
            return 142;
        }
        if( strFieldName.equals("ReCusNo") ) {
            return 143;
        }
        if( strFieldName.equals("AutoRnewAge") ) {
            return 144;
        }
        if( strFieldName.equals("AutoRnewYear") ) {
            return 145;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PolID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "GrpPolNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "PolNo";
                break;
            case 7:
                strFieldName = "ProposalNo";
                break;
            case 8:
                strFieldName = "PrtNo";
                break;
            case 9:
                strFieldName = "ContType";
                break;
            case 10:
                strFieldName = "PolTypeFlag";
                break;
            case 11:
                strFieldName = "MainPolNo";
                break;
            case 12:
                strFieldName = "MasterPolNo";
                break;
            case 13:
                strFieldName = "KindCode";
                break;
            case 14:
                strFieldName = "RiskCode";
                break;
            case 15:
                strFieldName = "RiskVersion";
                break;
            case 16:
                strFieldName = "ManageCom";
                break;
            case 17:
                strFieldName = "AgentCom";
                break;
            case 18:
                strFieldName = "AgentType";
                break;
            case 19:
                strFieldName = "AgentCode";
                break;
            case 20:
                strFieldName = "AgentGroup";
                break;
            case 21:
                strFieldName = "AgentCode1";
                break;
            case 22:
                strFieldName = "SaleChnl";
                break;
            case 23:
                strFieldName = "Handler";
                break;
            case 24:
                strFieldName = "InsuredNo";
                break;
            case 25:
                strFieldName = "InsuredName";
                break;
            case 26:
                strFieldName = "InsuredSex";
                break;
            case 27:
                strFieldName = "InsuredBirthday";
                break;
            case 28:
                strFieldName = "InsuredAppAge";
                break;
            case 29:
                strFieldName = "InsuredPeoples";
                break;
            case 30:
                strFieldName = "OccupationType";
                break;
            case 31:
                strFieldName = "AppntNo";
                break;
            case 32:
                strFieldName = "AppntName";
                break;
            case 33:
                strFieldName = "CValiDate";
                break;
            case 34:
                strFieldName = "SignCom";
                break;
            case 35:
                strFieldName = "SignDate";
                break;
            case 36:
                strFieldName = "SignTime";
                break;
            case 37:
                strFieldName = "FirstPayDate";
                break;
            case 38:
                strFieldName = "PayEndDate";
                break;
            case 39:
                strFieldName = "PaytoDate";
                break;
            case 40:
                strFieldName = "GetStartDate";
                break;
            case 41:
                strFieldName = "EndDate";
                break;
            case 42:
                strFieldName = "AcciEndDate";
                break;
            case 43:
                strFieldName = "GetYearFlag";
                break;
            case 44:
                strFieldName = "GetYear";
                break;
            case 45:
                strFieldName = "PayEndYearFlag";
                break;
            case 46:
                strFieldName = "PayEndYear";
                break;
            case 47:
                strFieldName = "InsuYearFlag";
                break;
            case 48:
                strFieldName = "InsuYear";
                break;
            case 49:
                strFieldName = "AcciYearFlag";
                break;
            case 50:
                strFieldName = "AcciYear";
                break;
            case 51:
                strFieldName = "GetStartType";
                break;
            case 52:
                strFieldName = "SpecifyValiDate";
                break;
            case 53:
                strFieldName = "PayMode";
                break;
            case 54:
                strFieldName = "PayLocation";
                break;
            case 55:
                strFieldName = "PayIntv";
                break;
            case 56:
                strFieldName = "PayYears";
                break;
            case 57:
                strFieldName = "Years";
                break;
            case 58:
                strFieldName = "ManageFeeRate";
                break;
            case 59:
                strFieldName = "FloatRate";
                break;
            case 60:
                strFieldName = "PremToAmnt";
                break;
            case 61:
                strFieldName = "Mult";
                break;
            case 62:
                strFieldName = "StandPrem";
                break;
            case 63:
                strFieldName = "Prem";
                break;
            case 64:
                strFieldName = "SumPrem";
                break;
            case 65:
                strFieldName = "Amnt";
                break;
            case 66:
                strFieldName = "RiskAmnt";
                break;
            case 67:
                strFieldName = "LeavingMoney";
                break;
            case 68:
                strFieldName = "EndorseTimes";
                break;
            case 69:
                strFieldName = "ClaimTimes";
                break;
            case 70:
                strFieldName = "LiveTimes";
                break;
            case 71:
                strFieldName = "RenewCount";
                break;
            case 72:
                strFieldName = "LastGetDate";
                break;
            case 73:
                strFieldName = "LastLoanDate";
                break;
            case 74:
                strFieldName = "LastRegetDate";
                break;
            case 75:
                strFieldName = "LastEdorDate";
                break;
            case 76:
                strFieldName = "LastRevDate";
                break;
            case 77:
                strFieldName = "RnewFlag";
                break;
            case 78:
                strFieldName = "StopFlag";
                break;
            case 79:
                strFieldName = "ExpiryFlag";
                break;
            case 80:
                strFieldName = "AutoPayFlag";
                break;
            case 81:
                strFieldName = "InterestDifFlag";
                break;
            case 82:
                strFieldName = "SubFlag";
                break;
            case 83:
                strFieldName = "BnfFlag";
                break;
            case 84:
                strFieldName = "HealthCheckFlag";
                break;
            case 85:
                strFieldName = "ImpartFlag";
                break;
            case 86:
                strFieldName = "ReinsureFlag";
                break;
            case 87:
                strFieldName = "AgentPayFlag";
                break;
            case 88:
                strFieldName = "AgentGetFlag";
                break;
            case 89:
                strFieldName = "LiveGetMode";
                break;
            case 90:
                strFieldName = "DeadGetMode";
                break;
            case 91:
                strFieldName = "BonusGetMode";
                break;
            case 92:
                strFieldName = "BonusMan";
                break;
            case 93:
                strFieldName = "DeadFlag";
                break;
            case 94:
                strFieldName = "SmokeFlag";
                break;
            case 95:
                strFieldName = "Remark";
                break;
            case 96:
                strFieldName = "ApproveFlag";
                break;
            case 97:
                strFieldName = "ApproveCode";
                break;
            case 98:
                strFieldName = "ApproveDate";
                break;
            case 99:
                strFieldName = "ApproveTime";
                break;
            case 100:
                strFieldName = "UWFlag";
                break;
            case 101:
                strFieldName = "UWCode";
                break;
            case 102:
                strFieldName = "UWDate";
                break;
            case 103:
                strFieldName = "UWTime";
                break;
            case 104:
                strFieldName = "PolApplyDate";
                break;
            case 105:
                strFieldName = "AppFlag";
                break;
            case 106:
                strFieldName = "PolState";
                break;
            case 107:
                strFieldName = "StandbyFlag1";
                break;
            case 108:
                strFieldName = "StandbyFlag2";
                break;
            case 109:
                strFieldName = "StandbyFlag3";
                break;
            case 110:
                strFieldName = "Operator";
                break;
            case 111:
                strFieldName = "MakeDate";
                break;
            case 112:
                strFieldName = "MakeTime";
                break;
            case 113:
                strFieldName = "ModifyDate";
                break;
            case 114:
                strFieldName = "ModifyTime";
                break;
            case 115:
                strFieldName = "WaitPeriod";
                break;
            case 116:
                strFieldName = "GetForm";
                break;
            case 117:
                strFieldName = "GetBankCode";
                break;
            case 118:
                strFieldName = "GetBankAccNo";
                break;
            case 119:
                strFieldName = "GetAccName";
                break;
            case 120:
                strFieldName = "KeepValueOpt";
                break;
            case 121:
                strFieldName = "AscriptionRuleCode";
                break;
            case 122:
                strFieldName = "PayRuleCode";
                break;
            case 123:
                strFieldName = "AscriptionFlag";
                break;
            case 124:
                strFieldName = "AutoPubAccFlag";
                break;
            case 125:
                strFieldName = "CombiFlag";
                break;
            case 126:
                strFieldName = "InvestRuleCode";
                break;
            case 127:
                strFieldName = "UintLinkValiFlag";
                break;
            case 128:
                strFieldName = "ProdSetCode";
                break;
            case 129:
                strFieldName = "InsurPolFlag";
                break;
            case 130:
                strFieldName = "NewReinsureFlag";
                break;
            case 131:
                strFieldName = "LiveAccFlag";
                break;
            case 132:
                strFieldName = "StandRatePrem";
                break;
            case 133:
                strFieldName = "AppntFirstName";
                break;
            case 134:
                strFieldName = "AppntLastName";
                break;
            case 135:
                strFieldName = "InsuredFirstName";
                break;
            case 136:
                strFieldName = "InsuredLastName";
                break;
            case 137:
                strFieldName = "FQGetMode";
                break;
            case 138:
                strFieldName = "GetPeriod";
                break;
            case 139:
                strFieldName = "GetPeriodFlag";
                break;
            case 140:
                strFieldName = "DelayPeriod";
                break;
            case 141:
                strFieldName = "OtherAmnt";
                break;
            case 142:
                strFieldName = "Relation";
                break;
            case 143:
                strFieldName = "ReCusNo";
                break;
            case 144:
                strFieldName = "AutoRnewAge";
                break;
            case 145:
                strFieldName = "AutoRnewYear";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "POLID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "GRPPOLNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "POLTYPEFLAG":
                return Schema.TYPE_STRING;
            case "MAINPOLNO":
                return Schema.TYPE_STRING;
            case "MASTERPOLNO":
                return Schema.TYPE_STRING;
            case "KINDCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVERSION":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "AGENTTYPE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTCODE1":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "HANDLER":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "INSUREDSEX":
                return Schema.TYPE_STRING;
            case "INSUREDBIRTHDAY":
                return Schema.TYPE_DATE;
            case "INSUREDAPPAGE":
                return Schema.TYPE_INT;
            case "INSUREDPEOPLES":
                return Schema.TYPE_INT;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_DATE;
            case "SIGNCOM":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_DATE;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "FIRSTPAYDATE":
                return Schema.TYPE_DATE;
            case "PAYENDDATE":
                return Schema.TYPE_DATE;
            case "PAYTODATE":
                return Schema.TYPE_DATE;
            case "GETSTARTDATE":
                return Schema.TYPE_DATE;
            case "ENDDATE":
                return Schema.TYPE_DATE;
            case "ACCIENDDATE":
                return Schema.TYPE_DATE;
            case "GETYEARFLAG":
                return Schema.TYPE_STRING;
            case "GETYEAR":
                return Schema.TYPE_INT;
            case "PAYENDYEARFLAG":
                return Schema.TYPE_STRING;
            case "PAYENDYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "ACCIYEARFLAG":
                return Schema.TYPE_STRING;
            case "ACCIYEAR":
                return Schema.TYPE_INT;
            case "GETSTARTTYPE":
                return Schema.TYPE_STRING;
            case "SPECIFYVALIDATE":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "PAYLOCATION":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PAYYEARS":
                return Schema.TYPE_INT;
            case "YEARS":
                return Schema.TYPE_INT;
            case "MANAGEFEERATE":
                return Schema.TYPE_DOUBLE;
            case "FLOATRATE":
                return Schema.TYPE_DOUBLE;
            case "PREMTOAMNT":
                return Schema.TYPE_STRING;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "STANDPREM":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "RISKAMNT":
                return Schema.TYPE_DOUBLE;
            case "LEAVINGMONEY":
                return Schema.TYPE_DOUBLE;
            case "ENDORSETIMES":
                return Schema.TYPE_INT;
            case "CLAIMTIMES":
                return Schema.TYPE_INT;
            case "LIVETIMES":
                return Schema.TYPE_INT;
            case "RENEWCOUNT":
                return Schema.TYPE_INT;
            case "LASTGETDATE":
                return Schema.TYPE_DATE;
            case "LASTLOANDATE":
                return Schema.TYPE_DATE;
            case "LASTREGETDATE":
                return Schema.TYPE_DATE;
            case "LASTEDORDATE":
                return Schema.TYPE_DATE;
            case "LASTREVDATE":
                return Schema.TYPE_DATE;
            case "RNEWFLAG":
                return Schema.TYPE_INT;
            case "STOPFLAG":
                return Schema.TYPE_STRING;
            case "EXPIRYFLAG":
                return Schema.TYPE_STRING;
            case "AUTOPAYFLAG":
                return Schema.TYPE_STRING;
            case "INTERESTDIFFLAG":
                return Schema.TYPE_STRING;
            case "SUBFLAG":
                return Schema.TYPE_STRING;
            case "BNFFLAG":
                return Schema.TYPE_STRING;
            case "HEALTHCHECKFLAG":
                return Schema.TYPE_STRING;
            case "IMPARTFLAG":
                return Schema.TYPE_STRING;
            case "REINSUREFLAG":
                return Schema.TYPE_STRING;
            case "AGENTPAYFLAG":
                return Schema.TYPE_STRING;
            case "AGENTGETFLAG":
                return Schema.TYPE_STRING;
            case "LIVEGETMODE":
                return Schema.TYPE_STRING;
            case "DEADGETMODE":
                return Schema.TYPE_STRING;
            case "BONUSGETMODE":
                return Schema.TYPE_STRING;
            case "BONUSMAN":
                return Schema.TYPE_STRING;
            case "DEADFLAG":
                return Schema.TYPE_STRING;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWCODE":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_DATE;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "POLAPPLYDATE":
                return Schema.TYPE_DATE;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "POLSTATE":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "WAITPERIOD":
                return Schema.TYPE_INT;
            case "GETFORM":
                return Schema.TYPE_STRING;
            case "GETBANKCODE":
                return Schema.TYPE_STRING;
            case "GETBANKACCNO":
                return Schema.TYPE_STRING;
            case "GETACCNAME":
                return Schema.TYPE_STRING;
            case "KEEPVALUEOPT":
                return Schema.TYPE_STRING;
            case "ASCRIPTIONRULECODE":
                return Schema.TYPE_STRING;
            case "PAYRULECODE":
                return Schema.TYPE_STRING;
            case "ASCRIPTIONFLAG":
                return Schema.TYPE_STRING;
            case "AUTOPUBACCFLAG":
                return Schema.TYPE_STRING;
            case "COMBIFLAG":
                return Schema.TYPE_STRING;
            case "INVESTRULECODE":
                return Schema.TYPE_STRING;
            case "UINTLINKVALIFLAG":
                return Schema.TYPE_STRING;
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "INSURPOLFLAG":
                return Schema.TYPE_STRING;
            case "NEWREINSUREFLAG":
                return Schema.TYPE_STRING;
            case "LIVEACCFLAG":
                return Schema.TYPE_STRING;
            case "STANDRATEPREM":
                return Schema.TYPE_DOUBLE;
            case "APPNTFIRSTNAME":
                return Schema.TYPE_STRING;
            case "APPNTLASTNAME":
                return Schema.TYPE_STRING;
            case "INSUREDFIRSTNAME":
                return Schema.TYPE_STRING;
            case "INSUREDLASTNAME":
                return Schema.TYPE_STRING;
            case "FQGETMODE":
                return Schema.TYPE_STRING;
            case "GETPERIOD":
                return Schema.TYPE_STRING;
            case "GETPERIODFLAG":
                return Schema.TYPE_STRING;
            case "DELAYPERIOD":
                return Schema.TYPE_STRING;
            case "OTHERAMNT":
                return Schema.TYPE_DOUBLE;
            case "RELATION":
                return Schema.TYPE_STRING;
            case "RECUSNO":
                return Schema.TYPE_STRING;
            case "AUTORNEWAGE":
                return Schema.TYPE_STRING;
            case "AUTORNEWYEAR":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_DATE;
            case 28:
                return Schema.TYPE_INT;
            case 29:
                return Schema.TYPE_INT;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_DATE;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_DATE;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_DATE;
            case 38:
                return Schema.TYPE_DATE;
            case 39:
                return Schema.TYPE_DATE;
            case 40:
                return Schema.TYPE_DATE;
            case 41:
                return Schema.TYPE_DATE;
            case 42:
                return Schema.TYPE_DATE;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_INT;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_INT;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_INT;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_INT;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_INT;
            case 56:
                return Schema.TYPE_INT;
            case 57:
                return Schema.TYPE_INT;
            case 58:
                return Schema.TYPE_DOUBLE;
            case 59:
                return Schema.TYPE_DOUBLE;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_DOUBLE;
            case 62:
                return Schema.TYPE_DOUBLE;
            case 63:
                return Schema.TYPE_DOUBLE;
            case 64:
                return Schema.TYPE_DOUBLE;
            case 65:
                return Schema.TYPE_DOUBLE;
            case 66:
                return Schema.TYPE_DOUBLE;
            case 67:
                return Schema.TYPE_DOUBLE;
            case 68:
                return Schema.TYPE_INT;
            case 69:
                return Schema.TYPE_INT;
            case 70:
                return Schema.TYPE_INT;
            case 71:
                return Schema.TYPE_INT;
            case 72:
                return Schema.TYPE_DATE;
            case 73:
                return Schema.TYPE_DATE;
            case 74:
                return Schema.TYPE_DATE;
            case 75:
                return Schema.TYPE_DATE;
            case 76:
                return Schema.TYPE_DATE;
            case 77:
                return Schema.TYPE_INT;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_STRING;
            case 80:
                return Schema.TYPE_STRING;
            case 81:
                return Schema.TYPE_STRING;
            case 82:
                return Schema.TYPE_STRING;
            case 83:
                return Schema.TYPE_STRING;
            case 84:
                return Schema.TYPE_STRING;
            case 85:
                return Schema.TYPE_STRING;
            case 86:
                return Schema.TYPE_STRING;
            case 87:
                return Schema.TYPE_STRING;
            case 88:
                return Schema.TYPE_STRING;
            case 89:
                return Schema.TYPE_STRING;
            case 90:
                return Schema.TYPE_STRING;
            case 91:
                return Schema.TYPE_STRING;
            case 92:
                return Schema.TYPE_STRING;
            case 93:
                return Schema.TYPE_STRING;
            case 94:
                return Schema.TYPE_STRING;
            case 95:
                return Schema.TYPE_STRING;
            case 96:
                return Schema.TYPE_STRING;
            case 97:
                return Schema.TYPE_STRING;
            case 98:
                return Schema.TYPE_DATE;
            case 99:
                return Schema.TYPE_STRING;
            case 100:
                return Schema.TYPE_STRING;
            case 101:
                return Schema.TYPE_STRING;
            case 102:
                return Schema.TYPE_DATE;
            case 103:
                return Schema.TYPE_STRING;
            case 104:
                return Schema.TYPE_DATE;
            case 105:
                return Schema.TYPE_STRING;
            case 106:
                return Schema.TYPE_STRING;
            case 107:
                return Schema.TYPE_STRING;
            case 108:
                return Schema.TYPE_STRING;
            case 109:
                return Schema.TYPE_STRING;
            case 110:
                return Schema.TYPE_STRING;
            case 111:
                return Schema.TYPE_DATE;
            case 112:
                return Schema.TYPE_STRING;
            case 113:
                return Schema.TYPE_DATE;
            case 114:
                return Schema.TYPE_STRING;
            case 115:
                return Schema.TYPE_INT;
            case 116:
                return Schema.TYPE_STRING;
            case 117:
                return Schema.TYPE_STRING;
            case 118:
                return Schema.TYPE_STRING;
            case 119:
                return Schema.TYPE_STRING;
            case 120:
                return Schema.TYPE_STRING;
            case 121:
                return Schema.TYPE_STRING;
            case 122:
                return Schema.TYPE_STRING;
            case 123:
                return Schema.TYPE_STRING;
            case 124:
                return Schema.TYPE_STRING;
            case 125:
                return Schema.TYPE_STRING;
            case 126:
                return Schema.TYPE_STRING;
            case 127:
                return Schema.TYPE_STRING;
            case 128:
                return Schema.TYPE_STRING;
            case 129:
                return Schema.TYPE_STRING;
            case 130:
                return Schema.TYPE_STRING;
            case 131:
                return Schema.TYPE_STRING;
            case 132:
                return Schema.TYPE_DOUBLE;
            case 133:
                return Schema.TYPE_STRING;
            case 134:
                return Schema.TYPE_STRING;
            case 135:
                return Schema.TYPE_STRING;
            case 136:
                return Schema.TYPE_STRING;
            case 137:
                return Schema.TYPE_STRING;
            case 138:
                return Schema.TYPE_STRING;
            case 139:
                return Schema.TYPE_STRING;
            case 140:
                return Schema.TYPE_STRING;
            case 141:
                return Schema.TYPE_DOUBLE;
            case 142:
                return Schema.TYPE_STRING;
            case 143:
                return Schema.TYPE_STRING;
            case 144:
                return Schema.TYPE_STRING;
            case 145:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
