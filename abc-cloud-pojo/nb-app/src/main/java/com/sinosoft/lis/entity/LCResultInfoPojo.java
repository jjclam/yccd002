/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: lcresultinfoPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-09-05
 */
public class LCResultInfoPojo implements Pojo,Serializable {
    // @Field
    /** Resultid */
    private long Resultid;
    /** Tradecode */
    private String TradeCode;
    /** Transacno */
    private String TransacNo;
    /** Contno */
    private String ContNo;
    /** Prtno */
    private String PrtNo;
    /** Channeltype */
    private String ChannelType;
    /** Resultno */
    private String ResultNo;
    /** Resultcontent */
    private String ResultContent;
    /** Key1 */
    private String key1;
    /** Key2 */
    private String key2;
    /** Key3 */
    private String key3;
    /** Key4 */
    private String key4;


    public static final int FIELDNUM = 12;    // 数据库表的字段个数
    public long getResultid() {
        return Resultid;
    }
    public void setResultid(long aResultid) {
        Resultid = aResultid;
    }
    public void setResultid(String aResultid) {
        if (aResultid != null && !aResultid.equals("")) {
            Resultid = new Long(aResultid).longValue();
        }
    }

    public String getTradeCode() {
        return TradeCode;
    }
    public void setTradeCode(String aTradeCode) {
        TradeCode = aTradeCode;
    }
    public String getTransacNo() {
        return TransacNo;
    }
    public void setTransacNo(String aTransacNo) {
        TransacNo = aTransacNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getChannelType() {
        return ChannelType;
    }
    public void setChannelType(String aChannelType) {
        ChannelType = aChannelType;
    }
    public String getResultNo() {
        return ResultNo;
    }
    public void setResultNo(String aResultNo) {
        ResultNo = aResultNo;
    }
    public String getResultContent() {
        return ResultContent;
    }
    public void setResultContent(String aResultContent) {
        ResultContent = aResultContent;
    }
    public String getKey1() {
        return key1;
    }
    public void setKey1(String akey1) {
        key1 = akey1;
    }
    public String getKey2() {
        return key2;
    }
    public void setKey2(String akey2) {
        key2 = akey2;
    }
    public String getKey3() {
        return key3;
    }
    public void setKey3(String akey3) {
        key3 = akey3;
    }
    public String getKey4() {
        return key4;
    }
    public void setKey4(String akey4) {
        key4 = akey4;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("Resultid") ) {
            return 0;
        }
        if( strFieldName.equals("TradeCode") ) {
            return 1;
        }
        if( strFieldName.equals("TransacNo") ) {
            return 2;
        }
        if( strFieldName.equals("ContNo") ) {
            return 3;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 4;
        }
        if( strFieldName.equals("ChannelType") ) {
            return 5;
        }
        if( strFieldName.equals("ResultNo") ) {
            return 6;
        }
        if( strFieldName.equals("ResultContent") ) {
            return 7;
        }
        if( strFieldName.equals("key1") ) {
            return 8;
        }
        if( strFieldName.equals("key2") ) {
            return 9;
        }
        if( strFieldName.equals("key3") ) {
            return 10;
        }
        if( strFieldName.equals("key4") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "Resultid";
                break;
            case 1:
                strFieldName = "TradeCode";
                break;
            case 2:
                strFieldName = "TransacNo";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "PrtNo";
                break;
            case 5:
                strFieldName = "ChannelType";
                break;
            case 6:
                strFieldName = "ResultNo";
                break;
            case 7:
                strFieldName = "ResultContent";
                break;
            case 8:
                strFieldName = "key1";
                break;
            case 9:
                strFieldName = "key2";
                break;
            case 10:
                strFieldName = "key3";
                break;
            case 11:
                strFieldName = "key4";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RESULTID":
                return Schema.TYPE_LONG;
            case "TRADECODE":
                return Schema.TYPE_STRING;
            case "TRANSACNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CHANNELTYPE":
                return Schema.TYPE_STRING;
            case "RESULTNO":
                return Schema.TYPE_STRING;
            case "RESULTCONTENT":
                return Schema.TYPE_STRING;
            case "KEY1":
                return Schema.TYPE_STRING;
            case "KEY2":
                return Schema.TYPE_STRING;
            case "KEY3":
                return Schema.TYPE_STRING;
            case "KEY4":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("Resultid")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Resultid));
        }
        if (FCode.equalsIgnoreCase("TradeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TradeCode));
        }
        if (FCode.equalsIgnoreCase("TransacNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransacNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ChannelType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelType));
        }
        if (FCode.equalsIgnoreCase("ResultNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultNo));
        }
        if (FCode.equalsIgnoreCase("ResultContent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultContent));
        }
        if (FCode.equalsIgnoreCase("key1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(key1));
        }
        if (FCode.equalsIgnoreCase("key2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(key2));
        }
        if (FCode.equalsIgnoreCase("key3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(key3));
        }
        if (FCode.equalsIgnoreCase("key4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(key4));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(Resultid);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TradeCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TransacNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ChannelType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ResultNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ResultContent);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(key1);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(key2);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(key3);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(key4);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("Resultid")) {
            if( FValue != null && !FValue.equals("")) {
                Resultid = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("TradeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TradeCode = FValue.trim();
            }
            else
                TradeCode = null;
        }
        if (FCode.equalsIgnoreCase("TransacNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransacNo = FValue.trim();
            }
            else
                TransacNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ChannelType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelType = FValue.trim();
            }
            else
                ChannelType = null;
        }
        if (FCode.equalsIgnoreCase("ResultNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultNo = FValue.trim();
            }
            else
                ResultNo = null;
        }
        if (FCode.equalsIgnoreCase("ResultContent")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultContent = FValue.trim();
            }
            else
                ResultContent = null;
        }
        if (FCode.equalsIgnoreCase("key1")) {
            if( FValue != null && !FValue.equals(""))
            {
                key1 = FValue.trim();
            }
            else
                key1 = null;
        }
        if (FCode.equalsIgnoreCase("key2")) {
            if( FValue != null && !FValue.equals(""))
            {
                key2 = FValue.trim();
            }
            else
                key2 = null;
        }
        if (FCode.equalsIgnoreCase("key3")) {
            if( FValue != null && !FValue.equals(""))
            {
                key3 = FValue.trim();
            }
            else
                key3 = null;
        }
        if (FCode.equalsIgnoreCase("key4")) {
            if( FValue != null && !FValue.equals(""))
            {
                key4 = FValue.trim();
            }
            else
                key4 = null;
        }
        return true;
    }


    public String toString() {
    return "lcresultinfoPojo [" +
            "Resultid="+Resultid +
            ", TradeCode="+TradeCode +
            ", TransacNo="+TransacNo +
            ", ContNo="+ContNo +
            ", PrtNo="+PrtNo +
            ", ChannelType="+ChannelType +
            ", ResultNo="+ResultNo +
            ", ResultContent="+ResultContent +
            ", key1="+key1 +
            ", key2="+key2 +
            ", key3="+key3 +
            ", key4="+key4 +"]";
    }
}
