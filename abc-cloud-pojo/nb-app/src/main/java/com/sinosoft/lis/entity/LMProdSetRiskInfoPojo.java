/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMProdSetRiskInfoPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMProdSetRiskInfoPojo implements Pojo,Serializable {
    // @Field
    /** 产品组合编码 */
    private String ProdSetCode; 
    /** 产品组合名称 */
    private String ProdSetName; 
    /** 险种编码 */
    private String RiskCode; 
    /** 险种名称 */
    private String RiskName; 
    /** 要素类型 */
    private String FactorType; 
    /** 要素值 */
    private double FactorValue; 
    /** 主险标记 */
    private String MainRiskFlag; 
    /** 主险编码 */
    private String MainRiskCode; 
    /** 主险要素类型 */
    private String MainRiskFactorType; 
    /** 主险要素值 */
    private double MainRiskFactorValue; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** 备用字段1 */
    private String StandByFlag1; 
    /** 备用字段2 */
    private String StandByFlag2; 
    /** 备用字段3 */
    private String  StandByFlag3;
    /** 备用字段4 */
    private String  StandByFlag4;
    /** 备用字段5 */
    private double StandByFlag5; 
    /** 备用字段6 */
    private double StandByFlag6; 
    /** 保险年龄年期 */
    private int InsuYear; 
    /** 保险年龄年期标志 */
    private String InsuYearFlag; 
    /** 组合明细序号 */
    private String ProdSetInfoID; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 组合险序号 */
    private String ProdSetID; 


    public static final int FIELDNUM = 26;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getProdSetName() {
        return ProdSetName;
    }
    public void setProdSetName(String aProdSetName) {
        ProdSetName = aProdSetName;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getFactorType() {
        return FactorType;
    }
    public void setFactorType(String aFactorType) {
        FactorType = aFactorType;
    }
    public double getFactorValue() {
        return FactorValue;
    }
    public void setFactorValue(double aFactorValue) {
        FactorValue = aFactorValue;
    }
    public void setFactorValue(String aFactorValue) {
        if (aFactorValue != null && !aFactorValue.equals("")) {
            Double tDouble = new Double(aFactorValue);
            double d = tDouble.doubleValue();
            FactorValue = d;
        }
    }

    public String getMainRiskFlag() {
        return MainRiskFlag;
    }
    public void setMainRiskFlag(String aMainRiskFlag) {
        MainRiskFlag = aMainRiskFlag;
    }
    public String getMainRiskCode() {
        return MainRiskCode;
    }
    public void setMainRiskCode(String aMainRiskCode) {
        MainRiskCode = aMainRiskCode;
    }
    public String getMainRiskFactorType() {
        return MainRiskFactorType;
    }
    public void setMainRiskFactorType(String aMainRiskFactorType) {
        MainRiskFactorType = aMainRiskFactorType;
    }
    public double getMainRiskFactorValue() {
        return MainRiskFactorValue;
    }
    public void setMainRiskFactorValue(double aMainRiskFactorValue) {
        MainRiskFactorValue = aMainRiskFactorValue;
    }
    public void setMainRiskFactorValue(String aMainRiskFactorValue) {
        if (aMainRiskFactorValue != null && !aMainRiskFactorValue.equals("")) {
            Double tDouble = new Double(aMainRiskFactorValue);
            double d = tDouble.doubleValue();
            MainRiskFactorValue = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getStandByFlag2() {
        return StandByFlag2;
    }
    public void setStandByFlag2(String aStandByFlag2) {
        StandByFlag2 = aStandByFlag2;
    }
    public String getStandByFlag3() {
        return StandByFlag3;
    }
    public void setStandByFlag3(String aStandByFlag3) {
        StandByFlag3 = aStandByFlag3;
    }
    public String getStandByFlag4() {
        return StandByFlag4;
    }
    public void setStandByFlag4(String aStandByFlag4) {
        StandByFlag4 = aStandByFlag4;
    }
    public double getStandByFlag5() {
        return StandByFlag5;
    }
    public void setStandByFlag5(double aStandByFlag5) {
        StandByFlag5 = aStandByFlag5;
    }
    public void setStandByFlag5(String aStandByFlag5) {
        if (aStandByFlag5 != null && !aStandByFlag5.equals("")) {
            Double tDouble = new Double(aStandByFlag5);
            double d = tDouble.doubleValue();
            StandByFlag5 = d;
        }
    }

    public double getStandByFlag6() {
        return StandByFlag6;
    }
    public void setStandByFlag6(double aStandByFlag6) {
        StandByFlag6 = aStandByFlag6;
    }
    public void setStandByFlag6(String aStandByFlag6) {
        if (aStandByFlag6 != null && !aStandByFlag6.equals("")) {
            Double tDouble = new Double(aStandByFlag6);
            double d = tDouble.doubleValue();
            StandByFlag6 = d;
        }
    }

    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public String getProdSetInfoID() {
        return ProdSetInfoID;
    }
    public void setProdSetInfoID(String aProdSetInfoID) {
        ProdSetInfoID = aProdSetInfoID;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getProdSetID() {
        return ProdSetID;
    }
    public void setProdSetID(String aProdSetID) {
        ProdSetID = aProdSetID;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ProdSetCode") ) {
            return 0;
        }
        if( strFieldName.equals("ProdSetName") ) {
            return 1;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 2;
        }
        if( strFieldName.equals("RiskName") ) {
            return 3;
        }
        if( strFieldName.equals("FactorType") ) {
            return 4;
        }
        if( strFieldName.equals("FactorValue") ) {
            return 5;
        }
        if( strFieldName.equals("MainRiskFlag") ) {
            return 6;
        }
        if( strFieldName.equals("MainRiskCode") ) {
            return 7;
        }
        if( strFieldName.equals("MainRiskFactorType") ) {
            return 8;
        }
        if( strFieldName.equals("MainRiskFactorValue") ) {
            return 9;
        }
        if( strFieldName.equals("Operator") ) {
            return 10;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 11;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 12;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 13;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 14;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 15;
        }
        if( strFieldName.equals("StandByFlag2") ) {
            return 16;
        }
        if( strFieldName.equals("StandByFlag3") ) {
            return 17;
        }
        if( strFieldName.equals("StandByFlag4") ) {
            return 18;
        }
        if( strFieldName.equals("StandByFlag5") ) {
            return 19;
        }
        if( strFieldName.equals("StandByFlag6") ) {
            return 20;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 21;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 22;
        }
        if( strFieldName.equals("ProdSetInfoID") ) {
            return 23;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 24;
        }
        if( strFieldName.equals("ProdSetID") ) {
            return 25;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ProdSetCode";
                break;
            case 1:
                strFieldName = "ProdSetName";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "RiskName";
                break;
            case 4:
                strFieldName = "FactorType";
                break;
            case 5:
                strFieldName = "FactorValue";
                break;
            case 6:
                strFieldName = "MainRiskFlag";
                break;
            case 7:
                strFieldName = "MainRiskCode";
                break;
            case 8:
                strFieldName = "MainRiskFactorType";
                break;
            case 9:
                strFieldName = "MainRiskFactorValue";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            case 15:
                strFieldName = "StandByFlag1";
                break;
            case 16:
                strFieldName = "StandByFlag2";
                break;
            case 17:
                strFieldName = "StandByFlag3";
                break;
            case 18:
                strFieldName = "StandByFlag4";
                break;
            case 19:
                strFieldName = "StandByFlag5";
                break;
            case 20:
                strFieldName = "StandByFlag6";
                break;
            case 21:
                strFieldName = "InsuYear";
                break;
            case 22:
                strFieldName = "InsuYearFlag";
                break;
            case 23:
                strFieldName = "ProdSetInfoID";
                break;
            case 24:
                strFieldName = "SaleChnl";
                break;
            case 25:
                strFieldName = "ProdSetID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "PRODSETNAME":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "FACTORTYPE":
                return Schema.TYPE_STRING;
            case "FACTORVALUE":
                return Schema.TYPE_DOUBLE;
            case "MAINRISKFLAG":
                return Schema.TYPE_STRING;
            case "MAINRISKCODE":
                return Schema.TYPE_STRING;
            case "MAINRISKFACTORTYPE":
                return Schema.TYPE_STRING;
            case "MAINRISKFACTORVALUE":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG6":
                return Schema.TYPE_DOUBLE;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "PRODSETINFOID":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "PRODSETID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetName));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("FactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorType));
        }
        if (FCode.equalsIgnoreCase("FactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorValue));
        }
        if (FCode.equalsIgnoreCase("MainRiskFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskFlag));
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskCode));
        }
        if (FCode.equalsIgnoreCase("MainRiskFactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskFactorType));
        }
        if (FCode.equalsIgnoreCase("MainRiskFactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskFactorValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag3));
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag4));
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag5));
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag6));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("ProdSetInfoID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetInfoID));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ProdSetCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ProdSetName);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 3:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 4:
                strFieldValue = String.valueOf(FactorType);
                break;
            case 5:
                strFieldValue = String.valueOf(FactorValue);
                break;
            case 6:
                strFieldValue = String.valueOf(MainRiskFlag);
                break;
            case 7:
                strFieldValue = String.valueOf(MainRiskCode);
                break;
            case 8:
                strFieldValue = String.valueOf(MainRiskFactorType);
                break;
            case 9:
                strFieldValue = String.valueOf(MainRiskFactorValue);
                break;
            case 10:
                strFieldValue = String.valueOf(Operator);
                break;
            case 11:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 12:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 13:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 14:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 15:
                strFieldValue = String.valueOf(StandByFlag1);
                break;
            case 16:
                strFieldValue = String.valueOf(StandByFlag2);
                break;
            case 17:
                strFieldValue = String.valueOf(StandByFlag3);
                break;
            case 18:
                strFieldValue = String.valueOf(StandByFlag4);
                break;
            case 19:
                strFieldValue = String.valueOf(StandByFlag5);
                break;
            case 20:
                strFieldValue = String.valueOf(StandByFlag6);
                break;
            case 21:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 22:
                strFieldValue = String.valueOf(InsuYearFlag);
                break;
            case 23:
                strFieldValue = String.valueOf(ProdSetInfoID);
                break;
            case 24:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 25:
                strFieldValue = String.valueOf(ProdSetID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetName = FValue.trim();
            }
            else
                ProdSetName = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("FactorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorType = FValue.trim();
            }
            else
                FactorType = null;
        }
        if (FCode.equalsIgnoreCase("FactorValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FactorValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("MainRiskFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskFlag = FValue.trim();
            }
            else
                MainRiskFlag = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskCode = FValue.trim();
            }
            else
                MainRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskFactorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskFactorType = FValue.trim();
            }
            else
                MainRiskFactorType = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskFactorValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MainRiskFactorValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag2 = FValue.trim();
            }
            else
                StandByFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag3 = FValue.trim();
            }
            else
                StandByFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag4 = FValue.trim();
            }
            else
                StandByFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandByFlag5 = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandByFlag6 = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetInfoID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetInfoID = FValue.trim();
            }
            else
                ProdSetInfoID = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetID = FValue.trim();
            }
            else
                ProdSetID = null;
        }
        return true;
    }


    public String toString() {
    return "LMProdSetRiskInfoPojo [" +
            "ProdSetCode="+ProdSetCode +
            ", ProdSetName="+ProdSetName +
            ", RiskCode="+RiskCode +
            ", RiskName="+RiskName +
            ", FactorType="+FactorType +
            ", FactorValue="+FactorValue +
            ", MainRiskFlag="+MainRiskFlag +
            ", MainRiskCode="+MainRiskCode +
            ", MainRiskFactorType="+MainRiskFactorType +
            ", MainRiskFactorValue="+MainRiskFactorValue +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", StandByFlag1="+StandByFlag1 +
            ", StandByFlag2="+StandByFlag2 +
            ", StandByFlag3="+StandByFlag3 +
            ", StandByFlag4="+StandByFlag4 +
            ", StandByFlag5="+StandByFlag5 +
            ", StandByFlag6="+StandByFlag6 +
            ", InsuYear="+InsuYear +
            ", InsuYearFlag="+InsuYearFlag +
            ", ProdSetInfoID="+ProdSetInfoID +
            ", SaleChnl="+SaleChnl +
            ", ProdSetID="+ProdSetID +"]";
    }
}
