/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMCalFactorPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMCalFactorPojo implements Pojo,Serializable {
    // @Field
    /** 算法编码 */
    private String CalCode; 
    /** 要素编码 */
    private String FactorCode; 
    /** 要素名称 */
    private String FactorName; 
    /** 要素类型 */
    private String FactorType; 
    /** 要素优先级 */
    private String FactorGrade; 
    /** 要素算法编码 */
    private String FactorCalCode; 
    /** 默认值 */
    private String FactorDefault; 
    /** 要素声明类型 */
    private String FactorClass; 


    public static final int FIELDNUM = 8;    // 数据库表的字段个数
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public String getFactorCode() {
        return FactorCode;
    }
    public void setFactorCode(String aFactorCode) {
        FactorCode = aFactorCode;
    }
    public String getFactorName() {
        return FactorName;
    }
    public void setFactorName(String aFactorName) {
        FactorName = aFactorName;
    }
    public String getFactorType() {
        return FactorType;
    }
    public void setFactorType(String aFactorType) {
        FactorType = aFactorType;
    }
    public String getFactorGrade() {
        return FactorGrade;
    }
    public void setFactorGrade(String aFactorGrade) {
        FactorGrade = aFactorGrade;
    }
    public String getFactorCalCode() {
        return FactorCalCode;
    }
    public void setFactorCalCode(String aFactorCalCode) {
        FactorCalCode = aFactorCalCode;
    }
    public String getFactorDefault() {
        return FactorDefault;
    }
    public void setFactorDefault(String aFactorDefault) {
        FactorDefault = aFactorDefault;
    }
    public String getFactorClass() {
        return FactorClass;
    }
    public void setFactorClass(String aFactorClass) {
        FactorClass = aFactorClass;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CalCode") ) {
            return 0;
        }
        if( strFieldName.equals("FactorCode") ) {
            return 1;
        }
        if( strFieldName.equals("FactorName") ) {
            return 2;
        }
        if( strFieldName.equals("FactorType") ) {
            return 3;
        }
        if( strFieldName.equals("FactorGrade") ) {
            return 4;
        }
        if( strFieldName.equals("FactorCalCode") ) {
            return 5;
        }
        if( strFieldName.equals("FactorDefault") ) {
            return 6;
        }
        if( strFieldName.equals("FactorClass") ) {
            return 7;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CalCode";
                break;
            case 1:
                strFieldName = "FactorCode";
                break;
            case 2:
                strFieldName = "FactorName";
                break;
            case 3:
                strFieldName = "FactorType";
                break;
            case 4:
                strFieldName = "FactorGrade";
                break;
            case 5:
                strFieldName = "FactorCalCode";
                break;
            case 6:
                strFieldName = "FactorDefault";
                break;
            case 7:
                strFieldName = "FactorClass";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "FACTORCODE":
                return Schema.TYPE_STRING;
            case "FACTORNAME":
                return Schema.TYPE_STRING;
            case "FACTORTYPE":
                return Schema.TYPE_STRING;
            case "FACTORGRADE":
                return Schema.TYPE_STRING;
            case "FACTORCALCODE":
                return Schema.TYPE_STRING;
            case "FACTORDEFAULT":
                return Schema.TYPE_STRING;
            case "FACTORCLASS":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("FactorCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorCode));
        }
        if (FCode.equalsIgnoreCase("FactorName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorName));
        }
        if (FCode.equalsIgnoreCase("FactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorType));
        }
        if (FCode.equalsIgnoreCase("FactorGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorGrade));
        }
        if (FCode.equalsIgnoreCase("FactorCalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorCalCode));
        }
        if (FCode.equalsIgnoreCase("FactorDefault")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorDefault));
        }
        if (FCode.equalsIgnoreCase("FactorClass")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorClass));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 1:
                strFieldValue = String.valueOf(FactorCode);
                break;
            case 2:
                strFieldValue = String.valueOf(FactorName);
                break;
            case 3:
                strFieldValue = String.valueOf(FactorType);
                break;
            case 4:
                strFieldValue = String.valueOf(FactorGrade);
                break;
            case 5:
                strFieldValue = String.valueOf(FactorCalCode);
                break;
            case 6:
                strFieldValue = String.valueOf(FactorDefault);
                break;
            case 7:
                strFieldValue = String.valueOf(FactorClass);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("FactorCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorCode = FValue.trim();
            }
            else
                FactorCode = null;
        }
        if (FCode.equalsIgnoreCase("FactorName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorName = FValue.trim();
            }
            else
                FactorName = null;
        }
        if (FCode.equalsIgnoreCase("FactorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorType = FValue.trim();
            }
            else
                FactorType = null;
        }
        if (FCode.equalsIgnoreCase("FactorGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorGrade = FValue.trim();
            }
            else
                FactorGrade = null;
        }
        if (FCode.equalsIgnoreCase("FactorCalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorCalCode = FValue.trim();
            }
            else
                FactorCalCode = null;
        }
        if (FCode.equalsIgnoreCase("FactorDefault")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorDefault = FValue.trim();
            }
            else
                FactorDefault = null;
        }
        if (FCode.equalsIgnoreCase("FactorClass")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorClass = FValue.trim();
            }
            else
                FactorClass = null;
        }
        return true;
    }


    public String toString() {
    return "LMCalFactorPojo [" +
            "CalCode="+CalCode +
            ", FactorCode="+FactorCode +
            ", FactorName="+FactorName +
            ", FactorType="+FactorType +
            ", FactorGrade="+FactorGrade +
            ", FactorCalCode="+FactorCalCode +
            ", FactorDefault="+FactorDefault +
            ", FactorClass="+FactorClass +"]";
    }
}
