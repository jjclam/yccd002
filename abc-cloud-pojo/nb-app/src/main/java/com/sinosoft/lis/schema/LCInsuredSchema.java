/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCInsuredSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LCInsuredSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long InsuredID;
    /** Fk_lccont */
    private long ContID;
    /** Fk_ldperson */
    private long PersonID;
    /** Shardingid */
    private String ShardingID;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 被保人客户号 */
    private String InsuredNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 管理机构 */
    private String ManageCom;
    /** 处理机构 */
    private String ExecuteCom;
    /** 家庭保障号 */
    private String FamilyID;
    /** 与主被保人关系 */
    private String RelationToMainInsured;
    /** 与投保人关系 */
    private String RelationToAppnt;
    /** 客户地址号码 */
    private String AddressNo;
    /** 客户内部号码 */
    private String SequenceNo;
    /** 被保人名称 */
    private String Name;
    /** 被保人性别 */
    private String Sex;
    /** 被保人出生日期 */
    private Date Birthday;
    /** 证件类型 */
    private String IDType;
    /** 证件号码 */
    private String IDNo;
    /** 国籍 */
    private String NativePlace;
    /** 民族 */
    private String Nationality;
    /** 户口所在地 */
    private String RgtAddress;
    /** 婚姻状况 */
    private String Marriage;
    /** 结婚日期 */
    private Date MarriageDate;
    /** 健康状况 */
    private String Health;
    /** 身高 */
    private double Stature;
    /** 体重 */
    private double Avoirdupois;
    /** 学历 */
    private String Degree;
    /** 信用等级 */
    private String CreditGrade;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 银行帐户名 */
    private String AccName;
    /** 入司日期 */
    private Date JoinCompanyDate;
    /** 参加工作日期 */
    private Date StartWorkDate;
    /** 职位 */
    private String Position;
    /** 工资 */
    private double Salary;
    /** 职业类别 */
    private String OccupationType;
    /** 职业代码 */
    private String OccupationCode;
    /** 职业（工种） */
    private String WorkType;
    /** 兼职（工种） */
    private String PluralityType;
    /** 是否吸烟标志 */
    private String SmokeFlag;
    /** 保险计划编码 */
    private String ContPlanCode;
    /** 操作员 */
    private String Operator;
    /** 被保人状态 */
    private String InsuredStat;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 核保状态 */
    private String UWFlag;
    /** 最终核保人编码 */
    private String UWCode;
    /** 核保完成日期 */
    private Date UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 身体指标 */
    private double BMI;
    /** 被保人数目 */
    private int InsuredPeoples;
    /** 驾照 */
    private String License;
    /** 驾照类型 */
    private String LicenseType;
    /** 团体客户序号 */
    private int CustomerSeqNo;
    /** 工作号 */
    private String WorkNo;
    /** 社保登记号 */
    private String SocialInsuNo;
    /** 职业描述 */
    private String OccupationDesb;
    /** 证件有效期 */
    private String IdValiDate;
    /** 是否有摩托车驾照 */
    private String HaveMotorcycleLicence;
    /** 兼职 */
    private String PartTimeJob;
    /** 医保标识 */
    private String HealthFlag;
    /** 在职标识 */
    private String ServiceMark;
    /** Firstname */
    private String FirstName;
    /** Lastname */
    private String LastName;
    /** 社保标记 */
    private String SSFlag;
    /** 美国纳税人识别号 */
    private String TINNO;
    /** 是否为美国纳税义务的个人 */
    private String TINFlag;
    /** 被保险人类型 */
    private String InsuredType;
    /** 证件是否长期 */
    private String IsLongValid;
    /** 证件开始日期 */
    private Date IDStartDate;

    public static final int FIELDNUM = 75;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCInsuredSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "InsuredID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCInsuredSchema cloned = (LCInsuredSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getInsuredID() {
        return InsuredID;
    }
    public void setInsuredID(long aInsuredID) {
        InsuredID = aInsuredID;
    }
    public void setInsuredID(String aInsuredID) {
        if (aInsuredID != null && !aInsuredID.equals("")) {
            InsuredID = new Long(aInsuredID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public long getPersonID() {
        return PersonID;
    }
    public void setPersonID(long aPersonID) {
        PersonID = aPersonID;
    }
    public void setPersonID(String aPersonID) {
        if (aPersonID != null && !aPersonID.equals("")) {
            PersonID = new Long(aPersonID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getExecuteCom() {
        return ExecuteCom;
    }
    public void setExecuteCom(String aExecuteCom) {
        ExecuteCom = aExecuteCom;
    }
    public String getFamilyID() {
        return FamilyID;
    }
    public void setFamilyID(String aFamilyID) {
        FamilyID = aFamilyID;
    }
    public String getRelationToMainInsured() {
        return RelationToMainInsured;
    }
    public void setRelationToMainInsured(String aRelationToMainInsured) {
        RelationToMainInsured = aRelationToMainInsured;
    }
    public String getRelationToAppnt() {
        return RelationToAppnt;
    }
    public void setRelationToAppnt(String aRelationToAppnt) {
        RelationToAppnt = aRelationToAppnt;
    }
    public String getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }
    public String getSequenceNo() {
        return SequenceNo;
    }
    public void setSequenceNo(String aSequenceNo) {
        SequenceNo = aSequenceNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        if(Birthday != null) {
            return fDate.getString(Birthday);
        } else {
            return null;
        }
    }
    public void setBirthday(Date aBirthday) {
        Birthday = aBirthday;
    }
    public void setBirthday(String aBirthday) {
        if (aBirthday != null && !aBirthday.equals("")) {
            Birthday = fDate.getDate(aBirthday);
        } else
            Birthday = null;
    }

    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getMarriageDate() {
        if(MarriageDate != null) {
            return fDate.getString(MarriageDate);
        } else {
            return null;
        }
    }
    public void setMarriageDate(Date aMarriageDate) {
        MarriageDate = aMarriageDate;
    }
    public void setMarriageDate(String aMarriageDate) {
        if (aMarriageDate != null && !aMarriageDate.equals("")) {
            MarriageDate = fDate.getDate(aMarriageDate);
        } else
            MarriageDate = null;
    }

    public String getHealth() {
        return Health;
    }
    public void setHealth(String aHealth) {
        Health = aHealth;
    }
    public double getStature() {
        return Stature;
    }
    public void setStature(double aStature) {
        Stature = aStature;
    }
    public void setStature(String aStature) {
        if (aStature != null && !aStature.equals("")) {
            Double tDouble = new Double(aStature);
            double d = tDouble.doubleValue();
            Stature = d;
        }
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }
    public void setAvoirdupois(double aAvoirdupois) {
        Avoirdupois = aAvoirdupois;
    }
    public void setAvoirdupois(String aAvoirdupois) {
        if (aAvoirdupois != null && !aAvoirdupois.equals("")) {
            Double tDouble = new Double(aAvoirdupois);
            double d = tDouble.doubleValue();
            Avoirdupois = d;
        }
    }

    public String getDegree() {
        return Degree;
    }
    public void setDegree(String aDegree) {
        Degree = aDegree;
    }
    public String getCreditGrade() {
        return CreditGrade;
    }
    public void setCreditGrade(String aCreditGrade) {
        CreditGrade = aCreditGrade;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getJoinCompanyDate() {
        if(JoinCompanyDate != null) {
            return fDate.getString(JoinCompanyDate);
        } else {
            return null;
        }
    }
    public void setJoinCompanyDate(Date aJoinCompanyDate) {
        JoinCompanyDate = aJoinCompanyDate;
    }
    public void setJoinCompanyDate(String aJoinCompanyDate) {
        if (aJoinCompanyDate != null && !aJoinCompanyDate.equals("")) {
            JoinCompanyDate = fDate.getDate(aJoinCompanyDate);
        } else
            JoinCompanyDate = null;
    }

    public String getStartWorkDate() {
        if(StartWorkDate != null) {
            return fDate.getString(StartWorkDate);
        } else {
            return null;
        }
    }
    public void setStartWorkDate(Date aStartWorkDate) {
        StartWorkDate = aStartWorkDate;
    }
    public void setStartWorkDate(String aStartWorkDate) {
        if (aStartWorkDate != null && !aStartWorkDate.equals("")) {
            StartWorkDate = fDate.getDate(aStartWorkDate);
        } else
            StartWorkDate = null;
    }

    public String getPosition() {
        return Position;
    }
    public void setPosition(String aPosition) {
        Position = aPosition;
    }
    public double getSalary() {
        return Salary;
    }
    public void setSalary(double aSalary) {
        Salary = aSalary;
    }
    public void setSalary(String aSalary) {
        if (aSalary != null && !aSalary.equals("")) {
            Double tDouble = new Double(aSalary);
            double d = tDouble.doubleValue();
            Salary = d;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getWorkType() {
        return WorkType;
    }
    public void setWorkType(String aWorkType) {
        WorkType = aWorkType;
    }
    public String getPluralityType() {
        return PluralityType;
    }
    public void setPluralityType(String aPluralityType) {
        PluralityType = aPluralityType;
    }
    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getContPlanCode() {
        return ContPlanCode;
    }
    public void setContPlanCode(String aContPlanCode) {
        ContPlanCode = aContPlanCode;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getInsuredStat() {
        return InsuredStat;
    }
    public void setInsuredStat(String aInsuredStat) {
        InsuredStat = aInsuredStat;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWCode() {
        return UWCode;
    }
    public void setUWCode(String aUWCode) {
        UWCode = aUWCode;
    }
    public String getUWDate() {
        if(UWDate != null) {
            return fDate.getString(UWDate);
        } else {
            return null;
        }
    }
    public void setUWDate(Date aUWDate) {
        UWDate = aUWDate;
    }
    public void setUWDate(String aUWDate) {
        if (aUWDate != null && !aUWDate.equals("")) {
            UWDate = fDate.getDate(aUWDate);
        } else
            UWDate = null;
    }

    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public double getBMI() {
        return BMI;
    }
    public void setBMI(double aBMI) {
        BMI = aBMI;
    }
    public void setBMI(String aBMI) {
        if (aBMI != null && !aBMI.equals("")) {
            Double tDouble = new Double(aBMI);
            double d = tDouble.doubleValue();
            BMI = d;
        }
    }

    public int getInsuredPeoples() {
        return InsuredPeoples;
    }
    public void setInsuredPeoples(int aInsuredPeoples) {
        InsuredPeoples = aInsuredPeoples;
    }
    public void setInsuredPeoples(String aInsuredPeoples) {
        if (aInsuredPeoples != null && !aInsuredPeoples.equals("")) {
            Integer tInteger = new Integer(aInsuredPeoples);
            int i = tInteger.intValue();
            InsuredPeoples = i;
        }
    }

    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getLicenseType() {
        return LicenseType;
    }
    public void setLicenseType(String aLicenseType) {
        LicenseType = aLicenseType;
    }
    public int getCustomerSeqNo() {
        return CustomerSeqNo;
    }
    public void setCustomerSeqNo(int aCustomerSeqNo) {
        CustomerSeqNo = aCustomerSeqNo;
    }
    public void setCustomerSeqNo(String aCustomerSeqNo) {
        if (aCustomerSeqNo != null && !aCustomerSeqNo.equals("")) {
            Integer tInteger = new Integer(aCustomerSeqNo);
            int i = tInteger.intValue();
            CustomerSeqNo = i;
        }
    }

    public String getWorkNo() {
        return WorkNo;
    }
    public void setWorkNo(String aWorkNo) {
        WorkNo = aWorkNo;
    }
    public String getSocialInsuNo() {
        return SocialInsuNo;
    }
    public void setSocialInsuNo(String aSocialInsuNo) {
        SocialInsuNo = aSocialInsuNo;
    }
    public String getOccupationDesb() {
        return OccupationDesb;
    }
    public void setOccupationDesb(String aOccupationDesb) {
        OccupationDesb = aOccupationDesb;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }
    public void setHaveMotorcycleLicence(String aHaveMotorcycleLicence) {
        HaveMotorcycleLicence = aHaveMotorcycleLicence;
    }
    public String getPartTimeJob() {
        return PartTimeJob;
    }
    public void setPartTimeJob(String aPartTimeJob) {
        PartTimeJob = aPartTimeJob;
    }
    public String getHealthFlag() {
        return HealthFlag;
    }
    public void setHealthFlag(String aHealthFlag) {
        HealthFlag = aHealthFlag;
    }
    public String getServiceMark() {
        return ServiceMark;
    }
    public void setServiceMark(String aServiceMark) {
        ServiceMark = aServiceMark;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String aFirstName) {
        FirstName = aFirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String aLastName) {
        LastName = aLastName;
    }
    public String getSSFlag() {
        return SSFlag;
    }
    public void setSSFlag(String aSSFlag) {
        SSFlag = aSSFlag;
    }
    public String getTINNO() {
        return TINNO;
    }
    public void setTINNO(String aTINNO) {
        TINNO = aTINNO;
    }
    public String getTINFlag() {
        return TINFlag;
    }
    public void setTINFlag(String aTINFlag) {
        TINFlag = aTINFlag;
    }
    public String getInsuredType() {
        return InsuredType;
    }
    public void setInsuredType(String aInsuredType) {
        InsuredType = aInsuredType;
    }
    public String getIsLongValid() {
        return IsLongValid;
    }
    public void setIsLongValid(String aIsLongValid) {
        IsLongValid = aIsLongValid;
    }
    public String getIDStartDate() {
        if(IDStartDate != null) {
            return fDate.getString(IDStartDate);
        } else {
            return null;
        }
    }
    public void setIDStartDate(Date aIDStartDate) {
        IDStartDate = aIDStartDate;
    }
    public void setIDStartDate(String aIDStartDate) {
        if (aIDStartDate != null && !aIDStartDate.equals("")) {
            IDStartDate = fDate.getDate(aIDStartDate);
        } else
            IDStartDate = null;
    }


    /**
    * 使用另外一个 LCInsuredSchema 对象给 Schema 赋值
    * @param: aLCInsuredSchema LCInsuredSchema
    **/
    public void setSchema(LCInsuredSchema aLCInsuredSchema) {
        this.InsuredID = aLCInsuredSchema.getInsuredID();
        this.ContID = aLCInsuredSchema.getContID();
        this.PersonID = aLCInsuredSchema.getPersonID();
        this.ShardingID = aLCInsuredSchema.getShardingID();
        this.GrpContNo = aLCInsuredSchema.getGrpContNo();
        this.ContNo = aLCInsuredSchema.getContNo();
        this.InsuredNo = aLCInsuredSchema.getInsuredNo();
        this.PrtNo = aLCInsuredSchema.getPrtNo();
        this.AppntNo = aLCInsuredSchema.getAppntNo();
        this.ManageCom = aLCInsuredSchema.getManageCom();
        this.ExecuteCom = aLCInsuredSchema.getExecuteCom();
        this.FamilyID = aLCInsuredSchema.getFamilyID();
        this.RelationToMainInsured = aLCInsuredSchema.getRelationToMainInsured();
        this.RelationToAppnt = aLCInsuredSchema.getRelationToAppnt();
        this.AddressNo = aLCInsuredSchema.getAddressNo();
        this.SequenceNo = aLCInsuredSchema.getSequenceNo();
        this.Name = aLCInsuredSchema.getName();
        this.Sex = aLCInsuredSchema.getSex();
        this.Birthday = fDate.getDate( aLCInsuredSchema.getBirthday());
        this.IDType = aLCInsuredSchema.getIDType();
        this.IDNo = aLCInsuredSchema.getIDNo();
        this.NativePlace = aLCInsuredSchema.getNativePlace();
        this.Nationality = aLCInsuredSchema.getNationality();
        this.RgtAddress = aLCInsuredSchema.getRgtAddress();
        this.Marriage = aLCInsuredSchema.getMarriage();
        this.MarriageDate = fDate.getDate( aLCInsuredSchema.getMarriageDate());
        this.Health = aLCInsuredSchema.getHealth();
        this.Stature = aLCInsuredSchema.getStature();
        this.Avoirdupois = aLCInsuredSchema.getAvoirdupois();
        this.Degree = aLCInsuredSchema.getDegree();
        this.CreditGrade = aLCInsuredSchema.getCreditGrade();
        this.BankCode = aLCInsuredSchema.getBankCode();
        this.BankAccNo = aLCInsuredSchema.getBankAccNo();
        this.AccName = aLCInsuredSchema.getAccName();
        this.JoinCompanyDate = fDate.getDate( aLCInsuredSchema.getJoinCompanyDate());
        this.StartWorkDate = fDate.getDate( aLCInsuredSchema.getStartWorkDate());
        this.Position = aLCInsuredSchema.getPosition();
        this.Salary = aLCInsuredSchema.getSalary();
        this.OccupationType = aLCInsuredSchema.getOccupationType();
        this.OccupationCode = aLCInsuredSchema.getOccupationCode();
        this.WorkType = aLCInsuredSchema.getWorkType();
        this.PluralityType = aLCInsuredSchema.getPluralityType();
        this.SmokeFlag = aLCInsuredSchema.getSmokeFlag();
        this.ContPlanCode = aLCInsuredSchema.getContPlanCode();
        this.Operator = aLCInsuredSchema.getOperator();
        this.InsuredStat = aLCInsuredSchema.getInsuredStat();
        this.MakeDate = fDate.getDate( aLCInsuredSchema.getMakeDate());
        this.MakeTime = aLCInsuredSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCInsuredSchema.getModifyDate());
        this.ModifyTime = aLCInsuredSchema.getModifyTime();
        this.UWFlag = aLCInsuredSchema.getUWFlag();
        this.UWCode = aLCInsuredSchema.getUWCode();
        this.UWDate = fDate.getDate( aLCInsuredSchema.getUWDate());
        this.UWTime = aLCInsuredSchema.getUWTime();
        this.BMI = aLCInsuredSchema.getBMI();
        this.InsuredPeoples = aLCInsuredSchema.getInsuredPeoples();
        this.License = aLCInsuredSchema.getLicense();
        this.LicenseType = aLCInsuredSchema.getLicenseType();
        this.CustomerSeqNo = aLCInsuredSchema.getCustomerSeqNo();
        this.WorkNo = aLCInsuredSchema.getWorkNo();
        this.SocialInsuNo = aLCInsuredSchema.getSocialInsuNo();
        this.OccupationDesb = aLCInsuredSchema.getOccupationDesb();
        this.IdValiDate = aLCInsuredSchema.getIdValiDate();
        this.HaveMotorcycleLicence = aLCInsuredSchema.getHaveMotorcycleLicence();
        this.PartTimeJob = aLCInsuredSchema.getPartTimeJob();
        this.HealthFlag = aLCInsuredSchema.getHealthFlag();
        this.ServiceMark = aLCInsuredSchema.getServiceMark();
        this.FirstName = aLCInsuredSchema.getFirstName();
        this.LastName = aLCInsuredSchema.getLastName();
        this.SSFlag = aLCInsuredSchema.getSSFlag();
        this.TINNO = aLCInsuredSchema.getTINNO();
        this.TINFlag = aLCInsuredSchema.getTINFlag();
        this.InsuredType = aLCInsuredSchema.getInsuredType();
        this.IsLongValid = aLCInsuredSchema.getIsLongValid();
        this.IDStartDate = fDate.getDate( aLCInsuredSchema.getIDStartDate());
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.InsuredID = rs.getLong("InsuredID");
            this.ContID = rs.getLong("ContID");
            this.PersonID = rs.getLong("PersonID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("ExecuteCom") == null )
                this.ExecuteCom = null;
            else
                this.ExecuteCom = rs.getString("ExecuteCom").trim();

            if( rs.getString("FamilyID") == null )
                this.FamilyID = null;
            else
                this.FamilyID = rs.getString("FamilyID").trim();

            if( rs.getString("RelationToMainInsured") == null )
                this.RelationToMainInsured = null;
            else
                this.RelationToMainInsured = rs.getString("RelationToMainInsured").trim();

            if( rs.getString("RelationToAppnt") == null )
                this.RelationToAppnt = null;
            else
                this.RelationToAppnt = rs.getString("RelationToAppnt").trim();

            if( rs.getString("AddressNo") == null )
                this.AddressNo = null;
            else
                this.AddressNo = rs.getString("AddressNo").trim();

            if( rs.getString("SequenceNo") == null )
                this.SequenceNo = null;
            else
                this.SequenceNo = rs.getString("SequenceNo").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            this.Birthday = rs.getDate("Birthday");
            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            if( rs.getString("NativePlace") == null )
                this.NativePlace = null;
            else
                this.NativePlace = rs.getString("NativePlace").trim();

            if( rs.getString("Nationality") == null )
                this.Nationality = null;
            else
                this.Nationality = rs.getString("Nationality").trim();

            if( rs.getString("RgtAddress") == null )
                this.RgtAddress = null;
            else
                this.RgtAddress = rs.getString("RgtAddress").trim();

            if( rs.getString("Marriage") == null )
                this.Marriage = null;
            else
                this.Marriage = rs.getString("Marriage").trim();

            this.MarriageDate = rs.getDate("MarriageDate");
            if( rs.getString("Health") == null )
                this.Health = null;
            else
                this.Health = rs.getString("Health").trim();

            this.Stature = rs.getDouble("Stature");
            this.Avoirdupois = rs.getDouble("Avoirdupois");
            if( rs.getString("Degree") == null )
                this.Degree = null;
            else
                this.Degree = rs.getString("Degree").trim();

            if( rs.getString("CreditGrade") == null )
                this.CreditGrade = null;
            else
                this.CreditGrade = rs.getString("CreditGrade").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

            this.JoinCompanyDate = rs.getDate("JoinCompanyDate");
            this.StartWorkDate = rs.getDate("StartWorkDate");
            if( rs.getString("Position") == null )
                this.Position = null;
            else
                this.Position = rs.getString("Position").trim();

            this.Salary = rs.getDouble("Salary");
            if( rs.getString("OccupationType") == null )
                this.OccupationType = null;
            else
                this.OccupationType = rs.getString("OccupationType").trim();

            if( rs.getString("OccupationCode") == null )
                this.OccupationCode = null;
            else
                this.OccupationCode = rs.getString("OccupationCode").trim();

            if( rs.getString("WorkType") == null )
                this.WorkType = null;
            else
                this.WorkType = rs.getString("WorkType").trim();

            if( rs.getString("PluralityType") == null )
                this.PluralityType = null;
            else
                this.PluralityType = rs.getString("PluralityType").trim();

            if( rs.getString("SmokeFlag") == null )
                this.SmokeFlag = null;
            else
                this.SmokeFlag = rs.getString("SmokeFlag").trim();

            if( rs.getString("ContPlanCode") == null )
                this.ContPlanCode = null;
            else
                this.ContPlanCode = rs.getString("ContPlanCode").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            if( rs.getString("InsuredStat") == null )
                this.InsuredStat = null;
            else
                this.InsuredStat = rs.getString("InsuredStat").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("UWFlag") == null )
                this.UWFlag = null;
            else
                this.UWFlag = rs.getString("UWFlag").trim();

            if( rs.getString("UWCode") == null )
                this.UWCode = null;
            else
                this.UWCode = rs.getString("UWCode").trim();

            this.UWDate = rs.getDate("UWDate");
            if( rs.getString("UWTime") == null )
                this.UWTime = null;
            else
                this.UWTime = rs.getString("UWTime").trim();

            this.BMI = rs.getDouble("BMI");
            this.InsuredPeoples = rs.getInt("InsuredPeoples");
            if( rs.getString("License") == null )
                this.License = null;
            else
                this.License = rs.getString("License").trim();

            if( rs.getString("LicenseType") == null )
                this.LicenseType = null;
            else
                this.LicenseType = rs.getString("LicenseType").trim();

            this.CustomerSeqNo = rs.getInt("CustomerSeqNo");
            if( rs.getString("WorkNo") == null )
                this.WorkNo = null;
            else
                this.WorkNo = rs.getString("WorkNo").trim();

            if( rs.getString("SocialInsuNo") == null )
                this.SocialInsuNo = null;
            else
                this.SocialInsuNo = rs.getString("SocialInsuNo").trim();

            if( rs.getString("OccupationDesb") == null )
                this.OccupationDesb = null;
            else
                this.OccupationDesb = rs.getString("OccupationDesb").trim();

            if( rs.getString("IdValiDate") == null )
                this.IdValiDate = null;
            else
                this.IdValiDate = rs.getString("IdValiDate").trim();

            if( rs.getString("HaveMotorcycleLicence") == null )
                this.HaveMotorcycleLicence = null;
            else
                this.HaveMotorcycleLicence = rs.getString("HaveMotorcycleLicence").trim();

            if( rs.getString("PartTimeJob") == null )
                this.PartTimeJob = null;
            else
                this.PartTimeJob = rs.getString("PartTimeJob").trim();

            if( rs.getString("HealthFlag") == null )
                this.HealthFlag = null;
            else
                this.HealthFlag = rs.getString("HealthFlag").trim();

            if( rs.getString("ServiceMark") == null )
                this.ServiceMark = null;
            else
                this.ServiceMark = rs.getString("ServiceMark").trim();

            if( rs.getString("FirstName") == null )
                this.FirstName = null;
            else
                this.FirstName = rs.getString("FirstName").trim();

            if( rs.getString("LastName") == null )
                this.LastName = null;
            else
                this.LastName = rs.getString("LastName").trim();

            if( rs.getString("SSFlag") == null )
                this.SSFlag = null;
            else
                this.SSFlag = rs.getString("SSFlag").trim();

            if( rs.getString("TINNO") == null )
                this.TINNO = null;
            else
                this.TINNO = rs.getString("TINNO").trim();

            if( rs.getString("TINFlag") == null )
                this.TINFlag = null;
            else
                this.TINFlag = rs.getString("TINFlag").trim();

            if( rs.getString("InsuredType") == null )
                this.InsuredType = null;
            else
                this.InsuredType = rs.getString("InsuredType").trim();

            if( rs.getString("IsLongValid") == null )
                this.IsLongValid = null;
            else
                this.IsLongValid = rs.getString("IsLongValid").trim();

            this.IDStartDate = rs.getDate("IDStartDate");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInsuredSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCInsuredSchema getSchema() {
        LCInsuredSchema aLCInsuredSchema = new LCInsuredSchema();
        aLCInsuredSchema.setSchema(this);
        return aLCInsuredSchema;
    }

    public LCInsuredDB getDB() {
        LCInsuredDB aDBOper = new LCInsuredDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsured描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(InsuredID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ContID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PersonID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FamilyID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToMainInsured)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToAppnt)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Marriage)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MarriageDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Health)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Stature));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Avoirdupois));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Degree)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CreditGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( JoinCompanyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartWorkDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Salary));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WorkType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PluralityType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SmokeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredStat)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BMI));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuredPeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(License)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LicenseType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CustomerSeqNo));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WorkNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SocialInsuNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationDesb)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IdValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HaveMotorcycleLicence)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PartTimeJob)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HealthFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceMark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LastName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SSFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TINNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TINFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsLongValid)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate )));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsured>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            InsuredID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ContID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2, SysConst.PACKAGESPILTER))).longValue();
            PersonID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,3, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            FamilyID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            RelationToMainInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            RelationToAppnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            SequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            RgtAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            Marriage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            MarriageDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            Health = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            Stature = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28, SysConst.PACKAGESPILTER))).doubleValue();
            Avoirdupois = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29, SysConst.PACKAGESPILTER))).doubleValue();
            Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            CreditGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            JoinCompanyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER));
            StartWorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER));
            Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            Salary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38, SysConst.PACKAGESPILTER))).doubleValue();
            OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            WorkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            PluralityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            SmokeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            InsuredStat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            UWCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
            BMI = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,55, SysConst.PACKAGESPILTER))).doubleValue();
            InsuredPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,56, SysConst.PACKAGESPILTER))).intValue();
            License = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
            LicenseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
            CustomerSeqNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,59, SysConst.PACKAGESPILTER))).intValue();
            WorkNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
            SocialInsuNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
            OccupationDesb = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
            IdValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
            HaveMotorcycleLicence = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
            PartTimeJob = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
            HealthFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
            ServiceMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
            FirstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
            LastName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
            SSFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
            TINNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
            TINFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
            InsuredType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
            IsLongValid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74, SysConst.PACKAGESPILTER );
            IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER));
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInsuredSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsuredID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("PersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PersonID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ExecuteCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
        }
        if (FCode.equalsIgnoreCase("FamilyID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyID));
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToMainInsured));
        }
        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("SequenceNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarriageDate()));
        }
        if (FCode.equalsIgnoreCase("Health")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Health));
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stature));
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Avoirdupois));
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartWorkDate()));
        }
        if (FCode.equalsIgnoreCase("Position")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("InsuredStat")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredStat));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWCode));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("BMI")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BMI));
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredPeoples));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseType));
        }
        if (FCode.equalsIgnoreCase("CustomerSeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerSeqNo));
        }
        if (FCode.equalsIgnoreCase("WorkNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkNo));
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuNo));
        }
        if (FCode.equalsIgnoreCase("OccupationDesb")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationDesb));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HaveMotorcycleLicence));
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PartTimeJob));
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthFlag));
        }
        if (FCode.equalsIgnoreCase("ServiceMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceMark));
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstName));
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastName));
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SSFlag));
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINNO));
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINFlag));
        }
        if (FCode.equalsIgnoreCase("InsuredType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredType));
        }
        if (FCode.equalsIgnoreCase("IsLongValid")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsLongValid));
        }
        if (FCode.equalsIgnoreCase("IDStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsuredID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = String.valueOf(PersonID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(FamilyID);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(RelationToMainInsured);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RelationToAppnt);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AddressNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(SequenceNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(NativePlace);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(Nationality);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(RgtAddress);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Marriage);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarriageDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(Health);
                break;
            case 27:
                strFieldValue = String.valueOf(Stature);
                break;
            case 28:
                strFieldValue = String.valueOf(Avoirdupois);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(Degree);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(CreditGrade);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartWorkDate()));
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(Position);
                break;
            case 37:
                strFieldValue = String.valueOf(Salary);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(OccupationType);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(OccupationCode);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(WorkType);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(PluralityType);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(SmokeFlag);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(InsuredStat);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(UWFlag);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(UWCode);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(UWTime);
                break;
            case 54:
                strFieldValue = String.valueOf(BMI);
                break;
            case 55:
                strFieldValue = String.valueOf(InsuredPeoples);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(License);
                break;
            case 57:
                strFieldValue = StrTool.GBKToUnicode(LicenseType);
                break;
            case 58:
                strFieldValue = String.valueOf(CustomerSeqNo);
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(WorkNo);
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(SocialInsuNo);
                break;
            case 61:
                strFieldValue = StrTool.GBKToUnicode(OccupationDesb);
                break;
            case 62:
                strFieldValue = StrTool.GBKToUnicode(IdValiDate);
                break;
            case 63:
                strFieldValue = StrTool.GBKToUnicode(HaveMotorcycleLicence);
                break;
            case 64:
                strFieldValue = StrTool.GBKToUnicode(PartTimeJob);
                break;
            case 65:
                strFieldValue = StrTool.GBKToUnicode(HealthFlag);
                break;
            case 66:
                strFieldValue = StrTool.GBKToUnicode(ServiceMark);
                break;
            case 67:
                strFieldValue = StrTool.GBKToUnicode(FirstName);
                break;
            case 68:
                strFieldValue = StrTool.GBKToUnicode(LastName);
                break;
            case 69:
                strFieldValue = StrTool.GBKToUnicode(SSFlag);
                break;
            case 70:
                strFieldValue = StrTool.GBKToUnicode(TINNO);
                break;
            case 71:
                strFieldValue = StrTool.GBKToUnicode(TINFlag);
                break;
            case 72:
                strFieldValue = StrTool.GBKToUnicode(InsuredType);
                break;
            case 73:
                strFieldValue = StrTool.GBKToUnicode(IsLongValid);
                break;
            case 74:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsuredID")) {
            if( FValue != null && !FValue.equals("")) {
                InsuredID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("PersonID")) {
            if( FValue != null && !FValue.equals("")) {
                PersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ExecuteCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExecuteCom = FValue.trim();
            }
            else
                ExecuteCom = null;
        }
        if (FCode.equalsIgnoreCase("FamilyID")) {
            if( FValue != null && !FValue.equals(""))
            {
                FamilyID = FValue.trim();
            }
            else
                FamilyID = null;
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToMainInsured = FValue.trim();
            }
            else
                RelationToMainInsured = null;
        }
        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToAppnt = FValue.trim();
            }
            else
                RelationToAppnt = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddressNo = FValue.trim();
            }
            else
                AddressNo = null;
        }
        if (FCode.equalsIgnoreCase("SequenceNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SequenceNo = FValue.trim();
            }
            else
                SequenceNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if(FValue != null && !FValue.equals("")) {
                Birthday = fDate.getDate( FValue );
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            if(FValue != null && !FValue.equals("")) {
                MarriageDate = fDate.getDate( FValue );
            }
            else
                MarriageDate = null;
        }
        if (FCode.equalsIgnoreCase("Health")) {
            if( FValue != null && !FValue.equals(""))
            {
                Health = FValue.trim();
            }
            else
                Health = null;
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Stature = d;
            }
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Avoirdupois = d;
            }
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            if( FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
                Degree = null;
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
                CreditGrade = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            if(FValue != null && !FValue.equals("")) {
                JoinCompanyDate = fDate.getDate( FValue );
            }
            else
                JoinCompanyDate = null;
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartWorkDate = fDate.getDate( FValue );
            }
            else
                StartWorkDate = null;
        }
        if (FCode.equalsIgnoreCase("Position")) {
            if( FValue != null && !FValue.equals(""))
            {
                Position = FValue.trim();
            }
            else
                Position = null;
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Salary = d;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkType = FValue.trim();
            }
            else
                WorkType = null;
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
                PluralityType = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
                ContPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("InsuredStat")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredStat = FValue.trim();
            }
            else
                InsuredStat = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWCode = FValue.trim();
            }
            else
                UWCode = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if(FValue != null && !FValue.equals("")) {
                UWDate = fDate.getDate( FValue );
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("BMI")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BMI = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuredPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseType = FValue.trim();
            }
            else
                LicenseType = null;
        }
        if (FCode.equalsIgnoreCase("CustomerSeqNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                CustomerSeqNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("WorkNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkNo = FValue.trim();
            }
            else
                WorkNo = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuNo = FValue.trim();
            }
            else
                SocialInsuNo = null;
        }
        if (FCode.equalsIgnoreCase("OccupationDesb")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationDesb = FValue.trim();
            }
            else
                OccupationDesb = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            if( FValue != null && !FValue.equals(""))
            {
                HaveMotorcycleLicence = FValue.trim();
            }
            else
                HaveMotorcycleLicence = null;
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            if( FValue != null && !FValue.equals(""))
            {
                PartTimeJob = FValue.trim();
            }
            else
                PartTimeJob = null;
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthFlag = FValue.trim();
            }
            else
                HealthFlag = null;
        }
        if (FCode.equalsIgnoreCase("ServiceMark")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceMark = FValue.trim();
            }
            else
                ServiceMark = null;
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstName = FValue.trim();
            }
            else
                FirstName = null;
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastName = FValue.trim();
            }
            else
                LastName = null;
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SSFlag = FValue.trim();
            }
            else
                SSFlag = null;
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINNO = FValue.trim();
            }
            else
                TINNO = null;
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINFlag = FValue.trim();
            }
            else
                TINFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuredType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredType = FValue.trim();
            }
            else
                InsuredType = null;
        }
        if (FCode.equalsIgnoreCase("IsLongValid")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsLongValid = FValue.trim();
            }
            else
                IsLongValid = null;
        }
        if (FCode.equalsIgnoreCase("IDStartDate")) {
            if(FValue != null && !FValue.equals("")) {
                IDStartDate = fDate.getDate( FValue );
            }
            else
                IDStartDate = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCInsuredSchema other = (LCInsuredSchema)otherObject;
        return
            InsuredID == other.getInsuredID()
            && ContID == other.getContID()
            && PersonID == other.getPersonID()
            && ShardingID.equals(other.getShardingID())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && InsuredNo.equals(other.getInsuredNo())
            && PrtNo.equals(other.getPrtNo())
            && AppntNo.equals(other.getAppntNo())
            && ManageCom.equals(other.getManageCom())
            && ExecuteCom.equals(other.getExecuteCom())
            && FamilyID.equals(other.getFamilyID())
            && RelationToMainInsured.equals(other.getRelationToMainInsured())
            && RelationToAppnt.equals(other.getRelationToAppnt())
            && AddressNo.equals(other.getAddressNo())
            && SequenceNo.equals(other.getSequenceNo())
            && Name.equals(other.getName())
            && Sex.equals(other.getSex())
            && fDate.getString(Birthday).equals(other.getBirthday())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && NativePlace.equals(other.getNativePlace())
            && Nationality.equals(other.getNationality())
            && RgtAddress.equals(other.getRgtAddress())
            && Marriage.equals(other.getMarriage())
            && fDate.getString(MarriageDate).equals(other.getMarriageDate())
            && Health.equals(other.getHealth())
            && Stature == other.getStature()
            && Avoirdupois == other.getAvoirdupois()
            && Degree.equals(other.getDegree())
            && CreditGrade.equals(other.getCreditGrade())
            && BankCode.equals(other.getBankCode())
            && BankAccNo.equals(other.getBankAccNo())
            && AccName.equals(other.getAccName())
            && fDate.getString(JoinCompanyDate).equals(other.getJoinCompanyDate())
            && fDate.getString(StartWorkDate).equals(other.getStartWorkDate())
            && Position.equals(other.getPosition())
            && Salary == other.getSalary()
            && OccupationType.equals(other.getOccupationType())
            && OccupationCode.equals(other.getOccupationCode())
            && WorkType.equals(other.getWorkType())
            && PluralityType.equals(other.getPluralityType())
            && SmokeFlag.equals(other.getSmokeFlag())
            && ContPlanCode.equals(other.getContPlanCode())
            && Operator.equals(other.getOperator())
            && InsuredStat.equals(other.getInsuredStat())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && UWFlag.equals(other.getUWFlag())
            && UWCode.equals(other.getUWCode())
            && fDate.getString(UWDate).equals(other.getUWDate())
            && UWTime.equals(other.getUWTime())
            && BMI == other.getBMI()
            && InsuredPeoples == other.getInsuredPeoples()
            && License.equals(other.getLicense())
            && LicenseType.equals(other.getLicenseType())
            && CustomerSeqNo == other.getCustomerSeqNo()
            && WorkNo.equals(other.getWorkNo())
            && SocialInsuNo.equals(other.getSocialInsuNo())
            && OccupationDesb.equals(other.getOccupationDesb())
            && IdValiDate.equals(other.getIdValiDate())
            && HaveMotorcycleLicence.equals(other.getHaveMotorcycleLicence())
            && PartTimeJob.equals(other.getPartTimeJob())
            && HealthFlag.equals(other.getHealthFlag())
            && ServiceMark.equals(other.getServiceMark())
            && FirstName.equals(other.getFirstName())
            && LastName.equals(other.getLastName())
            && SSFlag.equals(other.getSSFlag())
            && TINNO.equals(other.getTINNO())
            && TINFlag.equals(other.getTINFlag())
            && InsuredType.equals(other.getInsuredType())
            && IsLongValid.equals(other.getIsLongValid())
            && fDate.getString(IDStartDate).equals(other.getIDStartDate());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsuredID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("PersonID") ) {
            return 2;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 3;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 4;
        }
        if( strFieldName.equals("ContNo") ) {
            return 5;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 6;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 7;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 8;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 9;
        }
        if( strFieldName.equals("ExecuteCom") ) {
            return 10;
        }
        if( strFieldName.equals("FamilyID") ) {
            return 11;
        }
        if( strFieldName.equals("RelationToMainInsured") ) {
            return 12;
        }
        if( strFieldName.equals("RelationToAppnt") ) {
            return 13;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 14;
        }
        if( strFieldName.equals("SequenceNo") ) {
            return 15;
        }
        if( strFieldName.equals("Name") ) {
            return 16;
        }
        if( strFieldName.equals("Sex") ) {
            return 17;
        }
        if( strFieldName.equals("Birthday") ) {
            return 18;
        }
        if( strFieldName.equals("IDType") ) {
            return 19;
        }
        if( strFieldName.equals("IDNo") ) {
            return 20;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 21;
        }
        if( strFieldName.equals("Nationality") ) {
            return 22;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 23;
        }
        if( strFieldName.equals("Marriage") ) {
            return 24;
        }
        if( strFieldName.equals("MarriageDate") ) {
            return 25;
        }
        if( strFieldName.equals("Health") ) {
            return 26;
        }
        if( strFieldName.equals("Stature") ) {
            return 27;
        }
        if( strFieldName.equals("Avoirdupois") ) {
            return 28;
        }
        if( strFieldName.equals("Degree") ) {
            return 29;
        }
        if( strFieldName.equals("CreditGrade") ) {
            return 30;
        }
        if( strFieldName.equals("BankCode") ) {
            return 31;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 32;
        }
        if( strFieldName.equals("AccName") ) {
            return 33;
        }
        if( strFieldName.equals("JoinCompanyDate") ) {
            return 34;
        }
        if( strFieldName.equals("StartWorkDate") ) {
            return 35;
        }
        if( strFieldName.equals("Position") ) {
            return 36;
        }
        if( strFieldName.equals("Salary") ) {
            return 37;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 38;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 39;
        }
        if( strFieldName.equals("WorkType") ) {
            return 40;
        }
        if( strFieldName.equals("PluralityType") ) {
            return 41;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 42;
        }
        if( strFieldName.equals("ContPlanCode") ) {
            return 43;
        }
        if( strFieldName.equals("Operator") ) {
            return 44;
        }
        if( strFieldName.equals("InsuredStat") ) {
            return 45;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 46;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 47;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 48;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 49;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 50;
        }
        if( strFieldName.equals("UWCode") ) {
            return 51;
        }
        if( strFieldName.equals("UWDate") ) {
            return 52;
        }
        if( strFieldName.equals("UWTime") ) {
            return 53;
        }
        if( strFieldName.equals("BMI") ) {
            return 54;
        }
        if( strFieldName.equals("InsuredPeoples") ) {
            return 55;
        }
        if( strFieldName.equals("License") ) {
            return 56;
        }
        if( strFieldName.equals("LicenseType") ) {
            return 57;
        }
        if( strFieldName.equals("CustomerSeqNo") ) {
            return 58;
        }
        if( strFieldName.equals("WorkNo") ) {
            return 59;
        }
        if( strFieldName.equals("SocialInsuNo") ) {
            return 60;
        }
        if( strFieldName.equals("OccupationDesb") ) {
            return 61;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 62;
        }
        if( strFieldName.equals("HaveMotorcycleLicence") ) {
            return 63;
        }
        if( strFieldName.equals("PartTimeJob") ) {
            return 64;
        }
        if( strFieldName.equals("HealthFlag") ) {
            return 65;
        }
        if( strFieldName.equals("ServiceMark") ) {
            return 66;
        }
        if( strFieldName.equals("FirstName") ) {
            return 67;
        }
        if( strFieldName.equals("LastName") ) {
            return 68;
        }
        if( strFieldName.equals("SSFlag") ) {
            return 69;
        }
        if( strFieldName.equals("TINNO") ) {
            return 70;
        }
        if( strFieldName.equals("TINFlag") ) {
            return 71;
        }
        if( strFieldName.equals("InsuredType") ) {
            return 72;
        }
        if( strFieldName.equals("IsLongValid") ) {
            return 73;
        }
        if( strFieldName.equals("IDStartDate") ) {
            return 74;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsuredID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "PersonID";
                break;
            case 3:
                strFieldName = "ShardingID";
                break;
            case 4:
                strFieldName = "GrpContNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "InsuredNo";
                break;
            case 7:
                strFieldName = "PrtNo";
                break;
            case 8:
                strFieldName = "AppntNo";
                break;
            case 9:
                strFieldName = "ManageCom";
                break;
            case 10:
                strFieldName = "ExecuteCom";
                break;
            case 11:
                strFieldName = "FamilyID";
                break;
            case 12:
                strFieldName = "RelationToMainInsured";
                break;
            case 13:
                strFieldName = "RelationToAppnt";
                break;
            case 14:
                strFieldName = "AddressNo";
                break;
            case 15:
                strFieldName = "SequenceNo";
                break;
            case 16:
                strFieldName = "Name";
                break;
            case 17:
                strFieldName = "Sex";
                break;
            case 18:
                strFieldName = "Birthday";
                break;
            case 19:
                strFieldName = "IDType";
                break;
            case 20:
                strFieldName = "IDNo";
                break;
            case 21:
                strFieldName = "NativePlace";
                break;
            case 22:
                strFieldName = "Nationality";
                break;
            case 23:
                strFieldName = "RgtAddress";
                break;
            case 24:
                strFieldName = "Marriage";
                break;
            case 25:
                strFieldName = "MarriageDate";
                break;
            case 26:
                strFieldName = "Health";
                break;
            case 27:
                strFieldName = "Stature";
                break;
            case 28:
                strFieldName = "Avoirdupois";
                break;
            case 29:
                strFieldName = "Degree";
                break;
            case 30:
                strFieldName = "CreditGrade";
                break;
            case 31:
                strFieldName = "BankCode";
                break;
            case 32:
                strFieldName = "BankAccNo";
                break;
            case 33:
                strFieldName = "AccName";
                break;
            case 34:
                strFieldName = "JoinCompanyDate";
                break;
            case 35:
                strFieldName = "StartWorkDate";
                break;
            case 36:
                strFieldName = "Position";
                break;
            case 37:
                strFieldName = "Salary";
                break;
            case 38:
                strFieldName = "OccupationType";
                break;
            case 39:
                strFieldName = "OccupationCode";
                break;
            case 40:
                strFieldName = "WorkType";
                break;
            case 41:
                strFieldName = "PluralityType";
                break;
            case 42:
                strFieldName = "SmokeFlag";
                break;
            case 43:
                strFieldName = "ContPlanCode";
                break;
            case 44:
                strFieldName = "Operator";
                break;
            case 45:
                strFieldName = "InsuredStat";
                break;
            case 46:
                strFieldName = "MakeDate";
                break;
            case 47:
                strFieldName = "MakeTime";
                break;
            case 48:
                strFieldName = "ModifyDate";
                break;
            case 49:
                strFieldName = "ModifyTime";
                break;
            case 50:
                strFieldName = "UWFlag";
                break;
            case 51:
                strFieldName = "UWCode";
                break;
            case 52:
                strFieldName = "UWDate";
                break;
            case 53:
                strFieldName = "UWTime";
                break;
            case 54:
                strFieldName = "BMI";
                break;
            case 55:
                strFieldName = "InsuredPeoples";
                break;
            case 56:
                strFieldName = "License";
                break;
            case 57:
                strFieldName = "LicenseType";
                break;
            case 58:
                strFieldName = "CustomerSeqNo";
                break;
            case 59:
                strFieldName = "WorkNo";
                break;
            case 60:
                strFieldName = "SocialInsuNo";
                break;
            case 61:
                strFieldName = "OccupationDesb";
                break;
            case 62:
                strFieldName = "IdValiDate";
                break;
            case 63:
                strFieldName = "HaveMotorcycleLicence";
                break;
            case 64:
                strFieldName = "PartTimeJob";
                break;
            case 65:
                strFieldName = "HealthFlag";
                break;
            case 66:
                strFieldName = "ServiceMark";
                break;
            case 67:
                strFieldName = "FirstName";
                break;
            case 68:
                strFieldName = "LastName";
                break;
            case 69:
                strFieldName = "SSFlag";
                break;
            case 70:
                strFieldName = "TINNO";
                break;
            case 71:
                strFieldName = "TINFlag";
                break;
            case 72:
                strFieldName = "InsuredType";
                break;
            case 73:
                strFieldName = "IsLongValid";
                break;
            case 74:
                strFieldName = "IDStartDate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUREDID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "PERSONID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "EXECUTECOM":
                return Schema.TYPE_STRING;
            case "FAMILYID":
                return Schema.TYPE_STRING;
            case "RELATIONTOMAININSURED":
                return Schema.TYPE_STRING;
            case "RELATIONTOAPPNT":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_STRING;
            case "SEQUENCENO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_DATE;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "MARRIAGEDATE":
                return Schema.TYPE_DATE;
            case "HEALTH":
                return Schema.TYPE_STRING;
            case "STATURE":
                return Schema.TYPE_DOUBLE;
            case "AVOIRDUPOIS":
                return Schema.TYPE_DOUBLE;
            case "DEGREE":
                return Schema.TYPE_STRING;
            case "CREDITGRADE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "JOINCOMPANYDATE":
                return Schema.TYPE_DATE;
            case "STARTWORKDATE":
                return Schema.TYPE_DATE;
            case "POSITION":
                return Schema.TYPE_STRING;
            case "SALARY":
                return Schema.TYPE_DOUBLE;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "WORKTYPE":
                return Schema.TYPE_STRING;
            case "PLURALITYTYPE":
                return Schema.TYPE_STRING;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "CONTPLANCODE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "INSUREDSTAT":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWCODE":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_DATE;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "BMI":
                return Schema.TYPE_DOUBLE;
            case "INSUREDPEOPLES":
                return Schema.TYPE_INT;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "LICENSETYPE":
                return Schema.TYPE_STRING;
            case "CUSTOMERSEQNO":
                return Schema.TYPE_INT;
            case "WORKNO":
                return Schema.TYPE_STRING;
            case "SOCIALINSUNO":
                return Schema.TYPE_STRING;
            case "OCCUPATIONDESB":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "HAVEMOTORCYCLELICENCE":
                return Schema.TYPE_STRING;
            case "PARTTIMEJOB":
                return Schema.TYPE_STRING;
            case "HEALTHFLAG":
                return Schema.TYPE_STRING;
            case "SERVICEMARK":
                return Schema.TYPE_STRING;
            case "FIRSTNAME":
                return Schema.TYPE_STRING;
            case "LASTNAME":
                return Schema.TYPE_STRING;
            case "SSFLAG":
                return Schema.TYPE_STRING;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            case "INSUREDTYPE":
                return Schema.TYPE_STRING;
            case "ISLONGVALID":
                return Schema.TYPE_STRING;
            case "IDSTARTDATE":
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_LONG;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_DOUBLE;
            case 28:
                return Schema.TYPE_DOUBLE;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_DATE;
            case 35:
                return Schema.TYPE_DATE;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_DATE;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_DATE;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_DATE;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_DOUBLE;
            case 55:
                return Schema.TYPE_INT;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_INT;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
