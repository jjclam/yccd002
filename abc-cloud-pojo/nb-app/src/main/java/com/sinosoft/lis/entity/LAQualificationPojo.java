/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LAQualificationPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-22
 */
public class LAQualificationPojo implements Pojo,Serializable {
    // @Field
    /** 业务员编码 */
    @RedisPrimaryHKey
    @RedisIndexHKey
    private String AgentCode; 
    /** 资格证书号 */
    @RedisPrimaryHKey
    private String QualifNo; 
    /** 资格证类型 */
    @RedisPrimaryHKey
    private int Idx; 
    /** 批准单位 */
    private String GrantUnit; 
    /** 发放日期 */
    private String  GrantDate;
    /** 失效日期 */
    private String  InvalidDate;
    /** 失效原因 */
    private String InvalidRsn; 
    /** 补发日期 */
    private String  reissueDate;
    /** 补发原因 */
    private String reissueRsn; 
    /** 有效日期 */
    private int ValidPeriod; 
    /** 资格证书状态 */
    private String State; 
    /** 通过考试日期 */
    private String  PasExamDate;
    /** 考试年度 */
    private String ExamYear; 
    /** 考试次数 */
    private String ExamTimes; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 上一次修改日期 */
    private String  ModifyDate;
    /** 上一次修改时间 */
    private String ModifyTime; 
    /** 有效起期 */
    private String  ValidStart;
    /** 有效止期 */
    private String  ValidEnd;
    /** 旧资格证号码 */
    private String OldQualifNo; 
    /** 资格证名称 */
    private String QualifName; 
    /** State1 */
    private String State1; 


    public static final int FIELDNUM = 24;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getQualifNo() {
        return QualifNo;
    }
    public void setQualifNo(String aQualifNo) {
        QualifNo = aQualifNo;
    }
    public int getIdx() {
        return Idx;
    }
    public void setIdx(int aIdx) {
        Idx = aIdx;
    }
    public void setIdx(String aIdx) {
        if (aIdx != null && !aIdx.equals("")) {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public String getGrantUnit() {
        return GrantUnit;
    }
    public void setGrantUnit(String aGrantUnit) {
        GrantUnit = aGrantUnit;
    }
    public String getGrantDate() {
        return GrantDate;
    }
    public void setGrantDate(String aGrantDate) {
        GrantDate = aGrantDate;
    }
    public String getInvalidDate() {
        return InvalidDate;
    }
    public void setInvalidDate(String aInvalidDate) {
        InvalidDate = aInvalidDate;
    }
    public String getInvalidRsn() {
        return InvalidRsn;
    }
    public void setInvalidRsn(String aInvalidRsn) {
        InvalidRsn = aInvalidRsn;
    }
    public String getReissueDate() {
        return reissueDate;
    }
    public void setReissueDate(String areissueDate) {
        reissueDate = areissueDate;
    }
    public String getReissueRsn() {
        return reissueRsn;
    }
    public void setReissueRsn(String areissueRsn) {
        reissueRsn = areissueRsn;
    }
    public int getValidPeriod() {
        return ValidPeriod;
    }
    public void setValidPeriod(int aValidPeriod) {
        ValidPeriod = aValidPeriod;
    }
    public void setValidPeriod(String aValidPeriod) {
        if (aValidPeriod != null && !aValidPeriod.equals("")) {
            Integer tInteger = new Integer(aValidPeriod);
            int i = tInteger.intValue();
            ValidPeriod = i;
        }
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getPasExamDate() {
        return PasExamDate;
    }
    public void setPasExamDate(String aPasExamDate) {
        PasExamDate = aPasExamDate;
    }
    public String getExamYear() {
        return ExamYear;
    }
    public void setExamYear(String aExamYear) {
        ExamYear = aExamYear;
    }
    public String getExamTimes() {
        return ExamTimes;
    }
    public void setExamTimes(String aExamTimes) {
        ExamTimes = aExamTimes;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getValidStart() {
        return ValidStart;
    }
    public void setValidStart(String aValidStart) {
        ValidStart = aValidStart;
    }
    public String getValidEnd() {
        return ValidEnd;
    }
    public void setValidEnd(String aValidEnd) {
        ValidEnd = aValidEnd;
    }
    public String getOldQualifNo() {
        return OldQualifNo;
    }
    public void setOldQualifNo(String aOldQualifNo) {
        OldQualifNo = aOldQualifNo;
    }
    public String getQualifName() {
        return QualifName;
    }
    public void setQualifName(String aQualifName) {
        QualifName = aQualifName;
    }
    public String getState1() {
        return State1;
    }
    public void setState1(String aState1) {
        State1 = aState1;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCode") ) {
            return 0;
        }
        if( strFieldName.equals("QualifNo") ) {
            return 1;
        }
        if( strFieldName.equals("Idx") ) {
            return 2;
        }
        if( strFieldName.equals("GrantUnit") ) {
            return 3;
        }
        if( strFieldName.equals("GrantDate") ) {
            return 4;
        }
        if( strFieldName.equals("InvalidDate") ) {
            return 5;
        }
        if( strFieldName.equals("InvalidRsn") ) {
            return 6;
        }
        if( strFieldName.equals("reissueDate") ) {
            return 7;
        }
        if( strFieldName.equals("reissueRsn") ) {
            return 8;
        }
        if( strFieldName.equals("ValidPeriod") ) {
            return 9;
        }
        if( strFieldName.equals("State") ) {
            return 10;
        }
        if( strFieldName.equals("PasExamDate") ) {
            return 11;
        }
        if( strFieldName.equals("ExamYear") ) {
            return 12;
        }
        if( strFieldName.equals("ExamTimes") ) {
            return 13;
        }
        if( strFieldName.equals("Operator") ) {
            return 14;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 15;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 18;
        }
        if( strFieldName.equals("ValidStart") ) {
            return 19;
        }
        if( strFieldName.equals("ValidEnd") ) {
            return 20;
        }
        if( strFieldName.equals("OldQualifNo") ) {
            return 21;
        }
        if( strFieldName.equals("QualifName") ) {
            return 22;
        }
        if( strFieldName.equals("State1") ) {
            return 23;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "QualifNo";
                break;
            case 2:
                strFieldName = "Idx";
                break;
            case 3:
                strFieldName = "GrantUnit";
                break;
            case 4:
                strFieldName = "GrantDate";
                break;
            case 5:
                strFieldName = "InvalidDate";
                break;
            case 6:
                strFieldName = "InvalidRsn";
                break;
            case 7:
                strFieldName = "reissueDate";
                break;
            case 8:
                strFieldName = "reissueRsn";
                break;
            case 9:
                strFieldName = "ValidPeriod";
                break;
            case 10:
                strFieldName = "State";
                break;
            case 11:
                strFieldName = "PasExamDate";
                break;
            case 12:
                strFieldName = "ExamYear";
                break;
            case 13:
                strFieldName = "ExamTimes";
                break;
            case 14:
                strFieldName = "Operator";
                break;
            case 15:
                strFieldName = "MakeDate";
                break;
            case 16:
                strFieldName = "MakeTime";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            case 19:
                strFieldName = "ValidStart";
                break;
            case 20:
                strFieldName = "ValidEnd";
                break;
            case 21:
                strFieldName = "OldQualifNo";
                break;
            case 22:
                strFieldName = "QualifName";
                break;
            case 23:
                strFieldName = "State1";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "QUALIFNO":
                return Schema.TYPE_STRING;
            case "IDX":
                return Schema.TYPE_INT;
            case "GRANTUNIT":
                return Schema.TYPE_STRING;
            case "GRANTDATE":
                return Schema.TYPE_STRING;
            case "INVALIDDATE":
                return Schema.TYPE_STRING;
            case "INVALIDRSN":
                return Schema.TYPE_STRING;
            case "REISSUEDATE":
                return Schema.TYPE_STRING;
            case "REISSUERSN":
                return Schema.TYPE_STRING;
            case "VALIDPERIOD":
                return Schema.TYPE_INT;
            case "STATE":
                return Schema.TYPE_STRING;
            case "PASEXAMDATE":
                return Schema.TYPE_STRING;
            case "EXAMYEAR":
                return Schema.TYPE_STRING;
            case "EXAMTIMES":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "VALIDSTART":
                return Schema.TYPE_STRING;
            case "VALIDEND":
                return Schema.TYPE_STRING;
            case "OLDQUALIFNO":
                return Schema.TYPE_STRING;
            case "QUALIFNAME":
                return Schema.TYPE_STRING;
            case "STATE1":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_INT;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_INT;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("QualifNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualifNo));
        }
        if (FCode.equalsIgnoreCase("Idx")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equalsIgnoreCase("GrantUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrantUnit));
        }
        if (FCode.equalsIgnoreCase("GrantDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrantDate));
        }
        if (FCode.equalsIgnoreCase("InvalidDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvalidDate));
        }
        if (FCode.equalsIgnoreCase("InvalidRsn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvalidRsn));
        }
        if (FCode.equalsIgnoreCase("reissueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(reissueDate));
        }
        if (FCode.equalsIgnoreCase("reissueRsn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(reissueRsn));
        }
        if (FCode.equalsIgnoreCase("ValidPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValidPeriod));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("PasExamDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PasExamDate));
        }
        if (FCode.equalsIgnoreCase("ExamYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExamYear));
        }
        if (FCode.equalsIgnoreCase("ExamTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExamTimes));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ValidStart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValidStart));
        }
        if (FCode.equalsIgnoreCase("ValidEnd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValidEnd));
        }
        if (FCode.equalsIgnoreCase("OldQualifNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldQualifNo));
        }
        if (FCode.equalsIgnoreCase("QualifName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualifName));
        }
        if (FCode.equalsIgnoreCase("State1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State1));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 1:
                strFieldValue = String.valueOf(QualifNo);
                break;
            case 2:
                strFieldValue = String.valueOf(Idx);
                break;
            case 3:
                strFieldValue = String.valueOf(GrantUnit);
                break;
            case 4:
                strFieldValue = String.valueOf(GrantDate);
                break;
            case 5:
                strFieldValue = String.valueOf(InvalidDate);
                break;
            case 6:
                strFieldValue = String.valueOf(InvalidRsn);
                break;
            case 7:
                strFieldValue = String.valueOf(reissueDate);
                break;
            case 8:
                strFieldValue = String.valueOf(reissueRsn);
                break;
            case 9:
                strFieldValue = String.valueOf(ValidPeriod);
                break;
            case 10:
                strFieldValue = String.valueOf(State);
                break;
            case 11:
                strFieldValue = String.valueOf(PasExamDate);
                break;
            case 12:
                strFieldValue = String.valueOf(ExamYear);
                break;
            case 13:
                strFieldValue = String.valueOf(ExamTimes);
                break;
            case 14:
                strFieldValue = String.valueOf(Operator);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 16:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 17:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 19:
                strFieldValue = String.valueOf(ValidStart);
                break;
            case 20:
                strFieldValue = String.valueOf(ValidEnd);
                break;
            case 21:
                strFieldValue = String.valueOf(OldQualifNo);
                break;
            case 22:
                strFieldValue = String.valueOf(QualifName);
                break;
            case 23:
                strFieldValue = String.valueOf(State1);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("QualifNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualifNo = FValue.trim();
            }
            else
                QualifNo = null;
        }
        if (FCode.equalsIgnoreCase("Idx")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrantUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrantUnit = FValue.trim();
            }
            else
                GrantUnit = null;
        }
        if (FCode.equalsIgnoreCase("GrantDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrantDate = FValue.trim();
            }
            else
                GrantDate = null;
        }
        if (FCode.equalsIgnoreCase("InvalidDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvalidDate = FValue.trim();
            }
            else
                InvalidDate = null;
        }
        if (FCode.equalsIgnoreCase("InvalidRsn")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvalidRsn = FValue.trim();
            }
            else
                InvalidRsn = null;
        }
        if (FCode.equalsIgnoreCase("reissueDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                reissueDate = FValue.trim();
            }
            else
                reissueDate = null;
        }
        if (FCode.equalsIgnoreCase("reissueRsn")) {
            if( FValue != null && !FValue.equals(""))
            {
                reissueRsn = FValue.trim();
            }
            else
                reissueRsn = null;
        }
        if (FCode.equalsIgnoreCase("ValidPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ValidPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("PasExamDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PasExamDate = FValue.trim();
            }
            else
                PasExamDate = null;
        }
        if (FCode.equalsIgnoreCase("ExamYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExamYear = FValue.trim();
            }
            else
                ExamYear = null;
        }
        if (FCode.equalsIgnoreCase("ExamTimes")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExamTimes = FValue.trim();
            }
            else
                ExamTimes = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ValidStart")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValidStart = FValue.trim();
            }
            else
                ValidStart = null;
        }
        if (FCode.equalsIgnoreCase("ValidEnd")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValidEnd = FValue.trim();
            }
            else
                ValidEnd = null;
        }
        if (FCode.equalsIgnoreCase("OldQualifNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldQualifNo = FValue.trim();
            }
            else
                OldQualifNo = null;
        }
        if (FCode.equalsIgnoreCase("QualifName")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualifName = FValue.trim();
            }
            else
                QualifName = null;
        }
        if (FCode.equalsIgnoreCase("State1")) {
            if( FValue != null && !FValue.equals(""))
            {
                State1 = FValue.trim();
            }
            else
                State1 = null;
        }
        return true;
    }


    public String toString() {
    return "LAQualificationPojo [" +
            "AgentCode="+AgentCode +
            ", QualifNo="+QualifNo +
            ", Idx="+Idx +
            ", GrantUnit="+GrantUnit +
            ", GrantDate="+GrantDate +
            ", InvalidDate="+InvalidDate +
            ", InvalidRsn="+InvalidRsn +
            ", reissueDate="+reissueDate +
            ", reissueRsn="+reissueRsn +
            ", ValidPeriod="+ValidPeriod +
            ", State="+State +
            ", PasExamDate="+PasExamDate +
            ", ExamYear="+ExamYear +
            ", ExamTimes="+ExamTimes +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", ValidStart="+ValidStart +
            ", ValidEnd="+ValidEnd +
            ", OldQualifNo="+OldQualifNo +
            ", QualifName="+QualifName +
            ", State1="+State1 +"]";
    }
}
