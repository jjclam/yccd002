/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LKNRTBizTransPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-07
 */
public class LKNRTBizTransPojo implements Pojo,Serializable {
    // @Field
    /** Lknrtbizid */
    private long LKNRTBizID; 
    /** Shardingid */
    private String ShardingID; 
    /** Transcode */
    private String TransCode; 
    /** Bankcode */
    private String BankCode; 
    /** Trandate */
    private String  TranDate;
    /** Zoneno */
    private String ZoneNo; 
    /** Banknode */
    private String BankNode; 
    /** Tellerno */
    private String TellerNo; 
    /** Transno */
    private String TransNo; 
    /** Proposalno */
    private String ProposalNo; 
    /** Banksalechnl */
    private String BanksaleChnl; 
    /** Uwtype */
    private String UwType; 
    /** Appntname */
    private String AppntName; 
    /** Appntidtype */
    private String AppntidType; 
    /** Appntidno */
    private String AppntidNo; 
    /** Appntaccno */
    private String AppntaccNo; 
    /** Bizstatus */
    private String BizStatus; 
    /** Uwstatus */
    private String UwStatus; 
    /** Respdate */
    private String  RespDate;
    /** Resptime */
    private String RespTime; 
    /** Makedate */
    private String  MakeDate;
    /** Maketime */
    private String MakeTime; 
    /** Modifydate */
    private String  ModifyDate;
    /** Modifytime */
    private String ModifyTime; 
    /** Bak1 */
    private String Bak1; 
    /** Bak2 */
    private String Bak2; 


    public static final int FIELDNUM = 26;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getLKNRTBizID() {
        return LKNRTBizID;
    }
    public void setLKNRTBizID(long aLKNRTBizID) {
        LKNRTBizID = aLKNRTBizID;
    }
    public void setLKNRTBizID(String aLKNRTBizID) {
        if (aLKNRTBizID != null && !aLKNRTBizID.equals("")) {
            LKNRTBizID = new Long(aLKNRTBizID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getTransCode() {
        return TransCode;
    }
    public void setTransCode(String aTransCode) {
        TransCode = aTransCode;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getTranDate() {
        return TranDate;
    }
    public void setTranDate(String aTranDate) {
        TranDate = aTranDate;
    }
    public String getZoneNo() {
        return ZoneNo;
    }
    public void setZoneNo(String aZoneNo) {
        ZoneNo = aZoneNo;
    }
    public String getBankNode() {
        return BankNode;
    }
    public void setBankNode(String aBankNode) {
        BankNode = aBankNode;
    }
    public String getTellerNo() {
        return TellerNo;
    }
    public void setTellerNo(String aTellerNo) {
        TellerNo = aTellerNo;
    }
    public String getTransNo() {
        return TransNo;
    }
    public void setTransNo(String aTransNo) {
        TransNo = aTransNo;
    }
    public String getProposalNo() {
        return ProposalNo;
    }
    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }
    public String getBanksaleChnl() {
        return BanksaleChnl;
    }
    public void setBanksaleChnl(String aBanksaleChnl) {
        BanksaleChnl = aBanksaleChnl;
    }
    public String getUwType() {
        return UwType;
    }
    public void setUwType(String aUwType) {
        UwType = aUwType;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAppntidType() {
        return AppntidType;
    }
    public void setAppntidType(String aAppntidType) {
        AppntidType = aAppntidType;
    }
    public String getAppntidNo() {
        return AppntidNo;
    }
    public void setAppntidNo(String aAppntidNo) {
        AppntidNo = aAppntidNo;
    }
    public String getAppntaccNo() {
        return AppntaccNo;
    }
    public void setAppntaccNo(String aAppntaccNo) {
        AppntaccNo = aAppntaccNo;
    }
    public String getBizStatus() {
        return BizStatus;
    }
    public void setBizStatus(String aBizStatus) {
        BizStatus = aBizStatus;
    }
    public String getUwStatus() {
        return UwStatus;
    }
    public void setUwStatus(String aUwStatus) {
        UwStatus = aUwStatus;
    }
    public String getRespDate() {
        return RespDate;
    }
    public void setRespDate(String aRespDate) {
        RespDate = aRespDate;
    }
    public String getRespTime() {
        return RespTime;
    }
    public void setRespTime(String aRespTime) {
        RespTime = aRespTime;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBak1() {
        return Bak1;
    }
    public void setBak1(String aBak1) {
        Bak1 = aBak1;
    }
    public String getBak2() {
        return Bak2;
    }
    public void setBak2(String aBak2) {
        Bak2 = aBak2;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("LKNRTBizID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("TransCode") ) {
            return 2;
        }
        if( strFieldName.equals("BankCode") ) {
            return 3;
        }
        if( strFieldName.equals("TranDate") ) {
            return 4;
        }
        if( strFieldName.equals("ZoneNo") ) {
            return 5;
        }
        if( strFieldName.equals("BankNode") ) {
            return 6;
        }
        if( strFieldName.equals("TellerNo") ) {
            return 7;
        }
        if( strFieldName.equals("TransNo") ) {
            return 8;
        }
        if( strFieldName.equals("ProposalNo") ) {
            return 9;
        }
        if( strFieldName.equals("BanksaleChnl") ) {
            return 10;
        }
        if( strFieldName.equals("UwType") ) {
            return 11;
        }
        if( strFieldName.equals("AppntName") ) {
            return 12;
        }
        if( strFieldName.equals("AppntidType") ) {
            return 13;
        }
        if( strFieldName.equals("AppntidNo") ) {
            return 14;
        }
        if( strFieldName.equals("AppntaccNo") ) {
            return 15;
        }
        if( strFieldName.equals("BizStatus") ) {
            return 16;
        }
        if( strFieldName.equals("UwStatus") ) {
            return 17;
        }
        if( strFieldName.equals("RespDate") ) {
            return 18;
        }
        if( strFieldName.equals("RespTime") ) {
            return 19;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 20;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 21;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 23;
        }
        if( strFieldName.equals("Bak1") ) {
            return 24;
        }
        if( strFieldName.equals("Bak2") ) {
            return 25;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "LKNRTBizID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "TransCode";
                break;
            case 3:
                strFieldName = "BankCode";
                break;
            case 4:
                strFieldName = "TranDate";
                break;
            case 5:
                strFieldName = "ZoneNo";
                break;
            case 6:
                strFieldName = "BankNode";
                break;
            case 7:
                strFieldName = "TellerNo";
                break;
            case 8:
                strFieldName = "TransNo";
                break;
            case 9:
                strFieldName = "ProposalNo";
                break;
            case 10:
                strFieldName = "BanksaleChnl";
                break;
            case 11:
                strFieldName = "UwType";
                break;
            case 12:
                strFieldName = "AppntName";
                break;
            case 13:
                strFieldName = "AppntidType";
                break;
            case 14:
                strFieldName = "AppntidNo";
                break;
            case 15:
                strFieldName = "AppntaccNo";
                break;
            case 16:
                strFieldName = "BizStatus";
                break;
            case 17:
                strFieldName = "UwStatus";
                break;
            case 18:
                strFieldName = "RespDate";
                break;
            case 19:
                strFieldName = "RespTime";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "Bak1";
                break;
            case 25:
                strFieldName = "Bak2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "LKNRTBIZID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "TRANSCODE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "TRANDATE":
                return Schema.TYPE_STRING;
            case "ZONENO":
                return Schema.TYPE_STRING;
            case "BANKNODE":
                return Schema.TYPE_STRING;
            case "TELLERNO":
                return Schema.TYPE_STRING;
            case "TRANSNO":
                return Schema.TYPE_STRING;
            case "PROPOSALNO":
                return Schema.TYPE_STRING;
            case "BANKSALECHNL":
                return Schema.TYPE_STRING;
            case "UWTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "APPNTIDTYPE":
                return Schema.TYPE_STRING;
            case "APPNTIDNO":
                return Schema.TYPE_STRING;
            case "APPNTACCNO":
                return Schema.TYPE_STRING;
            case "BIZSTATUS":
                return Schema.TYPE_STRING;
            case "UWSTATUS":
                return Schema.TYPE_STRING;
            case "RESPDATE":
                return Schema.TYPE_STRING;
            case "RESPTIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("LKNRTBizID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LKNRTBizID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("TranDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TranDate));
        }
        if (FCode.equalsIgnoreCase("ZoneNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZoneNo));
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
        }
        if (FCode.equalsIgnoreCase("TellerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TellerNo));
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equalsIgnoreCase("BanksaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BanksaleChnl));
        }
        if (FCode.equalsIgnoreCase("UwType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UwType));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AppntidType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntidType));
        }
        if (FCode.equalsIgnoreCase("AppntidNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntidNo));
        }
        if (FCode.equalsIgnoreCase("AppntaccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntaccNo));
        }
        if (FCode.equalsIgnoreCase("BizStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BizStatus));
        }
        if (FCode.equalsIgnoreCase("UwStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UwStatus));
        }
        if (FCode.equalsIgnoreCase("RespDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RespDate));
        }
        if (FCode.equalsIgnoreCase("RespTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RespTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(LKNRTBizID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(TransCode);
                break;
            case 3:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 4:
                strFieldValue = String.valueOf(TranDate);
                break;
            case 5:
                strFieldValue = String.valueOf(ZoneNo);
                break;
            case 6:
                strFieldValue = String.valueOf(BankNode);
                break;
            case 7:
                strFieldValue = String.valueOf(TellerNo);
                break;
            case 8:
                strFieldValue = String.valueOf(TransNo);
                break;
            case 9:
                strFieldValue = String.valueOf(ProposalNo);
                break;
            case 10:
                strFieldValue = String.valueOf(BanksaleChnl);
                break;
            case 11:
                strFieldValue = String.valueOf(UwType);
                break;
            case 12:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 13:
                strFieldValue = String.valueOf(AppntidType);
                break;
            case 14:
                strFieldValue = String.valueOf(AppntidNo);
                break;
            case 15:
                strFieldValue = String.valueOf(AppntaccNo);
                break;
            case 16:
                strFieldValue = String.valueOf(BizStatus);
                break;
            case 17:
                strFieldValue = String.valueOf(UwStatus);
                break;
            case 18:
                strFieldValue = String.valueOf(RespDate);
                break;
            case 19:
                strFieldValue = String.valueOf(RespTime);
                break;
            case 20:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 21:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 22:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 23:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 24:
                strFieldValue = String.valueOf(Bak1);
                break;
            case 25:
                strFieldValue = String.valueOf(Bak2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("LKNRTBizID")) {
            if( FValue != null && !FValue.equals("")) {
                LKNRTBizID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("TransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransCode = FValue.trim();
            }
            else
                TransCode = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("TranDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                TranDate = FValue.trim();
            }
            else
                TranDate = null;
        }
        if (FCode.equalsIgnoreCase("ZoneNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZoneNo = FValue.trim();
            }
            else
                ZoneNo = null;
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankNode = FValue.trim();
            }
            else
                BankNode = null;
        }
        if (FCode.equalsIgnoreCase("TellerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TellerNo = FValue.trim();
            }
            else
                TellerNo = null;
        }
        if (FCode.equalsIgnoreCase("TransNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransNo = FValue.trim();
            }
            else
                TransNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalNo = FValue.trim();
            }
            else
                ProposalNo = null;
        }
        if (FCode.equalsIgnoreCase("BanksaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                BanksaleChnl = FValue.trim();
            }
            else
                BanksaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("UwType")) {
            if( FValue != null && !FValue.equals(""))
            {
                UwType = FValue.trim();
            }
            else
                UwType = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AppntidType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntidType = FValue.trim();
            }
            else
                AppntidType = null;
        }
        if (FCode.equalsIgnoreCase("AppntidNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntidNo = FValue.trim();
            }
            else
                AppntidNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntaccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntaccNo = FValue.trim();
            }
            else
                AppntaccNo = null;
        }
        if (FCode.equalsIgnoreCase("BizStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                BizStatus = FValue.trim();
            }
            else
                BizStatus = null;
        }
        if (FCode.equalsIgnoreCase("UwStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                UwStatus = FValue.trim();
            }
            else
                UwStatus = null;
        }
        if (FCode.equalsIgnoreCase("RespDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                RespDate = FValue.trim();
            }
            else
                RespDate = null;
        }
        if (FCode.equalsIgnoreCase("RespTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                RespTime = FValue.trim();
            }
            else
                RespTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak1 = FValue.trim();
            }
            else
                Bak1 = null;
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak2 = FValue.trim();
            }
            else
                Bak2 = null;
        }
        return true;
    }


    public String toString() {
    return "LKNRTBizTransPojo [" +
            "LKNRTBizID="+LKNRTBizID +
            ", ShardingID="+ShardingID +
            ", TransCode="+TransCode +
            ", BankCode="+BankCode +
            ", TranDate="+TranDate +
            ", ZoneNo="+ZoneNo +
            ", BankNode="+BankNode +
            ", TellerNo="+TellerNo +
            ", TransNo="+TransNo +
            ", ProposalNo="+ProposalNo +
            ", BanksaleChnl="+BanksaleChnl +
            ", UwType="+UwType +
            ", AppntName="+AppntName +
            ", AppntidType="+AppntidType +
            ", AppntidNo="+AppntidNo +
            ", AppntaccNo="+AppntaccNo +
            ", BizStatus="+BizStatus +
            ", UwStatus="+UwStatus +
            ", RespDate="+RespDate +
            ", RespTime="+RespTime +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Bak1="+Bak1 +
            ", Bak2="+Bak2 +"]";
    }
}
