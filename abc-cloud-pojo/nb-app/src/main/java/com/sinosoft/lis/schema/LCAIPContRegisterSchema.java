/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCAIPContRegisterDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCAIPContRegisterSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-06-30
 */
public class LCAIPContRegisterSchema implements Schema, Cloneable {
    // @Field
    /** 合同号码 */
    private String ContNo;
    /** 单证印刷号码 */
    private String CardNo;
    /** 单证类型码 */
    private String CertifyCode;
    /** 网点 */
    private String AgentCom;
    /** 结算标记 */
    private String PayFlag;
    /** 结算日期 */
    private Date PayDate;
    /** 结算时间 */
    private String PayTime;
    /** 收费模式 */
    private String ChargeMode;
    /** 收费类型 */
    private String ChargeType;
    /** 渠道编码 */
    private String ChannelCode;
    /** 账户编码 */
    private String InBankCode;
    /** 账户号码 */
    private String InBankAccNo;
    /** 账户名称 */
    private String InBankName;

    public static final int FIELDNUM = 13;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCAIPContRegisterSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ContNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCAIPContRegisterSchema cloned = (LCAIPContRegisterSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getCardNo() {
        return CardNo;
    }
    public void setCardNo(String aCardNo) {
        CardNo = aCardNo;
    }
    public String getCertifyCode() {
        return CertifyCode;
    }
    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getPayFlag() {
        return PayFlag;
    }
    public void setPayFlag(String aPayFlag) {
        PayFlag = aPayFlag;
    }
    public String getPayDate() {
        if(PayDate != null) {
            return fDate.getString(PayDate);
        } else {
            return null;
        }
    }
    public void setPayDate(Date aPayDate) {
        PayDate = aPayDate;
    }
    public void setPayDate(String aPayDate) {
        if (aPayDate != null && !aPayDate.equals("")) {
            PayDate = fDate.getDate(aPayDate);
        } else
            PayDate = null;
    }

    public String getPayTime() {
        return PayTime;
    }
    public void setPayTime(String aPayTime) {
        PayTime = aPayTime;
    }
    public String getChargeMode() {
        return ChargeMode;
    }
    public void setChargeMode(String aChargeMode) {
        ChargeMode = aChargeMode;
    }
    public String getChargeType() {
        return ChargeType;
    }
    public void setChargeType(String aChargeType) {
        ChargeType = aChargeType;
    }
    public String getChannelCode() {
        return ChannelCode;
    }
    public void setChannelCode(String aChannelCode) {
        ChannelCode = aChannelCode;
    }
    public String getInBankCode() {
        return InBankCode;
    }
    public void setInBankCode(String aInBankCode) {
        InBankCode = aInBankCode;
    }
    public String getInBankAccNo() {
        return InBankAccNo;
    }
    public void setInBankAccNo(String aInBankAccNo) {
        InBankAccNo = aInBankAccNo;
    }
    public String getInBankName() {
        return InBankName;
    }
    public void setInBankName(String aInBankName) {
        InBankName = aInBankName;
    }

    /**
    * 使用另外一个 LCAIPContRegisterSchema 对象给 Schema 赋值
    * @param: aLCAIPContRegisterSchema LCAIPContRegisterSchema
    **/
    public void setSchema(LCAIPContRegisterSchema aLCAIPContRegisterSchema) {
        this.ContNo = aLCAIPContRegisterSchema.getContNo();
        this.CardNo = aLCAIPContRegisterSchema.getCardNo();
        this.CertifyCode = aLCAIPContRegisterSchema.getCertifyCode();
        this.AgentCom = aLCAIPContRegisterSchema.getAgentCom();
        this.PayFlag = aLCAIPContRegisterSchema.getPayFlag();
        this.PayDate = fDate.getDate( aLCAIPContRegisterSchema.getPayDate());
        this.PayTime = aLCAIPContRegisterSchema.getPayTime();
        this.ChargeMode = aLCAIPContRegisterSchema.getChargeMode();
        this.ChargeType = aLCAIPContRegisterSchema.getChargeType();
        this.ChannelCode = aLCAIPContRegisterSchema.getChannelCode();
        this.InBankCode = aLCAIPContRegisterSchema.getInBankCode();
        this.InBankAccNo = aLCAIPContRegisterSchema.getInBankAccNo();
        this.InBankName = aLCAIPContRegisterSchema.getInBankName();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("CardNo") == null )
                this.CardNo = null;
            else
                this.CardNo = rs.getString("CardNo").trim();

            if( rs.getString("CertifyCode") == null )
                this.CertifyCode = null;
            else
                this.CertifyCode = rs.getString("CertifyCode").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("PayFlag") == null )
                this.PayFlag = null;
            else
                this.PayFlag = rs.getString("PayFlag").trim();

            this.PayDate = rs.getDate("PayDate");
            if( rs.getString("PayTime") == null )
                this.PayTime = null;
            else
                this.PayTime = rs.getString("PayTime").trim();

            if( rs.getString("ChargeMode") == null )
                this.ChargeMode = null;
            else
                this.ChargeMode = rs.getString("ChargeMode").trim();

            if( rs.getString("ChargeType") == null )
                this.ChargeType = null;
            else
                this.ChargeType = rs.getString("ChargeType").trim();

            if( rs.getString("ChannelCode") == null )
                this.ChannelCode = null;
            else
                this.ChannelCode = rs.getString("ChannelCode").trim();

            if( rs.getString("InBankCode") == null )
                this.InBankCode = null;
            else
                this.InBankCode = rs.getString("InBankCode").trim();

            if( rs.getString("InBankAccNo") == null )
                this.InBankAccNo = null;
            else
                this.InBankAccNo = rs.getString("InBankAccNo").trim();

            if( rs.getString("InBankName") == null )
                this.InBankName = null;
            else
                this.InBankName = rs.getString("InBankName").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAIPContRegisterSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCAIPContRegisterSchema getSchema() {
        LCAIPContRegisterSchema aLCAIPContRegisterSchema = new LCAIPContRegisterSchema();
        aLCAIPContRegisterSchema.setSchema(this);
        return aLCAIPContRegisterSchema;
    }

    public LCAIPContRegisterDB getDB() {
        LCAIPContRegisterDB aDBOper = new LCAIPContRegisterDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAIPContRegister描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChargeMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChargeType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChannelCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InBankName));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAIPContRegister>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            CardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            PayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER));
            PayTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ChargeMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ChargeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ChannelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            InBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            InBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            InBankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAIPContRegisterSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("CardNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("PayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayFlag));
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
        }
        if (FCode.equalsIgnoreCase("PayTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTime));
        }
        if (FCode.equalsIgnoreCase("ChargeMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeMode));
        }
        if (FCode.equalsIgnoreCase("ChargeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeType));
        }
        if (FCode.equalsIgnoreCase("ChannelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelCode));
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankCode));
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankAccNo));
        }
        if (FCode.equalsIgnoreCase("InBankName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBankName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CardNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CertifyCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PayFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PayTime);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ChargeMode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ChargeType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ChannelCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InBankCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(InBankAccNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(InBankName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("CardNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CardNo = FValue.trim();
            }
            else
                CardNo = null;
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CertifyCode = FValue.trim();
            }
            else
                CertifyCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("PayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayFlag = FValue.trim();
            }
            else
                PayFlag = null;
        }
        if (FCode.equalsIgnoreCase("PayDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayDate = fDate.getDate( FValue );
            }
            else
                PayDate = null;
        }
        if (FCode.equalsIgnoreCase("PayTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayTime = FValue.trim();
            }
            else
                PayTime = null;
        }
        if (FCode.equalsIgnoreCase("ChargeMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeMode = FValue.trim();
            }
            else
                ChargeMode = null;
        }
        if (FCode.equalsIgnoreCase("ChargeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeType = FValue.trim();
            }
            else
                ChargeType = null;
        }
        if (FCode.equalsIgnoreCase("ChannelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelCode = FValue.trim();
            }
            else
                ChannelCode = null;
        }
        if (FCode.equalsIgnoreCase("InBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankCode = FValue.trim();
            }
            else
                InBankCode = null;
        }
        if (FCode.equalsIgnoreCase("InBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankAccNo = FValue.trim();
            }
            else
                InBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InBankName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InBankName = FValue.trim();
            }
            else
                InBankName = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCAIPContRegisterSchema other = (LCAIPContRegisterSchema)otherObject;
        return
            ContNo.equals(other.getContNo())
            && CardNo.equals(other.getCardNo())
            && CertifyCode.equals(other.getCertifyCode())
            && AgentCom.equals(other.getAgentCom())
            && PayFlag.equals(other.getPayFlag())
            && fDate.getString(PayDate).equals(other.getPayDate())
            && PayTime.equals(other.getPayTime())
            && ChargeMode.equals(other.getChargeMode())
            && ChargeType.equals(other.getChargeType())
            && ChannelCode.equals(other.getChannelCode())
            && InBankCode.equals(other.getInBankCode())
            && InBankAccNo.equals(other.getInBankAccNo())
            && InBankName.equals(other.getInBankName());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ContNo") ) {
            return 0;
        }
        if( strFieldName.equals("CardNo") ) {
            return 1;
        }
        if( strFieldName.equals("CertifyCode") ) {
            return 2;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 3;
        }
        if( strFieldName.equals("PayFlag") ) {
            return 4;
        }
        if( strFieldName.equals("PayDate") ) {
            return 5;
        }
        if( strFieldName.equals("PayTime") ) {
            return 6;
        }
        if( strFieldName.equals("ChargeMode") ) {
            return 7;
        }
        if( strFieldName.equals("ChargeType") ) {
            return 8;
        }
        if( strFieldName.equals("ChannelCode") ) {
            return 9;
        }
        if( strFieldName.equals("InBankCode") ) {
            return 10;
        }
        if( strFieldName.equals("InBankAccNo") ) {
            return 11;
        }
        if( strFieldName.equals("InBankName") ) {
            return 12;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "CardNo";
                break;
            case 2:
                strFieldName = "CertifyCode";
                break;
            case 3:
                strFieldName = "AgentCom";
                break;
            case 4:
                strFieldName = "PayFlag";
                break;
            case 5:
                strFieldName = "PayDate";
                break;
            case 6:
                strFieldName = "PayTime";
                break;
            case 7:
                strFieldName = "ChargeMode";
                break;
            case 8:
                strFieldName = "ChargeType";
                break;
            case 9:
                strFieldName = "ChannelCode";
                break;
            case 10:
                strFieldName = "InBankCode";
                break;
            case 11:
                strFieldName = "InBankAccNo";
                break;
            case 12:
                strFieldName = "InBankName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "CARDNO":
                return Schema.TYPE_STRING;
            case "CERTIFYCODE":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "PAYFLAG":
                return Schema.TYPE_STRING;
            case "PAYDATE":
                return Schema.TYPE_DATE;
            case "PAYTIME":
                return Schema.TYPE_STRING;
            case "CHARGEMODE":
                return Schema.TYPE_STRING;
            case "CHARGETYPE":
                return Schema.TYPE_STRING;
            case "CHANNELCODE":
                return Schema.TYPE_STRING;
            case "INBANKCODE":
                return Schema.TYPE_STRING;
            case "INBANKACCNO":
                return Schema.TYPE_STRING;
            case "INBANKNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DATE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
