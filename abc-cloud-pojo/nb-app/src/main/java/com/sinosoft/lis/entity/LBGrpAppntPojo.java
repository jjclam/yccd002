/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LBGrpAppntPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBGrpAppntPojo implements Pojo,Serializable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 客户号码 */
    private String CustomerNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 客户地址号码 */
    private String AddressNo; 
    /** 投保人级别 */
    private String AppntGrade; 
    /** 单位名称 */
    private String Name; 
    /** 通讯地址 */
    private String PostalAddress; 
    /** 单位邮编 */
    private String ZipCode; 
    /** 单位电话 */
    private String Phone; 
    /** 密码 */
    private String Password; 
    /** 状态 */
    private String State; 
    /** 投保人类型 */
    private String AppntType; 
    /** 被保人与投保人关系 */
    private String RelationToInsured; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 行业分类 */
    private String BusiCategory; 
    /** 单位性质 */
    private String GrpNature; 
    /** 员工总人数 */
    private int SumNumPeople; 
    /** 经营范围 */
    private String MainBusiness; 
    /** 法人 */
    private String Corporation; 
    /** 法人证件类型 */
    private String CorIDType; 
    /** 法人证件号码 */
    private String CorID; 
    /** 联系人证件有效止期 */
    private String  CorIDExpiryDate;
    /** 在职人数 */
    private int OnJobNumber; 
    /** 退休人数 */
    private int RetireNumber; 
    /** 其它人数 */
    private int OtherNumber; 
    /** 注册资本 */
    private double RgtCapital; 
    /** 资产总额 */
    private double TotalAssets; 
    /** 净资产收益率 */
    private double NetProfitRate; 
    /** 负责人 */
    private String Satrap; 
    /** 实际控制人 */
    private String ActuCtrl; 
    /** 营业执照号 */
    private String License; 
    /** 社保保险登记证号 */
    private String SocialInsuCode; 
    /** 组织机构代码 */
    private String OrganizationCode; 
    /** 税务登记证 */
    private String TaxCode; 
    /** 单位传真 */
    private String Fax; 
    /** 单位emaill */
    private String EMail; 
    /** 成立日期 */
    private String  FoundDate;
    /** 备注 */
    private String Remark; 
    /** 备用字段1 */
    private String Segment1; 
    /** 备用字段2 */
    private String Segment2; 
    /** 备用字段3 */
    private String Segment3; 
    /** 管理机构 */
    private String ManageCom; 
    /** 公司代码 */
    private String ComCode; 
    /** 最后一次修改操作员 */
    private String ModifyOperator; 
    /** 性别 */
    private String Sex; 
    /** 国籍 */
    private String NativePlace; 
    /** 职业描述 */
    private String Occupation; 
    /** 职业编码 */
    private String OccupationCode; 
    /** 纳税类型 */
    private String TaxPayerType; 
    /** 是否固定方案 */
    private String FixedPlan; 
    /** 是否开具有增值税的专用发票 */
    private String AddTaxFlag; 
    /** 取得增值税人资质日期 */
    private String  AddTaxDate;
    /** 银行账号(税务登记) */
    private String BankAccTaxNo; 
    /** 开户银行全称(税务登记) */
    private String BankTaxName; 
    /** 开户银行编码(税务登记) */
    private String BankTaxCode; 
    /** 投保团体证件类型 */
    private String GrpAppIDType; 
    /** 投保团体证件有效期 */
    private String  GrpAppIDExpDate;
    /** 控股股东/实际控制人 */
    private String CtrPeople; 
    /** 单位可投保人数 */
    private int Peoples3; 
    /** 连带投保人数 */
    private int RelaPeoples; 
    /** 连带配偶投保人数 */
    private int RelaMatePeoples; 
    /** 连带子女投保人数 */
    private int RelaYoungPeoples; 
    /** 连带其它投保人数 */
    private int RelaOtherPeoples; 


    public static final int FIELDNUM = 67;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getAddressNo() {
        return AddressNo;
    }
    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }
    public String getAppntGrade() {
        return AppntGrade;
    }
    public void setAppntGrade(String aAppntGrade) {
        AppntGrade = aAppntGrade;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getPostalAddress() {
        return PostalAddress;
    }
    public void setPostalAddress(String aPostalAddress) {
        PostalAddress = aPostalAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getAppntType() {
        return AppntType;
    }
    public void setAppntType(String aAppntType) {
        AppntType = aAppntType;
    }
    public String getRelationToInsured() {
        return RelationToInsured;
    }
    public void setRelationToInsured(String aRelationToInsured) {
        RelationToInsured = aRelationToInsured;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBusiCategory() {
        return BusiCategory;
    }
    public void setBusiCategory(String aBusiCategory) {
        BusiCategory = aBusiCategory;
    }
    public String getGrpNature() {
        return GrpNature;
    }
    public void setGrpNature(String aGrpNature) {
        GrpNature = aGrpNature;
    }
    public int getSumNumPeople() {
        return SumNumPeople;
    }
    public void setSumNumPeople(int aSumNumPeople) {
        SumNumPeople = aSumNumPeople;
    }
    public void setSumNumPeople(String aSumNumPeople) {
        if (aSumNumPeople != null && !aSumNumPeople.equals("")) {
            Integer tInteger = new Integer(aSumNumPeople);
            int i = tInteger.intValue();
            SumNumPeople = i;
        }
    }

    public String getMainBusiness() {
        return MainBusiness;
    }
    public void setMainBusiness(String aMainBusiness) {
        MainBusiness = aMainBusiness;
    }
    public String getCorporation() {
        return Corporation;
    }
    public void setCorporation(String aCorporation) {
        Corporation = aCorporation;
    }
    public String getCorIDType() {
        return CorIDType;
    }
    public void setCorIDType(String aCorIDType) {
        CorIDType = aCorIDType;
    }
    public String getCorID() {
        return CorID;
    }
    public void setCorID(String aCorID) {
        CorID = aCorID;
    }
    public String getCorIDExpiryDate() {
        return CorIDExpiryDate;
    }
    public void setCorIDExpiryDate(String aCorIDExpiryDate) {
        CorIDExpiryDate = aCorIDExpiryDate;
    }
    public int getOnJobNumber() {
        return OnJobNumber;
    }
    public void setOnJobNumber(int aOnJobNumber) {
        OnJobNumber = aOnJobNumber;
    }
    public void setOnJobNumber(String aOnJobNumber) {
        if (aOnJobNumber != null && !aOnJobNumber.equals("")) {
            Integer tInteger = new Integer(aOnJobNumber);
            int i = tInteger.intValue();
            OnJobNumber = i;
        }
    }

    public int getRetireNumber() {
        return RetireNumber;
    }
    public void setRetireNumber(int aRetireNumber) {
        RetireNumber = aRetireNumber;
    }
    public void setRetireNumber(String aRetireNumber) {
        if (aRetireNumber != null && !aRetireNumber.equals("")) {
            Integer tInteger = new Integer(aRetireNumber);
            int i = tInteger.intValue();
            RetireNumber = i;
        }
    }

    public int getOtherNumber() {
        return OtherNumber;
    }
    public void setOtherNumber(int aOtherNumber) {
        OtherNumber = aOtherNumber;
    }
    public void setOtherNumber(String aOtherNumber) {
        if (aOtherNumber != null && !aOtherNumber.equals("")) {
            Integer tInteger = new Integer(aOtherNumber);
            int i = tInteger.intValue();
            OtherNumber = i;
        }
    }

    public double getRgtCapital() {
        return RgtCapital;
    }
    public void setRgtCapital(double aRgtCapital) {
        RgtCapital = aRgtCapital;
    }
    public void setRgtCapital(String aRgtCapital) {
        if (aRgtCapital != null && !aRgtCapital.equals("")) {
            Double tDouble = new Double(aRgtCapital);
            double d = tDouble.doubleValue();
            RgtCapital = d;
        }
    }

    public double getTotalAssets() {
        return TotalAssets;
    }
    public void setTotalAssets(double aTotalAssets) {
        TotalAssets = aTotalAssets;
    }
    public void setTotalAssets(String aTotalAssets) {
        if (aTotalAssets != null && !aTotalAssets.equals("")) {
            Double tDouble = new Double(aTotalAssets);
            double d = tDouble.doubleValue();
            TotalAssets = d;
        }
    }

    public double getNetProfitRate() {
        return NetProfitRate;
    }
    public void setNetProfitRate(double aNetProfitRate) {
        NetProfitRate = aNetProfitRate;
    }
    public void setNetProfitRate(String aNetProfitRate) {
        if (aNetProfitRate != null && !aNetProfitRate.equals("")) {
            Double tDouble = new Double(aNetProfitRate);
            double d = tDouble.doubleValue();
            NetProfitRate = d;
        }
    }

    public String getSatrap() {
        return Satrap;
    }
    public void setSatrap(String aSatrap) {
        Satrap = aSatrap;
    }
    public String getActuCtrl() {
        return ActuCtrl;
    }
    public void setActuCtrl(String aActuCtrl) {
        ActuCtrl = aActuCtrl;
    }
    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getSocialInsuCode() {
        return SocialInsuCode;
    }
    public void setSocialInsuCode(String aSocialInsuCode) {
        SocialInsuCode = aSocialInsuCode;
    }
    public String getOrganizationCode() {
        return OrganizationCode;
    }
    public void setOrganizationCode(String aOrganizationCode) {
        OrganizationCode = aOrganizationCode;
    }
    public String getTaxCode() {
        return TaxCode;
    }
    public void setTaxCode(String aTaxCode) {
        TaxCode = aTaxCode;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getFoundDate() {
        return FoundDate;
    }
    public void setFoundDate(String aFoundDate) {
        FoundDate = aFoundDate;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getSegment1() {
        return Segment1;
    }
    public void setSegment1(String aSegment1) {
        Segment1 = aSegment1;
    }
    public String getSegment2() {
        return Segment2;
    }
    public void setSegment2(String aSegment2) {
        Segment2 = aSegment2;
    }
    public String getSegment3() {
        return Segment3;
    }
    public void setSegment3(String aSegment3) {
        Segment3 = aSegment3;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getOccupation() {
        return Occupation;
    }
    public void setOccupation(String aOccupation) {
        Occupation = aOccupation;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getTaxPayerType() {
        return TaxPayerType;
    }
    public void setTaxPayerType(String aTaxPayerType) {
        TaxPayerType = aTaxPayerType;
    }
    public String getFixedPlan() {
        return FixedPlan;
    }
    public void setFixedPlan(String aFixedPlan) {
        FixedPlan = aFixedPlan;
    }
    public String getAddTaxFlag() {
        return AddTaxFlag;
    }
    public void setAddTaxFlag(String aAddTaxFlag) {
        AddTaxFlag = aAddTaxFlag;
    }
    public String getAddTaxDate() {
        return AddTaxDate;
    }
    public void setAddTaxDate(String aAddTaxDate) {
        AddTaxDate = aAddTaxDate;
    }
    public String getBankAccTaxNo() {
        return BankAccTaxNo;
    }
    public void setBankAccTaxNo(String aBankAccTaxNo) {
        BankAccTaxNo = aBankAccTaxNo;
    }
    public String getBankTaxName() {
        return BankTaxName;
    }
    public void setBankTaxName(String aBankTaxName) {
        BankTaxName = aBankTaxName;
    }
    public String getBankTaxCode() {
        return BankTaxCode;
    }
    public void setBankTaxCode(String aBankTaxCode) {
        BankTaxCode = aBankTaxCode;
    }
    public String getGrpAppIDType() {
        return GrpAppIDType;
    }
    public void setGrpAppIDType(String aGrpAppIDType) {
        GrpAppIDType = aGrpAppIDType;
    }
    public String getGrpAppIDExpDate() {
        return GrpAppIDExpDate;
    }
    public void setGrpAppIDExpDate(String aGrpAppIDExpDate) {
        GrpAppIDExpDate = aGrpAppIDExpDate;
    }
    public String getCtrPeople() {
        return CtrPeople;
    }
    public void setCtrPeople(String aCtrPeople) {
        CtrPeople = aCtrPeople;
    }
    public int getPeoples3() {
        return Peoples3;
    }
    public void setPeoples3(int aPeoples3) {
        Peoples3 = aPeoples3;
    }
    public void setPeoples3(String aPeoples3) {
        if (aPeoples3 != null && !aPeoples3.equals("")) {
            Integer tInteger = new Integer(aPeoples3);
            int i = tInteger.intValue();
            Peoples3 = i;
        }
    }

    public int getRelaPeoples() {
        return RelaPeoples;
    }
    public void setRelaPeoples(int aRelaPeoples) {
        RelaPeoples = aRelaPeoples;
    }
    public void setRelaPeoples(String aRelaPeoples) {
        if (aRelaPeoples != null && !aRelaPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaPeoples);
            int i = tInteger.intValue();
            RelaPeoples = i;
        }
    }

    public int getRelaMatePeoples() {
        return RelaMatePeoples;
    }
    public void setRelaMatePeoples(int aRelaMatePeoples) {
        RelaMatePeoples = aRelaMatePeoples;
    }
    public void setRelaMatePeoples(String aRelaMatePeoples) {
        if (aRelaMatePeoples != null && !aRelaMatePeoples.equals("")) {
            Integer tInteger = new Integer(aRelaMatePeoples);
            int i = tInteger.intValue();
            RelaMatePeoples = i;
        }
    }

    public int getRelaYoungPeoples() {
        return RelaYoungPeoples;
    }
    public void setRelaYoungPeoples(int aRelaYoungPeoples) {
        RelaYoungPeoples = aRelaYoungPeoples;
    }
    public void setRelaYoungPeoples(String aRelaYoungPeoples) {
        if (aRelaYoungPeoples != null && !aRelaYoungPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaYoungPeoples);
            int i = tInteger.intValue();
            RelaYoungPeoples = i;
        }
    }

    public int getRelaOtherPeoples() {
        return RelaOtherPeoples;
    }
    public void setRelaOtherPeoples(int aRelaOtherPeoples) {
        RelaOtherPeoples = aRelaOtherPeoples;
    }
    public void setRelaOtherPeoples(String aRelaOtherPeoples) {
        if (aRelaOtherPeoples != null && !aRelaOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaOtherPeoples);
            int i = tInteger.intValue();
            RelaOtherPeoples = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 1;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 2;
        }
        if( strFieldName.equals("AddressNo") ) {
            return 3;
        }
        if( strFieldName.equals("AppntGrade") ) {
            return 4;
        }
        if( strFieldName.equals("Name") ) {
            return 5;
        }
        if( strFieldName.equals("PostalAddress") ) {
            return 6;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 7;
        }
        if( strFieldName.equals("Phone") ) {
            return 8;
        }
        if( strFieldName.equals("Password") ) {
            return 9;
        }
        if( strFieldName.equals("State") ) {
            return 10;
        }
        if( strFieldName.equals("AppntType") ) {
            return 11;
        }
        if( strFieldName.equals("RelationToInsured") ) {
            return 12;
        }
        if( strFieldName.equals("Operator") ) {
            return 13;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 14;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 17;
        }
        if( strFieldName.equals("BusiCategory") ) {
            return 18;
        }
        if( strFieldName.equals("GrpNature") ) {
            return 19;
        }
        if( strFieldName.equals("SumNumPeople") ) {
            return 20;
        }
        if( strFieldName.equals("MainBusiness") ) {
            return 21;
        }
        if( strFieldName.equals("Corporation") ) {
            return 22;
        }
        if( strFieldName.equals("CorIDType") ) {
            return 23;
        }
        if( strFieldName.equals("CorID") ) {
            return 24;
        }
        if( strFieldName.equals("CorIDExpiryDate") ) {
            return 25;
        }
        if( strFieldName.equals("OnJobNumber") ) {
            return 26;
        }
        if( strFieldName.equals("RetireNumber") ) {
            return 27;
        }
        if( strFieldName.equals("OtherNumber") ) {
            return 28;
        }
        if( strFieldName.equals("RgtCapital") ) {
            return 29;
        }
        if( strFieldName.equals("TotalAssets") ) {
            return 30;
        }
        if( strFieldName.equals("NetProfitRate") ) {
            return 31;
        }
        if( strFieldName.equals("Satrap") ) {
            return 32;
        }
        if( strFieldName.equals("ActuCtrl") ) {
            return 33;
        }
        if( strFieldName.equals("License") ) {
            return 34;
        }
        if( strFieldName.equals("SocialInsuCode") ) {
            return 35;
        }
        if( strFieldName.equals("OrganizationCode") ) {
            return 36;
        }
        if( strFieldName.equals("TaxCode") ) {
            return 37;
        }
        if( strFieldName.equals("Fax") ) {
            return 38;
        }
        if( strFieldName.equals("EMail") ) {
            return 39;
        }
        if( strFieldName.equals("FoundDate") ) {
            return 40;
        }
        if( strFieldName.equals("Remark") ) {
            return 41;
        }
        if( strFieldName.equals("Segment1") ) {
            return 42;
        }
        if( strFieldName.equals("Segment2") ) {
            return 43;
        }
        if( strFieldName.equals("Segment3") ) {
            return 44;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 45;
        }
        if( strFieldName.equals("ComCode") ) {
            return 46;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 47;
        }
        if( strFieldName.equals("Sex") ) {
            return 48;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 49;
        }
        if( strFieldName.equals("Occupation") ) {
            return 50;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 51;
        }
        if( strFieldName.equals("TaxPayerType") ) {
            return 52;
        }
        if( strFieldName.equals("FixedPlan") ) {
            return 53;
        }
        if( strFieldName.equals("AddTaxFlag") ) {
            return 54;
        }
        if( strFieldName.equals("AddTaxDate") ) {
            return 55;
        }
        if( strFieldName.equals("BankAccTaxNo") ) {
            return 56;
        }
        if( strFieldName.equals("BankTaxName") ) {
            return 57;
        }
        if( strFieldName.equals("BankTaxCode") ) {
            return 58;
        }
        if( strFieldName.equals("GrpAppIDType") ) {
            return 59;
        }
        if( strFieldName.equals("GrpAppIDExpDate") ) {
            return 60;
        }
        if( strFieldName.equals("CtrPeople") ) {
            return 61;
        }
        if( strFieldName.equals("Peoples3") ) {
            return 62;
        }
        if( strFieldName.equals("RelaPeoples") ) {
            return 63;
        }
        if( strFieldName.equals("RelaMatePeoples") ) {
            return 64;
        }
        if( strFieldName.equals("RelaYoungPeoples") ) {
            return 65;
        }
        if( strFieldName.equals("RelaOtherPeoples") ) {
            return 66;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "PrtNo";
                break;
            case 3:
                strFieldName = "AddressNo";
                break;
            case 4:
                strFieldName = "AppntGrade";
                break;
            case 5:
                strFieldName = "Name";
                break;
            case 6:
                strFieldName = "PostalAddress";
                break;
            case 7:
                strFieldName = "ZipCode";
                break;
            case 8:
                strFieldName = "Phone";
                break;
            case 9:
                strFieldName = "Password";
                break;
            case 10:
                strFieldName = "State";
                break;
            case 11:
                strFieldName = "AppntType";
                break;
            case 12:
                strFieldName = "RelationToInsured";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            case 18:
                strFieldName = "BusiCategory";
                break;
            case 19:
                strFieldName = "GrpNature";
                break;
            case 20:
                strFieldName = "SumNumPeople";
                break;
            case 21:
                strFieldName = "MainBusiness";
                break;
            case 22:
                strFieldName = "Corporation";
                break;
            case 23:
                strFieldName = "CorIDType";
                break;
            case 24:
                strFieldName = "CorID";
                break;
            case 25:
                strFieldName = "CorIDExpiryDate";
                break;
            case 26:
                strFieldName = "OnJobNumber";
                break;
            case 27:
                strFieldName = "RetireNumber";
                break;
            case 28:
                strFieldName = "OtherNumber";
                break;
            case 29:
                strFieldName = "RgtCapital";
                break;
            case 30:
                strFieldName = "TotalAssets";
                break;
            case 31:
                strFieldName = "NetProfitRate";
                break;
            case 32:
                strFieldName = "Satrap";
                break;
            case 33:
                strFieldName = "ActuCtrl";
                break;
            case 34:
                strFieldName = "License";
                break;
            case 35:
                strFieldName = "SocialInsuCode";
                break;
            case 36:
                strFieldName = "OrganizationCode";
                break;
            case 37:
                strFieldName = "TaxCode";
                break;
            case 38:
                strFieldName = "Fax";
                break;
            case 39:
                strFieldName = "EMail";
                break;
            case 40:
                strFieldName = "FoundDate";
                break;
            case 41:
                strFieldName = "Remark";
                break;
            case 42:
                strFieldName = "Segment1";
                break;
            case 43:
                strFieldName = "Segment2";
                break;
            case 44:
                strFieldName = "Segment3";
                break;
            case 45:
                strFieldName = "ManageCom";
                break;
            case 46:
                strFieldName = "ComCode";
                break;
            case 47:
                strFieldName = "ModifyOperator";
                break;
            case 48:
                strFieldName = "Sex";
                break;
            case 49:
                strFieldName = "NativePlace";
                break;
            case 50:
                strFieldName = "Occupation";
                break;
            case 51:
                strFieldName = "OccupationCode";
                break;
            case 52:
                strFieldName = "TaxPayerType";
                break;
            case 53:
                strFieldName = "FixedPlan";
                break;
            case 54:
                strFieldName = "AddTaxFlag";
                break;
            case 55:
                strFieldName = "AddTaxDate";
                break;
            case 56:
                strFieldName = "BankAccTaxNo";
                break;
            case 57:
                strFieldName = "BankTaxName";
                break;
            case 58:
                strFieldName = "BankTaxCode";
                break;
            case 59:
                strFieldName = "GrpAppIDType";
                break;
            case 60:
                strFieldName = "GrpAppIDExpDate";
                break;
            case 61:
                strFieldName = "CtrPeople";
                break;
            case 62:
                strFieldName = "Peoples3";
                break;
            case 63:
                strFieldName = "RelaPeoples";
                break;
            case 64:
                strFieldName = "RelaMatePeoples";
                break;
            case 65:
                strFieldName = "RelaYoungPeoples";
                break;
            case 66:
                strFieldName = "RelaOtherPeoples";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "ADDRESSNO":
                return Schema.TYPE_STRING;
            case "APPNTGRADE":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "POSTALADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "APPNTTYPE":
                return Schema.TYPE_STRING;
            case "RELATIONTOINSURED":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BUSICATEGORY":
                return Schema.TYPE_STRING;
            case "GRPNATURE":
                return Schema.TYPE_STRING;
            case "SUMNUMPEOPLE":
                return Schema.TYPE_INT;
            case "MAINBUSINESS":
                return Schema.TYPE_STRING;
            case "CORPORATION":
                return Schema.TYPE_STRING;
            case "CORIDTYPE":
                return Schema.TYPE_STRING;
            case "CORID":
                return Schema.TYPE_STRING;
            case "CORIDEXPIRYDATE":
                return Schema.TYPE_STRING;
            case "ONJOBNUMBER":
                return Schema.TYPE_INT;
            case "RETIRENUMBER":
                return Schema.TYPE_INT;
            case "OTHERNUMBER":
                return Schema.TYPE_INT;
            case "RGTCAPITAL":
                return Schema.TYPE_DOUBLE;
            case "TOTALASSETS":
                return Schema.TYPE_DOUBLE;
            case "NETPROFITRATE":
                return Schema.TYPE_DOUBLE;
            case "SATRAP":
                return Schema.TYPE_STRING;
            case "ACTUCTRL":
                return Schema.TYPE_STRING;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "SOCIALINSUCODE":
                return Schema.TYPE_STRING;
            case "ORGANIZATIONCODE":
                return Schema.TYPE_STRING;
            case "TAXCODE":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "FOUNDDATE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "SEGMENT1":
                return Schema.TYPE_STRING;
            case "SEGMENT2":
                return Schema.TYPE_STRING;
            case "SEGMENT3":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "OCCUPATION":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "TAXPAYERTYPE":
                return Schema.TYPE_STRING;
            case "FIXEDPLAN":
                return Schema.TYPE_STRING;
            case "ADDTAXFLAG":
                return Schema.TYPE_STRING;
            case "ADDTAXDATE":
                return Schema.TYPE_STRING;
            case "BANKACCTAXNO":
                return Schema.TYPE_STRING;
            case "BANKTAXNAME":
                return Schema.TYPE_STRING;
            case "BANKTAXCODE":
                return Schema.TYPE_STRING;
            case "GRPAPPIDTYPE":
                return Schema.TYPE_STRING;
            case "GRPAPPIDEXPDATE":
                return Schema.TYPE_STRING;
            case "CTRPEOPLE":
                return Schema.TYPE_STRING;
            case "PEOPLES3":
                return Schema.TYPE_INT;
            case "RELAPEOPLES":
                return Schema.TYPE_INT;
            case "RELAMATEPEOPLES":
                return Schema.TYPE_INT;
            case "RELAYOUNGPEOPLES":
                return Schema.TYPE_INT;
            case "RELAOTHERPEOPLES":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_INT;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_INT;
            case 27:
                return Schema.TYPE_INT;
            case 28:
                return Schema.TYPE_INT;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_DOUBLE;
            case 31:
                return Schema.TYPE_DOUBLE;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_INT;
            case 63:
                return Schema.TYPE_INT;
            case 64:
                return Schema.TYPE_INT;
            case 65:
                return Schema.TYPE_INT;
            case 66:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equalsIgnoreCase("AppntGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntGrade));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
        }
        if (FCode.equalsIgnoreCase("RelationToInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToInsured));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BusiCategory")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusiCategory));
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
        }
        if (FCode.equalsIgnoreCase("SumNumPeople")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumNumPeople));
        }
        if (FCode.equalsIgnoreCase("MainBusiness")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainBusiness));
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Corporation));
        }
        if (FCode.equalsIgnoreCase("CorIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorIDType));
        }
        if (FCode.equalsIgnoreCase("CorID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorID));
        }
        if (FCode.equalsIgnoreCase("CorIDExpiryDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorIDExpiryDate));
        }
        if (FCode.equalsIgnoreCase("OnJobNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OnJobNumber));
        }
        if (FCode.equalsIgnoreCase("RetireNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetireNumber));
        }
        if (FCode.equalsIgnoreCase("OtherNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNumber));
        }
        if (FCode.equalsIgnoreCase("RgtCapital")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtCapital));
        }
        if (FCode.equalsIgnoreCase("TotalAssets")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TotalAssets));
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NetProfitRate));
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Satrap));
        }
        if (FCode.equalsIgnoreCase("ActuCtrl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuCtrl));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("SocialInsuCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuCode));
        }
        if (FCode.equalsIgnoreCase("OrganizationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrganizationCode));
        }
        if (FCode.equalsIgnoreCase("TaxCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxCode));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FoundDate));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Segment1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Segment1));
        }
        if (FCode.equalsIgnoreCase("Segment2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Segment2));
        }
        if (FCode.equalsIgnoreCase("Segment3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Segment3));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Occupation));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("TaxPayerType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxPayerType));
        }
        if (FCode.equalsIgnoreCase("FixedPlan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FixedPlan));
        }
        if (FCode.equalsIgnoreCase("AddTaxFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddTaxFlag));
        }
        if (FCode.equalsIgnoreCase("AddTaxDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddTaxDate));
        }
        if (FCode.equalsIgnoreCase("BankAccTaxNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccTaxNo));
        }
        if (FCode.equalsIgnoreCase("BankTaxName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankTaxName));
        }
        if (FCode.equalsIgnoreCase("BankTaxCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankTaxCode));
        }
        if (FCode.equalsIgnoreCase("GrpAppIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAppIDType));
        }
        if (FCode.equalsIgnoreCase("GrpAppIDExpDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAppIDExpDate));
        }
        if (FCode.equalsIgnoreCase("CtrPeople")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CtrPeople));
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
        }
        if (FCode.equalsIgnoreCase("RelaPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPeoples));
        }
        if (FCode.equalsIgnoreCase("RelaMatePeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaMatePeoples));
        }
        if (FCode.equalsIgnoreCase("RelaYoungPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaYoungPeoples));
        }
        if (FCode.equalsIgnoreCase("RelaOtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherPeoples));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 2:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 3:
                strFieldValue = String.valueOf(AddressNo);
                break;
            case 4:
                strFieldValue = String.valueOf(AppntGrade);
                break;
            case 5:
                strFieldValue = String.valueOf(Name);
                break;
            case 6:
                strFieldValue = String.valueOf(PostalAddress);
                break;
            case 7:
                strFieldValue = String.valueOf(ZipCode);
                break;
            case 8:
                strFieldValue = String.valueOf(Phone);
                break;
            case 9:
                strFieldValue = String.valueOf(Password);
                break;
            case 10:
                strFieldValue = String.valueOf(State);
                break;
            case 11:
                strFieldValue = String.valueOf(AppntType);
                break;
            case 12:
                strFieldValue = String.valueOf(RelationToInsured);
                break;
            case 13:
                strFieldValue = String.valueOf(Operator);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 16:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 17:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 18:
                strFieldValue = String.valueOf(BusiCategory);
                break;
            case 19:
                strFieldValue = String.valueOf(GrpNature);
                break;
            case 20:
                strFieldValue = String.valueOf(SumNumPeople);
                break;
            case 21:
                strFieldValue = String.valueOf(MainBusiness);
                break;
            case 22:
                strFieldValue = String.valueOf(Corporation);
                break;
            case 23:
                strFieldValue = String.valueOf(CorIDType);
                break;
            case 24:
                strFieldValue = String.valueOf(CorID);
                break;
            case 25:
                strFieldValue = String.valueOf(CorIDExpiryDate);
                break;
            case 26:
                strFieldValue = String.valueOf(OnJobNumber);
                break;
            case 27:
                strFieldValue = String.valueOf(RetireNumber);
                break;
            case 28:
                strFieldValue = String.valueOf(OtherNumber);
                break;
            case 29:
                strFieldValue = String.valueOf(RgtCapital);
                break;
            case 30:
                strFieldValue = String.valueOf(TotalAssets);
                break;
            case 31:
                strFieldValue = String.valueOf(NetProfitRate);
                break;
            case 32:
                strFieldValue = String.valueOf(Satrap);
                break;
            case 33:
                strFieldValue = String.valueOf(ActuCtrl);
                break;
            case 34:
                strFieldValue = String.valueOf(License);
                break;
            case 35:
                strFieldValue = String.valueOf(SocialInsuCode);
                break;
            case 36:
                strFieldValue = String.valueOf(OrganizationCode);
                break;
            case 37:
                strFieldValue = String.valueOf(TaxCode);
                break;
            case 38:
                strFieldValue = String.valueOf(Fax);
                break;
            case 39:
                strFieldValue = String.valueOf(EMail);
                break;
            case 40:
                strFieldValue = String.valueOf(FoundDate);
                break;
            case 41:
                strFieldValue = String.valueOf(Remark);
                break;
            case 42:
                strFieldValue = String.valueOf(Segment1);
                break;
            case 43:
                strFieldValue = String.valueOf(Segment2);
                break;
            case 44:
                strFieldValue = String.valueOf(Segment3);
                break;
            case 45:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 46:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 47:
                strFieldValue = String.valueOf(ModifyOperator);
                break;
            case 48:
                strFieldValue = String.valueOf(Sex);
                break;
            case 49:
                strFieldValue = String.valueOf(NativePlace);
                break;
            case 50:
                strFieldValue = String.valueOf(Occupation);
                break;
            case 51:
                strFieldValue = String.valueOf(OccupationCode);
                break;
            case 52:
                strFieldValue = String.valueOf(TaxPayerType);
                break;
            case 53:
                strFieldValue = String.valueOf(FixedPlan);
                break;
            case 54:
                strFieldValue = String.valueOf(AddTaxFlag);
                break;
            case 55:
                strFieldValue = String.valueOf(AddTaxDate);
                break;
            case 56:
                strFieldValue = String.valueOf(BankAccTaxNo);
                break;
            case 57:
                strFieldValue = String.valueOf(BankTaxName);
                break;
            case 58:
                strFieldValue = String.valueOf(BankTaxCode);
                break;
            case 59:
                strFieldValue = String.valueOf(GrpAppIDType);
                break;
            case 60:
                strFieldValue = String.valueOf(GrpAppIDExpDate);
                break;
            case 61:
                strFieldValue = String.valueOf(CtrPeople);
                break;
            case 62:
                strFieldValue = String.valueOf(Peoples3);
                break;
            case 63:
                strFieldValue = String.valueOf(RelaPeoples);
                break;
            case 64:
                strFieldValue = String.valueOf(RelaMatePeoples);
                break;
            case 65:
                strFieldValue = String.valueOf(RelaYoungPeoples);
                break;
            case 66:
                strFieldValue = String.valueOf(RelaOtherPeoples);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddressNo = FValue.trim();
            }
            else
                AddressNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntGrade = FValue.trim();
            }
            else
                AppntGrade = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("PostalAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
                PostalAddress = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntType = FValue.trim();
            }
            else
                AppntType = null;
        }
        if (FCode.equalsIgnoreCase("RelationToInsured")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToInsured = FValue.trim();
            }
            else
                RelationToInsured = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BusiCategory")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusiCategory = FValue.trim();
            }
            else
                BusiCategory = null;
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNature = FValue.trim();
            }
            else
                GrpNature = null;
        }
        if (FCode.equalsIgnoreCase("SumNumPeople")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                SumNumPeople = i;
            }
        }
        if (FCode.equalsIgnoreCase("MainBusiness")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainBusiness = FValue.trim();
            }
            else
                MainBusiness = null;
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Corporation = FValue.trim();
            }
            else
                Corporation = null;
        }
        if (FCode.equalsIgnoreCase("CorIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorIDType = FValue.trim();
            }
            else
                CorIDType = null;
        }
        if (FCode.equalsIgnoreCase("CorID")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorID = FValue.trim();
            }
            else
                CorID = null;
        }
        if (FCode.equalsIgnoreCase("CorIDExpiryDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorIDExpiryDate = FValue.trim();
            }
            else
                CorIDExpiryDate = null;
        }
        if (FCode.equalsIgnoreCase("OnJobNumber")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OnJobNumber = i;
            }
        }
        if (FCode.equalsIgnoreCase("RetireNumber")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RetireNumber = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherNumber")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OtherNumber = i;
            }
        }
        if (FCode.equalsIgnoreCase("RgtCapital")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RgtCapital = d;
            }
        }
        if (FCode.equalsIgnoreCase("TotalAssets")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TotalAssets = d;
            }
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NetProfitRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            if( FValue != null && !FValue.equals(""))
            {
                Satrap = FValue.trim();
            }
            else
                Satrap = null;
        }
        if (FCode.equalsIgnoreCase("ActuCtrl")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActuCtrl = FValue.trim();
            }
            else
                ActuCtrl = null;
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuCode = FValue.trim();
            }
            else
                SocialInsuCode = null;
        }
        if (FCode.equalsIgnoreCase("OrganizationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrganizationCode = FValue.trim();
            }
            else
                OrganizationCode = null;
        }
        if (FCode.equalsIgnoreCase("TaxCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxCode = FValue.trim();
            }
            else
                TaxCode = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FoundDate = FValue.trim();
            }
            else
                FoundDate = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Segment1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Segment1 = FValue.trim();
            }
            else
                Segment1 = null;
        }
        if (FCode.equalsIgnoreCase("Segment2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Segment2 = FValue.trim();
            }
            else
                Segment2 = null;
        }
        if (FCode.equalsIgnoreCase("Segment3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Segment3 = FValue.trim();
            }
            else
                Segment3 = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Occupation = FValue.trim();
            }
            else
                Occupation = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("TaxPayerType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxPayerType = FValue.trim();
            }
            else
                TaxPayerType = null;
        }
        if (FCode.equalsIgnoreCase("FixedPlan")) {
            if( FValue != null && !FValue.equals(""))
            {
                FixedPlan = FValue.trim();
            }
            else
                FixedPlan = null;
        }
        if (FCode.equalsIgnoreCase("AddTaxFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddTaxFlag = FValue.trim();
            }
            else
                AddTaxFlag = null;
        }
        if (FCode.equalsIgnoreCase("AddTaxDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddTaxDate = FValue.trim();
            }
            else
                AddTaxDate = null;
        }
        if (FCode.equalsIgnoreCase("BankAccTaxNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccTaxNo = FValue.trim();
            }
            else
                BankAccTaxNo = null;
        }
        if (FCode.equalsIgnoreCase("BankTaxName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankTaxName = FValue.trim();
            }
            else
                BankTaxName = null;
        }
        if (FCode.equalsIgnoreCase("BankTaxCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankTaxCode = FValue.trim();
            }
            else
                BankTaxCode = null;
        }
        if (FCode.equalsIgnoreCase("GrpAppIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAppIDType = FValue.trim();
            }
            else
                GrpAppIDType = null;
        }
        if (FCode.equalsIgnoreCase("GrpAppIDExpDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAppIDExpDate = FValue.trim();
            }
            else
                GrpAppIDExpDate = null;
        }
        if (FCode.equalsIgnoreCase("CtrPeople")) {
            if( FValue != null && !FValue.equals(""))
            {
                CtrPeople = FValue.trim();
            }
            else
                CtrPeople = null;
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples3 = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaMatePeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaMatePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaYoungPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaYoungPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaOtherPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RelaOtherPeoples = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LBGrpAppntPojo [" +
            "GrpContNo="+GrpContNo +
            ", CustomerNo="+CustomerNo +
            ", PrtNo="+PrtNo +
            ", AddressNo="+AddressNo +
            ", AppntGrade="+AppntGrade +
            ", Name="+Name +
            ", PostalAddress="+PostalAddress +
            ", ZipCode="+ZipCode +
            ", Phone="+Phone +
            ", Password="+Password +
            ", State="+State +
            ", AppntType="+AppntType +
            ", RelationToInsured="+RelationToInsured +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", BusiCategory="+BusiCategory +
            ", GrpNature="+GrpNature +
            ", SumNumPeople="+SumNumPeople +
            ", MainBusiness="+MainBusiness +
            ", Corporation="+Corporation +
            ", CorIDType="+CorIDType +
            ", CorID="+CorID +
            ", CorIDExpiryDate="+CorIDExpiryDate +
            ", OnJobNumber="+OnJobNumber +
            ", RetireNumber="+RetireNumber +
            ", OtherNumber="+OtherNumber +
            ", RgtCapital="+RgtCapital +
            ", TotalAssets="+TotalAssets +
            ", NetProfitRate="+NetProfitRate +
            ", Satrap="+Satrap +
            ", ActuCtrl="+ActuCtrl +
            ", License="+License +
            ", SocialInsuCode="+SocialInsuCode +
            ", OrganizationCode="+OrganizationCode +
            ", TaxCode="+TaxCode +
            ", Fax="+Fax +
            ", EMail="+EMail +
            ", FoundDate="+FoundDate +
            ", Remark="+Remark +
            ", Segment1="+Segment1 +
            ", Segment2="+Segment2 +
            ", Segment3="+Segment3 +
            ", ManageCom="+ManageCom +
            ", ComCode="+ComCode +
            ", ModifyOperator="+ModifyOperator +
            ", Sex="+Sex +
            ", NativePlace="+NativePlace +
            ", Occupation="+Occupation +
            ", OccupationCode="+OccupationCode +
            ", TaxPayerType="+TaxPayerType +
            ", FixedPlan="+FixedPlan +
            ", AddTaxFlag="+AddTaxFlag +
            ", AddTaxDate="+AddTaxDate +
            ", BankAccTaxNo="+BankAccTaxNo +
            ", BankTaxName="+BankTaxName +
            ", BankTaxCode="+BankTaxCode +
            ", GrpAppIDType="+GrpAppIDType +
            ", GrpAppIDExpDate="+GrpAppIDExpDate +
            ", CtrPeople="+CtrPeople +
            ", Peoples3="+Peoples3 +
            ", RelaPeoples="+RelaPeoples +
            ", RelaMatePeoples="+RelaMatePeoples +
            ", RelaYoungPeoples="+RelaYoungPeoples +
            ", RelaOtherPeoples="+RelaOtherPeoples +"]";
    }
}
