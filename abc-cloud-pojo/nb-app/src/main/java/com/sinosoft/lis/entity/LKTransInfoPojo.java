/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LKTransInfoPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LKTransInfoPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long TransInfoID; 
    /** Shardingid */
    private String ShardingID; 
    /** Sstranscode */
    @RedisPrimaryHKey
    private String SSTransCode; 
    /** Suspendtranscode */
    private String SuSpendTransCode; 
    /** Tbtranscode */
    private String TBTransCode; 
    /** Newpolicyno */
    private String NewPolicyNo; 
    /** Oldpolicyno */
    private String OldPolicyNo; 
    /** Edoracceptno */
    private String EdorAcceptNo; 
    /** Occurbala */
    private double OccurBala; 
    /** Prem */
    private double Prem; 
    /** Transferprem */
    private double TransferPrem; 
    /** Funcflag */
    private String FuncFlag; 
    /** Transdate */
    private String  TransDate;
    /** Transtime */
    private String TransTime; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后修改日期 */
    private String  ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime; 
    /** Bak1 */
    private String Bak1; 
    /** Bak2 */
    private String Bak2; 
    /** Bak3 */
    private String Bak3; 
    /** Bak4 */
    private String Bak4; 


    public static final int FIELDNUM = 22;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getTransInfoID() {
        return TransInfoID;
    }
    public void setTransInfoID(long aTransInfoID) {
        TransInfoID = aTransInfoID;
    }
    public void setTransInfoID(String aTransInfoID) {
        if (aTransInfoID != null && !aTransInfoID.equals("")) {
            TransInfoID = new Long(aTransInfoID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getSSTransCode() {
        return SSTransCode;
    }
    public void setSSTransCode(String aSSTransCode) {
        SSTransCode = aSSTransCode;
    }
    public String getSuSpendTransCode() {
        return SuSpendTransCode;
    }
    public void setSuSpendTransCode(String aSuSpendTransCode) {
        SuSpendTransCode = aSuSpendTransCode;
    }
    public String getTBTransCode() {
        return TBTransCode;
    }
    public void setTBTransCode(String aTBTransCode) {
        TBTransCode = aTBTransCode;
    }
    public String getNewPolicyNo() {
        return NewPolicyNo;
    }
    public void setNewPolicyNo(String aNewPolicyNo) {
        NewPolicyNo = aNewPolicyNo;
    }
    public String getOldPolicyNo() {
        return OldPolicyNo;
    }
    public void setOldPolicyNo(String aOldPolicyNo) {
        OldPolicyNo = aOldPolicyNo;
    }
    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }
    public void setEdorAcceptNo(String aEdorAcceptNo) {
        EdorAcceptNo = aEdorAcceptNo;
    }
    public double getOccurBala() {
        return OccurBala;
    }
    public void setOccurBala(double aOccurBala) {
        OccurBala = aOccurBala;
    }
    public void setOccurBala(String aOccurBala) {
        if (aOccurBala != null && !aOccurBala.equals("")) {
            Double tDouble = new Double(aOccurBala);
            double d = tDouble.doubleValue();
            OccurBala = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getTransferPrem() {
        return TransferPrem;
    }
    public void setTransferPrem(double aTransferPrem) {
        TransferPrem = aTransferPrem;
    }
    public void setTransferPrem(String aTransferPrem) {
        if (aTransferPrem != null && !aTransferPrem.equals("")) {
            Double tDouble = new Double(aTransferPrem);
            double d = tDouble.doubleValue();
            TransferPrem = d;
        }
    }

    public String getFuncFlag() {
        return FuncFlag;
    }
    public void setFuncFlag(String aFuncFlag) {
        FuncFlag = aFuncFlag;
    }
    public String getTransDate() {
        return TransDate;
    }
    public void setTransDate(String aTransDate) {
        TransDate = aTransDate;
    }
    public String getTransTime() {
        return TransTime;
    }
    public void setTransTime(String aTransTime) {
        TransTime = aTransTime;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBak1() {
        return Bak1;
    }
    public void setBak1(String aBak1) {
        Bak1 = aBak1;
    }
    public String getBak2() {
        return Bak2;
    }
    public void setBak2(String aBak2) {
        Bak2 = aBak2;
    }
    public String getBak3() {
        return Bak3;
    }
    public void setBak3(String aBak3) {
        Bak3 = aBak3;
    }
    public String getBak4() {
        return Bak4;
    }
    public void setBak4(String aBak4) {
        Bak4 = aBak4;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TransInfoID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("SSTransCode") ) {
            return 2;
        }
        if( strFieldName.equals("SuSpendTransCode") ) {
            return 3;
        }
        if( strFieldName.equals("TBTransCode") ) {
            return 4;
        }
        if( strFieldName.equals("NewPolicyNo") ) {
            return 5;
        }
        if( strFieldName.equals("OldPolicyNo") ) {
            return 6;
        }
        if( strFieldName.equals("EdorAcceptNo") ) {
            return 7;
        }
        if( strFieldName.equals("OccurBala") ) {
            return 8;
        }
        if( strFieldName.equals("Prem") ) {
            return 9;
        }
        if( strFieldName.equals("TransferPrem") ) {
            return 10;
        }
        if( strFieldName.equals("FuncFlag") ) {
            return 11;
        }
        if( strFieldName.equals("TransDate") ) {
            return 12;
        }
        if( strFieldName.equals("TransTime") ) {
            return 13;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 14;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 17;
        }
        if( strFieldName.equals("Bak1") ) {
            return 18;
        }
        if( strFieldName.equals("Bak2") ) {
            return 19;
        }
        if( strFieldName.equals("Bak3") ) {
            return 20;
        }
        if( strFieldName.equals("Bak4") ) {
            return 21;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TransInfoID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "SSTransCode";
                break;
            case 3:
                strFieldName = "SuSpendTransCode";
                break;
            case 4:
                strFieldName = "TBTransCode";
                break;
            case 5:
                strFieldName = "NewPolicyNo";
                break;
            case 6:
                strFieldName = "OldPolicyNo";
                break;
            case 7:
                strFieldName = "EdorAcceptNo";
                break;
            case 8:
                strFieldName = "OccurBala";
                break;
            case 9:
                strFieldName = "Prem";
                break;
            case 10:
                strFieldName = "TransferPrem";
                break;
            case 11:
                strFieldName = "FuncFlag";
                break;
            case 12:
                strFieldName = "TransDate";
                break;
            case 13:
                strFieldName = "TransTime";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            case 18:
                strFieldName = "Bak1";
                break;
            case 19:
                strFieldName = "Bak2";
                break;
            case 20:
                strFieldName = "Bak3";
                break;
            case 21:
                strFieldName = "Bak4";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSINFOID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "SSTRANSCODE":
                return Schema.TYPE_STRING;
            case "SUSPENDTRANSCODE":
                return Schema.TYPE_STRING;
            case "TBTRANSCODE":
                return Schema.TYPE_STRING;
            case "NEWPOLICYNO":
                return Schema.TYPE_STRING;
            case "OLDPOLICYNO":
                return Schema.TYPE_STRING;
            case "EDORACCEPTNO":
                return Schema.TYPE_STRING;
            case "OCCURBALA":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "TRANSFERPREM":
                return Schema.TYPE_DOUBLE;
            case "FUNCFLAG":
                return Schema.TYPE_STRING;
            case "TRANSDATE":
                return Schema.TYPE_STRING;
            case "TRANSTIME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BAK1":
                return Schema.TYPE_STRING;
            case "BAK2":
                return Schema.TYPE_STRING;
            case "BAK3":
                return Schema.TYPE_STRING;
            case "BAK4":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TransInfoID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransInfoID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("SSTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SSTransCode));
        }
        if (FCode.equalsIgnoreCase("SuSpendTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SuSpendTransCode));
        }
        if (FCode.equalsIgnoreCase("TBTransCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TBTransCode));
        }
        if (FCode.equalsIgnoreCase("NewPolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewPolicyNo));
        }
        if (FCode.equalsIgnoreCase("OldPolicyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldPolicyNo));
        }
        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equalsIgnoreCase("OccurBala")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccurBala));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("TransferPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransferPrem));
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FuncFlag));
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransDate));
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransTime));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
        }
        if (FCode.equalsIgnoreCase("Bak4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bak4));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TransInfoID);
                break;
            case 1:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 2:
                strFieldValue = String.valueOf(SSTransCode);
                break;
            case 3:
                strFieldValue = String.valueOf(SuSpendTransCode);
                break;
            case 4:
                strFieldValue = String.valueOf(TBTransCode);
                break;
            case 5:
                strFieldValue = String.valueOf(NewPolicyNo);
                break;
            case 6:
                strFieldValue = String.valueOf(OldPolicyNo);
                break;
            case 7:
                strFieldValue = String.valueOf(EdorAcceptNo);
                break;
            case 8:
                strFieldValue = String.valueOf(OccurBala);
                break;
            case 9:
                strFieldValue = String.valueOf(Prem);
                break;
            case 10:
                strFieldValue = String.valueOf(TransferPrem);
                break;
            case 11:
                strFieldValue = String.valueOf(FuncFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(TransDate);
                break;
            case 13:
                strFieldValue = String.valueOf(TransTime);
                break;
            case 14:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 15:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 16:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 17:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 18:
                strFieldValue = String.valueOf(Bak1);
                break;
            case 19:
                strFieldValue = String.valueOf(Bak2);
                break;
            case 20:
                strFieldValue = String.valueOf(Bak3);
                break;
            case 21:
                strFieldValue = String.valueOf(Bak4);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TransInfoID")) {
            if( FValue != null && !FValue.equals("")) {
                TransInfoID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("SSTransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SSTransCode = FValue.trim();
            }
            else
                SSTransCode = null;
        }
        if (FCode.equalsIgnoreCase("SuSpendTransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SuSpendTransCode = FValue.trim();
            }
            else
                SuSpendTransCode = null;
        }
        if (FCode.equalsIgnoreCase("TBTransCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TBTransCode = FValue.trim();
            }
            else
                TBTransCode = null;
        }
        if (FCode.equalsIgnoreCase("NewPolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewPolicyNo = FValue.trim();
            }
            else
                NewPolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("OldPolicyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldPolicyNo = FValue.trim();
            }
            else
                OldPolicyNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
                EdorAcceptNo = null;
        }
        if (FCode.equalsIgnoreCase("OccurBala")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                OccurBala = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("TransferPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                TransferPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FuncFlag = FValue.trim();
            }
            else
                FuncFlag = null;
        }
        if (FCode.equalsIgnoreCase("TransDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransDate = FValue.trim();
            }
            else
                TransDate = null;
        }
        if (FCode.equalsIgnoreCase("TransTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransTime = FValue.trim();
            }
            else
                TransTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Bak1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak1 = FValue.trim();
            }
            else
                Bak1 = null;
        }
        if (FCode.equalsIgnoreCase("Bak2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak2 = FValue.trim();
            }
            else
                Bak2 = null;
        }
        if (FCode.equalsIgnoreCase("Bak3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak3 = FValue.trim();
            }
            else
                Bak3 = null;
        }
        if (FCode.equalsIgnoreCase("Bak4")) {
            if( FValue != null && !FValue.equals(""))
            {
                Bak4 = FValue.trim();
            }
            else
                Bak4 = null;
        }
        return true;
    }


    public String toString() {
    return "LKTransInfoPojo [" +
            "TransInfoID="+TransInfoID +
            ", ShardingID="+ShardingID +
            ", SSTransCode="+SSTransCode +
            ", SuSpendTransCode="+SuSpendTransCode +
            ", TBTransCode="+TBTransCode +
            ", NewPolicyNo="+NewPolicyNo +
            ", OldPolicyNo="+OldPolicyNo +
            ", EdorAcceptNo="+EdorAcceptNo +
            ", OccurBala="+OccurBala +
            ", Prem="+Prem +
            ", TransferPrem="+TransferPrem +
            ", FuncFlag="+FuncFlag +
            ", TransDate="+TransDate +
            ", TransTime="+TransTime +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Bak1="+Bak1 +
            ", Bak2="+Bak2 +
            ", Bak3="+Bak3 +
            ", Bak4="+Bak4 +"]";
    }
}
