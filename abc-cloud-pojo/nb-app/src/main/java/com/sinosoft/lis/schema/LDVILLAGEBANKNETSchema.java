/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDVILLAGEBANKNETDB;

/**
 * <p>ClassName: LDVILLAGEBANKNETSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-01-09
 */
public class LDVILLAGEBANKNETSchema implements Schema, Cloneable {
    // @Field
    /** Bank_brchnet_code */
    private String BANK_BRCHNET_CODE;
    /** Bank_brchnet_name */
    private String BANK_BRCHNET_NAME;
    /** Bank_inside_code */
    private String BANK_INSIDE_CODE;
    /** Agentcode */
    private String AGENTCODE;
    /** Agency_brchnet_code */
    private String AGENCY_BRCHNET_CODE;
    /** Agencycode */
    private String AGENCYCODE;
    /** Makedate */
    private Date MAKEDATE;
    /** Maketime */
    private String MAKETIME;
    /** Modifydate */
    private Date MODIFYDATE;
    /** Modifytime */
    private String MODIFYTIME;

    public static final int FIELDNUM = 10;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDVILLAGEBANKNETSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "BANK_BRCHNET_CODE";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDVILLAGEBANKNETSchema cloned = (LDVILLAGEBANKNETSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getBANK_BRCHNET_CODE() {
        return BANK_BRCHNET_CODE;
    }
    public void setBANK_BRCHNET_CODE(String aBANK_BRCHNET_CODE) {
        BANK_BRCHNET_CODE = aBANK_BRCHNET_CODE;
    }
    public String getBANK_BRCHNET_NAME() {
        return BANK_BRCHNET_NAME;
    }
    public void setBANK_BRCHNET_NAME(String aBANK_BRCHNET_NAME) {
        BANK_BRCHNET_NAME = aBANK_BRCHNET_NAME;
    }
    public String getBANK_INSIDE_CODE() {
        return BANK_INSIDE_CODE;
    }
    public void setBANK_INSIDE_CODE(String aBANK_INSIDE_CODE) {
        BANK_INSIDE_CODE = aBANK_INSIDE_CODE;
    }
    public String getAGENTCODE() {
        return AGENTCODE;
    }
    public void setAGENTCODE(String aAGENTCODE) {
        AGENTCODE = aAGENTCODE;
    }
    public String getAGENCY_BRCHNET_CODE() {
        return AGENCY_BRCHNET_CODE;
    }
    public void setAGENCY_BRCHNET_CODE(String aAGENCY_BRCHNET_CODE) {
        AGENCY_BRCHNET_CODE = aAGENCY_BRCHNET_CODE;
    }
    public String getAGENCYCODE() {
        return AGENCYCODE;
    }
    public void setAGENCYCODE(String aAGENCYCODE) {
        AGENCYCODE = aAGENCYCODE;
    }
    public String getMAKEDATE() {
        if(MAKEDATE != null) {
            return fDate.getString(MAKEDATE);
        } else {
            return null;
        }
    }
    public void setMAKEDATE(Date aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        if (aMAKEDATE != null && !aMAKEDATE.equals("")) {
            MAKEDATE = fDate.getDate(aMAKEDATE);
        } else
            MAKEDATE = null;
    }

    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }
    public String getMODIFYDATE() {
        if(MODIFYDATE != null) {
            return fDate.getString(MODIFYDATE);
        } else {
            return null;
        }
    }
    public void setMODIFYDATE(Date aMODIFYDATE) {
        MODIFYDATE = aMODIFYDATE;
    }
    public void setMODIFYDATE(String aMODIFYDATE) {
        if (aMODIFYDATE != null && !aMODIFYDATE.equals("")) {
            MODIFYDATE = fDate.getDate(aMODIFYDATE);
        } else
            MODIFYDATE = null;
    }

    public String getMODIFYTIME() {
        return MODIFYTIME;
    }
    public void setMODIFYTIME(String aMODIFYTIME) {
        MODIFYTIME = aMODIFYTIME;
    }

    /**
    * 使用另外一个 LDVILLAGEBANKNETSchema 对象给 Schema 赋值
    * @param: aLDVILLAGEBANKNETSchema LDVILLAGEBANKNETSchema
    **/
    public void setSchema(LDVILLAGEBANKNETSchema aLDVILLAGEBANKNETSchema) {
        this.BANK_BRCHNET_CODE = aLDVILLAGEBANKNETSchema.getBANK_BRCHNET_CODE();
        this.BANK_BRCHNET_NAME = aLDVILLAGEBANKNETSchema.getBANK_BRCHNET_NAME();
        this.BANK_INSIDE_CODE = aLDVILLAGEBANKNETSchema.getBANK_INSIDE_CODE();
        this.AGENTCODE = aLDVILLAGEBANKNETSchema.getAGENTCODE();
        this.AGENCY_BRCHNET_CODE = aLDVILLAGEBANKNETSchema.getAGENCY_BRCHNET_CODE();
        this.AGENCYCODE = aLDVILLAGEBANKNETSchema.getAGENCYCODE();
        this.MAKEDATE = fDate.getDate( aLDVILLAGEBANKNETSchema.getMAKEDATE());
        this.MAKETIME = aLDVILLAGEBANKNETSchema.getMAKETIME();
        this.MODIFYDATE = fDate.getDate( aLDVILLAGEBANKNETSchema.getMODIFYDATE());
        this.MODIFYTIME = aLDVILLAGEBANKNETSchema.getMODIFYTIME();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("BANK_BRCHNET_CODE") == null )
                this.BANK_BRCHNET_CODE = null;
            else
                this.BANK_BRCHNET_CODE = rs.getString("BANK_BRCHNET_CODE").trim();

            if( rs.getString("BANK_BRCHNET_NAME") == null )
                this.BANK_BRCHNET_NAME = null;
            else
                this.BANK_BRCHNET_NAME = rs.getString("BANK_BRCHNET_NAME").trim();

            if( rs.getString("BANK_INSIDE_CODE") == null )
                this.BANK_INSIDE_CODE = null;
            else
                this.BANK_INSIDE_CODE = rs.getString("BANK_INSIDE_CODE").trim();

            if( rs.getString("AGENTCODE") == null )
                this.AGENTCODE = null;
            else
                this.AGENTCODE = rs.getString("AGENTCODE").trim();

            if( rs.getString("AGENCY_BRCHNET_CODE") == null )
                this.AGENCY_BRCHNET_CODE = null;
            else
                this.AGENCY_BRCHNET_CODE = rs.getString("AGENCY_BRCHNET_CODE").trim();

            if( rs.getString("AGENCYCODE") == null )
                this.AGENCYCODE = null;
            else
                this.AGENCYCODE = rs.getString("AGENCYCODE").trim();

            this.MAKEDATE = rs.getDate("MAKEDATE");
            if( rs.getString("MAKETIME") == null )
                this.MAKETIME = null;
            else
                this.MAKETIME = rs.getString("MAKETIME").trim();

            this.MODIFYDATE = rs.getDate("MODIFYDATE");
            if( rs.getString("MODIFYTIME") == null )
                this.MODIFYTIME = null;
            else
                this.MODIFYTIME = rs.getString("MODIFYTIME").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDVILLAGEBANKNETSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDVILLAGEBANKNETSchema getSchema() {
        LDVILLAGEBANKNETSchema aLDVILLAGEBANKNETSchema = new LDVILLAGEBANKNETSchema();
        aLDVILLAGEBANKNETSchema.setSchema(this);
        return aLDVILLAGEBANKNETSchema;
    }

    public LDVILLAGEBANKNETDB getDB() {
        LDVILLAGEBANKNETDB aDBOper = new LDVILLAGEBANKNETDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDVILLAGEBANKNET描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(BANK_BRCHNET_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BANK_BRCHNET_NAME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BANK_INSIDE_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENTCODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_BRCHNET_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCYCODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MAKEDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MAKETIME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MODIFYDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MODIFYTIME));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDVILLAGEBANKNET>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            BANK_BRCHNET_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            BANK_BRCHNET_NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            BANK_INSIDE_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AGENTCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AGENCY_BRCHNET_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AGENCYCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            MAKEDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
            MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            MODIFYDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
            MODIFYTIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDVILLAGEBANKNETSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_BRCHNET_CODE));
        }
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_BRCHNET_NAME));
        }
        if (FCode.equalsIgnoreCase("BANK_INSIDE_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_INSIDE_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENTCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENTCODE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_BRCHNET_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_BRCHNET_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENCYCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCYCODE));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(BANK_BRCHNET_CODE);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BANK_BRCHNET_NAME);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BANK_INSIDE_CODE);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AGENTCODE);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_BRCHNET_CODE);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AGENCYCODE);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MAKETIME);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MODIFYTIME);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BANK_BRCHNET_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_BRCHNET_CODE = FValue.trim();
            }
            else
                BANK_BRCHNET_CODE = null;
        }
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_BRCHNET_NAME = FValue.trim();
            }
            else
                BANK_BRCHNET_NAME = null;
        }
        if (FCode.equalsIgnoreCase("BANK_INSIDE_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_INSIDE_CODE = FValue.trim();
            }
            else
                BANK_INSIDE_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENTCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENTCODE = FValue.trim();
            }
            else
                AGENTCODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_BRCHNET_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_BRCHNET_CODE = FValue.trim();
            }
            else
                AGENCY_BRCHNET_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCYCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCYCODE = FValue.trim();
            }
            else
                AGENCYCODE = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if(FValue != null && !FValue.equals("")) {
                MAKEDATE = fDate.getDate( FValue );
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            if(FValue != null && !FValue.equals("")) {
                MODIFYDATE = fDate.getDate( FValue );
            }
            else
                MODIFYDATE = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MODIFYTIME = FValue.trim();
            }
            else
                MODIFYTIME = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDVILLAGEBANKNETSchema other = (LDVILLAGEBANKNETSchema)otherObject;
        return
            BANK_BRCHNET_CODE.equals(other.getBANK_BRCHNET_CODE())
            && BANK_BRCHNET_NAME.equals(other.getBANK_BRCHNET_NAME())
            && BANK_INSIDE_CODE.equals(other.getBANK_INSIDE_CODE())
            && AGENTCODE.equals(other.getAGENTCODE())
            && AGENCY_BRCHNET_CODE.equals(other.getAGENCY_BRCHNET_CODE())
            && AGENCYCODE.equals(other.getAGENCYCODE())
            && fDate.getString(MAKEDATE).equals(other.getMAKEDATE())
            && MAKETIME.equals(other.getMAKETIME())
            && fDate.getString(MODIFYDATE).equals(other.getMODIFYDATE())
            && MODIFYTIME.equals(other.getMODIFYTIME());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BANK_BRCHNET_CODE") ) {
            return 0;
        }
        if( strFieldName.equals("BANK_BRCHNET_NAME") ) {
            return 1;
        }
        if( strFieldName.equals("BANK_INSIDE_CODE") ) {
            return 2;
        }
        if( strFieldName.equals("AGENTCODE") ) {
            return 3;
        }
        if( strFieldName.equals("AGENCY_BRCHNET_CODE") ) {
            return 4;
        }
        if( strFieldName.equals("AGENCYCODE") ) {
            return 5;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 6;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 7;
        }
        if( strFieldName.equals("MODIFYDATE") ) {
            return 8;
        }
        if( strFieldName.equals("MODIFYTIME") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BANK_BRCHNET_CODE";
                break;
            case 1:
                strFieldName = "BANK_BRCHNET_NAME";
                break;
            case 2:
                strFieldName = "BANK_INSIDE_CODE";
                break;
            case 3:
                strFieldName = "AGENTCODE";
                break;
            case 4:
                strFieldName = "AGENCY_BRCHNET_CODE";
                break;
            case 5:
                strFieldName = "AGENCYCODE";
                break;
            case 6:
                strFieldName = "MAKEDATE";
                break;
            case 7:
                strFieldName = "MAKETIME";
                break;
            case 8:
                strFieldName = "MODIFYDATE";
                break;
            case 9:
                strFieldName = "MODIFYTIME";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BANK_BRCHNET_CODE":
                return Schema.TYPE_STRING;
            case "BANK_BRCHNET_NAME":
                return Schema.TYPE_STRING;
            case "BANK_INSIDE_CODE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENCY_BRCHNET_CODE":
                return Schema.TYPE_STRING;
            case "AGENCYCODE":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
