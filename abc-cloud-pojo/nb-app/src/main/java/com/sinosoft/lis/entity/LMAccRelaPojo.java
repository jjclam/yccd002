/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMAccRelaPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMAccRelaPojo implements Pojo,Serializable {
    // @Field
    /** 保险帐户号码 */
    private String InsuAccNo; 
    /** 下级保险帐户号码 */
    private String InsuAccNoLower; 
    /** 关联是否有效 */
    private String ValiState; 


    public static final int FIELDNUM = 3;    // 数据库表的字段个数
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getInsuAccNoLower() {
        return InsuAccNoLower;
    }
    public void setInsuAccNoLower(String aInsuAccNoLower) {
        InsuAccNoLower = aInsuAccNoLower;
    }
    public String getValiState() {
        return ValiState;
    }
    public void setValiState(String aValiState) {
        ValiState = aValiState;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsuAccNo") ) {
            return 0;
        }
        if( strFieldName.equals("InsuAccNoLower") ) {
            return 1;
        }
        if( strFieldName.equals("ValiState") ) {
            return 2;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsuAccNo";
                break;
            case 1:
                strFieldName = "InsuAccNoLower";
                break;
            case 2:
                strFieldName = "ValiState";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "INSUACCNOLOWER":
                return Schema.TYPE_STRING;
            case "VALISTATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccNoLower")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNoLower));
        }
        if (FCode.equalsIgnoreCase("ValiState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValiState));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(InsuAccNo);
                break;
            case 1:
                strFieldValue = String.valueOf(InsuAccNoLower);
                break;
            case 2:
                strFieldValue = String.valueOf(ValiState);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNoLower")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNoLower = FValue.trim();
            }
            else
                InsuAccNoLower = null;
        }
        if (FCode.equalsIgnoreCase("ValiState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValiState = FValue.trim();
            }
            else
                ValiState = null;
        }
        return true;
    }


    public String toString() {
    return "LMAccRelaPojo [" +
            "InsuAccNo="+InsuAccNo +
            ", InsuAccNoLower="+InsuAccNoLower +
            ", ValiState="+ValiState +"]";
    }
}
