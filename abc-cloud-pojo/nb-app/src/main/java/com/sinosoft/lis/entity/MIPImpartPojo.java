package com.sinosoft.lis.entity;


import java.io.Serializable;

/**
 * 存放移动展业报文中的告知数据
 */
public class MIPImpartPojo implements Serializable {

    //判断是否是投被保人 dx0002 投保人  dx0003被保人
    private String impartObject;
    //告知类型
    private String impartVer;
    //告知编码
    private String impartCode;
    //告知内容
    private String impartContent;
    //批次号
    private String patchNo;

    public String getImpartCode() {
        return impartCode;
    }

    public void setImpartCode(String impartCode) {
        this.impartCode = impartCode;
    }

    public String getImpartContent() {
        return impartContent;
    }

    public void setImpartContent(String impartContent) {
        this.impartContent = impartContent;
    }

    public String getImpartObject() {
        return impartObject;
    }

    public void setImpartObject(String impartObject) {
        this.impartObject = impartObject;
    }

    public String getImpartVer() {
        return impartVer;
    }

    public void setImpartVer(String impartVer) {
        this.impartVer = impartVer;
    }

    public String getPatchNo() {
        return patchNo;
    }

    public void setPatchNo(String patchNo) {
        this.patchNo = patchNo;
    }

    @Override
    public String toString() {
        return "MIPImpartPojo{" +
                "impartCode='" + impartCode + '\'' +
                ", impartObject='" + impartObject + '\'' +
                ", impartVer='" + impartVer + '\'' +
                ", impartContent='" + impartContent + '\'' +
                ", patchNo='" + patchNo + '\'' +
                '}';
    }
}