/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LXBlackListIndvDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LXBlackListIndvSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LXBlackListIndvSchema implements Schema, Cloneable {
    // @Field
    /** 黑名单id */
    private String BlacklistID;
    /** 姓名 */
    private String Name;
    /** 曾用名 */
    private String Usedname;
    /** 性别 */
    private String Gender;
    /** 籍贯(出生地) */
    private String Birthplace;
    /** 国籍 */
    private String Nationality;
    /** 证件类型 */
    private String IDType;
    /** 证件号码 */
    private String IDNo;
    /** 民族 */
    private String Nation;
    /** 职业 */
    private String Occupation;
    /** 职位 */
    private String Title;
    /** 地址 */
    private String Address;
    /** 其他信息 */
    private String OtherInfo;
    /** 参考号码 */
    private String ReferNo;
    /** 备注 */
    private String Remark;
    /** 备用字段1 */
    private String StandbyString1;
    /** 备用字段2 */
    private String StandbyString2;
    /** 备用字段3 */
    private String StandbyString3;
    /** 备用字段4 */
    private String StandbyString4;
    /** 备用字段5 */
    private String StandbyString5;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 入机操作员 */
    private String MakeOperator;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;
    /** 最后修改人 */
    private String ModifyOperator;
    /** Middlename */
    private String MiddleName;
    /** Surname */
    private String SurName;
    /** 关联人id */
    private String RelevanceId;
    /** 关联关系 */
    private String RelevanceType;
    /** 名单性质 */
    private String ListProperty;
    /** 描述1 */
    private String Description1Name;
    /** 描述2 */
    private String Description2Name;
    /** 描述3 */
    private String Description3Name;
    /** 描述4 */
    private String ProFileNotes;
    /** 描述5 */
    private String ReferenceName;
    /** 个人/实体 */
    private String RecordType;
    /** 出生日期 */
    private String DateOfBirth;
    /** 生日下限 */
    private String MinBirthday;
    /** 生日上限 */
    private String MaxBirthday;
    /** 列入名单日期 */
    private String ListedDate;
    /** 本国语言类别 */
    private String ScriptLanguageId;
    /** Firstname */
    private String FirstName;

    public static final int FIELDNUM = 43;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LXBlackListIndvSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "BlacklistID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LXBlackListIndvSchema cloned = (LXBlackListIndvSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getBlacklistID() {
        return BlacklistID;
    }
    public void setBlacklistID(String aBlacklistID) {
        BlacklistID = aBlacklistID;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getUsedname() {
        return Usedname;
    }
    public void setUsedname(String aUsedname) {
        Usedname = aUsedname;
    }
    public String getGender() {
        return Gender;
    }
    public void setGender(String aGender) {
        Gender = aGender;
    }
    public String getBirthplace() {
        return Birthplace;
    }
    public void setBirthplace(String aBirthplace) {
        Birthplace = aBirthplace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getNation() {
        return Nation;
    }
    public void setNation(String aNation) {
        Nation = aNation;
    }
    public String getOccupation() {
        return Occupation;
    }
    public void setOccupation(String aOccupation) {
        Occupation = aOccupation;
    }
    public String getTitle() {
        return Title;
    }
    public void setTitle(String aTitle) {
        Title = aTitle;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String aAddress) {
        Address = aAddress;
    }
    public String getOtherInfo() {
        return OtherInfo;
    }
    public void setOtherInfo(String aOtherInfo) {
        OtherInfo = aOtherInfo;
    }
    public String getReferNo() {
        return ReferNo;
    }
    public void setReferNo(String aReferNo) {
        ReferNo = aReferNo;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getStandbyString1() {
        return StandbyString1;
    }
    public void setStandbyString1(String aStandbyString1) {
        StandbyString1 = aStandbyString1;
    }
    public String getStandbyString2() {
        return StandbyString2;
    }
    public void setStandbyString2(String aStandbyString2) {
        StandbyString2 = aStandbyString2;
    }
    public String getStandbyString3() {
        return StandbyString3;
    }
    public void setStandbyString3(String aStandbyString3) {
        StandbyString3 = aStandbyString3;
    }
    public String getStandbyString4() {
        return StandbyString4;
    }
    public void setStandbyString4(String aStandbyString4) {
        StandbyString4 = aStandbyString4;
    }
    public String getStandbyString5() {
        return StandbyString5;
    }
    public void setStandbyString5(String aStandbyString5) {
        StandbyString5 = aStandbyString5;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getMakeOperator() {
        return MakeOperator;
    }
    public void setMakeOperator(String aMakeOperator) {
        MakeOperator = aMakeOperator;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getModifyOperator() {
        return ModifyOperator;
    }
    public void setModifyOperator(String aModifyOperator) {
        ModifyOperator = aModifyOperator;
    }
    public String getMiddleName() {
        return MiddleName;
    }
    public void setMiddleName(String aMiddleName) {
        MiddleName = aMiddleName;
    }
    public String getSurName() {
        return SurName;
    }
    public void setSurName(String aSurName) {
        SurName = aSurName;
    }
    public String getRelevanceId() {
        return RelevanceId;
    }
    public void setRelevanceId(String aRelevanceId) {
        RelevanceId = aRelevanceId;
    }
    public String getRelevanceType() {
        return RelevanceType;
    }
    public void setRelevanceType(String aRelevanceType) {
        RelevanceType = aRelevanceType;
    }
    public String getListProperty() {
        return ListProperty;
    }
    public void setListProperty(String aListProperty) {
        ListProperty = aListProperty;
    }
    public String getDescription1Name() {
        return Description1Name;
    }
    public void setDescription1Name(String aDescription1Name) {
        Description1Name = aDescription1Name;
    }
    public String getDescription2Name() {
        return Description2Name;
    }
    public void setDescription2Name(String aDescription2Name) {
        Description2Name = aDescription2Name;
    }
    public String getDescription3Name() {
        return Description3Name;
    }
    public void setDescription3Name(String aDescription3Name) {
        Description3Name = aDescription3Name;
    }
    public String getProFileNotes() {
        return ProFileNotes;
    }
    public void setProFileNotes(String aProFileNotes) {
        ProFileNotes = aProFileNotes;
    }
    public String getReferenceName() {
        return ReferenceName;
    }
    public void setReferenceName(String aReferenceName) {
        ReferenceName = aReferenceName;
    }
    public String getRecordType() {
        return RecordType;
    }
    public void setRecordType(String aRecordType) {
        RecordType = aRecordType;
    }
    public String getDateOfBirth() {
        return DateOfBirth;
    }
    public void setDateOfBirth(String aDateOfBirth) {
        DateOfBirth = aDateOfBirth;
    }
    public String getMinBirthday() {
        return MinBirthday;
    }
    public void setMinBirthday(String aMinBirthday) {
        MinBirthday = aMinBirthday;
    }
    public String getMaxBirthday() {
        return MaxBirthday;
    }
    public void setMaxBirthday(String aMaxBirthday) {
        MaxBirthday = aMaxBirthday;
    }
    public String getListedDate() {
        return ListedDate;
    }
    public void setListedDate(String aListedDate) {
        ListedDate = aListedDate;
    }
    public String getScriptLanguageId() {
        return ScriptLanguageId;
    }
    public void setScriptLanguageId(String aScriptLanguageId) {
        ScriptLanguageId = aScriptLanguageId;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String aFirstName) {
        FirstName = aFirstName;
    }

    /**
    * 使用另外一个 LXBlackListIndvSchema 对象给 Schema 赋值
    * @param: aLXBlackListIndvSchema LXBlackListIndvSchema
    **/
    public void setSchema(LXBlackListIndvSchema aLXBlackListIndvSchema) {
        this.BlacklistID = aLXBlackListIndvSchema.getBlacklistID();
        this.Name = aLXBlackListIndvSchema.getName();
        this.Usedname = aLXBlackListIndvSchema.getUsedname();
        this.Gender = aLXBlackListIndvSchema.getGender();
        this.Birthplace = aLXBlackListIndvSchema.getBirthplace();
        this.Nationality = aLXBlackListIndvSchema.getNationality();
        this.IDType = aLXBlackListIndvSchema.getIDType();
        this.IDNo = aLXBlackListIndvSchema.getIDNo();
        this.Nation = aLXBlackListIndvSchema.getNation();
        this.Occupation = aLXBlackListIndvSchema.getOccupation();
        this.Title = aLXBlackListIndvSchema.getTitle();
        this.Address = aLXBlackListIndvSchema.getAddress();
        this.OtherInfo = aLXBlackListIndvSchema.getOtherInfo();
        this.ReferNo = aLXBlackListIndvSchema.getReferNo();
        this.Remark = aLXBlackListIndvSchema.getRemark();
        this.StandbyString1 = aLXBlackListIndvSchema.getStandbyString1();
        this.StandbyString2 = aLXBlackListIndvSchema.getStandbyString2();
        this.StandbyString3 = aLXBlackListIndvSchema.getStandbyString3();
        this.StandbyString4 = aLXBlackListIndvSchema.getStandbyString4();
        this.StandbyString5 = aLXBlackListIndvSchema.getStandbyString5();
        this.MakeDate = fDate.getDate( aLXBlackListIndvSchema.getMakeDate());
        this.MakeTime = aLXBlackListIndvSchema.getMakeTime();
        this.MakeOperator = aLXBlackListIndvSchema.getMakeOperator();
        this.ModifyDate = fDate.getDate( aLXBlackListIndvSchema.getModifyDate());
        this.ModifyTime = aLXBlackListIndvSchema.getModifyTime();
        this.ModifyOperator = aLXBlackListIndvSchema.getModifyOperator();
        this.MiddleName = aLXBlackListIndvSchema.getMiddleName();
        this.SurName = aLXBlackListIndvSchema.getSurName();
        this.RelevanceId = aLXBlackListIndvSchema.getRelevanceId();
        this.RelevanceType = aLXBlackListIndvSchema.getRelevanceType();
        this.ListProperty = aLXBlackListIndvSchema.getListProperty();
        this.Description1Name = aLXBlackListIndvSchema.getDescription1Name();
        this.Description2Name = aLXBlackListIndvSchema.getDescription2Name();
        this.Description3Name = aLXBlackListIndvSchema.getDescription3Name();
        this.ProFileNotes = aLXBlackListIndvSchema.getProFileNotes();
        this.ReferenceName = aLXBlackListIndvSchema.getReferenceName();
        this.RecordType = aLXBlackListIndvSchema.getRecordType();
        this.DateOfBirth = aLXBlackListIndvSchema.getDateOfBirth();
        this.MinBirthday = aLXBlackListIndvSchema.getMinBirthday();
        this.MaxBirthday = aLXBlackListIndvSchema.getMaxBirthday();
        this.ListedDate = aLXBlackListIndvSchema.getListedDate();
        this.ScriptLanguageId = aLXBlackListIndvSchema.getScriptLanguageId();
        this.FirstName = aLXBlackListIndvSchema.getFirstName();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("BlacklistID") == null )
                this.BlacklistID = null;
            else
                this.BlacklistID = rs.getString("BlacklistID").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("Usedname") == null )
                this.Usedname = null;
            else
                this.Usedname = rs.getString("Usedname").trim();

            if( rs.getString("Gender") == null )
                this.Gender = null;
            else
                this.Gender = rs.getString("Gender").trim();

            if( rs.getString("Birthplace") == null )
                this.Birthplace = null;
            else
                this.Birthplace = rs.getString("Birthplace").trim();

            if( rs.getString("Nationality") == null )
                this.Nationality = null;
            else
                this.Nationality = rs.getString("Nationality").trim();

            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            if( rs.getString("Nation") == null )
                this.Nation = null;
            else
                this.Nation = rs.getString("Nation").trim();

            if( rs.getString("Occupation") == null )
                this.Occupation = null;
            else
                this.Occupation = rs.getString("Occupation").trim();

            if( rs.getString("Title") == null )
                this.Title = null;
            else
                this.Title = rs.getString("Title").trim();

            if( rs.getString("Address") == null )
                this.Address = null;
            else
                this.Address = rs.getString("Address").trim();

            if( rs.getString("OtherInfo") == null )
                this.OtherInfo = null;
            else
                this.OtherInfo = rs.getString("OtherInfo").trim();

            if( rs.getString("ReferNo") == null )
                this.ReferNo = null;
            else
                this.ReferNo = rs.getString("ReferNo").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("StandbyString1") == null )
                this.StandbyString1 = null;
            else
                this.StandbyString1 = rs.getString("StandbyString1").trim();

            if( rs.getString("StandbyString2") == null )
                this.StandbyString2 = null;
            else
                this.StandbyString2 = rs.getString("StandbyString2").trim();

            if( rs.getString("StandbyString3") == null )
                this.StandbyString3 = null;
            else
                this.StandbyString3 = rs.getString("StandbyString3").trim();

            if( rs.getString("StandbyString4") == null )
                this.StandbyString4 = null;
            else
                this.StandbyString4 = rs.getString("StandbyString4").trim();

            if( rs.getString("StandbyString5") == null )
                this.StandbyString5 = null;
            else
                this.StandbyString5 = rs.getString("StandbyString5").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            if( rs.getString("MakeOperator") == null )
                this.MakeOperator = null;
            else
                this.MakeOperator = rs.getString("MakeOperator").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("ModifyOperator") == null )
                this.ModifyOperator = null;
            else
                this.ModifyOperator = rs.getString("ModifyOperator").trim();

            if( rs.getString("MiddleName") == null )
                this.MiddleName = null;
            else
                this.MiddleName = rs.getString("MiddleName").trim();

            if( rs.getString("SurName") == null )
                this.SurName = null;
            else
                this.SurName = rs.getString("SurName").trim();

            if( rs.getString("RelevanceId") == null )
                this.RelevanceId = null;
            else
                this.RelevanceId = rs.getString("RelevanceId").trim();

            if( rs.getString("RelevanceType") == null )
                this.RelevanceType = null;
            else
                this.RelevanceType = rs.getString("RelevanceType").trim();

            if( rs.getString("ListProperty") == null )
                this.ListProperty = null;
            else
                this.ListProperty = rs.getString("ListProperty").trim();

            if( rs.getString("Description1Name") == null )
                this.Description1Name = null;
            else
                this.Description1Name = rs.getString("Description1Name").trim();

            if( rs.getString("Description2Name") == null )
                this.Description2Name = null;
            else
                this.Description2Name = rs.getString("Description2Name").trim();

            if( rs.getString("Description3Name") == null )
                this.Description3Name = null;
            else
                this.Description3Name = rs.getString("Description3Name").trim();

            if( rs.getString("ProFileNotes") == null )
                this.ProFileNotes = null;
            else
                this.ProFileNotes = rs.getString("ProFileNotes").trim();

            if( rs.getString("ReferenceName") == null )
                this.ReferenceName = null;
            else
                this.ReferenceName = rs.getString("ReferenceName").trim();

            if( rs.getString("RecordType") == null )
                this.RecordType = null;
            else
                this.RecordType = rs.getString("RecordType").trim();

            if( rs.getString("DateOfBirth") == null )
                this.DateOfBirth = null;
            else
                this.DateOfBirth = rs.getString("DateOfBirth").trim();

            if( rs.getString("MinBirthday") == null )
                this.MinBirthday = null;
            else
                this.MinBirthday = rs.getString("MinBirthday").trim();

            if( rs.getString("MaxBirthday") == null )
                this.MaxBirthday = null;
            else
                this.MaxBirthday = rs.getString("MaxBirthday").trim();

            if( rs.getString("ListedDate") == null )
                this.ListedDate = null;
            else
                this.ListedDate = rs.getString("ListedDate").trim();

            if( rs.getString("ScriptLanguageId") == null )
                this.ScriptLanguageId = null;
            else
                this.ScriptLanguageId = rs.getString("ScriptLanguageId").trim();

            if( rs.getString("FirstName") == null )
                this.FirstName = null;
            else
                this.FirstName = rs.getString("FirstName").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LXBlackListIndvSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LXBlackListIndvSchema getSchema() {
        LXBlackListIndvSchema aLXBlackListIndvSchema = new LXBlackListIndvSchema();
        aLXBlackListIndvSchema.setSchema(this);
        return aLXBlackListIndvSchema;
    }

    public LXBlackListIndvDB getDB() {
        LXBlackListIndvDB aDBOper = new LXBlackListIndvDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLXBlackListIndv描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(BlacklistID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Usedname)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Gender)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Birthplace)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Nation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Occupation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Title)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherInfo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReferNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyString1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyString2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyString3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyString4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyString5)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MiddleName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelevanceId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelevanceType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ListProperty)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Description1Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Description2Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Description3Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProFileNotes)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReferenceName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RecordType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DateOfBirth)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MinBirthday)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MaxBirthday)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ListedDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ScriptLanguageId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstName));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLXBlackListIndv>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            BlacklistID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            Usedname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            Gender = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            Birthplace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Nation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            Occupation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            Title = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            OtherInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            ReferNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            StandbyString1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            StandbyString2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            StandbyString3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            StandbyString4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            StandbyString5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            MakeOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            ModifyOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            MiddleName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            SurName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            RelevanceId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            RelevanceType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            ListProperty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            Description1Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            Description2Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            Description3Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            ProFileNotes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            ReferenceName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            RecordType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            DateOfBirth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            MinBirthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            MaxBirthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            ListedDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            ScriptLanguageId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            FirstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LXBlackListIndvSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BlacklistID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistID));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Usedname")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Usedname));
        }
        if (FCode.equalsIgnoreCase("Gender")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Gender));
        }
        if (FCode.equalsIgnoreCase("Birthplace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Birthplace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("Nation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nation));
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Occupation));
        }
        if (FCode.equalsIgnoreCase("Title")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Title));
        }
        if (FCode.equalsIgnoreCase("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equalsIgnoreCase("OtherInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherInfo));
        }
        if (FCode.equalsIgnoreCase("ReferNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReferNo));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("StandbyString1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString1));
        }
        if (FCode.equalsIgnoreCase("StandbyString2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString2));
        }
        if (FCode.equalsIgnoreCase("StandbyString3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString3));
        }
        if (FCode.equalsIgnoreCase("StandbyString4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString4));
        }
        if (FCode.equalsIgnoreCase("StandbyString5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyString5));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeOperator));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyOperator));
        }
        if (FCode.equalsIgnoreCase("MiddleName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MiddleName));
        }
        if (FCode.equalsIgnoreCase("SurName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurName));
        }
        if (FCode.equalsIgnoreCase("RelevanceId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelevanceId));
        }
        if (FCode.equalsIgnoreCase("RelevanceType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelevanceType));
        }
        if (FCode.equalsIgnoreCase("ListProperty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ListProperty));
        }
        if (FCode.equalsIgnoreCase("Description1Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Description1Name));
        }
        if (FCode.equalsIgnoreCase("Description2Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Description2Name));
        }
        if (FCode.equalsIgnoreCase("Description3Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Description3Name));
        }
        if (FCode.equalsIgnoreCase("ProFileNotes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProFileNotes));
        }
        if (FCode.equalsIgnoreCase("ReferenceName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReferenceName));
        }
        if (FCode.equalsIgnoreCase("RecordType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecordType));
        }
        if (FCode.equalsIgnoreCase("DateOfBirth")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DateOfBirth));
        }
        if (FCode.equalsIgnoreCase("MinBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinBirthday));
        }
        if (FCode.equalsIgnoreCase("MaxBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxBirthday));
        }
        if (FCode.equalsIgnoreCase("ListedDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ListedDate));
        }
        if (FCode.equalsIgnoreCase("ScriptLanguageId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ScriptLanguageId));
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(BlacklistID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Usedname);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Gender);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Birthplace);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Nationality);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Nation);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Occupation);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Title);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(OtherInfo);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ReferNo);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(StandbyString1);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(StandbyString2);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(StandbyString3);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(StandbyString4);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(StandbyString5);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MakeOperator);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyOperator);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(MiddleName);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(SurName);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(RelevanceId);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(RelevanceType);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(ListProperty);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(Description1Name);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(Description2Name);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(Description3Name);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ProFileNotes);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(ReferenceName);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(RecordType);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(DateOfBirth);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(MinBirthday);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(MaxBirthday);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(ListedDate);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(ScriptLanguageId);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(FirstName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BlacklistID")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistID = FValue.trim();
            }
            else
                BlacklistID = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Usedname")) {
            if( FValue != null && !FValue.equals(""))
            {
                Usedname = FValue.trim();
            }
            else
                Usedname = null;
        }
        if (FCode.equalsIgnoreCase("Gender")) {
            if( FValue != null && !FValue.equals(""))
            {
                Gender = FValue.trim();
            }
            else
                Gender = null;
        }
        if (FCode.equalsIgnoreCase("Birthplace")) {
            if( FValue != null && !FValue.equals(""))
            {
                Birthplace = FValue.trim();
            }
            else
                Birthplace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("Nation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nation = FValue.trim();
            }
            else
                Nation = null;
        }
        if (FCode.equalsIgnoreCase("Occupation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Occupation = FValue.trim();
            }
            else
                Occupation = null;
        }
        if (FCode.equalsIgnoreCase("Title")) {
            if( FValue != null && !FValue.equals(""))
            {
                Title = FValue.trim();
            }
            else
                Title = null;
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if( FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
                Address = null;
        }
        if (FCode.equalsIgnoreCase("OtherInfo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherInfo = FValue.trim();
            }
            else
                OtherInfo = null;
        }
        if (FCode.equalsIgnoreCase("ReferNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReferNo = FValue.trim();
            }
            else
                ReferNo = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString1 = FValue.trim();
            }
            else
                StandbyString1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString2 = FValue.trim();
            }
            else
                StandbyString2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString3 = FValue.trim();
            }
            else
                StandbyString3 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString4 = FValue.trim();
            }
            else
                StandbyString4 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyString5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyString5 = FValue.trim();
            }
            else
                StandbyString5 = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("MakeOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeOperator = FValue.trim();
            }
            else
                MakeOperator = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyOperator = FValue.trim();
            }
            else
                ModifyOperator = null;
        }
        if (FCode.equalsIgnoreCase("MiddleName")) {
            if( FValue != null && !FValue.equals(""))
            {
                MiddleName = FValue.trim();
            }
            else
                MiddleName = null;
        }
        if (FCode.equalsIgnoreCase("SurName")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurName = FValue.trim();
            }
            else
                SurName = null;
        }
        if (FCode.equalsIgnoreCase("RelevanceId")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelevanceId = FValue.trim();
            }
            else
                RelevanceId = null;
        }
        if (FCode.equalsIgnoreCase("RelevanceType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelevanceType = FValue.trim();
            }
            else
                RelevanceType = null;
        }
        if (FCode.equalsIgnoreCase("ListProperty")) {
            if( FValue != null && !FValue.equals(""))
            {
                ListProperty = FValue.trim();
            }
            else
                ListProperty = null;
        }
        if (FCode.equalsIgnoreCase("Description1Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Description1Name = FValue.trim();
            }
            else
                Description1Name = null;
        }
        if (FCode.equalsIgnoreCase("Description2Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Description2Name = FValue.trim();
            }
            else
                Description2Name = null;
        }
        if (FCode.equalsIgnoreCase("Description3Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Description3Name = FValue.trim();
            }
            else
                Description3Name = null;
        }
        if (FCode.equalsIgnoreCase("ProFileNotes")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProFileNotes = FValue.trim();
            }
            else
                ProFileNotes = null;
        }
        if (FCode.equalsIgnoreCase("ReferenceName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReferenceName = FValue.trim();
            }
            else
                ReferenceName = null;
        }
        if (FCode.equalsIgnoreCase("RecordType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RecordType = FValue.trim();
            }
            else
                RecordType = null;
        }
        if (FCode.equalsIgnoreCase("DateOfBirth")) {
            if( FValue != null && !FValue.equals(""))
            {
                DateOfBirth = FValue.trim();
            }
            else
                DateOfBirth = null;
        }
        if (FCode.equalsIgnoreCase("MinBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                MinBirthday = FValue.trim();
            }
            else
                MinBirthday = null;
        }
        if (FCode.equalsIgnoreCase("MaxBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                MaxBirthday = FValue.trim();
            }
            else
                MaxBirthday = null;
        }
        if (FCode.equalsIgnoreCase("ListedDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ListedDate = FValue.trim();
            }
            else
                ListedDate = null;
        }
        if (FCode.equalsIgnoreCase("ScriptLanguageId")) {
            if( FValue != null && !FValue.equals(""))
            {
                ScriptLanguageId = FValue.trim();
            }
            else
                ScriptLanguageId = null;
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstName = FValue.trim();
            }
            else
                FirstName = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LXBlackListIndvSchema other = (LXBlackListIndvSchema)otherObject;
        return
            BlacklistID.equals(other.getBlacklistID())
            && Name.equals(other.getName())
            && Usedname.equals(other.getUsedname())
            && Gender.equals(other.getGender())
            && Birthplace.equals(other.getBirthplace())
            && Nationality.equals(other.getNationality())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && Nation.equals(other.getNation())
            && Occupation.equals(other.getOccupation())
            && Title.equals(other.getTitle())
            && Address.equals(other.getAddress())
            && OtherInfo.equals(other.getOtherInfo())
            && ReferNo.equals(other.getReferNo())
            && Remark.equals(other.getRemark())
            && StandbyString1.equals(other.getStandbyString1())
            && StandbyString2.equals(other.getStandbyString2())
            && StandbyString3.equals(other.getStandbyString3())
            && StandbyString4.equals(other.getStandbyString4())
            && StandbyString5.equals(other.getStandbyString5())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && MakeOperator.equals(other.getMakeOperator())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && ModifyOperator.equals(other.getModifyOperator())
            && MiddleName.equals(other.getMiddleName())
            && SurName.equals(other.getSurName())
            && RelevanceId.equals(other.getRelevanceId())
            && RelevanceType.equals(other.getRelevanceType())
            && ListProperty.equals(other.getListProperty())
            && Description1Name.equals(other.getDescription1Name())
            && Description2Name.equals(other.getDescription2Name())
            && Description3Name.equals(other.getDescription3Name())
            && ProFileNotes.equals(other.getProFileNotes())
            && ReferenceName.equals(other.getReferenceName())
            && RecordType.equals(other.getRecordType())
            && DateOfBirth.equals(other.getDateOfBirth())
            && MinBirthday.equals(other.getMinBirthday())
            && MaxBirthday.equals(other.getMaxBirthday())
            && ListedDate.equals(other.getListedDate())
            && ScriptLanguageId.equals(other.getScriptLanguageId())
            && FirstName.equals(other.getFirstName());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BlacklistID") ) {
            return 0;
        }
        if( strFieldName.equals("Name") ) {
            return 1;
        }
        if( strFieldName.equals("Usedname") ) {
            return 2;
        }
        if( strFieldName.equals("Gender") ) {
            return 3;
        }
        if( strFieldName.equals("Birthplace") ) {
            return 4;
        }
        if( strFieldName.equals("Nationality") ) {
            return 5;
        }
        if( strFieldName.equals("IDType") ) {
            return 6;
        }
        if( strFieldName.equals("IDNo") ) {
            return 7;
        }
        if( strFieldName.equals("Nation") ) {
            return 8;
        }
        if( strFieldName.equals("Occupation") ) {
            return 9;
        }
        if( strFieldName.equals("Title") ) {
            return 10;
        }
        if( strFieldName.equals("Address") ) {
            return 11;
        }
        if( strFieldName.equals("OtherInfo") ) {
            return 12;
        }
        if( strFieldName.equals("ReferNo") ) {
            return 13;
        }
        if( strFieldName.equals("Remark") ) {
            return 14;
        }
        if( strFieldName.equals("StandbyString1") ) {
            return 15;
        }
        if( strFieldName.equals("StandbyString2") ) {
            return 16;
        }
        if( strFieldName.equals("StandbyString3") ) {
            return 17;
        }
        if( strFieldName.equals("StandbyString4") ) {
            return 18;
        }
        if( strFieldName.equals("StandbyString5") ) {
            return 19;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 20;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 21;
        }
        if( strFieldName.equals("MakeOperator") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyOperator") ) {
            return 25;
        }
        if( strFieldName.equals("MiddleName") ) {
            return 26;
        }
        if( strFieldName.equals("SurName") ) {
            return 27;
        }
        if( strFieldName.equals("RelevanceId") ) {
            return 28;
        }
        if( strFieldName.equals("RelevanceType") ) {
            return 29;
        }
        if( strFieldName.equals("ListProperty") ) {
            return 30;
        }
        if( strFieldName.equals("Description1Name") ) {
            return 31;
        }
        if( strFieldName.equals("Description2Name") ) {
            return 32;
        }
        if( strFieldName.equals("Description3Name") ) {
            return 33;
        }
        if( strFieldName.equals("ProFileNotes") ) {
            return 34;
        }
        if( strFieldName.equals("ReferenceName") ) {
            return 35;
        }
        if( strFieldName.equals("RecordType") ) {
            return 36;
        }
        if( strFieldName.equals("DateOfBirth") ) {
            return 37;
        }
        if( strFieldName.equals("MinBirthday") ) {
            return 38;
        }
        if( strFieldName.equals("MaxBirthday") ) {
            return 39;
        }
        if( strFieldName.equals("ListedDate") ) {
            return 40;
        }
        if( strFieldName.equals("ScriptLanguageId") ) {
            return 41;
        }
        if( strFieldName.equals("FirstName") ) {
            return 42;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BlacklistID";
                break;
            case 1:
                strFieldName = "Name";
                break;
            case 2:
                strFieldName = "Usedname";
                break;
            case 3:
                strFieldName = "Gender";
                break;
            case 4:
                strFieldName = "Birthplace";
                break;
            case 5:
                strFieldName = "Nationality";
                break;
            case 6:
                strFieldName = "IDType";
                break;
            case 7:
                strFieldName = "IDNo";
                break;
            case 8:
                strFieldName = "Nation";
                break;
            case 9:
                strFieldName = "Occupation";
                break;
            case 10:
                strFieldName = "Title";
                break;
            case 11:
                strFieldName = "Address";
                break;
            case 12:
                strFieldName = "OtherInfo";
                break;
            case 13:
                strFieldName = "ReferNo";
                break;
            case 14:
                strFieldName = "Remark";
                break;
            case 15:
                strFieldName = "StandbyString1";
                break;
            case 16:
                strFieldName = "StandbyString2";
                break;
            case 17:
                strFieldName = "StandbyString3";
                break;
            case 18:
                strFieldName = "StandbyString4";
                break;
            case 19:
                strFieldName = "StandbyString5";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "MakeOperator";
                break;
            case 23:
                strFieldName = "ModifyDate";
                break;
            case 24:
                strFieldName = "ModifyTime";
                break;
            case 25:
                strFieldName = "ModifyOperator";
                break;
            case 26:
                strFieldName = "MiddleName";
                break;
            case 27:
                strFieldName = "SurName";
                break;
            case 28:
                strFieldName = "RelevanceId";
                break;
            case 29:
                strFieldName = "RelevanceType";
                break;
            case 30:
                strFieldName = "ListProperty";
                break;
            case 31:
                strFieldName = "Description1Name";
                break;
            case 32:
                strFieldName = "Description2Name";
                break;
            case 33:
                strFieldName = "Description3Name";
                break;
            case 34:
                strFieldName = "ProFileNotes";
                break;
            case 35:
                strFieldName = "ReferenceName";
                break;
            case 36:
                strFieldName = "RecordType";
                break;
            case 37:
                strFieldName = "DateOfBirth";
                break;
            case 38:
                strFieldName = "MinBirthday";
                break;
            case 39:
                strFieldName = "MaxBirthday";
                break;
            case 40:
                strFieldName = "ListedDate";
                break;
            case 41:
                strFieldName = "ScriptLanguageId";
                break;
            case 42:
                strFieldName = "FirstName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BLACKLISTID":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "USEDNAME":
                return Schema.TYPE_STRING;
            case "GENDER":
                return Schema.TYPE_STRING;
            case "BIRTHPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "NATION":
                return Schema.TYPE_STRING;
            case "OCCUPATION":
                return Schema.TYPE_STRING;
            case "TITLE":
                return Schema.TYPE_STRING;
            case "ADDRESS":
                return Schema.TYPE_STRING;
            case "OTHERINFO":
                return Schema.TYPE_STRING;
            case "REFERNO":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING1":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING2":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING3":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING4":
                return Schema.TYPE_STRING;
            case "STANDBYSTRING5":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MAKEOPERATOR":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MODIFYOPERATOR":
                return Schema.TYPE_STRING;
            case "MIDDLENAME":
                return Schema.TYPE_STRING;
            case "SURNAME":
                return Schema.TYPE_STRING;
            case "RELEVANCEID":
                return Schema.TYPE_STRING;
            case "RELEVANCETYPE":
                return Schema.TYPE_STRING;
            case "LISTPROPERTY":
                return Schema.TYPE_STRING;
            case "DESCRIPTION1NAME":
                return Schema.TYPE_STRING;
            case "DESCRIPTION2NAME":
                return Schema.TYPE_STRING;
            case "DESCRIPTION3NAME":
                return Schema.TYPE_STRING;
            case "PROFILENOTES":
                return Schema.TYPE_STRING;
            case "REFERENCENAME":
                return Schema.TYPE_STRING;
            case "RECORDTYPE":
                return Schema.TYPE_STRING;
            case "DATEOFBIRTH":
                return Schema.TYPE_STRING;
            case "MINBIRTHDAY":
                return Schema.TYPE_STRING;
            case "MAXBIRTHDAY":
                return Schema.TYPE_STRING;
            case "LISTEDDATE":
                return Schema.TYPE_STRING;
            case "SCRIPTLANGUAGEID":
                return Schema.TYPE_STRING;
            case "FIRSTNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_DATE;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
