/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: VMS_CODEPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class VMS_CODEPojo implements Pojo,Serializable {
    // @Field
    /** 交易类型 */
    @RedisPrimaryHKey
    private String TRANSTYPE; 
    /** 费用类型 */
    @RedisPrimaryHKey
    private String FEETYPE; 
    /** 费用描述 */
    private String FEEDES; 
    /** 收入类型 */
    private String INCOMETYP; 
    /** 收付标识 */
    private String DC_FLAG; 
    /** 上传标记 */
    private String UPLOAD_FLAG; 


    public static final int FIELDNUM = 6;    // 数据库表的字段个数
    public String getTRANSTYPE() {
        return TRANSTYPE;
    }
    public void setTRANSTYPE(String aTRANSTYPE) {
        TRANSTYPE = aTRANSTYPE;
    }
    public String getFEETYPE() {
        return FEETYPE;
    }
    public void setFEETYPE(String aFEETYPE) {
        FEETYPE = aFEETYPE;
    }
    public String getFEEDES() {
        return FEEDES;
    }
    public void setFEEDES(String aFEEDES) {
        FEEDES = aFEEDES;
    }
    public String getINCOMETYP() {
        return INCOMETYP;
    }
    public void setINCOMETYP(String aINCOMETYP) {
        INCOMETYP = aINCOMETYP;
    }
    public String getDC_FLAG() {
        return DC_FLAG;
    }
    public void setDC_FLAG(String aDC_FLAG) {
        DC_FLAG = aDC_FLAG;
    }
    public String getUPLOAD_FLAG() {
        return UPLOAD_FLAG;
    }
    public void setUPLOAD_FLAG(String aUPLOAD_FLAG) {
        UPLOAD_FLAG = aUPLOAD_FLAG;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TRANSTYPE") ) {
            return 0;
        }
        if( strFieldName.equals("FEETYPE") ) {
            return 1;
        }
        if( strFieldName.equals("FEEDES") ) {
            return 2;
        }
        if( strFieldName.equals("INCOMETYP") ) {
            return 3;
        }
        if( strFieldName.equals("DC_FLAG") ) {
            return 4;
        }
        if( strFieldName.equals("UPLOAD_FLAG") ) {
            return 5;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TRANSTYPE";
                break;
            case 1:
                strFieldName = "FEETYPE";
                break;
            case 2:
                strFieldName = "FEEDES";
                break;
            case 3:
                strFieldName = "INCOMETYP";
                break;
            case 4:
                strFieldName = "DC_FLAG";
                break;
            case 5:
                strFieldName = "UPLOAD_FLAG";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSTYPE":
                return Schema.TYPE_STRING;
            case "FEETYPE":
                return Schema.TYPE_STRING;
            case "FEEDES":
                return Schema.TYPE_STRING;
            case "INCOMETYP":
                return Schema.TYPE_STRING;
            case "DC_FLAG":
                return Schema.TYPE_STRING;
            case "UPLOAD_FLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TRANSTYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TRANSTYPE));
        }
        if (FCode.equalsIgnoreCase("FEETYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FEETYPE));
        }
        if (FCode.equalsIgnoreCase("FEEDES")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FEEDES));
        }
        if (FCode.equalsIgnoreCase("INCOMETYP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INCOMETYP));
        }
        if (FCode.equalsIgnoreCase("DC_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DC_FLAG));
        }
        if (FCode.equalsIgnoreCase("UPLOAD_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPLOAD_FLAG));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(TRANSTYPE);
                break;
            case 1:
                strFieldValue = String.valueOf(FEETYPE);
                break;
            case 2:
                strFieldValue = String.valueOf(FEEDES);
                break;
            case 3:
                strFieldValue = String.valueOf(INCOMETYP);
                break;
            case 4:
                strFieldValue = String.valueOf(DC_FLAG);
                break;
            case 5:
                strFieldValue = String.valueOf(UPLOAD_FLAG);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TRANSTYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                TRANSTYPE = FValue.trim();
            }
            else
                TRANSTYPE = null;
        }
        if (FCode.equalsIgnoreCase("FEETYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                FEETYPE = FValue.trim();
            }
            else
                FEETYPE = null;
        }
        if (FCode.equalsIgnoreCase("FEEDES")) {
            if( FValue != null && !FValue.equals(""))
            {
                FEEDES = FValue.trim();
            }
            else
                FEEDES = null;
        }
        if (FCode.equalsIgnoreCase("INCOMETYP")) {
            if( FValue != null && !FValue.equals(""))
            {
                INCOMETYP = FValue.trim();
            }
            else
                INCOMETYP = null;
        }
        if (FCode.equalsIgnoreCase("DC_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                DC_FLAG = FValue.trim();
            }
            else
                DC_FLAG = null;
        }
        if (FCode.equalsIgnoreCase("UPLOAD_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPLOAD_FLAG = FValue.trim();
            }
            else
                UPLOAD_FLAG = null;
        }
        return true;
    }


    public String toString() {
    return "VMS_CODEPojo [" +
            "TRANSTYPE="+TRANSTYPE +
            ", FEETYPE="+FEETYPE +
            ", FEEDES="+FEEDES +
            ", INCOMETYP="+INCOMETYP +
            ", DC_FLAG="+DC_FLAG +
            ", UPLOAD_FLAG="+UPLOAD_FLAG +"]";
    }
}
