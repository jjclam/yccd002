/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDImpartParamPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LDImpartParamPojo implements Pojo,Serializable {
    // @Field
    /** 告知版别 */
    @RedisPrimaryHKey
    private String ImpartVer; 
    /** 告知编码 */
    @RedisPrimaryHKey
    private String ImpartCode; 
    /** 告知参数顺序号 */
    @RedisPrimaryHKey
    private String ImpartParamNo; 
    /** 告知参数名 */
    private String ImpartParamName; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** 操作员 */
    private String Operator; 
    /** 批次号 */
    @RedisPrimaryHKey
    private int PatchNo; 


    public static final int FIELDNUM = 10;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getImpartVer() {
        return ImpartVer;
    }
    public void setImpartVer(String aImpartVer) {
        ImpartVer = aImpartVer;
    }
    public String getImpartCode() {
        return ImpartCode;
    }
    public void setImpartCode(String aImpartCode) {
        ImpartCode = aImpartCode;
    }
    public String getImpartParamNo() {
        return ImpartParamNo;
    }
    public void setImpartParamNo(String aImpartParamNo) {
        ImpartParamNo = aImpartParamNo;
    }
    public String getImpartParamName() {
        return ImpartParamName;
    }
    public void setImpartParamName(String aImpartParamName) {
        ImpartParamName = aImpartParamName;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public int getPatchNo() {
        return PatchNo;
    }
    public void setPatchNo(int aPatchNo) {
        PatchNo = aPatchNo;
    }
    public void setPatchNo(String aPatchNo) {
        if (aPatchNo != null && !aPatchNo.equals("")) {
            Integer tInteger = new Integer(aPatchNo);
            int i = tInteger.intValue();
            PatchNo = i;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ImpartVer") ) {
            return 0;
        }
        if( strFieldName.equals("ImpartCode") ) {
            return 1;
        }
        if( strFieldName.equals("ImpartParamNo") ) {
            return 2;
        }
        if( strFieldName.equals("ImpartParamName") ) {
            return 3;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 4;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 5;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 6;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 7;
        }
        if( strFieldName.equals("Operator") ) {
            return 8;
        }
        if( strFieldName.equals("PatchNo") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ImpartVer";
                break;
            case 1:
                strFieldName = "ImpartCode";
                break;
            case 2:
                strFieldName = "ImpartParamNo";
                break;
            case 3:
                strFieldName = "ImpartParamName";
                break;
            case 4:
                strFieldName = "MakeDate";
                break;
            case 5:
                strFieldName = "MakeTime";
                break;
            case 6:
                strFieldName = "ModifyDate";
                break;
            case 7:
                strFieldName = "ModifyTime";
                break;
            case 8:
                strFieldName = "Operator";
                break;
            case 9:
                strFieldName = "PatchNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "IMPARTVER":
                return Schema.TYPE_STRING;
            case "IMPARTCODE":
                return Schema.TYPE_STRING;
            case "IMPARTPARAMNO":
                return Schema.TYPE_STRING;
            case "IMPARTPARAMNAME":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "PATCHNO":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ImpartVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartVer));
        }
        if (FCode.equalsIgnoreCase("ImpartCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartCode));
        }
        if (FCode.equalsIgnoreCase("ImpartParamNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartParamNo));
        }
        if (FCode.equalsIgnoreCase("ImpartParamName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ImpartParamName));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("PatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PatchNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ImpartVer);
                break;
            case 1:
                strFieldValue = String.valueOf(ImpartCode);
                break;
            case 2:
                strFieldValue = String.valueOf(ImpartParamNo);
                break;
            case 3:
                strFieldValue = String.valueOf(ImpartParamName);
                break;
            case 4:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 5:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 6:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 7:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 8:
                strFieldValue = String.valueOf(Operator);
                break;
            case 9:
                strFieldValue = String.valueOf(PatchNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ImpartVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartVer = FValue.trim();
            }
            else
                ImpartVer = null;
        }
        if (FCode.equalsIgnoreCase("ImpartCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartCode = FValue.trim();
            }
            else
                ImpartCode = null;
        }
        if (FCode.equalsIgnoreCase("ImpartParamNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartParamNo = FValue.trim();
            }
            else
                ImpartParamNo = null;
        }
        if (FCode.equalsIgnoreCase("ImpartParamName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ImpartParamName = FValue.trim();
            }
            else
                ImpartParamName = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("PatchNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PatchNo = i;
            }
        }
        return true;
    }


    public String toString() {
    return "LDImpartParamPojo [" +
            "ImpartVer="+ImpartVer +
            ", ImpartCode="+ImpartCode +
            ", ImpartParamNo="+ImpartParamNo +
            ", ImpartParamName="+ImpartParamName +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Operator="+Operator +
            ", PatchNo="+PatchNo +"]";
    }
}
