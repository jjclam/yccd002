/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LCAppntLinkManInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LCAppntLinkManInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-13
 */
public class LCAppntLinkManInfoSchema implements Schema, Cloneable {
    // @Field
    /** Appntlinkid */
    private long AppntLinkID;
    /** Contid */
    private long ContID;
    /** Shardingid */
    private String ShardingID;
    /** 投保单号 */
    private String PrtNo;
    /** 保单号 */
    private String ContNo;
    /** 投保人客户号 */
    private String AppntNo;
    /** 联系人姓名 */
    private String Name;
    /** 联系人性别 */
    private String Sex;
    /** 联系人生日 */
    private Date Birthday;
    /** 联系人证件类型 */
    private String IDType;
    /** 联系人证件号码 */
    private String IDNo;
    /** 与投保人关系 */
    private String RelationToApp;
    /** 联系电话1 */
    private String Tel1;
    /** 联系电话3 */
    private String Tel3;
    /** 联系电话4 */
    private String Tel4;
    /** 联系电话5 */
    private String Tel5;
    /** 联系电话2 */
    private String Tel2;
    /** 手机1 */
    private String Mobile1;
    /** 手机2 */
    private String Mobile2;
    /** 地址 */
    private String Address;
    /** 邮编 */
    private String ZipCode;
    /** 电子邮箱 */
    private String Email;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 信函发送形式 */
    private String LetterSendMode;

    public static final int FIELDNUM = 29;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCAppntLinkManInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AppntLinkID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCAppntLinkManInfoSchema cloned = (LCAppntLinkManInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getAppntLinkID() {
        return AppntLinkID;
    }
    public void setAppntLinkID(long aAppntLinkID) {
        AppntLinkID = aAppntLinkID;
    }
    public void setAppntLinkID(String aAppntLinkID) {
        if (aAppntLinkID != null && !aAppntLinkID.equals("")) {
            AppntLinkID = new Long(aAppntLinkID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        if(Birthday != null) {
            return fDate.getString(Birthday);
        } else {
            return null;
        }
    }
    public void setBirthday(Date aBirthday) {
        Birthday = aBirthday;
    }
    public void setBirthday(String aBirthday) {
        if (aBirthday != null && !aBirthday.equals("")) {
            Birthday = fDate.getDate(aBirthday);
        } else
            Birthday = null;
    }

    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getRelationToApp() {
        return RelationToApp;
    }
    public void setRelationToApp(String aRelationToApp) {
        RelationToApp = aRelationToApp;
    }
    public String getTel1() {
        return Tel1;
    }
    public void setTel1(String aTel1) {
        Tel1 = aTel1;
    }
    public String getTel3() {
        return Tel3;
    }
    public void setTel3(String aTel3) {
        Tel3 = aTel3;
    }
    public String getTel4() {
        return Tel4;
    }
    public void setTel4(String aTel4) {
        Tel4 = aTel4;
    }
    public String getTel5() {
        return Tel5;
    }
    public void setTel5(String aTel5) {
        Tel5 = aTel5;
    }
    public String getTel2() {
        return Tel2;
    }
    public void setTel2(String aTel2) {
        Tel2 = aTel2;
    }
    public String getMobile1() {
        return Mobile1;
    }
    public void setMobile1(String aMobile1) {
        Mobile1 = aMobile1;
    }
    public String getMobile2() {
        return Mobile2;
    }
    public void setMobile2(String aMobile2) {
        Mobile2 = aMobile2;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String aAddress) {
        Address = aAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getEmail() {
        return Email;
    }
    public void setEmail(String aEmail) {
        Email = aEmail;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getLetterSendMode() {
        return LetterSendMode;
    }
    public void setLetterSendMode(String aLetterSendMode) {
        LetterSendMode = aLetterSendMode;
    }

    /**
    * 使用另外一个 LCAppntLinkManInfoSchema 对象给 Schema 赋值
    * @param: aLCAppntLinkManInfoSchema LCAppntLinkManInfoSchema
    **/
    public void setSchema(LCAppntLinkManInfoSchema aLCAppntLinkManInfoSchema) {
        this.AppntLinkID = aLCAppntLinkManInfoSchema.getAppntLinkID();
        this.ContID = aLCAppntLinkManInfoSchema.getContID();
        this.ShardingID = aLCAppntLinkManInfoSchema.getShardingID();
        this.PrtNo = aLCAppntLinkManInfoSchema.getPrtNo();
        this.ContNo = aLCAppntLinkManInfoSchema.getContNo();
        this.AppntNo = aLCAppntLinkManInfoSchema.getAppntNo();
        this.Name = aLCAppntLinkManInfoSchema.getName();
        this.Sex = aLCAppntLinkManInfoSchema.getSex();
        this.Birthday = fDate.getDate( aLCAppntLinkManInfoSchema.getBirthday());
        this.IDType = aLCAppntLinkManInfoSchema.getIDType();
        this.IDNo = aLCAppntLinkManInfoSchema.getIDNo();
        this.RelationToApp = aLCAppntLinkManInfoSchema.getRelationToApp();
        this.Tel1 = aLCAppntLinkManInfoSchema.getTel1();
        this.Tel3 = aLCAppntLinkManInfoSchema.getTel3();
        this.Tel4 = aLCAppntLinkManInfoSchema.getTel4();
        this.Tel5 = aLCAppntLinkManInfoSchema.getTel5();
        this.Tel2 = aLCAppntLinkManInfoSchema.getTel2();
        this.Mobile1 = aLCAppntLinkManInfoSchema.getMobile1();
        this.Mobile2 = aLCAppntLinkManInfoSchema.getMobile2();
        this.Address = aLCAppntLinkManInfoSchema.getAddress();
        this.ZipCode = aLCAppntLinkManInfoSchema.getZipCode();
        this.Email = aLCAppntLinkManInfoSchema.getEmail();
        this.ManageCom = aLCAppntLinkManInfoSchema.getManageCom();
        this.Operator = aLCAppntLinkManInfoSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCAppntLinkManInfoSchema.getMakeDate());
        this.MakeTime = aLCAppntLinkManInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCAppntLinkManInfoSchema.getModifyDate());
        this.ModifyTime = aLCAppntLinkManInfoSchema.getModifyTime();
        this.LetterSendMode = aLCAppntLinkManInfoSchema.getLetterSendMode();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.AppntLinkID = rs.getLong("AppntLinkID");
            this.ContID = rs.getLong("ContID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            this.Birthday = rs.getDate("Birthday");
            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            if( rs.getString("RelationToApp") == null )
                this.RelationToApp = null;
            else
                this.RelationToApp = rs.getString("RelationToApp").trim();

            if( rs.getString("Tel1") == null )
                this.Tel1 = null;
            else
                this.Tel1 = rs.getString("Tel1").trim();

            if( rs.getString("Tel3") == null )
                this.Tel3 = null;
            else
                this.Tel3 = rs.getString("Tel3").trim();

            if( rs.getString("Tel4") == null )
                this.Tel4 = null;
            else
                this.Tel4 = rs.getString("Tel4").trim();

            if( rs.getString("Tel5") == null )
                this.Tel5 = null;
            else
                this.Tel5 = rs.getString("Tel5").trim();

            if( rs.getString("Tel2") == null )
                this.Tel2 = null;
            else
                this.Tel2 = rs.getString("Tel2").trim();

            if( rs.getString("Mobile1") == null )
                this.Mobile1 = null;
            else
                this.Mobile1 = rs.getString("Mobile1").trim();

            if( rs.getString("Mobile2") == null )
                this.Mobile2 = null;
            else
                this.Mobile2 = rs.getString("Mobile2").trim();

            if( rs.getString("Address") == null )
                this.Address = null;
            else
                this.Address = rs.getString("Address").trim();

            if( rs.getString("ZipCode") == null )
                this.ZipCode = null;
            else
                this.ZipCode = rs.getString("ZipCode").trim();

            if( rs.getString("Email") == null )
                this.Email = null;
            else
                this.Email = rs.getString("Email").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("LetterSendMode") == null )
                this.LetterSendMode = null;
            else
                this.LetterSendMode = rs.getString("LetterSendMode").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntLinkManInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCAppntLinkManInfoSchema getSchema() {
        LCAppntLinkManInfoSchema aLCAppntLinkManInfoSchema = new LCAppntLinkManInfoSchema();
        aLCAppntLinkManInfoSchema.setSchema(this);
        return aLCAppntLinkManInfoSchema;
    }

    public LCAppntLinkManInfoDB getDB() {
        LCAppntLinkManInfoDB aDBOper = new LCAppntLinkManInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAppntLinkManInfo描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(AppntLinkID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ContID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToApp)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Tel1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Tel3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Tel4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Tel5)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Tel2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Email)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LetterSendMode));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAppntLinkManInfo>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            AppntLinkID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ContID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            RelationToApp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            Tel1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            Tel3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            Tel4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            Tel5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            Tel2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            Mobile1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Mobile2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            LetterSendMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntLinkManInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AppntLinkID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntLinkID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("RelationToApp")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToApp));
        }
        if (FCode.equalsIgnoreCase("Tel1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Tel1));
        }
        if (FCode.equalsIgnoreCase("Tel3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Tel3));
        }
        if (FCode.equalsIgnoreCase("Tel4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Tel4));
        }
        if (FCode.equalsIgnoreCase("Tel5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Tel5));
        }
        if (FCode.equalsIgnoreCase("Tel2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Tel2));
        }
        if (FCode.equalsIgnoreCase("Mobile1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile1));
        }
        if (FCode.equalsIgnoreCase("Mobile2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile2));
        }
        if (FCode.equalsIgnoreCase("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Email")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("LetterSendMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LetterSendMode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AppntLinkID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(RelationToApp);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Tel1);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Tel3);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Tel4);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Tel5);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Tel2);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Mobile1);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Mobile2);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Email);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(LetterSendMode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AppntLinkID")) {
            if( FValue != null && !FValue.equals("")) {
                AppntLinkID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if(FValue != null && !FValue.equals("")) {
                Birthday = fDate.getDate( FValue );
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("RelationToApp")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToApp = FValue.trim();
            }
            else
                RelationToApp = null;
        }
        if (FCode.equalsIgnoreCase("Tel1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Tel1 = FValue.trim();
            }
            else
                Tel1 = null;
        }
        if (FCode.equalsIgnoreCase("Tel3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Tel3 = FValue.trim();
            }
            else
                Tel3 = null;
        }
        if (FCode.equalsIgnoreCase("Tel4")) {
            if( FValue != null && !FValue.equals(""))
            {
                Tel4 = FValue.trim();
            }
            else
                Tel4 = null;
        }
        if (FCode.equalsIgnoreCase("Tel5")) {
            if( FValue != null && !FValue.equals(""))
            {
                Tel5 = FValue.trim();
            }
            else
                Tel5 = null;
        }
        if (FCode.equalsIgnoreCase("Tel2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Tel2 = FValue.trim();
            }
            else
                Tel2 = null;
        }
        if (FCode.equalsIgnoreCase("Mobile1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile1 = FValue.trim();
            }
            else
                Mobile1 = null;
        }
        if (FCode.equalsIgnoreCase("Mobile2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile2 = FValue.trim();
            }
            else
                Mobile2 = null;
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if( FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
                Address = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Email")) {
            if( FValue != null && !FValue.equals(""))
            {
                Email = FValue.trim();
            }
            else
                Email = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("LetterSendMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                LetterSendMode = FValue.trim();
            }
            else
                LetterSendMode = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCAppntLinkManInfoSchema other = (LCAppntLinkManInfoSchema)otherObject;
        return
            AppntLinkID == other.getAppntLinkID()
            && ContID == other.getContID()
            && ShardingID.equals(other.getShardingID())
            && PrtNo.equals(other.getPrtNo())
            && ContNo.equals(other.getContNo())
            && AppntNo.equals(other.getAppntNo())
            && Name.equals(other.getName())
            && Sex.equals(other.getSex())
            && fDate.getString(Birthday).equals(other.getBirthday())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && RelationToApp.equals(other.getRelationToApp())
            && Tel1.equals(other.getTel1())
            && Tel3.equals(other.getTel3())
            && Tel4.equals(other.getTel4())
            && Tel5.equals(other.getTel5())
            && Tel2.equals(other.getTel2())
            && Mobile1.equals(other.getMobile1())
            && Mobile2.equals(other.getMobile2())
            && Address.equals(other.getAddress())
            && ZipCode.equals(other.getZipCode())
            && Email.equals(other.getEmail())
            && ManageCom.equals(other.getManageCom())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && LetterSendMode.equals(other.getLetterSendMode());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AppntLinkID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 5;
        }
        if( strFieldName.equals("Name") ) {
            return 6;
        }
        if( strFieldName.equals("Sex") ) {
            return 7;
        }
        if( strFieldName.equals("Birthday") ) {
            return 8;
        }
        if( strFieldName.equals("IDType") ) {
            return 9;
        }
        if( strFieldName.equals("IDNo") ) {
            return 10;
        }
        if( strFieldName.equals("RelationToApp") ) {
            return 11;
        }
        if( strFieldName.equals("Tel1") ) {
            return 12;
        }
        if( strFieldName.equals("Tel3") ) {
            return 13;
        }
        if( strFieldName.equals("Tel4") ) {
            return 14;
        }
        if( strFieldName.equals("Tel5") ) {
            return 15;
        }
        if( strFieldName.equals("Tel2") ) {
            return 16;
        }
        if( strFieldName.equals("Mobile1") ) {
            return 17;
        }
        if( strFieldName.equals("Mobile2") ) {
            return 18;
        }
        if( strFieldName.equals("Address") ) {
            return 19;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 20;
        }
        if( strFieldName.equals("Email") ) {
            return 21;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 22;
        }
        if( strFieldName.equals("Operator") ) {
            return 23;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 24;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 25;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 26;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 27;
        }
        if( strFieldName.equals("LetterSendMode") ) {
            return 28;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AppntLinkID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "PrtNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "AppntNo";
                break;
            case 6:
                strFieldName = "Name";
                break;
            case 7:
                strFieldName = "Sex";
                break;
            case 8:
                strFieldName = "Birthday";
                break;
            case 9:
                strFieldName = "IDType";
                break;
            case 10:
                strFieldName = "IDNo";
                break;
            case 11:
                strFieldName = "RelationToApp";
                break;
            case 12:
                strFieldName = "Tel1";
                break;
            case 13:
                strFieldName = "Tel3";
                break;
            case 14:
                strFieldName = "Tel4";
                break;
            case 15:
                strFieldName = "Tel5";
                break;
            case 16:
                strFieldName = "Tel2";
                break;
            case 17:
                strFieldName = "Mobile1";
                break;
            case 18:
                strFieldName = "Mobile2";
                break;
            case 19:
                strFieldName = "Address";
                break;
            case 20:
                strFieldName = "ZipCode";
                break;
            case 21:
                strFieldName = "Email";
                break;
            case 22:
                strFieldName = "ManageCom";
                break;
            case 23:
                strFieldName = "Operator";
                break;
            case 24:
                strFieldName = "MakeDate";
                break;
            case 25:
                strFieldName = "MakeTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "ModifyTime";
                break;
            case 28:
                strFieldName = "LetterSendMode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "APPNTLINKID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_DATE;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "RELATIONTOAPP":
                return Schema.TYPE_STRING;
            case "TEL1":
                return Schema.TYPE_STRING;
            case "TEL3":
                return Schema.TYPE_STRING;
            case "TEL4":
                return Schema.TYPE_STRING;
            case "TEL5":
                return Schema.TYPE_STRING;
            case "TEL2":
                return Schema.TYPE_STRING;
            case "MOBILE1":
                return Schema.TYPE_STRING;
            case "MOBILE2":
                return Schema.TYPE_STRING;
            case "ADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "LETTERSENDMODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_DATE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "LCAppntLinkManInfoSchema {" +
            "AppntLinkID="+AppntLinkID +
            ", ContID="+ContID +
            ", ShardingID="+ShardingID +
            ", PrtNo="+PrtNo +
            ", ContNo="+ContNo +
            ", AppntNo="+AppntNo +
            ", Name="+Name +
            ", Sex="+Sex +
            ", Birthday="+Birthday +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", RelationToApp="+RelationToApp +
            ", Tel1="+Tel1 +
            ", Tel3="+Tel3 +
            ", Tel4="+Tel4 +
            ", Tel5="+Tel5 +
            ", Tel2="+Tel2 +
            ", Mobile1="+Mobile1 +
            ", Mobile2="+Mobile2 +
            ", Address="+Address +
            ", ZipCode="+ZipCode +
            ", Email="+Email +
            ", ManageCom="+ManageCom +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", LetterSendMode="+LetterSendMode +"}";
    }
}
