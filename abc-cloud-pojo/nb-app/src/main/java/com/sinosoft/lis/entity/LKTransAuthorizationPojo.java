/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LKTransAuthorizationPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LKTransAuthorizationPojo implements Pojo,Serializable {
    // @Field
    /** 管理机构 */
    @RedisPrimaryHKey
    private String ManageCom; 
    /** 银行编码 */
    @RedisPrimaryHKey
    private String BankCode; 
    /** 地区编码 */
    @RedisPrimaryHKey
    private String BankBranch; 
    /** 网点编码 */
    @RedisPrimaryHKey
    private String BankNode; 
    /** 交易码 */
    @RedisPrimaryHKey
    private String FuncFlag; 
    /** 备用 */
    private String StandByFlag; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** 操作员号 */
    private String Operator; 
    /** Issue_way */
    private String ISSUE_WAY; 
    /** State_id */
    private String STATE_ID; 


    public static final int FIELDNUM = 13;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankBranch() {
        return BankBranch;
    }
    public void setBankBranch(String aBankBranch) {
        BankBranch = aBankBranch;
    }
    public String getBankNode() {
        return BankNode;
    }
    public void setBankNode(String aBankNode) {
        BankNode = aBankNode;
    }
    public String getFuncFlag() {
        return FuncFlag;
    }
    public void setFuncFlag(String aFuncFlag) {
        FuncFlag = aFuncFlag;
    }
    public String getStandByFlag() {
        return StandByFlag;
    }
    public void setStandByFlag(String aStandByFlag) {
        StandByFlag = aStandByFlag;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getISSUE_WAY() {
        return ISSUE_WAY;
    }
    public void setISSUE_WAY(String aISSUE_WAY) {
        ISSUE_WAY = aISSUE_WAY;
    }
    public String getSTATE_ID() {
        return STATE_ID;
    }
    public void setSTATE_ID(String aSTATE_ID) {
        STATE_ID = aSTATE_ID;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ManageCom") ) {
            return 0;
        }
        if( strFieldName.equals("BankCode") ) {
            return 1;
        }
        if( strFieldName.equals("BankBranch") ) {
            return 2;
        }
        if( strFieldName.equals("BankNode") ) {
            return 3;
        }
        if( strFieldName.equals("FuncFlag") ) {
            return 4;
        }
        if( strFieldName.equals("StandByFlag") ) {
            return 5;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 6;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 7;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 8;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 9;
        }
        if( strFieldName.equals("Operator") ) {
            return 10;
        }
        if( strFieldName.equals("ISSUE_WAY") ) {
            return 11;
        }
        if( strFieldName.equals("STATE_ID") ) {
            return 12;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "BankCode";
                break;
            case 2:
                strFieldName = "BankBranch";
                break;
            case 3:
                strFieldName = "BankNode";
                break;
            case 4:
                strFieldName = "FuncFlag";
                break;
            case 5:
                strFieldName = "StandByFlag";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "ISSUE_WAY";
                break;
            case 12:
                strFieldName = "STATE_ID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKBRANCH":
                return Schema.TYPE_STRING;
            case "BANKNODE":
                return Schema.TYPE_STRING;
            case "FUNCFLAG":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "ISSUE_WAY":
                return Schema.TYPE_STRING;
            case "STATE_ID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankBranch));
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FuncFlag));
        }
        if (FCode.equalsIgnoreCase("StandByFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("ISSUE_WAY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ISSUE_WAY));
        }
        if (FCode.equalsIgnoreCase("STATE_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STATE_ID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 1:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 2:
                strFieldValue = String.valueOf(BankBranch);
                break;
            case 3:
                strFieldValue = String.valueOf(BankNode);
                break;
            case 4:
                strFieldValue = String.valueOf(FuncFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(StandByFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 7:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 8:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 9:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 10:
                strFieldValue = String.valueOf(Operator);
                break;
            case 11:
                strFieldValue = String.valueOf(ISSUE_WAY);
                break;
            case 12:
                strFieldValue = String.valueOf(STATE_ID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankBranch = FValue.trim();
            }
            else
                BankBranch = null;
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankNode = FValue.trim();
            }
            else
                BankNode = null;
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FuncFlag = FValue.trim();
            }
            else
                FuncFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag = FValue.trim();
            }
            else
                StandByFlag = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("ISSUE_WAY")) {
            if( FValue != null && !FValue.equals(""))
            {
                ISSUE_WAY = FValue.trim();
            }
            else
                ISSUE_WAY = null;
        }
        if (FCode.equalsIgnoreCase("STATE_ID")) {
            if( FValue != null && !FValue.equals(""))
            {
                STATE_ID = FValue.trim();
            }
            else
                STATE_ID = null;
        }
        return true;
    }


    public String toString() {
    return "LKTransAuthorizationPojo [" +
            "ManageCom="+ManageCom +
            ", BankCode="+BankCode +
            ", BankBranch="+BankBranch +
            ", BankNode="+BankNode +
            ", FuncFlag="+FuncFlag +
            ", StandByFlag="+StandByFlag +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Operator="+Operator +
            ", ISSUE_WAY="+ISSUE_WAY +
            ", STATE_ID="+STATE_ID +"]";
    }
}
