/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LDPersonSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LDPersonSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long PersonID;
    /** Shardingid */
    private String ShardingID;
    /** 客户号码 */
    private String CustomerNo;
    /** 客户姓名 */
    private String Name;
    /** 客户性别 */
    private String Sex;
    /** 客户出生日期 */
    private Date Birthday;
    /** 证件类型 */
    private String IDType;
    /** 证件号码 */
    private String IDNo;
    /** 密码 */
    private String Password;
    /** 国籍 */
    private String NativePlace;
    /** 民族 */
    private String Nationality;
    /** 户口所在地 */
    private String RgtAddress;
    /** 婚姻状况 */
    private String Marriage;
    /** 结婚日期 */
    private Date MarriageDate;
    /** 健康状况 */
    private String Health;
    /** 身高 */
    private double Stature;
    /** 体重 */
    private double Avoirdupois;
    /** 学历 */
    private String Degree;
    /** 信用等级 */
    private String CreditGrade;
    /** 其它证件类型 */
    private String OthIDType;
    /** 其它证件号码 */
    private String OthIDNo;
    /** Ic卡号 */
    private String ICNo;
    /** 单位编码 */
    private String GrpNo;
    /** 入司日期 */
    private Date JoinCompanyDate;
    /** 参加工作日期 */
    private Date StartWorkDate;
    /** 职位 */
    private String Position;
    /** 工资 */
    private double Salary;
    /** 职业类别 */
    private String OccupationType;
    /** 职业代码 */
    private String OccupationCode;
    /** 职业（工种） */
    private String WorkType;
    /** 兼职（工种） */
    private String PluralityType;
    /** 死亡日期 */
    private Date DeathDate;
    /** 是否吸烟标志 */
    private String SmokeFlag;
    /** 黑名单标记 */
    private String BlacklistFlag;
    /** 属性 */
    private String Proterty;
    /** 备注 */
    private String Remark;
    /** 状态 */
    private String State;
    /** Vip值 */
    private String VIPValue;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 单位名称 */
    private String GrpName;
    /** 驾照 */
    private String License;
    /** 驾照类型 */
    private String LicenseType;
    /** 社保登记号 */
    private String SocialInsuNo;
    /** 证件有效期 */
    private String IdValiDate;
    /** 是否有摩托车驾照 */
    private String HaveMotorcycleLicence;
    /** 兼职 */
    private String PartTimeJob;
    /** 医保标识 */
    private String HealthFlag;
    /** 在职标识 */
    private String ServiceMark;
    /** Firstname */
    private String FirstName;
    /** Lastname */
    private String LastName;
    /** 客户级别 */
    private String CUSLevel;
    /** 社保标记 */
    private String SSFlag;
    /** 居民类型 */
    private String RgtTpye;
    /** 美国纳税人识别号 */
    private String TINNO;
    /** 是否为美国纳税义务的个人 */
    private String TINFlag;
    /** 新客户标识 */
    private String NewCustomerFlag;

    public static final int FIELDNUM = 60;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDPersonSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PersonID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDPersonSchema cloned = (LDPersonSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getPersonID() {
        return PersonID;
    }
    public void setPersonID(long aPersonID) {
        PersonID = aPersonID;
    }
    public void setPersonID(String aPersonID) {
        if (aPersonID != null && !aPersonID.equals("")) {
            PersonID = new Long(aPersonID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        if(Birthday != null) {
            return fDate.getString(Birthday);
        } else {
            return null;
        }
    }
    public void setBirthday(Date aBirthday) {
        Birthday = aBirthday;
    }
    public void setBirthday(String aBirthday) {
        if (aBirthday != null && !aBirthday.equals("")) {
            Birthday = fDate.getDate(aBirthday);
        } else
            Birthday = null;
    }

    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getMarriageDate() {
        if(MarriageDate != null) {
            return fDate.getString(MarriageDate);
        } else {
            return null;
        }
    }
    public void setMarriageDate(Date aMarriageDate) {
        MarriageDate = aMarriageDate;
    }
    public void setMarriageDate(String aMarriageDate) {
        if (aMarriageDate != null && !aMarriageDate.equals("")) {
            MarriageDate = fDate.getDate(aMarriageDate);
        } else
            MarriageDate = null;
    }

    public String getHealth() {
        return Health;
    }
    public void setHealth(String aHealth) {
        Health = aHealth;
    }
    public double getStature() {
        return Stature;
    }
    public void setStature(double aStature) {
        Stature = aStature;
    }
    public void setStature(String aStature) {
        if (aStature != null && !aStature.equals("")) {
            Double tDouble = new Double(aStature);
            double d = tDouble.doubleValue();
            Stature = d;
        }
    }

    public double getAvoirdupois() {
        return Avoirdupois;
    }
    public void setAvoirdupois(double aAvoirdupois) {
        Avoirdupois = aAvoirdupois;
    }
    public void setAvoirdupois(String aAvoirdupois) {
        if (aAvoirdupois != null && !aAvoirdupois.equals("")) {
            Double tDouble = new Double(aAvoirdupois);
            double d = tDouble.doubleValue();
            Avoirdupois = d;
        }
    }

    public String getDegree() {
        return Degree;
    }
    public void setDegree(String aDegree) {
        Degree = aDegree;
    }
    public String getCreditGrade() {
        return CreditGrade;
    }
    public void setCreditGrade(String aCreditGrade) {
        CreditGrade = aCreditGrade;
    }
    public String getOthIDType() {
        return OthIDType;
    }
    public void setOthIDType(String aOthIDType) {
        OthIDType = aOthIDType;
    }
    public String getOthIDNo() {
        return OthIDNo;
    }
    public void setOthIDNo(String aOthIDNo) {
        OthIDNo = aOthIDNo;
    }
    public String getICNo() {
        return ICNo;
    }
    public void setICNo(String aICNo) {
        ICNo = aICNo;
    }
    public String getGrpNo() {
        return GrpNo;
    }
    public void setGrpNo(String aGrpNo) {
        GrpNo = aGrpNo;
    }
    public String getJoinCompanyDate() {
        if(JoinCompanyDate != null) {
            return fDate.getString(JoinCompanyDate);
        } else {
            return null;
        }
    }
    public void setJoinCompanyDate(Date aJoinCompanyDate) {
        JoinCompanyDate = aJoinCompanyDate;
    }
    public void setJoinCompanyDate(String aJoinCompanyDate) {
        if (aJoinCompanyDate != null && !aJoinCompanyDate.equals("")) {
            JoinCompanyDate = fDate.getDate(aJoinCompanyDate);
        } else
            JoinCompanyDate = null;
    }

    public String getStartWorkDate() {
        if(StartWorkDate != null) {
            return fDate.getString(StartWorkDate);
        } else {
            return null;
        }
    }
    public void setStartWorkDate(Date aStartWorkDate) {
        StartWorkDate = aStartWorkDate;
    }
    public void setStartWorkDate(String aStartWorkDate) {
        if (aStartWorkDate != null && !aStartWorkDate.equals("")) {
            StartWorkDate = fDate.getDate(aStartWorkDate);
        } else
            StartWorkDate = null;
    }

    public String getPosition() {
        return Position;
    }
    public void setPosition(String aPosition) {
        Position = aPosition;
    }
    public double getSalary() {
        return Salary;
    }
    public void setSalary(double aSalary) {
        Salary = aSalary;
    }
    public void setSalary(String aSalary) {
        if (aSalary != null && !aSalary.equals("")) {
            Double tDouble = new Double(aSalary);
            double d = tDouble.doubleValue();
            Salary = d;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getWorkType() {
        return WorkType;
    }
    public void setWorkType(String aWorkType) {
        WorkType = aWorkType;
    }
    public String getPluralityType() {
        return PluralityType;
    }
    public void setPluralityType(String aPluralityType) {
        PluralityType = aPluralityType;
    }
    public String getDeathDate() {
        if(DeathDate != null) {
            return fDate.getString(DeathDate);
        } else {
            return null;
        }
    }
    public void setDeathDate(Date aDeathDate) {
        DeathDate = aDeathDate;
    }
    public void setDeathDate(String aDeathDate) {
        if (aDeathDate != null && !aDeathDate.equals("")) {
            DeathDate = fDate.getDate(aDeathDate);
        } else
            DeathDate = null;
    }

    public String getSmokeFlag() {
        return SmokeFlag;
    }
    public void setSmokeFlag(String aSmokeFlag) {
        SmokeFlag = aSmokeFlag;
    }
    public String getBlacklistFlag() {
        return BlacklistFlag;
    }
    public void setBlacklistFlag(String aBlacklistFlag) {
        BlacklistFlag = aBlacklistFlag;
    }
    public String getProterty() {
        return Proterty;
    }
    public void setProterty(String aProterty) {
        Proterty = aProterty;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getVIPValue() {
        return VIPValue;
    }
    public void setVIPValue(String aVIPValue) {
        VIPValue = aVIPValue;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getLicense() {
        return License;
    }
    public void setLicense(String aLicense) {
        License = aLicense;
    }
    public String getLicenseType() {
        return LicenseType;
    }
    public void setLicenseType(String aLicenseType) {
        LicenseType = aLicenseType;
    }
    public String getSocialInsuNo() {
        return SocialInsuNo;
    }
    public void setSocialInsuNo(String aSocialInsuNo) {
        SocialInsuNo = aSocialInsuNo;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getHaveMotorcycleLicence() {
        return HaveMotorcycleLicence;
    }
    public void setHaveMotorcycleLicence(String aHaveMotorcycleLicence) {
        HaveMotorcycleLicence = aHaveMotorcycleLicence;
    }
    public String getPartTimeJob() {
        return PartTimeJob;
    }
    public void setPartTimeJob(String aPartTimeJob) {
        PartTimeJob = aPartTimeJob;
    }
    public String getHealthFlag() {
        return HealthFlag;
    }
    public void setHealthFlag(String aHealthFlag) {
        HealthFlag = aHealthFlag;
    }
    public String getServiceMark() {
        return ServiceMark;
    }
    public void setServiceMark(String aServiceMark) {
        ServiceMark = aServiceMark;
    }
    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String aFirstName) {
        FirstName = aFirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public void setLastName(String aLastName) {
        LastName = aLastName;
    }
    public String getCUSLevel() {
        return CUSLevel;
    }
    public void setCUSLevel(String aCUSLevel) {
        CUSLevel = aCUSLevel;
    }
    public String getSSFlag() {
        return SSFlag;
    }
    public void setSSFlag(String aSSFlag) {
        SSFlag = aSSFlag;
    }
    public String getRgtTpye() {
        return RgtTpye;
    }
    public void setRgtTpye(String aRgtTpye) {
        RgtTpye = aRgtTpye;
    }
    public String getTINNO() {
        return TINNO;
    }
    public void setTINNO(String aTINNO) {
        TINNO = aTINNO;
    }
    public String getTINFlag() {
        return TINFlag;
    }
    public void setTINFlag(String aTINFlag) {
        TINFlag = aTINFlag;
    }
    public String getNewCustomerFlag() {
        return NewCustomerFlag;
    }
    public void setNewCustomerFlag(String aNewCustomerFlag) {
        NewCustomerFlag = aNewCustomerFlag;
    }

    /**
    * 使用另外一个 LDPersonSchema 对象给 Schema 赋值
    * @param: aLDPersonSchema LDPersonSchema
    **/
    public void setSchema(LDPersonSchema aLDPersonSchema) {
        this.PersonID = aLDPersonSchema.getPersonID();
        this.ShardingID = aLDPersonSchema.getShardingID();
        this.CustomerNo = aLDPersonSchema.getCustomerNo();
        this.Name = aLDPersonSchema.getName();
        this.Sex = aLDPersonSchema.getSex();
        this.Birthday = fDate.getDate( aLDPersonSchema.getBirthday());
        this.IDType = aLDPersonSchema.getIDType();
        this.IDNo = aLDPersonSchema.getIDNo();
        this.Password = aLDPersonSchema.getPassword();
        this.NativePlace = aLDPersonSchema.getNativePlace();
        this.Nationality = aLDPersonSchema.getNationality();
        this.RgtAddress = aLDPersonSchema.getRgtAddress();
        this.Marriage = aLDPersonSchema.getMarriage();
        this.MarriageDate = fDate.getDate( aLDPersonSchema.getMarriageDate());
        this.Health = aLDPersonSchema.getHealth();
        this.Stature = aLDPersonSchema.getStature();
        this.Avoirdupois = aLDPersonSchema.getAvoirdupois();
        this.Degree = aLDPersonSchema.getDegree();
        this.CreditGrade = aLDPersonSchema.getCreditGrade();
        this.OthIDType = aLDPersonSchema.getOthIDType();
        this.OthIDNo = aLDPersonSchema.getOthIDNo();
        this.ICNo = aLDPersonSchema.getICNo();
        this.GrpNo = aLDPersonSchema.getGrpNo();
        this.JoinCompanyDate = fDate.getDate( aLDPersonSchema.getJoinCompanyDate());
        this.StartWorkDate = fDate.getDate( aLDPersonSchema.getStartWorkDate());
        this.Position = aLDPersonSchema.getPosition();
        this.Salary = aLDPersonSchema.getSalary();
        this.OccupationType = aLDPersonSchema.getOccupationType();
        this.OccupationCode = aLDPersonSchema.getOccupationCode();
        this.WorkType = aLDPersonSchema.getWorkType();
        this.PluralityType = aLDPersonSchema.getPluralityType();
        this.DeathDate = fDate.getDate( aLDPersonSchema.getDeathDate());
        this.SmokeFlag = aLDPersonSchema.getSmokeFlag();
        this.BlacklistFlag = aLDPersonSchema.getBlacklistFlag();
        this.Proterty = aLDPersonSchema.getProterty();
        this.Remark = aLDPersonSchema.getRemark();
        this.State = aLDPersonSchema.getState();
        this.VIPValue = aLDPersonSchema.getVIPValue();
        this.Operator = aLDPersonSchema.getOperator();
        this.MakeDate = fDate.getDate( aLDPersonSchema.getMakeDate());
        this.MakeTime = aLDPersonSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLDPersonSchema.getModifyDate());
        this.ModifyTime = aLDPersonSchema.getModifyTime();
        this.GrpName = aLDPersonSchema.getGrpName();
        this.License = aLDPersonSchema.getLicense();
        this.LicenseType = aLDPersonSchema.getLicenseType();
        this.SocialInsuNo = aLDPersonSchema.getSocialInsuNo();
        this.IdValiDate = aLDPersonSchema.getIdValiDate();
        this.HaveMotorcycleLicence = aLDPersonSchema.getHaveMotorcycleLicence();
        this.PartTimeJob = aLDPersonSchema.getPartTimeJob();
        this.HealthFlag = aLDPersonSchema.getHealthFlag();
        this.ServiceMark = aLDPersonSchema.getServiceMark();
        this.FirstName = aLDPersonSchema.getFirstName();
        this.LastName = aLDPersonSchema.getLastName();
        this.CUSLevel = aLDPersonSchema.getCUSLevel();
        this.SSFlag = aLDPersonSchema.getSSFlag();
        this.RgtTpye = aLDPersonSchema.getRgtTpye();
        this.TINNO = aLDPersonSchema.getTINNO();
        this.TINFlag = aLDPersonSchema.getTINFlag();
        this.NewCustomerFlag = aLDPersonSchema.getNewCustomerFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.PersonID = rs.getLong("PersonID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            this.Birthday = rs.getDate("Birthday");
            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            if( rs.getString("Password") == null )
                this.Password = null;
            else
                this.Password = rs.getString("Password").trim();

            if( rs.getString("NativePlace") == null )
                this.NativePlace = null;
            else
                this.NativePlace = rs.getString("NativePlace").trim();

            if( rs.getString("Nationality") == null )
                this.Nationality = null;
            else
                this.Nationality = rs.getString("Nationality").trim();

            if( rs.getString("RgtAddress") == null )
                this.RgtAddress = null;
            else
                this.RgtAddress = rs.getString("RgtAddress").trim();

            if( rs.getString("Marriage") == null )
                this.Marriage = null;
            else
                this.Marriage = rs.getString("Marriage").trim();

            this.MarriageDate = rs.getDate("MarriageDate");
            if( rs.getString("Health") == null )
                this.Health = null;
            else
                this.Health = rs.getString("Health").trim();

            this.Stature = rs.getDouble("Stature");
            this.Avoirdupois = rs.getDouble("Avoirdupois");
            if( rs.getString("Degree") == null )
                this.Degree = null;
            else
                this.Degree = rs.getString("Degree").trim();

            if( rs.getString("CreditGrade") == null )
                this.CreditGrade = null;
            else
                this.CreditGrade = rs.getString("CreditGrade").trim();

            if( rs.getString("OthIDType") == null )
                this.OthIDType = null;
            else
                this.OthIDType = rs.getString("OthIDType").trim();

            if( rs.getString("OthIDNo") == null )
                this.OthIDNo = null;
            else
                this.OthIDNo = rs.getString("OthIDNo").trim();

            if( rs.getString("ICNo") == null )
                this.ICNo = null;
            else
                this.ICNo = rs.getString("ICNo").trim();

            if( rs.getString("GrpNo") == null )
                this.GrpNo = null;
            else
                this.GrpNo = rs.getString("GrpNo").trim();

            this.JoinCompanyDate = rs.getDate("JoinCompanyDate");
            this.StartWorkDate = rs.getDate("StartWorkDate");
            if( rs.getString("Position") == null )
                this.Position = null;
            else
                this.Position = rs.getString("Position").trim();

            this.Salary = rs.getDouble("Salary");
            if( rs.getString("OccupationType") == null )
                this.OccupationType = null;
            else
                this.OccupationType = rs.getString("OccupationType").trim();

            if( rs.getString("OccupationCode") == null )
                this.OccupationCode = null;
            else
                this.OccupationCode = rs.getString("OccupationCode").trim();

            if( rs.getString("WorkType") == null )
                this.WorkType = null;
            else
                this.WorkType = rs.getString("WorkType").trim();

            if( rs.getString("PluralityType") == null )
                this.PluralityType = null;
            else
                this.PluralityType = rs.getString("PluralityType").trim();

            this.DeathDate = rs.getDate("DeathDate");
            if( rs.getString("SmokeFlag") == null )
                this.SmokeFlag = null;
            else
                this.SmokeFlag = rs.getString("SmokeFlag").trim();

            if( rs.getString("BlacklistFlag") == null )
                this.BlacklistFlag = null;
            else
                this.BlacklistFlag = rs.getString("BlacklistFlag").trim();

            if( rs.getString("Proterty") == null )
                this.Proterty = null;
            else
                this.Proterty = rs.getString("Proterty").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("VIPValue") == null )
                this.VIPValue = null;
            else
                this.VIPValue = rs.getString("VIPValue").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("GrpName") == null )
                this.GrpName = null;
            else
                this.GrpName = rs.getString("GrpName").trim();

            if( rs.getString("License") == null )
                this.License = null;
            else
                this.License = rs.getString("License").trim();

            if( rs.getString("LicenseType") == null )
                this.LicenseType = null;
            else
                this.LicenseType = rs.getString("LicenseType").trim();

            if( rs.getString("SocialInsuNo") == null )
                this.SocialInsuNo = null;
            else
                this.SocialInsuNo = rs.getString("SocialInsuNo").trim();

            if( rs.getString("IdValiDate") == null )
                this.IdValiDate = null;
            else
                this.IdValiDate = rs.getString("IdValiDate").trim();

            if( rs.getString("HaveMotorcycleLicence") == null )
                this.HaveMotorcycleLicence = null;
            else
                this.HaveMotorcycleLicence = rs.getString("HaveMotorcycleLicence").trim();

            if( rs.getString("PartTimeJob") == null )
                this.PartTimeJob = null;
            else
                this.PartTimeJob = rs.getString("PartTimeJob").trim();

            if( rs.getString("HealthFlag") == null )
                this.HealthFlag = null;
            else
                this.HealthFlag = rs.getString("HealthFlag").trim();

            if( rs.getString("ServiceMark") == null )
                this.ServiceMark = null;
            else
                this.ServiceMark = rs.getString("ServiceMark").trim();

            if( rs.getString("FirstName") == null )
                this.FirstName = null;
            else
                this.FirstName = rs.getString("FirstName").trim();

            if( rs.getString("LastName") == null )
                this.LastName = null;
            else
                this.LastName = rs.getString("LastName").trim();

            if( rs.getString("CUSLevel") == null )
                this.CUSLevel = null;
            else
                this.CUSLevel = rs.getString("CUSLevel").trim();

            if( rs.getString("SSFlag") == null )
                this.SSFlag = null;
            else
                this.SSFlag = rs.getString("SSFlag").trim();

            if( rs.getString("RgtTpye") == null )
                this.RgtTpye = null;
            else
                this.RgtTpye = rs.getString("RgtTpye").trim();

            if( rs.getString("TINNO") == null )
                this.TINNO = null;
            else
                this.TINNO = rs.getString("TINNO").trim();

            if( rs.getString("TINFlag") == null )
                this.TINFlag = null;
            else
                this.TINFlag = rs.getString("TINFlag").trim();

            if( rs.getString("NewCustomerFlag") == null )
                this.NewCustomerFlag = null;
            else
                this.NewCustomerFlag = rs.getString("NewCustomerFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPersonSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDPersonSchema getSchema() {
        LDPersonSchema aLDPersonSchema = new LDPersonSchema();
        aLDPersonSchema.setSchema(this);
        return aLDPersonSchema;
    }

    public LDPersonDB getDB() {
        LDPersonDB aDBOper = new LDPersonDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDPerson描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(PersonID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Marriage)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MarriageDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Health)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Stature));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Avoirdupois));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Degree)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CreditGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OthIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OthIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ICNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( JoinCompanyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartWorkDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Salary));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WorkType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PluralityType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( DeathDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SmokeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlacklistFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Proterty)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIPValue)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(License)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LicenseType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SocialInsuNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IdValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HaveMotorcycleLicence)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PartTimeJob)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HealthFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceMark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LastName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CUSLevel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SSFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtTpye)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TINNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TINFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NewCustomerFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDPerson>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            PersonID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER));
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            RgtAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            Marriage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            MarriageDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            Health = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            Stature = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16, SysConst.PACKAGESPILTER))).doubleValue();
            Avoirdupois = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17, SysConst.PACKAGESPILTER))).doubleValue();
            Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            CreditGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            OthIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            OthIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            ICNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            JoinCompanyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER));
            StartWorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            Salary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27, SysConst.PACKAGESPILTER))).doubleValue();
            OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            WorkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            PluralityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            DeathDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER));
            SmokeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            BlacklistFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            Proterty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            VIPValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            License = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            LicenseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            SocialInsuNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            IdValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            HaveMotorcycleLicence = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            PartTimeJob = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            HealthFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            ServiceMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            FirstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
            LastName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
            CUSLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
            SSFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
            RgtTpye = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
            TINNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
            TINFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
            NewCustomerFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDPersonSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PersonID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PersonID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarriageDate()));
        }
        if (FCode.equalsIgnoreCase("Health")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Health));
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stature));
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Avoirdupois));
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
        }
        if (FCode.equalsIgnoreCase("OthIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDType));
        }
        if (FCode.equalsIgnoreCase("OthIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDNo));
        }
        if (FCode.equalsIgnoreCase("ICNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ICNo));
        }
        if (FCode.equalsIgnoreCase("GrpNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartWorkDate()));
        }
        if (FCode.equalsIgnoreCase("Position")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
        }
        if (FCode.equalsIgnoreCase("DeathDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDeathDate()));
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistFlag));
        }
        if (FCode.equalsIgnoreCase("Proterty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Proterty));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIPValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("License")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(License));
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseType));
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuNo));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HaveMotorcycleLicence));
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PartTimeJob));
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthFlag));
        }
        if (FCode.equalsIgnoreCase("ServiceMark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceMark));
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstName));
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastName));
        }
        if (FCode.equalsIgnoreCase("CUSLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUSLevel));
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SSFlag));
        }
        if (FCode.equalsIgnoreCase("RgtTpye")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtTpye));
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINNO));
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINFlag));
        }
        if (FCode.equalsIgnoreCase("NewCustomerFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewCustomerFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PersonID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(NativePlace);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Nationality);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(RgtAddress);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Marriage);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarriageDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Health);
                break;
            case 15:
                strFieldValue = String.valueOf(Stature);
                break;
            case 16:
                strFieldValue = String.valueOf(Avoirdupois);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Degree);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(CreditGrade);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(OthIDType);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(OthIDNo);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ICNo);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(GrpNo);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartWorkDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Position);
                break;
            case 26:
                strFieldValue = String.valueOf(Salary);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(OccupationType);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(OccupationCode);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(WorkType);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(PluralityType);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDeathDate()));
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(SmokeFlag);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(BlacklistFlag);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(Proterty);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(VIPValue);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(GrpName);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(License);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(LicenseType);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(SocialInsuNo);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(IdValiDate);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(HaveMotorcycleLicence);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(PartTimeJob);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(HealthFlag);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(ServiceMark);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(FirstName);
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(LastName);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(CUSLevel);
                break;
            case 55:
                strFieldValue = StrTool.GBKToUnicode(SSFlag);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(RgtTpye);
                break;
            case 57:
                strFieldValue = StrTool.GBKToUnicode(TINNO);
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(TINFlag);
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(NewCustomerFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PersonID")) {
            if( FValue != null && !FValue.equals("")) {
                PersonID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if(FValue != null && !FValue.equals("")) {
                Birthday = fDate.getDate( FValue );
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("MarriageDate")) {
            if(FValue != null && !FValue.equals("")) {
                MarriageDate = fDate.getDate( FValue );
            }
            else
                MarriageDate = null;
        }
        if (FCode.equalsIgnoreCase("Health")) {
            if( FValue != null && !FValue.equals(""))
            {
                Health = FValue.trim();
            }
            else
                Health = null;
        }
        if (FCode.equalsIgnoreCase("Stature")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Stature = d;
            }
        }
        if (FCode.equalsIgnoreCase("Avoirdupois")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Avoirdupois = d;
            }
        }
        if (FCode.equalsIgnoreCase("Degree")) {
            if( FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
                Degree = null;
        }
        if (FCode.equalsIgnoreCase("CreditGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                CreditGrade = FValue.trim();
            }
            else
                CreditGrade = null;
        }
        if (FCode.equalsIgnoreCase("OthIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthIDType = FValue.trim();
            }
            else
                OthIDType = null;
        }
        if (FCode.equalsIgnoreCase("OthIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthIDNo = FValue.trim();
            }
            else
                OthIDNo = null;
        }
        if (FCode.equalsIgnoreCase("ICNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ICNo = FValue.trim();
            }
            else
                ICNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNo = FValue.trim();
            }
            else
                GrpNo = null;
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            if(FValue != null && !FValue.equals("")) {
                JoinCompanyDate = fDate.getDate( FValue );
            }
            else
                JoinCompanyDate = null;
        }
        if (FCode.equalsIgnoreCase("StartWorkDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartWorkDate = fDate.getDate( FValue );
            }
            else
                StartWorkDate = null;
        }
        if (FCode.equalsIgnoreCase("Position")) {
            if( FValue != null && !FValue.equals(""))
            {
                Position = FValue.trim();
            }
            else
                Position = null;
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Salary = d;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("WorkType")) {
            if( FValue != null && !FValue.equals(""))
            {
                WorkType = FValue.trim();
            }
            else
                WorkType = null;
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
                PluralityType = null;
        }
        if (FCode.equalsIgnoreCase("DeathDate")) {
            if(FValue != null && !FValue.equals("")) {
                DeathDate = fDate.getDate( FValue );
            }
            else
                DeathDate = null;
        }
        if (FCode.equalsIgnoreCase("SmokeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SmokeFlag = FValue.trim();
            }
            else
                SmokeFlag = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistFlag = FValue.trim();
            }
            else
                BlacklistFlag = null;
        }
        if (FCode.equalsIgnoreCase("Proterty")) {
            if( FValue != null && !FValue.equals(""))
            {
                Proterty = FValue.trim();
            }
            else
                Proterty = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIPValue = FValue.trim();
            }
            else
                VIPValue = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("License")) {
            if( FValue != null && !FValue.equals(""))
            {
                License = FValue.trim();
            }
            else
                License = null;
        }
        if (FCode.equalsIgnoreCase("LicenseType")) {
            if( FValue != null && !FValue.equals(""))
            {
                LicenseType = FValue.trim();
            }
            else
                LicenseType = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuNo = FValue.trim();
            }
            else
                SocialInsuNo = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("HaveMotorcycleLicence")) {
            if( FValue != null && !FValue.equals(""))
            {
                HaveMotorcycleLicence = FValue.trim();
            }
            else
                HaveMotorcycleLicence = null;
        }
        if (FCode.equalsIgnoreCase("PartTimeJob")) {
            if( FValue != null && !FValue.equals(""))
            {
                PartTimeJob = FValue.trim();
            }
            else
                PartTimeJob = null;
        }
        if (FCode.equalsIgnoreCase("HealthFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthFlag = FValue.trim();
            }
            else
                HealthFlag = null;
        }
        if (FCode.equalsIgnoreCase("ServiceMark")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceMark = FValue.trim();
            }
            else
                ServiceMark = null;
        }
        if (FCode.equalsIgnoreCase("FirstName")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstName = FValue.trim();
            }
            else
                FirstName = null;
        }
        if (FCode.equalsIgnoreCase("LastName")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastName = FValue.trim();
            }
            else
                LastName = null;
        }
        if (FCode.equalsIgnoreCase("CUSLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUSLevel = FValue.trim();
            }
            else
                CUSLevel = null;
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SSFlag = FValue.trim();
            }
            else
                SSFlag = null;
        }
        if (FCode.equalsIgnoreCase("RgtTpye")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtTpye = FValue.trim();
            }
            else
                RgtTpye = null;
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINNO = FValue.trim();
            }
            else
                TINNO = null;
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINFlag = FValue.trim();
            }
            else
                TINFlag = null;
        }
        if (FCode.equalsIgnoreCase("NewCustomerFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                NewCustomerFlag = FValue.trim();
            }
            else
                NewCustomerFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDPersonSchema other = (LDPersonSchema)otherObject;
        return
            PersonID == other.getPersonID()
            && ShardingID.equals(other.getShardingID())
            && CustomerNo.equals(other.getCustomerNo())
            && Name.equals(other.getName())
            && Sex.equals(other.getSex())
            && fDate.getString(Birthday).equals(other.getBirthday())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && Password.equals(other.getPassword())
            && NativePlace.equals(other.getNativePlace())
            && Nationality.equals(other.getNationality())
            && RgtAddress.equals(other.getRgtAddress())
            && Marriage.equals(other.getMarriage())
            && fDate.getString(MarriageDate).equals(other.getMarriageDate())
            && Health.equals(other.getHealth())
            && Stature == other.getStature()
            && Avoirdupois == other.getAvoirdupois()
            && Degree.equals(other.getDegree())
            && CreditGrade.equals(other.getCreditGrade())
            && OthIDType.equals(other.getOthIDType())
            && OthIDNo.equals(other.getOthIDNo())
            && ICNo.equals(other.getICNo())
            && GrpNo.equals(other.getGrpNo())
            && fDate.getString(JoinCompanyDate).equals(other.getJoinCompanyDate())
            && fDate.getString(StartWorkDate).equals(other.getStartWorkDate())
            && Position.equals(other.getPosition())
            && Salary == other.getSalary()
            && OccupationType.equals(other.getOccupationType())
            && OccupationCode.equals(other.getOccupationCode())
            && WorkType.equals(other.getWorkType())
            && PluralityType.equals(other.getPluralityType())
            && fDate.getString(DeathDate).equals(other.getDeathDate())
            && SmokeFlag.equals(other.getSmokeFlag())
            && BlacklistFlag.equals(other.getBlacklistFlag())
            && Proterty.equals(other.getProterty())
            && Remark.equals(other.getRemark())
            && State.equals(other.getState())
            && VIPValue.equals(other.getVIPValue())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && GrpName.equals(other.getGrpName())
            && License.equals(other.getLicense())
            && LicenseType.equals(other.getLicenseType())
            && SocialInsuNo.equals(other.getSocialInsuNo())
            && IdValiDate.equals(other.getIdValiDate())
            && HaveMotorcycleLicence.equals(other.getHaveMotorcycleLicence())
            && PartTimeJob.equals(other.getPartTimeJob())
            && HealthFlag.equals(other.getHealthFlag())
            && ServiceMark.equals(other.getServiceMark())
            && FirstName.equals(other.getFirstName())
            && LastName.equals(other.getLastName())
            && CUSLevel.equals(other.getCUSLevel())
            && SSFlag.equals(other.getSSFlag())
            && RgtTpye.equals(other.getRgtTpye())
            && TINNO.equals(other.getTINNO())
            && TINFlag.equals(other.getTINFlag())
            && NewCustomerFlag.equals(other.getNewCustomerFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PersonID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 2;
        }
        if( strFieldName.equals("Name") ) {
            return 3;
        }
        if( strFieldName.equals("Sex") ) {
            return 4;
        }
        if( strFieldName.equals("Birthday") ) {
            return 5;
        }
        if( strFieldName.equals("IDType") ) {
            return 6;
        }
        if( strFieldName.equals("IDNo") ) {
            return 7;
        }
        if( strFieldName.equals("Password") ) {
            return 8;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 9;
        }
        if( strFieldName.equals("Nationality") ) {
            return 10;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 11;
        }
        if( strFieldName.equals("Marriage") ) {
            return 12;
        }
        if( strFieldName.equals("MarriageDate") ) {
            return 13;
        }
        if( strFieldName.equals("Health") ) {
            return 14;
        }
        if( strFieldName.equals("Stature") ) {
            return 15;
        }
        if( strFieldName.equals("Avoirdupois") ) {
            return 16;
        }
        if( strFieldName.equals("Degree") ) {
            return 17;
        }
        if( strFieldName.equals("CreditGrade") ) {
            return 18;
        }
        if( strFieldName.equals("OthIDType") ) {
            return 19;
        }
        if( strFieldName.equals("OthIDNo") ) {
            return 20;
        }
        if( strFieldName.equals("ICNo") ) {
            return 21;
        }
        if( strFieldName.equals("GrpNo") ) {
            return 22;
        }
        if( strFieldName.equals("JoinCompanyDate") ) {
            return 23;
        }
        if( strFieldName.equals("StartWorkDate") ) {
            return 24;
        }
        if( strFieldName.equals("Position") ) {
            return 25;
        }
        if( strFieldName.equals("Salary") ) {
            return 26;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 27;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 28;
        }
        if( strFieldName.equals("WorkType") ) {
            return 29;
        }
        if( strFieldName.equals("PluralityType") ) {
            return 30;
        }
        if( strFieldName.equals("DeathDate") ) {
            return 31;
        }
        if( strFieldName.equals("SmokeFlag") ) {
            return 32;
        }
        if( strFieldName.equals("BlacklistFlag") ) {
            return 33;
        }
        if( strFieldName.equals("Proterty") ) {
            return 34;
        }
        if( strFieldName.equals("Remark") ) {
            return 35;
        }
        if( strFieldName.equals("State") ) {
            return 36;
        }
        if( strFieldName.equals("VIPValue") ) {
            return 37;
        }
        if( strFieldName.equals("Operator") ) {
            return 38;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 39;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 40;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 41;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 42;
        }
        if( strFieldName.equals("GrpName") ) {
            return 43;
        }
        if( strFieldName.equals("License") ) {
            return 44;
        }
        if( strFieldName.equals("LicenseType") ) {
            return 45;
        }
        if( strFieldName.equals("SocialInsuNo") ) {
            return 46;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 47;
        }
        if( strFieldName.equals("HaveMotorcycleLicence") ) {
            return 48;
        }
        if( strFieldName.equals("PartTimeJob") ) {
            return 49;
        }
        if( strFieldName.equals("HealthFlag") ) {
            return 50;
        }
        if( strFieldName.equals("ServiceMark") ) {
            return 51;
        }
        if( strFieldName.equals("FirstName") ) {
            return 52;
        }
        if( strFieldName.equals("LastName") ) {
            return 53;
        }
        if( strFieldName.equals("CUSLevel") ) {
            return 54;
        }
        if( strFieldName.equals("SSFlag") ) {
            return 55;
        }
        if( strFieldName.equals("RgtTpye") ) {
            return 56;
        }
        if( strFieldName.equals("TINNO") ) {
            return 57;
        }
        if( strFieldName.equals("TINFlag") ) {
            return 58;
        }
        if( strFieldName.equals("NewCustomerFlag") ) {
            return 59;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PersonID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "CustomerNo";
                break;
            case 3:
                strFieldName = "Name";
                break;
            case 4:
                strFieldName = "Sex";
                break;
            case 5:
                strFieldName = "Birthday";
                break;
            case 6:
                strFieldName = "IDType";
                break;
            case 7:
                strFieldName = "IDNo";
                break;
            case 8:
                strFieldName = "Password";
                break;
            case 9:
                strFieldName = "NativePlace";
                break;
            case 10:
                strFieldName = "Nationality";
                break;
            case 11:
                strFieldName = "RgtAddress";
                break;
            case 12:
                strFieldName = "Marriage";
                break;
            case 13:
                strFieldName = "MarriageDate";
                break;
            case 14:
                strFieldName = "Health";
                break;
            case 15:
                strFieldName = "Stature";
                break;
            case 16:
                strFieldName = "Avoirdupois";
                break;
            case 17:
                strFieldName = "Degree";
                break;
            case 18:
                strFieldName = "CreditGrade";
                break;
            case 19:
                strFieldName = "OthIDType";
                break;
            case 20:
                strFieldName = "OthIDNo";
                break;
            case 21:
                strFieldName = "ICNo";
                break;
            case 22:
                strFieldName = "GrpNo";
                break;
            case 23:
                strFieldName = "JoinCompanyDate";
                break;
            case 24:
                strFieldName = "StartWorkDate";
                break;
            case 25:
                strFieldName = "Position";
                break;
            case 26:
                strFieldName = "Salary";
                break;
            case 27:
                strFieldName = "OccupationType";
                break;
            case 28:
                strFieldName = "OccupationCode";
                break;
            case 29:
                strFieldName = "WorkType";
                break;
            case 30:
                strFieldName = "PluralityType";
                break;
            case 31:
                strFieldName = "DeathDate";
                break;
            case 32:
                strFieldName = "SmokeFlag";
                break;
            case 33:
                strFieldName = "BlacklistFlag";
                break;
            case 34:
                strFieldName = "Proterty";
                break;
            case 35:
                strFieldName = "Remark";
                break;
            case 36:
                strFieldName = "State";
                break;
            case 37:
                strFieldName = "VIPValue";
                break;
            case 38:
                strFieldName = "Operator";
                break;
            case 39:
                strFieldName = "MakeDate";
                break;
            case 40:
                strFieldName = "MakeTime";
                break;
            case 41:
                strFieldName = "ModifyDate";
                break;
            case 42:
                strFieldName = "ModifyTime";
                break;
            case 43:
                strFieldName = "GrpName";
                break;
            case 44:
                strFieldName = "License";
                break;
            case 45:
                strFieldName = "LicenseType";
                break;
            case 46:
                strFieldName = "SocialInsuNo";
                break;
            case 47:
                strFieldName = "IdValiDate";
                break;
            case 48:
                strFieldName = "HaveMotorcycleLicence";
                break;
            case 49:
                strFieldName = "PartTimeJob";
                break;
            case 50:
                strFieldName = "HealthFlag";
                break;
            case 51:
                strFieldName = "ServiceMark";
                break;
            case 52:
                strFieldName = "FirstName";
                break;
            case 53:
                strFieldName = "LastName";
                break;
            case 54:
                strFieldName = "CUSLevel";
                break;
            case 55:
                strFieldName = "SSFlag";
                break;
            case 56:
                strFieldName = "RgtTpye";
                break;
            case 57:
                strFieldName = "TINNO";
                break;
            case 58:
                strFieldName = "TINFlag";
                break;
            case 59:
                strFieldName = "NewCustomerFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PERSONID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_DATE;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "MARRIAGEDATE":
                return Schema.TYPE_DATE;
            case "HEALTH":
                return Schema.TYPE_STRING;
            case "STATURE":
                return Schema.TYPE_DOUBLE;
            case "AVOIRDUPOIS":
                return Schema.TYPE_DOUBLE;
            case "DEGREE":
                return Schema.TYPE_STRING;
            case "CREDITGRADE":
                return Schema.TYPE_STRING;
            case "OTHIDTYPE":
                return Schema.TYPE_STRING;
            case "OTHIDNO":
                return Schema.TYPE_STRING;
            case "ICNO":
                return Schema.TYPE_STRING;
            case "GRPNO":
                return Schema.TYPE_STRING;
            case "JOINCOMPANYDATE":
                return Schema.TYPE_DATE;
            case "STARTWORKDATE":
                return Schema.TYPE_DATE;
            case "POSITION":
                return Schema.TYPE_STRING;
            case "SALARY":
                return Schema.TYPE_DOUBLE;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "WORKTYPE":
                return Schema.TYPE_STRING;
            case "PLURALITYTYPE":
                return Schema.TYPE_STRING;
            case "DEATHDATE":
                return Schema.TYPE_DATE;
            case "SMOKEFLAG":
                return Schema.TYPE_STRING;
            case "BLACKLISTFLAG":
                return Schema.TYPE_STRING;
            case "PROTERTY":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "VIPVALUE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "LICENSE":
                return Schema.TYPE_STRING;
            case "LICENSETYPE":
                return Schema.TYPE_STRING;
            case "SOCIALINSUNO":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "HAVEMOTORCYCLELICENCE":
                return Schema.TYPE_STRING;
            case "PARTTIMEJOB":
                return Schema.TYPE_STRING;
            case "HEALTHFLAG":
                return Schema.TYPE_STRING;
            case "SERVICEMARK":
                return Schema.TYPE_STRING;
            case "FIRSTNAME":
                return Schema.TYPE_STRING;
            case "LASTNAME":
                return Schema.TYPE_STRING;
            case "CUSLEVEL":
                return Schema.TYPE_STRING;
            case "SSFLAG":
                return Schema.TYPE_STRING;
            case "RGTTPYE":
                return Schema.TYPE_STRING;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            case "NEWCUSTOMERFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DATE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_DOUBLE;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_DATE;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_DOUBLE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_DATE;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_DATE;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_DATE;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
