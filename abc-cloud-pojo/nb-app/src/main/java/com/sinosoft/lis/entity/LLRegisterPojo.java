/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LLRegisterPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LLRegisterPojo implements Pojo,Serializable {
    // @Field
    /** 个人立案号(个人赔案号) */
    private String RgtNo; 
    /** 案件类型 */
    private String RgtState; 
    /** 申请类型 */
    private String RgtClass; 
    /** 号码类型 */
    private String RgtObj; 
    /** 其他号码 */
    private String RgtObjNo; 
    /** 受理方式 */
    private String RgtType; 
    /** 代理人代码 */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 申请人身份 */
    private String ApplyerType; 
    /** 申请人证件类型 */
    private String IDType; 
    /** 申请人证件号码 */
    private String IDNo; 
    /** 立案人/申请人姓名 */
    private String RgtantName; 
    /** 立案人/申请人性别 */
    private String RgtantSex; 
    /** 立案人/申请人与被保人关系 */
    private String Relation; 
    /** 立案人/申请人地址 */
    private String RgtantAddress; 
    /** 立案人/申请人电话 */
    private String RgtantPhone; 
    /** 立案人/申请人手机 */
    private String RgtantMobile; 
    /** 立案人/申请人电邮 */
    private String Email; 
    /** 立案人/申请人邮政编码 */
    private String PostCode; 
    /** 客户号 */
    private String CustomerNo; 
    /** 单位名称 */
    private String GrpName; 
    /** 立案日期 */
    private String  RgtDate;
    /** 出险地点 */
    private String AccidentSite; 
    /** 出险原因 */
    private String AccidentReason; 
    /** 出险过程和结果 */
    private String AccidentCourse; 
    /** 出险开始日期 */
    private String  AccStartDate;
    /** 出险结束日期 */
    private String  AccidentDate;
    /** 立案撤销原因 */
    private String RgtReason; 
    /** 申请人数 */
    private int AppPeoples; 
    /** 预估申请金额 */
    private double AppAmnt; 
    /** 赔付金领取方式 */
    private String GetMode; 
    /** 赔付金领取间隔 */
    private int GetIntv; 
    /** 保险金领取方式 */
    private String CaseGetMode; 
    /** 回执发送方式 */
    private String ReturnMode; 
    /** 备注 */
    private String Remark; 
    /** 审核人 */
    private String Handler; 
    /** 统一给付标记 */
    private String TogetherFlag; 
    /** 报案标志 */
    private String RptFlag; 
    /** 核算标记 */
    private String CalFlag; 
    /** 核赔标记 */
    private String UWFlag; 
    /** 拒赔标记 */
    private String DeclineFlag; 
    /** 结案标记 */
    private String EndCaseFlag; 
    /** 结案日期 */
    private String  EndCaseDate;
    /** 管理机构 */
    private String MngCom; 
    /** 赔案状态 */
    private String ClmState; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 银行帐户名 */
    private String AccName; 
    /** 经办人 */
    private String Handler1; 
    /** 经办人联系电话 */
    private String Handler1Phone; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 立案结论 */
    private String RgtConclusion; 
    /** 不立案原因 */
    private String NoRgtReason; 
    /** 受托人类型 */
    private String AssigneeType; 
    /** 受托人代码 */
    private String AssigneeCode; 
    /** 受托人姓名 */
    private String AssigneeName; 
    /** 受托人性别 */
    private String AssigneeSex; 
    /** 受托人电话 */
    private String AssigneePhone; 
    /** 受托人地址 */
    private String AssigneeAddr; 
    /** 受托人邮编 */
    private String AssigneeZip; 
    /** 预估金额 */
    private double BeAdjSum; 
    /** 账单录入标记 */
    private String FeeInputFlag; 
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体客户号码 */
    private String AppntNo; 
    /** 投保总人数 */
    private int Peoples2; 
    /** 团体预估金额 */
    private double GrpStandpay; 
    /** 保单险种号 */
    private String RiskCode; 
    /** 收件人 */
    private String Recipients; 
    /** 收件人姓名 */
    private String ReciName; 
    /** 收件人地址 */
    private String ReciAddress; 
    /** 收件人细节 */
    private String ReciDetails; 
    /** 收件人关系 */
    private String ReciRela; 
    /** 收件人电话 */
    private String ReciPhone; 
    /** 收件人手机 */
    private String ReciMobile; 
    /** 收件人邮编 */
    private String ReciZip; 
    /** 收件人性别 */
    private String ReciSex; 
    /** 收件人电邮 */
    private String ReciEmail; 
    /** 立案差错标记 */
    private String ErrorFlag; 
    /** 立案差错原因 */
    private String ErrorDesc; 
    /** 伤残鉴定日期 */
    private String  DisDate;
    /** 住院开始日期 */
    private String  HosStartDate;
    /** 住院结束日期 */
    private String  HosEndDate;
    /** 理赔申请日期 */
    private String  RgtantDate;
    /** 手术编码 */
    private String Operation; 
    /** 警示原因 */
    private String AlarmReason; 


    public static final int FIELDNUM = 89;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getRgtNo() {
        return RgtNo;
    }
    public void setRgtNo(String aRgtNo) {
        RgtNo = aRgtNo;
    }
    public String getRgtState() {
        return RgtState;
    }
    public void setRgtState(String aRgtState) {
        RgtState = aRgtState;
    }
    public String getRgtClass() {
        return RgtClass;
    }
    public void setRgtClass(String aRgtClass) {
        RgtClass = aRgtClass;
    }
    public String getRgtObj() {
        return RgtObj;
    }
    public void setRgtObj(String aRgtObj) {
        RgtObj = aRgtObj;
    }
    public String getRgtObjNo() {
        return RgtObjNo;
    }
    public void setRgtObjNo(String aRgtObjNo) {
        RgtObjNo = aRgtObjNo;
    }
    public String getRgtType() {
        return RgtType;
    }
    public void setRgtType(String aRgtType) {
        RgtType = aRgtType;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getApplyerType() {
        return ApplyerType;
    }
    public void setApplyerType(String aApplyerType) {
        ApplyerType = aApplyerType;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getRgtantName() {
        return RgtantName;
    }
    public void setRgtantName(String aRgtantName) {
        RgtantName = aRgtantName;
    }
    public String getRgtantSex() {
        return RgtantSex;
    }
    public void setRgtantSex(String aRgtantSex) {
        RgtantSex = aRgtantSex;
    }
    public String getRelation() {
        return Relation;
    }
    public void setRelation(String aRelation) {
        Relation = aRelation;
    }
    public String getRgtantAddress() {
        return RgtantAddress;
    }
    public void setRgtantAddress(String aRgtantAddress) {
        RgtantAddress = aRgtantAddress;
    }
    public String getRgtantPhone() {
        return RgtantPhone;
    }
    public void setRgtantPhone(String aRgtantPhone) {
        RgtantPhone = aRgtantPhone;
    }
    public String getRgtantMobile() {
        return RgtantMobile;
    }
    public void setRgtantMobile(String aRgtantMobile) {
        RgtantMobile = aRgtantMobile;
    }
    public String getEmail() {
        return Email;
    }
    public void setEmail(String aEmail) {
        Email = aEmail;
    }
    public String getPostCode() {
        return PostCode;
    }
    public void setPostCode(String aPostCode) {
        PostCode = aPostCode;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getRgtDate() {
        return RgtDate;
    }
    public void setRgtDate(String aRgtDate) {
        RgtDate = aRgtDate;
    }
    public String getAccidentSite() {
        return AccidentSite;
    }
    public void setAccidentSite(String aAccidentSite) {
        AccidentSite = aAccidentSite;
    }
    public String getAccidentReason() {
        return AccidentReason;
    }
    public void setAccidentReason(String aAccidentReason) {
        AccidentReason = aAccidentReason;
    }
    public String getAccidentCourse() {
        return AccidentCourse;
    }
    public void setAccidentCourse(String aAccidentCourse) {
        AccidentCourse = aAccidentCourse;
    }
    public String getAccStartDate() {
        return AccStartDate;
    }
    public void setAccStartDate(String aAccStartDate) {
        AccStartDate = aAccStartDate;
    }
    public String getAccidentDate() {
        return AccidentDate;
    }
    public void setAccidentDate(String aAccidentDate) {
        AccidentDate = aAccidentDate;
    }
    public String getRgtReason() {
        return RgtReason;
    }
    public void setRgtReason(String aRgtReason) {
        RgtReason = aRgtReason;
    }
    public int getAppPeoples() {
        return AppPeoples;
    }
    public void setAppPeoples(int aAppPeoples) {
        AppPeoples = aAppPeoples;
    }
    public void setAppPeoples(String aAppPeoples) {
        if (aAppPeoples != null && !aAppPeoples.equals("")) {
            Integer tInteger = new Integer(aAppPeoples);
            int i = tInteger.intValue();
            AppPeoples = i;
        }
    }

    public double getAppAmnt() {
        return AppAmnt;
    }
    public void setAppAmnt(double aAppAmnt) {
        AppAmnt = aAppAmnt;
    }
    public void setAppAmnt(String aAppAmnt) {
        if (aAppAmnt != null && !aAppAmnt.equals("")) {
            Double tDouble = new Double(aAppAmnt);
            double d = tDouble.doubleValue();
            AppAmnt = d;
        }
    }

    public String getGetMode() {
        return GetMode;
    }
    public void setGetMode(String aGetMode) {
        GetMode = aGetMode;
    }
    public int getGetIntv() {
        return GetIntv;
    }
    public void setGetIntv(int aGetIntv) {
        GetIntv = aGetIntv;
    }
    public void setGetIntv(String aGetIntv) {
        if (aGetIntv != null && !aGetIntv.equals("")) {
            Integer tInteger = new Integer(aGetIntv);
            int i = tInteger.intValue();
            GetIntv = i;
        }
    }

    public String getCaseGetMode() {
        return CaseGetMode;
    }
    public void setCaseGetMode(String aCaseGetMode) {
        CaseGetMode = aCaseGetMode;
    }
    public String getReturnMode() {
        return ReturnMode;
    }
    public void setReturnMode(String aReturnMode) {
        ReturnMode = aReturnMode;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getHandler() {
        return Handler;
    }
    public void setHandler(String aHandler) {
        Handler = aHandler;
    }
    public String getTogetherFlag() {
        return TogetherFlag;
    }
    public void setTogetherFlag(String aTogetherFlag) {
        TogetherFlag = aTogetherFlag;
    }
    public String getRptFlag() {
        return RptFlag;
    }
    public void setRptFlag(String aRptFlag) {
        RptFlag = aRptFlag;
    }
    public String getCalFlag() {
        return CalFlag;
    }
    public void setCalFlag(String aCalFlag) {
        CalFlag = aCalFlag;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getDeclineFlag() {
        return DeclineFlag;
    }
    public void setDeclineFlag(String aDeclineFlag) {
        DeclineFlag = aDeclineFlag;
    }
    public String getEndCaseFlag() {
        return EndCaseFlag;
    }
    public void setEndCaseFlag(String aEndCaseFlag) {
        EndCaseFlag = aEndCaseFlag;
    }
    public String getEndCaseDate() {
        return EndCaseDate;
    }
    public void setEndCaseDate(String aEndCaseDate) {
        EndCaseDate = aEndCaseDate;
    }
    public String getMngCom() {
        return MngCom;
    }
    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }
    public String getClmState() {
        return ClmState;
    }
    public void setClmState(String aClmState) {
        ClmState = aClmState;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getHandler1() {
        return Handler1;
    }
    public void setHandler1(String aHandler1) {
        Handler1 = aHandler1;
    }
    public String getHandler1Phone() {
        return Handler1Phone;
    }
    public void setHandler1Phone(String aHandler1Phone) {
        Handler1Phone = aHandler1Phone;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getRgtConclusion() {
        return RgtConclusion;
    }
    public void setRgtConclusion(String aRgtConclusion) {
        RgtConclusion = aRgtConclusion;
    }
    public String getNoRgtReason() {
        return NoRgtReason;
    }
    public void setNoRgtReason(String aNoRgtReason) {
        NoRgtReason = aNoRgtReason;
    }
    public String getAssigneeType() {
        return AssigneeType;
    }
    public void setAssigneeType(String aAssigneeType) {
        AssigneeType = aAssigneeType;
    }
    public String getAssigneeCode() {
        return AssigneeCode;
    }
    public void setAssigneeCode(String aAssigneeCode) {
        AssigneeCode = aAssigneeCode;
    }
    public String getAssigneeName() {
        return AssigneeName;
    }
    public void setAssigneeName(String aAssigneeName) {
        AssigneeName = aAssigneeName;
    }
    public String getAssigneeSex() {
        return AssigneeSex;
    }
    public void setAssigneeSex(String aAssigneeSex) {
        AssigneeSex = aAssigneeSex;
    }
    public String getAssigneePhone() {
        return AssigneePhone;
    }
    public void setAssigneePhone(String aAssigneePhone) {
        AssigneePhone = aAssigneePhone;
    }
    public String getAssigneeAddr() {
        return AssigneeAddr;
    }
    public void setAssigneeAddr(String aAssigneeAddr) {
        AssigneeAddr = aAssigneeAddr;
    }
    public String getAssigneeZip() {
        return AssigneeZip;
    }
    public void setAssigneeZip(String aAssigneeZip) {
        AssigneeZip = aAssigneeZip;
    }
    public double getBeAdjSum() {
        return BeAdjSum;
    }
    public void setBeAdjSum(double aBeAdjSum) {
        BeAdjSum = aBeAdjSum;
    }
    public void setBeAdjSum(String aBeAdjSum) {
        if (aBeAdjSum != null && !aBeAdjSum.equals("")) {
            Double tDouble = new Double(aBeAdjSum);
            double d = tDouble.doubleValue();
            BeAdjSum = d;
        }
    }

    public String getFeeInputFlag() {
        return FeeInputFlag;
    }
    public void setFeeInputFlag(String aFeeInputFlag) {
        FeeInputFlag = aFeeInputFlag;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public int getPeoples2() {
        return Peoples2;
    }
    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }
    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public double getGrpStandpay() {
        return GrpStandpay;
    }
    public void setGrpStandpay(double aGrpStandpay) {
        GrpStandpay = aGrpStandpay;
    }
    public void setGrpStandpay(String aGrpStandpay) {
        if (aGrpStandpay != null && !aGrpStandpay.equals("")) {
            Double tDouble = new Double(aGrpStandpay);
            double d = tDouble.doubleValue();
            GrpStandpay = d;
        }
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRecipients() {
        return Recipients;
    }
    public void setRecipients(String aRecipients) {
        Recipients = aRecipients;
    }
    public String getReciName() {
        return ReciName;
    }
    public void setReciName(String aReciName) {
        ReciName = aReciName;
    }
    public String getReciAddress() {
        return ReciAddress;
    }
    public void setReciAddress(String aReciAddress) {
        ReciAddress = aReciAddress;
    }
    public String getReciDetails() {
        return ReciDetails;
    }
    public void setReciDetails(String aReciDetails) {
        ReciDetails = aReciDetails;
    }
    public String getReciRela() {
        return ReciRela;
    }
    public void setReciRela(String aReciRela) {
        ReciRela = aReciRela;
    }
    public String getReciPhone() {
        return ReciPhone;
    }
    public void setReciPhone(String aReciPhone) {
        ReciPhone = aReciPhone;
    }
    public String getReciMobile() {
        return ReciMobile;
    }
    public void setReciMobile(String aReciMobile) {
        ReciMobile = aReciMobile;
    }
    public String getReciZip() {
        return ReciZip;
    }
    public void setReciZip(String aReciZip) {
        ReciZip = aReciZip;
    }
    public String getReciSex() {
        return ReciSex;
    }
    public void setReciSex(String aReciSex) {
        ReciSex = aReciSex;
    }
    public String getReciEmail() {
        return ReciEmail;
    }
    public void setReciEmail(String aReciEmail) {
        ReciEmail = aReciEmail;
    }
    public String getErrorFlag() {
        return ErrorFlag;
    }
    public void setErrorFlag(String aErrorFlag) {
        ErrorFlag = aErrorFlag;
    }
    public String getErrorDesc() {
        return ErrorDesc;
    }
    public void setErrorDesc(String aErrorDesc) {
        ErrorDesc = aErrorDesc;
    }
    public String getDisDate() {
        return DisDate;
    }
    public void setDisDate(String aDisDate) {
        DisDate = aDisDate;
    }
    public String getHosStartDate() {
        return HosStartDate;
    }
    public void setHosStartDate(String aHosStartDate) {
        HosStartDate = aHosStartDate;
    }
    public String getHosEndDate() {
        return HosEndDate;
    }
    public void setHosEndDate(String aHosEndDate) {
        HosEndDate = aHosEndDate;
    }
    public String getRgtantDate() {
        return RgtantDate;
    }
    public void setRgtantDate(String aRgtantDate) {
        RgtantDate = aRgtantDate;
    }
    public String getOperation() {
        return Operation;
    }
    public void setOperation(String aOperation) {
        Operation = aOperation;
    }
    public String getAlarmReason() {
        return AlarmReason;
    }
    public void setAlarmReason(String aAlarmReason) {
        AlarmReason = aAlarmReason;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RgtNo") ) {
            return 0;
        }
        if( strFieldName.equals("RgtState") ) {
            return 1;
        }
        if( strFieldName.equals("RgtClass") ) {
            return 2;
        }
        if( strFieldName.equals("RgtObj") ) {
            return 3;
        }
        if( strFieldName.equals("RgtObjNo") ) {
            return 4;
        }
        if( strFieldName.equals("RgtType") ) {
            return 5;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 6;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 7;
        }
        if( strFieldName.equals("ApplyerType") ) {
            return 8;
        }
        if( strFieldName.equals("IDType") ) {
            return 9;
        }
        if( strFieldName.equals("IDNo") ) {
            return 10;
        }
        if( strFieldName.equals("RgtantName") ) {
            return 11;
        }
        if( strFieldName.equals("RgtantSex") ) {
            return 12;
        }
        if( strFieldName.equals("Relation") ) {
            return 13;
        }
        if( strFieldName.equals("RgtantAddress") ) {
            return 14;
        }
        if( strFieldName.equals("RgtantPhone") ) {
            return 15;
        }
        if( strFieldName.equals("RgtantMobile") ) {
            return 16;
        }
        if( strFieldName.equals("Email") ) {
            return 17;
        }
        if( strFieldName.equals("PostCode") ) {
            return 18;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 19;
        }
        if( strFieldName.equals("GrpName") ) {
            return 20;
        }
        if( strFieldName.equals("RgtDate") ) {
            return 21;
        }
        if( strFieldName.equals("AccidentSite") ) {
            return 22;
        }
        if( strFieldName.equals("AccidentReason") ) {
            return 23;
        }
        if( strFieldName.equals("AccidentCourse") ) {
            return 24;
        }
        if( strFieldName.equals("AccStartDate") ) {
            return 25;
        }
        if( strFieldName.equals("AccidentDate") ) {
            return 26;
        }
        if( strFieldName.equals("RgtReason") ) {
            return 27;
        }
        if( strFieldName.equals("AppPeoples") ) {
            return 28;
        }
        if( strFieldName.equals("AppAmnt") ) {
            return 29;
        }
        if( strFieldName.equals("GetMode") ) {
            return 30;
        }
        if( strFieldName.equals("GetIntv") ) {
            return 31;
        }
        if( strFieldName.equals("CaseGetMode") ) {
            return 32;
        }
        if( strFieldName.equals("ReturnMode") ) {
            return 33;
        }
        if( strFieldName.equals("Remark") ) {
            return 34;
        }
        if( strFieldName.equals("Handler") ) {
            return 35;
        }
        if( strFieldName.equals("TogetherFlag") ) {
            return 36;
        }
        if( strFieldName.equals("RptFlag") ) {
            return 37;
        }
        if( strFieldName.equals("CalFlag") ) {
            return 38;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 39;
        }
        if( strFieldName.equals("DeclineFlag") ) {
            return 40;
        }
        if( strFieldName.equals("EndCaseFlag") ) {
            return 41;
        }
        if( strFieldName.equals("EndCaseDate") ) {
            return 42;
        }
        if( strFieldName.equals("MngCom") ) {
            return 43;
        }
        if( strFieldName.equals("ClmState") ) {
            return 44;
        }
        if( strFieldName.equals("BankCode") ) {
            return 45;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 46;
        }
        if( strFieldName.equals("AccName") ) {
            return 47;
        }
        if( strFieldName.equals("Handler1") ) {
            return 48;
        }
        if( strFieldName.equals("Handler1Phone") ) {
            return 49;
        }
        if( strFieldName.equals("Operator") ) {
            return 50;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 51;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 52;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 53;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 54;
        }
        if( strFieldName.equals("RgtConclusion") ) {
            return 55;
        }
        if( strFieldName.equals("NoRgtReason") ) {
            return 56;
        }
        if( strFieldName.equals("AssigneeType") ) {
            return 57;
        }
        if( strFieldName.equals("AssigneeCode") ) {
            return 58;
        }
        if( strFieldName.equals("AssigneeName") ) {
            return 59;
        }
        if( strFieldName.equals("AssigneeSex") ) {
            return 60;
        }
        if( strFieldName.equals("AssigneePhone") ) {
            return 61;
        }
        if( strFieldName.equals("AssigneeAddr") ) {
            return 62;
        }
        if( strFieldName.equals("AssigneeZip") ) {
            return 63;
        }
        if( strFieldName.equals("BeAdjSum") ) {
            return 64;
        }
        if( strFieldName.equals("FeeInputFlag") ) {
            return 65;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 66;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 67;
        }
        if( strFieldName.equals("Peoples2") ) {
            return 68;
        }
        if( strFieldName.equals("GrpStandpay") ) {
            return 69;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 70;
        }
        if( strFieldName.equals("Recipients") ) {
            return 71;
        }
        if( strFieldName.equals("ReciName") ) {
            return 72;
        }
        if( strFieldName.equals("ReciAddress") ) {
            return 73;
        }
        if( strFieldName.equals("ReciDetails") ) {
            return 74;
        }
        if( strFieldName.equals("ReciRela") ) {
            return 75;
        }
        if( strFieldName.equals("ReciPhone") ) {
            return 76;
        }
        if( strFieldName.equals("ReciMobile") ) {
            return 77;
        }
        if( strFieldName.equals("ReciZip") ) {
            return 78;
        }
        if( strFieldName.equals("ReciSex") ) {
            return 79;
        }
        if( strFieldName.equals("ReciEmail") ) {
            return 80;
        }
        if( strFieldName.equals("ErrorFlag") ) {
            return 81;
        }
        if( strFieldName.equals("ErrorDesc") ) {
            return 82;
        }
        if( strFieldName.equals("DisDate") ) {
            return 83;
        }
        if( strFieldName.equals("HosStartDate") ) {
            return 84;
        }
        if( strFieldName.equals("HosEndDate") ) {
            return 85;
        }
        if( strFieldName.equals("RgtantDate") ) {
            return 86;
        }
        if( strFieldName.equals("Operation") ) {
            return 87;
        }
        if( strFieldName.equals("AlarmReason") ) {
            return 88;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RgtNo";
                break;
            case 1:
                strFieldName = "RgtState";
                break;
            case 2:
                strFieldName = "RgtClass";
                break;
            case 3:
                strFieldName = "RgtObj";
                break;
            case 4:
                strFieldName = "RgtObjNo";
                break;
            case 5:
                strFieldName = "RgtType";
                break;
            case 6:
                strFieldName = "AgentCode";
                break;
            case 7:
                strFieldName = "AgentGroup";
                break;
            case 8:
                strFieldName = "ApplyerType";
                break;
            case 9:
                strFieldName = "IDType";
                break;
            case 10:
                strFieldName = "IDNo";
                break;
            case 11:
                strFieldName = "RgtantName";
                break;
            case 12:
                strFieldName = "RgtantSex";
                break;
            case 13:
                strFieldName = "Relation";
                break;
            case 14:
                strFieldName = "RgtantAddress";
                break;
            case 15:
                strFieldName = "RgtantPhone";
                break;
            case 16:
                strFieldName = "RgtantMobile";
                break;
            case 17:
                strFieldName = "Email";
                break;
            case 18:
                strFieldName = "PostCode";
                break;
            case 19:
                strFieldName = "CustomerNo";
                break;
            case 20:
                strFieldName = "GrpName";
                break;
            case 21:
                strFieldName = "RgtDate";
                break;
            case 22:
                strFieldName = "AccidentSite";
                break;
            case 23:
                strFieldName = "AccidentReason";
                break;
            case 24:
                strFieldName = "AccidentCourse";
                break;
            case 25:
                strFieldName = "AccStartDate";
                break;
            case 26:
                strFieldName = "AccidentDate";
                break;
            case 27:
                strFieldName = "RgtReason";
                break;
            case 28:
                strFieldName = "AppPeoples";
                break;
            case 29:
                strFieldName = "AppAmnt";
                break;
            case 30:
                strFieldName = "GetMode";
                break;
            case 31:
                strFieldName = "GetIntv";
                break;
            case 32:
                strFieldName = "CaseGetMode";
                break;
            case 33:
                strFieldName = "ReturnMode";
                break;
            case 34:
                strFieldName = "Remark";
                break;
            case 35:
                strFieldName = "Handler";
                break;
            case 36:
                strFieldName = "TogetherFlag";
                break;
            case 37:
                strFieldName = "RptFlag";
                break;
            case 38:
                strFieldName = "CalFlag";
                break;
            case 39:
                strFieldName = "UWFlag";
                break;
            case 40:
                strFieldName = "DeclineFlag";
                break;
            case 41:
                strFieldName = "EndCaseFlag";
                break;
            case 42:
                strFieldName = "EndCaseDate";
                break;
            case 43:
                strFieldName = "MngCom";
                break;
            case 44:
                strFieldName = "ClmState";
                break;
            case 45:
                strFieldName = "BankCode";
                break;
            case 46:
                strFieldName = "BankAccNo";
                break;
            case 47:
                strFieldName = "AccName";
                break;
            case 48:
                strFieldName = "Handler1";
                break;
            case 49:
                strFieldName = "Handler1Phone";
                break;
            case 50:
                strFieldName = "Operator";
                break;
            case 51:
                strFieldName = "MakeDate";
                break;
            case 52:
                strFieldName = "MakeTime";
                break;
            case 53:
                strFieldName = "ModifyDate";
                break;
            case 54:
                strFieldName = "ModifyTime";
                break;
            case 55:
                strFieldName = "RgtConclusion";
                break;
            case 56:
                strFieldName = "NoRgtReason";
                break;
            case 57:
                strFieldName = "AssigneeType";
                break;
            case 58:
                strFieldName = "AssigneeCode";
                break;
            case 59:
                strFieldName = "AssigneeName";
                break;
            case 60:
                strFieldName = "AssigneeSex";
                break;
            case 61:
                strFieldName = "AssigneePhone";
                break;
            case 62:
                strFieldName = "AssigneeAddr";
                break;
            case 63:
                strFieldName = "AssigneeZip";
                break;
            case 64:
                strFieldName = "BeAdjSum";
                break;
            case 65:
                strFieldName = "FeeInputFlag";
                break;
            case 66:
                strFieldName = "GrpContNo";
                break;
            case 67:
                strFieldName = "AppntNo";
                break;
            case 68:
                strFieldName = "Peoples2";
                break;
            case 69:
                strFieldName = "GrpStandpay";
                break;
            case 70:
                strFieldName = "RiskCode";
                break;
            case 71:
                strFieldName = "Recipients";
                break;
            case 72:
                strFieldName = "ReciName";
                break;
            case 73:
                strFieldName = "ReciAddress";
                break;
            case 74:
                strFieldName = "ReciDetails";
                break;
            case 75:
                strFieldName = "ReciRela";
                break;
            case 76:
                strFieldName = "ReciPhone";
                break;
            case 77:
                strFieldName = "ReciMobile";
                break;
            case 78:
                strFieldName = "ReciZip";
                break;
            case 79:
                strFieldName = "ReciSex";
                break;
            case 80:
                strFieldName = "ReciEmail";
                break;
            case 81:
                strFieldName = "ErrorFlag";
                break;
            case 82:
                strFieldName = "ErrorDesc";
                break;
            case 83:
                strFieldName = "DisDate";
                break;
            case 84:
                strFieldName = "HosStartDate";
                break;
            case 85:
                strFieldName = "HosEndDate";
                break;
            case 86:
                strFieldName = "RgtantDate";
                break;
            case 87:
                strFieldName = "Operation";
                break;
            case 88:
                strFieldName = "AlarmReason";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RGTNO":
                return Schema.TYPE_STRING;
            case "RGTSTATE":
                return Schema.TYPE_STRING;
            case "RGTCLASS":
                return Schema.TYPE_STRING;
            case "RGTOBJ":
                return Schema.TYPE_STRING;
            case "RGTOBJNO":
                return Schema.TYPE_STRING;
            case "RGTTYPE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "APPLYERTYPE":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "RGTANTNAME":
                return Schema.TYPE_STRING;
            case "RGTANTSEX":
                return Schema.TYPE_STRING;
            case "RELATION":
                return Schema.TYPE_STRING;
            case "RGTANTADDRESS":
                return Schema.TYPE_STRING;
            case "RGTANTPHONE":
                return Schema.TYPE_STRING;
            case "RGTANTMOBILE":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "POSTCODE":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "RGTDATE":
                return Schema.TYPE_STRING;
            case "ACCIDENTSITE":
                return Schema.TYPE_STRING;
            case "ACCIDENTREASON":
                return Schema.TYPE_STRING;
            case "ACCIDENTCOURSE":
                return Schema.TYPE_STRING;
            case "ACCSTARTDATE":
                return Schema.TYPE_STRING;
            case "ACCIDENTDATE":
                return Schema.TYPE_STRING;
            case "RGTREASON":
                return Schema.TYPE_STRING;
            case "APPPEOPLES":
                return Schema.TYPE_INT;
            case "APPAMNT":
                return Schema.TYPE_DOUBLE;
            case "GETMODE":
                return Schema.TYPE_STRING;
            case "GETINTV":
                return Schema.TYPE_INT;
            case "CASEGETMODE":
                return Schema.TYPE_STRING;
            case "RETURNMODE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "HANDLER":
                return Schema.TYPE_STRING;
            case "TOGETHERFLAG":
                return Schema.TYPE_STRING;
            case "RPTFLAG":
                return Schema.TYPE_STRING;
            case "CALFLAG":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "DECLINEFLAG":
                return Schema.TYPE_STRING;
            case "ENDCASEFLAG":
                return Schema.TYPE_STRING;
            case "ENDCASEDATE":
                return Schema.TYPE_STRING;
            case "MNGCOM":
                return Schema.TYPE_STRING;
            case "CLMSTATE":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "HANDLER1":
                return Schema.TYPE_STRING;
            case "HANDLER1PHONE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "RGTCONCLUSION":
                return Schema.TYPE_STRING;
            case "NORGTREASON":
                return Schema.TYPE_STRING;
            case "ASSIGNEETYPE":
                return Schema.TYPE_STRING;
            case "ASSIGNEECODE":
                return Schema.TYPE_STRING;
            case "ASSIGNEENAME":
                return Schema.TYPE_STRING;
            case "ASSIGNEESEX":
                return Schema.TYPE_STRING;
            case "ASSIGNEEPHONE":
                return Schema.TYPE_STRING;
            case "ASSIGNEEADDR":
                return Schema.TYPE_STRING;
            case "ASSIGNEEZIP":
                return Schema.TYPE_STRING;
            case "BEADJSUM":
                return Schema.TYPE_DOUBLE;
            case "FEEINPUTFLAG":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "PEOPLES2":
                return Schema.TYPE_INT;
            case "GRPSTANDPAY":
                return Schema.TYPE_DOUBLE;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RECIPIENTS":
                return Schema.TYPE_STRING;
            case "RECINAME":
                return Schema.TYPE_STRING;
            case "RECIADDRESS":
                return Schema.TYPE_STRING;
            case "RECIDETAILS":
                return Schema.TYPE_STRING;
            case "RECIRELA":
                return Schema.TYPE_STRING;
            case "RECIPHONE":
                return Schema.TYPE_STRING;
            case "RECIMOBILE":
                return Schema.TYPE_STRING;
            case "RECIZIP":
                return Schema.TYPE_STRING;
            case "RECISEX":
                return Schema.TYPE_STRING;
            case "RECIEMAIL":
                return Schema.TYPE_STRING;
            case "ERRORFLAG":
                return Schema.TYPE_STRING;
            case "ERRORDESC":
                return Schema.TYPE_STRING;
            case "DISDATE":
                return Schema.TYPE_STRING;
            case "HOSSTARTDATE":
                return Schema.TYPE_STRING;
            case "HOSENDDATE":
                return Schema.TYPE_STRING;
            case "RGTANTDATE":
                return Schema.TYPE_STRING;
            case "OPERATION":
                return Schema.TYPE_STRING;
            case "ALARMREASON":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_INT;
            case 29:
                return Schema.TYPE_DOUBLE;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_INT;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_DOUBLE;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_INT;
            case 69:
                return Schema.TYPE_DOUBLE;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_STRING;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_STRING;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_STRING;
            case 80:
                return Schema.TYPE_STRING;
            case 81:
                return Schema.TYPE_STRING;
            case 82:
                return Schema.TYPE_STRING;
            case 83:
                return Schema.TYPE_STRING;
            case 84:
                return Schema.TYPE_STRING;
            case 85:
                return Schema.TYPE_STRING;
            case 86:
                return Schema.TYPE_STRING;
            case 87:
                return Schema.TYPE_STRING;
            case 88:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RgtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
        }
        if (FCode.equalsIgnoreCase("RgtState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtState));
        }
        if (FCode.equalsIgnoreCase("RgtClass")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtClass));
        }
        if (FCode.equalsIgnoreCase("RgtObj")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtObj));
        }
        if (FCode.equalsIgnoreCase("RgtObjNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtObjNo));
        }
        if (FCode.equalsIgnoreCase("RgtType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtType));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("ApplyerType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyerType));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("RgtantName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantName));
        }
        if (FCode.equalsIgnoreCase("RgtantSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantSex));
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
        }
        if (FCode.equalsIgnoreCase("RgtantAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantAddress));
        }
        if (FCode.equalsIgnoreCase("RgtantPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantPhone));
        }
        if (FCode.equalsIgnoreCase("RgtantMobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantMobile));
        }
        if (FCode.equalsIgnoreCase("Email")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
        }
        if (FCode.equalsIgnoreCase("PostCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostCode));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("RgtDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtDate));
        }
        if (FCode.equalsIgnoreCase("AccidentSite")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentSite));
        }
        if (FCode.equalsIgnoreCase("AccidentReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentReason));
        }
        if (FCode.equalsIgnoreCase("AccidentCourse")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentCourse));
        }
        if (FCode.equalsIgnoreCase("AccStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccStartDate));
        }
        if (FCode.equalsIgnoreCase("AccidentDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentDate));
        }
        if (FCode.equalsIgnoreCase("RgtReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtReason));
        }
        if (FCode.equalsIgnoreCase("AppPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppPeoples));
        }
        if (FCode.equalsIgnoreCase("AppAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppAmnt));
        }
        if (FCode.equalsIgnoreCase("GetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMode));
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetIntv));
        }
        if (FCode.equalsIgnoreCase("CaseGetMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseGetMode));
        }
        if (FCode.equalsIgnoreCase("ReturnMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnMode));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
        }
        if (FCode.equalsIgnoreCase("TogetherFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TogetherFlag));
        }
        if (FCode.equalsIgnoreCase("RptFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptFlag));
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("DeclineFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeclineFlag));
        }
        if (FCode.equalsIgnoreCase("EndCaseFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndCaseFlag));
        }
        if (FCode.equalsIgnoreCase("EndCaseDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndCaseDate));
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equalsIgnoreCase("ClmState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmState));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("Handler1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler1));
        }
        if (FCode.equalsIgnoreCase("Handler1Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler1Phone));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("RgtConclusion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtConclusion));
        }
        if (FCode.equalsIgnoreCase("NoRgtReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NoRgtReason));
        }
        if (FCode.equalsIgnoreCase("AssigneeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssigneeType));
        }
        if (FCode.equalsIgnoreCase("AssigneeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssigneeCode));
        }
        if (FCode.equalsIgnoreCase("AssigneeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssigneeName));
        }
        if (FCode.equalsIgnoreCase("AssigneeSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssigneeSex));
        }
        if (FCode.equalsIgnoreCase("AssigneePhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssigneePhone));
        }
        if (FCode.equalsIgnoreCase("AssigneeAddr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssigneeAddr));
        }
        if (FCode.equalsIgnoreCase("AssigneeZip")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssigneeZip));
        }
        if (FCode.equalsIgnoreCase("BeAdjSum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BeAdjSum));
        }
        if (FCode.equalsIgnoreCase("FeeInputFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeInputFlag));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equalsIgnoreCase("GrpStandpay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpStandpay));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("Recipients")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Recipients));
        }
        if (FCode.equalsIgnoreCase("ReciName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciName));
        }
        if (FCode.equalsIgnoreCase("ReciAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciAddress));
        }
        if (FCode.equalsIgnoreCase("ReciDetails")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciDetails));
        }
        if (FCode.equalsIgnoreCase("ReciRela")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciRela));
        }
        if (FCode.equalsIgnoreCase("ReciPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciPhone));
        }
        if (FCode.equalsIgnoreCase("ReciMobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciMobile));
        }
        if (FCode.equalsIgnoreCase("ReciZip")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciZip));
        }
        if (FCode.equalsIgnoreCase("ReciSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciSex));
        }
        if (FCode.equalsIgnoreCase("ReciEmail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReciEmail));
        }
        if (FCode.equalsIgnoreCase("ErrorFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorFlag));
        }
        if (FCode.equalsIgnoreCase("ErrorDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorDesc));
        }
        if (FCode.equalsIgnoreCase("DisDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisDate));
        }
        if (FCode.equalsIgnoreCase("HosStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HosStartDate));
        }
        if (FCode.equalsIgnoreCase("HosEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HosEndDate));
        }
        if (FCode.equalsIgnoreCase("RgtantDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantDate));
        }
        if (FCode.equalsIgnoreCase("Operation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operation));
        }
        if (FCode.equalsIgnoreCase("AlarmReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AlarmReason));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RgtNo);
                break;
            case 1:
                strFieldValue = String.valueOf(RgtState);
                break;
            case 2:
                strFieldValue = String.valueOf(RgtClass);
                break;
            case 3:
                strFieldValue = String.valueOf(RgtObj);
                break;
            case 4:
                strFieldValue = String.valueOf(RgtObjNo);
                break;
            case 5:
                strFieldValue = String.valueOf(RgtType);
                break;
            case 6:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 7:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 8:
                strFieldValue = String.valueOf(ApplyerType);
                break;
            case 9:
                strFieldValue = String.valueOf(IDType);
                break;
            case 10:
                strFieldValue = String.valueOf(IDNo);
                break;
            case 11:
                strFieldValue = String.valueOf(RgtantName);
                break;
            case 12:
                strFieldValue = String.valueOf(RgtantSex);
                break;
            case 13:
                strFieldValue = String.valueOf(Relation);
                break;
            case 14:
                strFieldValue = String.valueOf(RgtantAddress);
                break;
            case 15:
                strFieldValue = String.valueOf(RgtantPhone);
                break;
            case 16:
                strFieldValue = String.valueOf(RgtantMobile);
                break;
            case 17:
                strFieldValue = String.valueOf(Email);
                break;
            case 18:
                strFieldValue = String.valueOf(PostCode);
                break;
            case 19:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 20:
                strFieldValue = String.valueOf(GrpName);
                break;
            case 21:
                strFieldValue = String.valueOf(RgtDate);
                break;
            case 22:
                strFieldValue = String.valueOf(AccidentSite);
                break;
            case 23:
                strFieldValue = String.valueOf(AccidentReason);
                break;
            case 24:
                strFieldValue = String.valueOf(AccidentCourse);
                break;
            case 25:
                strFieldValue = String.valueOf(AccStartDate);
                break;
            case 26:
                strFieldValue = String.valueOf(AccidentDate);
                break;
            case 27:
                strFieldValue = String.valueOf(RgtReason);
                break;
            case 28:
                strFieldValue = String.valueOf(AppPeoples);
                break;
            case 29:
                strFieldValue = String.valueOf(AppAmnt);
                break;
            case 30:
                strFieldValue = String.valueOf(GetMode);
                break;
            case 31:
                strFieldValue = String.valueOf(GetIntv);
                break;
            case 32:
                strFieldValue = String.valueOf(CaseGetMode);
                break;
            case 33:
                strFieldValue = String.valueOf(ReturnMode);
                break;
            case 34:
                strFieldValue = String.valueOf(Remark);
                break;
            case 35:
                strFieldValue = String.valueOf(Handler);
                break;
            case 36:
                strFieldValue = String.valueOf(TogetherFlag);
                break;
            case 37:
                strFieldValue = String.valueOf(RptFlag);
                break;
            case 38:
                strFieldValue = String.valueOf(CalFlag);
                break;
            case 39:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 40:
                strFieldValue = String.valueOf(DeclineFlag);
                break;
            case 41:
                strFieldValue = String.valueOf(EndCaseFlag);
                break;
            case 42:
                strFieldValue = String.valueOf(EndCaseDate);
                break;
            case 43:
                strFieldValue = String.valueOf(MngCom);
                break;
            case 44:
                strFieldValue = String.valueOf(ClmState);
                break;
            case 45:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 46:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 47:
                strFieldValue = String.valueOf(AccName);
                break;
            case 48:
                strFieldValue = String.valueOf(Handler1);
                break;
            case 49:
                strFieldValue = String.valueOf(Handler1Phone);
                break;
            case 50:
                strFieldValue = String.valueOf(Operator);
                break;
            case 51:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 52:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 53:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 54:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 55:
                strFieldValue = String.valueOf(RgtConclusion);
                break;
            case 56:
                strFieldValue = String.valueOf(NoRgtReason);
                break;
            case 57:
                strFieldValue = String.valueOf(AssigneeType);
                break;
            case 58:
                strFieldValue = String.valueOf(AssigneeCode);
                break;
            case 59:
                strFieldValue = String.valueOf(AssigneeName);
                break;
            case 60:
                strFieldValue = String.valueOf(AssigneeSex);
                break;
            case 61:
                strFieldValue = String.valueOf(AssigneePhone);
                break;
            case 62:
                strFieldValue = String.valueOf(AssigneeAddr);
                break;
            case 63:
                strFieldValue = String.valueOf(AssigneeZip);
                break;
            case 64:
                strFieldValue = String.valueOf(BeAdjSum);
                break;
            case 65:
                strFieldValue = String.valueOf(FeeInputFlag);
                break;
            case 66:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 67:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 68:
                strFieldValue = String.valueOf(Peoples2);
                break;
            case 69:
                strFieldValue = String.valueOf(GrpStandpay);
                break;
            case 70:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 71:
                strFieldValue = String.valueOf(Recipients);
                break;
            case 72:
                strFieldValue = String.valueOf(ReciName);
                break;
            case 73:
                strFieldValue = String.valueOf(ReciAddress);
                break;
            case 74:
                strFieldValue = String.valueOf(ReciDetails);
                break;
            case 75:
                strFieldValue = String.valueOf(ReciRela);
                break;
            case 76:
                strFieldValue = String.valueOf(ReciPhone);
                break;
            case 77:
                strFieldValue = String.valueOf(ReciMobile);
                break;
            case 78:
                strFieldValue = String.valueOf(ReciZip);
                break;
            case 79:
                strFieldValue = String.valueOf(ReciSex);
                break;
            case 80:
                strFieldValue = String.valueOf(ReciEmail);
                break;
            case 81:
                strFieldValue = String.valueOf(ErrorFlag);
                break;
            case 82:
                strFieldValue = String.valueOf(ErrorDesc);
                break;
            case 83:
                strFieldValue = String.valueOf(DisDate);
                break;
            case 84:
                strFieldValue = String.valueOf(HosStartDate);
                break;
            case 85:
                strFieldValue = String.valueOf(HosEndDate);
                break;
            case 86:
                strFieldValue = String.valueOf(RgtantDate);
                break;
            case 87:
                strFieldValue = String.valueOf(Operation);
                break;
            case 88:
                strFieldValue = String.valueOf(AlarmReason);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RgtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtNo = FValue.trim();
            }
            else
                RgtNo = null;
        }
        if (FCode.equalsIgnoreCase("RgtState")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtState = FValue.trim();
            }
            else
                RgtState = null;
        }
        if (FCode.equalsIgnoreCase("RgtClass")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtClass = FValue.trim();
            }
            else
                RgtClass = null;
        }
        if (FCode.equalsIgnoreCase("RgtObj")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtObj = FValue.trim();
            }
            else
                RgtObj = null;
        }
        if (FCode.equalsIgnoreCase("RgtObjNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtObjNo = FValue.trim();
            }
            else
                RgtObjNo = null;
        }
        if (FCode.equalsIgnoreCase("RgtType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtType = FValue.trim();
            }
            else
                RgtType = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("ApplyerType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyerType = FValue.trim();
            }
            else
                ApplyerType = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("RgtantName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtantName = FValue.trim();
            }
            else
                RgtantName = null;
        }
        if (FCode.equalsIgnoreCase("RgtantSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtantSex = FValue.trim();
            }
            else
                RgtantSex = null;
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Relation = FValue.trim();
            }
            else
                Relation = null;
        }
        if (FCode.equalsIgnoreCase("RgtantAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtantAddress = FValue.trim();
            }
            else
                RgtantAddress = null;
        }
        if (FCode.equalsIgnoreCase("RgtantPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtantPhone = FValue.trim();
            }
            else
                RgtantPhone = null;
        }
        if (FCode.equalsIgnoreCase("RgtantMobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtantMobile = FValue.trim();
            }
            else
                RgtantMobile = null;
        }
        if (FCode.equalsIgnoreCase("Email")) {
            if( FValue != null && !FValue.equals(""))
            {
                Email = FValue.trim();
            }
            else
                Email = null;
        }
        if (FCode.equalsIgnoreCase("PostCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostCode = FValue.trim();
            }
            else
                PostCode = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("RgtDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtDate = FValue.trim();
            }
            else
                RgtDate = null;
        }
        if (FCode.equalsIgnoreCase("AccidentSite")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentSite = FValue.trim();
            }
            else
                AccidentSite = null;
        }
        if (FCode.equalsIgnoreCase("AccidentReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentReason = FValue.trim();
            }
            else
                AccidentReason = null;
        }
        if (FCode.equalsIgnoreCase("AccidentCourse")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentCourse = FValue.trim();
            }
            else
                AccidentCourse = null;
        }
        if (FCode.equalsIgnoreCase("AccStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccStartDate = FValue.trim();
            }
            else
                AccStartDate = null;
        }
        if (FCode.equalsIgnoreCase("AccidentDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccidentDate = FValue.trim();
            }
            else
                AccidentDate = null;
        }
        if (FCode.equalsIgnoreCase("RgtReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtReason = FValue.trim();
            }
            else
                RgtReason = null;
        }
        if (FCode.equalsIgnoreCase("AppPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AppPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("AppAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AppAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetMode = FValue.trim();
            }
            else
                GetMode = null;
        }
        if (FCode.equalsIgnoreCase("GetIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                GetIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("CaseGetMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CaseGetMode = FValue.trim();
            }
            else
                CaseGetMode = null;
        }
        if (FCode.equalsIgnoreCase("ReturnMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReturnMode = FValue.trim();
            }
            else
                ReturnMode = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            if( FValue != null && !FValue.equals(""))
            {
                Handler = FValue.trim();
            }
            else
                Handler = null;
        }
        if (FCode.equalsIgnoreCase("TogetherFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TogetherFlag = FValue.trim();
            }
            else
                TogetherFlag = null;
        }
        if (FCode.equalsIgnoreCase("RptFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RptFlag = FValue.trim();
            }
            else
                RptFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFlag = FValue.trim();
            }
            else
                CalFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("DeclineFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DeclineFlag = FValue.trim();
            }
            else
                DeclineFlag = null;
        }
        if (FCode.equalsIgnoreCase("EndCaseFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndCaseFlag = FValue.trim();
            }
            else
                EndCaseFlag = null;
        }
        if (FCode.equalsIgnoreCase("EndCaseDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndCaseDate = FValue.trim();
            }
            else
                EndCaseDate = null;
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
                MngCom = null;
        }
        if (FCode.equalsIgnoreCase("ClmState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmState = FValue.trim();
            }
            else
                ClmState = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("Handler1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Handler1 = FValue.trim();
            }
            else
                Handler1 = null;
        }
        if (FCode.equalsIgnoreCase("Handler1Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Handler1Phone = FValue.trim();
            }
            else
                Handler1Phone = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("RgtConclusion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtConclusion = FValue.trim();
            }
            else
                RgtConclusion = null;
        }
        if (FCode.equalsIgnoreCase("NoRgtReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                NoRgtReason = FValue.trim();
            }
            else
                NoRgtReason = null;
        }
        if (FCode.equalsIgnoreCase("AssigneeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssigneeType = FValue.trim();
            }
            else
                AssigneeType = null;
        }
        if (FCode.equalsIgnoreCase("AssigneeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssigneeCode = FValue.trim();
            }
            else
                AssigneeCode = null;
        }
        if (FCode.equalsIgnoreCase("AssigneeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssigneeName = FValue.trim();
            }
            else
                AssigneeName = null;
        }
        if (FCode.equalsIgnoreCase("AssigneeSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssigneeSex = FValue.trim();
            }
            else
                AssigneeSex = null;
        }
        if (FCode.equalsIgnoreCase("AssigneePhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssigneePhone = FValue.trim();
            }
            else
                AssigneePhone = null;
        }
        if (FCode.equalsIgnoreCase("AssigneeAddr")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssigneeAddr = FValue.trim();
            }
            else
                AssigneeAddr = null;
        }
        if (FCode.equalsIgnoreCase("AssigneeZip")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssigneeZip = FValue.trim();
            }
            else
                AssigneeZip = null;
        }
        if (FCode.equalsIgnoreCase("BeAdjSum")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                BeAdjSum = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeInputFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FeeInputFlag = FValue.trim();
            }
            else
                FeeInputFlag = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrpStandpay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GrpStandpay = d;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("Recipients")) {
            if( FValue != null && !FValue.equals(""))
            {
                Recipients = FValue.trim();
            }
            else
                Recipients = null;
        }
        if (FCode.equalsIgnoreCase("ReciName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciName = FValue.trim();
            }
            else
                ReciName = null;
        }
        if (FCode.equalsIgnoreCase("ReciAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciAddress = FValue.trim();
            }
            else
                ReciAddress = null;
        }
        if (FCode.equalsIgnoreCase("ReciDetails")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciDetails = FValue.trim();
            }
            else
                ReciDetails = null;
        }
        if (FCode.equalsIgnoreCase("ReciRela")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciRela = FValue.trim();
            }
            else
                ReciRela = null;
        }
        if (FCode.equalsIgnoreCase("ReciPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciPhone = FValue.trim();
            }
            else
                ReciPhone = null;
        }
        if (FCode.equalsIgnoreCase("ReciMobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciMobile = FValue.trim();
            }
            else
                ReciMobile = null;
        }
        if (FCode.equalsIgnoreCase("ReciZip")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciZip = FValue.trim();
            }
            else
                ReciZip = null;
        }
        if (FCode.equalsIgnoreCase("ReciSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciSex = FValue.trim();
            }
            else
                ReciSex = null;
        }
        if (FCode.equalsIgnoreCase("ReciEmail")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReciEmail = FValue.trim();
            }
            else
                ReciEmail = null;
        }
        if (FCode.equalsIgnoreCase("ErrorFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ErrorFlag = FValue.trim();
            }
            else
                ErrorFlag = null;
        }
        if (FCode.equalsIgnoreCase("ErrorDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                ErrorDesc = FValue.trim();
            }
            else
                ErrorDesc = null;
        }
        if (FCode.equalsIgnoreCase("DisDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisDate = FValue.trim();
            }
            else
                DisDate = null;
        }
        if (FCode.equalsIgnoreCase("HosStartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                HosStartDate = FValue.trim();
            }
            else
                HosStartDate = null;
        }
        if (FCode.equalsIgnoreCase("HosEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                HosEndDate = FValue.trim();
            }
            else
                HosEndDate = null;
        }
        if (FCode.equalsIgnoreCase("RgtantDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtantDate = FValue.trim();
            }
            else
                RgtantDate = null;
        }
        if (FCode.equalsIgnoreCase("Operation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operation = FValue.trim();
            }
            else
                Operation = null;
        }
        if (FCode.equalsIgnoreCase("AlarmReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                AlarmReason = FValue.trim();
            }
            else
                AlarmReason = null;
        }
        return true;
    }


    public String toString() {
    return "LLRegisterPojo [" +
            "RgtNo="+RgtNo +
            ", RgtState="+RgtState +
            ", RgtClass="+RgtClass +
            ", RgtObj="+RgtObj +
            ", RgtObjNo="+RgtObjNo +
            ", RgtType="+RgtType +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", ApplyerType="+ApplyerType +
            ", IDType="+IDType +
            ", IDNo="+IDNo +
            ", RgtantName="+RgtantName +
            ", RgtantSex="+RgtantSex +
            ", Relation="+Relation +
            ", RgtantAddress="+RgtantAddress +
            ", RgtantPhone="+RgtantPhone +
            ", RgtantMobile="+RgtantMobile +
            ", Email="+Email +
            ", PostCode="+PostCode +
            ", CustomerNo="+CustomerNo +
            ", GrpName="+GrpName +
            ", RgtDate="+RgtDate +
            ", AccidentSite="+AccidentSite +
            ", AccidentReason="+AccidentReason +
            ", AccidentCourse="+AccidentCourse +
            ", AccStartDate="+AccStartDate +
            ", AccidentDate="+AccidentDate +
            ", RgtReason="+RgtReason +
            ", AppPeoples="+AppPeoples +
            ", AppAmnt="+AppAmnt +
            ", GetMode="+GetMode +
            ", GetIntv="+GetIntv +
            ", CaseGetMode="+CaseGetMode +
            ", ReturnMode="+ReturnMode +
            ", Remark="+Remark +
            ", Handler="+Handler +
            ", TogetherFlag="+TogetherFlag +
            ", RptFlag="+RptFlag +
            ", CalFlag="+CalFlag +
            ", UWFlag="+UWFlag +
            ", DeclineFlag="+DeclineFlag +
            ", EndCaseFlag="+EndCaseFlag +
            ", EndCaseDate="+EndCaseDate +
            ", MngCom="+MngCom +
            ", ClmState="+ClmState +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", AccName="+AccName +
            ", Handler1="+Handler1 +
            ", Handler1Phone="+Handler1Phone +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", RgtConclusion="+RgtConclusion +
            ", NoRgtReason="+NoRgtReason +
            ", AssigneeType="+AssigneeType +
            ", AssigneeCode="+AssigneeCode +
            ", AssigneeName="+AssigneeName +
            ", AssigneeSex="+AssigneeSex +
            ", AssigneePhone="+AssigneePhone +
            ", AssigneeAddr="+AssigneeAddr +
            ", AssigneeZip="+AssigneeZip +
            ", BeAdjSum="+BeAdjSum +
            ", FeeInputFlag="+FeeInputFlag +
            ", GrpContNo="+GrpContNo +
            ", AppntNo="+AppntNo +
            ", Peoples2="+Peoples2 +
            ", GrpStandpay="+GrpStandpay +
            ", RiskCode="+RiskCode +
            ", Recipients="+Recipients +
            ", ReciName="+ReciName +
            ", ReciAddress="+ReciAddress +
            ", ReciDetails="+ReciDetails +
            ", ReciRela="+ReciRela +
            ", ReciPhone="+ReciPhone +
            ", ReciMobile="+ReciMobile +
            ", ReciZip="+ReciZip +
            ", ReciSex="+ReciSex +
            ", ReciEmail="+ReciEmail +
            ", ErrorFlag="+ErrorFlag +
            ", ErrorDesc="+ErrorDesc +
            ", DisDate="+DisDate +
            ", HosStartDate="+HosStartDate +
            ", HosEndDate="+HosEndDate +
            ", RgtantDate="+RgtantDate +
            ", Operation="+Operation +
            ", AlarmReason="+AlarmReason +"]";
    }
}
