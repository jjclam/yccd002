/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LASaleInfoDB;

/**
 * <p>ClassName: LASaleInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-11-20
 */
public class LASaleInfoSchema implements Schema, Cloneable {
    // @Field
    /** 渠道编码 */
    private String ChannelCode;
    /** 渠道名称 */
    private String ChannelName;
    /** 机构编码 */
    private String ComCode;
    /** 销售渠道 */
    private String SaleChnl;
    /** 机构代码 */
    private String ManageCom;
    /** 备用字段1 */
    private String Remark1;
    /** 备用字段2 */
    private String Remark2;

    public static final int FIELDNUM = 7;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LASaleInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ChannelCode";
        pk[1] = "SaleChnl";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LASaleInfoSchema cloned = (LASaleInfoSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getChannelCode() {
        return ChannelCode;
    }
    public void setChannelCode(String aChannelCode) {
        ChannelCode = aChannelCode;
    }
    public String getChannelName() {
        return ChannelName;
    }
    public void setChannelName(String aChannelName) {
        ChannelName = aChannelName;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getRemark1() {
        return Remark1;
    }
    public void setRemark1(String aRemark1) {
        Remark1 = aRemark1;
    }
    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }

    /**
    * 使用另外一个 LASaleInfoSchema 对象给 Schema 赋值
    * @param: aLASaleInfoSchema LASaleInfoSchema
    **/
    public void setSchema(LASaleInfoSchema aLASaleInfoSchema) {
        this.ChannelCode = aLASaleInfoSchema.getChannelCode();
        this.ChannelName = aLASaleInfoSchema.getChannelName();
        this.ComCode = aLASaleInfoSchema.getComCode();
        this.SaleChnl = aLASaleInfoSchema.getSaleChnl();
        this.ManageCom = aLASaleInfoSchema.getManageCom();
        this.Remark1 = aLASaleInfoSchema.getRemark1();
        this.Remark2 = aLASaleInfoSchema.getRemark2();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ChannelCode") == null )
                this.ChannelCode = null;
            else
                this.ChannelCode = rs.getString("ChannelCode").trim();

            if( rs.getString("ChannelName") == null )
                this.ChannelName = null;
            else
                this.ChannelName = rs.getString("ChannelName").trim();

            if( rs.getString("ComCode") == null )
                this.ComCode = null;
            else
                this.ComCode = rs.getString("ComCode").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("Remark1") == null )
                this.Remark1 = null;
            else
                this.Remark1 = rs.getString("Remark1").trim();

            if( rs.getString("Remark2") == null )
                this.Remark2 = null;
            else
                this.Remark2 = rs.getString("Remark2").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASaleInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LASaleInfoSchema getSchema() {
        LASaleInfoSchema aLASaleInfoSchema = new LASaleInfoSchema();
        aLASaleInfoSchema.setSchema(this);
        return aLASaleInfoSchema;
    }

    public LASaleInfoDB getDB() {
        LASaleInfoDB aDBOper = new LASaleInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLASaleInfo描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ChannelCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChannelName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark2));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLASaleInfo>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ChannelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            ChannelName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASaleInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ChannelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelCode));
        }
        if (FCode.equalsIgnoreCase("ChannelName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChannelName));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ChannelCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ChannelName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Remark1);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Remark2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ChannelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelCode = FValue.trim();
            }
            else
                ChannelCode = null;
        }
        if (FCode.equalsIgnoreCase("ChannelName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChannelName = FValue.trim();
            }
            else
                ChannelName = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark1 = FValue.trim();
            }
            else
                Remark1 = null;
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LASaleInfoSchema other = (LASaleInfoSchema)otherObject;
        return
            ChannelCode.equals(other.getChannelCode())
            && ChannelName.equals(other.getChannelName())
            && ComCode.equals(other.getComCode())
            && SaleChnl.equals(other.getSaleChnl())
            && ManageCom.equals(other.getManageCom())
            && Remark1.equals(other.getRemark1())
            && Remark2.equals(other.getRemark2());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ChannelCode") ) {
            return 0;
        }
        if( strFieldName.equals("ChannelName") ) {
            return 1;
        }
        if( strFieldName.equals("ComCode") ) {
            return 2;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 3;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 4;
        }
        if( strFieldName.equals("Remark1") ) {
            return 5;
        }
        if( strFieldName.equals("Remark2") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ChannelCode";
                break;
            case 1:
                strFieldName = "ChannelName";
                break;
            case 2:
                strFieldName = "ComCode";
                break;
            case 3:
                strFieldName = "SaleChnl";
                break;
            case 4:
                strFieldName = "ManageCom";
                break;
            case 5:
                strFieldName = "Remark1";
                break;
            case 6:
                strFieldName = "Remark2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CHANNELCODE":
                return Schema.TYPE_STRING;
            case "CHANNELNAME":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "REMARK1":
                return Schema.TYPE_STRING;
            case "REMARK2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
