/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskParamsDefPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskParamsDefPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVer; 
    /** 责任代码 */
    private String DutyCode; 
    /** 其他编码 */
    private String OtherCode; 
    /** 控制类型 */
    private String CtrlType; 
    /** 险种参数名 */
    @RedisPrimaryHKey
    private String ParamsType; 
    /** 险种参数值 */
    private String ParamsCode; 
    /** 险种参数显示 */
    private String ParamsName; 


    public static final int FIELDNUM = 8;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getOtherCode() {
        return OtherCode;
    }
    public void setOtherCode(String aOtherCode) {
        OtherCode = aOtherCode;
    }
    public String getCtrlType() {
        return CtrlType;
    }
    public void setCtrlType(String aCtrlType) {
        CtrlType = aCtrlType;
    }
    public String getParamsType() {
        return ParamsType;
    }
    public void setParamsType(String aParamsType) {
        ParamsType = aParamsType;
    }
    public String getParamsCode() {
        return ParamsCode;
    }
    public void setParamsCode(String aParamsCode) {
        ParamsCode = aParamsCode;
    }
    public String getParamsName() {
        return ParamsName;
    }
    public void setParamsName(String aParamsName) {
        ParamsName = aParamsName;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 2;
        }
        if( strFieldName.equals("OtherCode") ) {
            return 3;
        }
        if( strFieldName.equals("CtrlType") ) {
            return 4;
        }
        if( strFieldName.equals("ParamsType") ) {
            return 5;
        }
        if( strFieldName.equals("ParamsCode") ) {
            return 6;
        }
        if( strFieldName.equals("ParamsName") ) {
            return 7;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "OtherCode";
                break;
            case 4:
                strFieldName = "CtrlType";
                break;
            case 5:
                strFieldName = "ParamsType";
                break;
            case 6:
                strFieldName = "ParamsCode";
                break;
            case 7:
                strFieldName = "ParamsName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "OTHERCODE":
                return Schema.TYPE_STRING;
            case "CTRLTYPE":
                return Schema.TYPE_STRING;
            case "PARAMSTYPE":
                return Schema.TYPE_STRING;
            case "PARAMSCODE":
                return Schema.TYPE_STRING;
            case "PARAMSNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("OtherCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherCode));
        }
        if (FCode.equalsIgnoreCase("CtrlType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CtrlType));
        }
        if (FCode.equalsIgnoreCase("ParamsType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamsType));
        }
        if (FCode.equalsIgnoreCase("ParamsCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamsCode));
        }
        if (FCode.equalsIgnoreCase("ParamsName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ParamsName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 3:
                strFieldValue = String.valueOf(OtherCode);
                break;
            case 4:
                strFieldValue = String.valueOf(CtrlType);
                break;
            case 5:
                strFieldValue = String.valueOf(ParamsType);
                break;
            case 6:
                strFieldValue = String.valueOf(ParamsCode);
                break;
            case 7:
                strFieldValue = String.valueOf(ParamsName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("OtherCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherCode = FValue.trim();
            }
            else
                OtherCode = null;
        }
        if (FCode.equalsIgnoreCase("CtrlType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CtrlType = FValue.trim();
            }
            else
                CtrlType = null;
        }
        if (FCode.equalsIgnoreCase("ParamsType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ParamsType = FValue.trim();
            }
            else
                ParamsType = null;
        }
        if (FCode.equalsIgnoreCase("ParamsCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ParamsCode = FValue.trim();
            }
            else
                ParamsCode = null;
        }
        if (FCode.equalsIgnoreCase("ParamsName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ParamsName = FValue.trim();
            }
            else
                ParamsName = null;
        }
        return true;
    }


    public String toString() {
    return "LMRiskParamsDefPojo [" +
            "RiskCode="+RiskCode +
            ", RiskVer="+RiskVer +
            ", DutyCode="+DutyCode +
            ", OtherCode="+OtherCode +
            ", CtrlType="+CtrlType +
            ", ParamsType="+ParamsType +
            ", ParamsCode="+ParamsCode +
            ", ParamsName="+ParamsName +"]";
    }
}
