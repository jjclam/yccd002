/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMProdRulesPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-10-09
 */
public class LMProdRulesPojo implements Pojo,Serializable {
    // @Field
    /** 规则编码 */
    private String RulesCode; 
    /** 规则类型 */
    private String RulesType; 
    /** 规则类型说明 */
    private String RulesTypeName; 
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVersion; 
    /** 险种名称 */
    private String RiskName; 
    /** 规则适用销售渠道 */
    private String RulesChnl; 
    /** 规则适用管理机构 */
    private String RulesMngCom; 
    /** 规则扩展属性1 */
    private String RulesProperty1; 
    /** 规则扩展属性2 */
    private String RulesProperty2; 
    /** 规则扩展属性3 */
    private String RulesProperty3; 
    /** 规则扩展属性5 */
    private String RulesProperty5; 
    /** 规则扩展属性4 */
    private String RulesProperty4; 
    /** 规则扩展属性6 */
    private String RulesProperty6; 
    /** 规则算法 */
    private String CalCode; 
    /** 规则执行顺序 */
    private int RulesOrder; 
    /** 提示信息 */
    private String Message; 
    /** 显示提示信息标记 */
    private String ShowMsgFlag; 
    /** 备注 */
    private String Remark; 
    /** 规则执行标记 */
    private String CalType; 
    /** 复核标记 */
    private String ReviewFlag; 


    public static final int FIELDNUM = 21;    // 数据库表的字段个数
    public String getRulesCode() {
        return RulesCode;
    }
    public void setRulesCode(String aRulesCode) {
        RulesCode = aRulesCode;
    }
    public String getRulesType() {
        return RulesType;
    }
    public void setRulesType(String aRulesType) {
        RulesType = aRulesType;
    }
    public String getRulesTypeName() {
        return RulesTypeName;
    }
    public void setRulesTypeName(String aRulesTypeName) {
        RulesTypeName = aRulesTypeName;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVersion() {
        return RiskVersion;
    }
    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getRulesChnl() {
        return RulesChnl;
    }
    public void setRulesChnl(String aRulesChnl) {
        RulesChnl = aRulesChnl;
    }
    public String getRulesMngCom() {
        return RulesMngCom;
    }
    public void setRulesMngCom(String aRulesMngCom) {
        RulesMngCom = aRulesMngCom;
    }
    public String getRulesProperty1() {
        return RulesProperty1;
    }
    public void setRulesProperty1(String aRulesProperty1) {
        RulesProperty1 = aRulesProperty1;
    }
    public String getRulesProperty2() {
        return RulesProperty2;
    }
    public void setRulesProperty2(String aRulesProperty2) {
        RulesProperty2 = aRulesProperty2;
    }
    public String getRulesProperty3() {
        return RulesProperty3;
    }
    public void setRulesProperty3(String aRulesProperty3) {
        RulesProperty3 = aRulesProperty3;
    }
    public String getRulesProperty5() {
        return RulesProperty5;
    }
    public void setRulesProperty5(String aRulesProperty5) {
        RulesProperty5 = aRulesProperty5;
    }
    public String getRulesProperty4() {
        return RulesProperty4;
    }
    public void setRulesProperty4(String aRulesProperty4) {
        RulesProperty4 = aRulesProperty4;
    }
    public String getRulesProperty6() {
        return RulesProperty6;
    }
    public void setRulesProperty6(String aRulesProperty6) {
        RulesProperty6 = aRulesProperty6;
    }
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public int getRulesOrder() {
        return RulesOrder;
    }
    public void setRulesOrder(int aRulesOrder) {
        RulesOrder = aRulesOrder;
    }
    public void setRulesOrder(String aRulesOrder) {
        if (aRulesOrder != null && !aRulesOrder.equals("")) {
            Integer tInteger = new Integer(aRulesOrder);
            int i = tInteger.intValue();
            RulesOrder = i;
        }
    }

    public String getMessage() {
        return Message;
    }
    public void setMessage(String aMessage) {
        Message = aMessage;
    }
    public String getShowMsgFlag() {
        return ShowMsgFlag;
    }
    public void setShowMsgFlag(String aShowMsgFlag) {
        ShowMsgFlag = aShowMsgFlag;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getCalType() {
        return CalType;
    }
    public void setCalType(String aCalType) {
        CalType = aCalType;
    }
    public String getReviewFlag() {
        return ReviewFlag;
    }
    public void setReviewFlag(String aReviewFlag) {
        ReviewFlag = aReviewFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RulesCode") ) {
            return 0;
        }
        if( strFieldName.equals("RulesType") ) {
            return 1;
        }
        if( strFieldName.equals("RulesTypeName") ) {
            return 2;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 3;
        }
        if( strFieldName.equals("RiskVersion") ) {
            return 4;
        }
        if( strFieldName.equals("RiskName") ) {
            return 5;
        }
        if( strFieldName.equals("RulesChnl") ) {
            return 6;
        }
        if( strFieldName.equals("RulesMngCom") ) {
            return 7;
        }
        if( strFieldName.equals("RulesProperty1") ) {
            return 8;
        }
        if( strFieldName.equals("RulesProperty2") ) {
            return 9;
        }
        if( strFieldName.equals("RulesProperty3") ) {
            return 10;
        }
        if( strFieldName.equals("RulesProperty5") ) {
            return 11;
        }
        if( strFieldName.equals("RulesProperty4") ) {
            return 12;
        }
        if( strFieldName.equals("RulesProperty6") ) {
            return 13;
        }
        if( strFieldName.equals("CalCode") ) {
            return 14;
        }
        if( strFieldName.equals("RulesOrder") ) {
            return 15;
        }
        if( strFieldName.equals("Message") ) {
            return 16;
        }
        if( strFieldName.equals("ShowMsgFlag") ) {
            return 17;
        }
        if( strFieldName.equals("Remark") ) {
            return 18;
        }
        if( strFieldName.equals("CalType") ) {
            return 19;
        }
        if( strFieldName.equals("ReviewFlag") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RulesCode";
                break;
            case 1:
                strFieldName = "RulesType";
                break;
            case 2:
                strFieldName = "RulesTypeName";
                break;
            case 3:
                strFieldName = "RiskCode";
                break;
            case 4:
                strFieldName = "RiskVersion";
                break;
            case 5:
                strFieldName = "RiskName";
                break;
            case 6:
                strFieldName = "RulesChnl";
                break;
            case 7:
                strFieldName = "RulesMngCom";
                break;
            case 8:
                strFieldName = "RulesProperty1";
                break;
            case 9:
                strFieldName = "RulesProperty2";
                break;
            case 10:
                strFieldName = "RulesProperty3";
                break;
            case 11:
                strFieldName = "RulesProperty5";
                break;
            case 12:
                strFieldName = "RulesProperty4";
                break;
            case 13:
                strFieldName = "RulesProperty6";
                break;
            case 14:
                strFieldName = "CalCode";
                break;
            case 15:
                strFieldName = "RulesOrder";
                break;
            case 16:
                strFieldName = "Message";
                break;
            case 17:
                strFieldName = "ShowMsgFlag";
                break;
            case 18:
                strFieldName = "Remark";
                break;
            case 19:
                strFieldName = "CalType";
                break;
            case 20:
                strFieldName = "ReviewFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RULESCODE":
                return Schema.TYPE_STRING;
            case "RULESTYPE":
                return Schema.TYPE_STRING;
            case "RULESTYPENAME":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVERSION":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "RULESCHNL":
                return Schema.TYPE_STRING;
            case "RULESMNGCOM":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY1":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY2":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY3":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY5":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY4":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY6":
                return Schema.TYPE_STRING;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "RULESORDER":
                return Schema.TYPE_INT;
            case "MESSAGE":
                return Schema.TYPE_STRING;
            case "SHOWMSGFLAG":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "CALTYPE":
                return Schema.TYPE_STRING;
            case "REVIEWFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_INT;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RulesCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesCode));
        }
        if (FCode.equalsIgnoreCase("RulesType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesType));
        }
        if (FCode.equalsIgnoreCase("RulesTypeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesTypeName));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("RulesChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesChnl));
        }
        if (FCode.equalsIgnoreCase("RulesMngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesMngCom));
        }
        if (FCode.equalsIgnoreCase("RulesProperty1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty1));
        }
        if (FCode.equalsIgnoreCase("RulesProperty2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty2));
        }
        if (FCode.equalsIgnoreCase("RulesProperty3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty3));
        }
        if (FCode.equalsIgnoreCase("RulesProperty5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty5));
        }
        if (FCode.equalsIgnoreCase("RulesProperty4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty4));
        }
        if (FCode.equalsIgnoreCase("RulesProperty6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty6));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("RulesOrder")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesOrder));
        }
        if (FCode.equalsIgnoreCase("Message")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Message));
        }
        if (FCode.equalsIgnoreCase("ShowMsgFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShowMsgFlag));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("CalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalType));
        }
        if (FCode.equalsIgnoreCase("ReviewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReviewFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RulesCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RulesType);
                break;
            case 2:
                strFieldValue = String.valueOf(RulesTypeName);
                break;
            case 3:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 4:
                strFieldValue = String.valueOf(RiskVersion);
                break;
            case 5:
                strFieldValue = String.valueOf(RiskName);
                break;
            case 6:
                strFieldValue = String.valueOf(RulesChnl);
                break;
            case 7:
                strFieldValue = String.valueOf(RulesMngCom);
                break;
            case 8:
                strFieldValue = String.valueOf(RulesProperty1);
                break;
            case 9:
                strFieldValue = String.valueOf(RulesProperty2);
                break;
            case 10:
                strFieldValue = String.valueOf(RulesProperty3);
                break;
            case 11:
                strFieldValue = String.valueOf(RulesProperty5);
                break;
            case 12:
                strFieldValue = String.valueOf(RulesProperty4);
                break;
            case 13:
                strFieldValue = String.valueOf(RulesProperty6);
                break;
            case 14:
                strFieldValue = String.valueOf(CalCode);
                break;
            case 15:
                strFieldValue = String.valueOf(RulesOrder);
                break;
            case 16:
                strFieldValue = String.valueOf(Message);
                break;
            case 17:
                strFieldValue = String.valueOf(ShowMsgFlag);
                break;
            case 18:
                strFieldValue = String.valueOf(Remark);
                break;
            case 19:
                strFieldValue = String.valueOf(CalType);
                break;
            case 20:
                strFieldValue = String.valueOf(ReviewFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RulesCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesCode = FValue.trim();
            }
            else
                RulesCode = null;
        }
        if (FCode.equalsIgnoreCase("RulesType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesType = FValue.trim();
            }
            else
                RulesType = null;
        }
        if (FCode.equalsIgnoreCase("RulesTypeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesTypeName = FValue.trim();
            }
            else
                RulesTypeName = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
                RiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("RulesChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesChnl = FValue.trim();
            }
            else
                RulesChnl = null;
        }
        if (FCode.equalsIgnoreCase("RulesMngCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesMngCom = FValue.trim();
            }
            else
                RulesMngCom = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty1")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty1 = FValue.trim();
            }
            else
                RulesProperty1 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty2")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty2 = FValue.trim();
            }
            else
                RulesProperty2 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty3")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty3 = FValue.trim();
            }
            else
                RulesProperty3 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty5")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty5 = FValue.trim();
            }
            else
                RulesProperty5 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty4")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty4 = FValue.trim();
            }
            else
                RulesProperty4 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty6")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty6 = FValue.trim();
            }
            else
                RulesProperty6 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("RulesOrder")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RulesOrder = i;
            }
        }
        if (FCode.equalsIgnoreCase("Message")) {
            if( FValue != null && !FValue.equals(""))
            {
                Message = FValue.trim();
            }
            else
                Message = null;
        }
        if (FCode.equalsIgnoreCase("ShowMsgFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShowMsgFlag = FValue.trim();
            }
            else
                ShowMsgFlag = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("CalType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalType = FValue.trim();
            }
            else
                CalType = null;
        }
        if (FCode.equalsIgnoreCase("ReviewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReviewFlag = FValue.trim();
            }
            else
                ReviewFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LMProdRulesPojo [" +
            "RulesCode="+RulesCode +
            ", RulesType="+RulesType +
            ", RulesTypeName="+RulesTypeName +
            ", RiskCode="+RiskCode +
            ", RiskVersion="+RiskVersion +
            ", RiskName="+RiskName +
            ", RulesChnl="+RulesChnl +
            ", RulesMngCom="+RulesMngCom +
            ", RulesProperty1="+RulesProperty1 +
            ", RulesProperty2="+RulesProperty2 +
            ", RulesProperty3="+RulesProperty3 +
            ", RulesProperty5="+RulesProperty5 +
            ", RulesProperty4="+RulesProperty4 +
            ", RulesProperty6="+RulesProperty6 +
            ", CalCode="+CalCode +
            ", RulesOrder="+RulesOrder +
            ", Message="+Message +
            ", ShowMsgFlag="+ShowMsgFlag +
            ", Remark="+Remark +
            ", CalType="+CalType +
            ", ReviewFlag="+ReviewFlag +"]";
    }
}
