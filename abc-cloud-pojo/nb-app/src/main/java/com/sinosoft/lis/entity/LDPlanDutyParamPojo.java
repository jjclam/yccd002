/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDPlanDutyParamPojo </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-24
 */
public class LDPlanDutyParamPojo implements Pojo,Serializable {
    // @Field
    /** 主险险种编码 */
    @RedisPrimaryHKey
    private String MainRiskCode; 
    /** 主险险种版本 */
    private String MainRiskVersion; 
    /** 险种编码 */
    @RedisPrimaryHKey
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVersion; 
    /** 保险计划编码 */
    @RedisIndexHKey
    @RedisPrimaryHKey
    private String ContPlanCode; 
    /** 保险计划名称 */
    private String ContPlanName; 
    /** 责任编码 */
    @RedisPrimaryHKey
    private String DutyCode; 
    /** 计划要素 */
    @RedisPrimaryHKey
    private String CalFactor; 
    /** 计划要素类型 */
    private String CalFactorType; 
    /** 计划要素值 */
    private String CalFactorValue; 
    /** 备注 */
    private String Remark; 
    /** 计划类别 */
    @RedisPrimaryHKey
    private String PlanType; 
    /** 产品组合标记 */
    private String PortfolioFlag; 
    /** 字段编码 */
    private String FieldCode; 


    public static final int FIELDNUM = 14;    // 数据库表的字段个数
    public String getMainRiskCode() {
        return MainRiskCode;
    }
    public void setMainRiskCode(String aMainRiskCode) {
        MainRiskCode = aMainRiskCode;
    }
    public String getMainRiskVersion() {
        return MainRiskVersion;
    }
    public void setMainRiskVersion(String aMainRiskVersion) {
        MainRiskVersion = aMainRiskVersion;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVersion() {
        return RiskVersion;
    }
    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }
    public String getContPlanCode() {
        return ContPlanCode;
    }
    public void setContPlanCode(String aContPlanCode) {
        ContPlanCode = aContPlanCode;
    }
    public String getContPlanName() {
        return ContPlanName;
    }
    public void setContPlanName(String aContPlanName) {
        ContPlanName = aContPlanName;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getCalFactor() {
        return CalFactor;
    }
    public void setCalFactor(String aCalFactor) {
        CalFactor = aCalFactor;
    }
    public String getCalFactorType() {
        return CalFactorType;
    }
    public void setCalFactorType(String aCalFactorType) {
        CalFactorType = aCalFactorType;
    }
    public String getCalFactorValue() {
        return CalFactorValue;
    }
    public void setCalFactorValue(String aCalFactorValue) {
        CalFactorValue = aCalFactorValue;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getPlanType() {
        return PlanType;
    }
    public void setPlanType(String aPlanType) {
        PlanType = aPlanType;
    }
    public String getPortfolioFlag() {
        return PortfolioFlag;
    }
    public void setPortfolioFlag(String aPortfolioFlag) {
        PortfolioFlag = aPortfolioFlag;
    }
    public String getFieldCode() {
        return FieldCode;
    }
    public void setFieldCode(String aFieldCode) {
        FieldCode = aFieldCode;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("MainRiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("MainRiskVersion") ) {
            return 1;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 2;
        }
        if( strFieldName.equals("RiskVersion") ) {
            return 3;
        }
        if( strFieldName.equals("ContPlanCode") ) {
            return 4;
        }
        if( strFieldName.equals("ContPlanName") ) {
            return 5;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 6;
        }
        if( strFieldName.equals("CalFactor") ) {
            return 7;
        }
        if( strFieldName.equals("CalFactorType") ) {
            return 8;
        }
        if( strFieldName.equals("CalFactorValue") ) {
            return 9;
        }
        if( strFieldName.equals("Remark") ) {
            return 10;
        }
        if( strFieldName.equals("PlanType") ) {
            return 11;
        }
        if( strFieldName.equals("PortfolioFlag") ) {
            return 12;
        }
        if( strFieldName.equals("FieldCode") ) {
            return 13;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "MainRiskCode";
                break;
            case 1:
                strFieldName = "MainRiskVersion";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "RiskVersion";
                break;
            case 4:
                strFieldName = "ContPlanCode";
                break;
            case 5:
                strFieldName = "ContPlanName";
                break;
            case 6:
                strFieldName = "DutyCode";
                break;
            case 7:
                strFieldName = "CalFactor";
                break;
            case 8:
                strFieldName = "CalFactorType";
                break;
            case 9:
                strFieldName = "CalFactorValue";
                break;
            case 10:
                strFieldName = "Remark";
                break;
            case 11:
                strFieldName = "PlanType";
                break;
            case 12:
                strFieldName = "PortfolioFlag";
                break;
            case 13:
                strFieldName = "FieldCode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MAINRISKCODE":
                return Schema.TYPE_STRING;
            case "MAINRISKVERSION":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVERSION":
                return Schema.TYPE_STRING;
            case "CONTPLANCODE":
                return Schema.TYPE_STRING;
            case "CONTPLANNAME":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "CALFACTOR":
                return Schema.TYPE_STRING;
            case "CALFACTORTYPE":
                return Schema.TYPE_STRING;
            case "CALFACTORVALUE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "PLANTYPE":
                return Schema.TYPE_STRING;
            case "PORTFOLIOFLAG":
                return Schema.TYPE_STRING;
            case "FIELDCODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskCode));
        }
        if (FCode.equalsIgnoreCase("MainRiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskVersion));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
        }
        if (FCode.equalsIgnoreCase("ContPlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanName));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("CalFactor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactor));
        }
        if (FCode.equalsIgnoreCase("CalFactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactorType));
        }
        if (FCode.equalsIgnoreCase("CalFactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactorValue));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("PlanType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanType));
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PortfolioFlag));
        }
        if (FCode.equalsIgnoreCase("FieldCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FieldCode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(MainRiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(MainRiskVersion);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 3:
                strFieldValue = String.valueOf(RiskVersion);
                break;
            case 4:
                strFieldValue = String.valueOf(ContPlanCode);
                break;
            case 5:
                strFieldValue = String.valueOf(ContPlanName);
                break;
            case 6:
                strFieldValue = String.valueOf(DutyCode);
                break;
            case 7:
                strFieldValue = String.valueOf(CalFactor);
                break;
            case 8:
                strFieldValue = String.valueOf(CalFactorType);
                break;
            case 9:
                strFieldValue = String.valueOf(CalFactorValue);
                break;
            case 10:
                strFieldValue = String.valueOf(Remark);
                break;
            case 11:
                strFieldValue = String.valueOf(PlanType);
                break;
            case 12:
                strFieldValue = String.valueOf(PortfolioFlag);
                break;
            case 13:
                strFieldValue = String.valueOf(FieldCode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskCode = FValue.trim();
            }
            else
                MainRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskVersion = FValue.trim();
            }
            else
                MainRiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
                RiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlanCode = FValue.trim();
            }
            else
                ContPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("ContPlanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlanName = FValue.trim();
            }
            else
                ContPlanName = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("CalFactor")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFactor = FValue.trim();
            }
            else
                CalFactor = null;
        }
        if (FCode.equalsIgnoreCase("CalFactorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFactorType = FValue.trim();
            }
            else
                CalFactorType = null;
        }
        if (FCode.equalsIgnoreCase("CalFactorValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFactorValue = FValue.trim();
            }
            else
                CalFactorValue = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("PlanType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanType = FValue.trim();
            }
            else
                PlanType = null;
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PortfolioFlag = FValue.trim();
            }
            else
                PortfolioFlag = null;
        }
        if (FCode.equalsIgnoreCase("FieldCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FieldCode = FValue.trim();
            }
            else
                FieldCode = null;
        }
        return true;
    }


    public String toString() {
    return "LDPlanDutyParamPojo [" +
            "MainRiskCode="+MainRiskCode +
            ", MainRiskVersion="+MainRiskVersion +
            ", RiskCode="+RiskCode +
            ", RiskVersion="+RiskVersion +
            ", ContPlanCode="+ContPlanCode +
            ", ContPlanName="+ContPlanName +
            ", DutyCode="+DutyCode +
            ", CalFactor="+CalFactor +
            ", CalFactorType="+CalFactorType +
            ", CalFactorValue="+CalFactorValue +
            ", Remark="+Remark +
            ", PlanType="+PlanType +
            ", PortfolioFlag="+PortfolioFlag +
            ", FieldCode="+FieldCode +"]";
    }
}
