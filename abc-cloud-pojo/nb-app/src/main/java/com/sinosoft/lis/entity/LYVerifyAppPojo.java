/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LYVerifyAppPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LYVerifyAppPojo implements Pojo,Serializable {
    // @Field
    /** Id */
    private long VerifyAppID; 
    /** Fk_lccont */
    private long ContID; 
    /** Shardingid */
    private String ShardingID; 
    /** 初审号码 */
    @RedisPrimaryHKey
    private String ContNo; 
    /** 受理日期 */
    private String  ApplyDate;
    /** 受理完毕日期 */
    private String  ApplyEndDate;
    /** 销售渠道 */
    private String SaleChnl; 
    /** 初审状态 */
    private String State; 
    /** 投保单号 */
    private String PrtNo; 
    /** 银代银行代码 */
    private String AgentBankCode; 
    /** 银行网点(或中介编码） */
    private String AgentCom; 
    /** 银代柜员（或中介代理人） */
    private String BankAgent; 
    /** 代理人姓名（中介） */
    private String AgentName; 
    /** 代理人电话(中介) */
    private String AgentPhone; 
    /** 代理人编码(银代专官员或中介经理) */
    private String AgentCode; 
    /** 代理人组别 */
    private String AgentGroup; 
    /** 管理机构 */
    private String ManageCom; 
    /** 总保费 */
    private double Prem; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 投保人名称 */
    private String AppntName; 
    /** 投保人性别 */
    private String AppntSex; 
    /** 投保人出生日期 */
    private String  AppntBirthday;
    /** 与被保人关系 */
    private String RelatToInsu; 
    /** 投保人年龄 */
    private int AppAge; 
    /** 投保人职业类别 */
    private String AppntOccupationType; 
    /** 投保人职业代码 */
    private String AppntOccupationCode; 
    /** 被保人名称 */
    private String Name; 
    /** 被保人性别 */
    private String Sex; 
    /** 被保人出生日期 */
    private String  Birthday;
    /** 被保人年龄 */
    private int Age; 
    /** 职业类别 */
    private String OccupationType; 
    /** 职业代码 */
    private String OccupationCode; 
    /** 外包录入标识 */
    private String BpoFlag; 
    /** 风险测评结果 */
    private String RiskEvaluationResult; 
    /** 投保人年收入 */
    private double Income; 
    /** 美国纳税人识别号 */
    private String TINNO; 
    /** 是否为美国纳税义务的个人 */
    private String TINFlag; 


    public static final int FIELDNUM = 44;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public long getVerifyAppID() {
        return VerifyAppID;
    }
    public void setVerifyAppID(long aVerifyAppID) {
        VerifyAppID = aVerifyAppID;
    }
    public void setVerifyAppID(String aVerifyAppID) {
        if (aVerifyAppID != null && !aVerifyAppID.equals("")) {
            VerifyAppID = new Long(aVerifyAppID).longValue();
        }
    }

    public long getContID() {
        return ContID;
    }
    public void setContID(long aContID) {
        ContID = aContID;
    }
    public void setContID(String aContID) {
        if (aContID != null && !aContID.equals("")) {
            ContID = new Long(aContID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getApplyDate() {
        return ApplyDate;
    }
    public void setApplyDate(String aApplyDate) {
        ApplyDate = aApplyDate;
    }
    public String getApplyEndDate() {
        return ApplyEndDate;
    }
    public void setApplyEndDate(String aApplyEndDate) {
        ApplyEndDate = aApplyEndDate;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getAgentBankCode() {
        return AgentBankCode;
    }
    public void setAgentBankCode(String aAgentBankCode) {
        AgentBankCode = aAgentBankCode;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getBankAgent() {
        return BankAgent;
    }
    public void setBankAgent(String aBankAgent) {
        BankAgent = aBankAgent;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getAgentPhone() {
        return AgentPhone;
    }
    public void setAgentPhone(String aAgentPhone) {
        AgentPhone = aAgentPhone;
    }
    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getAppntName() {
        return AppntName;
    }
    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }
    public String getAppntSex() {
        return AppntSex;
    }
    public void setAppntSex(String aAppntSex) {
        AppntSex = aAppntSex;
    }
    public String getAppntBirthday() {
        return AppntBirthday;
    }
    public void setAppntBirthday(String aAppntBirthday) {
        AppntBirthday = aAppntBirthday;
    }
    public String getRelatToInsu() {
        return RelatToInsu;
    }
    public void setRelatToInsu(String aRelatToInsu) {
        RelatToInsu = aRelatToInsu;
    }
    public int getAppAge() {
        return AppAge;
    }
    public void setAppAge(int aAppAge) {
        AppAge = aAppAge;
    }
    public void setAppAge(String aAppAge) {
        if (aAppAge != null && !aAppAge.equals("")) {
            Integer tInteger = new Integer(aAppAge);
            int i = tInteger.intValue();
            AppAge = i;
        }
    }

    public String getAppntOccupationType() {
        return AppntOccupationType;
    }
    public void setAppntOccupationType(String aAppntOccupationType) {
        AppntOccupationType = aAppntOccupationType;
    }
    public String getAppntOccupationCode() {
        return AppntOccupationCode;
    }
    public void setAppntOccupationCode(String aAppntOccupationCode) {
        AppntOccupationCode = aAppntOccupationCode;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBirthday() {
        return Birthday;
    }
    public void setBirthday(String aBirthday) {
        Birthday = aBirthday;
    }
    public int getAge() {
        return Age;
    }
    public void setAge(int aAge) {
        Age = aAge;
    }
    public void setAge(String aAge) {
        if (aAge != null && !aAge.equals("")) {
            Integer tInteger = new Integer(aAge);
            int i = tInteger.intValue();
            Age = i;
        }
    }

    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getBpoFlag() {
        return BpoFlag;
    }
    public void setBpoFlag(String aBpoFlag) {
        BpoFlag = aBpoFlag;
    }
    public String getRiskEvaluationResult() {
        return RiskEvaluationResult;
    }
    public void setRiskEvaluationResult(String aRiskEvaluationResult) {
        RiskEvaluationResult = aRiskEvaluationResult;
    }
    public double getIncome() {
        return Income;
    }
    public void setIncome(double aIncome) {
        Income = aIncome;
    }
    public void setIncome(String aIncome) {
        if (aIncome != null && !aIncome.equals("")) {
            Double tDouble = new Double(aIncome);
            double d = tDouble.doubleValue();
            Income = d;
        }
    }

    public String getTINNO() {
        return TINNO;
    }
    public void setTINNO(String aTINNO) {
        TINNO = aTINNO;
    }
    public String getTINFlag() {
        return TINFlag;
    }
    public void setTINFlag(String aTINFlag) {
        TINFlag = aTINFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("VerifyAppID") ) {
            return 0;
        }
        if( strFieldName.equals("ContID") ) {
            return 1;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 2;
        }
        if( strFieldName.equals("ContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ApplyDate") ) {
            return 4;
        }
        if( strFieldName.equals("ApplyEndDate") ) {
            return 5;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 6;
        }
        if( strFieldName.equals("State") ) {
            return 7;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 8;
        }
        if( strFieldName.equals("AgentBankCode") ) {
            return 9;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 10;
        }
        if( strFieldName.equals("BankAgent") ) {
            return 11;
        }
        if( strFieldName.equals("AgentName") ) {
            return 12;
        }
        if( strFieldName.equals("AgentPhone") ) {
            return 13;
        }
        if( strFieldName.equals("AgentCode") ) {
            return 14;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 15;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 16;
        }
        if( strFieldName.equals("Prem") ) {
            return 17;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 18;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 19;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 20;
        }
        if( strFieldName.equals("Operator") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 25;
        }
        if( strFieldName.equals("AppntName") ) {
            return 26;
        }
        if( strFieldName.equals("AppntSex") ) {
            return 27;
        }
        if( strFieldName.equals("AppntBirthday") ) {
            return 28;
        }
        if( strFieldName.equals("RelatToInsu") ) {
            return 29;
        }
        if( strFieldName.equals("AppAge") ) {
            return 30;
        }
        if( strFieldName.equals("AppntOccupationType") ) {
            return 31;
        }
        if( strFieldName.equals("AppntOccupationCode") ) {
            return 32;
        }
        if( strFieldName.equals("Name") ) {
            return 33;
        }
        if( strFieldName.equals("Sex") ) {
            return 34;
        }
        if( strFieldName.equals("Birthday") ) {
            return 35;
        }
        if( strFieldName.equals("Age") ) {
            return 36;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 37;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 38;
        }
        if( strFieldName.equals("BpoFlag") ) {
            return 39;
        }
        if( strFieldName.equals("RiskEvaluationResult") ) {
            return 40;
        }
        if( strFieldName.equals("Income") ) {
            return 41;
        }
        if( strFieldName.equals("TINNO") ) {
            return 42;
        }
        if( strFieldName.equals("TINFlag") ) {
            return 43;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "VerifyAppID";
                break;
            case 1:
                strFieldName = "ContID";
                break;
            case 2:
                strFieldName = "ShardingID";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "ApplyDate";
                break;
            case 5:
                strFieldName = "ApplyEndDate";
                break;
            case 6:
                strFieldName = "SaleChnl";
                break;
            case 7:
                strFieldName = "State";
                break;
            case 8:
                strFieldName = "PrtNo";
                break;
            case 9:
                strFieldName = "AgentBankCode";
                break;
            case 10:
                strFieldName = "AgentCom";
                break;
            case 11:
                strFieldName = "BankAgent";
                break;
            case 12:
                strFieldName = "AgentName";
                break;
            case 13:
                strFieldName = "AgentPhone";
                break;
            case 14:
                strFieldName = "AgentCode";
                break;
            case 15:
                strFieldName = "AgentGroup";
                break;
            case 16:
                strFieldName = "ManageCom";
                break;
            case 17:
                strFieldName = "Prem";
                break;
            case 18:
                strFieldName = "StandbyFlag1";
                break;
            case 19:
                strFieldName = "StandbyFlag2";
                break;
            case 20:
                strFieldName = "StandbyFlag3";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyTime";
                break;
            case 25:
                strFieldName = "ModifyDate";
                break;
            case 26:
                strFieldName = "AppntName";
                break;
            case 27:
                strFieldName = "AppntSex";
                break;
            case 28:
                strFieldName = "AppntBirthday";
                break;
            case 29:
                strFieldName = "RelatToInsu";
                break;
            case 30:
                strFieldName = "AppAge";
                break;
            case 31:
                strFieldName = "AppntOccupationType";
                break;
            case 32:
                strFieldName = "AppntOccupationCode";
                break;
            case 33:
                strFieldName = "Name";
                break;
            case 34:
                strFieldName = "Sex";
                break;
            case 35:
                strFieldName = "Birthday";
                break;
            case 36:
                strFieldName = "Age";
                break;
            case 37:
                strFieldName = "OccupationType";
                break;
            case 38:
                strFieldName = "OccupationCode";
                break;
            case 39:
                strFieldName = "BpoFlag";
                break;
            case 40:
                strFieldName = "RiskEvaluationResult";
                break;
            case 41:
                strFieldName = "Income";
                break;
            case 42:
                strFieldName = "TINNO";
                break;
            case 43:
                strFieldName = "TINFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "VERIFYAPPID":
                return Schema.TYPE_LONG;
            case "CONTID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "APPLYDATE":
                return Schema.TYPE_STRING;
            case "APPLYENDDATE":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "AGENTBANKCODE":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "BANKAGENT":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "AGENTPHONE":
                return Schema.TYPE_STRING;
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "APPNTNAME":
                return Schema.TYPE_STRING;
            case "APPNTSEX":
                return Schema.TYPE_STRING;
            case "APPNTBIRTHDAY":
                return Schema.TYPE_STRING;
            case "RELATTOINSU":
                return Schema.TYPE_STRING;
            case "APPAGE":
                return Schema.TYPE_INT;
            case "APPNTOCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "APPNTOCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_STRING;
            case "AGE":
                return Schema.TYPE_INT;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "BPOFLAG":
                return Schema.TYPE_STRING;
            case "RISKEVALUATIONRESULT":
                return Schema.TYPE_STRING;
            case "INCOME":
                return Schema.TYPE_DOUBLE;
            case "TINNO":
                return Schema.TYPE_STRING;
            case "TINFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_INT;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_INT;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_DOUBLE;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("VerifyAppID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VerifyAppID));
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyDate));
        }
        if (FCode.equalsIgnoreCase("ApplyEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyEndDate));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentBankCode));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAgent));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("AgentPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPhone));
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntSex));
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntBirthday));
        }
        if (FCode.equalsIgnoreCase("RelatToInsu")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelatToInsu));
        }
        if (FCode.equalsIgnoreCase("AppAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppAge));
        }
        if (FCode.equalsIgnoreCase("AppntOccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntOccupationType));
        }
        if (FCode.equalsIgnoreCase("AppntOccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntOccupationCode));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
        }
        if (FCode.equalsIgnoreCase("Age")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Age));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("BpoFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BpoFlag));
        }
        if (FCode.equalsIgnoreCase("RiskEvaluationResult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskEvaluationResult));
        }
        if (FCode.equalsIgnoreCase("Income")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Income));
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINNO));
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TINFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(VerifyAppID);
                break;
            case 1:
                strFieldValue = String.valueOf(ContID);
                break;
            case 2:
                strFieldValue = String.valueOf(ShardingID);
                break;
            case 3:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 4:
                strFieldValue = String.valueOf(ApplyDate);
                break;
            case 5:
                strFieldValue = String.valueOf(ApplyEndDate);
                break;
            case 6:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 7:
                strFieldValue = String.valueOf(State);
                break;
            case 8:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 9:
                strFieldValue = String.valueOf(AgentBankCode);
                break;
            case 10:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 11:
                strFieldValue = String.valueOf(BankAgent);
                break;
            case 12:
                strFieldValue = String.valueOf(AgentName);
                break;
            case 13:
                strFieldValue = String.valueOf(AgentPhone);
                break;
            case 14:
                strFieldValue = String.valueOf(AgentCode);
                break;
            case 15:
                strFieldValue = String.valueOf(AgentGroup);
                break;
            case 16:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 17:
                strFieldValue = String.valueOf(Prem);
                break;
            case 18:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 19:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 20:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 21:
                strFieldValue = String.valueOf(Operator);
                break;
            case 22:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 23:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 24:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 25:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 26:
                strFieldValue = String.valueOf(AppntName);
                break;
            case 27:
                strFieldValue = String.valueOf(AppntSex);
                break;
            case 28:
                strFieldValue = String.valueOf(AppntBirthday);
                break;
            case 29:
                strFieldValue = String.valueOf(RelatToInsu);
                break;
            case 30:
                strFieldValue = String.valueOf(AppAge);
                break;
            case 31:
                strFieldValue = String.valueOf(AppntOccupationType);
                break;
            case 32:
                strFieldValue = String.valueOf(AppntOccupationCode);
                break;
            case 33:
                strFieldValue = String.valueOf(Name);
                break;
            case 34:
                strFieldValue = String.valueOf(Sex);
                break;
            case 35:
                strFieldValue = String.valueOf(Birthday);
                break;
            case 36:
                strFieldValue = String.valueOf(Age);
                break;
            case 37:
                strFieldValue = String.valueOf(OccupationType);
                break;
            case 38:
                strFieldValue = String.valueOf(OccupationCode);
                break;
            case 39:
                strFieldValue = String.valueOf(BpoFlag);
                break;
            case 40:
                strFieldValue = String.valueOf(RiskEvaluationResult);
                break;
            case 41:
                strFieldValue = String.valueOf(Income);
                break;
            case 42:
                strFieldValue = String.valueOf(TINNO);
                break;
            case 43:
                strFieldValue = String.valueOf(TINFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("VerifyAppID")) {
            if( FValue != null && !FValue.equals("")) {
                VerifyAppID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ContID")) {
            if( FValue != null && !FValue.equals("")) {
                ContID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyDate = FValue.trim();
            }
            else
                ApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("ApplyEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyEndDate = FValue.trim();
            }
            else
                ApplyEndDate = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("AgentBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentBankCode = FValue.trim();
            }
            else
                AgentBankCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("BankAgent")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAgent = FValue.trim();
            }
            else
                BankAgent = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("AgentPhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentPhone = FValue.trim();
            }
            else
                AgentPhone = null;
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
                AppntName = null;
        }
        if (FCode.equalsIgnoreCase("AppntSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntSex = FValue.trim();
            }
            else
                AppntSex = null;
        }
        if (FCode.equalsIgnoreCase("AppntBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntBirthday = FValue.trim();
            }
            else
                AppntBirthday = null;
        }
        if (FCode.equalsIgnoreCase("RelatToInsu")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelatToInsu = FValue.trim();
            }
            else
                RelatToInsu = null;
        }
        if (FCode.equalsIgnoreCase("AppAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                AppAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("AppntOccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntOccupationType = FValue.trim();
            }
            else
                AppntOccupationType = null;
        }
        if (FCode.equalsIgnoreCase("AppntOccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntOccupationCode = FValue.trim();
            }
            else
                AppntOccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                Birthday = FValue.trim();
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("Age")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Age = i;
            }
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("BpoFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BpoFlag = FValue.trim();
            }
            else
                BpoFlag = null;
        }
        if (FCode.equalsIgnoreCase("RiskEvaluationResult")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskEvaluationResult = FValue.trim();
            }
            else
                RiskEvaluationResult = null;
        }
        if (FCode.equalsIgnoreCase("Income")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Income = d;
            }
        }
        if (FCode.equalsIgnoreCase("TINNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINNO = FValue.trim();
            }
            else
                TINNO = null;
        }
        if (FCode.equalsIgnoreCase("TINFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TINFlag = FValue.trim();
            }
            else
                TINFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LYVerifyAppPojo [" +
            "VerifyAppID="+VerifyAppID +
            ", ContID="+ContID +
            ", ShardingID="+ShardingID +
            ", ContNo="+ContNo +
            ", ApplyDate="+ApplyDate +
            ", ApplyEndDate="+ApplyEndDate +
            ", SaleChnl="+SaleChnl +
            ", State="+State +
            ", PrtNo="+PrtNo +
            ", AgentBankCode="+AgentBankCode +
            ", AgentCom="+AgentCom +
            ", BankAgent="+BankAgent +
            ", AgentName="+AgentName +
            ", AgentPhone="+AgentPhone +
            ", AgentCode="+AgentCode +
            ", AgentGroup="+AgentGroup +
            ", ManageCom="+ManageCom +
            ", Prem="+Prem +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", StandbyFlag3="+StandbyFlag3 +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyTime="+ModifyTime +
            ", ModifyDate="+ModifyDate +
            ", AppntName="+AppntName +
            ", AppntSex="+AppntSex +
            ", AppntBirthday="+AppntBirthday +
            ", RelatToInsu="+RelatToInsu +
            ", AppAge="+AppAge +
            ", AppntOccupationType="+AppntOccupationType +
            ", AppntOccupationCode="+AppntOccupationCode +
            ", Name="+Name +
            ", Sex="+Sex +
            ", Birthday="+Birthday +
            ", Age="+Age +
            ", OccupationType="+OccupationType +
            ", OccupationCode="+OccupationCode +
            ", BpoFlag="+BpoFlag +
            ", RiskEvaluationResult="+RiskEvaluationResult +
            ", Income="+Income +
            ", TINNO="+TINNO +
            ", TINFlag="+TINFlag +"]";
    }
}
