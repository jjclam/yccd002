/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LXBlackDatePojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LXBlackDatePojo implements Pojo,Serializable {
    // @Field
    /** 黑名单id */
    private String BlackListID; 
    /** 日期（出生日期或注册日期） */
    private String BlackDate; 
    /** 创建者操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String MakeDate; 
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String ModifityDate; 
    /** 修改时间 */
    private String ModifyTime; 
    /** Stringbyfalg1 */
    private String Stringbyfalg1; 
    /** Stringbyfalg2 */
    private String Stringbyfalg2; 


    public static final int FIELDNUM = 9;    // 数据库表的字段个数
    public String getBlackListID() {
        return BlackListID;
    }
    public void setBlackListID(String aBlackListID) {
        BlackListID = aBlackListID;
    }
    public String getBlackDate() {
        return BlackDate;
    }
    public void setBlackDate(String aBlackDate) {
        BlackDate = aBlackDate;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifityDate() {
        return ModifityDate;
    }
    public void setModifityDate(String aModifityDate) {
        ModifityDate = aModifityDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStringbyfalg1() {
        return Stringbyfalg1;
    }
    public void setStringbyfalg1(String aStringbyfalg1) {
        Stringbyfalg1 = aStringbyfalg1;
    }
    public String getStringbyfalg2() {
        return Stringbyfalg2;
    }
    public void setStringbyfalg2(String aStringbyfalg2) {
        Stringbyfalg2 = aStringbyfalg2;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BlackListID") ) {
            return 0;
        }
        if( strFieldName.equals("BlackDate") ) {
            return 1;
        }
        if( strFieldName.equals("Operator") ) {
            return 2;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 3;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 4;
        }
        if( strFieldName.equals("ModifityDate") ) {
            return 5;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 6;
        }
        if( strFieldName.equals("Stringbyfalg1") ) {
            return 7;
        }
        if( strFieldName.equals("Stringbyfalg2") ) {
            return 8;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BlackListID";
                break;
            case 1:
                strFieldName = "BlackDate";
                break;
            case 2:
                strFieldName = "Operator";
                break;
            case 3:
                strFieldName = "MakeDate";
                break;
            case 4:
                strFieldName = "MakeTime";
                break;
            case 5:
                strFieldName = "ModifityDate";
                break;
            case 6:
                strFieldName = "ModifyTime";
                break;
            case 7:
                strFieldName = "Stringbyfalg1";
                break;
            case 8:
                strFieldName = "Stringbyfalg2";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BLACKLISTID":
                return Schema.TYPE_STRING;
            case "BLACKDATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFITYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STRINGBYFALG1":
                return Schema.TYPE_STRING;
            case "STRINGBYFALG2":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BlackListID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListID));
        }
        if (FCode.equalsIgnoreCase("BlackDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackDate));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifityDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifityDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Stringbyfalg1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stringbyfalg1));
        }
        if (FCode.equalsIgnoreCase("Stringbyfalg2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Stringbyfalg2));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BlackListID);
                break;
            case 1:
                strFieldValue = String.valueOf(BlackDate);
                break;
            case 2:
                strFieldValue = String.valueOf(Operator);
                break;
            case 3:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 4:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 5:
                strFieldValue = String.valueOf(ModifityDate);
                break;
            case 6:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 7:
                strFieldValue = String.valueOf(Stringbyfalg1);
                break;
            case 8:
                strFieldValue = String.valueOf(Stringbyfalg2);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BlackListID")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackListID = FValue.trim();
            }
            else
                BlackListID = null;
        }
        if (FCode.equalsIgnoreCase("BlackDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackDate = FValue.trim();
            }
            else
                BlackDate = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifityDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifityDate = FValue.trim();
            }
            else
                ModifityDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Stringbyfalg1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Stringbyfalg1 = FValue.trim();
            }
            else
                Stringbyfalg1 = null;
        }
        if (FCode.equalsIgnoreCase("Stringbyfalg2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Stringbyfalg2 = FValue.trim();
            }
            else
                Stringbyfalg2 = null;
        }
        return true;
    }


    public String toString() {
    return "LXBlackDatePojo [" +
            "BlackListID="+BlackListID +
            ", BlackDate="+BlackDate +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifityDate="+ModifityDate +
            ", ModifyTime="+ModifyTime +
            ", Stringbyfalg1="+Stringbyfalg1 +
            ", Stringbyfalg2="+Stringbyfalg2 +"]";
    }
}
