/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LYVerifyAppBSchema;
import com.sinosoft.lis.vschema.LYVerifyAppBSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LYVerifyAppBDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LYVerifyAppBDB extends LYVerifyAppBSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LYVerifyAppBDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LYVerifyAppB" );
        mflag = true;
    }

    public LYVerifyAppBDB() {
        con = null;
        db = new DBOper( "LYVerifyAppB" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LYVerifyAppBSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LYVerifyAppBSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LYVerifyAppB WHERE  1=1  AND VerifyAppID = ?");
            pstmt.setLong(1, this.getVerifyAppID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LYVerifyAppB");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LYVerifyAppB SET  VerifyAppID = ? , ContID = ? , ShardingID = ? , EdorNo = ? , ContNo = ? , ApplyDate = ? , ApplyEndDate = ? , SaleChnl = ? , State = ? , PrtNo = ? , AgentBankCode = ? , AgentCom = ? , BankAgent = ? , AgentName = ? , AgentPhone = ? , AgentCode = ? , AgentGroup = ? , ManageCom = ? , Prem = ? , StandbyFlag1 = ? , StandbyFlag2 = ? , StandbyFlag3 = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyTime = ? , ModifyDate = ? , AppntName = ? , AppntSex = ? , AppntBirthday = ? , RelatToInsu = ? , AppAge = ? , AppntOccupationType = ? , AppntOccupationCode = ? , Name = ? , Sex = ? , Birthday = ? , Age = ? , OccupationType = ? , OccupationCode = ? , BpoFlag = ? , RiskEvaluationResult = ? , Income = ? , TINNO = ? , TINFlag = ? WHERE  1=1  AND VerifyAppID = ?");
            pstmt.setLong(1, this.getVerifyAppID());
            pstmt.setLong(2, this.getContID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getShardingID());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getEdorNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getContNo());
            }
            if(this.getApplyDate() == null || this.getApplyDate().equals("null")) {
            	pstmt.setNull(6, 93);
            } else {
            	pstmt.setDate(6, Date.valueOf(this.getApplyDate()));
            }
            if(this.getApplyEndDate() == null || this.getApplyEndDate().equals("null")) {
            	pstmt.setNull(7, 93);
            } else {
            	pstmt.setDate(7, Date.valueOf(this.getApplyEndDate()));
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getSaleChnl());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getState());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getPrtNo());
            }
            if(this.getAgentBankCode() == null || this.getAgentBankCode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAgentBankCode());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getAgentCom());
            }
            if(this.getBankAgent() == null || this.getBankAgent().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getBankAgent());
            }
            if(this.getAgentName() == null || this.getAgentName().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAgentName());
            }
            if(this.getAgentPhone() == null || this.getAgentPhone().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getAgentPhone());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getAgentGroup());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getManageCom());
            }
            pstmt.setDouble(19, this.getPrem());
            if(this.getStandbyFlag1() == null || this.getStandbyFlag1().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getStandbyFlag1());
            }
            if(this.getStandbyFlag2() == null || this.getStandbyFlag2().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getStandbyFlag2());
            }
            if(this.getStandbyFlag3() == null || this.getStandbyFlag3().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getStandbyFlag3());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getMakeTime());
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getModifyTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(27, 93);
            } else {
            	pstmt.setDate(27, Date.valueOf(this.getModifyDate()));
            }
            if(this.getAppntName() == null || this.getAppntName().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getAppntName());
            }
            if(this.getAppntSex() == null || this.getAppntSex().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getAppntSex());
            }
            if(this.getAppntBirthday() == null || this.getAppntBirthday().equals("null")) {
            	pstmt.setNull(30, 93);
            } else {
            	pstmt.setDate(30, Date.valueOf(this.getAppntBirthday()));
            }
            if(this.getRelatToInsu() == null || this.getRelatToInsu().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getRelatToInsu());
            }
            pstmt.setInt(32, this.getAppAge());
            if(this.getAppntOccupationType() == null || this.getAppntOccupationType().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getAppntOccupationType());
            }
            if(this.getAppntOccupationCode() == null || this.getAppntOccupationCode().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getAppntOccupationCode());
            }
            if(this.getName() == null || this.getName().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getName());
            }
            if(this.getSex() == null || this.getSex().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getSex());
            }
            if(this.getBirthday() == null || this.getBirthday().equals("null")) {
            	pstmt.setNull(37, 93);
            } else {
            	pstmt.setDate(37, Date.valueOf(this.getBirthday()));
            }
            pstmt.setInt(38, this.getAge());
            if(this.getOccupationType() == null || this.getOccupationType().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getOccupationType());
            }
            if(this.getOccupationCode() == null || this.getOccupationCode().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getOccupationCode());
            }
            if(this.getBpoFlag() == null || this.getBpoFlag().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getBpoFlag());
            }
            if(this.getRiskEvaluationResult() == null || this.getRiskEvaluationResult().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getRiskEvaluationResult());
            }
            pstmt.setDouble(43, this.getIncome());
            if(this.getTINNO() == null || this.getTINNO().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getTINNO());
            }
            if(this.getTINFlag() == null || this.getTINFlag().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getTINFlag());
            }
            // set where condition
            pstmt.setLong(46, this.getVerifyAppID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LYVerifyAppB");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LYVerifyAppB VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getVerifyAppID());
            pstmt.setLong(2, this.getContID());
            if(this.getShardingID() == null || this.getShardingID().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getShardingID());
            }
            if(this.getEdorNo() == null || this.getEdorNo().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getEdorNo());
            }
            if(this.getContNo() == null || this.getContNo().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getContNo());
            }
            if(this.getApplyDate() == null || this.getApplyDate().equals("null")) {
            	pstmt.setNull(6, 93);
            } else {
            	pstmt.setDate(6, Date.valueOf(this.getApplyDate()));
            }
            if(this.getApplyEndDate() == null || this.getApplyEndDate().equals("null")) {
            	pstmt.setNull(7, 93);
            } else {
            	pstmt.setDate(7, Date.valueOf(this.getApplyEndDate()));
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getSaleChnl());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getState());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getPrtNo());
            }
            if(this.getAgentBankCode() == null || this.getAgentBankCode().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAgentBankCode());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getAgentCom());
            }
            if(this.getBankAgent() == null || this.getBankAgent().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getBankAgent());
            }
            if(this.getAgentName() == null || this.getAgentName().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAgentName());
            }
            if(this.getAgentPhone() == null || this.getAgentPhone().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getAgentPhone());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getAgentCode());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getAgentGroup());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getManageCom());
            }
            pstmt.setDouble(19, this.getPrem());
            if(this.getStandbyFlag1() == null || this.getStandbyFlag1().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getStandbyFlag1());
            }
            if(this.getStandbyFlag2() == null || this.getStandbyFlag2().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getStandbyFlag2());
            }
            if(this.getStandbyFlag3() == null || this.getStandbyFlag3().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getStandbyFlag3());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getMakeTime());
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getModifyTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(27, 93);
            } else {
            	pstmt.setDate(27, Date.valueOf(this.getModifyDate()));
            }
            if(this.getAppntName() == null || this.getAppntName().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getAppntName());
            }
            if(this.getAppntSex() == null || this.getAppntSex().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getAppntSex());
            }
            if(this.getAppntBirthday() == null || this.getAppntBirthday().equals("null")) {
            	pstmt.setNull(30, 93);
            } else {
            	pstmt.setDate(30, Date.valueOf(this.getAppntBirthday()));
            }
            if(this.getRelatToInsu() == null || this.getRelatToInsu().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getRelatToInsu());
            }
            pstmt.setInt(32, this.getAppAge());
            if(this.getAppntOccupationType() == null || this.getAppntOccupationType().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getAppntOccupationType());
            }
            if(this.getAppntOccupationCode() == null || this.getAppntOccupationCode().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getAppntOccupationCode());
            }
            if(this.getName() == null || this.getName().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getName());
            }
            if(this.getSex() == null || this.getSex().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getSex());
            }
            if(this.getBirthday() == null || this.getBirthday().equals("null")) {
            	pstmt.setNull(37, 93);
            } else {
            	pstmt.setDate(37, Date.valueOf(this.getBirthday()));
            }
            pstmt.setInt(38, this.getAge());
            if(this.getOccupationType() == null || this.getOccupationType().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getOccupationType());
            }
            if(this.getOccupationCode() == null || this.getOccupationCode().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getOccupationCode());
            }
            if(this.getBpoFlag() == null || this.getBpoFlag().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getBpoFlag());
            }
            if(this.getRiskEvaluationResult() == null || this.getRiskEvaluationResult().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getRiskEvaluationResult());
            }
            pstmt.setDouble(43, this.getIncome());
            if(this.getTINNO() == null || this.getTINNO().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getTINNO());
            }
            if(this.getTINFlag() == null || this.getTINFlag().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getTINFlag());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LYVerifyAppB WHERE  1=1  AND VerifyAppID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getVerifyAppID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LYVerifyAppBDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LYVerifyAppBSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LYVerifyAppBSet aLYVerifyAppBSet = new LYVerifyAppBSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LYVerifyAppB");
            LYVerifyAppBSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LYVerifyAppBDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LYVerifyAppBSchema s1 = new LYVerifyAppBSchema();
                s1.setSchema(rs,i);
                aLYVerifyAppBSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLYVerifyAppBSet;
    }

    public LYVerifyAppBSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LYVerifyAppBSet aLYVerifyAppBSet = new LYVerifyAppBSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LYVerifyAppBDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LYVerifyAppBSchema s1 = new LYVerifyAppBSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LYVerifyAppBDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLYVerifyAppBSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLYVerifyAppBSet;
    }

    public LYVerifyAppBSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LYVerifyAppBSet aLYVerifyAppBSet = new LYVerifyAppBSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LYVerifyAppB");
            LYVerifyAppBSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LYVerifyAppBSchema s1 = new LYVerifyAppBSchema();
                s1.setSchema(rs,i);
                aLYVerifyAppBSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLYVerifyAppBSet;
    }

    public LYVerifyAppBSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LYVerifyAppBSet aLYVerifyAppBSet = new LYVerifyAppBSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LYVerifyAppBSchema s1 = new LYVerifyAppBSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LYVerifyAppBDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLYVerifyAppBSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLYVerifyAppBSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LYVerifyAppB");
            LYVerifyAppBSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LYVerifyAppB " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LYVerifyAppBDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LYVerifyAppBSet
     */
    public LYVerifyAppBSet getData() {
        int tCount = 0;
        LYVerifyAppBSet tLYVerifyAppBSet = new LYVerifyAppBSet();
        LYVerifyAppBSchema tLYVerifyAppBSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLYVerifyAppBSchema = new LYVerifyAppBSchema();
            tLYVerifyAppBSchema.setSchema(mResultSet, 1);
            tLYVerifyAppBSet.add(tLYVerifyAppBSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLYVerifyAppBSchema = new LYVerifyAppBSchema();
                    tLYVerifyAppBSchema.setSchema(mResultSet, 1);
                    tLYVerifyAppBSet.add(tLYVerifyAppBSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLYVerifyAppBSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LYVerifyAppBDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LYVerifyAppBDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LYVerifyAppBDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
