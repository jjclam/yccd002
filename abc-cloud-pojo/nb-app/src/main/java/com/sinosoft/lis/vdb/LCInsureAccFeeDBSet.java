/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LCInsureAccFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LCInsureAccFeeDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LCInsureAccFeeDBSet extends LCInsureAccFeeSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LCInsureAccFeeDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCInsureAccFee");
        mflag = true;
    }

    public LCInsureAccFeeDBSet() {
        db = new DBOper( "LCInsureAccFee" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInsureAccFeeDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCInsureAccFee WHERE  1=1  AND InsureAccFeeID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getInsureAccFeeID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInsureAccFeeDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCInsureAccFee SET  InsureAccFeeID = ? , ShardingID = ? , GrpContNo = ? , GrpPolNo = ? , PolNo = ? , InsuAccNo = ? , ContNo = ? , ManageCom = ? , PrtNo = ? , RiskCode = ? , AccType = ? , InvestType = ? , FundCompanyCode = ? , InsuredNo = ? , AppntNo = ? , Owner = ? , AccComputeFlag = ? , AccFoundDate = ? , AccFoundTime = ? , FeeRate = ? , Fee = ? , FeeUnit = ? , BalaDate = ? , BalaTime = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? WHERE  1=1  AND InsureAccFeeID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getInsureAccFeeID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGrpContNo());
            }
            if(this.get(i).getGrpPolNo() == null || this.get(i).getGrpPolNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getGrpPolNo());
            }
            if(this.get(i).getPolNo() == null || this.get(i).getPolNo().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getPolNo());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getContNo());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getManageCom());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getPrtNo());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getRiskCode());
            }
            if(this.get(i).getAccType() == null || this.get(i).getAccType().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAccType());
            }
            if(this.get(i).getInvestType() == null || this.get(i).getInvestType().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getInvestType());
            }
            if(this.get(i).getFundCompanyCode() == null || this.get(i).getFundCompanyCode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getFundCompanyCode());
            }
            if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getInsuredNo());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAppntNo());
            }
            if(this.get(i).getOwner() == null || this.get(i).getOwner().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getOwner());
            }
            if(this.get(i).getAccComputeFlag() == null || this.get(i).getAccComputeFlag().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getAccComputeFlag());
            }
            if(this.get(i).getAccFoundDate() == null || this.get(i).getAccFoundDate().equals("null")) {
                pstmt.setDate(18,null);
            } else {
                pstmt.setDate(18, Date.valueOf(this.get(i).getAccFoundDate()));
            }
            if(this.get(i).getAccFoundTime() == null || this.get(i).getAccFoundTime().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getAccFoundTime());
            }
            pstmt.setDouble(20, this.get(i).getFeeRate());
            pstmt.setDouble(21, this.get(i).getFee());
            pstmt.setDouble(22, this.get(i).getFeeUnit());
            if(this.get(i).getBalaDate() == null || this.get(i).getBalaDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getBalaDate()));
            }
            if(this.get(i).getBalaTime() == null || this.get(i).getBalaTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getBalaTime());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(28,null);
            } else {
                pstmt.setDate(28, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getModifyTime());
            }
            // set where condition
            pstmt.setLong(30, this.get(i).getInsureAccFeeID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInsureAccFeeDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCInsureAccFee VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getInsureAccFeeID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGrpContNo());
            }
            if(this.get(i).getGrpPolNo() == null || this.get(i).getGrpPolNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getGrpPolNo());
            }
            if(this.get(i).getPolNo() == null || this.get(i).getPolNo().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getPolNo());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getContNo());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getManageCom());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getPrtNo());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getRiskCode());
            }
            if(this.get(i).getAccType() == null || this.get(i).getAccType().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAccType());
            }
            if(this.get(i).getInvestType() == null || this.get(i).getInvestType().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getInvestType());
            }
            if(this.get(i).getFundCompanyCode() == null || this.get(i).getFundCompanyCode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getFundCompanyCode());
            }
            if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getInsuredNo());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAppntNo());
            }
            if(this.get(i).getOwner() == null || this.get(i).getOwner().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getOwner());
            }
            if(this.get(i).getAccComputeFlag() == null || this.get(i).getAccComputeFlag().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getAccComputeFlag());
            }
            if(this.get(i).getAccFoundDate() == null || this.get(i).getAccFoundDate().equals("null")) {
                pstmt.setDate(18,null);
            } else {
                pstmt.setDate(18, Date.valueOf(this.get(i).getAccFoundDate()));
            }
            if(this.get(i).getAccFoundTime() == null || this.get(i).getAccFoundTime().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getAccFoundTime());
            }
            pstmt.setDouble(20, this.get(i).getFeeRate());
            pstmt.setDouble(21, this.get(i).getFee());
            pstmt.setDouble(22, this.get(i).getFeeUnit());
            if(this.get(i).getBalaDate() == null || this.get(i).getBalaDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getBalaDate()));
            }
            if(this.get(i).getBalaTime() == null || this.get(i).getBalaTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getBalaTime());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(28,null);
            } else {
                pstmt.setDate(28, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getModifyTime());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInsureAccFeeDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
