/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LKCodeMappingPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LKCodeMappingPojo implements Pojo,Serializable {
    // @Field
    /** 银行编码 */
    @RedisPrimaryHKey
    private String BankCode; 
    /** 地区代码 */
    @RedisPrimaryHKey
    private String ZoneNo; 
    /** 银行网点 */
    @RedisPrimaryHKey
    private String BankNode; 
    /** 代理机构 */
    private String AgentCom; 
    /** 登陆机构 */
    private String ComCode; 
    /** 管理机构 */
    private String ManageCom; 
    /** 专管员 */
    private String Operator; 
    /** 文件存放路径 */
    private String Remark; 
    /** 对帐网点 */
    private String Remark1; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModifyTime; 
    /** Ftp地址 */
    private String FTPAddress; 
    /** Ftp目录 */
    private String FTPDir; 
    /** Ftp用户 */
    private String FTPUser; 
    /** Ftp密码 */
    private String FTPPassWord; 
    /** 备用2 */
    private String Remark2; 
    /** 对帐地区码 */
    private String Remark3; 
    /** 服务器文件目录 */
    private String ServerDir; 
    /** 服务器ip地址 */
    private String ServerAddress; 
    /** 服务器端口 */
    private int ServerPort; 
    /** 服务器用户 */
    private String ServerUser; 
    /** 服务器密码 */
    private String ServerPassWord; 
    /** 备用4 */
    private String Remark4; 
    /** 备用5 */
    private String Remark5; 


    public static final int FIELDNUM = 26;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getZoneNo() {
        return ZoneNo;
    }
    public void setZoneNo(String aZoneNo) {
        ZoneNo = aZoneNo;
    }
    public String getBankNode() {
        return BankNode;
    }
    public void setBankNode(String aBankNode) {
        BankNode = aBankNode;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getRemark1() {
        return Remark1;
    }
    public void setRemark1(String aRemark1) {
        Remark1 = aRemark1;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFTPAddress() {
        return FTPAddress;
    }
    public void setFTPAddress(String aFTPAddress) {
        FTPAddress = aFTPAddress;
    }
    public String getFTPDir() {
        return FTPDir;
    }
    public void setFTPDir(String aFTPDir) {
        FTPDir = aFTPDir;
    }
    public String getFTPUser() {
        return FTPUser;
    }
    public void setFTPUser(String aFTPUser) {
        FTPUser = aFTPUser;
    }
    public String getFTPPassWord() {
        return FTPPassWord;
    }
    public void setFTPPassWord(String aFTPPassWord) {
        FTPPassWord = aFTPPassWord;
    }
    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }
    public String getRemark3() {
        return Remark3;
    }
    public void setRemark3(String aRemark3) {
        Remark3 = aRemark3;
    }
    public String getServerDir() {
        return ServerDir;
    }
    public void setServerDir(String aServerDir) {
        ServerDir = aServerDir;
    }
    public String getServerAddress() {
        return ServerAddress;
    }
    public void setServerAddress(String aServerAddress) {
        ServerAddress = aServerAddress;
    }
    public int getServerPort() {
        return ServerPort;
    }
    public void setServerPort(int aServerPort) {
        ServerPort = aServerPort;
    }
    public void setServerPort(String aServerPort) {
        if (aServerPort != null && !aServerPort.equals("")) {
            Integer tInteger = new Integer(aServerPort);
            int i = tInteger.intValue();
            ServerPort = i;
        }
    }

    public String getServerUser() {
        return ServerUser;
    }
    public void setServerUser(String aServerUser) {
        ServerUser = aServerUser;
    }
    public String getServerPassWord() {
        return ServerPassWord;
    }
    public void setServerPassWord(String aServerPassWord) {
        ServerPassWord = aServerPassWord;
    }
    public String getRemark4() {
        return Remark4;
    }
    public void setRemark4(String aRemark4) {
        Remark4 = aRemark4;
    }
    public String getRemark5() {
        return Remark5;
    }
    public void setRemark5(String aRemark5) {
        Remark5 = aRemark5;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BankCode") ) {
            return 0;
        }
        if( strFieldName.equals("ZoneNo") ) {
            return 1;
        }
        if( strFieldName.equals("BankNode") ) {
            return 2;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 3;
        }
        if( strFieldName.equals("ComCode") ) {
            return 4;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 5;
        }
        if( strFieldName.equals("Operator") ) {
            return 6;
        }
        if( strFieldName.equals("Remark") ) {
            return 7;
        }
        if( strFieldName.equals("Remark1") ) {
            return 8;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 9;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 11;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 12;
        }
        if( strFieldName.equals("FTPAddress") ) {
            return 13;
        }
        if( strFieldName.equals("FTPDir") ) {
            return 14;
        }
        if( strFieldName.equals("FTPUser") ) {
            return 15;
        }
        if( strFieldName.equals("FTPPassWord") ) {
            return 16;
        }
        if( strFieldName.equals("Remark2") ) {
            return 17;
        }
        if( strFieldName.equals("Remark3") ) {
            return 18;
        }
        if( strFieldName.equals("ServerDir") ) {
            return 19;
        }
        if( strFieldName.equals("ServerAddress") ) {
            return 20;
        }
        if( strFieldName.equals("ServerPort") ) {
            return 21;
        }
        if( strFieldName.equals("ServerUser") ) {
            return 22;
        }
        if( strFieldName.equals("ServerPassWord") ) {
            return 23;
        }
        if( strFieldName.equals("Remark4") ) {
            return 24;
        }
        if( strFieldName.equals("Remark5") ) {
            return 25;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BankCode";
                break;
            case 1:
                strFieldName = "ZoneNo";
                break;
            case 2:
                strFieldName = "BankNode";
                break;
            case 3:
                strFieldName = "AgentCom";
                break;
            case 4:
                strFieldName = "ComCode";
                break;
            case 5:
                strFieldName = "ManageCom";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "Remark";
                break;
            case 8:
                strFieldName = "Remark1";
                break;
            case 9:
                strFieldName = "MakeDate";
                break;
            case 10:
                strFieldName = "MakeTime";
                break;
            case 11:
                strFieldName = "ModifyDate";
                break;
            case 12:
                strFieldName = "ModifyTime";
                break;
            case 13:
                strFieldName = "FTPAddress";
                break;
            case 14:
                strFieldName = "FTPDir";
                break;
            case 15:
                strFieldName = "FTPUser";
                break;
            case 16:
                strFieldName = "FTPPassWord";
                break;
            case 17:
                strFieldName = "Remark2";
                break;
            case 18:
                strFieldName = "Remark3";
                break;
            case 19:
                strFieldName = "ServerDir";
                break;
            case 20:
                strFieldName = "ServerAddress";
                break;
            case 21:
                strFieldName = "ServerPort";
                break;
            case 22:
                strFieldName = "ServerUser";
                break;
            case 23:
                strFieldName = "ServerPassWord";
                break;
            case 24:
                strFieldName = "Remark4";
                break;
            case 25:
                strFieldName = "Remark5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "ZONENO":
                return Schema.TYPE_STRING;
            case "BANKNODE":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "REMARK1":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FTPADDRESS":
                return Schema.TYPE_STRING;
            case "FTPDIR":
                return Schema.TYPE_STRING;
            case "FTPUSER":
                return Schema.TYPE_STRING;
            case "FTPPASSWORD":
                return Schema.TYPE_STRING;
            case "REMARK2":
                return Schema.TYPE_STRING;
            case "REMARK3":
                return Schema.TYPE_STRING;
            case "SERVERDIR":
                return Schema.TYPE_STRING;
            case "SERVERADDRESS":
                return Schema.TYPE_STRING;
            case "SERVERPORT":
                return Schema.TYPE_INT;
            case "SERVERUSER":
                return Schema.TYPE_STRING;
            case "SERVERPASSWORD":
                return Schema.TYPE_STRING;
            case "REMARK4":
                return Schema.TYPE_STRING;
            case "REMARK5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("ZoneNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZoneNo));
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FTPAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FTPAddress));
        }
        if (FCode.equalsIgnoreCase("FTPDir")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FTPDir));
        }
        if (FCode.equalsIgnoreCase("FTPUser")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FTPUser));
        }
        if (FCode.equalsIgnoreCase("FTPPassWord")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FTPPassWord));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (FCode.equalsIgnoreCase("Remark3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark3));
        }
        if (FCode.equalsIgnoreCase("ServerDir")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServerDir));
        }
        if (FCode.equalsIgnoreCase("ServerAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServerAddress));
        }
        if (FCode.equalsIgnoreCase("ServerPort")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServerPort));
        }
        if (FCode.equalsIgnoreCase("ServerUser")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServerUser));
        }
        if (FCode.equalsIgnoreCase("ServerPassWord")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServerPassWord));
        }
        if (FCode.equalsIgnoreCase("Remark4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark4));
        }
        if (FCode.equalsIgnoreCase("Remark5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ZoneNo);
                break;
            case 2:
                strFieldValue = String.valueOf(BankNode);
                break;
            case 3:
                strFieldValue = String.valueOf(AgentCom);
                break;
            case 4:
                strFieldValue = String.valueOf(ComCode);
                break;
            case 5:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 6:
                strFieldValue = String.valueOf(Operator);
                break;
            case 7:
                strFieldValue = String.valueOf(Remark);
                break;
            case 8:
                strFieldValue = String.valueOf(Remark1);
                break;
            case 9:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 10:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 12:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 13:
                strFieldValue = String.valueOf(FTPAddress);
                break;
            case 14:
                strFieldValue = String.valueOf(FTPDir);
                break;
            case 15:
                strFieldValue = String.valueOf(FTPUser);
                break;
            case 16:
                strFieldValue = String.valueOf(FTPPassWord);
                break;
            case 17:
                strFieldValue = String.valueOf(Remark2);
                break;
            case 18:
                strFieldValue = String.valueOf(Remark3);
                break;
            case 19:
                strFieldValue = String.valueOf(ServerDir);
                break;
            case 20:
                strFieldValue = String.valueOf(ServerAddress);
                break;
            case 21:
                strFieldValue = String.valueOf(ServerPort);
                break;
            case 22:
                strFieldValue = String.valueOf(ServerUser);
                break;
            case 23:
                strFieldValue = String.valueOf(ServerPassWord);
                break;
            case 24:
                strFieldValue = String.valueOf(Remark4);
                break;
            case 25:
                strFieldValue = String.valueOf(Remark5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("ZoneNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZoneNo = FValue.trim();
            }
            else
                ZoneNo = null;
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankNode = FValue.trim();
            }
            else
                BankNode = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Remark1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark1 = FValue.trim();
            }
            else
                Remark1 = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FTPAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                FTPAddress = FValue.trim();
            }
            else
                FTPAddress = null;
        }
        if (FCode.equalsIgnoreCase("FTPDir")) {
            if( FValue != null && !FValue.equals(""))
            {
                FTPDir = FValue.trim();
            }
            else
                FTPDir = null;
        }
        if (FCode.equalsIgnoreCase("FTPUser")) {
            if( FValue != null && !FValue.equals(""))
            {
                FTPUser = FValue.trim();
            }
            else
                FTPUser = null;
        }
        if (FCode.equalsIgnoreCase("FTPPassWord")) {
            if( FValue != null && !FValue.equals(""))
            {
                FTPPassWord = FValue.trim();
            }
            else
                FTPPassWord = null;
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        if (FCode.equalsIgnoreCase("Remark3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark3 = FValue.trim();
            }
            else
                Remark3 = null;
        }
        if (FCode.equalsIgnoreCase("ServerDir")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServerDir = FValue.trim();
            }
            else
                ServerDir = null;
        }
        if (FCode.equalsIgnoreCase("ServerAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServerAddress = FValue.trim();
            }
            else
                ServerAddress = null;
        }
        if (FCode.equalsIgnoreCase("ServerPort")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ServerPort = i;
            }
        }
        if (FCode.equalsIgnoreCase("ServerUser")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServerUser = FValue.trim();
            }
            else
                ServerUser = null;
        }
        if (FCode.equalsIgnoreCase("ServerPassWord")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServerPassWord = FValue.trim();
            }
            else
                ServerPassWord = null;
        }
        if (FCode.equalsIgnoreCase("Remark4")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark4 = FValue.trim();
            }
            else
                Remark4 = null;
        }
        if (FCode.equalsIgnoreCase("Remark5")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark5 = FValue.trim();
            }
            else
                Remark5 = null;
        }
        return true;
    }


    public String toString() {
    return "LKCodeMappingPojo [" +
            "BankCode="+BankCode +
            ", ZoneNo="+ZoneNo +
            ", BankNode="+BankNode +
            ", AgentCom="+AgentCom +
            ", ComCode="+ComCode +
            ", ManageCom="+ManageCom +
            ", Operator="+Operator +
            ", Remark="+Remark +
            ", Remark1="+Remark1 +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", FTPAddress="+FTPAddress +
            ", FTPDir="+FTPDir +
            ", FTPUser="+FTPUser +
            ", FTPPassWord="+FTPPassWord +
            ", Remark2="+Remark2 +
            ", Remark3="+Remark3 +
            ", ServerDir="+ServerDir +
            ", ServerAddress="+ServerAddress +
            ", ServerPort="+ServerPort +
            ", ServerUser="+ServerUser +
            ", ServerPassWord="+ServerPassWord +
            ", Remark4="+Remark4 +
            ", Remark5="+Remark5 +"]";
    }
}
