/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCCIITCMajorDiseaseCheckPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-06-25
 */
public class LCCIITCMajorDiseaseCheckPojo implements  Pojo,Serializable {
    // @Field
    /** 投保单号 */
    private String PrtNo; 
    /** 校验次数 */
    private int CheckNum; 
    /** 超时标记 */
    private String OutTimeFlag; 
    /** 超时原因详述 */
    private String OutTimeDescribe; 
    /** 响应报文 */
    private String ResponseMsg; 
    /** Http状态码 */
    private String HttpCode; 
    /** Body响应码 */
    private String RetCode; 
    /** Body响应信息 */
    private String RetMessage; 
    /** 被保险人客户号 */
    private String InsuredNo; 
    /** 被保险人姓名 */
    private String InsuredName; 
    /** 被保险人证件类型 */
    private String InsuredIDType; 
    /** 被保险人证件号码 */
    private String InsuredIDNO; 
    /** 产品编码 */
    private String ProductCode; 
    /** 非正常核保结论 */
    private String AbnormalCheck; 
    /** 非正常理赔结论 */
    private String AbnormalPayment; 
    /** Ci */
    private String MajorDiseasePayment; 
    /** Ncds */
    private String ChronicDiseasePayment; 
    /** 重疾保额提示 */
    private String MajorDiseaseMoney; 
    /** 多家公司承包提示 */
    private String MultiCompany; 
    /** 是否密集投保 */
    private String Dense; 
    /** 网页查询码（25位） */
    private String PageQueryCode; 
    /** 数据截止日期 */
    private String TagDate; 
    /** 是否有网页数据 */
    private String DisplayPage; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 备用属性字段1 */
    private String StandByFlag1; 
    /** 请求流水号 */
    private String InsurerUuid; 


    public static final int FIELDNUM = 30;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public int getCheckNum() {
        return CheckNum;
    }
    public void setCheckNum(int aCheckNum) {
        CheckNum = aCheckNum;
    }
    public void setCheckNum(String aCheckNum) {
        if (aCheckNum != null && !aCheckNum.equals("")) {
            Integer tInteger = new Integer(aCheckNum);
            int i = tInteger.intValue();
            CheckNum = i;
        }
    }

    public String getOutTimeFlag() {
        return OutTimeFlag;
    }
    public void setOutTimeFlag(String aOutTimeFlag) {
        OutTimeFlag = aOutTimeFlag;
    }
    public String getOutTimeDescribe() {
        return OutTimeDescribe;
    }
    public void setOutTimeDescribe(String aOutTimeDescribe) {
        OutTimeDescribe = aOutTimeDescribe;
    }
    public String getResponseMsg() {
        return ResponseMsg;
    }
    public void setResponseMsg(String aResponseMsg) {
        ResponseMsg = aResponseMsg;
    }
    public String getHttpCode() {
        return HttpCode;
    }
    public void setHttpCode(String aHttpCode) {
        HttpCode = aHttpCode;
    }
    public String getRetCode() {
        return RetCode;
    }
    public void setRetCode(String aRetCode) {
        RetCode = aRetCode;
    }
    public String getRetMessage() {
        return RetMessage;
    }
    public void setRetMessage(String aRetMessage) {
        RetMessage = aRetMessage;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getInsuredIDType() {
        return InsuredIDType;
    }
    public void setInsuredIDType(String aInsuredIDType) {
        InsuredIDType = aInsuredIDType;
    }
    public String getInsuredIDNO() {
        return InsuredIDNO;
    }
    public void setInsuredIDNO(String aInsuredIDNO) {
        InsuredIDNO = aInsuredIDNO;
    }
    public String getProductCode() {
        return ProductCode;
    }
    public void setProductCode(String aProductCode) {
        ProductCode = aProductCode;
    }
    public String getAbnormalCheck() {
        return AbnormalCheck;
    }
    public void setAbnormalCheck(String aAbnormalCheck) {
        AbnormalCheck = aAbnormalCheck;
    }
    public String getAbnormalPayment() {
        return AbnormalPayment;
    }
    public void setAbnormalPayment(String aAbnormalPayment) {
        AbnormalPayment = aAbnormalPayment;
    }
    public String getMajorDiseasePayment() {
        return MajorDiseasePayment;
    }
    public void setMajorDiseasePayment(String aMajorDiseasePayment) {
        MajorDiseasePayment = aMajorDiseasePayment;
    }
    public String getChronicDiseasePayment() {
        return ChronicDiseasePayment;
    }
    public void setChronicDiseasePayment(String aChronicDiseasePayment) {
        ChronicDiseasePayment = aChronicDiseasePayment;
    }
    public String getMajorDiseaseMoney() {
        return MajorDiseaseMoney;
    }
    public void setMajorDiseaseMoney(String aMajorDiseaseMoney) {
        MajorDiseaseMoney = aMajorDiseaseMoney;
    }
    public String getMultiCompany() {
        return MultiCompany;
    }
    public void setMultiCompany(String aMultiCompany) {
        MultiCompany = aMultiCompany;
    }
    public String getDense() {
        return Dense;
    }
    public void setDense(String aDense) {
        Dense = aDense;
    }
    public String getPageQueryCode() {
        return PageQueryCode;
    }
    public void setPageQueryCode(String aPageQueryCode) {
        PageQueryCode = aPageQueryCode;
    }
    public String getTagDate() {
        return TagDate;
    }
    public void setTagDate(String aTagDate) {
        TagDate = aTagDate;
    }
    public String getDisplayPage() {
        return DisplayPage;
    }
    public void setDisplayPage(String aDisplayPage) {
        DisplayPage = aDisplayPage;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getInsurerUuid() {
        return InsurerUuid;
    }
    public void setInsurerUuid(String aInsurerUuid) {
        InsurerUuid = aInsurerUuid;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PrtNo") ) {
            return 0;
        }
        if( strFieldName.equals("CheckNum") ) {
            return 1;
        }
        if( strFieldName.equals("OutTimeFlag") ) {
            return 2;
        }
        if( strFieldName.equals("OutTimeDescribe") ) {
            return 3;
        }
        if( strFieldName.equals("ResponseMsg") ) {
            return 4;
        }
        if( strFieldName.equals("HttpCode") ) {
            return 5;
        }
        if( strFieldName.equals("RetCode") ) {
            return 6;
        }
        if( strFieldName.equals("RetMessage") ) {
            return 7;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 8;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 9;
        }
        if( strFieldName.equals("InsuredIDType") ) {
            return 10;
        }
        if( strFieldName.equals("InsuredIDNO") ) {
            return 11;
        }
        if( strFieldName.equals("ProductCode") ) {
            return 12;
        }
        if( strFieldName.equals("AbnormalCheck") ) {
            return 13;
        }
        if( strFieldName.equals("AbnormalPayment") ) {
            return 14;
        }
        if( strFieldName.equals("MajorDiseasePayment") ) {
            return 15;
        }
        if( strFieldName.equals("ChronicDiseasePayment") ) {
            return 16;
        }
        if( strFieldName.equals("MajorDiseaseMoney") ) {
            return 17;
        }
        if( strFieldName.equals("MultiCompany") ) {
            return 18;
        }
        if( strFieldName.equals("Dense") ) {
            return 19;
        }
        if( strFieldName.equals("PageQueryCode") ) {
            return 20;
        }
        if( strFieldName.equals("TagDate") ) {
            return 21;
        }
        if( strFieldName.equals("DisplayPage") ) {
            return 22;
        }
        if( strFieldName.equals("Operator") ) {
            return 23;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 24;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 25;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 26;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 27;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 28;
        }
        if( strFieldName.equals("InsurerUuid") ) {
            return 29;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PrtNo";
                break;
            case 1:
                strFieldName = "CheckNum";
                break;
            case 2:
                strFieldName = "OutTimeFlag";
                break;
            case 3:
                strFieldName = "OutTimeDescribe";
                break;
            case 4:
                strFieldName = "ResponseMsg";
                break;
            case 5:
                strFieldName = "HttpCode";
                break;
            case 6:
                strFieldName = "RetCode";
                break;
            case 7:
                strFieldName = "RetMessage";
                break;
            case 8:
                strFieldName = "InsuredNo";
                break;
            case 9:
                strFieldName = "InsuredName";
                break;
            case 10:
                strFieldName = "InsuredIDType";
                break;
            case 11:
                strFieldName = "InsuredIDNO";
                break;
            case 12:
                strFieldName = "ProductCode";
                break;
            case 13:
                strFieldName = "AbnormalCheck";
                break;
            case 14:
                strFieldName = "AbnormalPayment";
                break;
            case 15:
                strFieldName = "MajorDiseasePayment";
                break;
            case 16:
                strFieldName = "ChronicDiseasePayment";
                break;
            case 17:
                strFieldName = "MajorDiseaseMoney";
                break;
            case 18:
                strFieldName = "MultiCompany";
                break;
            case 19:
                strFieldName = "Dense";
                break;
            case 20:
                strFieldName = "PageQueryCode";
                break;
            case 21:
                strFieldName = "TagDate";
                break;
            case 22:
                strFieldName = "DisplayPage";
                break;
            case 23:
                strFieldName = "Operator";
                break;
            case 24:
                strFieldName = "MakeDate";
                break;
            case 25:
                strFieldName = "MakeTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "ModifyTime";
                break;
            case 28:
                strFieldName = "StandByFlag1";
                break;
            case 29:
                strFieldName = "InsurerUuid";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "CHECKNUM":
                return Schema.TYPE_INT;
            case "OUTTIMEFLAG":
                return Schema.TYPE_STRING;
            case "OUTTIMEDESCRIBE":
                return Schema.TYPE_STRING;
            case "RESPONSEMSG":
                return Schema.TYPE_STRING;
            case "HTTPCODE":
                return Schema.TYPE_STRING;
            case "RETCODE":
                return Schema.TYPE_STRING;
            case "RETMESSAGE":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "INSUREDIDTYPE":
                return Schema.TYPE_STRING;
            case "INSUREDIDNO":
                return Schema.TYPE_STRING;
            case "PRODUCTCODE":
                return Schema.TYPE_STRING;
            case "ABNORMALCHECK":
                return Schema.TYPE_STRING;
            case "ABNORMALPAYMENT":
                return Schema.TYPE_STRING;
            case "MAJORDISEASEPAYMENT":
                return Schema.TYPE_STRING;
            case "CHRONICDISEASEPAYMENT":
                return Schema.TYPE_STRING;
            case "MAJORDISEASEMONEY":
                return Schema.TYPE_STRING;
            case "MULTICOMPANY":
                return Schema.TYPE_STRING;
            case "DENSE":
                return Schema.TYPE_STRING;
            case "PAGEQUERYCODE":
                return Schema.TYPE_STRING;
            case "TAGDATE":
                return Schema.TYPE_STRING;
            case "DISPLAYPAGE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "INSURERUUID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_INT;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("CheckNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckNum));
        }
        if (FCode.equalsIgnoreCase("OutTimeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutTimeFlag));
        }
        if (FCode.equalsIgnoreCase("OutTimeDescribe")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutTimeDescribe));
        }
        if (FCode.equalsIgnoreCase("ResponseMsg")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResponseMsg));
        }
        if (FCode.equalsIgnoreCase("HttpCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HttpCode));
        }
        if (FCode.equalsIgnoreCase("RetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetCode));
        }
        if (FCode.equalsIgnoreCase("RetMessage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetMessage));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
        }
        if (FCode.equalsIgnoreCase("InsuredIDNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNO));
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProductCode));
        }
        if (FCode.equalsIgnoreCase("AbnormalCheck")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AbnormalCheck));
        }
        if (FCode.equalsIgnoreCase("AbnormalPayment")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AbnormalPayment));
        }
        if (FCode.equalsIgnoreCase("MajorDiseasePayment")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MajorDiseasePayment));
        }
        if (FCode.equalsIgnoreCase("ChronicDiseasePayment")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChronicDiseasePayment));
        }
        if (FCode.equalsIgnoreCase("MajorDiseaseMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MajorDiseaseMoney));
        }
        if (FCode.equalsIgnoreCase("MultiCompany")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MultiCompany));
        }
        if (FCode.equalsIgnoreCase("Dense")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dense));
        }
        if (FCode.equalsIgnoreCase("PageQueryCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageQueryCode));
        }
        if (FCode.equalsIgnoreCase("TagDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TagDate));
        }
        if (FCode.equalsIgnoreCase("DisplayPage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisplayPage));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("InsurerUuid")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurerUuid));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 1:
                strFieldValue = String.valueOf(CheckNum);
                break;
            case 2:
                strFieldValue = String.valueOf(OutTimeFlag);
                break;
            case 3:
                strFieldValue = String.valueOf(OutTimeDescribe);
                break;
            case 4:
                strFieldValue = String.valueOf(ResponseMsg);
                break;
            case 5:
                strFieldValue = String.valueOf(HttpCode);
                break;
            case 6:
                strFieldValue = String.valueOf(RetCode);
                break;
            case 7:
                strFieldValue = String.valueOf(RetMessage);
                break;
            case 8:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 9:
                strFieldValue = String.valueOf(InsuredName);
                break;
            case 10:
                strFieldValue = String.valueOf(InsuredIDType);
                break;
            case 11:
                strFieldValue = String.valueOf(InsuredIDNO);
                break;
            case 12:
                strFieldValue = String.valueOf(ProductCode);
                break;
            case 13:
                strFieldValue = String.valueOf(AbnormalCheck);
                break;
            case 14:
                strFieldValue = String.valueOf(AbnormalPayment);
                break;
            case 15:
                strFieldValue = String.valueOf(MajorDiseasePayment);
                break;
            case 16:
                strFieldValue = String.valueOf(ChronicDiseasePayment);
                break;
            case 17:
                strFieldValue = String.valueOf(MajorDiseaseMoney);
                break;
            case 18:
                strFieldValue = String.valueOf(MultiCompany);
                break;
            case 19:
                strFieldValue = String.valueOf(Dense);
                break;
            case 20:
                strFieldValue = String.valueOf(PageQueryCode);
                break;
            case 21:
                strFieldValue = String.valueOf(TagDate);
                break;
            case 22:
                strFieldValue = String.valueOf(DisplayPage);
                break;
            case 23:
                strFieldValue = String.valueOf(Operator);
                break;
            case 24:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 25:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 26:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 27:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 28:
                strFieldValue = String.valueOf(StandByFlag1);
                break;
            case 29:
                strFieldValue = String.valueOf(InsurerUuid);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("CheckNum")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                CheckNum = i;
            }
        }
        if (FCode.equalsIgnoreCase("OutTimeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutTimeFlag = FValue.trim();
            }
            else
                OutTimeFlag = null;
        }
        if (FCode.equalsIgnoreCase("OutTimeDescribe")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutTimeDescribe = FValue.trim();
            }
            else
                OutTimeDescribe = null;
        }
        if (FCode.equalsIgnoreCase("ResponseMsg")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResponseMsg = FValue.trim();
            }
            else
                ResponseMsg = null;
        }
        if (FCode.equalsIgnoreCase("HttpCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                HttpCode = FValue.trim();
            }
            else
                HttpCode = null;
        }
        if (FCode.equalsIgnoreCase("RetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RetCode = FValue.trim();
            }
            else
                RetCode = null;
        }
        if (FCode.equalsIgnoreCase("RetMessage")) {
            if( FValue != null && !FValue.equals(""))
            {
                RetMessage = FValue.trim();
            }
            else
                RetMessage = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDType = FValue.trim();
            }
            else
                InsuredIDType = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDNO = FValue.trim();
            }
            else
                InsuredIDNO = null;
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProductCode = FValue.trim();
            }
            else
                ProductCode = null;
        }
        if (FCode.equalsIgnoreCase("AbnormalCheck")) {
            if( FValue != null && !FValue.equals(""))
            {
                AbnormalCheck = FValue.trim();
            }
            else
                AbnormalCheck = null;
        }
        if (FCode.equalsIgnoreCase("AbnormalPayment")) {
            if( FValue != null && !FValue.equals(""))
            {
                AbnormalPayment = FValue.trim();
            }
            else
                AbnormalPayment = null;
        }
        if (FCode.equalsIgnoreCase("MajorDiseasePayment")) {
            if( FValue != null && !FValue.equals(""))
            {
                MajorDiseasePayment = FValue.trim();
            }
            else
                MajorDiseasePayment = null;
        }
        if (FCode.equalsIgnoreCase("ChronicDiseasePayment")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChronicDiseasePayment = FValue.trim();
            }
            else
                ChronicDiseasePayment = null;
        }
        if (FCode.equalsIgnoreCase("MajorDiseaseMoney")) {
            if( FValue != null && !FValue.equals(""))
            {
                MajorDiseaseMoney = FValue.trim();
            }
            else
                MajorDiseaseMoney = null;
        }
        if (FCode.equalsIgnoreCase("MultiCompany")) {
            if( FValue != null && !FValue.equals(""))
            {
                MultiCompany = FValue.trim();
            }
            else
                MultiCompany = null;
        }
        if (FCode.equalsIgnoreCase("Dense")) {
            if( FValue != null && !FValue.equals(""))
            {
                Dense = FValue.trim();
            }
            else
                Dense = null;
        }
        if (FCode.equalsIgnoreCase("PageQueryCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PageQueryCode = FValue.trim();
            }
            else
                PageQueryCode = null;
        }
        if (FCode.equalsIgnoreCase("TagDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                TagDate = FValue.trim();
            }
            else
                TagDate = null;
        }
        if (FCode.equalsIgnoreCase("DisplayPage")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisplayPage = FValue.trim();
            }
            else
                DisplayPage = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("InsurerUuid")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsurerUuid = FValue.trim();
            }
            else
                InsurerUuid = null;
        }
        return true;
    }


    public String toString() {
    return "LCCIITCMajorDiseaseCheckPojo [" +
            "PrtNo="+PrtNo +
            ", CheckNum="+CheckNum +
            ", OutTimeFlag="+OutTimeFlag +
            ", OutTimeDescribe="+OutTimeDescribe +
            ", ResponseMsg="+ResponseMsg +
            ", HttpCode="+HttpCode +
            ", RetCode="+RetCode +
            ", RetMessage="+RetMessage +
            ", InsuredNo="+InsuredNo +
            ", InsuredName="+InsuredName +
            ", InsuredIDType="+InsuredIDType +
            ", InsuredIDNO="+InsuredIDNO +
            ", ProductCode="+ProductCode +
            ", AbnormalCheck="+AbnormalCheck +
            ", AbnormalPayment="+AbnormalPayment +
            ", MajorDiseasePayment="+MajorDiseasePayment +
            ", ChronicDiseasePayment="+ChronicDiseasePayment +
            ", MajorDiseaseMoney="+MajorDiseaseMoney +
            ", MultiCompany="+MultiCompany +
            ", Dense="+Dense +
            ", PageQueryCode="+PageQueryCode +
            ", TagDate="+TagDate +
            ", DisplayPage="+DisplayPage +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", StandByFlag1="+StandByFlag1 +
            ", InsurerUuid="+InsurerUuid +"]";
    }
}
