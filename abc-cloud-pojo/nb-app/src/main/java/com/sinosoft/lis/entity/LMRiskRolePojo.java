/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;

import com.sinosoft.cloud.cache.annotation.RedisIndexHKey;
import com.sinosoft.cloud.cache.annotation.RedisPrimaryHKey;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LMRiskRolePojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-05-09
 */
public class LMRiskRolePojo implements  Pojo,Serializable {
    // @Field
    /** 险种编码 */
    @RedisIndexHKey
    @RedisPrimaryHKey
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 角色 */
    @RedisPrimaryHKey
    private String RiskRole;
    /** 性别 */
    @RedisPrimaryHKey
    private String RiskRoleSex;
    /** 序号（级别） */
    @RedisPrimaryHKey
    private String RiskRoleNo;
    /** 最小年龄单位 */
    private String MinAppAgeFlag;
    /** 最小年龄 */
    private int MinAppAge;
    /** 最大年龄单位 */
    private String MAXAppAgeFlag;
    /** 最大年龄 */
    private int MAXAppAge;
    /** 续保最小年龄单位 */
    private String MinRnewAgeFlag;
    /** 续保最小年龄 */
    private int MinRnewAge;
    /** 续保最大年龄单位 */
    private String MaxRnewAgeFlag;
    /** 续保最大年龄 */
    private int MaxRnewAge;


    public static final int FIELDNUM = 13;    // 数据库表的字段个数
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getRiskRole() {
        return RiskRole;
    }
    public void setRiskRole(String aRiskRole) {
        RiskRole = aRiskRole;
    }
    public String getRiskRoleSex() {
        return RiskRoleSex;
    }
    public void setRiskRoleSex(String aRiskRoleSex) {
        RiskRoleSex = aRiskRoleSex;
    }
    public String getRiskRoleNo() {
        return RiskRoleNo;
    }
    public void setRiskRoleNo(String aRiskRoleNo) {
        RiskRoleNo = aRiskRoleNo;
    }
    public String getMinAppAgeFlag() {
        return MinAppAgeFlag;
    }
    public void setMinAppAgeFlag(String aMinAppAgeFlag) {
        MinAppAgeFlag = aMinAppAgeFlag;
    }
    public int getMinAppAge() {
        return MinAppAge;
    }
    public void setMinAppAge(int aMinAppAge) {
        MinAppAge = aMinAppAge;
    }
    public void setMinAppAge(String aMinAppAge) {
        if (aMinAppAge != null && !aMinAppAge.equals("")) {
            Integer tInteger = new Integer(aMinAppAge);
            int i = tInteger.intValue();
            MinAppAge = i;
        }
    }

    public String getMAXAppAgeFlag() {
        return MAXAppAgeFlag;
    }
    public void setMAXAppAgeFlag(String aMAXAppAgeFlag) {
        MAXAppAgeFlag = aMAXAppAgeFlag;
    }
    public int getMAXAppAge() {
        return MAXAppAge;
    }
    public void setMAXAppAge(int aMAXAppAge) {
        MAXAppAge = aMAXAppAge;
    }
    public void setMAXAppAge(String aMAXAppAge) {
        if (aMAXAppAge != null && !aMAXAppAge.equals("")) {
            Integer tInteger = new Integer(aMAXAppAge);
            int i = tInteger.intValue();
            MAXAppAge = i;
        }
    }

    public String getMinRnewAgeFlag() {
        return MinRnewAgeFlag;
    }
    public void setMinRnewAgeFlag(String aMinRnewAgeFlag) {
        MinRnewAgeFlag = aMinRnewAgeFlag;
    }
    public int getMinRnewAge() {
        return MinRnewAge;
    }
    public void setMinRnewAge(int aMinRnewAge) {
        MinRnewAge = aMinRnewAge;
    }
    public void setMinRnewAge(String aMinRnewAge) {
        if (aMinRnewAge != null && !aMinRnewAge.equals("")) {
            Integer tInteger = new Integer(aMinRnewAge);
            int i = tInteger.intValue();
            MinRnewAge = i;
        }
    }

    public String getMaxRnewAgeFlag() {
        return MaxRnewAgeFlag;
    }
    public void setMaxRnewAgeFlag(String aMaxRnewAgeFlag) {
        MaxRnewAgeFlag = aMaxRnewAgeFlag;
    }
    public int getMaxRnewAge() {
        return MaxRnewAge;
    }
    public void setMaxRnewAge(int aMaxRnewAge) {
        MaxRnewAge = aMaxRnewAge;
    }
    public void setMaxRnewAge(String aMaxRnewAge) {
        if (aMaxRnewAge != null && !aMaxRnewAge.equals("")) {
            Integer tInteger = new Integer(aMaxRnewAge);
            int i = tInteger.intValue();
            MaxRnewAge = i;
        }
    }


    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("RiskRole") ) {
            return 2;
        }
        if( strFieldName.equals("RiskRoleSex") ) {
            return 3;
        }
        if( strFieldName.equals("RiskRoleNo") ) {
            return 4;
        }
        if( strFieldName.equals("MinAppAgeFlag") ) {
            return 5;
        }
        if( strFieldName.equals("MinAppAge") ) {
            return 6;
        }
        if( strFieldName.equals("MAXAppAgeFlag") ) {
            return 7;
        }
        if( strFieldName.equals("MAXAppAge") ) {
            return 8;
        }
        if( strFieldName.equals("MinRnewAgeFlag") ) {
            return 9;
        }
        if( strFieldName.equals("MinRnewAge") ) {
            return 10;
        }
        if( strFieldName.equals("MaxRnewAgeFlag") ) {
            return 11;
        }
        if( strFieldName.equals("MaxRnewAge") ) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskRole";
                break;
            case 3:
                strFieldName = "RiskRoleSex";
                break;
            case 4:
                strFieldName = "RiskRoleNo";
                break;
            case 5:
                strFieldName = "MinAppAgeFlag";
                break;
            case 6:
                strFieldName = "MinAppAge";
                break;
            case 7:
                strFieldName = "MAXAppAgeFlag";
                break;
            case 8:
                strFieldName = "MAXAppAge";
                break;
            case 9:
                strFieldName = "MinRnewAgeFlag";
                break;
            case 10:
                strFieldName = "MinRnewAge";
                break;
            case 11:
                strFieldName = "MaxRnewAgeFlag";
                break;
            case 12:
                strFieldName = "MaxRnewAge";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "RISKROLE":
                return Schema.TYPE_STRING;
            case "RISKROLESEX":
                return Schema.TYPE_STRING;
            case "RISKROLENO":
                return Schema.TYPE_STRING;
            case "MINAPPAGEFLAG":
                return Schema.TYPE_STRING;
            case "MINAPPAGE":
                return Schema.TYPE_INT;
            case "MAXAPPAGEFLAG":
                return Schema.TYPE_STRING;
            case "MAXAPPAGE":
                return Schema.TYPE_INT;
            case "MINRNEWAGEFLAG":
                return Schema.TYPE_STRING;
            case "MINRNEWAGE":
                return Schema.TYPE_INT;
            case "MAXRNEWAGEFLAG":
                return Schema.TYPE_STRING;
            case "MAXRNEWAGE":
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_INT;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_INT;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_INT;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskRole")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskRole));
        }
        if (FCode.equalsIgnoreCase("RiskRoleSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskRoleSex));
        }
        if (FCode.equalsIgnoreCase("RiskRoleNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskRoleNo));
        }
        if (FCode.equalsIgnoreCase("MinAppAgeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAppAgeFlag));
        }
        if (FCode.equalsIgnoreCase("MinAppAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAppAge));
        }
        if (FCode.equalsIgnoreCase("MAXAppAgeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXAppAgeFlag));
        }
        if (FCode.equalsIgnoreCase("MAXAppAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXAppAge));
        }
        if (FCode.equalsIgnoreCase("MinRnewAgeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinRnewAgeFlag));
        }
        if (FCode.equalsIgnoreCase("MinRnewAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinRnewAge));
        }
        if (FCode.equalsIgnoreCase("MaxRnewAgeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxRnewAgeFlag));
        }
        if (FCode.equalsIgnoreCase("MaxRnewAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxRnewAge));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(RiskVer);
                break;
            case 2:
                strFieldValue = String.valueOf(RiskRole);
                break;
            case 3:
                strFieldValue = String.valueOf(RiskRoleSex);
                break;
            case 4:
                strFieldValue = String.valueOf(RiskRoleNo);
                break;
            case 5:
                strFieldValue = String.valueOf(MinAppAgeFlag);
                break;
            case 6:
                strFieldValue = String.valueOf(MinAppAge);
                break;
            case 7:
                strFieldValue = String.valueOf(MAXAppAgeFlag);
                break;
            case 8:
                strFieldValue = String.valueOf(MAXAppAge);
                break;
            case 9:
                strFieldValue = String.valueOf(MinRnewAgeFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(MinRnewAge);
                break;
            case 11:
                strFieldValue = String.valueOf(MaxRnewAgeFlag);
                break;
            case 12:
                strFieldValue = String.valueOf(MaxRnewAge);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskRole")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskRole = FValue.trim();
            }
            else
                RiskRole = null;
        }
        if (FCode.equalsIgnoreCase("RiskRoleSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskRoleSex = FValue.trim();
            }
            else
                RiskRoleSex = null;
        }
        if (FCode.equalsIgnoreCase("RiskRoleNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskRoleNo = FValue.trim();
            }
            else
                RiskRoleNo = null;
        }
        if (FCode.equalsIgnoreCase("MinAppAgeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MinAppAgeFlag = FValue.trim();
            }
            else
                MinAppAgeFlag = null;
        }
        if (FCode.equalsIgnoreCase("MinAppAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MinAppAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MAXAppAgeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAXAppAgeFlag = FValue.trim();
            }
            else
                MAXAppAgeFlag = null;
        }
        if (FCode.equalsIgnoreCase("MAXAppAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MAXAppAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MinRnewAgeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MinRnewAgeFlag = FValue.trim();
            }
            else
                MinRnewAgeFlag = null;
        }
        if (FCode.equalsIgnoreCase("MinRnewAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MinRnewAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MaxRnewAgeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MaxRnewAgeFlag = FValue.trim();
            }
            else
                MaxRnewAgeFlag = null;
        }
        if (FCode.equalsIgnoreCase("MaxRnewAge")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MaxRnewAge = i;
            }
        }
        return true;
    }


    public String toString() {
        return "LMRiskRolePojo [" +
                "RiskCode="+RiskCode +
                ", RiskVer="+RiskVer +
                ", RiskRole="+RiskRole +
                ", RiskRoleSex="+RiskRoleSex +
                ", RiskRoleNo="+RiskRoleNo +
                ", MinAppAgeFlag="+MinAppAgeFlag +
                ", MinAppAge="+MinAppAge +
                ", MAXAppAgeFlag="+MAXAppAgeFlag +
                ", MAXAppAge="+MAXAppAge +
                ", MinRnewAgeFlag="+MinRnewAgeFlag +
                ", MinRnewAge="+MinRnewAge +
                ", MaxRnewAgeFlag="+MaxRnewAgeFlag +
                ", MaxRnewAge="+MaxRnewAge +"]";
    }
}
