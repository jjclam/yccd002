/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LMRiskComCtrlPojo </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-17
 */
public class LMRiskComCtrlPojo implements Pojo,Serializable {
    // @Field
    /** 险种编码 */
    private String RiskCode; 
    /** 管理机构 */
    private String ManageComGrp; 
    /** 开办日期 */
    private String  StartDate;
    /** 停办日期 */
    private String  EndDate;
    /** 销售保额上限 */
    private double MAXAmnt; 
    /** 销售份数上限 */
    private double MAXMult; 
    /** 销售保费上限 */
    private double MAXPrem; 
    /** 销售保额下限 */
    private double MINAmnt; 
    /** 销售份数下限 */
    private double MINMult; 
    /** 销售保费下限 */
    private double MINPrem; 


    public static final int FIELDNUM = 10;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getManageComGrp() {
        return ManageComGrp;
    }
    public void setManageComGrp(String aManageComGrp) {
        ManageComGrp = aManageComGrp;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public double getMAXAmnt() {
        return MAXAmnt;
    }
    public void setMAXAmnt(double aMAXAmnt) {
        MAXAmnt = aMAXAmnt;
    }
    public void setMAXAmnt(String aMAXAmnt) {
        if (aMAXAmnt != null && !aMAXAmnt.equals("")) {
            Double tDouble = new Double(aMAXAmnt);
            double d = tDouble.doubleValue();
            MAXAmnt = d;
        }
    }

    public double getMAXMult() {
        return MAXMult;
    }
    public void setMAXMult(double aMAXMult) {
        MAXMult = aMAXMult;
    }
    public void setMAXMult(String aMAXMult) {
        if (aMAXMult != null && !aMAXMult.equals("")) {
            Double tDouble = new Double(aMAXMult);
            double d = tDouble.doubleValue();
            MAXMult = d;
        }
    }

    public double getMAXPrem() {
        return MAXPrem;
    }
    public void setMAXPrem(double aMAXPrem) {
        MAXPrem = aMAXPrem;
    }
    public void setMAXPrem(String aMAXPrem) {
        if (aMAXPrem != null && !aMAXPrem.equals("")) {
            Double tDouble = new Double(aMAXPrem);
            double d = tDouble.doubleValue();
            MAXPrem = d;
        }
    }

    public double getMINAmnt() {
        return MINAmnt;
    }
    public void setMINAmnt(double aMINAmnt) {
        MINAmnt = aMINAmnt;
    }
    public void setMINAmnt(String aMINAmnt) {
        if (aMINAmnt != null && !aMINAmnt.equals("")) {
            Double tDouble = new Double(aMINAmnt);
            double d = tDouble.doubleValue();
            MINAmnt = d;
        }
    }

    public double getMINMult() {
        return MINMult;
    }
    public void setMINMult(double aMINMult) {
        MINMult = aMINMult;
    }
    public void setMINMult(String aMINMult) {
        if (aMINMult != null && !aMINMult.equals("")) {
            Double tDouble = new Double(aMINMult);
            double d = tDouble.doubleValue();
            MINMult = d;
        }
    }

    public double getMINPrem() {
        return MINPrem;
    }
    public void setMINPrem(double aMINPrem) {
        MINPrem = aMINPrem;
    }
    public void setMINPrem(String aMINPrem) {
        if (aMINPrem != null && !aMINPrem.equals("")) {
            Double tDouble = new Double(aMINPrem);
            double d = tDouble.doubleValue();
            MINPrem = d;
        }
    }


    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("ManageComGrp") ) {
            return 1;
        }
        if( strFieldName.equals("StartDate") ) {
            return 2;
        }
        if( strFieldName.equals("EndDate") ) {
            return 3;
        }
        if( strFieldName.equals("MAXAmnt") ) {
            return 4;
        }
        if( strFieldName.equals("MAXMult") ) {
            return 5;
        }
        if( strFieldName.equals("MAXPrem") ) {
            return 6;
        }
        if( strFieldName.equals("MINAmnt") ) {
            return 7;
        }
        if( strFieldName.equals("MINMult") ) {
            return 8;
        }
        if( strFieldName.equals("MINPrem") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "ManageComGrp";
                break;
            case 2:
                strFieldName = "StartDate";
                break;
            case 3:
                strFieldName = "EndDate";
                break;
            case 4:
                strFieldName = "MAXAmnt";
                break;
            case 5:
                strFieldName = "MAXMult";
                break;
            case 6:
                strFieldName = "MAXPrem";
                break;
            case 7:
                strFieldName = "MINAmnt";
                break;
            case 8:
                strFieldName = "MINMult";
                break;
            case 9:
                strFieldName = "MINPrem";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "MANAGECOMGRP":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "MAXAMNT":
                return Schema.TYPE_DOUBLE;
            case "MAXMULT":
                return Schema.TYPE_DOUBLE;
            case "MAXPREM":
                return Schema.TYPE_DOUBLE;
            case "MINAMNT":
                return Schema.TYPE_DOUBLE;
            case "MINMULT":
                return Schema.TYPE_DOUBLE;
            case "MINPREM":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DOUBLE;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("ManageComGrp")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageComGrp));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("MAXAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXAmnt));
        }
        if (FCode.equalsIgnoreCase("MAXMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXMult));
        }
        if (FCode.equalsIgnoreCase("MAXPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXPrem));
        }
        if (FCode.equalsIgnoreCase("MINAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINAmnt));
        }
        if (FCode.equalsIgnoreCase("MINMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINMult));
        }
        if (FCode.equalsIgnoreCase("MINPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINPrem));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 1:
                strFieldValue = String.valueOf(ManageComGrp);
                break;
            case 2:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 3:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 4:
                strFieldValue = String.valueOf(MAXAmnt);
                break;
            case 5:
                strFieldValue = String.valueOf(MAXMult);
                break;
            case 6:
                strFieldValue = String.valueOf(MAXPrem);
                break;
            case 7:
                strFieldValue = String.valueOf(MINAmnt);
                break;
            case 8:
                strFieldValue = String.valueOf(MINMult);
                break;
            case 9:
                strFieldValue = String.valueOf(MINPrem);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("ManageComGrp")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageComGrp = FValue.trim();
            }
            else
                ManageComGrp = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("MAXAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MAXAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("MAXMult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MAXMult = d;
            }
        }
        if (FCode.equalsIgnoreCase("MAXPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MAXPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("MINAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MINAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("MINMult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MINMult = d;
            }
        }
        if (FCode.equalsIgnoreCase("MINPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MINPrem = d;
            }
        }
        return true;
    }


    public String toString() {
    return "LMRiskComCtrlPojo [" +
            "RiskCode="+RiskCode +
            ", ManageComGrp="+ManageComGrp +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", MAXAmnt="+MAXAmnt +
            ", MAXMult="+MAXMult +
            ", MAXPrem="+MAXPrem +
            ", MINAmnt="+MINAmnt +
            ", MINMult="+MINMult +
            ", MINPrem="+MINPrem +"]";
    }
}
