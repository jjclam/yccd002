/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LBPremDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LBPremSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-05-23
 */
public class LBPremSchema implements Schema, Cloneable {
    // @Field
    /** Id */
    private long PremID;
    /** Shardingid */
    private String ShardingID;
    /** 批单号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 保单险种号码 */
    private String PolNo;
    /** 责任编码 */
    private String DutyCode;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 交费计划类型 */
    private String PayPlanType;
    /** 投保人类型 */
    private String AppntType;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 催缴标记 */
    private String UrgePayFlag;
    /** 是否和账户相关 */
    private String NeedAcc;
    /** 已交费次数 */
    private int PayTimes;
    /** 保费分配比率 */
    private double Rate;
    /** 起交日期 */
    private Date PayStartDate;
    /** 终交日期 */
    private Date PayEndDate;
    /** 交至日期 */
    private Date PaytoDate;
    /** 交费间隔 */
    private int PayIntv;
    /** 每期保费 */
    private double StandPrem;
    /** 实际保费 */
    private double Prem;
    /** 累计保费 */
    private double SumPrem;
    /** 额外风险评分 */
    private double SuppRiskScore;
    /** 免交标志 */
    private String FreeFlag;
    /** 免交比率 */
    private double FreeRate;
    /** 免交起期 */
    private Date FreeStartDate;
    /** 免交止期 */
    private Date FreeEndDate;
    /** 状态 */
    private String State;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 加费指向标记 */
    private String AddFeeDirect;
    /** 第二被保人加费评点 */
    private double SecInsuAddPoint;
    /** 加费开始时间类型 */
    private String AddForm;
    /** 期交保费 */
    private double PeriodPrem;
    /** 加费类型 */
    private String PayType;

    public static final int FIELDNUM = 39;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LBPremSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PremID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LBPremSchema cloned = (LBPremSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getPremID() {
        return PremID;
    }
    public void setPremID(long aPremID) {
        PremID = aPremID;
    }
    public void setPremID(String aPremID) {
        if (aPremID != null && !aPremID.equals("")) {
            PremID = new Long(aPremID).longValue();
        }
    }

    public String getShardingID() {
        return ShardingID;
    }
    public void setShardingID(String aShardingID) {
        ShardingID = aShardingID;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getPayPlanCode() {
        return PayPlanCode;
    }
    public void setPayPlanCode(String aPayPlanCode) {
        PayPlanCode = aPayPlanCode;
    }
    public String getPayPlanType() {
        return PayPlanType;
    }
    public void setPayPlanType(String aPayPlanType) {
        PayPlanType = aPayPlanType;
    }
    public String getAppntType() {
        return AppntType;
    }
    public void setAppntType(String aAppntType) {
        AppntType = aAppntType;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getUrgePayFlag() {
        return UrgePayFlag;
    }
    public void setUrgePayFlag(String aUrgePayFlag) {
        UrgePayFlag = aUrgePayFlag;
    }
    public String getNeedAcc() {
        return NeedAcc;
    }
    public void setNeedAcc(String aNeedAcc) {
        NeedAcc = aNeedAcc;
    }
    public int getPayTimes() {
        return PayTimes;
    }
    public void setPayTimes(int aPayTimes) {
        PayTimes = aPayTimes;
    }
    public void setPayTimes(String aPayTimes) {
        if (aPayTimes != null && !aPayTimes.equals("")) {
            Integer tInteger = new Integer(aPayTimes);
            int i = tInteger.intValue();
            PayTimes = i;
        }
    }

    public double getRate() {
        return Rate;
    }
    public void setRate(double aRate) {
        Rate = aRate;
    }
    public void setRate(String aRate) {
        if (aRate != null && !aRate.equals("")) {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public String getPayStartDate() {
        if(PayStartDate != null) {
            return fDate.getString(PayStartDate);
        } else {
            return null;
        }
    }
    public void setPayStartDate(Date aPayStartDate) {
        PayStartDate = aPayStartDate;
    }
    public void setPayStartDate(String aPayStartDate) {
        if (aPayStartDate != null && !aPayStartDate.equals("")) {
            PayStartDate = fDate.getDate(aPayStartDate);
        } else
            PayStartDate = null;
    }

    public String getPayEndDate() {
        if(PayEndDate != null) {
            return fDate.getString(PayEndDate);
        } else {
            return null;
        }
    }
    public void setPayEndDate(Date aPayEndDate) {
        PayEndDate = aPayEndDate;
    }
    public void setPayEndDate(String aPayEndDate) {
        if (aPayEndDate != null && !aPayEndDate.equals("")) {
            PayEndDate = fDate.getDate(aPayEndDate);
        } else
            PayEndDate = null;
    }

    public String getPaytoDate() {
        if(PaytoDate != null) {
            return fDate.getString(PaytoDate);
        } else {
            return null;
        }
    }
    public void setPaytoDate(Date aPaytoDate) {
        PaytoDate = aPaytoDate;
    }
    public void setPaytoDate(String aPaytoDate) {
        if (aPaytoDate != null && !aPaytoDate.equals("")) {
            PaytoDate = fDate.getDate(aPaytoDate);
        } else
            PaytoDate = null;
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public double getStandPrem() {
        return StandPrem;
    }
    public void setStandPrem(double aStandPrem) {
        StandPrem = aStandPrem;
    }
    public void setStandPrem(String aStandPrem) {
        if (aStandPrem != null && !aStandPrem.equals("")) {
            Double tDouble = new Double(aStandPrem);
            double d = tDouble.doubleValue();
            StandPrem = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getSuppRiskScore() {
        return SuppRiskScore;
    }
    public void setSuppRiskScore(double aSuppRiskScore) {
        SuppRiskScore = aSuppRiskScore;
    }
    public void setSuppRiskScore(String aSuppRiskScore) {
        if (aSuppRiskScore != null && !aSuppRiskScore.equals("")) {
            Double tDouble = new Double(aSuppRiskScore);
            double d = tDouble.doubleValue();
            SuppRiskScore = d;
        }
    }

    public String getFreeFlag() {
        return FreeFlag;
    }
    public void setFreeFlag(String aFreeFlag) {
        FreeFlag = aFreeFlag;
    }
    public double getFreeRate() {
        return FreeRate;
    }
    public void setFreeRate(double aFreeRate) {
        FreeRate = aFreeRate;
    }
    public void setFreeRate(String aFreeRate) {
        if (aFreeRate != null && !aFreeRate.equals("")) {
            Double tDouble = new Double(aFreeRate);
            double d = tDouble.doubleValue();
            FreeRate = d;
        }
    }

    public String getFreeStartDate() {
        if(FreeStartDate != null) {
            return fDate.getString(FreeStartDate);
        } else {
            return null;
        }
    }
    public void setFreeStartDate(Date aFreeStartDate) {
        FreeStartDate = aFreeStartDate;
    }
    public void setFreeStartDate(String aFreeStartDate) {
        if (aFreeStartDate != null && !aFreeStartDate.equals("")) {
            FreeStartDate = fDate.getDate(aFreeStartDate);
        } else
            FreeStartDate = null;
    }

    public String getFreeEndDate() {
        if(FreeEndDate != null) {
            return fDate.getString(FreeEndDate);
        } else {
            return null;
        }
    }
    public void setFreeEndDate(Date aFreeEndDate) {
        FreeEndDate = aFreeEndDate;
    }
    public void setFreeEndDate(String aFreeEndDate) {
        if (aFreeEndDate != null && !aFreeEndDate.equals("")) {
            FreeEndDate = fDate.getDate(aFreeEndDate);
        } else
            FreeEndDate = null;
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getAddFeeDirect() {
        return AddFeeDirect;
    }
    public void setAddFeeDirect(String aAddFeeDirect) {
        AddFeeDirect = aAddFeeDirect;
    }
    public double getSecInsuAddPoint() {
        return SecInsuAddPoint;
    }
    public void setSecInsuAddPoint(double aSecInsuAddPoint) {
        SecInsuAddPoint = aSecInsuAddPoint;
    }
    public void setSecInsuAddPoint(String aSecInsuAddPoint) {
        if (aSecInsuAddPoint != null && !aSecInsuAddPoint.equals("")) {
            Double tDouble = new Double(aSecInsuAddPoint);
            double d = tDouble.doubleValue();
            SecInsuAddPoint = d;
        }
    }

    public String getAddForm() {
        return AddForm;
    }
    public void setAddForm(String aAddForm) {
        AddForm = aAddForm;
    }
    public double getPeriodPrem() {
        return PeriodPrem;
    }
    public void setPeriodPrem(double aPeriodPrem) {
        PeriodPrem = aPeriodPrem;
    }
    public void setPeriodPrem(String aPeriodPrem) {
        if (aPeriodPrem != null && !aPeriodPrem.equals("")) {
            Double tDouble = new Double(aPeriodPrem);
            double d = tDouble.doubleValue();
            PeriodPrem = d;
        }
    }

    public String getPayType() {
        return PayType;
    }
    public void setPayType(String aPayType) {
        PayType = aPayType;
    }

    /**
    * 使用另外一个 LBPremSchema 对象给 Schema 赋值
    * @param: aLBPremSchema LBPremSchema
    **/
    public void setSchema(LBPremSchema aLBPremSchema) {
        this.PremID = aLBPremSchema.getPremID();
        this.ShardingID = aLBPremSchema.getShardingID();
        this.EdorNo = aLBPremSchema.getEdorNo();
        this.GrpContNo = aLBPremSchema.getGrpContNo();
        this.ContNo = aLBPremSchema.getContNo();
        this.PolNo = aLBPremSchema.getPolNo();
        this.DutyCode = aLBPremSchema.getDutyCode();
        this.PayPlanCode = aLBPremSchema.getPayPlanCode();
        this.PayPlanType = aLBPremSchema.getPayPlanType();
        this.AppntType = aLBPremSchema.getAppntType();
        this.AppntNo = aLBPremSchema.getAppntNo();
        this.UrgePayFlag = aLBPremSchema.getUrgePayFlag();
        this.NeedAcc = aLBPremSchema.getNeedAcc();
        this.PayTimes = aLBPremSchema.getPayTimes();
        this.Rate = aLBPremSchema.getRate();
        this.PayStartDate = fDate.getDate( aLBPremSchema.getPayStartDate());
        this.PayEndDate = fDate.getDate( aLBPremSchema.getPayEndDate());
        this.PaytoDate = fDate.getDate( aLBPremSchema.getPaytoDate());
        this.PayIntv = aLBPremSchema.getPayIntv();
        this.StandPrem = aLBPremSchema.getStandPrem();
        this.Prem = aLBPremSchema.getPrem();
        this.SumPrem = aLBPremSchema.getSumPrem();
        this.SuppRiskScore = aLBPremSchema.getSuppRiskScore();
        this.FreeFlag = aLBPremSchema.getFreeFlag();
        this.FreeRate = aLBPremSchema.getFreeRate();
        this.FreeStartDate = fDate.getDate( aLBPremSchema.getFreeStartDate());
        this.FreeEndDate = fDate.getDate( aLBPremSchema.getFreeEndDate());
        this.State = aLBPremSchema.getState();
        this.ManageCom = aLBPremSchema.getManageCom();
        this.Operator = aLBPremSchema.getOperator();
        this.MakeDate = fDate.getDate( aLBPremSchema.getMakeDate());
        this.MakeTime = aLBPremSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLBPremSchema.getModifyDate());
        this.ModifyTime = aLBPremSchema.getModifyTime();
        this.AddFeeDirect = aLBPremSchema.getAddFeeDirect();
        this.SecInsuAddPoint = aLBPremSchema.getSecInsuAddPoint();
        this.AddForm = aLBPremSchema.getAddForm();
        this.PeriodPrem = aLBPremSchema.getPeriodPrem();
        this.PayType = aLBPremSchema.getPayType();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.PremID = rs.getLong("PremID");
            if( rs.getString("ShardingID") == null )
                this.ShardingID = null;
            else
                this.ShardingID = rs.getString("ShardingID").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("DutyCode") == null )
                this.DutyCode = null;
            else
                this.DutyCode = rs.getString("DutyCode").trim();

            if( rs.getString("PayPlanCode") == null )
                this.PayPlanCode = null;
            else
                this.PayPlanCode = rs.getString("PayPlanCode").trim();

            if( rs.getString("PayPlanType") == null )
                this.PayPlanType = null;
            else
                this.PayPlanType = rs.getString("PayPlanType").trim();

            if( rs.getString("AppntType") == null )
                this.AppntType = null;
            else
                this.AppntType = rs.getString("AppntType").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("UrgePayFlag") == null )
                this.UrgePayFlag = null;
            else
                this.UrgePayFlag = rs.getString("UrgePayFlag").trim();

            if( rs.getString("NeedAcc") == null )
                this.NeedAcc = null;
            else
                this.NeedAcc = rs.getString("NeedAcc").trim();

            this.PayTimes = rs.getInt("PayTimes");
            this.Rate = rs.getDouble("Rate");
            this.PayStartDate = rs.getDate("PayStartDate");
            this.PayEndDate = rs.getDate("PayEndDate");
            this.PaytoDate = rs.getDate("PaytoDate");
            this.PayIntv = rs.getInt("PayIntv");
            this.StandPrem = rs.getDouble("StandPrem");
            this.Prem = rs.getDouble("Prem");
            this.SumPrem = rs.getDouble("SumPrem");
            this.SuppRiskScore = rs.getDouble("SuppRiskScore");
            if( rs.getString("FreeFlag") == null )
                this.FreeFlag = null;
            else
                this.FreeFlag = rs.getString("FreeFlag").trim();

            this.FreeRate = rs.getDouble("FreeRate");
            this.FreeStartDate = rs.getDate("FreeStartDate");
            this.FreeEndDate = rs.getDate("FreeEndDate");
            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("AddFeeDirect") == null )
                this.AddFeeDirect = null;
            else
                this.AddFeeDirect = rs.getString("AddFeeDirect").trim();

            this.SecInsuAddPoint = rs.getDouble("SecInsuAddPoint");
            if( rs.getString("AddForm") == null )
                this.AddForm = null;
            else
                this.AddForm = rs.getString("AddForm").trim();

            this.PeriodPrem = rs.getDouble("PeriodPrem");
            if( rs.getString("PayType") == null )
                this.PayType = null;
            else
                this.PayType = rs.getString("PayType").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBPremSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LBPremSchema getSchema() {
        LBPremSchema aLBPremSchema = new LBPremSchema();
        aLBPremSchema.setSchema(this);
        return aLBPremSchema;
    }

    public LBPremDB getDB() {
        LBPremDB aDBOper = new LBPremDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBPrem描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(PremID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShardingID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayPlanType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UrgePayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NeedAcc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayTimes));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Rate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PayEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PaytoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SuppRiskScore));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FreeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FreeRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FreeStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FreeEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddFeeDirect)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SecInsuAddPoint));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddForm)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PeriodPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayType));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBPrem>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            PremID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            ShardingID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            PayPlanType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AppntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            UrgePayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            NeedAcc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            PayTimes = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,14, SysConst.PACKAGESPILTER))).intValue();
            Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15, SysConst.PACKAGESPILTER))).doubleValue();
            PayStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            PayEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER));
            PaytoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,19, SysConst.PACKAGESPILTER))).intValue();
            StandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).doubleValue();
            SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22, SysConst.PACKAGESPILTER))).doubleValue();
            SuppRiskScore = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23, SysConst.PACKAGESPILTER))).doubleValue();
            FreeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            FreeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25, SysConst.PACKAGESPILTER))).doubleValue();
            FreeStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            FreeEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            AddFeeDirect = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            SecInsuAddPoint = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36, SysConst.PACKAGESPILTER))).doubleValue();
            AddForm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            PeriodPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38, SysConst.PACKAGESPILTER))).doubleValue();
            PayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBPremSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("PremID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremID));
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShardingID));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
        }
        if (FCode.equalsIgnoreCase("PayPlanType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanType));
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntType));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("UrgePayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UrgePayFlag));
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedAcc));
        }
        if (FCode.equalsIgnoreCase("PayTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayTimes));
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
        }
        if (FCode.equalsIgnoreCase("PayStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayStartDate()));
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayEndDate()));
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("SuppRiskScore")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SuppRiskScore));
        }
        if (FCode.equalsIgnoreCase("FreeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeFlag));
        }
        if (FCode.equalsIgnoreCase("FreeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FreeRate));
        }
        if (FCode.equalsIgnoreCase("FreeStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFreeStartDate()));
        }
        if (FCode.equalsIgnoreCase("FreeEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFreeEndDate()));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("AddFeeDirect")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddFeeDirect));
        }
        if (FCode.equalsIgnoreCase("SecInsuAddPoint")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SecInsuAddPoint));
        }
        if (FCode.equalsIgnoreCase("AddForm")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddForm));
        }
        if (FCode.equalsIgnoreCase("PeriodPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeriodPrem));
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(PremID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ShardingID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PayPlanType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AppntType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(UrgePayFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(NeedAcc);
                break;
            case 13:
                strFieldValue = String.valueOf(PayTimes);
                break;
            case 14:
                strFieldValue = String.valueOf(Rate);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayStartDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayEndDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPaytoDate()));
                break;
            case 18:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 19:
                strFieldValue = String.valueOf(StandPrem);
                break;
            case 20:
                strFieldValue = String.valueOf(Prem);
                break;
            case 21:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 22:
                strFieldValue = String.valueOf(SuppRiskScore);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(FreeFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(FreeRate);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFreeStartDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFreeEndDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(AddFeeDirect);
                break;
            case 35:
                strFieldValue = String.valueOf(SecInsuAddPoint);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(AddForm);
                break;
            case 37:
                strFieldValue = String.valueOf(PeriodPrem);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(PayType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("PremID")) {
            if( FValue != null && !FValue.equals("")) {
                PremID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("ShardingID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShardingID = FValue.trim();
            }
            else
                ShardingID = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
                PayPlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PayPlanType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayPlanType = FValue.trim();
            }
            else
                PayPlanType = null;
        }
        if (FCode.equalsIgnoreCase("AppntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntType = FValue.trim();
            }
            else
                AppntType = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("UrgePayFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UrgePayFlag = FValue.trim();
            }
            else
                UrgePayFlag = null;
        }
        if (FCode.equalsIgnoreCase("NeedAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                NeedAcc = FValue.trim();
            }
            else
                NeedAcc = null;
        }
        if (FCode.equalsIgnoreCase("PayTimes")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayTimes = i;
            }
        }
        if (FCode.equalsIgnoreCase("Rate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayStartDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayStartDate = fDate.getDate( FValue );
            }
            else
                PayStartDate = null;
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                PayEndDate = fDate.getDate( FValue );
            }
            else
                PayEndDate = null;
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if(FValue != null && !FValue.equals("")) {
                PaytoDate = fDate.getDate( FValue );
            }
            else
                PaytoDate = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("StandPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SuppRiskScore")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SuppRiskScore = d;
            }
        }
        if (FCode.equalsIgnoreCase("FreeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FreeFlag = FValue.trim();
            }
            else
                FreeFlag = null;
        }
        if (FCode.equalsIgnoreCase("FreeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FreeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FreeStartDate")) {
            if(FValue != null && !FValue.equals("")) {
                FreeStartDate = fDate.getDate( FValue );
            }
            else
                FreeStartDate = null;
        }
        if (FCode.equalsIgnoreCase("FreeEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                FreeEndDate = fDate.getDate( FValue );
            }
            else
                FreeEndDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("AddFeeDirect")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddFeeDirect = FValue.trim();
            }
            else
                AddFeeDirect = null;
        }
        if (FCode.equalsIgnoreCase("SecInsuAddPoint")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SecInsuAddPoint = d;
            }
        }
        if (FCode.equalsIgnoreCase("AddForm")) {
            if( FValue != null && !FValue.equals(""))
            {
                AddForm = FValue.trim();
            }
            else
                AddForm = null;
        }
        if (FCode.equalsIgnoreCase("PeriodPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                PeriodPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayType = FValue.trim();
            }
            else
                PayType = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LBPremSchema other = (LBPremSchema)otherObject;
        return
            PremID == other.getPremID()
            && ShardingID.equals(other.getShardingID())
            && EdorNo.equals(other.getEdorNo())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && PolNo.equals(other.getPolNo())
            && DutyCode.equals(other.getDutyCode())
            && PayPlanCode.equals(other.getPayPlanCode())
            && PayPlanType.equals(other.getPayPlanType())
            && AppntType.equals(other.getAppntType())
            && AppntNo.equals(other.getAppntNo())
            && UrgePayFlag.equals(other.getUrgePayFlag())
            && NeedAcc.equals(other.getNeedAcc())
            && PayTimes == other.getPayTimes()
            && Rate == other.getRate()
            && fDate.getString(PayStartDate).equals(other.getPayStartDate())
            && fDate.getString(PayEndDate).equals(other.getPayEndDate())
            && fDate.getString(PaytoDate).equals(other.getPaytoDate())
            && PayIntv == other.getPayIntv()
            && StandPrem == other.getStandPrem()
            && Prem == other.getPrem()
            && SumPrem == other.getSumPrem()
            && SuppRiskScore == other.getSuppRiskScore()
            && FreeFlag.equals(other.getFreeFlag())
            && FreeRate == other.getFreeRate()
            && fDate.getString(FreeStartDate).equals(other.getFreeStartDate())
            && fDate.getString(FreeEndDate).equals(other.getFreeEndDate())
            && State.equals(other.getState())
            && ManageCom.equals(other.getManageCom())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && AddFeeDirect.equals(other.getAddFeeDirect())
            && SecInsuAddPoint == other.getSecInsuAddPoint()
            && AddForm.equals(other.getAddForm())
            && PeriodPrem == other.getPeriodPrem()
            && PayType.equals(other.getPayType());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("PremID") ) {
            return 0;
        }
        if( strFieldName.equals("ShardingID") ) {
            return 1;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 2;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 3;
        }
        if( strFieldName.equals("ContNo") ) {
            return 4;
        }
        if( strFieldName.equals("PolNo") ) {
            return 5;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 6;
        }
        if( strFieldName.equals("PayPlanCode") ) {
            return 7;
        }
        if( strFieldName.equals("PayPlanType") ) {
            return 8;
        }
        if( strFieldName.equals("AppntType") ) {
            return 9;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 10;
        }
        if( strFieldName.equals("UrgePayFlag") ) {
            return 11;
        }
        if( strFieldName.equals("NeedAcc") ) {
            return 12;
        }
        if( strFieldName.equals("PayTimes") ) {
            return 13;
        }
        if( strFieldName.equals("Rate") ) {
            return 14;
        }
        if( strFieldName.equals("PayStartDate") ) {
            return 15;
        }
        if( strFieldName.equals("PayEndDate") ) {
            return 16;
        }
        if( strFieldName.equals("PaytoDate") ) {
            return 17;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 18;
        }
        if( strFieldName.equals("StandPrem") ) {
            return 19;
        }
        if( strFieldName.equals("Prem") ) {
            return 20;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 21;
        }
        if( strFieldName.equals("SuppRiskScore") ) {
            return 22;
        }
        if( strFieldName.equals("FreeFlag") ) {
            return 23;
        }
        if( strFieldName.equals("FreeRate") ) {
            return 24;
        }
        if( strFieldName.equals("FreeStartDate") ) {
            return 25;
        }
        if( strFieldName.equals("FreeEndDate") ) {
            return 26;
        }
        if( strFieldName.equals("State") ) {
            return 27;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 28;
        }
        if( strFieldName.equals("Operator") ) {
            return 29;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 30;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 31;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 32;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 33;
        }
        if( strFieldName.equals("AddFeeDirect") ) {
            return 34;
        }
        if( strFieldName.equals("SecInsuAddPoint") ) {
            return 35;
        }
        if( strFieldName.equals("AddForm") ) {
            return 36;
        }
        if( strFieldName.equals("PeriodPrem") ) {
            return 37;
        }
        if( strFieldName.equals("PayType") ) {
            return 38;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "PremID";
                break;
            case 1:
                strFieldName = "ShardingID";
                break;
            case 2:
                strFieldName = "EdorNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "ContNo";
                break;
            case 5:
                strFieldName = "PolNo";
                break;
            case 6:
                strFieldName = "DutyCode";
                break;
            case 7:
                strFieldName = "PayPlanCode";
                break;
            case 8:
                strFieldName = "PayPlanType";
                break;
            case 9:
                strFieldName = "AppntType";
                break;
            case 10:
                strFieldName = "AppntNo";
                break;
            case 11:
                strFieldName = "UrgePayFlag";
                break;
            case 12:
                strFieldName = "NeedAcc";
                break;
            case 13:
                strFieldName = "PayTimes";
                break;
            case 14:
                strFieldName = "Rate";
                break;
            case 15:
                strFieldName = "PayStartDate";
                break;
            case 16:
                strFieldName = "PayEndDate";
                break;
            case 17:
                strFieldName = "PaytoDate";
                break;
            case 18:
                strFieldName = "PayIntv";
                break;
            case 19:
                strFieldName = "StandPrem";
                break;
            case 20:
                strFieldName = "Prem";
                break;
            case 21:
                strFieldName = "SumPrem";
                break;
            case 22:
                strFieldName = "SuppRiskScore";
                break;
            case 23:
                strFieldName = "FreeFlag";
                break;
            case 24:
                strFieldName = "FreeRate";
                break;
            case 25:
                strFieldName = "FreeStartDate";
                break;
            case 26:
                strFieldName = "FreeEndDate";
                break;
            case 27:
                strFieldName = "State";
                break;
            case 28:
                strFieldName = "ManageCom";
                break;
            case 29:
                strFieldName = "Operator";
                break;
            case 30:
                strFieldName = "MakeDate";
                break;
            case 31:
                strFieldName = "MakeTime";
                break;
            case 32:
                strFieldName = "ModifyDate";
                break;
            case 33:
                strFieldName = "ModifyTime";
                break;
            case 34:
                strFieldName = "AddFeeDirect";
                break;
            case 35:
                strFieldName = "SecInsuAddPoint";
                break;
            case 36:
                strFieldName = "AddForm";
                break;
            case 37:
                strFieldName = "PeriodPrem";
                break;
            case 38:
                strFieldName = "PayType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PREMID":
                return Schema.TYPE_LONG;
            case "SHARDINGID":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANCODE":
                return Schema.TYPE_STRING;
            case "PAYPLANTYPE":
                return Schema.TYPE_STRING;
            case "APPNTTYPE":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "URGEPAYFLAG":
                return Schema.TYPE_STRING;
            case "NEEDACC":
                return Schema.TYPE_STRING;
            case "PAYTIMES":
                return Schema.TYPE_INT;
            case "RATE":
                return Schema.TYPE_DOUBLE;
            case "PAYSTARTDATE":
                return Schema.TYPE_DATE;
            case "PAYENDDATE":
                return Schema.TYPE_DATE;
            case "PAYTODATE":
                return Schema.TYPE_DATE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "STANDPREM":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "SUPPRISKSCORE":
                return Schema.TYPE_DOUBLE;
            case "FREEFLAG":
                return Schema.TYPE_STRING;
            case "FREERATE":
                return Schema.TYPE_DOUBLE;
            case "FREESTARTDATE":
                return Schema.TYPE_DATE;
            case "FREEENDDATE":
                return Schema.TYPE_DATE;
            case "STATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "ADDFEEDIRECT":
                return Schema.TYPE_STRING;
            case "SECINSUADDPOINT":
                return Schema.TYPE_DOUBLE;
            case "ADDFORM":
                return Schema.TYPE_STRING;
            case "PERIODPREM":
                return Schema.TYPE_DOUBLE;
            case "PAYTYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_INT;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DOUBLE;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_DATE;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_DATE;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_DATE;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_DOUBLE;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_DOUBLE;
            case 38:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
