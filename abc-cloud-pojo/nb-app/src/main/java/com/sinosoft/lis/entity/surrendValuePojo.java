/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: surrendValuePojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class surrendValuePojo implements  Pojo,Serializable {
    // @Field
    /** 请求报文id */
    private String reqMsgId; 
    /** 机构流水号 */
    private String instSerialNo; 
    /** 渠道编码 */
    private String sellType; 
    /** 渠道退保订单号 */
    private String endorseNo; 
    /** 渠道退保申请时间 */
    private String  endorseCreateTime;
    /** 渠道退保时间 */
    private String  endorseTime;
    /** 退保原因类型 */
    private String endorseReason; 
    /** 退保原因描述 */
    private String endorseReasonDesc; 
    /** 退保金额 */
    private String endorseFee; 
    /** 保单号 */
    private String contNo; 
    /** 渠道保单号 */
    private String policyNo; 
    /** 渠道产品编号 */
    private String productCode; 
    /** 产品编号 */
    private String riskCode; 
    /** 退保性质 */
    private String surrenderReason; 
    /** 退保犹豫期类型 */
    private String hesitationType; 
    /** 收入账户 */
    private String inAccount; 
    /** 支出账号 */
    private String outAccount; 
    /** 支付发生时间 */
    private String  payTime;
    /** 发生金额 */
    private String fee; 
    /** 支付流水号 */
    private String payFlowId; 
    /** 优惠金额 */
    private String discountFee; 


    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getReqMsgId() {
        return reqMsgId;
    }
    public void setReqMsgId(String areqMsgId) {
        reqMsgId = areqMsgId;
    }
    public String getInstSerialNo() {
        return instSerialNo;
    }
    public void setInstSerialNo(String ainstSerialNo) {
        instSerialNo = ainstSerialNo;
    }
    public String getSellType() {
        return sellType;
    }
    public void setSellType(String asellType) {
        sellType = asellType;
    }
    public String getEndorseNo() {
        return endorseNo;
    }
    public void setEndorseNo(String aendorseNo) {
        endorseNo = aendorseNo;
    }
    public String getEndorseCreateTime() {
        return endorseCreateTime;
    }
    public void setEndorseCreateTime(String aendorseCreateTime) {
        endorseCreateTime = aendorseCreateTime;
    }
    public String getEndorseTime() {
        return endorseTime;
    }
    public void setEndorseTime(String aendorseTime) {
        endorseTime = aendorseTime;
    }
    public String getEndorseReason() {
        return endorseReason;
    }
    public void setEndorseReason(String aendorseReason) {
        endorseReason = aendorseReason;
    }
    public String getEndorseReasonDesc() {
        return endorseReasonDesc;
    }
    public void setEndorseReasonDesc(String aendorseReasonDesc) {
        endorseReasonDesc = aendorseReasonDesc;
    }
    public String getEndorseFee() {
        return endorseFee;
    }
    public void setEndorseFee(String aendorseFee) {
        endorseFee = aendorseFee;
    }
    public String getContNo() {
        return contNo;
    }
    public void setContNo(String acontNo) {
        contNo = acontNo;
    }
    public String getPolicyNo() {
        return policyNo;
    }
    public void setPolicyNo(String apolicyNo) {
        policyNo = apolicyNo;
    }
    public String getProductCode() {
        return productCode;
    }
    public void setProductCode(String aproductCode) {
        productCode = aproductCode;
    }
    public String getRiskCode() {
        return riskCode;
    }
    public void setRiskCode(String ariskCode) {
        riskCode = ariskCode;
    }
    public String getSurrenderReason() {
        return surrenderReason;
    }
    public void setSurrenderReason(String asurrenderReason) {
        surrenderReason = asurrenderReason;
    }
    public String getHesitationType() {
        return hesitationType;
    }
    public void setHesitationType(String ahesitationType) {
        hesitationType = ahesitationType;
    }
    public String getInAccount() {
        return inAccount;
    }
    public void setInAccount(String ainAccount) {
        inAccount = ainAccount;
    }
    public String getOutAccount() {
        return outAccount;
    }
    public void setOutAccount(String aoutAccount) {
        outAccount = aoutAccount;
    }
    public String getPayTime() {
        return payTime;
    }
    public void setPayTime(String apayTime) {
        payTime = apayTime;
    }
    public String getFee() {
        return fee;
    }
    public void setFee(String afee) {
        fee = afee;
    }
    public String getPayFlowId() {
        return payFlowId;
    }
    public void setPayFlowId(String apayFlowId) {
        payFlowId = apayFlowId;
    }
    public String getDiscountFee() {
        return discountFee;
    }
    public void setDiscountFee(String adiscountFee) {
        discountFee = adiscountFee;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("reqMsgId") ) {
            return 0;
        }
        if( strFieldName.equals("instSerialNo") ) {
            return 1;
        }
        if( strFieldName.equals("sellType") ) {
            return 2;
        }
        if( strFieldName.equals("endorseNo") ) {
            return 3;
        }
        if( strFieldName.equals("endorseCreateTime") ) {
            return 4;
        }
        if( strFieldName.equals("endorseTime") ) {
            return 5;
        }
        if( strFieldName.equals("endorseReason") ) {
            return 6;
        }
        if( strFieldName.equals("endorseReasonDesc") ) {
            return 7;
        }
        if( strFieldName.equals("endorseFee") ) {
            return 8;
        }
        if( strFieldName.equals("contNo") ) {
            return 9;
        }
        if( strFieldName.equals("policyNo") ) {
            return 10;
        }
        if( strFieldName.equals("productCode") ) {
            return 11;
        }
        if( strFieldName.equals("riskCode") ) {
            return 12;
        }
        if( strFieldName.equals("surrenderReason") ) {
            return 13;
        }
        if( strFieldName.equals("hesitationType") ) {
            return 14;
        }
        if( strFieldName.equals("inAccount") ) {
            return 15;
        }
        if( strFieldName.equals("outAccount") ) {
            return 16;
        }
        if( strFieldName.equals("payTime") ) {
            return 17;
        }
        if( strFieldName.equals("fee") ) {
            return 18;
        }
        if( strFieldName.equals("payFlowId") ) {
            return 19;
        }
        if( strFieldName.equals("discountFee") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "reqMsgId";
                break;
            case 1:
                strFieldName = "instSerialNo";
                break;
            case 2:
                strFieldName = "sellType";
                break;
            case 3:
                strFieldName = "endorseNo";
                break;
            case 4:
                strFieldName = "endorseCreateTime";
                break;
            case 5:
                strFieldName = "endorseTime";
                break;
            case 6:
                strFieldName = "endorseReason";
                break;
            case 7:
                strFieldName = "endorseReasonDesc";
                break;
            case 8:
                strFieldName = "endorseFee";
                break;
            case 9:
                strFieldName = "contNo";
                break;
            case 10:
                strFieldName = "policyNo";
                break;
            case 11:
                strFieldName = "productCode";
                break;
            case 12:
                strFieldName = "riskCode";
                break;
            case 13:
                strFieldName = "surrenderReason";
                break;
            case 14:
                strFieldName = "hesitationType";
                break;
            case 15:
                strFieldName = "inAccount";
                break;
            case 16:
                strFieldName = "outAccount";
                break;
            case 17:
                strFieldName = "payTime";
                break;
            case 18:
                strFieldName = "fee";
                break;
            case 19:
                strFieldName = "payFlowId";
                break;
            case 20:
                strFieldName = "discountFee";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "REQMSGID":
                return Schema.TYPE_STRING;
            case "INSTSERIALNO":
                return Schema.TYPE_STRING;
            case "SELLTYPE":
                return Schema.TYPE_STRING;
            case "ENDORSENO":
                return Schema.TYPE_STRING;
            case "ENDORSECREATETIME":
                return Schema.TYPE_STRING;
            case "ENDORSETIME":
                return Schema.TYPE_STRING;
            case "ENDORSEREASON":
                return Schema.TYPE_STRING;
            case "ENDORSEREASONDESC":
                return Schema.TYPE_STRING;
            case "ENDORSEFEE":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "POLICYNO":
                return Schema.TYPE_STRING;
            case "PRODUCTCODE":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "SURRENDERREASON":
                return Schema.TYPE_STRING;
            case "HESITATIONTYPE":
                return Schema.TYPE_STRING;
            case "INACCOUNT":
                return Schema.TYPE_STRING;
            case "OUTACCOUNT":
                return Schema.TYPE_STRING;
            case "PAYTIME":
                return Schema.TYPE_STRING;
            case "FEE":
                return Schema.TYPE_STRING;
            case "PAYFLOWID":
                return Schema.TYPE_STRING;
            case "DISCOUNTFEE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("reqMsgId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(reqMsgId));
        }
        if (FCode.equalsIgnoreCase("instSerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(instSerialNo));
        }
        if (FCode.equalsIgnoreCase("sellType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(sellType));
        }
        if (FCode.equalsIgnoreCase("endorseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseNo));
        }
        if (FCode.equalsIgnoreCase("endorseCreateTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseCreateTime));
        }
        if (FCode.equalsIgnoreCase("endorseTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseTime));
        }
        if (FCode.equalsIgnoreCase("endorseReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseReason));
        }
        if (FCode.equalsIgnoreCase("endorseReasonDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseReasonDesc));
        }
        if (FCode.equalsIgnoreCase("endorseFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endorseFee));
        }
        if (FCode.equalsIgnoreCase("contNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(contNo));
        }
        if (FCode.equalsIgnoreCase("policyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
        }
        if (FCode.equalsIgnoreCase("productCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(productCode));
        }
        if (FCode.equalsIgnoreCase("riskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(riskCode));
        }
        if (FCode.equalsIgnoreCase("surrenderReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(surrenderReason));
        }
        if (FCode.equalsIgnoreCase("hesitationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(hesitationType));
        }
        if (FCode.equalsIgnoreCase("inAccount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(inAccount));
        }
        if (FCode.equalsIgnoreCase("outAccount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(outAccount));
        }
        if (FCode.equalsIgnoreCase("payTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(payTime));
        }
        if (FCode.equalsIgnoreCase("fee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(fee));
        }
        if (FCode.equalsIgnoreCase("payFlowId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(payFlowId));
        }
        if (FCode.equalsIgnoreCase("discountFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(discountFee));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(reqMsgId);
                break;
            case 1:
                strFieldValue = String.valueOf(instSerialNo);
                break;
            case 2:
                strFieldValue = String.valueOf(sellType);
                break;
            case 3:
                strFieldValue = String.valueOf(endorseNo);
                break;
            case 4:
                strFieldValue = String.valueOf(endorseCreateTime);
                break;
            case 5:
                strFieldValue = String.valueOf(endorseTime);
                break;
            case 6:
                strFieldValue = String.valueOf(endorseReason);
                break;
            case 7:
                strFieldValue = String.valueOf(endorseReasonDesc);
                break;
            case 8:
                strFieldValue = String.valueOf(endorseFee);
                break;
            case 9:
                strFieldValue = String.valueOf(contNo);
                break;
            case 10:
                strFieldValue = String.valueOf(policyNo);
                break;
            case 11:
                strFieldValue = String.valueOf(productCode);
                break;
            case 12:
                strFieldValue = String.valueOf(riskCode);
                break;
            case 13:
                strFieldValue = String.valueOf(surrenderReason);
                break;
            case 14:
                strFieldValue = String.valueOf(hesitationType);
                break;
            case 15:
                strFieldValue = String.valueOf(inAccount);
                break;
            case 16:
                strFieldValue = String.valueOf(outAccount);
                break;
            case 17:
                strFieldValue = String.valueOf(payTime);
                break;
            case 18:
                strFieldValue = String.valueOf(fee);
                break;
            case 19:
                strFieldValue = String.valueOf(payFlowId);
                break;
            case 20:
                strFieldValue = String.valueOf(discountFee);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("reqMsgId")) {
            if( FValue != null && !FValue.equals(""))
            {
                reqMsgId = FValue.trim();
            }
            else
                reqMsgId = null;
        }
        if (FCode.equalsIgnoreCase("instSerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                instSerialNo = FValue.trim();
            }
            else
                instSerialNo = null;
        }
        if (FCode.equalsIgnoreCase("sellType")) {
            if( FValue != null && !FValue.equals(""))
            {
                sellType = FValue.trim();
            }
            else
                sellType = null;
        }
        if (FCode.equalsIgnoreCase("endorseNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseNo = FValue.trim();
            }
            else
                endorseNo = null;
        }
        if (FCode.equalsIgnoreCase("endorseCreateTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseCreateTime = FValue.trim();
            }
            else
                endorseCreateTime = null;
        }
        if (FCode.equalsIgnoreCase("endorseTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseTime = FValue.trim();
            }
            else
                endorseTime = null;
        }
        if (FCode.equalsIgnoreCase("endorseReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseReason = FValue.trim();
            }
            else
                endorseReason = null;
        }
        if (FCode.equalsIgnoreCase("endorseReasonDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseReasonDesc = FValue.trim();
            }
            else
                endorseReasonDesc = null;
        }
        if (FCode.equalsIgnoreCase("endorseFee")) {
            if( FValue != null && !FValue.equals(""))
            {
                endorseFee = FValue.trim();
            }
            else
                endorseFee = null;
        }
        if (FCode.equalsIgnoreCase("contNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                contNo = FValue.trim();
            }
            else
                contNo = null;
        }
        if (FCode.equalsIgnoreCase("policyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                policyNo = FValue.trim();
            }
            else
                policyNo = null;
        }
        if (FCode.equalsIgnoreCase("productCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                productCode = FValue.trim();
            }
            else
                productCode = null;
        }
        if (FCode.equalsIgnoreCase("riskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                riskCode = FValue.trim();
            }
            else
                riskCode = null;
        }
        if (FCode.equalsIgnoreCase("surrenderReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                surrenderReason = FValue.trim();
            }
            else
                surrenderReason = null;
        }
        if (FCode.equalsIgnoreCase("hesitationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                hesitationType = FValue.trim();
            }
            else
                hesitationType = null;
        }
        if (FCode.equalsIgnoreCase("inAccount")) {
            if( FValue != null && !FValue.equals(""))
            {
                inAccount = FValue.trim();
            }
            else
                inAccount = null;
        }
        if (FCode.equalsIgnoreCase("outAccount")) {
            if( FValue != null && !FValue.equals(""))
            {
                outAccount = FValue.trim();
            }
            else
                outAccount = null;
        }
        if (FCode.equalsIgnoreCase("payTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                payTime = FValue.trim();
            }
            else
                payTime = null;
        }
        if (FCode.equalsIgnoreCase("fee")) {
            if( FValue != null && !FValue.equals(""))
            {
                fee = FValue.trim();
            }
            else
                fee = null;
        }
        if (FCode.equalsIgnoreCase("payFlowId")) {
            if( FValue != null && !FValue.equals(""))
            {
                payFlowId = FValue.trim();
            }
            else
                payFlowId = null;
        }
        if (FCode.equalsIgnoreCase("discountFee")) {
            if( FValue != null && !FValue.equals(""))
            {
                discountFee = FValue.trim();
            }
            else
                discountFee = null;
        }
        return true;
    }


    public String toString() {
    return "surrendValuePojo [" +
            "reqMsgId="+reqMsgId +
            ", instSerialNo="+instSerialNo +
            ", sellType="+sellType +
            ", endorseNo="+endorseNo +
            ", endorseCreateTime="+endorseCreateTime +
            ", endorseTime="+endorseTime +
            ", endorseReason="+endorseReason +
            ", endorseReasonDesc="+endorseReasonDesc +
            ", endorseFee="+endorseFee +
            ", contNo="+contNo +
            ", policyNo="+policyNo +
            ", productCode="+productCode +
            ", riskCode="+riskCode +
            ", surrenderReason="+surrenderReason +
            ", hesitationType="+hesitationType +
            ", inAccount="+inAccount +
            ", outAccount="+outAccount +
            ", payTime="+payTime +
            ", fee="+fee +
            ", payFlowId="+payFlowId +
            ", discountFee="+discountFee +"]";
    }
}
