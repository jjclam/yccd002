/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */


package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LETransOrderStatusPojo </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-12-03
 */
public class LETransOrderStatusPojo implements  Pojo,Serializable {
    // @Field
    /** 订单号 */
    private String SerialNo; 
    /** 交易类型 */
    private String TransType; 
    /** 交易名称 */
    private String TradeName; 
    /** 合作伙伴代码 */
    private String InstitutionCode; 
    /** Orderserialnumber */
    private String OrderSerialNumber; 
    /** 业务流水号 */
    private String TransRefGUID; 
    /** 产品代码 */
    private String ProductCode; 
    /** 产品名称 */
    private String ProductName; 
    /** 保单号 */
    private String PolicySerialNumber; 
    /** 投保单号 */
    private String ApplicationNumber; 
    /** 对账单号 */
    private String CheckPayNo; 
    /** 缴费订单号 */
    private String PayOrderSerialNumber; 
    /** 流程状态 */
    private String ProcessState; 
    /** 结果状态 */
    private String ResultStatus; 
    /** 结果描述 */
    private String ResultInfoDesc; 
    /** 结果信息 */
    private String InfoMessage; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后修改日期 */
    private String  ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime; 


    public static final int FIELDNUM = 20;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getTransType() {
        return TransType;
    }
    public void setTransType(String aTransType) {
        TransType = aTransType;
    }
    public String getTradeName() {
        return TradeName;
    }
    public void setTradeName(String aTradeName) {
        TradeName = aTradeName;
    }
    public String getInstitutionCode() {
        return InstitutionCode;
    }
    public void setInstitutionCode(String aInstitutionCode) {
        InstitutionCode = aInstitutionCode;
    }
    public String getOrderSerialNumber() {
        return OrderSerialNumber;
    }
    public void setOrderSerialNumber(String aOrderSerialNumber) {
        OrderSerialNumber = aOrderSerialNumber;
    }
    public String getTransRefGUID() {
        return TransRefGUID;
    }
    public void setTransRefGUID(String aTransRefGUID) {
        TransRefGUID = aTransRefGUID;
    }
    public String getProductCode() {
        return ProductCode;
    }
    public void setProductCode(String aProductCode) {
        ProductCode = aProductCode;
    }
    public String getProductName() {
        return ProductName;
    }
    public void setProductName(String aProductName) {
        ProductName = aProductName;
    }
    public String getPolicySerialNumber() {
        return PolicySerialNumber;
    }
    public void setPolicySerialNumber(String aPolicySerialNumber) {
        PolicySerialNumber = aPolicySerialNumber;
    }
    public String getApplicationNumber() {
        return ApplicationNumber;
    }
    public void setApplicationNumber(String aApplicationNumber) {
        ApplicationNumber = aApplicationNumber;
    }
    public String getCheckPayNo() {
        return CheckPayNo;
    }
    public void setCheckPayNo(String aCheckPayNo) {
        CheckPayNo = aCheckPayNo;
    }
    public String getPayOrderSerialNumber() {
        return PayOrderSerialNumber;
    }
    public void setPayOrderSerialNumber(String aPayOrderSerialNumber) {
        PayOrderSerialNumber = aPayOrderSerialNumber;
    }
    public String getProcessState() {
        return ProcessState;
    }
    public void setProcessState(String aProcessState) {
        ProcessState = aProcessState;
    }
    public String getResultStatus() {
        return ResultStatus;
    }
    public void setResultStatus(String aResultStatus) {
        ResultStatus = aResultStatus;
    }
    public String getResultInfoDesc() {
        return ResultInfoDesc;
    }
    public void setResultInfoDesc(String aResultInfoDesc) {
        ResultInfoDesc = aResultInfoDesc;
    }
    public String getInfoMessage() {
        return InfoMessage;
    }
    public void setInfoMessage(String aInfoMessage) {
        InfoMessage = aInfoMessage;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("TransType") ) {
            return 1;
        }
        if( strFieldName.equals("TradeName") ) {
            return 2;
        }
        if( strFieldName.equals("InstitutionCode") ) {
            return 3;
        }
        if( strFieldName.equals("OrderSerialNumber") ) {
            return 4;
        }
        if( strFieldName.equals("TransRefGUID") ) {
            return 5;
        }
        if( strFieldName.equals("ProductCode") ) {
            return 6;
        }
        if( strFieldName.equals("ProductName") ) {
            return 7;
        }
        if( strFieldName.equals("PolicySerialNumber") ) {
            return 8;
        }
        if( strFieldName.equals("ApplicationNumber") ) {
            return 9;
        }
        if( strFieldName.equals("CheckPayNo") ) {
            return 10;
        }
        if( strFieldName.equals("PayOrderSerialNumber") ) {
            return 11;
        }
        if( strFieldName.equals("ProcessState") ) {
            return 12;
        }
        if( strFieldName.equals("ResultStatus") ) {
            return 13;
        }
        if( strFieldName.equals("ResultInfoDesc") ) {
            return 14;
        }
        if( strFieldName.equals("InfoMessage") ) {
            return 15;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 16;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 18;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 19;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "TransType";
                break;
            case 2:
                strFieldName = "TradeName";
                break;
            case 3:
                strFieldName = "InstitutionCode";
                break;
            case 4:
                strFieldName = "OrderSerialNumber";
                break;
            case 5:
                strFieldName = "TransRefGUID";
                break;
            case 6:
                strFieldName = "ProductCode";
                break;
            case 7:
                strFieldName = "ProductName";
                break;
            case 8:
                strFieldName = "PolicySerialNumber";
                break;
            case 9:
                strFieldName = "ApplicationNumber";
                break;
            case 10:
                strFieldName = "CheckPayNo";
                break;
            case 11:
                strFieldName = "PayOrderSerialNumber";
                break;
            case 12:
                strFieldName = "ProcessState";
                break;
            case 13:
                strFieldName = "ResultStatus";
                break;
            case 14:
                strFieldName = "ResultInfoDesc";
                break;
            case 15:
                strFieldName = "InfoMessage";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "TRANSTYPE":
                return Schema.TYPE_STRING;
            case "TRADENAME":
                return Schema.TYPE_STRING;
            case "INSTITUTIONCODE":
                return Schema.TYPE_STRING;
            case "ORDERSERIALNUMBER":
                return Schema.TYPE_STRING;
            case "TRANSREFGUID":
                return Schema.TYPE_STRING;
            case "PRODUCTCODE":
                return Schema.TYPE_STRING;
            case "PRODUCTNAME":
                return Schema.TYPE_STRING;
            case "POLICYSERIALNUMBER":
                return Schema.TYPE_STRING;
            case "APPLICATIONNUMBER":
                return Schema.TYPE_STRING;
            case "CHECKPAYNO":
                return Schema.TYPE_STRING;
            case "PAYORDERSERIALNUMBER":
                return Schema.TYPE_STRING;
            case "PROCESSSTATE":
                return Schema.TYPE_STRING;
            case "RESULTSTATUS":
                return Schema.TYPE_STRING;
            case "RESULTINFODESC":
                return Schema.TYPE_STRING;
            case "INFOMESSAGE":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("TransType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
        }
        if (FCode.equalsIgnoreCase("TradeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TradeName));
        }
        if (FCode.equalsIgnoreCase("InstitutionCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InstitutionCode));
        }
        if (FCode.equalsIgnoreCase("OrderSerialNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderSerialNumber));
        }
        if (FCode.equalsIgnoreCase("TransRefGUID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TransRefGUID));
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProductCode));
        }
        if (FCode.equalsIgnoreCase("ProductName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProductName));
        }
        if (FCode.equalsIgnoreCase("PolicySerialNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolicySerialNumber));
        }
        if (FCode.equalsIgnoreCase("ApplicationNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplicationNumber));
        }
        if (FCode.equalsIgnoreCase("CheckPayNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckPayNo));
        }
        if (FCode.equalsIgnoreCase("PayOrderSerialNumber")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayOrderSerialNumber));
        }
        if (FCode.equalsIgnoreCase("ProcessState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProcessState));
        }
        if (FCode.equalsIgnoreCase("ResultStatus")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultStatus));
        }
        if (FCode.equalsIgnoreCase("ResultInfoDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ResultInfoDesc));
        }
        if (FCode.equalsIgnoreCase("InfoMessage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InfoMessage));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(TransType);
                break;
            case 2:
                strFieldValue = String.valueOf(TradeName);
                break;
            case 3:
                strFieldValue = String.valueOf(InstitutionCode);
                break;
            case 4:
                strFieldValue = String.valueOf(OrderSerialNumber);
                break;
            case 5:
                strFieldValue = String.valueOf(TransRefGUID);
                break;
            case 6:
                strFieldValue = String.valueOf(ProductCode);
                break;
            case 7:
                strFieldValue = String.valueOf(ProductName);
                break;
            case 8:
                strFieldValue = String.valueOf(PolicySerialNumber);
                break;
            case 9:
                strFieldValue = String.valueOf(ApplicationNumber);
                break;
            case 10:
                strFieldValue = String.valueOf(CheckPayNo);
                break;
            case 11:
                strFieldValue = String.valueOf(PayOrderSerialNumber);
                break;
            case 12:
                strFieldValue = String.valueOf(ProcessState);
                break;
            case 13:
                strFieldValue = String.valueOf(ResultStatus);
                break;
            case 14:
                strFieldValue = String.valueOf(ResultInfoDesc);
                break;
            case 15:
                strFieldValue = String.valueOf(InfoMessage);
                break;
            case 16:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 17:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 18:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 19:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("TransType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransType = FValue.trim();
            }
            else
                TransType = null;
        }
        if (FCode.equalsIgnoreCase("TradeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                TradeName = FValue.trim();
            }
            else
                TradeName = null;
        }
        if (FCode.equalsIgnoreCase("InstitutionCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InstitutionCode = FValue.trim();
            }
            else
                InstitutionCode = null;
        }
        if (FCode.equalsIgnoreCase("OrderSerialNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                OrderSerialNumber = FValue.trim();
            }
            else
                OrderSerialNumber = null;
        }
        if (FCode.equalsIgnoreCase("TransRefGUID")) {
            if( FValue != null && !FValue.equals(""))
            {
                TransRefGUID = FValue.trim();
            }
            else
                TransRefGUID = null;
        }
        if (FCode.equalsIgnoreCase("ProductCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProductCode = FValue.trim();
            }
            else
                ProductCode = null;
        }
        if (FCode.equalsIgnoreCase("ProductName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProductName = FValue.trim();
            }
            else
                ProductName = null;
        }
        if (FCode.equalsIgnoreCase("PolicySerialNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolicySerialNumber = FValue.trim();
            }
            else
                PolicySerialNumber = null;
        }
        if (FCode.equalsIgnoreCase("ApplicationNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplicationNumber = FValue.trim();
            }
            else
                ApplicationNumber = null;
        }
        if (FCode.equalsIgnoreCase("CheckPayNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CheckPayNo = FValue.trim();
            }
            else
                CheckPayNo = null;
        }
        if (FCode.equalsIgnoreCase("PayOrderSerialNumber")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayOrderSerialNumber = FValue.trim();
            }
            else
                PayOrderSerialNumber = null;
        }
        if (FCode.equalsIgnoreCase("ProcessState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProcessState = FValue.trim();
            }
            else
                ProcessState = null;
        }
        if (FCode.equalsIgnoreCase("ResultStatus")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultStatus = FValue.trim();
            }
            else
                ResultStatus = null;
        }
        if (FCode.equalsIgnoreCase("ResultInfoDesc")) {
            if( FValue != null && !FValue.equals(""))
            {
                ResultInfoDesc = FValue.trim();
            }
            else
                ResultInfoDesc = null;
        }
        if (FCode.equalsIgnoreCase("InfoMessage")) {
            if( FValue != null && !FValue.equals(""))
            {
                InfoMessage = FValue.trim();
            }
            else
                InfoMessage = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        return true;
    }


    public String toString() {
    return "LETransOrderStatusPojo [" +
            "SerialNo="+SerialNo +
            ", TransType="+TransType +
            ", TradeName="+TradeName +
            ", InstitutionCode="+InstitutionCode +
            ", OrderSerialNumber="+OrderSerialNumber +
            ", TransRefGUID="+TransRefGUID +
            ", ProductCode="+ProductCode +
            ", ProductName="+ProductName +
            ", PolicySerialNumber="+PolicySerialNumber +
            ", ApplicationNumber="+ApplicationNumber +
            ", CheckPayNo="+CheckPayNo +
            ", PayOrderSerialNumber="+PayOrderSerialNumber +
            ", ProcessState="+ProcessState +
            ", ResultStatus="+ResultStatus +
            ", ResultInfoDesc="+ResultInfoDesc +
            ", InfoMessage="+InfoMessage +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +"]";
    }
}
