/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMRiskDutyDB;

/**
 * <p>ClassName: LMRiskDutySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-10-16
 */
public class LMRiskDutySchema implements Schema, Cloneable {
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 责任代码 */
    private String DutyCode;
    /** 选择标记 */
    private String ChoFlag;
    /** 个案标记 */
    private String SpecFlag;
    /** 责任分类 */
    private String Dutytype;
    /** 账单类型 */
    private String Accprop;

    public static final int FIELDNUM = 7;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMRiskDutySchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "DutyCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMRiskDutySchema cloned = (LMRiskDutySchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getChoFlag() {
        return ChoFlag;
    }
    public void setChoFlag(String aChoFlag) {
        ChoFlag = aChoFlag;
    }
    public String getSpecFlag() {
        return SpecFlag;
    }
    public void setSpecFlag(String aSpecFlag) {
        SpecFlag = aSpecFlag;
    }
    public String getDutytype() {
        return Dutytype;
    }
    public void setDutytype(String aDutytype) {
        Dutytype = aDutytype;
    }
    public String getAccprop() {
        return Accprop;
    }
    public void setAccprop(String aAccprop) {
        Accprop = aAccprop;
    }

    /**
    * 使用另外一个 LMRiskDutySchema 对象给 Schema 赋值
    * @param: aLMRiskDutySchema LMRiskDutySchema
    **/
    public void setSchema(LMRiskDutySchema aLMRiskDutySchema) {
        this.RiskCode = aLMRiskDutySchema.getRiskCode();
        this.RiskVer = aLMRiskDutySchema.getRiskVer();
        this.DutyCode = aLMRiskDutySchema.getDutyCode();
        this.ChoFlag = aLMRiskDutySchema.getChoFlag();
        this.SpecFlag = aLMRiskDutySchema.getSpecFlag();
        this.Dutytype = aLMRiskDutySchema.getDutytype();
        this.Accprop = aLMRiskDutySchema.getAccprop();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("RiskVer") == null )
                this.RiskVer = null;
            else
                this.RiskVer = rs.getString("RiskVer").trim();

            if( rs.getString("DutyCode") == null )
                this.DutyCode = null;
            else
                this.DutyCode = rs.getString("DutyCode").trim();

            if( rs.getString("ChoFlag") == null )
                this.ChoFlag = null;
            else
                this.ChoFlag = rs.getString("ChoFlag").trim();

            if( rs.getString("SpecFlag") == null )
                this.SpecFlag = null;
            else
                this.SpecFlag = rs.getString("SpecFlag").trim();

            if( rs.getString("Dutytype") == null )
                this.Dutytype = null;
            else
                this.Dutytype = rs.getString("Dutytype").trim();

            if( rs.getString("Accprop") == null )
                this.Accprop = null;
            else
                this.Accprop = rs.getString("Accprop").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskDutySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMRiskDutySchema getSchema() {
        LMRiskDutySchema aLMRiskDutySchema = new LMRiskDutySchema();
        aLMRiskDutySchema.setSchema(this);
        return aLMRiskDutySchema;
    }

    public LMRiskDutyDB getDB() {
        LMRiskDutyDB aDBOper = new LMRiskDutyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskDuty描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVer)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChoFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Dutytype)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Accprop));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskDuty>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ChoFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            SpecFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Dutytype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Accprop = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskDutySchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("ChoFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChoFlag));
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecFlag));
        }
        if (FCode.equalsIgnoreCase("Dutytype")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dutytype));
        }
        if (FCode.equalsIgnoreCase("Accprop")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Accprop));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ChoFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SpecFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Dutytype);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Accprop);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("ChoFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChoFlag = FValue.trim();
            }
            else
                ChoFlag = null;
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpecFlag = FValue.trim();
            }
            else
                SpecFlag = null;
        }
        if (FCode.equalsIgnoreCase("Dutytype")) {
            if( FValue != null && !FValue.equals(""))
            {
                Dutytype = FValue.trim();
            }
            else
                Dutytype = null;
        }
        if (FCode.equalsIgnoreCase("Accprop")) {
            if( FValue != null && !FValue.equals(""))
            {
                Accprop = FValue.trim();
            }
            else
                Accprop = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMRiskDutySchema other = (LMRiskDutySchema)otherObject;
        return
            RiskCode.equals(other.getRiskCode())
            && RiskVer.equals(other.getRiskVer())
            && DutyCode.equals(other.getDutyCode())
            && ChoFlag.equals(other.getChoFlag())
            && SpecFlag.equals(other.getSpecFlag())
            && Dutytype.equals(other.getDutytype())
            && Accprop.equals(other.getAccprop());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 2;
        }
        if( strFieldName.equals("ChoFlag") ) {
            return 3;
        }
        if( strFieldName.equals("SpecFlag") ) {
            return 4;
        }
        if( strFieldName.equals("Dutytype") ) {
            return 5;
        }
        if( strFieldName.equals("Accprop") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "ChoFlag";
                break;
            case 4:
                strFieldName = "SpecFlag";
                break;
            case 5:
                strFieldName = "Dutytype";
                break;
            case 6:
                strFieldName = "Accprop";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "CHOFLAG":
                return Schema.TYPE_STRING;
            case "SPECFLAG":
                return Schema.TYPE_STRING;
            case "DUTYTYPE":
                return Schema.TYPE_STRING;
            case "ACCPROP":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
