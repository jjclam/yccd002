/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LMProdRulesSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LMProdRulesDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-10-09
 */
public class LMProdRulesDBSet extends LMProdRulesSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LMProdRulesDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LMProdRules");
        mflag = true;
    }

    public LMProdRulesDBSet() {
        db = new DBOper( "LMProdRules" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMProdRulesDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LMProdRules WHERE  1=1  AND RulesCode = ? AND RulesType = ? AND RiskCode = ? AND RulesChnl = ? AND RulesMngCom = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRulesCode() == null || this.get(i).getRulesCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRulesCode());
            }
            if(this.get(i).getRulesType() == null || this.get(i).getRulesType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getRulesType());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getRiskCode());
            }
            if(this.get(i).getRulesChnl() == null || this.get(i).getRulesChnl().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getRulesChnl());
            }
            if(this.get(i).getRulesMngCom() == null || this.get(i).getRulesMngCom().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getRulesMngCom());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMProdRulesDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LMProdRules SET  RulesCode = ? , RulesType = ? , RulesTypeName = ? , RiskCode = ? , RiskVersion = ? , RiskName = ? , RulesChnl = ? , RulesMngCom = ? , RulesProperty1 = ? , RulesProperty2 = ? , RulesProperty3 = ? , RulesProperty5 = ? , RulesProperty4 = ? , RulesProperty6 = ? , CalCode = ? , RulesOrder = ? , Message = ? , ShowMsgFlag = ? , Remark = ? , CalType = ? , ReviewFlag = ? WHERE  1=1  AND RulesCode = ? AND RulesType = ? AND RiskCode = ? AND RulesChnl = ? AND RulesMngCom = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRulesCode() == null || this.get(i).getRulesCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRulesCode());
            }
            if(this.get(i).getRulesType() == null || this.get(i).getRulesType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getRulesType());
            }
            if(this.get(i).getRulesTypeName() == null || this.get(i).getRulesTypeName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getRulesTypeName());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getRiskCode());
            }
            if(this.get(i).getRiskVersion() == null || this.get(i).getRiskVersion().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getRiskVersion());
            }
            if(this.get(i).getRiskName() == null || this.get(i).getRiskName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getRiskName());
            }
            if(this.get(i).getRulesChnl() == null || this.get(i).getRulesChnl().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getRulesChnl());
            }
            if(this.get(i).getRulesMngCom() == null || this.get(i).getRulesMngCom().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getRulesMngCom());
            }
            if(this.get(i).getRulesProperty1() == null || this.get(i).getRulesProperty1().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getRulesProperty1());
            }
            if(this.get(i).getRulesProperty2() == null || this.get(i).getRulesProperty2().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getRulesProperty2());
            }
            if(this.get(i).getRulesProperty3() == null || this.get(i).getRulesProperty3().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getRulesProperty3());
            }
            if(this.get(i).getRulesProperty5() == null || this.get(i).getRulesProperty5().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getRulesProperty5());
            }
            if(this.get(i).getRulesProperty4() == null || this.get(i).getRulesProperty4().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getRulesProperty4());
            }
            if(this.get(i).getRulesProperty6() == null || this.get(i).getRulesProperty6().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getRulesProperty6());
            }
            if(this.get(i).getCalCode() == null || this.get(i).getCalCode().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getCalCode());
            }
            pstmt.setInt(16, this.get(i).getRulesOrder());
            if(this.get(i).getMessage() == null || this.get(i).getMessage().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getMessage());
            }
            if(this.get(i).getShowMsgFlag() == null || this.get(i).getShowMsgFlag().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getShowMsgFlag());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getRemark());
            }
            if(this.get(i).getCalType() == null || this.get(i).getCalType().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getCalType());
            }
            if(this.get(i).getReviewFlag() == null || this.get(i).getReviewFlag().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getReviewFlag());
            }
            // set where condition
            if(this.get(i).getRulesCode() == null || this.get(i).getRulesCode().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getRulesCode());
            }
            if(this.get(i).getRulesType() == null || this.get(i).getRulesType().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getRulesType());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getRiskCode());
            }
            if(this.get(i).getRulesChnl() == null || this.get(i).getRulesChnl().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getRulesChnl());
            }
            if(this.get(i).getRulesMngCom() == null || this.get(i).getRulesMngCom().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getRulesMngCom());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMProdRulesDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LMProdRules VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRulesCode() == null || this.get(i).getRulesCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRulesCode());
            }
            if(this.get(i).getRulesType() == null || this.get(i).getRulesType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getRulesType());
            }
            if(this.get(i).getRulesTypeName() == null || this.get(i).getRulesTypeName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getRulesTypeName());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getRiskCode());
            }
            if(this.get(i).getRiskVersion() == null || this.get(i).getRiskVersion().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getRiskVersion());
            }
            if(this.get(i).getRiskName() == null || this.get(i).getRiskName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getRiskName());
            }
            if(this.get(i).getRulesChnl() == null || this.get(i).getRulesChnl().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getRulesChnl());
            }
            if(this.get(i).getRulesMngCom() == null || this.get(i).getRulesMngCom().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getRulesMngCom());
            }
            if(this.get(i).getRulesProperty1() == null || this.get(i).getRulesProperty1().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getRulesProperty1());
            }
            if(this.get(i).getRulesProperty2() == null || this.get(i).getRulesProperty2().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getRulesProperty2());
            }
            if(this.get(i).getRulesProperty3() == null || this.get(i).getRulesProperty3().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getRulesProperty3());
            }
            if(this.get(i).getRulesProperty5() == null || this.get(i).getRulesProperty5().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getRulesProperty5());
            }
            if(this.get(i).getRulesProperty4() == null || this.get(i).getRulesProperty4().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getRulesProperty4());
            }
            if(this.get(i).getRulesProperty6() == null || this.get(i).getRulesProperty6().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getRulesProperty6());
            }
            if(this.get(i).getCalCode() == null || this.get(i).getCalCode().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getCalCode());
            }
            pstmt.setInt(16, this.get(i).getRulesOrder());
            if(this.get(i).getMessage() == null || this.get(i).getMessage().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getMessage());
            }
            if(this.get(i).getShowMsgFlag() == null || this.get(i).getShowMsgFlag().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getShowMsgFlag());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getRemark());
            }
            if(this.get(i).getCalType() == null || this.get(i).getCalType().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getCalType());
            }
            if(this.get(i).getReviewFlag() == null || this.get(i).getReviewFlag().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getReviewFlag());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMProdRulesDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
