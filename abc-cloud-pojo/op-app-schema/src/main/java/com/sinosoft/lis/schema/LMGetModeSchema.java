/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMGetModeDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LMGetModeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMGetModeSchema implements Schema, Cloneable {
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 给付代码 */
    private String GetDutyCode;
    /** 给付名称 */
    private String GetDutyName;
    /** 给付责任类型 */
    private String GetDutyKind;

    public static final int FIELDNUM = 5;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMGetModeSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";
        pk[2] = "GetDutyCode";
        pk[3] = "GetDutyKind";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMGetModeSchema cloned = (LMGetModeSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getGetDutyCode() {
        return GetDutyCode;
    }
    public void setGetDutyCode(String aGetDutyCode) {
        GetDutyCode = aGetDutyCode;
    }
    public String getGetDutyName() {
        return GetDutyName;
    }
    public void setGetDutyName(String aGetDutyName) {
        GetDutyName = aGetDutyName;
    }
    public String getGetDutyKind() {
        return GetDutyKind;
    }
    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }

    /**
    * 使用另外一个 LMGetModeSchema 对象给 Schema 赋值
    * @param: aLMGetModeSchema LMGetModeSchema
    **/
    public void setSchema(LMGetModeSchema aLMGetModeSchema) {
        this.RiskCode = aLMGetModeSchema.getRiskCode();
        this.RiskVer = aLMGetModeSchema.getRiskVer();
        this.GetDutyCode = aLMGetModeSchema.getGetDutyCode();
        this.GetDutyName = aLMGetModeSchema.getGetDutyName();
        this.GetDutyKind = aLMGetModeSchema.getGetDutyKind();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("RiskVer") == null )
                this.RiskVer = null;
            else
                this.RiskVer = rs.getString("RiskVer").trim();

            if( rs.getString("GetDutyCode") == null )
                this.GetDutyCode = null;
            else
                this.GetDutyCode = rs.getString("GetDutyCode").trim();

            if( rs.getString("GetDutyName") == null )
                this.GetDutyName = null;
            else
                this.GetDutyName = rs.getString("GetDutyName").trim();

            if( rs.getString("GetDutyKind") == null )
                this.GetDutyKind = null;
            else
                this.GetDutyKind = rs.getString("GetDutyKind").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMGetModeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMGetModeSchema getSchema() {
        LMGetModeSchema aLMGetModeSchema = new LMGetModeSchema();
        aLMGetModeSchema.setSchema(this);
        return aLMGetModeSchema;
    }

    public LMGetModeDB getDB() {
        LMGetModeDB aDBOper = new LMGetModeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMGetMode描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVer)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyKind));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMGetMode>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            GetDutyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMGetModeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyName));
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GetDutyName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
                GetDutyCode = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
                GetDutyName = null;
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
                GetDutyKind = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMGetModeSchema other = (LMGetModeSchema)otherObject;
        return
            RiskCode.equals(other.getRiskCode())
            && RiskVer.equals(other.getRiskVer())
            && GetDutyCode.equals(other.getGetDutyCode())
            && GetDutyName.equals(other.getGetDutyName())
            && GetDutyKind.equals(other.getGetDutyKind());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("GetDutyCode") ) {
            return 2;
        }
        if( strFieldName.equals("GetDutyName") ) {
            return 3;
        }
        if( strFieldName.equals("GetDutyKind") ) {
            return 4;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "GetDutyCode";
                break;
            case 3:
                strFieldName = "GetDutyName";
                break;
            case 4:
                strFieldName = "GetDutyKind";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "GETDUTYCODE":
                return Schema.TYPE_STRING;
            case "GETDUTYNAME":
                return Schema.TYPE_STRING;
            case "GETDUTYKIND":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
