/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LAStateDataDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LAStateDataSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-27
 */
public class LAStateDataSchema implements Schema, Cloneable {
    // @Field
    /** Objectid */
    private String ObjectId;
    /** Startdate */
    private String StartDate;
    /** Enddate */
    private String EndDate;
    /** Branchtype */
    private String BranchType;
    /** Statetype */
    private String StateType;
    /** T1 */
    private double T1;
    /** T2 */
    private double T2;
    /** T3 */
    private double T3;
    /** T4 */
    private double T4;
    /** T5 */
    private double T5;
    /** T6 */
    private double T6;
    /** T7 */
    private double T7;
    /** T8 */
    private double T8;
    /** T9 */
    private double T9;
    /** T10 */
    private double T10;
    /** T11 */
    private double T11;
    /** T12 */
    private double T12;
    /** T13 */
    private double T13;
    /** T14 */
    private double T14;
    /** T15 */
    private double T15;
    /** T16 */
    private double T16;
    /** T17 */
    private double T17;
    /** T18 */
    private double T18;
    /** T19 */
    private double T19;
    /** T20 */
    private double T20;

    public static final int FIELDNUM = 25;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LAStateDataSchema() {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "ObjectId";
        pk[1] = "StartDate";
        pk[2] = "EndDate";
        pk[3] = "BranchType";
        pk[4] = "StateType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAStateDataSchema cloned = (LAStateDataSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getObjectId() {
        return ObjectId;
    }
    public void setObjectId(String aObjectId) {
        ObjectId = aObjectId;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getStateType() {
        return StateType;
    }
    public void setStateType(String aStateType) {
        StateType = aStateType;
    }
    public double getT1() {
        return T1;
    }
    public void setT1(double aT1) {
        T1 = aT1;
    }
    public void setT1(String aT1) {
        if (aT1 != null && !aT1.equals("")) {
            Double tDouble = new Double(aT1);
            double d = tDouble.doubleValue();
            T1 = d;
        }
    }

    public double getT2() {
        return T2;
    }
    public void setT2(double aT2) {
        T2 = aT2;
    }
    public void setT2(String aT2) {
        if (aT2 != null && !aT2.equals("")) {
            Double tDouble = new Double(aT2);
            double d = tDouble.doubleValue();
            T2 = d;
        }
    }

    public double getT3() {
        return T3;
    }
    public void setT3(double aT3) {
        T3 = aT3;
    }
    public void setT3(String aT3) {
        if (aT3 != null && !aT3.equals("")) {
            Double tDouble = new Double(aT3);
            double d = tDouble.doubleValue();
            T3 = d;
        }
    }

    public double getT4() {
        return T4;
    }
    public void setT4(double aT4) {
        T4 = aT4;
    }
    public void setT4(String aT4) {
        if (aT4 != null && !aT4.equals("")) {
            Double tDouble = new Double(aT4);
            double d = tDouble.doubleValue();
            T4 = d;
        }
    }

    public double getT5() {
        return T5;
    }
    public void setT5(double aT5) {
        T5 = aT5;
    }
    public void setT5(String aT5) {
        if (aT5 != null && !aT5.equals("")) {
            Double tDouble = new Double(aT5);
            double d = tDouble.doubleValue();
            T5 = d;
        }
    }

    public double getT6() {
        return T6;
    }
    public void setT6(double aT6) {
        T6 = aT6;
    }
    public void setT6(String aT6) {
        if (aT6 != null && !aT6.equals("")) {
            Double tDouble = new Double(aT6);
            double d = tDouble.doubleValue();
            T6 = d;
        }
    }

    public double getT7() {
        return T7;
    }
    public void setT7(double aT7) {
        T7 = aT7;
    }
    public void setT7(String aT7) {
        if (aT7 != null && !aT7.equals("")) {
            Double tDouble = new Double(aT7);
            double d = tDouble.doubleValue();
            T7 = d;
        }
    }

    public double getT8() {
        return T8;
    }
    public void setT8(double aT8) {
        T8 = aT8;
    }
    public void setT8(String aT8) {
        if (aT8 != null && !aT8.equals("")) {
            Double tDouble = new Double(aT8);
            double d = tDouble.doubleValue();
            T8 = d;
        }
    }

    public double getT9() {
        return T9;
    }
    public void setT9(double aT9) {
        T9 = aT9;
    }
    public void setT9(String aT9) {
        if (aT9 != null && !aT9.equals("")) {
            Double tDouble = new Double(aT9);
            double d = tDouble.doubleValue();
            T9 = d;
        }
    }

    public double getT10() {
        return T10;
    }
    public void setT10(double aT10) {
        T10 = aT10;
    }
    public void setT10(String aT10) {
        if (aT10 != null && !aT10.equals("")) {
            Double tDouble = new Double(aT10);
            double d = tDouble.doubleValue();
            T10 = d;
        }
    }

    public double getT11() {
        return T11;
    }
    public void setT11(double aT11) {
        T11 = aT11;
    }
    public void setT11(String aT11) {
        if (aT11 != null && !aT11.equals("")) {
            Double tDouble = new Double(aT11);
            double d = tDouble.doubleValue();
            T11 = d;
        }
    }

    public double getT12() {
        return T12;
    }
    public void setT12(double aT12) {
        T12 = aT12;
    }
    public void setT12(String aT12) {
        if (aT12 != null && !aT12.equals("")) {
            Double tDouble = new Double(aT12);
            double d = tDouble.doubleValue();
            T12 = d;
        }
    }

    public double getT13() {
        return T13;
    }
    public void setT13(double aT13) {
        T13 = aT13;
    }
    public void setT13(String aT13) {
        if (aT13 != null && !aT13.equals("")) {
            Double tDouble = new Double(aT13);
            double d = tDouble.doubleValue();
            T13 = d;
        }
    }

    public double getT14() {
        return T14;
    }
    public void setT14(double aT14) {
        T14 = aT14;
    }
    public void setT14(String aT14) {
        if (aT14 != null && !aT14.equals("")) {
            Double tDouble = new Double(aT14);
            double d = tDouble.doubleValue();
            T14 = d;
        }
    }

    public double getT15() {
        return T15;
    }
    public void setT15(double aT15) {
        T15 = aT15;
    }
    public void setT15(String aT15) {
        if (aT15 != null && !aT15.equals("")) {
            Double tDouble = new Double(aT15);
            double d = tDouble.doubleValue();
            T15 = d;
        }
    }

    public double getT16() {
        return T16;
    }
    public void setT16(double aT16) {
        T16 = aT16;
    }
    public void setT16(String aT16) {
        if (aT16 != null && !aT16.equals("")) {
            Double tDouble = new Double(aT16);
            double d = tDouble.doubleValue();
            T16 = d;
        }
    }

    public double getT17() {
        return T17;
    }
    public void setT17(double aT17) {
        T17 = aT17;
    }
    public void setT17(String aT17) {
        if (aT17 != null && !aT17.equals("")) {
            Double tDouble = new Double(aT17);
            double d = tDouble.doubleValue();
            T17 = d;
        }
    }

    public double getT18() {
        return T18;
    }
    public void setT18(double aT18) {
        T18 = aT18;
    }
    public void setT18(String aT18) {
        if (aT18 != null && !aT18.equals("")) {
            Double tDouble = new Double(aT18);
            double d = tDouble.doubleValue();
            T18 = d;
        }
    }

    public double getT19() {
        return T19;
    }
    public void setT19(double aT19) {
        T19 = aT19;
    }
    public void setT19(String aT19) {
        if (aT19 != null && !aT19.equals("")) {
            Double tDouble = new Double(aT19);
            double d = tDouble.doubleValue();
            T19 = d;
        }
    }

    public double getT20() {
        return T20;
    }
    public void setT20(double aT20) {
        T20 = aT20;
    }
    public void setT20(String aT20) {
        if (aT20 != null && !aT20.equals("")) {
            Double tDouble = new Double(aT20);
            double d = tDouble.doubleValue();
            T20 = d;
        }
    }


    /**
    * 使用另外一个 LAStateDataSchema 对象给 Schema 赋值
    * @param: aLAStateDataSchema LAStateDataSchema
    **/
    public void setSchema(LAStateDataSchema aLAStateDataSchema) {
        this.ObjectId = aLAStateDataSchema.getObjectId();
        this.StartDate = aLAStateDataSchema.getStartDate();
        this.EndDate = aLAStateDataSchema.getEndDate();
        this.BranchType = aLAStateDataSchema.getBranchType();
        this.StateType = aLAStateDataSchema.getStateType();
        this.T1 = aLAStateDataSchema.getT1();
        this.T2 = aLAStateDataSchema.getT2();
        this.T3 = aLAStateDataSchema.getT3();
        this.T4 = aLAStateDataSchema.getT4();
        this.T5 = aLAStateDataSchema.getT5();
        this.T6 = aLAStateDataSchema.getT6();
        this.T7 = aLAStateDataSchema.getT7();
        this.T8 = aLAStateDataSchema.getT8();
        this.T9 = aLAStateDataSchema.getT9();
        this.T10 = aLAStateDataSchema.getT10();
        this.T11 = aLAStateDataSchema.getT11();
        this.T12 = aLAStateDataSchema.getT12();
        this.T13 = aLAStateDataSchema.getT13();
        this.T14 = aLAStateDataSchema.getT14();
        this.T15 = aLAStateDataSchema.getT15();
        this.T16 = aLAStateDataSchema.getT16();
        this.T17 = aLAStateDataSchema.getT17();
        this.T18 = aLAStateDataSchema.getT18();
        this.T19 = aLAStateDataSchema.getT19();
        this.T20 = aLAStateDataSchema.getT20();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ObjectId") == null )
                this.ObjectId = null;
            else
                this.ObjectId = rs.getString("ObjectId").trim();

            if( rs.getString("StartDate") == null )
                this.StartDate = null;
            else
                this.StartDate = rs.getString("StartDate").trim();

            if( rs.getString("EndDate") == null )
                this.EndDate = null;
            else
                this.EndDate = rs.getString("EndDate").trim();

            if( rs.getString("BranchType") == null )
                this.BranchType = null;
            else
                this.BranchType = rs.getString("BranchType").trim();

            if( rs.getString("StateType") == null )
                this.StateType = null;
            else
                this.StateType = rs.getString("StateType").trim();

            this.T1 = rs.getDouble("T1");
            this.T2 = rs.getDouble("T2");
            this.T3 = rs.getDouble("T3");
            this.T4 = rs.getDouble("T4");
            this.T5 = rs.getDouble("T5");
            this.T6 = rs.getDouble("T6");
            this.T7 = rs.getDouble("T7");
            this.T8 = rs.getDouble("T8");
            this.T9 = rs.getDouble("T9");
            this.T10 = rs.getDouble("T10");
            this.T11 = rs.getDouble("T11");
            this.T12 = rs.getDouble("T12");
            this.T13 = rs.getDouble("T13");
            this.T14 = rs.getDouble("T14");
            this.T15 = rs.getDouble("T15");
            this.T16 = rs.getDouble("T16");
            this.T17 = rs.getDouble("T17");
            this.T18 = rs.getDouble("T18");
            this.T19 = rs.getDouble("T19");
            this.T20 = rs.getDouble("T20");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAStateDataSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LAStateDataSchema getSchema() {
        LAStateDataSchema aLAStateDataSchema = new LAStateDataSchema();
        aLAStateDataSchema.setSchema(this);
        return aLAStateDataSchema;
    }

    public LAStateDataDB getDB() {
        LAStateDataDB aDBOper = new LAStateDataDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAStateData描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ObjectId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StartDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EndDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T1));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T2));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T3));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T4));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T5));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T6));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T7));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T8));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T9));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T10));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T11));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T12));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T13));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T14));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T15));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T16));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T17));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T18));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T19));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(T20));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAStateData>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ObjectId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            StartDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            EndDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            StateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            T1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6, SysConst.PACKAGESPILTER))).doubleValue();
            T2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7, SysConst.PACKAGESPILTER))).doubleValue();
            T3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8, SysConst.PACKAGESPILTER))).doubleValue();
            T4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9, SysConst.PACKAGESPILTER))).doubleValue();
            T5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10, SysConst.PACKAGESPILTER))).doubleValue();
            T6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11, SysConst.PACKAGESPILTER))).doubleValue();
            T7 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12, SysConst.PACKAGESPILTER))).doubleValue();
            T8 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13, SysConst.PACKAGESPILTER))).doubleValue();
            T9 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14, SysConst.PACKAGESPILTER))).doubleValue();
            T10 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15, SysConst.PACKAGESPILTER))).doubleValue();
            T11 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16, SysConst.PACKAGESPILTER))).doubleValue();
            T12 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17, SysConst.PACKAGESPILTER))).doubleValue();
            T13 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18, SysConst.PACKAGESPILTER))).doubleValue();
            T14 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19, SysConst.PACKAGESPILTER))).doubleValue();
            T15 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).doubleValue();
            T16 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).doubleValue();
            T17 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22, SysConst.PACKAGESPILTER))).doubleValue();
            T18 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23, SysConst.PACKAGESPILTER))).doubleValue();
            T19 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24, SysConst.PACKAGESPILTER))).doubleValue();
            T20 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAStateDataSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ObjectId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectId));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateType));
        }
        if (FCode.equalsIgnoreCase("T1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T1));
        }
        if (FCode.equalsIgnoreCase("T2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T2));
        }
        if (FCode.equalsIgnoreCase("T3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T3));
        }
        if (FCode.equalsIgnoreCase("T4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T4));
        }
        if (FCode.equalsIgnoreCase("T5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T5));
        }
        if (FCode.equalsIgnoreCase("T6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T6));
        }
        if (FCode.equalsIgnoreCase("T7")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T7));
        }
        if (FCode.equalsIgnoreCase("T8")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T8));
        }
        if (FCode.equalsIgnoreCase("T9")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T9));
        }
        if (FCode.equalsIgnoreCase("T10")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T10));
        }
        if (FCode.equalsIgnoreCase("T11")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T11));
        }
        if (FCode.equalsIgnoreCase("T12")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T12));
        }
        if (FCode.equalsIgnoreCase("T13")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T13));
        }
        if (FCode.equalsIgnoreCase("T14")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T14));
        }
        if (FCode.equalsIgnoreCase("T15")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T15));
        }
        if (FCode.equalsIgnoreCase("T16")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T16));
        }
        if (FCode.equalsIgnoreCase("T17")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T17));
        }
        if (FCode.equalsIgnoreCase("T18")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T18));
        }
        if (FCode.equalsIgnoreCase("T19")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T19));
        }
        if (FCode.equalsIgnoreCase("T20")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T20));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ObjectId);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(StartDate);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EndDate);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(StateType);
                break;
            case 5:
                strFieldValue = String.valueOf(T1);
                break;
            case 6:
                strFieldValue = String.valueOf(T2);
                break;
            case 7:
                strFieldValue = String.valueOf(T3);
                break;
            case 8:
                strFieldValue = String.valueOf(T4);
                break;
            case 9:
                strFieldValue = String.valueOf(T5);
                break;
            case 10:
                strFieldValue = String.valueOf(T6);
                break;
            case 11:
                strFieldValue = String.valueOf(T7);
                break;
            case 12:
                strFieldValue = String.valueOf(T8);
                break;
            case 13:
                strFieldValue = String.valueOf(T9);
                break;
            case 14:
                strFieldValue = String.valueOf(T10);
                break;
            case 15:
                strFieldValue = String.valueOf(T11);
                break;
            case 16:
                strFieldValue = String.valueOf(T12);
                break;
            case 17:
                strFieldValue = String.valueOf(T13);
                break;
            case 18:
                strFieldValue = String.valueOf(T14);
                break;
            case 19:
                strFieldValue = String.valueOf(T15);
                break;
            case 20:
                strFieldValue = String.valueOf(T16);
                break;
            case 21:
                strFieldValue = String.valueOf(T17);
                break;
            case 22:
                strFieldValue = String.valueOf(T18);
                break;
            case 23:
                strFieldValue = String.valueOf(T19);
                break;
            case 24:
                strFieldValue = String.valueOf(T20);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ObjectId")) {
            if( FValue != null && !FValue.equals(""))
            {
                ObjectId = FValue.trim();
            }
            else
                ObjectId = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateType = FValue.trim();
            }
            else
                StateType = null;
        }
        if (FCode.equalsIgnoreCase("T1")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T1 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T2")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T2 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T3")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T3 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T4")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T4 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T5")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T5 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T6")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T6 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T7")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T7 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T8")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T8 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T9")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T9 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T10")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T10 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T11")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T11 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T12")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T12 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T13")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T13 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T14")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T14 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T15")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T15 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T16")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T16 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T17")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T17 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T18")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T18 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T19")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T19 = d;
            }
        }
        if (FCode.equalsIgnoreCase("T20")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                T20 = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LAStateDataSchema other = (LAStateDataSchema)otherObject;
        return
            ObjectId.equals(other.getObjectId())
            && StartDate.equals(other.getStartDate())
            && EndDate.equals(other.getEndDate())
            && BranchType.equals(other.getBranchType())
            && StateType.equals(other.getStateType())
            && T1 == other.getT1()
            && T2 == other.getT2()
            && T3 == other.getT3()
            && T4 == other.getT4()
            && T5 == other.getT5()
            && T6 == other.getT6()
            && T7 == other.getT7()
            && T8 == other.getT8()
            && T9 == other.getT9()
            && T10 == other.getT10()
            && T11 == other.getT11()
            && T12 == other.getT12()
            && T13 == other.getT13()
            && T14 == other.getT14()
            && T15 == other.getT15()
            && T16 == other.getT16()
            && T17 == other.getT17()
            && T18 == other.getT18()
            && T19 == other.getT19()
            && T20 == other.getT20();
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ObjectId") ) {
            return 0;
        }
        if( strFieldName.equals("StartDate") ) {
            return 1;
        }
        if( strFieldName.equals("EndDate") ) {
            return 2;
        }
        if( strFieldName.equals("BranchType") ) {
            return 3;
        }
        if( strFieldName.equals("StateType") ) {
            return 4;
        }
        if( strFieldName.equals("T1") ) {
            return 5;
        }
        if( strFieldName.equals("T2") ) {
            return 6;
        }
        if( strFieldName.equals("T3") ) {
            return 7;
        }
        if( strFieldName.equals("T4") ) {
            return 8;
        }
        if( strFieldName.equals("T5") ) {
            return 9;
        }
        if( strFieldName.equals("T6") ) {
            return 10;
        }
        if( strFieldName.equals("T7") ) {
            return 11;
        }
        if( strFieldName.equals("T8") ) {
            return 12;
        }
        if( strFieldName.equals("T9") ) {
            return 13;
        }
        if( strFieldName.equals("T10") ) {
            return 14;
        }
        if( strFieldName.equals("T11") ) {
            return 15;
        }
        if( strFieldName.equals("T12") ) {
            return 16;
        }
        if( strFieldName.equals("T13") ) {
            return 17;
        }
        if( strFieldName.equals("T14") ) {
            return 18;
        }
        if( strFieldName.equals("T15") ) {
            return 19;
        }
        if( strFieldName.equals("T16") ) {
            return 20;
        }
        if( strFieldName.equals("T17") ) {
            return 21;
        }
        if( strFieldName.equals("T18") ) {
            return 22;
        }
        if( strFieldName.equals("T19") ) {
            return 23;
        }
        if( strFieldName.equals("T20") ) {
            return 24;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ObjectId";
                break;
            case 1:
                strFieldName = "StartDate";
                break;
            case 2:
                strFieldName = "EndDate";
                break;
            case 3:
                strFieldName = "BranchType";
                break;
            case 4:
                strFieldName = "StateType";
                break;
            case 5:
                strFieldName = "T1";
                break;
            case 6:
                strFieldName = "T2";
                break;
            case 7:
                strFieldName = "T3";
                break;
            case 8:
                strFieldName = "T4";
                break;
            case 9:
                strFieldName = "T5";
                break;
            case 10:
                strFieldName = "T6";
                break;
            case 11:
                strFieldName = "T7";
                break;
            case 12:
                strFieldName = "T8";
                break;
            case 13:
                strFieldName = "T9";
                break;
            case 14:
                strFieldName = "T10";
                break;
            case 15:
                strFieldName = "T11";
                break;
            case 16:
                strFieldName = "T12";
                break;
            case 17:
                strFieldName = "T13";
                break;
            case 18:
                strFieldName = "T14";
                break;
            case 19:
                strFieldName = "T15";
                break;
            case 20:
                strFieldName = "T16";
                break;
            case 21:
                strFieldName = "T17";
                break;
            case 22:
                strFieldName = "T18";
                break;
            case 23:
                strFieldName = "T19";
                break;
            case 24:
                strFieldName = "T20";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "OBJECTID":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "STATETYPE":
                return Schema.TYPE_STRING;
            case "T1":
                return Schema.TYPE_DOUBLE;
            case "T2":
                return Schema.TYPE_DOUBLE;
            case "T3":
                return Schema.TYPE_DOUBLE;
            case "T4":
                return Schema.TYPE_DOUBLE;
            case "T5":
                return Schema.TYPE_DOUBLE;
            case "T6":
                return Schema.TYPE_DOUBLE;
            case "T7":
                return Schema.TYPE_DOUBLE;
            case "T8":
                return Schema.TYPE_DOUBLE;
            case "T9":
                return Schema.TYPE_DOUBLE;
            case "T10":
                return Schema.TYPE_DOUBLE;
            case "T11":
                return Schema.TYPE_DOUBLE;
            case "T12":
                return Schema.TYPE_DOUBLE;
            case "T13":
                return Schema.TYPE_DOUBLE;
            case "T14":
                return Schema.TYPE_DOUBLE;
            case "T15":
                return Schema.TYPE_DOUBLE;
            case "T16":
                return Schema.TYPE_DOUBLE;
            case "T17":
                return Schema.TYPE_DOUBLE;
            case "T18":
                return Schema.TYPE_DOUBLE;
            case "T19":
                return Schema.TYPE_DOUBLE;
            case "T20":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_DOUBLE;
            case 13:
                return Schema.TYPE_DOUBLE;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_DOUBLE;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_DOUBLE;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
