/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.VMS_VERIFICATION_RATEDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: VMS_VERIFICATION_RATESchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_VERIFICATION_RATESchema implements Schema, Cloneable {
    // @Field
    /** 序列号 */
    private String SERIALNO;
    /** 险种 */
    private String RISKCODE;
    /** 机构 */
    private String MANAGECOM;
    /** 税率 */
    private double RATE;
    /** 备用字段1 */
    private String STANDBYFLAG1;
    /** 创建日期 */
    private String MAKEDATE;
    /** 创建时间 */
    private String MAKETIME;

    public static final int FIELDNUM = 7;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public VMS_VERIFICATION_RATESchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SERIALNO";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        VMS_VERIFICATION_RATESchema cloned = (VMS_VERIFICATION_RATESchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSERIALNO() {
        return SERIALNO;
    }
    public void setSERIALNO(String aSERIALNO) {
        SERIALNO = aSERIALNO;
    }
    public String getRISKCODE() {
        return RISKCODE;
    }
    public void setRISKCODE(String aRISKCODE) {
        RISKCODE = aRISKCODE;
    }
    public String getMANAGECOM() {
        return MANAGECOM;
    }
    public void setMANAGECOM(String aMANAGECOM) {
        MANAGECOM = aMANAGECOM;
    }
    public double getRATE() {
        return RATE;
    }
    public void setRATE(double aRATE) {
        RATE = aRATE;
    }
    public void setRATE(String aRATE) {
        if (aRATE != null && !aRATE.equals("")) {
            Double tDouble = new Double(aRATE);
            double d = tDouble.doubleValue();
            RATE = d;
        }
    }

    public String getSTANDBYFLAG1() {
        return STANDBYFLAG1;
    }
    public void setSTANDBYFLAG1(String aSTANDBYFLAG1) {
        STANDBYFLAG1 = aSTANDBYFLAG1;
    }
    public String getMAKEDATE() {
        return MAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }

    /**
    * 使用另外一个 VMS_VERIFICATION_RATESchema 对象给 Schema 赋值
    * @param: aVMS_VERIFICATION_RATESchema VMS_VERIFICATION_RATESchema
    **/
    public void setSchema(VMS_VERIFICATION_RATESchema aVMS_VERIFICATION_RATESchema) {
        this.SERIALNO = aVMS_VERIFICATION_RATESchema.getSERIALNO();
        this.RISKCODE = aVMS_VERIFICATION_RATESchema.getRISKCODE();
        this.MANAGECOM = aVMS_VERIFICATION_RATESchema.getMANAGECOM();
        this.RATE = aVMS_VERIFICATION_RATESchema.getRATE();
        this.STANDBYFLAG1 = aVMS_VERIFICATION_RATESchema.getSTANDBYFLAG1();
        this.MAKEDATE = aVMS_VERIFICATION_RATESchema.getMAKEDATE();
        this.MAKETIME = aVMS_VERIFICATION_RATESchema.getMAKETIME();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SERIALNO") == null )
                this.SERIALNO = null;
            else
                this.SERIALNO = rs.getString("SERIALNO").trim();

            if( rs.getString("RISKCODE") == null )
                this.RISKCODE = null;
            else
                this.RISKCODE = rs.getString("RISKCODE").trim();

            if( rs.getString("MANAGECOM") == null )
                this.MANAGECOM = null;
            else
                this.MANAGECOM = rs.getString("MANAGECOM").trim();

            this.RATE = rs.getDouble("RATE");
            if( rs.getString("STANDBYFLAG1") == null )
                this.STANDBYFLAG1 = null;
            else
                this.STANDBYFLAG1 = rs.getString("STANDBYFLAG1").trim();

            if( rs.getString("MAKEDATE") == null )
                this.MAKEDATE = null;
            else
                this.MAKEDATE = rs.getString("MAKEDATE").trim();

            if( rs.getString("MAKETIME") == null )
                this.MAKETIME = null;
            else
                this.MAKETIME = rs.getString("MAKETIME").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_VERIFICATION_RATESchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public VMS_VERIFICATION_RATESchema getSchema() {
        VMS_VERIFICATION_RATESchema aVMS_VERIFICATION_RATESchema = new VMS_VERIFICATION_RATESchema();
        aVMS_VERIFICATION_RATESchema.setSchema(this);
        return aVMS_VERIFICATION_RATESchema;
    }

    public VMS_VERIFICATION_RATEDB getDB() {
        VMS_VERIFICATION_RATEDB aDBOper = new VMS_VERIFICATION_RATEDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_VERIFICATION_RATE描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SERIALNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RISKCODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MANAGECOM)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RATE));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(STANDBYFLAG1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MAKEDATE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MAKETIME));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_VERIFICATION_RATE>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SERIALNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RISKCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            MANAGECOM = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            RATE = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4, SysConst.PACKAGESPILTER))).doubleValue();
            STANDBYFLAG1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            MAKEDATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_VERIFICATION_RATESchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SERIALNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SERIALNO));
        }
        if (FCode.equalsIgnoreCase("RISKCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RISKCODE));
        }
        if (FCode.equalsIgnoreCase("MANAGECOM")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MANAGECOM));
        }
        if (FCode.equalsIgnoreCase("RATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RATE));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG1));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKEDATE));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SERIALNO);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RISKCODE);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(MANAGECOM);
                break;
            case 3:
                strFieldValue = String.valueOf(RATE);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(STANDBYFLAG1);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MAKEDATE);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MAKETIME);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SERIALNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SERIALNO = FValue.trim();
            }
            else
                SERIALNO = null;
        }
        if (FCode.equalsIgnoreCase("RISKCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                RISKCODE = FValue.trim();
            }
            else
                RISKCODE = null;
        }
        if (FCode.equalsIgnoreCase("MANAGECOM")) {
            if( FValue != null && !FValue.equals(""))
            {
                MANAGECOM = FValue.trim();
            }
            else
                MANAGECOM = null;
        }
        if (FCode.equalsIgnoreCase("RATE")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RATE = d;
            }
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG1 = FValue.trim();
            }
            else
                STANDBYFLAG1 = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKEDATE = FValue.trim();
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        VMS_VERIFICATION_RATESchema other = (VMS_VERIFICATION_RATESchema)otherObject;
        return
            SERIALNO.equals(other.getSERIALNO())
            && RISKCODE.equals(other.getRISKCODE())
            && MANAGECOM.equals(other.getMANAGECOM())
            && RATE == other.getRATE()
            && STANDBYFLAG1.equals(other.getSTANDBYFLAG1())
            && MAKEDATE.equals(other.getMAKEDATE())
            && MAKETIME.equals(other.getMAKETIME());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SERIALNO") ) {
            return 0;
        }
        if( strFieldName.equals("RISKCODE") ) {
            return 1;
        }
        if( strFieldName.equals("MANAGECOM") ) {
            return 2;
        }
        if( strFieldName.equals("RATE") ) {
            return 3;
        }
        if( strFieldName.equals("STANDBYFLAG1") ) {
            return 4;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 5;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SERIALNO";
                break;
            case 1:
                strFieldName = "RISKCODE";
                break;
            case 2:
                strFieldName = "MANAGECOM";
                break;
            case 3:
                strFieldName = "RATE";
                break;
            case 4:
                strFieldName = "STANDBYFLAG1";
                break;
            case 5:
                strFieldName = "MAKEDATE";
                break;
            case 6:
                strFieldName = "MAKETIME";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "RATE":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_DOUBLE;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
