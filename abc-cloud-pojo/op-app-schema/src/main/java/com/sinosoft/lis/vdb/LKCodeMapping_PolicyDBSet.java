/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LKCodeMapping_PolicySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LKCodeMapping_PolicyDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-07-05
 */
public class LKCodeMapping_PolicyDBSet extends LKCodeMapping_PolicySet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    // @Constructor
    public LKCodeMapping_PolicyDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LKCodeMapping_Policy");
        mflag = true;
    }

    public LKCodeMapping_PolicyDBSet() {
        db = new DBOper( "LKCodeMapping_Policy" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LKCodeMapping_Policy WHERE  1=1  AND BankCode = ? AND ZoneNo = ? AND BankNode = ? AND System = ?");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getBankCode());
                }
                if(this.get(i).getZoneNo() == null || this.get(i).getZoneNo().equals("null")) {
                    pstmt.setString(2,null);
                } else {
                    pstmt.setString(2, this.get(i).getZoneNo());
                }
                if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
                    pstmt.setString(3,null);
                } else {
                    pstmt.setString(3, this.get(i).getBankNode());
                }
                if(this.get(i).getSystem() == null || this.get(i).getSystem().equals("null")) {
                    pstmt.setString(4,null);
                } else {
                    pstmt.setString(4, this.get(i).getSystem());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LKCodeMapping_Policy SET  BankCode = ? , ZoneNo = ? , BankNode = ? , AgentCom = ? , ComCode = ? , ManageCom = ? , Operator = ? , Remark = ? , Remark1 = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FTPAddress = ? , FTPDir = ? , FTPUser = ? , FTPPassWord = ? , Remark2 = ? , Remark3 = ? , ServerDir = ? , ServerAddress = ? , ServerPort = ? , ServerUser = ? , ServerPassWord = ? , Remark4 = ? , Remark5 = ? , BRCHNetCode = ? , System = ? , Bak1 = ? , Bak2 = ? , Bak3 = ? WHERE  1=1  AND BankCode = ? AND ZoneNo = ? AND BankNode = ? AND System = ?");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getBankCode());
                }
                if(this.get(i).getZoneNo() == null || this.get(i).getZoneNo().equals("null")) {
                    pstmt.setString(2,null);
                } else {
                    pstmt.setString(2, this.get(i).getZoneNo());
                }
                if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
                    pstmt.setString(3,null);
                } else {
                    pstmt.setString(3, this.get(i).getBankNode());
                }
                if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
                    pstmt.setString(4,null);
                } else {
                    pstmt.setString(4, this.get(i).getAgentCom());
                }
                if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("null")) {
                    pstmt.setString(5,null);
                } else {
                    pstmt.setString(5, this.get(i).getComCode());
                }
                if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(6,null);
                } else {
                    pstmt.setString(6, this.get(i).getManageCom());
                }
                if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
                    pstmt.setString(7,null);
                } else {
                    pstmt.setString(7, this.get(i).getOperator());
                }
                if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
                    pstmt.setString(8,null);
                } else {
                    pstmt.setString(8, this.get(i).getRemark());
                }
                if(this.get(i).getRemark1() == null || this.get(i).getRemark1().equals("null")) {
                    pstmt.setString(9,null);
                } else {
                    pstmt.setString(9, this.get(i).getRemark1());
                }
                if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(10,null);
                } else {
                    pstmt.setDate(10, Date.valueOf(this.get(i).getMakeDate()));
                }
                if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(11,null);
                } else {
                    pstmt.setString(11, this.get(i).getMakeTime());
                }
                if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(12,null);
                } else {
                    pstmt.setDate(12, Date.valueOf(this.get(i).getModifyDate()));
                }
                if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(13,null);
                } else {
                    pstmt.setString(13, this.get(i).getModifyTime());
                }
                if(this.get(i).getFTPAddress() == null || this.get(i).getFTPAddress().equals("null")) {
                    pstmt.setString(14,null);
                } else {
                    pstmt.setString(14, this.get(i).getFTPAddress());
                }
                if(this.get(i).getFTPDir() == null || this.get(i).getFTPDir().equals("null")) {
                    pstmt.setString(15,null);
                } else {
                    pstmt.setString(15, this.get(i).getFTPDir());
                }
                if(this.get(i).getFTPUser() == null || this.get(i).getFTPUser().equals("null")) {
                    pstmt.setString(16,null);
                } else {
                    pstmt.setString(16, this.get(i).getFTPUser());
                }
                if(this.get(i).getFTPPassWord() == null || this.get(i).getFTPPassWord().equals("null")) {
                    pstmt.setString(17,null);
                } else {
                    pstmt.setString(17, this.get(i).getFTPPassWord());
                }
                if(this.get(i).getRemark2() == null || this.get(i).getRemark2().equals("null")) {
                    pstmt.setString(18,null);
                } else {
                    pstmt.setString(18, this.get(i).getRemark2());
                }
                if(this.get(i).getRemark3() == null || this.get(i).getRemark3().equals("null")) {
                    pstmt.setString(19,null);
                } else {
                    pstmt.setString(19, this.get(i).getRemark3());
                }
                if(this.get(i).getServerDir() == null || this.get(i).getServerDir().equals("null")) {
                    pstmt.setString(20,null);
                } else {
                    pstmt.setString(20, this.get(i).getServerDir());
                }
                if(this.get(i).getServerAddress() == null || this.get(i).getServerAddress().equals("null")) {
                    pstmt.setString(21,null);
                } else {
                    pstmt.setString(21, this.get(i).getServerAddress());
                }
                pstmt.setInt(22, this.get(i).getServerPort());
                if(this.get(i).getServerUser() == null || this.get(i).getServerUser().equals("null")) {
                    pstmt.setString(23,null);
                } else {
                    pstmt.setString(23, this.get(i).getServerUser());
                }
                if(this.get(i).getServerPassWord() == null || this.get(i).getServerPassWord().equals("null")) {
                    pstmt.setString(24,null);
                } else {
                    pstmt.setString(24, this.get(i).getServerPassWord());
                }
                if(this.get(i).getRemark4() == null || this.get(i).getRemark4().equals("null")) {
                    pstmt.setString(25,null);
                } else {
                    pstmt.setString(25, this.get(i).getRemark4());
                }
                if(this.get(i).getRemark5() == null || this.get(i).getRemark5().equals("null")) {
                    pstmt.setString(26,null);
                } else {
                    pstmt.setString(26, this.get(i).getRemark5());
                }
                if(this.get(i).getBRCHNetCode() == null || this.get(i).getBRCHNetCode().equals("null")) {
                    pstmt.setString(27,null);
                } else {
                    pstmt.setString(27, this.get(i).getBRCHNetCode());
                }
                if(this.get(i).getSystem() == null || this.get(i).getSystem().equals("null")) {
                    pstmt.setString(28,null);
                } else {
                    pstmt.setString(28, this.get(i).getSystem());
                }
                if(this.get(i).getBak1() == null || this.get(i).getBak1().equals("null")) {
                    pstmt.setString(29,null);
                } else {
                    pstmt.setString(29, this.get(i).getBak1());
                }
                if(this.get(i).getBak2() == null || this.get(i).getBak2().equals("null")) {
                    pstmt.setString(30,null);
                } else {
                    pstmt.setString(30, this.get(i).getBak2());
                }
                if(this.get(i).getBak3() == null || this.get(i).getBak3().equals("null")) {
                    pstmt.setString(31,null);
                } else {
                    pstmt.setString(31, this.get(i).getBak3());
                }
                // set where condition
                if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
                    pstmt.setString(32,null);
                } else {
                    pstmt.setString(32, this.get(i).getBankCode());
                }
                if(this.get(i).getZoneNo() == null || this.get(i).getZoneNo().equals("null")) {
                    pstmt.setString(33,null);
                } else {
                    pstmt.setString(33, this.get(i).getZoneNo());
                }
                if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
                    pstmt.setString(34,null);
                } else {
                    pstmt.setString(34, this.get(i).getBankNode());
                }
                if(this.get(i).getSystem() == null || this.get(i).getSystem().equals("null")) {
                    pstmt.setString(35,null);
                } else {
                    pstmt.setString(35, this.get(i).getSystem());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LKCodeMapping_Policy VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
                if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
                    pstmt.setString(1,null);
                } else {
                    pstmt.setString(1, this.get(i).getBankCode());
                }
                if(this.get(i).getZoneNo() == null || this.get(i).getZoneNo().equals("null")) {
                    pstmt.setString(2,null);
                } else {
                    pstmt.setString(2, this.get(i).getZoneNo());
                }
                if(this.get(i).getBankNode() == null || this.get(i).getBankNode().equals("null")) {
                    pstmt.setString(3,null);
                } else {
                    pstmt.setString(3, this.get(i).getBankNode());
                }
                if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
                    pstmt.setString(4,null);
                } else {
                    pstmt.setString(4, this.get(i).getAgentCom());
                }
                if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("null")) {
                    pstmt.setString(5,null);
                } else {
                    pstmt.setString(5, this.get(i).getComCode());
                }
                if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
                    pstmt.setString(6,null);
                } else {
                    pstmt.setString(6, this.get(i).getManageCom());
                }
                if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
                    pstmt.setString(7,null);
                } else {
                    pstmt.setString(7, this.get(i).getOperator());
                }
                if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
                    pstmt.setString(8,null);
                } else {
                    pstmt.setString(8, this.get(i).getRemark());
                }
                if(this.get(i).getRemark1() == null || this.get(i).getRemark1().equals("null")) {
                    pstmt.setString(9,null);
                } else {
                    pstmt.setString(9, this.get(i).getRemark1());
                }
                if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                    pstmt.setDate(10,null);
                } else {
                    pstmt.setDate(10, Date.valueOf(this.get(i).getMakeDate()));
                }
                if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
                    pstmt.setString(11,null);
                } else {
                    pstmt.setString(11, this.get(i).getMakeTime());
                }
                if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                    pstmt.setDate(12,null);
                } else {
                    pstmt.setDate(12, Date.valueOf(this.get(i).getModifyDate()));
                }
                if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
                    pstmt.setString(13,null);
                } else {
                    pstmt.setString(13, this.get(i).getModifyTime());
                }
                if(this.get(i).getFTPAddress() == null || this.get(i).getFTPAddress().equals("null")) {
                    pstmt.setString(14,null);
                } else {
                    pstmt.setString(14, this.get(i).getFTPAddress());
                }
                if(this.get(i).getFTPDir() == null || this.get(i).getFTPDir().equals("null")) {
                    pstmt.setString(15,null);
                } else {
                    pstmt.setString(15, this.get(i).getFTPDir());
                }
                if(this.get(i).getFTPUser() == null || this.get(i).getFTPUser().equals("null")) {
                    pstmt.setString(16,null);
                } else {
                    pstmt.setString(16, this.get(i).getFTPUser());
                }
                if(this.get(i).getFTPPassWord() == null || this.get(i).getFTPPassWord().equals("null")) {
                    pstmt.setString(17,null);
                } else {
                    pstmt.setString(17, this.get(i).getFTPPassWord());
                }
                if(this.get(i).getRemark2() == null || this.get(i).getRemark2().equals("null")) {
                    pstmt.setString(18,null);
                } else {
                    pstmt.setString(18, this.get(i).getRemark2());
                }
                if(this.get(i).getRemark3() == null || this.get(i).getRemark3().equals("null")) {
                    pstmt.setString(19,null);
                } else {
                    pstmt.setString(19, this.get(i).getRemark3());
                }
                if(this.get(i).getServerDir() == null || this.get(i).getServerDir().equals("null")) {
                    pstmt.setString(20,null);
                } else {
                    pstmt.setString(20, this.get(i).getServerDir());
                }
                if(this.get(i).getServerAddress() == null || this.get(i).getServerAddress().equals("null")) {
                    pstmt.setString(21,null);
                } else {
                    pstmt.setString(21, this.get(i).getServerAddress());
                }
                pstmt.setInt(22, this.get(i).getServerPort());
                if(this.get(i).getServerUser() == null || this.get(i).getServerUser().equals("null")) {
                    pstmt.setString(23,null);
                } else {
                    pstmt.setString(23, this.get(i).getServerUser());
                }
                if(this.get(i).getServerPassWord() == null || this.get(i).getServerPassWord().equals("null")) {
                    pstmt.setString(24,null);
                } else {
                    pstmt.setString(24, this.get(i).getServerPassWord());
                }
                if(this.get(i).getRemark4() == null || this.get(i).getRemark4().equals("null")) {
                    pstmt.setString(25,null);
                } else {
                    pstmt.setString(25, this.get(i).getRemark4());
                }
                if(this.get(i).getRemark5() == null || this.get(i).getRemark5().equals("null")) {
                    pstmt.setString(26,null);
                } else {
                    pstmt.setString(26, this.get(i).getRemark5());
                }
                if(this.get(i).getBRCHNetCode() == null || this.get(i).getBRCHNetCode().equals("null")) {
                    pstmt.setString(27,null);
                } else {
                    pstmt.setString(27, this.get(i).getBRCHNetCode());
                }
                if(this.get(i).getSystem() == null || this.get(i).getSystem().equals("null")) {
                    pstmt.setString(28,null);
                } else {
                    pstmt.setString(28, this.get(i).getSystem());
                }
                if(this.get(i).getBak1() == null || this.get(i).getBak1().equals("null")) {
                    pstmt.setString(29,null);
                } else {
                    pstmt.setString(29, this.get(i).getBak1());
                }
                if(this.get(i).getBak2() == null || this.get(i).getBak2().equals("null")) {
                    pstmt.setString(30,null);
                } else {
                    pstmt.setString(30, this.get(i).getBak2());
                }
                if(this.get(i).getBak3() == null || this.get(i).getBak3().equals("null")) {
                    pstmt.setString(31,null);
                } else {
                    pstmt.setString(31, this.get(i).getBak3());
                }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
