/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LAAgentMonitorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LAAgentMonitorDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LAAgentMonitorDBSet extends LAAgentMonitorSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LAAgentMonitorDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LAAgentMonitor");
        mflag = true;
    }

    public LAAgentMonitorDBSet() {
        db = new DBOper( "LAAgentMonitor" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentMonitorDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LAAgentMonitor WHERE  1=1  AND AgentCode = ? AND MonitorType = ? AND MonitorNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAgentCode());
            }
            if(this.get(i).getMonitorType() == null || this.get(i).getMonitorType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getMonitorType());
            }
            pstmt.setInt(3, this.get(i).getMonitorNo());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentMonitorDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LAAgentMonitor SET  AgentCode = ? , MonitorType = ? , MonitorNo = ? , ManageCom = ? , AgentGroup = ? , AgentName = ? , AgentSex = ? , Birthday = ? , IDNoType = ? , IDNo = ? , BranchAttr = ? , BranchManager = ? , BranchGroupName = ? , MonitorState = ? , MonitorDate = ? , CancelDate = ? , ReportCom = ? , ReportReason = ? , CancelCom = ? , CancelReason = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , StandByFlag1 = ? , StandByFlag2 = ? , StandByFlag3 = ? , StandByFlag4 = ? , StandByFlag5 = ? , StandByFlag6 = ? , StandByFlag7 = ? , StandByFlag8 = ? , StandByFlag9 = ? , StandByFlag10 = ? , StandByFlag11 = ? , StandByFlag12 = ? , StandByFlag13 = ? , StandByFlag14 = ? , StandByFlag15 = ? WHERE  1=1  AND AgentCode = ? AND MonitorType = ? AND MonitorNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAgentCode());
            }
            if(this.get(i).getMonitorType() == null || this.get(i).getMonitorType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getMonitorType());
            }
            pstmt.setInt(3, this.get(i).getMonitorNo());
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getManageCom());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAgentGroup());
            }
            if(this.get(i).getAgentName() == null || this.get(i).getAgentName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getAgentName());
            }
            if(this.get(i).getAgentSex() == null || this.get(i).getAgentSex().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getAgentSex());
            }
            if(this.get(i).getBirthday() == null || this.get(i).getBirthday().equals("null")) {
                pstmt.setDate(8,null);
            } else {
                pstmt.setDate(8, Date.valueOf(this.get(i).getBirthday()));
            }
            if(this.get(i).getIDNoType() == null || this.get(i).getIDNoType().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getIDNoType());
            }
            if(this.get(i).getIDNo() == null || this.get(i).getIDNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getIDNo());
            }
            if(this.get(i).getBranchAttr() == null || this.get(i).getBranchAttr().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getBranchAttr());
            }
            if(this.get(i).getBranchManager() == null || this.get(i).getBranchManager().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getBranchManager());
            }
            if(this.get(i).getBranchGroupName() == null || this.get(i).getBranchGroupName().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getBranchGroupName());
            }
            if(this.get(i).getMonitorState() == null || this.get(i).getMonitorState().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getMonitorState());
            }
            if(this.get(i).getMonitorDate() == null || this.get(i).getMonitorDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getMonitorDate()));
            }
            if(this.get(i).getCancelDate() == null || this.get(i).getCancelDate().equals("null")) {
                pstmt.setDate(16,null);
            } else {
                pstmt.setDate(16, Date.valueOf(this.get(i).getCancelDate()));
            }
            if(this.get(i).getReportCom() == null || this.get(i).getReportCom().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getReportCom());
            }
            if(this.get(i).getReportReason() == null || this.get(i).getReportReason().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getReportReason());
            }
            if(this.get(i).getCancelCom() == null || this.get(i).getCancelCom().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getCancelCom());
            }
            if(this.get(i).getCancelReason() == null || this.get(i).getCancelReason().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getCancelReason());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(22,null);
            } else {
                pstmt.setDate(22, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(24,null);
            } else {
                pstmt.setDate(24, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getModifyTime());
            }
            if(this.get(i).getStandByFlag1() == null || this.get(i).getStandByFlag1().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getStandByFlag1());
            }
            if(this.get(i).getStandByFlag2() == null || this.get(i).getStandByFlag2().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getStandByFlag2());
            }
            if(this.get(i).getStandByFlag3() == null || this.get(i).getStandByFlag3().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getStandByFlag3());
            }
            if(this.get(i).getStandByFlag4() == null || this.get(i).getStandByFlag4().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getStandByFlag4());
            }
            if(this.get(i).getStandByFlag5() == null || this.get(i).getStandByFlag5().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getStandByFlag5());
            }
            if(this.get(i).getStandByFlag6() == null || this.get(i).getStandByFlag6().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getStandByFlag6());
            }
            if(this.get(i).getStandByFlag7() == null || this.get(i).getStandByFlag7().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getStandByFlag7());
            }
            if(this.get(i).getStandByFlag8() == null || this.get(i).getStandByFlag8().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getStandByFlag8());
            }
            if(this.get(i).getStandByFlag9() == null || this.get(i).getStandByFlag9().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getStandByFlag9());
            }
            if(this.get(i).getStandByFlag10() == null || this.get(i).getStandByFlag10().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getStandByFlag10());
            }
            if(this.get(i).getStandByFlag11() == null || this.get(i).getStandByFlag11().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getStandByFlag11());
            }
            if(this.get(i).getStandByFlag12() == null || this.get(i).getStandByFlag12().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getStandByFlag12());
            }
            if(this.get(i).getStandByFlag13() == null || this.get(i).getStandByFlag13().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getStandByFlag13());
            }
            if(this.get(i).getStandByFlag14() == null || this.get(i).getStandByFlag14().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getStandByFlag14());
            }
            if(this.get(i).getStandByFlag15() == null || this.get(i).getStandByFlag15().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getStandByFlag15());
            }
            // set where condition
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getAgentCode());
            }
            if(this.get(i).getMonitorType() == null || this.get(i).getMonitorType().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getMonitorType());
            }
            pstmt.setInt(43, this.get(i).getMonitorNo());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentMonitorDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LAAgentMonitor VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAgentCode());
            }
            if(this.get(i).getMonitorType() == null || this.get(i).getMonitorType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getMonitorType());
            }
            pstmt.setInt(3, this.get(i).getMonitorNo());
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getManageCom());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAgentGroup());
            }
            if(this.get(i).getAgentName() == null || this.get(i).getAgentName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getAgentName());
            }
            if(this.get(i).getAgentSex() == null || this.get(i).getAgentSex().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getAgentSex());
            }
            if(this.get(i).getBirthday() == null || this.get(i).getBirthday().equals("null")) {
                pstmt.setDate(8,null);
            } else {
                pstmt.setDate(8, Date.valueOf(this.get(i).getBirthday()));
            }
            if(this.get(i).getIDNoType() == null || this.get(i).getIDNoType().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getIDNoType());
            }
            if(this.get(i).getIDNo() == null || this.get(i).getIDNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getIDNo());
            }
            if(this.get(i).getBranchAttr() == null || this.get(i).getBranchAttr().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getBranchAttr());
            }
            if(this.get(i).getBranchManager() == null || this.get(i).getBranchManager().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getBranchManager());
            }
            if(this.get(i).getBranchGroupName() == null || this.get(i).getBranchGroupName().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getBranchGroupName());
            }
            if(this.get(i).getMonitorState() == null || this.get(i).getMonitorState().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getMonitorState());
            }
            if(this.get(i).getMonitorDate() == null || this.get(i).getMonitorDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getMonitorDate()));
            }
            if(this.get(i).getCancelDate() == null || this.get(i).getCancelDate().equals("null")) {
                pstmt.setDate(16,null);
            } else {
                pstmt.setDate(16, Date.valueOf(this.get(i).getCancelDate()));
            }
            if(this.get(i).getReportCom() == null || this.get(i).getReportCom().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getReportCom());
            }
            if(this.get(i).getReportReason() == null || this.get(i).getReportReason().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getReportReason());
            }
            if(this.get(i).getCancelCom() == null || this.get(i).getCancelCom().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getCancelCom());
            }
            if(this.get(i).getCancelReason() == null || this.get(i).getCancelReason().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getCancelReason());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(22,null);
            } else {
                pstmt.setDate(22, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(24,null);
            } else {
                pstmt.setDate(24, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getModifyTime());
            }
            if(this.get(i).getStandByFlag1() == null || this.get(i).getStandByFlag1().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getStandByFlag1());
            }
            if(this.get(i).getStandByFlag2() == null || this.get(i).getStandByFlag2().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getStandByFlag2());
            }
            if(this.get(i).getStandByFlag3() == null || this.get(i).getStandByFlag3().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getStandByFlag3());
            }
            if(this.get(i).getStandByFlag4() == null || this.get(i).getStandByFlag4().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getStandByFlag4());
            }
            if(this.get(i).getStandByFlag5() == null || this.get(i).getStandByFlag5().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getStandByFlag5());
            }
            if(this.get(i).getStandByFlag6() == null || this.get(i).getStandByFlag6().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getStandByFlag6());
            }
            if(this.get(i).getStandByFlag7() == null || this.get(i).getStandByFlag7().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getStandByFlag7());
            }
            if(this.get(i).getStandByFlag8() == null || this.get(i).getStandByFlag8().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getStandByFlag8());
            }
            if(this.get(i).getStandByFlag9() == null || this.get(i).getStandByFlag9().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getStandByFlag9());
            }
            if(this.get(i).getStandByFlag10() == null || this.get(i).getStandByFlag10().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getStandByFlag10());
            }
            if(this.get(i).getStandByFlag11() == null || this.get(i).getStandByFlag11().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getStandByFlag11());
            }
            if(this.get(i).getStandByFlag12() == null || this.get(i).getStandByFlag12().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getStandByFlag12());
            }
            if(this.get(i).getStandByFlag13() == null || this.get(i).getStandByFlag13().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getStandByFlag13());
            }
            if(this.get(i).getStandByFlag14() == null || this.get(i).getStandByFlag14().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getStandByFlag14());
            }
            if(this.get(i).getStandByFlag15() == null || this.get(i).getStandByFlag15().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getStandByFlag15());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentMonitorDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
