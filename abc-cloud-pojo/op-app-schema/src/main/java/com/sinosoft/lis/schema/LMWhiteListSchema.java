/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMWhiteListDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LMWhiteListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-08-22
 */
public class LMWhiteListSchema implements Schema, Cloneable {
    // @Field
    /** 序列号 */
    private String SeqNo;
    /** 客户号 */
    private String CustomerNo;
    /** 姓名 */
    private String Name;
    /** 性别 */
    private String Sex;
    /** 证件类型 */
    private String IDType;
    /** 证件号码 */
    private String IDNo;
    /** 出生日期 */
    private Date BirthDay;
    /** 累计保额 */
    private double Amnt;
    /** 白名单产品类型标记 */
    private String WFWFlag;
    /** 保额类型 */
    private String AmntType;
    /** 申请人 */
    private String ApplyOpertor;
    /** 申请日期 */
    private Date ApplyDate;
    /** 申请时间 */
    private String ApplyTime;
    /** 申请状态 */
    private String ApplyState;
    /** 回退人 */
    private String BackOpertor;
    /** 审批人一 */
    private String ApproveOne;
    /** 审批一日期 */
    private Date ApproveOneDate;
    /** 审批一时间 */
    private String ApproveOneTime;
    /** 审批一结论 */
    private String AOVerdict;
    /** 审批一意见 */
    private String AOSuggestion;
    /** 审批人二 */
    private String ApproveTwo;
    /** 审批二日期 */
    private Date ApproveTwoDate;
    /** 审批二时间 */
    private String ApproveTwoTime;
    /** 审批二结论 */
    private String ATVerdict;
    /** 审批二意见 */
    private String ATSuggestion;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 申请险种 */
    private String RiskCode;
    /** 机构编码 */
    private String ManageCom;
    /** 客户状态 */
    private String UserState;
    /** 证件有效期 */
    private String IdValiDate;
    /** 备用字段 */
    private String StandByFlag1;
    /** 申请账号 */
    private String DiCardNo;
    /** 账号类型 */
    private String DiType;

    public static final int FIELDNUM = 34;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMWhiteListSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SeqNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMWhiteListSchema cloned = (LMWhiteListSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSeqNo() {
        return SeqNo;
    }
    public void setSeqNo(String aSeqNo) {
        SeqNo = aSeqNo;
    }
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getBirthDay() {
        if(BirthDay != null) {
            return fDate.getString(BirthDay);
        } else {
            return null;
        }
    }
    public void setBirthDay(Date aBirthDay) {
        BirthDay = aBirthDay;
    }
    public void setBirthDay(String aBirthDay) {
        if (aBirthDay != null && !aBirthDay.equals("")) {
            BirthDay = fDate.getDate(aBirthDay);
        } else
            BirthDay = null;
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public String getWFWFlag() {
        return WFWFlag;
    }
    public void setWFWFlag(String aWFWFlag) {
        WFWFlag = aWFWFlag;
    }
    public String getAmntType() {
        return AmntType;
    }
    public void setAmntType(String aAmntType) {
        AmntType = aAmntType;
    }
    public String getApplyOpertor() {
        return ApplyOpertor;
    }
    public void setApplyOpertor(String aApplyOpertor) {
        ApplyOpertor = aApplyOpertor;
    }
    public String getApplyDate() {
        if(ApplyDate != null) {
            return fDate.getString(ApplyDate);
        } else {
            return null;
        }
    }
    public void setApplyDate(Date aApplyDate) {
        ApplyDate = aApplyDate;
    }
    public void setApplyDate(String aApplyDate) {
        if (aApplyDate != null && !aApplyDate.equals("")) {
            ApplyDate = fDate.getDate(aApplyDate);
        } else
            ApplyDate = null;
    }

    public String getApplyTime() {
        return ApplyTime;
    }
    public void setApplyTime(String aApplyTime) {
        ApplyTime = aApplyTime;
    }
    public String getApplyState() {
        return ApplyState;
    }
    public void setApplyState(String aApplyState) {
        ApplyState = aApplyState;
    }
    public String getBackOpertor() {
        return BackOpertor;
    }
    public void setBackOpertor(String aBackOpertor) {
        BackOpertor = aBackOpertor;
    }
    public String getApproveOne() {
        return ApproveOne;
    }
    public void setApproveOne(String aApproveOne) {
        ApproveOne = aApproveOne;
    }
    public String getApproveOneDate() {
        if(ApproveOneDate != null) {
            return fDate.getString(ApproveOneDate);
        } else {
            return null;
        }
    }
    public void setApproveOneDate(Date aApproveOneDate) {
        ApproveOneDate = aApproveOneDate;
    }
    public void setApproveOneDate(String aApproveOneDate) {
        if (aApproveOneDate != null && !aApproveOneDate.equals("")) {
            ApproveOneDate = fDate.getDate(aApproveOneDate);
        } else
            ApproveOneDate = null;
    }

    public String getApproveOneTime() {
        return ApproveOneTime;
    }
    public void setApproveOneTime(String aApproveOneTime) {
        ApproveOneTime = aApproveOneTime;
    }
    public String getAOVerdict() {
        return AOVerdict;
    }
    public void setAOVerdict(String aAOVerdict) {
        AOVerdict = aAOVerdict;
    }
    public String getAOSuggestion() {
        return AOSuggestion;
    }
    public void setAOSuggestion(String aAOSuggestion) {
        AOSuggestion = aAOSuggestion;
    }
    public String getApproveTwo() {
        return ApproveTwo;
    }
    public void setApproveTwo(String aApproveTwo) {
        ApproveTwo = aApproveTwo;
    }
    public String getApproveTwoDate() {
        if(ApproveTwoDate != null) {
            return fDate.getString(ApproveTwoDate);
        } else {
            return null;
        }
    }
    public void setApproveTwoDate(Date aApproveTwoDate) {
        ApproveTwoDate = aApproveTwoDate;
    }
    public void setApproveTwoDate(String aApproveTwoDate) {
        if (aApproveTwoDate != null && !aApproveTwoDate.equals("")) {
            ApproveTwoDate = fDate.getDate(aApproveTwoDate);
        } else
            ApproveTwoDate = null;
    }

    public String getApproveTwoTime() {
        return ApproveTwoTime;
    }
    public void setApproveTwoTime(String aApproveTwoTime) {
        ApproveTwoTime = aApproveTwoTime;
    }
    public String getATVerdict() {
        return ATVerdict;
    }
    public void setATVerdict(String aATVerdict) {
        ATVerdict = aATVerdict;
    }
    public String getATSuggestion() {
        return ATSuggestion;
    }
    public void setATSuggestion(String aATSuggestion) {
        ATSuggestion = aATSuggestion;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getUserState() {
        return UserState;
    }
    public void setUserState(String aUserState) {
        UserState = aUserState;
    }
    public String getIdValiDate() {
        return IdValiDate;
    }
    public void setIdValiDate(String aIdValiDate) {
        IdValiDate = aIdValiDate;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getDiCardNo() {
        return DiCardNo;
    }
    public void setDiCardNo(String aDiCardNo) {
        DiCardNo = aDiCardNo;
    }
    public String getDiType() {
        return DiType;
    }
    public void setDiType(String aDiType) {
        DiType = aDiType;
    }

    /**
    * 使用另外一个 LMWhiteListSchema 对象给 Schema 赋值
    * @param: aLMWhiteListSchema LMWhiteListSchema
    **/
    public void setSchema(LMWhiteListSchema aLMWhiteListSchema) {
        this.SeqNo = aLMWhiteListSchema.getSeqNo();
        this.CustomerNo = aLMWhiteListSchema.getCustomerNo();
        this.Name = aLMWhiteListSchema.getName();
        this.Sex = aLMWhiteListSchema.getSex();
        this.IDType = aLMWhiteListSchema.getIDType();
        this.IDNo = aLMWhiteListSchema.getIDNo();
        this.BirthDay = fDate.getDate( aLMWhiteListSchema.getBirthDay());
        this.Amnt = aLMWhiteListSchema.getAmnt();
        this.WFWFlag = aLMWhiteListSchema.getWFWFlag();
        this.AmntType = aLMWhiteListSchema.getAmntType();
        this.ApplyOpertor = aLMWhiteListSchema.getApplyOpertor();
        this.ApplyDate = fDate.getDate( aLMWhiteListSchema.getApplyDate());
        this.ApplyTime = aLMWhiteListSchema.getApplyTime();
        this.ApplyState = aLMWhiteListSchema.getApplyState();
        this.BackOpertor = aLMWhiteListSchema.getBackOpertor();
        this.ApproveOne = aLMWhiteListSchema.getApproveOne();
        this.ApproveOneDate = fDate.getDate( aLMWhiteListSchema.getApproveOneDate());
        this.ApproveOneTime = aLMWhiteListSchema.getApproveOneTime();
        this.AOVerdict = aLMWhiteListSchema.getAOVerdict();
        this.AOSuggestion = aLMWhiteListSchema.getAOSuggestion();
        this.ApproveTwo = aLMWhiteListSchema.getApproveTwo();
        this.ApproveTwoDate = fDate.getDate( aLMWhiteListSchema.getApproveTwoDate());
        this.ApproveTwoTime = aLMWhiteListSchema.getApproveTwoTime();
        this.ATVerdict = aLMWhiteListSchema.getATVerdict();
        this.ATSuggestion = aLMWhiteListSchema.getATSuggestion();
        this.ModifyDate = fDate.getDate( aLMWhiteListSchema.getModifyDate());
        this.ModifyTime = aLMWhiteListSchema.getModifyTime();
        this.RiskCode = aLMWhiteListSchema.getRiskCode();
        this.ManageCom = aLMWhiteListSchema.getManageCom();
        this.UserState = aLMWhiteListSchema.getUserState();
        this.IdValiDate = aLMWhiteListSchema.getIdValiDate();
        this.StandByFlag1 = aLMWhiteListSchema.getStandByFlag1();
        this.DiCardNo = aLMWhiteListSchema.getDiCardNo();
        this.DiType = aLMWhiteListSchema.getDiType();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SeqNo") == null )
                this.SeqNo = null;
            else
                this.SeqNo = rs.getString("SeqNo").trim();

            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            this.BirthDay = rs.getDate("BirthDay");
            this.Amnt = rs.getDouble("Amnt");
            if( rs.getString("WFWFlag") == null )
                this.WFWFlag = null;
            else
                this.WFWFlag = rs.getString("WFWFlag").trim();

            if( rs.getString("AmntType") == null )
                this.AmntType = null;
            else
                this.AmntType = rs.getString("AmntType").trim();

            if( rs.getString("ApplyOpertor") == null )
                this.ApplyOpertor = null;
            else
                this.ApplyOpertor = rs.getString("ApplyOpertor").trim();

            this.ApplyDate = rs.getDate("ApplyDate");
            if( rs.getString("ApplyTime") == null )
                this.ApplyTime = null;
            else
                this.ApplyTime = rs.getString("ApplyTime").trim();

            if( rs.getString("ApplyState") == null )
                this.ApplyState = null;
            else
                this.ApplyState = rs.getString("ApplyState").trim();

            if( rs.getString("BackOpertor") == null )
                this.BackOpertor = null;
            else
                this.BackOpertor = rs.getString("BackOpertor").trim();

            if( rs.getString("ApproveOne") == null )
                this.ApproveOne = null;
            else
                this.ApproveOne = rs.getString("ApproveOne").trim();

            this.ApproveOneDate = rs.getDate("ApproveOneDate");
            if( rs.getString("ApproveOneTime") == null )
                this.ApproveOneTime = null;
            else
                this.ApproveOneTime = rs.getString("ApproveOneTime").trim();

            if( rs.getString("AOVerdict") == null )
                this.AOVerdict = null;
            else
                this.AOVerdict = rs.getString("AOVerdict").trim();

            if( rs.getString("AOSuggestion") == null )
                this.AOSuggestion = null;
            else
                this.AOSuggestion = rs.getString("AOSuggestion").trim();

            if( rs.getString("ApproveTwo") == null )
                this.ApproveTwo = null;
            else
                this.ApproveTwo = rs.getString("ApproveTwo").trim();

            this.ApproveTwoDate = rs.getDate("ApproveTwoDate");
            if( rs.getString("ApproveTwoTime") == null )
                this.ApproveTwoTime = null;
            else
                this.ApproveTwoTime = rs.getString("ApproveTwoTime").trim();

            if( rs.getString("ATVerdict") == null )
                this.ATVerdict = null;
            else
                this.ATVerdict = rs.getString("ATVerdict").trim();

            if( rs.getString("ATSuggestion") == null )
                this.ATSuggestion = null;
            else
                this.ATSuggestion = rs.getString("ATSuggestion").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("UserState") == null )
                this.UserState = null;
            else
                this.UserState = rs.getString("UserState").trim();

            if( rs.getString("IdValiDate") == null )
                this.IdValiDate = null;
            else
                this.IdValiDate = rs.getString("IdValiDate").trim();

            if( rs.getString("StandByFlag1") == null )
                this.StandByFlag1 = null;
            else
                this.StandByFlag1 = rs.getString("StandByFlag1").trim();

            if( rs.getString("DiCardNo") == null )
                this.DiCardNo = null;
            else
                this.DiCardNo = rs.getString("DiCardNo").trim();

            if( rs.getString("DiType") == null )
                this.DiType = null;
            else
                this.DiType = rs.getString("DiType").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMWhiteListSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMWhiteListSchema getSchema() {
        LMWhiteListSchema aLMWhiteListSchema = new LMWhiteListSchema();
        aLMWhiteListSchema.setSchema(this);
        return aLMWhiteListSchema;
    }

    public LMWhiteListDB getDB() {
        LMWhiteListDB aDBOper = new LMWhiteListDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMWhiteList描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SeqNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( BirthDay ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WFWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AmntType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApplyOpertor)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApplyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApplyState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BackOpertor)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveOne)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveOneDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveOneTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AOVerdict)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AOSuggestion)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTwo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveTwoDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTwoTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ATVerdict)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ATSuggestion)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UserState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IdValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DiCardNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DiType));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMWhiteList>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SeqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            BirthDay = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8, SysConst.PACKAGESPILTER))).doubleValue();
            WFWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AmntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            ApplyOpertor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            ApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER));
            ApplyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            ApplyState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            BackOpertor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            ApproveOne = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            ApproveOneDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER));
            ApproveOneTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            AOVerdict = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            AOSuggestion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            ApproveTwo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            ApproveTwoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER));
            ApproveTwoTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            ATVerdict = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            ATSuggestion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            UserState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            IdValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            DiCardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            DiType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMWhiteListSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeqNo));
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("BirthDay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthDay()));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("WFWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WFWFlag));
        }
        if (FCode.equalsIgnoreCase("AmntType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AmntType));
        }
        if (FCode.equalsIgnoreCase("ApplyOpertor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyOpertor));
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
        }
        if (FCode.equalsIgnoreCase("ApplyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyTime));
        }
        if (FCode.equalsIgnoreCase("ApplyState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyState));
        }
        if (FCode.equalsIgnoreCase("BackOpertor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackOpertor));
        }
        if (FCode.equalsIgnoreCase("ApproveOne")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOne));
        }
        if (FCode.equalsIgnoreCase("ApproveOneDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveOneDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveOneTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOneTime));
        }
        if (FCode.equalsIgnoreCase("AOVerdict")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AOVerdict));
        }
        if (FCode.equalsIgnoreCase("AOSuggestion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AOSuggestion));
        }
        if (FCode.equalsIgnoreCase("ApproveTwo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTwo));
        }
        if (FCode.equalsIgnoreCase("ApproveTwoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveTwoDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveTwoTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTwoTime));
        }
        if (FCode.equalsIgnoreCase("ATVerdict")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ATVerdict));
        }
        if (FCode.equalsIgnoreCase("ATSuggestion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ATSuggestion));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("UserState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UserState));
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IdValiDate));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("DiCardNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiCardNo));
        }
        if (FCode.equalsIgnoreCase("DiType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiType));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SeqNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthDay()));
                break;
            case 7:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(WFWFlag);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AmntType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ApplyOpertor);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApplyDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ApplyTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ApplyState);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(BackOpertor);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ApproveOne);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveOneDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ApproveOneTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(AOVerdict);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(AOSuggestion);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ApproveTwo);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveTwoDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ApproveTwoTime);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ATVerdict);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(ATSuggestion);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(UserState);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(IdValiDate);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(DiCardNo);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(DiType);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SeqNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SeqNo = FValue.trim();
            }
            else
                SeqNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("BirthDay")) {
            if(FValue != null && !FValue.equals("")) {
                BirthDay = fDate.getDate( FValue );
            }
            else
                BirthDay = null;
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("WFWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                WFWFlag = FValue.trim();
            }
            else
                WFWFlag = null;
        }
        if (FCode.equalsIgnoreCase("AmntType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AmntType = FValue.trim();
            }
            else
                AmntType = null;
        }
        if (FCode.equalsIgnoreCase("ApplyOpertor")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyOpertor = FValue.trim();
            }
            else
                ApplyOpertor = null;
        }
        if (FCode.equalsIgnoreCase("ApplyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApplyDate = fDate.getDate( FValue );
            }
            else
                ApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("ApplyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyTime = FValue.trim();
            }
            else
                ApplyTime = null;
        }
        if (FCode.equalsIgnoreCase("ApplyState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApplyState = FValue.trim();
            }
            else
                ApplyState = null;
        }
        if (FCode.equalsIgnoreCase("BackOpertor")) {
            if( FValue != null && !FValue.equals(""))
            {
                BackOpertor = FValue.trim();
            }
            else
                BackOpertor = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOne")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveOne = FValue.trim();
            }
            else
                ApproveOne = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOneDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveOneDate = fDate.getDate( FValue );
            }
            else
                ApproveOneDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOneTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveOneTime = FValue.trim();
            }
            else
                ApproveOneTime = null;
        }
        if (FCode.equalsIgnoreCase("AOVerdict")) {
            if( FValue != null && !FValue.equals(""))
            {
                AOVerdict = FValue.trim();
            }
            else
                AOVerdict = null;
        }
        if (FCode.equalsIgnoreCase("AOSuggestion")) {
            if( FValue != null && !FValue.equals(""))
            {
                AOSuggestion = FValue.trim();
            }
            else
                AOSuggestion = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTwo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTwo = FValue.trim();
            }
            else
                ApproveTwo = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTwoDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveTwoDate = fDate.getDate( FValue );
            }
            else
                ApproveTwoDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTwoTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTwoTime = FValue.trim();
            }
            else
                ApproveTwoTime = null;
        }
        if (FCode.equalsIgnoreCase("ATVerdict")) {
            if( FValue != null && !FValue.equals(""))
            {
                ATVerdict = FValue.trim();
            }
            else
                ATVerdict = null;
        }
        if (FCode.equalsIgnoreCase("ATSuggestion")) {
            if( FValue != null && !FValue.equals(""))
            {
                ATSuggestion = FValue.trim();
            }
            else
                ATSuggestion = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("UserState")) {
            if( FValue != null && !FValue.equals(""))
            {
                UserState = FValue.trim();
            }
            else
                UserState = null;
        }
        if (FCode.equalsIgnoreCase("IdValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IdValiDate = FValue.trim();
            }
            else
                IdValiDate = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("DiCardNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                DiCardNo = FValue.trim();
            }
            else
                DiCardNo = null;
        }
        if (FCode.equalsIgnoreCase("DiType")) {
            if( FValue != null && !FValue.equals(""))
            {
                DiType = FValue.trim();
            }
            else
                DiType = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMWhiteListSchema other = (LMWhiteListSchema)otherObject;
        return
            SeqNo.equals(other.getSeqNo())
            && CustomerNo.equals(other.getCustomerNo())
            && Name.equals(other.getName())
            && Sex.equals(other.getSex())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && fDate.getString(BirthDay).equals(other.getBirthDay())
            && Amnt == other.getAmnt()
            && WFWFlag.equals(other.getWFWFlag())
            && AmntType.equals(other.getAmntType())
            && ApplyOpertor.equals(other.getApplyOpertor())
            && fDate.getString(ApplyDate).equals(other.getApplyDate())
            && ApplyTime.equals(other.getApplyTime())
            && ApplyState.equals(other.getApplyState())
            && BackOpertor.equals(other.getBackOpertor())
            && ApproveOne.equals(other.getApproveOne())
            && fDate.getString(ApproveOneDate).equals(other.getApproveOneDate())
            && ApproveOneTime.equals(other.getApproveOneTime())
            && AOVerdict.equals(other.getAOVerdict())
            && AOSuggestion.equals(other.getAOSuggestion())
            && ApproveTwo.equals(other.getApproveTwo())
            && fDate.getString(ApproveTwoDate).equals(other.getApproveTwoDate())
            && ApproveTwoTime.equals(other.getApproveTwoTime())
            && ATVerdict.equals(other.getATVerdict())
            && ATSuggestion.equals(other.getATSuggestion())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && RiskCode.equals(other.getRiskCode())
            && ManageCom.equals(other.getManageCom())
            && UserState.equals(other.getUserState())
            && IdValiDate.equals(other.getIdValiDate())
            && StandByFlag1.equals(other.getStandByFlag1())
            && DiCardNo.equals(other.getDiCardNo())
            && DiType.equals(other.getDiType());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SeqNo") ) {
            return 0;
        }
        if( strFieldName.equals("CustomerNo") ) {
            return 1;
        }
        if( strFieldName.equals("Name") ) {
            return 2;
        }
        if( strFieldName.equals("Sex") ) {
            return 3;
        }
        if( strFieldName.equals("IDType") ) {
            return 4;
        }
        if( strFieldName.equals("IDNo") ) {
            return 5;
        }
        if( strFieldName.equals("BirthDay") ) {
            return 6;
        }
        if( strFieldName.equals("Amnt") ) {
            return 7;
        }
        if( strFieldName.equals("WFWFlag") ) {
            return 8;
        }
        if( strFieldName.equals("AmntType") ) {
            return 9;
        }
        if( strFieldName.equals("ApplyOpertor") ) {
            return 10;
        }
        if( strFieldName.equals("ApplyDate") ) {
            return 11;
        }
        if( strFieldName.equals("ApplyTime") ) {
            return 12;
        }
        if( strFieldName.equals("ApplyState") ) {
            return 13;
        }
        if( strFieldName.equals("BackOpertor") ) {
            return 14;
        }
        if( strFieldName.equals("ApproveOne") ) {
            return 15;
        }
        if( strFieldName.equals("ApproveOneDate") ) {
            return 16;
        }
        if( strFieldName.equals("ApproveOneTime") ) {
            return 17;
        }
        if( strFieldName.equals("AOVerdict") ) {
            return 18;
        }
        if( strFieldName.equals("AOSuggestion") ) {
            return 19;
        }
        if( strFieldName.equals("ApproveTwo") ) {
            return 20;
        }
        if( strFieldName.equals("ApproveTwoDate") ) {
            return 21;
        }
        if( strFieldName.equals("ApproveTwoTime") ) {
            return 22;
        }
        if( strFieldName.equals("ATVerdict") ) {
            return 23;
        }
        if( strFieldName.equals("ATSuggestion") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 25;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 26;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 27;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 28;
        }
        if( strFieldName.equals("UserState") ) {
            return 29;
        }
        if( strFieldName.equals("IdValiDate") ) {
            return 30;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 31;
        }
        if( strFieldName.equals("DiCardNo") ) {
            return 32;
        }
        if( strFieldName.equals("DiType") ) {
            return 33;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SeqNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "Name";
                break;
            case 3:
                strFieldName = "Sex";
                break;
            case 4:
                strFieldName = "IDType";
                break;
            case 5:
                strFieldName = "IDNo";
                break;
            case 6:
                strFieldName = "BirthDay";
                break;
            case 7:
                strFieldName = "Amnt";
                break;
            case 8:
                strFieldName = "WFWFlag";
                break;
            case 9:
                strFieldName = "AmntType";
                break;
            case 10:
                strFieldName = "ApplyOpertor";
                break;
            case 11:
                strFieldName = "ApplyDate";
                break;
            case 12:
                strFieldName = "ApplyTime";
                break;
            case 13:
                strFieldName = "ApplyState";
                break;
            case 14:
                strFieldName = "BackOpertor";
                break;
            case 15:
                strFieldName = "ApproveOne";
                break;
            case 16:
                strFieldName = "ApproveOneDate";
                break;
            case 17:
                strFieldName = "ApproveOneTime";
                break;
            case 18:
                strFieldName = "AOVerdict";
                break;
            case 19:
                strFieldName = "AOSuggestion";
                break;
            case 20:
                strFieldName = "ApproveTwo";
                break;
            case 21:
                strFieldName = "ApproveTwoDate";
                break;
            case 22:
                strFieldName = "ApproveTwoTime";
                break;
            case 23:
                strFieldName = "ATVerdict";
                break;
            case 24:
                strFieldName = "ATSuggestion";
                break;
            case 25:
                strFieldName = "ModifyDate";
                break;
            case 26:
                strFieldName = "ModifyTime";
                break;
            case 27:
                strFieldName = "RiskCode";
                break;
            case 28:
                strFieldName = "ManageCom";
                break;
            case 29:
                strFieldName = "UserState";
                break;
            case 30:
                strFieldName = "IdValiDate";
                break;
            case 31:
                strFieldName = "StandByFlag1";
                break;
            case 32:
                strFieldName = "DiCardNo";
                break;
            case 33:
                strFieldName = "DiType";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SEQNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SEX":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_DATE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "WFWFLAG":
                return Schema.TYPE_STRING;
            case "AMNTTYPE":
                return Schema.TYPE_STRING;
            case "APPLYOPERTOR":
                return Schema.TYPE_STRING;
            case "APPLYDATE":
                return Schema.TYPE_DATE;
            case "APPLYTIME":
                return Schema.TYPE_STRING;
            case "APPLYSTATE":
                return Schema.TYPE_STRING;
            case "BACKOPERTOR":
                return Schema.TYPE_STRING;
            case "APPROVEONE":
                return Schema.TYPE_STRING;
            case "APPROVEONEDATE":
                return Schema.TYPE_DATE;
            case "APPROVEONETIME":
                return Schema.TYPE_STRING;
            case "AOVERDICT":
                return Schema.TYPE_STRING;
            case "AOSUGGESTION":
                return Schema.TYPE_STRING;
            case "APPROVETWO":
                return Schema.TYPE_STRING;
            case "APPROVETWODATE":
                return Schema.TYPE_DATE;
            case "APPROVETWOTIME":
                return Schema.TYPE_STRING;
            case "ATVERDICT":
                return Schema.TYPE_STRING;
            case "ATSUGGESTION":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "USERSTATE":
                return Schema.TYPE_STRING;
            case "IDVALIDATE":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "DICARDNO":
                return Schema.TYPE_STRING;
            case "DITYPE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DATE;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_DATE;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
