/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LDComSchema;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LDComDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-03-21
 */
public class LDComDB extends LDComSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LDComDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LDCom" );
        mflag = true;
    }

    public LDComDB() {
        con = null;
        db = new DBOper( "LDCom" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LDComSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LDComSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LDCom WHERE  1=1  AND ComCode = ?");
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getComCode());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LDCom");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LDCom SET  ComCode = ? , OutComCode = ? , Name = ? , ShortName = ? , Address = ? , ZipCode = ? , Phone = ? , Fax = ? , EMail = ? , WebAddress = ? , SatrapName = ? , InsuMonitorCode = ? , InsureID = ? , SignID = ? , RegionalismCode = ? , ComNature = ? , ValidCode = ? , Sign = ? , ComCitySize = ? , ServiceName = ? , ServiceNo = ? , ServicePhone = ? , ServicePostAddress = ? , COMGRADE = ? , COMAREATYPE = ? , UPCOMCODE = ? , ISDIRUNDER = ? , COMAREATYPE1 = ? , PROVINCE = ? , CITY = ? , COUNTY = ? , FINDB = ? , LETTERSERVICENAME = ? , COMSTATE = ? , COMPREPAREDATE = ? , SUPMANAGEDATE = ? , SUPAPPROVALDATE = ? , THEOPENINGDATE = ? , COMREPLYCANCELDATE = ? , SUPREPLYCANCELDATE = ? , BUSPAPERCANCELDATE = ? WHERE  1=1  AND ComCode = ?");
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getComCode());
            }
            if(this.getOutComCode() == null || this.getOutComCode().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getOutComCode());
            }
            if(this.getName() == null || this.getName().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getName());
            }
            if(this.getShortName() == null || this.getShortName().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getShortName());
            }
            if(this.getAddress() == null || this.getAddress().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAddress());
            }
            if(this.getZipCode() == null || this.getZipCode().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getZipCode());
            }
            if(this.getPhone() == null || this.getPhone().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getPhone());
            }
            if(this.getFax() == null || this.getFax().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getFax());
            }
            if(this.getEMail() == null || this.getEMail().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getEMail());
            }
            if(this.getWebAddress() == null || this.getWebAddress().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getWebAddress());
            }
            if(this.getSatrapName() == null || this.getSatrapName().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getSatrapName());
            }
            if(this.getInsuMonitorCode() == null || this.getInsuMonitorCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getInsuMonitorCode());
            }
            if(this.getInsureID() == null || this.getInsureID().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getInsureID());
            }
            if(this.getSignID() == null || this.getSignID().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getSignID());
            }
            if(this.getRegionalismCode() == null || this.getRegionalismCode().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getRegionalismCode());
            }
            if(this.getComNature() == null || this.getComNature().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getComNature());
            }
            if(this.getValidCode() == null || this.getValidCode().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getValidCode());
            }
            if(this.getSign() == null || this.getSign().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getSign());
            }
            if(this.getComCitySize() == null || this.getComCitySize().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getComCitySize());
            }
            if(this.getServiceName() == null || this.getServiceName().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getServiceName());
            }
            if(this.getServiceNo() == null || this.getServiceNo().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getServiceNo());
            }
            if(this.getServicePhone() == null || this.getServicePhone().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getServicePhone());
            }
            if(this.getServicePostAddress() == null || this.getServicePostAddress().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getServicePostAddress());
            }
            if(this.getCOMGRADE() == null || this.getCOMGRADE().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getCOMGRADE());
            }
            if(this.getCOMAREATYPE() == null || this.getCOMAREATYPE().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getCOMAREATYPE());
            }
            if(this.getUPCOMCODE() == null || this.getUPCOMCODE().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getUPCOMCODE());
            }
            if(this.getISDIRUNDER() == null || this.getISDIRUNDER().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getISDIRUNDER());
            }
            if(this.getCOMAREATYPE1() == null || this.getCOMAREATYPE1().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getCOMAREATYPE1());
            }
            if(this.getPROVINCE() == null || this.getPROVINCE().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getPROVINCE());
            }
            if(this.getCITY() == null || this.getCITY().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getCITY());
            }
            if(this.getCOUNTY() == null || this.getCOUNTY().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getCOUNTY());
            }
            if(this.getFINDB() == null || this.getFINDB().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getFINDB());
            }
            if(this.getLETTERSERVICENAME() == null || this.getLETTERSERVICENAME().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getLETTERSERVICENAME());
            }
            if(this.getCOMSTATE() == null || this.getCOMSTATE().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getCOMSTATE());
            }
            if(this.getCOMPREPAREDATE() == null || this.getCOMPREPAREDATE().equals("null")) {
            	pstmt.setNull(35, 93);
            } else {
            	pstmt.setDate(35, Date.valueOf(this.getCOMPREPAREDATE()));
            }
            if(this.getSUPMANAGEDATE() == null || this.getSUPMANAGEDATE().equals("null")) {
            	pstmt.setNull(36, 93);
            } else {
            	pstmt.setDate(36, Date.valueOf(this.getSUPMANAGEDATE()));
            }
            if(this.getSUPAPPROVALDATE() == null || this.getSUPAPPROVALDATE().equals("null")) {
            	pstmt.setNull(37, 93);
            } else {
            	pstmt.setDate(37, Date.valueOf(this.getSUPAPPROVALDATE()));
            }
            if(this.getTHEOPENINGDATE() == null || this.getTHEOPENINGDATE().equals("null")) {
            	pstmt.setNull(38, 93);
            } else {
            	pstmt.setDate(38, Date.valueOf(this.getTHEOPENINGDATE()));
            }
            if(this.getCOMREPLYCANCELDATE() == null || this.getCOMREPLYCANCELDATE().equals("null")) {
            	pstmt.setNull(39, 93);
            } else {
            	pstmt.setDate(39, Date.valueOf(this.getCOMREPLYCANCELDATE()));
            }
            if(this.getSUPREPLYCANCELDATE() == null || this.getSUPREPLYCANCELDATE().equals("null")) {
            	pstmt.setNull(40, 93);
            } else {
            	pstmt.setDate(40, Date.valueOf(this.getSUPREPLYCANCELDATE()));
            }
            if(this.getBUSPAPERCANCELDATE() == null || this.getBUSPAPERCANCELDATE().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getBUSPAPERCANCELDATE()));
            }
            // set where condition
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getComCode());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LDCom");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LDCom VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getComCode());
            }
            if(this.getOutComCode() == null || this.getOutComCode().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getOutComCode());
            }
            if(this.getName() == null || this.getName().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getName());
            }
            if(this.getShortName() == null || this.getShortName().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getShortName());
            }
            if(this.getAddress() == null || this.getAddress().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAddress());
            }
            if(this.getZipCode() == null || this.getZipCode().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getZipCode());
            }
            if(this.getPhone() == null || this.getPhone().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getPhone());
            }
            if(this.getFax() == null || this.getFax().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getFax());
            }
            if(this.getEMail() == null || this.getEMail().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getEMail());
            }
            if(this.getWebAddress() == null || this.getWebAddress().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getWebAddress());
            }
            if(this.getSatrapName() == null || this.getSatrapName().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getSatrapName());
            }
            if(this.getInsuMonitorCode() == null || this.getInsuMonitorCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getInsuMonitorCode());
            }
            if(this.getInsureID() == null || this.getInsureID().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getInsureID());
            }
            if(this.getSignID() == null || this.getSignID().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getSignID());
            }
            if(this.getRegionalismCode() == null || this.getRegionalismCode().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getRegionalismCode());
            }
            if(this.getComNature() == null || this.getComNature().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getComNature());
            }
            if(this.getValidCode() == null || this.getValidCode().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getValidCode());
            }
            if(this.getSign() == null || this.getSign().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getSign());
            }
            if(this.getComCitySize() == null || this.getComCitySize().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getComCitySize());
            }
            if(this.getServiceName() == null || this.getServiceName().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getServiceName());
            }
            if(this.getServiceNo() == null || this.getServiceNo().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getServiceNo());
            }
            if(this.getServicePhone() == null || this.getServicePhone().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getServicePhone());
            }
            if(this.getServicePostAddress() == null || this.getServicePostAddress().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getServicePostAddress());
            }
            if(this.getCOMGRADE() == null || this.getCOMGRADE().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getCOMGRADE());
            }
            if(this.getCOMAREATYPE() == null || this.getCOMAREATYPE().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getCOMAREATYPE());
            }
            if(this.getUPCOMCODE() == null || this.getUPCOMCODE().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getUPCOMCODE());
            }
            if(this.getISDIRUNDER() == null || this.getISDIRUNDER().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getISDIRUNDER());
            }
            if(this.getCOMAREATYPE1() == null || this.getCOMAREATYPE1().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getCOMAREATYPE1());
            }
            if(this.getPROVINCE() == null || this.getPROVINCE().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getPROVINCE());
            }
            if(this.getCITY() == null || this.getCITY().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getCITY());
            }
            if(this.getCOUNTY() == null || this.getCOUNTY().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getCOUNTY());
            }
            if(this.getFINDB() == null || this.getFINDB().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getFINDB());
            }
            if(this.getLETTERSERVICENAME() == null || this.getLETTERSERVICENAME().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getLETTERSERVICENAME());
            }
            if(this.getCOMSTATE() == null || this.getCOMSTATE().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getCOMSTATE());
            }
            if(this.getCOMPREPAREDATE() == null || this.getCOMPREPAREDATE().equals("null")) {
            	pstmt.setNull(35, 93);
            } else {
            	pstmt.setDate(35, Date.valueOf(this.getCOMPREPAREDATE()));
            }
            if(this.getSUPMANAGEDATE() == null || this.getSUPMANAGEDATE().equals("null")) {
            	pstmt.setNull(36, 93);
            } else {
            	pstmt.setDate(36, Date.valueOf(this.getSUPMANAGEDATE()));
            }
            if(this.getSUPAPPROVALDATE() == null || this.getSUPAPPROVALDATE().equals("null")) {
            	pstmt.setNull(37, 93);
            } else {
            	pstmt.setDate(37, Date.valueOf(this.getSUPAPPROVALDATE()));
            }
            if(this.getTHEOPENINGDATE() == null || this.getTHEOPENINGDATE().equals("null")) {
            	pstmt.setNull(38, 93);
            } else {
            	pstmt.setDate(38, Date.valueOf(this.getTHEOPENINGDATE()));
            }
            if(this.getCOMREPLYCANCELDATE() == null || this.getCOMREPLYCANCELDATE().equals("null")) {
            	pstmt.setNull(39, 93);
            } else {
            	pstmt.setDate(39, Date.valueOf(this.getCOMREPLYCANCELDATE()));
            }
            if(this.getSUPREPLYCANCELDATE() == null || this.getSUPREPLYCANCELDATE().equals("null")) {
            	pstmt.setNull(40, 93);
            } else {
            	pstmt.setDate(40, Date.valueOf(this.getSUPREPLYCANCELDATE()));
            }
            if(this.getBUSPAPERCANCELDATE() == null || this.getBUSPAPERCANCELDATE().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getBUSPAPERCANCELDATE()));
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LDCom WHERE  1=1  AND ComCode = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getComCode() == null || this.getComCode().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getComCode());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDComDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LDComSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LDComSet aLDComSet = new LDComSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDCom");
            LDComSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDComDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LDComSchema s1 = new LDComSchema();
                s1.setSchema(rs,i);
                aLDComSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLDComSet;
    }

    public LDComSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LDComSet aLDComSet = new LDComSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDComDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LDComSchema s1 = new LDComSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDComDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLDComSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDComSet;
    }

    public LDComSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LDComSet aLDComSet = new LDComSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDCom");
            LDComSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LDComSchema s1 = new LDComSchema();
                s1.setSchema(rs,i);
                aLDComSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDComSet;
    }

    public LDComSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LDComSet aLDComSet = new LDComSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LDComSchema s1 = new LDComSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDComDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLDComSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDComSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDCom");
            LDComSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LDCom " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LDComDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LDComSet
     */
    public LDComSet getData() {
        int tCount = 0;
        LDComSet tLDComSet = new LDComSet();
        LDComSchema tLDComSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLDComSchema = new LDComSchema();
            tLDComSchema.setSchema(mResultSet, 1);
            tLDComSet.add(tLDComSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLDComSchema = new LDComSchema();
                    tLDComSchema.setSchema(mResultSet, 1);
                    tLDComSet.add(tLDComSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLDComSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LDComDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LDComDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LDComDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
