/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMProdSetInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LMProdSetInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMProdSetInfoSchema implements Schema, Cloneable {
    // @Field
    /** 产品组合编码 */
    private String ProdSetCode;
    /** 产品组合名称 */
    private String ProdSetName;
    /** 产品组合简称 */
    private String ProdSetShortName;
    /** 产品组合英文名称 */
    private String ProdSetEnName;
    /** 产品组合英文简称 */
    private String ProdSetEnShortName;
    /** 销售渠道 */
    private String SaleChnl;
    /** 产品组合停售日期 */
    private Date EndDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** Standbyflag1 */
    private String STANDBYFLAG1;
    /** Standbyflag2 */
    private String STANDBYFLAG2;
    /** Standbyflag3 */
    private Date STANDBYFLAG3;
    /** Standbyflag4 */
    private Date STANDBYFLAG4;
    /** Standbyflag5 */
    private double STANDBYFLAG5;
    /** Standbyflag6 */
    private double STANDBYFLAG6;
    /** 产品组合启售日期 */
    private Date StartDate;
    /** 序号 */
    private String ProdSetID;

    public static final int FIELDNUM = 20;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMProdSetInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ProdSetID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMProdSetInfoSchema cloned = (LMProdSetInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getProdSetName() {
        return ProdSetName;
    }
    public void setProdSetName(String aProdSetName) {
        ProdSetName = aProdSetName;
    }
    public String getProdSetShortName() {
        return ProdSetShortName;
    }
    public void setProdSetShortName(String aProdSetShortName) {
        ProdSetShortName = aProdSetShortName;
    }
    public String getProdSetEnName() {
        return ProdSetEnName;
    }
    public void setProdSetEnName(String aProdSetEnName) {
        ProdSetEnName = aProdSetEnName;
    }
    public String getProdSetEnShortName() {
        return ProdSetEnShortName;
    }
    public void setProdSetEnShortName(String aProdSetEnShortName) {
        ProdSetEnShortName = aProdSetEnShortName;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getEndDate() {
        if(EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }
    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }
    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else
            EndDate = null;
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getSTANDBYFLAG1() {
        return STANDBYFLAG1;
    }
    public void setSTANDBYFLAG1(String aSTANDBYFLAG1) {
        STANDBYFLAG1 = aSTANDBYFLAG1;
    }
    public String getSTANDBYFLAG2() {
        return STANDBYFLAG2;
    }
    public void setSTANDBYFLAG2(String aSTANDBYFLAG2) {
        STANDBYFLAG2 = aSTANDBYFLAG2;
    }
    public String getSTANDBYFLAG3() {
        if(STANDBYFLAG3 != null) {
            return fDate.getString(STANDBYFLAG3);
        } else {
            return null;
        }
    }
    public void setSTANDBYFLAG3(Date aSTANDBYFLAG3) {
        STANDBYFLAG3 = aSTANDBYFLAG3;
    }
    public void setSTANDBYFLAG3(String aSTANDBYFLAG3) {
        if (aSTANDBYFLAG3 != null && !aSTANDBYFLAG3.equals("")) {
            STANDBYFLAG3 = fDate.getDate(aSTANDBYFLAG3);
        } else
            STANDBYFLAG3 = null;
    }

    public String getSTANDBYFLAG4() {
        if(STANDBYFLAG4 != null) {
            return fDate.getString(STANDBYFLAG4);
        } else {
            return null;
        }
    }
    public void setSTANDBYFLAG4(Date aSTANDBYFLAG4) {
        STANDBYFLAG4 = aSTANDBYFLAG4;
    }
    public void setSTANDBYFLAG4(String aSTANDBYFLAG4) {
        if (aSTANDBYFLAG4 != null && !aSTANDBYFLAG4.equals("")) {
            STANDBYFLAG4 = fDate.getDate(aSTANDBYFLAG4);
        } else
            STANDBYFLAG4 = null;
    }

    public double getSTANDBYFLAG5() {
        return STANDBYFLAG5;
    }
    public void setSTANDBYFLAG5(double aSTANDBYFLAG5) {
        STANDBYFLAG5 = aSTANDBYFLAG5;
    }
    public void setSTANDBYFLAG5(String aSTANDBYFLAG5) {
        if (aSTANDBYFLAG5 != null && !aSTANDBYFLAG5.equals("")) {
            Double tDouble = new Double(aSTANDBYFLAG5);
            double d = tDouble.doubleValue();
            STANDBYFLAG5 = d;
        }
    }

    public double getSTANDBYFLAG6() {
        return STANDBYFLAG6;
    }
    public void setSTANDBYFLAG6(double aSTANDBYFLAG6) {
        STANDBYFLAG6 = aSTANDBYFLAG6;
    }
    public void setSTANDBYFLAG6(String aSTANDBYFLAG6) {
        if (aSTANDBYFLAG6 != null && !aSTANDBYFLAG6.equals("")) {
            Double tDouble = new Double(aSTANDBYFLAG6);
            double d = tDouble.doubleValue();
            STANDBYFLAG6 = d;
        }
    }

    public String getStartDate() {
        if(StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }
    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }
    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else
            StartDate = null;
    }

    public String getProdSetID() {
        return ProdSetID;
    }
    public void setProdSetID(String aProdSetID) {
        ProdSetID = aProdSetID;
    }

    /**
    * 使用另外一个 LMProdSetInfoSchema 对象给 Schema 赋值
    * @param: aLMProdSetInfoSchema LMProdSetInfoSchema
    **/
    public void setSchema(LMProdSetInfoSchema aLMProdSetInfoSchema) {
        this.ProdSetCode = aLMProdSetInfoSchema.getProdSetCode();
        this.ProdSetName = aLMProdSetInfoSchema.getProdSetName();
        this.ProdSetShortName = aLMProdSetInfoSchema.getProdSetShortName();
        this.ProdSetEnName = aLMProdSetInfoSchema.getProdSetEnName();
        this.ProdSetEnShortName = aLMProdSetInfoSchema.getProdSetEnShortName();
        this.SaleChnl = aLMProdSetInfoSchema.getSaleChnl();
        this.EndDate = fDate.getDate( aLMProdSetInfoSchema.getEndDate());
        this.Operator = aLMProdSetInfoSchema.getOperator();
        this.MakeDate = fDate.getDate( aLMProdSetInfoSchema.getMakeDate());
        this.MakeTime = aLMProdSetInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLMProdSetInfoSchema.getModifyDate());
        this.ModifyTime = aLMProdSetInfoSchema.getModifyTime();
        this.STANDBYFLAG1 = aLMProdSetInfoSchema.getSTANDBYFLAG1();
        this.STANDBYFLAG2 = aLMProdSetInfoSchema.getSTANDBYFLAG2();
        this.STANDBYFLAG3 = fDate.getDate( aLMProdSetInfoSchema.getSTANDBYFLAG3());
        this.STANDBYFLAG4 = fDate.getDate( aLMProdSetInfoSchema.getSTANDBYFLAG4());
        this.STANDBYFLAG5 = aLMProdSetInfoSchema.getSTANDBYFLAG5();
        this.STANDBYFLAG6 = aLMProdSetInfoSchema.getSTANDBYFLAG6();
        this.StartDate = fDate.getDate( aLMProdSetInfoSchema.getStartDate());
        this.ProdSetID = aLMProdSetInfoSchema.getProdSetID();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ProdSetCode") == null )
                this.ProdSetCode = null;
            else
                this.ProdSetCode = rs.getString("ProdSetCode").trim();

            if( rs.getString("ProdSetName") == null )
                this.ProdSetName = null;
            else
                this.ProdSetName = rs.getString("ProdSetName").trim();

            if( rs.getString("ProdSetShortName") == null )
                this.ProdSetShortName = null;
            else
                this.ProdSetShortName = rs.getString("ProdSetShortName").trim();

            if( rs.getString("ProdSetEnName") == null )
                this.ProdSetEnName = null;
            else
                this.ProdSetEnName = rs.getString("ProdSetEnName").trim();

            if( rs.getString("ProdSetEnShortName") == null )
                this.ProdSetEnShortName = null;
            else
                this.ProdSetEnShortName = rs.getString("ProdSetEnShortName").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            this.EndDate = rs.getDate("EndDate");
            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("STANDBYFLAG1") == null )
                this.STANDBYFLAG1 = null;
            else
                this.STANDBYFLAG1 = rs.getString("STANDBYFLAG1").trim();

            if( rs.getString("STANDBYFLAG2") == null )
                this.STANDBYFLAG2 = null;
            else
                this.STANDBYFLAG2 = rs.getString("STANDBYFLAG2").trim();

            this.STANDBYFLAG3 = rs.getDate("STANDBYFLAG3");
            this.STANDBYFLAG4 = rs.getDate("STANDBYFLAG4");
            this.STANDBYFLAG5 = rs.getDouble("STANDBYFLAG5");
            this.STANDBYFLAG6 = rs.getDouble("STANDBYFLAG6");
            this.StartDate = rs.getDate("StartDate");
            if( rs.getString("ProdSetID") == null )
                this.ProdSetID = null;
            else
                this.ProdSetID = rs.getString("ProdSetID").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMProdSetInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMProdSetInfoSchema getSchema() {
        LMProdSetInfoSchema aLMProdSetInfoSchema = new LMProdSetInfoSchema();
        aLMProdSetInfoSchema.setSchema(this);
        return aLMProdSetInfoSchema;
    }

    public LMProdSetInfoDB getDB() {
        LMProdSetInfoDB aDBOper = new LMProdSetInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMProdSetInfo描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ProdSetCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetShortName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetEnName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetEnShortName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(STANDBYFLAG1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(STANDBYFLAG2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( STANDBYFLAG3 ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( STANDBYFLAG4 ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(STANDBYFLAG5));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(STANDBYFLAG6));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetID));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMProdSetInfo>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ProdSetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            ProdSetName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ProdSetShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ProdSetEnName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ProdSetEnShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            STANDBYFLAG1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            STANDBYFLAG2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            STANDBYFLAG3 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER));
            STANDBYFLAG4 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            STANDBYFLAG5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17, SysConst.PACKAGESPILTER))).doubleValue();
            STANDBYFLAG6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18, SysConst.PACKAGESPILTER))).doubleValue();
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            ProdSetID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMProdSetInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetName));
        }
        if (FCode.equalsIgnoreCase("ProdSetShortName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetShortName));
        }
        if (FCode.equalsIgnoreCase("ProdSetEnName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetEnName));
        }
        if (FCode.equalsIgnoreCase("ProdSetEnShortName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetEnShortName));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG1));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG2));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSTANDBYFLAG3()));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSTANDBYFLAG4()));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG5));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG6));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
        }
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ProdSetCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProdSetName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ProdSetShortName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ProdSetEnName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ProdSetEnShortName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(STANDBYFLAG1);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(STANDBYFLAG2);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSTANDBYFLAG3()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSTANDBYFLAG4()));
                break;
            case 16:
                strFieldValue = String.valueOf(STANDBYFLAG5);
                break;
            case 17:
                strFieldValue = String.valueOf(STANDBYFLAG6);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ProdSetID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetName = FValue.trim();
            }
            else
                ProdSetName = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetShortName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetShortName = FValue.trim();
            }
            else
                ProdSetShortName = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetEnName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetEnName = FValue.trim();
            }
            else
                ProdSetEnName = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetEnShortName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetEnShortName = FValue.trim();
            }
            else
                ProdSetEnShortName = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if(FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate( FValue );
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG1 = FValue.trim();
            }
            else
                STANDBYFLAG1 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG2")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG2 = FValue.trim();
            }
            else
                STANDBYFLAG2 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG3")) {
            if(FValue != null && !FValue.equals("")) {
                STANDBYFLAG3 = fDate.getDate( FValue );
            }
            else
                STANDBYFLAG3 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG4")) {
            if(FValue != null && !FValue.equals("")) {
                STANDBYFLAG4 = fDate.getDate( FValue );
            }
            else
                STANDBYFLAG4 = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG5")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                STANDBYFLAG5 = d;
            }
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG6")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                STANDBYFLAG6 = d;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate( FValue );
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetID = FValue.trim();
            }
            else
                ProdSetID = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMProdSetInfoSchema other = (LMProdSetInfoSchema)otherObject;
        return
            ProdSetCode.equals(other.getProdSetCode())
            && ProdSetName.equals(other.getProdSetName())
            && ProdSetShortName.equals(other.getProdSetShortName())
            && ProdSetEnName.equals(other.getProdSetEnName())
            && ProdSetEnShortName.equals(other.getProdSetEnShortName())
            && SaleChnl.equals(other.getSaleChnl())
            && fDate.getString(EndDate).equals(other.getEndDate())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && STANDBYFLAG1.equals(other.getSTANDBYFLAG1())
            && STANDBYFLAG2.equals(other.getSTANDBYFLAG2())
            && fDate.getString(STANDBYFLAG3).equals(other.getSTANDBYFLAG3())
            && fDate.getString(STANDBYFLAG4).equals(other.getSTANDBYFLAG4())
            && STANDBYFLAG5 == other.getSTANDBYFLAG5()
            && STANDBYFLAG6 == other.getSTANDBYFLAG6()
            && fDate.getString(StartDate).equals(other.getStartDate())
            && ProdSetID.equals(other.getProdSetID());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ProdSetCode") ) {
            return 0;
        }
        if( strFieldName.equals("ProdSetName") ) {
            return 1;
        }
        if( strFieldName.equals("ProdSetShortName") ) {
            return 2;
        }
        if( strFieldName.equals("ProdSetEnName") ) {
            return 3;
        }
        if( strFieldName.equals("ProdSetEnShortName") ) {
            return 4;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 5;
        }
        if( strFieldName.equals("EndDate") ) {
            return 6;
        }
        if( strFieldName.equals("Operator") ) {
            return 7;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 8;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 11;
        }
        if( strFieldName.equals("STANDBYFLAG1") ) {
            return 12;
        }
        if( strFieldName.equals("STANDBYFLAG2") ) {
            return 13;
        }
        if( strFieldName.equals("STANDBYFLAG3") ) {
            return 14;
        }
        if( strFieldName.equals("STANDBYFLAG4") ) {
            return 15;
        }
        if( strFieldName.equals("STANDBYFLAG5") ) {
            return 16;
        }
        if( strFieldName.equals("STANDBYFLAG6") ) {
            return 17;
        }
        if( strFieldName.equals("StartDate") ) {
            return 18;
        }
        if( strFieldName.equals("ProdSetID") ) {
            return 19;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ProdSetCode";
                break;
            case 1:
                strFieldName = "ProdSetName";
                break;
            case 2:
                strFieldName = "ProdSetShortName";
                break;
            case 3:
                strFieldName = "ProdSetEnName";
                break;
            case 4:
                strFieldName = "ProdSetEnShortName";
                break;
            case 5:
                strFieldName = "SaleChnl";
                break;
            case 6:
                strFieldName = "EndDate";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "STANDBYFLAG1";
                break;
            case 13:
                strFieldName = "STANDBYFLAG2";
                break;
            case 14:
                strFieldName = "STANDBYFLAG3";
                break;
            case 15:
                strFieldName = "STANDBYFLAG4";
                break;
            case 16:
                strFieldName = "STANDBYFLAG5";
                break;
            case 17:
                strFieldName = "STANDBYFLAG6";
                break;
            case 18:
                strFieldName = "StartDate";
                break;
            case 19:
                strFieldName = "ProdSetID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "PRODSETNAME":
                return Schema.TYPE_STRING;
            case "PRODSETSHORTNAME":
                return Schema.TYPE_STRING;
            case "PRODSETENNAME":
                return Schema.TYPE_STRING;
            case "PRODSETENSHORTNAME":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_DATE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_DATE;
            case "STANDBYFLAG4":
                return Schema.TYPE_DATE;
            case "STANDBYFLAG5":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG6":
                return Schema.TYPE_DOUBLE;
            case "STARTDATE":
                return Schema.TYPE_DATE;
            case "PRODSETID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DATE;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_DOUBLE;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
