/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LKCodeMapping_PolicySchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/**
 * <p>ClassName: LKCodeMapping_PolicySet </p>
 * <p>Description: LKCodeMapping_PolicySchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-07-05
 */
public class LKCodeMapping_PolicySet extends SchemaSet {
	// @Method
	public boolean add(LKCodeMapping_PolicySchema aSchema) {
		return super.add(aSchema);
	}

	public boolean add(LKCodeMapping_PolicySet aSet) {
		return super.add(aSet);
	}

	public boolean remove(LKCodeMapping_PolicySchema aSchema) {
		return super.remove(aSchema);
	}

	public LKCodeMapping_PolicySchema get(int index) {
		LKCodeMapping_PolicySchema tSchema = (LKCodeMapping_PolicySchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LKCodeMapping_PolicySchema aSchema) {
		return super.set(index,aSchema);
	}

	public boolean set(LKCodeMapping_PolicySet aSet) {
		return super.set(aSet);
	}

	/**
	 * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKCodeMapping_Policy描述/A>表字段
	 * @return: String 返回打包后字符串
	 **/
	public String encode() {
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++) {
			LKCodeMapping_PolicySchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	 * 数据解包
	 * @param: str String 打包后字符串
	 * @return: boolean
	 **/
	public boolean decode( String str ) {
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 ) {
			LKCodeMapping_PolicySchema aSchema = new LKCodeMapping_PolicySchema();
			if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
				this.add(aSchema);
				nBeginPos = nEndPos + 1;
				nEndPos = str.indexOf('^', nEndPos + 1);
			} else {
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LKCodeMapping_PolicySchema tSchema = new LKCodeMapping_PolicySchema();
		if(tSchema.decode(str.substring(nBeginPos))) {
			this.add(tSchema);
			return true;
		} else {
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
