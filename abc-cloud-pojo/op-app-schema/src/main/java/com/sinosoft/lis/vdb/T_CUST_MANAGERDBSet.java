/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.T_CUST_MANAGERSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: T_CUST_MANAGERDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */
public class T_CUST_MANAGERDBSet extends T_CUST_MANAGERSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public T_CUST_MANAGERDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"T_CUST_MANAGER");
        mflag = true;
    }

    public T_CUST_MANAGERDBSet() {
        db = new DBOper( "T_CUST_MANAGER" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGERDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM T_CUST_MANAGER WHERE  1=1  AND CUST_MANAGER_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getCUST_MANAGER_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGERDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE T_CUST_MANAGER SET  CUST_MANAGER_ID = ? , CUST_MANAGER_CODE = ? , CUST_MANAGER_LEVEL = ? , ENABLE_STATUS = ? , USER_ID = ? , REMARK = ? , INSERT_OPER = ? , INSERT_CONSIGNOR = ? , INSERT_TIME = ? , UPDATE_OPER = ? , UPDATE_CONSIGNOR = ? , UPDATE_TIME = ? , CUST_NAME = ? , MNGORG_ID = ? WHERE  1=1  AND CUST_MANAGER_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getCUST_MANAGER_ID());
            if(this.get(i).getCUST_MANAGER_CODE() == null || this.get(i).getCUST_MANAGER_CODE().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCUST_MANAGER_CODE());
            }
            if(this.get(i).getCUST_MANAGER_LEVEL() == null || this.get(i).getCUST_MANAGER_LEVEL().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getCUST_MANAGER_LEVEL());
            }
            if(this.get(i).getENABLE_STATUS() == null || this.get(i).getENABLE_STATUS().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getENABLE_STATUS());
            }
            pstmt.setLong(5, this.get(i).getUSER_ID());
            if(this.get(i).getREMARK() == null || this.get(i).getREMARK().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getREMARK());
            }
            if(this.get(i).getINSERT_OPER() == null || this.get(i).getINSERT_OPER().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getINSERT_OPER());
            }
            if(this.get(i).getINSERT_CONSIGNOR() == null || this.get(i).getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getINSERT_CONSIGNOR());
            }
            if(this.get(i).getINSERT_TIME() == null || this.get(i).getINSERT_TIME().equals("null")) {
                pstmt.setDate(9,null);
            } else {
                pstmt.setDate(9, Date.valueOf(this.get(i).getINSERT_TIME()));
            }
            if(this.get(i).getUPDATE_OPER() == null || this.get(i).getUPDATE_OPER().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getUPDATE_OPER());
            }
            if(this.get(i).getUPDATE_CONSIGNOR() == null || this.get(i).getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getUPDATE_CONSIGNOR());
            }
            if(this.get(i).getUPDATE_TIME() == null || this.get(i).getUPDATE_TIME().equals("null")) {
                pstmt.setDate(12,null);
            } else {
                pstmt.setDate(12, Date.valueOf(this.get(i).getUPDATE_TIME()));
            }
            if(this.get(i).getCUST_NAME() == null || this.get(i).getCUST_NAME().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getCUST_NAME());
            }
            pstmt.setLong(14, this.get(i).getMNGORG_ID());
            // set where condition
            pstmt.setLong(15, this.get(i).getCUST_MANAGER_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGERDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO T_CUST_MANAGER VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getCUST_MANAGER_ID());
            if(this.get(i).getCUST_MANAGER_CODE() == null || this.get(i).getCUST_MANAGER_CODE().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCUST_MANAGER_CODE());
            }
            if(this.get(i).getCUST_MANAGER_LEVEL() == null || this.get(i).getCUST_MANAGER_LEVEL().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getCUST_MANAGER_LEVEL());
            }
            if(this.get(i).getENABLE_STATUS() == null || this.get(i).getENABLE_STATUS().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getENABLE_STATUS());
            }
            pstmt.setLong(5, this.get(i).getUSER_ID());
            if(this.get(i).getREMARK() == null || this.get(i).getREMARK().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getREMARK());
            }
            if(this.get(i).getINSERT_OPER() == null || this.get(i).getINSERT_OPER().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getINSERT_OPER());
            }
            if(this.get(i).getINSERT_CONSIGNOR() == null || this.get(i).getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getINSERT_CONSIGNOR());
            }
            if(this.get(i).getINSERT_TIME() == null || this.get(i).getINSERT_TIME().equals("null")) {
                pstmt.setDate(9,null);
            } else {
                pstmt.setDate(9, Date.valueOf(this.get(i).getINSERT_TIME()));
            }
            if(this.get(i).getUPDATE_OPER() == null || this.get(i).getUPDATE_OPER().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getUPDATE_OPER());
            }
            if(this.get(i).getUPDATE_CONSIGNOR() == null || this.get(i).getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getUPDATE_CONSIGNOR());
            }
            if(this.get(i).getUPDATE_TIME() == null || this.get(i).getUPDATE_TIME().equals("null")) {
                pstmt.setDate(12,null);
            } else {
                pstmt.setDate(12, Date.valueOf(this.get(i).getUPDATE_TIME()));
            }
            if(this.get(i).getCUST_NAME() == null || this.get(i).getCUST_NAME().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getCUST_NAME());
            }
            pstmt.setLong(14, this.get(i).getMNGORG_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGERDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
