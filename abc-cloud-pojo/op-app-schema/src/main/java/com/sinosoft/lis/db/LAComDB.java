/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LAComDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-11-25
 */
public class LAComDB extends LAComSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LAComDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LACom" );
        mflag = true;
    }

    public LAComDB() {
        con = null;
        db = new DBOper( "LACom" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LAComSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LAComSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LACom WHERE  1=1  AND AgentCom = ?");
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getAgentCom());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LACom");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LACom SET  AgentCom = ? , ManageCom = ? , AreaType = ? , ChannelType = ? , UpAgentCom = ? , Name = ? , Address = ? , ZipCode = ? , Phone = ? , Fax = ? , EMail = ? , WebAddress = ? , LinkMan = ? , Password = ? , Corporation = ? , BankCode = ? , BankAccNo = ? , BusinessType = ? , GrpNature = ? , ACType = ? , SellFlag = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , BankType = ? , CalFlag = ? , BusiLicenseCode = ? , InsureID = ? , InsurePrincipal = ? , ChiefBusiness = ? , BusiAddress = ? , SubscribeMan = ? , SubscribeManDuty = ? , LicenseNo = ? , RegionalismCode = ? , AppAgentCom = ? , State = ? , Noti = ? , BusinessCode = ? , LicenseStartDate = ? , LicenseEndDate = ? , BranchType = ? , BranchType2 = ? , Assets = ? , Income = ? , Profits = ? , PersonnalSum = ? , ProtocalNo = ? , HeadOffice = ? , FoundDate = ? , EndDate = ? , Bank = ? , AccName = ? , DraWer = ? , DraWerAccNo = ? , RepManageCom = ? , DraWerAccCode = ? , DraWerAccName = ? , ChannelType2 = ? , AreaType2 = ? , ComToAgentflag = ? , AgentCode = ? , AgentComCode = ? , CooperateBank = ? , CooperationEndDate = ? , CooperationStartDate = ? , AgentType = ? , NewBankCode = ? , BankCity = ? WHERE  1=1  AND AgentCom = ?");
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getAgentCom());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getManageCom());
            }
            if(this.getAreaType() == null || this.getAreaType().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getAreaType());
            }
            if(this.getChannelType() == null || this.getChannelType().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getChannelType());
            }
            if(this.getUpAgentCom() == null || this.getUpAgentCom().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getUpAgentCom());
            }
            if(this.getName() == null || this.getName().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getName());
            }
            if(this.getAddress() == null || this.getAddress().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getAddress());
            }
            if(this.getZipCode() == null || this.getZipCode().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getZipCode());
            }
            if(this.getPhone() == null || this.getPhone().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getPhone());
            }
            if(this.getFax() == null || this.getFax().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getFax());
            }
            if(this.getEMail() == null || this.getEMail().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getEMail());
            }
            if(this.getWebAddress() == null || this.getWebAddress().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getWebAddress());
            }
            if(this.getLinkMan() == null || this.getLinkMan().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getLinkMan());
            }
            if(this.getPassword() == null || this.getPassword().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getPassword());
            }
            if(this.getCorporation() == null || this.getCorporation().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getCorporation());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getBankAccNo());
            }
            if(this.getBusinessType() == null || this.getBusinessType().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getBusinessType());
            }
            if(this.getGrpNature() == null || this.getGrpNature().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getGrpNature());
            }
            if(this.getACType() == null || this.getACType().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getACType());
            }
            if(this.getSellFlag() == null || this.getSellFlag().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getSellFlag());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(23, 93);
            } else {
            	pstmt.setDate(23, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getModifyTime());
            }
            if(this.getBankType() == null || this.getBankType().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getBankType());
            }
            if(this.getCalFlag() == null || this.getCalFlag().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getCalFlag());
            }
            if(this.getBusiLicenseCode() == null || this.getBusiLicenseCode().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getBusiLicenseCode());
            }
            if(this.getInsureID() == null || this.getInsureID().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getInsureID());
            }
            if(this.getInsurePrincipal() == null || this.getInsurePrincipal().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getInsurePrincipal());
            }
            if(this.getChiefBusiness() == null || this.getChiefBusiness().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getChiefBusiness());
            }
            if(this.getBusiAddress() == null || this.getBusiAddress().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getBusiAddress());
            }
            if(this.getSubscribeMan() == null || this.getSubscribeMan().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getSubscribeMan());
            }
            if(this.getSubscribeManDuty() == null || this.getSubscribeManDuty().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getSubscribeManDuty());
            }
            if(this.getLicenseNo() == null || this.getLicenseNo().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getLicenseNo());
            }
            if(this.getRegionalismCode() == null || this.getRegionalismCode().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getRegionalismCode());
            }
            if(this.getAppAgentCom() == null || this.getAppAgentCom().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getAppAgentCom());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getState());
            }
            if(this.getNoti() == null || this.getNoti().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getNoti());
            }
            if(this.getBusinessCode() == null || this.getBusinessCode().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getBusinessCode());
            }
            if(this.getLicenseStartDate() == null || this.getLicenseStartDate().equals("null")) {
            	pstmt.setNull(42, 93);
            } else {
            	pstmt.setDate(42, Date.valueOf(this.getLicenseStartDate()));
            }
            if(this.getLicenseEndDate() == null || this.getLicenseEndDate().equals("null")) {
            	pstmt.setNull(43, 93);
            } else {
            	pstmt.setDate(43, Date.valueOf(this.getLicenseEndDate()));
            }
            if(this.getBranchType() == null || this.getBranchType().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getBranchType());
            }
            if(this.getBranchType2() == null || this.getBranchType2().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getBranchType2());
            }
            pstmt.setDouble(46, this.getAssets());
            pstmt.setDouble(47, this.getIncome());
            pstmt.setDouble(48, this.getProfits());
            pstmt.setInt(49, this.getPersonnalSum());
            if(this.getProtocalNo() == null || this.getProtocalNo().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getProtocalNo());
            }
            if(this.getHeadOffice() == null || this.getHeadOffice().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getHeadOffice());
            }
            if(this.getFoundDate() == null || this.getFoundDate().equals("null")) {
            	pstmt.setNull(52, 93);
            } else {
            	pstmt.setDate(52, Date.valueOf(this.getFoundDate()));
            }
            if(this.getEndDate() == null || this.getEndDate().equals("null")) {
            	pstmt.setNull(53, 93);
            } else {
            	pstmt.setDate(53, Date.valueOf(this.getEndDate()));
            }
            if(this.getBank() == null || this.getBank().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getBank());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getAccName());
            }
            if(this.getDraWer() == null || this.getDraWer().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getDraWer());
            }
            if(this.getDraWerAccNo() == null || this.getDraWerAccNo().equals("null")) {
            	pstmt.setNull(57, 12);
            } else {
            	pstmt.setString(57, this.getDraWerAccNo());
            }
            if(this.getRepManageCom() == null || this.getRepManageCom().equals("null")) {
            	pstmt.setNull(58, 12);
            } else {
            	pstmt.setString(58, this.getRepManageCom());
            }
            if(this.getDraWerAccCode() == null || this.getDraWerAccCode().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getDraWerAccCode());
            }
            if(this.getDraWerAccName() == null || this.getDraWerAccName().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getDraWerAccName());
            }
            if(this.getChannelType2() == null || this.getChannelType2().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getChannelType2());
            }
            if(this.getAreaType2() == null || this.getAreaType2().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getAreaType2());
            }
            if(this.getComToAgentflag() == null || this.getComToAgentflag().equals("null")) {
            	pstmt.setNull(63, 1);
            } else {
            	pstmt.setString(63, this.getComToAgentflag());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getAgentCode());
            }
            if(this.getAgentComCode() == null || this.getAgentComCode().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getAgentComCode());
            }
            if(this.getCooperateBank() == null || this.getCooperateBank().equals("null")) {
            	pstmt.setNull(66, 1);
            } else {
            	pstmt.setString(66, this.getCooperateBank());
            }
            if(this.getCooperationEndDate() == null || this.getCooperationEndDate().equals("null")) {
            	pstmt.setNull(67, 93);
            } else {
            	pstmt.setDate(67, Date.valueOf(this.getCooperationEndDate()));
            }
            if(this.getCooperationStartDate() == null || this.getCooperationStartDate().equals("null")) {
            	pstmt.setNull(68, 93);
            } else {
            	pstmt.setDate(68, Date.valueOf(this.getCooperationStartDate()));
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(69, 1);
            } else {
            	pstmt.setString(69, this.getAgentType());
            }
            if(this.getNewBankCode() == null || this.getNewBankCode().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getNewBankCode());
            }
            if(this.getBankCity() == null || this.getBankCity().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getBankCity());
            }
            // set where condition
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getAgentCom());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LACom");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LACom VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getAgentCom());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getManageCom());
            }
            if(this.getAreaType() == null || this.getAreaType().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getAreaType());
            }
            if(this.getChannelType() == null || this.getChannelType().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getChannelType());
            }
            if(this.getUpAgentCom() == null || this.getUpAgentCom().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getUpAgentCom());
            }
            if(this.getName() == null || this.getName().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getName());
            }
            if(this.getAddress() == null || this.getAddress().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getAddress());
            }
            if(this.getZipCode() == null || this.getZipCode().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getZipCode());
            }
            if(this.getPhone() == null || this.getPhone().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getPhone());
            }
            if(this.getFax() == null || this.getFax().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getFax());
            }
            if(this.getEMail() == null || this.getEMail().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getEMail());
            }
            if(this.getWebAddress() == null || this.getWebAddress().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getWebAddress());
            }
            if(this.getLinkMan() == null || this.getLinkMan().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getLinkMan());
            }
            if(this.getPassword() == null || this.getPassword().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getPassword());
            }
            if(this.getCorporation() == null || this.getCorporation().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getCorporation());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getBankAccNo());
            }
            if(this.getBusinessType() == null || this.getBusinessType().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getBusinessType());
            }
            if(this.getGrpNature() == null || this.getGrpNature().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getGrpNature());
            }
            if(this.getACType() == null || this.getACType().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getACType());
            }
            if(this.getSellFlag() == null || this.getSellFlag().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getSellFlag());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(23, 93);
            } else {
            	pstmt.setDate(23, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(25, 93);
            } else {
            	pstmt.setDate(25, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getModifyTime());
            }
            if(this.getBankType() == null || this.getBankType().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getBankType());
            }
            if(this.getCalFlag() == null || this.getCalFlag().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getCalFlag());
            }
            if(this.getBusiLicenseCode() == null || this.getBusiLicenseCode().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getBusiLicenseCode());
            }
            if(this.getInsureID() == null || this.getInsureID().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getInsureID());
            }
            if(this.getInsurePrincipal() == null || this.getInsurePrincipal().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getInsurePrincipal());
            }
            if(this.getChiefBusiness() == null || this.getChiefBusiness().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getChiefBusiness());
            }
            if(this.getBusiAddress() == null || this.getBusiAddress().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getBusiAddress());
            }
            if(this.getSubscribeMan() == null || this.getSubscribeMan().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getSubscribeMan());
            }
            if(this.getSubscribeManDuty() == null || this.getSubscribeManDuty().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getSubscribeManDuty());
            }
            if(this.getLicenseNo() == null || this.getLicenseNo().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getLicenseNo());
            }
            if(this.getRegionalismCode() == null || this.getRegionalismCode().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getRegionalismCode());
            }
            if(this.getAppAgentCom() == null || this.getAppAgentCom().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getAppAgentCom());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getState());
            }
            if(this.getNoti() == null || this.getNoti().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getNoti());
            }
            if(this.getBusinessCode() == null || this.getBusinessCode().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getBusinessCode());
            }
            if(this.getLicenseStartDate() == null || this.getLicenseStartDate().equals("null")) {
            	pstmt.setNull(42, 93);
            } else {
            	pstmt.setDate(42, Date.valueOf(this.getLicenseStartDate()));
            }
            if(this.getLicenseEndDate() == null || this.getLicenseEndDate().equals("null")) {
            	pstmt.setNull(43, 93);
            } else {
            	pstmt.setDate(43, Date.valueOf(this.getLicenseEndDate()));
            }
            if(this.getBranchType() == null || this.getBranchType().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getBranchType());
            }
            if(this.getBranchType2() == null || this.getBranchType2().equals("null")) {
            	pstmt.setNull(45, 12);
            } else {
            	pstmt.setString(45, this.getBranchType2());
            }
            pstmt.setDouble(46, this.getAssets());
            pstmt.setDouble(47, this.getIncome());
            pstmt.setDouble(48, this.getProfits());
            pstmt.setInt(49, this.getPersonnalSum());
            if(this.getProtocalNo() == null || this.getProtocalNo().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getProtocalNo());
            }
            if(this.getHeadOffice() == null || this.getHeadOffice().equals("null")) {
            	pstmt.setNull(51, 12);
            } else {
            	pstmt.setString(51, this.getHeadOffice());
            }
            if(this.getFoundDate() == null || this.getFoundDate().equals("null")) {
            	pstmt.setNull(52, 93);
            } else {
            	pstmt.setDate(52, Date.valueOf(this.getFoundDate()));
            }
            if(this.getEndDate() == null || this.getEndDate().equals("null")) {
            	pstmt.setNull(53, 93);
            } else {
            	pstmt.setDate(53, Date.valueOf(this.getEndDate()));
            }
            if(this.getBank() == null || this.getBank().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getBank());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getAccName());
            }
            if(this.getDraWer() == null || this.getDraWer().equals("null")) {
            	pstmt.setNull(56, 12);
            } else {
            	pstmt.setString(56, this.getDraWer());
            }
            if(this.getDraWerAccNo() == null || this.getDraWerAccNo().equals("null")) {
            	pstmt.setNull(57, 12);
            } else {
            	pstmt.setString(57, this.getDraWerAccNo());
            }
            if(this.getRepManageCom() == null || this.getRepManageCom().equals("null")) {
            	pstmt.setNull(58, 12);
            } else {
            	pstmt.setString(58, this.getRepManageCom());
            }
            if(this.getDraWerAccCode() == null || this.getDraWerAccCode().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getDraWerAccCode());
            }
            if(this.getDraWerAccName() == null || this.getDraWerAccName().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getDraWerAccName());
            }
            if(this.getChannelType2() == null || this.getChannelType2().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getChannelType2());
            }
            if(this.getAreaType2() == null || this.getAreaType2().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getAreaType2());
            }
            if(this.getComToAgentflag() == null || this.getComToAgentflag().equals("null")) {
            	pstmt.setNull(63, 1);
            } else {
            	pstmt.setString(63, this.getComToAgentflag());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getAgentCode());
            }
            if(this.getAgentComCode() == null || this.getAgentComCode().equals("null")) {
            	pstmt.setNull(65, 12);
            } else {
            	pstmt.setString(65, this.getAgentComCode());
            }
            if(this.getCooperateBank() == null || this.getCooperateBank().equals("null")) {
            	pstmt.setNull(66, 1);
            } else {
            	pstmt.setString(66, this.getCooperateBank());
            }
            if(this.getCooperationEndDate() == null || this.getCooperationEndDate().equals("null")) {
            	pstmt.setNull(67, 93);
            } else {
            	pstmt.setDate(67, Date.valueOf(this.getCooperationEndDate()));
            }
            if(this.getCooperationStartDate() == null || this.getCooperationStartDate().equals("null")) {
            	pstmt.setNull(68, 93);
            } else {
            	pstmt.setDate(68, Date.valueOf(this.getCooperationStartDate()));
            }
            if(this.getAgentType() == null || this.getAgentType().equals("null")) {
            	pstmt.setNull(69, 1);
            } else {
            	pstmt.setString(69, this.getAgentType());
            }
            if(this.getNewBankCode() == null || this.getNewBankCode().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getNewBankCode());
            }
            if(this.getBankCity() == null || this.getBankCity().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getBankCity());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LACom WHERE  1=1  AND AgentCom = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getAgentCom());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAComDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LAComSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LAComSet aLAComSet = new LAComSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LACom");
            LAComSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAComDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LAComSchema s1 = new LAComSchema();
                s1.setSchema(rs,i);
                aLAComSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLAComSet;
    }

    public LAComSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LAComSet aLAComSet = new LAComSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAComDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LAComSchema s1 = new LAComSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAComDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLAComSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLAComSet;
    }

    public LAComSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LAComSet aLAComSet = new LAComSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LACom");
            LAComSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LAComSchema s1 = new LAComSchema();
                s1.setSchema(rs,i);
                aLAComSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLAComSet;
    }

    public LAComSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LAComSet aLAComSet = new LAComSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LAComSchema s1 = new LAComSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAComDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLAComSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLAComSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LACom");
            LAComSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LACom " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAComDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LAComSet
     */
    public LAComSet getData() {
        int tCount = 0;
        LAComSet tLAComSet = new LAComSet();
        LAComSchema tLAComSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLAComSchema = new LAComSchema();
            tLAComSchema.setSchema(mResultSet, 1);
            tLAComSet.add(tLAComSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLAComSchema = new LAComSchema();
                    tLAComSchema.setSchema(mResultSet, 1);
                    tLAComSet.add(tLAComSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLAComSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LAComDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LAComDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LAComDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
