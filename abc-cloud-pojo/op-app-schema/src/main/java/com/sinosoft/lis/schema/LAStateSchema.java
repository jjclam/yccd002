/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LAStateDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LAStateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-27
 */
public class LAStateSchema implements Schema, Cloneable {
    // @Field
    /** Managecom */
    private String ManageCom;
    /** Branchtype */
    private String BranchType;
    /** Branchtype2 */
    private String BranchType2;
    /** Startdate */
    private String StartDate;
    /** Enddate */
    private String EndDate;
    /** Statetype */
    private String StateType;
    /** Statevalue */
    private String StateValue;
    /** Statevalue1 */
    private String StateValue1;
    /** Statevalue2 */
    private String StateValue2;
    /** Objecttype */
    private String ObjectType;
    /** Objectid */
    private String ObjectId;
    /** Objectlevel */
    private String ObjectLevel;
    /** Operator */
    private String Operator;
    /** Makedate */
    private Date MakeDate;
    /** Maketime */
    private String MakeTime;
    /** Modifydate */
    private Date ModifyDate;
    /** Modifytime */
    private String ModifyTime;
    /** Branchattr */
    private String BranchAttr;
    /** Gradedate */
    private Date GradeDate;

    public static final int FIELDNUM = 19;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LAStateSchema() {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "BranchType";
        pk[1] = "StartDate";
        pk[2] = "EndDate";
        pk[3] = "StateType";
        pk[4] = "ObjectId";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAStateSchema cloned = (LAStateSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getStateType() {
        return StateType;
    }
    public void setStateType(String aStateType) {
        StateType = aStateType;
    }
    public String getStateValue() {
        return StateValue;
    }
    public void setStateValue(String aStateValue) {
        StateValue = aStateValue;
    }
    public String getStateValue1() {
        return StateValue1;
    }
    public void setStateValue1(String aStateValue1) {
        StateValue1 = aStateValue1;
    }
    public String getStateValue2() {
        return StateValue2;
    }
    public void setStateValue2(String aStateValue2) {
        StateValue2 = aStateValue2;
    }
    public String getObjectType() {
        return ObjectType;
    }
    public void setObjectType(String aObjectType) {
        ObjectType = aObjectType;
    }
    public String getObjectId() {
        return ObjectId;
    }
    public void setObjectId(String aObjectId) {
        ObjectId = aObjectId;
    }
    public String getObjectLevel() {
        return ObjectLevel;
    }
    public void setObjectLevel(String aObjectLevel) {
        ObjectLevel = aObjectLevel;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBranchAttr() {
        return BranchAttr;
    }
    public void setBranchAttr(String aBranchAttr) {
        BranchAttr = aBranchAttr;
    }
    public String getGradeDate() {
        if(GradeDate != null) {
            return fDate.getString(GradeDate);
        } else {
            return null;
        }
    }
    public void setGradeDate(Date aGradeDate) {
        GradeDate = aGradeDate;
    }
    public void setGradeDate(String aGradeDate) {
        if (aGradeDate != null && !aGradeDate.equals("")) {
            GradeDate = fDate.getDate(aGradeDate);
        } else
            GradeDate = null;
    }


    /**
    * 使用另外一个 LAStateSchema 对象给 Schema 赋值
    * @param: aLAStateSchema LAStateSchema
    **/
    public void setSchema(LAStateSchema aLAStateSchema) {
        this.ManageCom = aLAStateSchema.getManageCom();
        this.BranchType = aLAStateSchema.getBranchType();
        this.BranchType2 = aLAStateSchema.getBranchType2();
        this.StartDate = aLAStateSchema.getStartDate();
        this.EndDate = aLAStateSchema.getEndDate();
        this.StateType = aLAStateSchema.getStateType();
        this.StateValue = aLAStateSchema.getStateValue();
        this.StateValue1 = aLAStateSchema.getStateValue1();
        this.StateValue2 = aLAStateSchema.getStateValue2();
        this.ObjectType = aLAStateSchema.getObjectType();
        this.ObjectId = aLAStateSchema.getObjectId();
        this.ObjectLevel = aLAStateSchema.getObjectLevel();
        this.Operator = aLAStateSchema.getOperator();
        this.MakeDate = fDate.getDate( aLAStateSchema.getMakeDate());
        this.MakeTime = aLAStateSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLAStateSchema.getModifyDate());
        this.ModifyTime = aLAStateSchema.getModifyTime();
        this.BranchAttr = aLAStateSchema.getBranchAttr();
        this.GradeDate = fDate.getDate( aLAStateSchema.getGradeDate());
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("BranchType") == null )
                this.BranchType = null;
            else
                this.BranchType = rs.getString("BranchType").trim();

            if( rs.getString("BranchType2") == null )
                this.BranchType2 = null;
            else
                this.BranchType2 = rs.getString("BranchType2").trim();

            if( rs.getString("StartDate") == null )
                this.StartDate = null;
            else
                this.StartDate = rs.getString("StartDate").trim();

            if( rs.getString("EndDate") == null )
                this.EndDate = null;
            else
                this.EndDate = rs.getString("EndDate").trim();

            if( rs.getString("StateType") == null )
                this.StateType = null;
            else
                this.StateType = rs.getString("StateType").trim();

            if( rs.getString("StateValue") == null )
                this.StateValue = null;
            else
                this.StateValue = rs.getString("StateValue").trim();

            if( rs.getString("StateValue1") == null )
                this.StateValue1 = null;
            else
                this.StateValue1 = rs.getString("StateValue1").trim();

            if( rs.getString("StateValue2") == null )
                this.StateValue2 = null;
            else
                this.StateValue2 = rs.getString("StateValue2").trim();

            if( rs.getString("ObjectType") == null )
                this.ObjectType = null;
            else
                this.ObjectType = rs.getString("ObjectType").trim();

            if( rs.getString("ObjectId") == null )
                this.ObjectId = null;
            else
                this.ObjectId = rs.getString("ObjectId").trim();

            if( rs.getString("ObjectLevel") == null )
                this.ObjectLevel = null;
            else
                this.ObjectLevel = rs.getString("ObjectLevel").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("BranchAttr") == null )
                this.BranchAttr = null;
            else
                this.BranchAttr = rs.getString("BranchAttr").trim();

            this.GradeDate = rs.getDate("GradeDate");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAStateSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LAStateSchema getSchema() {
        LAStateSchema aLAStateSchema = new LAStateSchema();
        aLAStateSchema.setSchema(this);
        return aLAStateSchema;
    }

    public LAStateDB getDB() {
        LAStateDB aDBOper = new LAStateDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAState描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StartDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EndDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateValue)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateValue1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateValue2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ObjectType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ObjectId)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ObjectLevel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchAttr)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GradeDate )));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAState>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            StartDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            EndDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            StateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            StateValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            StateValue1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            StateValue2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ObjectType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            ObjectId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            ObjectLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            GradeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAStateSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateType));
        }
        if (FCode.equalsIgnoreCase("StateValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateValue));
        }
        if (FCode.equalsIgnoreCase("StateValue1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateValue1));
        }
        if (FCode.equalsIgnoreCase("StateValue2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateValue2));
        }
        if (FCode.equalsIgnoreCase("ObjectType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectType));
        }
        if (FCode.equalsIgnoreCase("ObjectId")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectId));
        }
        if (FCode.equalsIgnoreCase("ObjectLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ObjectLevel));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equalsIgnoreCase("GradeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGradeDate()));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(StartDate);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(EndDate);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(StateType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(StateValue);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(StateValue1);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(StateValue2);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ObjectType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ObjectId);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ObjectLevel);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(BranchAttr);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGradeDate()));
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateType = FValue.trim();
            }
            else
                StateType = null;
        }
        if (FCode.equalsIgnoreCase("StateValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateValue = FValue.trim();
            }
            else
                StateValue = null;
        }
        if (FCode.equalsIgnoreCase("StateValue1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateValue1 = FValue.trim();
            }
            else
                StateValue1 = null;
        }
        if (FCode.equalsIgnoreCase("StateValue2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateValue2 = FValue.trim();
            }
            else
                StateValue2 = null;
        }
        if (FCode.equalsIgnoreCase("ObjectType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ObjectType = FValue.trim();
            }
            else
                ObjectType = null;
        }
        if (FCode.equalsIgnoreCase("ObjectId")) {
            if( FValue != null && !FValue.equals(""))
            {
                ObjectId = FValue.trim();
            }
            else
                ObjectId = null;
        }
        if (FCode.equalsIgnoreCase("ObjectLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                ObjectLevel = FValue.trim();
            }
            else
                ObjectLevel = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
                BranchAttr = null;
        }
        if (FCode.equalsIgnoreCase("GradeDate")) {
            if(FValue != null && !FValue.equals("")) {
                GradeDate = fDate.getDate( FValue );
            }
            else
                GradeDate = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LAStateSchema other = (LAStateSchema)otherObject;
        return
            ManageCom.equals(other.getManageCom())
            && BranchType.equals(other.getBranchType())
            && BranchType2.equals(other.getBranchType2())
            && StartDate.equals(other.getStartDate())
            && EndDate.equals(other.getEndDate())
            && StateType.equals(other.getStateType())
            && StateValue.equals(other.getStateValue())
            && StateValue1.equals(other.getStateValue1())
            && StateValue2.equals(other.getStateValue2())
            && ObjectType.equals(other.getObjectType())
            && ObjectId.equals(other.getObjectId())
            && ObjectLevel.equals(other.getObjectLevel())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && BranchAttr.equals(other.getBranchAttr())
            && fDate.getString(GradeDate).equals(other.getGradeDate());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ManageCom") ) {
            return 0;
        }
        if( strFieldName.equals("BranchType") ) {
            return 1;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 2;
        }
        if( strFieldName.equals("StartDate") ) {
            return 3;
        }
        if( strFieldName.equals("EndDate") ) {
            return 4;
        }
        if( strFieldName.equals("StateType") ) {
            return 5;
        }
        if( strFieldName.equals("StateValue") ) {
            return 6;
        }
        if( strFieldName.equals("StateValue1") ) {
            return 7;
        }
        if( strFieldName.equals("StateValue2") ) {
            return 8;
        }
        if( strFieldName.equals("ObjectType") ) {
            return 9;
        }
        if( strFieldName.equals("ObjectId") ) {
            return 10;
        }
        if( strFieldName.equals("ObjectLevel") ) {
            return 11;
        }
        if( strFieldName.equals("Operator") ) {
            return 12;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 13;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 16;
        }
        if( strFieldName.equals("BranchAttr") ) {
            return 17;
        }
        if( strFieldName.equals("GradeDate") ) {
            return 18;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "BranchType2";
                break;
            case 3:
                strFieldName = "StartDate";
                break;
            case 4:
                strFieldName = "EndDate";
                break;
            case 5:
                strFieldName = "StateType";
                break;
            case 6:
                strFieldName = "StateValue";
                break;
            case 7:
                strFieldName = "StateValue1";
                break;
            case 8:
                strFieldName = "StateValue2";
                break;
            case 9:
                strFieldName = "ObjectType";
                break;
            case 10:
                strFieldName = "ObjectId";
                break;
            case 11:
                strFieldName = "ObjectLevel";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "BranchAttr";
                break;
            case 18:
                strFieldName = "GradeDate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "STATETYPE":
                return Schema.TYPE_STRING;
            case "STATEVALUE":
                return Schema.TYPE_STRING;
            case "STATEVALUE1":
                return Schema.TYPE_STRING;
            case "STATEVALUE2":
                return Schema.TYPE_STRING;
            case "OBJECTTYPE":
                return Schema.TYPE_STRING;
            case "OBJECTID":
                return Schema.TYPE_STRING;
            case "OBJECTLEVEL":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BRANCHATTR":
                return Schema.TYPE_STRING;
            case "GRADEDATE":
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
