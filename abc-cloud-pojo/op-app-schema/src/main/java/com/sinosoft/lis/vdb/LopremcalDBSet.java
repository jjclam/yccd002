/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LopremcalSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LopremcalDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-12-18
 */
public class LopremcalDBSet extends LopremcalSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LopremcalDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"Lopremcal");
        mflag = true;
    }

    public LopremcalDBSet() {
        db = new DBOper( "Lopremcal" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LopremcalDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM Lopremcal WHERE  1=1  AND RiskCode = ? AND ManageCom = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getManageCom());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LopremcalDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE Lopremcal SET  RiskCode = ? , ManageCom = ? , SaleChnl = ? , SumPrem = ? , TotalPrem = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? WHERE  1=1  AND RiskCode = ? AND ManageCom = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getManageCom());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getSaleChnl());
            }
            pstmt.setDouble(4, this.get(i).getSumPrem());
            pstmt.setDouble(5, this.get(i).getTotalPrem());
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(9,null);
            } else {
                pstmt.setDate(9, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getModifyTime());
            }
            // set where condition
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getManageCom());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LopremcalDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO Lopremcal VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getManageCom());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getSaleChnl());
            }
            pstmt.setDouble(4, this.get(i).getSumPrem());
            pstmt.setDouble(5, this.get(i).getTotalPrem());
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(9,null);
            } else {
                pstmt.setDate(9, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getModifyTime());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LopremcalDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
