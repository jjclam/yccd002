/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LMRiskInsuAccSchema;
import com.sinosoft.lis.vschema.LMRiskInsuAccSet;
import com.sinosoft.utility.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * <p>ClassName: LMRiskInsuAccDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMRiskInsuAccDB extends LMRiskInsuAccSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LMRiskInsuAccDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LMRiskInsuAcc" );
        mflag = true;
    }

    public LMRiskInsuAccDB() {
        con = null;
        db = new DBOper( "LMRiskInsuAcc" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LMRiskInsuAccSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LMRiskInsuAccSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LMRiskInsuAcc WHERE  1=1  AND InsuAccNo = ?");
            if(this.getInsuAccNo() == null || this.getInsuAccNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getInsuAccNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LMRiskInsuAcc");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LMRiskInsuAcc SET  InsuAccNo = ? , AccType = ? , AccKind = ? , InsuAccName = ? , AccCreatePos = ? , AccCreateType = ? , AccRate = ? , AccRateTable = ? , AccCancelCode = ? , AccComputeFlag = ? , InvestType = ? , FundCompanyCode = ? , Owner = ? , AccBonusMode = ? , BonusToInsuAccNo = ? , InsuAccCalBalaFlag = ? , BonusMode = ? , InvestFlag = ? , CalValueFreq = ? , CalValueRule = ? , UnitDecimal = ? , RoundMethod = ? , AccBonusFlag = ? , BonusFlag = ? WHERE  1=1  AND InsuAccNo = ?");
            if(this.getInsuAccNo() == null || this.getInsuAccNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getInsuAccNo());
            }
            if(this.getAccType() == null || this.getAccType().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getAccType());
            }
            if(this.getAccKind() == null || this.getAccKind().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getAccKind());
            }
            if(this.getInsuAccName() == null || this.getInsuAccName().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getInsuAccName());
            }
            if(this.getAccCreatePos() == null || this.getAccCreatePos().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAccCreatePos());
            }
            if(this.getAccCreateType() == null || this.getAccCreateType().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getAccCreateType());
            }
            pstmt.setDouble(7, this.getAccRate());
            if(this.getAccRateTable() == null || this.getAccRateTable().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAccRateTable());
            }
            if(this.getAccCancelCode() == null || this.getAccCancelCode().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAccCancelCode());
            }
            if(this.getAccComputeFlag() == null || this.getAccComputeFlag().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAccComputeFlag());
            }
            if(this.getInvestType() == null || this.getInvestType().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getInvestType());
            }
            if(this.getFundCompanyCode() == null || this.getFundCompanyCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getFundCompanyCode());
            }
            if(this.getOwner() == null || this.getOwner().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getOwner());
            }
            if(this.getAccBonusMode() == null || this.getAccBonusMode().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAccBonusMode());
            }
            if(this.getBonusToInsuAccNo() == null || this.getBonusToInsuAccNo().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getBonusToInsuAccNo());
            }
            if(this.getInsuAccCalBalaFlag() == null || this.getInsuAccCalBalaFlag().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getInsuAccCalBalaFlag());
            }
            if(this.getBonusMode() == null || this.getBonusMode().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getBonusMode());
            }
            if(this.getInvestFlag() == null || this.getInvestFlag().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getInvestFlag());
            }
            if(this.getCalValueFreq() == null || this.getCalValueFreq().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getCalValueFreq());
            }
            if(this.getCalValueRule() == null || this.getCalValueRule().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getCalValueRule());
            }
            if(this.getUnitDecimal() == null || this.getUnitDecimal().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getUnitDecimal());
            }
            if(this.getRoundMethod() == null || this.getRoundMethod().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getRoundMethod());
            }
            if(this.getAccBonusFlag() == null || this.getAccBonusFlag().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getAccBonusFlag());
            }
            if(this.getBonusFlag() == null || this.getBonusFlag().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getBonusFlag());
            }
            // set where condition
            if(this.getInsuAccNo() == null || this.getInsuAccNo().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getInsuAccNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LMRiskInsuAcc");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LMRiskInsuAcc VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getInsuAccNo() == null || this.getInsuAccNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getInsuAccNo());
            }
            if(this.getAccType() == null || this.getAccType().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getAccType());
            }
            if(this.getAccKind() == null || this.getAccKind().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getAccKind());
            }
            if(this.getInsuAccName() == null || this.getInsuAccName().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getInsuAccName());
            }
            if(this.getAccCreatePos() == null || this.getAccCreatePos().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAccCreatePos());
            }
            if(this.getAccCreateType() == null || this.getAccCreateType().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getAccCreateType());
            }
            pstmt.setDouble(7, this.getAccRate());
            if(this.getAccRateTable() == null || this.getAccRateTable().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAccRateTable());
            }
            if(this.getAccCancelCode() == null || this.getAccCancelCode().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAccCancelCode());
            }
            if(this.getAccComputeFlag() == null || this.getAccComputeFlag().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAccComputeFlag());
            }
            if(this.getInvestType() == null || this.getInvestType().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getInvestType());
            }
            if(this.getFundCompanyCode() == null || this.getFundCompanyCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getFundCompanyCode());
            }
            if(this.getOwner() == null || this.getOwner().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getOwner());
            }
            if(this.getAccBonusMode() == null || this.getAccBonusMode().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getAccBonusMode());
            }
            if(this.getBonusToInsuAccNo() == null || this.getBonusToInsuAccNo().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getBonusToInsuAccNo());
            }
            if(this.getInsuAccCalBalaFlag() == null || this.getInsuAccCalBalaFlag().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getInsuAccCalBalaFlag());
            }
            if(this.getBonusMode() == null || this.getBonusMode().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getBonusMode());
            }
            if(this.getInvestFlag() == null || this.getInvestFlag().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getInvestFlag());
            }
            if(this.getCalValueFreq() == null || this.getCalValueFreq().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getCalValueFreq());
            }
            if(this.getCalValueRule() == null || this.getCalValueRule().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getCalValueRule());
            }
            if(this.getUnitDecimal() == null || this.getUnitDecimal().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getUnitDecimal());
            }
            if(this.getRoundMethod() == null || this.getRoundMethod().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getRoundMethod());
            }
            if(this.getAccBonusFlag() == null || this.getAccBonusFlag().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getAccBonusFlag());
            }
            if(this.getBonusFlag() == null || this.getBonusFlag().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getBonusFlag());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LMRiskInsuAcc WHERE  1=1  AND InsuAccNo = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getInsuAccNo() == null || this.getInsuAccNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getInsuAccNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskInsuAccDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LMRiskInsuAccSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LMRiskInsuAccSet aLMRiskInsuAccSet = new LMRiskInsuAccSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LMRiskInsuAcc");
            LMRiskInsuAccSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskInsuAccDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LMRiskInsuAccSchema s1 = new LMRiskInsuAccSchema();
                s1.setSchema(rs,i);
                aLMRiskInsuAccSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLMRiskInsuAccSet;
    }

    public LMRiskInsuAccSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LMRiskInsuAccSet aLMRiskInsuAccSet = new LMRiskInsuAccSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskInsuAccDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LMRiskInsuAccSchema s1 = new LMRiskInsuAccSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskInsuAccDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLMRiskInsuAccSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLMRiskInsuAccSet;
    }

    public LMRiskInsuAccSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LMRiskInsuAccSet aLMRiskInsuAccSet = new LMRiskInsuAccSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LMRiskInsuAcc");
            LMRiskInsuAccSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LMRiskInsuAccSchema s1 = new LMRiskInsuAccSchema();
                s1.setSchema(rs,i);
                aLMRiskInsuAccSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLMRiskInsuAccSet;
    }

    public LMRiskInsuAccSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LMRiskInsuAccSet aLMRiskInsuAccSet = new LMRiskInsuAccSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LMRiskInsuAccSchema s1 = new LMRiskInsuAccSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskInsuAccDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLMRiskInsuAccSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLMRiskInsuAccSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LMRiskInsuAcc");
            LMRiskInsuAccSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LMRiskInsuAcc " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LMRiskInsuAccDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LMRiskInsuAccSet
     */
    public LMRiskInsuAccSet getData() {
        int tCount = 0;
        LMRiskInsuAccSet tLMRiskInsuAccSet = new LMRiskInsuAccSet();
        LMRiskInsuAccSchema tLMRiskInsuAccSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
            tLMRiskInsuAccSchema.setSchema(mResultSet, 1);
            tLMRiskInsuAccSet.add(tLMRiskInsuAccSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
                    tLMRiskInsuAccSchema.setSchema(mResultSet, 1);
                    tLMRiskInsuAccSet.add(tLMRiskInsuAccSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLMRiskInsuAccSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LMRiskInsuAccDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LMRiskInsuAccDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
