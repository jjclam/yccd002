/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LKTransAuthorizationDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LKTransAuthorizationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LKTransAuthorizationSchema implements Schema, Cloneable {
    // @Field
    /** 管理机构 */
    private String ManageCom;
    /** 银行编码 */
    private String BankCode;
    /** 地区编码 */
    private String BankBranch;
    /** 网点编码 */
    private String BankNode;
    /** 交易码 */
    private String FuncFlag;
    /** 备用 */
    private String StandByFlag;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 操作员号 */
    private String Operator;
    /** Issue_way */
    private String ISSUE_WAY;
    /** State_id */
    private String STATE_ID;

    public static final int FIELDNUM = 13;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LKTransAuthorizationSchema() {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "ManageCom";
        pk[1] = "BankCode";
        pk[2] = "BankBranch";
        pk[3] = "BankNode";
        pk[4] = "FuncFlag";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LKTransAuthorizationSchema cloned = (LKTransAuthorizationSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankBranch() {
        return BankBranch;
    }
    public void setBankBranch(String aBankBranch) {
        BankBranch = aBankBranch;
    }
    public String getBankNode() {
        return BankNode;
    }
    public void setBankNode(String aBankNode) {
        BankNode = aBankNode;
    }
    public String getFuncFlag() {
        return FuncFlag;
    }
    public void setFuncFlag(String aFuncFlag) {
        FuncFlag = aFuncFlag;
    }
    public String getStandByFlag() {
        return StandByFlag;
    }
    public void setStandByFlag(String aStandByFlag) {
        StandByFlag = aStandByFlag;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getISSUE_WAY() {
        return ISSUE_WAY;
    }
    public void setISSUE_WAY(String aISSUE_WAY) {
        ISSUE_WAY = aISSUE_WAY;
    }
    public String getSTATE_ID() {
        return STATE_ID;
    }
    public void setSTATE_ID(String aSTATE_ID) {
        STATE_ID = aSTATE_ID;
    }

    /**
    * 使用另外一个 LKTransAuthorizationSchema 对象给 Schema 赋值
    * @param: aLKTransAuthorizationSchema LKTransAuthorizationSchema
    **/
    public void setSchema(LKTransAuthorizationSchema aLKTransAuthorizationSchema) {
        this.ManageCom = aLKTransAuthorizationSchema.getManageCom();
        this.BankCode = aLKTransAuthorizationSchema.getBankCode();
        this.BankBranch = aLKTransAuthorizationSchema.getBankBranch();
        this.BankNode = aLKTransAuthorizationSchema.getBankNode();
        this.FuncFlag = aLKTransAuthorizationSchema.getFuncFlag();
        this.StandByFlag = aLKTransAuthorizationSchema.getStandByFlag();
        this.MakeDate = fDate.getDate( aLKTransAuthorizationSchema.getMakeDate());
        this.MakeTime = aLKTransAuthorizationSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLKTransAuthorizationSchema.getModifyDate());
        this.ModifyTime = aLKTransAuthorizationSchema.getModifyTime();
        this.Operator = aLKTransAuthorizationSchema.getOperator();
        this.ISSUE_WAY = aLKTransAuthorizationSchema.getISSUE_WAY();
        this.STATE_ID = aLKTransAuthorizationSchema.getSTATE_ID();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankBranch") == null )
                this.BankBranch = null;
            else
                this.BankBranch = rs.getString("BankBranch").trim();

            if( rs.getString("BankNode") == null )
                this.BankNode = null;
            else
                this.BankNode = rs.getString("BankNode").trim();

            if( rs.getString("FuncFlag") == null )
                this.FuncFlag = null;
            else
                this.FuncFlag = rs.getString("FuncFlag").trim();

            if( rs.getString("StandByFlag") == null )
                this.StandByFlag = null;
            else
                this.StandByFlag = rs.getString("StandByFlag").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            if( rs.getString("ISSUE_WAY") == null )
                this.ISSUE_WAY = null;
            else
                this.ISSUE_WAY = rs.getString("ISSUE_WAY").trim();

            if( rs.getString("STATE_ID") == null )
                this.STATE_ID = null;
            else
                this.STATE_ID = rs.getString("STATE_ID").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTransAuthorizationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LKTransAuthorizationSchema getSchema() {
        LKTransAuthorizationSchema aLKTransAuthorizationSchema = new LKTransAuthorizationSchema();
        aLKTransAuthorizationSchema.setSchema(this);
        return aLKTransAuthorizationSchema;
    }

    public LKTransAuthorizationDB getDB() {
        LKTransAuthorizationDB aDBOper = new LKTransAuthorizationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransAuthorization描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankBranch)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FuncFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ISSUE_WAY)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(STATE_ID));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransAuthorization>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            BankBranch = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            FuncFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            StandByFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            ISSUE_WAY = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            STATE_ID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKTransAuthorizationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankBranch));
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FuncFlag));
        }
        if (FCode.equalsIgnoreCase("StandByFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("ISSUE_WAY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ISSUE_WAY));
        }
        if (FCode.equalsIgnoreCase("STATE_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STATE_ID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BankBranch);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BankNode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(FuncFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ISSUE_WAY);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(STATE_ID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankBranch")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankBranch = FValue.trim();
            }
            else
                BankBranch = null;
        }
        if (FCode.equalsIgnoreCase("BankNode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankNode = FValue.trim();
            }
            else
                BankNode = null;
        }
        if (FCode.equalsIgnoreCase("FuncFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                FuncFlag = FValue.trim();
            }
            else
                FuncFlag = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag = FValue.trim();
            }
            else
                StandByFlag = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("ISSUE_WAY")) {
            if( FValue != null && !FValue.equals(""))
            {
                ISSUE_WAY = FValue.trim();
            }
            else
                ISSUE_WAY = null;
        }
        if (FCode.equalsIgnoreCase("STATE_ID")) {
            if( FValue != null && !FValue.equals(""))
            {
                STATE_ID = FValue.trim();
            }
            else
                STATE_ID = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LKTransAuthorizationSchema other = (LKTransAuthorizationSchema)otherObject;
        return
            ManageCom.equals(other.getManageCom())
            && BankCode.equals(other.getBankCode())
            && BankBranch.equals(other.getBankBranch())
            && BankNode.equals(other.getBankNode())
            && FuncFlag.equals(other.getFuncFlag())
            && StandByFlag.equals(other.getStandByFlag())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Operator.equals(other.getOperator())
            && ISSUE_WAY.equals(other.getISSUE_WAY())
            && STATE_ID.equals(other.getSTATE_ID());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ManageCom") ) {
            return 0;
        }
        if( strFieldName.equals("BankCode") ) {
            return 1;
        }
        if( strFieldName.equals("BankBranch") ) {
            return 2;
        }
        if( strFieldName.equals("BankNode") ) {
            return 3;
        }
        if( strFieldName.equals("FuncFlag") ) {
            return 4;
        }
        if( strFieldName.equals("StandByFlag") ) {
            return 5;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 6;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 7;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 8;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 9;
        }
        if( strFieldName.equals("Operator") ) {
            return 10;
        }
        if( strFieldName.equals("ISSUE_WAY") ) {
            return 11;
        }
        if( strFieldName.equals("STATE_ID") ) {
            return 12;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "BankCode";
                break;
            case 2:
                strFieldName = "BankBranch";
                break;
            case 3:
                strFieldName = "BankNode";
                break;
            case 4:
                strFieldName = "FuncFlag";
                break;
            case 5:
                strFieldName = "StandByFlag";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "ISSUE_WAY";
                break;
            case 12:
                strFieldName = "STATE_ID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKBRANCH":
                return Schema.TYPE_STRING;
            case "BANKNODE":
                return Schema.TYPE_STRING;
            case "FUNCFLAG":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "ISSUE_WAY":
                return Schema.TYPE_STRING;
            case "STATE_ID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
