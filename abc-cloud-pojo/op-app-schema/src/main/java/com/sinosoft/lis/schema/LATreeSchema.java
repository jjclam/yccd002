/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATreeDB;

/**
 * <p>ClassName: LATreeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-02
 */
public class LATreeSchema implements Schema, Cloneable {
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 管理机构 */
    private String ManageCom;
    /** 代理人系列 */
    private String AgentSeries;
    /** 代理人级别 */
    private String AgentGrade;
    /** 代理人上次系列 */
    private String AgentLastSeries;
    /** 代理人上次级别 */
    private String AgentLastGrade;
    /** 增员代理人 */
    private String IntroAgency;
    /** 上级代理人 */
    private String UpAgent;
    /** 代理人类别 */
    private String OthUpAgent;
    /** 增员链断裂标记 */
    private String IntroBreakFlag;
    /** 增员抽佣起期 */
    private Date IntroCommStart;
    /** 增员抽佣止期 */
    private Date IntroCommEnd;
    /** 育成主管代理人 */
    private String EduManager;
    /** 育成链断裂标记 */
    private String RearBreakFlag;
    /** 育成抽佣起期 */
    private Date RearCommStart;
    /** 育成抽佣止期 */
    private Date RearCommEnd;
    /** 归属顺序 */
    private String AscriptSeries;
    /** 前职级起聘日期 */
    private Date OldStartDate;
    /** 前职级解聘日期 */
    private Date OldEndDate;
    /** 现职级起聘日期 */
    private Date StartDate;
    /** 考核开始日期 */
    private Date AstartDate;
    /** 考核类型 */
    private String AssessType;
    /** 考核状态 */
    private String State;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 员工属性/续收员薪资计算难度系数 */
    private String BranchCode;
    /** 上次外部显示职级 */
    private String LastAgentGrade1;
    /** 外部显示职级 */
    private String AgentGrade1;
    /** 入司职级 */
    private String EmployGrade;
    /** 黑名单标记 */
    private String BlackLisFlag;
    /** 原因类别 */
    private String ReasonType;
    /** 原因 */
    private String Reason;
    /** 代理人核保类别 */
    private String UWClass;
    /** 代理人差异化级别 */
    private String UWLevel;
    /** 差异化维护时间 */
    private String UWModifyTime;
    /** 差异化维护日期 */
    private Date UWModifyDate;
    /** 衔接人员状态标志 */
    private String ConnManagerState;
    /** 兼专职收费员标志 */
    private String TollFlag;
    /** 展业类型 */
    private String BranchType;
    /** 展业子渠道 */
    private String BranchType2;
    /** 代理人类别1 */
    private String AgentKind;
    /** 收费员难度系数 */
    private double Difficulty;
    /** 当前职级起聘原因 */
    private String AgentGradeRsn;
    /** 定级完成日期 */
    private Date ConnSuccDate;
    /** 内外勤标记 */
    private String InsideFlag;
    /** 代理人发展路线 */
    private String AgentLine;
    /** 同业衔接人员标记 */
    private String isConnMan;
    /** 初始职级 */
    private String InitGrade;
    /** 特殊人员标记 */
    private String SpeciFlag;
    /** 特殊人员起期 */
    private Date SpeciStartDate;
    /** 特殊人员止期 */
    private Date SpeciEndDate;
    /** 代理人vip属性 */
    private String VIPProperty;
    /** 特殊编码 */
    private String SpeciCode;

    public static final int FIELDNUM = 57;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LATreeSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AgentCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LATreeSchema cloned = (LATreeSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentSeries() {
        return AgentSeries;
    }
    public void setAgentSeries(String aAgentSeries) {
        AgentSeries = aAgentSeries;
    }
    public String getAgentGrade() {
        return AgentGrade;
    }
    public void setAgentGrade(String aAgentGrade) {
        AgentGrade = aAgentGrade;
    }
    public String getAgentLastSeries() {
        return AgentLastSeries;
    }
    public void setAgentLastSeries(String aAgentLastSeries) {
        AgentLastSeries = aAgentLastSeries;
    }
    public String getAgentLastGrade() {
        return AgentLastGrade;
    }
    public void setAgentLastGrade(String aAgentLastGrade) {
        AgentLastGrade = aAgentLastGrade;
    }
    public String getIntroAgency() {
        return IntroAgency;
    }
    public void setIntroAgency(String aIntroAgency) {
        IntroAgency = aIntroAgency;
    }
    public String getUpAgent() {
        return UpAgent;
    }
    public void setUpAgent(String aUpAgent) {
        UpAgent = aUpAgent;
    }
    public String getOthUpAgent() {
        return OthUpAgent;
    }
    public void setOthUpAgent(String aOthUpAgent) {
        OthUpAgent = aOthUpAgent;
    }
    public String getIntroBreakFlag() {
        return IntroBreakFlag;
    }
    public void setIntroBreakFlag(String aIntroBreakFlag) {
        IntroBreakFlag = aIntroBreakFlag;
    }
    public String getIntroCommStart() {
        if(IntroCommStart != null) {
            return fDate.getString(IntroCommStart);
        } else {
            return null;
        }
    }
    public void setIntroCommStart(Date aIntroCommStart) {
        IntroCommStart = aIntroCommStart;
    }
    public void setIntroCommStart(String aIntroCommStart) {
        if (aIntroCommStart != null && !aIntroCommStart.equals("")) {
            IntroCommStart = fDate.getDate(aIntroCommStart);
        } else
            IntroCommStart = null;
    }

    public String getIntroCommEnd() {
        if(IntroCommEnd != null) {
            return fDate.getString(IntroCommEnd);
        } else {
            return null;
        }
    }
    public void setIntroCommEnd(Date aIntroCommEnd) {
        IntroCommEnd = aIntroCommEnd;
    }
    public void setIntroCommEnd(String aIntroCommEnd) {
        if (aIntroCommEnd != null && !aIntroCommEnd.equals("")) {
            IntroCommEnd = fDate.getDate(aIntroCommEnd);
        } else
            IntroCommEnd = null;
    }

    public String getEduManager() {
        return EduManager;
    }
    public void setEduManager(String aEduManager) {
        EduManager = aEduManager;
    }
    public String getRearBreakFlag() {
        return RearBreakFlag;
    }
    public void setRearBreakFlag(String aRearBreakFlag) {
        RearBreakFlag = aRearBreakFlag;
    }
    public String getRearCommStart() {
        if(RearCommStart != null) {
            return fDate.getString(RearCommStart);
        } else {
            return null;
        }
    }
    public void setRearCommStart(Date aRearCommStart) {
        RearCommStart = aRearCommStart;
    }
    public void setRearCommStart(String aRearCommStart) {
        if (aRearCommStart != null && !aRearCommStart.equals("")) {
            RearCommStart = fDate.getDate(aRearCommStart);
        } else
            RearCommStart = null;
    }

    public String getRearCommEnd() {
        if(RearCommEnd != null) {
            return fDate.getString(RearCommEnd);
        } else {
            return null;
        }
    }
    public void setRearCommEnd(Date aRearCommEnd) {
        RearCommEnd = aRearCommEnd;
    }
    public void setRearCommEnd(String aRearCommEnd) {
        if (aRearCommEnd != null && !aRearCommEnd.equals("")) {
            RearCommEnd = fDate.getDate(aRearCommEnd);
        } else
            RearCommEnd = null;
    }

    public String getAscriptSeries() {
        return AscriptSeries;
    }
    public void setAscriptSeries(String aAscriptSeries) {
        AscriptSeries = aAscriptSeries;
    }
    public String getOldStartDate() {
        if(OldStartDate != null) {
            return fDate.getString(OldStartDate);
        } else {
            return null;
        }
    }
    public void setOldStartDate(Date aOldStartDate) {
        OldStartDate = aOldStartDate;
    }
    public void setOldStartDate(String aOldStartDate) {
        if (aOldStartDate != null && !aOldStartDate.equals("")) {
            OldStartDate = fDate.getDate(aOldStartDate);
        } else
            OldStartDate = null;
    }

    public String getOldEndDate() {
        if(OldEndDate != null) {
            return fDate.getString(OldEndDate);
        } else {
            return null;
        }
    }
    public void setOldEndDate(Date aOldEndDate) {
        OldEndDate = aOldEndDate;
    }
    public void setOldEndDate(String aOldEndDate) {
        if (aOldEndDate != null && !aOldEndDate.equals("")) {
            OldEndDate = fDate.getDate(aOldEndDate);
        } else
            OldEndDate = null;
    }

    public String getStartDate() {
        if(StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }
    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }
    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else
            StartDate = null;
    }

    public String getAstartDate() {
        if(AstartDate != null) {
            return fDate.getString(AstartDate);
        } else {
            return null;
        }
    }
    public void setAstartDate(Date aAstartDate) {
        AstartDate = aAstartDate;
    }
    public void setAstartDate(String aAstartDate) {
        if (aAstartDate != null && !aAstartDate.equals("")) {
            AstartDate = fDate.getDate(aAstartDate);
        } else
            AstartDate = null;
    }

    public String getAssessType() {
        return AssessType;
    }
    public void setAssessType(String aAssessType) {
        AssessType = aAssessType;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getBranchCode() {
        return BranchCode;
    }
    public void setBranchCode(String aBranchCode) {
        BranchCode = aBranchCode;
    }
    public String getLastAgentGrade1() {
        return LastAgentGrade1;
    }
    public void setLastAgentGrade1(String aLastAgentGrade1) {
        LastAgentGrade1 = aLastAgentGrade1;
    }
    public String getAgentGrade1() {
        return AgentGrade1;
    }
    public void setAgentGrade1(String aAgentGrade1) {
        AgentGrade1 = aAgentGrade1;
    }
    public String getEmployGrade() {
        return EmployGrade;
    }
    public void setEmployGrade(String aEmployGrade) {
        EmployGrade = aEmployGrade;
    }
    public String getBlackLisFlag() {
        return BlackLisFlag;
    }
    public void setBlackLisFlag(String aBlackLisFlag) {
        BlackLisFlag = aBlackLisFlag;
    }
    public String getReasonType() {
        return ReasonType;
    }
    public void setReasonType(String aReasonType) {
        ReasonType = aReasonType;
    }
    public String getReason() {
        return Reason;
    }
    public void setReason(String aReason) {
        Reason = aReason;
    }
    public String getUWClass() {
        return UWClass;
    }
    public void setUWClass(String aUWClass) {
        UWClass = aUWClass;
    }
    public String getUWLevel() {
        return UWLevel;
    }
    public void setUWLevel(String aUWLevel) {
        UWLevel = aUWLevel;
    }
    public String getUWModifyTime() {
        return UWModifyTime;
    }
    public void setUWModifyTime(String aUWModifyTime) {
        UWModifyTime = aUWModifyTime;
    }
    public String getUWModifyDate() {
        if(UWModifyDate != null) {
            return fDate.getString(UWModifyDate);
        } else {
            return null;
        }
    }
    public void setUWModifyDate(Date aUWModifyDate) {
        UWModifyDate = aUWModifyDate;
    }
    public void setUWModifyDate(String aUWModifyDate) {
        if (aUWModifyDate != null && !aUWModifyDate.equals("")) {
            UWModifyDate = fDate.getDate(aUWModifyDate);
        } else
            UWModifyDate = null;
    }

    public String getConnManagerState() {
        return ConnManagerState;
    }
    public void setConnManagerState(String aConnManagerState) {
        ConnManagerState = aConnManagerState;
    }
    public String getTollFlag() {
        return TollFlag;
    }
    public void setTollFlag(String aTollFlag) {
        TollFlag = aTollFlag;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }
    public String getAgentKind() {
        return AgentKind;
    }
    public void setAgentKind(String aAgentKind) {
        AgentKind = aAgentKind;
    }
    public double getDifficulty() {
        return Difficulty;
    }
    public void setDifficulty(double aDifficulty) {
        Difficulty = aDifficulty;
    }
    public void setDifficulty(String aDifficulty) {
        if (aDifficulty != null && !aDifficulty.equals("")) {
            Double tDouble = new Double(aDifficulty);
            double d = tDouble.doubleValue();
            Difficulty = d;
        }
    }

    public String getAgentGradeRsn() {
        return AgentGradeRsn;
    }
    public void setAgentGradeRsn(String aAgentGradeRsn) {
        AgentGradeRsn = aAgentGradeRsn;
    }
    public String getConnSuccDate() {
        if(ConnSuccDate != null) {
            return fDate.getString(ConnSuccDate);
        } else {
            return null;
        }
    }
    public void setConnSuccDate(Date aConnSuccDate) {
        ConnSuccDate = aConnSuccDate;
    }
    public void setConnSuccDate(String aConnSuccDate) {
        if (aConnSuccDate != null && !aConnSuccDate.equals("")) {
            ConnSuccDate = fDate.getDate(aConnSuccDate);
        } else
            ConnSuccDate = null;
    }

    public String getInsideFlag() {
        return InsideFlag;
    }
    public void setInsideFlag(String aInsideFlag) {
        InsideFlag = aInsideFlag;
    }
    public String getAgentLine() {
        return AgentLine;
    }
    public void setAgentLine(String aAgentLine) {
        AgentLine = aAgentLine;
    }
    public String getIsConnMan() {
        return isConnMan;
    }
    public void setIsConnMan(String aisConnMan) {
        isConnMan = aisConnMan;
    }
    public String getInitGrade() {
        return InitGrade;
    }
    public void setInitGrade(String aInitGrade) {
        InitGrade = aInitGrade;
    }
    public String getSpeciFlag() {
        return SpeciFlag;
    }
    public void setSpeciFlag(String aSpeciFlag) {
        SpeciFlag = aSpeciFlag;
    }
    public String getSpeciStartDate() {
        if(SpeciStartDate != null) {
            return fDate.getString(SpeciStartDate);
        } else {
            return null;
        }
    }
    public void setSpeciStartDate(Date aSpeciStartDate) {
        SpeciStartDate = aSpeciStartDate;
    }
    public void setSpeciStartDate(String aSpeciStartDate) {
        if (aSpeciStartDate != null && !aSpeciStartDate.equals("")) {
            SpeciStartDate = fDate.getDate(aSpeciStartDate);
        } else
            SpeciStartDate = null;
    }

    public String getSpeciEndDate() {
        if(SpeciEndDate != null) {
            return fDate.getString(SpeciEndDate);
        } else {
            return null;
        }
    }
    public void setSpeciEndDate(Date aSpeciEndDate) {
        SpeciEndDate = aSpeciEndDate;
    }
    public void setSpeciEndDate(String aSpeciEndDate) {
        if (aSpeciEndDate != null && !aSpeciEndDate.equals("")) {
            SpeciEndDate = fDate.getDate(aSpeciEndDate);
        } else
            SpeciEndDate = null;
    }

    public String getVIPProperty() {
        return VIPProperty;
    }
    public void setVIPProperty(String aVIPProperty) {
        VIPProperty = aVIPProperty;
    }
    public String getSpeciCode() {
        return SpeciCode;
    }
    public void setSpeciCode(String aSpeciCode) {
        SpeciCode = aSpeciCode;
    }

    /**
    * 使用另外一个 LATreeSchema 对象给 Schema 赋值
    * @param: aLATreeSchema LATreeSchema
    **/
    public void setSchema(LATreeSchema aLATreeSchema) {
        this.AgentCode = aLATreeSchema.getAgentCode();
        this.AgentGroup = aLATreeSchema.getAgentGroup();
        this.ManageCom = aLATreeSchema.getManageCom();
        this.AgentSeries = aLATreeSchema.getAgentSeries();
        this.AgentGrade = aLATreeSchema.getAgentGrade();
        this.AgentLastSeries = aLATreeSchema.getAgentLastSeries();
        this.AgentLastGrade = aLATreeSchema.getAgentLastGrade();
        this.IntroAgency = aLATreeSchema.getIntroAgency();
        this.UpAgent = aLATreeSchema.getUpAgent();
        this.OthUpAgent = aLATreeSchema.getOthUpAgent();
        this.IntroBreakFlag = aLATreeSchema.getIntroBreakFlag();
        this.IntroCommStart = fDate.getDate( aLATreeSchema.getIntroCommStart());
        this.IntroCommEnd = fDate.getDate( aLATreeSchema.getIntroCommEnd());
        this.EduManager = aLATreeSchema.getEduManager();
        this.RearBreakFlag = aLATreeSchema.getRearBreakFlag();
        this.RearCommStart = fDate.getDate( aLATreeSchema.getRearCommStart());
        this.RearCommEnd = fDate.getDate( aLATreeSchema.getRearCommEnd());
        this.AscriptSeries = aLATreeSchema.getAscriptSeries();
        this.OldStartDate = fDate.getDate( aLATreeSchema.getOldStartDate());
        this.OldEndDate = fDate.getDate( aLATreeSchema.getOldEndDate());
        this.StartDate = fDate.getDate( aLATreeSchema.getStartDate());
        this.AstartDate = fDate.getDate( aLATreeSchema.getAstartDate());
        this.AssessType = aLATreeSchema.getAssessType();
        this.State = aLATreeSchema.getState();
        this.Operator = aLATreeSchema.getOperator();
        this.MakeDate = fDate.getDate( aLATreeSchema.getMakeDate());
        this.MakeTime = aLATreeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLATreeSchema.getModifyDate());
        this.ModifyTime = aLATreeSchema.getModifyTime();
        this.BranchCode = aLATreeSchema.getBranchCode();
        this.LastAgentGrade1 = aLATreeSchema.getLastAgentGrade1();
        this.AgentGrade1 = aLATreeSchema.getAgentGrade1();
        this.EmployGrade = aLATreeSchema.getEmployGrade();
        this.BlackLisFlag = aLATreeSchema.getBlackLisFlag();
        this.ReasonType = aLATreeSchema.getReasonType();
        this.Reason = aLATreeSchema.getReason();
        this.UWClass = aLATreeSchema.getUWClass();
        this.UWLevel = aLATreeSchema.getUWLevel();
        this.UWModifyTime = aLATreeSchema.getUWModifyTime();
        this.UWModifyDate = fDate.getDate( aLATreeSchema.getUWModifyDate());
        this.ConnManagerState = aLATreeSchema.getConnManagerState();
        this.TollFlag = aLATreeSchema.getTollFlag();
        this.BranchType = aLATreeSchema.getBranchType();
        this.BranchType2 = aLATreeSchema.getBranchType2();
        this.AgentKind = aLATreeSchema.getAgentKind();
        this.Difficulty = aLATreeSchema.getDifficulty();
        this.AgentGradeRsn = aLATreeSchema.getAgentGradeRsn();
        this.ConnSuccDate = fDate.getDate( aLATreeSchema.getConnSuccDate());
        this.InsideFlag = aLATreeSchema.getInsideFlag();
        this.AgentLine = aLATreeSchema.getAgentLine();
        this.isConnMan = aLATreeSchema.getIsConnMan();
        this.InitGrade = aLATreeSchema.getInitGrade();
        this.SpeciFlag = aLATreeSchema.getSpeciFlag();
        this.SpeciStartDate = fDate.getDate( aLATreeSchema.getSpeciStartDate());
        this.SpeciEndDate = fDate.getDate( aLATreeSchema.getSpeciEndDate());
        this.VIPProperty = aLATreeSchema.getVIPProperty();
        this.SpeciCode = aLATreeSchema.getSpeciCode();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("AgentSeries") == null )
                this.AgentSeries = null;
            else
                this.AgentSeries = rs.getString("AgentSeries").trim();

            if( rs.getString("AgentGrade") == null )
                this.AgentGrade = null;
            else
                this.AgentGrade = rs.getString("AgentGrade").trim();

            if( rs.getString("AgentLastSeries") == null )
                this.AgentLastSeries = null;
            else
                this.AgentLastSeries = rs.getString("AgentLastSeries").trim();

            if( rs.getString("AgentLastGrade") == null )
                this.AgentLastGrade = null;
            else
                this.AgentLastGrade = rs.getString("AgentLastGrade").trim();

            if( rs.getString("IntroAgency") == null )
                this.IntroAgency = null;
            else
                this.IntroAgency = rs.getString("IntroAgency").trim();

            if( rs.getString("UpAgent") == null )
                this.UpAgent = null;
            else
                this.UpAgent = rs.getString("UpAgent").trim();

            if( rs.getString("OthUpAgent") == null )
                this.OthUpAgent = null;
            else
                this.OthUpAgent = rs.getString("OthUpAgent").trim();

            if( rs.getString("IntroBreakFlag") == null )
                this.IntroBreakFlag = null;
            else
                this.IntroBreakFlag = rs.getString("IntroBreakFlag").trim();

            this.IntroCommStart = rs.getDate("IntroCommStart");
            this.IntroCommEnd = rs.getDate("IntroCommEnd");
            if( rs.getString("EduManager") == null )
                this.EduManager = null;
            else
                this.EduManager = rs.getString("EduManager").trim();

            if( rs.getString("RearBreakFlag") == null )
                this.RearBreakFlag = null;
            else
                this.RearBreakFlag = rs.getString("RearBreakFlag").trim();

            this.RearCommStart = rs.getDate("RearCommStart");
            this.RearCommEnd = rs.getDate("RearCommEnd");
            if( rs.getString("AscriptSeries") == null )
                this.AscriptSeries = null;
            else
                this.AscriptSeries = rs.getString("AscriptSeries").trim();

            this.OldStartDate = rs.getDate("OldStartDate");
            this.OldEndDate = rs.getDate("OldEndDate");
            this.StartDate = rs.getDate("StartDate");
            this.AstartDate = rs.getDate("AstartDate");
            if( rs.getString("AssessType") == null )
                this.AssessType = null;
            else
                this.AssessType = rs.getString("AssessType").trim();

            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("BranchCode") == null )
                this.BranchCode = null;
            else
                this.BranchCode = rs.getString("BranchCode").trim();

            if( rs.getString("LastAgentGrade1") == null )
                this.LastAgentGrade1 = null;
            else
                this.LastAgentGrade1 = rs.getString("LastAgentGrade1").trim();

            if( rs.getString("AgentGrade1") == null )
                this.AgentGrade1 = null;
            else
                this.AgentGrade1 = rs.getString("AgentGrade1").trim();

            if( rs.getString("EmployGrade") == null )
                this.EmployGrade = null;
            else
                this.EmployGrade = rs.getString("EmployGrade").trim();

            if( rs.getString("BlackLisFlag") == null )
                this.BlackLisFlag = null;
            else
                this.BlackLisFlag = rs.getString("BlackLisFlag").trim();

            if( rs.getString("ReasonType") == null )
                this.ReasonType = null;
            else
                this.ReasonType = rs.getString("ReasonType").trim();

            if( rs.getString("Reason") == null )
                this.Reason = null;
            else
                this.Reason = rs.getString("Reason").trim();

            if( rs.getString("UWClass") == null )
                this.UWClass = null;
            else
                this.UWClass = rs.getString("UWClass").trim();

            if( rs.getString("UWLevel") == null )
                this.UWLevel = null;
            else
                this.UWLevel = rs.getString("UWLevel").trim();

            if( rs.getString("UWModifyTime") == null )
                this.UWModifyTime = null;
            else
                this.UWModifyTime = rs.getString("UWModifyTime").trim();

            this.UWModifyDate = rs.getDate("UWModifyDate");
            if( rs.getString("ConnManagerState") == null )
                this.ConnManagerState = null;
            else
                this.ConnManagerState = rs.getString("ConnManagerState").trim();

            if( rs.getString("TollFlag") == null )
                this.TollFlag = null;
            else
                this.TollFlag = rs.getString("TollFlag").trim();

            if( rs.getString("BranchType") == null )
                this.BranchType = null;
            else
                this.BranchType = rs.getString("BranchType").trim();

            if( rs.getString("BranchType2") == null )
                this.BranchType2 = null;
            else
                this.BranchType2 = rs.getString("BranchType2").trim();

            if( rs.getString("AgentKind") == null )
                this.AgentKind = null;
            else
                this.AgentKind = rs.getString("AgentKind").trim();

            this.Difficulty = rs.getDouble("Difficulty");
            if( rs.getString("AgentGradeRsn") == null )
                this.AgentGradeRsn = null;
            else
                this.AgentGradeRsn = rs.getString("AgentGradeRsn").trim();

            this.ConnSuccDate = rs.getDate("ConnSuccDate");
            if( rs.getString("InsideFlag") == null )
                this.InsideFlag = null;
            else
                this.InsideFlag = rs.getString("InsideFlag").trim();

            if( rs.getString("AgentLine") == null )
                this.AgentLine = null;
            else
                this.AgentLine = rs.getString("AgentLine").trim();

            if( rs.getString("isConnMan") == null )
                this.isConnMan = null;
            else
                this.isConnMan = rs.getString("isConnMan").trim();

            if( rs.getString("InitGrade") == null )
                this.InitGrade = null;
            else
                this.InitGrade = rs.getString("InitGrade").trim();

            if( rs.getString("SpeciFlag") == null )
                this.SpeciFlag = null;
            else
                this.SpeciFlag = rs.getString("SpeciFlag").trim();

            this.SpeciStartDate = rs.getDate("SpeciStartDate");
            this.SpeciEndDate = rs.getDate("SpeciEndDate");
            if( rs.getString("VIPProperty") == null )
                this.VIPProperty = null;
            else
                this.VIPProperty = rs.getString("VIPProperty").trim();

            if( rs.getString("SpeciCode") == null )
                this.SpeciCode = null;
            else
                this.SpeciCode = rs.getString("SpeciCode").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LATreeSchema getSchema() {
        LATreeSchema aLATreeSchema = new LATreeSchema();
        aLATreeSchema.setSchema(this);
        return aLATreeSchema;
    }

    public LATreeDB getDB() {
        LATreeDB aDBOper = new LATreeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATree描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentSeries)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentLastSeries)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentLastGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IntroAgency)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UpAgent)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OthUpAgent)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IntroBreakFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( IntroCommStart ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( IntroCommEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EduManager)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RearBreakFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( RearCommStart ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( RearCommEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AscriptSeries)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( OldStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( OldEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AstartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AssessType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LastAgentGrade1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGrade1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EmployGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlackLisFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReasonType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Reason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWClass)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWLevel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UWModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConnManagerState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TollFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentKind)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Difficulty));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGradeRsn)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ConnSuccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsideFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentLine)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(isConnMan)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InitGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpeciFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SpeciStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SpeciEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIPProperty)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpeciCode));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATree>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AgentSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AgentLastSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            AgentLastGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            IntroAgency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            UpAgent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            OthUpAgent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            IntroBreakFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            IntroCommStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
            IntroCommEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
            EduManager = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            RearBreakFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            RearCommStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
            RearCommEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
            AscriptSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            OldStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
            OldEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
            AstartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
            AssessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            BranchCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            LastAgentGrade1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            AgentGrade1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            EmployGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            BlackLisFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            ReasonType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            UWClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            UWLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            UWModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            UWModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
            ConnManagerState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            TollFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            AgentKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            Difficulty = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
            AgentGradeRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            ConnSuccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48,SysConst.PACKAGESPILTER));
            InsideFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            AgentLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            isConnMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            InitGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            SpeciFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
            SpeciStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,SysConst.PACKAGESPILTER));
            SpeciEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,SysConst.PACKAGESPILTER));
            VIPProperty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
            SpeciCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentSeries")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSeries));
        }
        if (FCode.equalsIgnoreCase("AgentGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equalsIgnoreCase("AgentLastSeries")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentLastSeries));
        }
        if (FCode.equalsIgnoreCase("AgentLastGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentLastGrade));
        }
        if (FCode.equalsIgnoreCase("IntroAgency")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IntroAgency));
        }
        if (FCode.equalsIgnoreCase("UpAgent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UpAgent));
        }
        if (FCode.equalsIgnoreCase("OthUpAgent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OthUpAgent));
        }
        if (FCode.equalsIgnoreCase("IntroBreakFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IntroBreakFlag));
        }
        if (FCode.equalsIgnoreCase("IntroCommStart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIntroCommStart()));
        }
        if (FCode.equalsIgnoreCase("IntroCommEnd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIntroCommEnd()));
        }
        if (FCode.equalsIgnoreCase("EduManager")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EduManager));
        }
        if (FCode.equalsIgnoreCase("RearBreakFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearBreakFlag));
        }
        if (FCode.equalsIgnoreCase("RearCommStart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRearCommStart()));
        }
        if (FCode.equalsIgnoreCase("RearCommEnd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRearCommEnd()));
        }
        if (FCode.equalsIgnoreCase("AscriptSeries")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptSeries));
        }
        if (FCode.equalsIgnoreCase("OldStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOldStartDate()));
        }
        if (FCode.equalsIgnoreCase("OldEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOldEndDate()));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
        }
        if (FCode.equalsIgnoreCase("AstartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAstartDate()));
        }
        if (FCode.equalsIgnoreCase("AssessType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessType));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("BranchCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
        }
        if (FCode.equalsIgnoreCase("LastAgentGrade1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastAgentGrade1));
        }
        if (FCode.equalsIgnoreCase("AgentGrade1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade1));
        }
        if (FCode.equalsIgnoreCase("EmployGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmployGrade));
        }
        if (FCode.equalsIgnoreCase("BlackLisFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackLisFlag));
        }
        if (FCode.equalsIgnoreCase("ReasonType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReasonType));
        }
        if (FCode.equalsIgnoreCase("Reason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
        }
        if (FCode.equalsIgnoreCase("UWClass")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWClass));
        }
        if (FCode.equalsIgnoreCase("UWLevel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWLevel));
        }
        if (FCode.equalsIgnoreCase("UWModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWModifyTime));
        }
        if (FCode.equalsIgnoreCase("UWModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ConnManagerState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConnManagerState));
        }
        if (FCode.equalsIgnoreCase("TollFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TollFlag));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equalsIgnoreCase("AgentKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentKind));
        }
        if (FCode.equalsIgnoreCase("Difficulty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Difficulty));
        }
        if (FCode.equalsIgnoreCase("AgentGradeRsn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGradeRsn));
        }
        if (FCode.equalsIgnoreCase("ConnSuccDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConnSuccDate()));
        }
        if (FCode.equalsIgnoreCase("InsideFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsideFlag));
        }
        if (FCode.equalsIgnoreCase("AgentLine")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentLine));
        }
        if (FCode.equalsIgnoreCase("isConnMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(isConnMan));
        }
        if (FCode.equalsIgnoreCase("InitGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InitGrade));
        }
        if (FCode.equalsIgnoreCase("SpeciFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpeciFlag));
        }
        if (FCode.equalsIgnoreCase("SpeciStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSpeciStartDate()));
        }
        if (FCode.equalsIgnoreCase("SpeciEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSpeciEndDate()));
        }
        if (FCode.equalsIgnoreCase("VIPProperty")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIPProperty));
        }
        if (FCode.equalsIgnoreCase("SpeciCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpeciCode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentSeries);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AgentLastSeries);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AgentLastGrade);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(IntroAgency);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(UpAgent);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(OthUpAgent);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(IntroBreakFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIntroCommStart()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIntroCommEnd()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(EduManager);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(RearBreakFlag);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRearCommStart()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRearCommEnd()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(AscriptSeries);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOldStartDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOldEndDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAstartDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(AssessType);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(BranchCode);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(LastAgentGrade1);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade1);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(EmployGrade);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(BlackLisFlag);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(ReasonType);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(Reason);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(UWClass);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(UWLevel);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(UWModifyTime);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWModifyDate()));
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(ConnManagerState);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(TollFlag);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(AgentKind);
                break;
            case 45:
                strFieldValue = String.valueOf(Difficulty);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(AgentGradeRsn);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConnSuccDate()));
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(InsideFlag);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(AgentLine);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(isConnMan);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(InitGrade);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(SpeciFlag);
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSpeciStartDate()));
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSpeciEndDate()));
                break;
            case 55:
                strFieldValue = StrTool.GBKToUnicode(VIPProperty);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(SpeciCode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentSeries")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentSeries = FValue.trim();
            }
            else
                AgentSeries = null;
        }
        if (FCode.equalsIgnoreCase("AgentGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
                AgentGrade = null;
        }
        if (FCode.equalsIgnoreCase("AgentLastSeries")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentLastSeries = FValue.trim();
            }
            else
                AgentLastSeries = null;
        }
        if (FCode.equalsIgnoreCase("AgentLastGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentLastGrade = FValue.trim();
            }
            else
                AgentLastGrade = null;
        }
        if (FCode.equalsIgnoreCase("IntroAgency")) {
            if( FValue != null && !FValue.equals(""))
            {
                IntroAgency = FValue.trim();
            }
            else
                IntroAgency = null;
        }
        if (FCode.equalsIgnoreCase("UpAgent")) {
            if( FValue != null && !FValue.equals(""))
            {
                UpAgent = FValue.trim();
            }
            else
                UpAgent = null;
        }
        if (FCode.equalsIgnoreCase("OthUpAgent")) {
            if( FValue != null && !FValue.equals(""))
            {
                OthUpAgent = FValue.trim();
            }
            else
                OthUpAgent = null;
        }
        if (FCode.equalsIgnoreCase("IntroBreakFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                IntroBreakFlag = FValue.trim();
            }
            else
                IntroBreakFlag = null;
        }
        if (FCode.equalsIgnoreCase("IntroCommStart")) {
            if(FValue != null && !FValue.equals("")) {
                IntroCommStart = fDate.getDate( FValue );
            }
            else
                IntroCommStart = null;
        }
        if (FCode.equalsIgnoreCase("IntroCommEnd")) {
            if(FValue != null && !FValue.equals("")) {
                IntroCommEnd = fDate.getDate( FValue );
            }
            else
                IntroCommEnd = null;
        }
        if (FCode.equalsIgnoreCase("EduManager")) {
            if( FValue != null && !FValue.equals(""))
            {
                EduManager = FValue.trim();
            }
            else
                EduManager = null;
        }
        if (FCode.equalsIgnoreCase("RearBreakFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                RearBreakFlag = FValue.trim();
            }
            else
                RearBreakFlag = null;
        }
        if (FCode.equalsIgnoreCase("RearCommStart")) {
            if(FValue != null && !FValue.equals("")) {
                RearCommStart = fDate.getDate( FValue );
            }
            else
                RearCommStart = null;
        }
        if (FCode.equalsIgnoreCase("RearCommEnd")) {
            if(FValue != null && !FValue.equals("")) {
                RearCommEnd = fDate.getDate( FValue );
            }
            else
                RearCommEnd = null;
        }
        if (FCode.equalsIgnoreCase("AscriptSeries")) {
            if( FValue != null && !FValue.equals(""))
            {
                AscriptSeries = FValue.trim();
            }
            else
                AscriptSeries = null;
        }
        if (FCode.equalsIgnoreCase("OldStartDate")) {
            if(FValue != null && !FValue.equals("")) {
                OldStartDate = fDate.getDate( FValue );
            }
            else
                OldStartDate = null;
        }
        if (FCode.equalsIgnoreCase("OldEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                OldEndDate = fDate.getDate( FValue );
            }
            else
                OldEndDate = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate( FValue );
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("AstartDate")) {
            if(FValue != null && !FValue.equals("")) {
                AstartDate = fDate.getDate( FValue );
            }
            else
                AstartDate = null;
        }
        if (FCode.equalsIgnoreCase("AssessType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AssessType = FValue.trim();
            }
            else
                AssessType = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("BranchCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchCode = FValue.trim();
            }
            else
                BranchCode = null;
        }
        if (FCode.equalsIgnoreCase("LastAgentGrade1")) {
            if( FValue != null && !FValue.equals(""))
            {
                LastAgentGrade1 = FValue.trim();
            }
            else
                LastAgentGrade1 = null;
        }
        if (FCode.equalsIgnoreCase("AgentGrade1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGrade1 = FValue.trim();
            }
            else
                AgentGrade1 = null;
        }
        if (FCode.equalsIgnoreCase("EmployGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                EmployGrade = FValue.trim();
            }
            else
                EmployGrade = null;
        }
        if (FCode.equalsIgnoreCase("BlackLisFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackLisFlag = FValue.trim();
            }
            else
                BlackLisFlag = null;
        }
        if (FCode.equalsIgnoreCase("ReasonType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReasonType = FValue.trim();
            }
            else
                ReasonType = null;
        }
        if (FCode.equalsIgnoreCase("Reason")) {
            if( FValue != null && !FValue.equals(""))
            {
                Reason = FValue.trim();
            }
            else
                Reason = null;
        }
        if (FCode.equalsIgnoreCase("UWClass")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWClass = FValue.trim();
            }
            else
                UWClass = null;
        }
        if (FCode.equalsIgnoreCase("UWLevel")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWLevel = FValue.trim();
            }
            else
                UWLevel = null;
        }
        if (FCode.equalsIgnoreCase("UWModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWModifyTime = FValue.trim();
            }
            else
                UWModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("UWModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                UWModifyDate = fDate.getDate( FValue );
            }
            else
                UWModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ConnManagerState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ConnManagerState = FValue.trim();
            }
            else
                ConnManagerState = null;
        }
        if (FCode.equalsIgnoreCase("TollFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                TollFlag = FValue.trim();
            }
            else
                TollFlag = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        if (FCode.equalsIgnoreCase("AgentKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentKind = FValue.trim();
            }
            else
                AgentKind = null;
        }
        if (FCode.equalsIgnoreCase("Difficulty")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Difficulty = d;
            }
        }
        if (FCode.equalsIgnoreCase("AgentGradeRsn")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGradeRsn = FValue.trim();
            }
            else
                AgentGradeRsn = null;
        }
        if (FCode.equalsIgnoreCase("ConnSuccDate")) {
            if(FValue != null && !FValue.equals("")) {
                ConnSuccDate = fDate.getDate( FValue );
            }
            else
                ConnSuccDate = null;
        }
        if (FCode.equalsIgnoreCase("InsideFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsideFlag = FValue.trim();
            }
            else
                InsideFlag = null;
        }
        if (FCode.equalsIgnoreCase("AgentLine")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentLine = FValue.trim();
            }
            else
                AgentLine = null;
        }
        if (FCode.equalsIgnoreCase("isConnMan")) {
            if( FValue != null && !FValue.equals(""))
            {
                isConnMan = FValue.trim();
            }
            else
                isConnMan = null;
        }
        if (FCode.equalsIgnoreCase("InitGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                InitGrade = FValue.trim();
            }
            else
                InitGrade = null;
        }
        if (FCode.equalsIgnoreCase("SpeciFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpeciFlag = FValue.trim();
            }
            else
                SpeciFlag = null;
        }
        if (FCode.equalsIgnoreCase("SpeciStartDate")) {
            if(FValue != null && !FValue.equals("")) {
                SpeciStartDate = fDate.getDate( FValue );
            }
            else
                SpeciStartDate = null;
        }
        if (FCode.equalsIgnoreCase("SpeciEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                SpeciEndDate = fDate.getDate( FValue );
            }
            else
                SpeciEndDate = null;
        }
        if (FCode.equalsIgnoreCase("VIPProperty")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIPProperty = FValue.trim();
            }
            else
                VIPProperty = null;
        }
        if (FCode.equalsIgnoreCase("SpeciCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                SpeciCode = FValue.trim();
            }
            else
                SpeciCode = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LATreeSchema other = (LATreeSchema)otherObject;
        return
            AgentCode.equals(other.getAgentCode())
            && AgentGroup.equals(other.getAgentGroup())
            && ManageCom.equals(other.getManageCom())
            && AgentSeries.equals(other.getAgentSeries())
            && AgentGrade.equals(other.getAgentGrade())
            && AgentLastSeries.equals(other.getAgentLastSeries())
            && AgentLastGrade.equals(other.getAgentLastGrade())
            && IntroAgency.equals(other.getIntroAgency())
            && UpAgent.equals(other.getUpAgent())
            && OthUpAgent.equals(other.getOthUpAgent())
            && IntroBreakFlag.equals(other.getIntroBreakFlag())
            && fDate.getString(IntroCommStart).equals(other.getIntroCommStart())
            && fDate.getString(IntroCommEnd).equals(other.getIntroCommEnd())
            && EduManager.equals(other.getEduManager())
            && RearBreakFlag.equals(other.getRearBreakFlag())
            && fDate.getString(RearCommStart).equals(other.getRearCommStart())
            && fDate.getString(RearCommEnd).equals(other.getRearCommEnd())
            && AscriptSeries.equals(other.getAscriptSeries())
            && fDate.getString(OldStartDate).equals(other.getOldStartDate())
            && fDate.getString(OldEndDate).equals(other.getOldEndDate())
            && fDate.getString(StartDate).equals(other.getStartDate())
            && fDate.getString(AstartDate).equals(other.getAstartDate())
            && AssessType.equals(other.getAssessType())
            && State.equals(other.getState())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && BranchCode.equals(other.getBranchCode())
            && LastAgentGrade1.equals(other.getLastAgentGrade1())
            && AgentGrade1.equals(other.getAgentGrade1())
            && EmployGrade.equals(other.getEmployGrade())
            && BlackLisFlag.equals(other.getBlackLisFlag())
            && ReasonType.equals(other.getReasonType())
            && Reason.equals(other.getReason())
            && UWClass.equals(other.getUWClass())
            && UWLevel.equals(other.getUWLevel())
            && UWModifyTime.equals(other.getUWModifyTime())
            && fDate.getString(UWModifyDate).equals(other.getUWModifyDate())
            && ConnManagerState.equals(other.getConnManagerState())
            && TollFlag.equals(other.getTollFlag())
            && BranchType.equals(other.getBranchType())
            && BranchType2.equals(other.getBranchType2())
            && AgentKind.equals(other.getAgentKind())
            && Difficulty == other.getDifficulty()
            && AgentGradeRsn.equals(other.getAgentGradeRsn())
            && fDate.getString(ConnSuccDate).equals(other.getConnSuccDate())
            && InsideFlag.equals(other.getInsideFlag())
            && AgentLine.equals(other.getAgentLine())
            && isConnMan.equals(other.getIsConnMan())
            && InitGrade.equals(other.getInitGrade())
            && SpeciFlag.equals(other.getSpeciFlag())
            && fDate.getString(SpeciStartDate).equals(other.getSpeciStartDate())
            && fDate.getString(SpeciEndDate).equals(other.getSpeciEndDate())
            && VIPProperty.equals(other.getVIPProperty())
            && SpeciCode.equals(other.getSpeciCode());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCode") ) {
            return 0;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 1;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 2;
        }
        if( strFieldName.equals("AgentSeries") ) {
            return 3;
        }
        if( strFieldName.equals("AgentGrade") ) {
            return 4;
        }
        if( strFieldName.equals("AgentLastSeries") ) {
            return 5;
        }
        if( strFieldName.equals("AgentLastGrade") ) {
            return 6;
        }
        if( strFieldName.equals("IntroAgency") ) {
            return 7;
        }
        if( strFieldName.equals("UpAgent") ) {
            return 8;
        }
        if( strFieldName.equals("OthUpAgent") ) {
            return 9;
        }
        if( strFieldName.equals("IntroBreakFlag") ) {
            return 10;
        }
        if( strFieldName.equals("IntroCommStart") ) {
            return 11;
        }
        if( strFieldName.equals("IntroCommEnd") ) {
            return 12;
        }
        if( strFieldName.equals("EduManager") ) {
            return 13;
        }
        if( strFieldName.equals("RearBreakFlag") ) {
            return 14;
        }
        if( strFieldName.equals("RearCommStart") ) {
            return 15;
        }
        if( strFieldName.equals("RearCommEnd") ) {
            return 16;
        }
        if( strFieldName.equals("AscriptSeries") ) {
            return 17;
        }
        if( strFieldName.equals("OldStartDate") ) {
            return 18;
        }
        if( strFieldName.equals("OldEndDate") ) {
            return 19;
        }
        if( strFieldName.equals("StartDate") ) {
            return 20;
        }
        if( strFieldName.equals("AstartDate") ) {
            return 21;
        }
        if( strFieldName.equals("AssessType") ) {
            return 22;
        }
        if( strFieldName.equals("State") ) {
            return 23;
        }
        if( strFieldName.equals("Operator") ) {
            return 24;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 25;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 26;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 27;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 28;
        }
        if( strFieldName.equals("BranchCode") ) {
            return 29;
        }
        if( strFieldName.equals("LastAgentGrade1") ) {
            return 30;
        }
        if( strFieldName.equals("AgentGrade1") ) {
            return 31;
        }
        if( strFieldName.equals("EmployGrade") ) {
            return 32;
        }
        if( strFieldName.equals("BlackLisFlag") ) {
            return 33;
        }
        if( strFieldName.equals("ReasonType") ) {
            return 34;
        }
        if( strFieldName.equals("Reason") ) {
            return 35;
        }
        if( strFieldName.equals("UWClass") ) {
            return 36;
        }
        if( strFieldName.equals("UWLevel") ) {
            return 37;
        }
        if( strFieldName.equals("UWModifyTime") ) {
            return 38;
        }
        if( strFieldName.equals("UWModifyDate") ) {
            return 39;
        }
        if( strFieldName.equals("ConnManagerState") ) {
            return 40;
        }
        if( strFieldName.equals("TollFlag") ) {
            return 41;
        }
        if( strFieldName.equals("BranchType") ) {
            return 42;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 43;
        }
        if( strFieldName.equals("AgentKind") ) {
            return 44;
        }
        if( strFieldName.equals("Difficulty") ) {
            return 45;
        }
        if( strFieldName.equals("AgentGradeRsn") ) {
            return 46;
        }
        if( strFieldName.equals("ConnSuccDate") ) {
            return 47;
        }
        if( strFieldName.equals("InsideFlag") ) {
            return 48;
        }
        if( strFieldName.equals("AgentLine") ) {
            return 49;
        }
        if( strFieldName.equals("isConnMan") ) {
            return 50;
        }
        if( strFieldName.equals("InitGrade") ) {
            return 51;
        }
        if( strFieldName.equals("SpeciFlag") ) {
            return 52;
        }
        if( strFieldName.equals("SpeciStartDate") ) {
            return 53;
        }
        if( strFieldName.equals("SpeciEndDate") ) {
            return 54;
        }
        if( strFieldName.equals("VIPProperty") ) {
            return 55;
        }
        if( strFieldName.equals("SpeciCode") ) {
            return 56;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "AgentGroup";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "AgentSeries";
                break;
            case 4:
                strFieldName = "AgentGrade";
                break;
            case 5:
                strFieldName = "AgentLastSeries";
                break;
            case 6:
                strFieldName = "AgentLastGrade";
                break;
            case 7:
                strFieldName = "IntroAgency";
                break;
            case 8:
                strFieldName = "UpAgent";
                break;
            case 9:
                strFieldName = "OthUpAgent";
                break;
            case 10:
                strFieldName = "IntroBreakFlag";
                break;
            case 11:
                strFieldName = "IntroCommStart";
                break;
            case 12:
                strFieldName = "IntroCommEnd";
                break;
            case 13:
                strFieldName = "EduManager";
                break;
            case 14:
                strFieldName = "RearBreakFlag";
                break;
            case 15:
                strFieldName = "RearCommStart";
                break;
            case 16:
                strFieldName = "RearCommEnd";
                break;
            case 17:
                strFieldName = "AscriptSeries";
                break;
            case 18:
                strFieldName = "OldStartDate";
                break;
            case 19:
                strFieldName = "OldEndDate";
                break;
            case 20:
                strFieldName = "StartDate";
                break;
            case 21:
                strFieldName = "AstartDate";
                break;
            case 22:
                strFieldName = "AssessType";
                break;
            case 23:
                strFieldName = "State";
                break;
            case 24:
                strFieldName = "Operator";
                break;
            case 25:
                strFieldName = "MakeDate";
                break;
            case 26:
                strFieldName = "MakeTime";
                break;
            case 27:
                strFieldName = "ModifyDate";
                break;
            case 28:
                strFieldName = "ModifyTime";
                break;
            case 29:
                strFieldName = "BranchCode";
                break;
            case 30:
                strFieldName = "LastAgentGrade1";
                break;
            case 31:
                strFieldName = "AgentGrade1";
                break;
            case 32:
                strFieldName = "EmployGrade";
                break;
            case 33:
                strFieldName = "BlackLisFlag";
                break;
            case 34:
                strFieldName = "ReasonType";
                break;
            case 35:
                strFieldName = "Reason";
                break;
            case 36:
                strFieldName = "UWClass";
                break;
            case 37:
                strFieldName = "UWLevel";
                break;
            case 38:
                strFieldName = "UWModifyTime";
                break;
            case 39:
                strFieldName = "UWModifyDate";
                break;
            case 40:
                strFieldName = "ConnManagerState";
                break;
            case 41:
                strFieldName = "TollFlag";
                break;
            case 42:
                strFieldName = "BranchType";
                break;
            case 43:
                strFieldName = "BranchType2";
                break;
            case 44:
                strFieldName = "AgentKind";
                break;
            case 45:
                strFieldName = "Difficulty";
                break;
            case 46:
                strFieldName = "AgentGradeRsn";
                break;
            case 47:
                strFieldName = "ConnSuccDate";
                break;
            case 48:
                strFieldName = "InsideFlag";
                break;
            case 49:
                strFieldName = "AgentLine";
                break;
            case 50:
                strFieldName = "isConnMan";
                break;
            case 51:
                strFieldName = "InitGrade";
                break;
            case 52:
                strFieldName = "SpeciFlag";
                break;
            case 53:
                strFieldName = "SpeciStartDate";
                break;
            case 54:
                strFieldName = "SpeciEndDate";
                break;
            case 55:
                strFieldName = "VIPProperty";
                break;
            case 56:
                strFieldName = "SpeciCode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTSERIES":
                return Schema.TYPE_STRING;
            case "AGENTGRADE":
                return Schema.TYPE_STRING;
            case "AGENTLASTSERIES":
                return Schema.TYPE_STRING;
            case "AGENTLASTGRADE":
                return Schema.TYPE_STRING;
            case "INTROAGENCY":
                return Schema.TYPE_STRING;
            case "UPAGENT":
                return Schema.TYPE_STRING;
            case "OTHUPAGENT":
                return Schema.TYPE_STRING;
            case "INTROBREAKFLAG":
                return Schema.TYPE_STRING;
            case "INTROCOMMSTART":
                return Schema.TYPE_DATE;
            case "INTROCOMMEND":
                return Schema.TYPE_DATE;
            case "EDUMANAGER":
                return Schema.TYPE_STRING;
            case "REARBREAKFLAG":
                return Schema.TYPE_STRING;
            case "REARCOMMSTART":
                return Schema.TYPE_DATE;
            case "REARCOMMEND":
                return Schema.TYPE_DATE;
            case "ASCRIPTSERIES":
                return Schema.TYPE_STRING;
            case "OLDSTARTDATE":
                return Schema.TYPE_DATE;
            case "OLDENDDATE":
                return Schema.TYPE_DATE;
            case "STARTDATE":
                return Schema.TYPE_DATE;
            case "ASTARTDATE":
                return Schema.TYPE_DATE;
            case "ASSESSTYPE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "BRANCHCODE":
                return Schema.TYPE_STRING;
            case "LASTAGENTGRADE1":
                return Schema.TYPE_STRING;
            case "AGENTGRADE1":
                return Schema.TYPE_STRING;
            case "EMPLOYGRADE":
                return Schema.TYPE_STRING;
            case "BLACKLISFLAG":
                return Schema.TYPE_STRING;
            case "REASONTYPE":
                return Schema.TYPE_STRING;
            case "REASON":
                return Schema.TYPE_STRING;
            case "UWCLASS":
                return Schema.TYPE_STRING;
            case "UWLEVEL":
                return Schema.TYPE_STRING;
            case "UWMODIFYTIME":
                return Schema.TYPE_STRING;
            case "UWMODIFYDATE":
                return Schema.TYPE_DATE;
            case "CONNMANAGERSTATE":
                return Schema.TYPE_STRING;
            case "TOLLFLAG":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "AGENTKIND":
                return Schema.TYPE_STRING;
            case "DIFFICULTY":
                return Schema.TYPE_DOUBLE;
            case "AGENTGRADERSN":
                return Schema.TYPE_STRING;
            case "CONNSUCCDATE":
                return Schema.TYPE_DATE;
            case "INSIDEFLAG":
                return Schema.TYPE_STRING;
            case "AGENTLINE":
                return Schema.TYPE_STRING;
            case "ISCONNMAN":
                return Schema.TYPE_STRING;
            case "INITGRADE":
                return Schema.TYPE_STRING;
            case "SPECIFLAG":
                return Schema.TYPE_STRING;
            case "SPECISTARTDATE":
                return Schema.TYPE_DATE;
            case "SPECIENDDATE":
                return Schema.TYPE_DATE;
            case "VIPPROPERTY":
                return Schema.TYPE_STRING;
            case "SPECICODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DATE;
            case 12:
                return Schema.TYPE_DATE;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_DATE;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_DATE;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_DATE;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_DATE;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_DATE;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_DOUBLE;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_DATE;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_DATE;
            case 54:
                return Schema.TYPE_DATE;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
