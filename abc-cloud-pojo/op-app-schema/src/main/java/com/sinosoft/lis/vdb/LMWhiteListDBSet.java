/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LMWhiteListSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LMWhiteListDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-08-22
 */
public class LMWhiteListDBSet extends LMWhiteListSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LMWhiteListDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LMWhiteList");
        mflag = true;
    }

    public LMWhiteListDBSet() {
        db = new DBOper( "LMWhiteList" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMWhiteListDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LMWhiteList WHERE  1=1  AND SeqNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSeqNo() == null || this.get(i).getSeqNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSeqNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMWhiteListDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LMWhiteList SET  SeqNo = ? , CustomerNo = ? , Name = ? , Sex = ? , IDType = ? , IDNo = ? , BirthDay = ? , Amnt = ? , WFWFlag = ? , AmntType = ? , ApplyOpertor = ? , ApplyDate = ? , ApplyTime = ? , ApplyState = ? , BackOpertor = ? , ApproveOne = ? , ApproveOneDate = ? , ApproveOneTime = ? , AOVerdict = ? , AOSuggestion = ? , ApproveTwo = ? , ApproveTwoDate = ? , ApproveTwoTime = ? , ATVerdict = ? , ATSuggestion = ? , ModifyDate = ? , ModifyTime = ? , RiskCode = ? , ManageCom = ? , UserState = ? , IdValiDate = ? , StandByFlag1 = ? , DiCardNo = ? , DiType = ? WHERE  1=1  AND SeqNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSeqNo() == null || this.get(i).getSeqNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSeqNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getName());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSex());
            }
            if(this.get(i).getIDType() == null || this.get(i).getIDType().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getIDType());
            }
            if(this.get(i).getIDNo() == null || this.get(i).getIDNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getIDNo());
            }
            if(this.get(i).getBirthDay() == null || this.get(i).getBirthDay().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getBirthDay()));
            }
            pstmt.setDouble(8, this.get(i).getAmnt());
            if(this.get(i).getWFWFlag() == null || this.get(i).getWFWFlag().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getWFWFlag());
            }
            if(this.get(i).getAmntType() == null || this.get(i).getAmntType().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAmntType());
            }
            if(this.get(i).getApplyOpertor() == null || this.get(i).getApplyOpertor().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getApplyOpertor());
            }
            if(this.get(i).getApplyDate() == null || this.get(i).getApplyDate().equals("null")) {
                pstmt.setDate(12,null);
            } else {
                pstmt.setDate(12, Date.valueOf(this.get(i).getApplyDate()));
            }
            if(this.get(i).getApplyTime() == null || this.get(i).getApplyTime().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getApplyTime());
            }
            if(this.get(i).getApplyState() == null || this.get(i).getApplyState().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getApplyState());
            }
            if(this.get(i).getBackOpertor() == null || this.get(i).getBackOpertor().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getBackOpertor());
            }
            if(this.get(i).getApproveOne() == null || this.get(i).getApproveOne().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getApproveOne());
            }
            if(this.get(i).getApproveOneDate() == null || this.get(i).getApproveOneDate().equals("null")) {
                pstmt.setDate(17,null);
            } else {
                pstmt.setDate(17, Date.valueOf(this.get(i).getApproveOneDate()));
            }
            if(this.get(i).getApproveOneTime() == null || this.get(i).getApproveOneTime().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getApproveOneTime());
            }
            if(this.get(i).getAOVerdict() == null || this.get(i).getAOVerdict().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getAOVerdict());
            }
            if(this.get(i).getAOSuggestion() == null || this.get(i).getAOSuggestion().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getAOSuggestion());
            }
            if(this.get(i).getApproveTwo() == null || this.get(i).getApproveTwo().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getApproveTwo());
            }
            if(this.get(i).getApproveTwoDate() == null || this.get(i).getApproveTwoDate().equals("null")) {
                pstmt.setDate(22,null);
            } else {
                pstmt.setDate(22, Date.valueOf(this.get(i).getApproveTwoDate()));
            }
            if(this.get(i).getApproveTwoTime() == null || this.get(i).getApproveTwoTime().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getApproveTwoTime());
            }
            if(this.get(i).getATVerdict() == null || this.get(i).getATVerdict().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getATVerdict());
            }
            if(this.get(i).getATSuggestion() == null || this.get(i).getATSuggestion().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getATSuggestion());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getModifyTime());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getManageCom());
            }
            if(this.get(i).getUserState() == null || this.get(i).getUserState().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getUserState());
            }
            if(this.get(i).getIdValiDate() == null || this.get(i).getIdValiDate().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getIdValiDate());
            }
            if(this.get(i).getStandByFlag1() == null || this.get(i).getStandByFlag1().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getStandByFlag1());
            }
            if(this.get(i).getDiCardNo() == null || this.get(i).getDiCardNo().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getDiCardNo());
            }
            if(this.get(i).getDiType() == null || this.get(i).getDiType().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getDiType());
            }
            // set where condition
            if(this.get(i).getSeqNo() == null || this.get(i).getSeqNo().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getSeqNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMWhiteListDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LMWhiteList VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSeqNo() == null || this.get(i).getSeqNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSeqNo());
            }
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCustomerNo());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getName());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSex());
            }
            if(this.get(i).getIDType() == null || this.get(i).getIDType().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getIDType());
            }
            if(this.get(i).getIDNo() == null || this.get(i).getIDNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getIDNo());
            }
            if(this.get(i).getBirthDay() == null || this.get(i).getBirthDay().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getBirthDay()));
            }
            pstmt.setDouble(8, this.get(i).getAmnt());
            if(this.get(i).getWFWFlag() == null || this.get(i).getWFWFlag().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getWFWFlag());
            }
            if(this.get(i).getAmntType() == null || this.get(i).getAmntType().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAmntType());
            }
            if(this.get(i).getApplyOpertor() == null || this.get(i).getApplyOpertor().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getApplyOpertor());
            }
            if(this.get(i).getApplyDate() == null || this.get(i).getApplyDate().equals("null")) {
                pstmt.setDate(12,null);
            } else {
                pstmt.setDate(12, Date.valueOf(this.get(i).getApplyDate()));
            }
            if(this.get(i).getApplyTime() == null || this.get(i).getApplyTime().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getApplyTime());
            }
            if(this.get(i).getApplyState() == null || this.get(i).getApplyState().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getApplyState());
            }
            if(this.get(i).getBackOpertor() == null || this.get(i).getBackOpertor().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getBackOpertor());
            }
            if(this.get(i).getApproveOne() == null || this.get(i).getApproveOne().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getApproveOne());
            }
            if(this.get(i).getApproveOneDate() == null || this.get(i).getApproveOneDate().equals("null")) {
                pstmt.setDate(17,null);
            } else {
                pstmt.setDate(17, Date.valueOf(this.get(i).getApproveOneDate()));
            }
            if(this.get(i).getApproveOneTime() == null || this.get(i).getApproveOneTime().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getApproveOneTime());
            }
            if(this.get(i).getAOVerdict() == null || this.get(i).getAOVerdict().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getAOVerdict());
            }
            if(this.get(i).getAOSuggestion() == null || this.get(i).getAOSuggestion().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getAOSuggestion());
            }
            if(this.get(i).getApproveTwo() == null || this.get(i).getApproveTwo().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getApproveTwo());
            }
            if(this.get(i).getApproveTwoDate() == null || this.get(i).getApproveTwoDate().equals("null")) {
                pstmt.setDate(22,null);
            } else {
                pstmt.setDate(22, Date.valueOf(this.get(i).getApproveTwoDate()));
            }
            if(this.get(i).getApproveTwoTime() == null || this.get(i).getApproveTwoTime().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getApproveTwoTime());
            }
            if(this.get(i).getATVerdict() == null || this.get(i).getATVerdict().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getATVerdict());
            }
            if(this.get(i).getATSuggestion() == null || this.get(i).getATSuggestion().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getATSuggestion());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(26,null);
            } else {
                pstmt.setDate(26, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getModifyTime());
            }
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getManageCom());
            }
            if(this.get(i).getUserState() == null || this.get(i).getUserState().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getUserState());
            }
            if(this.get(i).getIdValiDate() == null || this.get(i).getIdValiDate().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getIdValiDate());
            }
            if(this.get(i).getStandByFlag1() == null || this.get(i).getStandByFlag1().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getStandByFlag1());
            }
            if(this.get(i).getDiCardNo() == null || this.get(i).getDiCardNo().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getDiCardNo());
            }
            if(this.get(i).getDiType() == null || this.get(i).getDiType().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getDiType());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMWhiteListDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
