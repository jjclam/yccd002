/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LDBlacklistDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LDBlacklistSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LDBlacklistSchema implements Schema, Cloneable {
    // @Field
    /** 序列号 */
    private String SerialNo;
    /** 黑名单客户号码 */
    private String BlacklistNo;
    /** 黑名单客户类型 */
    private String BlacklistType;
    /** 黑名单名称 */
    private String BlackName;
    /** 证件类型 */
    private String IDType;
    /** 证件号码 */
    private String IDNo;
    /** 出生日期 */
    private Date BirthDate;
    /** 性别 */
    private String Sex;
    /** 黑名单操作员 */
    private String BlacklistOperator;
    /** 黑名单日期 */
    private Date BlacklistMakeDate;
    /** 黑名单时间 */
    private String BlacklistMakeTime;
    /** 黑名单原因描述 */
    private String BlacklistReason;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 备用字段1 */
    private String StandByFlag1;
    /** 备用字段2 */
    private String StandByFlag2;
    /** 备用字段3 */
    private String StandByFlag3;
    /** 备用字段4 */
    private String StandByFlag4;
    /** 备用字段5 */
    private String StandByFlag5;

    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDBlacklistSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDBlacklistSchema cloned = (LDBlacklistSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getBlacklistNo() {
        return BlacklistNo;
    }
    public void setBlacklistNo(String aBlacklistNo) {
        BlacklistNo = aBlacklistNo;
    }
    public String getBlacklistType() {
        return BlacklistType;
    }
    public void setBlacklistType(String aBlacklistType) {
        BlacklistType = aBlacklistType;
    }
    public String getBlackName() {
        return BlackName;
    }
    public void setBlackName(String aBlackName) {
        BlackName = aBlackName;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getBirthDate() {
        if(BirthDate != null) {
            return fDate.getString(BirthDate);
        } else {
            return null;
        }
    }
    public void setBirthDate(Date aBirthDate) {
        BirthDate = aBirthDate;
    }
    public void setBirthDate(String aBirthDate) {
        if (aBirthDate != null && !aBirthDate.equals("")) {
            BirthDate = fDate.getDate(aBirthDate);
        } else
            BirthDate = null;
    }

    public String getSex() {
        return Sex;
    }
    public void setSex(String aSex) {
        Sex = aSex;
    }
    public String getBlacklistOperator() {
        return BlacklistOperator;
    }
    public void setBlacklistOperator(String aBlacklistOperator) {
        BlacklistOperator = aBlacklistOperator;
    }
    public String getBlacklistMakeDate() {
        if(BlacklistMakeDate != null) {
            return fDate.getString(BlacklistMakeDate);
        } else {
            return null;
        }
    }
    public void setBlacklistMakeDate(Date aBlacklistMakeDate) {
        BlacklistMakeDate = aBlacklistMakeDate;
    }
    public void setBlacklistMakeDate(String aBlacklistMakeDate) {
        if (aBlacklistMakeDate != null && !aBlacklistMakeDate.equals("")) {
            BlacklistMakeDate = fDate.getDate(aBlacklistMakeDate);
        } else
            BlacklistMakeDate = null;
    }

    public String getBlacklistMakeTime() {
        return BlacklistMakeTime;
    }
    public void setBlacklistMakeTime(String aBlacklistMakeTime) {
        BlacklistMakeTime = aBlacklistMakeTime;
    }
    public String getBlacklistReason() {
        return BlacklistReason;
    }
    public void setBlacklistReason(String aBlacklistReason) {
        BlacklistReason = aBlacklistReason;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getStandByFlag2() {
        return StandByFlag2;
    }
    public void setStandByFlag2(String aStandByFlag2) {
        StandByFlag2 = aStandByFlag2;
    }
    public String getStandByFlag3() {
        return StandByFlag3;
    }
    public void setStandByFlag3(String aStandByFlag3) {
        StandByFlag3 = aStandByFlag3;
    }
    public String getStandByFlag4() {
        return StandByFlag4;
    }
    public void setStandByFlag4(String aStandByFlag4) {
        StandByFlag4 = aStandByFlag4;
    }
    public String getStandByFlag5() {
        return StandByFlag5;
    }
    public void setStandByFlag5(String aStandByFlag5) {
        StandByFlag5 = aStandByFlag5;
    }

    /**
    * 使用另外一个 LDBlacklistSchema 对象给 Schema 赋值
    * @param: aLDBlacklistSchema LDBlacklistSchema
    **/
    public void setSchema(LDBlacklistSchema aLDBlacklistSchema) {
        this.SerialNo = aLDBlacklistSchema.getSerialNo();
        this.BlacklistNo = aLDBlacklistSchema.getBlacklistNo();
        this.BlacklistType = aLDBlacklistSchema.getBlacklistType();
        this.BlackName = aLDBlacklistSchema.getBlackName();
        this.IDType = aLDBlacklistSchema.getIDType();
        this.IDNo = aLDBlacklistSchema.getIDNo();
        this.BirthDate = fDate.getDate( aLDBlacklistSchema.getBirthDate());
        this.Sex = aLDBlacklistSchema.getSex();
        this.BlacklistOperator = aLDBlacklistSchema.getBlacklistOperator();
        this.BlacklistMakeDate = fDate.getDate( aLDBlacklistSchema.getBlacklistMakeDate());
        this.BlacklistMakeTime = aLDBlacklistSchema.getBlacklistMakeTime();
        this.BlacklistReason = aLDBlacklistSchema.getBlacklistReason();
        this.MakeDate = fDate.getDate( aLDBlacklistSchema.getMakeDate());
        this.MakeTime = aLDBlacklistSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLDBlacklistSchema.getModifyDate());
        this.ModifyTime = aLDBlacklistSchema.getModifyTime();
        this.StandByFlag1 = aLDBlacklistSchema.getStandByFlag1();
        this.StandByFlag2 = aLDBlacklistSchema.getStandByFlag2();
        this.StandByFlag3 = aLDBlacklistSchema.getStandByFlag3();
        this.StandByFlag4 = aLDBlacklistSchema.getStandByFlag4();
        this.StandByFlag5 = aLDBlacklistSchema.getStandByFlag5();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("BlacklistNo") == null )
                this.BlacklistNo = null;
            else
                this.BlacklistNo = rs.getString("BlacklistNo").trim();

            if( rs.getString("BlacklistType") == null )
                this.BlacklistType = null;
            else
                this.BlacklistType = rs.getString("BlacklistType").trim();

            if( rs.getString("BlackName") == null )
                this.BlackName = null;
            else
                this.BlackName = rs.getString("BlackName").trim();

            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            this.BirthDate = rs.getDate("BirthDate");
            if( rs.getString("Sex") == null )
                this.Sex = null;
            else
                this.Sex = rs.getString("Sex").trim();

            if( rs.getString("BlacklistOperator") == null )
                this.BlacklistOperator = null;
            else
                this.BlacklistOperator = rs.getString("BlacklistOperator").trim();

            this.BlacklistMakeDate = rs.getDate("BlacklistMakeDate");
            if( rs.getString("BlacklistMakeTime") == null )
                this.BlacklistMakeTime = null;
            else
                this.BlacklistMakeTime = rs.getString("BlacklistMakeTime").trim();

            if( rs.getString("BlacklistReason") == null )
                this.BlacklistReason = null;
            else
                this.BlacklistReason = rs.getString("BlacklistReason").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("StandByFlag1") == null )
                this.StandByFlag1 = null;
            else
                this.StandByFlag1 = rs.getString("StandByFlag1").trim();

            if( rs.getString("StandByFlag2") == null )
                this.StandByFlag2 = null;
            else
                this.StandByFlag2 = rs.getString("StandByFlag2").trim();

            if( rs.getString("StandByFlag3") == null )
                this.StandByFlag3 = null;
            else
                this.StandByFlag3 = rs.getString("StandByFlag3").trim();

            if( rs.getString("StandByFlag4") == null )
                this.StandByFlag4 = null;
            else
                this.StandByFlag4 = rs.getString("StandByFlag4").trim();

            if( rs.getString("StandByFlag5") == null )
                this.StandByFlag5 = null;
            else
                this.StandByFlag5 = rs.getString("StandByFlag5").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBlacklistSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDBlacklistSchema getSchema() {
        LDBlacklistSchema aLDBlacklistSchema = new LDBlacklistSchema();
        aLDBlacklistSchema.setSchema(this);
        return aLDBlacklistSchema;
    }

    public LDBlacklistDB getDB() {
        LDBlacklistDB aDBOper = new LDBlacklistDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBlacklist描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlacklistNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlacklistType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlackName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( BirthDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlacklistOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( BlacklistMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlacklistMakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlacklistReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag5));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBlacklist>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            BlacklistNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            BlacklistType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BlackName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            BirthDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER));
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            BlacklistOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            BlacklistMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER));
            BlacklistMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            BlacklistReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            StandByFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            StandByFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            StandByFlag4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            StandByFlag5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBlacklistSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("BlacklistNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistNo));
        }
        if (FCode.equalsIgnoreCase("BlacklistType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistType));
        }
        if (FCode.equalsIgnoreCase("BlackName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackName));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("BirthDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthDate()));
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equalsIgnoreCase("BlacklistOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistOperator));
        }
        if (FCode.equalsIgnoreCase("BlacklistMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBlacklistMakeDate()));
        }
        if (FCode.equalsIgnoreCase("BlacklistMakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistMakeTime));
        }
        if (FCode.equalsIgnoreCase("BlacklistReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistReason));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag3));
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag4));
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag5));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BlacklistNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BlacklistType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BlackName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(BlacklistOperator);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBlacklistMakeDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(BlacklistMakeTime);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(BlacklistReason);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag2);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag3);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag4);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag5);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistNo = FValue.trim();
            }
            else
                BlacklistNo = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistType = FValue.trim();
            }
            else
                BlacklistType = null;
        }
        if (FCode.equalsIgnoreCase("BlackName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackName = FValue.trim();
            }
            else
                BlackName = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("BirthDate")) {
            if(FValue != null && !FValue.equals("")) {
                BirthDate = fDate.getDate( FValue );
            }
            else
                BirthDate = null;
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
                Sex = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistOperator = FValue.trim();
            }
            else
                BlacklistOperator = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistMakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                BlacklistMakeDate = fDate.getDate( FValue );
            }
            else
                BlacklistMakeDate = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistMakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistMakeTime = FValue.trim();
            }
            else
                BlacklistMakeTime = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistReason = FValue.trim();
            }
            else
                BlacklistReason = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag2 = FValue.trim();
            }
            else
                StandByFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag3 = FValue.trim();
            }
            else
                StandByFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag4 = FValue.trim();
            }
            else
                StandByFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag5 = FValue.trim();
            }
            else
                StandByFlag5 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDBlacklistSchema other = (LDBlacklistSchema)otherObject;
        return
            SerialNo.equals(other.getSerialNo())
            && BlacklistNo.equals(other.getBlacklistNo())
            && BlacklistType.equals(other.getBlacklistType())
            && BlackName.equals(other.getBlackName())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && fDate.getString(BirthDate).equals(other.getBirthDate())
            && Sex.equals(other.getSex())
            && BlacklistOperator.equals(other.getBlacklistOperator())
            && fDate.getString(BlacklistMakeDate).equals(other.getBlacklistMakeDate())
            && BlacklistMakeTime.equals(other.getBlacklistMakeTime())
            && BlacklistReason.equals(other.getBlacklistReason())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && StandByFlag1.equals(other.getStandByFlag1())
            && StandByFlag2.equals(other.getStandByFlag2())
            && StandByFlag3.equals(other.getStandByFlag3())
            && StandByFlag4.equals(other.getStandByFlag4())
            && StandByFlag5.equals(other.getStandByFlag5());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("BlacklistNo") ) {
            return 1;
        }
        if( strFieldName.equals("BlacklistType") ) {
            return 2;
        }
        if( strFieldName.equals("BlackName") ) {
            return 3;
        }
        if( strFieldName.equals("IDType") ) {
            return 4;
        }
        if( strFieldName.equals("IDNo") ) {
            return 5;
        }
        if( strFieldName.equals("BirthDate") ) {
            return 6;
        }
        if( strFieldName.equals("Sex") ) {
            return 7;
        }
        if( strFieldName.equals("BlacklistOperator") ) {
            return 8;
        }
        if( strFieldName.equals("BlacklistMakeDate") ) {
            return 9;
        }
        if( strFieldName.equals("BlacklistMakeTime") ) {
            return 10;
        }
        if( strFieldName.equals("BlacklistReason") ) {
            return 11;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 12;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 13;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 15;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 16;
        }
        if( strFieldName.equals("StandByFlag2") ) {
            return 17;
        }
        if( strFieldName.equals("StandByFlag3") ) {
            return 18;
        }
        if( strFieldName.equals("StandByFlag4") ) {
            return 19;
        }
        if( strFieldName.equals("StandByFlag5") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "BlacklistNo";
                break;
            case 2:
                strFieldName = "BlacklistType";
                break;
            case 3:
                strFieldName = "BlackName";
                break;
            case 4:
                strFieldName = "IDType";
                break;
            case 5:
                strFieldName = "IDNo";
                break;
            case 6:
                strFieldName = "BirthDate";
                break;
            case 7:
                strFieldName = "Sex";
                break;
            case 8:
                strFieldName = "BlacklistOperator";
                break;
            case 9:
                strFieldName = "BlacklistMakeDate";
                break;
            case 10:
                strFieldName = "BlacklistMakeTime";
                break;
            case 11:
                strFieldName = "BlacklistReason";
                break;
            case 12:
                strFieldName = "MakeDate";
                break;
            case 13:
                strFieldName = "MakeTime";
                break;
            case 14:
                strFieldName = "ModifyDate";
                break;
            case 15:
                strFieldName = "ModifyTime";
                break;
            case 16:
                strFieldName = "StandByFlag1";
                break;
            case 17:
                strFieldName = "StandByFlag2";
                break;
            case 18:
                strFieldName = "StandByFlag3";
                break;
            case 19:
                strFieldName = "StandByFlag4";
                break;
            case 20:
                strFieldName = "StandByFlag5";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTTYPE":
                return Schema.TYPE_STRING;
            case "BLACKNAME":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "BIRTHDATE":
                return Schema.TYPE_DATE;
            case "SEX":
                return Schema.TYPE_STRING;
            case "BLACKLISTOPERATOR":
                return Schema.TYPE_STRING;
            case "BLACKLISTMAKEDATE":
                return Schema.TYPE_DATE;
            case "BLACKLISTMAKETIME":
                return Schema.TYPE_STRING;
            case "BLACKLISTREASON":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_DATE;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_DATE;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DATE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
