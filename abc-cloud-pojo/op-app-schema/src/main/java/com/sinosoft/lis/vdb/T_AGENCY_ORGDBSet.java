/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.T_AGENCY_ORGSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: T_AGENCY_ORGDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */
public class T_AGENCY_ORGDBSet extends T_AGENCY_ORGSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public T_AGENCY_ORGDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"T_AGENCY_ORG");
        mflag = true;
    }

    public T_AGENCY_ORGDBSet() {
        db = new DBOper( "T_AGENCY_ORG" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM T_AGENCY_ORG WHERE  1=1  AND AGENCY_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getAGENCY_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE T_AGENCY_ORG SET  AGENCY_ID = ? , MNGORG_ID = ? , AGENCY_CODE = ? , AGENCY_NAME = ? , AGENCY_ABBR = ? , EXTER_ANNOUNCE_ORGCODE = ? , SUPERIOR_ORG_ID = ? , AGENCY_LEVEL = ? , AGENCY_REGION_TYPE = ? , AGENCY_STATUS = ? , AGENCY_TYPE = ? , CORRESP_CORE_CODE = ? , LINKMAN_NAME = ? , FLG = ? , SUPERVISOR_NAME = ? , INSERT_OPER = ? , INSERT_CONSIGNOR = ? , INSERT_TIME = ? , UPDATE_OPER = ? , UPDATE_CONSIGNOR = ? , UPDATE_TIME = ? , BANK_CODE = ? , AGENCY_ADDR = ? , MNGORG_CODE = ? WHERE  1=1  AND AGENCY_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getAGENCY_ID());
            pstmt.setLong(2, this.get(i).getMNGORG_ID());
            if(this.get(i).getAGENCY_CODE() == null || this.get(i).getAGENCY_CODE().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getAGENCY_CODE());
            }
            if(this.get(i).getAGENCY_NAME() == null || this.get(i).getAGENCY_NAME().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAGENCY_NAME());
            }
            if(this.get(i).getAGENCY_ABBR() == null || this.get(i).getAGENCY_ABBR().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAGENCY_ABBR());
            }
            if(this.get(i).getEXTER_ANNOUNCE_ORGCODE() == null || this.get(i).getEXTER_ANNOUNCE_ORGCODE().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getEXTER_ANNOUNCE_ORGCODE());
            }
            pstmt.setLong(7, this.get(i).getSUPERIOR_ORG_ID());
            if(this.get(i).getAGENCY_LEVEL() == null || this.get(i).getAGENCY_LEVEL().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getAGENCY_LEVEL());
            }
            if(this.get(i).getAGENCY_REGION_TYPE() == null || this.get(i).getAGENCY_REGION_TYPE().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getAGENCY_REGION_TYPE());
            }
            if(this.get(i).getAGENCY_STATUS() == null || this.get(i).getAGENCY_STATUS().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAGENCY_STATUS());
            }
            if(this.get(i).getAGENCY_TYPE() == null || this.get(i).getAGENCY_TYPE().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAGENCY_TYPE());
            }
            if(this.get(i).getCORRESP_CORE_CODE() == null || this.get(i).getCORRESP_CORE_CODE().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getCORRESP_CORE_CODE());
            }
            if(this.get(i).getLINKMAN_NAME() == null || this.get(i).getLINKMAN_NAME().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getLINKMAN_NAME());
            }
            if(this.get(i).getFLG() == null || this.get(i).getFLG().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getFLG());
            }
            if(this.get(i).getSUPERVISOR_NAME() == null || this.get(i).getSUPERVISOR_NAME().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getSUPERVISOR_NAME());
            }
            if(this.get(i).getINSERT_OPER() == null || this.get(i).getINSERT_OPER().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getINSERT_OPER());
            }
            if(this.get(i).getINSERT_CONSIGNOR() == null || this.get(i).getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getINSERT_CONSIGNOR());
            }
            if(this.get(i).getINSERT_TIME() == null || this.get(i).getINSERT_TIME().equals("null")) {
                pstmt.setDate(18,null);
            } else {
                pstmt.setDate(18, Date.valueOf(this.get(i).getINSERT_TIME()));
            }
            if(this.get(i).getUPDATE_OPER() == null || this.get(i).getUPDATE_OPER().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getUPDATE_OPER());
            }
            if(this.get(i).getUPDATE_CONSIGNOR() == null || this.get(i).getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getUPDATE_CONSIGNOR());
            }
            if(this.get(i).getUPDATE_TIME() == null || this.get(i).getUPDATE_TIME().equals("null")) {
                pstmt.setDate(21,null);
            } else {
                pstmt.setDate(21, Date.valueOf(this.get(i).getUPDATE_TIME()));
            }
            if(this.get(i).getBANK_CODE() == null || this.get(i).getBANK_CODE().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getBANK_CODE());
            }
            if(this.get(i).getAGENCY_ADDR() == null || this.get(i).getAGENCY_ADDR().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getAGENCY_ADDR());
            }
            if(this.get(i).getMNGORG_CODE() == null || this.get(i).getMNGORG_CODE().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getMNGORG_CODE());
            }
            // set where condition
            pstmt.setLong(25, this.get(i).getAGENCY_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO T_AGENCY_ORG VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getAGENCY_ID());
            pstmt.setLong(2, this.get(i).getMNGORG_ID());
            if(this.get(i).getAGENCY_CODE() == null || this.get(i).getAGENCY_CODE().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getAGENCY_CODE());
            }
            if(this.get(i).getAGENCY_NAME() == null || this.get(i).getAGENCY_NAME().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAGENCY_NAME());
            }
            if(this.get(i).getAGENCY_ABBR() == null || this.get(i).getAGENCY_ABBR().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getAGENCY_ABBR());
            }
            if(this.get(i).getEXTER_ANNOUNCE_ORGCODE() == null || this.get(i).getEXTER_ANNOUNCE_ORGCODE().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getEXTER_ANNOUNCE_ORGCODE());
            }
            pstmt.setLong(7, this.get(i).getSUPERIOR_ORG_ID());
            if(this.get(i).getAGENCY_LEVEL() == null || this.get(i).getAGENCY_LEVEL().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getAGENCY_LEVEL());
            }
            if(this.get(i).getAGENCY_REGION_TYPE() == null || this.get(i).getAGENCY_REGION_TYPE().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getAGENCY_REGION_TYPE());
            }
            if(this.get(i).getAGENCY_STATUS() == null || this.get(i).getAGENCY_STATUS().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getAGENCY_STATUS());
            }
            if(this.get(i).getAGENCY_TYPE() == null || this.get(i).getAGENCY_TYPE().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getAGENCY_TYPE());
            }
            if(this.get(i).getCORRESP_CORE_CODE() == null || this.get(i).getCORRESP_CORE_CODE().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getCORRESP_CORE_CODE());
            }
            if(this.get(i).getLINKMAN_NAME() == null || this.get(i).getLINKMAN_NAME().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getLINKMAN_NAME());
            }
            if(this.get(i).getFLG() == null || this.get(i).getFLG().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getFLG());
            }
            if(this.get(i).getSUPERVISOR_NAME() == null || this.get(i).getSUPERVISOR_NAME().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getSUPERVISOR_NAME());
            }
            if(this.get(i).getINSERT_OPER() == null || this.get(i).getINSERT_OPER().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getINSERT_OPER());
            }
            if(this.get(i).getINSERT_CONSIGNOR() == null || this.get(i).getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getINSERT_CONSIGNOR());
            }
            if(this.get(i).getINSERT_TIME() == null || this.get(i).getINSERT_TIME().equals("null")) {
                pstmt.setDate(18,null);
            } else {
                pstmt.setDate(18, Date.valueOf(this.get(i).getINSERT_TIME()));
            }
            if(this.get(i).getUPDATE_OPER() == null || this.get(i).getUPDATE_OPER().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getUPDATE_OPER());
            }
            if(this.get(i).getUPDATE_CONSIGNOR() == null || this.get(i).getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getUPDATE_CONSIGNOR());
            }
            if(this.get(i).getUPDATE_TIME() == null || this.get(i).getUPDATE_TIME().equals("null")) {
                pstmt.setDate(21,null);
            } else {
                pstmt.setDate(21, Date.valueOf(this.get(i).getUPDATE_TIME()));
            }
            if(this.get(i).getBANK_CODE() == null || this.get(i).getBANK_CODE().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getBANK_CODE());
            }
            if(this.get(i).getAGENCY_ADDR() == null || this.get(i).getAGENCY_ADDR().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getAGENCY_ADDR());
            }
            if(this.get(i).getMNGORG_CODE() == null || this.get(i).getMNGORG_CODE().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getMNGORG_CODE());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
