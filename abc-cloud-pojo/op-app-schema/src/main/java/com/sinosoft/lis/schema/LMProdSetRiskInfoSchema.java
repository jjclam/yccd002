/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMProdSetRiskInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LMProdSetRiskInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMProdSetRiskInfoSchema implements Schema, Cloneable {
    // @Field
    /** 产品组合编码 */
    private String ProdSetCode;
    /** 产品组合名称 */
    private String ProdSetName;
    /** 险种编码 */
    private String RiskCode;
    /** 险种名称 */
    private String RiskName;
    /** 要素类型 */
    private String FactorType;
    /** 要素值 */
    private double FactorValue;
    /** 主险标记 */
    private String MainRiskFlag;
    /** 主险编码 */
    private String MainRiskCode;
    /** 主险要素类型 */
    private String MainRiskFactorType;
    /** 主险要素值 */
    private double MainRiskFactorValue;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 备用字段1 */
    private String StandByFlag1;
    /** 备用字段2 */
    private String StandByFlag2;
    /** 备用字段3 */
    private Date StandByFlag3;
    /** 备用字段4 */
    private Date StandByFlag4;
    /** 备用字段5 */
    private double StandByFlag5;
    /** 备用字段6 */
    private double StandByFlag6;
    /** 保险年龄年期 */
    private int InsuYear;
    /** 保险年龄年期标志 */
    private String InsuYearFlag;
    /** 组合明细序号 */
    private String ProdSetInfoID;
    /** 销售渠道 */
    private String SaleChnl;
    /** 组合险序号 */
    private String ProdSetID;

    public static final int FIELDNUM = 26;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMProdSetRiskInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ProdSetInfoID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMProdSetRiskInfoSchema cloned = (LMProdSetRiskInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getProdSetCode() {
        return ProdSetCode;
    }
    public void setProdSetCode(String aProdSetCode) {
        ProdSetCode = aProdSetCode;
    }
    public String getProdSetName() {
        return ProdSetName;
    }
    public void setProdSetName(String aProdSetName) {
        ProdSetName = aProdSetName;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getFactorType() {
        return FactorType;
    }
    public void setFactorType(String aFactorType) {
        FactorType = aFactorType;
    }
    public double getFactorValue() {
        return FactorValue;
    }
    public void setFactorValue(double aFactorValue) {
        FactorValue = aFactorValue;
    }
    public void setFactorValue(String aFactorValue) {
        if (aFactorValue != null && !aFactorValue.equals("")) {
            Double tDouble = new Double(aFactorValue);
            double d = tDouble.doubleValue();
            FactorValue = d;
        }
    }

    public String getMainRiskFlag() {
        return MainRiskFlag;
    }
    public void setMainRiskFlag(String aMainRiskFlag) {
        MainRiskFlag = aMainRiskFlag;
    }
    public String getMainRiskCode() {
        return MainRiskCode;
    }
    public void setMainRiskCode(String aMainRiskCode) {
        MainRiskCode = aMainRiskCode;
    }
    public String getMainRiskFactorType() {
        return MainRiskFactorType;
    }
    public void setMainRiskFactorType(String aMainRiskFactorType) {
        MainRiskFactorType = aMainRiskFactorType;
    }
    public double getMainRiskFactorValue() {
        return MainRiskFactorValue;
    }
    public void setMainRiskFactorValue(double aMainRiskFactorValue) {
        MainRiskFactorValue = aMainRiskFactorValue;
    }
    public void setMainRiskFactorValue(String aMainRiskFactorValue) {
        if (aMainRiskFactorValue != null && !aMainRiskFactorValue.equals("")) {
            Double tDouble = new Double(aMainRiskFactorValue);
            double d = tDouble.doubleValue();
            MainRiskFactorValue = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getStandByFlag2() {
        return StandByFlag2;
    }
    public void setStandByFlag2(String aStandByFlag2) {
        StandByFlag2 = aStandByFlag2;
    }
    public String getStandByFlag3() {
        if(StandByFlag3 != null) {
            return fDate.getString(StandByFlag3);
        } else {
            return null;
        }
    }
    public void setStandByFlag3(Date aStandByFlag3) {
        StandByFlag3 = aStandByFlag3;
    }
    public void setStandByFlag3(String aStandByFlag3) {
        if (aStandByFlag3 != null && !aStandByFlag3.equals("")) {
            StandByFlag3 = fDate.getDate(aStandByFlag3);
        } else
            StandByFlag3 = null;
    }

    public String getStandByFlag4() {
        if(StandByFlag4 != null) {
            return fDate.getString(StandByFlag4);
        } else {
            return null;
        }
    }
    public void setStandByFlag4(Date aStandByFlag4) {
        StandByFlag4 = aStandByFlag4;
    }
    public void setStandByFlag4(String aStandByFlag4) {
        if (aStandByFlag4 != null && !aStandByFlag4.equals("")) {
            StandByFlag4 = fDate.getDate(aStandByFlag4);
        } else
            StandByFlag4 = null;
    }

    public double getStandByFlag5() {
        return StandByFlag5;
    }
    public void setStandByFlag5(double aStandByFlag5) {
        StandByFlag5 = aStandByFlag5;
    }
    public void setStandByFlag5(String aStandByFlag5) {
        if (aStandByFlag5 != null && !aStandByFlag5.equals("")) {
            Double tDouble = new Double(aStandByFlag5);
            double d = tDouble.doubleValue();
            StandByFlag5 = d;
        }
    }

    public double getStandByFlag6() {
        return StandByFlag6;
    }
    public void setStandByFlag6(double aStandByFlag6) {
        StandByFlag6 = aStandByFlag6;
    }
    public void setStandByFlag6(String aStandByFlag6) {
        if (aStandByFlag6 != null && !aStandByFlag6.equals("")) {
            Double tDouble = new Double(aStandByFlag6);
            double d = tDouble.doubleValue();
            StandByFlag6 = d;
        }
    }

    public int getInsuYear() {
        return InsuYear;
    }
    public void setInsuYear(int aInsuYear) {
        InsuYear = aInsuYear;
    }
    public void setInsuYear(String aInsuYear) {
        if (aInsuYear != null && !aInsuYear.equals("")) {
            Integer tInteger = new Integer(aInsuYear);
            int i = tInteger.intValue();
            InsuYear = i;
        }
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }
    public void setInsuYearFlag(String aInsuYearFlag) {
        InsuYearFlag = aInsuYearFlag;
    }
    public String getProdSetInfoID() {
        return ProdSetInfoID;
    }
    public void setProdSetInfoID(String aProdSetInfoID) {
        ProdSetInfoID = aProdSetInfoID;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getProdSetID() {
        return ProdSetID;
    }
    public void setProdSetID(String aProdSetID) {
        ProdSetID = aProdSetID;
    }

    /**
    * 使用另外一个 LMProdSetRiskInfoSchema 对象给 Schema 赋值
    * @param: aLMProdSetRiskInfoSchema LMProdSetRiskInfoSchema
    **/
    public void setSchema(LMProdSetRiskInfoSchema aLMProdSetRiskInfoSchema) {
        this.ProdSetCode = aLMProdSetRiskInfoSchema.getProdSetCode();
        this.ProdSetName = aLMProdSetRiskInfoSchema.getProdSetName();
        this.RiskCode = aLMProdSetRiskInfoSchema.getRiskCode();
        this.RiskName = aLMProdSetRiskInfoSchema.getRiskName();
        this.FactorType = aLMProdSetRiskInfoSchema.getFactorType();
        this.FactorValue = aLMProdSetRiskInfoSchema.getFactorValue();
        this.MainRiskFlag = aLMProdSetRiskInfoSchema.getMainRiskFlag();
        this.MainRiskCode = aLMProdSetRiskInfoSchema.getMainRiskCode();
        this.MainRiskFactorType = aLMProdSetRiskInfoSchema.getMainRiskFactorType();
        this.MainRiskFactorValue = aLMProdSetRiskInfoSchema.getMainRiskFactorValue();
        this.Operator = aLMProdSetRiskInfoSchema.getOperator();
        this.MakeDate = fDate.getDate( aLMProdSetRiskInfoSchema.getMakeDate());
        this.MakeTime = aLMProdSetRiskInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLMProdSetRiskInfoSchema.getModifyDate());
        this.ModifyTime = aLMProdSetRiskInfoSchema.getModifyTime();
        this.StandByFlag1 = aLMProdSetRiskInfoSchema.getStandByFlag1();
        this.StandByFlag2 = aLMProdSetRiskInfoSchema.getStandByFlag2();
        this.StandByFlag3 = fDate.getDate( aLMProdSetRiskInfoSchema.getStandByFlag3());
        this.StandByFlag4 = fDate.getDate( aLMProdSetRiskInfoSchema.getStandByFlag4());
        this.StandByFlag5 = aLMProdSetRiskInfoSchema.getStandByFlag5();
        this.StandByFlag6 = aLMProdSetRiskInfoSchema.getStandByFlag6();
        this.InsuYear = aLMProdSetRiskInfoSchema.getInsuYear();
        this.InsuYearFlag = aLMProdSetRiskInfoSchema.getInsuYearFlag();
        this.ProdSetInfoID = aLMProdSetRiskInfoSchema.getProdSetInfoID();
        this.SaleChnl = aLMProdSetRiskInfoSchema.getSaleChnl();
        this.ProdSetID = aLMProdSetRiskInfoSchema.getProdSetID();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ProdSetCode") == null )
                this.ProdSetCode = null;
            else
                this.ProdSetCode = rs.getString("ProdSetCode").trim();

            if( rs.getString("ProdSetName") == null )
                this.ProdSetName = null;
            else
                this.ProdSetName = rs.getString("ProdSetName").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("RiskName") == null )
                this.RiskName = null;
            else
                this.RiskName = rs.getString("RiskName").trim();

            if( rs.getString("FactorType") == null )
                this.FactorType = null;
            else
                this.FactorType = rs.getString("FactorType").trim();

            this.FactorValue = rs.getDouble("FactorValue");
            if( rs.getString("MainRiskFlag") == null )
                this.MainRiskFlag = null;
            else
                this.MainRiskFlag = rs.getString("MainRiskFlag").trim();

            if( rs.getString("MainRiskCode") == null )
                this.MainRiskCode = null;
            else
                this.MainRiskCode = rs.getString("MainRiskCode").trim();

            if( rs.getString("MainRiskFactorType") == null )
                this.MainRiskFactorType = null;
            else
                this.MainRiskFactorType = rs.getString("MainRiskFactorType").trim();

            this.MainRiskFactorValue = rs.getDouble("MainRiskFactorValue");
            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("StandByFlag1") == null )
                this.StandByFlag1 = null;
            else
                this.StandByFlag1 = rs.getString("StandByFlag1").trim();

            if( rs.getString("StandByFlag2") == null )
                this.StandByFlag2 = null;
            else
                this.StandByFlag2 = rs.getString("StandByFlag2").trim();

            this.StandByFlag3 = rs.getDate("StandByFlag3");
            this.StandByFlag4 = rs.getDate("StandByFlag4");
            this.StandByFlag5 = rs.getDouble("StandByFlag5");
            this.StandByFlag6 = rs.getDouble("StandByFlag6");
            this.InsuYear = rs.getInt("InsuYear");
            if( rs.getString("InsuYearFlag") == null )
                this.InsuYearFlag = null;
            else
                this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

            if( rs.getString("ProdSetInfoID") == null )
                this.ProdSetInfoID = null;
            else
                this.ProdSetInfoID = rs.getString("ProdSetInfoID").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("ProdSetID") == null )
                this.ProdSetID = null;
            else
                this.ProdSetID = rs.getString("ProdSetID").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMProdSetRiskInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMProdSetRiskInfoSchema getSchema() {
        LMProdSetRiskInfoSchema aLMProdSetRiskInfoSchema = new LMProdSetRiskInfoSchema();
        aLMProdSetRiskInfoSchema.setSchema(this);
        return aLMProdSetRiskInfoSchema;
    }

    public LMProdSetRiskInfoDB getDB() {
        LMProdSetRiskInfoDB aDBOper = new LMProdSetRiskInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMProdSetRiskInfo描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ProdSetCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FactorType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FactorValue));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainRiskFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainRiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainRiskFactorType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MainRiskFactorValue));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StandByFlag3 ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StandByFlag4 ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandByFlag5));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandByFlag6));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetInfoID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProdSetID));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMProdSetRiskInfo>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ProdSetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            ProdSetName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            FactorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            FactorValue = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6, SysConst.PACKAGESPILTER))).doubleValue();
            MainRiskFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            MainRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            MainRiskFactorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            MainRiskFactorValue = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            StandByFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            StandByFlag3 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            StandByFlag4 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER));
            StandByFlag5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).doubleValue();
            StandByFlag6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).doubleValue();
            InsuYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22, SysConst.PACKAGESPILTER))).intValue();
            InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            ProdSetInfoID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            ProdSetID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMProdSetRiskInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetCode));
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetName));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("FactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorType));
        }
        if (FCode.equalsIgnoreCase("FactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorValue));
        }
        if (FCode.equalsIgnoreCase("MainRiskFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskFlag));
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskCode));
        }
        if (FCode.equalsIgnoreCase("MainRiskFactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskFactorType));
        }
        if (FCode.equalsIgnoreCase("MainRiskFactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskFactorValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByFlag3()));
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByFlag4()));
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag5));
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag6));
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
        }
        if (FCode.equalsIgnoreCase("ProdSetInfoID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetInfoID));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProdSetID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ProdSetCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProdSetName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(FactorType);
                break;
            case 5:
                strFieldValue = String.valueOf(FactorValue);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MainRiskFlag);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MainRiskCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MainRiskFactorType);
                break;
            case 9:
                strFieldValue = String.valueOf(MainRiskFactorValue);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag2);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByFlag3()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByFlag4()));
                break;
            case 19:
                strFieldValue = String.valueOf(StandByFlag5);
                break;
            case 20:
                strFieldValue = String.valueOf(StandByFlag6);
                break;
            case 21:
                strFieldValue = String.valueOf(InsuYear);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ProdSetInfoID);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ProdSetID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ProdSetCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetCode = FValue.trim();
            }
            else
                ProdSetCode = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetName = FValue.trim();
            }
            else
                ProdSetName = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("FactorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                FactorType = FValue.trim();
            }
            else
                FactorType = null;
        }
        if (FCode.equalsIgnoreCase("FactorValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FactorValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("MainRiskFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskFlag = FValue.trim();
            }
            else
                MainRiskFlag = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskCode = FValue.trim();
            }
            else
                MainRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskFactorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskFactorType = FValue.trim();
            }
            else
                MainRiskFactorType = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskFactorValue")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MainRiskFactorValue = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag2 = FValue.trim();
            }
            else
                StandByFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            if(FValue != null && !FValue.equals("")) {
                StandByFlag3 = fDate.getDate( FValue );
            }
            else
                StandByFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            if(FValue != null && !FValue.equals("")) {
                StandByFlag4 = fDate.getDate( FValue );
            }
            else
                StandByFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandByFlag5 = d;
            }
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                StandByFlag6 = d;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYear")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsuYear = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsuYearFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuYearFlag = FValue.trim();
            }
            else
                InsuYearFlag = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetInfoID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetInfoID = FValue.trim();
            }
            else
                ProdSetInfoID = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ProdSetID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProdSetID = FValue.trim();
            }
            else
                ProdSetID = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMProdSetRiskInfoSchema other = (LMProdSetRiskInfoSchema)otherObject;
        return
            ProdSetCode.equals(other.getProdSetCode())
            && ProdSetName.equals(other.getProdSetName())
            && RiskCode.equals(other.getRiskCode())
            && RiskName.equals(other.getRiskName())
            && FactorType.equals(other.getFactorType())
            && FactorValue == other.getFactorValue()
            && MainRiskFlag.equals(other.getMainRiskFlag())
            && MainRiskCode.equals(other.getMainRiskCode())
            && MainRiskFactorType.equals(other.getMainRiskFactorType())
            && MainRiskFactorValue == other.getMainRiskFactorValue()
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && StandByFlag1.equals(other.getStandByFlag1())
            && StandByFlag2.equals(other.getStandByFlag2())
            && fDate.getString(StandByFlag3).equals(other.getStandByFlag3())
            && fDate.getString(StandByFlag4).equals(other.getStandByFlag4())
            && StandByFlag5 == other.getStandByFlag5()
            && StandByFlag6 == other.getStandByFlag6()
            && InsuYear == other.getInsuYear()
            && InsuYearFlag.equals(other.getInsuYearFlag())
            && ProdSetInfoID.equals(other.getProdSetInfoID())
            && SaleChnl.equals(other.getSaleChnl())
            && ProdSetID.equals(other.getProdSetID());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ProdSetCode") ) {
            return 0;
        }
        if( strFieldName.equals("ProdSetName") ) {
            return 1;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 2;
        }
        if( strFieldName.equals("RiskName") ) {
            return 3;
        }
        if( strFieldName.equals("FactorType") ) {
            return 4;
        }
        if( strFieldName.equals("FactorValue") ) {
            return 5;
        }
        if( strFieldName.equals("MainRiskFlag") ) {
            return 6;
        }
        if( strFieldName.equals("MainRiskCode") ) {
            return 7;
        }
        if( strFieldName.equals("MainRiskFactorType") ) {
            return 8;
        }
        if( strFieldName.equals("MainRiskFactorValue") ) {
            return 9;
        }
        if( strFieldName.equals("Operator") ) {
            return 10;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 11;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 12;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 13;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 14;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 15;
        }
        if( strFieldName.equals("StandByFlag2") ) {
            return 16;
        }
        if( strFieldName.equals("StandByFlag3") ) {
            return 17;
        }
        if( strFieldName.equals("StandByFlag4") ) {
            return 18;
        }
        if( strFieldName.equals("StandByFlag5") ) {
            return 19;
        }
        if( strFieldName.equals("StandByFlag6") ) {
            return 20;
        }
        if( strFieldName.equals("InsuYear") ) {
            return 21;
        }
        if( strFieldName.equals("InsuYearFlag") ) {
            return 22;
        }
        if( strFieldName.equals("ProdSetInfoID") ) {
            return 23;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 24;
        }
        if( strFieldName.equals("ProdSetID") ) {
            return 25;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ProdSetCode";
                break;
            case 1:
                strFieldName = "ProdSetName";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "RiskName";
                break;
            case 4:
                strFieldName = "FactorType";
                break;
            case 5:
                strFieldName = "FactorValue";
                break;
            case 6:
                strFieldName = "MainRiskFlag";
                break;
            case 7:
                strFieldName = "MainRiskCode";
                break;
            case 8:
                strFieldName = "MainRiskFactorType";
                break;
            case 9:
                strFieldName = "MainRiskFactorValue";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            case 15:
                strFieldName = "StandByFlag1";
                break;
            case 16:
                strFieldName = "StandByFlag2";
                break;
            case 17:
                strFieldName = "StandByFlag3";
                break;
            case 18:
                strFieldName = "StandByFlag4";
                break;
            case 19:
                strFieldName = "StandByFlag5";
                break;
            case 20:
                strFieldName = "StandByFlag6";
                break;
            case 21:
                strFieldName = "InsuYear";
                break;
            case 22:
                strFieldName = "InsuYearFlag";
                break;
            case 23:
                strFieldName = "ProdSetInfoID";
                break;
            case 24:
                strFieldName = "SaleChnl";
                break;
            case 25:
                strFieldName = "ProdSetID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "PRODSETCODE":
                return Schema.TYPE_STRING;
            case "PRODSETNAME":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "FACTORTYPE":
                return Schema.TYPE_STRING;
            case "FACTORVALUE":
                return Schema.TYPE_DOUBLE;
            case "MAINRISKFLAG":
                return Schema.TYPE_STRING;
            case "MAINRISKCODE":
                return Schema.TYPE_STRING;
            case "MAINRISKFACTORTYPE":
                return Schema.TYPE_STRING;
            case "MAINRISKFACTORVALUE":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_DATE;
            case "STANDBYFLAG4":
                return Schema.TYPE_DATE;
            case "STANDBYFLAG5":
                return Schema.TYPE_DOUBLE;
            case "STANDBYFLAG6":
                return Schema.TYPE_DOUBLE;
            case "INSUYEAR":
                return Schema.TYPE_INT;
            case "INSUYEARFLAG":
                return Schema.TYPE_STRING;
            case "PRODSETINFOID":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "PRODSETID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_DOUBLE;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DATE;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_DATE;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_INT;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
