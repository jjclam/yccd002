/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMAccRelaDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LMAccRelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMAccRelaSchema implements Schema, Cloneable {
    // @Field
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 下级保险帐户号码 */
    private String InsuAccNoLower;
    /** 关联是否有效 */
    private String ValiState;

    public static final int FIELDNUM = 3;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMAccRelaSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "InsuAccNo";
        pk[1] = "InsuAccNoLower";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMAccRelaSchema cloned = (LMAccRelaSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getInsuAccNoLower() {
        return InsuAccNoLower;
    }
    public void setInsuAccNoLower(String aInsuAccNoLower) {
        InsuAccNoLower = aInsuAccNoLower;
    }
    public String getValiState() {
        return ValiState;
    }
    public void setValiState(String aValiState) {
        ValiState = aValiState;
    }

    /**
    * 使用另外一个 LMAccRelaSchema 对象给 Schema 赋值
    * @param: aLMAccRelaSchema LMAccRelaSchema
    **/
    public void setSchema(LMAccRelaSchema aLMAccRelaSchema) {
        this.InsuAccNo = aLMAccRelaSchema.getInsuAccNo();
        this.InsuAccNoLower = aLMAccRelaSchema.getInsuAccNoLower();
        this.ValiState = aLMAccRelaSchema.getValiState();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("InsuAccNo") == null )
                this.InsuAccNo = null;
            else
                this.InsuAccNo = rs.getString("InsuAccNo").trim();

            if( rs.getString("InsuAccNoLower") == null )
                this.InsuAccNoLower = null;
            else
                this.InsuAccNoLower = rs.getString("InsuAccNoLower").trim();

            if( rs.getString("ValiState") == null )
                this.ValiState = null;
            else
                this.ValiState = rs.getString("ValiState").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMAccRelaSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMAccRelaSchema getSchema() {
        LMAccRelaSchema aLMAccRelaSchema = new LMAccRelaSchema();
        aLMAccRelaSchema.setSchema(this);
        return aLMAccRelaSchema;
    }

    public LMAccRelaDB getDB() {
        LMAccRelaDB aDBOper = new LMAccRelaDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMAccRela描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(InsuAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuAccNoLower)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ValiState));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMAccRela>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            InsuAccNoLower = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ValiState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMAccRelaSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccNoLower")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNoLower));
        }
        if (FCode.equalsIgnoreCase("ValiState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValiState));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNoLower);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ValiState);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNoLower")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNoLower = FValue.trim();
            }
            else
                InsuAccNoLower = null;
        }
        if (FCode.equalsIgnoreCase("ValiState")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValiState = FValue.trim();
            }
            else
                ValiState = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMAccRelaSchema other = (LMAccRelaSchema)otherObject;
        return
            InsuAccNo.equals(other.getInsuAccNo())
            && InsuAccNoLower.equals(other.getInsuAccNoLower())
            && ValiState.equals(other.getValiState());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsuAccNo") ) {
            return 0;
        }
        if( strFieldName.equals("InsuAccNoLower") ) {
            return 1;
        }
        if( strFieldName.equals("ValiState") ) {
            return 2;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsuAccNo";
                break;
            case 1:
                strFieldName = "InsuAccNoLower";
                break;
            case 2:
                strFieldName = "ValiState";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "INSUACCNOLOWER":
                return Schema.TYPE_STRING;
            case "VALISTATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
