/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LAAgentStateDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LAAgentStateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LAAgentStateSchema implements Schema, Cloneable {
    // @Field
    /** 代理人号 */
    private String AgentCode;
    /** 代理人职级 */
    private String AgentGrade;
    /** 管理机构 */
    private String ManageCom;
    /** 展业类型 */
    private String BranchType;
    /** 渠道类型 */
    private String BranchType2;
    /** 状态类型 */
    private String StateType;
    /** 状态值 */
    private String StateValue;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 备注1 */
    private String Note1;
    /** 备注2 */
    private String Note2;
    /** 备注3 */
    private String Note3;

    public static final int FIELDNUM = 15;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LAAgentStateSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AgentCode";
        pk[1] = "StateType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAAgentStateSchema cloned = (LAAgentStateSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getAgentGrade() {
        return AgentGrade;
    }
    public void setAgentGrade(String aAgentGrade) {
        AgentGrade = aAgentGrade;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getBranchType2() {
        return BranchType2;
    }
    public void setBranchType2(String aBranchType2) {
        BranchType2 = aBranchType2;
    }
    public String getStateType() {
        return StateType;
    }
    public void setStateType(String aStateType) {
        StateType = aStateType;
    }
    public String getStateValue() {
        return StateValue;
    }
    public void setStateValue(String aStateValue) {
        StateValue = aStateValue;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getNote1() {
        return Note1;
    }
    public void setNote1(String aNote1) {
        Note1 = aNote1;
    }
    public String getNote2() {
        return Note2;
    }
    public void setNote2(String aNote2) {
        Note2 = aNote2;
    }
    public String getNote3() {
        return Note3;
    }
    public void setNote3(String aNote3) {
        Note3 = aNote3;
    }

    /**
    * 使用另外一个 LAAgentStateSchema 对象给 Schema 赋值
    * @param: aLAAgentStateSchema LAAgentStateSchema
    **/
    public void setSchema(LAAgentStateSchema aLAAgentStateSchema) {
        this.AgentCode = aLAAgentStateSchema.getAgentCode();
        this.AgentGrade = aLAAgentStateSchema.getAgentGrade();
        this.ManageCom = aLAAgentStateSchema.getManageCom();
        this.BranchType = aLAAgentStateSchema.getBranchType();
        this.BranchType2 = aLAAgentStateSchema.getBranchType2();
        this.StateType = aLAAgentStateSchema.getStateType();
        this.StateValue = aLAAgentStateSchema.getStateValue();
        this.Operator = aLAAgentStateSchema.getOperator();
        this.MakeDate = fDate.getDate( aLAAgentStateSchema.getMakeDate());
        this.MakeTime = aLAAgentStateSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLAAgentStateSchema.getModifyDate());
        this.ModifyTime = aLAAgentStateSchema.getModifyTime();
        this.Note1 = aLAAgentStateSchema.getNote1();
        this.Note2 = aLAAgentStateSchema.getNote2();
        this.Note3 = aLAAgentStateSchema.getNote3();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("AgentGrade") == null )
                this.AgentGrade = null;
            else
                this.AgentGrade = rs.getString("AgentGrade").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("BranchType") == null )
                this.BranchType = null;
            else
                this.BranchType = rs.getString("BranchType").trim();

            if( rs.getString("BranchType2") == null )
                this.BranchType2 = null;
            else
                this.BranchType2 = rs.getString("BranchType2").trim();

            if( rs.getString("StateType") == null )
                this.StateType = null;
            else
                this.StateType = rs.getString("StateType").trim();

            if( rs.getString("StateValue") == null )
                this.StateValue = null;
            else
                this.StateValue = rs.getString("StateValue").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("Note1") == null )
                this.Note1 = null;
            else
                this.Note1 = rs.getString("Note1").trim();

            if( rs.getString("Note2") == null )
                this.Note2 = null;
            else
                this.Note2 = rs.getString("Note2").trim();

            if( rs.getString("Note3") == null )
                this.Note3 = null;
            else
                this.Note3 = rs.getString("Note3").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentStateSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LAAgentStateSchema getSchema() {
        LAAgentStateSchema aLAAgentStateSchema = new LAAgentStateSchema();
        aLAAgentStateSchema.setSchema(this);
        return aLAAgentStateSchema;
    }

    public LAAgentStateDB getDB() {
        LAAgentStateDB aDBOper = new LAAgentStateDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentState描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGrade)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateValue)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Note1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Note2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Note3));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentState>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            StateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            StateValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            Note1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            Note2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            Note3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentStateSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentGrade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateType));
        }
        if (FCode.equalsIgnoreCase("StateValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Note1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Note1));
        }
        if (FCode.equalsIgnoreCase("Note2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Note2));
        }
        if (FCode.equalsIgnoreCase("Note3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Note3));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(StateType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(StateValue);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Note1);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Note2);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Note3);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentGrade")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
                AgentGrade = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("BranchType2")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
                BranchType2 = null;
        }
        if (FCode.equalsIgnoreCase("StateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateType = FValue.trim();
            }
            else
                StateType = null;
        }
        if (FCode.equalsIgnoreCase("StateValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                StateValue = FValue.trim();
            }
            else
                StateValue = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Note1")) {
            if( FValue != null && !FValue.equals(""))
            {
                Note1 = FValue.trim();
            }
            else
                Note1 = null;
        }
        if (FCode.equalsIgnoreCase("Note2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Note2 = FValue.trim();
            }
            else
                Note2 = null;
        }
        if (FCode.equalsIgnoreCase("Note3")) {
            if( FValue != null && !FValue.equals(""))
            {
                Note3 = FValue.trim();
            }
            else
                Note3 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LAAgentStateSchema other = (LAAgentStateSchema)otherObject;
        return
            AgentCode.equals(other.getAgentCode())
            && AgentGrade.equals(other.getAgentGrade())
            && ManageCom.equals(other.getManageCom())
            && BranchType.equals(other.getBranchType())
            && BranchType2.equals(other.getBranchType2())
            && StateType.equals(other.getStateType())
            && StateValue.equals(other.getStateValue())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Note1.equals(other.getNote1())
            && Note2.equals(other.getNote2())
            && Note3.equals(other.getNote3());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCode") ) {
            return 0;
        }
        if( strFieldName.equals("AgentGrade") ) {
            return 1;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 2;
        }
        if( strFieldName.equals("BranchType") ) {
            return 3;
        }
        if( strFieldName.equals("BranchType2") ) {
            return 4;
        }
        if( strFieldName.equals("StateType") ) {
            return 5;
        }
        if( strFieldName.equals("StateValue") ) {
            return 6;
        }
        if( strFieldName.equals("Operator") ) {
            return 7;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 8;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 11;
        }
        if( strFieldName.equals("Note1") ) {
            return 12;
        }
        if( strFieldName.equals("Note2") ) {
            return 13;
        }
        if( strFieldName.equals("Note3") ) {
            return 14;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "AgentGrade";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "BranchType";
                break;
            case 4:
                strFieldName = "BranchType2";
                break;
            case 5:
                strFieldName = "StateType";
                break;
            case 6:
                strFieldName = "StateValue";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "Note1";
                break;
            case 13:
                strFieldName = "Note2";
                break;
            case 14:
                strFieldName = "Note3";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTGRADE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE2":
                return Schema.TYPE_STRING;
            case "STATETYPE":
                return Schema.TYPE_STRING;
            case "STATEVALUE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "NOTE1":
                return Schema.TYPE_STRING;
            case "NOTE2":
                return Schema.TYPE_STRING;
            case "NOTE3":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
