/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.T_CUST_MANAGERDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: T_CUST_MANAGERSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */
public class T_CUST_MANAGERSchema implements Schema, Cloneable {
    // @Field
    /** 客户经理id */
    private long CUST_MANAGER_ID;
    /** 客户经理编码 */
    private String CUST_MANAGER_CODE;
    /** 客户经理级别 */
    private String CUST_MANAGER_LEVEL;
    /** 有效状态 */
    private String ENABLE_STATUS;
    /** 用户id */
    private long USER_ID;
    /** 备注 */
    private String REMARK;
    /** 插入操作员 */
    private String INSERT_OPER;
    /** 插入委托人 */
    private String INSERT_CONSIGNOR;
    /** 插入时间 */
    private Date INSERT_TIME;
    /** 更新操作员 */
    private String UPDATE_OPER;
    /** 更新委托人 */
    private String UPDATE_CONSIGNOR;
    /** 更新时间 */
    private Date UPDATE_TIME;
    /** 客户经理名称 */
    private String CUST_NAME;
    /** Mngorg_id */
    private long MNGORG_ID;

    public static final int FIELDNUM = 14;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public T_CUST_MANAGERSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "CUST_MANAGER_ID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        T_CUST_MANAGERSchema cloned = (T_CUST_MANAGERSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getCUST_MANAGER_ID() {
        return CUST_MANAGER_ID;
    }
    public void setCUST_MANAGER_ID(long aCUST_MANAGER_ID) {
        CUST_MANAGER_ID = aCUST_MANAGER_ID;
    }
    public void setCUST_MANAGER_ID(String aCUST_MANAGER_ID) {
        if (aCUST_MANAGER_ID != null && !aCUST_MANAGER_ID.equals("")) {
            CUST_MANAGER_ID = new Long(aCUST_MANAGER_ID).longValue();
        }
    }

    public String getCUST_MANAGER_CODE() {
        return CUST_MANAGER_CODE;
    }
    public void setCUST_MANAGER_CODE(String aCUST_MANAGER_CODE) {
        CUST_MANAGER_CODE = aCUST_MANAGER_CODE;
    }
    public String getCUST_MANAGER_LEVEL() {
        return CUST_MANAGER_LEVEL;
    }
    public void setCUST_MANAGER_LEVEL(String aCUST_MANAGER_LEVEL) {
        CUST_MANAGER_LEVEL = aCUST_MANAGER_LEVEL;
    }
    public String getENABLE_STATUS() {
        return ENABLE_STATUS;
    }
    public void setENABLE_STATUS(String aENABLE_STATUS) {
        ENABLE_STATUS = aENABLE_STATUS;
    }
    public long getUSER_ID() {
        return USER_ID;
    }
    public void setUSER_ID(long aUSER_ID) {
        USER_ID = aUSER_ID;
    }
    public void setUSER_ID(String aUSER_ID) {
        if (aUSER_ID != null && !aUSER_ID.equals("")) {
            USER_ID = new Long(aUSER_ID).longValue();
        }
    }

    public String getREMARK() {
        return REMARK;
    }
    public void setREMARK(String aREMARK) {
        REMARK = aREMARK;
    }
    public String getINSERT_OPER() {
        return INSERT_OPER;
    }
    public void setINSERT_OPER(String aINSERT_OPER) {
        INSERT_OPER = aINSERT_OPER;
    }
    public String getINSERT_CONSIGNOR() {
        return INSERT_CONSIGNOR;
    }
    public void setINSERT_CONSIGNOR(String aINSERT_CONSIGNOR) {
        INSERT_CONSIGNOR = aINSERT_CONSIGNOR;
    }
    public String getINSERT_TIME() {
        if(INSERT_TIME != null) {
            return fDate.getString(INSERT_TIME);
        } else {
            return null;
        }
    }
    public void setINSERT_TIME(Date aINSERT_TIME) {
        INSERT_TIME = aINSERT_TIME;
    }
    public void setINSERT_TIME(String aINSERT_TIME) {
        if (aINSERT_TIME != null && !aINSERT_TIME.equals("")) {
            INSERT_TIME = fDate.getDate(aINSERT_TIME);
        } else
            INSERT_TIME = null;
    }

    public String getUPDATE_OPER() {
        return UPDATE_OPER;
    }
    public void setUPDATE_OPER(String aUPDATE_OPER) {
        UPDATE_OPER = aUPDATE_OPER;
    }
    public String getUPDATE_CONSIGNOR() {
        return UPDATE_CONSIGNOR;
    }
    public void setUPDATE_CONSIGNOR(String aUPDATE_CONSIGNOR) {
        UPDATE_CONSIGNOR = aUPDATE_CONSIGNOR;
    }
    public String getUPDATE_TIME() {
        if(UPDATE_TIME != null) {
            return fDate.getString(UPDATE_TIME);
        } else {
            return null;
        }
    }
    public void setUPDATE_TIME(Date aUPDATE_TIME) {
        UPDATE_TIME = aUPDATE_TIME;
    }
    public void setUPDATE_TIME(String aUPDATE_TIME) {
        if (aUPDATE_TIME != null && !aUPDATE_TIME.equals("")) {
            UPDATE_TIME = fDate.getDate(aUPDATE_TIME);
        } else
            UPDATE_TIME = null;
    }

    public String getCUST_NAME() {
        return CUST_NAME;
    }
    public void setCUST_NAME(String aCUST_NAME) {
        CUST_NAME = aCUST_NAME;
    }
    public long getMNGORG_ID() {
        return MNGORG_ID;
    }
    public void setMNGORG_ID(long aMNGORG_ID) {
        MNGORG_ID = aMNGORG_ID;
    }
    public void setMNGORG_ID(String aMNGORG_ID) {
        if (aMNGORG_ID != null && !aMNGORG_ID.equals("")) {
            MNGORG_ID = new Long(aMNGORG_ID).longValue();
        }
    }


    /**
    * 使用另外一个 T_CUST_MANAGERSchema 对象给 Schema 赋值
    * @param: aT_CUST_MANAGERSchema T_CUST_MANAGERSchema
    **/
    public void setSchema(T_CUST_MANAGERSchema aT_CUST_MANAGERSchema) {
        this.CUST_MANAGER_ID = aT_CUST_MANAGERSchema.getCUST_MANAGER_ID();
        this.CUST_MANAGER_CODE = aT_CUST_MANAGERSchema.getCUST_MANAGER_CODE();
        this.CUST_MANAGER_LEVEL = aT_CUST_MANAGERSchema.getCUST_MANAGER_LEVEL();
        this.ENABLE_STATUS = aT_CUST_MANAGERSchema.getENABLE_STATUS();
        this.USER_ID = aT_CUST_MANAGERSchema.getUSER_ID();
        this.REMARK = aT_CUST_MANAGERSchema.getREMARK();
        this.INSERT_OPER = aT_CUST_MANAGERSchema.getINSERT_OPER();
        this.INSERT_CONSIGNOR = aT_CUST_MANAGERSchema.getINSERT_CONSIGNOR();
        this.INSERT_TIME = fDate.getDate( aT_CUST_MANAGERSchema.getINSERT_TIME());
        this.UPDATE_OPER = aT_CUST_MANAGERSchema.getUPDATE_OPER();
        this.UPDATE_CONSIGNOR = aT_CUST_MANAGERSchema.getUPDATE_CONSIGNOR();
        this.UPDATE_TIME = fDate.getDate( aT_CUST_MANAGERSchema.getUPDATE_TIME());
        this.CUST_NAME = aT_CUST_MANAGERSchema.getCUST_NAME();
        this.MNGORG_ID = aT_CUST_MANAGERSchema.getMNGORG_ID();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.CUST_MANAGER_ID = rs.getLong("CUST_MANAGER_ID");
            if( rs.getString("CUST_MANAGER_CODE") == null )
                this.CUST_MANAGER_CODE = null;
            else
                this.CUST_MANAGER_CODE = rs.getString("CUST_MANAGER_CODE").trim();

            if( rs.getString("CUST_MANAGER_LEVEL") == null )
                this.CUST_MANAGER_LEVEL = null;
            else
                this.CUST_MANAGER_LEVEL = rs.getString("CUST_MANAGER_LEVEL").trim();

            if( rs.getString("ENABLE_STATUS") == null )
                this.ENABLE_STATUS = null;
            else
                this.ENABLE_STATUS = rs.getString("ENABLE_STATUS").trim();

            this.USER_ID = rs.getLong("USER_ID");
            if( rs.getString("REMARK") == null )
                this.REMARK = null;
            else
                this.REMARK = rs.getString("REMARK").trim();

            if( rs.getString("INSERT_OPER") == null )
                this.INSERT_OPER = null;
            else
                this.INSERT_OPER = rs.getString("INSERT_OPER").trim();

            if( rs.getString("INSERT_CONSIGNOR") == null )
                this.INSERT_CONSIGNOR = null;
            else
                this.INSERT_CONSIGNOR = rs.getString("INSERT_CONSIGNOR").trim();

            this.INSERT_TIME = rs.getDate("INSERT_TIME");
            if( rs.getString("UPDATE_OPER") == null )
                this.UPDATE_OPER = null;
            else
                this.UPDATE_OPER = rs.getString("UPDATE_OPER").trim();

            if( rs.getString("UPDATE_CONSIGNOR") == null )
                this.UPDATE_CONSIGNOR = null;
            else
                this.UPDATE_CONSIGNOR = rs.getString("UPDATE_CONSIGNOR").trim();

            this.UPDATE_TIME = rs.getDate("UPDATE_TIME");
            if( rs.getString("CUST_NAME") == null )
                this.CUST_NAME = null;
            else
                this.CUST_NAME = rs.getString("CUST_NAME").trim();

            this.MNGORG_ID = rs.getLong("MNGORG_ID");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGERSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public T_CUST_MANAGERSchema getSchema() {
        T_CUST_MANAGERSchema aT_CUST_MANAGERSchema = new T_CUST_MANAGERSchema();
        aT_CUST_MANAGERSchema.setSchema(this);
        return aT_CUST_MANAGERSchema;
    }

    public T_CUST_MANAGERDB getDB() {
        T_CUST_MANAGERDB aDBOper = new T_CUST_MANAGERDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpT_CUST_MANAGER描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(CUST_MANAGER_ID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CUST_MANAGER_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CUST_MANAGER_LEVEL)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ENABLE_STATUS)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(USER_ID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(REMARK)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(INSERT_OPER)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(INSERT_CONSIGNOR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( INSERT_TIME ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UPDATE_OPER)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UPDATE_CONSIGNOR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UPDATE_TIME ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CUST_NAME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MNGORG_ID));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpT_CUST_MANAGER>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            CUST_MANAGER_ID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            CUST_MANAGER_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            CUST_MANAGER_LEVEL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ENABLE_STATUS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            USER_ID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,5, SysConst.PACKAGESPILTER))).longValue();
            REMARK = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            INSERT_OPER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            INSERT_CONSIGNOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            INSERT_TIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            UPDATE_OPER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            UPDATE_CONSIGNOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            UPDATE_TIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER));
            CUST_NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            MNGORG_ID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,14, SysConst.PACKAGESPILTER))).longValue();
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGERSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CUST_MANAGER_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_MANAGER_ID));
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_MANAGER_CODE));
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_LEVEL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_MANAGER_LEVEL));
        }
        if (FCode.equalsIgnoreCase("ENABLE_STATUS")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ENABLE_STATUS));
        }
        if (FCode.equalsIgnoreCase("USER_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(USER_ID));
        }
        if (FCode.equalsIgnoreCase("REMARK")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(REMARK));
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_OPER));
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getINSERT_TIME()));
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_OPER));
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUPDATE_TIME()));
        }
        if (FCode.equalsIgnoreCase("CUST_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CUST_NAME));
        }
        if (FCode.equalsIgnoreCase("MNGORG_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MNGORG_ID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CUST_MANAGER_ID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CUST_MANAGER_CODE);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CUST_MANAGER_LEVEL);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ENABLE_STATUS);
                break;
            case 4:
                strFieldValue = String.valueOf(USER_ID);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(REMARK);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(INSERT_OPER);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(INSERT_CONSIGNOR);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getINSERT_TIME()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(UPDATE_OPER);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(UPDATE_CONSIGNOR);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUPDATE_TIME()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(CUST_NAME);
                break;
            case 13:
                strFieldValue = String.valueOf(MNGORG_ID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CUST_MANAGER_ID")) {
            if( FValue != null && !FValue.equals("")) {
                CUST_MANAGER_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUST_MANAGER_CODE = FValue.trim();
            }
            else
                CUST_MANAGER_CODE = null;
        }
        if (FCode.equalsIgnoreCase("CUST_MANAGER_LEVEL")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUST_MANAGER_LEVEL = FValue.trim();
            }
            else
                CUST_MANAGER_LEVEL = null;
        }
        if (FCode.equalsIgnoreCase("ENABLE_STATUS")) {
            if( FValue != null && !FValue.equals(""))
            {
                ENABLE_STATUS = FValue.trim();
            }
            else
                ENABLE_STATUS = null;
        }
        if (FCode.equalsIgnoreCase("USER_ID")) {
            if( FValue != null && !FValue.equals("")) {
                USER_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("REMARK")) {
            if( FValue != null && !FValue.equals(""))
            {
                REMARK = FValue.trim();
            }
            else
                REMARK = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_OPER = FValue.trim();
            }
            else
                INSERT_OPER = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_CONSIGNOR = FValue.trim();
            }
            else
                INSERT_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            if(FValue != null && !FValue.equals("")) {
                INSERT_TIME = fDate.getDate( FValue );
            }
            else
                INSERT_TIME = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_OPER = FValue.trim();
            }
            else
                UPDATE_OPER = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_CONSIGNOR = FValue.trim();
            }
            else
                UPDATE_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            if(FValue != null && !FValue.equals("")) {
                UPDATE_TIME = fDate.getDate( FValue );
            }
            else
                UPDATE_TIME = null;
        }
        if (FCode.equalsIgnoreCase("CUST_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                CUST_NAME = FValue.trim();
            }
            else
                CUST_NAME = null;
        }
        if (FCode.equalsIgnoreCase("MNGORG_ID")) {
            if( FValue != null && !FValue.equals("")) {
                MNGORG_ID = new Long(FValue).longValue();
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        T_CUST_MANAGERSchema other = (T_CUST_MANAGERSchema)otherObject;
        return
            CUST_MANAGER_ID == other.getCUST_MANAGER_ID()
            && CUST_MANAGER_CODE.equals(other.getCUST_MANAGER_CODE())
            && CUST_MANAGER_LEVEL.equals(other.getCUST_MANAGER_LEVEL())
            && ENABLE_STATUS.equals(other.getENABLE_STATUS())
            && USER_ID == other.getUSER_ID()
            && REMARK.equals(other.getREMARK())
            && INSERT_OPER.equals(other.getINSERT_OPER())
            && INSERT_CONSIGNOR.equals(other.getINSERT_CONSIGNOR())
            && fDate.getString(INSERT_TIME).equals(other.getINSERT_TIME())
            && UPDATE_OPER.equals(other.getUPDATE_OPER())
            && UPDATE_CONSIGNOR.equals(other.getUPDATE_CONSIGNOR())
            && fDate.getString(UPDATE_TIME).equals(other.getUPDATE_TIME())
            && CUST_NAME.equals(other.getCUST_NAME())
            && MNGORG_ID == other.getMNGORG_ID();
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CUST_MANAGER_ID") ) {
            return 0;
        }
        if( strFieldName.equals("CUST_MANAGER_CODE") ) {
            return 1;
        }
        if( strFieldName.equals("CUST_MANAGER_LEVEL") ) {
            return 2;
        }
        if( strFieldName.equals("ENABLE_STATUS") ) {
            return 3;
        }
        if( strFieldName.equals("USER_ID") ) {
            return 4;
        }
        if( strFieldName.equals("REMARK") ) {
            return 5;
        }
        if( strFieldName.equals("INSERT_OPER") ) {
            return 6;
        }
        if( strFieldName.equals("INSERT_CONSIGNOR") ) {
            return 7;
        }
        if( strFieldName.equals("INSERT_TIME") ) {
            return 8;
        }
        if( strFieldName.equals("UPDATE_OPER") ) {
            return 9;
        }
        if( strFieldName.equals("UPDATE_CONSIGNOR") ) {
            return 10;
        }
        if( strFieldName.equals("UPDATE_TIME") ) {
            return 11;
        }
        if( strFieldName.equals("CUST_NAME") ) {
            return 12;
        }
        if( strFieldName.equals("MNGORG_ID") ) {
            return 13;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CUST_MANAGER_ID";
                break;
            case 1:
                strFieldName = "CUST_MANAGER_CODE";
                break;
            case 2:
                strFieldName = "CUST_MANAGER_LEVEL";
                break;
            case 3:
                strFieldName = "ENABLE_STATUS";
                break;
            case 4:
                strFieldName = "USER_ID";
                break;
            case 5:
                strFieldName = "REMARK";
                break;
            case 6:
                strFieldName = "INSERT_OPER";
                break;
            case 7:
                strFieldName = "INSERT_CONSIGNOR";
                break;
            case 8:
                strFieldName = "INSERT_TIME";
                break;
            case 9:
                strFieldName = "UPDATE_OPER";
                break;
            case 10:
                strFieldName = "UPDATE_CONSIGNOR";
                break;
            case 11:
                strFieldName = "UPDATE_TIME";
                break;
            case 12:
                strFieldName = "CUST_NAME";
                break;
            case 13:
                strFieldName = "MNGORG_ID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUST_MANAGER_ID":
                return Schema.TYPE_LONG;
            case "CUST_MANAGER_CODE":
                return Schema.TYPE_STRING;
            case "CUST_MANAGER_LEVEL":
                return Schema.TYPE_STRING;
            case "ENABLE_STATUS":
                return Schema.TYPE_STRING;
            case "USER_ID":
                return Schema.TYPE_LONG;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "INSERT_OPER":
                return Schema.TYPE_STRING;
            case "INSERT_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "INSERT_TIME":
                return Schema.TYPE_DATE;
            case "UPDATE_OPER":
                return Schema.TYPE_STRING;
            case "UPDATE_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "UPDATE_TIME":
                return Schema.TYPE_DATE;
            case "CUST_NAME":
                return Schema.TYPE_STRING;
            case "MNGORG_ID":
                return Schema.TYPE_LONG;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_LONG;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DATE;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_LONG;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "T_CUST_MANAGERSchema {" +
            "CUST_MANAGER_ID="+CUST_MANAGER_ID +
            ", CUST_MANAGER_CODE="+CUST_MANAGER_CODE +
            ", CUST_MANAGER_LEVEL="+CUST_MANAGER_LEVEL +
            ", ENABLE_STATUS="+ENABLE_STATUS +
            ", USER_ID="+USER_ID +
            ", REMARK="+REMARK +
            ", INSERT_OPER="+INSERT_OPER +
            ", INSERT_CONSIGNOR="+INSERT_CONSIGNOR +
            ", INSERT_TIME="+INSERT_TIME +
            ", UPDATE_OPER="+UPDATE_OPER +
            ", UPDATE_CONSIGNOR="+UPDATE_CONSIGNOR +
            ", UPDATE_TIME="+UPDATE_TIME +
            ", CUST_NAME="+CUST_NAME +
            ", MNGORG_ID="+MNGORG_ID +"}";
    }
}
