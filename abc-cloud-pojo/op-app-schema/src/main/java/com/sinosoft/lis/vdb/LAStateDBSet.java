/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LAStateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LAStateDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-27
 */
public class LAStateDBSet extends LAStateSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LAStateDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LAState");
        mflag = true;
    }

    public LAStateDBSet() {
        db = new DBOper( "LAState" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAStateDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LAState WHERE  1=1  AND BranchType = ? AND StartDate = ? AND EndDate = ? AND StateType = ? AND ObjectId = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getBranchType());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getStartDate());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getEndDate());
            }
            if(this.get(i).getStateType() == null || this.get(i).getStateType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getStateType());
            }
            if(this.get(i).getObjectId() == null || this.get(i).getObjectId().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getObjectId());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAStateDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LAState SET  ManageCom = ? , BranchType = ? , BranchType2 = ? , StartDate = ? , EndDate = ? , StateType = ? , StateValue = ? , StateValue1 = ? , StateValue2 = ? , ObjectType = ? , ObjectId = ? , ObjectLevel = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , BranchAttr = ? , GradeDate = ? WHERE  1=1  AND BranchType = ? AND StartDate = ? AND EndDate = ? AND StateType = ? AND ObjectId = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getManageCom());
            }
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getBranchType());
            }
            if(this.get(i).getBranchType2() == null || this.get(i).getBranchType2().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBranchType2());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getStartDate());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getEndDate());
            }
            if(this.get(i).getStateType() == null || this.get(i).getStateType().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getStateType());
            }
            if(this.get(i).getStateValue() == null || this.get(i).getStateValue().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getStateValue());
            }
            if(this.get(i).getStateValue1() == null || this.get(i).getStateValue1().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getStateValue1());
            }
            if(this.get(i).getStateValue2() == null || this.get(i).getStateValue2().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getStateValue2());
            }
            if(this.get(i).getObjectType() == null || this.get(i).getObjectType().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getObjectType());
            }
            if(this.get(i).getObjectId() == null || this.get(i).getObjectId().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getObjectId());
            }
            if(this.get(i).getObjectLevel() == null || this.get(i).getObjectLevel().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getObjectLevel());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(14,null);
            } else {
                pstmt.setDate(14, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(16,null);
            } else {
                pstmt.setDate(16, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getModifyTime());
            }
            if(this.get(i).getBranchAttr() == null || this.get(i).getBranchAttr().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getBranchAttr());
            }
            if(this.get(i).getGradeDate() == null || this.get(i).getGradeDate().equals("null")) {
                pstmt.setDate(19,null);
            } else {
                pstmt.setDate(19, Date.valueOf(this.get(i).getGradeDate()));
            }
            // set where condition
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getBranchType());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getStartDate());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getEndDate());
            }
            if(this.get(i).getStateType() == null || this.get(i).getStateType().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getStateType());
            }
            if(this.get(i).getObjectId() == null || this.get(i).getObjectId().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getObjectId());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAStateDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LAState VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getManageCom());
            }
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getBranchType());
            }
            if(this.get(i).getBranchType2() == null || this.get(i).getBranchType2().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getBranchType2());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getStartDate());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getEndDate());
            }
            if(this.get(i).getStateType() == null || this.get(i).getStateType().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getStateType());
            }
            if(this.get(i).getStateValue() == null || this.get(i).getStateValue().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getStateValue());
            }
            if(this.get(i).getStateValue1() == null || this.get(i).getStateValue1().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getStateValue1());
            }
            if(this.get(i).getStateValue2() == null || this.get(i).getStateValue2().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getStateValue2());
            }
            if(this.get(i).getObjectType() == null || this.get(i).getObjectType().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getObjectType());
            }
            if(this.get(i).getObjectId() == null || this.get(i).getObjectId().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getObjectId());
            }
            if(this.get(i).getObjectLevel() == null || this.get(i).getObjectLevel().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getObjectLevel());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(14,null);
            } else {
                pstmt.setDate(14, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(16,null);
            } else {
                pstmt.setDate(16, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getModifyTime());
            }
            if(this.get(i).getBranchAttr() == null || this.get(i).getBranchAttr().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getBranchAttr());
            }
            if(this.get(i).getGradeDate() == null || this.get(i).getGradeDate().equals("null")) {
                pstmt.setDate(19,null);
            } else {
                pstmt.setDate(19, Date.valueOf(this.get(i).getGradeDate()));
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAStateDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
