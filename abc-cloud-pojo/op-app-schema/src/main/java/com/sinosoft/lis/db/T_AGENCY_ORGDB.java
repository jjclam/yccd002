/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.T_AGENCY_ORGSchema;
import com.sinosoft.lis.vschema.T_AGENCY_ORGSet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: T_AGENCY_ORGDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */
public class T_AGENCY_ORGDB extends T_AGENCY_ORGSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public T_AGENCY_ORGDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "T_AGENCY_ORG" );
        mflag = true;
    }

    public T_AGENCY_ORGDB() {
        con = null;
        db = new DBOper( "T_AGENCY_ORG" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        T_AGENCY_ORGSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        T_AGENCY_ORGSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM T_AGENCY_ORG WHERE  1=1  AND AGENCY_ID = ?");
            pstmt.setLong(1, this.getAGENCY_ID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("T_AGENCY_ORG");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE T_AGENCY_ORG SET  AGENCY_ID = ? , MNGORG_ID = ? , AGENCY_CODE = ? , AGENCY_NAME = ? , AGENCY_ABBR = ? , EXTER_ANNOUNCE_ORGCODE = ? , SUPERIOR_ORG_ID = ? , AGENCY_LEVEL = ? , AGENCY_REGION_TYPE = ? , AGENCY_STATUS = ? , AGENCY_TYPE = ? , CORRESP_CORE_CODE = ? , LINKMAN_NAME = ? , FLG = ? , SUPERVISOR_NAME = ? , INSERT_OPER = ? , INSERT_CONSIGNOR = ? , INSERT_TIME = ? , UPDATE_OPER = ? , UPDATE_CONSIGNOR = ? , UPDATE_TIME = ? , BANK_CODE = ? , AGENCY_ADDR = ? , MNGORG_CODE = ? WHERE  1=1  AND AGENCY_ID = ?");
            pstmt.setLong(1, this.getAGENCY_ID());
            pstmt.setLong(2, this.getMNGORG_ID());
            if(this.getAGENCY_CODE() == null || this.getAGENCY_CODE().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getAGENCY_CODE());
            }
            if(this.getAGENCY_NAME() == null || this.getAGENCY_NAME().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getAGENCY_NAME());
            }
            if(this.getAGENCY_ABBR() == null || this.getAGENCY_ABBR().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAGENCY_ABBR());
            }
            if(this.getEXTER_ANNOUNCE_ORGCODE() == null || this.getEXTER_ANNOUNCE_ORGCODE().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getEXTER_ANNOUNCE_ORGCODE());
            }
            pstmt.setLong(7, this.getSUPERIOR_ORG_ID());
            if(this.getAGENCY_LEVEL() == null || this.getAGENCY_LEVEL().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAGENCY_LEVEL());
            }
            if(this.getAGENCY_REGION_TYPE() == null || this.getAGENCY_REGION_TYPE().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAGENCY_REGION_TYPE());
            }
            if(this.getAGENCY_STATUS() == null || this.getAGENCY_STATUS().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAGENCY_STATUS());
            }
            if(this.getAGENCY_TYPE() == null || this.getAGENCY_TYPE().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAGENCY_TYPE());
            }
            if(this.getCORRESP_CORE_CODE() == null || this.getCORRESP_CORE_CODE().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getCORRESP_CORE_CODE());
            }
            if(this.getLINKMAN_NAME() == null || this.getLINKMAN_NAME().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getLINKMAN_NAME());
            }
            if(this.getFLG() == null || this.getFLG().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getFLG());
            }
            if(this.getSUPERVISOR_NAME() == null || this.getSUPERVISOR_NAME().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getSUPERVISOR_NAME());
            }
            if(this.getINSERT_OPER() == null || this.getINSERT_OPER().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getINSERT_OPER());
            }
            if(this.getINSERT_CONSIGNOR() == null || this.getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getINSERT_CONSIGNOR());
            }
            if(this.getINSERT_TIME() == null || this.getINSERT_TIME().equals("null")) {
            	pstmt.setNull(18, 93);
            } else {
            	pstmt.setDate(18, Date.valueOf(this.getINSERT_TIME()));
            }
            if(this.getUPDATE_OPER() == null || this.getUPDATE_OPER().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getUPDATE_OPER());
            }
            if(this.getUPDATE_CONSIGNOR() == null || this.getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getUPDATE_CONSIGNOR());
            }
            if(this.getUPDATE_TIME() == null || this.getUPDATE_TIME().equals("null")) {
            	pstmt.setNull(21, 93);
            } else {
            	pstmt.setDate(21, Date.valueOf(this.getUPDATE_TIME()));
            }
            if(this.getBANK_CODE() == null || this.getBANK_CODE().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getBANK_CODE());
            }
            if(this.getAGENCY_ADDR() == null || this.getAGENCY_ADDR().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getAGENCY_ADDR());
            }
            if(this.getMNGORG_CODE() == null || this.getMNGORG_CODE().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getMNGORG_CODE());
            }
            // set where condition
            pstmt.setLong(25, this.getAGENCY_ID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("T_AGENCY_ORG");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO T_AGENCY_ORG VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getAGENCY_ID());
            pstmt.setLong(2, this.getMNGORG_ID());
            if(this.getAGENCY_CODE() == null || this.getAGENCY_CODE().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getAGENCY_CODE());
            }
            if(this.getAGENCY_NAME() == null || this.getAGENCY_NAME().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getAGENCY_NAME());
            }
            if(this.getAGENCY_ABBR() == null || this.getAGENCY_ABBR().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAGENCY_ABBR());
            }
            if(this.getEXTER_ANNOUNCE_ORGCODE() == null || this.getEXTER_ANNOUNCE_ORGCODE().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getEXTER_ANNOUNCE_ORGCODE());
            }
            pstmt.setLong(7, this.getSUPERIOR_ORG_ID());
            if(this.getAGENCY_LEVEL() == null || this.getAGENCY_LEVEL().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAGENCY_LEVEL());
            }
            if(this.getAGENCY_REGION_TYPE() == null || this.getAGENCY_REGION_TYPE().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getAGENCY_REGION_TYPE());
            }
            if(this.getAGENCY_STATUS() == null || this.getAGENCY_STATUS().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getAGENCY_STATUS());
            }
            if(this.getAGENCY_TYPE() == null || this.getAGENCY_TYPE().equals("null")) {
            	pstmt.setNull(11, 12);
            } else {
            	pstmt.setString(11, this.getAGENCY_TYPE());
            }
            if(this.getCORRESP_CORE_CODE() == null || this.getCORRESP_CORE_CODE().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getCORRESP_CORE_CODE());
            }
            if(this.getLINKMAN_NAME() == null || this.getLINKMAN_NAME().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getLINKMAN_NAME());
            }
            if(this.getFLG() == null || this.getFLG().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getFLG());
            }
            if(this.getSUPERVISOR_NAME() == null || this.getSUPERVISOR_NAME().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getSUPERVISOR_NAME());
            }
            if(this.getINSERT_OPER() == null || this.getINSERT_OPER().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getINSERT_OPER());
            }
            if(this.getINSERT_CONSIGNOR() == null || this.getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getINSERT_CONSIGNOR());
            }
            if(this.getINSERT_TIME() == null || this.getINSERT_TIME().equals("null")) {
            	pstmt.setNull(18, 93);
            } else {
            	pstmt.setDate(18, Date.valueOf(this.getINSERT_TIME()));
            }
            if(this.getUPDATE_OPER() == null || this.getUPDATE_OPER().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getUPDATE_OPER());
            }
            if(this.getUPDATE_CONSIGNOR() == null || this.getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setNull(20, 12);
            } else {
            	pstmt.setString(20, this.getUPDATE_CONSIGNOR());
            }
            if(this.getUPDATE_TIME() == null || this.getUPDATE_TIME().equals("null")) {
            	pstmt.setNull(21, 93);
            } else {
            	pstmt.setDate(21, Date.valueOf(this.getUPDATE_TIME()));
            }
            if(this.getBANK_CODE() == null || this.getBANK_CODE().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getBANK_CODE());
            }
            if(this.getAGENCY_ADDR() == null || this.getAGENCY_ADDR().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getAGENCY_ADDR());
            }
            if(this.getMNGORG_CODE() == null || this.getMNGORG_CODE().equals("null")) {
            	pstmt.setNull(24, 12);
            } else {
            	pstmt.setString(24, this.getMNGORG_CODE());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM T_AGENCY_ORG WHERE  1=1  AND AGENCY_ID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getAGENCY_ID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_AGENCY_ORGDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public T_AGENCY_ORGSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        T_AGENCY_ORGSet aT_AGENCY_ORGSet = new T_AGENCY_ORGSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("T_AGENCY_ORG");
            T_AGENCY_ORGSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_AGENCY_ORGDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                T_AGENCY_ORGSchema s1 = new T_AGENCY_ORGSchema();
                s1.setSchema(rs,i);
                aT_AGENCY_ORGSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aT_AGENCY_ORGSet;
    }

    public T_AGENCY_ORGSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        T_AGENCY_ORGSet aT_AGENCY_ORGSet = new T_AGENCY_ORGSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_AGENCY_ORGDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                T_AGENCY_ORGSchema s1 = new T_AGENCY_ORGSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_AGENCY_ORGDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aT_AGENCY_ORGSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aT_AGENCY_ORGSet;
    }

    public T_AGENCY_ORGSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        T_AGENCY_ORGSet aT_AGENCY_ORGSet = new T_AGENCY_ORGSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("T_AGENCY_ORG");
            T_AGENCY_ORGSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                T_AGENCY_ORGSchema s1 = new T_AGENCY_ORGSchema();
                s1.setSchema(rs,i);
                aT_AGENCY_ORGSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aT_AGENCY_ORGSet;
    }

    public T_AGENCY_ORGSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        T_AGENCY_ORGSet aT_AGENCY_ORGSet = new T_AGENCY_ORGSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                T_AGENCY_ORGSchema s1 = new T_AGENCY_ORGSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_AGENCY_ORGDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aT_AGENCY_ORGSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aT_AGENCY_ORGSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("T_AGENCY_ORG");
            T_AGENCY_ORGSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update T_AGENCY_ORG " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "T_AGENCY_ORGDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return T_AGENCY_ORGSet
     */
    public T_AGENCY_ORGSet getData() {
        int tCount = 0;
        T_AGENCY_ORGSet tT_AGENCY_ORGSet = new T_AGENCY_ORGSet();
        T_AGENCY_ORGSchema tT_AGENCY_ORGSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tT_AGENCY_ORGSchema = new T_AGENCY_ORGSchema();
            tT_AGENCY_ORGSchema.setSchema(mResultSet, 1);
            tT_AGENCY_ORGSet.add(tT_AGENCY_ORGSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tT_AGENCY_ORGSchema = new T_AGENCY_ORGSchema();
                    tT_AGENCY_ORGSchema.setSchema(mResultSet, 1);
                    tT_AGENCY_ORGSet.add(tT_AGENCY_ORGSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tT_AGENCY_ORGSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "T_AGENCY_ORGDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "T_AGENCY_ORGDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
