/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LMInsuAccValueSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LMInsuAccValueDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMInsuAccValueDBSet extends LMInsuAccValueSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LMInsuAccValueDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LMInsuAccValue");
        mflag = true;
    }

    public LMInsuAccValueDBSet() {
        db = new DBOper( "LMInsuAccValue" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMInsuAccValueDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LMInsuAccValue WHERE  1=1  AND RiskCode = ? AND InsuAccNo = ? AND ValueDate = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getValueDate() == null || this.get(i).getValueDate().equals("null")) {
                pstmt.setDate(3,null);
            } else {
                pstmt.setDate(3, Date.valueOf(this.get(i).getValueDate()));
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMInsuAccValueDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LMInsuAccValue SET  RiskCode = ? , InsuAccNo = ? , ValueDate = ? , SRateDate = ? , ARateDate = ? , RateIn = ? , RateOut = ? , RedeemRate = ? , RedeemMoney = ? , Operator = ? , MakeDate = ? , MakeTime = ? WHERE  1=1  AND RiskCode = ? AND InsuAccNo = ? AND ValueDate = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getValueDate() == null || this.get(i).getValueDate().equals("null")) {
                pstmt.setDate(3,null);
            } else {
                pstmt.setDate(3, Date.valueOf(this.get(i).getValueDate()));
            }
            if(this.get(i).getSRateDate() == null || this.get(i).getSRateDate().equals("null")) {
                pstmt.setDate(4,null);
            } else {
                pstmt.setDate(4, Date.valueOf(this.get(i).getSRateDate()));
            }
            if(this.get(i).getARateDate() == null || this.get(i).getARateDate().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getARateDate()));
            }
            pstmt.setDouble(6, this.get(i).getRateIn());
            pstmt.setDouble(7, this.get(i).getRateOut());
            pstmt.setDouble(8, this.get(i).getRedeemRate());
            pstmt.setDouble(9, this.get(i).getRedeemMoney());
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getMakeTime());
            }
            // set where condition
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getValueDate() == null || this.get(i).getValueDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getValueDate()));
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMInsuAccValueDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LMInsuAccValue VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getValueDate() == null || this.get(i).getValueDate().equals("null")) {
                pstmt.setDate(3,null);
            } else {
                pstmt.setDate(3, Date.valueOf(this.get(i).getValueDate()));
            }
            if(this.get(i).getSRateDate() == null || this.get(i).getSRateDate().equals("null")) {
                pstmt.setDate(4,null);
            } else {
                pstmt.setDate(4, Date.valueOf(this.get(i).getSRateDate()));
            }
            if(this.get(i).getARateDate() == null || this.get(i).getARateDate().equals("null")) {
                pstmt.setDate(5,null);
            } else {
                pstmt.setDate(5, Date.valueOf(this.get(i).getARateDate()));
            }
            pstmt.setDouble(6, this.get(i).getRateIn());
            pstmt.setDouble(7, this.get(i).getRateOut());
            pstmt.setDouble(8, this.get(i).getRedeemRate());
            pstmt.setDouble(9, this.get(i).getRedeemMoney());
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getMakeTime());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMInsuAccValueDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
