/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.T_Bank_Agency_Brchnet_MapDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: T_Bank_Agency_Brchnet_MapSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class T_Bank_Agency_Brchnet_MapSchema implements Schema, Cloneable {
    // @Field
    /** Bank_agency_brchnet_map_id */
    private long BANK_AGENCY_BRCHNET_MAP_ID;
    /** Bank_code */
    private String BANK_CODE;
    /** Bank_area_code */
    private String BANK_AREA_CODE;
    /** Bank_brchnet_code */
    private String BANK_BRCHNET_CODE;
    /** Agency_brchnet_code */
    private String AGENCY_BRCHNET_CODE;
    /** Agency_code */
    private String AGENCY_CODE;
    /** Mngorg_code */
    private String MNGORG_CODE;
    /** Insert_oper */
    private String INSERT_OPER;
    /** Insert_time */
    private Date INSERT_TIME;
    /** Insert_consignor */
    private String INSERT_CONSIGNOR;
    /** Update_oper */
    private String UPDATE_OPER;
    /** Update_time */
    private Date UPDATE_TIME;
    /** Update_consignor */
    private String UPDATE_CONSIGNOR;

    public static final int FIELDNUM = 13;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public T_Bank_Agency_Brchnet_MapSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "BANK_AGENCY_BRCHNET_MAP_ID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        T_Bank_Agency_Brchnet_MapSchema cloned = (T_Bank_Agency_Brchnet_MapSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getBANK_AGENCY_BRCHNET_MAP_ID() {
        return BANK_AGENCY_BRCHNET_MAP_ID;
    }
    public void setBANK_AGENCY_BRCHNET_MAP_ID(long aBANK_AGENCY_BRCHNET_MAP_ID) {
        BANK_AGENCY_BRCHNET_MAP_ID = aBANK_AGENCY_BRCHNET_MAP_ID;
    }
    public void setBANK_AGENCY_BRCHNET_MAP_ID(String aBANK_AGENCY_BRCHNET_MAP_ID) {
        if (aBANK_AGENCY_BRCHNET_MAP_ID != null && !aBANK_AGENCY_BRCHNET_MAP_ID.equals("")) {
            BANK_AGENCY_BRCHNET_MAP_ID = new Long(aBANK_AGENCY_BRCHNET_MAP_ID).longValue();
        }
    }

    public String getBANK_CODE() {
        return BANK_CODE;
    }
    public void setBANK_CODE(String aBANK_CODE) {
        BANK_CODE = aBANK_CODE;
    }
    public String getBANK_AREA_CODE() {
        return BANK_AREA_CODE;
    }
    public void setBANK_AREA_CODE(String aBANK_AREA_CODE) {
        BANK_AREA_CODE = aBANK_AREA_CODE;
    }
    public String getBANK_BRCHNET_CODE() {
        return BANK_BRCHNET_CODE;
    }
    public void setBANK_BRCHNET_CODE(String aBANK_BRCHNET_CODE) {
        BANK_BRCHNET_CODE = aBANK_BRCHNET_CODE;
    }
    public String getAGENCY_BRCHNET_CODE() {
        return AGENCY_BRCHNET_CODE;
    }
    public void setAGENCY_BRCHNET_CODE(String aAGENCY_BRCHNET_CODE) {
        AGENCY_BRCHNET_CODE = aAGENCY_BRCHNET_CODE;
    }
    public String getAGENCY_CODE() {
        return AGENCY_CODE;
    }
    public void setAGENCY_CODE(String aAGENCY_CODE) {
        AGENCY_CODE = aAGENCY_CODE;
    }
    public String getMNGORG_CODE() {
        return MNGORG_CODE;
    }
    public void setMNGORG_CODE(String aMNGORG_CODE) {
        MNGORG_CODE = aMNGORG_CODE;
    }
    public String getINSERT_OPER() {
        return INSERT_OPER;
    }
    public void setINSERT_OPER(String aINSERT_OPER) {
        INSERT_OPER = aINSERT_OPER;
    }
    public String getINSERT_TIME() {
        if(INSERT_TIME != null) {
            return fDate.getString(INSERT_TIME);
        } else {
            return null;
        }
    }
    public void setINSERT_TIME(Date aINSERT_TIME) {
        INSERT_TIME = aINSERT_TIME;
    }
    public void setINSERT_TIME(String aINSERT_TIME) {
        if (aINSERT_TIME != null && !aINSERT_TIME.equals("")) {
            INSERT_TIME = fDate.getDate(aINSERT_TIME);
        } else
            INSERT_TIME = null;
    }

    public String getINSERT_CONSIGNOR() {
        return INSERT_CONSIGNOR;
    }
    public void setINSERT_CONSIGNOR(String aINSERT_CONSIGNOR) {
        INSERT_CONSIGNOR = aINSERT_CONSIGNOR;
    }
    public String getUPDATE_OPER() {
        return UPDATE_OPER;
    }
    public void setUPDATE_OPER(String aUPDATE_OPER) {
        UPDATE_OPER = aUPDATE_OPER;
    }
    public String getUPDATE_TIME() {
        if(UPDATE_TIME != null) {
            return fDate.getString(UPDATE_TIME);
        } else {
            return null;
        }
    }
    public void setUPDATE_TIME(Date aUPDATE_TIME) {
        UPDATE_TIME = aUPDATE_TIME;
    }
    public void setUPDATE_TIME(String aUPDATE_TIME) {
        if (aUPDATE_TIME != null && !aUPDATE_TIME.equals("")) {
            UPDATE_TIME = fDate.getDate(aUPDATE_TIME);
        } else
            UPDATE_TIME = null;
    }

    public String getUPDATE_CONSIGNOR() {
        return UPDATE_CONSIGNOR;
    }
    public void setUPDATE_CONSIGNOR(String aUPDATE_CONSIGNOR) {
        UPDATE_CONSIGNOR = aUPDATE_CONSIGNOR;
    }

    /**
    * 使用另外一个 T_Bank_Agency_Brchnet_MapSchema 对象给 Schema 赋值
    * @param: aT_Bank_Agency_Brchnet_MapSchema T_Bank_Agency_Brchnet_MapSchema
    **/
    public void setSchema(T_Bank_Agency_Brchnet_MapSchema aT_Bank_Agency_Brchnet_MapSchema) {
        this.BANK_AGENCY_BRCHNET_MAP_ID = aT_Bank_Agency_Brchnet_MapSchema.getBANK_AGENCY_BRCHNET_MAP_ID();
        this.BANK_CODE = aT_Bank_Agency_Brchnet_MapSchema.getBANK_CODE();
        this.BANK_AREA_CODE = aT_Bank_Agency_Brchnet_MapSchema.getBANK_AREA_CODE();
        this.BANK_BRCHNET_CODE = aT_Bank_Agency_Brchnet_MapSchema.getBANK_BRCHNET_CODE();
        this.AGENCY_BRCHNET_CODE = aT_Bank_Agency_Brchnet_MapSchema.getAGENCY_BRCHNET_CODE();
        this.AGENCY_CODE = aT_Bank_Agency_Brchnet_MapSchema.getAGENCY_CODE();
        this.MNGORG_CODE = aT_Bank_Agency_Brchnet_MapSchema.getMNGORG_CODE();
        this.INSERT_OPER = aT_Bank_Agency_Brchnet_MapSchema.getINSERT_OPER();
        this.INSERT_TIME = fDate.getDate( aT_Bank_Agency_Brchnet_MapSchema.getINSERT_TIME());
        this.INSERT_CONSIGNOR = aT_Bank_Agency_Brchnet_MapSchema.getINSERT_CONSIGNOR();
        this.UPDATE_OPER = aT_Bank_Agency_Brchnet_MapSchema.getUPDATE_OPER();
        this.UPDATE_TIME = fDate.getDate( aT_Bank_Agency_Brchnet_MapSchema.getUPDATE_TIME());
        this.UPDATE_CONSIGNOR = aT_Bank_Agency_Brchnet_MapSchema.getUPDATE_CONSIGNOR();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.BANK_AGENCY_BRCHNET_MAP_ID = rs.getLong("BANK_AGENCY_BRCHNET_MAP_ID");
            if( rs.getString("BANK_CODE") == null )
                this.BANK_CODE = null;
            else
                this.BANK_CODE = rs.getString("BANK_CODE").trim();

            if( rs.getString("BANK_AREA_CODE") == null )
                this.BANK_AREA_CODE = null;
            else
                this.BANK_AREA_CODE = rs.getString("BANK_AREA_CODE").trim();

            if( rs.getString("BANK_BRCHNET_CODE") == null )
                this.BANK_BRCHNET_CODE = null;
            else
                this.BANK_BRCHNET_CODE = rs.getString("BANK_BRCHNET_CODE").trim();

            if( rs.getString("AGENCY_BRCHNET_CODE") == null )
                this.AGENCY_BRCHNET_CODE = null;
            else
                this.AGENCY_BRCHNET_CODE = rs.getString("AGENCY_BRCHNET_CODE").trim();

            if( rs.getString("AGENCY_CODE") == null )
                this.AGENCY_CODE = null;
            else
                this.AGENCY_CODE = rs.getString("AGENCY_CODE").trim();

            if( rs.getString("MNGORG_CODE") == null )
                this.MNGORG_CODE = null;
            else
                this.MNGORG_CODE = rs.getString("MNGORG_CODE").trim();

            if( rs.getString("INSERT_OPER") == null )
                this.INSERT_OPER = null;
            else
                this.INSERT_OPER = rs.getString("INSERT_OPER").trim();

            this.INSERT_TIME = rs.getDate("INSERT_TIME");
            if( rs.getString("INSERT_CONSIGNOR") == null )
                this.INSERT_CONSIGNOR = null;
            else
                this.INSERT_CONSIGNOR = rs.getString("INSERT_CONSIGNOR").trim();

            if( rs.getString("UPDATE_OPER") == null )
                this.UPDATE_OPER = null;
            else
                this.UPDATE_OPER = rs.getString("UPDATE_OPER").trim();

            this.UPDATE_TIME = rs.getDate("UPDATE_TIME");
            if( rs.getString("UPDATE_CONSIGNOR") == null )
                this.UPDATE_CONSIGNOR = null;
            else
                this.UPDATE_CONSIGNOR = rs.getString("UPDATE_CONSIGNOR").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_Bank_Agency_Brchnet_MapSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public T_Bank_Agency_Brchnet_MapSchema getSchema() {
        T_Bank_Agency_Brchnet_MapSchema aT_Bank_Agency_Brchnet_MapSchema = new T_Bank_Agency_Brchnet_MapSchema();
        aT_Bank_Agency_Brchnet_MapSchema.setSchema(this);
        return aT_Bank_Agency_Brchnet_MapSchema;
    }

    public T_Bank_Agency_Brchnet_MapDB getDB() {
        T_Bank_Agency_Brchnet_MapDB aDBOper = new T_Bank_Agency_Brchnet_MapDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpT_Bank_Agency_Brchnet_Map描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(BANK_AGENCY_BRCHNET_MAP_ID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BANK_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BANK_AREA_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BANK_BRCHNET_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_BRCHNET_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MNGORG_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(INSERT_OPER)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( INSERT_TIME ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(INSERT_CONSIGNOR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UPDATE_OPER)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UPDATE_TIME ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UPDATE_CONSIGNOR));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpT_Bank_Agency_Brchnet_Map>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            BANK_AGENCY_BRCHNET_MAP_ID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            BANK_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            BANK_AREA_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BANK_BRCHNET_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AGENCY_BRCHNET_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AGENCY_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            MNGORG_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            INSERT_OPER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            INSERT_TIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            INSERT_CONSIGNOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            UPDATE_OPER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            UPDATE_TIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER));
            UPDATE_CONSIGNOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_Bank_Agency_Brchnet_MapSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("BANK_AGENCY_BRCHNET_MAP_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_AGENCY_BRCHNET_MAP_ID));
        }
        if (FCode.equalsIgnoreCase("BANK_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_CODE));
        }
        if (FCode.equalsIgnoreCase("BANK_AREA_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_AREA_CODE));
        }
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_BRCHNET_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_BRCHNET_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_BRCHNET_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_CODE));
        }
        if (FCode.equalsIgnoreCase("MNGORG_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MNGORG_CODE));
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_OPER));
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getINSERT_TIME()));
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_OPER));
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUPDATE_TIME()));
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_CONSIGNOR));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(BANK_AGENCY_BRCHNET_MAP_ID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BANK_CODE);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BANK_AREA_CODE);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BANK_BRCHNET_CODE);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_BRCHNET_CODE);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_CODE);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MNGORG_CODE);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(INSERT_OPER);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getINSERT_TIME()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(INSERT_CONSIGNOR);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(UPDATE_OPER);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUPDATE_TIME()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(UPDATE_CONSIGNOR);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("BANK_AGENCY_BRCHNET_MAP_ID")) {
            if( FValue != null && !FValue.equals("")) {
                BANK_AGENCY_BRCHNET_MAP_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("BANK_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_CODE = FValue.trim();
            }
            else
                BANK_CODE = null;
        }
        if (FCode.equalsIgnoreCase("BANK_AREA_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_AREA_CODE = FValue.trim();
            }
            else
                BANK_AREA_CODE = null;
        }
        if (FCode.equalsIgnoreCase("BANK_BRCHNET_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_BRCHNET_CODE = FValue.trim();
            }
            else
                BANK_BRCHNET_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_BRCHNET_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_BRCHNET_CODE = FValue.trim();
            }
            else
                AGENCY_BRCHNET_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_CODE = FValue.trim();
            }
            else
                AGENCY_CODE = null;
        }
        if (FCode.equalsIgnoreCase("MNGORG_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MNGORG_CODE = FValue.trim();
            }
            else
                MNGORG_CODE = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_OPER = FValue.trim();
            }
            else
                INSERT_OPER = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            if(FValue != null && !FValue.equals("")) {
                INSERT_TIME = fDate.getDate( FValue );
            }
            else
                INSERT_TIME = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_CONSIGNOR = FValue.trim();
            }
            else
                INSERT_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_OPER = FValue.trim();
            }
            else
                UPDATE_OPER = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            if(FValue != null && !FValue.equals("")) {
                UPDATE_TIME = fDate.getDate( FValue );
            }
            else
                UPDATE_TIME = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_CONSIGNOR = FValue.trim();
            }
            else
                UPDATE_CONSIGNOR = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        T_Bank_Agency_Brchnet_MapSchema other = (T_Bank_Agency_Brchnet_MapSchema)otherObject;
        return
            BANK_AGENCY_BRCHNET_MAP_ID == other.getBANK_AGENCY_BRCHNET_MAP_ID()
            && BANK_CODE.equals(other.getBANK_CODE())
            && BANK_AREA_CODE.equals(other.getBANK_AREA_CODE())
            && BANK_BRCHNET_CODE.equals(other.getBANK_BRCHNET_CODE())
            && AGENCY_BRCHNET_CODE.equals(other.getAGENCY_BRCHNET_CODE())
            && AGENCY_CODE.equals(other.getAGENCY_CODE())
            && MNGORG_CODE.equals(other.getMNGORG_CODE())
            && INSERT_OPER.equals(other.getINSERT_OPER())
            && fDate.getString(INSERT_TIME).equals(other.getINSERT_TIME())
            && INSERT_CONSIGNOR.equals(other.getINSERT_CONSIGNOR())
            && UPDATE_OPER.equals(other.getUPDATE_OPER())
            && fDate.getString(UPDATE_TIME).equals(other.getUPDATE_TIME())
            && UPDATE_CONSIGNOR.equals(other.getUPDATE_CONSIGNOR());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("BANK_AGENCY_BRCHNET_MAP_ID") ) {
            return 0;
        }
        if( strFieldName.equals("BANK_CODE") ) {
            return 1;
        }
        if( strFieldName.equals("BANK_AREA_CODE") ) {
            return 2;
        }
        if( strFieldName.equals("BANK_BRCHNET_CODE") ) {
            return 3;
        }
        if( strFieldName.equals("AGENCY_BRCHNET_CODE") ) {
            return 4;
        }
        if( strFieldName.equals("AGENCY_CODE") ) {
            return 5;
        }
        if( strFieldName.equals("MNGORG_CODE") ) {
            return 6;
        }
        if( strFieldName.equals("INSERT_OPER") ) {
            return 7;
        }
        if( strFieldName.equals("INSERT_TIME") ) {
            return 8;
        }
        if( strFieldName.equals("INSERT_CONSIGNOR") ) {
            return 9;
        }
        if( strFieldName.equals("UPDATE_OPER") ) {
            return 10;
        }
        if( strFieldName.equals("UPDATE_TIME") ) {
            return 11;
        }
        if( strFieldName.equals("UPDATE_CONSIGNOR") ) {
            return 12;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "BANK_AGENCY_BRCHNET_MAP_ID";
                break;
            case 1:
                strFieldName = "BANK_CODE";
                break;
            case 2:
                strFieldName = "BANK_AREA_CODE";
                break;
            case 3:
                strFieldName = "BANK_BRCHNET_CODE";
                break;
            case 4:
                strFieldName = "AGENCY_BRCHNET_CODE";
                break;
            case 5:
                strFieldName = "AGENCY_CODE";
                break;
            case 6:
                strFieldName = "MNGORG_CODE";
                break;
            case 7:
                strFieldName = "INSERT_OPER";
                break;
            case 8:
                strFieldName = "INSERT_TIME";
                break;
            case 9:
                strFieldName = "INSERT_CONSIGNOR";
                break;
            case 10:
                strFieldName = "UPDATE_OPER";
                break;
            case 11:
                strFieldName = "UPDATE_TIME";
                break;
            case 12:
                strFieldName = "UPDATE_CONSIGNOR";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "BANK_AGENCY_BRCHNET_MAP_ID":
                return Schema.TYPE_LONG;
            case "BANK_CODE":
                return Schema.TYPE_STRING;
            case "BANK_AREA_CODE":
                return Schema.TYPE_STRING;
            case "BANK_BRCHNET_CODE":
                return Schema.TYPE_STRING;
            case "AGENCY_BRCHNET_CODE":
                return Schema.TYPE_STRING;
            case "AGENCY_CODE":
                return Schema.TYPE_STRING;
            case "MNGORG_CODE":
                return Schema.TYPE_STRING;
            case "INSERT_OPER":
                return Schema.TYPE_STRING;
            case "INSERT_TIME":
                return Schema.TYPE_DATE;
            case "INSERT_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "UPDATE_OPER":
                return Schema.TYPE_STRING;
            case "UPDATE_TIME":
                return Schema.TYPE_DATE;
            case "UPDATE_CONSIGNOR":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DATE;
            case 12:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "T_Bank_Agency_Brchnet_MapSchema {" +
            "BANK_AGENCY_BRCHNET_MAP_ID="+BANK_AGENCY_BRCHNET_MAP_ID +
            ", BANK_CODE="+BANK_CODE +
            ", BANK_AREA_CODE="+BANK_AREA_CODE +
            ", BANK_BRCHNET_CODE="+BANK_BRCHNET_CODE +
            ", AGENCY_BRCHNET_CODE="+AGENCY_BRCHNET_CODE +
            ", AGENCY_CODE="+AGENCY_CODE +
            ", MNGORG_CODE="+MNGORG_CODE +
            ", INSERT_OPER="+INSERT_OPER +
            ", INSERT_TIME="+INSERT_TIME +
            ", INSERT_CONSIGNOR="+INSERT_CONSIGNOR +
            ", UPDATE_OPER="+UPDATE_OPER +
            ", UPDATE_TIME="+UPDATE_TIME +
            ", UPDATE_CONSIGNOR="+UPDATE_CONSIGNOR +"}";
    }
}
