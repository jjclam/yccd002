/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMRiskInsuAccDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LMRiskInsuAccSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMRiskInsuAccSchema implements Schema, Cloneable {
    // @Field
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 账号类型 */
    private String AccType;
    /** 账户分类 */
    private String AccKind;
    /** 保险帐户名称 */
    private String InsuAccName;
    /** 账户产生位置 */
    private String AccCreatePos;
    /** 账号产生规则 */
    private String AccCreateType;
    /** 账户固定利率 */
    private double AccRate;
    /** 账户对应利率表 */
    private String AccRateTable;
    /** 账户结清计算公式 */
    private String AccCancelCode;
    /** 账户结算方式 */
    private String AccComputeFlag;
    /** 投资类型 */
    private String InvestType;
    /** 基金公司代码 */
    private String FundCompanyCode;
    /** 账户所有者 */
    private String Owner;
    /** 分红记入账户的方式 */
    private String AccBonusMode;
    /** 分红记入账户代码 */
    private String BonusToInsuAccNo;
    /** 分红时是否进行账户结算 */
    private String InsuAccCalBalaFlag;
    /** 分红方式 */
    private String BonusMode;
    /** 投资收益类型 */
    private String InvestFlag;
    /** 计价周期 */
    private String CalValueFreq;
    /** 计价价格获取规则 */
    private String CalValueRule;
    /** 小数位数 */
    private String UnitDecimal;
    /** 四舍五入标记 */
    private String RoundMethod;
    /** 账户是否参与分红 */
    private String AccBonusFlag;
    /** 是否参与分红 */
    private String BonusFlag;

    public static final int FIELDNUM = 24;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMRiskInsuAccSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "InsuAccNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMRiskInsuAccSchema cloned = (LMRiskInsuAccSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getAccKind() {
        return AccKind;
    }
    public void setAccKind(String aAccKind) {
        AccKind = aAccKind;
    }
    public String getInsuAccName() {
        return InsuAccName;
    }
    public void setInsuAccName(String aInsuAccName) {
        InsuAccName = aInsuAccName;
    }
    public String getAccCreatePos() {
        return AccCreatePos;
    }
    public void setAccCreatePos(String aAccCreatePos) {
        AccCreatePos = aAccCreatePos;
    }
    public String getAccCreateType() {
        return AccCreateType;
    }
    public void setAccCreateType(String aAccCreateType) {
        AccCreateType = aAccCreateType;
    }
    public double getAccRate() {
        return AccRate;
    }
    public void setAccRate(double aAccRate) {
        AccRate = aAccRate;
    }
    public void setAccRate(String aAccRate) {
        if (aAccRate != null && !aAccRate.equals("")) {
            Double tDouble = new Double(aAccRate);
            double d = tDouble.doubleValue();
            AccRate = d;
        }
    }

    public String getAccRateTable() {
        return AccRateTable;
    }
    public void setAccRateTable(String aAccRateTable) {
        AccRateTable = aAccRateTable;
    }
    public String getAccCancelCode() {
        return AccCancelCode;
    }
    public void setAccCancelCode(String aAccCancelCode) {
        AccCancelCode = aAccCancelCode;
    }
    public String getAccComputeFlag() {
        return AccComputeFlag;
    }
    public void setAccComputeFlag(String aAccComputeFlag) {
        AccComputeFlag = aAccComputeFlag;
    }
    public String getInvestType() {
        return InvestType;
    }
    public void setInvestType(String aInvestType) {
        InvestType = aInvestType;
    }
    public String getFundCompanyCode() {
        return FundCompanyCode;
    }
    public void setFundCompanyCode(String aFundCompanyCode) {
        FundCompanyCode = aFundCompanyCode;
    }
    public String getOwner() {
        return Owner;
    }
    public void setOwner(String aOwner) {
        Owner = aOwner;
    }
    public String getAccBonusMode() {
        return AccBonusMode;
    }
    public void setAccBonusMode(String aAccBonusMode) {
        AccBonusMode = aAccBonusMode;
    }
    public String getBonusToInsuAccNo() {
        return BonusToInsuAccNo;
    }
    public void setBonusToInsuAccNo(String aBonusToInsuAccNo) {
        BonusToInsuAccNo = aBonusToInsuAccNo;
    }
    public String getInsuAccCalBalaFlag() {
        return InsuAccCalBalaFlag;
    }
    public void setInsuAccCalBalaFlag(String aInsuAccCalBalaFlag) {
        InsuAccCalBalaFlag = aInsuAccCalBalaFlag;
    }
    public String getBonusMode() {
        return BonusMode;
    }
    public void setBonusMode(String aBonusMode) {
        BonusMode = aBonusMode;
    }
    public String getInvestFlag() {
        return InvestFlag;
    }
    public void setInvestFlag(String aInvestFlag) {
        InvestFlag = aInvestFlag;
    }
    public String getCalValueFreq() {
        return CalValueFreq;
    }
    public void setCalValueFreq(String aCalValueFreq) {
        CalValueFreq = aCalValueFreq;
    }
    public String getCalValueRule() {
        return CalValueRule;
    }
    public void setCalValueRule(String aCalValueRule) {
        CalValueRule = aCalValueRule;
    }
    public String getUnitDecimal() {
        return UnitDecimal;
    }
    public void setUnitDecimal(String aUnitDecimal) {
        UnitDecimal = aUnitDecimal;
    }
    public String getRoundMethod() {
        return RoundMethod;
    }
    public void setRoundMethod(String aRoundMethod) {
        RoundMethod = aRoundMethod;
    }
    public String getAccBonusFlag() {
        return AccBonusFlag;
    }
    public void setAccBonusFlag(String aAccBonusFlag) {
        AccBonusFlag = aAccBonusFlag;
    }
    public String getBonusFlag() {
        return BonusFlag;
    }
    public void setBonusFlag(String aBonusFlag) {
        BonusFlag = aBonusFlag;
    }

    /**
    * 使用另外一个 LMRiskInsuAccSchema 对象给 Schema 赋值
    * @param: aLMRiskInsuAccSchema LMRiskInsuAccSchema
    **/
    public void setSchema(LMRiskInsuAccSchema aLMRiskInsuAccSchema) {
        this.InsuAccNo = aLMRiskInsuAccSchema.getInsuAccNo();
        this.AccType = aLMRiskInsuAccSchema.getAccType();
        this.AccKind = aLMRiskInsuAccSchema.getAccKind();
        this.InsuAccName = aLMRiskInsuAccSchema.getInsuAccName();
        this.AccCreatePos = aLMRiskInsuAccSchema.getAccCreatePos();
        this.AccCreateType = aLMRiskInsuAccSchema.getAccCreateType();
        this.AccRate = aLMRiskInsuAccSchema.getAccRate();
        this.AccRateTable = aLMRiskInsuAccSchema.getAccRateTable();
        this.AccCancelCode = aLMRiskInsuAccSchema.getAccCancelCode();
        this.AccComputeFlag = aLMRiskInsuAccSchema.getAccComputeFlag();
        this.InvestType = aLMRiskInsuAccSchema.getInvestType();
        this.FundCompanyCode = aLMRiskInsuAccSchema.getFundCompanyCode();
        this.Owner = aLMRiskInsuAccSchema.getOwner();
        this.AccBonusMode = aLMRiskInsuAccSchema.getAccBonusMode();
        this.BonusToInsuAccNo = aLMRiskInsuAccSchema.getBonusToInsuAccNo();
        this.InsuAccCalBalaFlag = aLMRiskInsuAccSchema.getInsuAccCalBalaFlag();
        this.BonusMode = aLMRiskInsuAccSchema.getBonusMode();
        this.InvestFlag = aLMRiskInsuAccSchema.getInvestFlag();
        this.CalValueFreq = aLMRiskInsuAccSchema.getCalValueFreq();
        this.CalValueRule = aLMRiskInsuAccSchema.getCalValueRule();
        this.UnitDecimal = aLMRiskInsuAccSchema.getUnitDecimal();
        this.RoundMethod = aLMRiskInsuAccSchema.getRoundMethod();
        this.AccBonusFlag = aLMRiskInsuAccSchema.getAccBonusFlag();
        this.BonusFlag = aLMRiskInsuAccSchema.getBonusFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("InsuAccNo") == null )
                this.InsuAccNo = null;
            else
                this.InsuAccNo = rs.getString("InsuAccNo").trim();

            if( rs.getString("AccType") == null )
                this.AccType = null;
            else
                this.AccType = rs.getString("AccType").trim();

            if( rs.getString("AccKind") == null )
                this.AccKind = null;
            else
                this.AccKind = rs.getString("AccKind").trim();

            if( rs.getString("InsuAccName") == null )
                this.InsuAccName = null;
            else
                this.InsuAccName = rs.getString("InsuAccName").trim();

            if( rs.getString("AccCreatePos") == null )
                this.AccCreatePos = null;
            else
                this.AccCreatePos = rs.getString("AccCreatePos").trim();

            if( rs.getString("AccCreateType") == null )
                this.AccCreateType = null;
            else
                this.AccCreateType = rs.getString("AccCreateType").trim();

            this.AccRate = rs.getDouble("AccRate");
            if( rs.getString("AccRateTable") == null )
                this.AccRateTable = null;
            else
                this.AccRateTable = rs.getString("AccRateTable").trim();

            if( rs.getString("AccCancelCode") == null )
                this.AccCancelCode = null;
            else
                this.AccCancelCode = rs.getString("AccCancelCode").trim();

            if( rs.getString("AccComputeFlag") == null )
                this.AccComputeFlag = null;
            else
                this.AccComputeFlag = rs.getString("AccComputeFlag").trim();

            if( rs.getString("InvestType") == null )
                this.InvestType = null;
            else
                this.InvestType = rs.getString("InvestType").trim();

            if( rs.getString("FundCompanyCode") == null )
                this.FundCompanyCode = null;
            else
                this.FundCompanyCode = rs.getString("FundCompanyCode").trim();

            if( rs.getString("Owner") == null )
                this.Owner = null;
            else
                this.Owner = rs.getString("Owner").trim();

            if( rs.getString("AccBonusMode") == null )
                this.AccBonusMode = null;
            else
                this.AccBonusMode = rs.getString("AccBonusMode").trim();

            if( rs.getString("BonusToInsuAccNo") == null )
                this.BonusToInsuAccNo = null;
            else
                this.BonusToInsuAccNo = rs.getString("BonusToInsuAccNo").trim();

            if( rs.getString("InsuAccCalBalaFlag") == null )
                this.InsuAccCalBalaFlag = null;
            else
                this.InsuAccCalBalaFlag = rs.getString("InsuAccCalBalaFlag").trim();

            if( rs.getString("BonusMode") == null )
                this.BonusMode = null;
            else
                this.BonusMode = rs.getString("BonusMode").trim();

            if( rs.getString("InvestFlag") == null )
                this.InvestFlag = null;
            else
                this.InvestFlag = rs.getString("InvestFlag").trim();

            if( rs.getString("CalValueFreq") == null )
                this.CalValueFreq = null;
            else
                this.CalValueFreq = rs.getString("CalValueFreq").trim();

            if( rs.getString("CalValueRule") == null )
                this.CalValueRule = null;
            else
                this.CalValueRule = rs.getString("CalValueRule").trim();

            if( rs.getString("UnitDecimal") == null )
                this.UnitDecimal = null;
            else
                this.UnitDecimal = rs.getString("UnitDecimal").trim();

            if( rs.getString("RoundMethod") == null )
                this.RoundMethod = null;
            else
                this.RoundMethod = rs.getString("RoundMethod").trim();

            if( rs.getString("AccBonusFlag") == null )
                this.AccBonusFlag = null;
            else
                this.AccBonusFlag = rs.getString("AccBonusFlag").trim();

            if( rs.getString("BonusFlag") == null )
                this.BonusFlag = null;
            else
                this.BonusFlag = rs.getString("BonusFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMRiskInsuAccSchema getSchema() {
        LMRiskInsuAccSchema aLMRiskInsuAccSchema = new LMRiskInsuAccSchema();
        aLMRiskInsuAccSchema.setSchema(this);
        return aLMRiskInsuAccSchema;
    }

    public LMRiskInsuAccDB getDB() {
        LMRiskInsuAccDB aDBOper = new LMRiskInsuAccDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskInsuAcc描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(InsuAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccKind)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuAccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccCreatePos)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccCreateType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AccRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccRateTable)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccCancelCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccComputeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InvestType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FundCompanyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Owner)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccBonusMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BonusToInsuAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuAccCalBalaFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BonusMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InvestFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalValueFreq)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalValueRule)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UnitDecimal)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RoundMethod)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccBonusFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BonusFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskInsuAcc>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            AccKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            InsuAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AccCreatePos = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AccCreateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            AccRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7, SysConst.PACKAGESPILTER))).doubleValue();
            AccRateTable = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            AccCancelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AccComputeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            InvestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            FundCompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            Owner = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            AccBonusMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            BonusToInsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            InsuAccCalBalaFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            BonusMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            InvestFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            CalValueFreq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            CalValueRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            UnitDecimal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            RoundMethod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            AccBonusFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            BonusFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskInsuAccSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("AccKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccKind));
        }
        if (FCode.equalsIgnoreCase("InsuAccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccName));
        }
        if (FCode.equalsIgnoreCase("AccCreatePos")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCreatePos));
        }
        if (FCode.equalsIgnoreCase("AccCreateType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCreateType));
        }
        if (FCode.equalsIgnoreCase("AccRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccRate));
        }
        if (FCode.equalsIgnoreCase("AccRateTable")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccRateTable));
        }
        if (FCode.equalsIgnoreCase("AccCancelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccCancelCode));
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccComputeFlag));
        }
        if (FCode.equalsIgnoreCase("InvestType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestType));
        }
        if (FCode.equalsIgnoreCase("FundCompanyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FundCompanyCode));
        }
        if (FCode.equalsIgnoreCase("Owner")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Owner));
        }
        if (FCode.equalsIgnoreCase("AccBonusMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccBonusMode));
        }
        if (FCode.equalsIgnoreCase("BonusToInsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusToInsuAccNo));
        }
        if (FCode.equalsIgnoreCase("InsuAccCalBalaFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccCalBalaFlag));
        }
        if (FCode.equalsIgnoreCase("BonusMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusMode));
        }
        if (FCode.equalsIgnoreCase("InvestFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvestFlag));
        }
        if (FCode.equalsIgnoreCase("CalValueFreq")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalValueFreq));
        }
        if (FCode.equalsIgnoreCase("CalValueRule")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalValueRule));
        }
        if (FCode.equalsIgnoreCase("UnitDecimal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitDecimal));
        }
        if (FCode.equalsIgnoreCase("RoundMethod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RoundMethod));
        }
        if (FCode.equalsIgnoreCase("AccBonusFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccBonusFlag));
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AccKind);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InsuAccName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AccCreatePos);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AccCreateType);
                break;
            case 6:
                strFieldValue = String.valueOf(AccRate);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AccRateTable);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AccCancelCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AccComputeFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InvestType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(FundCompanyCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Owner);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AccBonusMode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(BonusToInsuAccNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InsuAccCalBalaFlag);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(BonusMode);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(InvestFlag);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(CalValueFreq);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(CalValueRule);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(UnitDecimal);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(RoundMethod);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(AccBonusFlag);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(BonusFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("AccKind")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccKind = FValue.trim();
            }
            else
                AccKind = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccName = FValue.trim();
            }
            else
                InsuAccName = null;
        }
        if (FCode.equalsIgnoreCase("AccCreatePos")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCreatePos = FValue.trim();
            }
            else
                AccCreatePos = null;
        }
        if (FCode.equalsIgnoreCase("AccCreateType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCreateType = FValue.trim();
            }
            else
                AccCreateType = null;
        }
        if (FCode.equalsIgnoreCase("AccRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                AccRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("AccRateTable")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccRateTable = FValue.trim();
            }
            else
                AccRateTable = null;
        }
        if (FCode.equalsIgnoreCase("AccCancelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccCancelCode = FValue.trim();
            }
            else
                AccCancelCode = null;
        }
        if (FCode.equalsIgnoreCase("AccComputeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccComputeFlag = FValue.trim();
            }
            else
                AccComputeFlag = null;
        }
        if (FCode.equalsIgnoreCase("InvestType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestType = FValue.trim();
            }
            else
                InvestType = null;
        }
        if (FCode.equalsIgnoreCase("FundCompanyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                FundCompanyCode = FValue.trim();
            }
            else
                FundCompanyCode = null;
        }
        if (FCode.equalsIgnoreCase("Owner")) {
            if( FValue != null && !FValue.equals(""))
            {
                Owner = FValue.trim();
            }
            else
                Owner = null;
        }
        if (FCode.equalsIgnoreCase("AccBonusMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccBonusMode = FValue.trim();
            }
            else
                AccBonusMode = null;
        }
        if (FCode.equalsIgnoreCase("BonusToInsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusToInsuAccNo = FValue.trim();
            }
            else
                BonusToInsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccCalBalaFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccCalBalaFlag = FValue.trim();
            }
            else
                InsuAccCalBalaFlag = null;
        }
        if (FCode.equalsIgnoreCase("BonusMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusMode = FValue.trim();
            }
            else
                BonusMode = null;
        }
        if (FCode.equalsIgnoreCase("InvestFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvestFlag = FValue.trim();
            }
            else
                InvestFlag = null;
        }
        if (FCode.equalsIgnoreCase("CalValueFreq")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalValueFreq = FValue.trim();
            }
            else
                CalValueFreq = null;
        }
        if (FCode.equalsIgnoreCase("CalValueRule")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalValueRule = FValue.trim();
            }
            else
                CalValueRule = null;
        }
        if (FCode.equalsIgnoreCase("UnitDecimal")) {
            if( FValue != null && !FValue.equals(""))
            {
                UnitDecimal = FValue.trim();
            }
            else
                UnitDecimal = null;
        }
        if (FCode.equalsIgnoreCase("RoundMethod")) {
            if( FValue != null && !FValue.equals(""))
            {
                RoundMethod = FValue.trim();
            }
            else
                RoundMethod = null;
        }
        if (FCode.equalsIgnoreCase("AccBonusFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccBonusFlag = FValue.trim();
            }
            else
                AccBonusFlag = null;
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BonusFlag = FValue.trim();
            }
            else
                BonusFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMRiskInsuAccSchema other = (LMRiskInsuAccSchema)otherObject;
        return
            InsuAccNo.equals(other.getInsuAccNo())
            && AccType.equals(other.getAccType())
            && AccKind.equals(other.getAccKind())
            && InsuAccName.equals(other.getInsuAccName())
            && AccCreatePos.equals(other.getAccCreatePos())
            && AccCreateType.equals(other.getAccCreateType())
            && AccRate == other.getAccRate()
            && AccRateTable.equals(other.getAccRateTable())
            && AccCancelCode.equals(other.getAccCancelCode())
            && AccComputeFlag.equals(other.getAccComputeFlag())
            && InvestType.equals(other.getInvestType())
            && FundCompanyCode.equals(other.getFundCompanyCode())
            && Owner.equals(other.getOwner())
            && AccBonusMode.equals(other.getAccBonusMode())
            && BonusToInsuAccNo.equals(other.getBonusToInsuAccNo())
            && InsuAccCalBalaFlag.equals(other.getInsuAccCalBalaFlag())
            && BonusMode.equals(other.getBonusMode())
            && InvestFlag.equals(other.getInvestFlag())
            && CalValueFreq.equals(other.getCalValueFreq())
            && CalValueRule.equals(other.getCalValueRule())
            && UnitDecimal.equals(other.getUnitDecimal())
            && RoundMethod.equals(other.getRoundMethod())
            && AccBonusFlag.equals(other.getAccBonusFlag())
            && BonusFlag.equals(other.getBonusFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("InsuAccNo") ) {
            return 0;
        }
        if( strFieldName.equals("AccType") ) {
            return 1;
        }
        if( strFieldName.equals("AccKind") ) {
            return 2;
        }
        if( strFieldName.equals("InsuAccName") ) {
            return 3;
        }
        if( strFieldName.equals("AccCreatePos") ) {
            return 4;
        }
        if( strFieldName.equals("AccCreateType") ) {
            return 5;
        }
        if( strFieldName.equals("AccRate") ) {
            return 6;
        }
        if( strFieldName.equals("AccRateTable") ) {
            return 7;
        }
        if( strFieldName.equals("AccCancelCode") ) {
            return 8;
        }
        if( strFieldName.equals("AccComputeFlag") ) {
            return 9;
        }
        if( strFieldName.equals("InvestType") ) {
            return 10;
        }
        if( strFieldName.equals("FundCompanyCode") ) {
            return 11;
        }
        if( strFieldName.equals("Owner") ) {
            return 12;
        }
        if( strFieldName.equals("AccBonusMode") ) {
            return 13;
        }
        if( strFieldName.equals("BonusToInsuAccNo") ) {
            return 14;
        }
        if( strFieldName.equals("InsuAccCalBalaFlag") ) {
            return 15;
        }
        if( strFieldName.equals("BonusMode") ) {
            return 16;
        }
        if( strFieldName.equals("InvestFlag") ) {
            return 17;
        }
        if( strFieldName.equals("CalValueFreq") ) {
            return 18;
        }
        if( strFieldName.equals("CalValueRule") ) {
            return 19;
        }
        if( strFieldName.equals("UnitDecimal") ) {
            return 20;
        }
        if( strFieldName.equals("RoundMethod") ) {
            return 21;
        }
        if( strFieldName.equals("AccBonusFlag") ) {
            return 22;
        }
        if( strFieldName.equals("BonusFlag") ) {
            return 23;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "InsuAccNo";
                break;
            case 1:
                strFieldName = "AccType";
                break;
            case 2:
                strFieldName = "AccKind";
                break;
            case 3:
                strFieldName = "InsuAccName";
                break;
            case 4:
                strFieldName = "AccCreatePos";
                break;
            case 5:
                strFieldName = "AccCreateType";
                break;
            case 6:
                strFieldName = "AccRate";
                break;
            case 7:
                strFieldName = "AccRateTable";
                break;
            case 8:
                strFieldName = "AccCancelCode";
                break;
            case 9:
                strFieldName = "AccComputeFlag";
                break;
            case 10:
                strFieldName = "InvestType";
                break;
            case 11:
                strFieldName = "FundCompanyCode";
                break;
            case 12:
                strFieldName = "Owner";
                break;
            case 13:
                strFieldName = "AccBonusMode";
                break;
            case 14:
                strFieldName = "BonusToInsuAccNo";
                break;
            case 15:
                strFieldName = "InsuAccCalBalaFlag";
                break;
            case 16:
                strFieldName = "BonusMode";
                break;
            case 17:
                strFieldName = "InvestFlag";
                break;
            case 18:
                strFieldName = "CalValueFreq";
                break;
            case 19:
                strFieldName = "CalValueRule";
                break;
            case 20:
                strFieldName = "UnitDecimal";
                break;
            case 21:
                strFieldName = "RoundMethod";
                break;
            case 22:
                strFieldName = "AccBonusFlag";
                break;
            case 23:
                strFieldName = "BonusFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "ACCKIND":
                return Schema.TYPE_STRING;
            case "INSUACCNAME":
                return Schema.TYPE_STRING;
            case "ACCCREATEPOS":
                return Schema.TYPE_STRING;
            case "ACCCREATETYPE":
                return Schema.TYPE_STRING;
            case "ACCRATE":
                return Schema.TYPE_DOUBLE;
            case "ACCRATETABLE":
                return Schema.TYPE_STRING;
            case "ACCCANCELCODE":
                return Schema.TYPE_STRING;
            case "ACCCOMPUTEFLAG":
                return Schema.TYPE_STRING;
            case "INVESTTYPE":
                return Schema.TYPE_STRING;
            case "FUNDCOMPANYCODE":
                return Schema.TYPE_STRING;
            case "OWNER":
                return Schema.TYPE_STRING;
            case "ACCBONUSMODE":
                return Schema.TYPE_STRING;
            case "BONUSTOINSUACCNO":
                return Schema.TYPE_STRING;
            case "INSUACCCALBALAFLAG":
                return Schema.TYPE_STRING;
            case "BONUSMODE":
                return Schema.TYPE_STRING;
            case "INVESTFLAG":
                return Schema.TYPE_STRING;
            case "CALVALUEFREQ":
                return Schema.TYPE_STRING;
            case "CALVALUERULE":
                return Schema.TYPE_STRING;
            case "UNITDECIMAL":
                return Schema.TYPE_STRING;
            case "ROUNDMETHOD":
                return Schema.TYPE_STRING;
            case "ACCBONUSFLAG":
                return Schema.TYPE_STRING;
            case "BONUSFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
