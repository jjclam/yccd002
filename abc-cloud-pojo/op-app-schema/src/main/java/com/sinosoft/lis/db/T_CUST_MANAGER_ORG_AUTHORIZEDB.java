/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.T_CUST_MANAGER_ORG_AUTHORIZESchema;
import com.sinosoft.lis.vschema.T_CUST_MANAGER_ORG_AUTHORIZESet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: T_CUST_MANAGER_ORG_AUTHORIZEDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */
public class T_CUST_MANAGER_ORG_AUTHORIZEDB extends T_CUST_MANAGER_ORG_AUTHORIZESchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public T_CUST_MANAGER_ORG_AUTHORIZEDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "T_CUST_MANAGER_ORG_AUTHORIZE" );
        mflag = true;
    }

    public T_CUST_MANAGER_ORG_AUTHORIZEDB() {
        con = null;
        db = new DBOper( "T_CUST_MANAGER_ORG_AUTHORIZE" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        T_CUST_MANAGER_ORG_AUTHORIZESchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        T_CUST_MANAGER_ORG_AUTHORIZESchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM T_CUST_MANAGER_ORG_AUTHORIZE WHERE  1=1  AND CUST_MANAGER_AUTHORIZE_ID = ?");
            pstmt.setLong(1, this.getCUST_MANAGER_AUTHORIZE_ID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("T_CUST_MANAGER_ORG_AUTHORIZE");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE T_CUST_MANAGER_ORG_AUTHORIZE SET  CUST_MANAGER_AUTHORIZE_ID = ? , MNGORG_ID = ? , AGENCY_ID = ? , CUST_MANAGER_ID = ? , INSERT_OPER = ? , INSERT_CONSIGNOR = ? , INSERT_TIME = ? , UPDATE_OPER = ? , UPDATE_CONSIGNOR = ? , UPDATE_TIME = ? WHERE  1=1  AND CUST_MANAGER_AUTHORIZE_ID = ?");
            pstmt.setLong(1, this.getCUST_MANAGER_AUTHORIZE_ID());
            pstmt.setLong(2, this.getMNGORG_ID());
            pstmt.setLong(3, this.getAGENCY_ID());
            pstmt.setLong(4, this.getCUST_MANAGER_ID());
            if(this.getINSERT_OPER() == null || this.getINSERT_OPER().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getINSERT_OPER());
            }
            if(this.getINSERT_CONSIGNOR() == null || this.getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getINSERT_CONSIGNOR());
            }
            if(this.getINSERT_TIME() == null || this.getINSERT_TIME().equals("null")) {
            	pstmt.setNull(7, 93);
            } else {
            	pstmt.setDate(7, Date.valueOf(this.getINSERT_TIME()));
            }
            if(this.getUPDATE_OPER() == null || this.getUPDATE_OPER().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getUPDATE_OPER());
            }
            if(this.getUPDATE_CONSIGNOR() == null || this.getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getUPDATE_CONSIGNOR());
            }
            if(this.getUPDATE_TIME() == null || this.getUPDATE_TIME().equals("null")) {
            	pstmt.setNull(10, 93);
            } else {
            	pstmt.setDate(10, Date.valueOf(this.getUPDATE_TIME()));
            }
            // set where condition
            pstmt.setLong(11, this.getCUST_MANAGER_AUTHORIZE_ID());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("T_CUST_MANAGER_ORG_AUTHORIZE");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO T_CUST_MANAGER_ORG_AUTHORIZE VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            pstmt.setLong(1, this.getCUST_MANAGER_AUTHORIZE_ID());
            pstmt.setLong(2, this.getMNGORG_ID());
            pstmt.setLong(3, this.getAGENCY_ID());
            pstmt.setLong(4, this.getCUST_MANAGER_ID());
            if(this.getINSERT_OPER() == null || this.getINSERT_OPER().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getINSERT_OPER());
            }
            if(this.getINSERT_CONSIGNOR() == null || this.getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getINSERT_CONSIGNOR());
            }
            if(this.getINSERT_TIME() == null || this.getINSERT_TIME().equals("null")) {
            	pstmt.setNull(7, 93);
            } else {
            	pstmt.setDate(7, Date.valueOf(this.getINSERT_TIME()));
            }
            if(this.getUPDATE_OPER() == null || this.getUPDATE_OPER().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getUPDATE_OPER());
            }
            if(this.getUPDATE_CONSIGNOR() == null || this.getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getUPDATE_CONSIGNOR());
            }
            if(this.getUPDATE_TIME() == null || this.getUPDATE_TIME().equals("null")) {
            	pstmt.setNull(10, 93);
            } else {
            	pstmt.setDate(10, Date.valueOf(this.getUPDATE_TIME()));
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM T_CUST_MANAGER_ORG_AUTHORIZE WHERE  1=1  AND CUST_MANAGER_AUTHORIZE_ID = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setLong(1, this.getCUST_MANAGER_AUTHORIZE_ID());
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public T_CUST_MANAGER_ORG_AUTHORIZESet query() {
        Statement stmt = null;
        ResultSet rs = null;
        T_CUST_MANAGER_ORG_AUTHORIZESet aT_CUST_MANAGER_ORG_AUTHORIZESet = new T_CUST_MANAGER_ORG_AUTHORIZESet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("T_CUST_MANAGER_ORG_AUTHORIZE");
            T_CUST_MANAGER_ORG_AUTHORIZESchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                T_CUST_MANAGER_ORG_AUTHORIZESchema s1 = new T_CUST_MANAGER_ORG_AUTHORIZESchema();
                s1.setSchema(rs,i);
                aT_CUST_MANAGER_ORG_AUTHORIZESet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aT_CUST_MANAGER_ORG_AUTHORIZESet;
    }

    public T_CUST_MANAGER_ORG_AUTHORIZESet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        T_CUST_MANAGER_ORG_AUTHORIZESet aT_CUST_MANAGER_ORG_AUTHORIZESet = new T_CUST_MANAGER_ORG_AUTHORIZESet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                T_CUST_MANAGER_ORG_AUTHORIZESchema s1 = new T_CUST_MANAGER_ORG_AUTHORIZESchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aT_CUST_MANAGER_ORG_AUTHORIZESet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aT_CUST_MANAGER_ORG_AUTHORIZESet;
    }

    public T_CUST_MANAGER_ORG_AUTHORIZESet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        T_CUST_MANAGER_ORG_AUTHORIZESet aT_CUST_MANAGER_ORG_AUTHORIZESet = new T_CUST_MANAGER_ORG_AUTHORIZESet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("T_CUST_MANAGER_ORG_AUTHORIZE");
            T_CUST_MANAGER_ORG_AUTHORIZESchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                T_CUST_MANAGER_ORG_AUTHORIZESchema s1 = new T_CUST_MANAGER_ORG_AUTHORIZESchema();
                s1.setSchema(rs,i);
                aT_CUST_MANAGER_ORG_AUTHORIZESet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aT_CUST_MANAGER_ORG_AUTHORIZESet;
    }

    public T_CUST_MANAGER_ORG_AUTHORIZESet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        T_CUST_MANAGER_ORG_AUTHORIZESet aT_CUST_MANAGER_ORG_AUTHORIZESet = new T_CUST_MANAGER_ORG_AUTHORIZESet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                T_CUST_MANAGER_ORG_AUTHORIZESchema s1 = new T_CUST_MANAGER_ORG_AUTHORIZESchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aT_CUST_MANAGER_ORG_AUTHORIZESet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aT_CUST_MANAGER_ORG_AUTHORIZESet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("T_CUST_MANAGER_ORG_AUTHORIZE");
            T_CUST_MANAGER_ORG_AUTHORIZESchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update T_CUST_MANAGER_ORG_AUTHORIZE " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return T_CUST_MANAGER_ORG_AUTHORIZESet
     */
    public T_CUST_MANAGER_ORG_AUTHORIZESet getData() {
        int tCount = 0;
        T_CUST_MANAGER_ORG_AUTHORIZESet tT_CUST_MANAGER_ORG_AUTHORIZESet = new T_CUST_MANAGER_ORG_AUTHORIZESet();
        T_CUST_MANAGER_ORG_AUTHORIZESchema tT_CUST_MANAGER_ORG_AUTHORIZESchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tT_CUST_MANAGER_ORG_AUTHORIZESchema = new T_CUST_MANAGER_ORG_AUTHORIZESchema();
            tT_CUST_MANAGER_ORG_AUTHORIZESchema.setSchema(mResultSet, 1);
            tT_CUST_MANAGER_ORG_AUTHORIZESet.add(tT_CUST_MANAGER_ORG_AUTHORIZESchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tT_CUST_MANAGER_ORG_AUTHORIZESchema = new T_CUST_MANAGER_ORG_AUTHORIZESchema();
                    tT_CUST_MANAGER_ORG_AUTHORIZESchema.setSchema(mResultSet, 1);
                    tT_CUST_MANAGER_ORG_AUTHORIZESet.add(tT_CUST_MANAGER_ORG_AUTHORIZESchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tT_CUST_MANAGER_ORG_AUTHORIZESet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
