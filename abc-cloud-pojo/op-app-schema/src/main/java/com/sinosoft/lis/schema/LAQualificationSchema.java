/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LAQualificationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-22
 */
public class LAQualificationSchema implements Schema, Cloneable {
    // @Field
    /** 业务员编码 */
    private String AgentCode;
    /** 资格证书号 */
    private String QualifNo;
    /** 资格证类型 */
    private int Idx;
    /** 批准单位 */
    private String GrantUnit;
    /** 发放日期 */
    private Date GrantDate;
    /** 失效日期 */
    private Date InvalidDate;
    /** 失效原因 */
    private String InvalidRsn;
    /** 补发日期 */
    private Date reissueDate;
    /** 补发原因 */
    private String reissueRsn;
    /** 有效日期 */
    private int ValidPeriod;
    /** 资格证书状态 */
    private String State;
    /** 通过考试日期 */
    private Date PasExamDate;
    /** 考试年度 */
    private String ExamYear;
    /** 考试次数 */
    private String ExamTimes;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 上一次修改日期 */
    private Date ModifyDate;
    /** 上一次修改时间 */
    private String ModifyTime;
    /** 有效起期 */
    private Date ValidStart;
    /** 有效止期 */
    private Date ValidEnd;
    /** 旧资格证号码 */
    private String OldQualifNo;
    /** 资格证名称 */
    private String QualifName;
    /** State1 */
    private String State1;

    public static final int FIELDNUM = 24;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LAQualificationSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "AgentCode";
        pk[1] = "QualifNo";
        pk[2] = "Idx";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAQualificationSchema cloned = (LAQualificationSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getQualifNo() {
        return QualifNo;
    }
    public void setQualifNo(String aQualifNo) {
        QualifNo = aQualifNo;
    }
    public int getIdx() {
        return Idx;
    }
    public void setIdx(int aIdx) {
        Idx = aIdx;
    }
    public void setIdx(String aIdx) {
        if (aIdx != null && !aIdx.equals("")) {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public String getGrantUnit() {
        return GrantUnit;
    }
    public void setGrantUnit(String aGrantUnit) {
        GrantUnit = aGrantUnit;
    }
    public String getGrantDate() {
        if(GrantDate != null) {
            return fDate.getString(GrantDate);
        } else {
            return null;
        }
    }
    public void setGrantDate(Date aGrantDate) {
        GrantDate = aGrantDate;
    }
    public void setGrantDate(String aGrantDate) {
        if (aGrantDate != null && !aGrantDate.equals("")) {
            GrantDate = fDate.getDate(aGrantDate);
        } else
            GrantDate = null;
    }

    public String getInvalidDate() {
        if(InvalidDate != null) {
            return fDate.getString(InvalidDate);
        } else {
            return null;
        }
    }
    public void setInvalidDate(Date aInvalidDate) {
        InvalidDate = aInvalidDate;
    }
    public void setInvalidDate(String aInvalidDate) {
        if (aInvalidDate != null && !aInvalidDate.equals("")) {
            InvalidDate = fDate.getDate(aInvalidDate);
        } else
            InvalidDate = null;
    }

    public String getInvalidRsn() {
        return InvalidRsn;
    }
    public void setInvalidRsn(String aInvalidRsn) {
        InvalidRsn = aInvalidRsn;
    }
    public String getReissueDate() {
        if(reissueDate != null) {
            return fDate.getString(reissueDate);
        } else {
            return null;
        }
    }
    public void setReissueDate(Date areissueDate) {
        reissueDate = areissueDate;
    }
    public void setReissueDate(String areissueDate) {
        if (areissueDate != null && !areissueDate.equals("")) {
            reissueDate = fDate.getDate(areissueDate);
        } else
            reissueDate = null;
    }

    public String getReissueRsn() {
        return reissueRsn;
    }
    public void setReissueRsn(String areissueRsn) {
        reissueRsn = areissueRsn;
    }
    public int getValidPeriod() {
        return ValidPeriod;
    }
    public void setValidPeriod(int aValidPeriod) {
        ValidPeriod = aValidPeriod;
    }
    public void setValidPeriod(String aValidPeriod) {
        if (aValidPeriod != null && !aValidPeriod.equals("")) {
            Integer tInteger = new Integer(aValidPeriod);
            int i = tInteger.intValue();
            ValidPeriod = i;
        }
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getPasExamDate() {
        if(PasExamDate != null) {
            return fDate.getString(PasExamDate);
        } else {
            return null;
        }
    }
    public void setPasExamDate(Date aPasExamDate) {
        PasExamDate = aPasExamDate;
    }
    public void setPasExamDate(String aPasExamDate) {
        if (aPasExamDate != null && !aPasExamDate.equals("")) {
            PasExamDate = fDate.getDate(aPasExamDate);
        } else
            PasExamDate = null;
    }

    public String getExamYear() {
        return ExamYear;
    }
    public void setExamYear(String aExamYear) {
        ExamYear = aExamYear;
    }
    public String getExamTimes() {
        return ExamTimes;
    }
    public void setExamTimes(String aExamTimes) {
        ExamTimes = aExamTimes;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getValidStart() {
        if(ValidStart != null) {
            return fDate.getString(ValidStart);
        } else {
            return null;
        }
    }
    public void setValidStart(Date aValidStart) {
        ValidStart = aValidStart;
    }
    public void setValidStart(String aValidStart) {
        if (aValidStart != null && !aValidStart.equals("")) {
            ValidStart = fDate.getDate(aValidStart);
        } else
            ValidStart = null;
    }

    public String getValidEnd() {
        if(ValidEnd != null) {
            return fDate.getString(ValidEnd);
        } else {
            return null;
        }
    }
    public void setValidEnd(Date aValidEnd) {
        ValidEnd = aValidEnd;
    }
    public void setValidEnd(String aValidEnd) {
        if (aValidEnd != null && !aValidEnd.equals("")) {
            ValidEnd = fDate.getDate(aValidEnd);
        } else
            ValidEnd = null;
    }

    public String getOldQualifNo() {
        return OldQualifNo;
    }
    public void setOldQualifNo(String aOldQualifNo) {
        OldQualifNo = aOldQualifNo;
    }
    public String getQualifName() {
        return QualifName;
    }
    public void setQualifName(String aQualifName) {
        QualifName = aQualifName;
    }
    public String getState1() {
        return State1;
    }
    public void setState1(String aState1) {
        State1 = aState1;
    }

    /**
    * 使用另外一个 LAQualificationSchema 对象给 Schema 赋值
    * @param: aLAQualificationSchema LAQualificationSchema
    **/
    public void setSchema(LAQualificationSchema aLAQualificationSchema) {
        this.AgentCode = aLAQualificationSchema.getAgentCode();
        this.QualifNo = aLAQualificationSchema.getQualifNo();
        this.Idx = aLAQualificationSchema.getIdx();
        this.GrantUnit = aLAQualificationSchema.getGrantUnit();
        this.GrantDate = fDate.getDate( aLAQualificationSchema.getGrantDate());
        this.InvalidDate = fDate.getDate( aLAQualificationSchema.getInvalidDate());
        this.InvalidRsn = aLAQualificationSchema.getInvalidRsn();
        this.reissueDate = fDate.getDate( aLAQualificationSchema.getReissueDate());
        this.reissueRsn = aLAQualificationSchema.getReissueRsn();
        this.ValidPeriod = aLAQualificationSchema.getValidPeriod();
        this.State = aLAQualificationSchema.getState();
        this.PasExamDate = fDate.getDate( aLAQualificationSchema.getPasExamDate());
        this.ExamYear = aLAQualificationSchema.getExamYear();
        this.ExamTimes = aLAQualificationSchema.getExamTimes();
        this.Operator = aLAQualificationSchema.getOperator();
        this.MakeDate = fDate.getDate( aLAQualificationSchema.getMakeDate());
        this.MakeTime = aLAQualificationSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLAQualificationSchema.getModifyDate());
        this.ModifyTime = aLAQualificationSchema.getModifyTime();
        this.ValidStart = fDate.getDate( aLAQualificationSchema.getValidStart());
        this.ValidEnd = fDate.getDate( aLAQualificationSchema.getValidEnd());
        this.OldQualifNo = aLAQualificationSchema.getOldQualifNo();
        this.QualifName = aLAQualificationSchema.getQualifName();
        this.State1 = aLAQualificationSchema.getState1();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("QualifNo") == null )
                this.QualifNo = null;
            else
                this.QualifNo = rs.getString("QualifNo").trim();

            this.Idx = rs.getInt("Idx");
            if( rs.getString("GrantUnit") == null )
                this.GrantUnit = null;
            else
                this.GrantUnit = rs.getString("GrantUnit").trim();

            this.GrantDate = rs.getDate("GrantDate");
            this.InvalidDate = rs.getDate("InvalidDate");
            if( rs.getString("InvalidRsn") == null )
                this.InvalidRsn = null;
            else
                this.InvalidRsn = rs.getString("InvalidRsn").trim();

            this.reissueDate = rs.getDate("reissueDate");
            if( rs.getString("reissueRsn") == null )
                this.reissueRsn = null;
            else
                this.reissueRsn = rs.getString("reissueRsn").trim();

            this.ValidPeriod = rs.getInt("ValidPeriod");
            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            this.PasExamDate = rs.getDate("PasExamDate");
            if( rs.getString("ExamYear") == null )
                this.ExamYear = null;
            else
                this.ExamYear = rs.getString("ExamYear").trim();

            if( rs.getString("ExamTimes") == null )
                this.ExamTimes = null;
            else
                this.ExamTimes = rs.getString("ExamTimes").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            this.ValidStart = rs.getDate("ValidStart");
            this.ValidEnd = rs.getDate("ValidEnd");
            if( rs.getString("OldQualifNo") == null )
                this.OldQualifNo = null;
            else
                this.OldQualifNo = rs.getString("OldQualifNo").trim();

            if( rs.getString("QualifName") == null )
                this.QualifName = null;
            else
                this.QualifName = rs.getString("QualifName").trim();

            if( rs.getString("State1") == null )
                this.State1 = null;
            else
                this.State1 = rs.getString("State1").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAQualificationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LAQualificationSchema getSchema() {
        LAQualificationSchema aLAQualificationSchema = new LAQualificationSchema();
        aLAQualificationSchema.setSchema(this);
        return aLAQualificationSchema;
    }

    public LAQualificationDB getDB() {
        LAQualificationDB aDBOper = new LAQualificationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAQualification描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QualifNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Idx));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrantUnit)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GrantDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( InvalidDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InvalidRsn)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( reissueDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(reissueRsn)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ValidPeriod));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PasExamDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExamYear)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExamTimes)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ValidStart ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ValidEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OldQualifNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QualifName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State1));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAQualification>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            QualifNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            Idx = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3, SysConst.PACKAGESPILTER))).intValue();
            GrantUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            GrantDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER));
            InvalidDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER));
            InvalidRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            reissueDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            reissueRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ValidPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10, SysConst.PACKAGESPILTER))).intValue();
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            PasExamDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER));
            ExamYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            ExamTimes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            ValidStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER));
            ValidEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            OldQualifNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            QualifName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            State1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAQualificationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("QualifNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualifNo));
        }
        if (FCode.equalsIgnoreCase("Idx")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equalsIgnoreCase("GrantUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrantUnit));
        }
        if (FCode.equalsIgnoreCase("GrantDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGrantDate()));
        }
        if (FCode.equalsIgnoreCase("InvalidDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInvalidDate()));
        }
        if (FCode.equalsIgnoreCase("InvalidRsn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InvalidRsn));
        }
        if (FCode.equalsIgnoreCase("reissueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReissueDate()));
        }
        if (FCode.equalsIgnoreCase("reissueRsn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(reissueRsn));
        }
        if (FCode.equalsIgnoreCase("ValidPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValidPeriod));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("PasExamDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPasExamDate()));
        }
        if (FCode.equalsIgnoreCase("ExamYear")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExamYear));
        }
        if (FCode.equalsIgnoreCase("ExamTimes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExamTimes));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("ValidStart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValidStart()));
        }
        if (FCode.equalsIgnoreCase("ValidEnd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValidEnd()));
        }
        if (FCode.equalsIgnoreCase("OldQualifNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldQualifNo));
        }
        if (FCode.equalsIgnoreCase("QualifName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualifName));
        }
        if (FCode.equalsIgnoreCase("State1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State1));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(QualifNo);
                break;
            case 2:
                strFieldValue = String.valueOf(Idx);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrantUnit);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGrantDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInvalidDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(InvalidRsn);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReissueDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(reissueRsn);
                break;
            case 9:
                strFieldValue = String.valueOf(ValidPeriod);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPasExamDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ExamYear);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ExamTimes);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValidStart()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValidEnd()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(OldQualifNo);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(QualifName);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(State1);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("QualifNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualifNo = FValue.trim();
            }
            else
                QualifNo = null;
        }
        if (FCode.equalsIgnoreCase("Idx")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equalsIgnoreCase("GrantUnit")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrantUnit = FValue.trim();
            }
            else
                GrantUnit = null;
        }
        if (FCode.equalsIgnoreCase("GrantDate")) {
            if(FValue != null && !FValue.equals("")) {
                GrantDate = fDate.getDate( FValue );
            }
            else
                GrantDate = null;
        }
        if (FCode.equalsIgnoreCase("InvalidDate")) {
            if(FValue != null && !FValue.equals("")) {
                InvalidDate = fDate.getDate( FValue );
            }
            else
                InvalidDate = null;
        }
        if (FCode.equalsIgnoreCase("InvalidRsn")) {
            if( FValue != null && !FValue.equals(""))
            {
                InvalidRsn = FValue.trim();
            }
            else
                InvalidRsn = null;
        }
        if (FCode.equalsIgnoreCase("reissueDate")) {
            if(FValue != null && !FValue.equals("")) {
                reissueDate = fDate.getDate( FValue );
            }
            else
                reissueDate = null;
        }
        if (FCode.equalsIgnoreCase("reissueRsn")) {
            if( FValue != null && !FValue.equals(""))
            {
                reissueRsn = FValue.trim();
            }
            else
                reissueRsn = null;
        }
        if (FCode.equalsIgnoreCase("ValidPeriod")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                ValidPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("PasExamDate")) {
            if(FValue != null && !FValue.equals("")) {
                PasExamDate = fDate.getDate( FValue );
            }
            else
                PasExamDate = null;
        }
        if (FCode.equalsIgnoreCase("ExamYear")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExamYear = FValue.trim();
            }
            else
                ExamYear = null;
        }
        if (FCode.equalsIgnoreCase("ExamTimes")) {
            if( FValue != null && !FValue.equals(""))
            {
                ExamTimes = FValue.trim();
            }
            else
                ExamTimes = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("ValidStart")) {
            if(FValue != null && !FValue.equals("")) {
                ValidStart = fDate.getDate( FValue );
            }
            else
                ValidStart = null;
        }
        if (FCode.equalsIgnoreCase("ValidEnd")) {
            if(FValue != null && !FValue.equals("")) {
                ValidEnd = fDate.getDate( FValue );
            }
            else
                ValidEnd = null;
        }
        if (FCode.equalsIgnoreCase("OldQualifNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OldQualifNo = FValue.trim();
            }
            else
                OldQualifNo = null;
        }
        if (FCode.equalsIgnoreCase("QualifName")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualifName = FValue.trim();
            }
            else
                QualifName = null;
        }
        if (FCode.equalsIgnoreCase("State1")) {
            if( FValue != null && !FValue.equals(""))
            {
                State1 = FValue.trim();
            }
            else
                State1 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LAQualificationSchema other = (LAQualificationSchema)otherObject;
        return
            AgentCode.equals(other.getAgentCode())
            && QualifNo.equals(other.getQualifNo())
            && Idx == other.getIdx()
            && GrantUnit.equals(other.getGrantUnit())
            && fDate.getString(GrantDate).equals(other.getGrantDate())
            && fDate.getString(InvalidDate).equals(other.getInvalidDate())
            && InvalidRsn.equals(other.getInvalidRsn())
            && fDate.getString(reissueDate).equals(other.getReissueDate())
            && reissueRsn.equals(other.getReissueRsn())
            && ValidPeriod == other.getValidPeriod()
            && State.equals(other.getState())
            && fDate.getString(PasExamDate).equals(other.getPasExamDate())
            && ExamYear.equals(other.getExamYear())
            && ExamTimes.equals(other.getExamTimes())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && fDate.getString(ValidStart).equals(other.getValidStart())
            && fDate.getString(ValidEnd).equals(other.getValidEnd())
            && OldQualifNo.equals(other.getOldQualifNo())
            && QualifName.equals(other.getQualifName())
            && State1.equals(other.getState1());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCode") ) {
            return 0;
        }
        if( strFieldName.equals("QualifNo") ) {
            return 1;
        }
        if( strFieldName.equals("Idx") ) {
            return 2;
        }
        if( strFieldName.equals("GrantUnit") ) {
            return 3;
        }
        if( strFieldName.equals("GrantDate") ) {
            return 4;
        }
        if( strFieldName.equals("InvalidDate") ) {
            return 5;
        }
        if( strFieldName.equals("InvalidRsn") ) {
            return 6;
        }
        if( strFieldName.equals("reissueDate") ) {
            return 7;
        }
        if( strFieldName.equals("reissueRsn") ) {
            return 8;
        }
        if( strFieldName.equals("ValidPeriod") ) {
            return 9;
        }
        if( strFieldName.equals("State") ) {
            return 10;
        }
        if( strFieldName.equals("PasExamDate") ) {
            return 11;
        }
        if( strFieldName.equals("ExamYear") ) {
            return 12;
        }
        if( strFieldName.equals("ExamTimes") ) {
            return 13;
        }
        if( strFieldName.equals("Operator") ) {
            return 14;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 15;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 16;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 17;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 18;
        }
        if( strFieldName.equals("ValidStart") ) {
            return 19;
        }
        if( strFieldName.equals("ValidEnd") ) {
            return 20;
        }
        if( strFieldName.equals("OldQualifNo") ) {
            return 21;
        }
        if( strFieldName.equals("QualifName") ) {
            return 22;
        }
        if( strFieldName.equals("State1") ) {
            return 23;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "QualifNo";
                break;
            case 2:
                strFieldName = "Idx";
                break;
            case 3:
                strFieldName = "GrantUnit";
                break;
            case 4:
                strFieldName = "GrantDate";
                break;
            case 5:
                strFieldName = "InvalidDate";
                break;
            case 6:
                strFieldName = "InvalidRsn";
                break;
            case 7:
                strFieldName = "reissueDate";
                break;
            case 8:
                strFieldName = "reissueRsn";
                break;
            case 9:
                strFieldName = "ValidPeriod";
                break;
            case 10:
                strFieldName = "State";
                break;
            case 11:
                strFieldName = "PasExamDate";
                break;
            case 12:
                strFieldName = "ExamYear";
                break;
            case 13:
                strFieldName = "ExamTimes";
                break;
            case 14:
                strFieldName = "Operator";
                break;
            case 15:
                strFieldName = "MakeDate";
                break;
            case 16:
                strFieldName = "MakeTime";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            case 19:
                strFieldName = "ValidStart";
                break;
            case 20:
                strFieldName = "ValidEnd";
                break;
            case 21:
                strFieldName = "OldQualifNo";
                break;
            case 22:
                strFieldName = "QualifName";
                break;
            case 23:
                strFieldName = "State1";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "QUALIFNO":
                return Schema.TYPE_STRING;
            case "IDX":
                return Schema.TYPE_INT;
            case "GRANTUNIT":
                return Schema.TYPE_STRING;
            case "GRANTDATE":
                return Schema.TYPE_DATE;
            case "INVALIDDATE":
                return Schema.TYPE_DATE;
            case "INVALIDRSN":
                return Schema.TYPE_STRING;
            case "REISSUEDATE":
                return Schema.TYPE_DATE;
            case "REISSUERSN":
                return Schema.TYPE_STRING;
            case "VALIDPERIOD":
                return Schema.TYPE_INT;
            case "STATE":
                return Schema.TYPE_STRING;
            case "PASEXAMDATE":
                return Schema.TYPE_DATE;
            case "EXAMYEAR":
                return Schema.TYPE_STRING;
            case "EXAMTIMES":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "VALIDSTART":
                return Schema.TYPE_DATE;
            case "VALIDEND":
                return Schema.TYPE_DATE;
            case "OLDQUALIFNO":
                return Schema.TYPE_STRING;
            case "QUALIFNAME":
                return Schema.TYPE_STRING;
            case "STATE1":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_INT;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DATE;
            case 5:
                return Schema.TYPE_DATE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DATE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_INT;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_DATE;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DATE;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
