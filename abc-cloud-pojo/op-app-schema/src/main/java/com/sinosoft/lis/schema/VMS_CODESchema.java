/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.VMS_CODEDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: VMS_CODESchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class VMS_CODESchema implements Schema, Cloneable {
    // @Field
    /** 交易类型 */
    private String TRANSTYPE;
    /** 费用类型 */
    private String FEETYPE;
    /** 费用描述 */
    private String FEEDES;
    /** 收入类型 */
    private String INCOMETYP;
    /** 收付标识 */
    private String DC_FLAG;
    /** 上传标记 */
    private String UPLOAD_FLAG;

    public static final int FIELDNUM = 6;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public VMS_CODESchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "TRANSTYPE";
        pk[1] = "FEETYPE";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        VMS_CODESchema cloned = (VMS_CODESchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getTRANSTYPE() {
        return TRANSTYPE;
    }
    public void setTRANSTYPE(String aTRANSTYPE) {
        TRANSTYPE = aTRANSTYPE;
    }
    public String getFEETYPE() {
        return FEETYPE;
    }
    public void setFEETYPE(String aFEETYPE) {
        FEETYPE = aFEETYPE;
    }
    public String getFEEDES() {
        return FEEDES;
    }
    public void setFEEDES(String aFEEDES) {
        FEEDES = aFEEDES;
    }
    public String getINCOMETYP() {
        return INCOMETYP;
    }
    public void setINCOMETYP(String aINCOMETYP) {
        INCOMETYP = aINCOMETYP;
    }
    public String getDC_FLAG() {
        return DC_FLAG;
    }
    public void setDC_FLAG(String aDC_FLAG) {
        DC_FLAG = aDC_FLAG;
    }
    public String getUPLOAD_FLAG() {
        return UPLOAD_FLAG;
    }
    public void setUPLOAD_FLAG(String aUPLOAD_FLAG) {
        UPLOAD_FLAG = aUPLOAD_FLAG;
    }

    /**
    * 使用另外一个 VMS_CODESchema 对象给 Schema 赋值
    * @param: aVMS_CODESchema VMS_CODESchema
    **/
    public void setSchema(VMS_CODESchema aVMS_CODESchema) {
        this.TRANSTYPE = aVMS_CODESchema.getTRANSTYPE();
        this.FEETYPE = aVMS_CODESchema.getFEETYPE();
        this.FEEDES = aVMS_CODESchema.getFEEDES();
        this.INCOMETYP = aVMS_CODESchema.getINCOMETYP();
        this.DC_FLAG = aVMS_CODESchema.getDC_FLAG();
        this.UPLOAD_FLAG = aVMS_CODESchema.getUPLOAD_FLAG();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("TRANSTYPE") == null )
                this.TRANSTYPE = null;
            else
                this.TRANSTYPE = rs.getString("TRANSTYPE").trim();

            if( rs.getString("FEETYPE") == null )
                this.FEETYPE = null;
            else
                this.FEETYPE = rs.getString("FEETYPE").trim();

            if( rs.getString("FEEDES") == null )
                this.FEEDES = null;
            else
                this.FEEDES = rs.getString("FEEDES").trim();

            if( rs.getString("INCOMETYP") == null )
                this.INCOMETYP = null;
            else
                this.INCOMETYP = rs.getString("INCOMETYP").trim();

            if( rs.getString("DC_FLAG") == null )
                this.DC_FLAG = null;
            else
                this.DC_FLAG = rs.getString("DC_FLAG").trim();

            if( rs.getString("UPLOAD_FLAG") == null )
                this.UPLOAD_FLAG = null;
            else
                this.UPLOAD_FLAG = rs.getString("UPLOAD_FLAG").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_CODESchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public VMS_CODESchema getSchema() {
        VMS_CODESchema aVMS_CODESchema = new VMS_CODESchema();
        aVMS_CODESchema.setSchema(this);
        return aVMS_CODESchema;
    }

    public VMS_CODEDB getDB() {
        VMS_CODEDB aDBOper = new VMS_CODEDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_CODE描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(TRANSTYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FEETYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FEEDES)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(INCOMETYP)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DC_FLAG)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UPLOAD_FLAG));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpVMS_CODE>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            TRANSTYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            FEETYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            FEEDES = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            INCOMETYP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            DC_FLAG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            UPLOAD_FLAG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VMS_CODESchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("TRANSTYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TRANSTYPE));
        }
        if (FCode.equalsIgnoreCase("FEETYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FEETYPE));
        }
        if (FCode.equalsIgnoreCase("FEEDES")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FEEDES));
        }
        if (FCode.equalsIgnoreCase("INCOMETYP")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INCOMETYP));
        }
        if (FCode.equalsIgnoreCase("DC_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DC_FLAG));
        }
        if (FCode.equalsIgnoreCase("UPLOAD_FLAG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPLOAD_FLAG));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TRANSTYPE);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(FEETYPE);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(FEEDES);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(INCOMETYP);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DC_FLAG);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(UPLOAD_FLAG);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("TRANSTYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                TRANSTYPE = FValue.trim();
            }
            else
                TRANSTYPE = null;
        }
        if (FCode.equalsIgnoreCase("FEETYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                FEETYPE = FValue.trim();
            }
            else
                FEETYPE = null;
        }
        if (FCode.equalsIgnoreCase("FEEDES")) {
            if( FValue != null && !FValue.equals(""))
            {
                FEEDES = FValue.trim();
            }
            else
                FEEDES = null;
        }
        if (FCode.equalsIgnoreCase("INCOMETYP")) {
            if( FValue != null && !FValue.equals(""))
            {
                INCOMETYP = FValue.trim();
            }
            else
                INCOMETYP = null;
        }
        if (FCode.equalsIgnoreCase("DC_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                DC_FLAG = FValue.trim();
            }
            else
                DC_FLAG = null;
        }
        if (FCode.equalsIgnoreCase("UPLOAD_FLAG")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPLOAD_FLAG = FValue.trim();
            }
            else
                UPLOAD_FLAG = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        VMS_CODESchema other = (VMS_CODESchema)otherObject;
        return
            TRANSTYPE.equals(other.getTRANSTYPE())
            && FEETYPE.equals(other.getFEETYPE())
            && FEEDES.equals(other.getFEEDES())
            && INCOMETYP.equals(other.getINCOMETYP())
            && DC_FLAG.equals(other.getDC_FLAG())
            && UPLOAD_FLAG.equals(other.getUPLOAD_FLAG());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("TRANSTYPE") ) {
            return 0;
        }
        if( strFieldName.equals("FEETYPE") ) {
            return 1;
        }
        if( strFieldName.equals("FEEDES") ) {
            return 2;
        }
        if( strFieldName.equals("INCOMETYP") ) {
            return 3;
        }
        if( strFieldName.equals("DC_FLAG") ) {
            return 4;
        }
        if( strFieldName.equals("UPLOAD_FLAG") ) {
            return 5;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "TRANSTYPE";
                break;
            case 1:
                strFieldName = "FEETYPE";
                break;
            case 2:
                strFieldName = "FEEDES";
                break;
            case 3:
                strFieldName = "INCOMETYP";
                break;
            case 4:
                strFieldName = "DC_FLAG";
                break;
            case 5:
                strFieldName = "UPLOAD_FLAG";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "TRANSTYPE":
                return Schema.TYPE_STRING;
            case "FEETYPE":
                return Schema.TYPE_STRING;
            case "FEEDES":
                return Schema.TYPE_STRING;
            case "INCOMETYP":
                return Schema.TYPE_STRING;
            case "DC_FLAG":
                return Schema.TYPE_STRING;
            case "UPLOAD_FLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
