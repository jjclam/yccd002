/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LPEdorItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LPEdorItemSchema implements Schema, Cloneable {
    // @Field
    /** 保全受理号 */
    private String EdorAcceptNo;
    /** 批单号 */
    private String EdorNo;
    /** 批改申请号 */
    private String EdorAppNo;
    /** 批改类型 */
    private String EdorType;
    /** 批改类型显示级别 */
    private String DisplayType;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 保单险种号码 */
    private String PolNo;
    /** 管理机构 */
    private String ManageCom;
    /** 批改生效日期 */
    private Date EdorValiDate;
    /** 批改申请日期 */
    private Date EdorAppDate;
    /** 批改状态 */
    private String EdorState;
    /** 核保状态 */
    private String UWFlag;
    /** 核保人 */
    private String UWOperator;
    /** 核保完成日期 */
    private Date UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 变动的保费 */
    private double ChgPrem;
    /** 变动的保额 */
    private double ChgAmnt;
    /** 补/退费金额 */
    private double GetMoney;
    /** 补/退费利息 */
    private double GetInterest;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 保全申请原因 */
    private String AppReason;
    /** 保全变更原因编码 */
    private String EdorReasonCode;
    /** 保全变更原因 */
    private String EdorReason;
    /** 复核状态 */
    private String ApproveFlag;
    /** 复核人 */
    private String ApproveOperator;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 保全计算类型 */
    private String EdorTypeCal;
    /** 备用属性字段4 */
    private String StandbyFlag4;
    /** 备用属性字段5 */
    private String StandbyFlag5;
    /** 备用属性字段6 */
    private String StandbyFlag6;
    /** 保全变更具体原因 */
    private String EdorReasonRemark;
    /** Oa编号 */
    private String OACode;

    public static final int FIELDNUM = 42;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LPEdorItemSchema() {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "EdorAcceptNo";
        pk[1] = "EdorNo";
        pk[2] = "EdorType";
        pk[3] = "ContNo";
        pk[4] = "InsuredNo";
        pk[5] = "PolNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LPEdorItemSchema cloned = (LPEdorItemSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }
    public void setEdorAcceptNo(String aEdorAcceptNo) {
        EdorAcceptNo = aEdorAcceptNo;
    }
    public String getEdorNo() {
        return EdorNo;
    }
    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }
    public String getEdorAppNo() {
        return EdorAppNo;
    }
    public void setEdorAppNo(String aEdorAppNo) {
        EdorAppNo = aEdorAppNo;
    }
    public String getEdorType() {
        return EdorType;
    }
    public void setEdorType(String aEdorType) {
        EdorType = aEdorType;
    }
    public String getDisplayType() {
        return DisplayType;
    }
    public void setDisplayType(String aDisplayType) {
        DisplayType = aDisplayType;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getPolNo() {
        return PolNo;
    }
    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getEdorValiDate() {
        if(EdorValiDate != null) {
            return fDate.getString(EdorValiDate);
        } else {
            return null;
        }
    }
    public void setEdorValiDate(Date aEdorValiDate) {
        EdorValiDate = aEdorValiDate;
    }
    public void setEdorValiDate(String aEdorValiDate) {
        if (aEdorValiDate != null && !aEdorValiDate.equals("")) {
            EdorValiDate = fDate.getDate(aEdorValiDate);
        } else
            EdorValiDate = null;
    }

    public String getEdorAppDate() {
        if(EdorAppDate != null) {
            return fDate.getString(EdorAppDate);
        } else {
            return null;
        }
    }
    public void setEdorAppDate(Date aEdorAppDate) {
        EdorAppDate = aEdorAppDate;
    }
    public void setEdorAppDate(String aEdorAppDate) {
        if (aEdorAppDate != null && !aEdorAppDate.equals("")) {
            EdorAppDate = fDate.getDate(aEdorAppDate);
        } else
            EdorAppDate = null;
    }

    public String getEdorState() {
        return EdorState;
    }
    public void setEdorState(String aEdorState) {
        EdorState = aEdorState;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWDate() {
        if(UWDate != null) {
            return fDate.getString(UWDate);
        } else {
            return null;
        }
    }
    public void setUWDate(Date aUWDate) {
        UWDate = aUWDate;
    }
    public void setUWDate(String aUWDate) {
        if (aUWDate != null && !aUWDate.equals("")) {
            UWDate = fDate.getDate(aUWDate);
        } else
            UWDate = null;
    }

    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public double getChgPrem() {
        return ChgPrem;
    }
    public void setChgPrem(double aChgPrem) {
        ChgPrem = aChgPrem;
    }
    public void setChgPrem(String aChgPrem) {
        if (aChgPrem != null && !aChgPrem.equals("")) {
            Double tDouble = new Double(aChgPrem);
            double d = tDouble.doubleValue();
            ChgPrem = d;
        }
    }

    public double getChgAmnt() {
        return ChgAmnt;
    }
    public void setChgAmnt(double aChgAmnt) {
        ChgAmnt = aChgAmnt;
    }
    public void setChgAmnt(String aChgAmnt) {
        if (aChgAmnt != null && !aChgAmnt.equals("")) {
            Double tDouble = new Double(aChgAmnt);
            double d = tDouble.doubleValue();
            ChgAmnt = d;
        }
    }

    public double getGetMoney() {
        return GetMoney;
    }
    public void setGetMoney(double aGetMoney) {
        GetMoney = aGetMoney;
    }
    public void setGetMoney(String aGetMoney) {
        if (aGetMoney != null && !aGetMoney.equals("")) {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public double getGetInterest() {
        return GetInterest;
    }
    public void setGetInterest(double aGetInterest) {
        GetInterest = aGetInterest;
    }
    public void setGetInterest(String aGetInterest) {
        if (aGetInterest != null && !aGetInterest.equals("")) {
            Double tDouble = new Double(aGetInterest);
            double d = tDouble.doubleValue();
            GetInterest = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getAppReason() {
        return AppReason;
    }
    public void setAppReason(String aAppReason) {
        AppReason = aAppReason;
    }
    public String getEdorReasonCode() {
        return EdorReasonCode;
    }
    public void setEdorReasonCode(String aEdorReasonCode) {
        EdorReasonCode = aEdorReasonCode;
    }
    public String getEdorReason() {
        return EdorReason;
    }
    public void setEdorReason(String aEdorReason) {
        EdorReason = aEdorReason;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveOperator() {
        return ApproveOperator;
    }
    public void setApproveOperator(String aApproveOperator) {
        ApproveOperator = aApproveOperator;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getEdorTypeCal() {
        return EdorTypeCal;
    }
    public void setEdorTypeCal(String aEdorTypeCal) {
        EdorTypeCal = aEdorTypeCal;
    }
    public String getStandbyFlag4() {
        return StandbyFlag4;
    }
    public void setStandbyFlag4(String aStandbyFlag4) {
        StandbyFlag4 = aStandbyFlag4;
    }
    public String getStandbyFlag5() {
        return StandbyFlag5;
    }
    public void setStandbyFlag5(String aStandbyFlag5) {
        StandbyFlag5 = aStandbyFlag5;
    }
    public String getStandbyFlag6() {
        return StandbyFlag6;
    }
    public void setStandbyFlag6(String aStandbyFlag6) {
        StandbyFlag6 = aStandbyFlag6;
    }
    public String getEdorReasonRemark() {
        return EdorReasonRemark;
    }
    public void setEdorReasonRemark(String aEdorReasonRemark) {
        EdorReasonRemark = aEdorReasonRemark;
    }
    public String getOACode() {
        return OACode;
    }
    public void setOACode(String aOACode) {
        OACode = aOACode;
    }

    /**
    * 使用另外一个 LPEdorItemSchema 对象给 Schema 赋值
    * @param: aLPEdorItemSchema LPEdorItemSchema
    **/
    public void setSchema(LPEdorItemSchema aLPEdorItemSchema) {
        this.EdorAcceptNo = aLPEdorItemSchema.getEdorAcceptNo();
        this.EdorNo = aLPEdorItemSchema.getEdorNo();
        this.EdorAppNo = aLPEdorItemSchema.getEdorAppNo();
        this.EdorType = aLPEdorItemSchema.getEdorType();
        this.DisplayType = aLPEdorItemSchema.getDisplayType();
        this.GrpContNo = aLPEdorItemSchema.getGrpContNo();
        this.ContNo = aLPEdorItemSchema.getContNo();
        this.InsuredNo = aLPEdorItemSchema.getInsuredNo();
        this.PolNo = aLPEdorItemSchema.getPolNo();
        this.ManageCom = aLPEdorItemSchema.getManageCom();
        this.EdorValiDate = fDate.getDate( aLPEdorItemSchema.getEdorValiDate());
        this.EdorAppDate = fDate.getDate( aLPEdorItemSchema.getEdorAppDate());
        this.EdorState = aLPEdorItemSchema.getEdorState();
        this.UWFlag = aLPEdorItemSchema.getUWFlag();
        this.UWOperator = aLPEdorItemSchema.getUWOperator();
        this.UWDate = fDate.getDate( aLPEdorItemSchema.getUWDate());
        this.UWTime = aLPEdorItemSchema.getUWTime();
        this.ChgPrem = aLPEdorItemSchema.getChgPrem();
        this.ChgAmnt = aLPEdorItemSchema.getChgAmnt();
        this.GetMoney = aLPEdorItemSchema.getGetMoney();
        this.GetInterest = aLPEdorItemSchema.getGetInterest();
        this.Operator = aLPEdorItemSchema.getOperator();
        this.MakeDate = fDate.getDate( aLPEdorItemSchema.getMakeDate());
        this.MakeTime = aLPEdorItemSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLPEdorItemSchema.getModifyDate());
        this.ModifyTime = aLPEdorItemSchema.getModifyTime();
        this.AppReason = aLPEdorItemSchema.getAppReason();
        this.EdorReasonCode = aLPEdorItemSchema.getEdorReasonCode();
        this.EdorReason = aLPEdorItemSchema.getEdorReason();
        this.ApproveFlag = aLPEdorItemSchema.getApproveFlag();
        this.ApproveOperator = aLPEdorItemSchema.getApproveOperator();
        this.ApproveDate = fDate.getDate( aLPEdorItemSchema.getApproveDate());
        this.ApproveTime = aLPEdorItemSchema.getApproveTime();
        this.StandbyFlag1 = aLPEdorItemSchema.getStandbyFlag1();
        this.StandbyFlag2 = aLPEdorItemSchema.getStandbyFlag2();
        this.StandbyFlag3 = aLPEdorItemSchema.getStandbyFlag3();
        this.EdorTypeCal = aLPEdorItemSchema.getEdorTypeCal();
        this.StandbyFlag4 = aLPEdorItemSchema.getStandbyFlag4();
        this.StandbyFlag5 = aLPEdorItemSchema.getStandbyFlag5();
        this.StandbyFlag6 = aLPEdorItemSchema.getStandbyFlag6();
        this.EdorReasonRemark = aLPEdorItemSchema.getEdorReasonRemark();
        this.OACode = aLPEdorItemSchema.getOACode();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("EdorAcceptNo") == null )
                this.EdorAcceptNo = null;
            else
                this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();

            if( rs.getString("EdorNo") == null )
                this.EdorNo = null;
            else
                this.EdorNo = rs.getString("EdorNo").trim();

            if( rs.getString("EdorAppNo") == null )
                this.EdorAppNo = null;
            else
                this.EdorAppNo = rs.getString("EdorAppNo").trim();

            if( rs.getString("EdorType") == null )
                this.EdorType = null;
            else
                this.EdorType = rs.getString("EdorType").trim();

            if( rs.getString("DisplayType") == null )
                this.DisplayType = null;
            else
                this.DisplayType = rs.getString("DisplayType").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("PolNo") == null )
                this.PolNo = null;
            else
                this.PolNo = rs.getString("PolNo").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            this.EdorValiDate = rs.getDate("EdorValiDate");
            this.EdorAppDate = rs.getDate("EdorAppDate");
            if( rs.getString("EdorState") == null )
                this.EdorState = null;
            else
                this.EdorState = rs.getString("EdorState").trim();

            if( rs.getString("UWFlag") == null )
                this.UWFlag = null;
            else
                this.UWFlag = rs.getString("UWFlag").trim();

            if( rs.getString("UWOperator") == null )
                this.UWOperator = null;
            else
                this.UWOperator = rs.getString("UWOperator").trim();

            this.UWDate = rs.getDate("UWDate");
            if( rs.getString("UWTime") == null )
                this.UWTime = null;
            else
                this.UWTime = rs.getString("UWTime").trim();

            this.ChgPrem = rs.getDouble("ChgPrem");
            this.ChgAmnt = rs.getDouble("ChgAmnt");
            this.GetMoney = rs.getDouble("GetMoney");
            this.GetInterest = rs.getDouble("GetInterest");
            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("AppReason") == null )
                this.AppReason = null;
            else
                this.AppReason = rs.getString("AppReason").trim();

            if( rs.getString("EdorReasonCode") == null )
                this.EdorReasonCode = null;
            else
                this.EdorReasonCode = rs.getString("EdorReasonCode").trim();

            if( rs.getString("EdorReason") == null )
                this.EdorReason = null;
            else
                this.EdorReason = rs.getString("EdorReason").trim();

            if( rs.getString("ApproveFlag") == null )
                this.ApproveFlag = null;
            else
                this.ApproveFlag = rs.getString("ApproveFlag").trim();

            if( rs.getString("ApproveOperator") == null )
                this.ApproveOperator = null;
            else
                this.ApproveOperator = rs.getString("ApproveOperator").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("ApproveTime") == null )
                this.ApproveTime = null;
            else
                this.ApproveTime = rs.getString("ApproveTime").trim();

            if( rs.getString("StandbyFlag1") == null )
                this.StandbyFlag1 = null;
            else
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

            if( rs.getString("StandbyFlag2") == null )
                this.StandbyFlag2 = null;
            else
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

            if( rs.getString("StandbyFlag3") == null )
                this.StandbyFlag3 = null;
            else
                this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

            if( rs.getString("EdorTypeCal") == null )
                this.EdorTypeCal = null;
            else
                this.EdorTypeCal = rs.getString("EdorTypeCal").trim();

            if( rs.getString("StandbyFlag4") == null )
                this.StandbyFlag4 = null;
            else
                this.StandbyFlag4 = rs.getString("StandbyFlag4").trim();

            if( rs.getString("StandbyFlag5") == null )
                this.StandbyFlag5 = null;
            else
                this.StandbyFlag5 = rs.getString("StandbyFlag5").trim();

            if( rs.getString("StandbyFlag6") == null )
                this.StandbyFlag6 = null;
            else
                this.StandbyFlag6 = rs.getString("StandbyFlag6").trim();

            if( rs.getString("EdorReasonRemark") == null )
                this.EdorReasonRemark = null;
            else
                this.EdorReasonRemark = rs.getString("EdorReasonRemark").trim();

            if( rs.getString("OACode") == null )
                this.OACode = null;
            else
                this.OACode = rs.getString("OACode").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPEdorItemSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LPEdorItemSchema getSchema() {
        LPEdorItemSchema aLPEdorItemSchema = new LPEdorItemSchema();
        aLPEdorItemSchema.setSchema(this);
        return aLPEdorItemSchema;
    }

    public LPEdorItemDB getDB() {
        LPEdorItemDB aDBOper = new LPEdorItemDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPEdorItem描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(EdorAcceptNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorAppNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisplayType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EdorValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EdorAppDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ChgPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ChgAmnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetInterest));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorReasonCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorTypeCal)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag5)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag6)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorReasonRemark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OACode));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPEdorItem>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            EdorAppNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            DisplayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            EdorValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            EdorAppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER));
            EdorState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ChgPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18, SysConst.PACKAGESPILTER))).doubleValue();
            ChgAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19, SysConst.PACKAGESPILTER))).doubleValue();
            GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20, SysConst.PACKAGESPILTER))).doubleValue();
            GetInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            AppReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            EdorReasonCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            EdorReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            ApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            ApproveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            EdorTypeCal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            StandbyFlag4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            StandbyFlag5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            StandbyFlag6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            EdorReasonRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            OACode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPEdorItemSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equalsIgnoreCase("EdorAppNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAppNo));
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equalsIgnoreCase("DisplayType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisplayType));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("EdorValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
        }
        if (FCode.equalsIgnoreCase("EdorAppDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEdorAppDate()));
        }
        if (FCode.equalsIgnoreCase("EdorState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorState));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("ChgPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgPrem));
        }
        if (FCode.equalsIgnoreCase("ChgAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgAmnt));
        }
        if (FCode.equalsIgnoreCase("GetMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMoney));
        }
        if (FCode.equalsIgnoreCase("GetInterest")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetInterest));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("AppReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppReason));
        }
        if (FCode.equalsIgnoreCase("EdorReasonCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorReasonCode));
        }
        if (FCode.equalsIgnoreCase("EdorReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorReason));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveOperator));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("EdorTypeCal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorTypeCal));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag4));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag5));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag6));
        }
        if (FCode.equalsIgnoreCase("EdorReasonRemark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorReasonRemark));
        }
        if (FCode.equalsIgnoreCase("OACode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OACode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorAppNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DisplayType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEdorAppDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(EdorState);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(UWFlag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(UWOperator);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(UWTime);
                break;
            case 17:
                strFieldValue = String.valueOf(ChgPrem);
                break;
            case 18:
                strFieldValue = String.valueOf(ChgAmnt);
                break;
            case 19:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 20:
                strFieldValue = String.valueOf(GetInterest);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(AppReason);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(EdorReasonCode);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(EdorReason);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ApproveFlag);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(ApproveOperator);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(EdorTypeCal);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag4);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag5);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag6);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(EdorReasonRemark);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(OACode);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
                EdorAcceptNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
                EdorNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorAppNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorAppNo = FValue.trim();
            }
            else
                EdorAppNo = null;
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
                EdorType = null;
        }
        if (FCode.equalsIgnoreCase("DisplayType")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisplayType = FValue.trim();
            }
            else
                DisplayType = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
                PolNo = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("EdorValiDate")) {
            if(FValue != null && !FValue.equals("")) {
                EdorValiDate = fDate.getDate( FValue );
            }
            else
                EdorValiDate = null;
        }
        if (FCode.equalsIgnoreCase("EdorAppDate")) {
            if(FValue != null && !FValue.equals("")) {
                EdorAppDate = fDate.getDate( FValue );
            }
            else
                EdorAppDate = null;
        }
        if (FCode.equalsIgnoreCase("EdorState")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorState = FValue.trim();
            }
            else
                EdorState = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if(FValue != null && !FValue.equals("")) {
                UWDate = fDate.getDate( FValue );
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("ChgPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ChgPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("ChgAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                ChgAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetInterest")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                GetInterest = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("AppReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppReason = FValue.trim();
            }
            else
                AppReason = null;
        }
        if (FCode.equalsIgnoreCase("EdorReasonCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorReasonCode = FValue.trim();
            }
            else
                EdorReasonCode = null;
        }
        if (FCode.equalsIgnoreCase("EdorReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorReason = FValue.trim();
            }
            else
                EdorReason = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveOperator = FValue.trim();
            }
            else
                ApproveOperator = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("EdorTypeCal")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorTypeCal = FValue.trim();
            }
            else
                EdorTypeCal = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag4 = FValue.trim();
            }
            else
                StandbyFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag5 = FValue.trim();
            }
            else
                StandbyFlag5 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag6")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag6 = FValue.trim();
            }
            else
                StandbyFlag6 = null;
        }
        if (FCode.equalsIgnoreCase("EdorReasonRemark")) {
            if( FValue != null && !FValue.equals(""))
            {
                EdorReasonRemark = FValue.trim();
            }
            else
                EdorReasonRemark = null;
        }
        if (FCode.equalsIgnoreCase("OACode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OACode = FValue.trim();
            }
            else
                OACode = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LPEdorItemSchema other = (LPEdorItemSchema)otherObject;
        return
            EdorAcceptNo.equals(other.getEdorAcceptNo())
            && EdorNo.equals(other.getEdorNo())
            && EdorAppNo.equals(other.getEdorAppNo())
            && EdorType.equals(other.getEdorType())
            && DisplayType.equals(other.getDisplayType())
            && GrpContNo.equals(other.getGrpContNo())
            && ContNo.equals(other.getContNo())
            && InsuredNo.equals(other.getInsuredNo())
            && PolNo.equals(other.getPolNo())
            && ManageCom.equals(other.getManageCom())
            && fDate.getString(EdorValiDate).equals(other.getEdorValiDate())
            && fDate.getString(EdorAppDate).equals(other.getEdorAppDate())
            && EdorState.equals(other.getEdorState())
            && UWFlag.equals(other.getUWFlag())
            && UWOperator.equals(other.getUWOperator())
            && fDate.getString(UWDate).equals(other.getUWDate())
            && UWTime.equals(other.getUWTime())
            && ChgPrem == other.getChgPrem()
            && ChgAmnt == other.getChgAmnt()
            && GetMoney == other.getGetMoney()
            && GetInterest == other.getGetInterest()
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && AppReason.equals(other.getAppReason())
            && EdorReasonCode.equals(other.getEdorReasonCode())
            && EdorReason.equals(other.getEdorReason())
            && ApproveFlag.equals(other.getApproveFlag())
            && ApproveOperator.equals(other.getApproveOperator())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && ApproveTime.equals(other.getApproveTime())
            && StandbyFlag1.equals(other.getStandbyFlag1())
            && StandbyFlag2.equals(other.getStandbyFlag2())
            && StandbyFlag3.equals(other.getStandbyFlag3())
            && EdorTypeCal.equals(other.getEdorTypeCal())
            && StandbyFlag4.equals(other.getStandbyFlag4())
            && StandbyFlag5.equals(other.getStandbyFlag5())
            && StandbyFlag6.equals(other.getStandbyFlag6())
            && EdorReasonRemark.equals(other.getEdorReasonRemark())
            && OACode.equals(other.getOACode());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("EdorAcceptNo") ) {
            return 0;
        }
        if( strFieldName.equals("EdorNo") ) {
            return 1;
        }
        if( strFieldName.equals("EdorAppNo") ) {
            return 2;
        }
        if( strFieldName.equals("EdorType") ) {
            return 3;
        }
        if( strFieldName.equals("DisplayType") ) {
            return 4;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 5;
        }
        if( strFieldName.equals("ContNo") ) {
            return 6;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 7;
        }
        if( strFieldName.equals("PolNo") ) {
            return 8;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 9;
        }
        if( strFieldName.equals("EdorValiDate") ) {
            return 10;
        }
        if( strFieldName.equals("EdorAppDate") ) {
            return 11;
        }
        if( strFieldName.equals("EdorState") ) {
            return 12;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 13;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 14;
        }
        if( strFieldName.equals("UWDate") ) {
            return 15;
        }
        if( strFieldName.equals("UWTime") ) {
            return 16;
        }
        if( strFieldName.equals("ChgPrem") ) {
            return 17;
        }
        if( strFieldName.equals("ChgAmnt") ) {
            return 18;
        }
        if( strFieldName.equals("GetMoney") ) {
            return 19;
        }
        if( strFieldName.equals("GetInterest") ) {
            return 20;
        }
        if( strFieldName.equals("Operator") ) {
            return 21;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 22;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 24;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 25;
        }
        if( strFieldName.equals("AppReason") ) {
            return 26;
        }
        if( strFieldName.equals("EdorReasonCode") ) {
            return 27;
        }
        if( strFieldName.equals("EdorReason") ) {
            return 28;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 29;
        }
        if( strFieldName.equals("ApproveOperator") ) {
            return 30;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 31;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 32;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 33;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 34;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 35;
        }
        if( strFieldName.equals("EdorTypeCal") ) {
            return 36;
        }
        if( strFieldName.equals("StandbyFlag4") ) {
            return 37;
        }
        if( strFieldName.equals("StandbyFlag5") ) {
            return 38;
        }
        if( strFieldName.equals("StandbyFlag6") ) {
            return 39;
        }
        if( strFieldName.equals("EdorReasonRemark") ) {
            return 40;
        }
        if( strFieldName.equals("OACode") ) {
            return 41;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "EdorAcceptNo";
                break;
            case 1:
                strFieldName = "EdorNo";
                break;
            case 2:
                strFieldName = "EdorAppNo";
                break;
            case 3:
                strFieldName = "EdorType";
                break;
            case 4:
                strFieldName = "DisplayType";
                break;
            case 5:
                strFieldName = "GrpContNo";
                break;
            case 6:
                strFieldName = "ContNo";
                break;
            case 7:
                strFieldName = "InsuredNo";
                break;
            case 8:
                strFieldName = "PolNo";
                break;
            case 9:
                strFieldName = "ManageCom";
                break;
            case 10:
                strFieldName = "EdorValiDate";
                break;
            case 11:
                strFieldName = "EdorAppDate";
                break;
            case 12:
                strFieldName = "EdorState";
                break;
            case 13:
                strFieldName = "UWFlag";
                break;
            case 14:
                strFieldName = "UWOperator";
                break;
            case 15:
                strFieldName = "UWDate";
                break;
            case 16:
                strFieldName = "UWTime";
                break;
            case 17:
                strFieldName = "ChgPrem";
                break;
            case 18:
                strFieldName = "ChgAmnt";
                break;
            case 19:
                strFieldName = "GetMoney";
                break;
            case 20:
                strFieldName = "GetInterest";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "AppReason";
                break;
            case 27:
                strFieldName = "EdorReasonCode";
                break;
            case 28:
                strFieldName = "EdorReason";
                break;
            case 29:
                strFieldName = "ApproveFlag";
                break;
            case 30:
                strFieldName = "ApproveOperator";
                break;
            case 31:
                strFieldName = "ApproveDate";
                break;
            case 32:
                strFieldName = "ApproveTime";
                break;
            case 33:
                strFieldName = "StandbyFlag1";
                break;
            case 34:
                strFieldName = "StandbyFlag2";
                break;
            case 35:
                strFieldName = "StandbyFlag3";
                break;
            case 36:
                strFieldName = "EdorTypeCal";
                break;
            case 37:
                strFieldName = "StandbyFlag4";
                break;
            case 38:
                strFieldName = "StandbyFlag5";
                break;
            case 39:
                strFieldName = "StandbyFlag6";
                break;
            case 40:
                strFieldName = "EdorReasonRemark";
                break;
            case 41:
                strFieldName = "OACode";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "EDORACCEPTNO":
                return Schema.TYPE_STRING;
            case "EDORNO":
                return Schema.TYPE_STRING;
            case "EDORAPPNO":
                return Schema.TYPE_STRING;
            case "EDORTYPE":
                return Schema.TYPE_STRING;
            case "DISPLAYTYPE":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "POLNO":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "EDORVALIDATE":
                return Schema.TYPE_DATE;
            case "EDORAPPDATE":
                return Schema.TYPE_DATE;
            case "EDORSTATE":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_DATE;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "CHGPREM":
                return Schema.TYPE_DOUBLE;
            case "CHGAMNT":
                return Schema.TYPE_DOUBLE;
            case "GETMONEY":
                return Schema.TYPE_DOUBLE;
            case "GETINTEREST":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "APPREASON":
                return Schema.TYPE_STRING;
            case "EDORREASONCODE":
                return Schema.TYPE_STRING;
            case "EDORREASON":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVEOPERATOR":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "EDORTYPECAL":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG6":
                return Schema.TYPE_STRING;
            case "EDORREASONREMARK":
                return Schema.TYPE_STRING;
            case "OACODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_DATE;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DOUBLE;
            case 18:
                return Schema.TYPE_DOUBLE;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_DATE;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_DATE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_DATE;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
