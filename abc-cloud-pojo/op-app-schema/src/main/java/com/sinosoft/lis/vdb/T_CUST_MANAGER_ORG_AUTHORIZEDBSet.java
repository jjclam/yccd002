/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.T_CUST_MANAGER_ORG_AUTHORIZESet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: T_CUST_MANAGER_ORG_AUTHORIZEDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */
public class T_CUST_MANAGER_ORG_AUTHORIZEDBSet extends T_CUST_MANAGER_ORG_AUTHORIZESet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public T_CUST_MANAGER_ORG_AUTHORIZEDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"T_CUST_MANAGER_ORG_AUTHORIZE");
        mflag = true;
    }

    public T_CUST_MANAGER_ORG_AUTHORIZEDBSet() {
        db = new DBOper( "T_CUST_MANAGER_ORG_AUTHORIZE" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM T_CUST_MANAGER_ORG_AUTHORIZE WHERE  1=1  AND CUST_MANAGER_AUTHORIZE_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getCUST_MANAGER_AUTHORIZE_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE T_CUST_MANAGER_ORG_AUTHORIZE SET  CUST_MANAGER_AUTHORIZE_ID = ? , MNGORG_ID = ? , AGENCY_ID = ? , CUST_MANAGER_ID = ? , INSERT_OPER = ? , INSERT_CONSIGNOR = ? , INSERT_TIME = ? , UPDATE_OPER = ? , UPDATE_CONSIGNOR = ? , UPDATE_TIME = ? WHERE  1=1  AND CUST_MANAGER_AUTHORIZE_ID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getCUST_MANAGER_AUTHORIZE_ID());
            pstmt.setLong(2, this.get(i).getMNGORG_ID());
            pstmt.setLong(3, this.get(i).getAGENCY_ID());
            pstmt.setLong(4, this.get(i).getCUST_MANAGER_ID());
            if(this.get(i).getINSERT_OPER() == null || this.get(i).getINSERT_OPER().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getINSERT_OPER());
            }
            if(this.get(i).getINSERT_CONSIGNOR() == null || this.get(i).getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getINSERT_CONSIGNOR());
            }
            if(this.get(i).getINSERT_TIME() == null || this.get(i).getINSERT_TIME().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getINSERT_TIME()));
            }
            if(this.get(i).getUPDATE_OPER() == null || this.get(i).getUPDATE_OPER().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getUPDATE_OPER());
            }
            if(this.get(i).getUPDATE_CONSIGNOR() == null || this.get(i).getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getUPDATE_CONSIGNOR());
            }
            if(this.get(i).getUPDATE_TIME() == null || this.get(i).getUPDATE_TIME().equals("null")) {
                pstmt.setDate(10,null);
            } else {
                pstmt.setDate(10, Date.valueOf(this.get(i).getUPDATE_TIME()));
            }
            // set where condition
            pstmt.setLong(11, this.get(i).getCUST_MANAGER_AUTHORIZE_ID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO T_CUST_MANAGER_ORG_AUTHORIZE VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getCUST_MANAGER_AUTHORIZE_ID());
            pstmt.setLong(2, this.get(i).getMNGORG_ID());
            pstmt.setLong(3, this.get(i).getAGENCY_ID());
            pstmt.setLong(4, this.get(i).getCUST_MANAGER_ID());
            if(this.get(i).getINSERT_OPER() == null || this.get(i).getINSERT_OPER().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getINSERT_OPER());
            }
            if(this.get(i).getINSERT_CONSIGNOR() == null || this.get(i).getINSERT_CONSIGNOR().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getINSERT_CONSIGNOR());
            }
            if(this.get(i).getINSERT_TIME() == null || this.get(i).getINSERT_TIME().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getINSERT_TIME()));
            }
            if(this.get(i).getUPDATE_OPER() == null || this.get(i).getUPDATE_OPER().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getUPDATE_OPER());
            }
            if(this.get(i).getUPDATE_CONSIGNOR() == null || this.get(i).getUPDATE_CONSIGNOR().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getUPDATE_CONSIGNOR());
            }
            if(this.get(i).getUPDATE_TIME() == null || this.get(i).getUPDATE_TIME().equals("null")) {
                pstmt.setDate(10,null);
            } else {
                pstmt.setDate(10, Date.valueOf(this.get(i).getUPDATE_TIME()));
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "T_CUST_MANAGER_ORG_AUTHORIZEDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
