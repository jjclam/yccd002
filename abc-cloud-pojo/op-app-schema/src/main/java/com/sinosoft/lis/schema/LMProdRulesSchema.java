/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMProdRulesDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LMProdRulesSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-10-09
 */
public class LMProdRulesSchema implements Schema, Cloneable {
    // @Field
    /** 规则编码 */
    private String RulesCode;
    /** 规则类型 */
    private String RulesType;
    /** 规则类型说明 */
    private String RulesTypeName;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 险种名称 */
    private String RiskName;
    /** 规则适用销售渠道 */
    private String RulesChnl;
    /** 规则适用管理机构 */
    private String RulesMngCom;
    /** 规则扩展属性1 */
    private String RulesProperty1;
    /** 规则扩展属性2 */
    private String RulesProperty2;
    /** 规则扩展属性3 */
    private String RulesProperty3;
    /** 规则扩展属性5 */
    private String RulesProperty5;
    /** 规则扩展属性4 */
    private String RulesProperty4;
    /** 规则扩展属性6 */
    private String RulesProperty6;
    /** 规则算法 */
    private String CalCode;
    /** 规则执行顺序 */
    private int RulesOrder;
    /** 提示信息 */
    private String Message;
    /** 显示提示信息标记 */
    private String ShowMsgFlag;
    /** 备注 */
    private String Remark;
    /** 规则执行标记 */
    private String CalType;
    /** 复核标记 */
    private String ReviewFlag;

    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMProdRulesSchema() {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "RulesCode";
        pk[1] = "RulesType";
        pk[2] = "RiskCode";
        pk[3] = "RulesChnl";
        pk[4] = "RulesMngCom";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMProdRulesSchema cloned = (LMProdRulesSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRulesCode() {
        return RulesCode;
    }
    public void setRulesCode(String aRulesCode) {
        RulesCode = aRulesCode;
    }
    public String getRulesType() {
        return RulesType;
    }
    public void setRulesType(String aRulesType) {
        RulesType = aRulesType;
    }
    public String getRulesTypeName() {
        return RulesTypeName;
    }
    public void setRulesTypeName(String aRulesTypeName) {
        RulesTypeName = aRulesTypeName;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVersion() {
        return RiskVersion;
    }
    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getRulesChnl() {
        return RulesChnl;
    }
    public void setRulesChnl(String aRulesChnl) {
        RulesChnl = aRulesChnl;
    }
    public String getRulesMngCom() {
        return RulesMngCom;
    }
    public void setRulesMngCom(String aRulesMngCom) {
        RulesMngCom = aRulesMngCom;
    }
    public String getRulesProperty1() {
        return RulesProperty1;
    }
    public void setRulesProperty1(String aRulesProperty1) {
        RulesProperty1 = aRulesProperty1;
    }
    public String getRulesProperty2() {
        return RulesProperty2;
    }
    public void setRulesProperty2(String aRulesProperty2) {
        RulesProperty2 = aRulesProperty2;
    }
    public String getRulesProperty3() {
        return RulesProperty3;
    }
    public void setRulesProperty3(String aRulesProperty3) {
        RulesProperty3 = aRulesProperty3;
    }
    public String getRulesProperty5() {
        return RulesProperty5;
    }
    public void setRulesProperty5(String aRulesProperty5) {
        RulesProperty5 = aRulesProperty5;
    }
    public String getRulesProperty4() {
        return RulesProperty4;
    }
    public void setRulesProperty4(String aRulesProperty4) {
        RulesProperty4 = aRulesProperty4;
    }
    public String getRulesProperty6() {
        return RulesProperty6;
    }
    public void setRulesProperty6(String aRulesProperty6) {
        RulesProperty6 = aRulesProperty6;
    }
    public String getCalCode() {
        return CalCode;
    }
    public void setCalCode(String aCalCode) {
        CalCode = aCalCode;
    }
    public int getRulesOrder() {
        return RulesOrder;
    }
    public void setRulesOrder(int aRulesOrder) {
        RulesOrder = aRulesOrder;
    }
    public void setRulesOrder(String aRulesOrder) {
        if (aRulesOrder != null && !aRulesOrder.equals("")) {
            Integer tInteger = new Integer(aRulesOrder);
            int i = tInteger.intValue();
            RulesOrder = i;
        }
    }

    public String getMessage() {
        return Message;
    }
    public void setMessage(String aMessage) {
        Message = aMessage;
    }
    public String getShowMsgFlag() {
        return ShowMsgFlag;
    }
    public void setShowMsgFlag(String aShowMsgFlag) {
        ShowMsgFlag = aShowMsgFlag;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getCalType() {
        return CalType;
    }
    public void setCalType(String aCalType) {
        CalType = aCalType;
    }
    public String getReviewFlag() {
        return ReviewFlag;
    }
    public void setReviewFlag(String aReviewFlag) {
        ReviewFlag = aReviewFlag;
    }

    /**
    * 使用另外一个 LMProdRulesSchema 对象给 Schema 赋值
    * @param: aLMProdRulesSchema LMProdRulesSchema
    **/
    public void setSchema(LMProdRulesSchema aLMProdRulesSchema) {
        this.RulesCode = aLMProdRulesSchema.getRulesCode();
        this.RulesType = aLMProdRulesSchema.getRulesType();
        this.RulesTypeName = aLMProdRulesSchema.getRulesTypeName();
        this.RiskCode = aLMProdRulesSchema.getRiskCode();
        this.RiskVersion = aLMProdRulesSchema.getRiskVersion();
        this.RiskName = aLMProdRulesSchema.getRiskName();
        this.RulesChnl = aLMProdRulesSchema.getRulesChnl();
        this.RulesMngCom = aLMProdRulesSchema.getRulesMngCom();
        this.RulesProperty1 = aLMProdRulesSchema.getRulesProperty1();
        this.RulesProperty2 = aLMProdRulesSchema.getRulesProperty2();
        this.RulesProperty3 = aLMProdRulesSchema.getRulesProperty3();
        this.RulesProperty5 = aLMProdRulesSchema.getRulesProperty5();
        this.RulesProperty4 = aLMProdRulesSchema.getRulesProperty4();
        this.RulesProperty6 = aLMProdRulesSchema.getRulesProperty6();
        this.CalCode = aLMProdRulesSchema.getCalCode();
        this.RulesOrder = aLMProdRulesSchema.getRulesOrder();
        this.Message = aLMProdRulesSchema.getMessage();
        this.ShowMsgFlag = aLMProdRulesSchema.getShowMsgFlag();
        this.Remark = aLMProdRulesSchema.getRemark();
        this.CalType = aLMProdRulesSchema.getCalType();
        this.ReviewFlag = aLMProdRulesSchema.getReviewFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RulesCode") == null )
                this.RulesCode = null;
            else
                this.RulesCode = rs.getString("RulesCode").trim();

            if( rs.getString("RulesType") == null )
                this.RulesType = null;
            else
                this.RulesType = rs.getString("RulesType").trim();

            if( rs.getString("RulesTypeName") == null )
                this.RulesTypeName = null;
            else
                this.RulesTypeName = rs.getString("RulesTypeName").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("RiskVersion") == null )
                this.RiskVersion = null;
            else
                this.RiskVersion = rs.getString("RiskVersion").trim();

            if( rs.getString("RiskName") == null )
                this.RiskName = null;
            else
                this.RiskName = rs.getString("RiskName").trim();

            if( rs.getString("RulesChnl") == null )
                this.RulesChnl = null;
            else
                this.RulesChnl = rs.getString("RulesChnl").trim();

            if( rs.getString("RulesMngCom") == null )
                this.RulesMngCom = null;
            else
                this.RulesMngCom = rs.getString("RulesMngCom").trim();

            if( rs.getString("RulesProperty1") == null )
                this.RulesProperty1 = null;
            else
                this.RulesProperty1 = rs.getString("RulesProperty1").trim();

            if( rs.getString("RulesProperty2") == null )
                this.RulesProperty2 = null;
            else
                this.RulesProperty2 = rs.getString("RulesProperty2").trim();

            if( rs.getString("RulesProperty3") == null )
                this.RulesProperty3 = null;
            else
                this.RulesProperty3 = rs.getString("RulesProperty3").trim();

            if( rs.getString("RulesProperty5") == null )
                this.RulesProperty5 = null;
            else
                this.RulesProperty5 = rs.getString("RulesProperty5").trim();

            if( rs.getString("RulesProperty4") == null )
                this.RulesProperty4 = null;
            else
                this.RulesProperty4 = rs.getString("RulesProperty4").trim();

            if( rs.getString("RulesProperty6") == null )
                this.RulesProperty6 = null;
            else
                this.RulesProperty6 = rs.getString("RulesProperty6").trim();

            if( rs.getString("CalCode") == null )
                this.CalCode = null;
            else
                this.CalCode = rs.getString("CalCode").trim();

            this.RulesOrder = rs.getInt("RulesOrder");
            if( rs.getString("Message") == null )
                this.Message = null;
            else
                this.Message = rs.getString("Message").trim();

            if( rs.getString("ShowMsgFlag") == null )
                this.ShowMsgFlag = null;
            else
                this.ShowMsgFlag = rs.getString("ShowMsgFlag").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("CalType") == null )
                this.CalType = null;
            else
                this.CalType = rs.getString("CalType").trim();

            if( rs.getString("ReviewFlag") == null )
                this.ReviewFlag = null;
            else
                this.ReviewFlag = rs.getString("ReviewFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMProdRulesSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMProdRulesSchema getSchema() {
        LMProdRulesSchema aLMProdRulesSchema = new LMProdRulesSchema();
        aLMProdRulesSchema.setSchema(this);
        return aLMProdRulesSchema;
    }

    public LMProdRulesDB getDB() {
        LMProdRulesDB aDBOper = new LMProdRulesDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMProdRules描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RulesCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesTypeName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesMngCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesProperty1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesProperty2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesProperty3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesProperty5)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesProperty4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesProperty6)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RulesOrder));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Message)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShowMsgFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReviewFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMProdRules>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RulesCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RulesType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            RulesTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            RulesChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            RulesMngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            RulesProperty1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            RulesProperty2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            RulesProperty3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            RulesProperty5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            RulesProperty4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            RulesProperty6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            RulesOrder = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,16, SysConst.PACKAGESPILTER))).intValue();
            Message = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ShowMsgFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            CalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            ReviewFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMProdRulesSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RulesCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesCode));
        }
        if (FCode.equalsIgnoreCase("RulesType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesType));
        }
        if (FCode.equalsIgnoreCase("RulesTypeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesTypeName));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("RulesChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesChnl));
        }
        if (FCode.equalsIgnoreCase("RulesMngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesMngCom));
        }
        if (FCode.equalsIgnoreCase("RulesProperty1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty1));
        }
        if (FCode.equalsIgnoreCase("RulesProperty2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty2));
        }
        if (FCode.equalsIgnoreCase("RulesProperty3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty3));
        }
        if (FCode.equalsIgnoreCase("RulesProperty5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty5));
        }
        if (FCode.equalsIgnoreCase("RulesProperty4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty4));
        }
        if (FCode.equalsIgnoreCase("RulesProperty6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesProperty6));
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
        }
        if (FCode.equalsIgnoreCase("RulesOrder")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesOrder));
        }
        if (FCode.equalsIgnoreCase("Message")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Message));
        }
        if (FCode.equalsIgnoreCase("ShowMsgFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShowMsgFlag));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("CalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalType));
        }
        if (FCode.equalsIgnoreCase("ReviewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReviewFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RulesCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RulesType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RulesTypeName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RulesChnl);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RulesMngCom);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RulesProperty1);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(RulesProperty2);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(RulesProperty3);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(RulesProperty5);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(RulesProperty4);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(RulesProperty6);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 15:
                strFieldValue = String.valueOf(RulesOrder);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Message);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ShowMsgFlag);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(CalType);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ReviewFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RulesCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesCode = FValue.trim();
            }
            else
                RulesCode = null;
        }
        if (FCode.equalsIgnoreCase("RulesType")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesType = FValue.trim();
            }
            else
                RulesType = null;
        }
        if (FCode.equalsIgnoreCase("RulesTypeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesTypeName = FValue.trim();
            }
            else
                RulesTypeName = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
                RiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("RulesChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesChnl = FValue.trim();
            }
            else
                RulesChnl = null;
        }
        if (FCode.equalsIgnoreCase("RulesMngCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesMngCom = FValue.trim();
            }
            else
                RulesMngCom = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty1")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty1 = FValue.trim();
            }
            else
                RulesProperty1 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty2")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty2 = FValue.trim();
            }
            else
                RulesProperty2 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty3")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty3 = FValue.trim();
            }
            else
                RulesProperty3 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty5")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty5 = FValue.trim();
            }
            else
                RulesProperty5 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty4")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty4 = FValue.trim();
            }
            else
                RulesProperty4 = null;
        }
        if (FCode.equalsIgnoreCase("RulesProperty6")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesProperty6 = FValue.trim();
            }
            else
                RulesProperty6 = null;
        }
        if (FCode.equalsIgnoreCase("CalCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
                CalCode = null;
        }
        if (FCode.equalsIgnoreCase("RulesOrder")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                RulesOrder = i;
            }
        }
        if (FCode.equalsIgnoreCase("Message")) {
            if( FValue != null && !FValue.equals(""))
            {
                Message = FValue.trim();
            }
            else
                Message = null;
        }
        if (FCode.equalsIgnoreCase("ShowMsgFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShowMsgFlag = FValue.trim();
            }
            else
                ShowMsgFlag = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("CalType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalType = FValue.trim();
            }
            else
                CalType = null;
        }
        if (FCode.equalsIgnoreCase("ReviewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReviewFlag = FValue.trim();
            }
            else
                ReviewFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMProdRulesSchema other = (LMProdRulesSchema)otherObject;
        return
            RulesCode.equals(other.getRulesCode())
            && RulesType.equals(other.getRulesType())
            && RulesTypeName.equals(other.getRulesTypeName())
            && RiskCode.equals(other.getRiskCode())
            && RiskVersion.equals(other.getRiskVersion())
            && RiskName.equals(other.getRiskName())
            && RulesChnl.equals(other.getRulesChnl())
            && RulesMngCom.equals(other.getRulesMngCom())
            && RulesProperty1.equals(other.getRulesProperty1())
            && RulesProperty2.equals(other.getRulesProperty2())
            && RulesProperty3.equals(other.getRulesProperty3())
            && RulesProperty5.equals(other.getRulesProperty5())
            && RulesProperty4.equals(other.getRulesProperty4())
            && RulesProperty6.equals(other.getRulesProperty6())
            && CalCode.equals(other.getCalCode())
            && RulesOrder == other.getRulesOrder()
            && Message.equals(other.getMessage())
            && ShowMsgFlag.equals(other.getShowMsgFlag())
            && Remark.equals(other.getRemark())
            && CalType.equals(other.getCalType())
            && ReviewFlag.equals(other.getReviewFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RulesCode") ) {
            return 0;
        }
        if( strFieldName.equals("RulesType") ) {
            return 1;
        }
        if( strFieldName.equals("RulesTypeName") ) {
            return 2;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 3;
        }
        if( strFieldName.equals("RiskVersion") ) {
            return 4;
        }
        if( strFieldName.equals("RiskName") ) {
            return 5;
        }
        if( strFieldName.equals("RulesChnl") ) {
            return 6;
        }
        if( strFieldName.equals("RulesMngCom") ) {
            return 7;
        }
        if( strFieldName.equals("RulesProperty1") ) {
            return 8;
        }
        if( strFieldName.equals("RulesProperty2") ) {
            return 9;
        }
        if( strFieldName.equals("RulesProperty3") ) {
            return 10;
        }
        if( strFieldName.equals("RulesProperty5") ) {
            return 11;
        }
        if( strFieldName.equals("RulesProperty4") ) {
            return 12;
        }
        if( strFieldName.equals("RulesProperty6") ) {
            return 13;
        }
        if( strFieldName.equals("CalCode") ) {
            return 14;
        }
        if( strFieldName.equals("RulesOrder") ) {
            return 15;
        }
        if( strFieldName.equals("Message") ) {
            return 16;
        }
        if( strFieldName.equals("ShowMsgFlag") ) {
            return 17;
        }
        if( strFieldName.equals("Remark") ) {
            return 18;
        }
        if( strFieldName.equals("CalType") ) {
            return 19;
        }
        if( strFieldName.equals("ReviewFlag") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RulesCode";
                break;
            case 1:
                strFieldName = "RulesType";
                break;
            case 2:
                strFieldName = "RulesTypeName";
                break;
            case 3:
                strFieldName = "RiskCode";
                break;
            case 4:
                strFieldName = "RiskVersion";
                break;
            case 5:
                strFieldName = "RiskName";
                break;
            case 6:
                strFieldName = "RulesChnl";
                break;
            case 7:
                strFieldName = "RulesMngCom";
                break;
            case 8:
                strFieldName = "RulesProperty1";
                break;
            case 9:
                strFieldName = "RulesProperty2";
                break;
            case 10:
                strFieldName = "RulesProperty3";
                break;
            case 11:
                strFieldName = "RulesProperty5";
                break;
            case 12:
                strFieldName = "RulesProperty4";
                break;
            case 13:
                strFieldName = "RulesProperty6";
                break;
            case 14:
                strFieldName = "CalCode";
                break;
            case 15:
                strFieldName = "RulesOrder";
                break;
            case 16:
                strFieldName = "Message";
                break;
            case 17:
                strFieldName = "ShowMsgFlag";
                break;
            case 18:
                strFieldName = "Remark";
                break;
            case 19:
                strFieldName = "CalType";
                break;
            case 20:
                strFieldName = "ReviewFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RULESCODE":
                return Schema.TYPE_STRING;
            case "RULESTYPE":
                return Schema.TYPE_STRING;
            case "RULESTYPENAME":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVERSION":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "RULESCHNL":
                return Schema.TYPE_STRING;
            case "RULESMNGCOM":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY1":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY2":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY3":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY5":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY4":
                return Schema.TYPE_STRING;
            case "RULESPROPERTY6":
                return Schema.TYPE_STRING;
            case "CALCODE":
                return Schema.TYPE_STRING;
            case "RULESORDER":
                return Schema.TYPE_INT;
            case "MESSAGE":
                return Schema.TYPE_STRING;
            case "SHOWMSGFLAG":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "CALTYPE":
                return Schema.TYPE_STRING;
            case "REVIEWFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_INT;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "LMProdRulesSchema {" +
            "RulesCode="+RulesCode +
            ", RulesType="+RulesType +
            ", RulesTypeName="+RulesTypeName +
            ", RiskCode="+RiskCode +
            ", RiskVersion="+RiskVersion +
            ", RiskName="+RiskName +
            ", RulesChnl="+RulesChnl +
            ", RulesMngCom="+RulesMngCom +
            ", RulesProperty1="+RulesProperty1 +
            ", RulesProperty2="+RulesProperty2 +
            ", RulesProperty3="+RulesProperty3 +
            ", RulesProperty5="+RulesProperty5 +
            ", RulesProperty4="+RulesProperty4 +
            ", RulesProperty6="+RulesProperty6 +
            ", CalCode="+CalCode +
            ", RulesOrder="+RulesOrder +
            ", Message="+Message +
            ", ShowMsgFlag="+ShowMsgFlag +
            ", Remark="+Remark +
            ", CalType="+CalType +
            ", ReviewFlag="+ReviewFlag +"}";
    }
}
