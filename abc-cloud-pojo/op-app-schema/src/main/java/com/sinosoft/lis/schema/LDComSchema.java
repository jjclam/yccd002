/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDComDB;

/**
 * <p>ClassName: LDComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-03-21
 */
public class LDComSchema implements Schema, Cloneable {
    // @Field
    /** 机构编码 */
    private String ComCode;
    /** 对外公布的机构代码 */
    private String OutComCode;
    /** 机构名称 */
    private String Name;
    /** 短名称 */
    private String ShortName;
    /** 机构地址 */
    private String Address;
    /** 机构邮编 */
    private String ZipCode;
    /** 机构电话 */
    private String Phone;
    /** 机构传真 */
    private String Fax;
    /** Email */
    private String EMail;
    /** 网址 */
    private String WebAddress;
    /** 主管人姓名 */
    private String SatrapName;
    /** 对应保监办代码 */
    private String InsuMonitorCode;
    /** 保险公司id */
    private String InsureID;
    /** 标识码 */
    private String SignID;
    /** 行政区划代码 */
    private String RegionalismCode;
    /** 公司性质 */
    private String ComNature;
    /** 校验码 */
    private String ValidCode;
    /** 标志 */
    private String Sign;
    /** 机构所在市规模 */
    private String ComCitySize;
    /** 服务机构名称 */
    private String ServiceName;
    /** 服务机构编码 */
    private String ServiceNo;
    /** 服务电话 */
    private String ServicePhone;
    /** 服务投递地址 */
    private String ServicePostAddress;
    /** 机构级别 */
    private String COMGRADE;
    /** 机构地区类型 */
    private String COMAREATYPE;
    /** 上级机构代码 */
    private String UPCOMCODE;
    /** Isdirunder */
    private String ISDIRUNDER;
    /** Comareatype1 */
    private String COMAREATYPE1;
    /** Province */
    private String PROVINCE;
    /** City */
    private String CITY;
    /** County */
    private String COUNTY;
    /** Findb */
    private String FINDB;
    /** Letterservicename */
    private String LETTERSERVICENAME;
    /** Comstate */
    private String COMSTATE;
    /** Compreparedate */
    private Date COMPREPAREDATE;
    /** Supmanagedate */
    private Date SUPMANAGEDATE;
    /** Supapprovaldate */
    private Date SUPAPPROVALDATE;
    /** Theopeningdate */
    private Date THEOPENINGDATE;
    /** Comreplycanceldate */
    private Date COMREPLYCANCELDATE;
    /** Supreplycanceldate */
    private Date SUPREPLYCANCELDATE;
    /** Buspapercanceldate */
    private Date BUSPAPERCANCELDATE;

    public static final int FIELDNUM = 41;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDComSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ComCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDComSchema cloned = (LDComSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getComCode() {
        return ComCode;
    }
    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }
    public String getOutComCode() {
        return OutComCode;
    }
    public void setOutComCode(String aOutComCode) {
        OutComCode = aOutComCode;
    }
    public String getName() {
        return Name;
    }
    public void setName(String aName) {
        Name = aName;
    }
    public String getShortName() {
        return ShortName;
    }
    public void setShortName(String aShortName) {
        ShortName = aShortName;
    }
    public String getAddress() {
        return Address;
    }
    public void setAddress(String aAddress) {
        Address = aAddress;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getWebAddress() {
        return WebAddress;
    }
    public void setWebAddress(String aWebAddress) {
        WebAddress = aWebAddress;
    }
    public String getSatrapName() {
        return SatrapName;
    }
    public void setSatrapName(String aSatrapName) {
        SatrapName = aSatrapName;
    }
    public String getInsuMonitorCode() {
        return InsuMonitorCode;
    }
    public void setInsuMonitorCode(String aInsuMonitorCode) {
        InsuMonitorCode = aInsuMonitorCode;
    }
    public String getInsureID() {
        return InsureID;
    }
    public void setInsureID(String aInsureID) {
        InsureID = aInsureID;
    }
    public String getSignID() {
        return SignID;
    }
    public void setSignID(String aSignID) {
        SignID = aSignID;
    }
    public String getRegionalismCode() {
        return RegionalismCode;
    }
    public void setRegionalismCode(String aRegionalismCode) {
        RegionalismCode = aRegionalismCode;
    }
    public String getComNature() {
        return ComNature;
    }
    public void setComNature(String aComNature) {
        ComNature = aComNature;
    }
    public String getValidCode() {
        return ValidCode;
    }
    public void setValidCode(String aValidCode) {
        ValidCode = aValidCode;
    }
    public String getSign() {
        return Sign;
    }
    public void setSign(String aSign) {
        Sign = aSign;
    }
    public String getComCitySize() {
        return ComCitySize;
    }
    public void setComCitySize(String aComCitySize) {
        ComCitySize = aComCitySize;
    }
    public String getServiceName() {
        return ServiceName;
    }
    public void setServiceName(String aServiceName) {
        ServiceName = aServiceName;
    }
    public String getServiceNo() {
        return ServiceNo;
    }
    public void setServiceNo(String aServiceNo) {
        ServiceNo = aServiceNo;
    }
    public String getServicePhone() {
        return ServicePhone;
    }
    public void setServicePhone(String aServicePhone) {
        ServicePhone = aServicePhone;
    }
    public String getServicePostAddress() {
        return ServicePostAddress;
    }
    public void setServicePostAddress(String aServicePostAddress) {
        ServicePostAddress = aServicePostAddress;
    }
    public String getCOMGRADE() {
        return COMGRADE;
    }
    public void setCOMGRADE(String aCOMGRADE) {
        COMGRADE = aCOMGRADE;
    }
    public String getCOMAREATYPE() {
        return COMAREATYPE;
    }
    public void setCOMAREATYPE(String aCOMAREATYPE) {
        COMAREATYPE = aCOMAREATYPE;
    }
    public String getUPCOMCODE() {
        return UPCOMCODE;
    }
    public void setUPCOMCODE(String aUPCOMCODE) {
        UPCOMCODE = aUPCOMCODE;
    }
    public String getISDIRUNDER() {
        return ISDIRUNDER;
    }
    public void setISDIRUNDER(String aISDIRUNDER) {
        ISDIRUNDER = aISDIRUNDER;
    }
    public String getCOMAREATYPE1() {
        return COMAREATYPE1;
    }
    public void setCOMAREATYPE1(String aCOMAREATYPE1) {
        COMAREATYPE1 = aCOMAREATYPE1;
    }
    public String getPROVINCE() {
        return PROVINCE;
    }
    public void setPROVINCE(String aPROVINCE) {
        PROVINCE = aPROVINCE;
    }
    public String getCITY() {
        return CITY;
    }
    public void setCITY(String aCITY) {
        CITY = aCITY;
    }
    public String getCOUNTY() {
        return COUNTY;
    }
    public void setCOUNTY(String aCOUNTY) {
        COUNTY = aCOUNTY;
    }
    public String getFINDB() {
        return FINDB;
    }
    public void setFINDB(String aFINDB) {
        FINDB = aFINDB;
    }
    public String getLETTERSERVICENAME() {
        return LETTERSERVICENAME;
    }
    public void setLETTERSERVICENAME(String aLETTERSERVICENAME) {
        LETTERSERVICENAME = aLETTERSERVICENAME;
    }
    public String getCOMSTATE() {
        return COMSTATE;
    }
    public void setCOMSTATE(String aCOMSTATE) {
        COMSTATE = aCOMSTATE;
    }
    public String getCOMPREPAREDATE() {
        if(COMPREPAREDATE != null) {
            return fDate.getString(COMPREPAREDATE);
        } else {
            return null;
        }
    }
    public void setCOMPREPAREDATE(Date aCOMPREPAREDATE) {
        COMPREPAREDATE = aCOMPREPAREDATE;
    }
    public void setCOMPREPAREDATE(String aCOMPREPAREDATE) {
        if (aCOMPREPAREDATE != null && !aCOMPREPAREDATE.equals("")) {
            COMPREPAREDATE = fDate.getDate(aCOMPREPAREDATE);
        } else
            COMPREPAREDATE = null;
    }

    public String getSUPMANAGEDATE() {
        if(SUPMANAGEDATE != null) {
            return fDate.getString(SUPMANAGEDATE);
        } else {
            return null;
        }
    }
    public void setSUPMANAGEDATE(Date aSUPMANAGEDATE) {
        SUPMANAGEDATE = aSUPMANAGEDATE;
    }
    public void setSUPMANAGEDATE(String aSUPMANAGEDATE) {
        if (aSUPMANAGEDATE != null && !aSUPMANAGEDATE.equals("")) {
            SUPMANAGEDATE = fDate.getDate(aSUPMANAGEDATE);
        } else
            SUPMANAGEDATE = null;
    }

    public String getSUPAPPROVALDATE() {
        if(SUPAPPROVALDATE != null) {
            return fDate.getString(SUPAPPROVALDATE);
        } else {
            return null;
        }
    }
    public void setSUPAPPROVALDATE(Date aSUPAPPROVALDATE) {
        SUPAPPROVALDATE = aSUPAPPROVALDATE;
    }
    public void setSUPAPPROVALDATE(String aSUPAPPROVALDATE) {
        if (aSUPAPPROVALDATE != null && !aSUPAPPROVALDATE.equals("")) {
            SUPAPPROVALDATE = fDate.getDate(aSUPAPPROVALDATE);
        } else
            SUPAPPROVALDATE = null;
    }

    public String getTHEOPENINGDATE() {
        if(THEOPENINGDATE != null) {
            return fDate.getString(THEOPENINGDATE);
        } else {
            return null;
        }
    }
    public void setTHEOPENINGDATE(Date aTHEOPENINGDATE) {
        THEOPENINGDATE = aTHEOPENINGDATE;
    }
    public void setTHEOPENINGDATE(String aTHEOPENINGDATE) {
        if (aTHEOPENINGDATE != null && !aTHEOPENINGDATE.equals("")) {
            THEOPENINGDATE = fDate.getDate(aTHEOPENINGDATE);
        } else
            THEOPENINGDATE = null;
    }

    public String getCOMREPLYCANCELDATE() {
        if(COMREPLYCANCELDATE != null) {
            return fDate.getString(COMREPLYCANCELDATE);
        } else {
            return null;
        }
    }
    public void setCOMREPLYCANCELDATE(Date aCOMREPLYCANCELDATE) {
        COMREPLYCANCELDATE = aCOMREPLYCANCELDATE;
    }
    public void setCOMREPLYCANCELDATE(String aCOMREPLYCANCELDATE) {
        if (aCOMREPLYCANCELDATE != null && !aCOMREPLYCANCELDATE.equals("")) {
            COMREPLYCANCELDATE = fDate.getDate(aCOMREPLYCANCELDATE);
        } else
            COMREPLYCANCELDATE = null;
    }

    public String getSUPREPLYCANCELDATE() {
        if(SUPREPLYCANCELDATE != null) {
            return fDate.getString(SUPREPLYCANCELDATE);
        } else {
            return null;
        }
    }
    public void setSUPREPLYCANCELDATE(Date aSUPREPLYCANCELDATE) {
        SUPREPLYCANCELDATE = aSUPREPLYCANCELDATE;
    }
    public void setSUPREPLYCANCELDATE(String aSUPREPLYCANCELDATE) {
        if (aSUPREPLYCANCELDATE != null && !aSUPREPLYCANCELDATE.equals("")) {
            SUPREPLYCANCELDATE = fDate.getDate(aSUPREPLYCANCELDATE);
        } else
            SUPREPLYCANCELDATE = null;
    }

    public String getBUSPAPERCANCELDATE() {
        if(BUSPAPERCANCELDATE != null) {
            return fDate.getString(BUSPAPERCANCELDATE);
        } else {
            return null;
        }
    }
    public void setBUSPAPERCANCELDATE(Date aBUSPAPERCANCELDATE) {
        BUSPAPERCANCELDATE = aBUSPAPERCANCELDATE;
    }
    public void setBUSPAPERCANCELDATE(String aBUSPAPERCANCELDATE) {
        if (aBUSPAPERCANCELDATE != null && !aBUSPAPERCANCELDATE.equals("")) {
            BUSPAPERCANCELDATE = fDate.getDate(aBUSPAPERCANCELDATE);
        } else
            BUSPAPERCANCELDATE = null;
    }


    /**
    * 使用另外一个 LDComSchema 对象给 Schema 赋值
    * @param: aLDComSchema LDComSchema
    **/
    public void setSchema(LDComSchema aLDComSchema) {
        this.ComCode = aLDComSchema.getComCode();
        this.OutComCode = aLDComSchema.getOutComCode();
        this.Name = aLDComSchema.getName();
        this.ShortName = aLDComSchema.getShortName();
        this.Address = aLDComSchema.getAddress();
        this.ZipCode = aLDComSchema.getZipCode();
        this.Phone = aLDComSchema.getPhone();
        this.Fax = aLDComSchema.getFax();
        this.EMail = aLDComSchema.getEMail();
        this.WebAddress = aLDComSchema.getWebAddress();
        this.SatrapName = aLDComSchema.getSatrapName();
        this.InsuMonitorCode = aLDComSchema.getInsuMonitorCode();
        this.InsureID = aLDComSchema.getInsureID();
        this.SignID = aLDComSchema.getSignID();
        this.RegionalismCode = aLDComSchema.getRegionalismCode();
        this.ComNature = aLDComSchema.getComNature();
        this.ValidCode = aLDComSchema.getValidCode();
        this.Sign = aLDComSchema.getSign();
        this.ComCitySize = aLDComSchema.getComCitySize();
        this.ServiceName = aLDComSchema.getServiceName();
        this.ServiceNo = aLDComSchema.getServiceNo();
        this.ServicePhone = aLDComSchema.getServicePhone();
        this.ServicePostAddress = aLDComSchema.getServicePostAddress();
        this.COMGRADE = aLDComSchema.getCOMGRADE();
        this.COMAREATYPE = aLDComSchema.getCOMAREATYPE();
        this.UPCOMCODE = aLDComSchema.getUPCOMCODE();
        this.ISDIRUNDER = aLDComSchema.getISDIRUNDER();
        this.COMAREATYPE1 = aLDComSchema.getCOMAREATYPE1();
        this.PROVINCE = aLDComSchema.getPROVINCE();
        this.CITY = aLDComSchema.getCITY();
        this.COUNTY = aLDComSchema.getCOUNTY();
        this.FINDB = aLDComSchema.getFINDB();
        this.LETTERSERVICENAME = aLDComSchema.getLETTERSERVICENAME();
        this.COMSTATE = aLDComSchema.getCOMSTATE();
        this.COMPREPAREDATE = fDate.getDate( aLDComSchema.getCOMPREPAREDATE());
        this.SUPMANAGEDATE = fDate.getDate( aLDComSchema.getSUPMANAGEDATE());
        this.SUPAPPROVALDATE = fDate.getDate( aLDComSchema.getSUPAPPROVALDATE());
        this.THEOPENINGDATE = fDate.getDate( aLDComSchema.getTHEOPENINGDATE());
        this.COMREPLYCANCELDATE = fDate.getDate( aLDComSchema.getCOMREPLYCANCELDATE());
        this.SUPREPLYCANCELDATE = fDate.getDate( aLDComSchema.getSUPREPLYCANCELDATE());
        this.BUSPAPERCANCELDATE = fDate.getDate( aLDComSchema.getBUSPAPERCANCELDATE());
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ComCode") == null )
                this.ComCode = null;
            else
                this.ComCode = rs.getString("ComCode").trim();

            if( rs.getString("OutComCode") == null )
                this.OutComCode = null;
            else
                this.OutComCode = rs.getString("OutComCode").trim();

            if( rs.getString("Name") == null )
                this.Name = null;
            else
                this.Name = rs.getString("Name").trim();

            if( rs.getString("ShortName") == null )
                this.ShortName = null;
            else
                this.ShortName = rs.getString("ShortName").trim();

            if( rs.getString("Address") == null )
                this.Address = null;
            else
                this.Address = rs.getString("Address").trim();

            if( rs.getString("ZipCode") == null )
                this.ZipCode = null;
            else
                this.ZipCode = rs.getString("ZipCode").trim();

            if( rs.getString("Phone") == null )
                this.Phone = null;
            else
                this.Phone = rs.getString("Phone").trim();

            if( rs.getString("Fax") == null )
                this.Fax = null;
            else
                this.Fax = rs.getString("Fax").trim();

            if( rs.getString("EMail") == null )
                this.EMail = null;
            else
                this.EMail = rs.getString("EMail").trim();

            if( rs.getString("WebAddress") == null )
                this.WebAddress = null;
            else
                this.WebAddress = rs.getString("WebAddress").trim();

            if( rs.getString("SatrapName") == null )
                this.SatrapName = null;
            else
                this.SatrapName = rs.getString("SatrapName").trim();

            if( rs.getString("InsuMonitorCode") == null )
                this.InsuMonitorCode = null;
            else
                this.InsuMonitorCode = rs.getString("InsuMonitorCode").trim();

            if( rs.getString("InsureID") == null )
                this.InsureID = null;
            else
                this.InsureID = rs.getString("InsureID").trim();

            if( rs.getString("SignID") == null )
                this.SignID = null;
            else
                this.SignID = rs.getString("SignID").trim();

            if( rs.getString("RegionalismCode") == null )
                this.RegionalismCode = null;
            else
                this.RegionalismCode = rs.getString("RegionalismCode").trim();

            if( rs.getString("ComNature") == null )
                this.ComNature = null;
            else
                this.ComNature = rs.getString("ComNature").trim();

            if( rs.getString("ValidCode") == null )
                this.ValidCode = null;
            else
                this.ValidCode = rs.getString("ValidCode").trim();

            if( rs.getString("Sign") == null )
                this.Sign = null;
            else
                this.Sign = rs.getString("Sign").trim();

            if( rs.getString("ComCitySize") == null )
                this.ComCitySize = null;
            else
                this.ComCitySize = rs.getString("ComCitySize").trim();

            if( rs.getString("ServiceName") == null )
                this.ServiceName = null;
            else
                this.ServiceName = rs.getString("ServiceName").trim();

            if( rs.getString("ServiceNo") == null )
                this.ServiceNo = null;
            else
                this.ServiceNo = rs.getString("ServiceNo").trim();

            if( rs.getString("ServicePhone") == null )
                this.ServicePhone = null;
            else
                this.ServicePhone = rs.getString("ServicePhone").trim();

            if( rs.getString("ServicePostAddress") == null )
                this.ServicePostAddress = null;
            else
                this.ServicePostAddress = rs.getString("ServicePostAddress").trim();

            if( rs.getString("COMGRADE") == null )
                this.COMGRADE = null;
            else
                this.COMGRADE = rs.getString("COMGRADE").trim();

            if( rs.getString("COMAREATYPE") == null )
                this.COMAREATYPE = null;
            else
                this.COMAREATYPE = rs.getString("COMAREATYPE").trim();

            if( rs.getString("UPCOMCODE") == null )
                this.UPCOMCODE = null;
            else
                this.UPCOMCODE = rs.getString("UPCOMCODE").trim();

            if( rs.getString("ISDIRUNDER") == null )
                this.ISDIRUNDER = null;
            else
                this.ISDIRUNDER = rs.getString("ISDIRUNDER").trim();

            if( rs.getString("COMAREATYPE1") == null )
                this.COMAREATYPE1 = null;
            else
                this.COMAREATYPE1 = rs.getString("COMAREATYPE1").trim();

            if( rs.getString("PROVINCE") == null )
                this.PROVINCE = null;
            else
                this.PROVINCE = rs.getString("PROVINCE").trim();

            if( rs.getString("CITY") == null )
                this.CITY = null;
            else
                this.CITY = rs.getString("CITY").trim();

            if( rs.getString("COUNTY") == null )
                this.COUNTY = null;
            else
                this.COUNTY = rs.getString("COUNTY").trim();

            if( rs.getString("FINDB") == null )
                this.FINDB = null;
            else
                this.FINDB = rs.getString("FINDB").trim();

            if( rs.getString("LETTERSERVICENAME") == null )
                this.LETTERSERVICENAME = null;
            else
                this.LETTERSERVICENAME = rs.getString("LETTERSERVICENAME").trim();

            if( rs.getString("COMSTATE") == null )
                this.COMSTATE = null;
            else
                this.COMSTATE = rs.getString("COMSTATE").trim();

            this.COMPREPAREDATE = rs.getDate("COMPREPAREDATE");
            this.SUPMANAGEDATE = rs.getDate("SUPMANAGEDATE");
            this.SUPAPPROVALDATE = rs.getDate("SUPAPPROVALDATE");
            this.THEOPENINGDATE = rs.getDate("THEOPENINGDATE");
            this.COMREPLYCANCELDATE = rs.getDate("COMREPLYCANCELDATE");
            this.SUPREPLYCANCELDATE = rs.getDate("SUPREPLYCANCELDATE");
            this.BUSPAPERCANCELDATE = rs.getDate("BUSPAPERCANCELDATE");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDComSchema getSchema() {
        LDComSchema aLDComSchema = new LDComSchema();
        aLDComSchema.setSchema(this);
        return aLDComSchema;
    }

    public LDComDB getDB() {
        LDComDB aDBOper = new LDComDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDCom描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OutComCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ShortName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WebAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SatrapName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuMonitorCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsureID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RegionalismCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComNature)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ValidCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sign)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCitySize)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServicePhone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServicePostAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(COMGRADE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(COMAREATYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UPCOMCODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ISDIRUNDER)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(COMAREATYPE1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PROVINCE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CITY)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(COUNTY)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FINDB)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LETTERSERVICENAME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(COMSTATE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( COMPREPAREDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SUPMANAGEDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SUPAPPROVALDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( THEOPENINGDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( COMREPLYCANCELDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SUPREPLYCANCELDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( BUSPAPERCANCELDATE )));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDCom>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            OutComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            ShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            SatrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            InsuMonitorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            InsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            SignID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            RegionalismCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            ComNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            ValidCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            Sign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            ComCitySize = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            ServiceName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            ServiceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            ServicePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            ServicePostAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            COMGRADE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            COMAREATYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            UPCOMCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            ISDIRUNDER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            COMAREATYPE1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            PROVINCE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            CITY = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            COUNTY = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            FINDB = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            LETTERSERVICENAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            COMSTATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            COMPREPAREDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,SysConst.PACKAGESPILTER));
            SUPMANAGEDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,SysConst.PACKAGESPILTER));
            SUPAPPROVALDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
            THEOPENINGDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
            COMREPLYCANCELDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
            SUPREPLYCANCELDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
            BUSPAPERCANCELDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDComSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equalsIgnoreCase("OutComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutComCode));
        }
        if (FCode.equalsIgnoreCase("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equalsIgnoreCase("ShortName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShortName));
        }
        if (FCode.equalsIgnoreCase("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("WebAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
        }
        if (FCode.equalsIgnoreCase("SatrapName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SatrapName));
        }
        if (FCode.equalsIgnoreCase("InsuMonitorCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuMonitorCode));
        }
        if (FCode.equalsIgnoreCase("InsureID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsureID));
        }
        if (FCode.equalsIgnoreCase("SignID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignID));
        }
        if (FCode.equalsIgnoreCase("RegionalismCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RegionalismCode));
        }
        if (FCode.equalsIgnoreCase("ComNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComNature));
        }
        if (FCode.equalsIgnoreCase("ValidCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ValidCode));
        }
        if (FCode.equalsIgnoreCase("Sign")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sign));
        }
        if (FCode.equalsIgnoreCase("ComCitySize")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCitySize));
        }
        if (FCode.equalsIgnoreCase("ServiceName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceName));
        }
        if (FCode.equalsIgnoreCase("ServiceNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceNo));
        }
        if (FCode.equalsIgnoreCase("ServicePhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePhone));
        }
        if (FCode.equalsIgnoreCase("ServicePostAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePostAddress));
        }
        if (FCode.equalsIgnoreCase("COMGRADE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMGRADE));
        }
        if (FCode.equalsIgnoreCase("COMAREATYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMAREATYPE));
        }
        if (FCode.equalsIgnoreCase("UPCOMCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPCOMCODE));
        }
        if (FCode.equalsIgnoreCase("ISDIRUNDER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ISDIRUNDER));
        }
        if (FCode.equalsIgnoreCase("COMAREATYPE1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMAREATYPE1));
        }
        if (FCode.equalsIgnoreCase("PROVINCE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PROVINCE));
        }
        if (FCode.equalsIgnoreCase("CITY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CITY));
        }
        if (FCode.equalsIgnoreCase("COUNTY")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COUNTY));
        }
        if (FCode.equalsIgnoreCase("FINDB")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FINDB));
        }
        if (FCode.equalsIgnoreCase("LETTERSERVICENAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LETTERSERVICENAME));
        }
        if (FCode.equalsIgnoreCase("COMSTATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMSTATE));
        }
        if (FCode.equalsIgnoreCase("COMPREPAREDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCOMPREPAREDATE()));
        }
        if (FCode.equalsIgnoreCase("SUPMANAGEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSUPMANAGEDATE()));
        }
        if (FCode.equalsIgnoreCase("SUPAPPROVALDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSUPAPPROVALDATE()));
        }
        if (FCode.equalsIgnoreCase("THEOPENINGDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTHEOPENINGDATE()));
        }
        if (FCode.equalsIgnoreCase("COMREPLYCANCELDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCOMREPLYCANCELDATE()));
        }
        if (FCode.equalsIgnoreCase("SUPREPLYCANCELDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSUPREPLYCANCELDATE()));
        }
        if (FCode.equalsIgnoreCase("BUSPAPERCANCELDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBUSPAPERCANCELDATE()));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OutComCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ShortName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Fax);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(WebAddress);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(SatrapName);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(InsuMonitorCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(InsureID);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(SignID);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(RegionalismCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ComNature);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ValidCode);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Sign);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(ComCitySize);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ServiceName);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ServiceNo);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ServicePhone);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ServicePostAddress);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(COMGRADE);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(COMAREATYPE);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(UPCOMCODE);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(ISDIRUNDER);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(COMAREATYPE1);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(PROVINCE);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(CITY);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(COUNTY);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(FINDB);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(LETTERSERVICENAME);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(COMSTATE);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCOMPREPAREDATE()));
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSUPMANAGEDATE()));
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSUPAPPROVALDATE()));
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTHEOPENINGDATE()));
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCOMREPLYCANCELDATE()));
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSUPREPLYCANCELDATE()));
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBUSPAPERCANCELDATE()));
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
                ComCode = null;
        }
        if (FCode.equalsIgnoreCase("OutComCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutComCode = FValue.trim();
            }
            else
                OutComCode = null;
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if( FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
                Name = null;
        }
        if (FCode.equalsIgnoreCase("ShortName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ShortName = FValue.trim();
            }
            else
                ShortName = null;
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if( FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
                Address = null;
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
                ZipCode = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("WebAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                WebAddress = FValue.trim();
            }
            else
                WebAddress = null;
        }
        if (FCode.equalsIgnoreCase("SatrapName")) {
            if( FValue != null && !FValue.equals(""))
            {
                SatrapName = FValue.trim();
            }
            else
                SatrapName = null;
        }
        if (FCode.equalsIgnoreCase("InsuMonitorCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuMonitorCode = FValue.trim();
            }
            else
                InsuMonitorCode = null;
        }
        if (FCode.equalsIgnoreCase("InsureID")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsureID = FValue.trim();
            }
            else
                InsureID = null;
        }
        if (FCode.equalsIgnoreCase("SignID")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignID = FValue.trim();
            }
            else
                SignID = null;
        }
        if (FCode.equalsIgnoreCase("RegionalismCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RegionalismCode = FValue.trim();
            }
            else
                RegionalismCode = null;
        }
        if (FCode.equalsIgnoreCase("ComNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComNature = FValue.trim();
            }
            else
                ComNature = null;
        }
        if (FCode.equalsIgnoreCase("ValidCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ValidCode = FValue.trim();
            }
            else
                ValidCode = null;
        }
        if (FCode.equalsIgnoreCase("Sign")) {
            if( FValue != null && !FValue.equals(""))
            {
                Sign = FValue.trim();
            }
            else
                Sign = null;
        }
        if (FCode.equalsIgnoreCase("ComCitySize")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCitySize = FValue.trim();
            }
            else
                ComCitySize = null;
        }
        if (FCode.equalsIgnoreCase("ServiceName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceName = FValue.trim();
            }
            else
                ServiceName = null;
        }
        if (FCode.equalsIgnoreCase("ServiceNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServiceNo = FValue.trim();
            }
            else
                ServiceNo = null;
        }
        if (FCode.equalsIgnoreCase("ServicePhone")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServicePhone = FValue.trim();
            }
            else
                ServicePhone = null;
        }
        if (FCode.equalsIgnoreCase("ServicePostAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                ServicePostAddress = FValue.trim();
            }
            else
                ServicePostAddress = null;
        }
        if (FCode.equalsIgnoreCase("COMGRADE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMGRADE = FValue.trim();
            }
            else
                COMGRADE = null;
        }
        if (FCode.equalsIgnoreCase("COMAREATYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMAREATYPE = FValue.trim();
            }
            else
                COMAREATYPE = null;
        }
        if (FCode.equalsIgnoreCase("UPCOMCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPCOMCODE = FValue.trim();
            }
            else
                UPCOMCODE = null;
        }
        if (FCode.equalsIgnoreCase("ISDIRUNDER")) {
            if( FValue != null && !FValue.equals(""))
            {
                ISDIRUNDER = FValue.trim();
            }
            else
                ISDIRUNDER = null;
        }
        if (FCode.equalsIgnoreCase("COMAREATYPE1")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMAREATYPE1 = FValue.trim();
            }
            else
                COMAREATYPE1 = null;
        }
        if (FCode.equalsIgnoreCase("PROVINCE")) {
            if( FValue != null && !FValue.equals(""))
            {
                PROVINCE = FValue.trim();
            }
            else
                PROVINCE = null;
        }
        if (FCode.equalsIgnoreCase("CITY")) {
            if( FValue != null && !FValue.equals(""))
            {
                CITY = FValue.trim();
            }
            else
                CITY = null;
        }
        if (FCode.equalsIgnoreCase("COUNTY")) {
            if( FValue != null && !FValue.equals(""))
            {
                COUNTY = FValue.trim();
            }
            else
                COUNTY = null;
        }
        if (FCode.equalsIgnoreCase("FINDB")) {
            if( FValue != null && !FValue.equals(""))
            {
                FINDB = FValue.trim();
            }
            else
                FINDB = null;
        }
        if (FCode.equalsIgnoreCase("LETTERSERVICENAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                LETTERSERVICENAME = FValue.trim();
            }
            else
                LETTERSERVICENAME = null;
        }
        if (FCode.equalsIgnoreCase("COMSTATE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMSTATE = FValue.trim();
            }
            else
                COMSTATE = null;
        }
        if (FCode.equalsIgnoreCase("COMPREPAREDATE")) {
            if(FValue != null && !FValue.equals("")) {
                COMPREPAREDATE = fDate.getDate( FValue );
            }
            else
                COMPREPAREDATE = null;
        }
        if (FCode.equalsIgnoreCase("SUPMANAGEDATE")) {
            if(FValue != null && !FValue.equals("")) {
                SUPMANAGEDATE = fDate.getDate( FValue );
            }
            else
                SUPMANAGEDATE = null;
        }
        if (FCode.equalsIgnoreCase("SUPAPPROVALDATE")) {
            if(FValue != null && !FValue.equals("")) {
                SUPAPPROVALDATE = fDate.getDate( FValue );
            }
            else
                SUPAPPROVALDATE = null;
        }
        if (FCode.equalsIgnoreCase("THEOPENINGDATE")) {
            if(FValue != null && !FValue.equals("")) {
                THEOPENINGDATE = fDate.getDate( FValue );
            }
            else
                THEOPENINGDATE = null;
        }
        if (FCode.equalsIgnoreCase("COMREPLYCANCELDATE")) {
            if(FValue != null && !FValue.equals("")) {
                COMREPLYCANCELDATE = fDate.getDate( FValue );
            }
            else
                COMREPLYCANCELDATE = null;
        }
        if (FCode.equalsIgnoreCase("SUPREPLYCANCELDATE")) {
            if(FValue != null && !FValue.equals("")) {
                SUPREPLYCANCELDATE = fDate.getDate( FValue );
            }
            else
                SUPREPLYCANCELDATE = null;
        }
        if (FCode.equalsIgnoreCase("BUSPAPERCANCELDATE")) {
            if(FValue != null && !FValue.equals("")) {
                BUSPAPERCANCELDATE = fDate.getDate( FValue );
            }
            else
                BUSPAPERCANCELDATE = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDComSchema other = (LDComSchema)otherObject;
        return
            ComCode.equals(other.getComCode())
            && OutComCode.equals(other.getOutComCode())
            && Name.equals(other.getName())
            && ShortName.equals(other.getShortName())
            && Address.equals(other.getAddress())
            && ZipCode.equals(other.getZipCode())
            && Phone.equals(other.getPhone())
            && Fax.equals(other.getFax())
            && EMail.equals(other.getEMail())
            && WebAddress.equals(other.getWebAddress())
            && SatrapName.equals(other.getSatrapName())
            && InsuMonitorCode.equals(other.getInsuMonitorCode())
            && InsureID.equals(other.getInsureID())
            && SignID.equals(other.getSignID())
            && RegionalismCode.equals(other.getRegionalismCode())
            && ComNature.equals(other.getComNature())
            && ValidCode.equals(other.getValidCode())
            && Sign.equals(other.getSign())
            && ComCitySize.equals(other.getComCitySize())
            && ServiceName.equals(other.getServiceName())
            && ServiceNo.equals(other.getServiceNo())
            && ServicePhone.equals(other.getServicePhone())
            && ServicePostAddress.equals(other.getServicePostAddress())
            && COMGRADE.equals(other.getCOMGRADE())
            && COMAREATYPE.equals(other.getCOMAREATYPE())
            && UPCOMCODE.equals(other.getUPCOMCODE())
            && ISDIRUNDER.equals(other.getISDIRUNDER())
            && COMAREATYPE1.equals(other.getCOMAREATYPE1())
            && PROVINCE.equals(other.getPROVINCE())
            && CITY.equals(other.getCITY())
            && COUNTY.equals(other.getCOUNTY())
            && FINDB.equals(other.getFINDB())
            && LETTERSERVICENAME.equals(other.getLETTERSERVICENAME())
            && COMSTATE.equals(other.getCOMSTATE())
            && fDate.getString(COMPREPAREDATE).equals(other.getCOMPREPAREDATE())
            && fDate.getString(SUPMANAGEDATE).equals(other.getSUPMANAGEDATE())
            && fDate.getString(SUPAPPROVALDATE).equals(other.getSUPAPPROVALDATE())
            && fDate.getString(THEOPENINGDATE).equals(other.getTHEOPENINGDATE())
            && fDate.getString(COMREPLYCANCELDATE).equals(other.getCOMREPLYCANCELDATE())
            && fDate.getString(SUPREPLYCANCELDATE).equals(other.getSUPREPLYCANCELDATE())
            && fDate.getString(BUSPAPERCANCELDATE).equals(other.getBUSPAPERCANCELDATE());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ComCode") ) {
            return 0;
        }
        if( strFieldName.equals("OutComCode") ) {
            return 1;
        }
        if( strFieldName.equals("Name") ) {
            return 2;
        }
        if( strFieldName.equals("ShortName") ) {
            return 3;
        }
        if( strFieldName.equals("Address") ) {
            return 4;
        }
        if( strFieldName.equals("ZipCode") ) {
            return 5;
        }
        if( strFieldName.equals("Phone") ) {
            return 6;
        }
        if( strFieldName.equals("Fax") ) {
            return 7;
        }
        if( strFieldName.equals("EMail") ) {
            return 8;
        }
        if( strFieldName.equals("WebAddress") ) {
            return 9;
        }
        if( strFieldName.equals("SatrapName") ) {
            return 10;
        }
        if( strFieldName.equals("InsuMonitorCode") ) {
            return 11;
        }
        if( strFieldName.equals("InsureID") ) {
            return 12;
        }
        if( strFieldName.equals("SignID") ) {
            return 13;
        }
        if( strFieldName.equals("RegionalismCode") ) {
            return 14;
        }
        if( strFieldName.equals("ComNature") ) {
            return 15;
        }
        if( strFieldName.equals("ValidCode") ) {
            return 16;
        }
        if( strFieldName.equals("Sign") ) {
            return 17;
        }
        if( strFieldName.equals("ComCitySize") ) {
            return 18;
        }
        if( strFieldName.equals("ServiceName") ) {
            return 19;
        }
        if( strFieldName.equals("ServiceNo") ) {
            return 20;
        }
        if( strFieldName.equals("ServicePhone") ) {
            return 21;
        }
        if( strFieldName.equals("ServicePostAddress") ) {
            return 22;
        }
        if( strFieldName.equals("COMGRADE") ) {
            return 23;
        }
        if( strFieldName.equals("COMAREATYPE") ) {
            return 24;
        }
        if( strFieldName.equals("UPCOMCODE") ) {
            return 25;
        }
        if( strFieldName.equals("ISDIRUNDER") ) {
            return 26;
        }
        if( strFieldName.equals("COMAREATYPE1") ) {
            return 27;
        }
        if( strFieldName.equals("PROVINCE") ) {
            return 28;
        }
        if( strFieldName.equals("CITY") ) {
            return 29;
        }
        if( strFieldName.equals("COUNTY") ) {
            return 30;
        }
        if( strFieldName.equals("FINDB") ) {
            return 31;
        }
        if( strFieldName.equals("LETTERSERVICENAME") ) {
            return 32;
        }
        if( strFieldName.equals("COMSTATE") ) {
            return 33;
        }
        if( strFieldName.equals("COMPREPAREDATE") ) {
            return 34;
        }
        if( strFieldName.equals("SUPMANAGEDATE") ) {
            return 35;
        }
        if( strFieldName.equals("SUPAPPROVALDATE") ) {
            return 36;
        }
        if( strFieldName.equals("THEOPENINGDATE") ) {
            return 37;
        }
        if( strFieldName.equals("COMREPLYCANCELDATE") ) {
            return 38;
        }
        if( strFieldName.equals("SUPREPLYCANCELDATE") ) {
            return 39;
        }
        if( strFieldName.equals("BUSPAPERCANCELDATE") ) {
            return 40;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ComCode";
                break;
            case 1:
                strFieldName = "OutComCode";
                break;
            case 2:
                strFieldName = "Name";
                break;
            case 3:
                strFieldName = "ShortName";
                break;
            case 4:
                strFieldName = "Address";
                break;
            case 5:
                strFieldName = "ZipCode";
                break;
            case 6:
                strFieldName = "Phone";
                break;
            case 7:
                strFieldName = "Fax";
                break;
            case 8:
                strFieldName = "EMail";
                break;
            case 9:
                strFieldName = "WebAddress";
                break;
            case 10:
                strFieldName = "SatrapName";
                break;
            case 11:
                strFieldName = "InsuMonitorCode";
                break;
            case 12:
                strFieldName = "InsureID";
                break;
            case 13:
                strFieldName = "SignID";
                break;
            case 14:
                strFieldName = "RegionalismCode";
                break;
            case 15:
                strFieldName = "ComNature";
                break;
            case 16:
                strFieldName = "ValidCode";
                break;
            case 17:
                strFieldName = "Sign";
                break;
            case 18:
                strFieldName = "ComCitySize";
                break;
            case 19:
                strFieldName = "ServiceName";
                break;
            case 20:
                strFieldName = "ServiceNo";
                break;
            case 21:
                strFieldName = "ServicePhone";
                break;
            case 22:
                strFieldName = "ServicePostAddress";
                break;
            case 23:
                strFieldName = "COMGRADE";
                break;
            case 24:
                strFieldName = "COMAREATYPE";
                break;
            case 25:
                strFieldName = "UPCOMCODE";
                break;
            case 26:
                strFieldName = "ISDIRUNDER";
                break;
            case 27:
                strFieldName = "COMAREATYPE1";
                break;
            case 28:
                strFieldName = "PROVINCE";
                break;
            case 29:
                strFieldName = "CITY";
                break;
            case 30:
                strFieldName = "COUNTY";
                break;
            case 31:
                strFieldName = "FINDB";
                break;
            case 32:
                strFieldName = "LETTERSERVICENAME";
                break;
            case 33:
                strFieldName = "COMSTATE";
                break;
            case 34:
                strFieldName = "COMPREPAREDATE";
                break;
            case 35:
                strFieldName = "SUPMANAGEDATE";
                break;
            case 36:
                strFieldName = "SUPAPPROVALDATE";
                break;
            case 37:
                strFieldName = "THEOPENINGDATE";
                break;
            case 38:
                strFieldName = "COMREPLYCANCELDATE";
                break;
            case 39:
                strFieldName = "SUPREPLYCANCELDATE";
                break;
            case 40:
                strFieldName = "BUSPAPERCANCELDATE";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "OUTCOMCODE":
                return Schema.TYPE_STRING;
            case "NAME":
                return Schema.TYPE_STRING;
            case "SHORTNAME":
                return Schema.TYPE_STRING;
            case "ADDRESS":
                return Schema.TYPE_STRING;
            case "ZIPCODE":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "WEBADDRESS":
                return Schema.TYPE_STRING;
            case "SATRAPNAME":
                return Schema.TYPE_STRING;
            case "INSUMONITORCODE":
                return Schema.TYPE_STRING;
            case "INSUREID":
                return Schema.TYPE_STRING;
            case "SIGNID":
                return Schema.TYPE_STRING;
            case "REGIONALISMCODE":
                return Schema.TYPE_STRING;
            case "COMNATURE":
                return Schema.TYPE_STRING;
            case "VALIDCODE":
                return Schema.TYPE_STRING;
            case "SIGN":
                return Schema.TYPE_STRING;
            case "COMCITYSIZE":
                return Schema.TYPE_STRING;
            case "SERVICENAME":
                return Schema.TYPE_STRING;
            case "SERVICENO":
                return Schema.TYPE_STRING;
            case "SERVICEPHONE":
                return Schema.TYPE_STRING;
            case "SERVICEPOSTADDRESS":
                return Schema.TYPE_STRING;
            case "COMGRADE":
                return Schema.TYPE_STRING;
            case "COMAREATYPE":
                return Schema.TYPE_STRING;
            case "UPCOMCODE":
                return Schema.TYPE_STRING;
            case "ISDIRUNDER":
                return Schema.TYPE_STRING;
            case "COMAREATYPE1":
                return Schema.TYPE_STRING;
            case "PROVINCE":
                return Schema.TYPE_STRING;
            case "CITY":
                return Schema.TYPE_STRING;
            case "COUNTY":
                return Schema.TYPE_STRING;
            case "FINDB":
                return Schema.TYPE_STRING;
            case "LETTERSERVICENAME":
                return Schema.TYPE_STRING;
            case "COMSTATE":
                return Schema.TYPE_STRING;
            case "COMPREPAREDATE":
                return Schema.TYPE_DATE;
            case "SUPMANAGEDATE":
                return Schema.TYPE_DATE;
            case "SUPAPPROVALDATE":
                return Schema.TYPE_DATE;
            case "THEOPENINGDATE":
                return Schema.TYPE_DATE;
            case "COMREPLYCANCELDATE":
                return Schema.TYPE_DATE;
            case "SUPREPLYCANCELDATE":
                return Schema.TYPE_DATE;
            case "BUSPAPERCANCELDATE":
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_DATE;
            case 35:
                return Schema.TYPE_DATE;
            case 36:
                return Schema.TYPE_DATE;
            case 37:
                return Schema.TYPE_DATE;
            case 38:
                return Schema.TYPE_DATE;
            case 39:
                return Schema.TYPE_DATE;
            case 40:
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "LDComSchema {" +
            "ComCode="+ComCode +
            ", OutComCode="+OutComCode +
            ", Name="+Name +
            ", ShortName="+ShortName +
            ", Address="+Address +
            ", ZipCode="+ZipCode +
            ", Phone="+Phone +
            ", Fax="+Fax +
            ", EMail="+EMail +
            ", WebAddress="+WebAddress +
            ", SatrapName="+SatrapName +
            ", InsuMonitorCode="+InsuMonitorCode +
            ", InsureID="+InsureID +
            ", SignID="+SignID +
            ", RegionalismCode="+RegionalismCode +
            ", ComNature="+ComNature +
            ", ValidCode="+ValidCode +
            ", Sign="+Sign +
            ", ComCitySize="+ComCitySize +
            ", ServiceName="+ServiceName +
            ", ServiceNo="+ServiceNo +
            ", ServicePhone="+ServicePhone +
            ", ServicePostAddress="+ServicePostAddress +
            ", COMGRADE="+COMGRADE +
            ", COMAREATYPE="+COMAREATYPE +
            ", UPCOMCODE="+UPCOMCODE +
            ", ISDIRUNDER="+ISDIRUNDER +
            ", COMAREATYPE1="+COMAREATYPE1 +
            ", PROVINCE="+PROVINCE +
            ", CITY="+CITY +
            ", COUNTY="+COUNTY +
            ", FINDB="+FINDB +
            ", LETTERSERVICENAME="+LETTERSERVICENAME +
            ", COMSTATE="+COMSTATE +
            ", COMPREPAREDATE="+COMPREPAREDATE +
            ", SUPMANAGEDATE="+SUPMANAGEDATE +
            ", SUPAPPROVALDATE="+SUPAPPROVALDATE +
            ", THEOPENINGDATE="+THEOPENINGDATE +
            ", COMREPLYCANCELDATE="+COMREPLYCANCELDATE +
            ", SUPREPLYCANCELDATE="+SUPREPLYCANCELDATE +
            ", BUSPAPERCANCELDATE="+BUSPAPERCANCELDATE +"}";
    }
}
