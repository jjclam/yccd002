/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LMProdSetInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LMProdSetInfoDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMProdSetInfoDBSet extends LMProdSetInfoSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LMProdSetInfoDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LMProdSetInfo");
        mflag = true;
    }

    public LMProdSetInfoDBSet() {
        db = new DBOper( "LMProdSetInfo" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMProdSetInfoDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LMProdSetInfo WHERE  1=1  AND ProdSetID = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getProdSetID() == null || this.get(i).getProdSetID().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getProdSetID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMProdSetInfoDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LMProdSetInfo SET  ProdSetCode = ? , ProdSetName = ? , ProdSetShortName = ? , ProdSetEnName = ? , ProdSetEnShortName = ? , SaleChnl = ? , EndDate = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , STANDBYFLAG1 = ? , STANDBYFLAG2 = ? , STANDBYFLAG3 = ? , STANDBYFLAG4 = ? , STANDBYFLAG5 = ? , STANDBYFLAG6 = ? , StartDate = ? , ProdSetID = ? WHERE  1=1  AND ProdSetID = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getProdSetCode() == null || this.get(i).getProdSetCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getProdSetCode());
            }
            if(this.get(i).getProdSetName() == null || this.get(i).getProdSetName().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getProdSetName());
            }
            if(this.get(i).getProdSetShortName() == null || this.get(i).getProdSetShortName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getProdSetShortName());
            }
            if(this.get(i).getProdSetEnName() == null || this.get(i).getProdSetEnName().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getProdSetEnName());
            }
            if(this.get(i).getProdSetEnShortName() == null || this.get(i).getProdSetEnShortName().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getProdSetEnShortName());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getSaleChnl());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getEndDate()));
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(9,null);
            } else {
                pstmt.setDate(9, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getModifyTime());
            }
            if(this.get(i).getSTANDBYFLAG1() == null || this.get(i).getSTANDBYFLAG1().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getSTANDBYFLAG1());
            }
            if(this.get(i).getSTANDBYFLAG2() == null || this.get(i).getSTANDBYFLAG2().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getSTANDBYFLAG2());
            }
            if(this.get(i).getSTANDBYFLAG3() == null || this.get(i).getSTANDBYFLAG3().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getSTANDBYFLAG3()));
            }
            if(this.get(i).getSTANDBYFLAG4() == null || this.get(i).getSTANDBYFLAG4().equals("null")) {
                pstmt.setDate(16,null);
            } else {
                pstmt.setDate(16, Date.valueOf(this.get(i).getSTANDBYFLAG4()));
            }
            pstmt.setDouble(17, this.get(i).getSTANDBYFLAG5());
            pstmt.setDouble(18, this.get(i).getSTANDBYFLAG6());
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
                pstmt.setDate(19,null);
            } else {
                pstmt.setDate(19, Date.valueOf(this.get(i).getStartDate()));
            }
            if(this.get(i).getProdSetID() == null || this.get(i).getProdSetID().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getProdSetID());
            }
            // set where condition
            if(this.get(i).getProdSetID() == null || this.get(i).getProdSetID().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getProdSetID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMProdSetInfoDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LMProdSetInfo VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getProdSetCode() == null || this.get(i).getProdSetCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getProdSetCode());
            }
            if(this.get(i).getProdSetName() == null || this.get(i).getProdSetName().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getProdSetName());
            }
            if(this.get(i).getProdSetShortName() == null || this.get(i).getProdSetShortName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getProdSetShortName());
            }
            if(this.get(i).getProdSetEnName() == null || this.get(i).getProdSetEnName().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getProdSetEnName());
            }
            if(this.get(i).getProdSetEnShortName() == null || this.get(i).getProdSetEnShortName().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getProdSetEnShortName());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getSaleChnl());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
                pstmt.setDate(7,null);
            } else {
                pstmt.setDate(7, Date.valueOf(this.get(i).getEndDate()));
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(9,null);
            } else {
                pstmt.setDate(9, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getModifyTime());
            }
            if(this.get(i).getSTANDBYFLAG1() == null || this.get(i).getSTANDBYFLAG1().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getSTANDBYFLAG1());
            }
            if(this.get(i).getSTANDBYFLAG2() == null || this.get(i).getSTANDBYFLAG2().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getSTANDBYFLAG2());
            }
            if(this.get(i).getSTANDBYFLAG3() == null || this.get(i).getSTANDBYFLAG3().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getSTANDBYFLAG3()));
            }
            if(this.get(i).getSTANDBYFLAG4() == null || this.get(i).getSTANDBYFLAG4().equals("null")) {
                pstmt.setDate(16,null);
            } else {
                pstmt.setDate(16, Date.valueOf(this.get(i).getSTANDBYFLAG4()));
            }
            pstmt.setDouble(17, this.get(i).getSTANDBYFLAG5());
            pstmt.setDouble(18, this.get(i).getSTANDBYFLAG6());
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
                pstmt.setDate(19,null);
            } else {
                pstmt.setDate(19, Date.valueOf(this.get(i).getStartDate()));
            }
            if(this.get(i).getProdSetID() == null || this.get(i).getProdSetID().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getProdSetID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMProdSetInfoDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
