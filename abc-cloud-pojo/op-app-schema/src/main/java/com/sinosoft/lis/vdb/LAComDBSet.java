/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LAComDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-11-25
 */
public class LAComDBSet extends LAComSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LAComDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LACom");
        mflag = true;
    }

    public LAComDBSet() {
        db = new DBOper( "LACom" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LACom WHERE  1=1  AND AgentCom = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAgentCom());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LACom SET  AgentCom = ? , ManageCom = ? , AreaType = ? , ChannelType = ? , UpAgentCom = ? , Name = ? , Address = ? , ZipCode = ? , Phone = ? , Fax = ? , EMail = ? , WebAddress = ? , LinkMan = ? , Password = ? , Corporation = ? , BankCode = ? , BankAccNo = ? , BusinessType = ? , GrpNature = ? , ACType = ? , SellFlag = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , BankType = ? , CalFlag = ? , BusiLicenseCode = ? , InsureID = ? , InsurePrincipal = ? , ChiefBusiness = ? , BusiAddress = ? , SubscribeMan = ? , SubscribeManDuty = ? , LicenseNo = ? , RegionalismCode = ? , AppAgentCom = ? , State = ? , Noti = ? , BusinessCode = ? , LicenseStartDate = ? , LicenseEndDate = ? , BranchType = ? , BranchType2 = ? , Assets = ? , Income = ? , Profits = ? , PersonnalSum = ? , ProtocalNo = ? , HeadOffice = ? , FoundDate = ? , EndDate = ? , Bank = ? , AccName = ? , DraWer = ? , DraWerAccNo = ? , RepManageCom = ? , DraWerAccCode = ? , DraWerAccName = ? , ChannelType2 = ? , AreaType2 = ? , ComToAgentflag = ? , AgentCode = ? , AgentComCode = ? , CooperateBank = ? , CooperationEndDate = ? , CooperationStartDate = ? , AgentType = ? , NewBankCode = ? , BankCity = ? WHERE  1=1  AND AgentCom = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAgentCom());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getManageCom());
            }
            if(this.get(i).getAreaType() == null || this.get(i).getAreaType().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getAreaType());
            }
            if(this.get(i).getChannelType() == null || this.get(i).getChannelType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getChannelType());
            }
            if(this.get(i).getUpAgentCom() == null || this.get(i).getUpAgentCom().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getUpAgentCom());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getName());
            }
            if(this.get(i).getAddress() == null || this.get(i).getAddress().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getAddress());
            }
            if(this.get(i).getZipCode() == null || this.get(i).getZipCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getZipCode());
            }
            if(this.get(i).getPhone() == null || this.get(i).getPhone().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getPhone());
            }
            if(this.get(i).getFax() == null || this.get(i).getFax().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getFax());
            }
            if(this.get(i).getEMail() == null || this.get(i).getEMail().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getEMail());
            }
            if(this.get(i).getWebAddress() == null || this.get(i).getWebAddress().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getWebAddress());
            }
            if(this.get(i).getLinkMan() == null || this.get(i).getLinkMan().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getLinkMan());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getPassword());
            }
            if(this.get(i).getCorporation() == null || this.get(i).getCorporation().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getCorporation());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getBankCode());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getBankAccNo());
            }
            if(this.get(i).getBusinessType() == null || this.get(i).getBusinessType().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getBusinessType());
            }
            if(this.get(i).getGrpNature() == null || this.get(i).getGrpNature().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getGrpNature());
            }
            if(this.get(i).getACType() == null || this.get(i).getACType().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getACType());
            }
            if(this.get(i).getSellFlag() == null || this.get(i).getSellFlag().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getSellFlag());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getModifyTime());
            }
            if(this.get(i).getBankType() == null || this.get(i).getBankType().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getBankType());
            }
            if(this.get(i).getCalFlag() == null || this.get(i).getCalFlag().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getCalFlag());
            }
            if(this.get(i).getBusiLicenseCode() == null || this.get(i).getBusiLicenseCode().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getBusiLicenseCode());
            }
            if(this.get(i).getInsureID() == null || this.get(i).getInsureID().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getInsureID());
            }
            if(this.get(i).getInsurePrincipal() == null || this.get(i).getInsurePrincipal().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getInsurePrincipal());
            }
            if(this.get(i).getChiefBusiness() == null || this.get(i).getChiefBusiness().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getChiefBusiness());
            }
            if(this.get(i).getBusiAddress() == null || this.get(i).getBusiAddress().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getBusiAddress());
            }
            if(this.get(i).getSubscribeMan() == null || this.get(i).getSubscribeMan().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getSubscribeMan());
            }
            if(this.get(i).getSubscribeManDuty() == null || this.get(i).getSubscribeManDuty().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getSubscribeManDuty());
            }
            if(this.get(i).getLicenseNo() == null || this.get(i).getLicenseNo().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getLicenseNo());
            }
            if(this.get(i).getRegionalismCode() == null || this.get(i).getRegionalismCode().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getRegionalismCode());
            }
            if(this.get(i).getAppAgentCom() == null || this.get(i).getAppAgentCom().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getAppAgentCom());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getState());
            }
            if(this.get(i).getNoti() == null || this.get(i).getNoti().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getNoti());
            }
            if(this.get(i).getBusinessCode() == null || this.get(i).getBusinessCode().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getBusinessCode());
            }
            if(this.get(i).getLicenseStartDate() == null || this.get(i).getLicenseStartDate().equals("null")) {
                pstmt.setDate(42,null);
            } else {
                pstmt.setDate(42, Date.valueOf(this.get(i).getLicenseStartDate()));
            }
            if(this.get(i).getLicenseEndDate() == null || this.get(i).getLicenseEndDate().equals("null")) {
                pstmt.setDate(43,null);
            } else {
                pstmt.setDate(43, Date.valueOf(this.get(i).getLicenseEndDate()));
            }
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getBranchType());
            }
            if(this.get(i).getBranchType2() == null || this.get(i).getBranchType2().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getBranchType2());
            }
            pstmt.setDouble(46, this.get(i).getAssets());
            pstmt.setDouble(47, this.get(i).getIncome());
            pstmt.setDouble(48, this.get(i).getProfits());
            pstmt.setInt(49, this.get(i).getPersonnalSum());
            if(this.get(i).getProtocalNo() == null || this.get(i).getProtocalNo().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getProtocalNo());
            }
            if(this.get(i).getHeadOffice() == null || this.get(i).getHeadOffice().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getHeadOffice());
            }
            if(this.get(i).getFoundDate() == null || this.get(i).getFoundDate().equals("null")) {
                pstmt.setDate(52,null);
            } else {
                pstmt.setDate(52, Date.valueOf(this.get(i).getFoundDate()));
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
                pstmt.setDate(53,null);
            } else {
                pstmt.setDate(53, Date.valueOf(this.get(i).getEndDate()));
            }
            if(this.get(i).getBank() == null || this.get(i).getBank().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getBank());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getAccName());
            }
            if(this.get(i).getDraWer() == null || this.get(i).getDraWer().equals("null")) {
            	pstmt.setString(56,null);
            } else {
            	pstmt.setString(56, this.get(i).getDraWer());
            }
            if(this.get(i).getDraWerAccNo() == null || this.get(i).getDraWerAccNo().equals("null")) {
            	pstmt.setString(57,null);
            } else {
            	pstmt.setString(57, this.get(i).getDraWerAccNo());
            }
            if(this.get(i).getRepManageCom() == null || this.get(i).getRepManageCom().equals("null")) {
            	pstmt.setString(58,null);
            } else {
            	pstmt.setString(58, this.get(i).getRepManageCom());
            }
            if(this.get(i).getDraWerAccCode() == null || this.get(i).getDraWerAccCode().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getDraWerAccCode());
            }
            if(this.get(i).getDraWerAccName() == null || this.get(i).getDraWerAccName().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getDraWerAccName());
            }
            if(this.get(i).getChannelType2() == null || this.get(i).getChannelType2().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getChannelType2());
            }
            if(this.get(i).getAreaType2() == null || this.get(i).getAreaType2().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getAreaType2());
            }
            if(this.get(i).getComToAgentflag() == null || this.get(i).getComToAgentflag().equals("null")) {
            	pstmt.setString(63,null);
            } else {
            	pstmt.setString(63, this.get(i).getComToAgentflag());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentComCode() == null || this.get(i).getAgentComCode().equals("null")) {
            	pstmt.setString(65,null);
            } else {
            	pstmt.setString(65, this.get(i).getAgentComCode());
            }
            if(this.get(i).getCooperateBank() == null || this.get(i).getCooperateBank().equals("null")) {
            	pstmt.setString(66,null);
            } else {
            	pstmt.setString(66, this.get(i).getCooperateBank());
            }
            if(this.get(i).getCooperationEndDate() == null || this.get(i).getCooperationEndDate().equals("null")) {
                pstmt.setDate(67,null);
            } else {
                pstmt.setDate(67, Date.valueOf(this.get(i).getCooperationEndDate()));
            }
            if(this.get(i).getCooperationStartDate() == null || this.get(i).getCooperationStartDate().equals("null")) {
                pstmt.setDate(68,null);
            } else {
                pstmt.setDate(68, Date.valueOf(this.get(i).getCooperationStartDate()));
            }
            if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
            	pstmt.setString(69,null);
            } else {
            	pstmt.setString(69, this.get(i).getAgentType());
            }
            if(this.get(i).getNewBankCode() == null || this.get(i).getNewBankCode().equals("null")) {
            	pstmt.setString(70,null);
            } else {
            	pstmt.setString(70, this.get(i).getNewBankCode());
            }
            if(this.get(i).getBankCity() == null || this.get(i).getBankCity().equals("null")) {
            	pstmt.setString(71,null);
            } else {
            	pstmt.setString(71, this.get(i).getBankCity());
            }
            // set where condition
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getAgentCom());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LACom VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getAgentCom());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getManageCom());
            }
            if(this.get(i).getAreaType() == null || this.get(i).getAreaType().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getAreaType());
            }
            if(this.get(i).getChannelType() == null || this.get(i).getChannelType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getChannelType());
            }
            if(this.get(i).getUpAgentCom() == null || this.get(i).getUpAgentCom().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getUpAgentCom());
            }
            if(this.get(i).getName() == null || this.get(i).getName().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getName());
            }
            if(this.get(i).getAddress() == null || this.get(i).getAddress().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getAddress());
            }
            if(this.get(i).getZipCode() == null || this.get(i).getZipCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getZipCode());
            }
            if(this.get(i).getPhone() == null || this.get(i).getPhone().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getPhone());
            }
            if(this.get(i).getFax() == null || this.get(i).getFax().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getFax());
            }
            if(this.get(i).getEMail() == null || this.get(i).getEMail().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getEMail());
            }
            if(this.get(i).getWebAddress() == null || this.get(i).getWebAddress().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getWebAddress());
            }
            if(this.get(i).getLinkMan() == null || this.get(i).getLinkMan().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getLinkMan());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getPassword());
            }
            if(this.get(i).getCorporation() == null || this.get(i).getCorporation().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getCorporation());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getBankCode());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getBankAccNo());
            }
            if(this.get(i).getBusinessType() == null || this.get(i).getBusinessType().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getBusinessType());
            }
            if(this.get(i).getGrpNature() == null || this.get(i).getGrpNature().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getGrpNature());
            }
            if(this.get(i).getACType() == null || this.get(i).getACType().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getACType());
            }
            if(this.get(i).getSellFlag() == null || this.get(i).getSellFlag().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getSellFlag());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(23,null);
            } else {
                pstmt.setDate(23, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getModifyTime());
            }
            if(this.get(i).getBankType() == null || this.get(i).getBankType().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getBankType());
            }
            if(this.get(i).getCalFlag() == null || this.get(i).getCalFlag().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getCalFlag());
            }
            if(this.get(i).getBusiLicenseCode() == null || this.get(i).getBusiLicenseCode().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getBusiLicenseCode());
            }
            if(this.get(i).getInsureID() == null || this.get(i).getInsureID().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getInsureID());
            }
            if(this.get(i).getInsurePrincipal() == null || this.get(i).getInsurePrincipal().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getInsurePrincipal());
            }
            if(this.get(i).getChiefBusiness() == null || this.get(i).getChiefBusiness().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getChiefBusiness());
            }
            if(this.get(i).getBusiAddress() == null || this.get(i).getBusiAddress().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getBusiAddress());
            }
            if(this.get(i).getSubscribeMan() == null || this.get(i).getSubscribeMan().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getSubscribeMan());
            }
            if(this.get(i).getSubscribeManDuty() == null || this.get(i).getSubscribeManDuty().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getSubscribeManDuty());
            }
            if(this.get(i).getLicenseNo() == null || this.get(i).getLicenseNo().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getLicenseNo());
            }
            if(this.get(i).getRegionalismCode() == null || this.get(i).getRegionalismCode().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getRegionalismCode());
            }
            if(this.get(i).getAppAgentCom() == null || this.get(i).getAppAgentCom().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getAppAgentCom());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getState());
            }
            if(this.get(i).getNoti() == null || this.get(i).getNoti().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getNoti());
            }
            if(this.get(i).getBusinessCode() == null || this.get(i).getBusinessCode().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getBusinessCode());
            }
            if(this.get(i).getLicenseStartDate() == null || this.get(i).getLicenseStartDate().equals("null")) {
                pstmt.setDate(42,null);
            } else {
                pstmt.setDate(42, Date.valueOf(this.get(i).getLicenseStartDate()));
            }
            if(this.get(i).getLicenseEndDate() == null || this.get(i).getLicenseEndDate().equals("null")) {
                pstmt.setDate(43,null);
            } else {
                pstmt.setDate(43, Date.valueOf(this.get(i).getLicenseEndDate()));
            }
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getBranchType());
            }
            if(this.get(i).getBranchType2() == null || this.get(i).getBranchType2().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getBranchType2());
            }
            pstmt.setDouble(46, this.get(i).getAssets());
            pstmt.setDouble(47, this.get(i).getIncome());
            pstmt.setDouble(48, this.get(i).getProfits());
            pstmt.setInt(49, this.get(i).getPersonnalSum());
            if(this.get(i).getProtocalNo() == null || this.get(i).getProtocalNo().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getProtocalNo());
            }
            if(this.get(i).getHeadOffice() == null || this.get(i).getHeadOffice().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getHeadOffice());
            }
            if(this.get(i).getFoundDate() == null || this.get(i).getFoundDate().equals("null")) {
                pstmt.setDate(52,null);
            } else {
                pstmt.setDate(52, Date.valueOf(this.get(i).getFoundDate()));
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
                pstmt.setDate(53,null);
            } else {
                pstmt.setDate(53, Date.valueOf(this.get(i).getEndDate()));
            }
            if(this.get(i).getBank() == null || this.get(i).getBank().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getBank());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getAccName());
            }
            if(this.get(i).getDraWer() == null || this.get(i).getDraWer().equals("null")) {
            	pstmt.setString(56,null);
            } else {
            	pstmt.setString(56, this.get(i).getDraWer());
            }
            if(this.get(i).getDraWerAccNo() == null || this.get(i).getDraWerAccNo().equals("null")) {
            	pstmt.setString(57,null);
            } else {
            	pstmt.setString(57, this.get(i).getDraWerAccNo());
            }
            if(this.get(i).getRepManageCom() == null || this.get(i).getRepManageCom().equals("null")) {
            	pstmt.setString(58,null);
            } else {
            	pstmt.setString(58, this.get(i).getRepManageCom());
            }
            if(this.get(i).getDraWerAccCode() == null || this.get(i).getDraWerAccCode().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getDraWerAccCode());
            }
            if(this.get(i).getDraWerAccName() == null || this.get(i).getDraWerAccName().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getDraWerAccName());
            }
            if(this.get(i).getChannelType2() == null || this.get(i).getChannelType2().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getChannelType2());
            }
            if(this.get(i).getAreaType2() == null || this.get(i).getAreaType2().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getAreaType2());
            }
            if(this.get(i).getComToAgentflag() == null || this.get(i).getComToAgentflag().equals("null")) {
            	pstmt.setString(63,null);
            } else {
            	pstmt.setString(63, this.get(i).getComToAgentflag());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentComCode() == null || this.get(i).getAgentComCode().equals("null")) {
            	pstmt.setString(65,null);
            } else {
            	pstmt.setString(65, this.get(i).getAgentComCode());
            }
            if(this.get(i).getCooperateBank() == null || this.get(i).getCooperateBank().equals("null")) {
            	pstmt.setString(66,null);
            } else {
            	pstmt.setString(66, this.get(i).getCooperateBank());
            }
            if(this.get(i).getCooperationEndDate() == null || this.get(i).getCooperationEndDate().equals("null")) {
                pstmt.setDate(67,null);
            } else {
                pstmt.setDate(67, Date.valueOf(this.get(i).getCooperationEndDate()));
            }
            if(this.get(i).getCooperationStartDate() == null || this.get(i).getCooperationStartDate().equals("null")) {
                pstmt.setDate(68,null);
            } else {
                pstmt.setDate(68, Date.valueOf(this.get(i).getCooperationStartDate()));
            }
            if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
            	pstmt.setString(69,null);
            } else {
            	pstmt.setString(69, this.get(i).getAgentType());
            }
            if(this.get(i).getNewBankCode() == null || this.get(i).getNewBankCode().equals("null")) {
            	pstmt.setString(70,null);
            } else {
            	pstmt.setString(70, this.get(i).getNewBankCode());
            }
            if(this.get(i).getBankCity() == null || this.get(i).getBankCity().equals("null")) {
            	pstmt.setString(71,null);
            } else {
            	pstmt.setString(71, this.get(i).getBankCity());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
