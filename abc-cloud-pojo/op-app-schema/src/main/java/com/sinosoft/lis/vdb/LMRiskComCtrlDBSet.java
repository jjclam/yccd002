/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LMRiskComCtrlSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LMRiskComCtrlDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMRiskComCtrlDBSet extends LMRiskComCtrlSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LMRiskComCtrlDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LMRiskComCtrl");
        mflag = true;
    }

    public LMRiskComCtrlDBSet() {
        db = new DBOper( "LMRiskComCtrl" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskComCtrlDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LMRiskComCtrl WHERE  1=1  AND RiskCode = ? AND ManageComGrp = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageComGrp() == null || this.get(i).getManageComGrp().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getManageComGrp());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskComCtrlDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LMRiskComCtrl SET  RiskCode = ? , ManageComGrp = ? , StartDate = ? , EndDate = ? , MAXAmnt = ? , MAXMult = ? , MAXPrem = ? , MINAmnt = ? , MINMult = ? , MINPrem = ? WHERE  1=1  AND RiskCode = ? AND ManageComGrp = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageComGrp() == null || this.get(i).getManageComGrp().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getManageComGrp());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
                pstmt.setDate(3,null);
            } else {
                pstmt.setDate(3, Date.valueOf(this.get(i).getStartDate()));
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
                pstmt.setDate(4,null);
            } else {
                pstmt.setDate(4, Date.valueOf(this.get(i).getEndDate()));
            }
            pstmt.setDouble(5, this.get(i).getMAXAmnt());
            pstmt.setDouble(6, this.get(i).getMAXMult());
            pstmt.setDouble(7, this.get(i).getMAXPrem());
            pstmt.setDouble(8, this.get(i).getMINAmnt());
            pstmt.setDouble(9, this.get(i).getMINMult());
            pstmt.setDouble(10, this.get(i).getMINPrem());
            // set where condition
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageComGrp() == null || this.get(i).getManageComGrp().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getManageComGrp());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskComCtrlDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LMRiskComCtrl VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getManageComGrp() == null || this.get(i).getManageComGrp().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getManageComGrp());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
                pstmt.setDate(3,null);
            } else {
                pstmt.setDate(3, Date.valueOf(this.get(i).getStartDate()));
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
                pstmt.setDate(4,null);
            } else {
                pstmt.setDate(4, Date.valueOf(this.get(i).getEndDate()));
            }
            pstmt.setDouble(5, this.get(i).getMAXAmnt());
            pstmt.setDouble(6, this.get(i).getMAXMult());
            pstmt.setDouble(7, this.get(i).getMAXPrem());
            pstmt.setDouble(8, this.get(i).getMINAmnt());
            pstmt.setDouble(9, this.get(i).getMINMult());
            pstmt.setDouble(10, this.get(i).getMINPrem());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskComCtrlDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
