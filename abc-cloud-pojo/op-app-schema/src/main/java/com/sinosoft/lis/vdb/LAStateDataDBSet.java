/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LAStateDataSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LAStateDataDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-03-27
 */
public class LAStateDataDBSet extends LAStateDataSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LAStateDataDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LAStateData");
        mflag = true;
    }

    public LAStateDataDBSet() {
        db = new DBOper( "LAStateData" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAStateDataDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LAStateData WHERE  1=1  AND ObjectId = ? AND StartDate = ? AND EndDate = ? AND BranchType = ? AND StateType = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getObjectId() == null || this.get(i).getObjectId().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getObjectId());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getStartDate());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getEndDate());
            }
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBranchType());
            }
            if(this.get(i).getStateType() == null || this.get(i).getStateType().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getStateType());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAStateDataDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LAStateData SET  ObjectId = ? , StartDate = ? , EndDate = ? , BranchType = ? , StateType = ? , T1 = ? , T2 = ? , T3 = ? , T4 = ? , T5 = ? , T6 = ? , T7 = ? , T8 = ? , T9 = ? , T10 = ? , T11 = ? , T12 = ? , T13 = ? , T14 = ? , T15 = ? , T16 = ? , T17 = ? , T18 = ? , T19 = ? , T20 = ? WHERE  1=1  AND ObjectId = ? AND StartDate = ? AND EndDate = ? AND BranchType = ? AND StateType = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getObjectId() == null || this.get(i).getObjectId().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getObjectId());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getStartDate());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getEndDate());
            }
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBranchType());
            }
            if(this.get(i).getStateType() == null || this.get(i).getStateType().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getStateType());
            }
            pstmt.setDouble(6, this.get(i).getT1());
            pstmt.setDouble(7, this.get(i).getT2());
            pstmt.setDouble(8, this.get(i).getT3());
            pstmt.setDouble(9, this.get(i).getT4());
            pstmt.setDouble(10, this.get(i).getT5());
            pstmt.setDouble(11, this.get(i).getT6());
            pstmt.setDouble(12, this.get(i).getT7());
            pstmt.setDouble(13, this.get(i).getT8());
            pstmt.setDouble(14, this.get(i).getT9());
            pstmt.setDouble(15, this.get(i).getT10());
            pstmt.setDouble(16, this.get(i).getT11());
            pstmt.setDouble(17, this.get(i).getT12());
            pstmt.setDouble(18, this.get(i).getT13());
            pstmt.setDouble(19, this.get(i).getT14());
            pstmt.setDouble(20, this.get(i).getT15());
            pstmt.setDouble(21, this.get(i).getT16());
            pstmt.setDouble(22, this.get(i).getT17());
            pstmt.setDouble(23, this.get(i).getT18());
            pstmt.setDouble(24, this.get(i).getT19());
            pstmt.setDouble(25, this.get(i).getT20());
            // set where condition
            if(this.get(i).getObjectId() == null || this.get(i).getObjectId().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getObjectId());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getStartDate());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getEndDate());
            }
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getBranchType());
            }
            if(this.get(i).getStateType() == null || this.get(i).getStateType().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getStateType());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAStateDataDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LAStateData VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getObjectId() == null || this.get(i).getObjectId().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getObjectId());
            }
            if(this.get(i).getStartDate() == null || this.get(i).getStartDate().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getStartDate());
            }
            if(this.get(i).getEndDate() == null || this.get(i).getEndDate().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getEndDate());
            }
            if(this.get(i).getBranchType() == null || this.get(i).getBranchType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBranchType());
            }
            if(this.get(i).getStateType() == null || this.get(i).getStateType().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getStateType());
            }
            pstmt.setDouble(6, this.get(i).getT1());
            pstmt.setDouble(7, this.get(i).getT2());
            pstmt.setDouble(8, this.get(i).getT3());
            pstmt.setDouble(9, this.get(i).getT4());
            pstmt.setDouble(10, this.get(i).getT5());
            pstmt.setDouble(11, this.get(i).getT6());
            pstmt.setDouble(12, this.get(i).getT7());
            pstmt.setDouble(13, this.get(i).getT8());
            pstmt.setDouble(14, this.get(i).getT9());
            pstmt.setDouble(15, this.get(i).getT10());
            pstmt.setDouble(16, this.get(i).getT11());
            pstmt.setDouble(17, this.get(i).getT12());
            pstmt.setDouble(18, this.get(i).getT13());
            pstmt.setDouble(19, this.get(i).getT14());
            pstmt.setDouble(20, this.get(i).getT15());
            pstmt.setDouble(21, this.get(i).getT16());
            pstmt.setDouble(22, this.get(i).getT17());
            pstmt.setDouble(23, this.get(i).getT18());
            pstmt.setDouble(24, this.get(i).getT19());
            pstmt.setDouble(25, this.get(i).getT20());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAStateDataDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
