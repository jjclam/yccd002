/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LKChargeDefDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LKChargeDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LKChargeDefSchema implements Schema, Cloneable {
    // @Field
    /** 管理机构 */
    private String ManageCom;
    /** 银行类别 */
    private String BankCode;
    /** 收费机构 */
    private String ChargeCom;
    /** 备用1 */
    private String StandByFlag1;
    /** 备用2 */
    private String StandByFlag2;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 操作员号 */
    private String Operator;

    public static final int FIELDNUM = 10;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LKChargeDefSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ManageCom";
        pk[1] = "BankCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LKChargeDefSchema cloned = (LKChargeDefSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getChargeCom() {
        return ChargeCom;
    }
    public void setChargeCom(String aChargeCom) {
        ChargeCom = aChargeCom;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getStandByFlag2() {
        return StandByFlag2;
    }
    public void setStandByFlag2(String aStandByFlag2) {
        StandByFlag2 = aStandByFlag2;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    /**
    * 使用另外一个 LKChargeDefSchema 对象给 Schema 赋值
    * @param: aLKChargeDefSchema LKChargeDefSchema
    **/
    public void setSchema(LKChargeDefSchema aLKChargeDefSchema) {
        this.ManageCom = aLKChargeDefSchema.getManageCom();
        this.BankCode = aLKChargeDefSchema.getBankCode();
        this.ChargeCom = aLKChargeDefSchema.getChargeCom();
        this.StandByFlag1 = aLKChargeDefSchema.getStandByFlag1();
        this.StandByFlag2 = aLKChargeDefSchema.getStandByFlag2();
        this.MakeDate = fDate.getDate( aLKChargeDefSchema.getMakeDate());
        this.MakeTime = aLKChargeDefSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLKChargeDefSchema.getModifyDate());
        this.ModifyTime = aLKChargeDefSchema.getModifyTime();
        this.Operator = aLKChargeDefSchema.getOperator();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("ChargeCom") == null )
                this.ChargeCom = null;
            else
                this.ChargeCom = rs.getString("ChargeCom").trim();

            if( rs.getString("StandByFlag1") == null )
                this.StandByFlag1 = null;
            else
                this.StandByFlag1 = rs.getString("StandByFlag1").trim();

            if( rs.getString("StandByFlag2") == null )
                this.StandByFlag2 = null;
            else
                this.StandByFlag2 = rs.getString("StandByFlag2").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKChargeDefSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LKChargeDefSchema getSchema() {
        LKChargeDefSchema aLKChargeDefSchema = new LKChargeDefSchema();
        aLKChargeDefSchema.setSchema(this);
        return aLKChargeDefSchema;
    }

    public LKChargeDefDB getDB() {
        LKChargeDefDB aDBOper = new LKChargeDefDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKChargeDef描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChargeCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKChargeDef>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ChargeCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            StandByFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKChargeDefSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("ChargeCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeCom));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ChargeCom);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag2);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("ChargeCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ChargeCom = FValue.trim();
            }
            else
                ChargeCom = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag2 = FValue.trim();
            }
            else
                StandByFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LKChargeDefSchema other = (LKChargeDefSchema)otherObject;
        return
            ManageCom.equals(other.getManageCom())
            && BankCode.equals(other.getBankCode())
            && ChargeCom.equals(other.getChargeCom())
            && StandByFlag1.equals(other.getStandByFlag1())
            && StandByFlag2.equals(other.getStandByFlag2())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Operator.equals(other.getOperator());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ManageCom") ) {
            return 0;
        }
        if( strFieldName.equals("BankCode") ) {
            return 1;
        }
        if( strFieldName.equals("ChargeCom") ) {
            return 2;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 3;
        }
        if( strFieldName.equals("StandByFlag2") ) {
            return 4;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 5;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 6;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 7;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 8;
        }
        if( strFieldName.equals("Operator") ) {
            return 9;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "BankCode";
                break;
            case 2:
                strFieldName = "ChargeCom";
                break;
            case 3:
                strFieldName = "StandByFlag1";
                break;
            case 4:
                strFieldName = "StandByFlag2";
                break;
            case 5:
                strFieldName = "MakeDate";
                break;
            case 6:
                strFieldName = "MakeTime";
                break;
            case 7:
                strFieldName = "ModifyDate";
                break;
            case 8:
                strFieldName = "ModifyTime";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "CHARGECOM":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_DATE;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DATE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
