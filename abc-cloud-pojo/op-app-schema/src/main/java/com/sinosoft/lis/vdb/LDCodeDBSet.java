/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LDCodeDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LDCodeDBSet extends LDCodeSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LDCodeDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LDCode");
        mflag = true;
    }

    public LDCodeDBSet() {
        db = new DBOper( "LDCode" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDCodeDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LDCode WHERE  1=1  AND CodeType = ? AND Code = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCodeType() == null || this.get(i).getCodeType().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCodeType());
            }
            if(this.get(i).getCode() == null || this.get(i).getCode().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCode());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDCodeDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LDCode SET  CodeType = ? , Code = ? , CodeName = ? , CodeAlias = ? , ComCode = ? , OtherSign = ? WHERE  1=1  AND CodeType = ? AND Code = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCodeType() == null || this.get(i).getCodeType().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCodeType());
            }
            if(this.get(i).getCode() == null || this.get(i).getCode().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCode());
            }
            if(this.get(i).getCodeName() == null || this.get(i).getCodeName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getCodeName());
            }
            if(this.get(i).getCodeAlias() == null || this.get(i).getCodeAlias().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getCodeAlias());
            }
            if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getComCode());
            }
            if(this.get(i).getOtherSign() == null || this.get(i).getOtherSign().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getOtherSign());
            }
            // set where condition
            if(this.get(i).getCodeType() == null || this.get(i).getCodeType().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getCodeType());
            }
            if(this.get(i).getCode() == null || this.get(i).getCode().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getCode());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDCodeDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LDCode VALUES( ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCodeType() == null || this.get(i).getCodeType().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCodeType());
            }
            if(this.get(i).getCode() == null || this.get(i).getCode().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getCode());
            }
            if(this.get(i).getCodeName() == null || this.get(i).getCodeName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getCodeName());
            }
            if(this.get(i).getCodeAlias() == null || this.get(i).getCodeAlias().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getCodeAlias());
            }
            if(this.get(i).getComCode() == null || this.get(i).getComCode().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getComCode());
            }
            if(this.get(i).getOtherSign() == null || this.get(i).getOtherSign().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getOtherSign());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDCodeDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
