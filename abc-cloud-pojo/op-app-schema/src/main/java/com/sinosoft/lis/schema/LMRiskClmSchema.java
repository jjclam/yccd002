/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMRiskClmDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LMRiskClmSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMRiskClmSchema implements Schema, Cloneable {
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 险种名称 */
    private String RiskName;
    /** 调查标记 */
    private String SurveyFlag;
    /** 调查开始位置 */
    private String SurveyStartFlag;
    /** 控制赔付比例标记 */
    private String ClmRateCtlFlag;
    /** 退预缴保费标记 */
    private String PrePremFlag;

    public static final int FIELDNUM = 7;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMRiskClmSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMRiskClmSchema cloned = (LMRiskClmSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVer() {
        return RiskVer;
    }
    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }
    public String getRiskName() {
        return RiskName;
    }
    public void setRiskName(String aRiskName) {
        RiskName = aRiskName;
    }
    public String getSurveyFlag() {
        return SurveyFlag;
    }
    public void setSurveyFlag(String aSurveyFlag) {
        SurveyFlag = aSurveyFlag;
    }
    public String getSurveyStartFlag() {
        return SurveyStartFlag;
    }
    public void setSurveyStartFlag(String aSurveyStartFlag) {
        SurveyStartFlag = aSurveyStartFlag;
    }
    public String getClmRateCtlFlag() {
        return ClmRateCtlFlag;
    }
    public void setClmRateCtlFlag(String aClmRateCtlFlag) {
        ClmRateCtlFlag = aClmRateCtlFlag;
    }
    public String getPrePremFlag() {
        return PrePremFlag;
    }
    public void setPrePremFlag(String aPrePremFlag) {
        PrePremFlag = aPrePremFlag;
    }

    /**
    * 使用另外一个 LMRiskClmSchema 对象给 Schema 赋值
    * @param: aLMRiskClmSchema LMRiskClmSchema
    **/
    public void setSchema(LMRiskClmSchema aLMRiskClmSchema) {
        this.RiskCode = aLMRiskClmSchema.getRiskCode();
        this.RiskVer = aLMRiskClmSchema.getRiskVer();
        this.RiskName = aLMRiskClmSchema.getRiskName();
        this.SurveyFlag = aLMRiskClmSchema.getSurveyFlag();
        this.SurveyStartFlag = aLMRiskClmSchema.getSurveyStartFlag();
        this.ClmRateCtlFlag = aLMRiskClmSchema.getClmRateCtlFlag();
        this.PrePremFlag = aLMRiskClmSchema.getPrePremFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("RiskVer") == null )
                this.RiskVer = null;
            else
                this.RiskVer = rs.getString("RiskVer").trim();

            if( rs.getString("RiskName") == null )
                this.RiskName = null;
            else
                this.RiskName = rs.getString("RiskName").trim();

            if( rs.getString("SurveyFlag") == null )
                this.SurveyFlag = null;
            else
                this.SurveyFlag = rs.getString("SurveyFlag").trim();

            if( rs.getString("SurveyStartFlag") == null )
                this.SurveyStartFlag = null;
            else
                this.SurveyStartFlag = rs.getString("SurveyStartFlag").trim();

            if( rs.getString("ClmRateCtlFlag") == null )
                this.ClmRateCtlFlag = null;
            else
                this.ClmRateCtlFlag = rs.getString("ClmRateCtlFlag").trim();

            if( rs.getString("PrePremFlag") == null )
                this.PrePremFlag = null;
            else
                this.PrePremFlag = rs.getString("PrePremFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskClmSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMRiskClmSchema getSchema() {
        LMRiskClmSchema aLMRiskClmSchema = new LMRiskClmSchema();
        aLMRiskClmSchema.setSchema(this);
        return aLMRiskClmSchema;
    }

    public LMRiskClmDB getDB() {
        LMRiskClmDB aDBOper = new LMRiskClmDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskClm描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVer)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyStartFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClmRateCtlFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrePremFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskClm>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            SurveyStartFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ClmRateCtlFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            PrePremFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskClmSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equalsIgnoreCase("SurveyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
        }
        if (FCode.equalsIgnoreCase("SurveyStartFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyStartFlag));
        }
        if (FCode.equalsIgnoreCase("ClmRateCtlFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmRateCtlFlag));
        }
        if (FCode.equalsIgnoreCase("PrePremFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrePremFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SurveyStartFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ClmRateCtlFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PrePremFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
                RiskVer = null;
        }
        if (FCode.equalsIgnoreCase("RiskName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
                RiskName = null;
        }
        if (FCode.equalsIgnoreCase("SurveyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurveyFlag = FValue.trim();
            }
            else
                SurveyFlag = null;
        }
        if (FCode.equalsIgnoreCase("SurveyStartFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SurveyStartFlag = FValue.trim();
            }
            else
                SurveyStartFlag = null;
        }
        if (FCode.equalsIgnoreCase("ClmRateCtlFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ClmRateCtlFlag = FValue.trim();
            }
            else
                ClmRateCtlFlag = null;
        }
        if (FCode.equalsIgnoreCase("PrePremFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrePremFlag = FValue.trim();
            }
            else
                PrePremFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMRiskClmSchema other = (LMRiskClmSchema)otherObject;
        return
            RiskCode.equals(other.getRiskCode())
            && RiskVer.equals(other.getRiskVer())
            && RiskName.equals(other.getRiskName())
            && SurveyFlag.equals(other.getSurveyFlag())
            && SurveyStartFlag.equals(other.getSurveyStartFlag())
            && ClmRateCtlFlag.equals(other.getClmRateCtlFlag())
            && PrePremFlag.equals(other.getPrePremFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("RiskVer") ) {
            return 1;
        }
        if( strFieldName.equals("RiskName") ) {
            return 2;
        }
        if( strFieldName.equals("SurveyFlag") ) {
            return 3;
        }
        if( strFieldName.equals("SurveyStartFlag") ) {
            return 4;
        }
        if( strFieldName.equals("ClmRateCtlFlag") ) {
            return 5;
        }
        if( strFieldName.equals("PrePremFlag") ) {
            return 6;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "SurveyFlag";
                break;
            case 4:
                strFieldName = "SurveyStartFlag";
                break;
            case 5:
                strFieldName = "ClmRateCtlFlag";
                break;
            case 6:
                strFieldName = "PrePremFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVER":
                return Schema.TYPE_STRING;
            case "RISKNAME":
                return Schema.TYPE_STRING;
            case "SURVEYFLAG":
                return Schema.TYPE_STRING;
            case "SURVEYSTARTFLAG":
                return Schema.TYPE_STRING;
            case "CLMRATECTLFLAG":
                return Schema.TYPE_STRING;
            case "PREPREMFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
