/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LMRiskRoleSchema;
import com.sinosoft.lis.vschema.LMRiskRoleSet;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LMRiskRoleDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-05-09
 */
public class LMRiskRoleDB extends LMRiskRoleSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LMRiskRoleDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LMRiskRole" );
        mflag = true;
    }

    public LMRiskRoleDB() {
        con = null;
        db = new DBOper( "LMRiskRole" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LMRiskRoleSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LMRiskRoleSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LMRiskRole WHERE  1=1  AND RiskCode = ? AND RiskRole = ? AND RiskRoleSex = ? AND RiskRoleNo = ?");
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getRiskCode());
            }
            if(this.getRiskRole() == null || this.getRiskRole().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getRiskRole());
            }
            if(this.getRiskRoleSex() == null || this.getRiskRoleSex().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getRiskRoleSex());
            }
            if(this.getRiskRoleNo() == null || this.getRiskRoleNo().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getRiskRoleNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LMRiskRole");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LMRiskRole SET  RiskCode = ? , RiskVer = ? , RiskRole = ? , RiskRoleSex = ? , RiskRoleNo = ? , MinAppAgeFlag = ? , MinAppAge = ? , MAXAppAgeFlag = ? , MAXAppAge = ? , MinRnewAgeFlag = ? , MinRnewAge = ? , MaxRnewAgeFlag = ? , MaxRnewAge = ? WHERE  1=1  AND RiskCode = ? AND RiskRole = ? AND RiskRoleSex = ? AND RiskRoleNo = ?");
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getRiskCode());
            }
            if(this.getRiskVer() == null || this.getRiskVer().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getRiskVer());
            }
            if(this.getRiskRole() == null || this.getRiskRole().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getRiskRole());
            }
            if(this.getRiskRoleSex() == null || this.getRiskRoleSex().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getRiskRoleSex());
            }
            if(this.getRiskRoleNo() == null || this.getRiskRoleNo().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, this.getRiskRoleNo());
            }
            if(this.getMinAppAgeFlag() == null || this.getMinAppAgeFlag().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, this.getMinAppAgeFlag());
            }
            pstmt.setInt(7, this.getMinAppAge());
            if(this.getMAXAppAgeFlag() == null || this.getMAXAppAgeFlag().equals("null")) {
                pstmt.setNull(8, 12);
            } else {
                pstmt.setString(8, this.getMAXAppAgeFlag());
            }
            pstmt.setInt(9, this.getMAXAppAge());
            if(this.getMinRnewAgeFlag() == null || this.getMinRnewAgeFlag().equals("null")) {
                pstmt.setNull(10, 12);
            } else {
                pstmt.setString(10, this.getMinRnewAgeFlag());
            }
            pstmt.setInt(11, this.getMinRnewAge());
            if(this.getMaxRnewAgeFlag() == null || this.getMaxRnewAgeFlag().equals("null")) {
                pstmt.setNull(12, 12);
            } else {
                pstmt.setString(12, this.getMaxRnewAgeFlag());
            }
            pstmt.setInt(13, this.getMaxRnewAge());
            // set where condition
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
                pstmt.setNull(14, 12);
            } else {
                pstmt.setString(14, this.getRiskCode());
            }
            if(this.getRiskRole() == null || this.getRiskRole().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getRiskRole());
            }
            if(this.getRiskRoleSex() == null || this.getRiskRoleSex().equals("null")) {
                pstmt.setNull(16, 12);
            } else {
                pstmt.setString(16, this.getRiskRoleSex());
            }
            if(this.getRiskRoleNo() == null || this.getRiskRoleNo().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getRiskRoleNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LMRiskRole");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LMRiskRole VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getRiskCode());
            }
            if(this.getRiskVer() == null || this.getRiskVer().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getRiskVer());
            }
            if(this.getRiskRole() == null || this.getRiskRole().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getRiskRole());
            }
            if(this.getRiskRoleSex() == null || this.getRiskRoleSex().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getRiskRoleSex());
            }
            if(this.getRiskRoleNo() == null || this.getRiskRoleNo().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, this.getRiskRoleNo());
            }
            if(this.getMinAppAgeFlag() == null || this.getMinAppAgeFlag().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, this.getMinAppAgeFlag());
            }
            pstmt.setInt(7, this.getMinAppAge());
            if(this.getMAXAppAgeFlag() == null || this.getMAXAppAgeFlag().equals("null")) {
                pstmt.setNull(8, 12);
            } else {
                pstmt.setString(8, this.getMAXAppAgeFlag());
            }
            pstmt.setInt(9, this.getMAXAppAge());
            if(this.getMinRnewAgeFlag() == null || this.getMinRnewAgeFlag().equals("null")) {
                pstmt.setNull(10, 12);
            } else {
                pstmt.setString(10, this.getMinRnewAgeFlag());
            }
            pstmt.setInt(11, this.getMinRnewAge());
            if(this.getMaxRnewAgeFlag() == null || this.getMaxRnewAgeFlag().equals("null")) {
                pstmt.setNull(12, 12);
            } else {
                pstmt.setString(12, this.getMaxRnewAgeFlag());
            }
            pstmt.setInt(13, this.getMaxRnewAge());
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LMRiskRole WHERE  1=1  AND RiskCode = ? AND RiskRole = ? AND RiskRoleSex = ? AND RiskRoleNo = ?",
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getRiskCode() == null || this.getRiskCode().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getRiskCode());
            }
            if(this.getRiskRole() == null || this.getRiskRole().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getRiskRole());
            }
            if(this.getRiskRoleSex() == null || this.getRiskRoleSex().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getRiskRoleSex());
            }
            if(this.getRiskRoleNo() == null || this.getRiskRoleNo().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getRiskRoleNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskRoleDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LMRiskRoleSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LMRiskRoleSet aLMRiskRoleSet = new LMRiskRoleSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LMRiskRole");
            LMRiskRoleSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskRoleDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                    break;
                }
                LMRiskRoleSchema s1 = new LMRiskRoleSchema();
                s1.setSchema(rs,i);
                aLMRiskRoleSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLMRiskRoleSet;
    }

    public LMRiskRoleSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LMRiskRoleSet aLMRiskRoleSet = new LMRiskRoleSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskRoleDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                    break;
                }
                LMRiskRoleSchema s1 = new LMRiskRoleSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskRoleDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLMRiskRoleSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLMRiskRoleSet;
    }

    public LMRiskRoleSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LMRiskRoleSet aLMRiskRoleSet = new LMRiskRoleSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LMRiskRole");
            LMRiskRoleSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LMRiskRoleSchema s1 = new LMRiskRoleSchema();
                s1.setSchema(rs,i);
                aLMRiskRoleSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLMRiskRoleSet;
    }

    public LMRiskRoleSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LMRiskRoleSet aLMRiskRoleSet = new LMRiskRoleSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LMRiskRoleSchema s1 = new LMRiskRoleSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LMRiskRoleDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLMRiskRoleSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLMRiskRoleSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LMRiskRole");
            LMRiskRoleSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LMRiskRole " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LMRiskRoleDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LMRiskRoleSet
     */
    public LMRiskRoleSet getData() {
        int tCount = 0;
        LMRiskRoleSet tLMRiskRoleSet = new LMRiskRoleSet();
        LMRiskRoleSchema tLMRiskRoleSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLMRiskRoleSchema = new LMRiskRoleSchema();
            tLMRiskRoleSchema.setSchema(mResultSet, 1);
            tLMRiskRoleSet.add(tLMRiskRoleSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLMRiskRoleSchema = new LMRiskRoleSchema();
                    tLMRiskRoleSchema.setSchema(mResultSet, 1);
                    tLMRiskRoleSet.add(tLMRiskRoleSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLMRiskRoleSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LMRiskRoleDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LMRiskRoleDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LMRiskRoleDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
