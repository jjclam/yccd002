/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.T_Bank_Agency_Brchnet_MapSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/**
 * <p>ClassName: T_Bank_Agency_Brchnet_MapSet </p>
 * <p>Description: T_Bank_Agency_Brchnet_MapSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-09-07
 */
public class T_Bank_Agency_Brchnet_MapSet extends SchemaSet {
	// @Method
	public boolean add(T_Bank_Agency_Brchnet_MapSchema aSchema) {
		return super.add(aSchema);
	}

	public boolean add(T_Bank_Agency_Brchnet_MapSet aSet) {
		return super.add(aSet);
	}

	public boolean remove(T_Bank_Agency_Brchnet_MapSchema aSchema) {
		return super.remove(aSchema);
	}

	public T_Bank_Agency_Brchnet_MapSchema get(int index) {
		T_Bank_Agency_Brchnet_MapSchema tSchema = (T_Bank_Agency_Brchnet_MapSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, T_Bank_Agency_Brchnet_MapSchema aSchema) {
		return super.set(index,aSchema);
	}

	public boolean set(T_Bank_Agency_Brchnet_MapSet aSet) {
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpT_Bank_Agency_Brchnet_Map描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode() {
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++) {
			T_Bank_Agency_Brchnet_MapSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str ) {
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 ) {
			T_Bank_Agency_Brchnet_MapSchema aSchema = new T_Bank_Agency_Brchnet_MapSchema();
			if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
			    this.add(aSchema);
			    nBeginPos = nEndPos + 1;
			    nEndPos = str.indexOf('^', nEndPos + 1);
			} else {
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		T_Bank_Agency_Brchnet_MapSchema tSchema = new T_Bank_Agency_Brchnet_MapSchema();
		if(tSchema.decode(str.substring(nBeginPos))) {
		    this.add(tSchema);
		    return true;
		} else {
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
