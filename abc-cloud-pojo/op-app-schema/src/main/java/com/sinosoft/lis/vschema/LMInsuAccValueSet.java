/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LMInsuAccValueSchema;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;

/**
 * <p>ClassName: LMInsuAccValueSet </p>
 * <p>Description: LMInsuAccValueSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMInsuAccValueSet extends SchemaSet {
	// @Method
	public boolean add(LMInsuAccValueSchema aSchema) {
		return super.add(aSchema);
	}

	public boolean add(LMInsuAccValueSet aSet) {
		return super.add(aSet);
	}

	public boolean remove(LMInsuAccValueSchema aSchema) {
		return super.remove(aSchema);
	}

	public LMInsuAccValueSchema get(int index) {
		LMInsuAccValueSchema tSchema = (LMInsuAccValueSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LMInsuAccValueSchema aSchema) {
		return super.set(index,aSchema);
	}

	public boolean set(LMInsuAccValueSet aSet) {
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMInsuAccValue描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode() {
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++) {
			LMInsuAccValueSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str ) {
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 ) {
			LMInsuAccValueSchema aSchema = new LMInsuAccValueSchema();
			if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
			    this.add(aSchema);
			    nBeginPos = nEndPos + 1;
			    nEndPos = str.indexOf('^', nEndPos + 1);
			} else {
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LMInsuAccValueSchema tSchema = new LMInsuAccValueSchema();
		if(tSchema.decode(str.substring(nBeginPos))) {
		    this.add(tSchema);
		    return true;
		} else {
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
