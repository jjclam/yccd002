/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.T_AGENCY_ORGDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: T_AGENCY_ORGSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2018-11-22
 */
public class T_AGENCY_ORGSchema implements Schema, Cloneable {
    // @Field
    /** 中介机构id */
    private long AGENCY_ID;
    /** 管理机构id */
    private long MNGORG_ID;
    /** 中介机构编码 */
    private String AGENCY_CODE;
    /** 中介机构名称 */
    private String AGENCY_NAME;
    /** 中介机构简称 */
    private String AGENCY_ABBR;
    /** 对外公布机构编码 */
    private String EXTER_ANNOUNCE_ORGCODE;
    /** 上级机构id */
    private long SUPERIOR_ORG_ID;
    /** 中介机构级别 */
    private String AGENCY_LEVEL;
    /** 中介机构地区类型 */
    private String AGENCY_REGION_TYPE;
    /** 中介机构状态 */
    private String AGENCY_STATUS;
    /** 中介机构类别 */
    private String AGENCY_TYPE;
    /** 对应核心编码 */
    private String CORRESP_CORE_CODE;
    /** 联系人名称 */
    private String LINKMAN_NAME;
    /** 标志 */
    private String FLG;
    /** 主管名称 */
    private String SUPERVISOR_NAME;
    /** 插入操作员 */
    private String INSERT_OPER;
    /** 插入委托人 */
    private String INSERT_CONSIGNOR;
    /** 插入时间 */
    private Date INSERT_TIME;
    /** 更新操作员 */
    private String UPDATE_OPER;
    /** 更新委托人 */
    private String UPDATE_CONSIGNOR;
    /** 更新时间 */
    private Date UPDATE_TIME;
    /** Bank_code */
    private String BANK_CODE;
    /** 中介机构地址 */
    private String AGENCY_ADDR;
    /** 管理机构编码 */
    private String MNGORG_CODE;

    public static final int FIELDNUM = 24;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public T_AGENCY_ORGSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AGENCY_ID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        T_AGENCY_ORGSchema cloned = (T_AGENCY_ORGSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public long getAGENCY_ID() {
        return AGENCY_ID;
    }
    public void setAGENCY_ID(long aAGENCY_ID) {
        AGENCY_ID = aAGENCY_ID;
    }
    public void setAGENCY_ID(String aAGENCY_ID) {
        if (aAGENCY_ID != null && !aAGENCY_ID.equals("")) {
            AGENCY_ID = new Long(aAGENCY_ID).longValue();
        }
    }

    public long getMNGORG_ID() {
        return MNGORG_ID;
    }
    public void setMNGORG_ID(long aMNGORG_ID) {
        MNGORG_ID = aMNGORG_ID;
    }
    public void setMNGORG_ID(String aMNGORG_ID) {
        if (aMNGORG_ID != null && !aMNGORG_ID.equals("")) {
            MNGORG_ID = new Long(aMNGORG_ID).longValue();
        }
    }

    public String getAGENCY_CODE() {
        return AGENCY_CODE;
    }
    public void setAGENCY_CODE(String aAGENCY_CODE) {
        AGENCY_CODE = aAGENCY_CODE;
    }
    public String getAGENCY_NAME() {
        return AGENCY_NAME;
    }
    public void setAGENCY_NAME(String aAGENCY_NAME) {
        AGENCY_NAME = aAGENCY_NAME;
    }
    public String getAGENCY_ABBR() {
        return AGENCY_ABBR;
    }
    public void setAGENCY_ABBR(String aAGENCY_ABBR) {
        AGENCY_ABBR = aAGENCY_ABBR;
    }
    public String getEXTER_ANNOUNCE_ORGCODE() {
        return EXTER_ANNOUNCE_ORGCODE;
    }
    public void setEXTER_ANNOUNCE_ORGCODE(String aEXTER_ANNOUNCE_ORGCODE) {
        EXTER_ANNOUNCE_ORGCODE = aEXTER_ANNOUNCE_ORGCODE;
    }
    public long getSUPERIOR_ORG_ID() {
        return SUPERIOR_ORG_ID;
    }
    public void setSUPERIOR_ORG_ID(long aSUPERIOR_ORG_ID) {
        SUPERIOR_ORG_ID = aSUPERIOR_ORG_ID;
    }
    public void setSUPERIOR_ORG_ID(String aSUPERIOR_ORG_ID) {
        if (aSUPERIOR_ORG_ID != null && !aSUPERIOR_ORG_ID.equals("")) {
            SUPERIOR_ORG_ID = new Long(aSUPERIOR_ORG_ID).longValue();
        }
    }

    public String getAGENCY_LEVEL() {
        return AGENCY_LEVEL;
    }
    public void setAGENCY_LEVEL(String aAGENCY_LEVEL) {
        AGENCY_LEVEL = aAGENCY_LEVEL;
    }
    public String getAGENCY_REGION_TYPE() {
        return AGENCY_REGION_TYPE;
    }
    public void setAGENCY_REGION_TYPE(String aAGENCY_REGION_TYPE) {
        AGENCY_REGION_TYPE = aAGENCY_REGION_TYPE;
    }
    public String getAGENCY_STATUS() {
        return AGENCY_STATUS;
    }
    public void setAGENCY_STATUS(String aAGENCY_STATUS) {
        AGENCY_STATUS = aAGENCY_STATUS;
    }
    public String getAGENCY_TYPE() {
        return AGENCY_TYPE;
    }
    public void setAGENCY_TYPE(String aAGENCY_TYPE) {
        AGENCY_TYPE = aAGENCY_TYPE;
    }
    public String getCORRESP_CORE_CODE() {
        return CORRESP_CORE_CODE;
    }
    public void setCORRESP_CORE_CODE(String aCORRESP_CORE_CODE) {
        CORRESP_CORE_CODE = aCORRESP_CORE_CODE;
    }
    public String getLINKMAN_NAME() {
        return LINKMAN_NAME;
    }
    public void setLINKMAN_NAME(String aLINKMAN_NAME) {
        LINKMAN_NAME = aLINKMAN_NAME;
    }
    public String getFLG() {
        return FLG;
    }
    public void setFLG(String aFLG) {
        FLG = aFLG;
    }
    public String getSUPERVISOR_NAME() {
        return SUPERVISOR_NAME;
    }
    public void setSUPERVISOR_NAME(String aSUPERVISOR_NAME) {
        SUPERVISOR_NAME = aSUPERVISOR_NAME;
    }
    public String getINSERT_OPER() {
        return INSERT_OPER;
    }
    public void setINSERT_OPER(String aINSERT_OPER) {
        INSERT_OPER = aINSERT_OPER;
    }
    public String getINSERT_CONSIGNOR() {
        return INSERT_CONSIGNOR;
    }
    public void setINSERT_CONSIGNOR(String aINSERT_CONSIGNOR) {
        INSERT_CONSIGNOR = aINSERT_CONSIGNOR;
    }
    public String getINSERT_TIME() {
        if(INSERT_TIME != null) {
            return fDate.getString(INSERT_TIME);
        } else {
            return null;
        }
    }
    public void setINSERT_TIME(Date aINSERT_TIME) {
        INSERT_TIME = aINSERT_TIME;
    }
    public void setINSERT_TIME(String aINSERT_TIME) {
        if (aINSERT_TIME != null && !aINSERT_TIME.equals("")) {
            INSERT_TIME = fDate.getDate(aINSERT_TIME);
        } else
            INSERT_TIME = null;
    }

    public String getUPDATE_OPER() {
        return UPDATE_OPER;
    }
    public void setUPDATE_OPER(String aUPDATE_OPER) {
        UPDATE_OPER = aUPDATE_OPER;
    }
    public String getUPDATE_CONSIGNOR() {
        return UPDATE_CONSIGNOR;
    }
    public void setUPDATE_CONSIGNOR(String aUPDATE_CONSIGNOR) {
        UPDATE_CONSIGNOR = aUPDATE_CONSIGNOR;
    }
    public String getUPDATE_TIME() {
        if(UPDATE_TIME != null) {
            return fDate.getString(UPDATE_TIME);
        } else {
            return null;
        }
    }
    public void setUPDATE_TIME(Date aUPDATE_TIME) {
        UPDATE_TIME = aUPDATE_TIME;
    }
    public void setUPDATE_TIME(String aUPDATE_TIME) {
        if (aUPDATE_TIME != null && !aUPDATE_TIME.equals("")) {
            UPDATE_TIME = fDate.getDate(aUPDATE_TIME);
        } else
            UPDATE_TIME = null;
    }

    public String getBANK_CODE() {
        return BANK_CODE;
    }
    public void setBANK_CODE(String aBANK_CODE) {
        BANK_CODE = aBANK_CODE;
    }
    public String getAGENCY_ADDR() {
        return AGENCY_ADDR;
    }
    public void setAGENCY_ADDR(String aAGENCY_ADDR) {
        AGENCY_ADDR = aAGENCY_ADDR;
    }
    public String getMNGORG_CODE() {
        return MNGORG_CODE;
    }
    public void setMNGORG_CODE(String aMNGORG_CODE) {
        MNGORG_CODE = aMNGORG_CODE;
    }

    /**
    * 使用另外一个 T_AGENCY_ORGSchema 对象给 Schema 赋值
    * @param: aT_AGENCY_ORGSchema T_AGENCY_ORGSchema
    **/
    public void setSchema(T_AGENCY_ORGSchema aT_AGENCY_ORGSchema) {
        this.AGENCY_ID = aT_AGENCY_ORGSchema.getAGENCY_ID();
        this.MNGORG_ID = aT_AGENCY_ORGSchema.getMNGORG_ID();
        this.AGENCY_CODE = aT_AGENCY_ORGSchema.getAGENCY_CODE();
        this.AGENCY_NAME = aT_AGENCY_ORGSchema.getAGENCY_NAME();
        this.AGENCY_ABBR = aT_AGENCY_ORGSchema.getAGENCY_ABBR();
        this.EXTER_ANNOUNCE_ORGCODE = aT_AGENCY_ORGSchema.getEXTER_ANNOUNCE_ORGCODE();
        this.SUPERIOR_ORG_ID = aT_AGENCY_ORGSchema.getSUPERIOR_ORG_ID();
        this.AGENCY_LEVEL = aT_AGENCY_ORGSchema.getAGENCY_LEVEL();
        this.AGENCY_REGION_TYPE = aT_AGENCY_ORGSchema.getAGENCY_REGION_TYPE();
        this.AGENCY_STATUS = aT_AGENCY_ORGSchema.getAGENCY_STATUS();
        this.AGENCY_TYPE = aT_AGENCY_ORGSchema.getAGENCY_TYPE();
        this.CORRESP_CORE_CODE = aT_AGENCY_ORGSchema.getCORRESP_CORE_CODE();
        this.LINKMAN_NAME = aT_AGENCY_ORGSchema.getLINKMAN_NAME();
        this.FLG = aT_AGENCY_ORGSchema.getFLG();
        this.SUPERVISOR_NAME = aT_AGENCY_ORGSchema.getSUPERVISOR_NAME();
        this.INSERT_OPER = aT_AGENCY_ORGSchema.getINSERT_OPER();
        this.INSERT_CONSIGNOR = aT_AGENCY_ORGSchema.getINSERT_CONSIGNOR();
        this.INSERT_TIME = fDate.getDate( aT_AGENCY_ORGSchema.getINSERT_TIME());
        this.UPDATE_OPER = aT_AGENCY_ORGSchema.getUPDATE_OPER();
        this.UPDATE_CONSIGNOR = aT_AGENCY_ORGSchema.getUPDATE_CONSIGNOR();
        this.UPDATE_TIME = fDate.getDate( aT_AGENCY_ORGSchema.getUPDATE_TIME());
        this.BANK_CODE = aT_AGENCY_ORGSchema.getBANK_CODE();
        this.AGENCY_ADDR = aT_AGENCY_ORGSchema.getAGENCY_ADDR();
        this.MNGORG_CODE = aT_AGENCY_ORGSchema.getMNGORG_CODE();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            this.AGENCY_ID = rs.getLong("AGENCY_ID");
            this.MNGORG_ID = rs.getLong("MNGORG_ID");
            if( rs.getString("AGENCY_CODE") == null )
                this.AGENCY_CODE = null;
            else
                this.AGENCY_CODE = rs.getString("AGENCY_CODE").trim();

            if( rs.getString("AGENCY_NAME") == null )
                this.AGENCY_NAME = null;
            else
                this.AGENCY_NAME = rs.getString("AGENCY_NAME").trim();

            if( rs.getString("AGENCY_ABBR") == null )
                this.AGENCY_ABBR = null;
            else
                this.AGENCY_ABBR = rs.getString("AGENCY_ABBR").trim();

            if( rs.getString("EXTER_ANNOUNCE_ORGCODE") == null )
                this.EXTER_ANNOUNCE_ORGCODE = null;
            else
                this.EXTER_ANNOUNCE_ORGCODE = rs.getString("EXTER_ANNOUNCE_ORGCODE").trim();

            this.SUPERIOR_ORG_ID = rs.getLong("SUPERIOR_ORG_ID");
            if( rs.getString("AGENCY_LEVEL") == null )
                this.AGENCY_LEVEL = null;
            else
                this.AGENCY_LEVEL = rs.getString("AGENCY_LEVEL").trim();

            if( rs.getString("AGENCY_REGION_TYPE") == null )
                this.AGENCY_REGION_TYPE = null;
            else
                this.AGENCY_REGION_TYPE = rs.getString("AGENCY_REGION_TYPE").trim();

            if( rs.getString("AGENCY_STATUS") == null )
                this.AGENCY_STATUS = null;
            else
                this.AGENCY_STATUS = rs.getString("AGENCY_STATUS").trim();

            if( rs.getString("AGENCY_TYPE") == null )
                this.AGENCY_TYPE = null;
            else
                this.AGENCY_TYPE = rs.getString("AGENCY_TYPE").trim();

            if( rs.getString("CORRESP_CORE_CODE") == null )
                this.CORRESP_CORE_CODE = null;
            else
                this.CORRESP_CORE_CODE = rs.getString("CORRESP_CORE_CODE").trim();

            if( rs.getString("LINKMAN_NAME") == null )
                this.LINKMAN_NAME = null;
            else
                this.LINKMAN_NAME = rs.getString("LINKMAN_NAME").trim();

            if( rs.getString("FLG") == null )
                this.FLG = null;
            else
                this.FLG = rs.getString("FLG").trim();

            if( rs.getString("SUPERVISOR_NAME") == null )
                this.SUPERVISOR_NAME = null;
            else
                this.SUPERVISOR_NAME = rs.getString("SUPERVISOR_NAME").trim();

            if( rs.getString("INSERT_OPER") == null )
                this.INSERT_OPER = null;
            else
                this.INSERT_OPER = rs.getString("INSERT_OPER").trim();

            if( rs.getString("INSERT_CONSIGNOR") == null )
                this.INSERT_CONSIGNOR = null;
            else
                this.INSERT_CONSIGNOR = rs.getString("INSERT_CONSIGNOR").trim();

            this.INSERT_TIME = rs.getDate("INSERT_TIME");
            if( rs.getString("UPDATE_OPER") == null )
                this.UPDATE_OPER = null;
            else
                this.UPDATE_OPER = rs.getString("UPDATE_OPER").trim();

            if( rs.getString("UPDATE_CONSIGNOR") == null )
                this.UPDATE_CONSIGNOR = null;
            else
                this.UPDATE_CONSIGNOR = rs.getString("UPDATE_CONSIGNOR").trim();

            this.UPDATE_TIME = rs.getDate("UPDATE_TIME");
            if( rs.getString("BANK_CODE") == null )
                this.BANK_CODE = null;
            else
                this.BANK_CODE = rs.getString("BANK_CODE").trim();

            if( rs.getString("AGENCY_ADDR") == null )
                this.AGENCY_ADDR = null;
            else
                this.AGENCY_ADDR = rs.getString("AGENCY_ADDR").trim();

            if( rs.getString("MNGORG_CODE") == null )
                this.MNGORG_CODE = null;
            else
                this.MNGORG_CODE = rs.getString("MNGORG_CODE").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public T_AGENCY_ORGSchema getSchema() {
        T_AGENCY_ORGSchema aT_AGENCY_ORGSchema = new T_AGENCY_ORGSchema();
        aT_AGENCY_ORGSchema.setSchema(this);
        return aT_AGENCY_ORGSchema;
    }

    public T_AGENCY_ORGDB getDB() {
        T_AGENCY_ORGDB aDBOper = new T_AGENCY_ORGDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpT_AGENCY_ORG描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(AGENCY_ID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MNGORG_ID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_NAME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_ABBR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EXTER_ANNOUNCE_ORGCODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SUPERIOR_ORG_ID));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_LEVEL)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_REGION_TYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_STATUS)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_TYPE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CORRESP_CORE_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LINKMAN_NAME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FLG)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SUPERVISOR_NAME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(INSERT_OPER)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(INSERT_CONSIGNOR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( INSERT_TIME ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UPDATE_OPER)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UPDATE_CONSIGNOR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UPDATE_TIME ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BANK_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AGENCY_ADDR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MNGORG_CODE));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpT_AGENCY_ORG>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            AGENCY_ID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,1, SysConst.PACKAGESPILTER))).longValue();
            MNGORG_ID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,2, SysConst.PACKAGESPILTER))).longValue();
            AGENCY_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AGENCY_NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AGENCY_ABBR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            EXTER_ANNOUNCE_ORGCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            SUPERIOR_ORG_ID = new Long(ChgData.chgNumericStr(StrTool.getStr(strMessage,7, SysConst.PACKAGESPILTER))).longValue();
            AGENCY_LEVEL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            AGENCY_REGION_TYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            AGENCY_STATUS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            AGENCY_TYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            CORRESP_CORE_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            LINKMAN_NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            FLG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            SUPERVISOR_NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            INSERT_OPER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            INSERT_CONSIGNOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            INSERT_TIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER));
            UPDATE_OPER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            UPDATE_CONSIGNOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            UPDATE_TIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER));
            BANK_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            AGENCY_ADDR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            MNGORG_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "T_AGENCY_ORGSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AGENCY_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_ID));
        }
        if (FCode.equalsIgnoreCase("MNGORG_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MNGORG_ID));
        }
        if (FCode.equalsIgnoreCase("AGENCY_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_NAME));
        }
        if (FCode.equalsIgnoreCase("AGENCY_ABBR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_ABBR));
        }
        if (FCode.equalsIgnoreCase("EXTER_ANNOUNCE_ORGCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EXTER_ANNOUNCE_ORGCODE));
        }
        if (FCode.equalsIgnoreCase("SUPERIOR_ORG_ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SUPERIOR_ORG_ID));
        }
        if (FCode.equalsIgnoreCase("AGENCY_LEVEL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_LEVEL));
        }
        if (FCode.equalsIgnoreCase("AGENCY_REGION_TYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_REGION_TYPE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_STATUS")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_STATUS));
        }
        if (FCode.equalsIgnoreCase("AGENCY_TYPE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_TYPE));
        }
        if (FCode.equalsIgnoreCase("CORRESP_CORE_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CORRESP_CORE_CODE));
        }
        if (FCode.equalsIgnoreCase("LINKMAN_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LINKMAN_NAME));
        }
        if (FCode.equalsIgnoreCase("FLG")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FLG));
        }
        if (FCode.equalsIgnoreCase("SUPERVISOR_NAME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SUPERVISOR_NAME));
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_OPER));
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(INSERT_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getINSERT_TIME()));
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_OPER));
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UPDATE_CONSIGNOR));
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUPDATE_TIME()));
        }
        if (FCode.equalsIgnoreCase("BANK_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_CODE));
        }
        if (FCode.equalsIgnoreCase("AGENCY_ADDR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AGENCY_ADDR));
        }
        if (FCode.equalsIgnoreCase("MNGORG_CODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MNGORG_CODE));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(AGENCY_ID);
                break;
            case 1:
                strFieldValue = String.valueOf(MNGORG_ID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_CODE);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_NAME);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_ABBR);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(EXTER_ANNOUNCE_ORGCODE);
                break;
            case 6:
                strFieldValue = String.valueOf(SUPERIOR_ORG_ID);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_LEVEL);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_REGION_TYPE);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_STATUS);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_TYPE);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(CORRESP_CORE_CODE);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(LINKMAN_NAME);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(FLG);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(SUPERVISOR_NAME);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(INSERT_OPER);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(INSERT_CONSIGNOR);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getINSERT_TIME()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(UPDATE_OPER);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(UPDATE_CONSIGNOR);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUPDATE_TIME()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(BANK_CODE);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(AGENCY_ADDR);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MNGORG_CODE);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AGENCY_ID")) {
            if( FValue != null && !FValue.equals("")) {
                AGENCY_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("MNGORG_ID")) {
            if( FValue != null && !FValue.equals("")) {
                MNGORG_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("AGENCY_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_CODE = FValue.trim();
            }
            else
                AGENCY_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_NAME = FValue.trim();
            }
            else
                AGENCY_NAME = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_ABBR")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_ABBR = FValue.trim();
            }
            else
                AGENCY_ABBR = null;
        }
        if (FCode.equalsIgnoreCase("EXTER_ANNOUNCE_ORGCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                EXTER_ANNOUNCE_ORGCODE = FValue.trim();
            }
            else
                EXTER_ANNOUNCE_ORGCODE = null;
        }
        if (FCode.equalsIgnoreCase("SUPERIOR_ORG_ID")) {
            if( FValue != null && !FValue.equals("")) {
                SUPERIOR_ORG_ID = new Long(FValue).longValue();
            }
        }
        if (FCode.equalsIgnoreCase("AGENCY_LEVEL")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_LEVEL = FValue.trim();
            }
            else
                AGENCY_LEVEL = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_REGION_TYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_REGION_TYPE = FValue.trim();
            }
            else
                AGENCY_REGION_TYPE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_STATUS")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_STATUS = FValue.trim();
            }
            else
                AGENCY_STATUS = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_TYPE")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_TYPE = FValue.trim();
            }
            else
                AGENCY_TYPE = null;
        }
        if (FCode.equalsIgnoreCase("CORRESP_CORE_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                CORRESP_CORE_CODE = FValue.trim();
            }
            else
                CORRESP_CORE_CODE = null;
        }
        if (FCode.equalsIgnoreCase("LINKMAN_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                LINKMAN_NAME = FValue.trim();
            }
            else
                LINKMAN_NAME = null;
        }
        if (FCode.equalsIgnoreCase("FLG")) {
            if( FValue != null && !FValue.equals(""))
            {
                FLG = FValue.trim();
            }
            else
                FLG = null;
        }
        if (FCode.equalsIgnoreCase("SUPERVISOR_NAME")) {
            if( FValue != null && !FValue.equals(""))
            {
                SUPERVISOR_NAME = FValue.trim();
            }
            else
                SUPERVISOR_NAME = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_OPER = FValue.trim();
            }
            else
                INSERT_OPER = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                INSERT_CONSIGNOR = FValue.trim();
            }
            else
                INSERT_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("INSERT_TIME")) {
            if(FValue != null && !FValue.equals("")) {
                INSERT_TIME = fDate.getDate( FValue );
            }
            else
                INSERT_TIME = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_OPER")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_OPER = FValue.trim();
            }
            else
                UPDATE_OPER = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_CONSIGNOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                UPDATE_CONSIGNOR = FValue.trim();
            }
            else
                UPDATE_CONSIGNOR = null;
        }
        if (FCode.equalsIgnoreCase("UPDATE_TIME")) {
            if(FValue != null && !FValue.equals("")) {
                UPDATE_TIME = fDate.getDate( FValue );
            }
            else
                UPDATE_TIME = null;
        }
        if (FCode.equalsIgnoreCase("BANK_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                BANK_CODE = FValue.trim();
            }
            else
                BANK_CODE = null;
        }
        if (FCode.equalsIgnoreCase("AGENCY_ADDR")) {
            if( FValue != null && !FValue.equals(""))
            {
                AGENCY_ADDR = FValue.trim();
            }
            else
                AGENCY_ADDR = null;
        }
        if (FCode.equalsIgnoreCase("MNGORG_CODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                MNGORG_CODE = FValue.trim();
            }
            else
                MNGORG_CODE = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        T_AGENCY_ORGSchema other = (T_AGENCY_ORGSchema)otherObject;
        return
            AGENCY_ID == other.getAGENCY_ID()
            && MNGORG_ID == other.getMNGORG_ID()
            && AGENCY_CODE.equals(other.getAGENCY_CODE())
            && AGENCY_NAME.equals(other.getAGENCY_NAME())
            && AGENCY_ABBR.equals(other.getAGENCY_ABBR())
            && EXTER_ANNOUNCE_ORGCODE.equals(other.getEXTER_ANNOUNCE_ORGCODE())
            && SUPERIOR_ORG_ID == other.getSUPERIOR_ORG_ID()
            && AGENCY_LEVEL.equals(other.getAGENCY_LEVEL())
            && AGENCY_REGION_TYPE.equals(other.getAGENCY_REGION_TYPE())
            && AGENCY_STATUS.equals(other.getAGENCY_STATUS())
            && AGENCY_TYPE.equals(other.getAGENCY_TYPE())
            && CORRESP_CORE_CODE.equals(other.getCORRESP_CORE_CODE())
            && LINKMAN_NAME.equals(other.getLINKMAN_NAME())
            && FLG.equals(other.getFLG())
            && SUPERVISOR_NAME.equals(other.getSUPERVISOR_NAME())
            && INSERT_OPER.equals(other.getINSERT_OPER())
            && INSERT_CONSIGNOR.equals(other.getINSERT_CONSIGNOR())
            && fDate.getString(INSERT_TIME).equals(other.getINSERT_TIME())
            && UPDATE_OPER.equals(other.getUPDATE_OPER())
            && UPDATE_CONSIGNOR.equals(other.getUPDATE_CONSIGNOR())
            && fDate.getString(UPDATE_TIME).equals(other.getUPDATE_TIME())
            && BANK_CODE.equals(other.getBANK_CODE())
            && AGENCY_ADDR.equals(other.getAGENCY_ADDR())
            && MNGORG_CODE.equals(other.getMNGORG_CODE());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AGENCY_ID") ) {
            return 0;
        }
        if( strFieldName.equals("MNGORG_ID") ) {
            return 1;
        }
        if( strFieldName.equals("AGENCY_CODE") ) {
            return 2;
        }
        if( strFieldName.equals("AGENCY_NAME") ) {
            return 3;
        }
        if( strFieldName.equals("AGENCY_ABBR") ) {
            return 4;
        }
        if( strFieldName.equals("EXTER_ANNOUNCE_ORGCODE") ) {
            return 5;
        }
        if( strFieldName.equals("SUPERIOR_ORG_ID") ) {
            return 6;
        }
        if( strFieldName.equals("AGENCY_LEVEL") ) {
            return 7;
        }
        if( strFieldName.equals("AGENCY_REGION_TYPE") ) {
            return 8;
        }
        if( strFieldName.equals("AGENCY_STATUS") ) {
            return 9;
        }
        if( strFieldName.equals("AGENCY_TYPE") ) {
            return 10;
        }
        if( strFieldName.equals("CORRESP_CORE_CODE") ) {
            return 11;
        }
        if( strFieldName.equals("LINKMAN_NAME") ) {
            return 12;
        }
        if( strFieldName.equals("FLG") ) {
            return 13;
        }
        if( strFieldName.equals("SUPERVISOR_NAME") ) {
            return 14;
        }
        if( strFieldName.equals("INSERT_OPER") ) {
            return 15;
        }
        if( strFieldName.equals("INSERT_CONSIGNOR") ) {
            return 16;
        }
        if( strFieldName.equals("INSERT_TIME") ) {
            return 17;
        }
        if( strFieldName.equals("UPDATE_OPER") ) {
            return 18;
        }
        if( strFieldName.equals("UPDATE_CONSIGNOR") ) {
            return 19;
        }
        if( strFieldName.equals("UPDATE_TIME") ) {
            return 20;
        }
        if( strFieldName.equals("BANK_CODE") ) {
            return 21;
        }
        if( strFieldName.equals("AGENCY_ADDR") ) {
            return 22;
        }
        if( strFieldName.equals("MNGORG_CODE") ) {
            return 23;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AGENCY_ID";
                break;
            case 1:
                strFieldName = "MNGORG_ID";
                break;
            case 2:
                strFieldName = "AGENCY_CODE";
                break;
            case 3:
                strFieldName = "AGENCY_NAME";
                break;
            case 4:
                strFieldName = "AGENCY_ABBR";
                break;
            case 5:
                strFieldName = "EXTER_ANNOUNCE_ORGCODE";
                break;
            case 6:
                strFieldName = "SUPERIOR_ORG_ID";
                break;
            case 7:
                strFieldName = "AGENCY_LEVEL";
                break;
            case 8:
                strFieldName = "AGENCY_REGION_TYPE";
                break;
            case 9:
                strFieldName = "AGENCY_STATUS";
                break;
            case 10:
                strFieldName = "AGENCY_TYPE";
                break;
            case 11:
                strFieldName = "CORRESP_CORE_CODE";
                break;
            case 12:
                strFieldName = "LINKMAN_NAME";
                break;
            case 13:
                strFieldName = "FLG";
                break;
            case 14:
                strFieldName = "SUPERVISOR_NAME";
                break;
            case 15:
                strFieldName = "INSERT_OPER";
                break;
            case 16:
                strFieldName = "INSERT_CONSIGNOR";
                break;
            case 17:
                strFieldName = "INSERT_TIME";
                break;
            case 18:
                strFieldName = "UPDATE_OPER";
                break;
            case 19:
                strFieldName = "UPDATE_CONSIGNOR";
                break;
            case 20:
                strFieldName = "UPDATE_TIME";
                break;
            case 21:
                strFieldName = "BANK_CODE";
                break;
            case 22:
                strFieldName = "AGENCY_ADDR";
                break;
            case 23:
                strFieldName = "MNGORG_CODE";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENCY_ID":
                return Schema.TYPE_LONG;
            case "MNGORG_ID":
                return Schema.TYPE_LONG;
            case "AGENCY_CODE":
                return Schema.TYPE_STRING;
            case "AGENCY_NAME":
                return Schema.TYPE_STRING;
            case "AGENCY_ABBR":
                return Schema.TYPE_STRING;
            case "EXTER_ANNOUNCE_ORGCODE":
                return Schema.TYPE_STRING;
            case "SUPERIOR_ORG_ID":
                return Schema.TYPE_LONG;
            case "AGENCY_LEVEL":
                return Schema.TYPE_STRING;
            case "AGENCY_REGION_TYPE":
                return Schema.TYPE_STRING;
            case "AGENCY_STATUS":
                return Schema.TYPE_STRING;
            case "AGENCY_TYPE":
                return Schema.TYPE_STRING;
            case "CORRESP_CORE_CODE":
                return Schema.TYPE_STRING;
            case "LINKMAN_NAME":
                return Schema.TYPE_STRING;
            case "FLG":
                return Schema.TYPE_STRING;
            case "SUPERVISOR_NAME":
                return Schema.TYPE_STRING;
            case "INSERT_OPER":
                return Schema.TYPE_STRING;
            case "INSERT_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "INSERT_TIME":
                return Schema.TYPE_DATE;
            case "UPDATE_OPER":
                return Schema.TYPE_STRING;
            case "UPDATE_CONSIGNOR":
                return Schema.TYPE_STRING;
            case "UPDATE_TIME":
                return Schema.TYPE_DATE;
            case "BANK_CODE":
                return Schema.TYPE_STRING;
            case "AGENCY_ADDR":
                return Schema.TYPE_STRING;
            case "MNGORG_CODE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_LONG;
            case 1:
                return Schema.TYPE_LONG;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_LONG;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_DATE;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "T_AGENCY_ORGSchema {" +
            "AGENCY_ID="+AGENCY_ID +
            ", MNGORG_ID="+MNGORG_ID +
            ", AGENCY_CODE="+AGENCY_CODE +
            ", AGENCY_NAME="+AGENCY_NAME +
            ", AGENCY_ABBR="+AGENCY_ABBR +
            ", EXTER_ANNOUNCE_ORGCODE="+EXTER_ANNOUNCE_ORGCODE +
            ", SUPERIOR_ORG_ID="+SUPERIOR_ORG_ID +
            ", AGENCY_LEVEL="+AGENCY_LEVEL +
            ", AGENCY_REGION_TYPE="+AGENCY_REGION_TYPE +
            ", AGENCY_STATUS="+AGENCY_STATUS +
            ", AGENCY_TYPE="+AGENCY_TYPE +
            ", CORRESP_CORE_CODE="+CORRESP_CORE_CODE +
            ", LINKMAN_NAME="+LINKMAN_NAME +
            ", FLG="+FLG +
            ", SUPERVISOR_NAME="+SUPERVISOR_NAME +
            ", INSERT_OPER="+INSERT_OPER +
            ", INSERT_CONSIGNOR="+INSERT_CONSIGNOR +
            ", INSERT_TIME="+INSERT_TIME +
            ", UPDATE_OPER="+UPDATE_OPER +
            ", UPDATE_CONSIGNOR="+UPDATE_CONSIGNOR +
            ", UPDATE_TIME="+UPDATE_TIME +
            ", BANK_CODE="+BANK_CODE +
            ", AGENCY_ADDR="+AGENCY_ADDR +
            ", MNGORG_CODE="+MNGORG_CODE +"}";
    }
}
