/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LDAvgInComDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LDAvgInComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-03-21
 */
public class LDAvgInComSchema implements Schema, Cloneable {
    // @Field
    /** Seqno */
    private String SEQNO;
    /** Comcode */
    private String COMCODE;
    /** Cityincome */
    private double CITYINCOME;
    /** Villageincome */
    private double VILLAGEINCOME;
    /** Year */
    private String YEAR;
    /** Operator */
    private String OPERATOR;
    /** Makedate */
    private Date MAKEDATE;
    /** Maketime */
    private String MAKETIME;
    /** Modifydate */
    private Date MODIFYDATE;
    /** Modifytime */
    private String MODIFYTIME;
    /** Standbyflag1 */
    private String STANDBYFLAG1;

    public static final int FIELDNUM = 11;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDAvgInComSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SEQNO";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDAvgInComSchema cloned = (LDAvgInComSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSEQNO() {
        return SEQNO;
    }
    public void setSEQNO(String aSEQNO) {
        SEQNO = aSEQNO;
    }
    public String getCOMCODE() {
        return COMCODE;
    }
    public void setCOMCODE(String aCOMCODE) {
        COMCODE = aCOMCODE;
    }
    public double getCITYINCOME() {
        return CITYINCOME;
    }
    public void setCITYINCOME(double aCITYINCOME) {
        CITYINCOME = aCITYINCOME;
    }
    public void setCITYINCOME(String aCITYINCOME) {
        if (aCITYINCOME != null && !aCITYINCOME.equals("")) {
            Double tDouble = new Double(aCITYINCOME);
            double d = tDouble.doubleValue();
            CITYINCOME = d;
        }
    }

    public double getVILLAGEINCOME() {
        return VILLAGEINCOME;
    }
    public void setVILLAGEINCOME(double aVILLAGEINCOME) {
        VILLAGEINCOME = aVILLAGEINCOME;
    }
    public void setVILLAGEINCOME(String aVILLAGEINCOME) {
        if (aVILLAGEINCOME != null && !aVILLAGEINCOME.equals("")) {
            Double tDouble = new Double(aVILLAGEINCOME);
            double d = tDouble.doubleValue();
            VILLAGEINCOME = d;
        }
    }

    public String getYEAR() {
        return YEAR;
    }
    public void setYEAR(String aYEAR) {
        YEAR = aYEAR;
    }
    public String getOPERATOR() {
        return OPERATOR;
    }
    public void setOPERATOR(String aOPERATOR) {
        OPERATOR = aOPERATOR;
    }
    public String getMAKEDATE() {
        if(MAKEDATE != null) {
            return fDate.getString(MAKEDATE);
        } else {
            return null;
        }
    }
    public void setMAKEDATE(Date aMAKEDATE) {
        MAKEDATE = aMAKEDATE;
    }
    public void setMAKEDATE(String aMAKEDATE) {
        if (aMAKEDATE != null && !aMAKEDATE.equals("")) {
            MAKEDATE = fDate.getDate(aMAKEDATE);
        } else
            MAKEDATE = null;
    }

    public String getMAKETIME() {
        return MAKETIME;
    }
    public void setMAKETIME(String aMAKETIME) {
        MAKETIME = aMAKETIME;
    }
    public String getMODIFYDATE() {
        if(MODIFYDATE != null) {
            return fDate.getString(MODIFYDATE);
        } else {
            return null;
        }
    }
    public void setMODIFYDATE(Date aMODIFYDATE) {
        MODIFYDATE = aMODIFYDATE;
    }
    public void setMODIFYDATE(String aMODIFYDATE) {
        if (aMODIFYDATE != null && !aMODIFYDATE.equals("")) {
            MODIFYDATE = fDate.getDate(aMODIFYDATE);
        } else
            MODIFYDATE = null;
    }

    public String getMODIFYTIME() {
        return MODIFYTIME;
    }
    public void setMODIFYTIME(String aMODIFYTIME) {
        MODIFYTIME = aMODIFYTIME;
    }
    public String getSTANDBYFLAG1() {
        return STANDBYFLAG1;
    }
    public void setSTANDBYFLAG1(String aSTANDBYFLAG1) {
        STANDBYFLAG1 = aSTANDBYFLAG1;
    }

    /**
    * 使用另外一个 LDAvgInComSchema 对象给 Schema 赋值
    * @param: aLDAvgInComSchema LDAvgInComSchema
    **/
    public void setSchema(LDAvgInComSchema aLDAvgInComSchema) {
        this.SEQNO = aLDAvgInComSchema.getSEQNO();
        this.COMCODE = aLDAvgInComSchema.getCOMCODE();
        this.CITYINCOME = aLDAvgInComSchema.getCITYINCOME();
        this.VILLAGEINCOME = aLDAvgInComSchema.getVILLAGEINCOME();
        this.YEAR = aLDAvgInComSchema.getYEAR();
        this.OPERATOR = aLDAvgInComSchema.getOPERATOR();
        this.MAKEDATE = fDate.getDate( aLDAvgInComSchema.getMAKEDATE());
        this.MAKETIME = aLDAvgInComSchema.getMAKETIME();
        this.MODIFYDATE = fDate.getDate( aLDAvgInComSchema.getMODIFYDATE());
        this.MODIFYTIME = aLDAvgInComSchema.getMODIFYTIME();
        this.STANDBYFLAG1 = aLDAvgInComSchema.getSTANDBYFLAG1();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SEQNO") == null )
                this.SEQNO = null;
            else
                this.SEQNO = rs.getString("SEQNO").trim();

            if( rs.getString("COMCODE") == null )
                this.COMCODE = null;
            else
                this.COMCODE = rs.getString("COMCODE").trim();

            this.CITYINCOME = rs.getDouble("CITYINCOME");
            this.VILLAGEINCOME = rs.getDouble("VILLAGEINCOME");
            if( rs.getString("YEAR") == null )
                this.YEAR = null;
            else
                this.YEAR = rs.getString("YEAR").trim();

            if( rs.getString("OPERATOR") == null )
                this.OPERATOR = null;
            else
                this.OPERATOR = rs.getString("OPERATOR").trim();

            this.MAKEDATE = rs.getDate("MAKEDATE");
            if( rs.getString("MAKETIME") == null )
                this.MAKETIME = null;
            else
                this.MAKETIME = rs.getString("MAKETIME").trim();

            this.MODIFYDATE = rs.getDate("MODIFYDATE");
            if( rs.getString("MODIFYTIME") == null )
                this.MODIFYTIME = null;
            else
                this.MODIFYTIME = rs.getString("MODIFYTIME").trim();

            if( rs.getString("STANDBYFLAG1") == null )
                this.STANDBYFLAG1 = null;
            else
                this.STANDBYFLAG1 = rs.getString("STANDBYFLAG1").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDAvgInComSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDAvgInComSchema getSchema() {
        LDAvgInComSchema aLDAvgInComSchema = new LDAvgInComSchema();
        aLDAvgInComSchema.setSchema(this);
        return aLDAvgInComSchema;
    }

    public LDAvgInComDB getDB() {
        LDAvgInComDB aDBOper = new LDAvgInComDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDAvgInCom描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SEQNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(COMCODE)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CITYINCOME));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(VILLAGEINCOME));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(YEAR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OPERATOR)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MAKEDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MAKETIME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MODIFYDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MODIFYTIME)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(STANDBYFLAG1));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDAvgInCom>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SEQNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            COMCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            CITYINCOME = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).doubleValue();
            VILLAGEINCOME = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).doubleValue();
            YEAR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            OPERATOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            MAKEDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
            MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            MODIFYDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
            MODIFYTIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            STANDBYFLAG1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDAvgInComSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SEQNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SEQNO));
        }
        if (FCode.equalsIgnoreCase("COMCODE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(COMCODE));
        }
        if (FCode.equalsIgnoreCase("CITYINCOME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CITYINCOME));
        }
        if (FCode.equalsIgnoreCase("VILLAGEINCOME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VILLAGEINCOME));
        }
        if (FCode.equalsIgnoreCase("YEAR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(YEAR));
        }
        if (FCode.equalsIgnoreCase("OPERATOR")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OPERATOR));
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(STANDBYFLAG1));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SEQNO);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(COMCODE);
                break;
            case 2:
                strFieldValue = String.valueOf(CITYINCOME);
                break;
            case 3:
                strFieldValue = String.valueOf(VILLAGEINCOME);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(YEAR);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(OPERATOR);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MAKETIME);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MODIFYTIME);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(STANDBYFLAG1);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SEQNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SEQNO = FValue.trim();
            }
            else
                SEQNO = null;
        }
        if (FCode.equalsIgnoreCase("COMCODE")) {
            if( FValue != null && !FValue.equals(""))
            {
                COMCODE = FValue.trim();
            }
            else
                COMCODE = null;
        }
        if (FCode.equalsIgnoreCase("CITYINCOME")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                CITYINCOME = d;
            }
        }
        if (FCode.equalsIgnoreCase("VILLAGEINCOME")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                VILLAGEINCOME = d;
            }
        }
        if (FCode.equalsIgnoreCase("YEAR")) {
            if( FValue != null && !FValue.equals(""))
            {
                YEAR = FValue.trim();
            }
            else
                YEAR = null;
        }
        if (FCode.equalsIgnoreCase("OPERATOR")) {
            if( FValue != null && !FValue.equals(""))
            {
                OPERATOR = FValue.trim();
            }
            else
                OPERATOR = null;
        }
        if (FCode.equalsIgnoreCase("MAKEDATE")) {
            if(FValue != null && !FValue.equals("")) {
                MAKEDATE = fDate.getDate( FValue );
            }
            else
                MAKEDATE = null;
        }
        if (FCode.equalsIgnoreCase("MAKETIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MAKETIME = FValue.trim();
            }
            else
                MAKETIME = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYDATE")) {
            if(FValue != null && !FValue.equals("")) {
                MODIFYDATE = fDate.getDate( FValue );
            }
            else
                MODIFYDATE = null;
        }
        if (FCode.equalsIgnoreCase("MODIFYTIME")) {
            if( FValue != null && !FValue.equals(""))
            {
                MODIFYTIME = FValue.trim();
            }
            else
                MODIFYTIME = null;
        }
        if (FCode.equalsIgnoreCase("STANDBYFLAG1")) {
            if( FValue != null && !FValue.equals(""))
            {
                STANDBYFLAG1 = FValue.trim();
            }
            else
                STANDBYFLAG1 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDAvgInComSchema other = (LDAvgInComSchema)otherObject;
        return
            SEQNO.equals(other.getSEQNO())
            && COMCODE.equals(other.getCOMCODE())
            && CITYINCOME == other.getCITYINCOME()
            && VILLAGEINCOME == other.getVILLAGEINCOME()
            && YEAR.equals(other.getYEAR())
            && OPERATOR.equals(other.getOPERATOR())
            && fDate.getString(MAKEDATE).equals(other.getMAKEDATE())
            && MAKETIME.equals(other.getMAKETIME())
            && fDate.getString(MODIFYDATE).equals(other.getMODIFYDATE())
            && MODIFYTIME.equals(other.getMODIFYTIME())
            && STANDBYFLAG1.equals(other.getSTANDBYFLAG1());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SEQNO") ) {
            return 0;
        }
        if( strFieldName.equals("COMCODE") ) {
            return 1;
        }
        if( strFieldName.equals("CITYINCOME") ) {
            return 2;
        }
        if( strFieldName.equals("VILLAGEINCOME") ) {
            return 3;
        }
        if( strFieldName.equals("YEAR") ) {
            return 4;
        }
        if( strFieldName.equals("OPERATOR") ) {
            return 5;
        }
        if( strFieldName.equals("MAKEDATE") ) {
            return 6;
        }
        if( strFieldName.equals("MAKETIME") ) {
            return 7;
        }
        if( strFieldName.equals("MODIFYDATE") ) {
            return 8;
        }
        if( strFieldName.equals("MODIFYTIME") ) {
            return 9;
        }
        if( strFieldName.equals("STANDBYFLAG1") ) {
            return 10;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SEQNO";
                break;
            case 1:
                strFieldName = "COMCODE";
                break;
            case 2:
                strFieldName = "CITYINCOME";
                break;
            case 3:
                strFieldName = "VILLAGEINCOME";
                break;
            case 4:
                strFieldName = "YEAR";
                break;
            case 5:
                strFieldName = "OPERATOR";
                break;
            case 6:
                strFieldName = "MAKEDATE";
                break;
            case 7:
                strFieldName = "MAKETIME";
                break;
            case 8:
                strFieldName = "MODIFYDATE";
                break;
            case 9:
                strFieldName = "MODIFYTIME";
                break;
            case 10:
                strFieldName = "STANDBYFLAG1";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SEQNO":
                return Schema.TYPE_STRING;
            case "COMCODE":
                return Schema.TYPE_STRING;
            case "CITYINCOME":
                return Schema.TYPE_DOUBLE;
            case "VILLAGEINCOME":
                return Schema.TYPE_DOUBLE;
            case "YEAR":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_DOUBLE;
            case 3:
                return Schema.TYPE_DOUBLE;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    public String toString() {
    return "LDAvgInComSchema {" +
            "SEQNO="+SEQNO +
            ", COMCODE="+COMCODE +
            ", CITYINCOME="+CITYINCOME +
            ", VILLAGEINCOME="+VILLAGEINCOME +
            ", YEAR="+YEAR +
            ", OPERATOR="+OPERATOR +
            ", MAKEDATE="+MAKEDATE +
            ", MAKETIME="+MAKETIME +
            ", MODIFYDATE="+MODIFYDATE +
            ", MODIFYTIME="+MODIFYTIME +
            ", STANDBYFLAG1="+STANDBYFLAG1 +"}";
    }
}
