/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LMAccGuratRateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LMAccGuratRateDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMAccGuratRateDBSet extends LMAccGuratRateSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LMAccGuratRateDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LMAccGuratRate");
        mflag = true;
    }

    public LMAccGuratRateDBSet() {
        db = new DBOper( "LMAccGuratRate" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMAccGuratRateDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LMAccGuratRate WHERE  1=1  AND RiskCode = ? AND InsuAccNo = ? AND RateStartDate = ? AND RateIntv = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getRateStartDate() == null || this.get(i).getRateStartDate().equals("null")) {
                pstmt.setDate(3,null);
            } else {
                pstmt.setDate(3, Date.valueOf(this.get(i).getRateStartDate()));
            }
            if(this.get(i).getRateIntv() == null || this.get(i).getRateIntv().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getRateIntv());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMAccGuratRateDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LMAccGuratRate SET  RiskCode = ? , InsuAccNo = ? , RateStartDate = ? , RateEndDate = ? , RateIntv = ? , Rate = ? , Operator = ? , MakeDate = ? , MakeTime = ? WHERE  1=1  AND RiskCode = ? AND InsuAccNo = ? AND RateStartDate = ? AND RateIntv = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getRateStartDate() == null || this.get(i).getRateStartDate().equals("null")) {
                pstmt.setDate(3,null);
            } else {
                pstmt.setDate(3, Date.valueOf(this.get(i).getRateStartDate()));
            }
            if(this.get(i).getRateEndDate() == null || this.get(i).getRateEndDate().equals("null")) {
                pstmt.setDate(4,null);
            } else {
                pstmt.setDate(4, Date.valueOf(this.get(i).getRateEndDate()));
            }
            if(this.get(i).getRateIntv() == null || this.get(i).getRateIntv().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getRateIntv());
            }
            pstmt.setDouble(6, this.get(i).getRate());
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(8,null);
            } else {
                pstmt.setDate(8, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getMakeTime());
            }
            // set where condition
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getRateStartDate() == null || this.get(i).getRateStartDate().equals("null")) {
                pstmt.setDate(12,null);
            } else {
                pstmt.setDate(12, Date.valueOf(this.get(i).getRateStartDate()));
            }
            if(this.get(i).getRateIntv() == null || this.get(i).getRateIntv().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getRateIntv());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMAccGuratRateDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LMAccGuratRate VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getRiskCode() == null || this.get(i).getRiskCode().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getRiskCode());
            }
            if(this.get(i).getInsuAccNo() == null || this.get(i).getInsuAccNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getInsuAccNo());
            }
            if(this.get(i).getRateStartDate() == null || this.get(i).getRateStartDate().equals("null")) {
                pstmt.setDate(3,null);
            } else {
                pstmt.setDate(3, Date.valueOf(this.get(i).getRateStartDate()));
            }
            if(this.get(i).getRateEndDate() == null || this.get(i).getRateEndDate().equals("null")) {
                pstmt.setDate(4,null);
            } else {
                pstmt.setDate(4, Date.valueOf(this.get(i).getRateEndDate()));
            }
            if(this.get(i).getRateIntv() == null || this.get(i).getRateIntv().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getRateIntv());
            }
            pstmt.setDouble(6, this.get(i).getRate());
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(8,null);
            } else {
                pstmt.setDate(8, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getMakeTime());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LMAccGuratRateDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
