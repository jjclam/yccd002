/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMInsuAccValueDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LMInsuAccValueSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMInsuAccValueSchema implements Schema, Cloneable {
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 计价日期 */
    private Date ValueDate;
    /** 价格应公布日期 */
    private Date SRateDate;
    /** 价格实际公布日期 */
    private Date ARateDate;
    /** 买入单位价格 */
    private double RateIn;
    /** 卖出单位价格 */
    private double RateOut;
    /** 赎回比例 */
    private double RedeemRate;
    /** 赎回金额 */
    private double RedeemMoney;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 12;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMInsuAccValueSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "RiskCode";
        pk[1] = "InsuAccNo";
        pk[2] = "ValueDate";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMInsuAccValueSchema cloned = (LMInsuAccValueSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getInsuAccNo() {
        return InsuAccNo;
    }
    public void setInsuAccNo(String aInsuAccNo) {
        InsuAccNo = aInsuAccNo;
    }
    public String getValueDate() {
        if(ValueDate != null) {
            return fDate.getString(ValueDate);
        } else {
            return null;
        }
    }
    public void setValueDate(Date aValueDate) {
        ValueDate = aValueDate;
    }
    public void setValueDate(String aValueDate) {
        if (aValueDate != null && !aValueDate.equals("")) {
            ValueDate = fDate.getDate(aValueDate);
        } else
            ValueDate = null;
    }

    public String getSRateDate() {
        if(SRateDate != null) {
            return fDate.getString(SRateDate);
        } else {
            return null;
        }
    }
    public void setSRateDate(Date aSRateDate) {
        SRateDate = aSRateDate;
    }
    public void setSRateDate(String aSRateDate) {
        if (aSRateDate != null && !aSRateDate.equals("")) {
            SRateDate = fDate.getDate(aSRateDate);
        } else
            SRateDate = null;
    }

    public String getARateDate() {
        if(ARateDate != null) {
            return fDate.getString(ARateDate);
        } else {
            return null;
        }
    }
    public void setARateDate(Date aARateDate) {
        ARateDate = aARateDate;
    }
    public void setARateDate(String aARateDate) {
        if (aARateDate != null && !aARateDate.equals("")) {
            ARateDate = fDate.getDate(aARateDate);
        } else
            ARateDate = null;
    }

    public double getRateIn() {
        return RateIn;
    }
    public void setRateIn(double aRateIn) {
        RateIn = aRateIn;
    }
    public void setRateIn(String aRateIn) {
        if (aRateIn != null && !aRateIn.equals("")) {
            Double tDouble = new Double(aRateIn);
            double d = tDouble.doubleValue();
            RateIn = d;
        }
    }

    public double getRateOut() {
        return RateOut;
    }
    public void setRateOut(double aRateOut) {
        RateOut = aRateOut;
    }
    public void setRateOut(String aRateOut) {
        if (aRateOut != null && !aRateOut.equals("")) {
            Double tDouble = new Double(aRateOut);
            double d = tDouble.doubleValue();
            RateOut = d;
        }
    }

    public double getRedeemRate() {
        return RedeemRate;
    }
    public void setRedeemRate(double aRedeemRate) {
        RedeemRate = aRedeemRate;
    }
    public void setRedeemRate(String aRedeemRate) {
        if (aRedeemRate != null && !aRedeemRate.equals("")) {
            Double tDouble = new Double(aRedeemRate);
            double d = tDouble.doubleValue();
            RedeemRate = d;
        }
    }

    public double getRedeemMoney() {
        return RedeemMoney;
    }
    public void setRedeemMoney(double aRedeemMoney) {
        RedeemMoney = aRedeemMoney;
    }
    public void setRedeemMoney(String aRedeemMoney) {
        if (aRedeemMoney != null && !aRedeemMoney.equals("")) {
            Double tDouble = new Double(aRedeemMoney);
            double d = tDouble.doubleValue();
            RedeemMoney = d;
        }
    }

    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
    * 使用另外一个 LMInsuAccValueSchema 对象给 Schema 赋值
    * @param: aLMInsuAccValueSchema LMInsuAccValueSchema
    **/
    public void setSchema(LMInsuAccValueSchema aLMInsuAccValueSchema) {
        this.RiskCode = aLMInsuAccValueSchema.getRiskCode();
        this.InsuAccNo = aLMInsuAccValueSchema.getInsuAccNo();
        this.ValueDate = fDate.getDate( aLMInsuAccValueSchema.getValueDate());
        this.SRateDate = fDate.getDate( aLMInsuAccValueSchema.getSRateDate());
        this.ARateDate = fDate.getDate( aLMInsuAccValueSchema.getARateDate());
        this.RateIn = aLMInsuAccValueSchema.getRateIn();
        this.RateOut = aLMInsuAccValueSchema.getRateOut();
        this.RedeemRate = aLMInsuAccValueSchema.getRedeemRate();
        this.RedeemMoney = aLMInsuAccValueSchema.getRedeemMoney();
        this.Operator = aLMInsuAccValueSchema.getOperator();
        this.MakeDate = fDate.getDate( aLMInsuAccValueSchema.getMakeDate());
        this.MakeTime = aLMInsuAccValueSchema.getMakeTime();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("InsuAccNo") == null )
                this.InsuAccNo = null;
            else
                this.InsuAccNo = rs.getString("InsuAccNo").trim();

            this.ValueDate = rs.getDate("ValueDate");
            this.SRateDate = rs.getDate("SRateDate");
            this.ARateDate = rs.getDate("ARateDate");
            this.RateIn = rs.getDouble("RateIn");
            this.RateOut = rs.getDouble("RateOut");
            this.RedeemRate = rs.getDouble("RedeemRate");
            this.RedeemMoney = rs.getDouble("RedeemMoney");
            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMInsuAccValueSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMInsuAccValueSchema getSchema() {
        LMInsuAccValueSchema aLMInsuAccValueSchema = new LMInsuAccValueSchema();
        aLMInsuAccValueSchema.setSchema(this);
        return aLMInsuAccValueSchema;
    }

    public LMInsuAccValueDB getDB() {
        LMInsuAccValueDB aDBOper = new LMInsuAccValueDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMInsuAccValue描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ValueDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SRateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ARateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RateIn));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RateOut));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RedeemRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RedeemMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMInsuAccValue>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ValueDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER));
            SRateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER));
            ARateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER));
            RateIn = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6, SysConst.PACKAGESPILTER))).doubleValue();
            RateOut = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7, SysConst.PACKAGESPILTER))).doubleValue();
            RedeemRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8, SysConst.PACKAGESPILTER))).doubleValue();
            RedeemMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMInsuAccValueSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
        }
        if (FCode.equalsIgnoreCase("ValueDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValueDate()));
        }
        if (FCode.equalsIgnoreCase("SRateDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSRateDate()));
        }
        if (FCode.equalsIgnoreCase("ARateDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getARateDate()));
        }
        if (FCode.equalsIgnoreCase("RateIn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RateIn));
        }
        if (FCode.equalsIgnoreCase("RateOut")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RateOut));
        }
        if (FCode.equalsIgnoreCase("RedeemRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RedeemRate));
        }
        if (FCode.equalsIgnoreCase("RedeemMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RedeemMoney));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValueDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSRateDate()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getARateDate()));
                break;
            case 5:
                strFieldValue = String.valueOf(RateIn);
                break;
            case 6:
                strFieldValue = String.valueOf(RateOut);
                break;
            case 7:
                strFieldValue = String.valueOf(RedeemRate);
                break;
            case 8:
                strFieldValue = String.valueOf(RedeemMoney);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("InsuAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
                InsuAccNo = null;
        }
        if (FCode.equalsIgnoreCase("ValueDate")) {
            if(FValue != null && !FValue.equals("")) {
                ValueDate = fDate.getDate( FValue );
            }
            else
                ValueDate = null;
        }
        if (FCode.equalsIgnoreCase("SRateDate")) {
            if(FValue != null && !FValue.equals("")) {
                SRateDate = fDate.getDate( FValue );
            }
            else
                SRateDate = null;
        }
        if (FCode.equalsIgnoreCase("ARateDate")) {
            if(FValue != null && !FValue.equals("")) {
                ARateDate = fDate.getDate( FValue );
            }
            else
                ARateDate = null;
        }
        if (FCode.equalsIgnoreCase("RateIn")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RateIn = d;
            }
        }
        if (FCode.equalsIgnoreCase("RateOut")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RateOut = d;
            }
        }
        if (FCode.equalsIgnoreCase("RedeemRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RedeemRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("RedeemMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RedeemMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMInsuAccValueSchema other = (LMInsuAccValueSchema)otherObject;
        return
            RiskCode.equals(other.getRiskCode())
            && InsuAccNo.equals(other.getInsuAccNo())
            && fDate.getString(ValueDate).equals(other.getValueDate())
            && fDate.getString(SRateDate).equals(other.getSRateDate())
            && fDate.getString(ARateDate).equals(other.getARateDate())
            && RateIn == other.getRateIn()
            && RateOut == other.getRateOut()
            && RedeemRate == other.getRedeemRate()
            && RedeemMoney == other.getRedeemMoney()
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("RiskCode") ) {
            return 0;
        }
        if( strFieldName.equals("InsuAccNo") ) {
            return 1;
        }
        if( strFieldName.equals("ValueDate") ) {
            return 2;
        }
        if( strFieldName.equals("SRateDate") ) {
            return 3;
        }
        if( strFieldName.equals("ARateDate") ) {
            return 4;
        }
        if( strFieldName.equals("RateIn") ) {
            return 5;
        }
        if( strFieldName.equals("RateOut") ) {
            return 6;
        }
        if( strFieldName.equals("RedeemRate") ) {
            return 7;
        }
        if( strFieldName.equals("RedeemMoney") ) {
            return 8;
        }
        if( strFieldName.equals("Operator") ) {
            return 9;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 10;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "InsuAccNo";
                break;
            case 2:
                strFieldName = "ValueDate";
                break;
            case 3:
                strFieldName = "SRateDate";
                break;
            case 4:
                strFieldName = "ARateDate";
                break;
            case 5:
                strFieldName = "RateIn";
                break;
            case 6:
                strFieldName = "RateOut";
                break;
            case 7:
                strFieldName = "RedeemRate";
                break;
            case 8:
                strFieldName = "RedeemMoney";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "INSUACCNO":
                return Schema.TYPE_STRING;
            case "VALUEDATE":
                return Schema.TYPE_DATE;
            case "SRATEDATE":
                return Schema.TYPE_DATE;
            case "ARATEDATE":
                return Schema.TYPE_DATE;
            case "RATEIN":
                return Schema.TYPE_DOUBLE;
            case "RATEOUT":
                return Schema.TYPE_DOUBLE;
            case "REDEEMRATE":
                return Schema.TYPE_DOUBLE;
            case "REDEEMMONEY":
                return Schema.TYPE_DOUBLE;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_DATE;
            case 3:
                return Schema.TYPE_DATE;
            case 4:
                return Schema.TYPE_DATE;
            case 5:
                return Schema.TYPE_DOUBLE;
            case 6:
                return Schema.TYPE_DOUBLE;
            case 7:
                return Schema.TYPE_DOUBLE;
            case 8:
                return Schema.TYPE_DOUBLE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
