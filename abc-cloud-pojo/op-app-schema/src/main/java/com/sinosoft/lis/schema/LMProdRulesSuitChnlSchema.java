/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LMProdRulesSuitChnlDB;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>ClassName: LMProdRulesSuitChnlSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LMProdRulesSuitChnlSchema implements Schema, Cloneable {
    // @Field
    /** 险种销售渠道 */
    private String SaleChnl;
    /** 规则适用销售渠道 */
    private String RulesChnl;
    /** 规则适用销售渠道名称 */
    private String RulesChnlName;

    public static final int FIELDNUM = 3;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LMProdRulesSuitChnlSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SaleChnl";
        pk[1] = "RulesChnl";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LMProdRulesSuitChnlSchema cloned = (LMProdRulesSuitChnlSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getRulesChnl() {
        return RulesChnl;
    }
    public void setRulesChnl(String aRulesChnl) {
        RulesChnl = aRulesChnl;
    }
    public String getRulesChnlName() {
        return RulesChnlName;
    }
    public void setRulesChnlName(String aRulesChnlName) {
        RulesChnlName = aRulesChnlName;
    }

    /**
    * 使用另外一个 LMProdRulesSuitChnlSchema 对象给 Schema 赋值
    * @param: aLMProdRulesSuitChnlSchema LMProdRulesSuitChnlSchema
    **/
    public void setSchema(LMProdRulesSuitChnlSchema aLMProdRulesSuitChnlSchema) {
        this.SaleChnl = aLMProdRulesSuitChnlSchema.getSaleChnl();
        this.RulesChnl = aLMProdRulesSuitChnlSchema.getRulesChnl();
        this.RulesChnlName = aLMProdRulesSuitChnlSchema.getRulesChnlName();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("RulesChnl") == null )
                this.RulesChnl = null;
            else
                this.RulesChnl = rs.getString("RulesChnl").trim();

            if( rs.getString("RulesChnlName") == null )
                this.RulesChnlName = null;
            else
                this.RulesChnlName = rs.getString("RulesChnlName").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMProdRulesSuitChnlSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LMProdRulesSuitChnlSchema getSchema() {
        LMProdRulesSuitChnlSchema aLMProdRulesSuitChnlSchema = new LMProdRulesSuitChnlSchema();
        aLMProdRulesSuitChnlSchema.setSchema(this);
        return aLMProdRulesSuitChnlSchema;
    }

    public LMProdRulesSuitChnlDB getDB() {
        LMProdRulesSuitChnlDB aDBOper = new LMProdRulesSuitChnlDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMProdRulesSuitChnl描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RulesChnlName));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMProdRulesSuitChnl>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            RulesChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            RulesChnlName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMProdRulesSuitChnlSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("RulesChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesChnl));
        }
        if (FCode.equalsIgnoreCase("RulesChnlName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RulesChnlName));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RulesChnl);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RulesChnlName);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("RulesChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesChnl = FValue.trim();
            }
            else
                RulesChnl = null;
        }
        if (FCode.equalsIgnoreCase("RulesChnlName")) {
            if( FValue != null && !FValue.equals(""))
            {
                RulesChnlName = FValue.trim();
            }
            else
                RulesChnlName = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LMProdRulesSuitChnlSchema other = (LMProdRulesSuitChnlSchema)otherObject;
        return
            SaleChnl.equals(other.getSaleChnl())
            && RulesChnl.equals(other.getRulesChnl())
            && RulesChnlName.equals(other.getRulesChnlName());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SaleChnl") ) {
            return 0;
        }
        if( strFieldName.equals("RulesChnl") ) {
            return 1;
        }
        if( strFieldName.equals("RulesChnlName") ) {
            return 2;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SaleChnl";
                break;
            case 1:
                strFieldName = "RulesChnl";
                break;
            case 2:
                strFieldName = "RulesChnlName";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "RULESCHNL":
                return Schema.TYPE_STRING;
            case "RULESCHNLNAME":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
