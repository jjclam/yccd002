/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LAAgent1DB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LAAgent1Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LAAgent1Schema implements Schema, Cloneable {
    // @Field
    /** Managecom */
    private String ManageCom;
    /** Agentcom */
    private String AgentCom;
    /** Outagentcode */
    private String OutAgentCode;
    /** Agentname */
    private String AgentName;
    /** Idtype */
    private String IDType;
    /** Idno */
    private String IDNo;
    /** Qualtype */
    private String QualType;
    /** Qualno */
    private String QualNo;
    /** Qualstartdate */
    private Date QualStartDate;
    /** Qualenddate */
    private Date QualEndDate;
    /** Agentstate */
    private String AgentState;
    /** Branchtype */
    private String BranchType;
    /** Operator */
    private String Operator;
    /** Makedate */
    private Date MakeDate;
    /** Maketime */
    private String MakeTime;
    /** Modifydate */
    private Date ModifyDate;
    /** Modifytime */
    private String ModifyTime;
    /** State1 */
    private String State1;
    /** Phone */
    private String Phone;
    /** Mobile */
    private String Mobile;
    /** Serialno */
    private String SerialNo;

    public static final int FIELDNUM = 21;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LAAgent1Schema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAAgent1Schema cloned = (LAAgent1Schema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentCom() {
        return AgentCom;
    }
    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }
    public String getOutAgentCode() {
        return OutAgentCode;
    }
    public void setOutAgentCode(String aOutAgentCode) {
        OutAgentCode = aOutAgentCode;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getIDType() {
        return IDType;
    }
    public void setIDType(String aIDType) {
        IDType = aIDType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getQualType() {
        return QualType;
    }
    public void setQualType(String aQualType) {
        QualType = aQualType;
    }
    public String getQualNo() {
        return QualNo;
    }
    public void setQualNo(String aQualNo) {
        QualNo = aQualNo;
    }
    public String getQualStartDate() {
        if(QualStartDate != null) {
            return fDate.getString(QualStartDate);
        } else {
            return null;
        }
    }
    public void setQualStartDate(Date aQualStartDate) {
        QualStartDate = aQualStartDate;
    }
    public void setQualStartDate(String aQualStartDate) {
        if (aQualStartDate != null && !aQualStartDate.equals("")) {
            QualStartDate = fDate.getDate(aQualStartDate);
        } else
            QualStartDate = null;
    }

    public String getQualEndDate() {
        if(QualEndDate != null) {
            return fDate.getString(QualEndDate);
        } else {
            return null;
        }
    }
    public void setQualEndDate(Date aQualEndDate) {
        QualEndDate = aQualEndDate;
    }
    public void setQualEndDate(String aQualEndDate) {
        if (aQualEndDate != null && !aQualEndDate.equals("")) {
            QualEndDate = fDate.getDate(aQualEndDate);
        } else
            QualEndDate = null;
    }

    public String getAgentState() {
        return AgentState;
    }
    public void setAgentState(String aAgentState) {
        AgentState = aAgentState;
    }
    public String getBranchType() {
        return BranchType;
    }
    public void setBranchType(String aBranchType) {
        BranchType = aBranchType;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getState1() {
        return State1;
    }
    public void setState1(String aState1) {
        State1 = aState1;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    /**
    * 使用另外一个 LAAgent1Schema 对象给 Schema 赋值
    * @param: aLAAgent1Schema LAAgent1Schema
    **/
    public void setSchema(LAAgent1Schema aLAAgent1Schema) {
        this.ManageCom = aLAAgent1Schema.getManageCom();
        this.AgentCom = aLAAgent1Schema.getAgentCom();
        this.OutAgentCode = aLAAgent1Schema.getOutAgentCode();
        this.AgentName = aLAAgent1Schema.getAgentName();
        this.IDType = aLAAgent1Schema.getIDType();
        this.IDNo = aLAAgent1Schema.getIDNo();
        this.QualType = aLAAgent1Schema.getQualType();
        this.QualNo = aLAAgent1Schema.getQualNo();
        this.QualStartDate = fDate.getDate( aLAAgent1Schema.getQualStartDate());
        this.QualEndDate = fDate.getDate( aLAAgent1Schema.getQualEndDate());
        this.AgentState = aLAAgent1Schema.getAgentState();
        this.BranchType = aLAAgent1Schema.getBranchType();
        this.Operator = aLAAgent1Schema.getOperator();
        this.MakeDate = fDate.getDate( aLAAgent1Schema.getMakeDate());
        this.MakeTime = aLAAgent1Schema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLAAgent1Schema.getModifyDate());
        this.ModifyTime = aLAAgent1Schema.getModifyTime();
        this.State1 = aLAAgent1Schema.getState1();
        this.Phone = aLAAgent1Schema.getPhone();
        this.Mobile = aLAAgent1Schema.getMobile();
        this.SerialNo = aLAAgent1Schema.getSerialNo();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("AgentCom") == null )
                this.AgentCom = null;
            else
                this.AgentCom = rs.getString("AgentCom").trim();

            if( rs.getString("OutAgentCode") == null )
                this.OutAgentCode = null;
            else
                this.OutAgentCode = rs.getString("OutAgentCode").trim();

            if( rs.getString("AgentName") == null )
                this.AgentName = null;
            else
                this.AgentName = rs.getString("AgentName").trim();

            if( rs.getString("IDType") == null )
                this.IDType = null;
            else
                this.IDType = rs.getString("IDType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            if( rs.getString("QualType") == null )
                this.QualType = null;
            else
                this.QualType = rs.getString("QualType").trim();

            if( rs.getString("QualNo") == null )
                this.QualNo = null;
            else
                this.QualNo = rs.getString("QualNo").trim();

            this.QualStartDate = rs.getDate("QualStartDate");
            this.QualEndDate = rs.getDate("QualEndDate");
            if( rs.getString("AgentState") == null )
                this.AgentState = null;
            else
                this.AgentState = rs.getString("AgentState").trim();

            if( rs.getString("BranchType") == null )
                this.BranchType = null;
            else
                this.BranchType = rs.getString("BranchType").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("State1") == null )
                this.State1 = null;
            else
                this.State1 = rs.getString("State1").trim();

            if( rs.getString("Phone") == null )
                this.Phone = null;
            else
                this.Phone = rs.getString("Phone").trim();

            if( rs.getString("Mobile") == null )
                this.Mobile = null;
            else
                this.Mobile = rs.getString("Mobile").trim();

            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgent1Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LAAgent1Schema getSchema() {
        LAAgent1Schema aLAAgent1Schema = new LAAgent1Schema();
        aLAAgent1Schema.setSchema(this);
        return aLAAgent1Schema;
    }

    public LAAgent1DB getDB() {
        LAAgent1DB aDBOper = new LAAgent1DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgent1描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OutAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QualType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(QualNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( QualStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( QualEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SerialNo));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgent1>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            OutAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            QualType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            QualNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            QualStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER));
            QualEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER));
            AgentState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            State1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgent1Schema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equalsIgnoreCase("OutAgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutAgentCode));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("QualType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualType));
        }
        if (FCode.equalsIgnoreCase("QualNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QualNo));
        }
        if (FCode.equalsIgnoreCase("QualStartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getQualStartDate()));
        }
        if (FCode.equalsIgnoreCase("QualEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getQualEndDate()));
        }
        if (FCode.equalsIgnoreCase("AgentState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentState));
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("State1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State1));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OutAgentCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AgentName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(IDType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(QualType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(QualNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getQualStartDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getQualEndDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AgentState);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(State1);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Mobile);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
                AgentCom = null;
        }
        if (FCode.equalsIgnoreCase("OutAgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OutAgentCode = FValue.trim();
            }
            else
                OutAgentCode = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("IDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDType = FValue.trim();
            }
            else
                IDType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("QualType")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualType = FValue.trim();
            }
            else
                QualType = null;
        }
        if (FCode.equalsIgnoreCase("QualNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                QualNo = FValue.trim();
            }
            else
                QualNo = null;
        }
        if (FCode.equalsIgnoreCase("QualStartDate")) {
            if(FValue != null && !FValue.equals("")) {
                QualStartDate = fDate.getDate( FValue );
            }
            else
                QualStartDate = null;
        }
        if (FCode.equalsIgnoreCase("QualEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                QualEndDate = fDate.getDate( FValue );
            }
            else
                QualEndDate = null;
        }
        if (FCode.equalsIgnoreCase("AgentState")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentState = FValue.trim();
            }
            else
                AgentState = null;
        }
        if (FCode.equalsIgnoreCase("BranchType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
                BranchType = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("State1")) {
            if( FValue != null && !FValue.equals(""))
            {
                State1 = FValue.trim();
            }
            else
                State1 = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LAAgent1Schema other = (LAAgent1Schema)otherObject;
        return
            ManageCom.equals(other.getManageCom())
            && AgentCom.equals(other.getAgentCom())
            && OutAgentCode.equals(other.getOutAgentCode())
            && AgentName.equals(other.getAgentName())
            && IDType.equals(other.getIDType())
            && IDNo.equals(other.getIDNo())
            && QualType.equals(other.getQualType())
            && QualNo.equals(other.getQualNo())
            && fDate.getString(QualStartDate).equals(other.getQualStartDate())
            && fDate.getString(QualEndDate).equals(other.getQualEndDate())
            && AgentState.equals(other.getAgentState())
            && BranchType.equals(other.getBranchType())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && State1.equals(other.getState1())
            && Phone.equals(other.getPhone())
            && Mobile.equals(other.getMobile())
            && SerialNo.equals(other.getSerialNo());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("ManageCom") ) {
            return 0;
        }
        if( strFieldName.equals("AgentCom") ) {
            return 1;
        }
        if( strFieldName.equals("OutAgentCode") ) {
            return 2;
        }
        if( strFieldName.equals("AgentName") ) {
            return 3;
        }
        if( strFieldName.equals("IDType") ) {
            return 4;
        }
        if( strFieldName.equals("IDNo") ) {
            return 5;
        }
        if( strFieldName.equals("QualType") ) {
            return 6;
        }
        if( strFieldName.equals("QualNo") ) {
            return 7;
        }
        if( strFieldName.equals("QualStartDate") ) {
            return 8;
        }
        if( strFieldName.equals("QualEndDate") ) {
            return 9;
        }
        if( strFieldName.equals("AgentState") ) {
            return 10;
        }
        if( strFieldName.equals("BranchType") ) {
            return 11;
        }
        if( strFieldName.equals("Operator") ) {
            return 12;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 13;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 14;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 15;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 16;
        }
        if( strFieldName.equals("State1") ) {
            return 17;
        }
        if( strFieldName.equals("Phone") ) {
            return 18;
        }
        if( strFieldName.equals("Mobile") ) {
            return 19;
        }
        if( strFieldName.equals("SerialNo") ) {
            return 20;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "AgentCom";
                break;
            case 2:
                strFieldName = "OutAgentCode";
                break;
            case 3:
                strFieldName = "AgentName";
                break;
            case 4:
                strFieldName = "IDType";
                break;
            case 5:
                strFieldName = "IDNo";
                break;
            case 6:
                strFieldName = "QualType";
                break;
            case 7:
                strFieldName = "QualNo";
                break;
            case 8:
                strFieldName = "QualStartDate";
                break;
            case 9:
                strFieldName = "QualEndDate";
                break;
            case 10:
                strFieldName = "AgentState";
                break;
            case 11:
                strFieldName = "BranchType";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            case 17:
                strFieldName = "State1";
                break;
            case 18:
                strFieldName = "Phone";
                break;
            case 19:
                strFieldName = "Mobile";
                break;
            case 20:
                strFieldName = "SerialNo";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTCOM":
                return Schema.TYPE_STRING;
            case "OUTAGENTCODE":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "IDTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "QUALTYPE":
                return Schema.TYPE_STRING;
            case "QUALNO":
                return Schema.TYPE_STRING;
            case "QUALSTARTDATE":
                return Schema.TYPE_DATE;
            case "QUALENDDATE":
                return Schema.TYPE_DATE;
            case "AGENTSTATE":
                return Schema.TYPE_STRING;
            case "BRANCHTYPE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STATE1":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "SERIALNO":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_DATE;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_DATE;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
