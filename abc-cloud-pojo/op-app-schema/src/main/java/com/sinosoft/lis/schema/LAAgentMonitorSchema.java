/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LAAgentMonitorDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LAAgentMonitorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-10-21
 */
public class LAAgentMonitorSchema implements Schema, Cloneable {
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 监控类型 */
    private String MonitorType;
    /** 监控号 */
    private int MonitorNo;
    /** 管理机构 */
    private String ManageCom;
    /** 代理人展业机构代码 */
    private String AgentGroup;
    /** 代理人姓名 */
    private String AgentName;
    /** 代理人性别 */
    private String AgentSex;
    /** 出生日期 */
    private Date Birthday;
    /** 证件类型 */
    private String IDNoType;
    /** 证件号码 */
    private String IDNo;
    /** 展业机构外部编码 */
    private String BranchAttr;
    /** 展业机构管理人员 */
    private String BranchManager;
    /** 展业机构名称 */
    private String BranchGroupName;
    /** 监控状态 */
    private String MonitorState;
    /** 监控日期 */
    private Date MonitorDate;
    /** 解除日期 */
    private Date CancelDate;
    /** 上报部门 */
    private String ReportCom;
    /** 上报原因 */
    private String ReportReason;
    /** 解除部门 */
    private String CancelCom;
    /** 解除原因 */
    private String CancelReason;
    /** 操作员 */
    private String Operator;
    /** 操作日期 */
    private Date MakeDate;
    /** 操作时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 备用1 */
    private String StandByFlag1;
    /** 备用2 */
    private String StandByFlag2;
    /** 备用3 */
    private String StandByFlag3;
    /** 备用4 */
    private String StandByFlag4;
    /** 备用5 */
    private String StandByFlag5;
    /** 备用6 */
    private String StandByFlag6;
    /** 备用7 */
    private String StandByFlag7;
    /** 备用8 */
    private String StandByFlag8;
    /** 备用9 */
    private String StandByFlag9;
    /** 备用10 */
    private String StandByFlag10;
    /** 备用11 */
    private String StandByFlag11;
    /** 备用12 */
    private String StandByFlag12;
    /** 备用13 */
    private String StandByFlag13;
    /** 备用14 */
    private String StandByFlag14;
    /** 备用15 */
    private String StandByFlag15;

    public static final int FIELDNUM = 40;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LAAgentMonitorSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "AgentCode";
        pk[1] = "MonitorType";
        pk[2] = "MonitorNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LAAgentMonitorSchema cloned = (LAAgentMonitorSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getAgentCode() {
        return AgentCode;
    }
    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }
    public String getMonitorType() {
        return MonitorType;
    }
    public void setMonitorType(String aMonitorType) {
        MonitorType = aMonitorType;
    }
    public int getMonitorNo() {
        return MonitorNo;
    }
    public void setMonitorNo(int aMonitorNo) {
        MonitorNo = aMonitorNo;
    }
    public void setMonitorNo(String aMonitorNo) {
        if (aMonitorNo != null && !aMonitorNo.equals("")) {
            Integer tInteger = new Integer(aMonitorNo);
            int i = tInteger.intValue();
            MonitorNo = i;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getAgentGroup() {
        return AgentGroup;
    }
    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }
    public String getAgentName() {
        return AgentName;
    }
    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }
    public String getAgentSex() {
        return AgentSex;
    }
    public void setAgentSex(String aAgentSex) {
        AgentSex = aAgentSex;
    }
    public String getBirthday() {
        if(Birthday != null) {
            return fDate.getString(Birthday);
        } else {
            return null;
        }
    }
    public void setBirthday(Date aBirthday) {
        Birthday = aBirthday;
    }
    public void setBirthday(String aBirthday) {
        if (aBirthday != null && !aBirthday.equals("")) {
            Birthday = fDate.getDate(aBirthday);
        } else
            Birthday = null;
    }

    public String getIDNoType() {
        return IDNoType;
    }
    public void setIDNoType(String aIDNoType) {
        IDNoType = aIDNoType;
    }
    public String getIDNo() {
        return IDNo;
    }
    public void setIDNo(String aIDNo) {
        IDNo = aIDNo;
    }
    public String getBranchAttr() {
        return BranchAttr;
    }
    public void setBranchAttr(String aBranchAttr) {
        BranchAttr = aBranchAttr;
    }
    public String getBranchManager() {
        return BranchManager;
    }
    public void setBranchManager(String aBranchManager) {
        BranchManager = aBranchManager;
    }
    public String getBranchGroupName() {
        return BranchGroupName;
    }
    public void setBranchGroupName(String aBranchGroupName) {
        BranchGroupName = aBranchGroupName;
    }
    public String getMonitorState() {
        return MonitorState;
    }
    public void setMonitorState(String aMonitorState) {
        MonitorState = aMonitorState;
    }
    public String getMonitorDate() {
        if(MonitorDate != null) {
            return fDate.getString(MonitorDate);
        } else {
            return null;
        }
    }
    public void setMonitorDate(Date aMonitorDate) {
        MonitorDate = aMonitorDate;
    }
    public void setMonitorDate(String aMonitorDate) {
        if (aMonitorDate != null && !aMonitorDate.equals("")) {
            MonitorDate = fDate.getDate(aMonitorDate);
        } else
            MonitorDate = null;
    }

    public String getCancelDate() {
        if(CancelDate != null) {
            return fDate.getString(CancelDate);
        } else {
            return null;
        }
    }
    public void setCancelDate(Date aCancelDate) {
        CancelDate = aCancelDate;
    }
    public void setCancelDate(String aCancelDate) {
        if (aCancelDate != null && !aCancelDate.equals("")) {
            CancelDate = fDate.getDate(aCancelDate);
        } else
            CancelDate = null;
    }

    public String getReportCom() {
        return ReportCom;
    }
    public void setReportCom(String aReportCom) {
        ReportCom = aReportCom;
    }
    public String getReportReason() {
        return ReportReason;
    }
    public void setReportReason(String aReportReason) {
        ReportReason = aReportReason;
    }
    public String getCancelCom() {
        return CancelCom;
    }
    public void setCancelCom(String aCancelCom) {
        CancelCom = aCancelCom;
    }
    public String getCancelReason() {
        return CancelReason;
    }
    public void setCancelReason(String aCancelReason) {
        CancelReason = aCancelReason;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStandByFlag1() {
        return StandByFlag1;
    }
    public void setStandByFlag1(String aStandByFlag1) {
        StandByFlag1 = aStandByFlag1;
    }
    public String getStandByFlag2() {
        return StandByFlag2;
    }
    public void setStandByFlag2(String aStandByFlag2) {
        StandByFlag2 = aStandByFlag2;
    }
    public String getStandByFlag3() {
        return StandByFlag3;
    }
    public void setStandByFlag3(String aStandByFlag3) {
        StandByFlag3 = aStandByFlag3;
    }
    public String getStandByFlag4() {
        return StandByFlag4;
    }
    public void setStandByFlag4(String aStandByFlag4) {
        StandByFlag4 = aStandByFlag4;
    }
    public String getStandByFlag5() {
        return StandByFlag5;
    }
    public void setStandByFlag5(String aStandByFlag5) {
        StandByFlag5 = aStandByFlag5;
    }
    public String getStandByFlag6() {
        return StandByFlag6;
    }
    public void setStandByFlag6(String aStandByFlag6) {
        StandByFlag6 = aStandByFlag6;
    }
    public String getStandByFlag7() {
        return StandByFlag7;
    }
    public void setStandByFlag7(String aStandByFlag7) {
        StandByFlag7 = aStandByFlag7;
    }
    public String getStandByFlag8() {
        return StandByFlag8;
    }
    public void setStandByFlag8(String aStandByFlag8) {
        StandByFlag8 = aStandByFlag8;
    }
    public String getStandByFlag9() {
        return StandByFlag9;
    }
    public void setStandByFlag9(String aStandByFlag9) {
        StandByFlag9 = aStandByFlag9;
    }
    public String getStandByFlag10() {
        return StandByFlag10;
    }
    public void setStandByFlag10(String aStandByFlag10) {
        StandByFlag10 = aStandByFlag10;
    }
    public String getStandByFlag11() {
        return StandByFlag11;
    }
    public void setStandByFlag11(String aStandByFlag11) {
        StandByFlag11 = aStandByFlag11;
    }
    public String getStandByFlag12() {
        return StandByFlag12;
    }
    public void setStandByFlag12(String aStandByFlag12) {
        StandByFlag12 = aStandByFlag12;
    }
    public String getStandByFlag13() {
        return StandByFlag13;
    }
    public void setStandByFlag13(String aStandByFlag13) {
        StandByFlag13 = aStandByFlag13;
    }
    public String getStandByFlag14() {
        return StandByFlag14;
    }
    public void setStandByFlag14(String aStandByFlag14) {
        StandByFlag14 = aStandByFlag14;
    }
    public String getStandByFlag15() {
        return StandByFlag15;
    }
    public void setStandByFlag15(String aStandByFlag15) {
        StandByFlag15 = aStandByFlag15;
    }

    /**
    * 使用另外一个 LAAgentMonitorSchema 对象给 Schema 赋值
    * @param: aLAAgentMonitorSchema LAAgentMonitorSchema
    **/
    public void setSchema(LAAgentMonitorSchema aLAAgentMonitorSchema) {
        this.AgentCode = aLAAgentMonitorSchema.getAgentCode();
        this.MonitorType = aLAAgentMonitorSchema.getMonitorType();
        this.MonitorNo = aLAAgentMonitorSchema.getMonitorNo();
        this.ManageCom = aLAAgentMonitorSchema.getManageCom();
        this.AgentGroup = aLAAgentMonitorSchema.getAgentGroup();
        this.AgentName = aLAAgentMonitorSchema.getAgentName();
        this.AgentSex = aLAAgentMonitorSchema.getAgentSex();
        this.Birthday = fDate.getDate( aLAAgentMonitorSchema.getBirthday());
        this.IDNoType = aLAAgentMonitorSchema.getIDNoType();
        this.IDNo = aLAAgentMonitorSchema.getIDNo();
        this.BranchAttr = aLAAgentMonitorSchema.getBranchAttr();
        this.BranchManager = aLAAgentMonitorSchema.getBranchManager();
        this.BranchGroupName = aLAAgentMonitorSchema.getBranchGroupName();
        this.MonitorState = aLAAgentMonitorSchema.getMonitorState();
        this.MonitorDate = fDate.getDate( aLAAgentMonitorSchema.getMonitorDate());
        this.CancelDate = fDate.getDate( aLAAgentMonitorSchema.getCancelDate());
        this.ReportCom = aLAAgentMonitorSchema.getReportCom();
        this.ReportReason = aLAAgentMonitorSchema.getReportReason();
        this.CancelCom = aLAAgentMonitorSchema.getCancelCom();
        this.CancelReason = aLAAgentMonitorSchema.getCancelReason();
        this.Operator = aLAAgentMonitorSchema.getOperator();
        this.MakeDate = fDate.getDate( aLAAgentMonitorSchema.getMakeDate());
        this.MakeTime = aLAAgentMonitorSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLAAgentMonitorSchema.getModifyDate());
        this.ModifyTime = aLAAgentMonitorSchema.getModifyTime();
        this.StandByFlag1 = aLAAgentMonitorSchema.getStandByFlag1();
        this.StandByFlag2 = aLAAgentMonitorSchema.getStandByFlag2();
        this.StandByFlag3 = aLAAgentMonitorSchema.getStandByFlag3();
        this.StandByFlag4 = aLAAgentMonitorSchema.getStandByFlag4();
        this.StandByFlag5 = aLAAgentMonitorSchema.getStandByFlag5();
        this.StandByFlag6 = aLAAgentMonitorSchema.getStandByFlag6();
        this.StandByFlag7 = aLAAgentMonitorSchema.getStandByFlag7();
        this.StandByFlag8 = aLAAgentMonitorSchema.getStandByFlag8();
        this.StandByFlag9 = aLAAgentMonitorSchema.getStandByFlag9();
        this.StandByFlag10 = aLAAgentMonitorSchema.getStandByFlag10();
        this.StandByFlag11 = aLAAgentMonitorSchema.getStandByFlag11();
        this.StandByFlag12 = aLAAgentMonitorSchema.getStandByFlag12();
        this.StandByFlag13 = aLAAgentMonitorSchema.getStandByFlag13();
        this.StandByFlag14 = aLAAgentMonitorSchema.getStandByFlag14();
        this.StandByFlag15 = aLAAgentMonitorSchema.getStandByFlag15();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("AgentCode") == null )
                this.AgentCode = null;
            else
                this.AgentCode = rs.getString("AgentCode").trim();

            if( rs.getString("MonitorType") == null )
                this.MonitorType = null;
            else
                this.MonitorType = rs.getString("MonitorType").trim();

            this.MonitorNo = rs.getInt("MonitorNo");
            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("AgentGroup") == null )
                this.AgentGroup = null;
            else
                this.AgentGroup = rs.getString("AgentGroup").trim();

            if( rs.getString("AgentName") == null )
                this.AgentName = null;
            else
                this.AgentName = rs.getString("AgentName").trim();

            if( rs.getString("AgentSex") == null )
                this.AgentSex = null;
            else
                this.AgentSex = rs.getString("AgentSex").trim();

            this.Birthday = rs.getDate("Birthday");
            if( rs.getString("IDNoType") == null )
                this.IDNoType = null;
            else
                this.IDNoType = rs.getString("IDNoType").trim();

            if( rs.getString("IDNo") == null )
                this.IDNo = null;
            else
                this.IDNo = rs.getString("IDNo").trim();

            if( rs.getString("BranchAttr") == null )
                this.BranchAttr = null;
            else
                this.BranchAttr = rs.getString("BranchAttr").trim();

            if( rs.getString("BranchManager") == null )
                this.BranchManager = null;
            else
                this.BranchManager = rs.getString("BranchManager").trim();

            if( rs.getString("BranchGroupName") == null )
                this.BranchGroupName = null;
            else
                this.BranchGroupName = rs.getString("BranchGroupName").trim();

            if( rs.getString("MonitorState") == null )
                this.MonitorState = null;
            else
                this.MonitorState = rs.getString("MonitorState").trim();

            this.MonitorDate = rs.getDate("MonitorDate");
            this.CancelDate = rs.getDate("CancelDate");
            if( rs.getString("ReportCom") == null )
                this.ReportCom = null;
            else
                this.ReportCom = rs.getString("ReportCom").trim();

            if( rs.getString("ReportReason") == null )
                this.ReportReason = null;
            else
                this.ReportReason = rs.getString("ReportReason").trim();

            if( rs.getString("CancelCom") == null )
                this.CancelCom = null;
            else
                this.CancelCom = rs.getString("CancelCom").trim();

            if( rs.getString("CancelReason") == null )
                this.CancelReason = null;
            else
                this.CancelReason = rs.getString("CancelReason").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("StandByFlag1") == null )
                this.StandByFlag1 = null;
            else
                this.StandByFlag1 = rs.getString("StandByFlag1").trim();

            if( rs.getString("StandByFlag2") == null )
                this.StandByFlag2 = null;
            else
                this.StandByFlag2 = rs.getString("StandByFlag2").trim();

            if( rs.getString("StandByFlag3") == null )
                this.StandByFlag3 = null;
            else
                this.StandByFlag3 = rs.getString("StandByFlag3").trim();

            if( rs.getString("StandByFlag4") == null )
                this.StandByFlag4 = null;
            else
                this.StandByFlag4 = rs.getString("StandByFlag4").trim();

            if( rs.getString("StandByFlag5") == null )
                this.StandByFlag5 = null;
            else
                this.StandByFlag5 = rs.getString("StandByFlag5").trim();

            if( rs.getString("StandByFlag6") == null )
                this.StandByFlag6 = null;
            else
                this.StandByFlag6 = rs.getString("StandByFlag6").trim();

            if( rs.getString("StandByFlag7") == null )
                this.StandByFlag7 = null;
            else
                this.StandByFlag7 = rs.getString("StandByFlag7").trim();

            if( rs.getString("StandByFlag8") == null )
                this.StandByFlag8 = null;
            else
                this.StandByFlag8 = rs.getString("StandByFlag8").trim();

            if( rs.getString("StandByFlag9") == null )
                this.StandByFlag9 = null;
            else
                this.StandByFlag9 = rs.getString("StandByFlag9").trim();

            if( rs.getString("StandByFlag10") == null )
                this.StandByFlag10 = null;
            else
                this.StandByFlag10 = rs.getString("StandByFlag10").trim();

            if( rs.getString("StandByFlag11") == null )
                this.StandByFlag11 = null;
            else
                this.StandByFlag11 = rs.getString("StandByFlag11").trim();

            if( rs.getString("StandByFlag12") == null )
                this.StandByFlag12 = null;
            else
                this.StandByFlag12 = rs.getString("StandByFlag12").trim();

            if( rs.getString("StandByFlag13") == null )
                this.StandByFlag13 = null;
            else
                this.StandByFlag13 = rs.getString("StandByFlag13").trim();

            if( rs.getString("StandByFlag14") == null )
                this.StandByFlag14 = null;
            else
                this.StandByFlag14 = rs.getString("StandByFlag14").trim();

            if( rs.getString("StandByFlag15") == null )
                this.StandByFlag15 = null;
            else
                this.StandByFlag15 = rs.getString("StandByFlag15").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentMonitorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LAAgentMonitorSchema getSchema() {
        LAAgentMonitorSchema aLAAgentMonitorSchema = new LAAgentMonitorSchema();
        aLAAgentMonitorSchema.setSchema(this);
        return aLAAgentMonitorSchema;
    }

    public LAAgentMonitorDB getDB() {
        LAAgentMonitorDB aDBOper = new LAAgentMonitorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentMonitor描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MonitorType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MonitorNo));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentSex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchAttr)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchManager)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BranchGroupName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MonitorState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MonitorDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CancelDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReportCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReportReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CancelCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CancelReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag4)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag5)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag6)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag7)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag8)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag9)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag10)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag11)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag12)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag13)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag14)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandByFlag15));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentMonitor>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            MonitorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            MonitorNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3, SysConst.PACKAGESPILTER))).intValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            AgentSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER));
            IDNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            BranchManager = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            BranchGroupName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            MonitorState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            MonitorDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER));
            CancelDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER));
            ReportCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            ReportReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            CancelCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            CancelReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            StandByFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            StandByFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            StandByFlag4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            StandByFlag5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            StandByFlag6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            StandByFlag7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            StandByFlag8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            StandByFlag9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            StandByFlag10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            StandByFlag11 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            StandByFlag12 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            StandByFlag13 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            StandByFlag14 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            StandByFlag15 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentMonitorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equalsIgnoreCase("MonitorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonitorType));
        }
        if (FCode.equalsIgnoreCase("MonitorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonitorNo));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equalsIgnoreCase("AgentSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSex));
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
        }
        if (FCode.equalsIgnoreCase("IDNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoType));
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equalsIgnoreCase("BranchManager")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchManager));
        }
        if (FCode.equalsIgnoreCase("BranchGroupName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchGroupName));
        }
        if (FCode.equalsIgnoreCase("MonitorState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonitorState));
        }
        if (FCode.equalsIgnoreCase("MonitorDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMonitorDate()));
        }
        if (FCode.equalsIgnoreCase("CancelDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCancelDate()));
        }
        if (FCode.equalsIgnoreCase("ReportCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportCom));
        }
        if (FCode.equalsIgnoreCase("ReportReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReportReason));
        }
        if (FCode.equalsIgnoreCase("CancelCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CancelCom));
        }
        if (FCode.equalsIgnoreCase("CancelReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CancelReason));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag3));
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag4));
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag5));
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag6));
        }
        if (FCode.equalsIgnoreCase("StandByFlag7")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag7));
        }
        if (FCode.equalsIgnoreCase("StandByFlag8")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag8));
        }
        if (FCode.equalsIgnoreCase("StandByFlag9")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag9));
        }
        if (FCode.equalsIgnoreCase("StandByFlag10")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag10));
        }
        if (FCode.equalsIgnoreCase("StandByFlag11")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag11));
        }
        if (FCode.equalsIgnoreCase("StandByFlag12")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag12));
        }
        if (FCode.equalsIgnoreCase("StandByFlag13")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag13));
        }
        if (FCode.equalsIgnoreCase("StandByFlag14")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag14));
        }
        if (FCode.equalsIgnoreCase("StandByFlag15")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag15));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(MonitorType);
                break;
            case 2:
                strFieldValue = String.valueOf(MonitorNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AgentName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AgentSex);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(IDNoType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(BranchAttr);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(BranchManager);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(BranchGroupName);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(MonitorState);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMonitorDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCancelDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ReportCom);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ReportReason);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(CancelCom);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(CancelReason);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag2);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag3);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag4);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag5);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag6);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag7);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag8);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag9);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag10);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag11);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag12);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag13);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag14);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(StandByFlag15);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("AgentCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
                AgentCode = null;
        }
        if (FCode.equalsIgnoreCase("MonitorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                MonitorType = FValue.trim();
            }
            else
                MonitorType = null;
        }
        if (FCode.equalsIgnoreCase("MonitorNo")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MonitorNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
                AgentGroup = null;
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentName = FValue.trim();
            }
            else
                AgentName = null;
        }
        if (FCode.equalsIgnoreCase("AgentSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                AgentSex = FValue.trim();
            }
            else
                AgentSex = null;
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if(FValue != null && !FValue.equals("")) {
                Birthday = fDate.getDate( FValue );
            }
            else
                Birthday = null;
        }
        if (FCode.equalsIgnoreCase("IDNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNoType = FValue.trim();
            }
            else
                IDNoType = null;
        }
        if (FCode.equalsIgnoreCase("IDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
                IDNo = null;
        }
        if (FCode.equalsIgnoreCase("BranchAttr")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
                BranchAttr = null;
        }
        if (FCode.equalsIgnoreCase("BranchManager")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchManager = FValue.trim();
            }
            else
                BranchManager = null;
        }
        if (FCode.equalsIgnoreCase("BranchGroupName")) {
            if( FValue != null && !FValue.equals(""))
            {
                BranchGroupName = FValue.trim();
            }
            else
                BranchGroupName = null;
        }
        if (FCode.equalsIgnoreCase("MonitorState")) {
            if( FValue != null && !FValue.equals(""))
            {
                MonitorState = FValue.trim();
            }
            else
                MonitorState = null;
        }
        if (FCode.equalsIgnoreCase("MonitorDate")) {
            if(FValue != null && !FValue.equals("")) {
                MonitorDate = fDate.getDate( FValue );
            }
            else
                MonitorDate = null;
        }
        if (FCode.equalsIgnoreCase("CancelDate")) {
            if(FValue != null && !FValue.equals("")) {
                CancelDate = fDate.getDate( FValue );
            }
            else
                CancelDate = null;
        }
        if (FCode.equalsIgnoreCase("ReportCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportCom = FValue.trim();
            }
            else
                ReportCom = null;
        }
        if (FCode.equalsIgnoreCase("ReportReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReportReason = FValue.trim();
            }
            else
                ReportReason = null;
        }
        if (FCode.equalsIgnoreCase("CancelCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                CancelCom = FValue.trim();
            }
            else
                CancelCom = null;
        }
        if (FCode.equalsIgnoreCase("CancelReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                CancelReason = FValue.trim();
            }
            else
                CancelReason = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag1 = FValue.trim();
            }
            else
                StandByFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag2 = FValue.trim();
            }
            else
                StandByFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag3 = FValue.trim();
            }
            else
                StandByFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag4")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag4 = FValue.trim();
            }
            else
                StandByFlag4 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag5")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag5 = FValue.trim();
            }
            else
                StandByFlag5 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag6")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag6 = FValue.trim();
            }
            else
                StandByFlag6 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag7")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag7 = FValue.trim();
            }
            else
                StandByFlag7 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag8")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag8 = FValue.trim();
            }
            else
                StandByFlag8 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag9")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag9 = FValue.trim();
            }
            else
                StandByFlag9 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag10")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag10 = FValue.trim();
            }
            else
                StandByFlag10 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag11")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag11 = FValue.trim();
            }
            else
                StandByFlag11 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag12")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag12 = FValue.trim();
            }
            else
                StandByFlag12 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag13")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag13 = FValue.trim();
            }
            else
                StandByFlag13 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag14")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag14 = FValue.trim();
            }
            else
                StandByFlag14 = null;
        }
        if (FCode.equalsIgnoreCase("StandByFlag15")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandByFlag15 = FValue.trim();
            }
            else
                StandByFlag15 = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LAAgentMonitorSchema other = (LAAgentMonitorSchema)otherObject;
        return
            AgentCode.equals(other.getAgentCode())
            && MonitorType.equals(other.getMonitorType())
            && MonitorNo == other.getMonitorNo()
            && ManageCom.equals(other.getManageCom())
            && AgentGroup.equals(other.getAgentGroup())
            && AgentName.equals(other.getAgentName())
            && AgentSex.equals(other.getAgentSex())
            && fDate.getString(Birthday).equals(other.getBirthday())
            && IDNoType.equals(other.getIDNoType())
            && IDNo.equals(other.getIDNo())
            && BranchAttr.equals(other.getBranchAttr())
            && BranchManager.equals(other.getBranchManager())
            && BranchGroupName.equals(other.getBranchGroupName())
            && MonitorState.equals(other.getMonitorState())
            && fDate.getString(MonitorDate).equals(other.getMonitorDate())
            && fDate.getString(CancelDate).equals(other.getCancelDate())
            && ReportCom.equals(other.getReportCom())
            && ReportReason.equals(other.getReportReason())
            && CancelCom.equals(other.getCancelCom())
            && CancelReason.equals(other.getCancelReason())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && StandByFlag1.equals(other.getStandByFlag1())
            && StandByFlag2.equals(other.getStandByFlag2())
            && StandByFlag3.equals(other.getStandByFlag3())
            && StandByFlag4.equals(other.getStandByFlag4())
            && StandByFlag5.equals(other.getStandByFlag5())
            && StandByFlag6.equals(other.getStandByFlag6())
            && StandByFlag7.equals(other.getStandByFlag7())
            && StandByFlag8.equals(other.getStandByFlag8())
            && StandByFlag9.equals(other.getStandByFlag9())
            && StandByFlag10.equals(other.getStandByFlag10())
            && StandByFlag11.equals(other.getStandByFlag11())
            && StandByFlag12.equals(other.getStandByFlag12())
            && StandByFlag13.equals(other.getStandByFlag13())
            && StandByFlag14.equals(other.getStandByFlag14())
            && StandByFlag15.equals(other.getStandByFlag15());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("AgentCode") ) {
            return 0;
        }
        if( strFieldName.equals("MonitorType") ) {
            return 1;
        }
        if( strFieldName.equals("MonitorNo") ) {
            return 2;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 3;
        }
        if( strFieldName.equals("AgentGroup") ) {
            return 4;
        }
        if( strFieldName.equals("AgentName") ) {
            return 5;
        }
        if( strFieldName.equals("AgentSex") ) {
            return 6;
        }
        if( strFieldName.equals("Birthday") ) {
            return 7;
        }
        if( strFieldName.equals("IDNoType") ) {
            return 8;
        }
        if( strFieldName.equals("IDNo") ) {
            return 9;
        }
        if( strFieldName.equals("BranchAttr") ) {
            return 10;
        }
        if( strFieldName.equals("BranchManager") ) {
            return 11;
        }
        if( strFieldName.equals("BranchGroupName") ) {
            return 12;
        }
        if( strFieldName.equals("MonitorState") ) {
            return 13;
        }
        if( strFieldName.equals("MonitorDate") ) {
            return 14;
        }
        if( strFieldName.equals("CancelDate") ) {
            return 15;
        }
        if( strFieldName.equals("ReportCom") ) {
            return 16;
        }
        if( strFieldName.equals("ReportReason") ) {
            return 17;
        }
        if( strFieldName.equals("CancelCom") ) {
            return 18;
        }
        if( strFieldName.equals("CancelReason") ) {
            return 19;
        }
        if( strFieldName.equals("Operator") ) {
            return 20;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 21;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 22;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 23;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 24;
        }
        if( strFieldName.equals("StandByFlag1") ) {
            return 25;
        }
        if( strFieldName.equals("StandByFlag2") ) {
            return 26;
        }
        if( strFieldName.equals("StandByFlag3") ) {
            return 27;
        }
        if( strFieldName.equals("StandByFlag4") ) {
            return 28;
        }
        if( strFieldName.equals("StandByFlag5") ) {
            return 29;
        }
        if( strFieldName.equals("StandByFlag6") ) {
            return 30;
        }
        if( strFieldName.equals("StandByFlag7") ) {
            return 31;
        }
        if( strFieldName.equals("StandByFlag8") ) {
            return 32;
        }
        if( strFieldName.equals("StandByFlag9") ) {
            return 33;
        }
        if( strFieldName.equals("StandByFlag10") ) {
            return 34;
        }
        if( strFieldName.equals("StandByFlag11") ) {
            return 35;
        }
        if( strFieldName.equals("StandByFlag12") ) {
            return 36;
        }
        if( strFieldName.equals("StandByFlag13") ) {
            return 37;
        }
        if( strFieldName.equals("StandByFlag14") ) {
            return 38;
        }
        if( strFieldName.equals("StandByFlag15") ) {
            return 39;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "MonitorType";
                break;
            case 2:
                strFieldName = "MonitorNo";
                break;
            case 3:
                strFieldName = "ManageCom";
                break;
            case 4:
                strFieldName = "AgentGroup";
                break;
            case 5:
                strFieldName = "AgentName";
                break;
            case 6:
                strFieldName = "AgentSex";
                break;
            case 7:
                strFieldName = "Birthday";
                break;
            case 8:
                strFieldName = "IDNoType";
                break;
            case 9:
                strFieldName = "IDNo";
                break;
            case 10:
                strFieldName = "BranchAttr";
                break;
            case 11:
                strFieldName = "BranchManager";
                break;
            case 12:
                strFieldName = "BranchGroupName";
                break;
            case 13:
                strFieldName = "MonitorState";
                break;
            case 14:
                strFieldName = "MonitorDate";
                break;
            case 15:
                strFieldName = "CancelDate";
                break;
            case 16:
                strFieldName = "ReportCom";
                break;
            case 17:
                strFieldName = "ReportReason";
                break;
            case 18:
                strFieldName = "CancelCom";
                break;
            case 19:
                strFieldName = "CancelReason";
                break;
            case 20:
                strFieldName = "Operator";
                break;
            case 21:
                strFieldName = "MakeDate";
                break;
            case 22:
                strFieldName = "MakeTime";
                break;
            case 23:
                strFieldName = "ModifyDate";
                break;
            case 24:
                strFieldName = "ModifyTime";
                break;
            case 25:
                strFieldName = "StandByFlag1";
                break;
            case 26:
                strFieldName = "StandByFlag2";
                break;
            case 27:
                strFieldName = "StandByFlag3";
                break;
            case 28:
                strFieldName = "StandByFlag4";
                break;
            case 29:
                strFieldName = "StandByFlag5";
                break;
            case 30:
                strFieldName = "StandByFlag6";
                break;
            case 31:
                strFieldName = "StandByFlag7";
                break;
            case 32:
                strFieldName = "StandByFlag8";
                break;
            case 33:
                strFieldName = "StandByFlag9";
                break;
            case 34:
                strFieldName = "StandByFlag10";
                break;
            case 35:
                strFieldName = "StandByFlag11";
                break;
            case 36:
                strFieldName = "StandByFlag12";
                break;
            case 37:
                strFieldName = "StandByFlag13";
                break;
            case 38:
                strFieldName = "StandByFlag14";
                break;
            case 39:
                strFieldName = "StandByFlag15";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "AGENTCODE":
                return Schema.TYPE_STRING;
            case "MONITORTYPE":
                return Schema.TYPE_STRING;
            case "MONITORNO":
                return Schema.TYPE_INT;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "AGENTGROUP":
                return Schema.TYPE_STRING;
            case "AGENTNAME":
                return Schema.TYPE_STRING;
            case "AGENTSEX":
                return Schema.TYPE_STRING;
            case "BIRTHDAY":
                return Schema.TYPE_DATE;
            case "IDNOTYPE":
                return Schema.TYPE_STRING;
            case "IDNO":
                return Schema.TYPE_STRING;
            case "BRANCHATTR":
                return Schema.TYPE_STRING;
            case "BRANCHMANAGER":
                return Schema.TYPE_STRING;
            case "BRANCHGROUPNAME":
                return Schema.TYPE_STRING;
            case "MONITORSTATE":
                return Schema.TYPE_STRING;
            case "MONITORDATE":
                return Schema.TYPE_DATE;
            case "CANCELDATE":
                return Schema.TYPE_DATE;
            case "REPORTCOM":
                return Schema.TYPE_STRING;
            case "REPORTREASON":
                return Schema.TYPE_STRING;
            case "CANCELCOM":
                return Schema.TYPE_STRING;
            case "CANCELREASON":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG4":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG5":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG6":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG7":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG8":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG9":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG10":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG11":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG12":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG13":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG14":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG15":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_INT;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_DATE;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DATE;
            case 15:
                return Schema.TYPE_DATE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_DATE;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_DATE;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
