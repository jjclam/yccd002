/**
 * Copyright (c) 2018 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import com.sinosoft.lis.schema.LKCodeMapping_PolicySchema;
import com.sinosoft.lis.vschema.LKCodeMapping_PolicySet;
import com.sinosoft.utility.*;

import java.sql.*;

/**
 * <p>ClassName: LKCodeMapping_PolicyDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: sinosoft </p>
 *
 * @CreateDate：2018-07-05
 */
public class LKCodeMapping_PolicyDB extends LKCodeMapping_PolicySchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
     * flag = true: 传入Connection
     * flag = false: 不传入Connection
     **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;

    // @Constructor
    public LKCodeMapping_PolicyDB(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con, "LKCodeMapping_Policy");
        mflag = true;
    }

    public LKCodeMapping_PolicyDB() {
        con = null;
        db = new DBOper("LKCodeMapping_Policy");
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LKCodeMapping_PolicySchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LKCodeMapping_PolicySchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LKCodeMapping_Policy WHERE  1=1  AND BankCode = ? AND ZoneNo = ? AND BankNode = ? AND System = ?");
            if (this.getBankCode() == null || this.getBankCode().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getBankCode());
            }
            if (this.getZoneNo() == null || this.getZoneNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getZoneNo());
            }
            if (this.getBankNode() == null || this.getBankNode().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getBankNode());
            }
            if (this.getSystem() == null || this.getSystem().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getSystem());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e) {
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LKCodeMapping_Policy");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LKCodeMapping_Policy SET  BankCode = ? , ZoneNo = ? , BankNode = ? , AgentCom = ? , ComCode = ? , ManageCom = ? , Operator = ? , Remark = ? , Remark1 = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FTPAddress = ? , FTPDir = ? , FTPUser = ? , FTPPassWord = ? , Remark2 = ? , Remark3 = ? , ServerDir = ? , ServerAddress = ? , ServerPort = ? , ServerUser = ? , ServerPassWord = ? , Remark4 = ? , Remark5 = ? , BRCHNetCode = ? , System = ? , Bak1 = ? , Bak2 = ? , Bak3 = ? WHERE  1=1  AND BankCode = ? AND ZoneNo = ? AND BankNode = ? AND System = ?");
            if (this.getBankCode() == null || this.getBankCode().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getBankCode());
            }
            if (this.getZoneNo() == null || this.getZoneNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getZoneNo());
            }
            if (this.getBankNode() == null || this.getBankNode().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getBankNode());
            }
            if (this.getAgentCom() == null || this.getAgentCom().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getAgentCom());
            }
            if (this.getComCode() == null || this.getComCode().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, this.getComCode());
            }
            if (this.getManageCom() == null || this.getManageCom().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, this.getManageCom());
            }
            if (this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(7, 12);
            } else {
                pstmt.setString(7, this.getOperator());
            }
            if (this.getRemark() == null || this.getRemark().equals("null")) {
                pstmt.setNull(8, 12);
            } else {
                pstmt.setString(8, this.getRemark());
            }
            if (this.getRemark1() == null || this.getRemark1().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getRemark1());
            }
            if (this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(10, 93);
            } else {
                pstmt.setDate(10, Date.valueOf(this.getMakeDate()));
            }
            if (this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getMakeTime());
            }
            if (this.getModifyDate() == null || this.getModifyDate().equals("null")) {
                pstmt.setNull(12, 93);
            } else {
                pstmt.setDate(12, Date.valueOf(this.getModifyDate()));
            }
            if (this.getModifyTime() == null || this.getModifyTime().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getModifyTime());
            }
            if (this.getFTPAddress() == null || this.getFTPAddress().equals("null")) {
                pstmt.setNull(14, 12);
            } else {
                pstmt.setString(14, this.getFTPAddress());
            }
            if (this.getFTPDir() == null || this.getFTPDir().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getFTPDir());
            }
            if (this.getFTPUser() == null || this.getFTPUser().equals("null")) {
                pstmt.setNull(16, 12);
            } else {
                pstmt.setString(16, this.getFTPUser());
            }
            if (this.getFTPPassWord() == null || this.getFTPPassWord().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getFTPPassWord());
            }
            if (this.getRemark2() == null || this.getRemark2().equals("null")) {
                pstmt.setNull(18, 12);
            } else {
                pstmt.setString(18, this.getRemark2());
            }
            if (this.getRemark3() == null || this.getRemark3().equals("null")) {
                pstmt.setNull(19, 12);
            } else {
                pstmt.setString(19, this.getRemark3());
            }
            if (this.getServerDir() == null || this.getServerDir().equals("null")) {
                pstmt.setNull(20, 12);
            } else {
                pstmt.setString(20, this.getServerDir());
            }
            if (this.getServerAddress() == null || this.getServerAddress().equals("null")) {
                pstmt.setNull(21, 12);
            } else {
                pstmt.setString(21, this.getServerAddress());
            }
            pstmt.setInt(22, this.getServerPort());
            if (this.getServerUser() == null || this.getServerUser().equals("null")) {
                pstmt.setNull(23, 12);
            } else {
                pstmt.setString(23, this.getServerUser());
            }
            if (this.getServerPassWord() == null || this.getServerPassWord().equals("null")) {
                pstmt.setNull(24, 12);
            } else {
                pstmt.setString(24, this.getServerPassWord());
            }
            if (this.getRemark4() == null || this.getRemark4().equals("null")) {
                pstmt.setNull(25, 12);
            } else {
                pstmt.setString(25, this.getRemark4());
            }
            if (this.getRemark5() == null || this.getRemark5().equals("null")) {
                pstmt.setNull(26, 12);
            } else {
                pstmt.setString(26, this.getRemark5());
            }
            if (this.getBRCHNetCode() == null || this.getBRCHNetCode().equals("null")) {
                pstmt.setNull(27, 12);
            } else {
                pstmt.setString(27, this.getBRCHNetCode());
            }
            if (this.getSystem() == null || this.getSystem().equals("null")) {
                pstmt.setNull(28, 12);
            } else {
                pstmt.setString(28, this.getSystem());
            }
            if (this.getBak1() == null || this.getBak1().equals("null")) {
                pstmt.setNull(29, 12);
            } else {
                pstmt.setString(29, this.getBak1());
            }
            if (this.getBak2() == null || this.getBak2().equals("null")) {
                pstmt.setNull(30, 12);
            } else {
                pstmt.setString(30, this.getBak2());
            }
            if (this.getBak3() == null || this.getBak3().equals("null")) {
                pstmt.setNull(31, 12);
            } else {
                pstmt.setString(31, this.getBak3());
            }
            // set where condition
            if (this.getBankCode() == null || this.getBankCode().equals("null")) {
                pstmt.setNull(32, 12);
            } else {
                pstmt.setString(32, this.getBankCode());
            }
            if (this.getZoneNo() == null || this.getZoneNo().equals("null")) {
                pstmt.setNull(33, 12);
            } else {
                pstmt.setString(33, this.getZoneNo());
            }
            if (this.getBankNode() == null || this.getBankNode().equals("null")) {
                pstmt.setNull(34, 12);
            } else {
                pstmt.setString(34, this.getBankNode());
            }
            if (this.getSystem() == null || this.getSystem().equals("null")) {
                pstmt.setNull(35, 12);
            } else {
                pstmt.setString(35, this.getSystem());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e) {
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LKCodeMapping_Policy");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LKCodeMapping_Policy VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if (this.getBankCode() == null || this.getBankCode().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getBankCode());
            }
            if (this.getZoneNo() == null || this.getZoneNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getZoneNo());
            }
            if (this.getBankNode() == null || this.getBankNode().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getBankNode());
            }
            if (this.getAgentCom() == null || this.getAgentCom().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getAgentCom());
            }
            if (this.getComCode() == null || this.getComCode().equals("null")) {
                pstmt.setNull(5, 12);
            } else {
                pstmt.setString(5, this.getComCode());
            }
            if (this.getManageCom() == null || this.getManageCom().equals("null")) {
                pstmt.setNull(6, 12);
            } else {
                pstmt.setString(6, this.getManageCom());
            }
            if (this.getOperator() == null || this.getOperator().equals("null")) {
                pstmt.setNull(7, 12);
            } else {
                pstmt.setString(7, this.getOperator());
            }
            if (this.getRemark() == null || this.getRemark().equals("null")) {
                pstmt.setNull(8, 12);
            } else {
                pstmt.setString(8, this.getRemark());
            }
            if (this.getRemark1() == null || this.getRemark1().equals("null")) {
                pstmt.setNull(9, 12);
            } else {
                pstmt.setString(9, this.getRemark1());
            }
            if (this.getMakeDate() == null || this.getMakeDate().equals("null")) {
                pstmt.setNull(10, 93);
            } else {
                pstmt.setDate(10, Date.valueOf(this.getMakeDate()));
            }
            if (this.getMakeTime() == null || this.getMakeTime().equals("null")) {
                pstmt.setNull(11, 12);
            } else {
                pstmt.setString(11, this.getMakeTime());
            }
            if (this.getModifyDate() == null || this.getModifyDate().equals("null")) {
                pstmt.setNull(12, 93);
            } else {
                pstmt.setDate(12, Date.valueOf(this.getModifyDate()));
            }
            if (this.getModifyTime() == null || this.getModifyTime().equals("null")) {
                pstmt.setNull(13, 12);
            } else {
                pstmt.setString(13, this.getModifyTime());
            }
            if (this.getFTPAddress() == null || this.getFTPAddress().equals("null")) {
                pstmt.setNull(14, 12);
            } else {
                pstmt.setString(14, this.getFTPAddress());
            }
            if (this.getFTPDir() == null || this.getFTPDir().equals("null")) {
                pstmt.setNull(15, 12);
            } else {
                pstmt.setString(15, this.getFTPDir());
            }
            if (this.getFTPUser() == null || this.getFTPUser().equals("null")) {
                pstmt.setNull(16, 12);
            } else {
                pstmt.setString(16, this.getFTPUser());
            }
            if (this.getFTPPassWord() == null || this.getFTPPassWord().equals("null")) {
                pstmt.setNull(17, 12);
            } else {
                pstmt.setString(17, this.getFTPPassWord());
            }
            if (this.getRemark2() == null || this.getRemark2().equals("null")) {
                pstmt.setNull(18, 12);
            } else {
                pstmt.setString(18, this.getRemark2());
            }
            if (this.getRemark3() == null || this.getRemark3().equals("null")) {
                pstmt.setNull(19, 12);
            } else {
                pstmt.setString(19, this.getRemark3());
            }
            if (this.getServerDir() == null || this.getServerDir().equals("null")) {
                pstmt.setNull(20, 12);
            } else {
                pstmt.setString(20, this.getServerDir());
            }
            if (this.getServerAddress() == null || this.getServerAddress().equals("null")) {
                pstmt.setNull(21, 12);
            } else {
                pstmt.setString(21, this.getServerAddress());
            }
            pstmt.setInt(22, this.getServerPort());
            if (this.getServerUser() == null || this.getServerUser().equals("null")) {
                pstmt.setNull(23, 12);
            } else {
                pstmt.setString(23, this.getServerUser());
            }
            if (this.getServerPassWord() == null || this.getServerPassWord().equals("null")) {
                pstmt.setNull(24, 12);
            } else {
                pstmt.setString(24, this.getServerPassWord());
            }
            if (this.getRemark4() == null || this.getRemark4().equals("null")) {
                pstmt.setNull(25, 12);
            } else {
                pstmt.setString(25, this.getRemark4());
            }
            if (this.getRemark5() == null || this.getRemark5().equals("null")) {
                pstmt.setNull(26, 12);
            } else {
                pstmt.setString(26, this.getRemark5());
            }
            if (this.getBRCHNetCode() == null || this.getBRCHNetCode().equals("null")) {
                pstmt.setNull(27, 12);
            } else {
                pstmt.setString(27, this.getBRCHNetCode());
            }
            if (this.getSystem() == null || this.getSystem().equals("null")) {
                pstmt.setNull(28, 12);
            } else {
                pstmt.setString(28, this.getSystem());
            }
            if (this.getBak1() == null || this.getBak1().equals("null")) {
                pstmt.setNull(29, 12);
            } else {
                pstmt.setString(29, this.getBak1());
            }
            if (this.getBak2() == null || this.getBak2().equals("null")) {
                pstmt.setNull(30, 12);
            } else {
                pstmt.setString(30, this.getBak2());
            }
            if (this.getBak3() == null || this.getBak3().equals("null")) {
                pstmt.setNull(31, 12);
            } else {
                pstmt.setString(31, this.getBak3());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e) {
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }

            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LKCodeMapping_Policy WHERE  1=1  AND BankCode = ? AND ZoneNo = ? AND BankNode = ? AND System = ?",
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if (this.getBankCode() == null || this.getBankCode().equals("null")) {
                pstmt.setNull(1, 12);
            } else {
                pstmt.setString(1, this.getBankCode());
            }
            if (this.getZoneNo() == null || this.getZoneNo().equals("null")) {
                pstmt.setNull(2, 12);
            } else {
                pstmt.setString(2, this.getZoneNo());
            }
            if (this.getBankNode() == null || this.getBankNode().equals("null")) {
                pstmt.setNull(3, 12);
            } else {
                pstmt.setString(3, this.getBankNode());
            }
            if (this.getSystem() == null || this.getSystem().equals("null")) {
                pstmt.setNull(4, 12);
            } else {
                pstmt.setString(4, this.getSystem());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LKCodeMapping_PolicyDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors.addOneError(tError);

                    try {
                        rs.close();
                    } catch (Exception ex) {
                    }
                    try {
                        pstmt.close();
                    } catch (Exception ex1) {
                    }

                    if (!mflag) {
                        try {
                            con.close();
                        } catch (Exception et) {
                        }
                    }
                    return false;
                }
                break;
            }
            try {
                rs.close();
            } catch (Exception ex2) {
            }
            try {
                pstmt.close();
            } catch (Exception ex3) {
            }

            if (i == 0) {
                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {
                    }
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                pstmt.close();
            } catch (Exception ex1) {
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                }
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return true;
    }

    public LKCodeMapping_PolicySet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LKCodeMapping_PolicySet aLKCodeMapping_PolicySet = new LKCodeMapping_PolicySet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LKCodeMapping_Policy");
            LKCodeMapping_PolicySchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i > 10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LKCodeMapping_PolicyDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors.addOneError(tError);
                    break;
                }
                LKCodeMapping_PolicySchema s1 = new LKCodeMapping_PolicySchema();
                s1.setSchema(rs, i);
                aLKCodeMapping_PolicySet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex1) {
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try {
                rs.close();
            } catch (Exception ex2) {
                ex2.printStackTrace();
            }
            try {
                stmt.close();
            } catch (Exception ex3) {
                ex3.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                    et.printStackTrace();
                }
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return aLKCodeMapping_PolicySet;
    }

    public LKCodeMapping_PolicySet executeQuery(String sql) {
        Statement stmt = null;
        ResultSet rs = null;
        LKCodeMapping_PolicySet aLKCodeMapping_PolicySet = new LKCodeMapping_PolicySet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i > 10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LKCodeMapping_PolicyDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors.addOneError(tError);
                    break;
                }
                LKCodeMapping_PolicySchema s1 = new LKCodeMapping_PolicySchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LKCodeMapping_PolicyDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLKCodeMapping_PolicySet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex1) {
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {
            }
            try {
                stmt.close();
            } catch (Exception ex3) {
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                }
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return aLKCodeMapping_PolicySet;
    }

    public LKCodeMapping_PolicySet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LKCodeMapping_PolicySet aLKCodeMapping_PolicySet = new LKCodeMapping_PolicySet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LKCodeMapping_Policy");
            LKCodeMapping_PolicySchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                LKCodeMapping_PolicySchema s1 = new LKCodeMapping_PolicySchema();
                s1.setSchema(rs, i);
                aLKCodeMapping_PolicySet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex1) {
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {
            }
            try {
                stmt.close();
            } catch (Exception ex3) {
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                }
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return aLKCodeMapping_PolicySet;
    }

    public LKCodeMapping_PolicySet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LKCodeMapping_PolicySet aLKCodeMapping_PolicySet = new LKCodeMapping_PolicySet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if (i < nStart) {
                    continue;
                }

                if (i >= nStart + nCount) {
                    break;
                }

                LKCodeMapping_PolicySchema s1 = new LKCodeMapping_PolicySchema();
                if (!s1.setSchema(rs, i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LKCodeMapping_PolicyDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors.addOneError(tError);
                }
                aLKCodeMapping_PolicySet.add(s1);
            }
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex1) {
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);

            try {
                rs.close();
            } catch (Exception ex2) {
            }
            try {
                stmt.close();
            } catch (Exception ex3) {
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                }
            }
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return aLKCodeMapping_PolicySet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LKCodeMapping_Policy");
            LKCodeMapping_PolicySchema aSchema = this.getSchema();
            sqlObj.setSQL(2, aSchema);
            String sql = "update LKCodeMapping_Policy " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LKCodeMapping_PolicyDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors.addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    } catch (Exception et) {
                    }
                }
                return false;
            }
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);

            try {
                stmt.close();
            } catch (Exception ex1) {
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                }
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }

        return true;
    }

    /**
     * 准备数据查询条件
     *
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            } catch (Exception ex2) {
            }
            try {
                mStatement.close();
            } catch (Exception ex3) {
            }
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                }
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            } catch (Exception e) {
            }
        }
        return true;
    }

    /**
     * 获取数据集
     *
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {
            }
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {
            }
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                }
            }
            return false;
        }
        return flag;
    }

    /**
     * 获取定量数据
     *
     * @return LKCodeMapping_PolicySet
     */
    public LKCodeMapping_PolicySet getData() {
        int tCount = 0;
        LKCodeMapping_PolicySet tLKCodeMapping_PolicySet = new LKCodeMapping_PolicySet();
        LKCodeMapping_PolicySchema tLKCodeMapping_PolicySchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLKCodeMapping_PolicySchema = new LKCodeMapping_PolicySchema();
            tLKCodeMapping_PolicySchema.setSchema(mResultSet, 1);
            tLKCodeMapping_PolicySet.add(tLKCodeMapping_PolicySchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLKCodeMapping_PolicySchema = new LKCodeMapping_PolicySchema();
                    tLKCodeMapping_PolicySchema.setSchema(mResultSet, 1);
                    tLKCodeMapping_PolicySet.add(tLKCodeMapping_PolicySchema);
                }
            }
        } catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            } catch (Exception ex2) {
            }
            try {
                mStatement.close();
                mStatement = null;
            } catch (Exception ex3) {
            }
            if (!mflag) {
                try {
                    con.close();
                } catch (Exception et) {
                }
            }
            return null;
        }
        return tLKCodeMapping_PolicySet;
    }

    /**
     * 关闭数据集
     *
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LKCodeMapping_PolicyDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        } catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LKCodeMapping_PolicyDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        } catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LKCodeMapping_PolicyDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
