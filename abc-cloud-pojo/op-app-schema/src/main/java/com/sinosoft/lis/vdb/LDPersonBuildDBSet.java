/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import com.sinosoft.lis.vschema.LDPersonBuildSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.DBOper;
import com.sinosoft.utility.ExceptionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * <p>ClassName: LDPersonBuildDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2017</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2017-11-23
 */
public class LDPersonBuildDBSet extends LDPersonBuildSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LDPersonBuildDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LDPersonBuild");
        mflag = true;
    }

    public LDPersonBuildDBSet() {
        db = new DBOper( "LDPersonBuild" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDPersonBuildDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LDPersonBuild WHERE  1=1  AND MinAge = ? AND MaxAge = ? AND AgeFlag = ? AND Sex = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setInt(1, this.get(i).getMinAge());
            pstmt.setInt(2, this.get(i).getMaxAge());
            if(this.get(i).getAgeFlag() == null || this.get(i).getAgeFlag().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getAgeFlag());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSex());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDPersonBuildDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LDPersonBuild SET  MinAge = ? , MaxAge = ? , AgeFlag = ? , Sex = ? , MinStature = ? , MaxStature = ? , MinAvoirdupois = ? , MaxAvoirdupois = ? , MinBMI = ? , MaxBMI = ? WHERE  1=1  AND MinAge = ? AND MaxAge = ? AND AgeFlag = ? AND Sex = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setInt(1, this.get(i).getMinAge());
            pstmt.setInt(2, this.get(i).getMaxAge());
            if(this.get(i).getAgeFlag() == null || this.get(i).getAgeFlag().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getAgeFlag());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSex());
            }
            pstmt.setDouble(5, this.get(i).getMinStature());
            pstmt.setDouble(6, this.get(i).getMaxStature());
            pstmt.setDouble(7, this.get(i).getMinAvoirdupois());
            pstmt.setDouble(8, this.get(i).getMaxAvoirdupois());
            pstmt.setDouble(9, this.get(i).getMinBMI());
            pstmt.setDouble(10, this.get(i).getMaxBMI());
            // set where condition
            pstmt.setInt(11, this.get(i).getMinAge());
            pstmt.setInt(12, this.get(i).getMaxAge());
            if(this.get(i).getAgeFlag() == null || this.get(i).getAgeFlag().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getAgeFlag());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getSex());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDPersonBuildDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LDPersonBuild VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setInt(1, this.get(i).getMinAge());
            pstmt.setInt(2, this.get(i).getMaxAge());
            if(this.get(i).getAgeFlag() == null || this.get(i).getAgeFlag().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getAgeFlag());
            }
            if(this.get(i).getSex() == null || this.get(i).getSex().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSex());
            }
            pstmt.setDouble(5, this.get(i).getMinStature());
            pstmt.setDouble(6, this.get(i).getMaxStature());
            pstmt.setDouble(7, this.get(i).getMinAvoirdupois());
            pstmt.setDouble(8, this.get(i).getMaxAvoirdupois());
            pstmt.setDouble(9, this.get(i).getMinBMI());
            pstmt.setDouble(10, this.get(i).getMaxBMI());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDPersonBuildDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
