package com.sinosoft.lis.pos.entity.preserve;


import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PolicyXHQueryInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 客服信函服务POS093.
 */

@Relation(transID = "POS093")
public class RespCustomerServicePojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;

    private PolicyXHQueryInfosPojo PolicyXHQueryInfosPojo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.lis.pos.entity.PolicyXHQueryInfosPojo getPolicyXHQueryInfosPojo() {
        return PolicyXHQueryInfosPojo;
    }

    @JSONField(name = "policyXHQueryInfos")
    public void setPolicyXHQueryInfosPojo(com.sinosoft.lis.pos.entity.PolicyXHQueryInfosPojo policyXHQueryInfosPojo) {
        PolicyXHQueryInfosPojo = policyXHQueryInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespCustomerServicePojo{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyXHQueryInfosPojo=" + PolicyXHQueryInfosPojo +
                '}';
    }
}