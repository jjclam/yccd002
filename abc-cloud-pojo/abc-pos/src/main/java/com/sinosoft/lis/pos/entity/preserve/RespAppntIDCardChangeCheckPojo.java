package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 投保人证件有效期校验
 */
@Relation(transID = "POS058")
public class RespAppntIDCardChangeCheckPojo implements Pojo,Body,Serializable{
    //是否过期
    private String IDStatus;
    //证件有效期
    private String ValidDate;

    @Override
    public String toString() {
        return "RespAppntIDCardChangeCheckPojo{" +
                "IDStatus='" + IDStatus + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                '}';
    }

    public String getIDStatus() {
        return IDStatus;
    }

    public void setIDStatus(String IDStatus) {
        this.IDStatus = IDStatus;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
