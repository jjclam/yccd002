package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;

public class Lcbnf implements Serializable {
    //受益人类型
    private String BnfType;
    //受益人姓名
    private String Name;
    //性别
    private String Sex;
    //出生日期
    private String Birthday;
    //证件类型
    private String IDKind;
    //证件号码
    private String IDCode;
    //与被保人关系
    private String RelationToInsured;
    //受益人受益顺序
    private String Sequence;
    //受益人受益比例
    private String Prop;
    //自增
    private String BnfNo;
    private String BnfLot;

    public String getBnfType() {
        return BnfType;
    }

    public void setBnfType(String bnfType) {
        BnfType = bnfType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getIDKind() {
        return IDKind;
    }

    public void setIDKind(String IDKind) {
        this.IDKind = IDKind;
    }

    public String getIDCode() {
        return IDCode;
    }

    public void setIDCode(String IDCode) {
        this.IDCode = IDCode;
    }

    public String getRelationToInsured() {
        return RelationToInsured;
    }

    public void setRelationToInsured(String relationToInsured) {
        RelationToInsured = relationToInsured;
    }

    public String getSequence() {
        return Sequence;
    }

    public void setSequence(String sequence) {
        Sequence = sequence;
    }

    public String getProp() {
        return Prop;
    }

    public void setProp(String prop) {
        Prop = prop;
    }

    public String getBnfNo() {
        return BnfNo;
    }

    public void setBnfNo(String bnfNo) {
        BnfNo = bnfNo;
    }

    public String getBnfLot() {
        return BnfLot;
    }

    public void setBnfLot(String bnfLot) {
        BnfLot = bnfLot;
    }

    @Override
    public String toString() {
        return "Lcbnf{" +
                "BnfType='" + BnfType + '\'' +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", IDKind='" + IDKind + '\'' +
                ", IDCode='" + IDCode + '\'' +
                ", RelationToInsured='" + RelationToInsured + '\'' +
                ", Sequence='" + Sequence + '\'' +
                ", Prop='" + Prop + '\'' +
                ", BnfNo='" + BnfNo + '\'' +
                ", BnfLot='" + BnfLot + '\'' +
                '}';
    }
}