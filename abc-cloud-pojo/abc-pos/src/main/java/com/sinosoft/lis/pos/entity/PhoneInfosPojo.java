package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class PhoneInfosPojo implements Body,Serializable{
    //联系电话个数
    private String Count;
    //联系电话内容节点
    private List<com.sinosoft.lis.pos.entity.PhoneInfoPojo> PhoneInfoPojo;

    @Override
    public String toString() {
        return "PhoneInfosPojo{" +
                "Count='" + Count + '\'' +
                ", PhoneInfoPojo=" + PhoneInfoPojo +
                '}';
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public List<com.sinosoft.lis.pos.entity.PhoneInfoPojo> getPhoneInfoPojo() {
        return PhoneInfoPojo;
    }
    @JSONField(name = "phoneInfo")
    public void setPhoneInfoPojo(List<com.sinosoft.lis.pos.entity.PhoneInfoPojo> phoneInfoPojo) {
        PhoneInfoPojo = phoneInfoPojo;
    }

}
