package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.BnfInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

;

/**
 * 受益人身份证有效期变更明细查询POS051.
 */
@Relation(transID = "POS051")
public class RespBnfIdValiDateDetailsQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    private BnfInfosPojo BnfInfosPojo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    @JSONField(name = "bnfInfos")
    public com.sinosoft.lis.pos.entity.BnfInfosPojo getBnfInfosPojo() {
        return BnfInfosPojo;
    }

    @JSONField(name = "bnfInfos")
    public void setBnfInfosPojo(com.sinosoft.lis.pos.entity.BnfInfosPojo bnfInfosPojo) {
        BnfInfosPojo = bnfInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespBnfIdValiDateDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", BnfInfosPojo=" + BnfInfosPojo +
                '}';
    }
}