package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

public class WecLPSyncImagesResPojo implements Body, Serializable {
    private List<WecLPSyncImagesRePojo> WecLPSyncImagesRePojo;

    public List<com.sinosoft.lis.pos.entity.WecLPSyncImagesRePojo> getWecLPSyncImagesRePojo() {
        return WecLPSyncImagesRePojo;
    }

    @JSONField(name = "wecLPSyncImagesRe")
    public void setWecLPSyncImagesRePojo(List<com.sinosoft.lis.pos.entity.WecLPSyncImagesRePojo> wecLPSyncImagesRePojo) {
        WecLPSyncImagesRePojo = wecLPSyncImagesRePojo;
    }

    @Override
    public String toString() {
        return "WecLPSyncImagesResPojo{" +
                "WecLPSyncImagesRePojo=" + WecLPSyncImagesRePojo +
                '}';
    }
}