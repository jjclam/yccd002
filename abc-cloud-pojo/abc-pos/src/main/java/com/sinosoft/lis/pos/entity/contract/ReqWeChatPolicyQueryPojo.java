package com.sinosoft.lis.pos.entity.contract;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.AgentPojo;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 微信保单查询NB012.
 */
public class ReqWeChatPolicyQueryPojo implements ReqBody, Body, Serializable {
    //网点信息
    private AgentPojo AgentPojo;
    //险种代码
    private String RiskCode;
    //保单号
    private String PolicyNo;
    //保单密码
    private String Policypwd;

    @JSONField(name = "agent")
    public com.sinosoft.lis.pos.entity.AgentPojo getAgentPojo() {
        return AgentPojo;
    }

    public void setAgentPojo(com.sinosoft.lis.pos.entity.AgentPojo agentPojo) {
        AgentPojo = agentPojo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }

    public void setPolicyNo(String policyNo) {
        PolicyNo = policyNo;
    }

    public String getPolicypwd() {
        return Policypwd;
    }

    public void setPolicypwd(String policypwd) {
        Policypwd = policypwd;
    }

    @Override
    public String toString() {
        return "ReqWeChatPolicyQueryPojo{" +
                "AgentPojo=" + AgentPojo +
                ", RiskCode='" + RiskCode + '\'' +
                ", PolicyNo='" + PolicyNo + '\'' +
                ", Policypwd='" + Policypwd + '\'' +
                '}';
    }
}