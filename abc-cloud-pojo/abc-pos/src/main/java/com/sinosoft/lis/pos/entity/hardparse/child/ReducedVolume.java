package com.sinosoft.lis.pos.entity.hardparse.child;

public class ReducedVolume {
    //保险年期
    private String PolYear;
    //减额交清保额
    private String Value;

    @Override
    public String toString() {
        return "ReducedVolume{" +
                "PolYear='" + PolYear + '\'' +
                ", Value='" + Value + '\'' +
                '}';
    }

    public String getPolYear() {
        return PolYear;
    }

    public void setPolYear(String polYear) {
        PolYear = polYear;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
