package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;


/**
 * 红利领取方式保全项目保单明细查询POS021.
 */
@Relation(transID = "POS021")
public class RespBonusGetModeDetailsQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //领取形式
    private String GetMode;
    //红利领取方式(一级)
    private String BonusGetMode;
    //红利领取方式(二级)
    private String BonusAccFlag;
    //红利领取方式(三级)
    private String BonusGetForm;
    //万能账户派发方式校验标识
    private String JudgeFlag;
    //户名
    private String AccName;
    //银行账号
    private String BankAccNo;
    //银行编码
    private String  BankCode;



    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getGetMode() {
        return GetMode;
    }

    public void setGetMode(String getMode) {
        GetMode = getMode;
    }

    public String getBonusGetMode() {
        return BonusGetMode;
    }

    public void setBonusGetMode(String bonusGetMode) {
        BonusGetMode = bonusGetMode;
    }

    public String getBonusAccFlag() {
        return BonusAccFlag;
    }

    public void setBonusAccFlag(String bonusAccFlag) {
        BonusAccFlag = bonusAccFlag;
    }

    public String getBonusGetForm() {
        return BonusGetForm;
    }

    public void setBonusGetForm(String bonusGetForm) {
        BonusGetForm = bonusGetForm;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getJudgeFlag() {
        return JudgeFlag;
    }

    public void setJudgeFlag(String judgeFlag) {
        JudgeFlag = judgeFlag;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespBonusGetModeDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", GetMode='" + GetMode + '\'' +
                ", BonusGetMode='" + BonusGetMode + '\'' +
                ", BonusAccFlag='" + BonusAccFlag + '\'' +
                ", BonusGetForm='" + BonusGetForm + '\'' +
                ", JudgeFlag='" + JudgeFlag + '\'' +
                ", AccName='" + AccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                '}';
    }
}