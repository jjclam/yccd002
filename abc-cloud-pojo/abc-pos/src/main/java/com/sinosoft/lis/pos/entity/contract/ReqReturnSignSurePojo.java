package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 银保通回执签收确认NB011  回执签收确认NB003.
 */
public class ReqReturnSignSurePojo implements ReqBody, Body, Serializable {
    //投保单号
    private String ProposalContNo;
    //业务类型
    private String BussType;
    //保单号
    private String ContNo;
    //保单送达日期
    private String SignDate;
    //保单送达时间
    private String GetPolTime;
    //保单回执客户签收日期
    private String CustomGetPolDate;
    //投保单号
    private String CardNo;
    //单证编码（9995-保单号）
    private String CertifyCode;
    //D+代理人工号
    private String SendOutCom;
    //D+代理人工号
    private String ReceiveCom;
    //单证回收日期
    private String TakeBackDate;
    //保单号
    private String CertifyNo;
    //代理人签收日期
    private String AgentTakeBackDate;
    //印刷号
    private String PrtNo;
    //保单送达日期
    private String GetPolDate;


    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        ProposalContNo = proposalContNo;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getSignDate() {
        return SignDate;
    }

    public void setSignDate(String signDate) {
        SignDate = signDate;
    }

    public String getGetPolTime() {
        return GetPolTime;
    }

    public void setGetPolTime(String getPolTime) {
        GetPolTime = getPolTime;
    }

    public String getCustomGetPolDate() {
        return CustomGetPolDate;
    }

    public void setCustomGetPolDate(String customGetPolDate) {
        CustomGetPolDate = customGetPolDate;
    }

    public String getCardNo() {
        return CardNo;
    }

    public void setCardNo(String cardNo) {
        CardNo = cardNo;
    }

    public String getCertifyCode() {
        return CertifyCode;
    }

    public void setCertifyCode(String certifyCode) {
        CertifyCode = certifyCode;
    }

    public String getSendOutCom() {
        return SendOutCom;
    }

    public void setSendOutCom(String sendOutCom) {
        SendOutCom = sendOutCom;
    }

    public String getReceiveCom() {
        return ReceiveCom;
    }

    public void setReceiveCom(String receiveCom) {
        ReceiveCom = receiveCom;
    }

    public String getTakeBackDate() {
        return TakeBackDate;
    }

    public void setTakeBackDate(String takeBackDate) {
        TakeBackDate = takeBackDate;
    }

    public String getCertifyNo() {
        return CertifyNo;
    }

    public void setCertifyNo(String certifyNo) {
        CertifyNo = certifyNo;
    }

    public String getAgentTakeBackDate() {
        return AgentTakeBackDate;
    }

    public void setAgentTakeBackDate(String agentTakeBackDate) {
        AgentTakeBackDate = agentTakeBackDate;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getGetPolDate() {
        return GetPolDate;
    }

    public void setGetPolDate(String getPolDate) {
        GetPolDate = getPolDate;
    }

    @Override
    public String toString() {
        return "ReqReturnSignSurePojo{" +
                "ProposalContNo='" + ProposalContNo + '\'' +
                ", BussType='" + BussType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", SignDate='" + SignDate + '\'' +
                ", GetPolTime='" + GetPolTime + '\'' +
                ", CustomGetPolDate='" + CustomGetPolDate + '\'' +
                ", CardNo='" + CardNo + '\'' +
                ", CertifyCode='" + CertifyCode + '\'' +
                ", SendOutCom='" + SendOutCom + '\'' +
                ", ReceiveCom='" + ReceiveCom + '\'' +
                ", TakeBackDate='" + TakeBackDate + '\'' +
                ", CertifyNo='" + CertifyNo + '\'' +
                ", AgentTakeBackDate='" + AgentTakeBackDate + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                ", GetPolDate='" + GetPolDate + '\'' +
                '}';
    }
}