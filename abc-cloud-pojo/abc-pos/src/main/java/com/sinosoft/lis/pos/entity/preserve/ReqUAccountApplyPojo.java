package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PersonInfoPojo;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4004保单万能账户追加保费申请POS028,官网4007万能账户追加保费_变更
 */
public class ReqUAccountApplyPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;
    //申请人信息
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    //领取人信息
    private com.sinosoft.lis.pos.entity.PersonInfoPojo PersonInfoPojo;
    //银行信息
    private com.sinosoft.lis.pos.entity.BankInfoPojo BankInfoPojo;
    //收付费方式
    private String PayMode;

    @Override
    public String toString() {
        return "ReqUAccountApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                ", PayMode='" + PayMode + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }
    @JSONField(name = "edorAppInfo")
    public com.sinosoft.lis.pos.entity.EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(com.sinosoft.lis.pos.entity.EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }
    @JSONField(name = "personInfo")
    public PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }
    @JSONField(name = "bankInfo")
    public com.sinosoft.lis.pos.entity.BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(com.sinosoft.lis.pos.entity.BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

}
