package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class AppInfoPojo implements Body,Serializable{
    //投保人名称
    private String AppName;
    //投保人证件类型
    private String AppIDType;
    //投保人证件号码
    private String AppIDCode;
    //投保人手机号码
    private String AppCellPhone;
    //投保人电话号码
    private String AppPhone;
    //投保人通信地址
    private String AppAddress;
    //投保人邮政编码
    private String AppZipCode;

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    public String getAppIDType() {
        return AppIDType;
    }

    public void setAppIDType(String appIDType) {
        AppIDType = appIDType;
    }

    public String getAppIDCode() {
        return AppIDCode;
    }

    public void setAppIDCode(String appIDCode) {
        AppIDCode = appIDCode;
    }

    public String getAppCellPhone() {
        return AppCellPhone;
    }

    public void setAppCellPhone(String appCellPhone) {
        AppCellPhone = appCellPhone;
    }

    public String getAppPhone() {
        return AppPhone;
    }

    public void setAppPhone(String appPhone) {
        AppPhone = appPhone;
    }

    public String getAppAddress() {
        return AppAddress;
    }

    public void setAppAddress(String appAddress) {
        AppAddress = appAddress;
    }

    public String getAppZipCode() {
        return AppZipCode;
    }

    public void setAppZipCode(String appZipCode) {
        AppZipCode = appZipCode;
    }

    @Override
    public String toString() {
        return "AppInfoPojo{" +
                "AppName='" + AppName + '\'' +
                ", AppIDType='" + AppIDType + '\'' +
                ", AppIDCode='" + AppIDCode + '\'' +
                ", AppCellPhone='" + AppCellPhone + '\'' +
                ", AppPhone='" + AppPhone + '\'' +
                ", AppAddress='" + AppAddress + '\'' +
                ", AppZipCode='" + AppZipCode + '\'' +
                '}';
    }
}