package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class BonusRiskPojo implements Body,Serializable{
    //红利派发年度
    private String BonusYear;
    //红利派发金额
    private String BonusValue;
    //增额交清标记
    private String AddAmntFlag;
    //增额交清标记
    private String AddAmnt;
    //主险名称
    private String MainRiskName;

    public String getBonusYear() {
        return BonusYear;
    }

    public void setBonusYear(String bonusYear) {
        BonusYear = bonusYear;
    }

    public String getBonusValue() {
        return BonusValue;
    }

    public void setBonusValue(String bonusValue) {
        BonusValue = bonusValue;
    }

    public String getAddAmntFlag() {
        return AddAmntFlag;
    }

    public void setAddAmntFlag(String addAmntFlag) {
        AddAmntFlag = addAmntFlag;
    }

    public String getAddAmnt() {
        return AddAmnt;
    }

    public void setAddAmnt(String addAmnt) {
        AddAmnt = addAmnt;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }


    @Override
    public String toString() {
        return "BonusRiskPojo{" +
                "BonusYear='" + BonusYear + '\'' +
                ", BonusValue='" + BonusValue + '\'' +
                ", AddAmntFlag='" + AddAmntFlag + '\'' +
                ", AddAmnt='" + AddAmnt + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                '}';
    }
}