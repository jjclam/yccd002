package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;
import java.util.List;

public class Addts implements Serializable {
    private List<Addt> Addt;

    public List<com.sinosoft.lis.pos.entity.hardparse.contDetails.Addt> getAddt() {
        return Addt;
    }

    public void setAddt(List<com.sinosoft.lis.pos.entity.hardparse.contDetails.Addt> addt) {
        Addt = addt;
    }

    @Override
    public String toString() {
        return "Addts{" +
                "Addt=" + Addt +
                '}';
    }
}