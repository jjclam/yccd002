package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;


/**
 * 4003万能补缴明细查询POS035  官网4007保单万能补缴明细查询.
 */
@Relation(transID = {"POS035", "POS107"})
public class RespPayMentDetailsQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //主险名称
    private String MainRiskName;
    //涉及险种
    private String RelationRisk;
    //补退费金额合计
    private String AllGetMoney;
    // 领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //开户银行
    private String NewBankName;
    //银行帐号
    private String NewBankAccNo;
    //户名
    private String NewBankAccName;
    //手机电话号码
    private String MobilePhone;
    //期缴保费
    private String Repremium;
    //欠缴续期保费期数
    private String UnrepreNumber;
    //万能账户余额
    private String AccontBalance;
    //最少缴费期数
    private String Leastprenumber;
    //缴费期数
    private String Reprenumber;
    //补缴保费
    private String PayPrem;
    //补缴保费利息
    private String PayPremInterest;
    //收付费方式
    private String PayMode;
    //银行账户
    private String BankAccNo;
    //户名
    private String BankAccName;
    //开户行
    private String BankCode;
    //补退费领取人
    private String RefundPeople;
    //补退费合计
    private String RefundMoney;
    //身份证号
    private String IDNo;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getRelationRisk() {
        return RelationRisk;
    }

    public void setRelationRisk(String relationRisk) {
        RelationRisk = relationRisk;
    }

    public String getAllGetMoney() {
        return AllGetMoney;
    }

    public void setAllGetMoney(String allGetMoney) {
        AllGetMoney = allGetMoney;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getNewBankName() {
        return NewBankName;
    }

    public void setNewBankName(String newBankName) {
        NewBankName = newBankName;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getRepremium() {
        return Repremium;
    }

    public void setRepremium(String repremium) {
        Repremium = repremium;
    }

    public String getUnrepreNumber() {
        return UnrepreNumber;
    }

    public void setUnrepreNumber(String unrepreNumber) {
        UnrepreNumber = unrepreNumber;
    }

    public String getAccontBalance() {
        return AccontBalance;
    }

    public void setAccontBalance(String accontBalance) {
        AccontBalance = accontBalance;
    }

    public String getLeastprenumber() {
        return Leastprenumber;
    }

    public void setLeastprenumber(String leastprenumber) {
        Leastprenumber = leastprenumber;
    }

    public String getReprenumber() {
        return Reprenumber;
    }

    public void setReprenumber(String reprenumber) {
        Reprenumber = reprenumber;
    }

    public String getPayPrem() {
        return PayPrem;
    }

    public void setPayPrem(String payPrem) {
        PayPrem = payPrem;
    }

    public String getPayPremInterest() {
        return PayPremInterest;
    }

    public void setPayPremInterest(String payPremInterest) {
        PayPremInterest = payPremInterest;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getRefundPeople() {
        return RefundPeople;
    }

    public void setRefundPeople(String refundPeople) {
        RefundPeople = refundPeople;
    }

    public String getRefundMoney() {
        return RefundMoney;
    }

    public void setRefundMoney(String refundMoney) {
        RefundMoney = refundMoney;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    @Override

    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPayMentDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", RelationRisk='" + RelationRisk + '\'' +
                ", AllGetMoney='" + AllGetMoney + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", NewBankName='" + NewBankName + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", Repremium='" + Repremium + '\'' +
                ", UnrepreNumber='" + UnrepreNumber + '\'' +
                ", AccontBalance='" + AccontBalance + '\'' +
                ", Leastprenumber='" + Leastprenumber + '\'' +
                ", Reprenumber='" + Reprenumber + '\'' +
                ", PayPrem='" + PayPrem + '\'' +
                ", PayPremInterest='" + PayPremInterest + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", RefundPeople='" + RefundPeople + '\'' +
                ", RefundMoney='" + RefundMoney + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                '}';
    }
}