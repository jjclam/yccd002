package com.sinosoft.lis.pos.entity.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PathInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

;

/**
 * 影像处理PC003.
 */
@Relation(transID = "PC003")
public class RespImageProcessingPojo implements Pojo, Body, Serializable {
    //流水号
    private String SerialNo;
    private String ABCFlag;
    private String AppendMessage;
    private PathInfosPojo PathInfosPojo;
    //业务类型
    private String ImgType;


    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getABCFlag() {
        return ABCFlag;
    }

    public void setABCFlag(String ABCFlag) {
        this.ABCFlag = ABCFlag;
    }

    public String getAppendMessage() {
        return AppendMessage;
    }

    public void setAppendMessage(String appendMessage) {
        AppendMessage = appendMessage;
    }

    public com.sinosoft.lis.pos.entity.PathInfosPojo getPathInfosPojo() {
        return PathInfosPojo;
    }

    @JSONField(name = "pathInfos")
    public void setPathInfosPojo(com.sinosoft.lis.pos.entity.PathInfosPojo pathInfosPojo) {
        PathInfosPojo = pathInfosPojo;
    }

    public String getImgType() {
        return ImgType;
    }

    public void setImgType(String imgType) {
        ImgType = imgType;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespImageProcessingPojo{" +
                "SerialNo='" + SerialNo + '\'' +
                ", ABCFlag='" + ABCFlag + '\'' +
                ", AppendMessage='" + AppendMessage + '\'' +
                ", PathInfosPojo=" + PathInfosPojo +
                ", ImgType='" + ImgType + '\'' +
                '}';
    }
}
