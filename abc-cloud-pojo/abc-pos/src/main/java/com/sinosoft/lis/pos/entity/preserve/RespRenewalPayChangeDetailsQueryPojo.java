package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4002查询保单列表POS062 4007官网保单保全明细查询
 */
@Relation(transID = {"POS062", "POS113"})
public class RespRenewalPayChangeDetailsQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //总保费
    private String SumPrem;
    //生效日期
    private String ValiDate;
    //缴费方式
    private String PayMode;
    //账户名称
    private String BankName;
    //开户银行
    private String BankAccName;
    //银行账号
    private String BankAccNo;
    //开户银行
    private String BankCode;
    //主险名称
    private String MainRiskName;
    //缴费形式
    private String OldPayMode;
    //账户名称
    private String OldBankName;
    //开户银行
    private String OldBankAccNo;
    //银行账号
    private String OldBankAccName;

    @Override
    public String toString() {
        return "RespRenewalPayChangeDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", SumPrem='" + SumPrem + '\'' +
                ", ValiDate='" + ValiDate + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", OldPayMode='" + OldPayMode + '\'' +
                ", OldBankName='" + OldBankName + '\'' +
                ", OldBankAccNo='" + OldBankAccNo + '\'' +
                ", OldBankAccName='" + OldBankAccName + '\'' +
                '}';
    }

    public String getOldPayMode() {
        return OldPayMode;
    }

    public void setOldPayMode(String oldPayMode) {
        OldPayMode = oldPayMode;
    }

    public String getOldBankName() {
        return OldBankName;
    }

    public void setOldBankName(String oldBankName) {
        OldBankName = oldBankName;
    }

    public String getOldBankAccNo() {
        return OldBankAccNo;
    }

    public void setOldBankAccNo(String oldBankAccNo) {
        OldBankAccNo = oldBankAccNo;
    }

    public String getOldBankAccName() {
        return OldBankAccName;
    }

    public void setOldBankAccName(String oldBankAccName) {
        OldBankAccName = oldBankAccName;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(String sumPrem) {
        SumPrem = sumPrem;
    }

    public String getValiDate() {
        return ValiDate;
    }

    public void setValiDate(String valiDate) {
        ValiDate = valiDate;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
