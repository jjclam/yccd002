package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

public class ManageComsPojo implements Body, Serializable {
    private List<ManageComPojo> ManageComPojo;

    public List<com.sinosoft.lis.pos.entity.ManageComPojo> getManageComPojo() {
        return ManageComPojo;
    }

    @JSONField(name = "manageCom")
    public void setManageComPojo(List<com.sinosoft.lis.pos.entity.ManageComPojo> manageComPojo) {
        ManageComPojo = manageComPojo;
    }

    @Override
    public String toString() {
        return "ManageComsPojo{" +
                "ManageComPojo=" + ManageComPojo +
                '}';
    }
}