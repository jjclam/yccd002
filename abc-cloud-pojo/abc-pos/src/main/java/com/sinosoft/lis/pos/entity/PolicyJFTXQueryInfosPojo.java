package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class PolicyJFTXQueryInfosPojo implements Body,Serializable{
    private List<PolicyJFTXQueryInfoPojo> PolicyJFTXQueryInfoPojo;

    @JSONField(name="policyJFTXQueryInfo")
    public List<com.sinosoft.lis.pos.entity.PolicyJFTXQueryInfoPojo> getPolicyJFTXQueryInfoPojo() {
        return PolicyJFTXQueryInfoPojo;
    }
    @JSONField(name="policyJFTXQueryInfo")
    public void setPolicyJFTXQueryInfoPojo(List<com.sinosoft.lis.pos.entity.PolicyJFTXQueryInfoPojo> policyJFTXQueryInfoPojo) {
        PolicyJFTXQueryInfoPojo = policyJFTXQueryInfoPojo;
    }

    @Override
    public String toString() {
        return "PolicyJFTXQueryInfosPojo{" +
                "PolicyJFTXQueryInfoPojo=" + PolicyJFTXQueryInfoPojo +
                '}';
    }
}