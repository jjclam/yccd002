package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4003保费自垫清偿详情查询POS009.4007保费自垫清偿POS097
 */
@Relation(transID = {"POS009","POS097"})
public class RespPremDictionaryClearDetailsQueryPojo implements Pojo,Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //涉及险种
    private String RelationRisk;
    //开户行
    private String BankCode;
    //开户银行
    private String NewBankName;
    //银行帐号
    private String NewBankAccNo;
    //户名
    private String NewBankAccName;
    //手机号码
    private String MobilePhone;
    //手机号码
    private String AdcancePrincipal;
    //垫交利息
    private String AdcanceInterest;
    //垫交本金利息和
    private String AdcancePAI;
    //自垫日期
    private String AdcanceDate;
    //补退费金额合计
    private String GetMoney;
    //清偿日期
    private String PayDate;
    //收付费方式
    private String PayMode;
    //清偿金额合计(含税))
    private String PayOffMoneyTax;
    //清偿金额合计(不含税))
    private String PayOffMoney;
    //补退费领取人
    private String RefundPeople;
    //身份证号
    private String IDNo;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;
    //补交保费
    private String PayPrem;
    //补交保费利息
    private String PayPremInterest;

    @Override
    public String toString() {
        return "RespPremDictionaryClearDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", RelationRisk='" + RelationRisk + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", NewBankName='" + NewBankName + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", AdcancePrincipal='" + AdcancePrincipal + '\'' +
                ", AdcanceInterest='" + AdcanceInterest + '\'' +
                ", AdcancePAI='" + AdcancePAI + '\'' +
                ", AdcanceDate='" + AdcanceDate + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", PayOffMoneyTax='" + PayOffMoneyTax + '\'' +
                ", PayOffMoney='" + PayOffMoney + '\'' +
                ", RefundPeople='" + RefundPeople + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                ", PayPrem='" + PayPrem + '\'' +
                ", PayPremInterest='" + PayPremInterest + '\'' +
                '}';
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getRelationRisk() {
        return RelationRisk;
    }

    public void setRelationRisk(String relationRisk) {
        RelationRisk = relationRisk;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getNewBankName() {
        return NewBankName;
    }

    public void setNewBankName(String newBankName) {
        NewBankName = newBankName;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getAdcancePrincipal() {
        return AdcancePrincipal;
    }

    public void setAdcancePrincipal(String adcancePrincipal) {
        AdcancePrincipal = adcancePrincipal;
    }

    public String getAdcanceInterest() {
        return AdcanceInterest;
    }

    public void setAdcanceInterest(String adcanceInterest) {
        AdcanceInterest = adcanceInterest;
    }

    public String getAdcancePAI() {
        return AdcancePAI;
    }

    public void setAdcancePAI(String adcancePAI) {
        AdcancePAI = adcancePAI;
    }

    public String getAdcanceDate() {
        return AdcanceDate;
    }

    public void setAdcanceDate(String adcanceDate) {
        AdcanceDate = adcanceDate;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getPayOffMoneyTax() {
        return PayOffMoneyTax;
    }

    public void setPayOffMoneyTax(String payOffMoneyTax) {
        PayOffMoneyTax = payOffMoneyTax;
    }

    public String getPayOffMoney() {
        return PayOffMoney;
    }

    public void setPayOffMoney(String payOffMoney) {
        PayOffMoney = payOffMoney;
    }

    public String getRefundPeople() {
        return RefundPeople;
    }

    public void setRefundPeople(String refundPeople) {
        RefundPeople = refundPeople;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    public String getPayPrem() {
        return PayPrem;
    }

    public void setPayPrem(String payPrem) {
        PayPrem = payPrem;
    }

    public String getPayPremInterest() {
        return PayPremInterest;
    }

    public void setPayPremInterest(String payPremInterest) {
        PayPremInterest = payPremInterest;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

}