package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class IdentityVerifyInfosPojo implements  Body,Serializable{
    private List<com.sinosoft.lis.pos.entity.IdentityVerifyInfoPojo> IdentityVerifyInfoPojo;

    @JSONField(name = "identityVerifyInfo")
    public List<com.sinosoft.lis.pos.entity.IdentityVerifyInfoPojo> getIdentityVerifyInfoPojo() {
        return IdentityVerifyInfoPojo;
    }
    @JSONField(name = "identityVerifyInfo")
    public void setIdentityVerifyInfoPojo(List<com.sinosoft.lis.pos.entity.IdentityVerifyInfoPojo> identityVerifyInfoPojo) {
        IdentityVerifyInfoPojo = identityVerifyInfoPojo;
    }

    @Override
    public String toString() {
        return "IdentityVerifyInfosPojo{" +
                "IdentityVerifyInfoPojo=" + IdentityVerifyInfoPojo +
                '}';
    }
}
