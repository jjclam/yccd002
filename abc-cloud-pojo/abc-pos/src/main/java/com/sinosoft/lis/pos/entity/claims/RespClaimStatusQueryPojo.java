package com.sinosoft.lis.pos.entity.claims;


import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.WecLPSyncImagesResPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 理赔影像状态查询CLM004.
 */
@Relation(transID = "CLM004")
public class RespClaimStatusQueryPojo implements Pojo, Body, Serializable {
    private WecLPSyncImagesResPojo WecLPSyncImagesResPojo;

    public com.sinosoft.lis.pos.entity.WecLPSyncImagesResPojo getWecLPSyncImagesResPojo() {
        return WecLPSyncImagesResPojo;
    }

    @JSONField(name = "wecLPSyncImagesRes")
    public void setWecLPSyncImagesResPojo(com.sinosoft.lis.pos.entity.WecLPSyncImagesResPojo wecLPSyncImagesResPojo) {
        WecLPSyncImagesResPojo = wecLPSyncImagesResPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespClaimStatusQueryPojo{" +
                "WecLPSyncImagesResPojo=" + WecLPSyncImagesResPojo +
                '}';
    }
}