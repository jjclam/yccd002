package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

;

public class XHPolicysPojo implements Body, Serializable {
    private List<XHPolicyPojo> XHPolicyPojo;

    @Override
    public String toString() {
        return "XHPolicysPojo{" +
                "XHPolicyPojo=" + XHPolicyPojo +
                '}';
    }

    @JSONField(name = "xHPolicy")
    public List<XHPolicyPojo> getXHPolicyPojo() {
        return XHPolicyPojo;
    }

    public void setXHPolicyPojo(List<XHPolicyPojo> XHPolicyPojo) {
        this.XHPolicyPojo = XHPolicyPojo;
    }

}
