package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;


/**
 * 保单补发详情查询POS002.
 */
@Relation(transID = "POS002")
public class RespPolicySupplyDetailsQueryPojo implements Pojo,Body,Serializable{
    //保单号
    private String ContNo;
    //投保人名称
    private String AppntName;
    //交费金额
    private String GetMoney;
    //补发原因
    private String CauseOfReissue;
    //开户行
    private String BankName;
    //帐号
    private String BankAccNo;
    //账户名
    private String BankAccName;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getCauseOfReissue() {
        return CauseOfReissue;
    }

    public void setCauseOfReissue(String causeOfReissue) {
        CauseOfReissue = causeOfReissue;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPolicySupplyDetailsQueryPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", CauseOfReissue='" + CauseOfReissue + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                '}';
    }
}