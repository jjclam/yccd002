/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.pos.entity.group;


import com.sinosoft.utility.Pojo;

import java.io.Serializable;

public class BQResultPojo implements Pojo,Serializable{
    // @Field
    /**
     * 旧保单号
     */
    private String PolicyNo;
    /**
     * 险种名称
     */
    private String RiskName;
    /**
     * 转保金额
     */
    private Double TransferAmt;
    /**
     * 保费(应缴金额)
     */
    private Double Prem;
    /**
     * 新单保费
     */
    private Double PolicyAmt;
    private Double PayTBFee;
    private String ValidDate;
    private String ExpireDate;
    private String BusinessType;

    /**
     * 险种代码
     */
    private String RiskCode;
    /**
     * 保单状态
     */
    private String PolicyStatus;
    /**
     * 保全申请状态
     */
    private String BQStatus;
    /**
     * 申请日期
     */
    private String ApplyDate;
    /**
     * 业务类别
     */
    private String BusinType;

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getBQStatus() {
        return BQStatus;
    }

    public void setBQStatus(String BQStatus) {
        this.BQStatus = BQStatus;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getBusinType() {
        return BusinType;
    }

    public void setBusinType(String businType) {
        BusinType = businType;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }

    public void setPolicyNo(String policyNo) {
        PolicyNo = policyNo;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public Double getTransferAmt() {
        return TransferAmt;
    }

    public void setTransferAmt(Double transferAmt) {
        TransferAmt = transferAmt;
    }

    public Double getPrem() {
        return Prem;
    }

    public void setPrem(Double prem) {
        Prem = prem;
    }

    public Double getPolicyAmt() {
        return PolicyAmt;
    }

    public void setPolicyAmt(Double policyAmt) {
        PolicyAmt = policyAmt;
    }

    public Double getPayTBFee() {
        return PayTBFee;
    }

    public void setPayTBFee(Double payTBFee) {
        PayTBFee = payTBFee;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    public String getExpireDate() {
        return ExpireDate;
    }

    public void setExpireDate(String expireDate) {
        ExpireDate = expireDate;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    @Override
    public String toString() {
        return "BQResultPojo{" +
                "PolicyNo='" + PolicyNo + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", TransferAmt=" + TransferAmt +
                ", Prem=" + Prem +
                ", PolicyAmt=" + PolicyAmt +
                ", PayTBFee=" + PayTBFee +
                ", ValidDate='" + ValidDate + '\'' +
                ", ExpireDate='" + ExpireDate + '\'' +
                ", BusinessType='" + BusinessType + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", PolicyStatus='" + PolicyStatus + '\'' +
                ", BQStatus='" + BQStatus + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", BusinType='" + BusinType + '\'' +
                '}';
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
