package com.sinosoft.lis.pos.entity;

import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;

public class PathInfoPojo implements Body, Serializable {
    //影像处理
    private String ImagePath;

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    @Override
    public String toString() {
        return "PathInfoPojo{" +
                "ImagePath='" + ImagePath + '\'' +
                '}';
    }
}