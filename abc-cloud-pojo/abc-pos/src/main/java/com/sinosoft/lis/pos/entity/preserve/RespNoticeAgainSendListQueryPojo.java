package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4008查询保单列表
 */
@Relation(transID = "POS060")
public class RespNoticeAgainSendListQueryPojo implements Pojo,Body,Serializable{
    //文本信息
    private com.sinosoft.lis.pos.entity.TextInfosPojo TextInfosPojo;

    @Override
    public String toString() {
        return "RespNoticeAgainSendListQueryPojo{" +
                "TextInfosPojo=" + TextInfosPojo +
                '}';
    }

    public com.sinosoft.lis.pos.entity.TextInfosPojo getTextInfosPojo() {
        return TextInfosPojo;
    }
    @JSONField(name = "textInfos")
    public void setTextInfosPojo(com.sinosoft.lis.pos.entity.TextInfosPojo textInfosPojo) {
        TextInfosPojo = textInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
