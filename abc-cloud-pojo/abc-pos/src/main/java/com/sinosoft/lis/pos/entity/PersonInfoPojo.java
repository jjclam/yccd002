package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * Created by Administrator on 2018/11/6.
 */
public class PersonInfoPojo implements Body,Serializable{
    //补退费领取人
    private String PersonName;
    //补退费领取人证件类型
    private String PersonIDType;
    //补退费领取人证件号码
    private String PersonIDNO;
    //补退费领取人证件有效期
    private String PersonIDValiDate;

    @Override
    public String toString() {
        return "PersonInfoPojo{" +
                "PersonName='" + PersonName + '\'' +
                ", PersonIDType='" + PersonIDType + '\'' +
                ", PersonIDNO='" + PersonIDNO + '\'' +
                ", PersonIDValiDate='" + PersonIDValiDate + '\'' +
                '}';
    }

    public String getPersonName() {
        return PersonName;
    }

    public void setPersonName(String personName) {
        PersonName = personName;
    }

    public String getPersonIDType() {
        return PersonIDType;
    }

    public void setPersonIDType(String personIDType) {
        PersonIDType = personIDType;
    }

    public String getPersonIDNO() {
        return PersonIDNO;
    }

    public void setPersonIDNO(String personIDNO) {
        PersonIDNO = personIDNO;
    }

    public String getPersonIDValiDate() {
        return PersonIDValiDate;
    }

    public void setPersonIDValiDate(String personIDValiDate) {
        PersonIDValiDate = personIDValiDate;
    }


}
