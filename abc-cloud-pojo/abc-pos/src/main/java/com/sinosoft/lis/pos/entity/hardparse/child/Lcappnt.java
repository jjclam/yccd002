package com.sinosoft.lis.pos.entity.hardparse.child;

public class Lcappnt {
    //投保人姓名
    private String Name;
    //投保人性别
    private String Sex;
    //投保人证件号
    private String IdNo;
    //投保人客户号
    private String CustomerNo;

    @Override
    public String toString() {
        return "Lcappnt{" +
                "Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", IdNo='" + IdNo + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                '}';
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getIdNo() {
        return IdNo;
    }

    public void setIdNo(String idNo) {
        IdNo = idNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }
}
