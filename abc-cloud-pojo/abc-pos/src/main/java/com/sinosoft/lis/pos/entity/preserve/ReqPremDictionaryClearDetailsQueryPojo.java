package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4003保费自垫清偿详情查询POS009.4007保费自垫清偿POS097
 */
public class ReqPremDictionaryClearDetailsQueryPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //批单号
    private String EdorNo;
    //保单号
    private String ContNo;

    @Override
    public String toString() {
        return "ReqPremDictionaryClearDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

}