package com.sinosoft.lis.pos.entity.preserve;


import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PersonInfoPojo;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;


/**
 * 4004保单贷款清偿申请POS006.
 */
public class ReqPolicyLoanClearApplyPojo implements ReqBody, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //是否还息不还本
    private String IsInterest;
    //清偿金额
    private String PayOffMoney;
    //申请人姓名
    private String CustomerName;
    //申请人身份证号码
    private String CustomerIdNo;
    //申请日期
    private String ApplyDate;
    //是否为美国纳税义务的个人
    private String TinFlag;
    //美国纳税人识别号
    private String TinNo;
    //收付费方式
    private String PayMode;
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    private PersonInfoPojo PersonInfoPojo;
    private com.sinosoft.lis.pos.entity.BankInfoPojo BankInfoPojo;
    //贷款本金
    private String LoanMoney;
    //清偿方式
    private String CompenMode;


    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getIsInterest() {
        return IsInterest;
    }

    public void setIsInterest(String isInterest) {
        IsInterest = isInterest;
    }

    public String getPayOffMoney() {
        return PayOffMoney;
    }

    public void setPayOffMoney(String payOffMoney) {
        PayOffMoney = payOffMoney;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIdNo() {
        return CustomerIdNo;
    }

    public void setCustomerIdNo(String customerIdNo) {
        CustomerIdNo = customerIdNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getTinFlag() {
        return TinFlag;
    }

    public void setTinFlag(String tinFlag) {
        TinFlag = tinFlag;
    }

    public String getTinNo() {
        return TinNo;
    }

    public void setTinNo(String tinNo) {
        TinNo = tinNo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    @JSONField(name = "edorAppInfo")
    public com.sinosoft.lis.pos.entity.EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(com.sinosoft.lis.pos.entity.EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }

    @JSONField(name = "personInfo")
    public com.sinosoft.lis.pos.entity.PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(com.sinosoft.lis.pos.entity.PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }

    @JSONField(name = "bankInfo")
    public com.sinosoft.lis.pos.entity.BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(com.sinosoft.lis.pos.entity.BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }

    public String getCompenMode() {
        return CompenMode;
    }

    public void setCompenMode(String compenMode) {
        CompenMode = compenMode;
    }

    @Override
    public String toString() {
        return "ReqPolicyLoanClearApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", IsInterest='" + IsInterest + '\'' +
                ", PayOffMoney='" + PayOffMoney + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIdNo='" + CustomerIdNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", TinFlag='" + TinFlag + '\'' +
                ", TinNo='" + TinNo + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                ", LoanMoney='" + LoanMoney + '\'' +
                ", CompenMode='" + CompenMode + '\'' +
                '}';
    }
}