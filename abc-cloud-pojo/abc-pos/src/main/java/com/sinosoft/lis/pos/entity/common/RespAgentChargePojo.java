package com.sinosoft.lis.pos.entity.common;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 代理人收费接口PC008.
 */
@Relation(transID = "PC008")
public class RespAgentChargePojo implements Pojo, Body, Serializable {
    //成功失败标志
    private String RetCode;
    //提示信息
    private String RetMsg;

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    public String getRetCode() {
        return RetCode;
    }

    public void setRetCode(String retCode) {
        RetCode = retCode;
    }

    public String getRetMsg() {
        return RetMsg;
    }

    public void setRetMsg(String retMsg) {
        RetMsg = retMsg;
    }

    @Override
    public String toString() {
        return "RespAgentChargePojo{" +
                "RetCode='" + RetCode + '\'' +
                ", RetMsg='" + RetMsg + '\'' +
                '}';
    }
}