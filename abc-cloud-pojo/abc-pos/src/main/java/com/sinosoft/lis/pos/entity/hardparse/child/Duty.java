package com.sinosoft.lis.pos.entity.hardparse.child;

public class Duty {
    //终交年龄年期
    private String PayEndYear;
    //终交年龄年期标志
    private String PayEndYearFlag;
    //保险年龄年期
    private String InsuYear;
    //保险年龄年期标志
    private String InsuYearFlag;

    @Override
    public String toString() {
        return "Duty{" +
                "PayEndYear='" + PayEndYear + '\'' +
                ", PayEndYearFlag='" + PayEndYearFlag + '\'' +
                ", InsuYear='" + InsuYear + '\'' +
                ", InsuYearFlag='" + InsuYearFlag + '\'' +
                '}';
    }

    public String getPayEndYear() {
        return PayEndYear;
    }

    public void setPayEndYear(String payEndYear) {
        PayEndYear = payEndYear;
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }

    public void setPayEndYearFlag(String payEndYearFlag) {
        PayEndYearFlag = payEndYearFlag;
    }

    public String getInsuYear() {
        return InsuYear;
    }

    public void setInsuYear(String insuYear) {
        InsuYear = insuYear;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        InsuYearFlag = insuYearFlag;
    }
}
