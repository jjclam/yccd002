package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 白名单查询NB004.
 */
public class ReqWhilteQueryPojo implements ReqBody, Body,Serializable{
    //姓名
    private String CustomerName;
    //性别
    private String CustomerSex;
    //生日
    private String CustomerBirthday;
    //证件类型
    private String CustomerIDType;
    //证件号码
    private String CustomerIDNo;
    //有效期
    private String ValidDate;
    //是否是钻石卡
    private String DiType;
    //银行账号
    private String BankAccNo;

    @Override
    public String toString() {
        return "ReqWhilteQueryPojo{" +
                "CustomerName='" + CustomerName + '\'' +
                ", CustomerSex='" + CustomerSex + '\'' +
                ", CustomerBirthday='" + CustomerBirthday + '\'' +
                ", CustomerIDType='" + CustomerIDType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                ", DiType='" + DiType + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                '}';
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerSex() {
        return CustomerSex;
    }

    public void setCustomerSex(String customerSex) {
        CustomerSex = customerSex;
    }

    public String getCustomerBirthday() {
        return CustomerBirthday;
    }

    public void setCustomerBirthday(String customerBirthday) {
        CustomerBirthday = customerBirthday;
    }

    public String getCustomerIDType() {
        return CustomerIDType;
    }

    public void setCustomerIDType(String customerIDType) {
        CustomerIDType = customerIDType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    public String getDiType() {
        return DiType;
    }

    public void setDiType(String diType) {
        DiType = diType;
    }

}