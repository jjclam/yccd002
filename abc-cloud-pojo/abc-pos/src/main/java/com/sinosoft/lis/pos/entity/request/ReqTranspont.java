package com.sinosoft.lis.pos.entity.request;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by Administrator on 2018/11/6.
 */
public class ReqTranspont {

    private ReqRoot request;

    @JSONField(name = "Request")
    public ReqRoot getRequest() {
        return request;
    }

    public void setRequest(ReqRoot request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "ReqTranspont{" +
                "request=" + request +
                '}';
    }
}
