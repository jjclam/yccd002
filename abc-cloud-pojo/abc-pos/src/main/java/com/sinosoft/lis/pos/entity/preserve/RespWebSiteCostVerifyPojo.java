package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 退保费用核算 微信、官网
 */

@Relation(transID = "POS127")
public class RespWebSiteCostVerifyPojo implements Pojo, Body, Serializable {
    //银行订单号
    private String ABCOrderId;
    //保单账户现金价值
    private String PolicyValue;
    //退保金额
    private String WithdrawMoney;
    //实际可支取金额
    private String AvailableMoney;
    // 是否成功
    private String IsSuccess;
    //确认告知信息或失败的信息
    private String Message;
    //是否为退保
    private String IsCancelPolicy;


    @Override

    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public String getWithdrawMoney() {
        return WithdrawMoney;
    }

    public void setWithdrawMoney(String withdrawMoney) {
        WithdrawMoney = withdrawMoney;
    }

    public String getAvailableMoney() {
        return AvailableMoney;
    }

    public void setAvailableMoney(String availableMoney) {
        AvailableMoney = availableMoney;
    }

    public String getIsSuccess() {
        return IsSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        IsSuccess = isSuccess;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getIsCancelPolicy() {
        return IsCancelPolicy;
    }

    public void setIsCancelPolicy(String isCancelPolicy) {
        IsCancelPolicy = isCancelPolicy;
    }

    @Override
    public String toString() {
        return "RespWebSiteCostVerifyPojo{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", WithdrawMoney='" + WithdrawMoney + '\'' +
                ", AvailableMoney='" + AvailableMoney + '\'' +
                ", IsSuccess='" + IsSuccess + '\'' +
                ", Message='" + Message + '\'' +
                ", IsCancelPolicy='" + IsCancelPolicy + '\'' +
                '}';
    }
}