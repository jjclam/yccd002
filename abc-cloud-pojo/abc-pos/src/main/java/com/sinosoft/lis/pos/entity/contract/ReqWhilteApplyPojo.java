package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

;


/**
 * 白名单提交NB005.
 */
public class ReqWhilteApplyPojo implements ReqBody, Body, Serializable {
    //姓名
    private String CustomerName;
    //性别
    private String CustomerSex;
    //生日
    private String CustomerBirthday;
    //证件类型
    private String CustomerIDType;
    //证件号码
    private String CustomerIDNo;
    //管理机构
    private String ManageCom;
    //有效期
    private String ValidDate;
    //新增银行账号
    private String BankAccNo;
    //是否是钻石卡
    private String DiType;
    //ocr识别
    private String IsOCR;
    //手动修改银行账号
    private String HandBank;


    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerSex() {
        return CustomerSex;
    }

    public void setCustomerSex(String customerSex) {
        CustomerSex = customerSex;
    }

    public String getCustomerBirthday() {
        return CustomerBirthday;
    }

    public void setCustomerBirthday(String customerBirthday) {
        CustomerBirthday = customerBirthday;
    }

    public String getCustomerIDType() {
        return CustomerIDType;
    }

    public void setCustomerIDType(String customerIDType) {
        CustomerIDType = customerIDType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getDiType() {
        return DiType;
    }

    public void setDiType(String diType) {
        DiType = diType;
    }

    public String getIsOCR() {
        return IsOCR;
    }

    public void setIsOCR(String isOCR) {
        IsOCR = isOCR;
    }

    public String getHandBank() {
        return HandBank;
    }

    public void setHandBank(String handBank) {
        HandBank = handBank;
    }

    @Override
    public String toString() {
        return "ReqWhilteApplyPojo{" +
                "CustomerName='" + CustomerName + '\'' +
                ", CustomerSex='" + CustomerSex + '\'' +
                ", CustomerBirthday='" + CustomerBirthday + '\'' +
                ", CustomerIDType='" + CustomerIDType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", DiType='" + DiType + '\'' +
                ", IsOCR='" + IsOCR + '\'' +
                ", HandBank='" + HandBank + '\'' +
                '}';
    }
}