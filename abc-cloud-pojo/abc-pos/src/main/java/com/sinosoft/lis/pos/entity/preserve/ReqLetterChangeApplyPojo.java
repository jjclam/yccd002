package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.BankInfoPojo;
import com.sinosoft.lis.pos.entity.EdorAppInfoPojo;
import com.sinosoft.lis.pos.entity.PersonInfoPojo;
import com.sinosoft.lis.pos.entity.XHPolicysPojo;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 信函接受方式变更申请POS068
 */
public class ReqLetterChangeApplyPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //客户号
    private String CustomerNo;
    //信函变更方式申请
    private com.sinosoft.lis.pos.entity.XHPolicysPojo XHPolicysPojo;
    //申请人信息
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    //领取人信息
    private com.sinosoft.lis.pos.entity.PersonInfoPojo PersonInfoPojo;
    //银行信息
    private com.sinosoft.lis.pos.entity.BankInfoPojo BankInfoPojo;
    //收付费方式
    private String PayMode;

    @Override
    public String toString() {
        return "ReqLetterChangeApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", XHPolicysPojo=" + XHPolicysPojo +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                ", PayMode='" + PayMode + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }
    @JSONField(name = "xHPolicys")
    public XHPolicysPojo getXHPolicysPojo() {
        return XHPolicysPojo;
    }

    public void setXHPolicysPojo(XHPolicysPojo XHPolicysPojo) {
        this.XHPolicysPojo = XHPolicysPojo;
    }
    @JSONField(name = "edorAppInfo")
    public EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }
    @JSONField(name = "personInfo")
    public PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }
    @JSONField(name = "bankInfo")
    public BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

}
