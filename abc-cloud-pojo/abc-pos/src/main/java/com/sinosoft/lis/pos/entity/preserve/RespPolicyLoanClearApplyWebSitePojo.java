package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 4010保单贷款清偿申请POS096
 */
@Relation(transID = "POS096")
public class RespPolicyLoanClearApplyWebSitePojo implements Pojo, Body, Serializable {
    private String EdorType;
    private String Principal;
    private String ContNo;
    private String PayOffMoney;
    private String LoanInterest;
    private String OverRate;
    private String OverInterest;

    @Override
    public String toString() {
        return "RespPolicyLoanClearApplyWebSitePojo{" +
                "EdorType='" + EdorType + '\'' +
                ", Principal='" + Principal + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PayOffMoney='" + PayOffMoney + '\'' +
                ", LoanInterest='" + LoanInterest + '\'' +
                ", OverRate='" + OverRate + '\'' +
                ", OverInterest='" + OverInterest + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getPrincipal() {
        return Principal;
    }

    public void setPrincipal(String principal) {
        Principal = principal;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPayOffMoney() {
        return PayOffMoney;
    }

    public void setPayOffMoney(String payOffMoney) {
        PayOffMoney = payOffMoney;
    }

    public String getLoanInterest() {
        return LoanInterest;
    }

    public void setLoanInterest(String loanInterest) {
        LoanInterest = loanInterest;
    }

    public String getOverRate() {
        return OverRate;
    }

    public void setOverRate(String overRate) {
        OverRate = overRate;
    }

    public String getOverInterest() {
        return OverInterest;
    }

    public void setOverInterest(String overInterest) {
        OverInterest = overInterest;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
