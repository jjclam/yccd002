package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 退保申请 微信、官网
 */
public class ReqWebSiteSurrenderApplyPojo implements ReqBody, Body, Serializable {
    //银行订单号
    private String ABCOrderId;
    //银行退保订单号
    private String ABCRefundId;
    //保单号
    private String ContNo;
    //保单账户现金价值
    private String PolicyValue;
    //退保金额
    private String WithdrawMoney;
    //实际可支取金额
    private String AvailableMoney;
    //是否为退保
    private String IsCancelPolicy;
    //是否全部领取
    private String IsWithdrawAll;

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getABCRefundId() {
        return ABCRefundId;
    }

    public void setABCRefundId(String ABCRefundId) {
        this.ABCRefundId = ABCRefundId;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public String getWithdrawMoney() {
        return WithdrawMoney;
    }

    public void setWithdrawMoney(String withdrawMoney) {
        WithdrawMoney = withdrawMoney;
    }

    public String getAvailableMoney() {
        return AvailableMoney;
    }

    public void setAvailableMoney(String availableMoney) {
        AvailableMoney = availableMoney;
    }

    public String getIsCancelPolicy() {
        return IsCancelPolicy;
    }

    public void setIsCancelPolicy(String isCancelPolicy) {
        IsCancelPolicy = isCancelPolicy;
    }

    public String getIsWithdrawAll() {
        return IsWithdrawAll;
    }

    public void setIsWithdrawAll(String isWithdrawAll) {
        IsWithdrawAll = isWithdrawAll;
    }

    @Override
    public String toString() {
        return "ReqWebSiteSurrenderApplyPojo{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", ABCRefundId='" + ABCRefundId + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", WithdrawMoney='" + WithdrawMoney + '\'' +
                ", AvailableMoney='" + AvailableMoney + '\'' +
                ", IsCancelPolicy='" + IsCancelPolicy + '\'' +
                ", IsWithdrawAll='" + IsWithdrawAll + '\'' +
                '}';
    }
}