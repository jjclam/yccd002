package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

public class PolicyXHQueryInfosPojo  implements Body,Serializable {

    private List<PolicyXHQueryInfoPojo> PolicyXHQueryInfoPojo;
    public List<com.sinosoft.lis.pos.entity.PolicyXHQueryInfoPojo> getPolicyXHQueryInfoPojo() {
        return PolicyXHQueryInfoPojo;
    }

    @JSONField(name = "policyXHQueryInfo")
    public void setPolicyXHQueryInfoPojo(List<com.sinosoft.lis.pos.entity.PolicyXHQueryInfoPojo> policyXHQueryInfoPojo) {
        PolicyXHQueryInfoPojo = policyXHQueryInfoPojo;
    }

    @Override
    public String toString() {
        return "PolicyXHQueryInfosPojo{" +
                "PolicyXHQueryInfoPojo=" + PolicyXHQueryInfoPojo +
                '}';
    }
}