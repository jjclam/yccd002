package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class ReducedVolumes {
    private List<ReducedVolume> ReducedVolume;

    @Override
    public String toString() {
        return "ReducedVolumes{" +
                "ReducedVolume=" + ReducedVolume +
                '}';
    }

    public List<ReducedVolume> getReducedVolume() {
        return ReducedVolume;
    }

    public void setReducedVolume(List<ReducedVolume> reducedVolume) {
        ReducedVolume = reducedVolume;
    }
}
