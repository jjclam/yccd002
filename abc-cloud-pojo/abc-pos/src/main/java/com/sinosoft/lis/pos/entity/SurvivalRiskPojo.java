package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class SurvivalRiskPojo implements Body,Serializable{
    //派发日期
    private String SurvivalDate;
    //险种名称
    private String MainRiskName;
    //金额
    private String SurvivalAddMoney;
    //累计增加保额
    private String SumAddMoney;
    //累计增加保额
    private String AddAmntFlag;

    public String getSurvivalDate() {
        return SurvivalDate;
    }

    public void setSurvivalDate(String survivalDate) {
        SurvivalDate = survivalDate;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getSurvivalAddMoney() {
        return SurvivalAddMoney;
    }

    public void setSurvivalAddMoney(String survivalAddMoney) {
        SurvivalAddMoney = survivalAddMoney;
    }

    public String getSumAddMoney() {
        return SumAddMoney;
    }

    public void setSumAddMoney(String sumAddMoney) {
        SumAddMoney = sumAddMoney;
    }

    public String getAddAmntFlag() {
        return AddAmntFlag;
    }

    public void setAddAmntFlag(String addAmntFlag) {
        AddAmntFlag = addAmntFlag;
    }

    @Override
    public String toString() {
        return "SurvivalRiskPojo{" +
                "SurvivalDate='" + SurvivalDate + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", SurvivalAddMoney='" + SurvivalAddMoney + '\'' +
                ", SumAddMoney='" + SumAddMoney + '\'' +
                ", AddAmntFlag='" + AddAmntFlag + '\'' +
                '}';
    }
}