package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class ImgPathsPojo implements  Body,Serializable{
    //单证类型
    private String SubType;
    //影像路径
    private String ImgPath;
    //影像名称
    private String ImgName;

    @Override
    public String toString() {
        return "ImgPathsPojo{" +
                "SubType='" + SubType + '\'' +
                ", ImgPath='" + ImgPath + '\'' +
                ", ImgName='" + ImgName + '\'' +
                '}';
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String subType) {
        SubType = subType;
    }

    public String getImgPath() {
        return ImgPath;
    }

    public void setImgPath(String imgPath) {
        ImgPath = imgPath;
    }

    public String getImgName() {
        return ImgName;
    }

    public void setImgName(String imgName) {
        ImgName = imgName;
    }


}
