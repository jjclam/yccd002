package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PersonInfoPojo;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

;

/**
 * 红利领取方式变更保全申请POS022.
 */
public class ReqBonusGetModeApplyPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //申请日期
    private String ApplyDate;
    //收付费方式
    private String PayMode;
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    private PersonInfoPojo PersonInfoPojo;
    private com.sinosoft.lis.pos.entity.BankInfoPojo BankInfoPojo;
    //红利领取方式(一级)
    private String BonusGetMode;
    //红利领取方式(二级)
    private String BonusAccFlag;
    //红利领取方式(三级)
    private String BonusGetform;


    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }
    @JSONField(name = "edorAppInfo")
    public com.sinosoft.lis.pos.entity.EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(com.sinosoft.lis.pos.entity.EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }
    @JSONField(name = "personInfo")
    public com.sinosoft.lis.pos.entity.PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(com.sinosoft.lis.pos.entity.PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }
    @JSONField(name = "bankInfo")
    public com.sinosoft.lis.pos.entity.BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(com.sinosoft.lis.pos.entity.BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    public String getBonusGetMode() {
        return BonusGetMode;
    }

    public void setBonusGetMode(String bonusGetMode) {
        BonusGetMode = bonusGetMode;
    }

    public String getBonusAccFlag() {
        return BonusAccFlag;
    }

    public void setBonusAccFlag(String bonusAccFlag) {
        BonusAccFlag = bonusAccFlag;
    }

    public String getBonusGetform() {
        return BonusGetform;
    }

    public void setBonusGetform(String bonusGetform) {
        BonusGetform = bonusGetform;
    }

    @Override
    public String toString() {
        return "ReqBonusGetModeApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                ", BonusGetMode='" + BonusGetMode + '\'' +
                ", BonusAccFlag='" + BonusAccFlag + '\'' +
                ", BonusGetform='" + BonusGetform + '\'' +
                '}';
    }
}