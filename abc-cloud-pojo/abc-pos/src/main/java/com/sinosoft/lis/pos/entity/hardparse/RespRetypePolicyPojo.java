package com.sinosoft.lis.pos.entity.hardparse;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.hardparse.child.*;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 保单重打NB006
 */
@Relation(transID = "NB006")
public class RespRetypePolicyPojo implements Pojo,Body,Serializable{
    //保单号
    private String ContNo;
    //签约日期
    private String SignDate;
    //签约时间
    private String SignTime;
    //保险生效日期
    private String CValiDate;
    //管理机构编码
    private String ManageCom;
    //申请日期
    private String PolApplyDate;
    //特别约定
    private String SpecialInfo;
    //合同终止日期
    private String ContEndDate;
    //终止日期
    private String PayEndDate;
    //总保费
    private String SumPrem;
    //管理机构名称
    private String ComName;
    //地址
    private String ComLocation;
    //通讯邮编
    private String ZipCode;
    //保单责任
    private com.sinosoft.lis.pos.entity.hardparse.child.Dutys Dutys;
    //保单险种
    private Risks Risks;
    //保单投保人
    private Lcappnts lcappnts;
    //保单被保人
    private LCInsureds LCInsureds;
    //保单受益人
    private com.sinosoft.lis.pos.entity.hardparse.child.LCBnfs LCBnfs;
    //网点信息
    private com.sinosoft.lis.pos.entity.hardparse.child.ChnlExtends ChnlExtends;
    //销售人员信息
    private SaleManInfos SaleManInfos;
    //保单险种
    private com.sinosoft.lis.pos.entity.hardparse.child.ActuaryRisks ActuaryRisks;
    //机构
    private ComInfo ComInfo;

    @Override
    public String toString() {
        return "RespRetypePolicyPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", SignDate='" + SignDate + '\'' +
                ", SignTime='" + SignTime + '\'' +
                ", CValiDate='" + CValiDate + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", PolApplyDate='" + PolApplyDate + '\'' +
                ", SpecialInfo='" + SpecialInfo + '\'' +
                ", ContEndDate='" + ContEndDate + '\'' +
                ", PayEndDate='" + PayEndDate + '\'' +
                ", SumPrem='" + SumPrem + '\'' +
                ", ComName='" + ComName + '\'' +
                ", ComLocation='" + ComLocation + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Dutys=" + Dutys +
                ", Risks=" + Risks +
                ", lcappnts=" + lcappnts +
                ", LCInsureds=" + LCInsureds +
                ", LCBnfs=" + LCBnfs +
                ", ChnlExtends=" + ChnlExtends +
                ", SaleManInfos=" + SaleManInfos +
                ", ActuaryRisks=" + ActuaryRisks +
                ", ComInfo=" + ComInfo +
                '}';
    }

    public ComInfo getComInfo() {
        return ComInfo;
    }

    public void setComInfo(ComInfo comInfo) {
        ComInfo = comInfo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getSignTime() {
        return SignTime;
    }

    public void setSignTime(String signTime) {
        SignTime = signTime;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setCValiDate(String CValiDate) {
        this.CValiDate = CValiDate;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getPolApplyDate() {
        return PolApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        PolApplyDate = polApplyDate;
    }

    public String getSpecialInfo() {
        return SpecialInfo;
    }

    public void setSpecialInfo(String specialInfo) {
        SpecialInfo = specialInfo;
    }

    public String getContEndDate() {
        return ContEndDate;
    }

    public void setContEndDate(String contEndDate) {
        ContEndDate = contEndDate;
    }

    public String getPayEndDate() {
        return PayEndDate;
    }

    public void setPayEndDate(String payEndDate) {
        PayEndDate = payEndDate;
    }

    public String getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(String sumPrem) {
        SumPrem = sumPrem;
    }

    public String getComName() {
        return ComName;
    }

    public void setComName(String comName) {
        ComName = comName;
    }

    public String getComLocation() {
        return ComLocation;
    }

    public void setComLocation(String comLocation) {
        ComLocation = comLocation;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public com.sinosoft.lis.pos.entity.hardparse.child.Dutys getDutys() {
        return Dutys;
    }

    public void setDutys(com.sinosoft.lis.pos.entity.hardparse.child.Dutys dutys) {
        Dutys = dutys;
    }

    public com.sinosoft.lis.pos.entity.hardparse.child.Risks getRisks() {
        return Risks;
    }

    public void setRisks(com.sinosoft.lis.pos.entity.hardparse.child.Risks risks) {
        Risks = risks;
    }

    public Lcappnts getLcappnts() {
        return lcappnts;
    }

    public void setLcappnts(Lcappnts lcappnts) {
        this.lcappnts = lcappnts;
    }

    public com.sinosoft.lis.pos.entity.hardparse.child.LCInsureds getLCInsureds() {
        return LCInsureds;
    }
    @JSONField(name = "lCInsureds")
    public void setLCInsureds(com.sinosoft.lis.pos.entity.hardparse.child.LCInsureds LCInsureds) {
        this.LCInsureds = LCInsureds;
    }

    public com.sinosoft.lis.pos.entity.hardparse.child.LCBnfs getLCBnfs() {
        return LCBnfs;
    }
    @JSONField(name="lCBnfs")
    public void setLCBnfs(com.sinosoft.lis.pos.entity.hardparse.child.LCBnfs LCBnfs) {
        this.LCBnfs = LCBnfs;
    }

    public com.sinosoft.lis.pos.entity.hardparse.child.ChnlExtends getChnlExtends() {
        return ChnlExtends;
    }

    public void setChnlExtends(com.sinosoft.lis.pos.entity.hardparse.child.ChnlExtends chnlExtends) {
        ChnlExtends = chnlExtends;
    }

    public com.sinosoft.lis.pos.entity.hardparse.child.SaleManInfos getSaleManInfos() {
        return SaleManInfos;
    }

    public void setSaleManInfos(com.sinosoft.lis.pos.entity.hardparse.child.SaleManInfos saleManInfos) {
        SaleManInfos = saleManInfos;
    }

    public com.sinosoft.lis.pos.entity.hardparse.child.ActuaryRisks getActuaryRisks() {
        return ActuaryRisks;
    }

    public void setActuaryRisks(com.sinosoft.lis.pos.entity.hardparse.child.ActuaryRisks actuaryRisks) {
        ActuaryRisks = actuaryRisks;
    }

    public String getSignDate() {
        return SignDate;
    }

    public void setSignDate(String signDate) {
        SignDate = signDate;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
