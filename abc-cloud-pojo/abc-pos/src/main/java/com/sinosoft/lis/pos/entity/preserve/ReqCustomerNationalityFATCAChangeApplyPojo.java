package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 客户国籍及FATCA变更保全申请POS077
 */
public class ReqCustomerNationalityFATCAChangeApplyPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单查询信息
    private com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo PolicyQueryInfosPojo;

    @Override
    public String toString() {
        return "ReqCustomerNationalityFATCAChangeApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfosPojo=" + PolicyQueryInfosPojo +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }
    @JSONField(name = "policyQueryInfos")
    public com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo getPolicyQueryInfosPojo() {
        return PolicyQueryInfosPojo;
    }

    public void setPolicyQueryInfosPojo(com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo policyQueryInfosPojo) {
        PolicyQueryInfosPojo = policyQueryInfosPojo;
    }

}
