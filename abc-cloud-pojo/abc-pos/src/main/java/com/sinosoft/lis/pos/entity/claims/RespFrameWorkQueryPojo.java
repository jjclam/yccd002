package com.sinosoft.lis.pos.entity.claims;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.ManageComsPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 机构信息查询CLM003.
 */
@Relation(transID = "CLM003")
public class RespFrameWorkQueryPojo implements Pojo, Body, Serializable {
    private ManageComsPojo ManageComsPojo;

    public com.sinosoft.lis.pos.entity.ManageComsPojo getManageComsPojo() {
        return ManageComsPojo;
    }

    @JSONField(name = "manageComs")
    public void setManageComsPojo(com.sinosoft.lis.pos.entity.ManageComsPojo manageComsPojo) {
        ManageComsPojo = manageComsPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespFrameWorkQueryPojo{" +
                "ManageComsPojo=" + ManageComsPojo +
                '}';
    }
}