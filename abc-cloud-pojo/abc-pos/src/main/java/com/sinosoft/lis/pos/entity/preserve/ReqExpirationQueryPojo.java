package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;


/**
 * 4003保单生存金领取(AGSQ)查询和满期查询POS043  官网4007官网保单生存金(AG/SQ)领取明细查询 官网4003生存金领取查询.
 */
public class ReqExpirationQueryPojo implements ReqBody, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人证件类型
    private String IdKind;
    //投保人证件号码
    private String IdCode;
    //保单印刷号
    private String PrintCode;
    //申请人姓名
    private String ClientName;
    //领款人姓名
    private String PayeetName;
    //领款人证件类型
    private String PayeeIdKind;
    //领款人证件号码
    private String PyeeIdCode;
    //账（卡）号
    private String PayAcc;
    //银行编码
    private String BankCode;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //柜员代码
    private String TellerNo;
    //保全受理号
    private String EdorAcceptNo;
    //保费
    private String Prem;


    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getIdKind() {
        return IdKind;
    }

    public void setIdKind(String idKind) {
        IdKind = idKind;
    }

    public String getIdCode() {
        return IdCode;
    }

    public void setIdCode(String idCode) {
        IdCode = idCode;
    }

    public String getPrintCode() {
        return PrintCode;
    }

    public void setPrintCode(String printCode) {
        PrintCode = printCode;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public String getPayeetName() {
        return PayeetName;
    }

    public void setPayeetName(String payeetName) {
        PayeetName = payeetName;
    }

    public String getPayeeIdKind() {
        return PayeeIdKind;
    }

    public void setPayeeIdKind(String payeeIdKind) {
        PayeeIdKind = payeeIdKind;
    }

    public String getPyeeIdCode() {
        return PyeeIdCode;
    }

    public void setPyeeIdCode(String pyeeIdCode) {
        PyeeIdCode = pyeeIdCode;
    }

    public String getPayAcc() {
        return PayAcc;
    }

    public void setPayAcc(String payAcc) {
        PayAcc = payAcc;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    @Override
    public String toString() {
        return "ReqExpirationQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", IdKind='" + IdKind + '\'' +
                ", IdCode='" + IdCode + '\'' +
                ", PrintCode='" + PrintCode + '\'' +
                ", ClientName='" + ClientName + '\'' +
                ", PayeetName='" + PayeetName + '\'' +
                ", PayeeIdKind='" + PayeeIdKind + '\'' +
                ", PyeeIdCode='" + PyeeIdCode + '\'' +
                ", PayAcc='" + PayAcc + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", Prem='" + Prem + '\'' +
                '}';
    }
}
