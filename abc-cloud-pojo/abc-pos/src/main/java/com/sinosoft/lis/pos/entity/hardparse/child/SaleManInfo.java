package com.sinosoft.lis.pos.entity.hardparse.child;

public class SaleManInfo {
    //销售人员编码
    private String SaleCode;
    //销售人员姓名
    private String SaleName;
    //销售资格证号
    private String SaleCard;
    //销售人员组别
    private String SaleGroup;

    @Override
    public String toString() {
        return "SaleManInfo{" +
                "SaleCode='" + SaleCode + '\'' +
                ", SaleName='" + SaleName + '\'' +
                ", SaleCard='" + SaleCard + '\'' +
                ", SaleGroup='" + SaleGroup + '\'' +
                '}';
    }

    public String getSaleCode() {
        return SaleCode;
    }

    public void setSaleCode(String saleCode) {
        SaleCode = saleCode;
    }

    public String getSaleName() {
        return SaleName;
    }

    public void setSaleName(String saleName) {
        SaleName = saleName;
    }

    public String getSaleCard() {
        return SaleCard;
    }

    public void setSaleCard(String saleCard) {
        SaleCard = saleCard;
    }

    public String getSaleGroup() {
        return SaleGroup;
    }

    public void setSaleGroup(String saleGroup) {
        SaleGroup = saleGroup;
    }
}
