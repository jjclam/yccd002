package com.sinosoft.lis.pos.entity;

import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;

/**
 * @program: abc-cloud-pojo
 * @description:
 * @author: BaoYongmeng
 * @create: 2019-03-07 17:29
 **/
public class AgentPojo  implements Body, Serializable {

    //内部销售人员编码
    private String AgentCode;
    //网点代码
    private String ManageCom;
    //柜员代码
    private String TellerNo;
    //银行代码
    private String BankCode;
    //省市代码
    private String ZoneNo;
    //网点号
    private String BrNo;

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBrNo() {
        return BrNo;
    }

    public void setBrNo(String brNo) {
        BrNo = brNo;
    }

    @Override
    public String toString() {
        return "AgentPojo{" +
                "AgentCode='" + AgentCode + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BrNo='" + BrNo + '\'' +
                '}';
    }
}
