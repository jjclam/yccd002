package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class PolicyQueryComInfosPojo implements Body,Serializable{
    //保单来源机构信息查询
    private List<PolicyQueryComInfoPojo> PolicyQueryComInfoPojo;

    @Override
    public String toString() {
        return "PolicyQueryComInfosPojo{" +
                "PolicyQueryComInfoPojo=" + PolicyQueryComInfoPojo +
                '}';
    }

    @JSONField(name = "policyQueryComInfo")
    public List<PolicyQueryComInfoPojo> getPolicyQueryComInfoPojo() {
        return PolicyQueryComInfoPojo;
    }
    @JSONField(name = "policyQueryComInfo")
    public void setPolicyQueryComInfoPojo(List<PolicyQueryComInfoPojo> policyQueryComInfoPojo) {
        PolicyQueryComInfoPojo = policyQueryComInfoPojo;
    }


}
