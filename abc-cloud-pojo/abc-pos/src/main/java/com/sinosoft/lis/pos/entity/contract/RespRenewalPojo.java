package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * @program: abc-cloud-pojo
 * @description: 微信续保查询
 * @author: BaoYongmeng
 * @create: 2019-03-20 17:51
 **/
@Relation(transID = "NB013")
public class RespRenewalPojo implements Pojo, Body, Serializable {

    //可续保标志
    private String RenewFlag;
    //产品名称
    private String ProuctName;
    //投保人
    private String AppntName;
    //被保人
    private String InsuRedName;
    //投被保人关系代码
    private String RelationToAppnt;
    //被保人关系代码
    private String RelaToInsured;
    //关系名称
    private String RelationName;
    //被保人是否有社保
    private String HealthFlag;
    //原保险合同号码
    private String ContNo;
    //原保险合同保险期间
    private String InsuYear;
    //投保人邮箱地址
    private String AppntEmail;
    //续保保费
    private String Premium;
    //续保次数
    private String RenewCount;
    //投保人职业代码
    private String AppntOccupationcode;
    //被保人职业代码
    private String InsuredOccupationcode;

    public String getRenewFlag() {
        return RenewFlag;
    }

    public void setRenewFlag(String renewFlag) {
        RenewFlag = renewFlag;
    }

    public String getProuctName() {
        return ProuctName;
    }

    public void setProuctName(String prouctName) {
        ProuctName = prouctName;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getInsuRedName() {
        return InsuRedName;
    }

    public void setInsuRedName(String insuRedName) {
        InsuRedName = insuRedName;
    }

    public String getRelationToAppnt() {
        return RelationToAppnt;
    }

    public void setRelationToAppnt(String relationToAppnt) {
        RelationToAppnt = relationToAppnt;
    }

    public String getRelationName() {
        return RelationName;
    }

    public void setRelationName(String relationName) {
        RelationName = relationName;
    }

    public String getHealthFlag() {
        return HealthFlag;
    }

    public void setHealthFlag(String healthFlag) {
        HealthFlag = healthFlag;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getInsuYear() {
        return InsuYear;
    }

    public void setInsuYear(String insuYear) {
        InsuYear = insuYear;
    }

    public String getAppntEmail() {
        return AppntEmail;
    }

    public void setAppntEmail(String appntEmail) {
        AppntEmail = appntEmail;
    }

    public String getPremium() {
        return Premium;
    }

    public void setPremium(String premium) {
        Premium = premium;
    }

    public String getRelaToInsured() {
        return RelaToInsured;
    }

    public void setRelaToInsured(String relaToInsured) {
        RelaToInsured = relaToInsured;
    }

    public String getRenewCount() {
        return RenewCount;
    }

    public void setRenewCount(String renewCount) {
        RenewCount = renewCount;
    }

    public String getAppntOccupationcode() {
        return AppntOccupationcode;
    }

    public void setAppntOccupationcode(String appntOccupationcode) {
        AppntOccupationcode = appntOccupationcode;
    }

    public String getInsuredOccupationcode() {
        return InsuredOccupationcode;
    }

    public void setInsuredOccupationcode(String insuredOccupationcode) {
        InsuredOccupationcode = insuredOccupationcode;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespRenewalPojo{" +
                "RenewFlag='" + RenewFlag + '\'' +
                ", ProuctName='" + ProuctName + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", InsuRedName='" + InsuRedName + '\'' +
                ", RelationToAppnt='" + RelationToAppnt + '\'' +
                ", RelaToInsured='" + RelaToInsured + '\'' +
                ", RelationName='" + RelationName + '\'' +
                ", HealthFlag='" + HealthFlag + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", InsuYear='" + InsuYear + '\'' +
                ", AppntEmail='" + AppntEmail + '\'' +
                ", Premium='" + Premium + '\'' +
                ", RenewCount='" + RenewCount + '\'' +
                ", AppntOccupationcode='" + AppntOccupationcode + '\'' +
                ", InsuredOccupationcode='" + InsuredOccupationcode + '\'' +
                '}';
    }
}
