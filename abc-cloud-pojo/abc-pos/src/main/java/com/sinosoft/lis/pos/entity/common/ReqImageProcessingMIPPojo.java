package com.sinosoft.lis.pos.entity.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.DocsPojo;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

/**
 * PC006	MIP影像处理
 */
public class ReqImageProcessingMIPPojo implements ReqBody,  Body,Serializable{
    private List<com.sinosoft.lis.pos.entity.DocsPojo>DocsPojo;

    @Override
    public String toString() {
        return "ReqImageProcessingMIPPojo{" +
                "DocsPojo=" + DocsPojo +
                '}';
    }
    @JSONField(name = "docs")
    public List<DocsPojo> getDocsPojo() {
        return DocsPojo;
    }

    public void setDocsPojo(List<DocsPojo> docsPojo) {
        DocsPojo = docsPojo;
    }


}
