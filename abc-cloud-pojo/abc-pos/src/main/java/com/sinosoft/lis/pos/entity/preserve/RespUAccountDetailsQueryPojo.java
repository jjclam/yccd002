package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4003保单万能账户追加保费明细查询 4007万能账户追加保费_变更.
 */
@Relation(transID = {"POS027", "POS122"})
public class RespUAccountDetailsQueryPojo implements Pojo, Body, Serializable {

    private String EdorType;
    private String ContNo;
    private String AppntName;
    private String RiskName;
    private String NewBankName;
    private String NewBankAccNo;
    private String NewBankAccName;
    private String DutyName;
    private String GetMoney;
    private String Phone;

    @Override
    public String toString() {
        return "RespUAccountDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", NewBankName='" + NewBankName + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", DutyName='" + DutyName + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", Phone='" + Phone + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getNewBankName() {
        return NewBankName;
    }

    public void setNewBankName(String newBankName) {
        NewBankName = newBankName;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getDutyName() {
        return DutyName;
    }

    public void setDutyName(String dutyName) {
        DutyName = dutyName;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
