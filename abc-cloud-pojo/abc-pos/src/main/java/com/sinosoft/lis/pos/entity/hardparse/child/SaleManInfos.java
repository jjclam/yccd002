package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class SaleManInfos {
    private List<SaleManInfo>SaleManInfo;

    @Override
    public String toString() {
        return "SaleManInfos{" +
                "SaleManInfo=" + SaleManInfo +
                '}';
    }

    public List<SaleManInfo> getSaleManInfo() {
        return SaleManInfo;
    }

    public void setSaleManInfo(List<SaleManInfo> saleManInfo) {
        SaleManInfo = saleManInfo;
    }
}
