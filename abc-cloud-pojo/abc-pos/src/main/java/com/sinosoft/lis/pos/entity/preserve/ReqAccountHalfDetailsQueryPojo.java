package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;


/**
 * 4003保单万能账户部分领取明细查询POS030.4007保单万能账户部分领取明细查询POS105
 */
public class ReqAccountHalfDetailsQueryPojo implements ReqBody, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保全号
    private String EdorAcceptNo;
    //保单号
    private String ContNo;


    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqAccountHalfDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}