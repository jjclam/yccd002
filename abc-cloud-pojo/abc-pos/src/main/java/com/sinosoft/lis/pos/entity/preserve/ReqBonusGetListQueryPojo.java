package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.BankInfoPojo;
import com.sinosoft.lis.pos.entity.PersonInfoPojo;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 红利列表查询POS019.4002微信官网红利列表查询
 */
public class ReqBonusGetListQueryPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;
    //收付费方式
    private String PayMode;
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    private PersonInfoPojo PersonInfoPojo;
    private BankInfoPojo BankInfoPojo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }
    @JSONField(name = "edorAppInfo")
    public com.sinosoft.lis.pos.entity.EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(com.sinosoft.lis.pos.entity.EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }
    @JSONField(name = "personInfo")
    public com.sinosoft.lis.pos.entity.PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(com.sinosoft.lis.pos.entity.PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }
    @JSONField(name = "bankInfo")
    public com.sinosoft.lis.pos.entity.BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(com.sinosoft.lis.pos.entity.BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    @Override
    public String toString() {
        return "ReqBonusGetListQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                '}';
    }
}