package com.sinosoft.lis.pos.entity.renewal;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

import java.util.List;

/**
 * 缴费提醒XQ005.
 */
@Relation(transID = "XQ005")
public class RespRenewalAwokePojo implements Pojo,Body,Serializable{
    //续期业务类型
    private String BusiType;
    private List<com.sinosoft.lis.pos.entity.PolicyJFTXQueryInfosPojo> PolicyJFTXQueryInfosPojo;

    public String getBusiType() {
        return BusiType;
    }

    public void setBusiType(String busiType) {
        BusiType = busiType;
    }

    public List<com.sinosoft.lis.pos.entity.PolicyJFTXQueryInfosPojo> getPolicyJFTXQueryInfosPojo() {
        return PolicyJFTXQueryInfosPojo;
    }
    @JSONField(name = "policyJFTXQueryInfos")
    public void setPolicyJFTXQueryInfosPojo(List<com.sinosoft.lis.pos.entity.PolicyJFTXQueryInfosPojo> policyJFTXQueryInfosPojo) {
        PolicyJFTXQueryInfosPojo = policyJFTXQueryInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespRenewalAwokePojo{" +
                "BusiType='" + BusiType + '\'' +
                ", PolicyJFTXQueryInfosPojo=" + PolicyJFTXQueryInfosPojo +
                '}';
    }
}