package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PhoneInfosPojo;
import com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 生存给付领取方式保全项目保单列表查询POS120.
 */
@Relation(transID = "POS120")
public class RespPreservePolicyListQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    private PolicyQueryInfosPojo PolicyQueryInfosPojo;
    private PhoneInfosPojo PhoneInfosPojo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo getPolicyQueryInfosPojo() {
        return PolicyQueryInfosPojo;
    }

    @JSONField(name = "policyQueryInfos")
    public void setPolicyQueryInfosPojo(com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo policyQueryInfosPojo) {
        PolicyQueryInfosPojo = policyQueryInfosPojo;
    }

    public com.sinosoft.lis.pos.entity.PhoneInfosPojo getPhoneInfosPojo() {
        return PhoneInfosPojo;
    }

    @JSONField(name = "phoneInfos")
    public void setPhoneInfosPojo(com.sinosoft.lis.pos.entity.PhoneInfosPojo phoneInfosPojo) {
        PhoneInfosPojo = phoneInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPreservePolicyListQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfosPojo=" + PolicyQueryInfosPojo +
                ", PhoneInfosPojo=" + PhoneInfosPojo +
                '}';
    }
}