package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

;

/**
 * 生存给付领取方式保全项目保单明细查询POS040.
 */
@Relation(transID = "POS040")
public class RespPreservePolicyDetailsQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //开户行
    private String BankCode;
    //户名
    private String BankAccName;
    //银行账户
    private String BankAccNo;
    //领取方式(一级)
    private String LiveGetMode;
    //领取方式(二级)
    private String LiveAccFlag;
    //领取方式(三级)
    private String GetForm;
    //领取方式校验标识
    private String JudgeFlag;


    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getLiveGetMode() {
        return LiveGetMode;
    }

    public void setLiveGetMode(String liveGetMode) {
        LiveGetMode = liveGetMode;
    }

    public String getLiveAccFlag() {
        return LiveAccFlag;
    }

    public void setLiveAccFlag(String liveAccFlag) {
        LiveAccFlag = liveAccFlag;
    }

    public String getGetForm() {
        return GetForm;
    }

    public void setGetForm(String getForm) {
        GetForm = getForm;
    }

    public String getJudgeFlag() {
        return JudgeFlag;
    }

    public void setJudgeFlag(String judgeFlag) {
        JudgeFlag = judgeFlag;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPreservePolicyDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", LiveGetMode='" + LiveGetMode + '\'' +
                ", LiveAccFlag='" + LiveAccFlag + '\'' +
                ", GetForm='" + GetForm + '\'' +
                ", JudgeFlag='" + JudgeFlag + '\'' +
                '}';
    }
}