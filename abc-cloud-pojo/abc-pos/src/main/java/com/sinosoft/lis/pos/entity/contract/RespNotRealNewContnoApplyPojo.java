package com.sinosoft.lis.pos.entity.contract;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 非实时出单申请NB007.
 */
@Relation(transID = "NB007")
public class RespNotRealNewContnoApplyPojo implements Pojo,Body,Serializable{
    //投保单号
    private String PolicyApplyNo;
    //险种代码
    private String RiskCode;
    //产品代码
    private String ProdCode;
    //保费
    private String Prem;
    private com.sinosoft.lis.pos.entity.AppInfoPojo AppInfoPojo;

    public String getPolicyApplyNo() {
        return PolicyApplyNo;
    }

    public void setPolicyApplyNo(String policyApplyNo) {
        PolicyApplyNo = policyApplyNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getProdCode() {
        return ProdCode;
    }

    public void setProdCode(String prodCode) {
        ProdCode = prodCode;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public com.sinosoft.lis.pos.entity.AppInfoPojo getAppInfoPojo() {
        return AppInfoPojo;
    }
    @JSONField(name = "appInfo")
    public void setAppInfoPojo(com.sinosoft.lis.pos.entity.AppInfoPojo appInfoPojo) {
        AppInfoPojo = appInfoPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespNotRealNewContnoApplyPojo{" +
                "PolicyApplyNo='" + PolicyApplyNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", ProdCode='" + ProdCode + '\'' +
                ", Prem='" + Prem + '\'' +
                ", AppInfoPojo=" + AppInfoPojo +
                '}';
    }
}