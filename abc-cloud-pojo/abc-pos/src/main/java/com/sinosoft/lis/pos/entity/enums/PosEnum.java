package com.sinosoft.lis.pos.entity.enums;


/**
 * Created by Administrator on 2018/11/6.
 */
public enum PosEnum {
    POS018("FuncFlag", "1013"), POS089("FuncFlag", "1013"), POS044("FuncFlag", "1013"), XQ002("FuncFlag", "1008"), NB007("FuncFlag", "1006");

    PosEnum(String field, String value) {
        this.field = field;
        this.value = value;
    }

    private String field;
    private String value;

    public String[] getValue() {
        String[] values = this.value.split(",");
        return values;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String[] getField() {
        String[] fields = this.field.split(",");
        return fields;
    }

    public void setField(String field) {
        this.field = field;
    }

    public static boolean contains(String type) {
        for (PosEnum posEnum : PosEnum.values()) {
            if (posEnum.name().equals(type)) {
                return true;
            }
        }
        return false;
    }
}
