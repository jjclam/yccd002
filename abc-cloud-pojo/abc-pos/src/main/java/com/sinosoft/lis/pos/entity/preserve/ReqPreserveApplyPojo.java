package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PersonInfoPojo;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

;

/**
 * 生存给付领取方式变更保全申请POS041.
 */
public class ReqPreserveApplyPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //申请日期
    private String ApplyDate;
    //业务类型
    private String BussType;
    //收付费方式
    private String PayMode;
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    private PersonInfoPojo PersonInfoPojo;
    private com.sinosoft.lis.pos.entity.BankInfoPojo BankInfoPojo;
    //生存金派发方式（一级）
    private String LiveGetMode	;
    //生存金派发方式（二级）
    private String LiveAccFlag;
    //生存金派发方式（三级）
    private String GetForm	;
    //领取方式校验标识
    private String JudgeFlag;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    @JSONField(name = "edorAppInfo")
    public com.sinosoft.lis.pos.entity.EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(com.sinosoft.lis.pos.entity.EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }

    @JSONField(name = "personInfo")
    public com.sinosoft.lis.pos.entity.PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(com.sinosoft.lis.pos.entity.PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }

    @JSONField(name = "bankInfo")
    public com.sinosoft.lis.pos.entity.BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(com.sinosoft.lis.pos.entity.BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    public String getLiveGetMode() {
        return LiveGetMode;
    }

    public void setLiveGetMode(String liveGetMode) {
        LiveGetMode = liveGetMode;
    }

    public String getLiveAccFlag() {
        return LiveAccFlag;
    }

    public void setLiveAccFlag(String liveAccFlag) {
        LiveAccFlag = liveAccFlag;
    }

    public String getGetForm() {
        return GetForm;
    }

    public void setGetForm(String getForm) {
        GetForm = getForm;
    }

    public String getJudgeFlag() {
        return JudgeFlag;
    }

    public void setJudgeFlag(String judgeFlag) {
        JudgeFlag = judgeFlag;
    }

    @Override
    public String toString() {
        return "ReqPreserveApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", BussType='" + BussType + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                ", LiveGetMode='" + LiveGetMode + '\'' +
                ", LiveAccFlag='" + LiveAccFlag + '\'' +
                ", GetForm='" + GetForm + '\'' +
                ", JudgeFlag='" + JudgeFlag + '\'' +
                '}';
    }
}