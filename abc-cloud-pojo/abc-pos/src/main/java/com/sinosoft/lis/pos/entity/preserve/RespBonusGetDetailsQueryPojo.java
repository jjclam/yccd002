package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.BonusRisksPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 红利详情查询POS020.4003微信官网红利详情查询
 */
@Relation(transID = "POS020")
public class RespBonusGetDetailsQueryPojo implements Pojo,Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //红利总金额
    private String TotalBonusValue;
    private BonusRisksPojo BonusRisksPojo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getTotalBonusValue() {
        return TotalBonusValue;
    }

    public void setTotalBonusValue(String totalBonusValue) {
        TotalBonusValue = totalBonusValue;
    }

    public com.sinosoft.lis.pos.entity.BonusRisksPojo getBonusRisksPojo() {
        return BonusRisksPojo;
    }
    @JSONField(name = "bonusRisks")
    public void setBonusRisksPojo(com.sinosoft.lis.pos.entity.BonusRisksPojo bonusRisksPojo) {
        BonusRisksPojo = bonusRisksPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespBonusGetDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", TotalBonusValue='" + TotalBonusValue + '\'' +
                ", BonusRisksPojo=" + BonusRisksPojo +
                '}';
    }
}