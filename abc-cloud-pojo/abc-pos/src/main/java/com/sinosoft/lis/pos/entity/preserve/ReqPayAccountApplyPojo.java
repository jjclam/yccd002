package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PersonInfoPojo;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

;

/**
 * 生存给付续领账户变更申请POS039.
 */
public class ReqPayAccountApplyPojo implements ReqBody, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //主险名称
    private String MainRiskName;
    //投保人姓名
    private String AppntName;
    //领取方式
    private String GetMode;
    //开户行
    private String BankCode;
    //银行帐户名
    private String BankAccName;
    //银行账号
    private String BankAccNo;
    //收付费方式
    private String PayMode;
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    private PersonInfoPojo PersonInfoPojo;
    private com.sinosoft.lis.pos.entity.BankInfoPojo BankInfoPojo;
    //客户名称
    private String CustomerName;
    //证件号码
    private String CustomerIdNo;
    //申请日期
    private String ApplyDate;

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    @JSONField(name = "edorAppInfo")
    public com.sinosoft.lis.pos.entity.EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(com.sinosoft.lis.pos.entity.EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }

    @JSONField(name = "personInfo")
    public com.sinosoft.lis.pos.entity.PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(com.sinosoft.lis.pos.entity.PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }

    @JSONField(name = "bankInfo")
    public com.sinosoft.lis.pos.entity.BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(com.sinosoft.lis.pos.entity.BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getGetMode() {
        return GetMode;
    }

    public void setGetMode(String getMode) {
        GetMode = getMode;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIdNo() {
        return CustomerIdNo;
    }

    public void setCustomerIdNo(String customerIdNo) {
        CustomerIdNo = customerIdNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    @Override
    public String toString() {
        return "ReqPayAccountApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", GetMode='" + GetMode + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIdNo='" + CustomerIdNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                '}';
    }
}