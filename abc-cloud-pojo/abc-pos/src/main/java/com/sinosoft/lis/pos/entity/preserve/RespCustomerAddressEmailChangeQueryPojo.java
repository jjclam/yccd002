package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PhoneInfosPojo;
import com.sinosoft.lis.pos.entity.PolicyQueryComInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 客户联系地址或邮箱保全项目保单明细查询POS079
 */
@Relation(transID = "POS079")
public class RespCustomerAddressEmailChangeQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //明细信息总节点
    private com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo PolicyQueryInfosPojo;
    //保单来源机构信息查询
    private com.sinosoft.lis.pos.entity.PolicyQueryComInfosPojo PolicyQueryComInfosPojo;
    private PhoneInfosPojo PhoneInfosPojo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo getPolicyQueryInfosPojo() {
        return PolicyQueryInfosPojo;
    }

    @JSONField(name = "policyQueryInfos")
    public void setPolicyQueryInfosPojo(com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo policyQueryInfosPojo) {
        PolicyQueryInfosPojo = policyQueryInfosPojo;
    }

    public PolicyQueryComInfosPojo getPolicyQueryComInfosPojo() {
        return PolicyQueryComInfosPojo;
    }

    @JSONField(name = "policyQueryComInfos")
    public void setPolicyQueryComInfosPojo(PolicyQueryComInfosPojo policyQueryComInfosPojo) {
        PolicyQueryComInfosPojo = policyQueryComInfosPojo;
    }

    public com.sinosoft.lis.pos.entity.PhoneInfosPojo getPhoneInfosPojo() {
        return PhoneInfosPojo;
    }

    @JSONField(name = "phoneInfos")
    public void setPhoneInfosPojo(com.sinosoft.lis.pos.entity.PhoneInfosPojo phoneInfosPojo) {
        PhoneInfosPojo = phoneInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespCustomerAddressEmailChangeQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfosPojo=" + PolicyQueryInfosPojo +
                ", PolicyQueryComInfosPojo=" + PolicyQueryComInfosPojo +
                ", PhoneInfosPojo=" + PhoneInfosPojo +
                '}';
    }
}
