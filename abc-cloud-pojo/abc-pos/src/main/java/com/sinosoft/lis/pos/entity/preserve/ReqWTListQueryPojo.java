package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4002保单犹豫期退保列表查询POS016.官网4002保单保全查询请求
 */
public class ReqWTListQueryPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //客户号
    private String CustomerNo;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }
    @Override
    public String toString() {
        return "ReqWTListQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}