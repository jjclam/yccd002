package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class PolicyQueryComInfoPojo implements Body,Serializable{
    //保单号
    private String ContNo;
    //管理机构
    private String ManageCom;

    @Override
    public String toString() {
        return "PolicyQueryComInfoPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                '}';
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }


}
