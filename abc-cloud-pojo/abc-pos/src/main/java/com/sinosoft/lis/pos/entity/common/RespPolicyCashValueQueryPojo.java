package com.sinosoft.lis.pos.entity.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;


/**
 * 保单价值查询PC001.
 */
@Relation(transID = "PC001")
public class RespPolicyCashValueQueryPojo implements Pojo,Body,Serializable{
    private com.sinosoft.lis.pos.entity.PolicyValueInfosPojo PolicyValueInfosPojo;

    public com.sinosoft.lis.pos.entity.PolicyValueInfosPojo getPolicyValueInfosPojo() {
        return PolicyValueInfosPojo;
    }
    @JSONField(name = "policyValueInfos")
    public void setPolicyValueInfosPojo(com.sinosoft.lis.pos.entity.PolicyValueInfosPojo policyValueInfosPojo) {
        PolicyValueInfosPojo = policyValueInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPolicyCashValueQueryPojo{" +
                "PolicyValueInfosPojo=" + PolicyValueInfosPojo +
                '}';
    }
}