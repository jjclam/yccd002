package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

;

public class TextInfosPojo implements Body, Serializable {
    private List<TextInfoPojo> TextInfoPojo;

    @Override
    public String toString() {
        return "TextInfosPojo{" +
                "TextInfoPojo=" + TextInfoPojo +
                '}';
    }

    @JSONField(name = "textInfo")
    public List<TextInfoPojo> getTextInfoPojo() {
        return TextInfoPojo;
    }

    @JSONField(name = "textInfo")
    public void setTextInfoPojo(List<TextInfoPojo> textInfoPojo) {
        TextInfoPojo = textInfoPojo;
    }

}
