package com.sinosoft.lis.pos.entity.hardparse.child;

public class ChnlExtend {
    //网点名称
    private String BankBranchName;
    //业务许可证编号
    private String Licenseno;
    //银行保险业务负责人
    private String Subscribeman;
    //工号
    private String SubscribemanDuty;
    //柜员代码
    private String TellerNo;
    //柜员名字
    private String TellerName;
    //业务员代码
    private String BankQualno;
    //银行帐号
    private String ConAccNo;

    @Override
    public String toString() {
        return "ChnlExtend{" +
                "BankBranchName='" + BankBranchName + '\'' +
                ", Licenseno='" + Licenseno + '\'' +
                ", Subscribeman='" + Subscribeman + '\'' +
                ", SubscribemanDuty='" + SubscribemanDuty + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", TellerName='" + TellerName + '\'' +
                ", BankQualno='" + BankQualno + '\'' +
                ", ConAccNo='" + ConAccNo + '\'' +
                '}';
    }

    public String getConAccNo() {
        return ConAccNo;
    }

    public void setConAccNo(String conAccNo) {
        ConAccNo = conAccNo;
    }

    public String getBankBranchName() {
        return BankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        BankBranchName = bankBranchName;
    }

    public String getLicenseno() {
        return Licenseno;
    }

    public void setLicenseno(String licenseno) {
        Licenseno = licenseno;
    }

    public String getSubscribeman() {
        return Subscribeman;
    }

    public void setSubscribeman(String subscribeman) {
        Subscribeman = subscribeman;
    }

    public String getSubscribemanDuty() {
        return SubscribemanDuty;
    }

    public void setSubscribemanDuty(String subscribemanDuty) {
        SubscribemanDuty = subscribemanDuty;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getTellerName() {
        return TellerName;
    }

    public void setTellerName(String tellerName) {
        TellerName = tellerName;
    }

    public String getBankQualno() {
        return BankQualno;
    }

    public void setBankQualno(String bankQualno) {
        BankQualno = bankQualno;
    }
}
