package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * @program: abc-cloud-pojo
 * @description: 微信续保查询
 * @author: BaoYongmeng
 * @create: 2019-03-20 17:48
 **/
public class ReqRenewalPojo implements ReqBody, Body, Serializable {

    private String ContNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqRenewalPojo{" +
                "ContNo='" + ContNo + '\'' +
                '}';
    }
}
