package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class SurvivalRisksPojo implements Body,Serializable{

private List<SurvivalRiskPojo> SurvivalRiskPojo;

    public List<com.sinosoft.lis.pos.entity.SurvivalRiskPojo> getSurvivalRiskPojo() {
        return SurvivalRiskPojo;
    }
    @JSONField(name = "survivalRisk")
    public void setSurvivalRiskPojo(List<com.sinosoft.lis.pos.entity.SurvivalRiskPojo> survivalRiskPojo) {
        SurvivalRiskPojo = survivalRiskPojo;
    }

    @Override
    public String toString() {
        return "SurvivalRisksPojo{" +
                "SurvivalRiskPojo=" + SurvivalRiskPojo +
                '}';
    }
}