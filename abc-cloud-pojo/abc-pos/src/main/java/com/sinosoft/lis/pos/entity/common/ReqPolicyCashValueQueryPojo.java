package com.sinosoft.lis.pos.entity.common;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 保单价值查询PC001.
 */
public class ReqPolicyCashValueQueryPojo implements ReqBody, Body,Serializable{
    //保单号
    private String ContNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqPolicyCashValueQueryPojo{" +
                "ContNo='" + ContNo + '\'' +
                '}';
    }
}