package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.PersonInfoPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4004保费自垫选择权变更申请POS012.
 */

public class ReqPremSelectApplyPojo implements ReqBody,Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保费自垫标记
    private String AdcanceFlag;
    //申请人姓名
    private String CustomerName;
    //申请人身份证号码
    private String CustomerIdNo;
    //申请日期
    private String ApplyDate;
    //收付费方式
    private String PayMode;
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    private PersonInfoPojo PersonInfoPojo;
    private com.sinosoft.lis.pos.entity.BankInfoPojo BankInfoPojo;

    public String getAdcanceFlag() {
        return AdcanceFlag;
    }

    public void setAdcanceFlag(String adcanceFlag) {
        AdcanceFlag = adcanceFlag;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIdNo() {
        return CustomerIdNo;
    }

    public void setCustomerIdNo(String customerIdNo) {
        CustomerIdNo = customerIdNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public com.sinosoft.lis.pos.entity.EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(com.sinosoft.lis.pos.entity.EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }

    public com.sinosoft.lis.pos.entity.PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(com.sinosoft.lis.pos.entity.PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }

    public com.sinosoft.lis.pos.entity.BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(com.sinosoft.lis.pos.entity.BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }


    @Override
    public String toString() {
        return "ReqPremSelectApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AdcanceFlag='" + AdcanceFlag + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIdNo='" + CustomerIdNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                '}';
    }
}