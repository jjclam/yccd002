package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;
import java.util.List;

public class Lcinsureds implements Serializable {
    private List<Lcinsured> Lcinsured;

    public List<com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcinsured> getLcinsured() {
        return Lcinsured;
    }

    public void setLcinsured(List<com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcinsured> lcinsured) {
        Lcinsured = lcinsured;
    }

    @Override
    public String toString() {
        return "Lcinsureds{" +
                "Lcinsured=" + Lcinsured +
                '}';
    }
}