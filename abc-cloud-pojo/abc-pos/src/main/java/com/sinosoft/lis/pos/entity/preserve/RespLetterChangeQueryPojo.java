package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 信函接受方式变更查询POS067
 */
@Relation(transID = "POS067")
public class RespLetterChangeQueryPojo implements Pojo,Body,Serializable{

    private String EdorType;
    private com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo PolicyQueryInfosPojo;

    @Override
    public String toString() {
        return "RespLetterChangeQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfosPojo=" + PolicyQueryInfosPojo +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public PolicyQueryInfosPojo getPolicyQueryInfosPojo() {
        return PolicyQueryInfosPojo;
    }
    @JSONField(name = "policyQueryInfos")
    public void setPolicyQueryInfosPojo(PolicyQueryInfosPojo policyQueryInfosPojo) {
        PolicyQueryInfosPojo = policyQueryInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
