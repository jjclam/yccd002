package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class Risks {
    private List<Risk> Risk;

    @Override
    public String toString() {
        return "Risks{" +
                "Risk=" + Risk +
                '}';
    }

    public List<Risk> getRisk() {
        return Risk;
    }

    public void setRisk(List<Risk> risk) {
        Risk = risk;
    }
}
