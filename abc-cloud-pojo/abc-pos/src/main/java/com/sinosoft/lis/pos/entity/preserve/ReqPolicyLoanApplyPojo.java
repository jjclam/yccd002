package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

;

/**
 * 4004保单贷款申请POS075
 */
public class ReqPolicyLoanApplyPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //贷款本金
    private String LoanMoney;
    //申请人信息
    private com.sinosoft.lis.pos.entity.EdorAppInfoPojo EdorAppInfoPojo;
    //领取人
    private com.sinosoft.lis.pos.entity.PersonInfoPojo PersonInfoPojo;
    //银行
    private com.sinosoft.lis.pos.entity.BankInfoPojo BankInfoPojo;
    //收付费方式
    private String PayMode;
    //是否预约还款
    private String RepayFlag;
    //预约日期
    private String RepayDate;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }
    @JSONField(name = "edorAppInfo")
    public com.sinosoft.lis.pos.entity.EdorAppInfoPojo getEdorAppInfoPojo() {
        return EdorAppInfoPojo;
    }

    public void setEdorAppInfoPojo(com.sinosoft.lis.pos.entity.EdorAppInfoPojo edorAppInfoPojo) {
        EdorAppInfoPojo = edorAppInfoPojo;
    }
    @JSONField(name = "personInfo")
    public com.sinosoft.lis.pos.entity.PersonInfoPojo getPersonInfoPojo() {
        return PersonInfoPojo;
    }

    public void setPersonInfoPojo(com.sinosoft.lis.pos.entity.PersonInfoPojo personInfoPojo) {
        PersonInfoPojo = personInfoPojo;
    }
    @JSONField(name = "bankInfo")
    public com.sinosoft.lis.pos.entity.BankInfoPojo getBankInfoPojo() {
        return BankInfoPojo;
    }

    public void setBankInfoPojo(com.sinosoft.lis.pos.entity.BankInfoPojo bankInfoPojo) {
        BankInfoPojo = bankInfoPojo;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getRepayFlag() {
        return RepayFlag;
    }

    public void setRepayFlag(String repayFlag) {
        RepayFlag = repayFlag;
    }

    public String getRepayDate() {
        return RepayDate;
    }

    public void setRepayDate(String repayDate) {
        RepayDate = repayDate;
    }

    @Override
    public String toString() {
        return "ReqPolicyLoanApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", LoanMoney='" + LoanMoney + '\'' +
                ", EdorAppInfoPojo=" + EdorAppInfoPojo +
                ", PersonInfoPojo=" + PersonInfoPojo +
                ", BankInfoPojo=" + BankInfoPojo +
                ", PayMode='" + PayMode + '\'' +
                ", RepayFlag='" + RepayFlag + '\'' +
                ", RepayDate='" + RepayDate + '\'' +
                '}';
    }
}
