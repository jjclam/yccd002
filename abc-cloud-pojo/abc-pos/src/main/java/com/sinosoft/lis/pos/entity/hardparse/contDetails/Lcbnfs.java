package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;
import java.util.List;

public class Lcbnfs implements Serializable {
    private List<Lcbnf> Lcbnf;

    public List<com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcbnf> getLcbnf() {
        return Lcbnf;
    }

    public void setLcbnf(List<com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcbnf> lcbnf) {
        Lcbnf = lcbnf;
    }

    @Override
    public String toString() {
        return "Lcbnfs{" +
                "Lcbnf=" + Lcbnf +
                '}';
    }
}