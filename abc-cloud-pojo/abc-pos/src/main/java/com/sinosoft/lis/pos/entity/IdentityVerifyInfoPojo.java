package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class IdentityVerifyInfoPojo implements Body,Serializable{
    //被保人号
    private String InsuredNo;
    //被保人姓名
    private String InsuredName;
    private String PolicyAppName;
    private String PolicyCom;

    @Override
    public String toString() {
        return "IdentityVerifyInfoPojo{" +
                "InsuredNo='" + InsuredNo + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", PolicyAppName='" + PolicyAppName + '\'' +
                ", PolicyCom='" + PolicyCom + '\'' +
                '}';
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        InsuredNo = insuredNo;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getPolicyAppName() {
        return PolicyAppName;
    }

    public void setPolicyAppName(String policyAppName) {
        PolicyAppName = policyAppName;
    }

    public String getPolicyCom() {
        return PolicyCom;
    }

    public void setPolicyCom(String policyCom) {
        PolicyCom = policyCom;
    }

}
