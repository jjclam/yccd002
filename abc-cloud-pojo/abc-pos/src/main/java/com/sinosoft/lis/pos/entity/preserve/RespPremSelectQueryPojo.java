package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4003保费自垫选择权变更查询POS011.
 */
@Relation(transID = "POS011")
public class RespPremSelectQueryPojo implements Pojo,Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //涉及险种
    private String RelationRisk;
    //保费自垫标记
    private String AdcanceFlag;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getRelationRisk() {
        return RelationRisk;
    }

    public void setRelationRisk(String relationRisk) {
        RelationRisk = relationRisk;
    }

    public String getAdcanceFlag() {
        return AdcanceFlag;
    }

    public void setAdcanceFlag(String adcanceFlag) {
        AdcanceFlag = adcanceFlag;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPremSelectQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", RelationRisk='" + RelationRisk + '\'' +
                ", AdcanceFlag='" + AdcanceFlag + '\'' +
                '}';
    }
}