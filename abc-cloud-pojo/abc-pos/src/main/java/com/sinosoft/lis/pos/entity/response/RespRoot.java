package com.sinosoft.lis.pos.entity.response;

import java.util.Map;

/**
 * Created by Administrator on 2018/11/1.
 */
public class RespRoot {

    private RespHeadPojo Head;
    private Map body;

    public RespHeadPojo getHead() {
        return Head;
    }

    public void setHead(RespHeadPojo head) {
        Head = head;
    }

    public Map getBody() {
        return body;
    }

    public void setBody(Map body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "RespRoot{" +
                "Head=" + Head +
                ", body=" + body +
                '}';
    }
}
