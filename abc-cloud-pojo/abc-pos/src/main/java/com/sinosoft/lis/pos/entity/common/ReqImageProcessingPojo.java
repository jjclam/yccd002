package com.sinosoft.lis.pos.entity.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.ImgPathsPojo;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

/**
 * 影像处理PC003.
 */
public class ReqImageProcessingPojo implements ReqBody, Body,Serializable{
    private String Count;
    private String ManageCom;
    private String SerialNo;
    private String Contno;
    private String ImgType;
    private List<ImgPathsPojo> ImgPathsPojo;

    @Override
    public String toString() {
        return "ReqImageProcessingPojo{" +
                "Count='" + Count + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", Contno='" + Contno + '\'' +
                ", ImgType='" + ImgType + '\'' +
                ", ImgPathsPojo=" + ImgPathsPojo +
                '}';
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getContno() {
        return Contno;
    }

    public void setContno(String contno) {
        Contno = contno;
    }

    public String getImgType() {
        return ImgType;
    }

    public void setImgType(String imgType) {
        ImgType = imgType;
    }

    @JSONField(name = "imgPaths")
    public List<com.sinosoft.lis.pos.entity.ImgPathsPojo> getImgPathsPojo() {
        return ImgPathsPojo;
    }

    public void setImgPathsPojo(List<com.sinosoft.lis.pos.entity.ImgPathsPojo> imgPathsPojo) {
        ImgPathsPojo = imgPathsPojo;
    }


}
