package com.sinosoft.lis.pos.entity.hardparse;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.hardparse.contDetails.*;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 保单详情查询PC004.
 */
@Relation(transID = "PC004")
public class RespContDetailsQueryPojo implements Pojo,Body,Serializable{

    //保单号
    private String ContNo;
    //保单印刷号
    private String VchNo;
    //保险公司险种代码
    private String RiskCode;
    //险种名称
    private String RiskName;
    //保单状态
    private String PolicyStatus;
    //保单质押状态
    private String PolicyPledge;
    //受理日期
    private String AcceptDate;
    //保单生效日
    private String PolicyBgnDate;
    //保单到期日
    private String PolicyEndDate;
    //投保份数
    private String PolicyAmmount;
    //保费
    private String Amt;
    //保额
    private String Beamt;
    //保单价值
    private String PolicyValue;
    //当前账户价值
    private String AccountValue;
    //保险期间
    private String InsuDueDate;
    //缴费省市代码
    private String PayProv;
    //缴费网点号
    private String PayBranch;
    //缴费方式
    private String PayType;
    //缴费期间
    private String PayDue;
    //缴费金额
    private String Prem;
    //缴费账户
    private String BankAccNo;
    //保单类型
    private String Conttype;
    //投保单申请日期
    private String PolApplyDate;
    //缴费年期
    private String PayDueDate;
    //终交年龄年期标志
    private String PayEndYearFlag;
    //终交年龄年期
    private String PayEndYear;
    //保险年龄年期标志
    private String InsuYearFlag;
    //保险年龄年期
    private String InsuYear;
    //日期标志
    private String DateFlag;
    //告知参数 ，投保人年收入
    private String AnnualIncome;
    //借款合同编号
    private String JYContNo;
    //借款凭证编号
    private String JYCertNo;
    //借款（起始）日期
    private String JYStartDate;
    //借款（终止）到期
    private String JYEndDate;
    //借意险贷款金额
    private String LoanAmount;
    //借意险贷款发放机构
    private String LendCom;
    private Lcappnts Lcappnts;
    private Lcinsureds Lcinsureds;
    private Lcbnfs Lcbnfs;
    private Addts Addts;
    private Objt Objt;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getVchNo() {
        return VchNo;
    }

    public void setVchNo(String vchNo) {
        VchNo = vchNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getPolicyPledge() {
        return PolicyPledge;
    }

    public void setPolicyPledge(String policyPledge) {
        PolicyPledge = policyPledge;
    }

    public String getAcceptDate() {
        return AcceptDate;
    }

    public void setAcceptDate(String acceptDate) {
        AcceptDate = acceptDate;
    }

    public String getPolicyBgnDate() {
        return PolicyBgnDate;
    }

    public void setPolicyBgnDate(String policyBgnDate) {
        PolicyBgnDate = policyBgnDate;
    }

    public String getPolicyEndDate() {
        return PolicyEndDate;
    }

    public void setPolicyEndDate(String policyEndDate) {
        PolicyEndDate = policyEndDate;
    }

    public String getPolicyAmmount() {
        return PolicyAmmount;
    }

    public void setPolicyAmmount(String policyAmmount) {
        PolicyAmmount = policyAmmount;
    }

    public String getAmt() {
        return Amt;
    }

    public void setAmt(String amt) {
        Amt = amt;
    }

    public String getBeamt() {
        return Beamt;
    }

    public void setBeamt(String beamt) {
        Beamt = beamt;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public String getAccountValue() {
        return AccountValue;
    }

    public void setAccountValue(String accountValue) {
        AccountValue = accountValue;
    }

    public String getInsuDueDate() {
        return InsuDueDate;
    }

    public void setInsuDueDate(String insuDueDate) {
        InsuDueDate = insuDueDate;
    }

    public String getPayProv() {
        return PayProv;
    }

    public void setPayProv(String payProv) {
        PayProv = payProv;
    }

    public String getPayBranch() {
        return PayBranch;
    }

    public void setPayBranch(String payBranch) {
        PayBranch = payBranch;
    }

    public String getPayType() {
        return PayType;
    }

    public void setPayType(String payType) {
        PayType = payType;
    }

    public String getPayDue() {
        return PayDue;
    }

    public void setPayDue(String payDue) {
        PayDue = payDue;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getConttype() {
        return Conttype;
    }

    public void setConttype(String conttype) {
        Conttype = conttype;
    }

    public String getPolApplyDate() {
        return PolApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        PolApplyDate = polApplyDate;
    }

    public String getPayDueDate() {
        return PayDueDate;
    }

    public void setPayDueDate(String payDueDate) {
        PayDueDate = payDueDate;
    }

    public String getPayEndYearFlag() {
        return PayEndYearFlag;
    }

    public void setPayEndYearFlag(String payEndYearFlag) {
        PayEndYearFlag = payEndYearFlag;
    }

    public String getPayEndYear() {
        return PayEndYear;
    }

    public void setPayEndYear(String payEndYear) {
        PayEndYear = payEndYear;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        InsuYearFlag = insuYearFlag;
    }

    public String getInsuYear() {
        return InsuYear;
    }

    public void setInsuYear(String insuYear) {
        InsuYear = insuYear;
    }

    public String getDateFlag() {
        return DateFlag;
    }

    public void setDateFlag(String dateFlag) {
        DateFlag = dateFlag;
    }

    public String getAnnualIncome() {
        return AnnualIncome;
    }

    public void setAnnualIncome(String annualIncome) {
        AnnualIncome = annualIncome;
    }

    public String getJYContNo() {
        return JYContNo;
    }

    public void setJYContNo(String JYContNo) {
        this.JYContNo = JYContNo;
    }

    public String getJYCertNo() {
        return JYCertNo;
    }

    public void setJYCertNo(String JYCertNo) {
        this.JYCertNo = JYCertNo;
    }

    public String getJYStartDate() {
        return JYStartDate;
    }

    public void setJYStartDate(String JYStartDate) {
        this.JYStartDate = JYStartDate;
    }

    public String getJYEndDate() {
        return JYEndDate;
    }

    public void setJYEndDate(String JYEndDate) {
        this.JYEndDate = JYEndDate;
    }

    public String getLoanAmount() {
        return LoanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        LoanAmount = loanAmount;
    }

    public String getLendCom() {
        return LendCom;
    }

    public void setLendCom(String lendCom) {
        LendCom = lendCom;
    }

    public com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcappnts getLcappnts() {
        return Lcappnts;
    }

    public void setLcappnts(com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcappnts lcappnts) {
        Lcappnts = lcappnts;
    }

    public com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcinsureds getLcinsureds() {
        return Lcinsureds;
    }

    public void setLcinsureds(com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcinsureds lcinsureds) {
        Lcinsureds = lcinsureds;
    }

    public com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcbnfs getLcbnfs() {
        return Lcbnfs;
    }

    public void setLcbnfs(com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcbnfs lcbnfs) {
        Lcbnfs = lcbnfs;
    }

    public com.sinosoft.lis.pos.entity.hardparse.contDetails.Addts getAddts() {
        return Addts;
    }

    public void setAddts(com.sinosoft.lis.pos.entity.hardparse.contDetails.Addts addts) {
        Addts = addts;
    }

    public com.sinosoft.lis.pos.entity.hardparse.contDetails.Objt getObjt() {
        return Objt;
    }

    public void setObjt(com.sinosoft.lis.pos.entity.hardparse.contDetails.Objt objt) {
        Objt = objt;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespContDetailsQueryPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", VchNo='" + VchNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", PolicyStatus='" + PolicyStatus + '\'' +
                ", PolicyPledge='" + PolicyPledge + '\'' +
                ", AcceptDate='" + AcceptDate + '\'' +
                ", PolicyBgnDate='" + PolicyBgnDate + '\'' +
                ", PolicyEndDate='" + PolicyEndDate + '\'' +
                ", PolicyAmmount='" + PolicyAmmount + '\'' +
                ", Amt='" + Amt + '\'' +
                ", Beamt='" + Beamt + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", AccountValue='" + AccountValue + '\'' +
                ", InsuDueDate='" + InsuDueDate + '\'' +
                ", PayProv='" + PayProv + '\'' +
                ", PayBranch='" + PayBranch + '\'' +
                ", PayType='" + PayType + '\'' +
                ", PayDue='" + PayDue + '\'' +
                ", Prem='" + Prem + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", Conttype='" + Conttype + '\'' +
                ", PolApplyDate='" + PolApplyDate + '\'' +
                ", PayDueDate='" + PayDueDate + '\'' +
                ", PayEndYearFlag='" + PayEndYearFlag + '\'' +
                ", PayEndYear='" + PayEndYear + '\'' +
                ", InsuYearFlag='" + InsuYearFlag + '\'' +
                ", InsuYear='" + InsuYear + '\'' +
                ", DateFlag='" + DateFlag + '\'' +
                ", AnnualIncome='" + AnnualIncome + '\'' +
                ", JYContNo='" + JYContNo + '\'' +
                ", JYCertNo='" + JYCertNo + '\'' +
                ", JYStartDate='" + JYStartDate + '\'' +
                ", JYEndDate='" + JYEndDate + '\'' +
                ", LoanAmount='" + LoanAmount + '\'' +
                ", LendCom='" + LendCom + '\'' +
                ", Lcappnts=" + Lcappnts +
                ", Lcinsureds=" + Lcinsureds +
                ", Lcbnfs=" + Lcbnfs +
                ", Addts=" + Addts +
                ", Objt=" + Objt +
                '}';
    }
}