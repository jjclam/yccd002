package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

public class PathInfosPojo implements Body, Serializable {
    private List<PathInfoPojo> PathInfoPojo;

    public List<com.sinosoft.lis.pos.entity.PathInfoPojo> getPathInfoPojo() {
        return PathInfoPojo;
    }

    @JSONField(name = "pathInfo")
    public void setPathInfoPojo(List<com.sinosoft.lis.pos.entity.PathInfoPojo> pathInfoPojo) {
        PathInfoPojo = pathInfoPojo;
    }

    @Override
    public String toString() {
        return "PathInfosPojo{" +
                "PathInfoPojo=" + PathInfoPojo +
                '}';
    }
}