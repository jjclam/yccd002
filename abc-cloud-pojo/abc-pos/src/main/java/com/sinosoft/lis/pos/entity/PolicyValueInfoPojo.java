package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class PolicyValueInfoPojo implements Body,Serializable{
    //退保可返回金额
    private String Amnt;
    //退保手续费
    private String FeeAmnt;
    //保单到期日
    private String ExpireDate;
    //险种编码
    private String RiskCode;
    private com.sinosoft.lis.pos.entity.CashValuesPojo CashValuesPojo;

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getFeeAmnt() {
        return FeeAmnt;
    }

    public void setFeeAmnt(String feeAmnt) {
        FeeAmnt = feeAmnt;
    }

    public String getExpireDate() {
        return ExpireDate;
    }

    public void setExpireDate(String expireDate) {
        ExpireDate = expireDate;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }
    @JSONField(name = "cashValues")
    public com.sinosoft.lis.pos.entity.CashValuesPojo getCashValuesPojo() {
        return CashValuesPojo;
    }
    @JSONField(name = "cashValues")
    public void setCashValuesPojo(com.sinosoft.lis.pos.entity.CashValuesPojo cashValuesPojo) {
        CashValuesPojo = cashValuesPojo;
    }

    @Override
    public String toString() {
        return "PolicyValueInfoPojo{" +
                "Amnt='" + Amnt + '\'' +
                ", FeeAmnt='" + FeeAmnt + '\'' +
                ", ExpireDate='" + ExpireDate + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", CashValuesPojo=" + CashValuesPojo +
                '}';
    }
}