package com.sinosoft.lis.pos.entity;



import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * Created by Administrator on 2018/11/6.
 */
public class EdorAppInfoPojo implements Body,Serializable{

    //申请人姓名
    private String EdorAppName;
    //申请人证件类型
    private String EdorAppIDType;
    //申请人证件号码
    private String EdorAppIDNO;
    //申请人证件有效期
    private String EdorAppIdValiDate;
    //申请人手机号
    private String EdorAppPhone;

    @Override
    public String toString() {
        return "EdorAppInfoPojo{" +
                "EdorAppName='" + EdorAppName + '\'' +
                ", EdorAppIDType='" + EdorAppIDType + '\'' +
                ", EdorAppIDNO='" + EdorAppIDNO + '\'' +
                ", EdorAppIdValiDate='" + EdorAppIdValiDate + '\'' +
                ", EdorAppPhone='" + EdorAppPhone + '\'' +
                '}';
    }

    public String getEdorAppName() {
        return EdorAppName;
    }

    public void setEdorAppName(String edorAppName) {
        EdorAppName = edorAppName;
    }

    public String getEdorAppIDType() {
        return EdorAppIDType;
    }

    public void setEdorAppIDType(String edorAppIDType) {
        EdorAppIDType = edorAppIDType;
    }

    public String getEdorAppIDNO() {
        return EdorAppIDNO;
    }

    public void setEdorAppIDNO(String edorAppIDNO) {
        EdorAppIDNO = edorAppIDNO;
    }

    public String getEdorAppIdValiDate() {
        return EdorAppIdValiDate;
    }

    public void setEdorAppIdValiDate(String edorAppIdValiDate) {
        EdorAppIdValiDate = edorAppIdValiDate;
    }

    public String getEdorAppPhone() {
        return EdorAppPhone;
    }

    public void setEdorAppPhone(String edorAppPhone) {
        EdorAppPhone = edorAppPhone;
    }


}
