package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 客户联系地址或邮箱保全项目保单明细查询POS079
 */
public class ReqCustomerAddressEmailChangeQueryPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //标记区分邮箱变更还是地址变更
    private String BussType;
    //证件号
    private String CustomerIDNo;
    //证件姓名
    private String CustomerName;
    //续期专员号
    private String CustomerNo;

    @Override
    public String toString() {
        return "ReqCustomerAddressEmailChangeQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", BussType='" + BussType + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }


}
