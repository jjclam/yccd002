package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * Created by Administrator on 2018/11/6.
 */
public class BankInfoPojo implements  Body,Serializable{

    //银行账号
    private String BankAccNo;
    //银行编码
    private String BankCode;
    //户名
    private String AccName;

    @Override
    public String toString() {
        return "BankInfoPojo{" +
                "BankAccNo='" + BankAccNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", AccName='" + AccName + '\'' +
                '}';
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }


}
