package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;


/**
 * 保全申请状态查询
 */
@Relation(transID = "POS085")
public class RespPreservationStateQueryPojo implements Pojo,Body,Serializable{
    //保单状态
    private String PolicyStatus;
    //保单号
    private String ContNo;
    //保全申请状态
    private String BqStatus;
    //业务类别
    private String EdorType;
    //申请日期
    private String ApplyDate;
    //险种名称
    private String RiskCode;
    //生效日期
    private String ValiDate;

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBqStatus() {
        return BqStatus;
    }

    public void setBqStatus(String bqStatus) {
        BqStatus = bqStatus;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getValiDate() {
        return ValiDate;
    }

    public void setValiDate(String valiDate) {
        ValiDate = valiDate;
    }

    @Override

    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPreservationStateQueryPojo{" +
                "PolicyStatus='" + PolicyStatus + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", BqStatus='" + BqStatus + '\'' +
                ", EdorTye='" + EdorType + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", ValiDate='" + ValiDate + '\'' +
                '}';
    }
}
