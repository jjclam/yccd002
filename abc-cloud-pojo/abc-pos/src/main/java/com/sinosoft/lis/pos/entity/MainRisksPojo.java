package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

public class MainRisksPojo implements Body, Serializable {

    private List<MainRiskPojo> MainRiskPojo;

    @JSONField(name = "mainRisk")
    public List<com.sinosoft.lis.pos.entity.MainRiskPojo> getMainRiskPojo() {
        return MainRiskPojo;
    }

    @JSONField(name = "mainRisk")
    public void setMainRiskPojo(List<com.sinosoft.lis.pos.entity.MainRiskPojo> mainRiskPojo) {
        MainRiskPojo = mainRiskPojo;
    }

    @Override
    public String toString() {
        return "MainRisksPojo{" +
                "MainRiskPojo=" + MainRiskPojo +
                '}';
    }
}