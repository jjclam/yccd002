package com.sinosoft.lis.pos.entity;

import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;

public class PolicyXHQueryInfoPojo implements Body, Serializable {
    //保单申请号
    private String PolicyApplyNo;
    //对账类型
    private String NoticeType;
    //保单申请号
    private String NoticeNo;

    public String getPolicyApplyNo() {
        return PolicyApplyNo;
    }

    public void setPolicyApplyNo(String policyApplyNo) {
        PolicyApplyNo = policyApplyNo;
    }

    public String getNoticeType() {
        return NoticeType;
    }

    public void setNoticeType(String noticeType) {
        NoticeType = noticeType;
    }

    public String getNoticeNo() {
        return NoticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        NoticeNo = noticeNo;
    }

    @Override
    public String toString() {
        return "PolicyXHQueryInfoPojo{" +
                "PolicyApplyNo='" + PolicyApplyNo + '\'' +
                ", NoticeType='" + NoticeType + '\'' +
                ", NoticeNo='" + NoticeNo + '\'' +
                '}';
    }
}