package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 被保人身份证有效期变更明细查询POS054.
 */
public class ReqInsuredIdValiDateDetailsQueryPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }


    @Override
    public String toString() {
        return "ReqInsuredIdValiDateDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}