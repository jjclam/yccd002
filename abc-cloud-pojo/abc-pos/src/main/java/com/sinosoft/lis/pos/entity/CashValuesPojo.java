package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class CashValuesPojo implements Body,Serializable{
    private List<CashValuePojo> CashValuePojo;

    @JSONField(name="cashValue")
    public List<com.sinosoft.lis.pos.entity.CashValuePojo> getCashValuePojo() {
        return CashValuePojo;
    }
    @JSONField(name="cashValue")
    public void setCashValuePojo(List<com.sinosoft.lis.pos.entity.CashValuePojo> cashValuePojo) {
        CashValuePojo = cashValuePojo;
    }


    @Override
    public String toString() {
        return "CashValuesPojo{" +
                "CashValuePojo=" + CashValuePojo +
                '}';
    }
}