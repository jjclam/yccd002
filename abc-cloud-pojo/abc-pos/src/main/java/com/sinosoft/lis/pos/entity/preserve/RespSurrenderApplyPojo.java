package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 退保申请
 */
@Relation(transID = "POS089")
public class RespSurrenderApplyPojo implements Pojo,Body,Serializable{
    //险种名称
    private String RiskName;
    //保单号
    private String ContNo;
    //退保金额
    private String OccurBala;
    //保单生效日
    private String ValidDate;

    @Override
    public String toString() {
        return "ResqSurrenderApplyPojo{" +
                "RiskName='" + RiskName + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", OccurBala='" + OccurBala + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                '}';
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
