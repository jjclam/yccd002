package com.sinosoft.lis.pos.entity.claims;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

;

/**
 * 理赔报案CLM002.
 */
public class ReqClaimReportPojo implements ReqBody, Body, Serializable {
    //报案人姓名
    private String RptorName;
    //报案人电话
    private String RptorPhone;
    //报案人与被保人关系
    private String RptorRelaToInsured;
    //报案方式
    private String RptMode;
    //报案人通讯地址
    private String RptorAddress;
    //邮政编码
    private String RptorPostCode;
    //报案日期
    private String RptDate;
    //管理机构
    private String ManageCom;
    //出险人客户号
    private String OccurCustomerNo;
    //出险原因
    private String OccurReason;
    //医院
    private String Hospital;
    //出险日期
    private String AccidentDate;
    //投保人类别
    private String ApplyType;
    //理赔类型
    private String ClaimType;
    //事故描述
    private String AccidentDesc;
    //证件类型
    private String RptorIDType;
    //证件号码
    private String RptorIDNo;
    //银行帐户名
    private String RptorAccName;
    //银行编码
    private String RptorBankCode;
    //银行帐号
    private String RptorBankAccNo;
    //通讯地址
    private String RptorPostalAddress;
    //业务类型
    private String BussType;
    //人脸识别标记
    private String FaceFlag;
    //管理机构
    private String ManageComCode;

    public String getRptorName() {
        return RptorName;
    }

    public void setRptorName(String rptorName) {
        RptorName = rptorName;
    }

    public String getRptorPhone() {
        return RptorPhone;
    }

    public void setRptorPhone(String rptorPhone) {
        RptorPhone = rptorPhone;
    }

    public String getRptorRelaToInsured() {
        return RptorRelaToInsured;
    }

    public void setRptorRelaToInsured(String rptorRelaToInsured) {
        RptorRelaToInsured = rptorRelaToInsured;
    }

    public String getRptMode() {
        return RptMode;
    }

    public void setRptMode(String rptMode) {
        RptMode = rptMode;
    }

    public String getRptorAddress() {
        return RptorAddress;
    }

    public void setRptorAddress(String rptorAddress) {
        RptorAddress = rptorAddress;
    }

    public String getRptorPostCode() {
        return RptorPostCode;
    }

    public void setRptorPostCode(String rptorPostCode) {
        RptorPostCode = rptorPostCode;
    }

    public String getRptDate() {
        return RptDate;
    }

    public void setRptDate(String rptDate) {
        RptDate = rptDate;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getOccurCustomerNo() {
        return OccurCustomerNo;
    }

    public void setOccurCustomerNo(String occurCustomerNo) {
        OccurCustomerNo = occurCustomerNo;
    }

    public String getOccurReason() {
        return OccurReason;
    }

    public void setOccurReason(String occurReason) {
        OccurReason = occurReason;
    }

    public String getHospital() {
        return Hospital;
    }

    public void setHospital(String hospital) {
        Hospital = hospital;
    }

    public String getAccidentDate() {
        return AccidentDate;
    }

    public void setAccidentDate(String accidentDate) {
        AccidentDate = accidentDate;
    }

    public String getApplyType() {
        return ApplyType;
    }

    public void setApplyType(String applyType) {
        ApplyType = applyType;
    }

    public String getClaimType() {
        return ClaimType;
    }

    public void setClaimType(String claimType) {
        ClaimType = claimType;
    }

    public String getAccidentDesc() {
        return AccidentDesc;
    }

    public void setAccidentDesc(String accidentDesc) {
        AccidentDesc = accidentDesc;
    }

    public String getRptorIDType() {
        return RptorIDType;
    }

    public void setRptorIDType(String rptorIDType) {
        RptorIDType = rptorIDType;
    }

    public String getRptorIDNo() {
        return RptorIDNo;
    }

    public void setRptorIDNo(String rptorIDNo) {
        RptorIDNo = rptorIDNo;
    }

    public String getRptorAccName() {
        return RptorAccName;
    }

    public void setRptorAccName(String rptorAccName) {
        RptorAccName = rptorAccName;
    }

    public String getRptorBankCode() {
        return RptorBankCode;
    }

    public void setRptorBankCode(String rptorBankCode) {
        RptorBankCode = rptorBankCode;
    }

    public String getRptorBankAccNo() {
        return RptorBankAccNo;
    }

    public void setRptorBankAccNo(String rptorBankAccNo) {
        RptorBankAccNo = rptorBankAccNo;
    }

    public String getRptorPostalAddress() {
        return RptorPostalAddress;
    }

    public void setRptorPostalAddress(String rptorPostalAddress) {
        RptorPostalAddress = rptorPostalAddress;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getFaceFlag() {
        return FaceFlag;
    }

    public void setFaceFlag(String faceFlag) {
        FaceFlag = faceFlag;
    }

    public String getManageComCode() {
        return ManageComCode;
    }

    public void setManageComCode(String manageComCode) {
        ManageComCode = manageComCode;
    }

    @Override
    public String toString() {
        return "ReqClaimReportPojo{" +
                "RptorName='" + RptorName + '\'' +
                ", RptorPhone='" + RptorPhone + '\'' +
                ", RptorRelaToInsured='" + RptorRelaToInsured + '\'' +
                ", RptMode='" + RptMode + '\'' +
                ", RptorAddress='" + RptorAddress + '\'' +
                ", RptorPostCode='" + RptorPostCode + '\'' +
                ", RptDate='" + RptDate + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", OccurCustomerNo='" + OccurCustomerNo + '\'' +
                ", OccurReason='" + OccurReason + '\'' +
                ", Hospital='" + Hospital + '\'' +
                ", AccidentDate='" + AccidentDate + '\'' +
                ", ApplyType='" + ApplyType + '\'' +
                ", ClaimType='" + ClaimType + '\'' +
                ", AccidentDesc='" + AccidentDesc + '\'' +
                ", RptorIDType='" + RptorIDType + '\'' +
                ", RptorIDNo='" + RptorIDNo + '\'' +
                ", RptorAccName='" + RptorAccName + '\'' +
                ", RptorBankCode='" + RptorBankCode + '\'' +
                ", RptorBankAccNo='" + RptorBankAccNo + '\'' +
                ", RptorPostalAddress='" + RptorPostalAddress + '\'' +
                ", BussType='" + BussType + '\'' +
                ", FaceFlag='" + FaceFlag + '\'' +
                ", ManageComCode='" + ManageComCode + '\'' +
                '}';
    }
}
