package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

;

/**
 * 电子保单补发NB008.
 */
@Relation(transID = "NB008")
public class RespEleCtronPolicyPojo implements Pojo, Body, Serializable {
    //保单号
    private String ContNo;
    //响应码
    private String RetCode;
    //响应消息
    private String RetMsg;

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getRetCode() {
        return RetCode;
    }

    public void setRetCode(String retCode) {
        RetCode = retCode;
    }

    public String getRetMsg() {
        return RetMsg;
    }

    public void setRetMsg(String retMsg) {
        RetMsg = retMsg;
    }

    @Override
    public String toString() {
        return "RespEleCtronPolicyPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", RetCode='" + RetCode + '\'' +
                ", RetMsg='" + RetMsg + '\'' +
                '}';
    }
}