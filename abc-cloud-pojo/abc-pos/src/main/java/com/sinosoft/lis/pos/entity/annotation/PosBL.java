package com.sinosoft.lis.pos.entity.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * Created by Administrator on 2018/11/6.
 */
@Component
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PosBL {

    String transID();

    String edorType() default "undefined";
}
