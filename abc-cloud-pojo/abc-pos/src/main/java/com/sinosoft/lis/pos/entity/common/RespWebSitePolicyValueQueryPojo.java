package com.sinosoft.lis.pos.entity.common;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 官网，微信 保单价值查询
 */
@Relation(transID = "PC007")
public class RespWebSitePolicyValueQueryPojo implements Pojo, Body, Serializable {
    //银行订单号
    private String ABCOrderId;
    //是否允许部分支取
    private String IsPartWithdraw;
    //保单账户现金价值
    private String PolicyValue;
    //是否可冲正
    private String IsReversal;
    //全额情况下是否必须退保
    private String IsMustCancel;
    //保单状态
    private String PolicyStatus;
    //现金价值
    private String CashValue;
    //部分领取最大金额
    private String MaxPartValue;

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getIsPartWithdraw() {
        return IsPartWithdraw;
    }

    public void setIsPartWithdraw(String isPartWithdraw) {
        IsPartWithdraw = isPartWithdraw;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public String getIsReversal() {
        return IsReversal;
    }

    public void setIsReversal(String isReversal) {
        IsReversal = isReversal;
    }

    public String getIsMustCancel() {
        return IsMustCancel;
    }

    public void setIsMustCancel(String isMustCancel) {
        IsMustCancel = isMustCancel;
    }

    public String getPolicyStatus() {
        return PolicyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        PolicyStatus = policyStatus;
    }

    public String getCashValue() {
        return CashValue;
    }

    public void setCashValue(String cashValue) {
        CashValue = cashValue;
    }

    public String getMaxPartValue() {
        return MaxPartValue;
    }

    public void setMaxPartValue(String maxPartValue) {
        MaxPartValue = maxPartValue;
    }

    @Override
    public String toString() {
        return "RespWebSitePolicyValueQueryPojo{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", IsPartWithdraw='" + IsPartWithdraw + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", IsReversal='" + IsReversal + '\'' +
                ", IsMustCancel='" + IsMustCancel + '\'' +
                ", PolicyStatus='" + PolicyStatus + '\'' +
                ", CashValue='" + CashValue + '\'' +
                ", MaxPartValue='" + MaxPartValue + '\'' +
                '}';
    }
}