package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;

public class Addt implements Serializable {
    //险种名称
    private String Name;
    //投保份数
    private String Share;
    //保费
    private String Prem;
    //保额
    private String Beamt;
    //保单价值
    private String PolicyValue;
    //当前账户价值
    private String AccountValue;
    //缴费年期
    private String PayDueDate;
    //缴费方式
    private String PayType;
    //保单到期日
    private String PolicyEndDate;
    //保险期间
    private String InsuDueDate;
    //险种代码
    private String RiskCode;
    //附加记录个数
    private String PrntCount;
    //附加内容
    private String Prnt1;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getShare() {
        return Share;
    }

    public void setShare(String share) {
        Share = share;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getBeamt() {
        return Beamt;
    }

    public void setBeamt(String beamt) {
        Beamt = beamt;
    }

    public String getPolicyValue() {
        return PolicyValue;
    }

    public void setPolicyValue(String policyValue) {
        PolicyValue = policyValue;
    }

    public String getAccountValue() {
        return AccountValue;
    }

    public void setAccountValue(String accountValue) {
        AccountValue = accountValue;
    }

    public String getPayDueDate() {
        return PayDueDate;
    }

    public void setPayDueDate(String payDueDate) {
        PayDueDate = payDueDate;
    }

    public String getPayType() {
        return PayType;
    }

    public void setPayType(String payType) {
        PayType = payType;
    }

    public String getPolicyEndDate() {
        return PolicyEndDate;
    }

    public void setPolicyEndDate(String policyEndDate) {
        PolicyEndDate = policyEndDate;
    }

    public String getInsuDueDate() {
        return InsuDueDate;
    }

    public void setInsuDueDate(String insuDueDate) {
        InsuDueDate = insuDueDate;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getPrntCount() {
        return PrntCount;
    }

    public void setPrntCount(String prntCount) {
        PrntCount = prntCount;
    }

    public String getPrnt1() {
        return Prnt1;
    }

    public void setPrnt1(String prnt1) {
        Prnt1 = prnt1;
    }

    @Override
    public String toString() {
        return "Addt{" +
                "Name='" + Name + '\'' +
                ", Share='" + Share + '\'' +
                ", Prem='" + Prem + '\'' +
                ", Beamt='" + Beamt + '\'' +
                ", PolicyValue='" + PolicyValue + '\'' +
                ", AccountValue='" + AccountValue + '\'' +
                ", PayDueDate='" + PayDueDate + '\'' +
                ", PayType='" + PayType + '\'' +
                ", PolicyEndDate='" + PolicyEndDate + '\'' +
                ", InsuDueDate='" + InsuDueDate + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", PrntCount='" + PrntCount + '\'' +
                ", Prnt1='" + Prnt1 + '\'' +
                '}';
    }
}