package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 红利领取要操作保全用户详情POS025.4003保单红利账户领取查询,4007官网保单红利账户领取明细查询POS102
 */
public class ReqBonusUserDetailsPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //
    private String EdorAcceptNo;
    //保单号
    private String ContNo;

    @Override
    public String toString() {
        return "ReqBonusUserDetailsPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }


}