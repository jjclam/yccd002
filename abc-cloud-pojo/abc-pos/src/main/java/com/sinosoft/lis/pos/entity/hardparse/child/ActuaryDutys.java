package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class ActuaryDutys {
    private List<ActuaryDuty>ActuaryDuty;

    @Override
    public String toString() {
        return "ActuaryDutys{" +
                "ActuaryDuty=" + ActuaryDuty +
                '}';
    }

    public List<ActuaryDuty> getActuaryDuty() {
        return ActuaryDuty;
    }

    public void setActuaryDuty(List<ActuaryDuty> actuaryDuty) {
        ActuaryDuty = actuaryDuty;
    }
}
