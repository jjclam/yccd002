package com.sinosoft.lis.pos.entity.response;


import java.io.Serializable;

/**
 * Created by Administrator on 2018/11/6.
 */
public class RespHeadPojo implements Serializable {
    //版本号
    private String Version;
    //交易流水号
    private String SerialNo;
    //交易编码
    private String TransID;
    //响应日期
    private String TransDate;
    //响应时间
    private String TransTime;
    //请求系统
    private String SysCode;
    //渠道
    private String Channel;
    //返回码
    private String RespFlag;
    //错误信息
    private String RespDesc;

    @Override
    public String toString() {
        return "RespHeadPojo{" +
                "Version='" + Version + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", TransID='" + TransID + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", SysCode='" + SysCode + '\'' +
                ", Channel='" + Channel + '\'' +
                ", RespFlag='" + RespFlag + '\'' +
                ", RespDesc='" + RespDesc + '\'' +
                '}';
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getTransID() {
        return TransID;
    }

    public void setTransID(String transID) {
        TransID = transID;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getSysCode() {
        return SysCode;
    }

    public void setSysCode(String sysCode) {
        SysCode = sysCode;
    }

    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }

    public String getRespFlag() {
        return RespFlag;
    }

    public void setRespFlag(String respFlag) {
        RespFlag = respFlag;
    }

    public String getRespDesc() {
        return RespDesc;
    }

    public void setRespDesc(String respDesc) {
        RespDesc = respDesc;
    }
}
