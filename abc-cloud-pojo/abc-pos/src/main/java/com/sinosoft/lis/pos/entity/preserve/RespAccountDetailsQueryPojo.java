package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.BalanceInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

;

/**
 * 万能账户价值详情查询POS033  官网4003官网微信万能账户价值详情查询.
 */
@Relation(transID = "POS033")
public class RespAccountDetailsQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //累积账户价值
    private String AccountBalance;
    private BalanceInfosPojo BalanceInfosPojo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAccountBalance() {
        return AccountBalance;
    }

    public void setAccountBalance(String accountBalance) {
        AccountBalance = accountBalance;
    }

    public com.sinosoft.lis.pos.entity.BalanceInfosPojo getBalanceInfosPojo() {
        return BalanceInfosPojo;
    }

    @JSONField(name = "balanceInfos")
    public void setBalanceInfosPojo(com.sinosoft.lis.pos.entity.BalanceInfosPojo balanceInfosPojo) {
        BalanceInfosPojo = balanceInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespAccountDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AccountBalance='" + AccountBalance + '\'' +
                ", BalanceInfosPojo=" + BalanceInfosPojo +
                '}';
    }
}