package com.sinosoft.lis.pos.entity.enums;

/**
 * 保全核心转码枚举类
 */
public enum PosChangeEnum {
    ABC("ABCOTC"),ABC_EBank("ABCEBANK"),ABC_SST("ABCSELF"),ABC_Mobile("ABCMOBILE"),ABC_Supotc("ABCSUPOTC");
    private String Channel;

    PosChangeEnum(String channel) {
        Channel = channel;
    }

    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }
    public static boolean contains(String type){
        for(PosEnum posEnum:PosEnum.values()){
            if(posEnum.name().equals(type)){
                return true;
            }
        }
        return false;
    }
}
