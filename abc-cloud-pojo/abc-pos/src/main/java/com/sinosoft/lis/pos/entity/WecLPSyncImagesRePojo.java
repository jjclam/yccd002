package com.sinosoft.lis.pos.entity;

import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;

public class WecLPSyncImagesRePojo implements Body, Serializable {
    //理赔号
    private String ClaimNo;
    //状态
    private String Status;

    public String getClaimNo() {
        return ClaimNo;
    }

    public void setClaimNo(String claimNo) {
        ClaimNo = claimNo;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "WecLPSyncImagesRePojo{" +
                "ClaimNo='" + ClaimNo + '\'' +
                ", Status='" + Status + '\'' +
                '}';
    }
}