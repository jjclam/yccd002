package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;

public class Objt  implements Serializable {
    //保险房屋地址(省/直辖市)
    private String Prov;
    //保险房屋地址(市)
    private String City;
    //保险房屋地址(区/县)
    private String Zone;
    //保险房屋地址(具体地址)
    private String Address;
    //房屋保险金额
    private String Amnt;
    //保险房屋邮编
    private String ZipCode;
    //保险房屋面积
    private String Area;
    //防盗抢设施
    private String PreFlag;
    //免赔率
    private String NoPayPortion;
    //免赔
    private String NoPayPrem;
    //保险房屋结构
    private String Struts;
    //保险房屋用途
    private String Usage;
    //固定资产分项保险金额
    private String FasAmnt;
    //流动资产分项保险金额
    private String FloAmnt;
    //保险目标个数
    private String Count;
    //保险目标名称1
    private String Name1;
    //保险目标金额1
    private String Amnt1;
    //保险目标名称2
    private String Name2;
    //保险目标金额2
    private String Amnt2;
    //保险目标名称3
    private String Name3;
    //保险目标金额3
    private String Amnt3;
    //保险目标名称4
    private String Name4;
    //保险目标金额4
    private String Amnt4;
    //保险目标名称5
    private String Name5;
    //保险目标金额5
    private String Amnt5;
    //保险目标名称6
    private String Name6;
    //保险目标金额6
    private String Amnt6;
    //保险目标名称7
    private String Name7;
    //保险目标金额7
    private String Amnt7;

    public String getProv() {
        return Prov;
    }

    public void setProv(String prov) {
        Prov = prov;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getZone() {
        return Zone;
    }

    public void setZone(String zone) {
        Zone = zone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getPreFlag() {
        return PreFlag;
    }

    public void setPreFlag(String preFlag) {
        PreFlag = preFlag;
    }

    public String getNoPayPortion() {
        return NoPayPortion;
    }

    public void setNoPayPortion(String noPayPortion) {
        NoPayPortion = noPayPortion;
    }

    public String getNoPayPrem() {
        return NoPayPrem;
    }

    public void setNoPayPrem(String noPayPrem) {
        NoPayPrem = noPayPrem;
    }

    public String getStruts() {
        return Struts;
    }

    public void setStruts(String struts) {
        Struts = struts;
    }

    public String getUsage() {
        return Usage;
    }

    public void setUsage(String usage) {
        Usage = usage;
    }

    public String getFasAmnt() {
        return FasAmnt;
    }

    public void setFasAmnt(String fasAmnt) {
        FasAmnt = fasAmnt;
    }

    public String getFloAmnt() {
        return FloAmnt;
    }

    public void setFloAmnt(String floAmnt) {
        FloAmnt = floAmnt;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public String getName1() {
        return Name1;
    }

    public void setName1(String name1) {
        Name1 = name1;
    }

    public String getAmnt1() {
        return Amnt1;
    }

    public void setAmnt1(String amnt1) {
        Amnt1 = amnt1;
    }

    public String getName2() {
        return Name2;
    }

    public void setName2(String name2) {
        Name2 = name2;
    }

    public String getAmnt2() {
        return Amnt2;
    }

    public void setAmnt2(String amnt2) {
        Amnt2 = amnt2;
    }

    public String getName3() {
        return Name3;
    }

    public void setName3(String name3) {
        Name3 = name3;
    }

    public String getAmnt3() {
        return Amnt3;
    }

    public void setAmnt3(String amnt3) {
        Amnt3 = amnt3;
    }

    public String getName4() {
        return Name4;
    }

    public void setName4(String name4) {
        Name4 = name4;
    }

    public String getAmnt4() {
        return Amnt4;
    }

    public void setAmnt4(String amnt4) {
        Amnt4 = amnt4;
    }

    public String getName5() {
        return Name5;
    }

    public void setName5(String name5) {
        Name5 = name5;
    }

    public String getAmnt5() {
        return Amnt5;
    }

    public void setAmnt5(String amnt5) {
        Amnt5 = amnt5;
    }

    public String getName6() {
        return Name6;
    }

    public void setName6(String name6) {
        Name6 = name6;
    }

    public String getAmnt6() {
        return Amnt6;
    }

    public void setAmnt6(String amnt6) {
        Amnt6 = amnt6;
    }

    public String getName7() {
        return Name7;
    }

    public void setName7(String name7) {
        Name7 = name7;
    }

    public String getAmnt7() {
        return Amnt7;
    }

    public void setAmnt7(String amnt7) {
        Amnt7 = amnt7;
    }

    @Override
    public String toString() {
        return "Objt{" +
                "Prov='" + Prov + '\'' +
                ", City='" + City + '\'' +
                ", Zone='" + Zone + '\'' +
                ", Address='" + Address + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Area='" + Area + '\'' +
                ", PreFlag='" + PreFlag + '\'' +
                ", NoPayPortion='" + NoPayPortion + '\'' +
                ", NoPayPrem='" + NoPayPrem + '\'' +
                ", Struts='" + Struts + '\'' +
                ", Usage='" + Usage + '\'' +
                ", FasAmnt='" + FasAmnt + '\'' +
                ", FloAmnt='" + FloAmnt + '\'' +
                ", Count='" + Count + '\'' +
                ", Name1='" + Name1 + '\'' +
                ", Amnt1='" + Amnt1 + '\'' +
                ", Name2='" + Name2 + '\'' +
                ", Amnt2='" + Amnt2 + '\'' +
                ", Name3='" + Name3 + '\'' +
                ", Amnt3='" + Amnt3 + '\'' +
                ", Name4='" + Name4 + '\'' +
                ", Amnt4='" + Amnt4 + '\'' +
                ", Name5='" + Name5 + '\'' +
                ", Amnt5='" + Amnt5 + '\'' +
                ", Name6='" + Name6 + '\'' +
                ", Amnt6='" + Amnt6 + '\'' +
                ", Name7='" + Name7 + '\'' +
                ", Amnt7='" + Amnt7 + '\'' +
                '}';
    }
}