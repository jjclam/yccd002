package com.sinosoft.lis.pos.entity.request;

/**
 * Created by Administrator on 2018/11/6.
 */
public class ReqRoot {

    private ReqHeadPojo head;
    private ReqBody body;

    @Override
    public String toString() {
        return "ReqRoot{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }

    public ReqHeadPojo getHead() {
        return head;
    }

    public void setHead(ReqHeadPojo head) {
        this.head = head;
    }

    public ReqBody getBody() {
        return body;
    }

    public void setBody(ReqBody body) {
        this.body = body;
    }
}
