package com.sinosoft.lis.pos.entity.claims;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.IdentityVerifyInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 身份验证环节CLM001.
 */

@Relation(transID = "CLM001")
public class RespAuthenticationPojo implements Pojo,Body,Serializable{
    private com.sinosoft.lis.pos.entity.IdentityVerifyInfosPojo IdentityVerifyInfosPojo;

    public IdentityVerifyInfosPojo getIdentityVerifyInfosPojo() {
        return IdentityVerifyInfosPojo;
    }
    @JSONField(name ="identityVerifyInfos" )
    public void setIdentityVerifyInfosPojo(IdentityVerifyInfosPojo identityVerifyInfosPojo) {
        IdentityVerifyInfosPojo = identityVerifyInfosPojo;
    }

    @Override
    public String toString() {
        return "RespAuthenticationPojo{" +
                "IdentityVerifyInfosPojo=" + IdentityVerifyInfosPojo +
                '}';
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
