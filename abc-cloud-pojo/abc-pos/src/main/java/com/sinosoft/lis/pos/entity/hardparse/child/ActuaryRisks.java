package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class ActuaryRisks {
    private List<ActuaryRisk> ActuaryRisk;

    public List<com.sinosoft.lis.pos.entity.hardparse.child.ActuaryRisk> getActuaryRisk() {
        return ActuaryRisk;
    }

    public void setActuaryRisk(List<com.sinosoft.lis.pos.entity.hardparse.child.ActuaryRisk> actuaryRisk) {
        ActuaryRisk = actuaryRisk;
    }

    @Override
    public String toString() {
        return "ActuaryRisks{" +
                "ActuaryRisk=" + ActuaryRisk +
                '}';
    }
}
