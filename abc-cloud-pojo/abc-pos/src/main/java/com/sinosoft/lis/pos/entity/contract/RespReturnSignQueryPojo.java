package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;


/**
 *微信回执签收查询NB002.
 */
@Relation(transID = "NB002")
public class RespReturnSignQueryPojo implements Pojo, Body, Serializable {
    //投保人姓名
    private String AppntName;

    @Override
    public String toString() {
        return "RespReturnSignQueryPojo{" +
                "AppntName='" + AppntName + '\'' +
                '}';
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

}