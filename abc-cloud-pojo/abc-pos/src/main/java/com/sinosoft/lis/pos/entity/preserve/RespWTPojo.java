package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

;

/**
 * 4004保单犹豫期退保申请，官网4004保单保全申请请求
 */
@Relation(transID = "POS018")
public class RespWTPojo implements Pojo,Body,Serializable{
    //保全受理号
    private String EdorAcceptNo;
    //保单号
    private String ContNo;
    //保全项目编码
    private String EdorType;
    //保全批单号
    private String EdorNo;
    //保全状态
    private String EdorStatus;
    //退保金额
    private String OccurBala;
    //保全生效日
    private String BqValidDate;
    //险种名称
    private String RiskName;


    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getEdorStatus() {
        return EdorStatus;
    }

    public void setEdorStatus(String edorStatus) {
        EdorStatus = edorStatus;
    }

    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getBqValidDate() {
        return BqValidDate;
    }

    public void setBqValidDate(String bqValidDate) {
        BqValidDate = bqValidDate;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespWTPojo{" +
                "EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorType='" + EdorType + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", EdorStatus='" + EdorStatus + '\'' +
                ", OccurBala='" + OccurBala + '\'' +
                ", BqValidDate='" + BqValidDate + '\'' +
                ", RiskName='" + RiskName + '\'' +
                '}';
    }
}
