package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 投保人证件有效期校验POS058
 */
public class ReqAppntIDCardChangeCheckPojo implements ReqBody, Body,Serializable{
    //身份证件号码
    private String CustomerIDNo;
    //姓名
    private String CustomerName;

    @Override
    public String toString() {
        return "ReqAppntIDCardChangeCheckPojo{" +
                "CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }


}
