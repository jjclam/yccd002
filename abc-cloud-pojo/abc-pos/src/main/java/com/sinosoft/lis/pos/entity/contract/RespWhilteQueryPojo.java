package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 白名单查询NB004.
 */
@Relation(transID = "NB004")
public class RespWhilteQueryPojo implements Pojo,Body,Serializable{
    //客户是否已存在
    private String DealResult;
    //提示信息
    private String ErrorMessage;

    public String getDealResult() {
        return DealResult;
    }

    public void setDealResult(String dealResult) {
        DealResult = dealResult;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        ErrorMessage = errorMessage;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespWhilteQueryPojo{" +
                "DealResult='" + DealResult + '\'' +
                ", ErrorMessage='" + ErrorMessage + '\'' +
                '}';
    }
}