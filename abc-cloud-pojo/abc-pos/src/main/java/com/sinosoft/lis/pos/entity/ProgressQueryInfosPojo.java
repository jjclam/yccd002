package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class ProgressQueryInfosPojo implements Body,Serializable{

    private List<ProgressQueryInfoPojo> ProgressQueryInfoPojo;

    @Override
    public String toString() {
        return "ProgressQueryInfosPojo{" +
                "ProgressQueryInfoPojo=" + ProgressQueryInfoPojo +
                '}';
    }

    public List<ProgressQueryInfoPojo> getProgressQueryInfoPojo() {
        return ProgressQueryInfoPojo;
    }
    @JSONField(name = "progressQueryInfo")
    public void setProgressQueryInfoPojo(List<ProgressQueryInfoPojo> progressQueryInfoPojo) {
        ProgressQueryInfoPojo = progressQueryInfoPojo;
    }

}
