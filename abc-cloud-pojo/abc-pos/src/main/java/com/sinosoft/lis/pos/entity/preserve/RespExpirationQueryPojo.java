package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4003保单生存金领取(AGSQ)查询和满期查询POS043  官网4007官网保单生存金(AG/SQ)领取明细查询 官网4003生存金领取查询.
 */
@Relation(transID = {"POS043", "POS114"})
public class RespExpirationQueryPojo implements Pojo, Body, Serializable {
    //险种名称
    private String RiskName;
    //退保金额
    private String OccurBala;
    //保单生效日
    private String ValidDate;
    //保单到期日
    private String ExpireDate;
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //申请日期
    private String ApplyDate;
    //投保人姓名
    private String AppntName;
    //涉及险种
    private String RelationRisk;
    //开户银行
    private String NewBankName;
    //银行帐号
    private String NewBankAccNo;
    //户名
    private String NewBankAccName;
    //账户余额/累积生息账户余额
    private String AccountBalance;
    //手机电话号码
    private String MobilePhone;
    //领取金额/万能账户余额/getMoney/账户价值/追加保费金额/退保金额
    private String GetMoney;
    //主险编码
    private String MainriskCode;
    //主险名称
    private String MainriskName;
    //被保人姓名
    private String InsuredName;
    //保额
    private String Amnt;
    //保费
    private String Prem;
    //份数
    private String Mult;
    //生存金领取标志/续保标记
    private String XBFlag;
    //生存金领取方式/续保方式
    private String XBPattern;
    //赔付日期/缴费对应日
    private String PayDate;
    //现金价值
    private String CashValue;
    //健康加费
    private String Healthprem;
    //职业加费
    private String Jobprem;
    //给付金额
    private String RiskgetMoney;
    //附加险
    private com.sinosoft.lis.pos.entity.AddtRisksPojo AddtRisksPojo;

    @Override
    public String toString() {
        return "RespExpirationQueryPojo{" +
                "RiskName='" + RiskName + '\'' +
                ", OccurBala='" + OccurBala + '\'' +
                ", ValidDate='" + ValidDate + '\'' +
                ", ExpireDate='" + ExpireDate + '\'' +
                ", EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", ApplyDate='" + ApplyDate + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", RelationRisk='" + RelationRisk + '\'' +
                ", NewBankName='" + NewBankName + '\'' +
                ", NewBankAccNo='" + NewBankAccNo + '\'' +
                ", NewBankAccName='" + NewBankAccName + '\'' +
                ", AccountBalance='" + AccountBalance + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", MainriskCode='" + MainriskCode + '\'' +
                ", MainriskName='" + MainriskName + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", Prem='" + Prem + '\'' +
                ", Mult='" + Mult + '\'' +
                ", XBFlag='" + XBFlag + '\'' +
                ", XBPattern='" + XBPattern + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", CashValue='" + CashValue + '\'' +
                ", Healthprem='" + Healthprem + '\'' +
                ", Jobprem='" + Jobprem + '\'' +
                ", RiskgetMoney='" + RiskgetMoney + '\'' +
                ", AddtRisksPojo=" + AddtRisksPojo +
                '}';
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getValidDate() {
        return ValidDate;
    }

    public void setValidDate(String validDate) {
        ValidDate = validDate;
    }

    public String getExpireDate() {
        return ExpireDate;
    }

    public void setExpireDate(String expireDate) {
        ExpireDate = expireDate;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public void setApplyDate(String applyDate) {
        ApplyDate = applyDate;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getRelationRisk() {
        return RelationRisk;
    }

    public void setRelationRisk(String relationRisk) {
        RelationRisk = relationRisk;
    }

    public String getNewBankName() {
        return NewBankName;
    }

    public void setNewBankName(String newBankName) {
        NewBankName = newBankName;
    }

    public String getNewBankAccNo() {
        return NewBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        NewBankAccNo = newBankAccNo;
    }

    public String getNewBankAccName() {
        return NewBankAccName;
    }

    public void setNewBankAccName(String newBankAccName) {
        NewBankAccName = newBankAccName;
    }

    public String getAccountBalance() {
        return AccountBalance;
    }

    public void setAccountBalance(String accountBalance) {
        AccountBalance = accountBalance;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getMainriskCode() {
        return MainriskCode;
    }

    public void setMainriskCode(String mainriskCode) {
        MainriskCode = mainriskCode;
    }

    public String getMainriskName() {
        return MainriskName;
    }

    public void setMainriskName(String mainriskName) {
        MainriskName = mainriskName;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getMult() {
        return Mult;
    }

    public void setMult(String mult) {
        Mult = mult;
    }

    public String getXBFlag() {
        return XBFlag;
    }

    public void setXBFlag(String XBFlag) {
        this.XBFlag = XBFlag;
    }

    public String getXBPattern() {
        return XBPattern;
    }

    public void setXBPattern(String XBPattern) {
        this.XBPattern = XBPattern;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getCashValue() {
        return CashValue;
    }

    public void setCashValue(String cashValue) {
        CashValue = cashValue;
    }

    public String getHealthprem() {
        return Healthprem;
    }

    public void setHealthprem(String healthprem) {
        Healthprem = healthprem;
    }

    public String getJobprem() {
        return Jobprem;
    }

    public void setJobprem(String jobprem) {
        Jobprem = jobprem;
    }

    public String getRiskgetMoney() {
        return RiskgetMoney;
    }

    public void setRiskgetMoney(String riskgetMoney) {
        RiskgetMoney = riskgetMoney;
    }

    public com.sinosoft.lis.pos.entity.AddtRisksPojo getAddtRisksPojo() {
        return AddtRisksPojo;
    }

    @JSONField(name = "addtRisks")
    public void setAddtRisksPojo(com.sinosoft.lis.pos.entity.AddtRisksPojo addtRisksPojo) {
        AddtRisksPojo = addtRisksPojo;
    }

    @Override

    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
