package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.WXContInfoRePojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;


/**
 * 换发纸质保单查询POS094.
 */
@Relation(transID = "POS094")
public class RespBarterpaperBumfQueryPojo implements Pojo, Body, Serializable {

    private WXContInfoRePojo WXContInfoRePojo;

    public com.sinosoft.lis.pos.entity.WXContInfoRePojo getWXContInfoRePojo() {
        return WXContInfoRePojo;
    }

    @JSONField(name = "wXContInfoRe")
    public void setWXContInfoRePojo(com.sinosoft.lis.pos.entity.WXContInfoRePojo WXContInfoRePojo) {
        this.WXContInfoRePojo = WXContInfoRePojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespBarterpaperBumfQueryPojo{" +
                "WXContInfoRePojo=" + WXContInfoRePojo +
                '}';
    }
}