package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 特别约定（出境告知）保全项目保单明细查询POS080
 */
@Relation(transID = "POS080")
public class RespSpecialConventionChangeQueryPojo implements Pojo,Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //特别约定信息
    private String SPMessage;

    @Override
    public String toString() {
        return "RespSpecialConventionChangeQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", SPMessage='" + SPMessage + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getSPMessage() {
        return SPMessage;
    }

    public void setSPMessage(String SPMessage) {
        this.SPMessage = SPMessage;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
