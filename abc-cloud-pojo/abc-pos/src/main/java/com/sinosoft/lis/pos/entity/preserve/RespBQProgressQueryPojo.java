package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;


/**
 * 保全进度查询
 */
@Relation(transID = "POS015")
public class RespBQProgressQueryPojo implements Pojo, Body, Serializable {

    private com.sinosoft.lis.pos.entity.ProgressQueryInfosPojo ProgressQueryInfosPojo;

    @Override
    public String toString() {
        return "RespBQProgressQueryPojo{" +
                "ProgressQueryInfosPojo=" + ProgressQueryInfosPojo +
                '}';
    }

    public com.sinosoft.lis.pos.entity.ProgressQueryInfosPojo getProgressQueryInfosPojo() {
        return ProgressQueryInfosPojo;
    }

    @JSONField(name = "progressQueryInfos")
    public void setProgressQueryInfosPojo(com.sinosoft.lis.pos.entity.ProgressQueryInfosPojo progressQueryInfosPojo) {
        ProgressQueryInfosPojo = progressQueryInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
