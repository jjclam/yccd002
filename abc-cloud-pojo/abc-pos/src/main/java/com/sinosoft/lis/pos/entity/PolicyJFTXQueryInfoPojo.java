package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class PolicyJFTXQueryInfoPojo implements Body,Serializable{
    //保单号
    private String ContNo;
    //支付日期
    private String PayDate;
    //保费
    private String Prem;
    //银行名称
    private String BankName;
    //银行账户号
    private String BankAccNo;
    //代理人姓名
    private String AgentName;
    //代理人电话
    private String AgentPhone;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getAgentName() {
        return AgentName;
    }

    public void setAgentName(String agentName) {
        AgentName = agentName;
    }

    public String getAgentPhone() {
        return AgentPhone;
    }

    public void setAgentPhone(String agentPhone) {
        AgentPhone = agentPhone;
    }

    @Override
    public String toString() {
        return "PolicyJFTXQueryInfoPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", Prem='" + Prem + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", AgentName='" + AgentName + '\'' +
                ", AgentPhone='" + AgentPhone + '\'' +
                '}';
    }
}