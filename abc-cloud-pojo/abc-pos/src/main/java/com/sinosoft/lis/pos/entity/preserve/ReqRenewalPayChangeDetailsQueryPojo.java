package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 *4002查询保单列表POS062 4007官网保单保全明细查询
 */
public class ReqRenewalPayChangeDetailsQueryPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    @Override
    public String toString() {
        return "ReqRenewalPayChangeDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

}
