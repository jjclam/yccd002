package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 生存给付领取方式保全项目保单明细查询POS040.
 */
public class ReqPreservePolicyDetailsQueryPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //业务类型
    private String BussType;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    @Override
    public String toString() {
        return "ReqPreservePolicyDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", BussType='" + BussType + '\'' +
                '}';
    }
}