/**
 * Copyright (c) 2017 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.pos.entity.group;


import com.sinosoft.utility.Pojo;

import java.io.Serializable;

public class BQApplyPojo implements Pojo,Serializable{
    // @Field
    /**
     * 旧保单号
     */
    private String AppNo;
    /**
     * 险种名称
     */
    private String PolicyNo;
    /**
     * 转保金额
     */
    private String IDNo;
    /**
     * 保费(应缴金额)
     */
    private String IDType;
    /**
     * 保单印刷号
     */
    private String PrintCode;
    /**
     * 申请人姓名
     */
    private String ClientName;
    private String PolicyPwd;
    /**
     * 领款人姓名
     */
    private String PayeetName;
    /**
     * 领款人证件类型
     */
    private String PayeeIdKind;
    /**
     * 领款人证件号码
     */
    private String PayeeIdCode;
    /**
     * 账（卡）号
     */
    private String PayAcc;
    /**
     * 保费
     */
    private double Amt;
    /**
     * 业务类别
     */
    private String BusiType;
    /**
     * 领取金额
     */
    private double GetAmt;
    /**
     * 图片文件名
     */
    private String PicFileName;
    /**
     * 保全申请方式
     */
    private String AppType;

    public String getAppType() {
        return AppType;
    }

    public void setAppType(String appType) {
        AppType = appType;
    }

    public String getAppNo() {
        return AppNo;
    }

    public void setAppNo(String appNo) {
        AppNo = appNo;
    }

    public String getPolicyNo() {
        return PolicyNo;
    }

    public void setPolicyNo(String policyNo) {
        PolicyNo = policyNo;
    }


    public String getPolicyPwd() {
        return PolicyPwd;
    }

    public void setPolicyPwd(String policyPwd) {
        PolicyPwd = policyPwd;
    }

    public String getPayeetName() {
        return PayeetName;
    }

    public void setPayeetName(String payeetName) {
        PayeetName = payeetName;
    }

    public String getPayeeIdKind() {
        return PayeeIdKind;
    }

    public void setPayeeIdKind(String payeeIdKind) {
        PayeeIdKind = payeeIdKind;
    }

    public String getPayeeIdCode() {
        return PayeeIdCode;
    }

    public void setPayeeIdCode(String payeeIdCode) {
        PayeeIdCode = payeeIdCode;
    }

    public String getPayAcc() {
        return PayAcc;
    }

    public void setPayAcc(String payAcc) {
        PayAcc = payAcc;
    }

    public String getBusiType() {
        return BusiType;
    }

    public void setBusiType(String busiType) {
        BusiType = busiType;
    }

    public String getPicFileName() {
        return PicFileName;
    }

    public void setPicFileName(String picFileName) {
        PicFileName = picFileName;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public double getAmt() {
        return Amt;
    }

    public void setAmt(double amt) {
        Amt = amt;
    }

    public double getGetAmt() {
        return GetAmt;
    }

    public void setGetAmt(double getAmt) {
        GetAmt = getAmt;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getPrintCode() {
        return PrintCode;
    }

    public void setPrintCode(String printCode) {
        PrintCode = printCode;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    @Override
    public String toString() {
        return "BQApplyPojo{" +
                "AppNo='" + AppNo + '\'' +
                ", PolicyNo='" + PolicyNo + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", IDType='" + IDType + '\'' +
                ", PrintCode='" + PrintCode + '\'' +
                ", ClientName='" + ClientName + '\'' +
                ", PolicyPwd='" + PolicyPwd + '\'' +
                ", PayeetName='" + PayeetName + '\'' +
                ", PayeeIdKind='" + PayeeIdKind + '\'' +
                ", PayeeIdCode='" + PayeeIdCode + '\'' +
                ", PayAcc='" + PayAcc + '\'' +
                ", Amt=" + Amt +
                ", BusiType='" + BusiType + '\'' +
                ", GetAmt=" + GetAmt +
                ", PicFileName='" + PicFileName + '\'' +
                ", AppType='" + AppType + '\'' +
                '}';
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
