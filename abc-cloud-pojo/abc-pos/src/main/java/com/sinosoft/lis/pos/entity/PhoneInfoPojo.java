package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class PhoneInfoPojo implements Body,Serializable{
    //手机号
    private String MobilePhone;

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    @Override
    public String toString() {
        return "PhoneInfoPojo{" +
                "MobilePhone='" + MobilePhone + '\'' +
                '}';
    }

}
