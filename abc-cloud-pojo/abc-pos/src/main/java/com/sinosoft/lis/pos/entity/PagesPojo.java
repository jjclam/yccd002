package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class PagesPojo implements Body,Serializable{
    private String PageCode;
    private String PageName;
    private String PageSuffix;
    private String PicPathFTP;
    private String Page_URL;

    @Override
    public String toString() {
        return "PagesPojo{" +
                "PageCode='" + PageCode + '\'' +
                ", PageName='" + PageName + '\'' +
                ", PageSuffix='" + PageSuffix + '\'' +
                ", PicPathFTP='" + PicPathFTP + '\'' +
                ", Page_URL='" + Page_URL + '\'' +
                '}';
    }

    public String getPageCode() {
        return PageCode;
    }

    public void setPageCode(String pageCode) {
        PageCode = pageCode;
    }

    public String getPageName() {
        return PageName;
    }

    public void setPageName(String pageName) {
        PageName = pageName;
    }

    public String getPageSuffix() {
        return PageSuffix;
    }

    public void setPageSuffix(String pageSuffix) {
        PageSuffix = pageSuffix;
    }

    public String getPicPathFTP() {
        return PicPathFTP;
    }

    public void setPicPathFTP(String picPathFTP) {
        PicPathFTP = picPathFTP;
    }

    public String getPage_URL() {
        return Page_URL;
    }

    public void setPage_URL(String page_URL) {
        Page_URL = page_URL;
    }


}
