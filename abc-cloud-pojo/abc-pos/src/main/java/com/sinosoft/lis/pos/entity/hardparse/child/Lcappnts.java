package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class Lcappnts {
   private  List<Lcappnt>lcappnt;

    @Override
    public String toString() {
        return "Lcappnts{" +
                "lcappnt=" + lcappnt +
                '}';
    }

    public List<Lcappnt> getLcappnt() {
        return lcappnt;
    }

    public void setLcappnt(List<Lcappnt> lcappnt) {
        this.lcappnt = lcappnt;
    }
}
