package com.sinosoft.lis.pos.entity.request;


import java.io.Serializable;

/**
 * Created by Administrator on 2018/11/6.
 */
public class ReqHeadPojo implements Serializable {
    //版本号
    private String Version;
    //交易流水号
    private String SerialNo;
    //交易编码
    private String TransID;
    //交易日期
    private String TransDate;
    //交易时间
    private String TransTime;
    //请求系统
    private String SysCode;
    //渠道
    private String Channel;

    @Override
    public String toString() {
        return "ReqHeadPojo{" +
                "Version='" + Version + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", TransID='" + TransID + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", SysCode='" + SysCode + '\'' +
                ", Channel='" + Channel + '\'' +
                '}';
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String version) {
        Version = version;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getTransID() {
        return TransID;
    }

    public void setTransID(String transID) {
        TransID = transID;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransTime() {
        return TransTime;
    }

    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    public String getSysCode() {
        return SysCode;
    }

    public void setSysCode(String sysCode) {
        SysCode = sysCode;
    }

    public String getChannel() {
        return Channel;
    }

    public void setChannel(String channel) {
        Channel = channel;
    }
}
