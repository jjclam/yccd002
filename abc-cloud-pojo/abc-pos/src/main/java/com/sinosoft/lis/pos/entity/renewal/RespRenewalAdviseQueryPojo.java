package com.sinosoft.lis.pos.entity.renewal;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PolicyJFTZQueryInfosPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 缴费成功通知书查询XQ004.
 */
@Relation(transID = "XQ004")
public class RespRenewalAdviseQueryPojo implements Pojo,Body,Serializable{
    //业务类型
    private String BusiType;
    private PolicyJFTZQueryInfosPojo PolicyJFTZQueryInfosPojo;

    public String getBusiType() {
        return BusiType;
    }

    public void setBusiType(String busiType) {
        BusiType = busiType;
    }

    public com.sinosoft.lis.pos.entity.PolicyJFTZQueryInfosPojo getPolicyJFTZQueryInfosPojo() {
        return PolicyJFTZQueryInfosPojo;
    }
    @JSONField(name ="policyJFTZQueryInfos" )
    public void setPolicyJFTZQueryInfosPojo(com.sinosoft.lis.pos.entity.PolicyJFTZQueryInfosPojo policyJFTZQueryInfosPojo) {
        PolicyJFTZQueryInfosPojo = policyJFTZQueryInfosPojo;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespRenewalAdviseQueryPojo{" +
                "BusiType='" + BusiType + '\'' +
                ", PolicyJFTZQueryInfosPojo=" + PolicyJFTZQueryInfosPojo +
                '}';
    }
}