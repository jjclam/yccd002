package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class DocsPojo implements Body,Serializable{
    private String DocCode;
    private String BussType;
    private String SubType;
    private String NumPages;
    private String UploadPages;
    private String ScanOperator;
    private String ManageCom;
    private List<PagesPojo> PagesPojo;

    @Override
    public String toString() {
        return "DocsPojo{" +
                "DocCode='" + DocCode + '\'' +
                ", BussType='" + BussType + '\'' +
                ", SubType='" + SubType + '\'' +
                ", NumPages='" + NumPages + '\'' +
                ", UploadPages='" + UploadPages + '\'' +
                ", ScanOperator='" + ScanOperator + '\'' +
                ", ManageCom='" + ManageCom + '\'' +
                ", PagesPojo=" + PagesPojo +
                '}';
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String subType) {
        SubType = subType;
    }

    public String getNumPages() {
        return NumPages;
    }

    public void setNumPages(String numPages) {
        NumPages = numPages;
    }

    public String getUploadPages() {
        return UploadPages;
    }

    public void setUploadPages(String uploadPages) {
        UploadPages = uploadPages;
    }

    public String getScanOperator() {
        return ScanOperator;
    }

    public void setScanOperator(String scanOperator) {
        ScanOperator = scanOperator;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    @JSONField(name = "pages")
    public List<PagesPojo> getPagesPojo() {
        return PagesPojo;
    }

    public void setPagesPojo(List<PagesPojo> pagesPojo) {
        PagesPojo = pagesPojo;
    }

    public String getDocCode() {
        return DocCode;
    }

    public void setDocCode(String docCode) {
        DocCode = docCode;
    }


}
