package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;


/**
 * 4009发送邮件POS059
 */
public class ReqNoticeAgainSendPojo implements ReqBody, Body, Serializable {
    //邮箱地址
    private String Email;
    //文本编号
    private String TextNo;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getTextNo() {
        return TextNo;
    }

    public void setTextNo(String textNo) {
        TextNo = textNo;
    }

    @Override
    public String toString() {
        return "ReqNoticeAgainSendPojo{" +
                "Email='" + Email + '\'' +
                ", TextNo='" + TextNo + '\'' +
                '}';
    }
}
