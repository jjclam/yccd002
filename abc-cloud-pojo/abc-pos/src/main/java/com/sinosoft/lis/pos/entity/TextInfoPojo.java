package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class TextInfoPojo implements Body,Serializable{
    //合同号码
    private String ContNo;
    //年份
    private String Year;
    //投保人姓名
    private String AppntName;
    //被保人姓名
    private String InsuredName;
    //附加险险种名称
    private String RiskName;
    //邮箱地址
    private String Email;
    //文本受理标志
    private String TextAcceptFlag;
    //文本编号
    private String TextNo;

    @Override
    public String toString() {
        return "TextInfoPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", Year='" + Year + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", Email='" + Email + '\'' +
                ", TextAcceptFlag='" + TextAcceptFlag + '\'' +
                ", TextNo='" + TextNo + '\'' +
                '}';
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getTextAcceptFlag() {
        return TextAcceptFlag;
    }

    public void setTextAcceptFlag(String textAcceptFlag) {
        TextAcceptFlag = textAcceptFlag;
    }

    public String getTextNo() {
        return TextNo;
    }

    public void setTextNo(String textNo) {
        TextNo = textNo;
    }

}
