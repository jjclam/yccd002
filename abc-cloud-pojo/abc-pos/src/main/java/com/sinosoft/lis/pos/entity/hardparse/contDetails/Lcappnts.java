package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;
import java.util.List;

public class Lcappnts implements Serializable {
    private List<Lcappnt> Lcappnt;

    public List<com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcappnt> getLcappnt() {
        return Lcappnt;
    }

    public void setLcappnt(List<com.sinosoft.lis.pos.entity.hardparse.contDetails.Lcappnt> lcappnt) {
        Lcappnt = lcappnt;
    }

    @Override
    public String toString() {
        return "Lcappnts{" +
                "Lcappnt=" + Lcappnt +
                '}';
    }
}