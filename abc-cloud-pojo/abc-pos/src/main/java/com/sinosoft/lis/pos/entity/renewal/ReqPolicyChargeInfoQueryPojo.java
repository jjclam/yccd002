package com.sinosoft.lis.pos.entity.renewal;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;



/**
 * 保单收费信息查询XQ006.
 */
public class ReqPolicyChargeInfoQueryPojo implements ReqBody, Body, Serializable {
    //保单号
    private String ContNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqPolicyChargeInfoQueryPojo{" +
                "ContNo='" + ContNo + '\'' +
                '}';
    }
}