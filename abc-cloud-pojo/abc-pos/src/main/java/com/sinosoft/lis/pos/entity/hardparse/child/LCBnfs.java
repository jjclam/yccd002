package com.sinosoft.lis.pos.entity.hardparse.child;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public class LCBnfs {
    private List<LCBnf>LCBnf;

    @Override
    public String toString() {
        return "LCBnfs{" +
                "LCBnf=" + LCBnf +
                '}';
    }

    public List<LCBnf> getLCBnf() {
        return LCBnf;
    }
    @JSONField(name = "lCBnf")
    public void setLCBnf(List<LCBnf> LCBnf) {
        this.LCBnf = LCBnf;
    }
}
