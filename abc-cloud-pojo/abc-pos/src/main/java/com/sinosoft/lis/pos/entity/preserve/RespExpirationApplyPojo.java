package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4004保单生存金领取(AGSQ)申请、4004保单生存金领取(SQ)申请、满期申请POS044、POS045.
 */
@Relation(transID = {"POS044", "POS045"})
public class RespExpirationApplyPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //保全受理号
    private String EdorAcceptNo;
    //保全批单号
    private String EdorNo;
    //险种名称
    private String RiskName;
    //退保金额
    private String OccurBala;
    //保全生效日
    private String BqValidDate;

    @Override
    public String toString() {
        return "RespExpirationApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorAcceptNo='" + EdorAcceptNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                ", RiskName='" + RiskName + '\'' +
                ", OccurBala='" + OccurBala + '\'' +
                ", BqValidDate='" + BqValidDate + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String edorAcceptNo) {
        EdorAcceptNo = edorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getOccurBala() {
        return OccurBala;
    }

    public void setOccurBala(String occurBala) {
        OccurBala = occurBala;
    }

    public String getBqValidDate() {
        return BqValidDate;
    }

    public void setBqValidDate(String bqValidDate) {
        BqValidDate = bqValidDate;
    }

    @Override

    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }
}
