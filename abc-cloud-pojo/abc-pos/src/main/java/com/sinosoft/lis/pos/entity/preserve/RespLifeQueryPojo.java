package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 生存调查列表查询POS048.
 */
@Relation(transID = "POS048")
public class RespLifeQueryPojo implements Pojo,Body,Serializable{
    //保全项目编码
    private String EdorType;
    //处理结果
    private String DealResult;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getDealResult() {
        return DealResult;
    }

    public void setDealResult(String dealResult) {
        DealResult = dealResult;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespLifeQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", DealResult='" + DealResult + '\'' +
                '}';
    }
}