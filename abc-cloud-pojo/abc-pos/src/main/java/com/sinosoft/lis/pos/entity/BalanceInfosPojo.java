package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

public class BalanceInfosPojo implements Body, Serializable {
    private List<BalanceInfoPojo> BalanceInfoPojo;

    public List<com.sinosoft.lis.pos.entity.BalanceInfoPojo> getBalanceInfoPojo() {
        return BalanceInfoPojo;
    }

    @JSONField(name = "balanceInfo")
    public void setBalanceInfoPojo(List<com.sinosoft.lis.pos.entity.BalanceInfoPojo> balanceInfoPojo) {
        BalanceInfoPojo = balanceInfoPojo;
    }

    @Override
    public String toString() {
        return "BalanceInfosPojo{" +
                "BalanceInfoPojo=" + BalanceInfoPojo +
                '}';
    }
}