package com.sinosoft.lis.pos.entity.common;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 官网，微信 保单价值查询
 */
public class ReqWebSitePolicyValueQueryPojo implements ReqBody, Body, Serializable {
    //银行订单号
    private String ABCOrderId;
    //保单号
    private String ContNo;

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqWebSitePolicyValueQueryPojo{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}