package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class ChnlExtends {
    private List<ChnlExtend>ChnlExtend;

    @Override
    public String toString() {
        return "ChnlExtends{" +
                "ChnlExtend=" + ChnlExtend +
                '}';
    }

    public List<ChnlExtend> getChnlExtend() {
        return ChnlExtend;
    }

    public void setChnlExtend(List<ChnlExtend> chnlExtend) {
        ChnlExtend = chnlExtend;
    }
}
