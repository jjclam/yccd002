package com.sinosoft.lis.pos.entity.hardparse;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 保单重打NB006
 */
public class ReqRetypePolicyPojo implements ReqBody, Body,Serializable{
    //老印刷号码
    private String OldVchNo;
    //新印刷号码
    private String NewVchNo;
    //总单投保单号码
    private String ProposalContNo;
    //银行编码
    private String BankCode;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //柜员代码
    private String TellerNo;
    //险种编码
    private String RiskCode;

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getNewVchNo() {
        return NewVchNo;
    }

    public void setNewVchNo(String newVchNo) {
        NewVchNo = newVchNo;
    }

    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        ProposalContNo = proposalContNo;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    public String getOldVchNo() {
        return OldVchNo;
    }

    public void setOldVchNo(String oldVchNo) {
        OldVchNo = oldVchNo;
    }

    @Override
    public String toString() {
        return "ReqRetypePolicyPojo{" +
                "OldVchNo='" + OldVchNo + '\'' +
                ", NewVchNo='" + NewVchNo + '\'' +
                ", ProposalContNo='" + ProposalContNo + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                '}';
    }
}
