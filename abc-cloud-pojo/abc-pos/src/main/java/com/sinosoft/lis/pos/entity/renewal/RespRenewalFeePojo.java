package com.sinosoft.lis.pos.entity.renewal;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 续期缴费XQ002.
 */
@Relation(transID = "XQ002")
public class RespRenewalFeePojo implements Pojo,Body,Serializable{
    //投保人证件类型
    private String IDType;
    //投保人证件号码
    private String IDNo;
    //保单号
    private String ContNo;
    //险种代码
    private String RiskCode;
    //投保人姓名
    private String AppntName;
    //缴费账户
    private String PayAcc;
    //缴费金额
    private String PayAmt;
    //产品名称
    private String ProdCode;

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getPayAcc() {
        return PayAcc;
    }

    public void setPayAcc(String payAcc) {
        PayAcc = payAcc;
    }

    public String getPayAmt() {
        return PayAmt;
    }

    public void setPayAmt(String payAmt) {
        PayAmt = payAmt;
    }

    public String getProdCode() {
        return ProdCode;
    }

    public void setProdCode(String prodCode) {
        ProdCode = prodCode;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespRenewalFeePojo{" +
                "IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", PayAcc='" + PayAcc + '\'' +
                ", PayAmt='" + PayAmt + '\'' +
                ", ProdCode='" + ProdCode + '\'' +
                '}';
    }
}