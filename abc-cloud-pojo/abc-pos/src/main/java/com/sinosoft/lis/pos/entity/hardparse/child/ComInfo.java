package com.sinosoft.lis.pos.entity.hardparse.child;

public class ComInfo {
    //机构名称
    private String ComName;
    //机构地址
    private String ComLocation;
    //机构邮编
    private String ZipCode;

    @Override
    public String toString() {
        return "ComInfo{" +
                "ComName='" + ComName + '\'' +
                ", ComLocation='" + ComLocation + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                '}';
    }

    public String getComName() {
        return ComName;
    }

    public void setComName(String comName) {
        ComName = comName;
    }

    public String getComLocation() {
        return ComLocation;
    }

    public void setComLocation(String comLocation) {
        ComLocation = comLocation;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }
}
