package com.sinosoft.lis.pos.entity.renewal;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 续期缴费XQ002.
 */
public class ReqRenewalFeePojo implements ReqBody, Body,Serializable{
    //险种代码
    private String RiskCode;
    //产品名称
    private String ProdCode;
    //保单号
    private String ContNo;
    //投保单号
    private String PolicyApplyNo;
    //投保人姓名
    private String AppntName;
    //投保人证件类型
    private String IDType;
    //投保人证件号码
    private String IDNo;
    //应缴期数
    private String DuePeriod;
    //缴费账户
    private String PayAcc;
    //缴费金额
    private String PayAmt;
    //银行编码
    private String BankCode;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //柜员代码
    private String TellerNo;

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getProdCode() {
        return ProdCode;
    }

    public void setProdCode(String prodCode) {
        ProdCode = prodCode;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPolicyApplyNo() {
        return PolicyApplyNo;
    }

    public void setPolicyApplyNo(String policyApplyNo) {
        PolicyApplyNo = policyApplyNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getDuePeriod() {
        return DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        DuePeriod = duePeriod;
    }

    public String getPayAcc() {
        return PayAcc;
    }

    public void setPayAcc(String payAcc) {
        PayAcc = payAcc;
    }

    public String getPayAmt() {
        return PayAmt;
    }

    public void setPayAmt(String payAmt) {
        PayAmt = payAmt;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

    @Override
    public String toString() {
        return "ReqRenewalFeePojo{" +
                "RiskCode='" + RiskCode + '\'' +
                ", ProdCode='" + ProdCode + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", PolicyApplyNo='" + PolicyApplyNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", DuePeriod='" + DuePeriod + '\'' +
                ", PayAcc='" + PayAcc + '\'' +
                ", PayAmt='" + PayAmt + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                '}';
    }
}