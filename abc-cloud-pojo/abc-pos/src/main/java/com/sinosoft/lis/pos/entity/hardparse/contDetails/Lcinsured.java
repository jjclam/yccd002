package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;

public class Lcinsured implements Serializable {
    //被保人姓名
    private String Name;
    //被保人性别
    private String Sex;
    //被保人证件类型
    private String IDKind;
    //被保人证件号码
    private String IDCode;
    //被保人通讯地址
    private String HomeAddress;
    //被保人邮政编码
    private String HomeZipCode;
    //被保人移动电话
    private String Mobile;
    //被保人出生日期
    private String Birthday;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getIDKind() {
        return IDKind;
    }

    public void setIDKind(String IDKind) {
        this.IDKind = IDKind;
    }

    public String getIDCode() {
        return IDCode;
    }

    public void setIDCode(String IDCode) {
        this.IDCode = IDCode;
    }

    public String getHomeAddress() {
        return HomeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        HomeAddress = homeAddress;
    }

    public String getHomeZipCode() {
        return HomeZipCode;
    }

    public void setHomeZipCode(String homeZipCode) {
        HomeZipCode = homeZipCode;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    @Override
    public String toString() {
        return "Lcinsured{" +
                "Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", IDKind='" + IDKind + '\'' +
                ", IDCode='" + IDCode + '\'' +
                ", HomeAddress='" + HomeAddress + '\'' +
                ", HomeZipCode='" + HomeZipCode + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", Birthday='" + Birthday + '\'' +
                '}';
    }
}