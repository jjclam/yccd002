package com.sinosoft.lis.pos.entity.annotation;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * Created by Administrator on 2018/11/1.
 */
@Component
@Inherited
@Scope("prototype")
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Relation {

    public static final String CORE = "core";
    public static final String GRP_CORE = "grp_core";
    public static final String ACCESS = "access";

    String sysID() default CORE;

    String[] transID();

}
