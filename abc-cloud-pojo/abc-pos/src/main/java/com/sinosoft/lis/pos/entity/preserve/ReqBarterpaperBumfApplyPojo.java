package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 换发纸质保单申请POS115.
 */
public class ReqBarterpaperBumfApplyPojo implements ReqBody, Body, Serializable {
    //保单号
    private String ContNo;
    //省
    private String Province;
    //市
    private String City;
    //县
    private String Powiat;
    //镇/乡
    private String Street;
    //村/社区（楼号)
    private String Village;
    //邮编
    private String ZipCode;
    //手机号
    private String MobilePhone;


    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPowiat() {
        return Powiat;
    }

    public void setPowiat(String powiat) {
        Powiat = powiat;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getVillage() {
        return Village;
    }

    public void setVillage(String village) {
        Village = village;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    @Override
    public String toString() {
        return "ReqBarterpaperBumfApplyPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", Province='" + Province + '\'' +
                ", City='" + City + '\'' +
                ", Powiat='" + Powiat + '\'' +
                ", Street='" + Street + '\'' +
                ", Village='" + Village + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                '}';
    }
}