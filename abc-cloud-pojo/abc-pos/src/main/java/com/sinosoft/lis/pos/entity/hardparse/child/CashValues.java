package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class CashValues {
    private List<CashValue> CashValue;

    @Override
    public String toString() {
        return "CashValues{" +
                "CashValue=" + CashValue +
                '}';
    }

    public List<CashValue> getCashValue() {
        return CashValue;
    }

    public void setCashValue(List<CashValue> cashValue) {
        CashValue = cashValue;
    }
}
