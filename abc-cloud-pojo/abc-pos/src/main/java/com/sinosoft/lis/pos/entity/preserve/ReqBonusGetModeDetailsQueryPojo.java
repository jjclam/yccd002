package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

import com.sinosoft.lis.pos.entity.inter.Body;

/**
 * 红利领取方式保全项目保单明细查询POS021.
 */
public class ReqBonusGetModeDetailsQueryPojo implements ReqBody, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }


    @Override
    public String toString() {
        return "ReqBonusGetModeDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}