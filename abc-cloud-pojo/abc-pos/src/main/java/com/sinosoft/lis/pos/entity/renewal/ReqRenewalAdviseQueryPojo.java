package com.sinosoft.lis.pos.entity.renewal;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 缴费成功通知书查询XQ004.
 */
public class ReqRenewalAdviseQueryPojo implements ReqBody, Body,Serializable{
    //续期业务类型
    private String BusiType;
    //客户号
    private String CustomerNo;
    //客户身份证号
    private String CustomerIDNo;
    //客户姓名
    private String CustomerName;

    public String getBusiType() {
        return BusiType;
    }

    public void setBusiType(String busiType) {
        BusiType = busiType;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    @Override
    public String toString() {
        return "ReqRenewalAdviseQueryPojo{" +
                "BusiType='" + BusiType + '\'' +
                ", CustomerNo='" + CustomerNo + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                '}';
    }
}