package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;
/**
 * 4010保单贷款清偿申请POS096
 */
public class ReqPolicyLoanClearApplyWebSitePojo implements ReqBody, Body, Serializable {
    private String EdorType;
    private String ContNo;
    private String LoanMoney;

    @Override
    public String toString() {
        return "ReqPolicyLoanClearApplyWebSitePojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", LoanMoney='" + LoanMoney + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }
}
