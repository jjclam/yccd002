package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class PolicysPojo implements Body,Serializable{
    private List<com.sinosoft.lis.pos.entity.PolicyPojo> PolicyPojo;

    @Override
    public String toString() {
        return "PolicysPojo{" +
                "PolicyPojo=" + PolicyPojo +
                '}';
    }

    public List<com.sinosoft.lis.pos.entity.PolicyPojo> getPolicyPojo() {
        return PolicyPojo;
    }
    @JSONField(name = "policy")
    public void setPolicyPojo(List<com.sinosoft.lis.pos.entity.PolicyPojo> policyPojo) {
        PolicyPojo = policyPojo;
    }

}
