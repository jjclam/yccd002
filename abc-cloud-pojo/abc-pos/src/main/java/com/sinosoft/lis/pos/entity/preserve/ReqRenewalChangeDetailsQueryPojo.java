package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

;

/**
 * 4003查询保单详细-续保方式变更POS065 4007续保方式变
 */
public class ReqRenewalChangeDetailsQueryPojo implements ReqBody, Body, Serializable {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //批单号
    private String EdorNo;

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String edorNo) {
        EdorNo = edorNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    @Override
    public String toString() {
        return "ReqRenewalChangeDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", EdorNo='" + EdorNo + '\'' +
                '}';
    }
}
