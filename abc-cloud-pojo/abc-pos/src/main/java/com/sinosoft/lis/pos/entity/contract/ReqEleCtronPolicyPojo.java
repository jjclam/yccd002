package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

;

/**
 * 电子保单补发NB008.
 */
public class ReqEleCtronPolicyPojo implements ReqBody, Body, Serializable {
    //保单号
    private String ContNo;
    //业务类型
    private String BussType;
    //邮箱
    private String EMail;
    //快递地址
    private String ExpressAddress;
    //备用1
    private String Reserve1;
    //备用2
    private String Reserve2;
    //备用3
    private String Reserve3;
    //备用4
    private String Reserve4;


    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String bussType) {
        BussType = bussType;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public String getExpressAddress() {
        return ExpressAddress;
    }

    public void setExpressAddress(String expressAddress) {
        ExpressAddress = expressAddress;
    }

    public String getReserve1() {
        return Reserve1;
    }

    public void setReserve1(String reserve1) {
        Reserve1 = reserve1;
    }

    public String getReserve2() {
        return Reserve2;
    }

    public void setReserve2(String reserve2) {
        Reserve2 = reserve2;
    }

    public String getReserve3() {
        return Reserve3;
    }

    public void setReserve3(String reserve3) {
        Reserve3 = reserve3;
    }

    public String getReserve4() {
        return Reserve4;
    }

    public void setReserve4(String reserve4) {
        Reserve4 = reserve4;
    }

    @Override
    public String toString() {
        return "ReqEleCtronPolicyPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", BussType='" + BussType + '\'' +
                ", EMail='" + EMail + '\'' +
                ", ExpressAddress='" + ExpressAddress + '\'' +
                ", Reserve1='" + Reserve1 + '\'' +
                ", Reserve2='" + Reserve2 + '\'' +
                ", Reserve3='" + Reserve3 + '\'' +
                ", Reserve4='" + Reserve4 + '\'' +
                '}';
    }
}