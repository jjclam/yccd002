package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 生存金详情查询POS047 官网4003微信生存金详情查询.
 */
@Relation(transID = "POS047")
public class RespLifeCashDetailsQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //生存受益人
    private String SurvivalAppntNo;
    //生存金总额
    private String SumSurvivalMoney;
    private com.sinosoft.lis.pos.entity.SurvivalRisksPojo SurvivalRisksPojo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getSurvivalAppntNo() {
        return SurvivalAppntNo;
    }

    public void setSurvivalAppntNo(String survivalAppntNo) {
        SurvivalAppntNo = survivalAppntNo;
    }

    public String getSumSurvivalMoney() {
        return SumSurvivalMoney;
    }

    public void setSumSurvivalMoney(String sumSurvivalMoney) {
        SumSurvivalMoney = sumSurvivalMoney;
    }

    public com.sinosoft.lis.pos.entity.SurvivalRisksPojo getSurvivalRisksPojo() {
        return SurvivalRisksPojo;
    }

    @JSONField(name = "survivalRisks")
    public void setSurvivalRisksPojo(com.sinosoft.lis.pos.entity.SurvivalRisksPojo survivalRisksPojo) {
        SurvivalRisksPojo = survivalRisksPojo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespLifeCashDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", SurvivalAppntNo='" + SurvivalAppntNo + '\'' +
                ", SumSurvivalMoney='" + SumSurvivalMoney + '\'' +
                ", SurvivalRisksPojo=" + SurvivalRisksPojo +
                '}';
    }
}