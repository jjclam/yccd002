package com.sinosoft.lis.pos.entity.common;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 保单挂起PC005.
 */
public class ReqPolicyUpPojo implements ReqBody, Body,Serializable{
    //保单号
    private String ContNo;
    //保费金额
    private String Prem;
    //应缴期数
    private String DuePeriod;
    //应缴日期
    private String DueDate;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getDuePeriod() {
        return DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        DuePeriod = duePeriod;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }


    @Override
    public String toString() {
        return "ReqPolicyUpPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", Prem='" + Prem + '\'' +
                ", DuePeriod='" + DuePeriod + '\'' +
                ", DueDate='" + DueDate + '\'' +
                '}';
    }
}