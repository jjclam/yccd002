package com.sinosoft.lis.pos.entity.common;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 保单挂起PC005.
 */
@Relation(transID = "PC005")
public class RespPolicyUpPojo implements Pojo,Body,Serializable{
    //保单号
    private String ContNo;
    //是否挂起成功
    private String HangUpFlag;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getHangUpFlag() {
        return HangUpFlag;
    }

    public void setHangUpFlag(String hangUpFlag) {
        HangUpFlag = hangUpFlag;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPolicyUpPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", HangUpFlag='" + HangUpFlag + '\'' +
                '}';
    }
}

