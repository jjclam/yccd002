package com.sinosoft.lis.pos.entity.claims;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 机构信息查询CLM003.
 */
public class ReqFrameWorkQueryPojo implements ReqBody, Body, Serializable {
    //被保险人姓名
    private String InsuredName;
    //证件类型
    private String InsuredIDType;
    //证件号码
    private String InsuredIDNo;
    //被保人性别
    private String InsuredSex;
    //被保人出生日期
    private String Birthday;

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getInsuredIDType() {
        return InsuredIDType;
    }

    public void setInsuredIDType(String insuredIDType) {
        InsuredIDType = insuredIDType;
    }

    public String getInsuredIDNo() {
        return InsuredIDNo;
    }

    public void setInsuredIDNo(String insuredIDNo) {
        InsuredIDNo = insuredIDNo;
    }

    public String getInsuredSex() {
        return InsuredSex;
    }

    public void setInsuredSex(String insuredSex) {
        InsuredSex = insuredSex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    @Override
    public String toString() {
        return "ReqFrameWorkQueryPojo{" +
                "InsuredName='" + InsuredName + '\'' +
                ", InsuredIDType='" + InsuredIDType + '\'' +
                ", InsuredIDNo='" + InsuredIDNo + '\'' +
                ", InsuredSex='" + InsuredSex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                '}';
    }
}