package com.sinosoft.lis.pos.entity.common;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 代理人收费接口PC008.
 */
public class ReqAgentChargePojo implements ReqBody, Body, Serializable {
    //支公司代码
    private String ManageCom;
    //营销员证件类型
    private String AgentIdType;
    //营销员身份证号
    private String AgentIdNo;
    //电话号码
    private String Phone;
    //交费类型
    private String IncomeType;
    //交费金额
    private String GetMoney;
    //交费方式
    private String PayMode;
    //户名
    private String AccName;
    //扣款银行代码
    private String BankCode;
    //扣款银行账号
    private String BankAccNo;
    //;交易日期 yyyy-MM-dd
    private String TradeDate;
    //交易时间 hh24:mi:ss
    private String TradeTime;
    //收款银行代码
    private String InBankCode;
    //公司账户名称
    private String CompanyAccName;
    //公司账号
    private String CompanyAccNo;
    //收费机构
    private String ComCode;
    //银行交易流水号
    private String TransNo;

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getAgentIdType() {
        return AgentIdType;
    }

    public void setAgentIdType(String agentIdType) {
        AgentIdType = agentIdType;
    }

    public String getAgentIdNo() {
        return AgentIdNo;
    }

    public void setAgentIdNo(String agentIdNo) {
        AgentIdNo = agentIdNo;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getIncomeType() {
        return IncomeType;
    }

    public void setIncomeType(String incomeType) {
        IncomeType = incomeType;
    }

    public String getGetMoney() {
        return GetMoney;
    }

    public void setGetMoney(String getMoney) {
        GetMoney = getMoney;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getTradeDate() {
        return TradeDate;
    }

    public void setTradeDate(String tradeDate) {
        TradeDate = tradeDate;
    }

    public String getTradeTime() {
        return TradeTime;
    }

    public void setTradeTime(String tradeTime) {
        TradeTime = tradeTime;
    }

    public String getInBankCode() {
        return InBankCode;
    }

    public void setInBankCode(String inBankCode) {
        InBankCode = inBankCode;
    }

    public String getCompanyAccName() {
        return CompanyAccName;
    }

    public void setCompanyAccName(String companyAccName) {
        CompanyAccName = companyAccName;
    }

    public String getCompanyAccNo() {
        return CompanyAccNo;
    }

    public void setCompanyAccNo(String companyAccNo) {
        CompanyAccNo = companyAccNo;
    }

    public String getComCode() {
        return ComCode;
    }

    public void setComCode(String comCode) {
        ComCode = comCode;
    }

    public String getTransNo() {
        return TransNo;
    }

    public void setTransNo(String transNo) {
        TransNo = transNo;
    }

    @Override
    public String toString() {
        return "ReqAgentChargePojo{" +
                "ManageCom='" + ManageCom + '\'' +
                ", AgentIdType='" + AgentIdType + '\'' +
                ", AgentIdNo='" + AgentIdNo + '\'' +
                ", Phone='" + Phone + '\'' +
                ", IncomeType='" + IncomeType + '\'' +
                ", GetMoney='" + GetMoney + '\'' +
                ", PayMode='" + PayMode + '\'' +
                ", AccName='" + AccName + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", TradeDate='" + TradeDate + '\'' +
                ", TradeTime='" + TradeTime + '\'' +
                ", InBankCode='" + InBankCode + '\'' +
                ", CompanyAccName='" + CompanyAccName + '\'' +
                ", CompanyAccNo='" + CompanyAccNo + '\'' +
                ", ComCode='" + ComCode + '\'' +
                ", TransNo='" + TransNo + '\'' +
                '}';
    }
}