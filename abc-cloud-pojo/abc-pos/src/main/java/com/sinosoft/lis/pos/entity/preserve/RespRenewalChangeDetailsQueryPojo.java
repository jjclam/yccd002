package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.MainRisksPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

;

/**
 * 4003查询保单详细-续保方式变更POS065 4007续保方式变
 */
@Relation(transID = {"POS065", "POS112"})
public class RespRenewalChangeDetailsQueryPojo implements Pojo, Body, Serializable {

    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人姓名
    private String AppntName;
    //被保人姓名
    private String InsuredName;
    //总保费
    private String TotalPrem;
    //赔付日期/缴费对应日
    private String PayDate;
    //主险编码
    private String MainRiskCode;
    //主险名称
    private String MainRiskName;
    //保额
    private String Amnt;
    //保费
    private String Prem;
    //份数
    private String Mult;
    //生存金领取标志/续保标记
    private String XBFlag;
    //生存金领取方式/续保方式
    private String XBPattern;
    //缴费银行账号
    private String BankAccNo;
    //缴费银行名称
    private String BankName;
    //新增首期保费交费形式
    private String FirstPayMode;
    //续保方式
    private String RenealMode;
    //附加险
    private com.sinosoft.lis.pos.entity.AddtRisksPojo AddtRisksPojo;
    //总保额
    private String TotalAmnt;
    //主险
    private MainRisksPojo MainRisksPojo;
    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String insuredName) {
        InsuredName = insuredName;
    }

    public String getTotalPrem() {
        return TotalPrem;
    }

    public void setTotalPrem(String totalPrem) {
        TotalPrem = totalPrem;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getMult() {
        return Mult;
    }

    public void setMult(String mult) {
        Mult = mult;
    }

    public String getXBFlag() {
        return XBFlag;
    }

    public void setXBFlag(String XBFlag) {
        this.XBFlag = XBFlag;
    }

    public String getXBPattern() {
        return XBPattern;
    }

    public void setXBPattern(String XBPattern) {
        this.XBPattern = XBPattern;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getFirstPayMode() {
        return FirstPayMode;
    }

    public void setFirstPayMode(String firstPayMode) {
        FirstPayMode = firstPayMode;
    }

    public String getRenealMode() {
        return RenealMode;
    }

    public void setRenealMode(String renealMode) {
        RenealMode = renealMode;
    }

    public com.sinosoft.lis.pos.entity.AddtRisksPojo getAddtRisksPojo() {
        return AddtRisksPojo;
    }

    @JSONField(name = "addtRisks")
    public void setAddtRisksPojo(com.sinosoft.lis.pos.entity.AddtRisksPojo addtRisksPojo) {
        AddtRisksPojo = addtRisksPojo;
    }
    @JSONField(name = "mainRisks")
    public void setMainRisksPojo(com.sinosoft.lis.pos.entity.MainRisksPojo mainRisksPojo) {
        MainRisksPojo = mainRisksPojo;
    }
    public com.sinosoft.lis.pos.entity.MainRisksPojo getMainRisksPojo() {
        return MainRisksPojo;
    }

    public String getTotalAmnt() {
        return TotalAmnt;
    }

    public void setTotalAmnt(String totalAmnt) {
        TotalAmnt = totalAmnt;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespRenewalChangeDetailsQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", AppntName='" + AppntName + '\'' +
                ", InsuredName='" + InsuredName + '\'' +
                ", TotalPrem='" + TotalPrem + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", MainRiskCode='" + MainRiskCode + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", Prem='" + Prem + '\'' +
                ", Mult='" + Mult + '\'' +
                ", XBFlag='" + XBFlag + '\'' +
                ", XBPattern='" + XBPattern + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", BankName='" + BankName + '\'' +
                ", FirstPayMode='" + FirstPayMode + '\'' +
                ", RenealMode='" + RenealMode + '\'' +
                ", AddtRisksPojo=" + AddtRisksPojo +
                ", TotalAmnt='" + TotalAmnt + '\'' +
                ", MainRisksPojo=" + MainRisksPojo +
                '}';
    }
}
