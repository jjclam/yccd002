package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 4004保全提交POS063
 */
public class ReqRenewalPayChangeApplyPojo implements ReqBody, Body,Serializable{

    //保全项目编码
    private String EdorType;
    private com.sinosoft.lis.pos.entity.PolicyQueryInfosPojo PolicyQueryInfosPojo;

    @Override
    public String toString() {
        return "ReqRenewalPayChangeApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", PolicyQueryInfosPojo=" + PolicyQueryInfosPojo +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    @JSONField(name = "policyQueryInfos")
    public PolicyQueryInfosPojo getPolicyQueryInfosPojo() {
        return PolicyQueryInfosPojo;
    }

    public void setPolicyQueryInfosPojo(PolicyQueryInfosPojo policyQueryInfosPojo) {
        PolicyQueryInfosPojo = policyQueryInfosPojo;
    }

}
