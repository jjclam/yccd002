package com.sinosoft.lis.pos.entity.hardparse.child;

public class ActuaryRisk {
    //险种编码
    private String RiskCode;
    //险种责任
    private ActuaryDutys ActuaryDutys;

    @Override
    public String toString() {
        return "ActuaryRisk{" +
                "RiskCode='" + RiskCode + '\'' +
                ", ActuaryDutys=" + ActuaryDutys +
                '}';
    }

    public com.sinosoft.lis.pos.entity.hardparse.child.ActuaryDutys getActuaryDutys() {
        return ActuaryDutys;
    }

    public void setActuaryDutys(com.sinosoft.lis.pos.entity.hardparse.child.ActuaryDutys actuaryDutys) {
        ActuaryDutys = actuaryDutys;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

}
