package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 退保申请 微信、官网
 */
@Relation(transID = "POS126")
public class RespWebSiteSurrenderApplyPojo implements Pojo, Body, Serializable {
    //银行订单号
    private String ABCOrderId;
    //银行退保订单号
    private String ABCRefundId;
    // 保险公司退保单号
    private String RefundProposalNo;
    //退保金额
    private String WithdrawMoney;
    //实际可支取金额
    private String AvailableMoney;
    // 是否成功
    private String IsSuccess;
    //审核失败原因
    private String FailReason;


    @Override

    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    public String getABCOrderId() {
        return ABCOrderId;
    }

    public void setABCOrderId(String ABCOrderId) {
        this.ABCOrderId = ABCOrderId;
    }

    public String getABCRefundId() {
        return ABCRefundId;
    }

    public void setABCRefundId(String ABCRefundId) {
        this.ABCRefundId = ABCRefundId;
    }

    public String getRefundProposalNo() {
        return RefundProposalNo;
    }

    public void setRefundProposalNo(String refundProposalNo) {
        RefundProposalNo = refundProposalNo;
    }

    public String getWithdrawMoney() {
        return WithdrawMoney;
    }

    public void setWithdrawMoney(String withdrawMoney) {
        WithdrawMoney = withdrawMoney;
    }

    public String getAvailableMoney() {
        return AvailableMoney;
    }

    public void setAvailableMoney(String availableMoney) {
        AvailableMoney = availableMoney;
    }

    public String getIsSuccess() {
        return IsSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        IsSuccess = isSuccess;
    }

    public String getFailReason() {
        return FailReason;
    }

    public void setFailReason(String failReason) {
        FailReason = failReason;
    }

    @Override
    public String toString() {
        return "RespWebSiteSurrenderApplyPojo{" +
                "ABCOrderId='" + ABCOrderId + '\'' +
                ", ABCRefundId='" + ABCRefundId + '\'' +
                ", RefundProposalNo='" + RefundProposalNo + '\'' +
                ", WithdrawMoney='" + WithdrawMoney + '\'' +
                ", AvailableMoney='" + AvailableMoney + '\'' +
                ", IsSuccess='" + IsSuccess + '\'' +
                ", FailReason='" + FailReason + '\'' +
                '}';
    }
}