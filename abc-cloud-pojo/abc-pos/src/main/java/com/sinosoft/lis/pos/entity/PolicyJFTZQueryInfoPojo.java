package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class PolicyJFTZQueryInfoPojo implements Body,Serializable{
    //保单号
    private String ContNo;
    //支付日期
    private String PayDate;
    //保费
    private String Prem;
    //银行名称
    private String BankName;
    //银行账户号
    private String BankAccNo;
    //生效日期
    private String CValiDate;
    //邮箱
    private String Email;
    private String ENoticeId;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getPayDate() {
        return PayDate;
    }

    public void setPayDate(String payDate) {
        PayDate = payDate;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getCValiDate() {
        return CValiDate;
    }

    public void setCValiDate(String CValiDate) {
        this.CValiDate = CValiDate;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getENoticeId() {
        return ENoticeId;
    }

    public void setENoticeId(String ENoticeId) {
        this.ENoticeId = ENoticeId;
    }

    @Override
    public String toString() {
        return "PolicyJFTZQueryInfoPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", PayDate='" + PayDate + '\'' +
                ", Prem='" + Prem + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", CValiDate='" + CValiDate + '\'' +
                ", Email='" + Email + '\'' +
                ", ENoticeId='" + ENoticeId + '\'' +
                '}';
    }
}