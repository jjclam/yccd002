package com.sinosoft.lis.pos.entity.hardparse.contDetails;

import java.io.Serializable;

public class Lcappnt  implements Serializable {
    //投保人证件类型
    private String IDKind;
    //投保人证件号码
    private String IDCode;
    //投保人名称
    private String Name;
    //投保人性别
    private String Sex;
    //投保人性别
    private String Birthday;
    //投保人通讯地址
    private String HomeAddress;
    //投保人邮政编码
    private String HomeZipCode;
    //投保人电子邮箱
    private String Email;
    //投保人固定电话
    private String Phone;
    //投保人移动电话
    private String Mobile;
    //投保人年收入
    private String AnnualIncome;
    //投保人国籍
    private String Nationality;
    //投保人国籍
    private String RelaToInsured;

    public String getIDKind() {
        return IDKind;
    }

    public void setIDKind(String IDKind) {
        this.IDKind = IDKind;
    }

    public String getIDCode() {
        return IDCode;
    }

    public void setIDCode(String IDCode) {
        this.IDCode = IDCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getHomeAddress() {
        return HomeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        HomeAddress = homeAddress;
    }

    public String getHomeZipCode() {
        return HomeZipCode;
    }

    public void setHomeZipCode(String homeZipCode) {
        HomeZipCode = homeZipCode;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getAnnualIncome() {
        return AnnualIncome;
    }

    public void setAnnualIncome(String annualIncome) {
        AnnualIncome = annualIncome;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getRelaToInsured() {
        return RelaToInsured;
    }

    public void setRelaToInsured(String relaToInsured) {
        RelaToInsured = relaToInsured;
    }

    @Override
    public String toString() {
        return "Lcappnt{" +
                "IDKind='" + IDKind + '\'' +
                ", IDCode='" + IDCode + '\'' +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Birthday='" + Birthday + '\'' +
                ", HomeAddress='" + HomeAddress + '\'' +
                ", HomeZipCode='" + HomeZipCode + '\'' +
                ", Email='" + Email + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", AnnualIncome='" + AnnualIncome + '\'' +
                ", Nationality='" + Nationality + '\'' +
                ", RelaToInsured='" + RelaToInsured + '\'' +
                '}';
    }
}