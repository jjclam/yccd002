package com.sinosoft.lis.pos.entity.hardparse.child;

public class CashValue {
    //保险年期
    private String PolYear;
    //现价
    private String Value;

    @Override
    public String toString() {
        return "CashValue{" +
                "PolYear='" + PolYear + '\'' +
                ", Value='" + Value + '\'' +
                '}';
    }

    public String getPolYear() {
        return PolYear;
    }

    public void setPolYear(String polYear) {
        PolYear = polYear;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
