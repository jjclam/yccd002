package com.sinosoft.lis.pos.entity.hardparse.child;

import java.util.List;

public class Dutys{
    private List<Duty> duty;

    @Override
    public String toString() {
        return "Dutys{" +
                "duty=" + duty +
                '}';
    }

    public List<Duty> getDuty() {
        return duty;
    }

    public void setDuty(List<Duty> duty) {
        this.duty = duty;
    }
}
