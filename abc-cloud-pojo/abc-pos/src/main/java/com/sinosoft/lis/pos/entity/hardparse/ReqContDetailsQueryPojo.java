package com.sinosoft.lis.pos.entity.hardparse;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 保单详情查询PC004.
 */
public class ReqContDetailsQueryPojo implements ReqBody, Body,Serializable{
    //渠道
    private String EntrustWay;
    //银行编码
    private String BankCode;
    //组合编码
    private String RiskCodeWr;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //险种编码
    private String RiskCode;
    //客户号
    private String ContNo;

    public String getEntrustWay() {
        return EntrustWay;
    }

    public void setEntrustWay(String entrustWay) {
        EntrustWay = entrustWay;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getRiskCodeWr() {
        return RiskCodeWr;
    }

    public void setRiskCodeWr(String riskCodeWr) {
        RiskCodeWr = riskCodeWr;
    }

    @Override
    public String toString() {
        return "ReqContDetailsQueryPojo{" +
                "EntrustWay='" + EntrustWay + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", RiskCodeWr='" + RiskCodeWr + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", ContNo='" + ContNo + '\'' +
                '}';
    }
}