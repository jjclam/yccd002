package com.sinosoft.lis.pos.entity.preserve;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.MainRisksPojo;
import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

;

/**
 * 4003保单贷款明细查询POS074 4007保单贷款已做保单查询POS125
 */
@Relation(transID = {"POS074", "POS125"})
public class RespPolicyLoanDetailQueryPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //证件姓名
    private String CustomerName;
    //贷款年利率
    private String LoanAnnualRate;
    //贷款时长
    private String LoanDays;
    //最大贷款数额
    private String MaxLoan;
    //账户名称
    private String BankName;
    //开户银行
    private String BankAccName;
    //银行账号
    private String BankAccNo;
    //手机电话号码
    private String MobilePhone;
    //主险编码
    private String MainRiskCode;
    //主险名称
    private String MainRiskName;
    //保额
    private String Amnt;
    //保费
    private String Prem;
    //现金价值
    private String CashValue;
    //附加险
    private com.sinosoft.lis.pos.entity.AddtRisksPojo AddtRisksPojo;
    //保单号
    private String ContNo;
    //贷款金额
    private String LoanMoney;
    private MainRisksPojo MainRisksPojo;
    //还款状态
    private String RepaymentStatus;


    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getLoanAnnualRate() {
        return LoanAnnualRate;
    }

    public void setLoanAnnualRate(String loanAnnualRate) {
        LoanAnnualRate = loanAnnualRate;
    }

    public String getLoanDays() {
        return LoanDays;
    }

    public void setLoanDays(String loanDays) {
        LoanDays = loanDays;
    }

    public String getMaxLoan() {
        return MaxLoan;
    }

    public void setMaxLoan(String maxLoan) {
        MaxLoan = maxLoan;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBankAccName() {
        return BankAccName;
    }

    public void setBankAccName(String bankAccName) {
        BankAccName = bankAccName;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String mainRiskCode) {
        MainRiskCode = mainRiskCode;
    }

    public String getMainRiskName() {
        return MainRiskName;
    }

    public void setMainRiskName(String mainRiskName) {
        MainRiskName = mainRiskName;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getCashValue() {
        return CashValue;
    }

    public void setCashValue(String cashValue) {
        CashValue = cashValue;
    }

    public com.sinosoft.lis.pos.entity.AddtRisksPojo getAddtRisksPojo() {
        return AddtRisksPojo;
    }

    @JSONField(name = "addtRisks")
    public void setAddtRisksPojo(com.sinosoft.lis.pos.entity.AddtRisksPojo addtRisksPojo) {
        AddtRisksPojo = addtRisksPojo;
    }


    public com.sinosoft.lis.pos.entity.MainRisksPojo getMainRisksPojo() {
        return MainRisksPojo;
    }

    @JSONField(name = "mainRisks")
    public void setMainRisksPojo(com.sinosoft.lis.pos.entity.MainRisksPojo mainRisksPojo) {
        MainRisksPojo = mainRisksPojo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getLoanMoney() {
        return LoanMoney;
    }

    public void setLoanMoney(String loanMoney) {
        LoanMoney = loanMoney;
    }

    public String getRepaymentStatus() {
        return RepaymentStatus;
    }

    public void setRepaymentStatus(String repaymentStatus) {
        RepaymentStatus = repaymentStatus;
    }

    @Override

    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespPolicyLoanDetailQueryPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", LoanAnnualRate='" + LoanAnnualRate + '\'' +
                ", LoanDays='" + LoanDays + '\'' +
                ", MaxLoan='" + MaxLoan + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BankAccName='" + BankAccName + '\'' +
                ", BankAccNo='" + BankAccNo + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                ", MainRiskCode='" + MainRiskCode + '\'' +
                ", MainRiskName='" + MainRiskName + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", Prem='" + Prem + '\'' +
                ", CashValue='" + CashValue + '\'' +
                ", AddtRisksPojo=" + AddtRisksPojo +
                ", ContNo='" + ContNo + '\'' +
                ", LoanMoney='" + LoanMoney + '\'' +
                ", MainRisksPojo=" + MainRisksPojo +
                ", RepaymentStatus='" + RepaymentStatus + '\'' +
                '}';
    }
}
