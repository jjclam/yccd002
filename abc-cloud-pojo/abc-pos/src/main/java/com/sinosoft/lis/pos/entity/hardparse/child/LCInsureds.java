package com.sinosoft.lis.pos.entity.hardparse.child;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public class LCInsureds {
    private List<LCInsured> LCInsured;

    @Override
    public String toString() {
        return "LCInsureds{" +
                "LCInsured=" + LCInsured +
                '}';
    }

    public List<LCInsured> getLCInsured() {
        return LCInsured;
    }
    @JSONField(name = "lCInsured")
    public void setLCInsured(List<LCInsured> LCInsured) {
        this.LCInsured = LCInsured;
    }
}
