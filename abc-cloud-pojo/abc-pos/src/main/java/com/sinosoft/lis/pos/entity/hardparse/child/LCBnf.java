package com.sinosoft.lis.pos.entity.hardparse.child;

public class LCBnf {
    //受益人类型
    private String BnfType;
    //受益人名字
    private String Name;
    //受益人证件号码
    private String IdNo;
    //与被保人关系
    private String RelaToInsured;
    //受益顺序
    private String BnfNo;

    @Override
    public String toString() {
        return "LCBnf{" +
                "BnfType='" + BnfType + '\'' +
                ", Name='" + Name + '\'' +
                ", IdNo='" + IdNo + '\'' +
                ", RelaToInsured='" + RelaToInsured + '\'' +
                ", BnfNo='" + BnfNo + '\'' +
                '}';
    }

    public String getBnfType() {
        return BnfType;
    }

    public void setBnfType(String bnfType) {
        BnfType = bnfType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getIdNo() {
        return IdNo;
    }

    public void setIdNo(String idNo) {
        IdNo = idNo;
    }

    public String getRelaToInsured() {
        return RelaToInsured;
    }

    public void setRelaToInsured(String relaToInsured) {
        RelaToInsured = relaToInsured;
    }

    public String getBnfNo() {
        return BnfNo;
    }

    public void setBnfNo(String bnfNo) {
        BnfNo = bnfNo;
    }
}
