package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class BonusRisksPojo implements Body,Serializable{
    private List<com.sinosoft.lis.pos.entity.BonusRiskPojo> BonusRiskPojo;

    @JSONField(name = "bonusRisk")
    public List<com.sinosoft.lis.pos.entity.BonusRiskPojo> getBonusRiskPojo() {
        return BonusRiskPojo;
    }
    @JSONField(name = "bonusRisk")
    public void setBonusRiskPojo(List<com.sinosoft.lis.pos.entity.BonusRiskPojo> bonusRiskPojo) {
        BonusRiskPojo = bonusRiskPojo;
    }



    @Override
    public String toString() {
        return "BonusRisksPojo{" +
                "BonusRiskPojo=" + BonusRiskPojo +
                '}';
    }
}