package com.sinosoft.lis.pos.entity;


import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

public class CashValuePojo implements Body,Serializable{
    //保单年期
    private String plcYear;
    //现金价值
    private String cash;

    @Override
    public String toString() {
        return "CashValuePojo{" +
                "plcYear='" + plcYear + '\'' +
                ", cash='" + cash + '\'' +
                '}';
    }

    public String getPlcYear() {
        return plcYear;
    }

    public void setPlcYear(String plcYear) {
        this.plcYear = plcYear;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

}