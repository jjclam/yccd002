package com.sinosoft.lis.pos.entity;

import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;

public class WXContInfoRePojo  implements Body,Serializable {
    //姓名
    private String AppntName;
    //省
    private String Province;
    //市
    private String City;
    private String Powiat;
    //街道
    private String Street;
    //通讯邮编
    private String ZipCode;
    private String Village;
    //手机号
    private String MobilePhone;

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPowiat() {
        return Powiat;
    }

    public void setPowiat(String powiat) {
        Powiat = powiat;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getVillage() {
        return Village;
    }

    public void setVillage(String village) {
        Village = village;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        MobilePhone = mobilePhone;
    }

    @Override
    public String toString() {
        return "WXContInfoRePojo{" +
                "AppntName='" + AppntName + '\'' +
                ", Province='" + Province + '\'' +
                ", City='" + City + '\'' +
                ", Powiat='" + Powiat + '\'' +
                ", Street='" + Street + '\'' +
                ", ZipCode='" + ZipCode + '\'' +
                ", Village='" + Village + '\'' +
                ", MobilePhone='" + MobilePhone + '\'' +
                '}';
    }
}