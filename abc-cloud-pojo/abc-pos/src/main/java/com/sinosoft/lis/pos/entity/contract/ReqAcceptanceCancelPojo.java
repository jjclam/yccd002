package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 承保前撤单NB001.
 */
public class ReqAcceptanceCancelPojo implements ReqBody, Body,Serializable{
    //保单号
    private String ContNo;
    //投保单号
    private String ProposalContNo;
    //撤单系统
    private String DrawSystem;
    //撤单原因编码
    private String DreasonCode;
    //撤单原因
    private String DrawReason;
    //印刷号
    private String PrtNo;

    @Override
    public String toString() {
        return "ReqAcceptanceCancelPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", ProposalContNo='" + ProposalContNo + '\'' +
                ", DrawSystem='" + DrawSystem + '\'' +
                ", DreasonCode='" + DreasonCode + '\'' +
                ", DrawReason='" + DrawReason + '\'' +
                ", PrtNo='" + PrtNo + '\'' +
                '}';
    }

    public String getDreasonCode() {
        return DreasonCode;
    }

    public void setDreasonCode(String dreasonCode) {
        DreasonCode = dreasonCode;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getProposalContNo() {
        return ProposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        ProposalContNo = proposalContNo;
    }

    public String getDrawSystem() {
        return DrawSystem;
    }

    public void setDrawSystem(String drawSystem) {
        DrawSystem = drawSystem;
    }

    public String getDrawReason() {
        return DrawReason;
    }

    public void setDrawReason(String drawReason) {
        DrawReason = drawReason;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }


}