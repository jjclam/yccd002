package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;

public class BnfInfosPojo implements Body, Serializable {
    private List<BnfInfoPojo> BnfInfoPojo;

    @JSONField(name = "bnfInfo")
    public List<com.sinosoft.lis.pos.entity.BnfInfoPojo> getBnfInfoPojo() {
        return BnfInfoPojo;
    }

    @JSONField(name = "bnfInfo")
    public void setBnfInfoPojo(List<com.sinosoft.lis.pos.entity.BnfInfoPojo> bnfInfoPojo) {
        BnfInfoPojo = bnfInfoPojo;
    }

    @Override
    public String toString() {
        return "BnfInfosPojo{" +
                "BnfInfoPojo=" + BnfInfoPojo +
                '}';
    }
}