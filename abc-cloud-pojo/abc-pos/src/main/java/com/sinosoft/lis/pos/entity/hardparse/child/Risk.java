package com.sinosoft.lis.pos.entity.hardparse.child;

public class Risk {
    //险种编码
    private String RiskCode;
    //份数
    private String Mult;
    //保费
    private String Prem;
    //主附险标识
    private String MainPolFlag;
    //险种名称
    private String RiskName;

    @Override
    public String toString() {
        return "Risk{" +
                "RiskCode='" + RiskCode + '\'' +
                ", Mult='" + Mult + '\'' +
                ", Prem='" + Prem + '\'' +
                ", MainPolFlag='" + MainPolFlag + '\'' +
                ", RiskName='" + RiskName + '\'' +
                '}';
    }

    public String getRiskName() {
        return RiskName;
    }

    public void setRiskName(String riskName) {
        RiskName = riskName;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getMult() {
        return Mult;
    }

    public void setMult(String mult) {
        Mult = mult;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getMainPolFlag() {
        return MainPolFlag;
    }

    public void setMainPolFlag(String mainPolFlag) {
        MainPolFlag = mainPolFlag;
    }
}
