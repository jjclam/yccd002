package com.sinosoft.lis.pos.entity.hardparse.child;

public class ActuaryDuty {
    //现金价值
    private CashValues CashValues;
    //减额交清保额
    private ReducedVolumes ReducedVolumes;

    @Override
    public String toString() {
        return "ActuaryDuty{" +
                "CashValues=" + CashValues +
                ", ReducedVolumes=" + ReducedVolumes +
                '}';
    }

    public ReducedVolumes getReducedVolumes() {
        return ReducedVolumes;
    }

    public void setReducedVolumes(ReducedVolumes reducedVolumes) {
        ReducedVolumes = reducedVolumes;
    }

    public CashValues getCashValues() {
        return CashValues;
    }

    public void setCashValues(CashValues cashValues) {
        CashValues = cashValues;
    }
}
