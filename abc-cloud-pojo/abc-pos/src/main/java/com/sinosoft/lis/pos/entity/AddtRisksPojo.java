package com.sinosoft.lis.pos.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;
import java.util.List;


public class AddtRisksPojo implements Body, Serializable {
    //附加险份数
    private String Count;

    private List<AddtRiskPojo> AddtRiskPojo;

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    @JSONField(name = "addtRisk")
    public List<AddtRiskPojo> getAddtRiskPojo() {
        return AddtRiskPojo;
    }

    @JSONField(name = "addtRisk")
    public void setAddtRiskPojo(List<AddtRiskPojo> addtRiskPojo) {
        AddtRiskPojo = addtRiskPojo;
    }

    @Override
    public String toString() {
        return "AddtRisksPojo{" +
                "Count='" + Count + '\'' +
                ", AddtRiskPojo=" + AddtRiskPojo +
                '}';
    }
}
