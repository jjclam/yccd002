package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class PolicyValueInfosPojo implements Body,Serializable{
    private List<PolicyValueInfoPojo> PolicyValueInfoPojo;

    @JSONField(name = "policyValueInfo")
    public List<com.sinosoft.lis.pos.entity.PolicyValueInfoPojo> getPolicyValueInfoPojo() {
        return PolicyValueInfoPojo;
    }
    @JSONField(name = "policyValueInfo")
    public void setPolicyValueInfoPojo(List<com.sinosoft.lis.pos.entity.PolicyValueInfoPojo> policyValueInfoPojo) {
        PolicyValueInfoPojo = policyValueInfoPojo;
    }


    @Override
    public String toString() {
        return "PolicyValueInfosPojo{" +
                "PolicyValueInfoPojo=" + PolicyValueInfoPojo +
                '}';
    }
}