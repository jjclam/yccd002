package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.utility.Pojo;

import java.io.Serializable;

/**
 * 4011-手续费与追加保费POS117.
 */

@Relation(transID = "POS117")
public class RespHandChargeAddPremPojo implements Pojo, Body, Serializable {
    //保全项目编码
    private String EdorType;
    //手续费
    private String ChargeFee;
    //账户减少金额
    private String AccontReduMoney;


    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getChargeFee() {
        return ChargeFee;
    }

    public void setChargeFee(String chargeFee) {
        ChargeFee = chargeFee;
    }

    public String getAccontReduMoney() {
        return AccontReduMoney;
    }

    public void setAccontReduMoney(String accontReduMoney) {
        AccontReduMoney = accontReduMoney;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespHandChargeAddPremPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ChargeFee='" + ChargeFee + '\'' +
                ", AccontReduMoney='" + AccontReduMoney + '\'' +
                '}';
    }
}