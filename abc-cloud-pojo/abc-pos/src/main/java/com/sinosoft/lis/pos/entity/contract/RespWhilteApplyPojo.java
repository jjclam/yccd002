package com.sinosoft.lis.pos.entity.contract;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 白名单提交NB005.
 */
@Relation(transID = "NB005")
public class RespWhilteApplyPojo implements Pojo,Body,Serializable{
    //姓名
    private String CustomerName;
    //证件号码
    private String CustomerIDNo;
    //客户是否已存在
    private String DealResult;

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        CustomerIDNo = customerIDNo;
    }

    public String getDealResult() {
        return DealResult;
    }

    public void setDealResult(String dealResult) {
        DealResult = dealResult;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespWhilteApplyPojo{" +
                "CustomerName='" + CustomerName + '\'' +
                ", CustomerIDNo='" + CustomerIDNo + '\'' +
                ", DealResult='" + DealResult + '\'' +
                '}';
    }
}