package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 退保申请POS089
 */
public class ReqSurrenderApplyPojo implements ReqBody, Body,Serializable{
    //保全项目编码
    private String EdorType;
    //保单号
    private String ContNo;
    //投保人证件类型
    private String IdKind;
    //投保人证件号码
    private String IdCode;
    //保单印刷号
    private String PrintCode;
    //申请人姓名
    private String ClientName;
    //账（卡）号
    private String PayAcc;
    //银行编码
    private String BankCode;
    //地区代码
    private String ZoneNo;
    //网点编码
    private String BranchNo;
    //领取金额
    private String GetAmnt;
    //柜员代码
    private String TellerNo;

    @Override
    public String toString() {
        return "ReqSurrenderApplyPojo{" +
                "EdorType='" + EdorType + '\'' +
                ", ContNo='" + ContNo + '\'' +
                ", IdKind='" + IdKind + '\'' +
                ", IdCode='" + IdCode + '\'' +
                ", PrintCode='" + PrintCode + '\'' +
                ", ClientName='" + ClientName + '\'' +
                ", PayAcc='" + PayAcc + '\'' +
                ", BankCode='" + BankCode + '\'' +
                ", ZoneNo='" + ZoneNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", GetAmnt='" + GetAmnt + '\'' +
                ", TellerNo='" + TellerNo + '\'' +
                '}';
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String edorType) {
        EdorType = edorType;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getIdKind() {
        return IdKind;
    }

    public void setIdKind(String idKind) {
        IdKind = idKind;
    }

    public String getIdCode() {
        return IdCode;
    }

    public void setIdCode(String idCode) {
        IdCode = idCode;
    }

    public String getPrintCode() {
        return PrintCode;
    }

    public void setPrintCode(String printCode) {
        PrintCode = printCode;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public String getPayAcc() {
        return PayAcc;
    }

    public void setPayAcc(String payAcc) {
        PayAcc = payAcc;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getZoneNo() {
        return ZoneNo;
    }

    public void setZoneNo(String zoneNo) {
        ZoneNo = zoneNo;
    }

    public String getBranchNo() {
        return BranchNo;
    }

    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    public String getGetAmnt() {
        return GetAmnt;
    }

    public void setGetAmnt(String getAmnt) {
        GetAmnt = getAmnt;
    }

    public String getTellerNo() {
        return TellerNo;
    }

    public void setTellerNo(String tellerNo) {
        TellerNo = tellerNo;
    }

}
