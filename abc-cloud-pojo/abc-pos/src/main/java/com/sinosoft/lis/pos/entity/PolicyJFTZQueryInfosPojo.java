package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class PolicyJFTZQueryInfosPojo implements Body,Serializable{
    private List<com.sinosoft.lis.pos.entity.PolicyJFTZQueryInfoPojo> PolicyJFTZQueryInfoPojo;

    @JSONField(name="policyJFTZQueryInfo")
    public List<com.sinosoft.lis.pos.entity.PolicyJFTZQueryInfoPojo> getPolicyJFTZQueryInfoPojo() {
        return PolicyJFTZQueryInfoPojo;
    }
    @JSONField(name="policyJFTZQueryInfo")
    public void setPolicyJFTZQueryInfoPojo(List<com.sinosoft.lis.pos.entity.PolicyJFTZQueryInfoPojo> policyJFTZQueryInfoPojo) {
        PolicyJFTZQueryInfoPojo = policyJFTZQueryInfoPojo;
    }

    @Override
    public String toString() {
        return "PolicyJFTZQueryInfosPojo{" +
                "PolicyJFTZQueryInfoPojo=" + PolicyJFTZQueryInfoPojo +
                '}';
    }
}