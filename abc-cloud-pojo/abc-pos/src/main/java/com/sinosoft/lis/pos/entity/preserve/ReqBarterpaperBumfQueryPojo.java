package com.sinosoft.lis.pos.entity.preserve;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 换发纸质保单查询POS094.
 */
public class ReqBarterpaperBumfQueryPojo implements ReqBody, Body, Serializable {
    //保单号
    private String ContNo;
    //保单查询类型
    private String ELecType;
    //客户姓名
    private String CustomerName;
    //证件类型
    private String CustomerIdType;
    //证件号码
    private String CustomerIdNo;

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String contNo) {
        ContNo = contNo;
    }

    public String getELecType() {
        return ELecType;
    }

    public void setELecType(String ELecType) {
        this.ELecType = ELecType;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerIdType() {
        return CustomerIdType;
    }

    public void setCustomerIdType(String customerIdType) {
        CustomerIdType = customerIdType;
    }

    public String getCustomerIdNo() {
        return CustomerIdNo;
    }

    public void setCustomerIdNo(String customerIdNo) {
        CustomerIdNo = customerIdNo;
    }

    @Override
    public String toString() {
        return "ReqBarterpaperBumfQueryPojo{" +
                "ContNo='" + ContNo + '\'' +
                ", ELecType='" + ELecType + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", CustomerIdType='" + CustomerIdType + '\'' +
                ", CustomerIdNo='" + CustomerIdNo + '\'' +
                '}';
    }
}