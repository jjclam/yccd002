package com.sinosoft.lis.pos.entity;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;
import java.util.List;

public class PolicyQueryInfosPojo implements Body,Serializable{
    private List<com.sinosoft.lis.pos.entity.PolicyQueryInfoPojo> PolicyQueryInfoPojo;

    @Override
    public String toString() {
        return "PolicyQueryInfosPojo{" +
                "PolicyQueryInfoPojo=" + PolicyQueryInfoPojo +
                '}';
    }

    @JSONField(name = "policyQueryInfo")
    public List<com.sinosoft.lis.pos.entity.PolicyQueryInfoPojo> getPolicyQueryInfoPojo() {
        return PolicyQueryInfoPojo;
    }
    @JSONField(name = "policyQueryInfo")
    public void setPolicyQueryInfoPojo(List<com.sinosoft.lis.pos.entity.PolicyQueryInfoPojo> policyQueryInfoPojo) {
        PolicyQueryInfoPojo = policyQueryInfoPojo;
    }
}
