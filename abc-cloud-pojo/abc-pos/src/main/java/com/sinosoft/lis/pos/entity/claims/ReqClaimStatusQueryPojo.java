package com.sinosoft.lis.pos.entity.claims;

import com.sinosoft.lis.pos.entity.inter.Body;
import com.sinosoft.lis.pos.entity.request.ReqBody;

import java.io.Serializable;

/**
 * 理赔影像状态查询CLM004.
 */
public class ReqClaimStatusQueryPojo implements ReqBody, Body,Serializable {
    //理赔号
    private String ClaimNo;

    public String getClaimNo() {
        return ClaimNo;
    }

    public void setClaimNo(String claimNo) {
        ClaimNo = claimNo;
    }

    @Override
    public String toString() {
        return "ReqClaimStatusQueryPojo{" +
                "ClaimNo='" + ClaimNo + '\'' +
                '}';
    }
}