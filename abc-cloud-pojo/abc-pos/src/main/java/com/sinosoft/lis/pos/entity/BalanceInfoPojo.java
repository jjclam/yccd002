package com.sinosoft.lis.pos.entity;

import com.sinosoft.lis.pos.entity.inter.Body;

import java.io.Serializable;

public class BalanceInfoPojo implements Body,Serializable {
    //结算日期
    private String BalanceDate;
    //结算利率
    private String BalanceRate;

    public String getBalanceDate() {
        return BalanceDate;
    }

    public void setBalanceDate(String balanceDate) {
        BalanceDate = balanceDate;
    }

    public String getBalanceRate() {
        return BalanceRate;
    }

    public void setBalanceRate(String balanceRate) {
        BalanceRate = balanceRate;
    }

    @Override
    public String toString() {
        return "BalanceInfoPojo{" +
                "BalanceDate='" + BalanceDate + '\'' +
                ", BalanceRate='" + BalanceRate + '\'' +
                '}';
    }
}