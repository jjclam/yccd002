package com.sinosoft.lis.pos.entity.renewal;

import com.sinosoft.lis.pos.entity.annotation.Relation;
import com.sinosoft.utility.Pojo;import java.io.Serializable;import com.sinosoft.lis.pos.entity.inter.Body;;

/**
 * 续期缴费查询XQ001.
 */
@Relation(transID = "XQ001")
public class RespRenewalFeeQueryPojo implements Pojo,Body,Serializable{
    //投保人姓名
    private String AppntName;
    //投保人证件类型
    private String IDType;
    //投保人证件类型
    private String IDNo;
    //应缴期数
    private String DuePeriod;
    //应缴日期
    private String DueDate;
    //应缴金额
    private String DueAmt;
    //险种编码
    private String RiskCode;
    //保单号
    private String Contno;

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String appntName) {
        AppntName = appntName;
    }

    public String getIDType() {
        return IDType;
    }

    public void setIDType(String IDType) {
        this.IDType = IDType;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getDuePeriod() {
        return DuePeriod;
    }

    public void setDuePeriod(String duePeriod) {
        DuePeriod = duePeriod;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getDueAmt() {
        return DueAmt;
    }

    public void setDueAmt(String dueAmt) {
        DueAmt = dueAmt;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getContno() {
        return Contno;
    }

    public void setContno(String contno) {
        Contno = contno;
    }

    @Override
    public String getV(String s) {
        return null;
    }

    @Override
    public String getV(int i) {
        return null;
    }

    @Override
    public int getFieldType(String s) {
        return 0;
    }

    @Override
    public int getFieldType(int i) {
        return 0;
    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public int getFieldIndex(String s) {
        return 0;
    }

    @Override
    public String getFieldName(int i) {
        return null;
    }

    @Override
    public boolean setV(String s, String s1) {
        return false;
    }

    @Override
    public String toString() {
        return "RespRenewalFeeQueryPojo{" +
                "AppntName='" + AppntName + '\'' +
                ", IDType='" + IDType + '\'' +
                ", IDNo='" + IDNo + '\'' +
                ", DuePeriod='" + DuePeriod + '\'' +
                ", DueDate='" + DueDate + '\'' +
                ", DueAmt='" + DueAmt + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", Contno='" + Contno + '\'' +
                '}';
    }
}