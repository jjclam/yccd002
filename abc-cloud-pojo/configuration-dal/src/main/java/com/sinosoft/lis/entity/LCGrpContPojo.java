/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCGrpContPojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LCGrpContPojo implements  Pojo,Serializable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo; 
    /** 集体投保单号码 */
    private String ProposalGrpContNo; 
    /** 印刷号码 */
    private String PrtNo; 
    /** 销售渠道 */
    private String SaleChnl; 
    /** 管理机构 */
    private String ManageCom; 
    /** 保单口令 */
    private String Password; 
    /** 密码 */
    private String Password2; 
    /** 客户号码 */
    private String AppntNo; 
    /** 投保总人数 */
    private int Peoples2; 
    /** 付款方式 */
    private String GetFlag; 
    /** 合同争议处理方式 */
    private String DisputedFlag; 
    /** 集体特别约定 */
    private String GrpSpec; 
    /** 交费方式 */
    private String PayMode; 
    /** 签单机构 */
    private String SignCom; 
    /** 签单日期 */
    private String  SignDate;
    /** 签单时间 */
    private String SignTime; 
    /** 保单生效日期 */
    private String  CValiDate;
    /** 交费间隔 */
    private int PayIntv; 
    /** 总人数 */
    private int Peoples; 
    /** 总份数 */
    private double Mult; 
    /** 总保费 */
    private double Prem; 
    /** 总保额 */
    private double Amnt; 
    /** 总累计保费 */
    private double SumPrem; 
    /** 总累计交费 */
    private double SumPay; 
    /** 差额 */
    private double Dif; 
    /** 备注 */
    private String Remark; 
    /** 备用属性字段1 */
    private String StandbyFlag1; 
    /** 备用属性字段2 */
    private String StandbyFlag2; 
    /** 备用属性字段3 */
    private String StandbyFlag3; 
    /** 复核状态 */
    private String ApproveFlag; 
    /** 复核人编码 */
    private String ApproveCode; 
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime; 
    /** 核保人 */
    private String UWOperator; 
    /** 核保状态 */
    private String UWFlag; 
    /** 核保完成日期 */
    private String  UWDate;
    /** 核保完成时间 */
    private String UWTime; 
    /** 投保单/保单标志 */
    private String AppFlag; 
    /** 投保单申请日期 */
    private String  PolApplyDate;
    /** 保单回执客户签收日期 */
    private String  CustomGetPolDate;
    /** 保单送达日期 */
    private String  GetPolDate;
    /** 保单送达时间 */
    private String GetPolTime; 
    /** 状态 */
    private String State; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 初审人 */
    private String FirstTrialOperator; 
    /** 初审日期 */
    private String  FirstTrialDate;
    /** 初审时间 */
    private String FirstTrialTime; 
    /** 总单类型 */
    private String ContType; 
    /** 年金领取日期 */
    private String  AnnuityReceiveDate;
    /** 保障终止日期 */
    private String  CEndDate;
    /** 最初承保人数 */
    private int NativePeoples; 
    /** 最初总保费 */
    private double NativePrem; 
    /** 最初总保额 */
    private double NativeAmnt; 
    /** 减保频率 */
    private String ReduceFre; 
    /** 官网登录账号 */
    private String OffAcc; 
    /** 官网登录密码 */
    private String OffPwd; 
    /** 销售渠道编码 */
    private String SaleChannels; 
    /** 电子投保单地址 */
    private String E_AppntID; 
    /** 电子投保单生成日期 */
    private String  E_AppntDate;
    /** 电子合同地址 */
    private String E_ContID; 
    /** 电子合同生成日期 */
    private String  E_ContDate;
    /** 是否为续保单 */
    private String IsRenewFlag; 


    public static final int FIELDNUM = 66;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getProposalGrpContNo() {
        return ProposalGrpContNo;
    }
    public void setProposalGrpContNo(String aProposalGrpContNo) {
        ProposalGrpContNo = aProposalGrpContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getPassword2() {
        return Password2;
    }
    public void setPassword2(String aPassword2) {
        Password2 = aPassword2;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public int getPeoples2() {
        return Peoples2;
    }
    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }
    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public String getGetFlag() {
        return GetFlag;
    }
    public void setGetFlag(String aGetFlag) {
        GetFlag = aGetFlag;
    }
    public String getDisputedFlag() {
        return DisputedFlag;
    }
    public void setDisputedFlag(String aDisputedFlag) {
        DisputedFlag = aDisputedFlag;
    }
    public String getGrpSpec() {
        return GrpSpec;
    }
    public void setGrpSpec(String aGrpSpec) {
        GrpSpec = aGrpSpec;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getSignCom() {
        return SignCom;
    }
    public void setSignCom(String aSignCom) {
        SignCom = aSignCom;
    }
    public String getSignDate() {
        return SignDate;
    }
    public void setSignDate(String aSignDate) {
        SignDate = aSignDate;
    }
    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getCValiDate() {
        return CValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        CValiDate = aCValiDate;
    }
    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getSumPay() {
        return SumPay;
    }
    public void setSumPay(double aSumPay) {
        SumPay = aSumPay;
    }
    public void setSumPay(String aSumPay) {
        if (aSumPay != null && !aSumPay.equals("")) {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = d;
        }
    }

    public double getDif() {
        return Dif;
    }
    public void setDif(double aDif) {
        Dif = aDif;
    }
    public void setDif(String aDif) {
        if (aDif != null && !aDif.equals("")) {
            Double tDouble = new Double(aDif);
            double d = tDouble.doubleValue();
            Dif = d;
        }
    }

    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        return ApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWDate() {
        return UWDate;
    }
    public void setUWDate(String aUWDate) {
        UWDate = aUWDate;
    }
    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getPolApplyDate() {
        return PolApplyDate;
    }
    public void setPolApplyDate(String aPolApplyDate) {
        PolApplyDate = aPolApplyDate;
    }
    public String getCustomGetPolDate() {
        return CustomGetPolDate;
    }
    public void setCustomGetPolDate(String aCustomGetPolDate) {
        CustomGetPolDate = aCustomGetPolDate;
    }
    public String getGetPolDate() {
        return GetPolDate;
    }
    public void setGetPolDate(String aGetPolDate) {
        GetPolDate = aGetPolDate;
    }
    public String getGetPolTime() {
        return GetPolTime;
    }
    public void setGetPolTime(String aGetPolTime) {
        GetPolTime = aGetPolTime;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFirstTrialOperator() {
        return FirstTrialOperator;
    }
    public void setFirstTrialOperator(String aFirstTrialOperator) {
        FirstTrialOperator = aFirstTrialOperator;
    }
    public String getFirstTrialDate() {
        return FirstTrialDate;
    }
    public void setFirstTrialDate(String aFirstTrialDate) {
        FirstTrialDate = aFirstTrialDate;
    }
    public String getFirstTrialTime() {
        return FirstTrialTime;
    }
    public void setFirstTrialTime(String aFirstTrialTime) {
        FirstTrialTime = aFirstTrialTime;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getAnnuityReceiveDate() {
        return AnnuityReceiveDate;
    }
    public void setAnnuityReceiveDate(String aAnnuityReceiveDate) {
        AnnuityReceiveDate = aAnnuityReceiveDate;
    }
    public String getCEndDate() {
        return CEndDate;
    }
    public void setCEndDate(String aCEndDate) {
        CEndDate = aCEndDate;
    }
    public int getNativePeoples() {
        return NativePeoples;
    }
    public void setNativePeoples(int aNativePeoples) {
        NativePeoples = aNativePeoples;
    }
    public void setNativePeoples(String aNativePeoples) {
        if (aNativePeoples != null && !aNativePeoples.equals("")) {
            Integer tInteger = new Integer(aNativePeoples);
            int i = tInteger.intValue();
            NativePeoples = i;
        }
    }

    public double getNativePrem() {
        return NativePrem;
    }
    public void setNativePrem(double aNativePrem) {
        NativePrem = aNativePrem;
    }
    public void setNativePrem(String aNativePrem) {
        if (aNativePrem != null && !aNativePrem.equals("")) {
            Double tDouble = new Double(aNativePrem);
            double d = tDouble.doubleValue();
            NativePrem = d;
        }
    }

    public double getNativeAmnt() {
        return NativeAmnt;
    }
    public void setNativeAmnt(double aNativeAmnt) {
        NativeAmnt = aNativeAmnt;
    }
    public void setNativeAmnt(String aNativeAmnt) {
        if (aNativeAmnt != null && !aNativeAmnt.equals("")) {
            Double tDouble = new Double(aNativeAmnt);
            double d = tDouble.doubleValue();
            NativeAmnt = d;
        }
    }

    public String getReduceFre() {
        return ReduceFre;
    }
    public void setReduceFre(String aReduceFre) {
        ReduceFre = aReduceFre;
    }
    public String getOffAcc() {
        return OffAcc;
    }
    public void setOffAcc(String aOffAcc) {
        OffAcc = aOffAcc;
    }
    public String getOffPwd() {
        return OffPwd;
    }
    public void setOffPwd(String aOffPwd) {
        OffPwd = aOffPwd;
    }
    public String getSaleChannels() {
        return SaleChannels;
    }
    public void setSaleChannels(String aSaleChannels) {
        SaleChannels = aSaleChannels;
    }
    public String getE_AppntID() {
        return E_AppntID;
    }
    public void setE_AppntID(String aE_AppntID) {
        E_AppntID = aE_AppntID;
    }
    public String getE_AppntDate() {
        return E_AppntDate;
    }
    public void setE_AppntDate(String aE_AppntDate) {
        E_AppntDate = aE_AppntDate;
    }
    public String getE_ContID() {
        return E_ContID;
    }
    public void setE_ContID(String aE_ContID) {
        E_ContID = aE_ContID;
    }
    public String getE_ContDate() {
        return E_ContDate;
    }
    public void setE_ContDate(String aE_ContDate) {
        E_ContDate = aE_ContDate;
    }
    public String getIsRenewFlag() {
        return IsRenewFlag;
    }
    public void setIsRenewFlag(String aIsRenewFlag) {
        IsRenewFlag = aIsRenewFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ProposalGrpContNo") ) {
            return 1;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 2;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 3;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 4;
        }
        if( strFieldName.equals("Password") ) {
            return 5;
        }
        if( strFieldName.equals("Password2") ) {
            return 6;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 7;
        }
        if( strFieldName.equals("Peoples2") ) {
            return 8;
        }
        if( strFieldName.equals("GetFlag") ) {
            return 9;
        }
        if( strFieldName.equals("DisputedFlag") ) {
            return 10;
        }
        if( strFieldName.equals("GrpSpec") ) {
            return 11;
        }
        if( strFieldName.equals("PayMode") ) {
            return 12;
        }
        if( strFieldName.equals("SignCom") ) {
            return 13;
        }
        if( strFieldName.equals("SignDate") ) {
            return 14;
        }
        if( strFieldName.equals("SignTime") ) {
            return 15;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 16;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 17;
        }
        if( strFieldName.equals("Peoples") ) {
            return 18;
        }
        if( strFieldName.equals("Mult") ) {
            return 19;
        }
        if( strFieldName.equals("Prem") ) {
            return 20;
        }
        if( strFieldName.equals("Amnt") ) {
            return 21;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 22;
        }
        if( strFieldName.equals("SumPay") ) {
            return 23;
        }
        if( strFieldName.equals("Dif") ) {
            return 24;
        }
        if( strFieldName.equals("Remark") ) {
            return 25;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 26;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 27;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 28;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 29;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 30;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 31;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 32;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 33;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 34;
        }
        if( strFieldName.equals("UWDate") ) {
            return 35;
        }
        if( strFieldName.equals("UWTime") ) {
            return 36;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 37;
        }
        if( strFieldName.equals("PolApplyDate") ) {
            return 38;
        }
        if( strFieldName.equals("CustomGetPolDate") ) {
            return 39;
        }
        if( strFieldName.equals("GetPolDate") ) {
            return 40;
        }
        if( strFieldName.equals("GetPolTime") ) {
            return 41;
        }
        if( strFieldName.equals("State") ) {
            return 42;
        }
        if( strFieldName.equals("Operator") ) {
            return 43;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 44;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 45;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 46;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 47;
        }
        if( strFieldName.equals("FirstTrialOperator") ) {
            return 48;
        }
        if( strFieldName.equals("FirstTrialDate") ) {
            return 49;
        }
        if( strFieldName.equals("FirstTrialTime") ) {
            return 50;
        }
        if( strFieldName.equals("ContType") ) {
            return 51;
        }
        if( strFieldName.equals("AnnuityReceiveDate") ) {
            return 52;
        }
        if( strFieldName.equals("CEndDate") ) {
            return 53;
        }
        if( strFieldName.equals("NativePeoples") ) {
            return 54;
        }
        if( strFieldName.equals("NativePrem") ) {
            return 55;
        }
        if( strFieldName.equals("NativeAmnt") ) {
            return 56;
        }
        if( strFieldName.equals("ReduceFre") ) {
            return 57;
        }
        if( strFieldName.equals("OffAcc") ) {
            return 58;
        }
        if( strFieldName.equals("OffPwd") ) {
            return 59;
        }
        if( strFieldName.equals("SaleChannels") ) {
            return 60;
        }
        if( strFieldName.equals("E_AppntID") ) {
            return 61;
        }
        if( strFieldName.equals("E_AppntDate") ) {
            return 62;
        }
        if( strFieldName.equals("E_ContID") ) {
            return 63;
        }
        if( strFieldName.equals("E_ContDate") ) {
            return 64;
        }
        if( strFieldName.equals("IsRenewFlag") ) {
            return 65;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "PrtNo";
                break;
            case 3:
                strFieldName = "SaleChnl";
                break;
            case 4:
                strFieldName = "ManageCom";
                break;
            case 5:
                strFieldName = "Password";
                break;
            case 6:
                strFieldName = "Password2";
                break;
            case 7:
                strFieldName = "AppntNo";
                break;
            case 8:
                strFieldName = "Peoples2";
                break;
            case 9:
                strFieldName = "GetFlag";
                break;
            case 10:
                strFieldName = "DisputedFlag";
                break;
            case 11:
                strFieldName = "GrpSpec";
                break;
            case 12:
                strFieldName = "PayMode";
                break;
            case 13:
                strFieldName = "SignCom";
                break;
            case 14:
                strFieldName = "SignDate";
                break;
            case 15:
                strFieldName = "SignTime";
                break;
            case 16:
                strFieldName = "CValiDate";
                break;
            case 17:
                strFieldName = "PayIntv";
                break;
            case 18:
                strFieldName = "Peoples";
                break;
            case 19:
                strFieldName = "Mult";
                break;
            case 20:
                strFieldName = "Prem";
                break;
            case 21:
                strFieldName = "Amnt";
                break;
            case 22:
                strFieldName = "SumPrem";
                break;
            case 23:
                strFieldName = "SumPay";
                break;
            case 24:
                strFieldName = "Dif";
                break;
            case 25:
                strFieldName = "Remark";
                break;
            case 26:
                strFieldName = "StandbyFlag1";
                break;
            case 27:
                strFieldName = "StandbyFlag2";
                break;
            case 28:
                strFieldName = "StandbyFlag3";
                break;
            case 29:
                strFieldName = "ApproveFlag";
                break;
            case 30:
                strFieldName = "ApproveCode";
                break;
            case 31:
                strFieldName = "ApproveDate";
                break;
            case 32:
                strFieldName = "ApproveTime";
                break;
            case 33:
                strFieldName = "UWOperator";
                break;
            case 34:
                strFieldName = "UWFlag";
                break;
            case 35:
                strFieldName = "UWDate";
                break;
            case 36:
                strFieldName = "UWTime";
                break;
            case 37:
                strFieldName = "AppFlag";
                break;
            case 38:
                strFieldName = "PolApplyDate";
                break;
            case 39:
                strFieldName = "CustomGetPolDate";
                break;
            case 40:
                strFieldName = "GetPolDate";
                break;
            case 41:
                strFieldName = "GetPolTime";
                break;
            case 42:
                strFieldName = "State";
                break;
            case 43:
                strFieldName = "Operator";
                break;
            case 44:
                strFieldName = "MakeDate";
                break;
            case 45:
                strFieldName = "MakeTime";
                break;
            case 46:
                strFieldName = "ModifyDate";
                break;
            case 47:
                strFieldName = "ModifyTime";
                break;
            case 48:
                strFieldName = "FirstTrialOperator";
                break;
            case 49:
                strFieldName = "FirstTrialDate";
                break;
            case 50:
                strFieldName = "FirstTrialTime";
                break;
            case 51:
                strFieldName = "ContType";
                break;
            case 52:
                strFieldName = "AnnuityReceiveDate";
                break;
            case 53:
                strFieldName = "CEndDate";
                break;
            case 54:
                strFieldName = "NativePeoples";
                break;
            case 55:
                strFieldName = "NativePrem";
                break;
            case 56:
                strFieldName = "NativeAmnt";
                break;
            case 57:
                strFieldName = "ReduceFre";
                break;
            case 58:
                strFieldName = "OffAcc";
                break;
            case 59:
                strFieldName = "OffPwd";
                break;
            case 60:
                strFieldName = "SaleChannels";
                break;
            case 61:
                strFieldName = "E_AppntID";
                break;
            case 62:
                strFieldName = "E_AppntDate";
                break;
            case 63:
                strFieldName = "E_ContID";
                break;
            case 64:
                strFieldName = "E_ContDate";
                break;
            case 65:
                strFieldName = "IsRenewFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALGRPCONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "PASSWORD2":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "PEOPLES2":
                return Schema.TYPE_INT;
            case "GETFLAG":
                return Schema.TYPE_STRING;
            case "DISPUTEDFLAG":
                return Schema.TYPE_STRING;
            case "GRPSPEC":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "SIGNCOM":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_STRING;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_STRING;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPAY":
                return Schema.TYPE_DOUBLE;
            case "DIF":
                return Schema.TYPE_DOUBLE;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_STRING;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_STRING;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "POLAPPLYDATE":
                return Schema.TYPE_STRING;
            case "CUSTOMGETPOLDATE":
                return Schema.TYPE_STRING;
            case "GETPOLDATE":
                return Schema.TYPE_STRING;
            case "GETPOLTIME":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALOPERATOR":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALDATE":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALTIME":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "ANNUITYRECEIVEDATE":
                return Schema.TYPE_STRING;
            case "CENDDATE":
                return Schema.TYPE_STRING;
            case "NATIVEPEOPLES":
                return Schema.TYPE_INT;
            case "NATIVEPREM":
                return Schema.TYPE_DOUBLE;
            case "NATIVEAMNT":
                return Schema.TYPE_DOUBLE;
            case "REDUCEFRE":
                return Schema.TYPE_STRING;
            case "OFFACC":
                return Schema.TYPE_STRING;
            case "OFFPWD":
                return Schema.TYPE_STRING;
            case "SALECHANNELS":
                return Schema.TYPE_STRING;
            case "E_APPNTID":
                return Schema.TYPE_STRING;
            case "E_APPNTDATE":
                return Schema.TYPE_STRING;
            case "E_CONTID":
                return Schema.TYPE_STRING;
            case "E_CONTDATE":
                return Schema.TYPE_STRING;
            case "ISRENEWFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_INT;
            case 18:
                return Schema.TYPE_INT;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_DOUBLE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_INT;
            case 55:
                return Schema.TYPE_DOUBLE;
            case 56:
                return Schema.TYPE_DOUBLE;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("Password2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password2));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetFlag));
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
        }
        if (FCode.equalsIgnoreCase("GrpSpec")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpSpec));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveDate));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWDate));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolApplyDate));
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomGetPolDate));
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolDate));
        }
        if (FCode.equalsIgnoreCase("GetPolTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolTime));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialDate));
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("AnnuityReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AnnuityReceiveDate));
        }
        if (FCode.equalsIgnoreCase("CEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CEndDate));
        }
        if (FCode.equalsIgnoreCase("NativePeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePeoples));
        }
        if (FCode.equalsIgnoreCase("NativePrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePrem));
        }
        if (FCode.equalsIgnoreCase("NativeAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativeAmnt));
        }
        if (FCode.equalsIgnoreCase("ReduceFre")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReduceFre));
        }
        if (FCode.equalsIgnoreCase("OffAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffAcc));
        }
        if (FCode.equalsIgnoreCase("OffPwd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffPwd));
        }
        if (FCode.equalsIgnoreCase("SaleChannels")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChannels));
        }
        if (FCode.equalsIgnoreCase("E_AppntID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_AppntID));
        }
        if (FCode.equalsIgnoreCase("E_AppntDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_AppntDate));
        }
        if (FCode.equalsIgnoreCase("E_ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_ContID));
        }
        if (FCode.equalsIgnoreCase("E_ContDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_ContDate));
        }
        if (FCode.equalsIgnoreCase("IsRenewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsRenewFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 3:
                strFieldValue = String.valueOf(SaleChnl);
                break;
            case 4:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 5:
                strFieldValue = String.valueOf(Password);
                break;
            case 6:
                strFieldValue = String.valueOf(Password2);
                break;
            case 7:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 8:
                strFieldValue = String.valueOf(Peoples2);
                break;
            case 9:
                strFieldValue = String.valueOf(GetFlag);
                break;
            case 10:
                strFieldValue = String.valueOf(DisputedFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(GrpSpec);
                break;
            case 12:
                strFieldValue = String.valueOf(PayMode);
                break;
            case 13:
                strFieldValue = String.valueOf(SignCom);
                break;
            case 14:
                strFieldValue = String.valueOf(SignDate);
                break;
            case 15:
                strFieldValue = String.valueOf(SignTime);
                break;
            case 16:
                strFieldValue = String.valueOf(CValiDate);
                break;
            case 17:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 18:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 19:
                strFieldValue = String.valueOf(Mult);
                break;
            case 20:
                strFieldValue = String.valueOf(Prem);
                break;
            case 21:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 22:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 23:
                strFieldValue = String.valueOf(SumPay);
                break;
            case 24:
                strFieldValue = String.valueOf(Dif);
                break;
            case 25:
                strFieldValue = String.valueOf(Remark);
                break;
            case 26:
                strFieldValue = String.valueOf(StandbyFlag1);
                break;
            case 27:
                strFieldValue = String.valueOf(StandbyFlag2);
                break;
            case 28:
                strFieldValue = String.valueOf(StandbyFlag3);
                break;
            case 29:
                strFieldValue = String.valueOf(ApproveFlag);
                break;
            case 30:
                strFieldValue = String.valueOf(ApproveCode);
                break;
            case 31:
                strFieldValue = String.valueOf(ApproveDate);
                break;
            case 32:
                strFieldValue = String.valueOf(ApproveTime);
                break;
            case 33:
                strFieldValue = String.valueOf(UWOperator);
                break;
            case 34:
                strFieldValue = String.valueOf(UWFlag);
                break;
            case 35:
                strFieldValue = String.valueOf(UWDate);
                break;
            case 36:
                strFieldValue = String.valueOf(UWTime);
                break;
            case 37:
                strFieldValue = String.valueOf(AppFlag);
                break;
            case 38:
                strFieldValue = String.valueOf(PolApplyDate);
                break;
            case 39:
                strFieldValue = String.valueOf(CustomGetPolDate);
                break;
            case 40:
                strFieldValue = String.valueOf(GetPolDate);
                break;
            case 41:
                strFieldValue = String.valueOf(GetPolTime);
                break;
            case 42:
                strFieldValue = String.valueOf(State);
                break;
            case 43:
                strFieldValue = String.valueOf(Operator);
                break;
            case 44:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 45:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 46:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 47:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 48:
                strFieldValue = String.valueOf(FirstTrialOperator);
                break;
            case 49:
                strFieldValue = String.valueOf(FirstTrialDate);
                break;
            case 50:
                strFieldValue = String.valueOf(FirstTrialTime);
                break;
            case 51:
                strFieldValue = String.valueOf(ContType);
                break;
            case 52:
                strFieldValue = String.valueOf(AnnuityReceiveDate);
                break;
            case 53:
                strFieldValue = String.valueOf(CEndDate);
                break;
            case 54:
                strFieldValue = String.valueOf(NativePeoples);
                break;
            case 55:
                strFieldValue = String.valueOf(NativePrem);
                break;
            case 56:
                strFieldValue = String.valueOf(NativeAmnt);
                break;
            case 57:
                strFieldValue = String.valueOf(ReduceFre);
                break;
            case 58:
                strFieldValue = String.valueOf(OffAcc);
                break;
            case 59:
                strFieldValue = String.valueOf(OffPwd);
                break;
            case 60:
                strFieldValue = String.valueOf(SaleChannels);
                break;
            case 61:
                strFieldValue = String.valueOf(E_AppntID);
                break;
            case 62:
                strFieldValue = String.valueOf(E_AppntDate);
                break;
            case 63:
                strFieldValue = String.valueOf(E_ContID);
                break;
            case 64:
                strFieldValue = String.valueOf(E_ContDate);
                break;
            case 65:
                strFieldValue = String.valueOf(IsRenewFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
                ProposalGrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("Password2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password2 = FValue.trim();
            }
            else
                Password2 = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetFlag = FValue.trim();
            }
            else
                GetFlag = null;
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisputedFlag = FValue.trim();
            }
            else
                DisputedFlag = null;
        }
        if (FCode.equalsIgnoreCase("GrpSpec")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpSpec = FValue.trim();
            }
            else
                GrpSpec = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignCom = FValue.trim();
            }
            else
                SignCom = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignDate = FValue.trim();
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CValiDate = FValue.trim();
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Dif = d;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveDate = FValue.trim();
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWDate = FValue.trim();
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolApplyDate = FValue.trim();
            }
            else
                PolApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomGetPolDate = FValue.trim();
            }
            else
                CustomGetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolDate = FValue.trim();
            }
            else
                GetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolTime = FValue.trim();
            }
            else
                GetPolTime = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialOperator = FValue.trim();
            }
            else
                FirstTrialOperator = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialDate = FValue.trim();
            }
            else
                FirstTrialDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialTime = FValue.trim();
            }
            else
                FirstTrialTime = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("AnnuityReceiveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AnnuityReceiveDate = FValue.trim();
            }
            else
                AnnuityReceiveDate = null;
        }
        if (FCode.equalsIgnoreCase("CEndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CEndDate = FValue.trim();
            }
            else
                CEndDate = null;
        }
        if (FCode.equalsIgnoreCase("NativePeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                NativePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("NativePrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NativePrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("NativeAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NativeAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("ReduceFre")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReduceFre = FValue.trim();
            }
            else
                ReduceFre = null;
        }
        if (FCode.equalsIgnoreCase("OffAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                OffAcc = FValue.trim();
            }
            else
                OffAcc = null;
        }
        if (FCode.equalsIgnoreCase("OffPwd")) {
            if( FValue != null && !FValue.equals(""))
            {
                OffPwd = FValue.trim();
            }
            else
                OffPwd = null;
        }
        if (FCode.equalsIgnoreCase("SaleChannels")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChannels = FValue.trim();
            }
            else
                SaleChannels = null;
        }
        if (FCode.equalsIgnoreCase("E_AppntID")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_AppntID = FValue.trim();
            }
            else
                E_AppntID = null;
        }
        if (FCode.equalsIgnoreCase("E_AppntDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_AppntDate = FValue.trim();
            }
            else
                E_AppntDate = null;
        }
        if (FCode.equalsIgnoreCase("E_ContID")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_ContID = FValue.trim();
            }
            else
                E_ContID = null;
        }
        if (FCode.equalsIgnoreCase("E_ContDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_ContDate = FValue.trim();
            }
            else
                E_ContDate = null;
        }
        if (FCode.equalsIgnoreCase("IsRenewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsRenewFlag = FValue.trim();
            }
            else
                IsRenewFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LCGrpContPojo [" +
            "GrpContNo="+GrpContNo +
            ", ProposalGrpContNo="+ProposalGrpContNo +
            ", PrtNo="+PrtNo +
            ", SaleChnl="+SaleChnl +
            ", ManageCom="+ManageCom +
            ", Password="+Password +
            ", Password2="+Password2 +
            ", AppntNo="+AppntNo +
            ", Peoples2="+Peoples2 +
            ", GetFlag="+GetFlag +
            ", DisputedFlag="+DisputedFlag +
            ", GrpSpec="+GrpSpec +
            ", PayMode="+PayMode +
            ", SignCom="+SignCom +
            ", SignDate="+SignDate +
            ", SignTime="+SignTime +
            ", CValiDate="+CValiDate +
            ", PayIntv="+PayIntv +
            ", Peoples="+Peoples +
            ", Mult="+Mult +
            ", Prem="+Prem +
            ", Amnt="+Amnt +
            ", SumPrem="+SumPrem +
            ", SumPay="+SumPay +
            ", Dif="+Dif +
            ", Remark="+Remark +
            ", StandbyFlag1="+StandbyFlag1 +
            ", StandbyFlag2="+StandbyFlag2 +
            ", StandbyFlag3="+StandbyFlag3 +
            ", ApproveFlag="+ApproveFlag +
            ", ApproveCode="+ApproveCode +
            ", ApproveDate="+ApproveDate +
            ", ApproveTime="+ApproveTime +
            ", UWOperator="+UWOperator +
            ", UWFlag="+UWFlag +
            ", UWDate="+UWDate +
            ", UWTime="+UWTime +
            ", AppFlag="+AppFlag +
            ", PolApplyDate="+PolApplyDate +
            ", CustomGetPolDate="+CustomGetPolDate +
            ", GetPolDate="+GetPolDate +
            ", GetPolTime="+GetPolTime +
            ", State="+State +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", FirstTrialOperator="+FirstTrialOperator +
            ", FirstTrialDate="+FirstTrialDate +
            ", FirstTrialTime="+FirstTrialTime +
            ", ContType="+ContType +
            ", AnnuityReceiveDate="+AnnuityReceiveDate +
            ", CEndDate="+CEndDate +
            ", NativePeoples="+NativePeoples +
            ", NativePrem="+NativePrem +
            ", NativeAmnt="+NativeAmnt +
            ", ReduceFre="+ReduceFre +
            ", OffAcc="+OffAcc +
            ", OffPwd="+OffPwd +
            ", SaleChannels="+SaleChannels +
            ", E_AppntID="+E_AppntID +
            ", E_AppntDate="+E_AppntDate +
            ", E_ContID="+E_ContID +
            ", E_ContDate="+E_ContDate +
            ", IsRenewFlag="+IsRenewFlag +"]";
    }
}
