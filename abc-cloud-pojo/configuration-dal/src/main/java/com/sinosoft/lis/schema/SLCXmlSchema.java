/**
 * Copyright (c) 2002 sinosoft  Co. L
 */

package com.sinosoft.lis.schema;

import lombok.Data;

import java.util.Date;

/*
 * <p>ClassName: SLCXmlSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2012-03-01
 */
@Data
public class SLCXmlSchema {
	// @Field
	/** 保单号 */
	private String PolNo;
	/** 保单校验码 */
	private String PolPassword;
	/** 单证流水号 */
	private String PIPNo;
	/** 险种编码 */
	private String RiskCode;
	/** 计划编码 */
	private String ContPlanCode;
	/** 被保人姓名 */
	private String InsuredName;
	/** 被保人证件类型 */
	private String InsuredIDType;
	/** 被保人证件号 */
	private String InsuredIDNo;
	/** 被保人联系电话 */
	private String InsuredPhone;
	/** 被保人性别 */
	private String InsuredSex;
	/** 被保人出生日期 */
	private Date InsuredBirthday;
	/** 生效日期 */
	private Date CValiDate;
	/** 生效时点 */
	private String CValiTime;
	/** 合同终止日期 */
	private Date CInValiDate;
	/** 合同终止时间 */
	private String CInvaliTime;
	/** 投保日期 */
	private Date PolApplyDate;
	/** 是否预约生效 */
	private String IsBooking;
	/** 航班号 */
	private String FlightNo;
	/** 乘机日期 */
	private Date FlightDate;
	/** 是否往返 */
	private String IsReturn;
	/** 款式 */
	private String InterCode;
	/** 保单类型 */
	private String PolType;
	/** 保单状态 */
	private String PolState;
	/** 保险期间 */
	private int InsuYear;
	/** 保险期间单位 */
	private String InsuYearFlag;
	/** 份数 */
	private int Mult;
	/** 保费 */
	private double Prem;
	/** 保额 */
	private double Amnt;
	/** 返程航班号 */
	private String ReFlighrNo;
	/** 返程日期 */
	private Date ReturnDate;
	/** 目的地 */
	private String Destination;
	/** 出国事由 */
	private String EvectionReason;
	/** 受益人标识 */
	private String BnfFlag;
	/** 受益人姓名 */
	private String BnfName;
	/** 与被保人关系 */
	private String BnfRelation;
	/** 受益人证件类型 */
	private String BnfIDType;
	/** 受益人证件号码 */
	private String BnfIDNo;
	/** 受益人性别 */
	private String BnfSex;
	/** 受益人出生日期 */
	private Date BnfBirthday;
	/** 受益人联系地址 */
	private String BnfAddress;
	/** 受益人邮编 */
	private String BnfZipCode;
	/** 受益人联系电话 */
	private String BnfPhone;
	/** 操作员 */
	private String Operator;
	/** 保全操作员 */
	private String EdorOperator;
	/** 管理机构 */
	private String ManageCom;
	/** 代理机构 */
	private String AgentCom;
	/** 代理人 */
	private String AgentCode;
	/** 生成日期 */
	private Date MakeDate;
	/** 生成时间 */
	private String MakeTime;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 最后修改时间 */
	private String ModifyTime;
	/** 签单日期 */
	private Date SignDate;
	/** 离线标志 */
	private String OffLineFlag;
	/** 单证类型 */
	private String CertifyCode;
	/** 备注 */
	private String Remark;
	/** 与投保人关系 */
	private String IsAppPerson;
	/** 投保人姓名 */
	private String AppPersonName;
	/** 投保人英文名 */
	private String AppPersonEnName;
	/** 证件类型 */
	private String AppIDType;
	/** 证件号码 */
	private String AppIDNumber;
	/** 投保人性别 */
	private String AppSex;
	/** 出生日期 */
	private Date AppBirthday;
	/** 联系电话 */
	private String AppIphone;
	/** 被保人英文名 */
	private String InsuredEnName;
	/** 发票号码 */
	private String InvoiceNo;
	/** 团体名称 */
	private String GroupName;
	/** 被保险人职业类别 */
	private String OccupationType;
	/** 借款金额 */
	private double LoanMoney;
	/** 借款合同编码 */
	private String LoanCont;
	/** 借款凭证编码 */
	private String LoanVouch;
	/** 借款起期 */
	private Date LoanStartDate;
	/** 借款止期 */
	private Date LoanEndDate;
	/** 健康告知1 */
	private String HealthInformOne;
	/** 健康告知2 */
	private String HealthInformTwo;
	/** 贷款业务栏1 */
	private String LoanBusinessOne;
	/** 贷款业务栏2 */
	private String LoanBusinessTwo;
	/** 被保险人职业 */
	private String InsureOccupation;

}
