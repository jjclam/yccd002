/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LCInsuredImportSubPojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LCInsuredImportSubPojo implements  Pojo,Serializable {
    // @Field
    /** 序列号 */
    private String SerialNo; 
    /** 业务号类型 */
    private String OtherNoType; 
    /** 业务号 */
    private String OtherNo; 
    /** 投保人客户号 */
    private String AppntNo; 
    /** 校验标识 */
    private String CheckFlag; 
    /** 投保单号 */
    private String PrtNo; 
    /** 团体保单号 */
    private String GrpContNo; 
    /** 批次号 */
    private String BatchNo; 
    /** Id */
    private String ID; 
    /** 合同id */
    private String ContNo; 
    /** 客户号 */
    private String InsuredNo; 
    /** 主被保险人合同id */
    private String MainInsuredNo; 
    /** 状态 */
    private String CustomerState; 
    /** 主被保险人姓名 */
    private String MainInsuredName; 
    /** 主被保险人证件号 */
    private String MainInsuredIDNo; 
    /** 被保人姓名 */
    private String InsuredName; 
    /** 与主被保险人关系 */
    private String EmployeeRelation; 
    /** 性别 */
    private String InsuredSex; 
    /** 出生日期 */
    private String InsuredBirthday; 
    /** 证件类型 */
    private String InsuredIDType; 
    /** 证件号码 */
    private String InsuredIDNo; 
    /** 户籍 */
    private String NativePlace; 
    /** 服务机构 */
    private String RgtAddress; 
    /** 婚姻状况 */
    private String Marriage; 
    /** 民族 */
    private String Nationality; 
    /** 保险计划 */
    private String ContPlan; 
    /** 职业类别 */
    private String OccupationType; 
    /** 职业代码 */
    private String OccupationCode; 
    /** 兼业工种 */
    private String PluralityType; 
    /** 理赔金转帐银行 */
    private String BankCode1; 
    /** 户名 */
    private String AccName1; 
    /** 帐号 */
    private String BankAccNo1; 
    /** 入司日期 */
    private String JoinCompanyDate; 
    /** 工资 */
    private String Salary; 
    /** 保单类型标记 */
    private String PolTypeFlag; 
    /** 被保人人数 */
    private String InsuredPeoples; 
    /** 邮件 */
    private String EMail; 
    /** 移动电话 */
    private String Mobile; 
    /** 卡单编码 */
    private String CertifyCode; 
    /** 卡单起号 */
    private String StartCode; 
    /** 卡单止号 */
    private String EndCode; 
    /** 保存计划或是否有连带被保人 */
    private String CalStandbyFlag1; 
    /** 连带人数 */
    private String CalStandbyFlag2; 
    /** 连带被保人连带险种 */
    private String RelationToRisk; 
    /** 生效日期 */
    private String ContCValiDate; 
    /** 团险合同号 */
    private String GrpNo; 
    /** 与投保人关系 */
    private String RelationToAppnt; 
    /** 社保标记 */
    private String SocialInsuFlag; 
    /** 与员工关系 */
    private String RelationToMainInsured; 
    /** 健康服务标识 */
    private String HealthServiceName; 
    /** 是否医保身份 */
    private String HealthInsFlagName; 
    /** 保险组别号 */
    private String GrpAlias; 
    /** 是否续保 */
    private String WhetherToRnew; 
    /** 开户银行 */
    private String BankCode; 
    /** 开户行所在省 */
    private String BankProvince; 
    /** 开户行所在市 */
    private String BankCity; 
    /** 银行网点 */
    private String BankLocations; 
    /** 账户名 */
    private String AccName; 
    /** 账号 */
    private String BankAccNo; 
    /** 卡折类型 */
    private String AccType; 
    /** 被保人工号 */
    private String BeInsuredNo; 
    /** 身份证有效期 */
    private String IDExpDate; 
    /** 是否中国税收居民 */
    private String TaxpayerIDType; 
    /** 所属公司编号 */
    private String CompanyNo; 
    /** 所属公司名称 */
    private String CompanyName; 
    /** 导入标志 */
    private String Flag; 
    /** 错误信息 */
    private String ErrorInfo; 
    /** 操作人 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 修改日期 */
    private String  ModifyDate;
    /** 修改时间 */
    private String ModIfyTime; 
    /** 电子个人凭证地址 */
    private String E_PolicyID; 


    public static final int FIELDNUM = 73;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getCheckFlag() {
        return CheckFlag;
    }
    public void setCheckFlag(String aCheckFlag) {
        CheckFlag = aCheckFlag;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getBatchNo() {
        return BatchNo;
    }
    public void setBatchNo(String aBatchNo) {
        BatchNo = aBatchNo;
    }
    public String getID() {
        return ID;
    }
    public void setID(String aID) {
        ID = aID;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getMainInsuredNo() {
        return MainInsuredNo;
    }
    public void setMainInsuredNo(String aMainInsuredNo) {
        MainInsuredNo = aMainInsuredNo;
    }
    public String getCustomerState() {
        return CustomerState;
    }
    public void setCustomerState(String aCustomerState) {
        CustomerState = aCustomerState;
    }
    public String getMainInsuredName() {
        return MainInsuredName;
    }
    public void setMainInsuredName(String aMainInsuredName) {
        MainInsuredName = aMainInsuredName;
    }
    public String getMainInsuredIDNo() {
        return MainInsuredIDNo;
    }
    public void setMainInsuredIDNo(String aMainInsuredIDNo) {
        MainInsuredIDNo = aMainInsuredIDNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getEmployeeRelation() {
        return EmployeeRelation;
    }
    public void setEmployeeRelation(String aEmployeeRelation) {
        EmployeeRelation = aEmployeeRelation;
    }
    public String getInsuredSex() {
        return InsuredSex;
    }
    public void setInsuredSex(String aInsuredSex) {
        InsuredSex = aInsuredSex;
    }
    public String getInsuredBirthday() {
        return InsuredBirthday;
    }
    public void setInsuredBirthday(String aInsuredBirthday) {
        InsuredBirthday = aInsuredBirthday;
    }
    public String getInsuredIDType() {
        return InsuredIDType;
    }
    public void setInsuredIDType(String aInsuredIDType) {
        InsuredIDType = aInsuredIDType;
    }
    public String getInsuredIDNo() {
        return InsuredIDNo;
    }
    public void setInsuredIDNo(String aInsuredIDNo) {
        InsuredIDNo = aInsuredIDNo;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getContPlan() {
        return ContPlan;
    }
    public void setContPlan(String aContPlan) {
        ContPlan = aContPlan;
    }
    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getPluralityType() {
        return PluralityType;
    }
    public void setPluralityType(String aPluralityType) {
        PluralityType = aPluralityType;
    }
    public String getBankCode1() {
        return BankCode1;
    }
    public void setBankCode1(String aBankCode1) {
        BankCode1 = aBankCode1;
    }
    public String getAccName1() {
        return AccName1;
    }
    public void setAccName1(String aAccName1) {
        AccName1 = aAccName1;
    }
    public String getBankAccNo1() {
        return BankAccNo1;
    }
    public void setBankAccNo1(String aBankAccNo1) {
        BankAccNo1 = aBankAccNo1;
    }
    public String getJoinCompanyDate() {
        return JoinCompanyDate;
    }
    public void setJoinCompanyDate(String aJoinCompanyDate) {
        JoinCompanyDate = aJoinCompanyDate;
    }
    public String getSalary() {
        return Salary;
    }
    public void setSalary(String aSalary) {
        Salary = aSalary;
    }
    public String getPolTypeFlag() {
        return PolTypeFlag;
    }
    public void setPolTypeFlag(String aPolTypeFlag) {
        PolTypeFlag = aPolTypeFlag;
    }
    public String getInsuredPeoples() {
        return InsuredPeoples;
    }
    public void setInsuredPeoples(String aInsuredPeoples) {
        InsuredPeoples = aInsuredPeoples;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getCertifyCode() {
        return CertifyCode;
    }
    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }
    public String getStartCode() {
        return StartCode;
    }
    public void setStartCode(String aStartCode) {
        StartCode = aStartCode;
    }
    public String getEndCode() {
        return EndCode;
    }
    public void setEndCode(String aEndCode) {
        EndCode = aEndCode;
    }
    public String getCalStandbyFlag1() {
        return CalStandbyFlag1;
    }
    public void setCalStandbyFlag1(String aCalStandbyFlag1) {
        CalStandbyFlag1 = aCalStandbyFlag1;
    }
    public String getCalStandbyFlag2() {
        return CalStandbyFlag2;
    }
    public void setCalStandbyFlag2(String aCalStandbyFlag2) {
        CalStandbyFlag2 = aCalStandbyFlag2;
    }
    public String getRelationToRisk() {
        return RelationToRisk;
    }
    public void setRelationToRisk(String aRelationToRisk) {
        RelationToRisk = aRelationToRisk;
    }
    public String getContCValiDate() {
        return ContCValiDate;
    }
    public void setContCValiDate(String aContCValiDate) {
        ContCValiDate = aContCValiDate;
    }
    public String getGrpNo() {
        return GrpNo;
    }
    public void setGrpNo(String aGrpNo) {
        GrpNo = aGrpNo;
    }
    public String getRelationToAppnt() {
        return RelationToAppnt;
    }
    public void setRelationToAppnt(String aRelationToAppnt) {
        RelationToAppnt = aRelationToAppnt;
    }
    public String getSocialInsuFlag() {
        return SocialInsuFlag;
    }
    public void setSocialInsuFlag(String aSocialInsuFlag) {
        SocialInsuFlag = aSocialInsuFlag;
    }
    public String getRelationToMainInsured() {
        return RelationToMainInsured;
    }
    public void setRelationToMainInsured(String aRelationToMainInsured) {
        RelationToMainInsured = aRelationToMainInsured;
    }
    public String getHealthServiceName() {
        return HealthServiceName;
    }
    public void setHealthServiceName(String aHealthServiceName) {
        HealthServiceName = aHealthServiceName;
    }
    public String getHealthInsFlagName() {
        return HealthInsFlagName;
    }
    public void setHealthInsFlagName(String aHealthInsFlagName) {
        HealthInsFlagName = aHealthInsFlagName;
    }
    public String getGrpAlias() {
        return GrpAlias;
    }
    public void setGrpAlias(String aGrpAlias) {
        GrpAlias = aGrpAlias;
    }
    public String getWhetherToRnew() {
        return WhetherToRnew;
    }
    public void setWhetherToRnew(String aWhetherToRnew) {
        WhetherToRnew = aWhetherToRnew;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankProvince() {
        return BankProvince;
    }
    public void setBankProvince(String aBankProvince) {
        BankProvince = aBankProvince;
    }
    public String getBankCity() {
        return BankCity;
    }
    public void setBankCity(String aBankCity) {
        BankCity = aBankCity;
    }
    public String getBankLocations() {
        return BankLocations;
    }
    public void setBankLocations(String aBankLocations) {
        BankLocations = aBankLocations;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getBeInsuredNo() {
        return BeInsuredNo;
    }
    public void setBeInsuredNo(String aBeInsuredNo) {
        BeInsuredNo = aBeInsuredNo;
    }
    public String getIDExpDate() {
        return IDExpDate;
    }
    public void setIDExpDate(String aIDExpDate) {
        IDExpDate = aIDExpDate;
    }
    public String getTaxpayerIDType() {
        return TaxpayerIDType;
    }
    public void setTaxpayerIDType(String aTaxpayerIDType) {
        TaxpayerIDType = aTaxpayerIDType;
    }
    public String getCompanyNo() {
        return CompanyNo;
    }
    public void setCompanyNo(String aCompanyNo) {
        CompanyNo = aCompanyNo;
    }
    public String getCompanyName() {
        return CompanyName;
    }
    public void setCompanyName(String aCompanyName) {
        CompanyName = aCompanyName;
    }
    public String getFlag() {
        return Flag;
    }
    public void setFlag(String aFlag) {
        Flag = aFlag;
    }
    public String getErrorInfo() {
        return ErrorInfo;
    }
    public void setErrorInfo(String aErrorInfo) {
        ErrorInfo = aErrorInfo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModIfyTime() {
        return ModIfyTime;
    }
    public void setModIfyTime(String aModIfyTime) {
        ModIfyTime = aModIfyTime;
    }
    public String getE_PolicyID() {
        return E_PolicyID;
    }
    public void setE_PolicyID(String aE_PolicyID) {
        E_PolicyID = aE_PolicyID;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 1;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 2;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 3;
        }
        if( strFieldName.equals("CheckFlag") ) {
            return 4;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 5;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 6;
        }
        if( strFieldName.equals("BatchNo") ) {
            return 7;
        }
        if( strFieldName.equals("ID") ) {
            return 8;
        }
        if( strFieldName.equals("ContNo") ) {
            return 9;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 10;
        }
        if( strFieldName.equals("MainInsuredNo") ) {
            return 11;
        }
        if( strFieldName.equals("CustomerState") ) {
            return 12;
        }
        if( strFieldName.equals("MainInsuredName") ) {
            return 13;
        }
        if( strFieldName.equals("MainInsuredIDNo") ) {
            return 14;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 15;
        }
        if( strFieldName.equals("EmployeeRelation") ) {
            return 16;
        }
        if( strFieldName.equals("InsuredSex") ) {
            return 17;
        }
        if( strFieldName.equals("InsuredBirthday") ) {
            return 18;
        }
        if( strFieldName.equals("InsuredIDType") ) {
            return 19;
        }
        if( strFieldName.equals("InsuredIDNo") ) {
            return 20;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 21;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 22;
        }
        if( strFieldName.equals("Marriage") ) {
            return 23;
        }
        if( strFieldName.equals("Nationality") ) {
            return 24;
        }
        if( strFieldName.equals("ContPlan") ) {
            return 25;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 26;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 27;
        }
        if( strFieldName.equals("PluralityType") ) {
            return 28;
        }
        if( strFieldName.equals("BankCode1") ) {
            return 29;
        }
        if( strFieldName.equals("AccName1") ) {
            return 30;
        }
        if( strFieldName.equals("BankAccNo1") ) {
            return 31;
        }
        if( strFieldName.equals("JoinCompanyDate") ) {
            return 32;
        }
        if( strFieldName.equals("Salary") ) {
            return 33;
        }
        if( strFieldName.equals("PolTypeFlag") ) {
            return 34;
        }
        if( strFieldName.equals("InsuredPeoples") ) {
            return 35;
        }
        if( strFieldName.equals("EMail") ) {
            return 36;
        }
        if( strFieldName.equals("Mobile") ) {
            return 37;
        }
        if( strFieldName.equals("CertifyCode") ) {
            return 38;
        }
        if( strFieldName.equals("StartCode") ) {
            return 39;
        }
        if( strFieldName.equals("EndCode") ) {
            return 40;
        }
        if( strFieldName.equals("CalStandbyFlag1") ) {
            return 41;
        }
        if( strFieldName.equals("CalStandbyFlag2") ) {
            return 42;
        }
        if( strFieldName.equals("RelationToRisk") ) {
            return 43;
        }
        if( strFieldName.equals("ContCValiDate") ) {
            return 44;
        }
        if( strFieldName.equals("GrpNo") ) {
            return 45;
        }
        if( strFieldName.equals("RelationToAppnt") ) {
            return 46;
        }
        if( strFieldName.equals("SocialInsuFlag") ) {
            return 47;
        }
        if( strFieldName.equals("RelationToMainInsured") ) {
            return 48;
        }
        if( strFieldName.equals("HealthServiceName") ) {
            return 49;
        }
        if( strFieldName.equals("HealthInsFlagName") ) {
            return 50;
        }
        if( strFieldName.equals("GrpAlias") ) {
            return 51;
        }
        if( strFieldName.equals("WhetherToRnew") ) {
            return 52;
        }
        if( strFieldName.equals("BankCode") ) {
            return 53;
        }
        if( strFieldName.equals("BankProvince") ) {
            return 54;
        }
        if( strFieldName.equals("BankCity") ) {
            return 55;
        }
        if( strFieldName.equals("BankLocations") ) {
            return 56;
        }
        if( strFieldName.equals("AccName") ) {
            return 57;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 58;
        }
        if( strFieldName.equals("AccType") ) {
            return 59;
        }
        if( strFieldName.equals("BeInsuredNo") ) {
            return 60;
        }
        if( strFieldName.equals("IDExpDate") ) {
            return 61;
        }
        if( strFieldName.equals("TaxpayerIDType") ) {
            return 62;
        }
        if( strFieldName.equals("CompanyNo") ) {
            return 63;
        }
        if( strFieldName.equals("CompanyName") ) {
            return 64;
        }
        if( strFieldName.equals("Flag") ) {
            return 65;
        }
        if( strFieldName.equals("ErrorInfo") ) {
            return 66;
        }
        if( strFieldName.equals("Operator") ) {
            return 67;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 68;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 69;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 70;
        }
        if( strFieldName.equals("ModIfyTime") ) {
            return 71;
        }
        if( strFieldName.equals("E_PolicyID") ) {
            return 72;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "OtherNoType";
                break;
            case 2:
                strFieldName = "OtherNo";
                break;
            case 3:
                strFieldName = "AppntNo";
                break;
            case 4:
                strFieldName = "CheckFlag";
                break;
            case 5:
                strFieldName = "PrtNo";
                break;
            case 6:
                strFieldName = "GrpContNo";
                break;
            case 7:
                strFieldName = "BatchNo";
                break;
            case 8:
                strFieldName = "ID";
                break;
            case 9:
                strFieldName = "ContNo";
                break;
            case 10:
                strFieldName = "InsuredNo";
                break;
            case 11:
                strFieldName = "MainInsuredNo";
                break;
            case 12:
                strFieldName = "CustomerState";
                break;
            case 13:
                strFieldName = "MainInsuredName";
                break;
            case 14:
                strFieldName = "MainInsuredIDNo";
                break;
            case 15:
                strFieldName = "InsuredName";
                break;
            case 16:
                strFieldName = "EmployeeRelation";
                break;
            case 17:
                strFieldName = "InsuredSex";
                break;
            case 18:
                strFieldName = "InsuredBirthday";
                break;
            case 19:
                strFieldName = "InsuredIDType";
                break;
            case 20:
                strFieldName = "InsuredIDNo";
                break;
            case 21:
                strFieldName = "NativePlace";
                break;
            case 22:
                strFieldName = "RgtAddress";
                break;
            case 23:
                strFieldName = "Marriage";
                break;
            case 24:
                strFieldName = "Nationality";
                break;
            case 25:
                strFieldName = "ContPlan";
                break;
            case 26:
                strFieldName = "OccupationType";
                break;
            case 27:
                strFieldName = "OccupationCode";
                break;
            case 28:
                strFieldName = "PluralityType";
                break;
            case 29:
                strFieldName = "BankCode1";
                break;
            case 30:
                strFieldName = "AccName1";
                break;
            case 31:
                strFieldName = "BankAccNo1";
                break;
            case 32:
                strFieldName = "JoinCompanyDate";
                break;
            case 33:
                strFieldName = "Salary";
                break;
            case 34:
                strFieldName = "PolTypeFlag";
                break;
            case 35:
                strFieldName = "InsuredPeoples";
                break;
            case 36:
                strFieldName = "EMail";
                break;
            case 37:
                strFieldName = "Mobile";
                break;
            case 38:
                strFieldName = "CertifyCode";
                break;
            case 39:
                strFieldName = "StartCode";
                break;
            case 40:
                strFieldName = "EndCode";
                break;
            case 41:
                strFieldName = "CalStandbyFlag1";
                break;
            case 42:
                strFieldName = "CalStandbyFlag2";
                break;
            case 43:
                strFieldName = "RelationToRisk";
                break;
            case 44:
                strFieldName = "ContCValiDate";
                break;
            case 45:
                strFieldName = "GrpNo";
                break;
            case 46:
                strFieldName = "RelationToAppnt";
                break;
            case 47:
                strFieldName = "SocialInsuFlag";
                break;
            case 48:
                strFieldName = "RelationToMainInsured";
                break;
            case 49:
                strFieldName = "HealthServiceName";
                break;
            case 50:
                strFieldName = "HealthInsFlagName";
                break;
            case 51:
                strFieldName = "GrpAlias";
                break;
            case 52:
                strFieldName = "WhetherToRnew";
                break;
            case 53:
                strFieldName = "BankCode";
                break;
            case 54:
                strFieldName = "BankProvince";
                break;
            case 55:
                strFieldName = "BankCity";
                break;
            case 56:
                strFieldName = "BankLocations";
                break;
            case 57:
                strFieldName = "AccName";
                break;
            case 58:
                strFieldName = "BankAccNo";
                break;
            case 59:
                strFieldName = "AccType";
                break;
            case 60:
                strFieldName = "BeInsuredNo";
                break;
            case 61:
                strFieldName = "IDExpDate";
                break;
            case 62:
                strFieldName = "TaxpayerIDType";
                break;
            case 63:
                strFieldName = "CompanyNo";
                break;
            case 64:
                strFieldName = "CompanyName";
                break;
            case 65:
                strFieldName = "Flag";
                break;
            case 66:
                strFieldName = "ErrorInfo";
                break;
            case 67:
                strFieldName = "Operator";
                break;
            case 68:
                strFieldName = "MakeDate";
                break;
            case 69:
                strFieldName = "MakeTime";
                break;
            case 70:
                strFieldName = "ModifyDate";
                break;
            case 71:
                strFieldName = "ModIfyTime";
                break;
            case 72:
                strFieldName = "E_PolicyID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "CHECKFLAG":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "BATCHNO":
                return Schema.TYPE_STRING;
            case "ID":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "MAININSUREDNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERSTATE":
                return Schema.TYPE_STRING;
            case "MAININSUREDNAME":
                return Schema.TYPE_STRING;
            case "MAININSUREDIDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "EMPLOYEERELATION":
                return Schema.TYPE_STRING;
            case "INSUREDSEX":
                return Schema.TYPE_STRING;
            case "INSUREDBIRTHDAY":
                return Schema.TYPE_STRING;
            case "INSUREDIDTYPE":
                return Schema.TYPE_STRING;
            case "INSUREDIDNO":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "CONTPLAN":
                return Schema.TYPE_STRING;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "PLURALITYTYPE":
                return Schema.TYPE_STRING;
            case "BANKCODE1":
                return Schema.TYPE_STRING;
            case "ACCNAME1":
                return Schema.TYPE_STRING;
            case "BANKACCNO1":
                return Schema.TYPE_STRING;
            case "JOINCOMPANYDATE":
                return Schema.TYPE_STRING;
            case "SALARY":
                return Schema.TYPE_STRING;
            case "POLTYPEFLAG":
                return Schema.TYPE_STRING;
            case "INSUREDPEOPLES":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "CERTIFYCODE":
                return Schema.TYPE_STRING;
            case "STARTCODE":
                return Schema.TYPE_STRING;
            case "ENDCODE":
                return Schema.TYPE_STRING;
            case "CALSTANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "CALSTANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "RELATIONTORISK":
                return Schema.TYPE_STRING;
            case "CONTCVALIDATE":
                return Schema.TYPE_STRING;
            case "GRPNO":
                return Schema.TYPE_STRING;
            case "RELATIONTOAPPNT":
                return Schema.TYPE_STRING;
            case "SOCIALINSUFLAG":
                return Schema.TYPE_STRING;
            case "RELATIONTOMAININSURED":
                return Schema.TYPE_STRING;
            case "HEALTHSERVICENAME":
                return Schema.TYPE_STRING;
            case "HEALTHINSFLAGNAME":
                return Schema.TYPE_STRING;
            case "GRPALIAS":
                return Schema.TYPE_STRING;
            case "WHETHERTORNEW":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKPROVINCE":
                return Schema.TYPE_STRING;
            case "BANKCITY":
                return Schema.TYPE_STRING;
            case "BANKLOCATIONS":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "BEINSUREDNO":
                return Schema.TYPE_STRING;
            case "IDEXPDATE":
                return Schema.TYPE_STRING;
            case "TAXPAYERIDTYPE":
                return Schema.TYPE_STRING;
            case "COMPANYNO":
                return Schema.TYPE_STRING;
            case "COMPANYNAME":
                return Schema.TYPE_STRING;
            case "FLAG":
                return Schema.TYPE_STRING;
            case "ERRORINFO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "E_POLICYID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("CheckFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
        }
        if (FCode.equalsIgnoreCase("ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ID));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("MainInsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainInsuredNo));
        }
        if (FCode.equalsIgnoreCase("CustomerState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerState));
        }
        if (FCode.equalsIgnoreCase("MainInsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainInsuredName));
        }
        if (FCode.equalsIgnoreCase("MainInsuredIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainInsuredIDNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("EmployeeRelation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeRelation));
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBirthday));
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNo));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("ContPlan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlan));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
        }
        if (FCode.equalsIgnoreCase("BankCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode1));
        }
        if (FCode.equalsIgnoreCase("AccName1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName1));
        }
        if (FCode.equalsIgnoreCase("BankAccNo1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo1));
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JoinCompanyDate));
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
        }
        if (FCode.equalsIgnoreCase("PolTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolTypeFlag));
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredPeoples));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equalsIgnoreCase("StartCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartCode));
        }
        if (FCode.equalsIgnoreCase("EndCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndCode));
        }
        if (FCode.equalsIgnoreCase("CalStandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalStandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("CalStandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalStandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("RelationToRisk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToRisk));
        }
        if (FCode.equalsIgnoreCase("ContCValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContCValiDate));
        }
        if (FCode.equalsIgnoreCase("GrpNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
        }
        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
        }
        if (FCode.equalsIgnoreCase("SocialInsuFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuFlag));
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToMainInsured));
        }
        if (FCode.equalsIgnoreCase("HealthServiceName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthServiceName));
        }
        if (FCode.equalsIgnoreCase("HealthInsFlagName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthInsFlagName));
        }
        if (FCode.equalsIgnoreCase("GrpAlias")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAlias));
        }
        if (FCode.equalsIgnoreCase("WhetherToRnew")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WhetherToRnew));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankProvince")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankProvince));
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCity));
        }
        if (FCode.equalsIgnoreCase("BankLocations")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankLocations));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("BeInsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BeInsuredNo));
        }
        if (FCode.equalsIgnoreCase("IDExpDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDExpDate));
        }
        if (FCode.equalsIgnoreCase("TaxpayerIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxpayerIDType));
        }
        if (FCode.equalsIgnoreCase("CompanyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyNo));
        }
        if (FCode.equalsIgnoreCase("CompanyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyName));
        }
        if (FCode.equalsIgnoreCase("Flag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
        }
        if (FCode.equalsIgnoreCase("ErrorInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorInfo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModIfyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModIfyTime));
        }
        if (FCode.equalsIgnoreCase("E_PolicyID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_PolicyID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(OtherNoType);
                break;
            case 2:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 3:
                strFieldValue = String.valueOf(AppntNo);
                break;
            case 4:
                strFieldValue = String.valueOf(CheckFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(PrtNo);
                break;
            case 6:
                strFieldValue = String.valueOf(GrpContNo);
                break;
            case 7:
                strFieldValue = String.valueOf(BatchNo);
                break;
            case 8:
                strFieldValue = String.valueOf(ID);
                break;
            case 9:
                strFieldValue = String.valueOf(ContNo);
                break;
            case 10:
                strFieldValue = String.valueOf(InsuredNo);
                break;
            case 11:
                strFieldValue = String.valueOf(MainInsuredNo);
                break;
            case 12:
                strFieldValue = String.valueOf(CustomerState);
                break;
            case 13:
                strFieldValue = String.valueOf(MainInsuredName);
                break;
            case 14:
                strFieldValue = String.valueOf(MainInsuredIDNo);
                break;
            case 15:
                strFieldValue = String.valueOf(InsuredName);
                break;
            case 16:
                strFieldValue = String.valueOf(EmployeeRelation);
                break;
            case 17:
                strFieldValue = String.valueOf(InsuredSex);
                break;
            case 18:
                strFieldValue = String.valueOf(InsuredBirthday);
                break;
            case 19:
                strFieldValue = String.valueOf(InsuredIDType);
                break;
            case 20:
                strFieldValue = String.valueOf(InsuredIDNo);
                break;
            case 21:
                strFieldValue = String.valueOf(NativePlace);
                break;
            case 22:
                strFieldValue = String.valueOf(RgtAddress);
                break;
            case 23:
                strFieldValue = String.valueOf(Marriage);
                break;
            case 24:
                strFieldValue = String.valueOf(Nationality);
                break;
            case 25:
                strFieldValue = String.valueOf(ContPlan);
                break;
            case 26:
                strFieldValue = String.valueOf(OccupationType);
                break;
            case 27:
                strFieldValue = String.valueOf(OccupationCode);
                break;
            case 28:
                strFieldValue = String.valueOf(PluralityType);
                break;
            case 29:
                strFieldValue = String.valueOf(BankCode1);
                break;
            case 30:
                strFieldValue = String.valueOf(AccName1);
                break;
            case 31:
                strFieldValue = String.valueOf(BankAccNo1);
                break;
            case 32:
                strFieldValue = String.valueOf(JoinCompanyDate);
                break;
            case 33:
                strFieldValue = String.valueOf(Salary);
                break;
            case 34:
                strFieldValue = String.valueOf(PolTypeFlag);
                break;
            case 35:
                strFieldValue = String.valueOf(InsuredPeoples);
                break;
            case 36:
                strFieldValue = String.valueOf(EMail);
                break;
            case 37:
                strFieldValue = String.valueOf(Mobile);
                break;
            case 38:
                strFieldValue = String.valueOf(CertifyCode);
                break;
            case 39:
                strFieldValue = String.valueOf(StartCode);
                break;
            case 40:
                strFieldValue = String.valueOf(EndCode);
                break;
            case 41:
                strFieldValue = String.valueOf(CalStandbyFlag1);
                break;
            case 42:
                strFieldValue = String.valueOf(CalStandbyFlag2);
                break;
            case 43:
                strFieldValue = String.valueOf(RelationToRisk);
                break;
            case 44:
                strFieldValue = String.valueOf(ContCValiDate);
                break;
            case 45:
                strFieldValue = String.valueOf(GrpNo);
                break;
            case 46:
                strFieldValue = String.valueOf(RelationToAppnt);
                break;
            case 47:
                strFieldValue = String.valueOf(SocialInsuFlag);
                break;
            case 48:
                strFieldValue = String.valueOf(RelationToMainInsured);
                break;
            case 49:
                strFieldValue = String.valueOf(HealthServiceName);
                break;
            case 50:
                strFieldValue = String.valueOf(HealthInsFlagName);
                break;
            case 51:
                strFieldValue = String.valueOf(GrpAlias);
                break;
            case 52:
                strFieldValue = String.valueOf(WhetherToRnew);
                break;
            case 53:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 54:
                strFieldValue = String.valueOf(BankProvince);
                break;
            case 55:
                strFieldValue = String.valueOf(BankCity);
                break;
            case 56:
                strFieldValue = String.valueOf(BankLocations);
                break;
            case 57:
                strFieldValue = String.valueOf(AccName);
                break;
            case 58:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 59:
                strFieldValue = String.valueOf(AccType);
                break;
            case 60:
                strFieldValue = String.valueOf(BeInsuredNo);
                break;
            case 61:
                strFieldValue = String.valueOf(IDExpDate);
                break;
            case 62:
                strFieldValue = String.valueOf(TaxpayerIDType);
                break;
            case 63:
                strFieldValue = String.valueOf(CompanyNo);
                break;
            case 64:
                strFieldValue = String.valueOf(CompanyName);
                break;
            case 65:
                strFieldValue = String.valueOf(Flag);
                break;
            case 66:
                strFieldValue = String.valueOf(ErrorInfo);
                break;
            case 67:
                strFieldValue = String.valueOf(Operator);
                break;
            case 68:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 69:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 70:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 71:
                strFieldValue = String.valueOf(ModIfyTime);
                break;
            case 72:
                strFieldValue = String.valueOf(E_PolicyID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("CheckFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CheckFlag = FValue.trim();
            }
            else
                CheckFlag = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BatchNo = FValue.trim();
            }
            else
                BatchNo = null;
        }
        if (FCode.equalsIgnoreCase("ID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ID = FValue.trim();
            }
            else
                ID = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("MainInsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainInsuredNo = FValue.trim();
            }
            else
                MainInsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerState")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerState = FValue.trim();
            }
            else
                CustomerState = null;
        }
        if (FCode.equalsIgnoreCase("MainInsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainInsuredName = FValue.trim();
            }
            else
                MainInsuredName = null;
        }
        if (FCode.equalsIgnoreCase("MainInsuredIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainInsuredIDNo = FValue.trim();
            }
            else
                MainInsuredIDNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("EmployeeRelation")) {
            if( FValue != null && !FValue.equals(""))
            {
                EmployeeRelation = FValue.trim();
            }
            else
                EmployeeRelation = null;
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredSex = FValue.trim();
            }
            else
                InsuredSex = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBirthday = FValue.trim();
            }
            else
                InsuredBirthday = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDType = FValue.trim();
            }
            else
                InsuredIDType = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDNo = FValue.trim();
            }
            else
                InsuredIDNo = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("ContPlan")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlan = FValue.trim();
            }
            else
                ContPlan = null;
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
                PluralityType = null;
        }
        if (FCode.equalsIgnoreCase("BankCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode1 = FValue.trim();
            }
            else
                BankCode1 = null;
        }
        if (FCode.equalsIgnoreCase("AccName1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName1 = FValue.trim();
            }
            else
                AccName1 = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo1")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo1 = FValue.trim();
            }
            else
                BankAccNo1 = null;
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JoinCompanyDate = FValue.trim();
            }
            else
                JoinCompanyDate = null;
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            if( FValue != null && !FValue.equals(""))
            {
                Salary = FValue.trim();
            }
            else
                Salary = null;
        }
        if (FCode.equalsIgnoreCase("PolTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolTypeFlag = FValue.trim();
            }
            else
                PolTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredPeoples = FValue.trim();
            }
            else
                InsuredPeoples = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CertifyCode = FValue.trim();
            }
            else
                CertifyCode = null;
        }
        if (FCode.equalsIgnoreCase("StartCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartCode = FValue.trim();
            }
            else
                StartCode = null;
        }
        if (FCode.equalsIgnoreCase("EndCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndCode = FValue.trim();
            }
            else
                EndCode = null;
        }
        if (FCode.equalsIgnoreCase("CalStandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalStandbyFlag1 = FValue.trim();
            }
            else
                CalStandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("CalStandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalStandbyFlag2 = FValue.trim();
            }
            else
                CalStandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("RelationToRisk")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToRisk = FValue.trim();
            }
            else
                RelationToRisk = null;
        }
        if (FCode.equalsIgnoreCase("ContCValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContCValiDate = FValue.trim();
            }
            else
                ContCValiDate = null;
        }
        if (FCode.equalsIgnoreCase("GrpNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNo = FValue.trim();
            }
            else
                GrpNo = null;
        }
        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToAppnt = FValue.trim();
            }
            else
                RelationToAppnt = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuFlag = FValue.trim();
            }
            else
                SocialInsuFlag = null;
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToMainInsured = FValue.trim();
            }
            else
                RelationToMainInsured = null;
        }
        if (FCode.equalsIgnoreCase("HealthServiceName")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthServiceName = FValue.trim();
            }
            else
                HealthServiceName = null;
        }
        if (FCode.equalsIgnoreCase("HealthInsFlagName")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthInsFlagName = FValue.trim();
            }
            else
                HealthInsFlagName = null;
        }
        if (FCode.equalsIgnoreCase("GrpAlias")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAlias = FValue.trim();
            }
            else
                GrpAlias = null;
        }
        if (FCode.equalsIgnoreCase("WhetherToRnew")) {
            if( FValue != null && !FValue.equals(""))
            {
                WhetherToRnew = FValue.trim();
            }
            else
                WhetherToRnew = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankProvince")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankProvince = FValue.trim();
            }
            else
                BankProvince = null;
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCity = FValue.trim();
            }
            else
                BankCity = null;
        }
        if (FCode.equalsIgnoreCase("BankLocations")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankLocations = FValue.trim();
            }
            else
                BankLocations = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("BeInsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BeInsuredNo = FValue.trim();
            }
            else
                BeInsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("IDExpDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDExpDate = FValue.trim();
            }
            else
                IDExpDate = null;
        }
        if (FCode.equalsIgnoreCase("TaxpayerIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxpayerIDType = FValue.trim();
            }
            else
                TaxpayerIDType = null;
        }
        if (FCode.equalsIgnoreCase("CompanyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyNo = FValue.trim();
            }
            else
                CompanyNo = null;
        }
        if (FCode.equalsIgnoreCase("CompanyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyName = FValue.trim();
            }
            else
                CompanyName = null;
        }
        if (FCode.equalsIgnoreCase("Flag")) {
            if( FValue != null && !FValue.equals(""))
            {
                Flag = FValue.trim();
            }
            else
                Flag = null;
        }
        if (FCode.equalsIgnoreCase("ErrorInfo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ErrorInfo = FValue.trim();
            }
            else
                ErrorInfo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModIfyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModIfyTime = FValue.trim();
            }
            else
                ModIfyTime = null;
        }
        if (FCode.equalsIgnoreCase("E_PolicyID")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_PolicyID = FValue.trim();
            }
            else
                E_PolicyID = null;
        }
        return true;
    }


    public String toString() {
    return "LCInsuredImportSubPojo [" +
            "SerialNo="+SerialNo +
            ", OtherNoType="+OtherNoType +
            ", OtherNo="+OtherNo +
            ", AppntNo="+AppntNo +
            ", CheckFlag="+CheckFlag +
            ", PrtNo="+PrtNo +
            ", GrpContNo="+GrpContNo +
            ", BatchNo="+BatchNo +
            ", ID="+ID +
            ", ContNo="+ContNo +
            ", InsuredNo="+InsuredNo +
            ", MainInsuredNo="+MainInsuredNo +
            ", CustomerState="+CustomerState +
            ", MainInsuredName="+MainInsuredName +
            ", MainInsuredIDNo="+MainInsuredIDNo +
            ", InsuredName="+InsuredName +
            ", EmployeeRelation="+EmployeeRelation +
            ", InsuredSex="+InsuredSex +
            ", InsuredBirthday="+InsuredBirthday +
            ", InsuredIDType="+InsuredIDType +
            ", InsuredIDNo="+InsuredIDNo +
            ", NativePlace="+NativePlace +
            ", RgtAddress="+RgtAddress +
            ", Marriage="+Marriage +
            ", Nationality="+Nationality +
            ", ContPlan="+ContPlan +
            ", OccupationType="+OccupationType +
            ", OccupationCode="+OccupationCode +
            ", PluralityType="+PluralityType +
            ", BankCode1="+BankCode1 +
            ", AccName1="+AccName1 +
            ", BankAccNo1="+BankAccNo1 +
            ", JoinCompanyDate="+JoinCompanyDate +
            ", Salary="+Salary +
            ", PolTypeFlag="+PolTypeFlag +
            ", InsuredPeoples="+InsuredPeoples +
            ", EMail="+EMail +
            ", Mobile="+Mobile +
            ", CertifyCode="+CertifyCode +
            ", StartCode="+StartCode +
            ", EndCode="+EndCode +
            ", CalStandbyFlag1="+CalStandbyFlag1 +
            ", CalStandbyFlag2="+CalStandbyFlag2 +
            ", RelationToRisk="+RelationToRisk +
            ", ContCValiDate="+ContCValiDate +
            ", GrpNo="+GrpNo +
            ", RelationToAppnt="+RelationToAppnt +
            ", SocialInsuFlag="+SocialInsuFlag +
            ", RelationToMainInsured="+RelationToMainInsured +
            ", HealthServiceName="+HealthServiceName +
            ", HealthInsFlagName="+HealthInsFlagName +
            ", GrpAlias="+GrpAlias +
            ", WhetherToRnew="+WhetherToRnew +
            ", BankCode="+BankCode +
            ", BankProvince="+BankProvince +
            ", BankCity="+BankCity +
            ", BankLocations="+BankLocations +
            ", AccName="+AccName +
            ", BankAccNo="+BankAccNo +
            ", AccType="+AccType +
            ", BeInsuredNo="+BeInsuredNo +
            ", IDExpDate="+IDExpDate +
            ", TaxpayerIDType="+TaxpayerIDType +
            ", CompanyNo="+CompanyNo +
            ", CompanyName="+CompanyName +
            ", Flag="+Flag +
            ", ErrorInfo="+ErrorInfo +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModIfyTime="+ModIfyTime +
            ", E_PolicyID="+E_PolicyID +"]";
    }
}
