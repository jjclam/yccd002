/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LDGrpToAgentSchema;
import com.sinosoft.lis.vschema.LDGrpToAgentSet;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LDGrpToAgentDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LDGrpToAgentDB extends LDGrpToAgentSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LDGrpToAgentDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LDGrpToAgent" );
        mflag = true;
    }

    public LDGrpToAgentDB() {
        con = null;
        db = new DBOper( "LDGrpToAgent" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LDGrpToAgentSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LDGrpToAgentSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LDGrpToAgent WHERE  1=1  AND CustomerNo = ? AND AgentCode = ?");
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getCustomerNo());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getAgentCode());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LDGrpToAgent");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LDGrpToAgent SET  CustomerNo = ? , AgentCode = ? , MainAgentFlag = ? , AgentName = ? , AgentCom = ? , AgentGroup = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? WHERE  1=1  AND CustomerNo = ? AND AgentCode = ?");
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getCustomerNo());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getAgentCode());
            }
            if(this.getMainAgentFlag() == null || this.getMainAgentFlag().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getMainAgentFlag());
            }
            if(this.getAgentName() == null || this.getAgentName().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getAgentName());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAgentCom());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getAgentGroup());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(8, 93);
            } else {
            	pstmt.setDate(8, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(9, 1);
            } else {
            	pstmt.setString(9, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(10, 93);
            } else {
            	pstmt.setDate(10, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(11, 1);
            } else {
            	pstmt.setString(11, this.getModifyTime());
            }
            // set where condition
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getCustomerNo());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getAgentCode());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LDGrpToAgent");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LDGrpToAgent VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getCustomerNo());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getAgentCode());
            }
            if(this.getMainAgentFlag() == null || this.getMainAgentFlag().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getMainAgentFlag());
            }
            if(this.getAgentName() == null || this.getAgentName().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getAgentName());
            }
            if(this.getAgentCom() == null || this.getAgentCom().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getAgentCom());
            }
            if(this.getAgentGroup() == null || this.getAgentGroup().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getAgentGroup());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(8, 93);
            } else {
            	pstmt.setDate(8, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(9, 1);
            } else {
            	pstmt.setString(9, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(10, 93);
            } else {
            	pstmt.setDate(10, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(11, 1);
            } else {
            	pstmt.setString(11, this.getModifyTime());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LDGrpToAgent WHERE  1=1  AND CustomerNo = ? AND AgentCode = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getCustomerNo());
            }
            if(this.getAgentCode() == null || this.getAgentCode().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getAgentCode());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpToAgentDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LDGrpToAgentSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LDGrpToAgentSet aLDGrpToAgentSet = new LDGrpToAgentSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDGrpToAgent");
            LDGrpToAgentSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpToAgentDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LDGrpToAgentSchema s1 = new LDGrpToAgentSchema();
                s1.setSchema(rs,i);
                aLDGrpToAgentSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLDGrpToAgentSet;
    }

    public LDGrpToAgentSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LDGrpToAgentSet aLDGrpToAgentSet = new LDGrpToAgentSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpToAgentDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LDGrpToAgentSchema s1 = new LDGrpToAgentSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpToAgentDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLDGrpToAgentSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDGrpToAgentSet;
    }

    public LDGrpToAgentSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LDGrpToAgentSet aLDGrpToAgentSet = new LDGrpToAgentSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDGrpToAgent");
            LDGrpToAgentSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LDGrpToAgentSchema s1 = new LDGrpToAgentSchema();
                s1.setSchema(rs,i);
                aLDGrpToAgentSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDGrpToAgentSet;
    }

    public LDGrpToAgentSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LDGrpToAgentSet aLDGrpToAgentSet = new LDGrpToAgentSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LDGrpToAgentSchema s1 = new LDGrpToAgentSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpToAgentDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLDGrpToAgentSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDGrpToAgentSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDGrpToAgent");
            LDGrpToAgentSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LDGrpToAgent " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LDGrpToAgentDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LDGrpToAgentSet
     */
    public LDGrpToAgentSet getData() {
        int tCount = 0;
        LDGrpToAgentSet tLDGrpToAgentSet = new LDGrpToAgentSet();
        LDGrpToAgentSchema tLDGrpToAgentSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLDGrpToAgentSchema = new LDGrpToAgentSchema();
            tLDGrpToAgentSchema.setSchema(mResultSet, 1);
            tLDGrpToAgentSet.add(tLDGrpToAgentSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLDGrpToAgentSchema = new LDGrpToAgentSchema();
                    tLDGrpToAgentSchema.setSchema(mResultSet, 1);
                    tLDGrpToAgentSet.add(tLDGrpToAgentSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLDGrpToAgentSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LDGrpToAgentDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LDGrpToAgentDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LDGrpToAgentDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
