/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCGrpContDB;

/**
 * <p>ClassName: LCGrpContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LCGrpContSchema implements Schema, Cloneable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体投保单号码 */
    private String ProposalGrpContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 销售渠道 */
    private String SaleChnl;
    /** 管理机构 */
    private String ManageCom;
    /** 保单口令 */
    private String Password;
    /** 密码 */
    private String Password2;
    /** 客户号码 */
    private String AppntNo;
    /** 投保总人数 */
    private int Peoples2;
    /** 付款方式 */
    private String GetFlag;
    /** 合同争议处理方式 */
    private String DisputedFlag;
    /** 集体特别约定 */
    private String GrpSpec;
    /** 交费方式 */
    private String PayMode;
    /** 签单机构 */
    private String SignCom;
    /** 签单日期 */
    private Date SignDate;
    /** 签单时间 */
    private String SignTime;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 交费间隔 */
    private int PayIntv;
    /** 总人数 */
    private int Peoples;
    /** 总份数 */
    private double Mult;
    /** 总保费 */
    private double Prem;
    /** 总保额 */
    private double Amnt;
    /** 总累计保费 */
    private double SumPrem;
    /** 总累计交费 */
    private double SumPay;
    /** 差额 */
    private double Dif;
    /** 备注 */
    private String Remark;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 复核状态 */
    private String ApproveFlag;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 核保人 */
    private String UWOperator;
    /** 核保状态 */
    private String UWFlag;
    /** 核保完成日期 */
    private Date UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 投保单/保单标志 */
    private String AppFlag;
    /** 投保单申请日期 */
    private Date PolApplyDate;
    /** 保单回执客户签收日期 */
    private Date CustomGetPolDate;
    /** 保单送达日期 */
    private Date GetPolDate;
    /** 保单送达时间 */
    private String GetPolTime;
    /** 状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 初审人 */
    private String FirstTrialOperator;
    /** 初审日期 */
    private Date FirstTrialDate;
    /** 初审时间 */
    private String FirstTrialTime;
    /** 总单类型 */
    private String ContType;
    /** 年金领取日期 */
    private Date AnnuityReceiveDate;
    /** 保障终止日期 */
    private Date CEndDate;
    /** 最初承保人数 */
    private int NativePeoples;
    /** 最初总保费 */
    private double NativePrem;
    /** 最初总保额 */
    private double NativeAmnt;
    /** 减保频率 */
    private String ReduceFre;
    /** 官网登录账号 */
    private String OffAcc;
    /** 官网登录密码 */
    private String OffPwd;
    /** 销售渠道编码 */
    private String SaleChannels;
    /** 电子投保单地址 */
    private String E_AppntID;
    /** 电子投保单生成日期 */
    private Date E_AppntDate;
    /** 电子合同地址 */
    private String E_ContID;
    /** 电子合同生成日期 */
    private Date E_ContDate;
    /** 是否为续保单 */
    private String IsRenewFlag;

    public static final int FIELDNUM = 66;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCGrpContSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "GrpContNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCGrpContSchema cloned = (LCGrpContSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getProposalGrpContNo() {
        return ProposalGrpContNo;
    }
    public void setProposalGrpContNo(String aProposalGrpContNo) {
        ProposalGrpContNo = aProposalGrpContNo;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getSaleChnl() {
        return SaleChnl;
    }
    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getPassword2() {
        return Password2;
    }
    public void setPassword2(String aPassword2) {
        Password2 = aPassword2;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public int getPeoples2() {
        return Peoples2;
    }
    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }
    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public String getGetFlag() {
        return GetFlag;
    }
    public void setGetFlag(String aGetFlag) {
        GetFlag = aGetFlag;
    }
    public String getDisputedFlag() {
        return DisputedFlag;
    }
    public void setDisputedFlag(String aDisputedFlag) {
        DisputedFlag = aDisputedFlag;
    }
    public String getGrpSpec() {
        return GrpSpec;
    }
    public void setGrpSpec(String aGrpSpec) {
        GrpSpec = aGrpSpec;
    }
    public String getPayMode() {
        return PayMode;
    }
    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }
    public String getSignCom() {
        return SignCom;
    }
    public void setSignCom(String aSignCom) {
        SignCom = aSignCom;
    }
    public String getSignDate() {
        if(SignDate != null) {
            return fDate.getString(SignDate);
        } else {
            return null;
        }
    }
    public void setSignDate(Date aSignDate) {
        SignDate = aSignDate;
    }
    public void setSignDate(String aSignDate) {
        if (aSignDate != null && !aSignDate.equals("")) {
            SignDate = fDate.getDate(aSignDate);
        } else
            SignDate = null;
    }

    public String getSignTime() {
        return SignTime;
    }
    public void setSignTime(String aSignTime) {
        SignTime = aSignTime;
    }
    public String getCValiDate() {
        if(CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }
    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }
    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else
            CValiDate = null;
    }

    public int getPayIntv() {
        return PayIntv;
    }
    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }
    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public double getMult() {
        return Mult;
    }
    public void setMult(double aMult) {
        Mult = aMult;
    }
    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = d;
        }
    }

    public double getPrem() {
        return Prem;
    }
    public void setPrem(double aPrem) {
        Prem = aPrem;
    }
    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = d;
        }
    }

    public double getAmnt() {
        return Amnt;
    }
    public void setAmnt(double aAmnt) {
        Amnt = aAmnt;
    }
    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = d;
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }
    public void setSumPrem(double aSumPrem) {
        SumPrem = aSumPrem;
    }
    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = d;
        }
    }

    public double getSumPay() {
        return SumPay;
    }
    public void setSumPay(double aSumPay) {
        SumPay = aSumPay;
    }
    public void setSumPay(String aSumPay) {
        if (aSumPay != null && !aSumPay.equals("")) {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = d;
        }
    }

    public double getDif() {
        return Dif;
    }
    public void setDif(double aDif) {
        Dif = aDif;
    }
    public void setDif(String aDif) {
        if (aDif != null && !aDif.equals("")) {
            Double tDouble = new Double(aDif);
            double d = tDouble.doubleValue();
            Dif = d;
        }
    }

    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getStandbyFlag1() {
        return StandbyFlag1;
    }
    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }
    public String getStandbyFlag2() {
        return StandbyFlag2;
    }
    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }
    public String getStandbyFlag3() {
        return StandbyFlag3;
    }
    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }
    public String getApproveFlag() {
        return ApproveFlag;
    }
    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }
    public String getApproveCode() {
        return ApproveCode;
    }
    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }
    public String getApproveDate() {
        if(ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }
    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }
    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else
            ApproveDate = null;
    }

    public String getApproveTime() {
        return ApproveTime;
    }
    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }
    public String getUWOperator() {
        return UWOperator;
    }
    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }
    public String getUWFlag() {
        return UWFlag;
    }
    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }
    public String getUWDate() {
        if(UWDate != null) {
            return fDate.getString(UWDate);
        } else {
            return null;
        }
    }
    public void setUWDate(Date aUWDate) {
        UWDate = aUWDate;
    }
    public void setUWDate(String aUWDate) {
        if (aUWDate != null && !aUWDate.equals("")) {
            UWDate = fDate.getDate(aUWDate);
        } else
            UWDate = null;
    }

    public String getUWTime() {
        return UWTime;
    }
    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }
    public String getAppFlag() {
        return AppFlag;
    }
    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }
    public String getPolApplyDate() {
        if(PolApplyDate != null) {
            return fDate.getString(PolApplyDate);
        } else {
            return null;
        }
    }
    public void setPolApplyDate(Date aPolApplyDate) {
        PolApplyDate = aPolApplyDate;
    }
    public void setPolApplyDate(String aPolApplyDate) {
        if (aPolApplyDate != null && !aPolApplyDate.equals("")) {
            PolApplyDate = fDate.getDate(aPolApplyDate);
        } else
            PolApplyDate = null;
    }

    public String getCustomGetPolDate() {
        if(CustomGetPolDate != null) {
            return fDate.getString(CustomGetPolDate);
        } else {
            return null;
        }
    }
    public void setCustomGetPolDate(Date aCustomGetPolDate) {
        CustomGetPolDate = aCustomGetPolDate;
    }
    public void setCustomGetPolDate(String aCustomGetPolDate) {
        if (aCustomGetPolDate != null && !aCustomGetPolDate.equals("")) {
            CustomGetPolDate = fDate.getDate(aCustomGetPolDate);
        } else
            CustomGetPolDate = null;
    }

    public String getGetPolDate() {
        if(GetPolDate != null) {
            return fDate.getString(GetPolDate);
        } else {
            return null;
        }
    }
    public void setGetPolDate(Date aGetPolDate) {
        GetPolDate = aGetPolDate;
    }
    public void setGetPolDate(String aGetPolDate) {
        if (aGetPolDate != null && !aGetPolDate.equals("")) {
            GetPolDate = fDate.getDate(aGetPolDate);
        } else
            GetPolDate = null;
    }

    public String getGetPolTime() {
        return GetPolTime;
    }
    public void setGetPolTime(String aGetPolTime) {
        GetPolTime = aGetPolTime;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getFirstTrialOperator() {
        return FirstTrialOperator;
    }
    public void setFirstTrialOperator(String aFirstTrialOperator) {
        FirstTrialOperator = aFirstTrialOperator;
    }
    public String getFirstTrialDate() {
        if(FirstTrialDate != null) {
            return fDate.getString(FirstTrialDate);
        } else {
            return null;
        }
    }
    public void setFirstTrialDate(Date aFirstTrialDate) {
        FirstTrialDate = aFirstTrialDate;
    }
    public void setFirstTrialDate(String aFirstTrialDate) {
        if (aFirstTrialDate != null && !aFirstTrialDate.equals("")) {
            FirstTrialDate = fDate.getDate(aFirstTrialDate);
        } else
            FirstTrialDate = null;
    }

    public String getFirstTrialTime() {
        return FirstTrialTime;
    }
    public void setFirstTrialTime(String aFirstTrialTime) {
        FirstTrialTime = aFirstTrialTime;
    }
    public String getContType() {
        return ContType;
    }
    public void setContType(String aContType) {
        ContType = aContType;
    }
    public String getAnnuityReceiveDate() {
        if(AnnuityReceiveDate != null) {
            return fDate.getString(AnnuityReceiveDate);
        } else {
            return null;
        }
    }
    public void setAnnuityReceiveDate(Date aAnnuityReceiveDate) {
        AnnuityReceiveDate = aAnnuityReceiveDate;
    }
    public void setAnnuityReceiveDate(String aAnnuityReceiveDate) {
        if (aAnnuityReceiveDate != null && !aAnnuityReceiveDate.equals("")) {
            AnnuityReceiveDate = fDate.getDate(aAnnuityReceiveDate);
        } else
            AnnuityReceiveDate = null;
    }

    public String getCEndDate() {
        if(CEndDate != null) {
            return fDate.getString(CEndDate);
        } else {
            return null;
        }
    }
    public void setCEndDate(Date aCEndDate) {
        CEndDate = aCEndDate;
    }
    public void setCEndDate(String aCEndDate) {
        if (aCEndDate != null && !aCEndDate.equals("")) {
            CEndDate = fDate.getDate(aCEndDate);
        } else
            CEndDate = null;
    }

    public int getNativePeoples() {
        return NativePeoples;
    }
    public void setNativePeoples(int aNativePeoples) {
        NativePeoples = aNativePeoples;
    }
    public void setNativePeoples(String aNativePeoples) {
        if (aNativePeoples != null && !aNativePeoples.equals("")) {
            Integer tInteger = new Integer(aNativePeoples);
            int i = tInteger.intValue();
            NativePeoples = i;
        }
    }

    public double getNativePrem() {
        return NativePrem;
    }
    public void setNativePrem(double aNativePrem) {
        NativePrem = aNativePrem;
    }
    public void setNativePrem(String aNativePrem) {
        if (aNativePrem != null && !aNativePrem.equals("")) {
            Double tDouble = new Double(aNativePrem);
            double d = tDouble.doubleValue();
            NativePrem = d;
        }
    }

    public double getNativeAmnt() {
        return NativeAmnt;
    }
    public void setNativeAmnt(double aNativeAmnt) {
        NativeAmnt = aNativeAmnt;
    }
    public void setNativeAmnt(String aNativeAmnt) {
        if (aNativeAmnt != null && !aNativeAmnt.equals("")) {
            Double tDouble = new Double(aNativeAmnt);
            double d = tDouble.doubleValue();
            NativeAmnt = d;
        }
    }

    public String getReduceFre() {
        return ReduceFre;
    }
    public void setReduceFre(String aReduceFre) {
        ReduceFre = aReduceFre;
    }
    public String getOffAcc() {
        return OffAcc;
    }
    public void setOffAcc(String aOffAcc) {
        OffAcc = aOffAcc;
    }
    public String getOffPwd() {
        return OffPwd;
    }
    public void setOffPwd(String aOffPwd) {
        OffPwd = aOffPwd;
    }
    public String getSaleChannels() {
        return SaleChannels;
    }
    public void setSaleChannels(String aSaleChannels) {
        SaleChannels = aSaleChannels;
    }
    public String getE_AppntID() {
        return E_AppntID;
    }
    public void setE_AppntID(String aE_AppntID) {
        E_AppntID = aE_AppntID;
    }
    public String getE_AppntDate() {
        if(E_AppntDate != null) {
            return fDate.getString(E_AppntDate);
        } else {
            return null;
        }
    }
    public void setE_AppntDate(Date aE_AppntDate) {
        E_AppntDate = aE_AppntDate;
    }
    public void setE_AppntDate(String aE_AppntDate) {
        if (aE_AppntDate != null && !aE_AppntDate.equals("")) {
            E_AppntDate = fDate.getDate(aE_AppntDate);
        } else
            E_AppntDate = null;
    }

    public String getE_ContID() {
        return E_ContID;
    }
    public void setE_ContID(String aE_ContID) {
        E_ContID = aE_ContID;
    }
    public String getE_ContDate() {
        if(E_ContDate != null) {
            return fDate.getString(E_ContDate);
        } else {
            return null;
        }
    }
    public void setE_ContDate(Date aE_ContDate) {
        E_ContDate = aE_ContDate;
    }
    public void setE_ContDate(String aE_ContDate) {
        if (aE_ContDate != null && !aE_ContDate.equals("")) {
            E_ContDate = fDate.getDate(aE_ContDate);
        } else
            E_ContDate = null;
    }

    public String getIsRenewFlag() {
        return IsRenewFlag;
    }
    public void setIsRenewFlag(String aIsRenewFlag) {
        IsRenewFlag = aIsRenewFlag;
    }

    /**
    * 使用另外一个 LCGrpContSchema 对象给 Schema 赋值
    * @param: aLCGrpContSchema LCGrpContSchema
    **/
    public void setSchema(LCGrpContSchema aLCGrpContSchema) {
        this.GrpContNo = aLCGrpContSchema.getGrpContNo();
        this.ProposalGrpContNo = aLCGrpContSchema.getProposalGrpContNo();
        this.PrtNo = aLCGrpContSchema.getPrtNo();
        this.SaleChnl = aLCGrpContSchema.getSaleChnl();
        this.ManageCom = aLCGrpContSchema.getManageCom();
        this.Password = aLCGrpContSchema.getPassword();
        this.Password2 = aLCGrpContSchema.getPassword2();
        this.AppntNo = aLCGrpContSchema.getAppntNo();
        this.Peoples2 = aLCGrpContSchema.getPeoples2();
        this.GetFlag = aLCGrpContSchema.getGetFlag();
        this.DisputedFlag = aLCGrpContSchema.getDisputedFlag();
        this.GrpSpec = aLCGrpContSchema.getGrpSpec();
        this.PayMode = aLCGrpContSchema.getPayMode();
        this.SignCom = aLCGrpContSchema.getSignCom();
        this.SignDate = fDate.getDate( aLCGrpContSchema.getSignDate());
        this.SignTime = aLCGrpContSchema.getSignTime();
        this.CValiDate = fDate.getDate( aLCGrpContSchema.getCValiDate());
        this.PayIntv = aLCGrpContSchema.getPayIntv();
        this.Peoples = aLCGrpContSchema.getPeoples();
        this.Mult = aLCGrpContSchema.getMult();
        this.Prem = aLCGrpContSchema.getPrem();
        this.Amnt = aLCGrpContSchema.getAmnt();
        this.SumPrem = aLCGrpContSchema.getSumPrem();
        this.SumPay = aLCGrpContSchema.getSumPay();
        this.Dif = aLCGrpContSchema.getDif();
        this.Remark = aLCGrpContSchema.getRemark();
        this.StandbyFlag1 = aLCGrpContSchema.getStandbyFlag1();
        this.StandbyFlag2 = aLCGrpContSchema.getStandbyFlag2();
        this.StandbyFlag3 = aLCGrpContSchema.getStandbyFlag3();
        this.ApproveFlag = aLCGrpContSchema.getApproveFlag();
        this.ApproveCode = aLCGrpContSchema.getApproveCode();
        this.ApproveDate = fDate.getDate( aLCGrpContSchema.getApproveDate());
        this.ApproveTime = aLCGrpContSchema.getApproveTime();
        this.UWOperator = aLCGrpContSchema.getUWOperator();
        this.UWFlag = aLCGrpContSchema.getUWFlag();
        this.UWDate = fDate.getDate( aLCGrpContSchema.getUWDate());
        this.UWTime = aLCGrpContSchema.getUWTime();
        this.AppFlag = aLCGrpContSchema.getAppFlag();
        this.PolApplyDate = fDate.getDate( aLCGrpContSchema.getPolApplyDate());
        this.CustomGetPolDate = fDate.getDate( aLCGrpContSchema.getCustomGetPolDate());
        this.GetPolDate = fDate.getDate( aLCGrpContSchema.getGetPolDate());
        this.GetPolTime = aLCGrpContSchema.getGetPolTime();
        this.State = aLCGrpContSchema.getState();
        this.Operator = aLCGrpContSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCGrpContSchema.getMakeDate());
        this.MakeTime = aLCGrpContSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCGrpContSchema.getModifyDate());
        this.ModifyTime = aLCGrpContSchema.getModifyTime();
        this.FirstTrialOperator = aLCGrpContSchema.getFirstTrialOperator();
        this.FirstTrialDate = fDate.getDate( aLCGrpContSchema.getFirstTrialDate());
        this.FirstTrialTime = aLCGrpContSchema.getFirstTrialTime();
        this.ContType = aLCGrpContSchema.getContType();
        this.AnnuityReceiveDate = fDate.getDate( aLCGrpContSchema.getAnnuityReceiveDate());
        this.CEndDate = fDate.getDate( aLCGrpContSchema.getCEndDate());
        this.NativePeoples = aLCGrpContSchema.getNativePeoples();
        this.NativePrem = aLCGrpContSchema.getNativePrem();
        this.NativeAmnt = aLCGrpContSchema.getNativeAmnt();
        this.ReduceFre = aLCGrpContSchema.getReduceFre();
        this.OffAcc = aLCGrpContSchema.getOffAcc();
        this.OffPwd = aLCGrpContSchema.getOffPwd();
        this.SaleChannels = aLCGrpContSchema.getSaleChannels();
        this.E_AppntID = aLCGrpContSchema.getE_AppntID();
        this.E_AppntDate = fDate.getDate( aLCGrpContSchema.getE_AppntDate());
        this.E_ContID = aLCGrpContSchema.getE_ContID();
        this.E_ContDate = fDate.getDate( aLCGrpContSchema.getE_ContDate());
        this.IsRenewFlag = aLCGrpContSchema.getIsRenewFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("ProposalGrpContNo") == null )
                this.ProposalGrpContNo = null;
            else
                this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("SaleChnl") == null )
                this.SaleChnl = null;
            else
                this.SaleChnl = rs.getString("SaleChnl").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("Password") == null )
                this.Password = null;
            else
                this.Password = rs.getString("Password").trim();

            if( rs.getString("Password2") == null )
                this.Password2 = null;
            else
                this.Password2 = rs.getString("Password2").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            this.Peoples2 = rs.getInt("Peoples2");
            if( rs.getString("GetFlag") == null )
                this.GetFlag = null;
            else
                this.GetFlag = rs.getString("GetFlag").trim();

            if( rs.getString("DisputedFlag") == null )
                this.DisputedFlag = null;
            else
                this.DisputedFlag = rs.getString("DisputedFlag").trim();

            if( rs.getString("GrpSpec") == null )
                this.GrpSpec = null;
            else
                this.GrpSpec = rs.getString("GrpSpec").trim();

            if( rs.getString("PayMode") == null )
                this.PayMode = null;
            else
                this.PayMode = rs.getString("PayMode").trim();

            if( rs.getString("SignCom") == null )
                this.SignCom = null;
            else
                this.SignCom = rs.getString("SignCom").trim();

            this.SignDate = rs.getDate("SignDate");
            if( rs.getString("SignTime") == null )
                this.SignTime = null;
            else
                this.SignTime = rs.getString("SignTime").trim();

            this.CValiDate = rs.getDate("CValiDate");
            this.PayIntv = rs.getInt("PayIntv");
            this.Peoples = rs.getInt("Peoples");
            this.Mult = rs.getDouble("Mult");
            this.Prem = rs.getDouble("Prem");
            this.Amnt = rs.getDouble("Amnt");
            this.SumPrem = rs.getDouble("SumPrem");
            this.SumPay = rs.getDouble("SumPay");
            this.Dif = rs.getDouble("Dif");
            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("StandbyFlag1") == null )
                this.StandbyFlag1 = null;
            else
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

            if( rs.getString("StandbyFlag2") == null )
                this.StandbyFlag2 = null;
            else
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

            if( rs.getString("StandbyFlag3") == null )
                this.StandbyFlag3 = null;
            else
                this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

            if( rs.getString("ApproveFlag") == null )
                this.ApproveFlag = null;
            else
                this.ApproveFlag = rs.getString("ApproveFlag").trim();

            if( rs.getString("ApproveCode") == null )
                this.ApproveCode = null;
            else
                this.ApproveCode = rs.getString("ApproveCode").trim();

            this.ApproveDate = rs.getDate("ApproveDate");
            if( rs.getString("ApproveTime") == null )
                this.ApproveTime = null;
            else
                this.ApproveTime = rs.getString("ApproveTime").trim();

            if( rs.getString("UWOperator") == null )
                this.UWOperator = null;
            else
                this.UWOperator = rs.getString("UWOperator").trim();

            if( rs.getString("UWFlag") == null )
                this.UWFlag = null;
            else
                this.UWFlag = rs.getString("UWFlag").trim();

            this.UWDate = rs.getDate("UWDate");
            if( rs.getString("UWTime") == null )
                this.UWTime = null;
            else
                this.UWTime = rs.getString("UWTime").trim();

            if( rs.getString("AppFlag") == null )
                this.AppFlag = null;
            else
                this.AppFlag = rs.getString("AppFlag").trim();

            this.PolApplyDate = rs.getDate("PolApplyDate");
            this.CustomGetPolDate = rs.getDate("CustomGetPolDate");
            this.GetPolDate = rs.getDate("GetPolDate");
            if( rs.getString("GetPolTime") == null )
                this.GetPolTime = null;
            else
                this.GetPolTime = rs.getString("GetPolTime").trim();

            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("FirstTrialOperator") == null )
                this.FirstTrialOperator = null;
            else
                this.FirstTrialOperator = rs.getString("FirstTrialOperator").trim();

            this.FirstTrialDate = rs.getDate("FirstTrialDate");
            if( rs.getString("FirstTrialTime") == null )
                this.FirstTrialTime = null;
            else
                this.FirstTrialTime = rs.getString("FirstTrialTime").trim();

            if( rs.getString("ContType") == null )
                this.ContType = null;
            else
                this.ContType = rs.getString("ContType").trim();

            this.AnnuityReceiveDate = rs.getDate("AnnuityReceiveDate");
            this.CEndDate = rs.getDate("CEndDate");
            this.NativePeoples = rs.getInt("NativePeoples");
            this.NativePrem = rs.getDouble("NativePrem");
            this.NativeAmnt = rs.getDouble("NativeAmnt");
            if( rs.getString("ReduceFre") == null )
                this.ReduceFre = null;
            else
                this.ReduceFre = rs.getString("ReduceFre").trim();

            if( rs.getString("OffAcc") == null )
                this.OffAcc = null;
            else
                this.OffAcc = rs.getString("OffAcc").trim();

            if( rs.getString("OffPwd") == null )
                this.OffPwd = null;
            else
                this.OffPwd = rs.getString("OffPwd").trim();

            if( rs.getString("SaleChannels") == null )
                this.SaleChannels = null;
            else
                this.SaleChannels = rs.getString("SaleChannels").trim();

            if( rs.getString("E_AppntID") == null )
                this.E_AppntID = null;
            else
                this.E_AppntID = rs.getString("E_AppntID").trim();

            this.E_AppntDate = rs.getDate("E_AppntDate");
            if( rs.getString("E_ContID") == null )
                this.E_ContID = null;
            else
                this.E_ContID = rs.getString("E_ContID").trim();

            this.E_ContDate = rs.getDate("E_ContDate");
            if( rs.getString("IsRenewFlag") == null )
                this.IsRenewFlag = null;
            else
                this.IsRenewFlag = rs.getString("IsRenewFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCGrpContSchema getSchema() {
        LCGrpContSchema aLCGrpContSchema = new LCGrpContSchema();
        aLCGrpContSchema.setSchema(this);
        return aLCGrpContSchema;
    }

    public LCGrpContDB getDB() {
        LCGrpContDB aDBOper = new LCGrpContDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpCont描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalGrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Password2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples2));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisputedFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpSpec)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SignTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPay));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Dif));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CustomGetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( GetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetPolTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstTrialOperator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FirstTrialDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstTrialTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AnnuityReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(NativePeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(NativePrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(NativeAmnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReduceFre)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OffAcc)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OffPwd)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChannels)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(E_AppntID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( E_AppntDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(E_ContID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( E_ContDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsRenewFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpCont>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Password2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            Peoples2 = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
            GetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            DisputedFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            GrpSpec = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            SignCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
            SignTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).intValue();
            Peoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).intValue();
            Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
            SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
            SumPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
            Dif = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            ApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
            CustomGetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
            GetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
            GetPolTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            FirstTrialOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            FirstTrialDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50,SysConst.PACKAGESPILTER));
            FirstTrialTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            AnnuityReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53,SysConst.PACKAGESPILTER));
            CEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,SysConst.PACKAGESPILTER));
            NativePeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,55,SysConst.PACKAGESPILTER))).intValue();
            NativePrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).doubleValue();
            NativeAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,57,SysConst.PACKAGESPILTER))).doubleValue();
            ReduceFre = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
            OffAcc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
            OffPwd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
            SaleChannels = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
            E_AppntID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
            E_AppntDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63,SysConst.PACKAGESPILTER));
            E_ContID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
            E_ContDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65,SysConst.PACKAGESPILTER));
            IsRenewFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("Password2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password2));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetFlag));
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
        }
        if (FCode.equalsIgnoreCase("GrpSpec")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpSpec));
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
        }
        if (FCode.equalsIgnoreCase("GetPolTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolTime));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equalsIgnoreCase("AnnuityReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAnnuityReceiveDate()));
        }
        if (FCode.equalsIgnoreCase("CEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCEndDate()));
        }
        if (FCode.equalsIgnoreCase("NativePeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePeoples));
        }
        if (FCode.equalsIgnoreCase("NativePrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePrem));
        }
        if (FCode.equalsIgnoreCase("NativeAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativeAmnt));
        }
        if (FCode.equalsIgnoreCase("ReduceFre")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReduceFre));
        }
        if (FCode.equalsIgnoreCase("OffAcc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffAcc));
        }
        if (FCode.equalsIgnoreCase("OffPwd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffPwd));
        }
        if (FCode.equalsIgnoreCase("SaleChannels")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChannels));
        }
        if (FCode.equalsIgnoreCase("E_AppntID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_AppntID));
        }
        if (FCode.equalsIgnoreCase("E_AppntDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getE_AppntDate()));
        }
        if (FCode.equalsIgnoreCase("E_ContID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_ContID));
        }
        if (FCode.equalsIgnoreCase("E_ContDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getE_ContDate()));
        }
        if (FCode.equalsIgnoreCase("IsRenewFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsRenewFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Password2);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 8:
                strFieldValue = String.valueOf(Peoples2);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(GetFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(DisputedFlag);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(GrpSpec);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(SignCom);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(SignTime);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
                break;
            case 17:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 18:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 19:
                strFieldValue = String.valueOf(Mult);
                break;
            case 20:
                strFieldValue = String.valueOf(Prem);
                break;
            case 21:
                strFieldValue = String.valueOf(Amnt);
                break;
            case 22:
                strFieldValue = String.valueOf(SumPrem);
                break;
            case 23:
                strFieldValue = String.valueOf(SumPay);
                break;
            case 24:
                strFieldValue = String.valueOf(Dif);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(ApproveFlag);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(ApproveTime);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(UWOperator);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(UWFlag);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(UWTime);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(AppFlag);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(GetPolTime);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(FirstTrialOperator);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(FirstTrialTime);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(ContType);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAnnuityReceiveDate()));
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCEndDate()));
                break;
            case 54:
                strFieldValue = String.valueOf(NativePeoples);
                break;
            case 55:
                strFieldValue = String.valueOf(NativePrem);
                break;
            case 56:
                strFieldValue = String.valueOf(NativeAmnt);
                break;
            case 57:
                strFieldValue = StrTool.GBKToUnicode(ReduceFre);
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(OffAcc);
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(OffPwd);
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(SaleChannels);
                break;
            case 61:
                strFieldValue = StrTool.GBKToUnicode(E_AppntID);
                break;
            case 62:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getE_AppntDate()));
                break;
            case 63:
                strFieldValue = StrTool.GBKToUnicode(E_ContID);
                break;
            case 64:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getE_ContDate()));
                break;
            case 65:
                strFieldValue = StrTool.GBKToUnicode(IsRenewFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
                ProposalGrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
                SaleChnl = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("Password2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password2 = FValue.trim();
            }
            else
                Password2 = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetFlag = FValue.trim();
            }
            else
                GetFlag = null;
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisputedFlag = FValue.trim();
            }
            else
                DisputedFlag = null;
        }
        if (FCode.equalsIgnoreCase("GrpSpec")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpSpec = FValue.trim();
            }
            else
                GrpSpec = null;
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
                PayMode = null;
        }
        if (FCode.equalsIgnoreCase("SignCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignCom = FValue.trim();
            }
            else
                SignCom = null;
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if(FValue != null && !FValue.equals("")) {
                SignDate = fDate.getDate( FValue );
            }
            else
                SignDate = null;
        }
        if (FCode.equalsIgnoreCase("SignTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                SignTime = FValue.trim();
            }
            else
                SignTime = null;
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if(FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate( FValue );
            }
            else
                CValiDate = null;
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Dif = d;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag1 = FValue.trim();
            }
            else
                StandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag2 = FValue.trim();
            }
            else
                StandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if( FValue != null && !FValue.equals(""))
            {
                StandbyFlag3 = FValue.trim();
            }
            else
                StandbyFlag3 = null;
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveFlag = FValue.trim();
            }
            else
                ApproveFlag = null;
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
                ApproveCode = null;
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if(FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate( FValue );
            }
            else
                ApproveDate = null;
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ApproveTime = FValue.trim();
            }
            else
                ApproveTime = null;
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
                UWOperator = null;
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWFlag = FValue.trim();
            }
            else
                UWFlag = null;
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if(FValue != null && !FValue.equals("")) {
                UWDate = fDate.getDate( FValue );
            }
            else
                UWDate = null;
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
                UWTime = null;
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppFlag = FValue.trim();
            }
            else
                AppFlag = null;
        }
        if (FCode.equalsIgnoreCase("PolApplyDate")) {
            if(FValue != null && !FValue.equals("")) {
                PolApplyDate = fDate.getDate( FValue );
            }
            else
                PolApplyDate = null;
        }
        if (FCode.equalsIgnoreCase("CustomGetPolDate")) {
            if(FValue != null && !FValue.equals("")) {
                CustomGetPolDate = fDate.getDate( FValue );
            }
            else
                CustomGetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolDate")) {
            if(FValue != null && !FValue.equals("")) {
                GetPolDate = fDate.getDate( FValue );
            }
            else
                GetPolDate = null;
        }
        if (FCode.equalsIgnoreCase("GetPolTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetPolTime = FValue.trim();
            }
            else
                GetPolTime = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialOperator")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialOperator = FValue.trim();
            }
            else
                FirstTrialOperator = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialDate")) {
            if(FValue != null && !FValue.equals("")) {
                FirstTrialDate = fDate.getDate( FValue );
            }
            else
                FirstTrialDate = null;
        }
        if (FCode.equalsIgnoreCase("FirstTrialTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                FirstTrialTime = FValue.trim();
            }
            else
                FirstTrialTime = null;
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContType = FValue.trim();
            }
            else
                ContType = null;
        }
        if (FCode.equalsIgnoreCase("AnnuityReceiveDate")) {
            if(FValue != null && !FValue.equals("")) {
                AnnuityReceiveDate = fDate.getDate( FValue );
            }
            else
                AnnuityReceiveDate = null;
        }
        if (FCode.equalsIgnoreCase("CEndDate")) {
            if(FValue != null && !FValue.equals("")) {
                CEndDate = fDate.getDate( FValue );
            }
            else
                CEndDate = null;
        }
        if (FCode.equalsIgnoreCase("NativePeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                NativePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("NativePrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NativePrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("NativeAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NativeAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("ReduceFre")) {
            if( FValue != null && !FValue.equals(""))
            {
                ReduceFre = FValue.trim();
            }
            else
                ReduceFre = null;
        }
        if (FCode.equalsIgnoreCase("OffAcc")) {
            if( FValue != null && !FValue.equals(""))
            {
                OffAcc = FValue.trim();
            }
            else
                OffAcc = null;
        }
        if (FCode.equalsIgnoreCase("OffPwd")) {
            if( FValue != null && !FValue.equals(""))
            {
                OffPwd = FValue.trim();
            }
            else
                OffPwd = null;
        }
        if (FCode.equalsIgnoreCase("SaleChannels")) {
            if( FValue != null && !FValue.equals(""))
            {
                SaleChannels = FValue.trim();
            }
            else
                SaleChannels = null;
        }
        if (FCode.equalsIgnoreCase("E_AppntID")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_AppntID = FValue.trim();
            }
            else
                E_AppntID = null;
        }
        if (FCode.equalsIgnoreCase("E_AppntDate")) {
            if(FValue != null && !FValue.equals("")) {
                E_AppntDate = fDate.getDate( FValue );
            }
            else
                E_AppntDate = null;
        }
        if (FCode.equalsIgnoreCase("E_ContID")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_ContID = FValue.trim();
            }
            else
                E_ContID = null;
        }
        if (FCode.equalsIgnoreCase("E_ContDate")) {
            if(FValue != null && !FValue.equals("")) {
                E_ContDate = fDate.getDate( FValue );
            }
            else
                E_ContDate = null;
        }
        if (FCode.equalsIgnoreCase("IsRenewFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                IsRenewFlag = FValue.trim();
            }
            else
                IsRenewFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCGrpContSchema other = (LCGrpContSchema)otherObject;
        return
            GrpContNo.equals(other.getGrpContNo())
            && ProposalGrpContNo.equals(other.getProposalGrpContNo())
            && PrtNo.equals(other.getPrtNo())
            && SaleChnl.equals(other.getSaleChnl())
            && ManageCom.equals(other.getManageCom())
            && Password.equals(other.getPassword())
            && Password2.equals(other.getPassword2())
            && AppntNo.equals(other.getAppntNo())
            && Peoples2 == other.getPeoples2()
            && GetFlag.equals(other.getGetFlag())
            && DisputedFlag.equals(other.getDisputedFlag())
            && GrpSpec.equals(other.getGrpSpec())
            && PayMode.equals(other.getPayMode())
            && SignCom.equals(other.getSignCom())
            && fDate.getString(SignDate).equals(other.getSignDate())
            && SignTime.equals(other.getSignTime())
            && fDate.getString(CValiDate).equals(other.getCValiDate())
            && PayIntv == other.getPayIntv()
            && Peoples == other.getPeoples()
            && Mult == other.getMult()
            && Prem == other.getPrem()
            && Amnt == other.getAmnt()
            && SumPrem == other.getSumPrem()
            && SumPay == other.getSumPay()
            && Dif == other.getDif()
            && Remark.equals(other.getRemark())
            && StandbyFlag1.equals(other.getStandbyFlag1())
            && StandbyFlag2.equals(other.getStandbyFlag2())
            && StandbyFlag3.equals(other.getStandbyFlag3())
            && ApproveFlag.equals(other.getApproveFlag())
            && ApproveCode.equals(other.getApproveCode())
            && fDate.getString(ApproveDate).equals(other.getApproveDate())
            && ApproveTime.equals(other.getApproveTime())
            && UWOperator.equals(other.getUWOperator())
            && UWFlag.equals(other.getUWFlag())
            && fDate.getString(UWDate).equals(other.getUWDate())
            && UWTime.equals(other.getUWTime())
            && AppFlag.equals(other.getAppFlag())
            && fDate.getString(PolApplyDate).equals(other.getPolApplyDate())
            && fDate.getString(CustomGetPolDate).equals(other.getCustomGetPolDate())
            && fDate.getString(GetPolDate).equals(other.getGetPolDate())
            && GetPolTime.equals(other.getGetPolTime())
            && State.equals(other.getState())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && FirstTrialOperator.equals(other.getFirstTrialOperator())
            && fDate.getString(FirstTrialDate).equals(other.getFirstTrialDate())
            && FirstTrialTime.equals(other.getFirstTrialTime())
            && ContType.equals(other.getContType())
            && fDate.getString(AnnuityReceiveDate).equals(other.getAnnuityReceiveDate())
            && fDate.getString(CEndDate).equals(other.getCEndDate())
            && NativePeoples == other.getNativePeoples()
            && NativePrem == other.getNativePrem()
            && NativeAmnt == other.getNativeAmnt()
            && ReduceFre.equals(other.getReduceFre())
            && OffAcc.equals(other.getOffAcc())
            && OffPwd.equals(other.getOffPwd())
            && SaleChannels.equals(other.getSaleChannels())
            && E_AppntID.equals(other.getE_AppntID())
            && fDate.getString(E_AppntDate).equals(other.getE_AppntDate())
            && E_ContID.equals(other.getE_ContID())
            && fDate.getString(E_ContDate).equals(other.getE_ContDate())
            && IsRenewFlag.equals(other.getIsRenewFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("GrpContNo") ) {
            return 0;
        }
        if( strFieldName.equals("ProposalGrpContNo") ) {
            return 1;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 2;
        }
        if( strFieldName.equals("SaleChnl") ) {
            return 3;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 4;
        }
        if( strFieldName.equals("Password") ) {
            return 5;
        }
        if( strFieldName.equals("Password2") ) {
            return 6;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 7;
        }
        if( strFieldName.equals("Peoples2") ) {
            return 8;
        }
        if( strFieldName.equals("GetFlag") ) {
            return 9;
        }
        if( strFieldName.equals("DisputedFlag") ) {
            return 10;
        }
        if( strFieldName.equals("GrpSpec") ) {
            return 11;
        }
        if( strFieldName.equals("PayMode") ) {
            return 12;
        }
        if( strFieldName.equals("SignCom") ) {
            return 13;
        }
        if( strFieldName.equals("SignDate") ) {
            return 14;
        }
        if( strFieldName.equals("SignTime") ) {
            return 15;
        }
        if( strFieldName.equals("CValiDate") ) {
            return 16;
        }
        if( strFieldName.equals("PayIntv") ) {
            return 17;
        }
        if( strFieldName.equals("Peoples") ) {
            return 18;
        }
        if( strFieldName.equals("Mult") ) {
            return 19;
        }
        if( strFieldName.equals("Prem") ) {
            return 20;
        }
        if( strFieldName.equals("Amnt") ) {
            return 21;
        }
        if( strFieldName.equals("SumPrem") ) {
            return 22;
        }
        if( strFieldName.equals("SumPay") ) {
            return 23;
        }
        if( strFieldName.equals("Dif") ) {
            return 24;
        }
        if( strFieldName.equals("Remark") ) {
            return 25;
        }
        if( strFieldName.equals("StandbyFlag1") ) {
            return 26;
        }
        if( strFieldName.equals("StandbyFlag2") ) {
            return 27;
        }
        if( strFieldName.equals("StandbyFlag3") ) {
            return 28;
        }
        if( strFieldName.equals("ApproveFlag") ) {
            return 29;
        }
        if( strFieldName.equals("ApproveCode") ) {
            return 30;
        }
        if( strFieldName.equals("ApproveDate") ) {
            return 31;
        }
        if( strFieldName.equals("ApproveTime") ) {
            return 32;
        }
        if( strFieldName.equals("UWOperator") ) {
            return 33;
        }
        if( strFieldName.equals("UWFlag") ) {
            return 34;
        }
        if( strFieldName.equals("UWDate") ) {
            return 35;
        }
        if( strFieldName.equals("UWTime") ) {
            return 36;
        }
        if( strFieldName.equals("AppFlag") ) {
            return 37;
        }
        if( strFieldName.equals("PolApplyDate") ) {
            return 38;
        }
        if( strFieldName.equals("CustomGetPolDate") ) {
            return 39;
        }
        if( strFieldName.equals("GetPolDate") ) {
            return 40;
        }
        if( strFieldName.equals("GetPolTime") ) {
            return 41;
        }
        if( strFieldName.equals("State") ) {
            return 42;
        }
        if( strFieldName.equals("Operator") ) {
            return 43;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 44;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 45;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 46;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 47;
        }
        if( strFieldName.equals("FirstTrialOperator") ) {
            return 48;
        }
        if( strFieldName.equals("FirstTrialDate") ) {
            return 49;
        }
        if( strFieldName.equals("FirstTrialTime") ) {
            return 50;
        }
        if( strFieldName.equals("ContType") ) {
            return 51;
        }
        if( strFieldName.equals("AnnuityReceiveDate") ) {
            return 52;
        }
        if( strFieldName.equals("CEndDate") ) {
            return 53;
        }
        if( strFieldName.equals("NativePeoples") ) {
            return 54;
        }
        if( strFieldName.equals("NativePrem") ) {
            return 55;
        }
        if( strFieldName.equals("NativeAmnt") ) {
            return 56;
        }
        if( strFieldName.equals("ReduceFre") ) {
            return 57;
        }
        if( strFieldName.equals("OffAcc") ) {
            return 58;
        }
        if( strFieldName.equals("OffPwd") ) {
            return 59;
        }
        if( strFieldName.equals("SaleChannels") ) {
            return 60;
        }
        if( strFieldName.equals("E_AppntID") ) {
            return 61;
        }
        if( strFieldName.equals("E_AppntDate") ) {
            return 62;
        }
        if( strFieldName.equals("E_ContID") ) {
            return 63;
        }
        if( strFieldName.equals("E_ContDate") ) {
            return 64;
        }
        if( strFieldName.equals("IsRenewFlag") ) {
            return 65;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "PrtNo";
                break;
            case 3:
                strFieldName = "SaleChnl";
                break;
            case 4:
                strFieldName = "ManageCom";
                break;
            case 5:
                strFieldName = "Password";
                break;
            case 6:
                strFieldName = "Password2";
                break;
            case 7:
                strFieldName = "AppntNo";
                break;
            case 8:
                strFieldName = "Peoples2";
                break;
            case 9:
                strFieldName = "GetFlag";
                break;
            case 10:
                strFieldName = "DisputedFlag";
                break;
            case 11:
                strFieldName = "GrpSpec";
                break;
            case 12:
                strFieldName = "PayMode";
                break;
            case 13:
                strFieldName = "SignCom";
                break;
            case 14:
                strFieldName = "SignDate";
                break;
            case 15:
                strFieldName = "SignTime";
                break;
            case 16:
                strFieldName = "CValiDate";
                break;
            case 17:
                strFieldName = "PayIntv";
                break;
            case 18:
                strFieldName = "Peoples";
                break;
            case 19:
                strFieldName = "Mult";
                break;
            case 20:
                strFieldName = "Prem";
                break;
            case 21:
                strFieldName = "Amnt";
                break;
            case 22:
                strFieldName = "SumPrem";
                break;
            case 23:
                strFieldName = "SumPay";
                break;
            case 24:
                strFieldName = "Dif";
                break;
            case 25:
                strFieldName = "Remark";
                break;
            case 26:
                strFieldName = "StandbyFlag1";
                break;
            case 27:
                strFieldName = "StandbyFlag2";
                break;
            case 28:
                strFieldName = "StandbyFlag3";
                break;
            case 29:
                strFieldName = "ApproveFlag";
                break;
            case 30:
                strFieldName = "ApproveCode";
                break;
            case 31:
                strFieldName = "ApproveDate";
                break;
            case 32:
                strFieldName = "ApproveTime";
                break;
            case 33:
                strFieldName = "UWOperator";
                break;
            case 34:
                strFieldName = "UWFlag";
                break;
            case 35:
                strFieldName = "UWDate";
                break;
            case 36:
                strFieldName = "UWTime";
                break;
            case 37:
                strFieldName = "AppFlag";
                break;
            case 38:
                strFieldName = "PolApplyDate";
                break;
            case 39:
                strFieldName = "CustomGetPolDate";
                break;
            case 40:
                strFieldName = "GetPolDate";
                break;
            case 41:
                strFieldName = "GetPolTime";
                break;
            case 42:
                strFieldName = "State";
                break;
            case 43:
                strFieldName = "Operator";
                break;
            case 44:
                strFieldName = "MakeDate";
                break;
            case 45:
                strFieldName = "MakeTime";
                break;
            case 46:
                strFieldName = "ModifyDate";
                break;
            case 47:
                strFieldName = "ModifyTime";
                break;
            case 48:
                strFieldName = "FirstTrialOperator";
                break;
            case 49:
                strFieldName = "FirstTrialDate";
                break;
            case 50:
                strFieldName = "FirstTrialTime";
                break;
            case 51:
                strFieldName = "ContType";
                break;
            case 52:
                strFieldName = "AnnuityReceiveDate";
                break;
            case 53:
                strFieldName = "CEndDate";
                break;
            case 54:
                strFieldName = "NativePeoples";
                break;
            case 55:
                strFieldName = "NativePrem";
                break;
            case 56:
                strFieldName = "NativeAmnt";
                break;
            case 57:
                strFieldName = "ReduceFre";
                break;
            case 58:
                strFieldName = "OffAcc";
                break;
            case 59:
                strFieldName = "OffPwd";
                break;
            case 60:
                strFieldName = "SaleChannels";
                break;
            case 61:
                strFieldName = "E_AppntID";
                break;
            case 62:
                strFieldName = "E_AppntDate";
                break;
            case 63:
                strFieldName = "E_ContID";
                break;
            case 64:
                strFieldName = "E_ContDate";
                break;
            case 65:
                strFieldName = "IsRenewFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "PROPOSALGRPCONTNO":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "PASSWORD2":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "PEOPLES2":
                return Schema.TYPE_INT;
            case "GETFLAG":
                return Schema.TYPE_STRING;
            case "DISPUTEDFLAG":
                return Schema.TYPE_STRING;
            case "GRPSPEC":
                return Schema.TYPE_STRING;
            case "PAYMODE":
                return Schema.TYPE_STRING;
            case "SIGNCOM":
                return Schema.TYPE_STRING;
            case "SIGNDATE":
                return Schema.TYPE_DATE;
            case "SIGNTIME":
                return Schema.TYPE_STRING;
            case "CVALIDATE":
                return Schema.TYPE_DATE;
            case "PAYINTV":
                return Schema.TYPE_INT;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "MULT":
                return Schema.TYPE_DOUBLE;
            case "PREM":
                return Schema.TYPE_DOUBLE;
            case "AMNT":
                return Schema.TYPE_DOUBLE;
            case "SUMPREM":
                return Schema.TYPE_DOUBLE;
            case "SUMPAY":
                return Schema.TYPE_DOUBLE;
            case "DIF":
                return Schema.TYPE_DOUBLE;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "STANDBYFLAG3":
                return Schema.TYPE_STRING;
            case "APPROVEFLAG":
                return Schema.TYPE_STRING;
            case "APPROVECODE":
                return Schema.TYPE_STRING;
            case "APPROVEDATE":
                return Schema.TYPE_DATE;
            case "APPROVETIME":
                return Schema.TYPE_STRING;
            case "UWOPERATOR":
                return Schema.TYPE_STRING;
            case "UWFLAG":
                return Schema.TYPE_STRING;
            case "UWDATE":
                return Schema.TYPE_DATE;
            case "UWTIME":
                return Schema.TYPE_STRING;
            case "APPFLAG":
                return Schema.TYPE_STRING;
            case "POLAPPLYDATE":
                return Schema.TYPE_DATE;
            case "CUSTOMGETPOLDATE":
                return Schema.TYPE_DATE;
            case "GETPOLDATE":
                return Schema.TYPE_DATE;
            case "GETPOLTIME":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALOPERATOR":
                return Schema.TYPE_STRING;
            case "FIRSTTRIALDATE":
                return Schema.TYPE_DATE;
            case "FIRSTTRIALTIME":
                return Schema.TYPE_STRING;
            case "CONTTYPE":
                return Schema.TYPE_STRING;
            case "ANNUITYRECEIVEDATE":
                return Schema.TYPE_DATE;
            case "CENDDATE":
                return Schema.TYPE_DATE;
            case "NATIVEPEOPLES":
                return Schema.TYPE_INT;
            case "NATIVEPREM":
                return Schema.TYPE_DOUBLE;
            case "NATIVEAMNT":
                return Schema.TYPE_DOUBLE;
            case "REDUCEFRE":
                return Schema.TYPE_STRING;
            case "OFFACC":
                return Schema.TYPE_STRING;
            case "OFFPWD":
                return Schema.TYPE_STRING;
            case "SALECHANNELS":
                return Schema.TYPE_STRING;
            case "E_APPNTID":
                return Schema.TYPE_STRING;
            case "E_APPNTDATE":
                return Schema.TYPE_DATE;
            case "E_CONTID":
                return Schema.TYPE_STRING;
            case "E_CONTDATE":
                return Schema.TYPE_DATE;
            case "ISRENEWFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_INT;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_DATE;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_INT;
            case 18:
                return Schema.TYPE_INT;
            case 19:
                return Schema.TYPE_DOUBLE;
            case 20:
                return Schema.TYPE_DOUBLE;
            case 21:
                return Schema.TYPE_DOUBLE;
            case 22:
                return Schema.TYPE_DOUBLE;
            case 23:
                return Schema.TYPE_DOUBLE;
            case 24:
                return Schema.TYPE_DOUBLE;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_DATE;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_DATE;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_DATE;
            case 39:
                return Schema.TYPE_DATE;
            case 40:
                return Schema.TYPE_DATE;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_DATE;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_DATE;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_DATE;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_DATE;
            case 53:
                return Schema.TYPE_DATE;
            case 54:
                return Schema.TYPE_INT;
            case 55:
                return Schema.TYPE_DOUBLE;
            case 56:
                return Schema.TYPE_DOUBLE;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_DATE;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_DATE;
            case 65:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
