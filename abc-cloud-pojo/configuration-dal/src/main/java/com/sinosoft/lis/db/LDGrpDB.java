/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.vschema.LDGrpSet;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LDGrpDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LDGrpDB extends LDGrpSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LDGrpDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LDGrp" );
        mflag = true;
    }

    public LDGrpDB() {
        con = null;
        db = new DBOper( "LDGrp" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LDGrpSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LDGrpSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LDGrp WHERE  1=1  AND CustomerNo = ?");
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getCustomerNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LDGrp");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LDGrp SET  CustomerNo = ? , Password = ? , GrpName = ? , BusinessType = ? , GrpNature = ? , Peoples = ? , OnWorkPeoples = ? , InsurePeoples = ? , ComDocIDType = ? , ComDocIDNo = ? , BusliceDate = ? , PostCode = ? , GrpAddress = ? , Fax = ? , UnitRegisteredAddress = ? , UnitDuration = ? , Principal = ? , PrincipalIDType = ? , PrincipalIDNo = ? , PrincipalValidate = ? , Telephone = ? , Mobile = ? , EMail = ? , AnnuityReceiveDate = ? , Managecom = ? , HeadShip = ? , GetFlag = ? , DisputedFlag = ? , BankCode = ? , BankAccNo = ? , AccName = ? , BankProivnce = ? , BankCity = ? , AccType = ? , BankLocations = ? , TaxPayerIDType = ? , TaxPayerIDNO = ? , TaxBureauAddr = ? , TaxBureauTel = ? , TaxBureauBankCode = ? , TaxBureauBankAccNo = ? , BillingType = ? , RgtMoney = ? , Asset = ? , NetProfitRate = ? , MainBussiness = ? , Corporation = ? , ComAera = ? , Phone = ? , Satrap = ? , FoundDate = ? , GrpGroupNo = ? , BlacklistFlag = ? , State = ? , Remark = ? , VIPValue = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , SubCompanyFlag = ? , SupCustoemrNo = ? , LevelCode = ? , OffWorkPeoples = ? , OtherPeoples = ? , BusinessBigType = ? , SocialInsuNo = ? , BlackListReason = ? , ComNo = ? , ComWebsite = ? , BusRegNum = ? , CorporationIDNo = ? , ComCorporationNo = ? , ContrShar = ? , HodingIDType = ? , HodingIDNo = ? , HodingValidate = ? , CorporationIDType = ? , CorporationValidate = ? WHERE  1=1  AND CustomerNo = ?");
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getCustomerNo());
            }
            if(this.getPassword() == null || this.getPassword().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getPassword());
            }
            if(this.getGrpName() == null || this.getGrpName().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGrpName());
            }
            if(this.getBusinessType() == null || this.getBusinessType().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getBusinessType());
            }
            if(this.getGrpNature() == null || this.getGrpNature().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getGrpNature());
            }
            pstmt.setInt(6, this.getPeoples());
            pstmt.setInt(7, this.getOnWorkPeoples());
            pstmt.setInt(8, this.getInsurePeoples());
            if(this.getComDocIDType() == null || this.getComDocIDType().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getComDocIDType());
            }
            if(this.getComDocIDNo() == null || this.getComDocIDNo().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getComDocIDNo());
            }
            if(this.getBusliceDate() == null || this.getBusliceDate().equals("null")) {
            	pstmt.setNull(11, 93);
            } else {
            	pstmt.setDate(11, Date.valueOf(this.getBusliceDate()));
            }
            if(this.getPostCode() == null || this.getPostCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getPostCode());
            }
            if(this.getGrpAddress() == null || this.getGrpAddress().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getGrpAddress());
            }
            if(this.getFax() == null || this.getFax().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getFax());
            }
            if(this.getUnitRegisteredAddress() == null || this.getUnitRegisteredAddress().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getUnitRegisteredAddress());
            }
            pstmt.setDouble(16, this.getUnitDuration());
            if(this.getPrincipal() == null || this.getPrincipal().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getPrincipal());
            }
            if(this.getPrincipalIDType() == null || this.getPrincipalIDType().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getPrincipalIDType());
            }
            if(this.getPrincipalIDNo() == null || this.getPrincipalIDNo().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getPrincipalIDNo());
            }
            if(this.getPrincipalValidate() == null || this.getPrincipalValidate().equals("null")) {
            	pstmt.setNull(20, 93);
            } else {
            	pstmt.setDate(20, Date.valueOf(this.getPrincipalValidate()));
            }
            if(this.getTelephone() == null || this.getTelephone().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getTelephone());
            }
            if(this.getMobile() == null || this.getMobile().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getMobile());
            }
            if(this.getEMail() == null || this.getEMail().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getEMail());
            }
            if(this.getAnnuityReceiveDate() == null || this.getAnnuityReceiveDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getAnnuityReceiveDate()));
            }
            if(this.getManagecom() == null || this.getManagecom().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getManagecom());
            }
            if(this.getHeadShip() == null || this.getHeadShip().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getHeadShip());
            }
            if(this.getGetFlag() == null || this.getGetFlag().equals("null")) {
            	pstmt.setNull(27, 1);
            } else {
            	pstmt.setString(27, this.getGetFlag());
            }
            if(this.getDisputedFlag() == null || this.getDisputedFlag().equals("null")) {
            	pstmt.setNull(28, 1);
            } else {
            	pstmt.setString(28, this.getDisputedFlag());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getBankAccNo());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getAccName());
            }
            if(this.getBankProivnce() == null || this.getBankProivnce().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getBankProivnce());
            }
            if(this.getBankCity() == null || this.getBankCity().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getBankCity());
            }
            if(this.getAccType() == null || this.getAccType().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getAccType());
            }
            if(this.getBankLocations() == null || this.getBankLocations().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getBankLocations());
            }
            if(this.getTaxPayerIDType() == null || this.getTaxPayerIDType().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getTaxPayerIDType());
            }
            if(this.getTaxPayerIDNO() == null || this.getTaxPayerIDNO().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getTaxPayerIDNO());
            }
            if(this.getTaxBureauAddr() == null || this.getTaxBureauAddr().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getTaxBureauAddr());
            }
            if(this.getTaxBureauTel() == null || this.getTaxBureauTel().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getTaxBureauTel());
            }
            if(this.getTaxBureauBankCode() == null || this.getTaxBureauBankCode().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getTaxBureauBankCode());
            }
            if(this.getTaxBureauBankAccNo() == null || this.getTaxBureauBankAccNo().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getTaxBureauBankAccNo());
            }
            if(this.getBillingType() == null || this.getBillingType().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getBillingType());
            }
            pstmt.setDouble(43, this.getRgtMoney());
            pstmt.setDouble(44, this.getAsset());
            pstmt.setDouble(45, this.getNetProfitRate());
            if(this.getMainBussiness() == null || this.getMainBussiness().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getMainBussiness());
            }
            if(this.getCorporation() == null || this.getCorporation().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getCorporation());
            }
            if(this.getComAera() == null || this.getComAera().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getComAera());
            }
            if(this.getPhone() == null || this.getPhone().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getPhone());
            }
            if(this.getSatrap() == null || this.getSatrap().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getSatrap());
            }
            if(this.getFoundDate() == null || this.getFoundDate().equals("null")) {
            	pstmt.setNull(51, 93);
            } else {
            	pstmt.setDate(51, Date.valueOf(this.getFoundDate()));
            }
            if(this.getGrpGroupNo() == null || this.getGrpGroupNo().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getGrpGroupNo());
            }
            if(this.getBlacklistFlag() == null || this.getBlacklistFlag().equals("null")) {
            	pstmt.setNull(53, 1);
            } else {
            	pstmt.setString(53, this.getBlacklistFlag());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getState());
            }
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getRemark());
            }
            if(this.getVIPValue() == null || this.getVIPValue().equals("null")) {
            	pstmt.setNull(56, 1);
            } else {
            	pstmt.setString(56, this.getVIPValue());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(57, 12);
            } else {
            	pstmt.setString(57, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(58, 93);
            } else {
            	pstmt.setDate(58, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(59, 1);
            } else {
            	pstmt.setString(59, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(60, 93);
            } else {
            	pstmt.setDate(60, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(61, 1);
            } else {
            	pstmt.setString(61, this.getModifyTime());
            }
            if(this.getSubCompanyFlag() == null || this.getSubCompanyFlag().equals("null")) {
            	pstmt.setNull(62, 1);
            } else {
            	pstmt.setString(62, this.getSubCompanyFlag());
            }
            if(this.getSupCustoemrNo() == null || this.getSupCustoemrNo().equals("null")) {
            	pstmt.setNull(63, 12);
            } else {
            	pstmt.setString(63, this.getSupCustoemrNo());
            }
            if(this.getLevelCode() == null || this.getLevelCode().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getLevelCode());
            }
            pstmt.setInt(65, this.getOffWorkPeoples());
            pstmt.setInt(66, this.getOtherPeoples());
            if(this.getBusinessBigType() == null || this.getBusinessBigType().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getBusinessBigType());
            }
            if(this.getSocialInsuNo() == null || this.getSocialInsuNo().equals("null")) {
            	pstmt.setNull(68, 12);
            } else {
            	pstmt.setString(68, this.getSocialInsuNo());
            }
            if(this.getBlackListReason() == null || this.getBlackListReason().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getBlackListReason());
            }
            if(this.getComNo() == null || this.getComNo().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getComNo());
            }
            if(this.getComWebsite() == null || this.getComWebsite().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getComWebsite());
            }
            if(this.getBusRegNum() == null || this.getBusRegNum().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getBusRegNum());
            }
            if(this.getCorporationIDNo() == null || this.getCorporationIDNo().equals("null")) {
            	pstmt.setNull(73, 12);
            } else {
            	pstmt.setString(73, this.getCorporationIDNo());
            }
            if(this.getComCorporationNo() == null || this.getComCorporationNo().equals("null")) {
            	pstmt.setNull(74, 12);
            } else {
            	pstmt.setString(74, this.getComCorporationNo());
            }
            if(this.getContrShar() == null || this.getContrShar().equals("null")) {
            	pstmt.setNull(75, 12);
            } else {
            	pstmt.setString(75, this.getContrShar());
            }
            if(this.getHodingIDType() == null || this.getHodingIDType().equals("null")) {
            	pstmt.setNull(76, 12);
            } else {
            	pstmt.setString(76, this.getHodingIDType());
            }
            if(this.getHodingIDNo() == null || this.getHodingIDNo().equals("null")) {
            	pstmt.setNull(77, 12);
            } else {
            	pstmt.setString(77, this.getHodingIDNo());
            }
            if(this.getHodingValidate() == null || this.getHodingValidate().equals("null")) {
            	pstmt.setNull(78, 93);
            } else {
            	pstmt.setDate(78, Date.valueOf(this.getHodingValidate()));
            }
            if(this.getCorporationIDType() == null || this.getCorporationIDType().equals("null")) {
            	pstmt.setNull(79, 12);
            } else {
            	pstmt.setString(79, this.getCorporationIDType());
            }
            if(this.getCorporationValidate() == null || this.getCorporationValidate().equals("null")) {
            	pstmt.setNull(80, 93);
            } else {
            	pstmt.setDate(80, Date.valueOf(this.getCorporationValidate()));
            }
            // set where condition
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(81, 12);
            } else {
            	pstmt.setString(81, this.getCustomerNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LDGrp");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LDGrp VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getCustomerNo());
            }
            if(this.getPassword() == null || this.getPassword().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getPassword());
            }
            if(this.getGrpName() == null || this.getGrpName().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getGrpName());
            }
            if(this.getBusinessType() == null || this.getBusinessType().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getBusinessType());
            }
            if(this.getGrpNature() == null || this.getGrpNature().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getGrpNature());
            }
            pstmt.setInt(6, this.getPeoples());
            pstmt.setInt(7, this.getOnWorkPeoples());
            pstmt.setInt(8, this.getInsurePeoples());
            if(this.getComDocIDType() == null || this.getComDocIDType().equals("null")) {
            	pstmt.setNull(9, 12);
            } else {
            	pstmt.setString(9, this.getComDocIDType());
            }
            if(this.getComDocIDNo() == null || this.getComDocIDNo().equals("null")) {
            	pstmt.setNull(10, 12);
            } else {
            	pstmt.setString(10, this.getComDocIDNo());
            }
            if(this.getBusliceDate() == null || this.getBusliceDate().equals("null")) {
            	pstmt.setNull(11, 93);
            } else {
            	pstmt.setDate(11, Date.valueOf(this.getBusliceDate()));
            }
            if(this.getPostCode() == null || this.getPostCode().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getPostCode());
            }
            if(this.getGrpAddress() == null || this.getGrpAddress().equals("null")) {
            	pstmt.setNull(13, 12);
            } else {
            	pstmt.setString(13, this.getGrpAddress());
            }
            if(this.getFax() == null || this.getFax().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getFax());
            }
            if(this.getUnitRegisteredAddress() == null || this.getUnitRegisteredAddress().equals("null")) {
            	pstmt.setNull(15, 12);
            } else {
            	pstmt.setString(15, this.getUnitRegisteredAddress());
            }
            pstmt.setDouble(16, this.getUnitDuration());
            if(this.getPrincipal() == null || this.getPrincipal().equals("null")) {
            	pstmt.setNull(17, 12);
            } else {
            	pstmt.setString(17, this.getPrincipal());
            }
            if(this.getPrincipalIDType() == null || this.getPrincipalIDType().equals("null")) {
            	pstmt.setNull(18, 12);
            } else {
            	pstmt.setString(18, this.getPrincipalIDType());
            }
            if(this.getPrincipalIDNo() == null || this.getPrincipalIDNo().equals("null")) {
            	pstmt.setNull(19, 12);
            } else {
            	pstmt.setString(19, this.getPrincipalIDNo());
            }
            if(this.getPrincipalValidate() == null || this.getPrincipalValidate().equals("null")) {
            	pstmt.setNull(20, 93);
            } else {
            	pstmt.setDate(20, Date.valueOf(this.getPrincipalValidate()));
            }
            if(this.getTelephone() == null || this.getTelephone().equals("null")) {
            	pstmt.setNull(21, 12);
            } else {
            	pstmt.setString(21, this.getTelephone());
            }
            if(this.getMobile() == null || this.getMobile().equals("null")) {
            	pstmt.setNull(22, 12);
            } else {
            	pstmt.setString(22, this.getMobile());
            }
            if(this.getEMail() == null || this.getEMail().equals("null")) {
            	pstmt.setNull(23, 12);
            } else {
            	pstmt.setString(23, this.getEMail());
            }
            if(this.getAnnuityReceiveDate() == null || this.getAnnuityReceiveDate().equals("null")) {
            	pstmt.setNull(24, 93);
            } else {
            	pstmt.setDate(24, Date.valueOf(this.getAnnuityReceiveDate()));
            }
            if(this.getManagecom() == null || this.getManagecom().equals("null")) {
            	pstmt.setNull(25, 12);
            } else {
            	pstmt.setString(25, this.getManagecom());
            }
            if(this.getHeadShip() == null || this.getHeadShip().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getHeadShip());
            }
            if(this.getGetFlag() == null || this.getGetFlag().equals("null")) {
            	pstmt.setNull(27, 1);
            } else {
            	pstmt.setString(27, this.getGetFlag());
            }
            if(this.getDisputedFlag() == null || this.getDisputedFlag().equals("null")) {
            	pstmt.setNull(28, 1);
            } else {
            	pstmt.setString(28, this.getDisputedFlag());
            }
            if(this.getBankCode() == null || this.getBankCode().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getBankCode());
            }
            if(this.getBankAccNo() == null || this.getBankAccNo().equals("null")) {
            	pstmt.setNull(30, 12);
            } else {
            	pstmt.setString(30, this.getBankAccNo());
            }
            if(this.getAccName() == null || this.getAccName().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getAccName());
            }
            if(this.getBankProivnce() == null || this.getBankProivnce().equals("null")) {
            	pstmt.setNull(32, 12);
            } else {
            	pstmt.setString(32, this.getBankProivnce());
            }
            if(this.getBankCity() == null || this.getBankCity().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getBankCity());
            }
            if(this.getAccType() == null || this.getAccType().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getAccType());
            }
            if(this.getBankLocations() == null || this.getBankLocations().equals("null")) {
            	pstmt.setNull(35, 12);
            } else {
            	pstmt.setString(35, this.getBankLocations());
            }
            if(this.getTaxPayerIDType() == null || this.getTaxPayerIDType().equals("null")) {
            	pstmt.setNull(36, 12);
            } else {
            	pstmt.setString(36, this.getTaxPayerIDType());
            }
            if(this.getTaxPayerIDNO() == null || this.getTaxPayerIDNO().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getTaxPayerIDNO());
            }
            if(this.getTaxBureauAddr() == null || this.getTaxBureauAddr().equals("null")) {
            	pstmt.setNull(38, 12);
            } else {
            	pstmt.setString(38, this.getTaxBureauAddr());
            }
            if(this.getTaxBureauTel() == null || this.getTaxBureauTel().equals("null")) {
            	pstmt.setNull(39, 12);
            } else {
            	pstmt.setString(39, this.getTaxBureauTel());
            }
            if(this.getTaxBureauBankCode() == null || this.getTaxBureauBankCode().equals("null")) {
            	pstmt.setNull(40, 12);
            } else {
            	pstmt.setString(40, this.getTaxBureauBankCode());
            }
            if(this.getTaxBureauBankAccNo() == null || this.getTaxBureauBankAccNo().equals("null")) {
            	pstmt.setNull(41, 12);
            } else {
            	pstmt.setString(41, this.getTaxBureauBankAccNo());
            }
            if(this.getBillingType() == null || this.getBillingType().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getBillingType());
            }
            pstmt.setDouble(43, this.getRgtMoney());
            pstmt.setDouble(44, this.getAsset());
            pstmt.setDouble(45, this.getNetProfitRate());
            if(this.getMainBussiness() == null || this.getMainBussiness().equals("null")) {
            	pstmt.setNull(46, 12);
            } else {
            	pstmt.setString(46, this.getMainBussiness());
            }
            if(this.getCorporation() == null || this.getCorporation().equals("null")) {
            	pstmt.setNull(47, 12);
            } else {
            	pstmt.setString(47, this.getCorporation());
            }
            if(this.getComAera() == null || this.getComAera().equals("null")) {
            	pstmt.setNull(48, 12);
            } else {
            	pstmt.setString(48, this.getComAera());
            }
            if(this.getPhone() == null || this.getPhone().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getPhone());
            }
            if(this.getSatrap() == null || this.getSatrap().equals("null")) {
            	pstmt.setNull(50, 12);
            } else {
            	pstmt.setString(50, this.getSatrap());
            }
            if(this.getFoundDate() == null || this.getFoundDate().equals("null")) {
            	pstmt.setNull(51, 93);
            } else {
            	pstmt.setDate(51, Date.valueOf(this.getFoundDate()));
            }
            if(this.getGrpGroupNo() == null || this.getGrpGroupNo().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getGrpGroupNo());
            }
            if(this.getBlacklistFlag() == null || this.getBlacklistFlag().equals("null")) {
            	pstmt.setNull(53, 1);
            } else {
            	pstmt.setString(53, this.getBlacklistFlag());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(54, 12);
            } else {
            	pstmt.setString(54, this.getState());
            }
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(55, 12);
            } else {
            	pstmt.setString(55, this.getRemark());
            }
            if(this.getVIPValue() == null || this.getVIPValue().equals("null")) {
            	pstmt.setNull(56, 1);
            } else {
            	pstmt.setString(56, this.getVIPValue());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(57, 12);
            } else {
            	pstmt.setString(57, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(58, 93);
            } else {
            	pstmt.setDate(58, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(59, 1);
            } else {
            	pstmt.setString(59, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(60, 93);
            } else {
            	pstmt.setDate(60, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(61, 1);
            } else {
            	pstmt.setString(61, this.getModifyTime());
            }
            if(this.getSubCompanyFlag() == null || this.getSubCompanyFlag().equals("null")) {
            	pstmt.setNull(62, 1);
            } else {
            	pstmt.setString(62, this.getSubCompanyFlag());
            }
            if(this.getSupCustoemrNo() == null || this.getSupCustoemrNo().equals("null")) {
            	pstmt.setNull(63, 12);
            } else {
            	pstmt.setString(63, this.getSupCustoemrNo());
            }
            if(this.getLevelCode() == null || this.getLevelCode().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getLevelCode());
            }
            pstmt.setInt(65, this.getOffWorkPeoples());
            pstmt.setInt(66, this.getOtherPeoples());
            if(this.getBusinessBigType() == null || this.getBusinessBigType().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getBusinessBigType());
            }
            if(this.getSocialInsuNo() == null || this.getSocialInsuNo().equals("null")) {
            	pstmt.setNull(68, 12);
            } else {
            	pstmt.setString(68, this.getSocialInsuNo());
            }
            if(this.getBlackListReason() == null || this.getBlackListReason().equals("null")) {
            	pstmt.setNull(69, 12);
            } else {
            	pstmt.setString(69, this.getBlackListReason());
            }
            if(this.getComNo() == null || this.getComNo().equals("null")) {
            	pstmt.setNull(70, 12);
            } else {
            	pstmt.setString(70, this.getComNo());
            }
            if(this.getComWebsite() == null || this.getComWebsite().equals("null")) {
            	pstmt.setNull(71, 12);
            } else {
            	pstmt.setString(71, this.getComWebsite());
            }
            if(this.getBusRegNum() == null || this.getBusRegNum().equals("null")) {
            	pstmt.setNull(72, 12);
            } else {
            	pstmt.setString(72, this.getBusRegNum());
            }
            if(this.getCorporationIDNo() == null || this.getCorporationIDNo().equals("null")) {
            	pstmt.setNull(73, 12);
            } else {
            	pstmt.setString(73, this.getCorporationIDNo());
            }
            if(this.getComCorporationNo() == null || this.getComCorporationNo().equals("null")) {
            	pstmt.setNull(74, 12);
            } else {
            	pstmt.setString(74, this.getComCorporationNo());
            }
            if(this.getContrShar() == null || this.getContrShar().equals("null")) {
            	pstmt.setNull(75, 12);
            } else {
            	pstmt.setString(75, this.getContrShar());
            }
            if(this.getHodingIDType() == null || this.getHodingIDType().equals("null")) {
            	pstmt.setNull(76, 12);
            } else {
            	pstmt.setString(76, this.getHodingIDType());
            }
            if(this.getHodingIDNo() == null || this.getHodingIDNo().equals("null")) {
            	pstmt.setNull(77, 12);
            } else {
            	pstmt.setString(77, this.getHodingIDNo());
            }
            if(this.getHodingValidate() == null || this.getHodingValidate().equals("null")) {
            	pstmt.setNull(78, 93);
            } else {
            	pstmt.setDate(78, Date.valueOf(this.getHodingValidate()));
            }
            if(this.getCorporationIDType() == null || this.getCorporationIDType().equals("null")) {
            	pstmt.setNull(79, 12);
            } else {
            	pstmt.setString(79, this.getCorporationIDType());
            }
            if(this.getCorporationValidate() == null || this.getCorporationValidate().equals("null")) {
            	pstmt.setNull(80, 93);
            } else {
            	pstmt.setDate(80, Date.valueOf(this.getCorporationValidate()));
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LDGrp WHERE  1=1  AND CustomerNo = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getCustomerNo() == null || this.getCustomerNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getCustomerNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LDGrpSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LDGrpSet aLDGrpSet = new LDGrpSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDGrp");
            LDGrpSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LDGrpSchema s1 = new LDGrpSchema();
                s1.setSchema(rs,i);
                aLDGrpSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLDGrpSet;
    }

    public LDGrpSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LDGrpSet aLDGrpSet = new LDGrpSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LDGrpSchema s1 = new LDGrpSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLDGrpSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDGrpSet;
    }

    public LDGrpSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LDGrpSet aLDGrpSet = new LDGrpSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDGrp");
            LDGrpSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LDGrpSchema s1 = new LDGrpSchema();
                s1.setSchema(rs,i);
                aLDGrpSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDGrpSet;
    }

    public LDGrpSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LDGrpSet aLDGrpSet = new LDGrpSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LDGrpSchema s1 = new LDGrpSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LDGrpDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLDGrpSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLDGrpSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LDGrp");
            LDGrpSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LDGrp " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LDGrpDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LDGrpSet
     */
    public LDGrpSet getData() {
        int tCount = 0;
        LDGrpSet tLDGrpSet = new LDGrpSet();
        LDGrpSchema tLDGrpSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLDGrpSchema = new LDGrpSchema();
            tLDGrpSchema.setSchema(mResultSet, 1);
            tLDGrpSet.add(tLDGrpSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLDGrpSchema = new LDGrpSchema();
                    tLDGrpSchema.setSchema(mResultSet, 1);
                    tLDGrpSet.add(tLDGrpSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLDGrpSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LDGrpDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LDGrpDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LDGrpDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
