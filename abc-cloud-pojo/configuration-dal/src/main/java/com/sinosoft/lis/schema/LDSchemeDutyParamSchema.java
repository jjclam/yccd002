/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDSchemeDutyParamDB;

/**
 * <p>ClassName: LDSchemeDutyParamSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-15
 */
public class LDSchemeDutyParamSchema implements Schema, Cloneable {
    // @Field
    /** 序号 */
    private String SelNO;
    /** 主险险种编码 */
    private String MainRiskCode;
    /** 主险险种版本 */
    private String MainRiskVersion;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 保险方案编码 */
    private String ContSchemeCode;
    /** 保险方案名称 */
    private String ContSchemeName;
    /** 责任编码 */
    private String DutyCode;
    /** 方案要素 */
    private String CalFactor;
    /** 方案要素类型 */
    private String CalFactorType;
    /** 方案要素值 */
    private String CalFactorValue;
    /** 备注 */
    private String Remark;
    /** 方案类别 */
    private String SchemeType;
    /** 产品组合标记 */
    private String PortfolioFlag;

    public static final int FIELDNUM = 14;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDSchemeDutyParamSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SelNO";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDSchemeDutyParamSchema cloned = (LDSchemeDutyParamSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSelNO() {
        return SelNO;
    }
    public void setSelNO(String aSelNO) {
        SelNO = aSelNO;
    }
    public String getMainRiskCode() {
        return MainRiskCode;
    }
    public void setMainRiskCode(String aMainRiskCode) {
        MainRiskCode = aMainRiskCode;
    }
    public String getMainRiskVersion() {
        return MainRiskVersion;
    }
    public void setMainRiskVersion(String aMainRiskVersion) {
        MainRiskVersion = aMainRiskVersion;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVersion() {
        return RiskVersion;
    }
    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }
    public String getContSchemeCode() {
        return ContSchemeCode;
    }
    public void setContSchemeCode(String aContSchemeCode) {
        ContSchemeCode = aContSchemeCode;
    }
    public String getContSchemeName() {
        return ContSchemeName;
    }
    public void setContSchemeName(String aContSchemeName) {
        ContSchemeName = aContSchemeName;
    }
    public String getDutyCode() {
        return DutyCode;
    }
    public void setDutyCode(String aDutyCode) {
        DutyCode = aDutyCode;
    }
    public String getCalFactor() {
        return CalFactor;
    }
    public void setCalFactor(String aCalFactor) {
        CalFactor = aCalFactor;
    }
    public String getCalFactorType() {
        return CalFactorType;
    }
    public void setCalFactorType(String aCalFactorType) {
        CalFactorType = aCalFactorType;
    }
    public String getCalFactorValue() {
        return CalFactorValue;
    }
    public void setCalFactorValue(String aCalFactorValue) {
        CalFactorValue = aCalFactorValue;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getSchemeType() {
        return SchemeType;
    }
    public void setSchemeType(String aSchemeType) {
        SchemeType = aSchemeType;
    }
    public String getPortfolioFlag() {
        return PortfolioFlag;
    }
    public void setPortfolioFlag(String aPortfolioFlag) {
        PortfolioFlag = aPortfolioFlag;
    }

    /**
    * 使用另外一个 LDSchemeDutyParamSchema 对象给 Schema 赋值
    * @param: aLDSchemeDutyParamSchema LDSchemeDutyParamSchema
    **/
    public void setSchema(LDSchemeDutyParamSchema aLDSchemeDutyParamSchema) {
        this.SelNO = aLDSchemeDutyParamSchema.getSelNO();
        this.MainRiskCode = aLDSchemeDutyParamSchema.getMainRiskCode();
        this.MainRiskVersion = aLDSchemeDutyParamSchema.getMainRiskVersion();
        this.RiskCode = aLDSchemeDutyParamSchema.getRiskCode();
        this.RiskVersion = aLDSchemeDutyParamSchema.getRiskVersion();
        this.ContSchemeCode = aLDSchemeDutyParamSchema.getContSchemeCode();
        this.ContSchemeName = aLDSchemeDutyParamSchema.getContSchemeName();
        this.DutyCode = aLDSchemeDutyParamSchema.getDutyCode();
        this.CalFactor = aLDSchemeDutyParamSchema.getCalFactor();
        this.CalFactorType = aLDSchemeDutyParamSchema.getCalFactorType();
        this.CalFactorValue = aLDSchemeDutyParamSchema.getCalFactorValue();
        this.Remark = aLDSchemeDutyParamSchema.getRemark();
        this.SchemeType = aLDSchemeDutyParamSchema.getSchemeType();
        this.PortfolioFlag = aLDSchemeDutyParamSchema.getPortfolioFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SelNO") == null )
                this.SelNO = null;
            else
                this.SelNO = rs.getString("SelNO").trim();

            if( rs.getString("MainRiskCode") == null )
                this.MainRiskCode = null;
            else
                this.MainRiskCode = rs.getString("MainRiskCode").trim();

            if( rs.getString("MainRiskVersion") == null )
                this.MainRiskVersion = null;
            else
                this.MainRiskVersion = rs.getString("MainRiskVersion").trim();

            if( rs.getString("RiskCode") == null )
                this.RiskCode = null;
            else
                this.RiskCode = rs.getString("RiskCode").trim();

            if( rs.getString("RiskVersion") == null )
                this.RiskVersion = null;
            else
                this.RiskVersion = rs.getString("RiskVersion").trim();

            if( rs.getString("ContSchemeCode") == null )
                this.ContSchemeCode = null;
            else
                this.ContSchemeCode = rs.getString("ContSchemeCode").trim();

            if( rs.getString("ContSchemeName") == null )
                this.ContSchemeName = null;
            else
                this.ContSchemeName = rs.getString("ContSchemeName").trim();

            if( rs.getString("DutyCode") == null )
                this.DutyCode = null;
            else
                this.DutyCode = rs.getString("DutyCode").trim();

            if( rs.getString("CalFactor") == null )
                this.CalFactor = null;
            else
                this.CalFactor = rs.getString("CalFactor").trim();

            if( rs.getString("CalFactorType") == null )
                this.CalFactorType = null;
            else
                this.CalFactorType = rs.getString("CalFactorType").trim();

            if( rs.getString("CalFactorValue") == null )
                this.CalFactorValue = null;
            else
                this.CalFactorValue = rs.getString("CalFactorValue").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("SchemeType") == null )
                this.SchemeType = null;
            else
                this.SchemeType = rs.getString("SchemeType").trim();

            if( rs.getString("PortfolioFlag") == null )
                this.PortfolioFlag = null;
            else
                this.PortfolioFlag = rs.getString("PortfolioFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSchemeDutyParamSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDSchemeDutyParamSchema getSchema() {
        LDSchemeDutyParamSchema aLDSchemeDutyParamSchema = new LDSchemeDutyParamSchema();
        aLDSchemeDutyParamSchema.setSchema(this);
        return aLDSchemeDutyParamSchema;
    }

    public LDSchemeDutyParamDB getDB() {
        LDSchemeDutyParamDB aDBOper = new LDSchemeDutyParamDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSchemeDutyParam描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SelNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainRiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainRiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContSchemeCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContSchemeName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalFactor)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalFactorType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalFactorValue)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SchemeType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PortfolioFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSchemeDutyParam>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SelNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            MainRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            MainRiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            ContSchemeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ContSchemeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            CalFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            CalFactorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            CalFactorValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            SchemeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            PortfolioFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSchemeDutyParamSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SelNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelNO));
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskCode));
        }
        if (FCode.equalsIgnoreCase("MainRiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskVersion));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equalsIgnoreCase("ContSchemeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContSchemeCode));
        }
        if (FCode.equalsIgnoreCase("ContSchemeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContSchemeName));
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
        }
        if (FCode.equalsIgnoreCase("CalFactor")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactor));
        }
        if (FCode.equalsIgnoreCase("CalFactorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactorType));
        }
        if (FCode.equalsIgnoreCase("CalFactorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFactorValue));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("SchemeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeType));
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PortfolioFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SelNO);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(MainRiskCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(MainRiskVersion);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContSchemeCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ContSchemeName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(CalFactor);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(CalFactorType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(CalFactorValue);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(SchemeType);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(PortfolioFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SelNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SelNO = FValue.trim();
            }
            else
                SelNO = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskCode = FValue.trim();
            }
            else
                MainRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskVersion = FValue.trim();
            }
            else
                MainRiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
                RiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("ContSchemeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContSchemeCode = FValue.trim();
            }
            else
                ContSchemeCode = null;
        }
        if (FCode.equalsIgnoreCase("ContSchemeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContSchemeName = FValue.trim();
            }
            else
                ContSchemeName = null;
        }
        if (FCode.equalsIgnoreCase("DutyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
                DutyCode = null;
        }
        if (FCode.equalsIgnoreCase("CalFactor")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFactor = FValue.trim();
            }
            else
                CalFactor = null;
        }
        if (FCode.equalsIgnoreCase("CalFactorType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFactorType = FValue.trim();
            }
            else
                CalFactorType = null;
        }
        if (FCode.equalsIgnoreCase("CalFactorValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalFactorValue = FValue.trim();
            }
            else
                CalFactorValue = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("SchemeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeType = FValue.trim();
            }
            else
                SchemeType = null;
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PortfolioFlag = FValue.trim();
            }
            else
                PortfolioFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDSchemeDutyParamSchema other = (LDSchemeDutyParamSchema)otherObject;
        return
            SelNO.equals(other.getSelNO())
            && MainRiskCode.equals(other.getMainRiskCode())
            && MainRiskVersion.equals(other.getMainRiskVersion())
            && RiskCode.equals(other.getRiskCode())
            && RiskVersion.equals(other.getRiskVersion())
            && ContSchemeCode.equals(other.getContSchemeCode())
            && ContSchemeName.equals(other.getContSchemeName())
            && DutyCode.equals(other.getDutyCode())
            && CalFactor.equals(other.getCalFactor())
            && CalFactorType.equals(other.getCalFactorType())
            && CalFactorValue.equals(other.getCalFactorValue())
            && Remark.equals(other.getRemark())
            && SchemeType.equals(other.getSchemeType())
            && PortfolioFlag.equals(other.getPortfolioFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SelNO") ) {
            return 0;
        }
        if( strFieldName.equals("MainRiskCode") ) {
            return 1;
        }
        if( strFieldName.equals("MainRiskVersion") ) {
            return 2;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 3;
        }
        if( strFieldName.equals("RiskVersion") ) {
            return 4;
        }
        if( strFieldName.equals("ContSchemeCode") ) {
            return 5;
        }
        if( strFieldName.equals("ContSchemeName") ) {
            return 6;
        }
        if( strFieldName.equals("DutyCode") ) {
            return 7;
        }
        if( strFieldName.equals("CalFactor") ) {
            return 8;
        }
        if( strFieldName.equals("CalFactorType") ) {
            return 9;
        }
        if( strFieldName.equals("CalFactorValue") ) {
            return 10;
        }
        if( strFieldName.equals("Remark") ) {
            return 11;
        }
        if( strFieldName.equals("SchemeType") ) {
            return 12;
        }
        if( strFieldName.equals("PortfolioFlag") ) {
            return 13;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SelNO";
                break;
            case 1:
                strFieldName = "MainRiskCode";
                break;
            case 2:
                strFieldName = "MainRiskVersion";
                break;
            case 3:
                strFieldName = "RiskCode";
                break;
            case 4:
                strFieldName = "RiskVersion";
                break;
            case 5:
                strFieldName = "ContSchemeCode";
                break;
            case 6:
                strFieldName = "ContSchemeName";
                break;
            case 7:
                strFieldName = "DutyCode";
                break;
            case 8:
                strFieldName = "CalFactor";
                break;
            case 9:
                strFieldName = "CalFactorType";
                break;
            case 10:
                strFieldName = "CalFactorValue";
                break;
            case 11:
                strFieldName = "Remark";
                break;
            case 12:
                strFieldName = "SchemeType";
                break;
            case 13:
                strFieldName = "PortfolioFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SELNO":
                return Schema.TYPE_STRING;
            case "MAINRISKCODE":
                return Schema.TYPE_STRING;
            case "MAINRISKVERSION":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVERSION":
                return Schema.TYPE_STRING;
            case "CONTSCHEMECODE":
                return Schema.TYPE_STRING;
            case "CONTSCHEMENAME":
                return Schema.TYPE_STRING;
            case "DUTYCODE":
                return Schema.TYPE_STRING;
            case "CALFACTOR":
                return Schema.TYPE_STRING;
            case "CALFACTORTYPE":
                return Schema.TYPE_STRING;
            case "CALFACTORVALUE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "SCHEMETYPE":
                return Schema.TYPE_STRING;
            case "PORTFOLIOFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
