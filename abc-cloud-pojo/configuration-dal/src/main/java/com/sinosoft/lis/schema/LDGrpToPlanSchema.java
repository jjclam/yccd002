/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import com.sinosoft.lis.db.LDGrpToPlanDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * <p>ClassName: LDGrpToPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-04-29
 */
public class LDGrpToPlanSchema implements Schema, Cloneable {
    // @Field
    /** 客户号码 */
    private String CustomerNo;
    /** 方案编码 */
    private String PlanCode;
    /** 方案名称 */
    private String PlanName;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 开办日期 */
    private Date StartDate;
    /** 停办日期 */
    private Date EndDate;
    /** 销售保额上限 */
    private double MAXAmnt;
    /** 销售保额下限 */
    private double MINAmnt;
    /** 销售份数上限 */
    private int MAXMult;
    /** 销售份数下限 */
    private int MINMult;
    /** 销售保费上限 */
    private double MAXPrem;
    /** 销售保费下限 */
    private double MINPrem;
    /** 手续费率 */
    private double FeeRate;

    public static final int FIELDNUM = 17;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDGrpToPlanSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "CustomerNo";
        pk[1] = "PlanCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDGrpToPlanSchema cloned = (LDGrpToPlanSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getPlanCode() {
        return PlanCode;
    }
    public void setPlanCode(String aPlanCode) {
        PlanCode = aPlanCode;
    }
    public String getPlanName() {
        return PlanName;
    }
    public void setPlanName(String aPlanName) {
        PlanName = aPlanName;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStartDate() {
        if(StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }
    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }
    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else
            StartDate = null;
    }

    public String getEndDate() {
        if(EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }
    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }
    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else
            EndDate = null;
    }

    public double getMAXAmnt() {
        return MAXAmnt;
    }
    public void setMAXAmnt(double aMAXAmnt) {
        MAXAmnt = aMAXAmnt;
    }
    public void setMAXAmnt(String aMAXAmnt) {
        if (aMAXAmnt != null && !aMAXAmnt.equals("")) {
            Double tDouble = new Double(aMAXAmnt);
            double d = tDouble.doubleValue();
            MAXAmnt = d;
        }
    }

    public double getMINAmnt() {
        return MINAmnt;
    }
    public void setMINAmnt(double aMINAmnt) {
        MINAmnt = aMINAmnt;
    }
    public void setMINAmnt(String aMINAmnt) {
        if (aMINAmnt != null && !aMINAmnt.equals("")) {
            Double tDouble = new Double(aMINAmnt);
            double d = tDouble.doubleValue();
            MINAmnt = d;
        }
    }

    public int getMAXMult() {
        return MAXMult;
    }
    public void setMAXMult(int aMAXMult) {
        MAXMult = aMAXMult;
    }
    public void setMAXMult(String aMAXMult) {
        if (aMAXMult != null && !aMAXMult.equals("")) {
            Integer tInteger = new Integer(aMAXMult);
            int i = tInteger.intValue();
            MAXMult = i;
        }
    }

    public int getMINMult() {
        return MINMult;
    }
    public void setMINMult(int aMINMult) {
        MINMult = aMINMult;
    }
    public void setMINMult(String aMINMult) {
        if (aMINMult != null && !aMINMult.equals("")) {
            Integer tInteger = new Integer(aMINMult);
            int i = tInteger.intValue();
            MINMult = i;
        }
    }

    public double getMAXPrem() {
        return MAXPrem;
    }
    public void setMAXPrem(double aMAXPrem) {
        MAXPrem = aMAXPrem;
    }
    public void setMAXPrem(String aMAXPrem) {
        if (aMAXPrem != null && !aMAXPrem.equals("")) {
            Double tDouble = new Double(aMAXPrem);
            double d = tDouble.doubleValue();
            MAXPrem = d;
        }
    }

    public double getMINPrem() {
        return MINPrem;
    }
    public void setMINPrem(double aMINPrem) {
        MINPrem = aMINPrem;
    }
    public void setMINPrem(String aMINPrem) {
        if (aMINPrem != null && !aMINPrem.equals("")) {
            Double tDouble = new Double(aMINPrem);
            double d = tDouble.doubleValue();
            MINPrem = d;
        }
    }

    public double getFeeRate() {
        return FeeRate;
    }
    public void setFeeRate(double aFeeRate) {
        FeeRate = aFeeRate;
    }
    public void setFeeRate(String aFeeRate) {
        if (aFeeRate != null && !aFeeRate.equals("")) {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = d;
        }
    }


    /**
     * 使用另外一个 LDGrpToPlanSchema 对象给 Schema 赋值
     * @param: aLDGrpToPlanSchema LDGrpToPlanSchema
     **/
    public void setSchema(LDGrpToPlanSchema aLDGrpToPlanSchema) {
        this.CustomerNo = aLDGrpToPlanSchema.getCustomerNo();
        this.PlanCode = aLDGrpToPlanSchema.getPlanCode();
        this.PlanName = aLDGrpToPlanSchema.getPlanName();
        this.Operator = aLDGrpToPlanSchema.getOperator();
        this.MakeDate = fDate.getDate( aLDGrpToPlanSchema.getMakeDate());
        this.MakeTime = aLDGrpToPlanSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLDGrpToPlanSchema.getModifyDate());
        this.ModifyTime = aLDGrpToPlanSchema.getModifyTime();
        this.StartDate = fDate.getDate( aLDGrpToPlanSchema.getStartDate());
        this.EndDate = fDate.getDate( aLDGrpToPlanSchema.getEndDate());
        this.MAXAmnt = aLDGrpToPlanSchema.getMAXAmnt();
        this.MINAmnt = aLDGrpToPlanSchema.getMINAmnt();
        this.MAXMult = aLDGrpToPlanSchema.getMAXMult();
        this.MINMult = aLDGrpToPlanSchema.getMINMult();
        this.MAXPrem = aLDGrpToPlanSchema.getMAXPrem();
        this.MINPrem = aLDGrpToPlanSchema.getMINPrem();
        this.FeeRate = aLDGrpToPlanSchema.getFeeRate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("PlanCode") == null )
                this.PlanCode = null;
            else
                this.PlanCode = rs.getString("PlanCode").trim();

            if( rs.getString("PlanName") == null )
                this.PlanName = null;
            else
                this.PlanName = rs.getString("PlanName").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            this.MAXAmnt = rs.getDouble("MAXAmnt");
            this.MINAmnt = rs.getDouble("MINAmnt");
            this.MAXMult = rs.getInt("MAXMult");
            this.MINMult = rs.getInt("MINMult");
            this.MAXPrem = rs.getDouble("MAXPrem");
            this.MINPrem = rs.getDouble("MINPrem");
            this.FeeRate = rs.getDouble("FeeRate");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToPlanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDGrpToPlanSchema getSchema() {
        LDGrpToPlanSchema aLDGrpToPlanSchema = new LDGrpToPlanSchema();
        aLDGrpToPlanSchema.setSchema(this);
        return aLDGrpToPlanSchema;
    }

    public LDGrpToPlanDB getDB() {
        LDGrpToPlanDB aDBOper = new LDGrpToPlanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDGrpToPlan描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PlanName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MAXAmnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MINAmnt));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MAXMult));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MINMult));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MAXPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MINPrem));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FeeRate));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDGrpToPlan>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            PlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            PlanName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
            MAXAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
            MINAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
            MAXMult = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).intValue();
            MINMult = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).intValue();
            MAXPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
            MINPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
            FeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpToPlanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equalsIgnoreCase("PlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanName));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
        }
        if (FCode.equalsIgnoreCase("MAXAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXAmnt));
        }
        if (FCode.equalsIgnoreCase("MINAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINAmnt));
        }
        if (FCode.equalsIgnoreCase("MAXMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXMult));
        }
        if (FCode.equalsIgnoreCase("MINMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINMult));
        }
        if (FCode.equalsIgnoreCase("MAXPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXPrem));
        }
        if (FCode.equalsIgnoreCase("MINPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINPrem));
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PlanCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PlanName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
                break;
            case 10:
                strFieldValue = String.valueOf(MAXAmnt);
                break;
            case 11:
                strFieldValue = String.valueOf(MINAmnt);
                break;
            case 12:
                strFieldValue = String.valueOf(MAXMult);
                break;
            case 13:
                strFieldValue = String.valueOf(MINMult);
                break;
            case 14:
                strFieldValue = String.valueOf(MAXPrem);
                break;
            case 15:
                strFieldValue = String.valueOf(MINPrem);
                break;
            case 16:
                strFieldValue = String.valueOf(FeeRate);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
                PlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PlanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanName = FValue.trim();
            }
            else
                PlanName = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate( FValue );
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if(FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate( FValue );
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("MAXAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MAXAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("MINAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MINAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("MAXMult")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MAXMult = i;
            }
        }
        if (FCode.equalsIgnoreCase("MINMult")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MINMult = i;
            }
        }
        if (FCode.equalsIgnoreCase("MAXPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MAXPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("MINPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MINPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDGrpToPlanSchema other = (LDGrpToPlanSchema)otherObject;
        return
                CustomerNo.equals(other.getCustomerNo())
                        && PlanCode.equals(other.getPlanCode())
                        && PlanName.equals(other.getPlanName())
                        && Operator.equals(other.getOperator())
                        && fDate.getString(MakeDate).equals(other.getMakeDate())
                        && MakeTime.equals(other.getMakeTime())
                        && fDate.getString(ModifyDate).equals(other.getModifyDate())
                        && ModifyTime.equals(other.getModifyTime())
                        && fDate.getString(StartDate).equals(other.getStartDate())
                        && fDate.getString(EndDate).equals(other.getEndDate())
                        && MAXAmnt == other.getMAXAmnt()
                        && MINAmnt == other.getMINAmnt()
                        && MAXMult == other.getMAXMult()
                        && MINMult == other.getMINMult()
                        && MAXPrem == other.getMAXPrem()
                        && MINPrem == other.getMINPrem()
                        && FeeRate == other.getFeeRate();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CustomerNo") ) {
            return 0;
        }
        if( strFieldName.equals("PlanCode") ) {
            return 1;
        }
        if( strFieldName.equals("PlanName") ) {
            return 2;
        }
        if( strFieldName.equals("Operator") ) {
            return 3;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 4;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 5;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 6;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 7;
        }
        if( strFieldName.equals("StartDate") ) {
            return 8;
        }
        if( strFieldName.equals("EndDate") ) {
            return 9;
        }
        if( strFieldName.equals("MAXAmnt") ) {
            return 10;
        }
        if( strFieldName.equals("MINAmnt") ) {
            return 11;
        }
        if( strFieldName.equals("MAXMult") ) {
            return 12;
        }
        if( strFieldName.equals("MINMult") ) {
            return 13;
        }
        if( strFieldName.equals("MAXPrem") ) {
            return 14;
        }
        if( strFieldName.equals("MINPrem") ) {
            return 15;
        }
        if( strFieldName.equals("FeeRate") ) {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CustomerNo";
                break;
            case 1:
                strFieldName = "PlanCode";
                break;
            case 2:
                strFieldName = "PlanName";
                break;
            case 3:
                strFieldName = "Operator";
                break;
            case 4:
                strFieldName = "MakeDate";
                break;
            case 5:
                strFieldName = "MakeTime";
                break;
            case 6:
                strFieldName = "ModifyDate";
                break;
            case 7:
                strFieldName = "ModifyTime";
                break;
            case 8:
                strFieldName = "StartDate";
                break;
            case 9:
                strFieldName = "EndDate";
                break;
            case 10:
                strFieldName = "MAXAmnt";
                break;
            case 11:
                strFieldName = "MINAmnt";
                break;
            case 12:
                strFieldName = "MAXMult";
                break;
            case 13:
                strFieldName = "MINMult";
                break;
            case 14:
                strFieldName = "MAXPrem";
                break;
            case 15:
                strFieldName = "MINPrem";
                break;
            case 16:
                strFieldName = "FeeRate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "PLANCODE":
                return Schema.TYPE_STRING;
            case "PLANNAME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_DATE;
            case "ENDDATE":
                return Schema.TYPE_DATE;
            case "MAXAMNT":
                return Schema.TYPE_DOUBLE;
            case "MINAMNT":
                return Schema.TYPE_DOUBLE;
            case "MAXMULT":
                return Schema.TYPE_INT;
            case "MINMULT":
                return Schema.TYPE_INT;
            case "MAXPREM":
                return Schema.TYPE_DOUBLE;
            case "MINPREM":
                return Schema.TYPE_DOUBLE;
            case "FEERATE":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_DATE;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_DATE;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_DATE;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_INT;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
