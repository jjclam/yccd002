/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LWTracePojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LWTracePojo implements  Pojo,Serializable {
    // @Field
    /** 流水号 */
    private String SerialNo; 
    /** 业务号 */
    private String OtherNo; 
    /** 活动id */
    private String ActivityID; 
    /** 活动状态 */
    private String ActivityFlag; 
    /** 操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 完成日期 */
    private String  EndDate;
    /** 完成时间 */
    private String EndTime; 
    /** 说明 */
    private String Rmack; 


    public static final int FIELDNUM = 12;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getActivityID() {
        return ActivityID;
    }
    public void setActivityID(String aActivityID) {
        ActivityID = aActivityID;
    }
    public String getActivityFlag() {
        return ActivityFlag;
    }
    public void setActivityFlag(String aActivityFlag) {
        ActivityFlag = aActivityFlag;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getEndTime() {
        return EndTime;
    }
    public void setEndTime(String aEndTime) {
        EndTime = aEndTime;
    }
    public String getRmack() {
        return Rmack;
    }
    public void setRmack(String aRmack) {
        Rmack = aRmack;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 1;
        }
        if( strFieldName.equals("ActivityID") ) {
            return 2;
        }
        if( strFieldName.equals("ActivityFlag") ) {
            return 3;
        }
        if( strFieldName.equals("Operator") ) {
            return 4;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 5;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 6;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 7;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 8;
        }
        if( strFieldName.equals("EndDate") ) {
            return 9;
        }
        if( strFieldName.equals("EndTime") ) {
            return 10;
        }
        if( strFieldName.equals("Rmack") ) {
            return 11;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "OtherNo";
                break;
            case 2:
                strFieldName = "ActivityID";
                break;
            case 3:
                strFieldName = "ActivityFlag";
                break;
            case 4:
                strFieldName = "Operator";
                break;
            case 5:
                strFieldName = "MakeDate";
                break;
            case 6:
                strFieldName = "MakeTime";
                break;
            case 7:
                strFieldName = "ModifyDate";
                break;
            case 8:
                strFieldName = "ModifyTime";
                break;
            case 9:
                strFieldName = "EndDate";
                break;
            case 10:
                strFieldName = "EndTime";
                break;
            case 11:
                strFieldName = "Rmack";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "ACTIVITYID":
                return Schema.TYPE_STRING;
            case "ACTIVITYFLAG":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "ENDTIME":
                return Schema.TYPE_STRING;
            case "RMACK":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("ActivityID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActivityID));
        }
        if (FCode.equalsIgnoreCase("ActivityFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActivityFlag));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("EndTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndTime));
        }
        if (FCode.equalsIgnoreCase("Rmack")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Rmack));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = String.valueOf(OtherNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ActivityID);
                break;
            case 3:
                strFieldValue = String.valueOf(ActivityFlag);
                break;
            case 4:
                strFieldValue = String.valueOf(Operator);
                break;
            case 5:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 6:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 7:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 8:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 9:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 10:
                strFieldValue = String.valueOf(EndTime);
                break;
            case 11:
                strFieldValue = String.valueOf(Rmack);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("ActivityID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActivityID = FValue.trim();
            }
            else
                ActivityID = null;
        }
        if (FCode.equalsIgnoreCase("ActivityFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                ActivityFlag = FValue.trim();
            }
            else
                ActivityFlag = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("EndTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndTime = FValue.trim();
            }
            else
                EndTime = null;
        }
        if (FCode.equalsIgnoreCase("Rmack")) {
            if( FValue != null && !FValue.equals(""))
            {
                Rmack = FValue.trim();
            }
            else
                Rmack = null;
        }
        return true;
    }


    public String toString() {
    return "LWTracePojo [" +
            "SerialNo="+SerialNo +
            ", OtherNo="+OtherNo +
            ", ActivityID="+ActivityID +
            ", ActivityFlag="+ActivityFlag +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", EndDate="+EndDate +
            ", EndTime="+EndTime +
            ", Rmack="+Rmack +"]";
    }
}
