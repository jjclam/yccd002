/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LDSchemeRiskPojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-15
 */
public class LDSchemeRiskPojo implements  Pojo,Serializable {
    // @Field
    /** 序号 */
    private String SelNO; 
    /** 主险险种代码 */
    private String MainRiskCode; 
    /** 主险险种版本 */
    private String MainRiskVersion; 
    /** 险种编码 */
    private String RiskCode; 
    /** 险种版本 */
    private String RiskVersion; 
    /** 保险方案编码 */
    private String ContSchemeCode; 
    /** 保险方案名称 */
    private String ContSchemeName; 
    /** 备注 */
    private String Remark; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 方案类别 */
    private String SchemeType; 
    /** 产品组合标记 */
    private String PortfolioFlag; 


    public static final int FIELDNUM = 15;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSelNO() {
        return SelNO;
    }
    public void setSelNO(String aSelNO) {
        SelNO = aSelNO;
    }
    public String getMainRiskCode() {
        return MainRiskCode;
    }
    public void setMainRiskCode(String aMainRiskCode) {
        MainRiskCode = aMainRiskCode;
    }
    public String getMainRiskVersion() {
        return MainRiskVersion;
    }
    public void setMainRiskVersion(String aMainRiskVersion) {
        MainRiskVersion = aMainRiskVersion;
    }
    public String getRiskCode() {
        return RiskCode;
    }
    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }
    public String getRiskVersion() {
        return RiskVersion;
    }
    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }
    public String getContSchemeCode() {
        return ContSchemeCode;
    }
    public void setContSchemeCode(String aContSchemeCode) {
        ContSchemeCode = aContSchemeCode;
    }
    public String getContSchemeName() {
        return ContSchemeName;
    }
    public void setContSchemeName(String aContSchemeName) {
        ContSchemeName = aContSchemeName;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getSchemeType() {
        return SchemeType;
    }
    public void setSchemeType(String aSchemeType) {
        SchemeType = aSchemeType;
    }
    public String getPortfolioFlag() {
        return PortfolioFlag;
    }
    public void setPortfolioFlag(String aPortfolioFlag) {
        PortfolioFlag = aPortfolioFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SelNO") ) {
            return 0;
        }
        if( strFieldName.equals("MainRiskCode") ) {
            return 1;
        }
        if( strFieldName.equals("MainRiskVersion") ) {
            return 2;
        }
        if( strFieldName.equals("RiskCode") ) {
            return 3;
        }
        if( strFieldName.equals("RiskVersion") ) {
            return 4;
        }
        if( strFieldName.equals("ContSchemeCode") ) {
            return 5;
        }
        if( strFieldName.equals("ContSchemeName") ) {
            return 6;
        }
        if( strFieldName.equals("Remark") ) {
            return 7;
        }
        if( strFieldName.equals("Operator") ) {
            return 8;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 9;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 11;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 12;
        }
        if( strFieldName.equals("SchemeType") ) {
            return 13;
        }
        if( strFieldName.equals("PortfolioFlag") ) {
            return 14;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SelNO";
                break;
            case 1:
                strFieldName = "MainRiskCode";
                break;
            case 2:
                strFieldName = "MainRiskVersion";
                break;
            case 3:
                strFieldName = "RiskCode";
                break;
            case 4:
                strFieldName = "RiskVersion";
                break;
            case 5:
                strFieldName = "ContSchemeCode";
                break;
            case 6:
                strFieldName = "ContSchemeName";
                break;
            case 7:
                strFieldName = "Remark";
                break;
            case 8:
                strFieldName = "Operator";
                break;
            case 9:
                strFieldName = "MakeDate";
                break;
            case 10:
                strFieldName = "MakeTime";
                break;
            case 11:
                strFieldName = "ModifyDate";
                break;
            case 12:
                strFieldName = "ModifyTime";
                break;
            case 13:
                strFieldName = "SchemeType";
                break;
            case 14:
                strFieldName = "PortfolioFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SELNO":
                return Schema.TYPE_STRING;
            case "MAINRISKCODE":
                return Schema.TYPE_STRING;
            case "MAINRISKVERSION":
                return Schema.TYPE_STRING;
            case "RISKCODE":
                return Schema.TYPE_STRING;
            case "RISKVERSION":
                return Schema.TYPE_STRING;
            case "CONTSCHEMECODE":
                return Schema.TYPE_STRING;
            case "CONTSCHEMENAME":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "SCHEMETYPE":
                return Schema.TYPE_STRING;
            case "PORTFOLIOFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SelNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelNO));
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskCode));
        }
        if (FCode.equalsIgnoreCase("MainRiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskVersion));
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equalsIgnoreCase("ContSchemeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContSchemeCode));
        }
        if (FCode.equalsIgnoreCase("ContSchemeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContSchemeName));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("SchemeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeType));
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PortfolioFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SelNO);
                break;
            case 1:
                strFieldValue = String.valueOf(MainRiskCode);
                break;
            case 2:
                strFieldValue = String.valueOf(MainRiskVersion);
                break;
            case 3:
                strFieldValue = String.valueOf(RiskCode);
                break;
            case 4:
                strFieldValue = String.valueOf(RiskVersion);
                break;
            case 5:
                strFieldValue = String.valueOf(ContSchemeCode);
                break;
            case 6:
                strFieldValue = String.valueOf(ContSchemeName);
                break;
            case 7:
                strFieldValue = String.valueOf(Remark);
                break;
            case 8:
                strFieldValue = String.valueOf(Operator);
                break;
            case 9:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 10:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 12:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 13:
                strFieldValue = String.valueOf(SchemeType);
                break;
            case 14:
                strFieldValue = String.valueOf(PortfolioFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SelNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SelNO = FValue.trim();
            }
            else
                SelNO = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskCode = FValue.trim();
            }
            else
                MainRiskCode = null;
        }
        if (FCode.equalsIgnoreCase("MainRiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainRiskVersion = FValue.trim();
            }
            else
                MainRiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
                RiskCode = null;
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if( FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
                RiskVersion = null;
        }
        if (FCode.equalsIgnoreCase("ContSchemeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContSchemeCode = FValue.trim();
            }
            else
                ContSchemeCode = null;
        }
        if (FCode.equalsIgnoreCase("ContSchemeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContSchemeName = FValue.trim();
            }
            else
                ContSchemeName = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("SchemeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeType = FValue.trim();
            }
            else
                SchemeType = null;
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PortfolioFlag = FValue.trim();
            }
            else
                PortfolioFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LDSchemeRiskPojo [" +
            "SelNO="+SelNO +
            ", MainRiskCode="+MainRiskCode +
            ", MainRiskVersion="+MainRiskVersion +
            ", RiskCode="+RiskCode +
            ", RiskVersion="+RiskVersion +
            ", ContSchemeCode="+ContSchemeCode +
            ", ContSchemeName="+ContSchemeName +
            ", Remark="+Remark +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", SchemeType="+SchemeType +
            ", PortfolioFlag="+PortfolioFlag +"]";
    }
}
