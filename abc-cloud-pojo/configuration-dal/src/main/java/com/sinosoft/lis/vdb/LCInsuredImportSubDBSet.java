/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LCInsuredImportSubSchema;
import com.sinosoft.lis.vschema.LCInsuredImportSubSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LCInsuredImportSubDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LCInsuredImportSubDBSet extends LCInsuredImportSubSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LCInsuredImportSubDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCInsuredImportSub");
        mflag = true;
    }

    public LCInsuredImportSubDBSet() {
        db = new DBOper( "LCInsuredImportSub" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInsuredImportSubDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCInsuredImportSub WHERE  1=1  AND SerialNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInsuredImportSubDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCInsuredImportSub SET  SerialNo = ? , OtherNoType = ? , OtherNo = ? , AppntNo = ? , CheckFlag = ? , PrtNo = ? , GrpContNo = ? , BatchNo = ? , ID = ? , ContNo = ? , InsuredNo = ? , MainInsuredNo = ? , CustomerState = ? , MainInsuredName = ? , MainInsuredIDNo = ? , InsuredName = ? , EmployeeRelation = ? , InsuredSex = ? , InsuredBirthday = ? , InsuredIDType = ? , InsuredIDNo = ? , NativePlace = ? , RgtAddress = ? , Marriage = ? , Nationality = ? , ContPlan = ? , OccupationType = ? , OccupationCode = ? , PluralityType = ? , BankCode1 = ? , AccName1 = ? , BankAccNo1 = ? , JoinCompanyDate = ? , Salary = ? , PolTypeFlag = ? , InsuredPeoples = ? , EMail = ? , Mobile = ? , CertifyCode = ? , StartCode = ? , EndCode = ? , CalStandbyFlag1 = ? , CalStandbyFlag2 = ? , RelationToRisk = ? , ContCValiDate = ? , GrpNo = ? , RelationToAppnt = ? , SocialInsuFlag = ? , RelationToMainInsured = ? , HealthServiceName = ? , HealthInsFlagName = ? , GrpAlias = ? , WhetherToRnew = ? , BankCode = ? , BankProvince = ? , BankCity = ? , BankLocations = ? , AccName = ? , BankAccNo = ? , AccType = ? , BeInsuredNo = ? , IDExpDate = ? , TaxpayerIDType = ? , CompanyNo = ? , CompanyName = ? , Flag = ? , ErrorInfo = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModIfyTime = ? , E_PolicyID = ? WHERE  1=1  AND SerialNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
            if(this.get(i).getOtherNoType() == null || this.get(i).getOtherNoType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getOtherNoType());
            }
            if(this.get(i).getOtherNo() == null || this.get(i).getOtherNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getOtherNo());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAppntNo());
            }
            if(this.get(i).getCheckFlag() == null || this.get(i).getCheckFlag().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getCheckFlag());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getPrtNo());
            }
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getGrpContNo());
            }
            if(this.get(i).getBatchNo() == null || this.get(i).getBatchNo().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getBatchNo());
            }
            if(this.get(i).getID() == null || this.get(i).getID().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getID());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getContNo());
            }
            if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getInsuredNo());
            }
            if(this.get(i).getMainInsuredNo() == null || this.get(i).getMainInsuredNo().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getMainInsuredNo());
            }
            if(this.get(i).getCustomerState() == null || this.get(i).getCustomerState().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getCustomerState());
            }
            if(this.get(i).getMainInsuredName() == null || this.get(i).getMainInsuredName().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getMainInsuredName());
            }
            if(this.get(i).getMainInsuredIDNo() == null || this.get(i).getMainInsuredIDNo().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getMainInsuredIDNo());
            }
            if(this.get(i).getInsuredName() == null || this.get(i).getInsuredName().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getInsuredName());
            }
            if(this.get(i).getEmployeeRelation() == null || this.get(i).getEmployeeRelation().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getEmployeeRelation());
            }
            if(this.get(i).getInsuredSex() == null || this.get(i).getInsuredSex().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getInsuredSex());
            }
            if(this.get(i).getInsuredBirthday() == null || this.get(i).getInsuredBirthday().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getInsuredBirthday());
            }
            if(this.get(i).getInsuredIDType() == null || this.get(i).getInsuredIDType().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getInsuredIDType());
            }
            if(this.get(i).getInsuredIDNo() == null || this.get(i).getInsuredIDNo().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getInsuredIDNo());
            }
            if(this.get(i).getNativePlace() == null || this.get(i).getNativePlace().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getNativePlace());
            }
            if(this.get(i).getRgtAddress() == null || this.get(i).getRgtAddress().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getRgtAddress());
            }
            if(this.get(i).getMarriage() == null || this.get(i).getMarriage().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getMarriage());
            }
            if(this.get(i).getNationality() == null || this.get(i).getNationality().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getNationality());
            }
            if(this.get(i).getContPlan() == null || this.get(i).getContPlan().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getContPlan());
            }
            if(this.get(i).getOccupationType() == null || this.get(i).getOccupationType().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getOccupationType());
            }
            if(this.get(i).getOccupationCode() == null || this.get(i).getOccupationCode().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getOccupationCode());
            }
            if(this.get(i).getPluralityType() == null || this.get(i).getPluralityType().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getPluralityType());
            }
            if(this.get(i).getBankCode1() == null || this.get(i).getBankCode1().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getBankCode1());
            }
            if(this.get(i).getAccName1() == null || this.get(i).getAccName1().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getAccName1());
            }
            if(this.get(i).getBankAccNo1() == null || this.get(i).getBankAccNo1().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getBankAccNo1());
            }
            if(this.get(i).getJoinCompanyDate() == null || this.get(i).getJoinCompanyDate().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getJoinCompanyDate());
            }
            if(this.get(i).getSalary() == null || this.get(i).getSalary().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getSalary());
            }
            if(this.get(i).getPolTypeFlag() == null || this.get(i).getPolTypeFlag().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getPolTypeFlag());
            }
            if(this.get(i).getInsuredPeoples() == null || this.get(i).getInsuredPeoples().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getInsuredPeoples());
            }
            if(this.get(i).getEMail() == null || this.get(i).getEMail().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getEMail());
            }
            if(this.get(i).getMobile() == null || this.get(i).getMobile().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getMobile());
            }
            if(this.get(i).getCertifyCode() == null || this.get(i).getCertifyCode().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getCertifyCode());
            }
            if(this.get(i).getStartCode() == null || this.get(i).getStartCode().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getStartCode());
            }
            if(this.get(i).getEndCode() == null || this.get(i).getEndCode().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getEndCode());
            }
            if(this.get(i).getCalStandbyFlag1() == null || this.get(i).getCalStandbyFlag1().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getCalStandbyFlag1());
            }
            if(this.get(i).getCalStandbyFlag2() == null || this.get(i).getCalStandbyFlag2().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getCalStandbyFlag2());
            }
            if(this.get(i).getRelationToRisk() == null || this.get(i).getRelationToRisk().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getRelationToRisk());
            }
            if(this.get(i).getContCValiDate() == null || this.get(i).getContCValiDate().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getContCValiDate());
            }
            if(this.get(i).getGrpNo() == null || this.get(i).getGrpNo().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getGrpNo());
            }
            if(this.get(i).getRelationToAppnt() == null || this.get(i).getRelationToAppnt().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getRelationToAppnt());
            }
            if(this.get(i).getSocialInsuFlag() == null || this.get(i).getSocialInsuFlag().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getSocialInsuFlag());
            }
            if(this.get(i).getRelationToMainInsured() == null || this.get(i).getRelationToMainInsured().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getRelationToMainInsured());
            }
            if(this.get(i).getHealthServiceName() == null || this.get(i).getHealthServiceName().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getHealthServiceName());
            }
            if(this.get(i).getHealthInsFlagName() == null || this.get(i).getHealthInsFlagName().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getHealthInsFlagName());
            }
            if(this.get(i).getGrpAlias() == null || this.get(i).getGrpAlias().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getGrpAlias());
            }
            if(this.get(i).getWhetherToRnew() == null || this.get(i).getWhetherToRnew().equals("null")) {
            	pstmt.setString(53,null);
            } else {
            	pstmt.setString(53, this.get(i).getWhetherToRnew());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getBankCode());
            }
            if(this.get(i).getBankProvince() == null || this.get(i).getBankProvince().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getBankProvince());
            }
            if(this.get(i).getBankCity() == null || this.get(i).getBankCity().equals("null")) {
            	pstmt.setString(56,null);
            } else {
            	pstmt.setString(56, this.get(i).getBankCity());
            }
            if(this.get(i).getBankLocations() == null || this.get(i).getBankLocations().equals("null")) {
            	pstmt.setString(57,null);
            } else {
            	pstmt.setString(57, this.get(i).getBankLocations());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(58,null);
            } else {
            	pstmt.setString(58, this.get(i).getAccName());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getBankAccNo());
            }
            if(this.get(i).getAccType() == null || this.get(i).getAccType().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getAccType());
            }
            if(this.get(i).getBeInsuredNo() == null || this.get(i).getBeInsuredNo().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getBeInsuredNo());
            }
            if(this.get(i).getIDExpDate() == null || this.get(i).getIDExpDate().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getIDExpDate());
            }
            if(this.get(i).getTaxpayerIDType() == null || this.get(i).getTaxpayerIDType().equals("null")) {
            	pstmt.setString(63,null);
            } else {
            	pstmt.setString(63, this.get(i).getTaxpayerIDType());
            }
            if(this.get(i).getCompanyNo() == null || this.get(i).getCompanyNo().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getCompanyNo());
            }
            if(this.get(i).getCompanyName() == null || this.get(i).getCompanyName().equals("null")) {
            	pstmt.setString(65,null);
            } else {
            	pstmt.setString(65, this.get(i).getCompanyName());
            }
            if(this.get(i).getFlag() == null || this.get(i).getFlag().equals("null")) {
            	pstmt.setString(66,null);
            } else {
            	pstmt.setString(66, this.get(i).getFlag());
            }
            if(this.get(i).getErrorInfo() == null || this.get(i).getErrorInfo().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getErrorInfo());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(69,null);
            } else {
                pstmt.setDate(69, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(70,null);
            } else {
            	pstmt.setString(70, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(71,null);
            } else {
                pstmt.setDate(71, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModIfyTime() == null || this.get(i).getModIfyTime().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getModIfyTime());
            }
            if(this.get(i).getE_PolicyID() == null || this.get(i).getE_PolicyID().equals("null")) {
            	pstmt.setString(73,null);
            } else {
            	pstmt.setString(73, this.get(i).getE_PolicyID());
            }
            // set where condition
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(74,null);
            } else {
            	pstmt.setString(74, this.get(i).getSerialNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInsuredImportSubDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCInsuredImportSub VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getSerialNo() == null || this.get(i).getSerialNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getSerialNo());
            }
            if(this.get(i).getOtherNoType() == null || this.get(i).getOtherNoType().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getOtherNoType());
            }
            if(this.get(i).getOtherNo() == null || this.get(i).getOtherNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getOtherNo());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getAppntNo());
            }
            if(this.get(i).getCheckFlag() == null || this.get(i).getCheckFlag().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getCheckFlag());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getPrtNo());
            }
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getGrpContNo());
            }
            if(this.get(i).getBatchNo() == null || this.get(i).getBatchNo().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getBatchNo());
            }
            if(this.get(i).getID() == null || this.get(i).getID().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getID());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getContNo());
            }
            if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getInsuredNo());
            }
            if(this.get(i).getMainInsuredNo() == null || this.get(i).getMainInsuredNo().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getMainInsuredNo());
            }
            if(this.get(i).getCustomerState() == null || this.get(i).getCustomerState().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getCustomerState());
            }
            if(this.get(i).getMainInsuredName() == null || this.get(i).getMainInsuredName().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getMainInsuredName());
            }
            if(this.get(i).getMainInsuredIDNo() == null || this.get(i).getMainInsuredIDNo().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getMainInsuredIDNo());
            }
            if(this.get(i).getInsuredName() == null || this.get(i).getInsuredName().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getInsuredName());
            }
            if(this.get(i).getEmployeeRelation() == null || this.get(i).getEmployeeRelation().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getEmployeeRelation());
            }
            if(this.get(i).getInsuredSex() == null || this.get(i).getInsuredSex().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getInsuredSex());
            }
            if(this.get(i).getInsuredBirthday() == null || this.get(i).getInsuredBirthday().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getInsuredBirthday());
            }
            if(this.get(i).getInsuredIDType() == null || this.get(i).getInsuredIDType().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getInsuredIDType());
            }
            if(this.get(i).getInsuredIDNo() == null || this.get(i).getInsuredIDNo().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getInsuredIDNo());
            }
            if(this.get(i).getNativePlace() == null || this.get(i).getNativePlace().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getNativePlace());
            }
            if(this.get(i).getRgtAddress() == null || this.get(i).getRgtAddress().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getRgtAddress());
            }
            if(this.get(i).getMarriage() == null || this.get(i).getMarriage().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getMarriage());
            }
            if(this.get(i).getNationality() == null || this.get(i).getNationality().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getNationality());
            }
            if(this.get(i).getContPlan() == null || this.get(i).getContPlan().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getContPlan());
            }
            if(this.get(i).getOccupationType() == null || this.get(i).getOccupationType().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getOccupationType());
            }
            if(this.get(i).getOccupationCode() == null || this.get(i).getOccupationCode().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getOccupationCode());
            }
            if(this.get(i).getPluralityType() == null || this.get(i).getPluralityType().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getPluralityType());
            }
            if(this.get(i).getBankCode1() == null || this.get(i).getBankCode1().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getBankCode1());
            }
            if(this.get(i).getAccName1() == null || this.get(i).getAccName1().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getAccName1());
            }
            if(this.get(i).getBankAccNo1() == null || this.get(i).getBankAccNo1().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getBankAccNo1());
            }
            if(this.get(i).getJoinCompanyDate() == null || this.get(i).getJoinCompanyDate().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getJoinCompanyDate());
            }
            if(this.get(i).getSalary() == null || this.get(i).getSalary().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getSalary());
            }
            if(this.get(i).getPolTypeFlag() == null || this.get(i).getPolTypeFlag().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getPolTypeFlag());
            }
            if(this.get(i).getInsuredPeoples() == null || this.get(i).getInsuredPeoples().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getInsuredPeoples());
            }
            if(this.get(i).getEMail() == null || this.get(i).getEMail().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getEMail());
            }
            if(this.get(i).getMobile() == null || this.get(i).getMobile().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getMobile());
            }
            if(this.get(i).getCertifyCode() == null || this.get(i).getCertifyCode().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getCertifyCode());
            }
            if(this.get(i).getStartCode() == null || this.get(i).getStartCode().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getStartCode());
            }
            if(this.get(i).getEndCode() == null || this.get(i).getEndCode().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getEndCode());
            }
            if(this.get(i).getCalStandbyFlag1() == null || this.get(i).getCalStandbyFlag1().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getCalStandbyFlag1());
            }
            if(this.get(i).getCalStandbyFlag2() == null || this.get(i).getCalStandbyFlag2().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getCalStandbyFlag2());
            }
            if(this.get(i).getRelationToRisk() == null || this.get(i).getRelationToRisk().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getRelationToRisk());
            }
            if(this.get(i).getContCValiDate() == null || this.get(i).getContCValiDate().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getContCValiDate());
            }
            if(this.get(i).getGrpNo() == null || this.get(i).getGrpNo().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getGrpNo());
            }
            if(this.get(i).getRelationToAppnt() == null || this.get(i).getRelationToAppnt().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getRelationToAppnt());
            }
            if(this.get(i).getSocialInsuFlag() == null || this.get(i).getSocialInsuFlag().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getSocialInsuFlag());
            }
            if(this.get(i).getRelationToMainInsured() == null || this.get(i).getRelationToMainInsured().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getRelationToMainInsured());
            }
            if(this.get(i).getHealthServiceName() == null || this.get(i).getHealthServiceName().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getHealthServiceName());
            }
            if(this.get(i).getHealthInsFlagName() == null || this.get(i).getHealthInsFlagName().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getHealthInsFlagName());
            }
            if(this.get(i).getGrpAlias() == null || this.get(i).getGrpAlias().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getGrpAlias());
            }
            if(this.get(i).getWhetherToRnew() == null || this.get(i).getWhetherToRnew().equals("null")) {
            	pstmt.setString(53,null);
            } else {
            	pstmt.setString(53, this.get(i).getWhetherToRnew());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getBankCode());
            }
            if(this.get(i).getBankProvince() == null || this.get(i).getBankProvince().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getBankProvince());
            }
            if(this.get(i).getBankCity() == null || this.get(i).getBankCity().equals("null")) {
            	pstmt.setString(56,null);
            } else {
            	pstmt.setString(56, this.get(i).getBankCity());
            }
            if(this.get(i).getBankLocations() == null || this.get(i).getBankLocations().equals("null")) {
            	pstmt.setString(57,null);
            } else {
            	pstmt.setString(57, this.get(i).getBankLocations());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(58,null);
            } else {
            	pstmt.setString(58, this.get(i).getAccName());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getBankAccNo());
            }
            if(this.get(i).getAccType() == null || this.get(i).getAccType().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getAccType());
            }
            if(this.get(i).getBeInsuredNo() == null || this.get(i).getBeInsuredNo().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getBeInsuredNo());
            }
            if(this.get(i).getIDExpDate() == null || this.get(i).getIDExpDate().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getIDExpDate());
            }
            if(this.get(i).getTaxpayerIDType() == null || this.get(i).getTaxpayerIDType().equals("null")) {
            	pstmt.setString(63,null);
            } else {
            	pstmt.setString(63, this.get(i).getTaxpayerIDType());
            }
            if(this.get(i).getCompanyNo() == null || this.get(i).getCompanyNo().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getCompanyNo());
            }
            if(this.get(i).getCompanyName() == null || this.get(i).getCompanyName().equals("null")) {
            	pstmt.setString(65,null);
            } else {
            	pstmt.setString(65, this.get(i).getCompanyName());
            }
            if(this.get(i).getFlag() == null || this.get(i).getFlag().equals("null")) {
            	pstmt.setString(66,null);
            } else {
            	pstmt.setString(66, this.get(i).getFlag());
            }
            if(this.get(i).getErrorInfo() == null || this.get(i).getErrorInfo().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getErrorInfo());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(69,null);
            } else {
                pstmt.setDate(69, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(70,null);
            } else {
            	pstmt.setString(70, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(71,null);
            } else {
                pstmt.setDate(71, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModIfyTime() == null || this.get(i).getModIfyTime().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getModIfyTime());
            }
            if(this.get(i).getE_PolicyID() == null || this.get(i).getE_PolicyID().equals("null")) {
            	pstmt.setString(73,null);
            } else {
            	pstmt.setString(73, this.get(i).getE_PolicyID());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCInsuredImportSubDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
