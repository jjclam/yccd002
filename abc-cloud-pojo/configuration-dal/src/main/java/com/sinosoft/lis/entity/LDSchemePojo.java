/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LDSchemePojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-15
 */
public class LDSchemePojo implements  Pojo,Serializable {
    // @Field
    /** 序号 */
    private String SelNO; 
    /** 保险方案编码 */
    private String ContSchemeCode; 
    /** 保险方案名称 */
    private String ContSchemeName; 
    /** 方案类别 */
    private String SchemeType; 
    /** 方案规则 */
    private String SchemeRule; 
    /** 方案规则sql */
    private String SchemeSQL; 
    /** 备注 */
    private String Remark; 
    /** 操作员 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 可投保人数 */
    private int Peoples3; 
    /** 备注2 */
    private String Remark2; 
    /** 管理机构 */
    private String ManageCom; 
    /** 销售渠道 */
    private String Salechnl; 
    /** 销售起期 */
    private String  StartDate;
    /** 销售止期 */
    private String  EndDate;
    /** 状态 */
    private String State; 
    /** 方案层级2 */
    private String SchemeKind2; 
    /** 方案层级3 */
    private String SchemeKind3; 
    /** 方案层级1 */
    private String SchemeKind1; 
    /** 产品组合标记 */
    private String PortfolioFlag; 
    /** 医保身份标记 */
    private String MedicalFlag; 
    /** 推送标记 */
    private String PushFlag; 


    public static final int FIELDNUM = 25;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getSelNO() {
        return SelNO;
    }
    public void setSelNO(String aSelNO) {
        SelNO = aSelNO;
    }
    public String getContSchemeCode() {
        return ContSchemeCode;
    }
    public void setContSchemeCode(String aContSchemeCode) {
        ContSchemeCode = aContSchemeCode;
    }
    public String getContSchemeName() {
        return ContSchemeName;
    }
    public void setContSchemeName(String aContSchemeName) {
        ContSchemeName = aContSchemeName;
    }
    public String getSchemeType() {
        return SchemeType;
    }
    public void setSchemeType(String aSchemeType) {
        SchemeType = aSchemeType;
    }
    public String getSchemeRule() {
        return SchemeRule;
    }
    public void setSchemeRule(String aSchemeRule) {
        SchemeRule = aSchemeRule;
    }
    public String getSchemeSQL() {
        return SchemeSQL;
    }
    public void setSchemeSQL(String aSchemeSQL) {
        SchemeSQL = aSchemeSQL;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public int getPeoples3() {
        return Peoples3;
    }
    public void setPeoples3(int aPeoples3) {
        Peoples3 = aPeoples3;
    }
    public void setPeoples3(String aPeoples3) {
        if (aPeoples3 != null && !aPeoples3.equals("")) {
            Integer tInteger = new Integer(aPeoples3);
            int i = tInteger.intValue();
            Peoples3 = i;
        }
    }

    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getSalechnl() {
        return Salechnl;
    }
    public void setSalechnl(String aSalechnl) {
        Salechnl = aSalechnl;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getSchemeKind2() {
        return SchemeKind2;
    }
    public void setSchemeKind2(String aSchemeKind2) {
        SchemeKind2 = aSchemeKind2;
    }
    public String getSchemeKind3() {
        return SchemeKind3;
    }
    public void setSchemeKind3(String aSchemeKind3) {
        SchemeKind3 = aSchemeKind3;
    }
    public String getSchemeKind1() {
        return SchemeKind1;
    }
    public void setSchemeKind1(String aSchemeKind1) {
        SchemeKind1 = aSchemeKind1;
    }
    public String getPortfolioFlag() {
        return PortfolioFlag;
    }
    public void setPortfolioFlag(String aPortfolioFlag) {
        PortfolioFlag = aPortfolioFlag;
    }
    public String getMedicalFlag() {
        return MedicalFlag;
    }
    public void setMedicalFlag(String aMedicalFlag) {
        MedicalFlag = aMedicalFlag;
    }
    public String getPushFlag() {
        return PushFlag;
    }
    public void setPushFlag(String aPushFlag) {
        PushFlag = aPushFlag;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SelNO") ) {
            return 0;
        }
        if( strFieldName.equals("ContSchemeCode") ) {
            return 1;
        }
        if( strFieldName.equals("ContSchemeName") ) {
            return 2;
        }
        if( strFieldName.equals("SchemeType") ) {
            return 3;
        }
        if( strFieldName.equals("SchemeRule") ) {
            return 4;
        }
        if( strFieldName.equals("SchemeSQL") ) {
            return 5;
        }
        if( strFieldName.equals("Remark") ) {
            return 6;
        }
        if( strFieldName.equals("Operator") ) {
            return 7;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 8;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 11;
        }
        if( strFieldName.equals("Peoples3") ) {
            return 12;
        }
        if( strFieldName.equals("Remark2") ) {
            return 13;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 14;
        }
        if( strFieldName.equals("Salechnl") ) {
            return 15;
        }
        if( strFieldName.equals("StartDate") ) {
            return 16;
        }
        if( strFieldName.equals("EndDate") ) {
            return 17;
        }
        if( strFieldName.equals("State") ) {
            return 18;
        }
        if( strFieldName.equals("SchemeKind2") ) {
            return 19;
        }
        if( strFieldName.equals("SchemeKind3") ) {
            return 20;
        }
        if( strFieldName.equals("SchemeKind1") ) {
            return 21;
        }
        if( strFieldName.equals("PortfolioFlag") ) {
            return 22;
        }
        if( strFieldName.equals("MedicalFlag") ) {
            return 23;
        }
        if( strFieldName.equals("PushFlag") ) {
            return 24;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SelNO";
                break;
            case 1:
                strFieldName = "ContSchemeCode";
                break;
            case 2:
                strFieldName = "ContSchemeName";
                break;
            case 3:
                strFieldName = "SchemeType";
                break;
            case 4:
                strFieldName = "SchemeRule";
                break;
            case 5:
                strFieldName = "SchemeSQL";
                break;
            case 6:
                strFieldName = "Remark";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "Peoples3";
                break;
            case 13:
                strFieldName = "Remark2";
                break;
            case 14:
                strFieldName = "ManageCom";
                break;
            case 15:
                strFieldName = "Salechnl";
                break;
            case 16:
                strFieldName = "StartDate";
                break;
            case 17:
                strFieldName = "EndDate";
                break;
            case 18:
                strFieldName = "State";
                break;
            case 19:
                strFieldName = "SchemeKind2";
                break;
            case 20:
                strFieldName = "SchemeKind3";
                break;
            case 21:
                strFieldName = "SchemeKind1";
                break;
            case 22:
                strFieldName = "PortfolioFlag";
                break;
            case 23:
                strFieldName = "MedicalFlag";
                break;
            case 24:
                strFieldName = "PushFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SELNO":
                return Schema.TYPE_STRING;
            case "CONTSCHEMECODE":
                return Schema.TYPE_STRING;
            case "CONTSCHEMENAME":
                return Schema.TYPE_STRING;
            case "SCHEMETYPE":
                return Schema.TYPE_STRING;
            case "SCHEMERULE":
                return Schema.TYPE_STRING;
            case "SCHEMESQL":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "PEOPLES3":
                return Schema.TYPE_INT;
            case "REMARK2":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "SCHEMEKIND2":
                return Schema.TYPE_STRING;
            case "SCHEMEKIND3":
                return Schema.TYPE_STRING;
            case "SCHEMEKIND1":
                return Schema.TYPE_STRING;
            case "PORTFOLIOFLAG":
                return Schema.TYPE_STRING;
            case "MEDICALFLAG":
                return Schema.TYPE_STRING;
            case "PUSHFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_INT;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SelNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelNO));
        }
        if (FCode.equalsIgnoreCase("ContSchemeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContSchemeCode));
        }
        if (FCode.equalsIgnoreCase("ContSchemeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContSchemeName));
        }
        if (FCode.equalsIgnoreCase("SchemeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeType));
        }
        if (FCode.equalsIgnoreCase("SchemeRule")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeRule));
        }
        if (FCode.equalsIgnoreCase("SchemeSQL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeSQL));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Salechnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salechnl));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("SchemeKind2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeKind2));
        }
        if (FCode.equalsIgnoreCase("SchemeKind3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeKind3));
        }
        if (FCode.equalsIgnoreCase("SchemeKind1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeKind1));
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PortfolioFlag));
        }
        if (FCode.equalsIgnoreCase("MedicalFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicalFlag));
        }
        if (FCode.equalsIgnoreCase("PushFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PushFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(SelNO);
                break;
            case 1:
                strFieldValue = String.valueOf(ContSchemeCode);
                break;
            case 2:
                strFieldValue = String.valueOf(ContSchemeName);
                break;
            case 3:
                strFieldValue = String.valueOf(SchemeType);
                break;
            case 4:
                strFieldValue = String.valueOf(SchemeRule);
                break;
            case 5:
                strFieldValue = String.valueOf(SchemeSQL);
                break;
            case 6:
                strFieldValue = String.valueOf(Remark);
                break;
            case 7:
                strFieldValue = String.valueOf(Operator);
                break;
            case 8:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 9:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 10:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 11:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 12:
                strFieldValue = String.valueOf(Peoples3);
                break;
            case 13:
                strFieldValue = String.valueOf(Remark2);
                break;
            case 14:
                strFieldValue = String.valueOf(ManageCom);
                break;
            case 15:
                strFieldValue = String.valueOf(Salechnl);
                break;
            case 16:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 17:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 18:
                strFieldValue = String.valueOf(State);
                break;
            case 19:
                strFieldValue = String.valueOf(SchemeKind2);
                break;
            case 20:
                strFieldValue = String.valueOf(SchemeKind3);
                break;
            case 21:
                strFieldValue = String.valueOf(SchemeKind1);
                break;
            case 22:
                strFieldValue = String.valueOf(PortfolioFlag);
                break;
            case 23:
                strFieldValue = String.valueOf(MedicalFlag);
                break;
            case 24:
                strFieldValue = String.valueOf(PushFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SelNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SelNO = FValue.trim();
            }
            else
                SelNO = null;
        }
        if (FCode.equalsIgnoreCase("ContSchemeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContSchemeCode = FValue.trim();
            }
            else
                ContSchemeCode = null;
        }
        if (FCode.equalsIgnoreCase("ContSchemeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContSchemeName = FValue.trim();
            }
            else
                ContSchemeName = null;
        }
        if (FCode.equalsIgnoreCase("SchemeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeType = FValue.trim();
            }
            else
                SchemeType = null;
        }
        if (FCode.equalsIgnoreCase("SchemeRule")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeRule = FValue.trim();
            }
            else
                SchemeRule = null;
        }
        if (FCode.equalsIgnoreCase("SchemeSQL")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeSQL = FValue.trim();
            }
            else
                SchemeSQL = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples3 = i;
            }
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Salechnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                Salechnl = FValue.trim();
            }
            else
                Salechnl = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("SchemeKind2")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeKind2 = FValue.trim();
            }
            else
                SchemeKind2 = null;
        }
        if (FCode.equalsIgnoreCase("SchemeKind3")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeKind3 = FValue.trim();
            }
            else
                SchemeKind3 = null;
        }
        if (FCode.equalsIgnoreCase("SchemeKind1")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeKind1 = FValue.trim();
            }
            else
                SchemeKind1 = null;
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PortfolioFlag = FValue.trim();
            }
            else
                PortfolioFlag = null;
        }
        if (FCode.equalsIgnoreCase("MedicalFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MedicalFlag = FValue.trim();
            }
            else
                MedicalFlag = null;
        }
        if (FCode.equalsIgnoreCase("PushFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PushFlag = FValue.trim();
            }
            else
                PushFlag = null;
        }
        return true;
    }


    public String toString() {
    return "LDSchemePojo [" +
            "SelNO="+SelNO +
            ", ContSchemeCode="+ContSchemeCode +
            ", ContSchemeName="+ContSchemeName +
            ", SchemeType="+SchemeType +
            ", SchemeRule="+SchemeRule +
            ", SchemeSQL="+SchemeSQL +
            ", Remark="+Remark +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", Peoples3="+Peoples3 +
            ", Remark2="+Remark2 +
            ", ManageCom="+ManageCom +
            ", Salechnl="+Salechnl +
            ", StartDate="+StartDate +
            ", EndDate="+EndDate +
            ", State="+State +
            ", SchemeKind2="+SchemeKind2 +
            ", SchemeKind3="+SchemeKind3 +
            ", SchemeKind1="+SchemeKind1 +
            ", PortfolioFlag="+PortfolioFlag +
            ", MedicalFlag="+MedicalFlag +
            ", PushFlag="+PushFlag +"]";
    }
}
