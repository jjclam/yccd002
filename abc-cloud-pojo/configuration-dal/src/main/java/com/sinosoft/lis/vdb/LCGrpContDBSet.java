/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LCGrpContDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LCGrpContDBSet extends LCGrpContSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LCGrpContDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCGrpCont");
        mflag = true;
    }

    public LCGrpContDBSet() {
        db = new DBOper( "LCGrpCont" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCGrpCont WHERE  1=1  AND GrpContNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCGrpCont SET  GrpContNo = ? , ProposalGrpContNo = ? , PrtNo = ? , SaleChnl = ? , ManageCom = ? , Password = ? , Password2 = ? , AppntNo = ? , Peoples2 = ? , GetFlag = ? , DisputedFlag = ? , GrpSpec = ? , PayMode = ? , SignCom = ? , SignDate = ? , SignTime = ? , CValiDate = ? , PayIntv = ? , Peoples = ? , Mult = ? , Prem = ? , Amnt = ? , SumPrem = ? , SumPay = ? , Dif = ? , Remark = ? , StandbyFlag1 = ? , StandbyFlag2 = ? , StandbyFlag3 = ? , ApproveFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , UWOperator = ? , UWFlag = ? , UWDate = ? , UWTime = ? , AppFlag = ? , PolApplyDate = ? , CustomGetPolDate = ? , GetPolDate = ? , GetPolTime = ? , State = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FirstTrialOperator = ? , FirstTrialDate = ? , FirstTrialTime = ? , ContType = ? , AnnuityReceiveDate = ? , CEndDate = ? , NativePeoples = ? , NativePrem = ? , NativeAmnt = ? , ReduceFre = ? , OffAcc = ? , OffPwd = ? , SaleChannels = ? , E_AppntID = ? , E_AppntDate = ? , E_ContID = ? , E_ContDate = ? , IsRenewFlag = ? WHERE  1=1  AND GrpContNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
            if(this.get(i).getProposalGrpContNo() == null || this.get(i).getProposalGrpContNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getProposalGrpContNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getPrtNo());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSaleChnl());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getManageCom());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getPassword());
            }
            if(this.get(i).getPassword2() == null || this.get(i).getPassword2().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getPassword2());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getAppntNo());
            }
            pstmt.setInt(9, this.get(i).getPeoples2());
            if(this.get(i).getGetFlag() == null || this.get(i).getGetFlag().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getGetFlag());
            }
            if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getDisputedFlag());
            }
            if(this.get(i).getGrpSpec() == null || this.get(i).getGrpSpec().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getGrpSpec());
            }
            if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getPayMode());
            }
            if(this.get(i).getSignCom() == null || this.get(i).getSignCom().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getSignCom());
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getSignTime());
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(17,null);
            } else {
                pstmt.setDate(17, Date.valueOf(this.get(i).getCValiDate()));
            }
            pstmt.setInt(18, this.get(i).getPayIntv());
            pstmt.setInt(19, this.get(i).getPeoples());
            pstmt.setDouble(20, this.get(i).getMult());
            pstmt.setDouble(21, this.get(i).getPrem());
            pstmt.setDouble(22, this.get(i).getAmnt());
            pstmt.setDouble(23, this.get(i).getSumPrem());
            pstmt.setDouble(24, this.get(i).getSumPay());
            pstmt.setDouble(25, this.get(i).getDif());
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getRemark());
            }
            if(this.get(i).getStandbyFlag1() == null || this.get(i).getStandbyFlag1().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getStandbyFlag1());
            }
            if(this.get(i).getStandbyFlag2() == null || this.get(i).getStandbyFlag2().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getStandbyFlag2());
            }
            if(this.get(i).getStandbyFlag3() == null || this.get(i).getStandbyFlag3().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getStandbyFlag3());
            }
            if(this.get(i).getApproveFlag() == null || this.get(i).getApproveFlag().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getApproveFlag());
            }
            if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getApproveCode());
            }
            if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                pstmt.setDate(32,null);
            } else {
                pstmt.setDate(32, Date.valueOf(this.get(i).getApproveDate()));
            }
            if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getApproveTime());
            }
            if(this.get(i).getUWOperator() == null || this.get(i).getUWOperator().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getUWOperator());
            }
            if(this.get(i).getUWFlag() == null || this.get(i).getUWFlag().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getUWFlag());
            }
            if(this.get(i).getUWDate() == null || this.get(i).getUWDate().equals("null")) {
                pstmt.setDate(36,null);
            } else {
                pstmt.setDate(36, Date.valueOf(this.get(i).getUWDate()));
            }
            if(this.get(i).getUWTime() == null || this.get(i).getUWTime().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getUWTime());
            }
            if(this.get(i).getAppFlag() == null || this.get(i).getAppFlag().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getAppFlag());
            }
            if(this.get(i).getPolApplyDate() == null || this.get(i).getPolApplyDate().equals("null")) {
                pstmt.setDate(39,null);
            } else {
                pstmt.setDate(39, Date.valueOf(this.get(i).getPolApplyDate()));
            }
            if(this.get(i).getCustomGetPolDate() == null || this.get(i).getCustomGetPolDate().equals("null")) {
                pstmt.setDate(40,null);
            } else {
                pstmt.setDate(40, Date.valueOf(this.get(i).getCustomGetPolDate()));
            }
            if(this.get(i).getGetPolDate() == null || this.get(i).getGetPolDate().equals("null")) {
                pstmt.setDate(41,null);
            } else {
                pstmt.setDate(41, Date.valueOf(this.get(i).getGetPolDate()));
            }
            if(this.get(i).getGetPolTime() == null || this.get(i).getGetPolTime().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getGetPolTime());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getState());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(45,null);
            } else {
                pstmt.setDate(45, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(47,null);
            } else {
                pstmt.setDate(47, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getModifyTime());
            }
            if(this.get(i).getFirstTrialOperator() == null || this.get(i).getFirstTrialOperator().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getFirstTrialOperator());
            }
            if(this.get(i).getFirstTrialDate() == null || this.get(i).getFirstTrialDate().equals("null")) {
                pstmt.setDate(50,null);
            } else {
                pstmt.setDate(50, Date.valueOf(this.get(i).getFirstTrialDate()));
            }
            if(this.get(i).getFirstTrialTime() == null || this.get(i).getFirstTrialTime().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getFirstTrialTime());
            }
            if(this.get(i).getContType() == null || this.get(i).getContType().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getContType());
            }
            if(this.get(i).getAnnuityReceiveDate() == null || this.get(i).getAnnuityReceiveDate().equals("null")) {
                pstmt.setDate(53,null);
            } else {
                pstmt.setDate(53, Date.valueOf(this.get(i).getAnnuityReceiveDate()));
            }
            if(this.get(i).getCEndDate() == null || this.get(i).getCEndDate().equals("null")) {
                pstmt.setDate(54,null);
            } else {
                pstmt.setDate(54, Date.valueOf(this.get(i).getCEndDate()));
            }
            pstmt.setInt(55, this.get(i).getNativePeoples());
            pstmt.setDouble(56, this.get(i).getNativePrem());
            pstmt.setDouble(57, this.get(i).getNativeAmnt());
            if(this.get(i).getReduceFre() == null || this.get(i).getReduceFre().equals("null")) {
            	pstmt.setString(58,null);
            } else {
            	pstmt.setString(58, this.get(i).getReduceFre());
            }
            if(this.get(i).getOffAcc() == null || this.get(i).getOffAcc().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getOffAcc());
            }
            if(this.get(i).getOffPwd() == null || this.get(i).getOffPwd().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getOffPwd());
            }
            if(this.get(i).getSaleChannels() == null || this.get(i).getSaleChannels().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getSaleChannels());
            }
            if(this.get(i).getE_AppntID() == null || this.get(i).getE_AppntID().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getE_AppntID());
            }
            if(this.get(i).getE_AppntDate() == null || this.get(i).getE_AppntDate().equals("null")) {
                pstmt.setDate(63,null);
            } else {
                pstmt.setDate(63, Date.valueOf(this.get(i).getE_AppntDate()));
            }
            if(this.get(i).getE_ContID() == null || this.get(i).getE_ContID().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getE_ContID());
            }
            if(this.get(i).getE_ContDate() == null || this.get(i).getE_ContDate().equals("null")) {
                pstmt.setDate(65,null);
            } else {
                pstmt.setDate(65, Date.valueOf(this.get(i).getE_ContDate()));
            }
            if(this.get(i).getIsRenewFlag() == null || this.get(i).getIsRenewFlag().equals("null")) {
            	pstmt.setString(66,null);
            } else {
            	pstmt.setString(66, this.get(i).getIsRenewFlag());
            }
            // set where condition
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getGrpContNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCGrpCont VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getGrpContNo());
            }
            if(this.get(i).getProposalGrpContNo() == null || this.get(i).getProposalGrpContNo().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getProposalGrpContNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getPrtNo());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getSaleChnl());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getManageCom());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getPassword());
            }
            if(this.get(i).getPassword2() == null || this.get(i).getPassword2().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getPassword2());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getAppntNo());
            }
            pstmt.setInt(9, this.get(i).getPeoples2());
            if(this.get(i).getGetFlag() == null || this.get(i).getGetFlag().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getGetFlag());
            }
            if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getDisputedFlag());
            }
            if(this.get(i).getGrpSpec() == null || this.get(i).getGrpSpec().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getGrpSpec());
            }
            if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getPayMode());
            }
            if(this.get(i).getSignCom() == null || this.get(i).getSignCom().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getSignCom());
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(15,null);
            } else {
                pstmt.setDate(15, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getSignTime());
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(17,null);
            } else {
                pstmt.setDate(17, Date.valueOf(this.get(i).getCValiDate()));
            }
            pstmt.setInt(18, this.get(i).getPayIntv());
            pstmt.setInt(19, this.get(i).getPeoples());
            pstmt.setDouble(20, this.get(i).getMult());
            pstmt.setDouble(21, this.get(i).getPrem());
            pstmt.setDouble(22, this.get(i).getAmnt());
            pstmt.setDouble(23, this.get(i).getSumPrem());
            pstmt.setDouble(24, this.get(i).getSumPay());
            pstmt.setDouble(25, this.get(i).getDif());
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getRemark());
            }
            if(this.get(i).getStandbyFlag1() == null || this.get(i).getStandbyFlag1().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getStandbyFlag1());
            }
            if(this.get(i).getStandbyFlag2() == null || this.get(i).getStandbyFlag2().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getStandbyFlag2());
            }
            if(this.get(i).getStandbyFlag3() == null || this.get(i).getStandbyFlag3().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getStandbyFlag3());
            }
            if(this.get(i).getApproveFlag() == null || this.get(i).getApproveFlag().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getApproveFlag());
            }
            if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getApproveCode());
            }
            if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                pstmt.setDate(32,null);
            } else {
                pstmt.setDate(32, Date.valueOf(this.get(i).getApproveDate()));
            }
            if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getApproveTime());
            }
            if(this.get(i).getUWOperator() == null || this.get(i).getUWOperator().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getUWOperator());
            }
            if(this.get(i).getUWFlag() == null || this.get(i).getUWFlag().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getUWFlag());
            }
            if(this.get(i).getUWDate() == null || this.get(i).getUWDate().equals("null")) {
                pstmt.setDate(36,null);
            } else {
                pstmt.setDate(36, Date.valueOf(this.get(i).getUWDate()));
            }
            if(this.get(i).getUWTime() == null || this.get(i).getUWTime().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getUWTime());
            }
            if(this.get(i).getAppFlag() == null || this.get(i).getAppFlag().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getAppFlag());
            }
            if(this.get(i).getPolApplyDate() == null || this.get(i).getPolApplyDate().equals("null")) {
                pstmt.setDate(39,null);
            } else {
                pstmt.setDate(39, Date.valueOf(this.get(i).getPolApplyDate()));
            }
            if(this.get(i).getCustomGetPolDate() == null || this.get(i).getCustomGetPolDate().equals("null")) {
                pstmt.setDate(40,null);
            } else {
                pstmt.setDate(40, Date.valueOf(this.get(i).getCustomGetPolDate()));
            }
            if(this.get(i).getGetPolDate() == null || this.get(i).getGetPolDate().equals("null")) {
                pstmt.setDate(41,null);
            } else {
                pstmt.setDate(41, Date.valueOf(this.get(i).getGetPolDate()));
            }
            if(this.get(i).getGetPolTime() == null || this.get(i).getGetPolTime().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getGetPolTime());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getState());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(45,null);
            } else {
                pstmt.setDate(45, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(47,null);
            } else {
                pstmt.setDate(47, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getModifyTime());
            }
            if(this.get(i).getFirstTrialOperator() == null || this.get(i).getFirstTrialOperator().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getFirstTrialOperator());
            }
            if(this.get(i).getFirstTrialDate() == null || this.get(i).getFirstTrialDate().equals("null")) {
                pstmt.setDate(50,null);
            } else {
                pstmt.setDate(50, Date.valueOf(this.get(i).getFirstTrialDate()));
            }
            if(this.get(i).getFirstTrialTime() == null || this.get(i).getFirstTrialTime().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getFirstTrialTime());
            }
            if(this.get(i).getContType() == null || this.get(i).getContType().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getContType());
            }
            if(this.get(i).getAnnuityReceiveDate() == null || this.get(i).getAnnuityReceiveDate().equals("null")) {
                pstmt.setDate(53,null);
            } else {
                pstmt.setDate(53, Date.valueOf(this.get(i).getAnnuityReceiveDate()));
            }
            if(this.get(i).getCEndDate() == null || this.get(i).getCEndDate().equals("null")) {
                pstmt.setDate(54,null);
            } else {
                pstmt.setDate(54, Date.valueOf(this.get(i).getCEndDate()));
            }
            pstmt.setInt(55, this.get(i).getNativePeoples());
            pstmt.setDouble(56, this.get(i).getNativePrem());
            pstmt.setDouble(57, this.get(i).getNativeAmnt());
            if(this.get(i).getReduceFre() == null || this.get(i).getReduceFre().equals("null")) {
            	pstmt.setString(58,null);
            } else {
            	pstmt.setString(58, this.get(i).getReduceFre());
            }
            if(this.get(i).getOffAcc() == null || this.get(i).getOffAcc().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getOffAcc());
            }
            if(this.get(i).getOffPwd() == null || this.get(i).getOffPwd().equals("null")) {
            	pstmt.setString(60,null);
            } else {
            	pstmt.setString(60, this.get(i).getOffPwd());
            }
            if(this.get(i).getSaleChannels() == null || this.get(i).getSaleChannels().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getSaleChannels());
            }
            if(this.get(i).getE_AppntID() == null || this.get(i).getE_AppntID().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getE_AppntID());
            }
            if(this.get(i).getE_AppntDate() == null || this.get(i).getE_AppntDate().equals("null")) {
                pstmt.setDate(63,null);
            } else {
                pstmt.setDate(63, Date.valueOf(this.get(i).getE_AppntDate()));
            }
            if(this.get(i).getE_ContID() == null || this.get(i).getE_ContID().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getE_ContID());
            }
            if(this.get(i).getE_ContDate() == null || this.get(i).getE_ContDate().equals("null")) {
                pstmt.setDate(65,null);
            } else {
                pstmt.setDate(65, Date.valueOf(this.get(i).getE_ContDate()));
            }
            if(this.get(i).getIsRenewFlag() == null || this.get(i).getIsRenewFlag().equals("null")) {
            	pstmt.setString(66,null);
            } else {
            	pstmt.setString(66, this.get(i).getIsRenewFlag());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
