/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.db;

import java.sql.*;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LCGrpContDB </p>
 * <p>Description: DB层数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 */
public class LCGrpContDB extends LCGrpContSchema {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    public CErrors mErrors = new CErrors();        // 错误信息

    /**
     * 为批量操作而准备的语句和游标对象
     */
    private ResultSet mResultSet = null;
    private Statement mStatement = null;
    // @Constructor
    public LCGrpContDB( Connection tConnection ) {
        con = tConnection;
        db = new DBOper( con, "LCGrpCont" );
        mflag = true;
    }

    public LCGrpContDB() {
        con = null;
        db = new DBOper( "LCGrpCont" );
        mflag = false;
    }

    // @Method
    public boolean deleteSQL() {
        LCGrpContSchema tSchema = this.getSchema();
        if (db.deleteSQL(tSchema)) {
             return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    public int getCount() {
        LCGrpContSchema tSchema = this.getSchema();

        int tCount = db.getCount(tSchema);
        if (tCount < 0) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "getCount";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);

            return -1;
        }

        return tCount;
    }

    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("DELETE FROM LCGrpCont WHERE  1=1  AND GrpContNo = ?");
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpContNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if(!mflag) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean update() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCGrpCont");
        sqlObj.setSQL(2, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("UPDATE LCGrpCont SET  GrpContNo = ? , ProposalGrpContNo = ? , PrtNo = ? , SaleChnl = ? , ManageCom = ? , Password = ? , Password2 = ? , AppntNo = ? , Peoples2 = ? , GetFlag = ? , DisputedFlag = ? , GrpSpec = ? , PayMode = ? , SignCom = ? , SignDate = ? , SignTime = ? , CValiDate = ? , PayIntv = ? , Peoples = ? , Mult = ? , Prem = ? , Amnt = ? , SumPrem = ? , SumPay = ? , Dif = ? , Remark = ? , StandbyFlag1 = ? , StandbyFlag2 = ? , StandbyFlag3 = ? , ApproveFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , UWOperator = ? , UWFlag = ? , UWDate = ? , UWTime = ? , AppFlag = ? , PolApplyDate = ? , CustomGetPolDate = ? , GetPolDate = ? , GetPolTime = ? , State = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FirstTrialOperator = ? , FirstTrialDate = ? , FirstTrialTime = ? , ContType = ? , AnnuityReceiveDate = ? , CEndDate = ? , NativePeoples = ? , NativePrem = ? , NativeAmnt = ? , ReduceFre = ? , OffAcc = ? , OffPwd = ? , SaleChannels = ? , E_AppntID = ? , E_AppntDate = ? , E_ContID = ? , E_ContDate = ? , IsRenewFlag = ? WHERE  1=1  AND GrpContNo = ?");
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpContNo());
            }
            if(this.getProposalGrpContNo() == null || this.getProposalGrpContNo().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getProposalGrpContNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getPrtNo());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getSaleChnl());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getManageCom());
            }
            if(this.getPassword() == null || this.getPassword().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getPassword());
            }
            if(this.getPassword2() == null || this.getPassword2().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getPassword2());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAppntNo());
            }
            pstmt.setInt(9, this.getPeoples2());
            if(this.getGetFlag() == null || this.getGetFlag().equals("null")) {
            	pstmt.setNull(10, 1);
            } else {
            	pstmt.setString(10, this.getGetFlag());
            }
            if(this.getDisputedFlag() == null || this.getDisputedFlag().equals("null")) {
            	pstmt.setNull(11, 1);
            } else {
            	pstmt.setString(11, this.getDisputedFlag());
            }
            if(this.getGrpSpec() == null || this.getGrpSpec().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getGrpSpec());
            }
            if(this.getPayMode() == null || this.getPayMode().equals("null")) {
            	pstmt.setNull(13, 1);
            } else {
            	pstmt.setString(13, this.getPayMode());
            }
            if(this.getSignCom() == null || this.getSignCom().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getSignCom());
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
            	pstmt.setNull(15, 93);
            } else {
            	pstmt.setDate(15, Date.valueOf(this.getSignDate()));
            }
            if(this.getSignTime() == null || this.getSignTime().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getSignTime());
            }
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(17, 93);
            } else {
            	pstmt.setDate(17, Date.valueOf(this.getCValiDate()));
            }
            pstmt.setInt(18, this.getPayIntv());
            pstmt.setInt(19, this.getPeoples());
            pstmt.setDouble(20, this.getMult());
            pstmt.setDouble(21, this.getPrem());
            pstmt.setDouble(22, this.getAmnt());
            pstmt.setDouble(23, this.getSumPrem());
            pstmt.setDouble(24, this.getSumPay());
            pstmt.setDouble(25, this.getDif());
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getRemark());
            }
            if(this.getStandbyFlag1() == null || this.getStandbyFlag1().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getStandbyFlag1());
            }
            if(this.getStandbyFlag2() == null || this.getStandbyFlag2().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getStandbyFlag2());
            }
            if(this.getStandbyFlag3() == null || this.getStandbyFlag3().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getStandbyFlag3());
            }
            if(this.getApproveFlag() == null || this.getApproveFlag().equals("null")) {
            	pstmt.setNull(30, 1);
            } else {
            	pstmt.setString(30, this.getApproveFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(32, 93);
            } else {
            	pstmt.setDate(32, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getApproveTime());
            }
            if(this.getUWOperator() == null || this.getUWOperator().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getUWOperator());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(35, 1);
            } else {
            	pstmt.setString(35, this.getUWFlag());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(36, 93);
            } else {
            	pstmt.setDate(36, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getUWTime());
            }
            if(this.getAppFlag() == null || this.getAppFlag().equals("null")) {
            	pstmt.setNull(38, 1);
            } else {
            	pstmt.setString(38, this.getAppFlag());
            }
            if(this.getPolApplyDate() == null || this.getPolApplyDate().equals("null")) {
            	pstmt.setNull(39, 93);
            } else {
            	pstmt.setDate(39, Date.valueOf(this.getPolApplyDate()));
            }
            if(this.getCustomGetPolDate() == null || this.getCustomGetPolDate().equals("null")) {
            	pstmt.setNull(40, 93);
            } else {
            	pstmt.setDate(40, Date.valueOf(this.getCustomGetPolDate()));
            }
            if(this.getGetPolDate() == null || this.getGetPolDate().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getGetPolDate()));
            }
            if(this.getGetPolTime() == null || this.getGetPolTime().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getGetPolTime());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getState());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(45, 93);
            } else {
            	pstmt.setDate(45, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(46, 1);
            } else {
            	pstmt.setString(46, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(47, 93);
            } else {
            	pstmt.setDate(47, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(48, 1);
            } else {
            	pstmt.setString(48, this.getModifyTime());
            }
            if(this.getFirstTrialOperator() == null || this.getFirstTrialOperator().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getFirstTrialOperator());
            }
            if(this.getFirstTrialDate() == null || this.getFirstTrialDate().equals("null")) {
            	pstmt.setNull(50, 93);
            } else {
            	pstmt.setDate(50, Date.valueOf(this.getFirstTrialDate()));
            }
            if(this.getFirstTrialTime() == null || this.getFirstTrialTime().equals("null")) {
            	pstmt.setNull(51, 1);
            } else {
            	pstmt.setString(51, this.getFirstTrialTime());
            }
            if(this.getContType() == null || this.getContType().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getContType());
            }
            if(this.getAnnuityReceiveDate() == null || this.getAnnuityReceiveDate().equals("null")) {
            	pstmt.setNull(53, 93);
            } else {
            	pstmt.setDate(53, Date.valueOf(this.getAnnuityReceiveDate()));
            }
            if(this.getCEndDate() == null || this.getCEndDate().equals("null")) {
            	pstmt.setNull(54, 93);
            } else {
            	pstmt.setDate(54, Date.valueOf(this.getCEndDate()));
            }
            pstmt.setInt(55, this.getNativePeoples());
            pstmt.setDouble(56, this.getNativePrem());
            pstmt.setDouble(57, this.getNativeAmnt());
            if(this.getReduceFre() == null || this.getReduceFre().equals("null")) {
            	pstmt.setNull(58, 12);
            } else {
            	pstmt.setString(58, this.getReduceFre());
            }
            if(this.getOffAcc() == null || this.getOffAcc().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getOffAcc());
            }
            if(this.getOffPwd() == null || this.getOffPwd().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getOffPwd());
            }
            if(this.getSaleChannels() == null || this.getSaleChannels().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getSaleChannels());
            }
            if(this.getE_AppntID() == null || this.getE_AppntID().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getE_AppntID());
            }
            if(this.getE_AppntDate() == null || this.getE_AppntDate().equals("null")) {
            	pstmt.setNull(63, 93);
            } else {
            	pstmt.setDate(63, Date.valueOf(this.getE_AppntDate()));
            }
            if(this.getE_ContID() == null || this.getE_ContID().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getE_ContID());
            }
            if(this.getE_ContDate() == null || this.getE_ContDate().equals("null")) {
            	pstmt.setNull(65, 93);
            } else {
            	pstmt.setDate(65, Date.valueOf(this.getE_ContDate()));
            }
            if(this.getIsRenewFlag() == null || this.getIsRenewFlag().equals("null")) {
            	pstmt.setNull(66, 12);
            } else {
            	pstmt.setString(66, this.getIsRenewFlag());
            }
            // set where condition
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(67, 12);
            } else {
            	pstmt.setString(67, this.getGrpContNo());
            }
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean insert() {
        PreparedStatement pstmt = null;

        SQLString sqlObj = new SQLString("LCGrpCont");
        sqlObj.setSQL(1, this);
        String sql = sqlObj.getSQL();

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("INSERT INTO LCGrpCont VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpContNo());
            }
            if(this.getProposalGrpContNo() == null || this.getProposalGrpContNo().equals("null")) {
            	pstmt.setNull(2, 12);
            } else {
            	pstmt.setString(2, this.getProposalGrpContNo());
            }
            if(this.getPrtNo() == null || this.getPrtNo().equals("null")) {
            	pstmt.setNull(3, 12);
            } else {
            	pstmt.setString(3, this.getPrtNo());
            }
            if(this.getSaleChnl() == null || this.getSaleChnl().equals("null")) {
            	pstmt.setNull(4, 12);
            } else {
            	pstmt.setString(4, this.getSaleChnl());
            }
            if(this.getManageCom() == null || this.getManageCom().equals("null")) {
            	pstmt.setNull(5, 12);
            } else {
            	pstmt.setString(5, this.getManageCom());
            }
            if(this.getPassword() == null || this.getPassword().equals("null")) {
            	pstmt.setNull(6, 12);
            } else {
            	pstmt.setString(6, this.getPassword());
            }
            if(this.getPassword2() == null || this.getPassword2().equals("null")) {
            	pstmt.setNull(7, 12);
            } else {
            	pstmt.setString(7, this.getPassword2());
            }
            if(this.getAppntNo() == null || this.getAppntNo().equals("null")) {
            	pstmt.setNull(8, 12);
            } else {
            	pstmt.setString(8, this.getAppntNo());
            }
            pstmt.setInt(9, this.getPeoples2());
            if(this.getGetFlag() == null || this.getGetFlag().equals("null")) {
            	pstmt.setNull(10, 1);
            } else {
            	pstmt.setString(10, this.getGetFlag());
            }
            if(this.getDisputedFlag() == null || this.getDisputedFlag().equals("null")) {
            	pstmt.setNull(11, 1);
            } else {
            	pstmt.setString(11, this.getDisputedFlag());
            }
            if(this.getGrpSpec() == null || this.getGrpSpec().equals("null")) {
            	pstmt.setNull(12, 12);
            } else {
            	pstmt.setString(12, this.getGrpSpec());
            }
            if(this.getPayMode() == null || this.getPayMode().equals("null")) {
            	pstmt.setNull(13, 1);
            } else {
            	pstmt.setString(13, this.getPayMode());
            }
            if(this.getSignCom() == null || this.getSignCom().equals("null")) {
            	pstmt.setNull(14, 12);
            } else {
            	pstmt.setString(14, this.getSignCom());
            }
            if(this.getSignDate() == null || this.getSignDate().equals("null")) {
            	pstmt.setNull(15, 93);
            } else {
            	pstmt.setDate(15, Date.valueOf(this.getSignDate()));
            }
            if(this.getSignTime() == null || this.getSignTime().equals("null")) {
            	pstmt.setNull(16, 12);
            } else {
            	pstmt.setString(16, this.getSignTime());
            }
            if(this.getCValiDate() == null || this.getCValiDate().equals("null")) {
            	pstmt.setNull(17, 93);
            } else {
            	pstmt.setDate(17, Date.valueOf(this.getCValiDate()));
            }
            pstmt.setInt(18, this.getPayIntv());
            pstmt.setInt(19, this.getPeoples());
            pstmt.setDouble(20, this.getMult());
            pstmt.setDouble(21, this.getPrem());
            pstmt.setDouble(22, this.getAmnt());
            pstmt.setDouble(23, this.getSumPrem());
            pstmt.setDouble(24, this.getSumPay());
            pstmt.setDouble(25, this.getDif());
            if(this.getRemark() == null || this.getRemark().equals("null")) {
            	pstmt.setNull(26, 12);
            } else {
            	pstmt.setString(26, this.getRemark());
            }
            if(this.getStandbyFlag1() == null || this.getStandbyFlag1().equals("null")) {
            	pstmt.setNull(27, 12);
            } else {
            	pstmt.setString(27, this.getStandbyFlag1());
            }
            if(this.getStandbyFlag2() == null || this.getStandbyFlag2().equals("null")) {
            	pstmt.setNull(28, 12);
            } else {
            	pstmt.setString(28, this.getStandbyFlag2());
            }
            if(this.getStandbyFlag3() == null || this.getStandbyFlag3().equals("null")) {
            	pstmt.setNull(29, 12);
            } else {
            	pstmt.setString(29, this.getStandbyFlag3());
            }
            if(this.getApproveFlag() == null || this.getApproveFlag().equals("null")) {
            	pstmt.setNull(30, 1);
            } else {
            	pstmt.setString(30, this.getApproveFlag());
            }
            if(this.getApproveCode() == null || this.getApproveCode().equals("null")) {
            	pstmt.setNull(31, 12);
            } else {
            	pstmt.setString(31, this.getApproveCode());
            }
            if(this.getApproveDate() == null || this.getApproveDate().equals("null")) {
            	pstmt.setNull(32, 93);
            } else {
            	pstmt.setDate(32, Date.valueOf(this.getApproveDate()));
            }
            if(this.getApproveTime() == null || this.getApproveTime().equals("null")) {
            	pstmt.setNull(33, 12);
            } else {
            	pstmt.setString(33, this.getApproveTime());
            }
            if(this.getUWOperator() == null || this.getUWOperator().equals("null")) {
            	pstmt.setNull(34, 12);
            } else {
            	pstmt.setString(34, this.getUWOperator());
            }
            if(this.getUWFlag() == null || this.getUWFlag().equals("null")) {
            	pstmt.setNull(35, 1);
            } else {
            	pstmt.setString(35, this.getUWFlag());
            }
            if(this.getUWDate() == null || this.getUWDate().equals("null")) {
            	pstmt.setNull(36, 93);
            } else {
            	pstmt.setDate(36, Date.valueOf(this.getUWDate()));
            }
            if(this.getUWTime() == null || this.getUWTime().equals("null")) {
            	pstmt.setNull(37, 12);
            } else {
            	pstmt.setString(37, this.getUWTime());
            }
            if(this.getAppFlag() == null || this.getAppFlag().equals("null")) {
            	pstmt.setNull(38, 1);
            } else {
            	pstmt.setString(38, this.getAppFlag());
            }
            if(this.getPolApplyDate() == null || this.getPolApplyDate().equals("null")) {
            	pstmt.setNull(39, 93);
            } else {
            	pstmt.setDate(39, Date.valueOf(this.getPolApplyDate()));
            }
            if(this.getCustomGetPolDate() == null || this.getCustomGetPolDate().equals("null")) {
            	pstmt.setNull(40, 93);
            } else {
            	pstmt.setDate(40, Date.valueOf(this.getCustomGetPolDate()));
            }
            if(this.getGetPolDate() == null || this.getGetPolDate().equals("null")) {
            	pstmt.setNull(41, 93);
            } else {
            	pstmt.setDate(41, Date.valueOf(this.getGetPolDate()));
            }
            if(this.getGetPolTime() == null || this.getGetPolTime().equals("null")) {
            	pstmt.setNull(42, 12);
            } else {
            	pstmt.setString(42, this.getGetPolTime());
            }
            if(this.getState() == null || this.getState().equals("null")) {
            	pstmt.setNull(43, 12);
            } else {
            	pstmt.setString(43, this.getState());
            }
            if(this.getOperator() == null || this.getOperator().equals("null")) {
            	pstmt.setNull(44, 12);
            } else {
            	pstmt.setString(44, this.getOperator());
            }
            if(this.getMakeDate() == null || this.getMakeDate().equals("null")) {
            	pstmt.setNull(45, 93);
            } else {
            	pstmt.setDate(45, Date.valueOf(this.getMakeDate()));
            }
            if(this.getMakeTime() == null || this.getMakeTime().equals("null")) {
            	pstmt.setNull(46, 1);
            } else {
            	pstmt.setString(46, this.getMakeTime());
            }
            if(this.getModifyDate() == null || this.getModifyDate().equals("null")) {
            	pstmt.setNull(47, 93);
            } else {
            	pstmt.setDate(47, Date.valueOf(this.getModifyDate()));
            }
            if(this.getModifyTime() == null || this.getModifyTime().equals("null")) {
            	pstmt.setNull(48, 1);
            } else {
            	pstmt.setString(48, this.getModifyTime());
            }
            if(this.getFirstTrialOperator() == null || this.getFirstTrialOperator().equals("null")) {
            	pstmt.setNull(49, 12);
            } else {
            	pstmt.setString(49, this.getFirstTrialOperator());
            }
            if(this.getFirstTrialDate() == null || this.getFirstTrialDate().equals("null")) {
            	pstmt.setNull(50, 93);
            } else {
            	pstmt.setDate(50, Date.valueOf(this.getFirstTrialDate()));
            }
            if(this.getFirstTrialTime() == null || this.getFirstTrialTime().equals("null")) {
            	pstmt.setNull(51, 1);
            } else {
            	pstmt.setString(51, this.getFirstTrialTime());
            }
            if(this.getContType() == null || this.getContType().equals("null")) {
            	pstmt.setNull(52, 12);
            } else {
            	pstmt.setString(52, this.getContType());
            }
            if(this.getAnnuityReceiveDate() == null || this.getAnnuityReceiveDate().equals("null")) {
            	pstmt.setNull(53, 93);
            } else {
            	pstmt.setDate(53, Date.valueOf(this.getAnnuityReceiveDate()));
            }
            if(this.getCEndDate() == null || this.getCEndDate().equals("null")) {
            	pstmt.setNull(54, 93);
            } else {
            	pstmt.setDate(54, Date.valueOf(this.getCEndDate()));
            }
            pstmt.setInt(55, this.getNativePeoples());
            pstmt.setDouble(56, this.getNativePrem());
            pstmt.setDouble(57, this.getNativeAmnt());
            if(this.getReduceFre() == null || this.getReduceFre().equals("null")) {
            	pstmt.setNull(58, 12);
            } else {
            	pstmt.setString(58, this.getReduceFre());
            }
            if(this.getOffAcc() == null || this.getOffAcc().equals("null")) {
            	pstmt.setNull(59, 12);
            } else {
            	pstmt.setString(59, this.getOffAcc());
            }
            if(this.getOffPwd() == null || this.getOffPwd().equals("null")) {
            	pstmt.setNull(60, 12);
            } else {
            	pstmt.setString(60, this.getOffPwd());
            }
            if(this.getSaleChannels() == null || this.getSaleChannels().equals("null")) {
            	pstmt.setNull(61, 12);
            } else {
            	pstmt.setString(61, this.getSaleChannels());
            }
            if(this.getE_AppntID() == null || this.getE_AppntID().equals("null")) {
            	pstmt.setNull(62, 12);
            } else {
            	pstmt.setString(62, this.getE_AppntID());
            }
            if(this.getE_AppntDate() == null || this.getE_AppntDate().equals("null")) {
            	pstmt.setNull(63, 93);
            } else {
            	pstmt.setDate(63, Date.valueOf(this.getE_AppntDate()));
            }
            if(this.getE_ContID() == null || this.getE_ContID().equals("null")) {
            	pstmt.setNull(64, 12);
            } else {
            	pstmt.setString(64, this.getE_ContID());
            }
            if(this.getE_ContDate() == null || this.getE_ContDate().equals("null")) {
            	pstmt.setNull(65, 93);
            } else {
            	pstmt.setDate(65, Date.valueOf(this.getE_ContDate()));
            }
            if(this.getIsRenewFlag() == null || this.getIsRenewFlag().equals("null")) {
            	pstmt.setNull(66, 12);
            } else {
            	pstmt.setString(66, this.getIsRenewFlag());
            }
            // execute sql
            pstmt.executeUpdate();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            ex.printStackTrace();

            try {
                pstmt.close();
            } catch (Exception e){}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){}
            }

            return false;
        }

        if(!mflag) {
            try {
                con.close();
            } catch (Exception e){}
        }

        return true;
    }

    public boolean getInfo() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            pstmt = con.prepareStatement("SELECT * FROM LCGrpCont WHERE  1=1  AND GrpContNo = ?", 
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            if(this.getGrpContNo() == null || this.getGrpContNo().equals("null")) {
            	pstmt.setNull(1, 12);
            } else {
            	pstmt.setString(1, this.getGrpContNo());
            }
            rs = pstmt.executeQuery();
            int i = 0;
            while (rs.next()) {
                i++;
                if (!this.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpContDB";
                    tError.functionName = "getInfo";
                    tError.errorMessage = "取数失败!";
                    this.mErrors .addOneError(tError);

                    try{ rs.close(); } catch( Exception ex ) {}
                    try{ pstmt.close(); } catch( Exception ex1 ) {}

                    if (!mflag) {
                        try {
                            con.close();
                        }
                        catch(Exception et){}
                    }
                    return false;
                }
                break;
            }
            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ pstmt.close(); } catch( Exception ex3 ) {}

            if( i == 0 ) {
                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "getInfo";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex ) {}
            try{ pstmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    public LCGrpContSet query() {
        Statement stmt = null;
        ResultSet rs = null;
        LCGrpContSet aLCGrpContSet = new LCGrpContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCGrpCont");
            LCGrpContSchema aSchema = this.getSchema();
            sqlObj.setSQL(5, aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpContDB";
                    tError.functionName = "query";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LCGrpContSchema s1 = new LCGrpContSchema();
                s1.setSchema(rs,i);
                aLCGrpContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            e.printStackTrace();

            try { rs.close(); } catch (Exception ex2) { ex2.printStackTrace(); }
            try { stmt.close(); } catch(Exception ex3) { ex3.printStackTrace(); }

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){et.printStackTrace();}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){e.printStackTrace();}
        }

        return aLCGrpContSet;
    }

    public LCGrpContSet executeQuery(String sql)  {
        Statement stmt = null;
        ResultSet rs = null;
        LCGrpContSet aLCGrpContSet = new LCGrpContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;
                if (i>10000) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "SQL语句返回结果集太多，请换用其他方式!";
                    this.mErrors .addOneError(tError);
                        break;
                }
                LCGrpContSchema s1 = new LCGrpContSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCGrpContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCGrpContSet;
    }

    public LCGrpContSet query(int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCGrpContSet aLCGrpContSet = new LCGrpContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCGrpCont");
            LCGrpContSchema aSchema = this.getSchema();
            sqlObj.setSQL(5,aSchema);
            String sql = sqlObj.getSQL();

            rs = stmt.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCGrpContSchema s1 = new LCGrpContSchema();
                s1.setSchema(rs,i);
                aLCGrpContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "query";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCGrpContSet;
    }

    public LCGrpContSet executeQuery(String sql, int nStart, int nCount) {
        Statement stmt = null;
        ResultSet rs = null;
        LCGrpContSet aLCGrpContSet = new LCGrpContSet();

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

            rs = stmt.executeQuery(StrTool.GBKToUnicode(sql));
            int i = 0;
            while (rs.next()) {
                i++;

                if( i < nStart ) {
                    continue;
                }

                if( i >= nStart + nCount ) {
                    break;
                }

                LCGrpContSchema s1 = new LCGrpContSchema();
                if (!s1.setSchema(rs,i)) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LCGrpContDB";
                    tError.functionName = "executeQuery";
                    tError.errorMessage = "sql语句有误，请查看表名及字段名信息!";
                    this.mErrors .addOneError(tError);
                }
                aLCGrpContSet.add(s1);
            }
            try{ rs.close(); } catch( Exception ex ) {}
            try{ stmt.close(); } catch( Exception ex1 ) {}
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "executeQuery";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ rs.close(); } catch( Exception ex2 ) {}
            try{ stmt.close(); } catch( Exception ex3 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return aLCGrpContSet;
    }

    public boolean update(String strWherePart) {
        Statement stmt = null;

        if (!mflag) {
            con = DBConnPool.getConnection();
        }

        try {
            stmt = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            SQLString sqlObj = new SQLString("LCGrpCont");
            LCGrpContSchema aSchema = this.getSchema();
            sqlObj.setSQL(2,aSchema);
            String sql = "update LCGrpCont " + sqlObj.getUpdPart() + " where " + strWherePart;

            int operCount = stmt.executeUpdate(sql);
            if (operCount == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LCGrpContDB";
                tError.functionName = "update";
                tError.errorMessage = "更新数据失败!";
                this.mErrors .addOneError(tError);

                if (!mflag) {
                    try {
                        con.close();
                    }
                    catch(Exception et){}
                }
                return false;
            }
        }
        catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "update";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors .addOneError(tError);

            try{ stmt.close(); } catch( Exception ex1 ) {}

            if (!mflag) {
                try {
                    con.close();
                }
                catch(Exception et){}
            }
            return false;
        }
        // 断开数据库连接
        if (!mflag) {
            try {
                con.close();
            }
            catch(Exception e){}
        }

        return true;
    }

    /**
     * 准备数据查询条件
     * @param strSQL String
     * @return boolean
     */
    public boolean prepareData(String strSQL) {
        if (mResultSet != null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "prepareData";
            tError.errorMessage = "数据集非空，程序在准备数据集之后，没有关闭！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!mflag) {
            con = DBConnPool.getConnection();
        }
        try {
            mStatement = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            mResultSet = mStatement.executeQuery(StrTool.GBKToUnicode(strSQL));
        }
        catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "prepareData";
            tError.errorMessage = ExceptionUtils.exceptionToString(e);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }

        if (!mflag) {
            try {
                con.close();
            }
            catch (Exception e) {}
        }
        return true;
    }

    /**
     * 获取数据集
     * @return boolean
     */
    public boolean hasMoreData() {
        boolean flag = true;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            flag = mResultSet.next();
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "hasMoreData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return false;
        }
        return flag;
    }
    /**
     * 获取定量数据
     * @return LCGrpContSet
     */
    public LCGrpContSet getData() {
        int tCount = 0;
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContSchema tLCGrpContSchema = null;
        if (null == mResultSet) {
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "getData";
            tError.errorMessage = "数据集为空，请先准备数据集！";
            this.mErrors.addOneError(tError);
            return null;
        }
        try {
            tCount = 1;
            tLCGrpContSchema = new LCGrpContSchema();
            tLCGrpContSchema.setSchema(mResultSet, 1);
            tLCGrpContSet.add(tLCGrpContSchema);
            //注意mResultSet.next()的作用
            while (tCount++ < SysConst.FETCHCOUNT) {
                if (mResultSet.next()) {
                    tLCGrpContSchema = new LCGrpContSchema();
                    tLCGrpContSchema.setSchema(mResultSet, 1);
                    tLCGrpContSet.add(tLCGrpContSchema);
                }
            }
        }
        catch (Exception ex) {
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "getData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors.addOneError(tError);
            try {
                mResultSet.close();
                mResultSet = null;
            }
            catch (Exception ex2) {}
            try {
                mStatement.close();
                mStatement = null;
            }
            catch (Exception ex3) {}
            if (!mflag) {
                try {
                    con.close();
                }
                catch (Exception et) {}
            }
            return null;
        }
        return tLCGrpContSet;
    }
    /**
     * 关闭数据集
     * @return boolean
     */
    public boolean closeData() {
        boolean flag = true;
        try {
            if (null == mResultSet) {
                CError tError = new CError();
                tError.moduleName = "LCGrpContDB";
                tError.functionName = "closeData";
                tError.errorMessage = "数据集已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mResultSet.close();
                mResultSet = null;
            }
        }
        catch (Exception ex2) {
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex2);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        try {
            if (null == mStatement) {
                CError tError = new CError();
                tError.moduleName = "LCGrpContDB";
                tError.functionName = "closeData";
                tError.errorMessage = "语句已经关闭了！";
                this.mErrors.addOneError(tError);
                flag = false;
            } else {
                mStatement.close();
                mStatement = null;
            }
        }
        catch (Exception ex3) {
            CError tError = new CError();
            tError.moduleName = "LCGrpContDB";
            tError.functionName = "closeData";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex3);
            this.mErrors.addOneError(tError);
            flag = false;
        }
        return flag;
    }
}
