/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vschema;

import com.sinosoft.lis.schema.LDSchemeSchema;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LDSchemeSet </p>
 * <p>Description: LDSchemeSchemaSet类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-15
 */
public class LDSchemeSet extends SchemaSet {
	// @Method
	public boolean add(LDSchemeSchema aSchema) {
		return super.add(aSchema);
	}

	public boolean add(LDSchemeSet aSet) {
		return super.add(aSet);
	}

	public boolean remove(LDSchemeSchema aSchema) {
		return super.remove(aSchema);
	}

	public LDSchemeSchema get(int index) {
		LDSchemeSchema tSchema = (LDSchemeSchema)super.getObj(index);
		return tSchema;
	}

	public boolean set(int index, LDSchemeSchema aSchema) {
		return super.set(index,aSchema);
	}

	public boolean set(LDSchemeSet aSet) {
		return super.set(aSet);
	}

	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDScheme描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode() {
		StringBuffer strReturn = new StringBuffer("");
		int n = this.size();
		for (int i = 1; i <= n; i++) {
			LDSchemeSchema aSchema = this.get(i);
			strReturn.append(aSchema.encode());
			if( i != n ) strReturn.append(SysConst.RECORDSPLITER);
		}

		return strReturn.toString();
	}

	/**
	* 数据解包
	* @param: str String 打包后字符串
	* @return: boolean
	**/
	public boolean decode( String str ) {
		int nBeginPos = 0;
		int nEndPos = str.indexOf('^');
		this.clear();

		while( nEndPos != -1 ) {
			LDSchemeSchema aSchema = new LDSchemeSchema();
			if (aSchema.decode(str.substring(nBeginPos, nEndPos))) {
			    this.add(aSchema);
			    nBeginPos = nEndPos + 1;
			    nEndPos = str.indexOf('^', nEndPos + 1);
			} else {
				// @@错误处理
				this.mErrors.copyAllErrors( aSchema.mErrors );
				return false;
			}
		}
		LDSchemeSchema tSchema = new LDSchemeSchema();
		if(tSchema.decode(str.substring(nBeginPos))) {
		    this.add(tSchema);
		    return true;
		} else {
			// @@错误处理
			this.mErrors.copyAllErrors( tSchema.mErrors );
			return false;
		}
	}

}
