/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCInsuredImportSubDB;

/**
 * <p>ClassName: LCInsuredImportSubSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LCInsuredImportSubSchema implements Schema, Cloneable {
    // @Field
    /** 序列号 */
    private String SerialNo;
    /** 业务号类型 */
    private String OtherNoType;
    /** 业务号 */
    private String OtherNo;
    /** 投保人客户号 */
    private String AppntNo;
    /** 校验标识 */
    private String CheckFlag;
    /** 投保单号 */
    private String PrtNo;
    /** 团体保单号 */
    private String GrpContNo;
    /** 批次号 */
    private String BatchNo;
    /** Id */
    private String ID;
    /** 合同id */
    private String ContNo;
    /** 客户号 */
    private String InsuredNo;
    /** 主被保险人合同id */
    private String MainInsuredNo;
    /** 状态 */
    private String CustomerState;
    /** 主被保险人姓名 */
    private String MainInsuredName;
    /** 主被保险人证件号 */
    private String MainInsuredIDNo;
    /** 被保人姓名 */
    private String InsuredName;
    /** 与主被保险人关系 */
    private String EmployeeRelation;
    /** 性别 */
    private String InsuredSex;
    /** 出生日期 */
    private String InsuredBirthday;
    /** 证件类型 */
    private String InsuredIDType;
    /** 证件号码 */
    private String InsuredIDNo;
    /** 户籍 */
    private String NativePlace;
    /** 服务机构 */
    private String RgtAddress;
    /** 婚姻状况 */
    private String Marriage;
    /** 民族 */
    private String Nationality;
    /** 保险计划 */
    private String ContPlan;
    /** 职业类别 */
    private String OccupationType;
    /** 职业代码 */
    private String OccupationCode;
    /** 兼业工种 */
    private String PluralityType;
    /** 理赔金转帐银行 */
    private String BankCode1;
    /** 户名 */
    private String AccName1;
    /** 帐号 */
    private String BankAccNo1;
    /** 入司日期 */
    private String JoinCompanyDate;
    /** 工资 */
    private String Salary;
    /** 保单类型标记 */
    private String PolTypeFlag;
    /** 被保人人数 */
    private String InsuredPeoples;
    /** 邮件 */
    private String EMail;
    /** 移动电话 */
    private String Mobile;
    /** 卡单编码 */
    private String CertifyCode;
    /** 卡单起号 */
    private String StartCode;
    /** 卡单止号 */
    private String EndCode;
    /** 保存计划或是否有连带被保人 */
    private String CalStandbyFlag1;
    /** 连带人数 */
    private String CalStandbyFlag2;
    /** 连带被保人连带险种 */
    private String RelationToRisk;
    /** 生效日期 */
    private String ContCValiDate;
    /** 团险合同号 */
    private String GrpNo;
    /** 与投保人关系 */
    private String RelationToAppnt;
    /** 社保标记 */
    private String SocialInsuFlag;
    /** 与员工关系 */
    private String RelationToMainInsured;
    /** 健康服务标识 */
    private String HealthServiceName;
    /** 是否医保身份 */
    private String HealthInsFlagName;
    /** 保险组别号 */
    private String GrpAlias;
    /** 是否续保 */
    private String WhetherToRnew;
    /** 开户银行 */
    private String BankCode;
    /** 开户行所在省 */
    private String BankProvince;
    /** 开户行所在市 */
    private String BankCity;
    /** 银行网点 */
    private String BankLocations;
    /** 账户名 */
    private String AccName;
    /** 账号 */
    private String BankAccNo;
    /** 卡折类型 */
    private String AccType;
    /** 被保人工号 */
    private String BeInsuredNo;
    /** 身份证有效期 */
    private String IDExpDate;
    /** 是否中国税收居民 */
    private String TaxpayerIDType;
    /** 所属公司编号 */
    private String CompanyNo;
    /** 所属公司名称 */
    private String CompanyName;
    /** 导入标志 */
    private String Flag;
    /** 错误信息 */
    private String ErrorInfo;
    /** 操作人 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModIfyTime;
    /** 电子个人凭证地址 */
    private String E_PolicyID;

    public static final int FIELDNUM = 73;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LCInsuredImportSubSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCInsuredImportSubSchema cloned = (LCInsuredImportSubSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }
    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }
    public String getOtherNoType() {
        return OtherNoType;
    }
    public void setOtherNoType(String aOtherNoType) {
        OtherNoType = aOtherNoType;
    }
    public String getOtherNo() {
        return OtherNo;
    }
    public void setOtherNo(String aOtherNo) {
        OtherNo = aOtherNo;
    }
    public String getAppntNo() {
        return AppntNo;
    }
    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }
    public String getCheckFlag() {
        return CheckFlag;
    }
    public void setCheckFlag(String aCheckFlag) {
        CheckFlag = aCheckFlag;
    }
    public String getPrtNo() {
        return PrtNo;
    }
    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }
    public String getGrpContNo() {
        return GrpContNo;
    }
    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }
    public String getBatchNo() {
        return BatchNo;
    }
    public void setBatchNo(String aBatchNo) {
        BatchNo = aBatchNo;
    }
    public String getID() {
        return ID;
    }
    public void setID(String aID) {
        ID = aID;
    }
    public String getContNo() {
        return ContNo;
    }
    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }
    public String getInsuredNo() {
        return InsuredNo;
    }
    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }
    public String getMainInsuredNo() {
        return MainInsuredNo;
    }
    public void setMainInsuredNo(String aMainInsuredNo) {
        MainInsuredNo = aMainInsuredNo;
    }
    public String getCustomerState() {
        return CustomerState;
    }
    public void setCustomerState(String aCustomerState) {
        CustomerState = aCustomerState;
    }
    public String getMainInsuredName() {
        return MainInsuredName;
    }
    public void setMainInsuredName(String aMainInsuredName) {
        MainInsuredName = aMainInsuredName;
    }
    public String getMainInsuredIDNo() {
        return MainInsuredIDNo;
    }
    public void setMainInsuredIDNo(String aMainInsuredIDNo) {
        MainInsuredIDNo = aMainInsuredIDNo;
    }
    public String getInsuredName() {
        return InsuredName;
    }
    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }
    public String getEmployeeRelation() {
        return EmployeeRelation;
    }
    public void setEmployeeRelation(String aEmployeeRelation) {
        EmployeeRelation = aEmployeeRelation;
    }
    public String getInsuredSex() {
        return InsuredSex;
    }
    public void setInsuredSex(String aInsuredSex) {
        InsuredSex = aInsuredSex;
    }
    public String getInsuredBirthday() {
        return InsuredBirthday;
    }
    public void setInsuredBirthday(String aInsuredBirthday) {
        InsuredBirthday = aInsuredBirthday;
    }
    public String getInsuredIDType() {
        return InsuredIDType;
    }
    public void setInsuredIDType(String aInsuredIDType) {
        InsuredIDType = aInsuredIDType;
    }
    public String getInsuredIDNo() {
        return InsuredIDNo;
    }
    public void setInsuredIDNo(String aInsuredIDNo) {
        InsuredIDNo = aInsuredIDNo;
    }
    public String getNativePlace() {
        return NativePlace;
    }
    public void setNativePlace(String aNativePlace) {
        NativePlace = aNativePlace;
    }
    public String getRgtAddress() {
        return RgtAddress;
    }
    public void setRgtAddress(String aRgtAddress) {
        RgtAddress = aRgtAddress;
    }
    public String getMarriage() {
        return Marriage;
    }
    public void setMarriage(String aMarriage) {
        Marriage = aMarriage;
    }
    public String getNationality() {
        return Nationality;
    }
    public void setNationality(String aNationality) {
        Nationality = aNationality;
    }
    public String getContPlan() {
        return ContPlan;
    }
    public void setContPlan(String aContPlan) {
        ContPlan = aContPlan;
    }
    public String getOccupationType() {
        return OccupationType;
    }
    public void setOccupationType(String aOccupationType) {
        OccupationType = aOccupationType;
    }
    public String getOccupationCode() {
        return OccupationCode;
    }
    public void setOccupationCode(String aOccupationCode) {
        OccupationCode = aOccupationCode;
    }
    public String getPluralityType() {
        return PluralityType;
    }
    public void setPluralityType(String aPluralityType) {
        PluralityType = aPluralityType;
    }
    public String getBankCode1() {
        return BankCode1;
    }
    public void setBankCode1(String aBankCode1) {
        BankCode1 = aBankCode1;
    }
    public String getAccName1() {
        return AccName1;
    }
    public void setAccName1(String aAccName1) {
        AccName1 = aAccName1;
    }
    public String getBankAccNo1() {
        return BankAccNo1;
    }
    public void setBankAccNo1(String aBankAccNo1) {
        BankAccNo1 = aBankAccNo1;
    }
    public String getJoinCompanyDate() {
        return JoinCompanyDate;
    }
    public void setJoinCompanyDate(String aJoinCompanyDate) {
        JoinCompanyDate = aJoinCompanyDate;
    }
    public String getSalary() {
        return Salary;
    }
    public void setSalary(String aSalary) {
        Salary = aSalary;
    }
    public String getPolTypeFlag() {
        return PolTypeFlag;
    }
    public void setPolTypeFlag(String aPolTypeFlag) {
        PolTypeFlag = aPolTypeFlag;
    }
    public String getInsuredPeoples() {
        return InsuredPeoples;
    }
    public void setInsuredPeoples(String aInsuredPeoples) {
        InsuredPeoples = aInsuredPeoples;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getCertifyCode() {
        return CertifyCode;
    }
    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }
    public String getStartCode() {
        return StartCode;
    }
    public void setStartCode(String aStartCode) {
        StartCode = aStartCode;
    }
    public String getEndCode() {
        return EndCode;
    }
    public void setEndCode(String aEndCode) {
        EndCode = aEndCode;
    }
    public String getCalStandbyFlag1() {
        return CalStandbyFlag1;
    }
    public void setCalStandbyFlag1(String aCalStandbyFlag1) {
        CalStandbyFlag1 = aCalStandbyFlag1;
    }
    public String getCalStandbyFlag2() {
        return CalStandbyFlag2;
    }
    public void setCalStandbyFlag2(String aCalStandbyFlag2) {
        CalStandbyFlag2 = aCalStandbyFlag2;
    }
    public String getRelationToRisk() {
        return RelationToRisk;
    }
    public void setRelationToRisk(String aRelationToRisk) {
        RelationToRisk = aRelationToRisk;
    }
    public String getContCValiDate() {
        return ContCValiDate;
    }
    public void setContCValiDate(String aContCValiDate) {
        ContCValiDate = aContCValiDate;
    }
    public String getGrpNo() {
        return GrpNo;
    }
    public void setGrpNo(String aGrpNo) {
        GrpNo = aGrpNo;
    }
    public String getRelationToAppnt() {
        return RelationToAppnt;
    }
    public void setRelationToAppnt(String aRelationToAppnt) {
        RelationToAppnt = aRelationToAppnt;
    }
    public String getSocialInsuFlag() {
        return SocialInsuFlag;
    }
    public void setSocialInsuFlag(String aSocialInsuFlag) {
        SocialInsuFlag = aSocialInsuFlag;
    }
    public String getRelationToMainInsured() {
        return RelationToMainInsured;
    }
    public void setRelationToMainInsured(String aRelationToMainInsured) {
        RelationToMainInsured = aRelationToMainInsured;
    }
    public String getHealthServiceName() {
        return HealthServiceName;
    }
    public void setHealthServiceName(String aHealthServiceName) {
        HealthServiceName = aHealthServiceName;
    }
    public String getHealthInsFlagName() {
        return HealthInsFlagName;
    }
    public void setHealthInsFlagName(String aHealthInsFlagName) {
        HealthInsFlagName = aHealthInsFlagName;
    }
    public String getGrpAlias() {
        return GrpAlias;
    }
    public void setGrpAlias(String aGrpAlias) {
        GrpAlias = aGrpAlias;
    }
    public String getWhetherToRnew() {
        return WhetherToRnew;
    }
    public void setWhetherToRnew(String aWhetherToRnew) {
        WhetherToRnew = aWhetherToRnew;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankProvince() {
        return BankProvince;
    }
    public void setBankProvince(String aBankProvince) {
        BankProvince = aBankProvince;
    }
    public String getBankCity() {
        return BankCity;
    }
    public void setBankCity(String aBankCity) {
        BankCity = aBankCity;
    }
    public String getBankLocations() {
        return BankLocations;
    }
    public void setBankLocations(String aBankLocations) {
        BankLocations = aBankLocations;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getBeInsuredNo() {
        return BeInsuredNo;
    }
    public void setBeInsuredNo(String aBeInsuredNo) {
        BeInsuredNo = aBeInsuredNo;
    }
    public String getIDExpDate() {
        return IDExpDate;
    }
    public void setIDExpDate(String aIDExpDate) {
        IDExpDate = aIDExpDate;
    }
    public String getTaxpayerIDType() {
        return TaxpayerIDType;
    }
    public void setTaxpayerIDType(String aTaxpayerIDType) {
        TaxpayerIDType = aTaxpayerIDType;
    }
    public String getCompanyNo() {
        return CompanyNo;
    }
    public void setCompanyNo(String aCompanyNo) {
        CompanyNo = aCompanyNo;
    }
    public String getCompanyName() {
        return CompanyName;
    }
    public void setCompanyName(String aCompanyName) {
        CompanyName = aCompanyName;
    }
    public String getFlag() {
        return Flag;
    }
    public void setFlag(String aFlag) {
        Flag = aFlag;
    }
    public String getErrorInfo() {
        return ErrorInfo;
    }
    public void setErrorInfo(String aErrorInfo) {
        ErrorInfo = aErrorInfo;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModIfyTime() {
        return ModIfyTime;
    }
    public void setModIfyTime(String aModIfyTime) {
        ModIfyTime = aModIfyTime;
    }
    public String getE_PolicyID() {
        return E_PolicyID;
    }
    public void setE_PolicyID(String aE_PolicyID) {
        E_PolicyID = aE_PolicyID;
    }

    /**
    * 使用另外一个 LCInsuredImportSubSchema 对象给 Schema 赋值
    * @param: aLCInsuredImportSubSchema LCInsuredImportSubSchema
    **/
    public void setSchema(LCInsuredImportSubSchema aLCInsuredImportSubSchema) {
        this.SerialNo = aLCInsuredImportSubSchema.getSerialNo();
        this.OtherNoType = aLCInsuredImportSubSchema.getOtherNoType();
        this.OtherNo = aLCInsuredImportSubSchema.getOtherNo();
        this.AppntNo = aLCInsuredImportSubSchema.getAppntNo();
        this.CheckFlag = aLCInsuredImportSubSchema.getCheckFlag();
        this.PrtNo = aLCInsuredImportSubSchema.getPrtNo();
        this.GrpContNo = aLCInsuredImportSubSchema.getGrpContNo();
        this.BatchNo = aLCInsuredImportSubSchema.getBatchNo();
        this.ID = aLCInsuredImportSubSchema.getID();
        this.ContNo = aLCInsuredImportSubSchema.getContNo();
        this.InsuredNo = aLCInsuredImportSubSchema.getInsuredNo();
        this.MainInsuredNo = aLCInsuredImportSubSchema.getMainInsuredNo();
        this.CustomerState = aLCInsuredImportSubSchema.getCustomerState();
        this.MainInsuredName = aLCInsuredImportSubSchema.getMainInsuredName();
        this.MainInsuredIDNo = aLCInsuredImportSubSchema.getMainInsuredIDNo();
        this.InsuredName = aLCInsuredImportSubSchema.getInsuredName();
        this.EmployeeRelation = aLCInsuredImportSubSchema.getEmployeeRelation();
        this.InsuredSex = aLCInsuredImportSubSchema.getInsuredSex();
        this.InsuredBirthday = aLCInsuredImportSubSchema.getInsuredBirthday();
        this.InsuredIDType = aLCInsuredImportSubSchema.getInsuredIDType();
        this.InsuredIDNo = aLCInsuredImportSubSchema.getInsuredIDNo();
        this.NativePlace = aLCInsuredImportSubSchema.getNativePlace();
        this.RgtAddress = aLCInsuredImportSubSchema.getRgtAddress();
        this.Marriage = aLCInsuredImportSubSchema.getMarriage();
        this.Nationality = aLCInsuredImportSubSchema.getNationality();
        this.ContPlan = aLCInsuredImportSubSchema.getContPlan();
        this.OccupationType = aLCInsuredImportSubSchema.getOccupationType();
        this.OccupationCode = aLCInsuredImportSubSchema.getOccupationCode();
        this.PluralityType = aLCInsuredImportSubSchema.getPluralityType();
        this.BankCode1 = aLCInsuredImportSubSchema.getBankCode1();
        this.AccName1 = aLCInsuredImportSubSchema.getAccName1();
        this.BankAccNo1 = aLCInsuredImportSubSchema.getBankAccNo1();
        this.JoinCompanyDate = aLCInsuredImportSubSchema.getJoinCompanyDate();
        this.Salary = aLCInsuredImportSubSchema.getSalary();
        this.PolTypeFlag = aLCInsuredImportSubSchema.getPolTypeFlag();
        this.InsuredPeoples = aLCInsuredImportSubSchema.getInsuredPeoples();
        this.EMail = aLCInsuredImportSubSchema.getEMail();
        this.Mobile = aLCInsuredImportSubSchema.getMobile();
        this.CertifyCode = aLCInsuredImportSubSchema.getCertifyCode();
        this.StartCode = aLCInsuredImportSubSchema.getStartCode();
        this.EndCode = aLCInsuredImportSubSchema.getEndCode();
        this.CalStandbyFlag1 = aLCInsuredImportSubSchema.getCalStandbyFlag1();
        this.CalStandbyFlag2 = aLCInsuredImportSubSchema.getCalStandbyFlag2();
        this.RelationToRisk = aLCInsuredImportSubSchema.getRelationToRisk();
        this.ContCValiDate = aLCInsuredImportSubSchema.getContCValiDate();
        this.GrpNo = aLCInsuredImportSubSchema.getGrpNo();
        this.RelationToAppnt = aLCInsuredImportSubSchema.getRelationToAppnt();
        this.SocialInsuFlag = aLCInsuredImportSubSchema.getSocialInsuFlag();
        this.RelationToMainInsured = aLCInsuredImportSubSchema.getRelationToMainInsured();
        this.HealthServiceName = aLCInsuredImportSubSchema.getHealthServiceName();
        this.HealthInsFlagName = aLCInsuredImportSubSchema.getHealthInsFlagName();
        this.GrpAlias = aLCInsuredImportSubSchema.getGrpAlias();
        this.WhetherToRnew = aLCInsuredImportSubSchema.getWhetherToRnew();
        this.BankCode = aLCInsuredImportSubSchema.getBankCode();
        this.BankProvince = aLCInsuredImportSubSchema.getBankProvince();
        this.BankCity = aLCInsuredImportSubSchema.getBankCity();
        this.BankLocations = aLCInsuredImportSubSchema.getBankLocations();
        this.AccName = aLCInsuredImportSubSchema.getAccName();
        this.BankAccNo = aLCInsuredImportSubSchema.getBankAccNo();
        this.AccType = aLCInsuredImportSubSchema.getAccType();
        this.BeInsuredNo = aLCInsuredImportSubSchema.getBeInsuredNo();
        this.IDExpDate = aLCInsuredImportSubSchema.getIDExpDate();
        this.TaxpayerIDType = aLCInsuredImportSubSchema.getTaxpayerIDType();
        this.CompanyNo = aLCInsuredImportSubSchema.getCompanyNo();
        this.CompanyName = aLCInsuredImportSubSchema.getCompanyName();
        this.Flag = aLCInsuredImportSubSchema.getFlag();
        this.ErrorInfo = aLCInsuredImportSubSchema.getErrorInfo();
        this.Operator = aLCInsuredImportSubSchema.getOperator();
        this.MakeDate = fDate.getDate( aLCInsuredImportSubSchema.getMakeDate());
        this.MakeTime = aLCInsuredImportSubSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLCInsuredImportSubSchema.getModifyDate());
        this.ModIfyTime = aLCInsuredImportSubSchema.getModIfyTime();
        this.E_PolicyID = aLCInsuredImportSubSchema.getE_PolicyID();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SerialNo") == null )
                this.SerialNo = null;
            else
                this.SerialNo = rs.getString("SerialNo").trim();

            if( rs.getString("OtherNoType") == null )
                this.OtherNoType = null;
            else
                this.OtherNoType = rs.getString("OtherNoType").trim();

            if( rs.getString("OtherNo") == null )
                this.OtherNo = null;
            else
                this.OtherNo = rs.getString("OtherNo").trim();

            if( rs.getString("AppntNo") == null )
                this.AppntNo = null;
            else
                this.AppntNo = rs.getString("AppntNo").trim();

            if( rs.getString("CheckFlag") == null )
                this.CheckFlag = null;
            else
                this.CheckFlag = rs.getString("CheckFlag").trim();

            if( rs.getString("PrtNo") == null )
                this.PrtNo = null;
            else
                this.PrtNo = rs.getString("PrtNo").trim();

            if( rs.getString("GrpContNo") == null )
                this.GrpContNo = null;
            else
                this.GrpContNo = rs.getString("GrpContNo").trim();

            if( rs.getString("BatchNo") == null )
                this.BatchNo = null;
            else
                this.BatchNo = rs.getString("BatchNo").trim();

            if( rs.getString("ID") == null )
                this.ID = null;
            else
                this.ID = rs.getString("ID").trim();

            if( rs.getString("ContNo") == null )
                this.ContNo = null;
            else
                this.ContNo = rs.getString("ContNo").trim();

            if( rs.getString("InsuredNo") == null )
                this.InsuredNo = null;
            else
                this.InsuredNo = rs.getString("InsuredNo").trim();

            if( rs.getString("MainInsuredNo") == null )
                this.MainInsuredNo = null;
            else
                this.MainInsuredNo = rs.getString("MainInsuredNo").trim();

            if( rs.getString("CustomerState") == null )
                this.CustomerState = null;
            else
                this.CustomerState = rs.getString("CustomerState").trim();

            if( rs.getString("MainInsuredName") == null )
                this.MainInsuredName = null;
            else
                this.MainInsuredName = rs.getString("MainInsuredName").trim();

            if( rs.getString("MainInsuredIDNo") == null )
                this.MainInsuredIDNo = null;
            else
                this.MainInsuredIDNo = rs.getString("MainInsuredIDNo").trim();

            if( rs.getString("InsuredName") == null )
                this.InsuredName = null;
            else
                this.InsuredName = rs.getString("InsuredName").trim();

            if( rs.getString("EmployeeRelation") == null )
                this.EmployeeRelation = null;
            else
                this.EmployeeRelation = rs.getString("EmployeeRelation").trim();

            if( rs.getString("InsuredSex") == null )
                this.InsuredSex = null;
            else
                this.InsuredSex = rs.getString("InsuredSex").trim();

            if( rs.getString("InsuredBirthday") == null )
                this.InsuredBirthday = null;
            else
                this.InsuredBirthday = rs.getString("InsuredBirthday").trim();

            if( rs.getString("InsuredIDType") == null )
                this.InsuredIDType = null;
            else
                this.InsuredIDType = rs.getString("InsuredIDType").trim();

            if( rs.getString("InsuredIDNo") == null )
                this.InsuredIDNo = null;
            else
                this.InsuredIDNo = rs.getString("InsuredIDNo").trim();

            if( rs.getString("NativePlace") == null )
                this.NativePlace = null;
            else
                this.NativePlace = rs.getString("NativePlace").trim();

            if( rs.getString("RgtAddress") == null )
                this.RgtAddress = null;
            else
                this.RgtAddress = rs.getString("RgtAddress").trim();

            if( rs.getString("Marriage") == null )
                this.Marriage = null;
            else
                this.Marriage = rs.getString("Marriage").trim();

            if( rs.getString("Nationality") == null )
                this.Nationality = null;
            else
                this.Nationality = rs.getString("Nationality").trim();

            if( rs.getString("ContPlan") == null )
                this.ContPlan = null;
            else
                this.ContPlan = rs.getString("ContPlan").trim();

            if( rs.getString("OccupationType") == null )
                this.OccupationType = null;
            else
                this.OccupationType = rs.getString("OccupationType").trim();

            if( rs.getString("OccupationCode") == null )
                this.OccupationCode = null;
            else
                this.OccupationCode = rs.getString("OccupationCode").trim();

            if( rs.getString("PluralityType") == null )
                this.PluralityType = null;
            else
                this.PluralityType = rs.getString("PluralityType").trim();

            if( rs.getString("BankCode1") == null )
                this.BankCode1 = null;
            else
                this.BankCode1 = rs.getString("BankCode1").trim();

            if( rs.getString("AccName1") == null )
                this.AccName1 = null;
            else
                this.AccName1 = rs.getString("AccName1").trim();

            if( rs.getString("BankAccNo1") == null )
                this.BankAccNo1 = null;
            else
                this.BankAccNo1 = rs.getString("BankAccNo1").trim();

            if( rs.getString("JoinCompanyDate") == null )
                this.JoinCompanyDate = null;
            else
                this.JoinCompanyDate = rs.getString("JoinCompanyDate").trim();

            if( rs.getString("Salary") == null )
                this.Salary = null;
            else
                this.Salary = rs.getString("Salary").trim();

            if( rs.getString("PolTypeFlag") == null )
                this.PolTypeFlag = null;
            else
                this.PolTypeFlag = rs.getString("PolTypeFlag").trim();

            if( rs.getString("InsuredPeoples") == null )
                this.InsuredPeoples = null;
            else
                this.InsuredPeoples = rs.getString("InsuredPeoples").trim();

            if( rs.getString("EMail") == null )
                this.EMail = null;
            else
                this.EMail = rs.getString("EMail").trim();

            if( rs.getString("Mobile") == null )
                this.Mobile = null;
            else
                this.Mobile = rs.getString("Mobile").trim();

            if( rs.getString("CertifyCode") == null )
                this.CertifyCode = null;
            else
                this.CertifyCode = rs.getString("CertifyCode").trim();

            if( rs.getString("StartCode") == null )
                this.StartCode = null;
            else
                this.StartCode = rs.getString("StartCode").trim();

            if( rs.getString("EndCode") == null )
                this.EndCode = null;
            else
                this.EndCode = rs.getString("EndCode").trim();

            if( rs.getString("CalStandbyFlag1") == null )
                this.CalStandbyFlag1 = null;
            else
                this.CalStandbyFlag1 = rs.getString("CalStandbyFlag1").trim();

            if( rs.getString("CalStandbyFlag2") == null )
                this.CalStandbyFlag2 = null;
            else
                this.CalStandbyFlag2 = rs.getString("CalStandbyFlag2").trim();

            if( rs.getString("RelationToRisk") == null )
                this.RelationToRisk = null;
            else
                this.RelationToRisk = rs.getString("RelationToRisk").trim();

            if( rs.getString("ContCValiDate") == null )
                this.ContCValiDate = null;
            else
                this.ContCValiDate = rs.getString("ContCValiDate").trim();

            if( rs.getString("GrpNo") == null )
                this.GrpNo = null;
            else
                this.GrpNo = rs.getString("GrpNo").trim();

            if( rs.getString("RelationToAppnt") == null )
                this.RelationToAppnt = null;
            else
                this.RelationToAppnt = rs.getString("RelationToAppnt").trim();

            if( rs.getString("SocialInsuFlag") == null )
                this.SocialInsuFlag = null;
            else
                this.SocialInsuFlag = rs.getString("SocialInsuFlag").trim();

            if( rs.getString("RelationToMainInsured") == null )
                this.RelationToMainInsured = null;
            else
                this.RelationToMainInsured = rs.getString("RelationToMainInsured").trim();

            if( rs.getString("HealthServiceName") == null )
                this.HealthServiceName = null;
            else
                this.HealthServiceName = rs.getString("HealthServiceName").trim();

            if( rs.getString("HealthInsFlagName") == null )
                this.HealthInsFlagName = null;
            else
                this.HealthInsFlagName = rs.getString("HealthInsFlagName").trim();

            if( rs.getString("GrpAlias") == null )
                this.GrpAlias = null;
            else
                this.GrpAlias = rs.getString("GrpAlias").trim();

            if( rs.getString("WhetherToRnew") == null )
                this.WhetherToRnew = null;
            else
                this.WhetherToRnew = rs.getString("WhetherToRnew").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankProvince") == null )
                this.BankProvince = null;
            else
                this.BankProvince = rs.getString("BankProvince").trim();

            if( rs.getString("BankCity") == null )
                this.BankCity = null;
            else
                this.BankCity = rs.getString("BankCity").trim();

            if( rs.getString("BankLocations") == null )
                this.BankLocations = null;
            else
                this.BankLocations = rs.getString("BankLocations").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("AccType") == null )
                this.AccType = null;
            else
                this.AccType = rs.getString("AccType").trim();

            if( rs.getString("BeInsuredNo") == null )
                this.BeInsuredNo = null;
            else
                this.BeInsuredNo = rs.getString("BeInsuredNo").trim();

            if( rs.getString("IDExpDate") == null )
                this.IDExpDate = null;
            else
                this.IDExpDate = rs.getString("IDExpDate").trim();

            if( rs.getString("TaxpayerIDType") == null )
                this.TaxpayerIDType = null;
            else
                this.TaxpayerIDType = rs.getString("TaxpayerIDType").trim();

            if( rs.getString("CompanyNo") == null )
                this.CompanyNo = null;
            else
                this.CompanyNo = rs.getString("CompanyNo").trim();

            if( rs.getString("CompanyName") == null )
                this.CompanyName = null;
            else
                this.CompanyName = rs.getString("CompanyName").trim();

            if( rs.getString("Flag") == null )
                this.Flag = null;
            else
                this.Flag = rs.getString("Flag").trim();

            if( rs.getString("ErrorInfo") == null )
                this.ErrorInfo = null;
            else
                this.ErrorInfo = rs.getString("ErrorInfo").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModIfyTime") == null )
                this.ModIfyTime = null;
            else
                this.ModIfyTime = rs.getString("ModIfyTime").trim();

            if( rs.getString("E_PolicyID") == null )
                this.E_PolicyID = null;
            else
                this.E_PolicyID = rs.getString("E_PolicyID").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInsuredImportSubSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LCInsuredImportSubSchema getSchema() {
        LCInsuredImportSubSchema aLCInsuredImportSubSchema = new LCInsuredImportSubSchema();
        aLCInsuredImportSubSchema.setSchema(this);
        return aLCInsuredImportSubSchema;
    }

    public LCInsuredImportSubDB getDB() {
        LCInsuredImportSubDB aDBOper = new LCInsuredImportSubDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsuredImportSub描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ID)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainInsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerState)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainInsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainInsuredIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EmployeeRelation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredSex)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredBirthday)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Marriage)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContPlan)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PluralityType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(JoinCompanyDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Salary)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolTypeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredPeoples)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StartCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EndCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalStandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalStandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToRisk)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContCValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToAppnt)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SocialInsuFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelationToMainInsured)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HealthServiceName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HealthInsFlagName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpAlias)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WhetherToRnew)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankProvince)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCity)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankLocations)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BeInsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IDExpDate)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxpayerIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CompanyName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Flag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ErrorInfo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModIfyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(E_PolicyID));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCInsuredImportSub>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            ID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
            MainInsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            CustomerState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            MainInsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            MainInsuredIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            EmployeeRelation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            InsuredBirthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            InsuredIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            InsuredIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            RgtAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            Marriage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            ContPlan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            PluralityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            BankCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            AccName1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            BankAccNo1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            JoinCompanyDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            Salary = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            PolTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            InsuredPeoples = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            StartCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            EndCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            CalStandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            CalStandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
            RelationToRisk = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
            ContCValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
            GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            RelationToAppnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            SocialInsuFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            RelationToMainInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            HealthServiceName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            HealthInsFlagName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
            GrpAlias = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            WhetherToRnew = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
            BankProvince = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
            BankCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
            BankLocations = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
            BeInsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
            IDExpDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
            TaxpayerIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
            CompanyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
            CompanyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
            Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
            ErrorInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71,SysConst.PACKAGESPILTER));
            ModIfyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
            E_PolicyID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCInsuredImportSubSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equalsIgnoreCase("CheckFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
        }
        if (FCode.equalsIgnoreCase("ID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ID));
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equalsIgnoreCase("MainInsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainInsuredNo));
        }
        if (FCode.equalsIgnoreCase("CustomerState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerState));
        }
        if (FCode.equalsIgnoreCase("MainInsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainInsuredName));
        }
        if (FCode.equalsIgnoreCase("MainInsuredIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainInsuredIDNo));
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equalsIgnoreCase("EmployeeRelation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeRelation));
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredBirthday));
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDType));
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIDNo));
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equalsIgnoreCase("ContPlan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlan));
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
        }
        if (FCode.equalsIgnoreCase("BankCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode1));
        }
        if (FCode.equalsIgnoreCase("AccName1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName1));
        }
        if (FCode.equalsIgnoreCase("BankAccNo1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo1));
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JoinCompanyDate));
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
        }
        if (FCode.equalsIgnoreCase("PolTypeFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolTypeFlag));
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredPeoples));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equalsIgnoreCase("StartCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartCode));
        }
        if (FCode.equalsIgnoreCase("EndCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndCode));
        }
        if (FCode.equalsIgnoreCase("CalStandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalStandbyFlag1));
        }
        if (FCode.equalsIgnoreCase("CalStandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalStandbyFlag2));
        }
        if (FCode.equalsIgnoreCase("RelationToRisk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToRisk));
        }
        if (FCode.equalsIgnoreCase("ContCValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContCValiDate));
        }
        if (FCode.equalsIgnoreCase("GrpNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
        }
        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
        }
        if (FCode.equalsIgnoreCase("SocialInsuFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuFlag));
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToMainInsured));
        }
        if (FCode.equalsIgnoreCase("HealthServiceName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthServiceName));
        }
        if (FCode.equalsIgnoreCase("HealthInsFlagName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HealthInsFlagName));
        }
        if (FCode.equalsIgnoreCase("GrpAlias")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAlias));
        }
        if (FCode.equalsIgnoreCase("WhetherToRnew")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WhetherToRnew));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankProvince")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankProvince));
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCity));
        }
        if (FCode.equalsIgnoreCase("BankLocations")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankLocations));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("BeInsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BeInsuredNo));
        }
        if (FCode.equalsIgnoreCase("IDExpDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDExpDate));
        }
        if (FCode.equalsIgnoreCase("TaxpayerIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxpayerIDType));
        }
        if (FCode.equalsIgnoreCase("CompanyNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyNo));
        }
        if (FCode.equalsIgnoreCase("CompanyName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyName));
        }
        if (FCode.equalsIgnoreCase("Flag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
        }
        if (FCode.equalsIgnoreCase("ErrorInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorInfo));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModIfyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModIfyTime));
        }
        if (FCode.equalsIgnoreCase("E_PolicyID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(E_PolicyID));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CheckFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(BatchNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ID);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MainInsuredNo);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(CustomerState);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(MainInsuredName);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MainInsuredIDNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(EmployeeRelation);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(InsuredSex);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(InsuredBirthday);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(InsuredIDType);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(InsuredIDNo);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(NativePlace);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(RgtAddress);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Marriage);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Nationality);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ContPlan);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(OccupationType);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(OccupationCode);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(PluralityType);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(BankCode1);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(AccName1);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo1);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(JoinCompanyDate);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(Salary);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(PolTypeFlag);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(InsuredPeoples);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(Mobile);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(CertifyCode);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(StartCode);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(EndCode);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(CalStandbyFlag1);
                break;
            case 42:
                strFieldValue = StrTool.GBKToUnicode(CalStandbyFlag2);
                break;
            case 43:
                strFieldValue = StrTool.GBKToUnicode(RelationToRisk);
                break;
            case 44:
                strFieldValue = StrTool.GBKToUnicode(ContCValiDate);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(GrpNo);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(RelationToAppnt);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(SocialInsuFlag);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(RelationToMainInsured);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(HealthServiceName);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(HealthInsFlagName);
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(GrpAlias);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(WhetherToRnew);
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(BankProvince);
                break;
            case 55:
                strFieldValue = StrTool.GBKToUnicode(BankCity);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(BankLocations);
                break;
            case 57:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(BeInsuredNo);
                break;
            case 61:
                strFieldValue = StrTool.GBKToUnicode(IDExpDate);
                break;
            case 62:
                strFieldValue = StrTool.GBKToUnicode(TaxpayerIDType);
                break;
            case 63:
                strFieldValue = StrTool.GBKToUnicode(CompanyNo);
                break;
            case 64:
                strFieldValue = StrTool.GBKToUnicode(CompanyName);
                break;
            case 65:
                strFieldValue = StrTool.GBKToUnicode(Flag);
                break;
            case 66:
                strFieldValue = StrTool.GBKToUnicode(ErrorInfo);
                break;
            case 67:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 68:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 69:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 70:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 71:
                strFieldValue = StrTool.GBKToUnicode(ModIfyTime);
                break;
            case 72:
                strFieldValue = StrTool.GBKToUnicode(E_PolicyID);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
                SerialNo = null;
        }
        if (FCode.equalsIgnoreCase("OtherNoType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
                OtherNoType = null;
        }
        if (FCode.equalsIgnoreCase("OtherNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
                OtherNo = null;
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
                AppntNo = null;
        }
        if (FCode.equalsIgnoreCase("CheckFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                CheckFlag = FValue.trim();
            }
            else
                CheckFlag = null;
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
                PrtNo = null;
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
                GrpContNo = null;
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BatchNo = FValue.trim();
            }
            else
                BatchNo = null;
        }
        if (FCode.equalsIgnoreCase("ID")) {
            if( FValue != null && !FValue.equals(""))
            {
                ID = FValue.trim();
            }
            else
                ID = null;
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
                ContNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
                InsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("MainInsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainInsuredNo = FValue.trim();
            }
            else
                MainInsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("CustomerState")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerState = FValue.trim();
            }
            else
                CustomerState = null;
        }
        if (FCode.equalsIgnoreCase("MainInsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainInsuredName = FValue.trim();
            }
            else
                MainInsuredName = null;
        }
        if (FCode.equalsIgnoreCase("MainInsuredIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainInsuredIDNo = FValue.trim();
            }
            else
                MainInsuredIDNo = null;
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
                InsuredName = null;
        }
        if (FCode.equalsIgnoreCase("EmployeeRelation")) {
            if( FValue != null && !FValue.equals(""))
            {
                EmployeeRelation = FValue.trim();
            }
            else
                EmployeeRelation = null;
        }
        if (FCode.equalsIgnoreCase("InsuredSex")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredSex = FValue.trim();
            }
            else
                InsuredSex = null;
        }
        if (FCode.equalsIgnoreCase("InsuredBirthday")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredBirthday = FValue.trim();
            }
            else
                InsuredBirthday = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDType = FValue.trim();
            }
            else
                InsuredIDType = null;
        }
        if (FCode.equalsIgnoreCase("InsuredIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredIDNo = FValue.trim();
            }
            else
                InsuredIDNo = null;
        }
        if (FCode.equalsIgnoreCase("NativePlace")) {
            if( FValue != null && !FValue.equals(""))
            {
                NativePlace = FValue.trim();
            }
            else
                NativePlace = null;
        }
        if (FCode.equalsIgnoreCase("RgtAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
                RgtAddress = null;
        }
        if (FCode.equalsIgnoreCase("Marriage")) {
            if( FValue != null && !FValue.equals(""))
            {
                Marriage = FValue.trim();
            }
            else
                Marriage = null;
        }
        if (FCode.equalsIgnoreCase("Nationality")) {
            if( FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
                Nationality = null;
        }
        if (FCode.equalsIgnoreCase("ContPlan")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContPlan = FValue.trim();
            }
            else
                ContPlan = null;
        }
        if (FCode.equalsIgnoreCase("OccupationType")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationType = FValue.trim();
            }
            else
                OccupationType = null;
        }
        if (FCode.equalsIgnoreCase("OccupationCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                OccupationCode = FValue.trim();
            }
            else
                OccupationCode = null;
        }
        if (FCode.equalsIgnoreCase("PluralityType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PluralityType = FValue.trim();
            }
            else
                PluralityType = null;
        }
        if (FCode.equalsIgnoreCase("BankCode1")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode1 = FValue.trim();
            }
            else
                BankCode1 = null;
        }
        if (FCode.equalsIgnoreCase("AccName1")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName1 = FValue.trim();
            }
            else
                AccName1 = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo1")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo1 = FValue.trim();
            }
            else
                BankAccNo1 = null;
        }
        if (FCode.equalsIgnoreCase("JoinCompanyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                JoinCompanyDate = FValue.trim();
            }
            else
                JoinCompanyDate = null;
        }
        if (FCode.equalsIgnoreCase("Salary")) {
            if( FValue != null && !FValue.equals(""))
            {
                Salary = FValue.trim();
            }
            else
                Salary = null;
        }
        if (FCode.equalsIgnoreCase("PolTypeFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PolTypeFlag = FValue.trim();
            }
            else
                PolTypeFlag = null;
        }
        if (FCode.equalsIgnoreCase("InsuredPeoples")) {
            if( FValue != null && !FValue.equals(""))
            {
                InsuredPeoples = FValue.trim();
            }
            else
                InsuredPeoples = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                CertifyCode = FValue.trim();
            }
            else
                CertifyCode = null;
        }
        if (FCode.equalsIgnoreCase("StartCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartCode = FValue.trim();
            }
            else
                StartCode = null;
        }
        if (FCode.equalsIgnoreCase("EndCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndCode = FValue.trim();
            }
            else
                EndCode = null;
        }
        if (FCode.equalsIgnoreCase("CalStandbyFlag1")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalStandbyFlag1 = FValue.trim();
            }
            else
                CalStandbyFlag1 = null;
        }
        if (FCode.equalsIgnoreCase("CalStandbyFlag2")) {
            if( FValue != null && !FValue.equals(""))
            {
                CalStandbyFlag2 = FValue.trim();
            }
            else
                CalStandbyFlag2 = null;
        }
        if (FCode.equalsIgnoreCase("RelationToRisk")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToRisk = FValue.trim();
            }
            else
                RelationToRisk = null;
        }
        if (FCode.equalsIgnoreCase("ContCValiDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContCValiDate = FValue.trim();
            }
            else
                ContCValiDate = null;
        }
        if (FCode.equalsIgnoreCase("GrpNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNo = FValue.trim();
            }
            else
                GrpNo = null;
        }
        if (FCode.equalsIgnoreCase("RelationToAppnt")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToAppnt = FValue.trim();
            }
            else
                RelationToAppnt = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuFlag = FValue.trim();
            }
            else
                SocialInsuFlag = null;
        }
        if (FCode.equalsIgnoreCase("RelationToMainInsured")) {
            if( FValue != null && !FValue.equals(""))
            {
                RelationToMainInsured = FValue.trim();
            }
            else
                RelationToMainInsured = null;
        }
        if (FCode.equalsIgnoreCase("HealthServiceName")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthServiceName = FValue.trim();
            }
            else
                HealthServiceName = null;
        }
        if (FCode.equalsIgnoreCase("HealthInsFlagName")) {
            if( FValue != null && !FValue.equals(""))
            {
                HealthInsFlagName = FValue.trim();
            }
            else
                HealthInsFlagName = null;
        }
        if (FCode.equalsIgnoreCase("GrpAlias")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAlias = FValue.trim();
            }
            else
                GrpAlias = null;
        }
        if (FCode.equalsIgnoreCase("WhetherToRnew")) {
            if( FValue != null && !FValue.equals(""))
            {
                WhetherToRnew = FValue.trim();
            }
            else
                WhetherToRnew = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankProvince")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankProvince = FValue.trim();
            }
            else
                BankProvince = null;
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCity = FValue.trim();
            }
            else
                BankCity = null;
        }
        if (FCode.equalsIgnoreCase("BankLocations")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankLocations = FValue.trim();
            }
            else
                BankLocations = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("BeInsuredNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BeInsuredNo = FValue.trim();
            }
            else
                BeInsuredNo = null;
        }
        if (FCode.equalsIgnoreCase("IDExpDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                IDExpDate = FValue.trim();
            }
            else
                IDExpDate = null;
        }
        if (FCode.equalsIgnoreCase("TaxpayerIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxpayerIDType = FValue.trim();
            }
            else
                TaxpayerIDType = null;
        }
        if (FCode.equalsIgnoreCase("CompanyNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyNo = FValue.trim();
            }
            else
                CompanyNo = null;
        }
        if (FCode.equalsIgnoreCase("CompanyName")) {
            if( FValue != null && !FValue.equals(""))
            {
                CompanyName = FValue.trim();
            }
            else
                CompanyName = null;
        }
        if (FCode.equalsIgnoreCase("Flag")) {
            if( FValue != null && !FValue.equals(""))
            {
                Flag = FValue.trim();
            }
            else
                Flag = null;
        }
        if (FCode.equalsIgnoreCase("ErrorInfo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ErrorInfo = FValue.trim();
            }
            else
                ErrorInfo = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModIfyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModIfyTime = FValue.trim();
            }
            else
                ModIfyTime = null;
        }
        if (FCode.equalsIgnoreCase("E_PolicyID")) {
            if( FValue != null && !FValue.equals(""))
            {
                E_PolicyID = FValue.trim();
            }
            else
                E_PolicyID = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LCInsuredImportSubSchema other = (LCInsuredImportSubSchema)otherObject;
        return
            SerialNo.equals(other.getSerialNo())
            && OtherNoType.equals(other.getOtherNoType())
            && OtherNo.equals(other.getOtherNo())
            && AppntNo.equals(other.getAppntNo())
            && CheckFlag.equals(other.getCheckFlag())
            && PrtNo.equals(other.getPrtNo())
            && GrpContNo.equals(other.getGrpContNo())
            && BatchNo.equals(other.getBatchNo())
            && ID.equals(other.getID())
            && ContNo.equals(other.getContNo())
            && InsuredNo.equals(other.getInsuredNo())
            && MainInsuredNo.equals(other.getMainInsuredNo())
            && CustomerState.equals(other.getCustomerState())
            && MainInsuredName.equals(other.getMainInsuredName())
            && MainInsuredIDNo.equals(other.getMainInsuredIDNo())
            && InsuredName.equals(other.getInsuredName())
            && EmployeeRelation.equals(other.getEmployeeRelation())
            && InsuredSex.equals(other.getInsuredSex())
            && InsuredBirthday.equals(other.getInsuredBirthday())
            && InsuredIDType.equals(other.getInsuredIDType())
            && InsuredIDNo.equals(other.getInsuredIDNo())
            && NativePlace.equals(other.getNativePlace())
            && RgtAddress.equals(other.getRgtAddress())
            && Marriage.equals(other.getMarriage())
            && Nationality.equals(other.getNationality())
            && ContPlan.equals(other.getContPlan())
            && OccupationType.equals(other.getOccupationType())
            && OccupationCode.equals(other.getOccupationCode())
            && PluralityType.equals(other.getPluralityType())
            && BankCode1.equals(other.getBankCode1())
            && AccName1.equals(other.getAccName1())
            && BankAccNo1.equals(other.getBankAccNo1())
            && JoinCompanyDate.equals(other.getJoinCompanyDate())
            && Salary.equals(other.getSalary())
            && PolTypeFlag.equals(other.getPolTypeFlag())
            && InsuredPeoples.equals(other.getInsuredPeoples())
            && EMail.equals(other.getEMail())
            && Mobile.equals(other.getMobile())
            && CertifyCode.equals(other.getCertifyCode())
            && StartCode.equals(other.getStartCode())
            && EndCode.equals(other.getEndCode())
            && CalStandbyFlag1.equals(other.getCalStandbyFlag1())
            && CalStandbyFlag2.equals(other.getCalStandbyFlag2())
            && RelationToRisk.equals(other.getRelationToRisk())
            && ContCValiDate.equals(other.getContCValiDate())
            && GrpNo.equals(other.getGrpNo())
            && RelationToAppnt.equals(other.getRelationToAppnt())
            && SocialInsuFlag.equals(other.getSocialInsuFlag())
            && RelationToMainInsured.equals(other.getRelationToMainInsured())
            && HealthServiceName.equals(other.getHealthServiceName())
            && HealthInsFlagName.equals(other.getHealthInsFlagName())
            && GrpAlias.equals(other.getGrpAlias())
            && WhetherToRnew.equals(other.getWhetherToRnew())
            && BankCode.equals(other.getBankCode())
            && BankProvince.equals(other.getBankProvince())
            && BankCity.equals(other.getBankCity())
            && BankLocations.equals(other.getBankLocations())
            && AccName.equals(other.getAccName())
            && BankAccNo.equals(other.getBankAccNo())
            && AccType.equals(other.getAccType())
            && BeInsuredNo.equals(other.getBeInsuredNo())
            && IDExpDate.equals(other.getIDExpDate())
            && TaxpayerIDType.equals(other.getTaxpayerIDType())
            && CompanyNo.equals(other.getCompanyNo())
            && CompanyName.equals(other.getCompanyName())
            && Flag.equals(other.getFlag())
            && ErrorInfo.equals(other.getErrorInfo())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModIfyTime.equals(other.getModIfyTime())
            && E_PolicyID.equals(other.getE_PolicyID());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SerialNo") ) {
            return 0;
        }
        if( strFieldName.equals("OtherNoType") ) {
            return 1;
        }
        if( strFieldName.equals("OtherNo") ) {
            return 2;
        }
        if( strFieldName.equals("AppntNo") ) {
            return 3;
        }
        if( strFieldName.equals("CheckFlag") ) {
            return 4;
        }
        if( strFieldName.equals("PrtNo") ) {
            return 5;
        }
        if( strFieldName.equals("GrpContNo") ) {
            return 6;
        }
        if( strFieldName.equals("BatchNo") ) {
            return 7;
        }
        if( strFieldName.equals("ID") ) {
            return 8;
        }
        if( strFieldName.equals("ContNo") ) {
            return 9;
        }
        if( strFieldName.equals("InsuredNo") ) {
            return 10;
        }
        if( strFieldName.equals("MainInsuredNo") ) {
            return 11;
        }
        if( strFieldName.equals("CustomerState") ) {
            return 12;
        }
        if( strFieldName.equals("MainInsuredName") ) {
            return 13;
        }
        if( strFieldName.equals("MainInsuredIDNo") ) {
            return 14;
        }
        if( strFieldName.equals("InsuredName") ) {
            return 15;
        }
        if( strFieldName.equals("EmployeeRelation") ) {
            return 16;
        }
        if( strFieldName.equals("InsuredSex") ) {
            return 17;
        }
        if( strFieldName.equals("InsuredBirthday") ) {
            return 18;
        }
        if( strFieldName.equals("InsuredIDType") ) {
            return 19;
        }
        if( strFieldName.equals("InsuredIDNo") ) {
            return 20;
        }
        if( strFieldName.equals("NativePlace") ) {
            return 21;
        }
        if( strFieldName.equals("RgtAddress") ) {
            return 22;
        }
        if( strFieldName.equals("Marriage") ) {
            return 23;
        }
        if( strFieldName.equals("Nationality") ) {
            return 24;
        }
        if( strFieldName.equals("ContPlan") ) {
            return 25;
        }
        if( strFieldName.equals("OccupationType") ) {
            return 26;
        }
        if( strFieldName.equals("OccupationCode") ) {
            return 27;
        }
        if( strFieldName.equals("PluralityType") ) {
            return 28;
        }
        if( strFieldName.equals("BankCode1") ) {
            return 29;
        }
        if( strFieldName.equals("AccName1") ) {
            return 30;
        }
        if( strFieldName.equals("BankAccNo1") ) {
            return 31;
        }
        if( strFieldName.equals("JoinCompanyDate") ) {
            return 32;
        }
        if( strFieldName.equals("Salary") ) {
            return 33;
        }
        if( strFieldName.equals("PolTypeFlag") ) {
            return 34;
        }
        if( strFieldName.equals("InsuredPeoples") ) {
            return 35;
        }
        if( strFieldName.equals("EMail") ) {
            return 36;
        }
        if( strFieldName.equals("Mobile") ) {
            return 37;
        }
        if( strFieldName.equals("CertifyCode") ) {
            return 38;
        }
        if( strFieldName.equals("StartCode") ) {
            return 39;
        }
        if( strFieldName.equals("EndCode") ) {
            return 40;
        }
        if( strFieldName.equals("CalStandbyFlag1") ) {
            return 41;
        }
        if( strFieldName.equals("CalStandbyFlag2") ) {
            return 42;
        }
        if( strFieldName.equals("RelationToRisk") ) {
            return 43;
        }
        if( strFieldName.equals("ContCValiDate") ) {
            return 44;
        }
        if( strFieldName.equals("GrpNo") ) {
            return 45;
        }
        if( strFieldName.equals("RelationToAppnt") ) {
            return 46;
        }
        if( strFieldName.equals("SocialInsuFlag") ) {
            return 47;
        }
        if( strFieldName.equals("RelationToMainInsured") ) {
            return 48;
        }
        if( strFieldName.equals("HealthServiceName") ) {
            return 49;
        }
        if( strFieldName.equals("HealthInsFlagName") ) {
            return 50;
        }
        if( strFieldName.equals("GrpAlias") ) {
            return 51;
        }
        if( strFieldName.equals("WhetherToRnew") ) {
            return 52;
        }
        if( strFieldName.equals("BankCode") ) {
            return 53;
        }
        if( strFieldName.equals("BankProvince") ) {
            return 54;
        }
        if( strFieldName.equals("BankCity") ) {
            return 55;
        }
        if( strFieldName.equals("BankLocations") ) {
            return 56;
        }
        if( strFieldName.equals("AccName") ) {
            return 57;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 58;
        }
        if( strFieldName.equals("AccType") ) {
            return 59;
        }
        if( strFieldName.equals("BeInsuredNo") ) {
            return 60;
        }
        if( strFieldName.equals("IDExpDate") ) {
            return 61;
        }
        if( strFieldName.equals("TaxpayerIDType") ) {
            return 62;
        }
        if( strFieldName.equals("CompanyNo") ) {
            return 63;
        }
        if( strFieldName.equals("CompanyName") ) {
            return 64;
        }
        if( strFieldName.equals("Flag") ) {
            return 65;
        }
        if( strFieldName.equals("ErrorInfo") ) {
            return 66;
        }
        if( strFieldName.equals("Operator") ) {
            return 67;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 68;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 69;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 70;
        }
        if( strFieldName.equals("ModIfyTime") ) {
            return 71;
        }
        if( strFieldName.equals("E_PolicyID") ) {
            return 72;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "OtherNoType";
                break;
            case 2:
                strFieldName = "OtherNo";
                break;
            case 3:
                strFieldName = "AppntNo";
                break;
            case 4:
                strFieldName = "CheckFlag";
                break;
            case 5:
                strFieldName = "PrtNo";
                break;
            case 6:
                strFieldName = "GrpContNo";
                break;
            case 7:
                strFieldName = "BatchNo";
                break;
            case 8:
                strFieldName = "ID";
                break;
            case 9:
                strFieldName = "ContNo";
                break;
            case 10:
                strFieldName = "InsuredNo";
                break;
            case 11:
                strFieldName = "MainInsuredNo";
                break;
            case 12:
                strFieldName = "CustomerState";
                break;
            case 13:
                strFieldName = "MainInsuredName";
                break;
            case 14:
                strFieldName = "MainInsuredIDNo";
                break;
            case 15:
                strFieldName = "InsuredName";
                break;
            case 16:
                strFieldName = "EmployeeRelation";
                break;
            case 17:
                strFieldName = "InsuredSex";
                break;
            case 18:
                strFieldName = "InsuredBirthday";
                break;
            case 19:
                strFieldName = "InsuredIDType";
                break;
            case 20:
                strFieldName = "InsuredIDNo";
                break;
            case 21:
                strFieldName = "NativePlace";
                break;
            case 22:
                strFieldName = "RgtAddress";
                break;
            case 23:
                strFieldName = "Marriage";
                break;
            case 24:
                strFieldName = "Nationality";
                break;
            case 25:
                strFieldName = "ContPlan";
                break;
            case 26:
                strFieldName = "OccupationType";
                break;
            case 27:
                strFieldName = "OccupationCode";
                break;
            case 28:
                strFieldName = "PluralityType";
                break;
            case 29:
                strFieldName = "BankCode1";
                break;
            case 30:
                strFieldName = "AccName1";
                break;
            case 31:
                strFieldName = "BankAccNo1";
                break;
            case 32:
                strFieldName = "JoinCompanyDate";
                break;
            case 33:
                strFieldName = "Salary";
                break;
            case 34:
                strFieldName = "PolTypeFlag";
                break;
            case 35:
                strFieldName = "InsuredPeoples";
                break;
            case 36:
                strFieldName = "EMail";
                break;
            case 37:
                strFieldName = "Mobile";
                break;
            case 38:
                strFieldName = "CertifyCode";
                break;
            case 39:
                strFieldName = "StartCode";
                break;
            case 40:
                strFieldName = "EndCode";
                break;
            case 41:
                strFieldName = "CalStandbyFlag1";
                break;
            case 42:
                strFieldName = "CalStandbyFlag2";
                break;
            case 43:
                strFieldName = "RelationToRisk";
                break;
            case 44:
                strFieldName = "ContCValiDate";
                break;
            case 45:
                strFieldName = "GrpNo";
                break;
            case 46:
                strFieldName = "RelationToAppnt";
                break;
            case 47:
                strFieldName = "SocialInsuFlag";
                break;
            case 48:
                strFieldName = "RelationToMainInsured";
                break;
            case 49:
                strFieldName = "HealthServiceName";
                break;
            case 50:
                strFieldName = "HealthInsFlagName";
                break;
            case 51:
                strFieldName = "GrpAlias";
                break;
            case 52:
                strFieldName = "WhetherToRnew";
                break;
            case 53:
                strFieldName = "BankCode";
                break;
            case 54:
                strFieldName = "BankProvince";
                break;
            case 55:
                strFieldName = "BankCity";
                break;
            case 56:
                strFieldName = "BankLocations";
                break;
            case 57:
                strFieldName = "AccName";
                break;
            case 58:
                strFieldName = "BankAccNo";
                break;
            case 59:
                strFieldName = "AccType";
                break;
            case 60:
                strFieldName = "BeInsuredNo";
                break;
            case 61:
                strFieldName = "IDExpDate";
                break;
            case 62:
                strFieldName = "TaxpayerIDType";
                break;
            case 63:
                strFieldName = "CompanyNo";
                break;
            case 64:
                strFieldName = "CompanyName";
                break;
            case 65:
                strFieldName = "Flag";
                break;
            case 66:
                strFieldName = "ErrorInfo";
                break;
            case 67:
                strFieldName = "Operator";
                break;
            case 68:
                strFieldName = "MakeDate";
                break;
            case 69:
                strFieldName = "MakeTime";
                break;
            case 70:
                strFieldName = "ModifyDate";
                break;
            case 71:
                strFieldName = "ModIfyTime";
                break;
            case 72:
                strFieldName = "E_PolicyID";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SERIALNO":
                return Schema.TYPE_STRING;
            case "OTHERNOTYPE":
                return Schema.TYPE_STRING;
            case "OTHERNO":
                return Schema.TYPE_STRING;
            case "APPNTNO":
                return Schema.TYPE_STRING;
            case "CHECKFLAG":
                return Schema.TYPE_STRING;
            case "PRTNO":
                return Schema.TYPE_STRING;
            case "GRPCONTNO":
                return Schema.TYPE_STRING;
            case "BATCHNO":
                return Schema.TYPE_STRING;
            case "ID":
                return Schema.TYPE_STRING;
            case "CONTNO":
                return Schema.TYPE_STRING;
            case "INSUREDNO":
                return Schema.TYPE_STRING;
            case "MAININSUREDNO":
                return Schema.TYPE_STRING;
            case "CUSTOMERSTATE":
                return Schema.TYPE_STRING;
            case "MAININSUREDNAME":
                return Schema.TYPE_STRING;
            case "MAININSUREDIDNO":
                return Schema.TYPE_STRING;
            case "INSUREDNAME":
                return Schema.TYPE_STRING;
            case "EMPLOYEERELATION":
                return Schema.TYPE_STRING;
            case "INSUREDSEX":
                return Schema.TYPE_STRING;
            case "INSUREDBIRTHDAY":
                return Schema.TYPE_STRING;
            case "INSUREDIDTYPE":
                return Schema.TYPE_STRING;
            case "INSUREDIDNO":
                return Schema.TYPE_STRING;
            case "NATIVEPLACE":
                return Schema.TYPE_STRING;
            case "RGTADDRESS":
                return Schema.TYPE_STRING;
            case "MARRIAGE":
                return Schema.TYPE_STRING;
            case "NATIONALITY":
                return Schema.TYPE_STRING;
            case "CONTPLAN":
                return Schema.TYPE_STRING;
            case "OCCUPATIONTYPE":
                return Schema.TYPE_STRING;
            case "OCCUPATIONCODE":
                return Schema.TYPE_STRING;
            case "PLURALITYTYPE":
                return Schema.TYPE_STRING;
            case "BANKCODE1":
                return Schema.TYPE_STRING;
            case "ACCNAME1":
                return Schema.TYPE_STRING;
            case "BANKACCNO1":
                return Schema.TYPE_STRING;
            case "JOINCOMPANYDATE":
                return Schema.TYPE_STRING;
            case "SALARY":
                return Schema.TYPE_STRING;
            case "POLTYPEFLAG":
                return Schema.TYPE_STRING;
            case "INSUREDPEOPLES":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "CERTIFYCODE":
                return Schema.TYPE_STRING;
            case "STARTCODE":
                return Schema.TYPE_STRING;
            case "ENDCODE":
                return Schema.TYPE_STRING;
            case "CALSTANDBYFLAG1":
                return Schema.TYPE_STRING;
            case "CALSTANDBYFLAG2":
                return Schema.TYPE_STRING;
            case "RELATIONTORISK":
                return Schema.TYPE_STRING;
            case "CONTCVALIDATE":
                return Schema.TYPE_STRING;
            case "GRPNO":
                return Schema.TYPE_STRING;
            case "RELATIONTOAPPNT":
                return Schema.TYPE_STRING;
            case "SOCIALINSUFLAG":
                return Schema.TYPE_STRING;
            case "RELATIONTOMAININSURED":
                return Schema.TYPE_STRING;
            case "HEALTHSERVICENAME":
                return Schema.TYPE_STRING;
            case "HEALTHINSFLAGNAME":
                return Schema.TYPE_STRING;
            case "GRPALIAS":
                return Schema.TYPE_STRING;
            case "WHETHERTORNEW":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKPROVINCE":
                return Schema.TYPE_STRING;
            case "BANKCITY":
                return Schema.TYPE_STRING;
            case "BANKLOCATIONS":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "BEINSUREDNO":
                return Schema.TYPE_STRING;
            case "IDEXPDATE":
                return Schema.TYPE_STRING;
            case "TAXPAYERIDTYPE":
                return Schema.TYPE_STRING;
            case "COMPANYNO":
                return Schema.TYPE_STRING;
            case "COMPANYNAME":
                return Schema.TYPE_STRING;
            case "FLAG":
                return Schema.TYPE_STRING;
            case "ERRORINFO":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "E_POLICYID":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_STRING;
            case 43:
                return Schema.TYPE_STRING;
            case 44:
                return Schema.TYPE_STRING;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_STRING;
            case 65:
                return Schema.TYPE_STRING;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_DATE;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_DATE;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
