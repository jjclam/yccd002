/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.Pojo;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;

import java.io.Serializable;

/**
 * <p>ClassName: LDGrpToPlanPojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-04-29
 */
public class LDGrpToPlanPojo implements  Pojo,Serializable {
    // @Field
    /** 客户号码 */
    private String CustomerNo;
    /** 方案编码 */
    private String PlanCode;
    /** 方案名称 */
    private String PlanName;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 开办日期 */
    private String  StartDate;
    /** 停办日期 */
    private String  EndDate;
    /** 销售保额上限 */
    private double MAXAmnt;
    /** 销售保额下限 */
    private double MINAmnt;
    /** 销售份数上限 */
    private int MAXMult;
    /** 销售份数下限 */
    private int MINMult;
    /** 销售保费上限 */
    private double MAXPrem;
    /** 销售保费下限 */
    private double MINPrem;
    /** 手续费率 */
    private double FeeRate;


    public static final int FIELDNUM = 17;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getPlanCode() {
        return PlanCode;
    }
    public void setPlanCode(String aPlanCode) {
        PlanCode = aPlanCode;
    }
    public String getPlanName() {
        return PlanName;
    }
    public void setPlanName(String aPlanName) {
        PlanName = aPlanName;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getStartDate() {
        return StartDate;
    }
    public void setStartDate(String aStartDate) {
        StartDate = aStartDate;
    }
    public String getEndDate() {
        return EndDate;
    }
    public void setEndDate(String aEndDate) {
        EndDate = aEndDate;
    }
    public double getMAXAmnt() {
        return MAXAmnt;
    }
    public void setMAXAmnt(double aMAXAmnt) {
        MAXAmnt = aMAXAmnt;
    }
    public void setMAXAmnt(String aMAXAmnt) {
        if (aMAXAmnt != null && !aMAXAmnt.equals("")) {
            Double tDouble = new Double(aMAXAmnt);
            double d = tDouble.doubleValue();
            MAXAmnt = d;
        }
    }

    public double getMINAmnt() {
        return MINAmnt;
    }
    public void setMINAmnt(double aMINAmnt) {
        MINAmnt = aMINAmnt;
    }
    public void setMINAmnt(String aMINAmnt) {
        if (aMINAmnt != null && !aMINAmnt.equals("")) {
            Double tDouble = new Double(aMINAmnt);
            double d = tDouble.doubleValue();
            MINAmnt = d;
        }
    }

    public int getMAXMult() {
        return MAXMult;
    }
    public void setMAXMult(int aMAXMult) {
        MAXMult = aMAXMult;
    }
    public void setMAXMult(String aMAXMult) {
        if (aMAXMult != null && !aMAXMult.equals("")) {
            Integer tInteger = new Integer(aMAXMult);
            int i = tInteger.intValue();
            MAXMult = i;
        }
    }

    public int getMINMult() {
        return MINMult;
    }
    public void setMINMult(int aMINMult) {
        MINMult = aMINMult;
    }
    public void setMINMult(String aMINMult) {
        if (aMINMult != null && !aMINMult.equals("")) {
            Integer tInteger = new Integer(aMINMult);
            int i = tInteger.intValue();
            MINMult = i;
        }
    }

    public double getMAXPrem() {
        return MAXPrem;
    }
    public void setMAXPrem(double aMAXPrem) {
        MAXPrem = aMAXPrem;
    }
    public void setMAXPrem(String aMAXPrem) {
        if (aMAXPrem != null && !aMAXPrem.equals("")) {
            Double tDouble = new Double(aMAXPrem);
            double d = tDouble.doubleValue();
            MAXPrem = d;
        }
    }

    public double getMINPrem() {
        return MINPrem;
    }
    public void setMINPrem(double aMINPrem) {
        MINPrem = aMINPrem;
    }
    public void setMINPrem(String aMINPrem) {
        if (aMINPrem != null && !aMINPrem.equals("")) {
            Double tDouble = new Double(aMINPrem);
            double d = tDouble.doubleValue();
            MINPrem = d;
        }
    }

    public double getFeeRate() {
        return FeeRate;
    }
    public void setFeeRate(double aFeeRate) {
        FeeRate = aFeeRate;
    }
    public void setFeeRate(String aFeeRate) {
        if (aFeeRate != null && !aFeeRate.equals("")) {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = d;
        }
    }


    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    @Override
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CustomerNo") ) {
            return 0;
        }
        if( strFieldName.equals("PlanCode") ) {
            return 1;
        }
        if( strFieldName.equals("PlanName") ) {
            return 2;
        }
        if( strFieldName.equals("Operator") ) {
            return 3;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 4;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 5;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 6;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 7;
        }
        if( strFieldName.equals("StartDate") ) {
            return 8;
        }
        if( strFieldName.equals("EndDate") ) {
            return 9;
        }
        if( strFieldName.equals("MAXAmnt") ) {
            return 10;
        }
        if( strFieldName.equals("MINAmnt") ) {
            return 11;
        }
        if( strFieldName.equals("MAXMult") ) {
            return 12;
        }
        if( strFieldName.equals("MINMult") ) {
            return 13;
        }
        if( strFieldName.equals("MAXPrem") ) {
            return 14;
        }
        if( strFieldName.equals("MINPrem") ) {
            return 15;
        }
        if( strFieldName.equals("FeeRate") ) {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CustomerNo";
                break;
            case 1:
                strFieldName = "PlanCode";
                break;
            case 2:
                strFieldName = "PlanName";
                break;
            case 3:
                strFieldName = "Operator";
                break;
            case 4:
                strFieldName = "MakeDate";
                break;
            case 5:
                strFieldName = "MakeTime";
                break;
            case 6:
                strFieldName = "ModifyDate";
                break;
            case 7:
                strFieldName = "ModifyTime";
                break;
            case 8:
                strFieldName = "StartDate";
                break;
            case 9:
                strFieldName = "EndDate";
                break;
            case 10:
                strFieldName = "MAXAmnt";
                break;
            case 11:
                strFieldName = "MINAmnt";
                break;
            case 12:
                strFieldName = "MAXMult";
                break;
            case 13:
                strFieldName = "MINMult";
                break;
            case 14:
                strFieldName = "MAXPrem";
                break;
            case 15:
                strFieldName = "MINPrem";
                break;
            case 16:
                strFieldName = "FeeRate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "PLANCODE":
                return Schema.TYPE_STRING;
            case "PLANNAME":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_STRING;
            case "ENDDATE":
                return Schema.TYPE_STRING;
            case "MAXAMNT":
                return Schema.TYPE_DOUBLE;
            case "MINAMNT":
                return Schema.TYPE_DOUBLE;
            case "MAXMULT":
                return Schema.TYPE_INT;
            case "MINMULT":
                return Schema.TYPE_INT;
            case "MAXPREM":
                return Schema.TYPE_DOUBLE;
            case "MINPREM":
                return Schema.TYPE_DOUBLE;
            case "FEERATE":
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DOUBLE;
            case 11:
                return Schema.TYPE_DOUBLE;
            case 12:
                return Schema.TYPE_INT;
            case 13:
                return Schema.TYPE_INT;
            case 14:
                return Schema.TYPE_DOUBLE;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_DOUBLE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanCode));
        }
        if (FCode.equalsIgnoreCase("PlanName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanName));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndDate));
        }
        if (FCode.equalsIgnoreCase("MAXAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXAmnt));
        }
        if (FCode.equalsIgnoreCase("MINAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINAmnt));
        }
        if (FCode.equalsIgnoreCase("MAXMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXMult));
        }
        if (FCode.equalsIgnoreCase("MINMult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINMult));
        }
        if (FCode.equalsIgnoreCase("MAXPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MAXPrem));
        }
        if (FCode.equalsIgnoreCase("MINPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MINPrem));
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 1:
                strFieldValue = String.valueOf(PlanCode);
                break;
            case 2:
                strFieldValue = String.valueOf(PlanName);
                break;
            case 3:
                strFieldValue = String.valueOf(Operator);
                break;
            case 4:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 5:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 6:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 7:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 8:
                strFieldValue = String.valueOf(StartDate);
                break;
            case 9:
                strFieldValue = String.valueOf(EndDate);
                break;
            case 10:
                strFieldValue = String.valueOf(MAXAmnt);
                break;
            case 11:
                strFieldValue = String.valueOf(MINAmnt);
                break;
            case 12:
                strFieldValue = String.valueOf(MAXMult);
                break;
            case 13:
                strFieldValue = String.valueOf(MINMult);
                break;
            case 14:
                strFieldValue = String.valueOf(MAXPrem);
                break;
            case 15:
                strFieldValue = String.valueOf(MINPrem);
                break;
            case 16:
                strFieldValue = String.valueOf(FeeRate);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("PlanCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanCode = FValue.trim();
            }
            else
                PlanCode = null;
        }
        if (FCode.equalsIgnoreCase("PlanName")) {
            if( FValue != null && !FValue.equals(""))
            {
                PlanName = FValue.trim();
            }
            else
                PlanName = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                EndDate = FValue.trim();
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("MAXAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MAXAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("MINAmnt")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MINAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("MAXMult")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MAXMult = i;
            }
        }
        if (FCode.equalsIgnoreCase("MINMult")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                MINMult = i;
            }
        }
        if (FCode.equalsIgnoreCase("MAXPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MAXPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("MINPrem")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                MINPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        return true;
    }


    public String toString() {
        return "LDGrpToPlanPojo [" +
                "CustomerNo="+CustomerNo +
                ", PlanCode="+PlanCode +
                ", PlanName="+PlanName +
                ", Operator="+Operator +
                ", MakeDate="+MakeDate +
                ", MakeTime="+MakeTime +
                ", ModifyDate="+ModifyDate +
                ", ModifyTime="+ModifyTime +
                ", StartDate="+StartDate +
                ", EndDate="+EndDate +
                ", MAXAmnt="+MAXAmnt +
                ", MINAmnt="+MINAmnt +
                ", MAXMult="+MAXMult +
                ", MINMult="+MINMult +
                ", MAXPrem="+MAXPrem +
                ", MINPrem="+MINPrem +
                ", FeeRate="+FeeRate +"]";
    }
}
