/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDSchemeDB;

/**
 * <p>ClassName: LDSchemeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-15
 */
public class LDSchemeSchema implements Schema, Cloneable {
    // @Field
    /** 序号 */
    private String SelNO;
    /** 保险方案编码 */
    private String ContSchemeCode;
    /** 保险方案名称 */
    private String ContSchemeName;
    /** 方案类别 */
    private String SchemeType;
    /** 方案规则 */
    private String SchemeRule;
    /** 方案规则sql */
    private String SchemeSQL;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 可投保人数 */
    private int Peoples3;
    /** 备注2 */
    private String Remark2;
    /** 管理机构 */
    private String ManageCom;
    /** 销售渠道 */
    private String Salechnl;
    /** 销售起期 */
    private Date StartDate;
    /** 销售止期 */
    private Date EndDate;
    /** 状态 */
    private String State;
    /** 方案层级2 */
    private String SchemeKind2;
    /** 方案层级3 */
    private String SchemeKind3;
    /** 方案层级1 */
    private String SchemeKind1;
    /** 产品组合标记 */
    private String PortfolioFlag;
    /** 医保身份标记 */
    private String MedicalFlag;
    /** 推送标记 */
    private String PushFlag;

    public static final int FIELDNUM = 25;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDSchemeSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SelNO";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDSchemeSchema cloned = (LDSchemeSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSelNO() {
        return SelNO;
    }
    public void setSelNO(String aSelNO) {
        SelNO = aSelNO;
    }
    public String getContSchemeCode() {
        return ContSchemeCode;
    }
    public void setContSchemeCode(String aContSchemeCode) {
        ContSchemeCode = aContSchemeCode;
    }
    public String getContSchemeName() {
        return ContSchemeName;
    }
    public void setContSchemeName(String aContSchemeName) {
        ContSchemeName = aContSchemeName;
    }
    public String getSchemeType() {
        return SchemeType;
    }
    public void setSchemeType(String aSchemeType) {
        SchemeType = aSchemeType;
    }
    public String getSchemeRule() {
        return SchemeRule;
    }
    public void setSchemeRule(String aSchemeRule) {
        SchemeRule = aSchemeRule;
    }
    public String getSchemeSQL() {
        return SchemeSQL;
    }
    public void setSchemeSQL(String aSchemeSQL) {
        SchemeSQL = aSchemeSQL;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public int getPeoples3() {
        return Peoples3;
    }
    public void setPeoples3(int aPeoples3) {
        Peoples3 = aPeoples3;
    }
    public void setPeoples3(String aPeoples3) {
        if (aPeoples3 != null && !aPeoples3.equals("")) {
            Integer tInteger = new Integer(aPeoples3);
            int i = tInteger.intValue();
            Peoples3 = i;
        }
    }

    public String getRemark2() {
        return Remark2;
    }
    public void setRemark2(String aRemark2) {
        Remark2 = aRemark2;
    }
    public String getManageCom() {
        return ManageCom;
    }
    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }
    public String getSalechnl() {
        return Salechnl;
    }
    public void setSalechnl(String aSalechnl) {
        Salechnl = aSalechnl;
    }
    public String getStartDate() {
        if(StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }
    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }
    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else
            StartDate = null;
    }

    public String getEndDate() {
        if(EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }
    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }
    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else
            EndDate = null;
    }

    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getSchemeKind2() {
        return SchemeKind2;
    }
    public void setSchemeKind2(String aSchemeKind2) {
        SchemeKind2 = aSchemeKind2;
    }
    public String getSchemeKind3() {
        return SchemeKind3;
    }
    public void setSchemeKind3(String aSchemeKind3) {
        SchemeKind3 = aSchemeKind3;
    }
    public String getSchemeKind1() {
        return SchemeKind1;
    }
    public void setSchemeKind1(String aSchemeKind1) {
        SchemeKind1 = aSchemeKind1;
    }
    public String getPortfolioFlag() {
        return PortfolioFlag;
    }
    public void setPortfolioFlag(String aPortfolioFlag) {
        PortfolioFlag = aPortfolioFlag;
    }
    public String getMedicalFlag() {
        return MedicalFlag;
    }
    public void setMedicalFlag(String aMedicalFlag) {
        MedicalFlag = aMedicalFlag;
    }
    public String getPushFlag() {
        return PushFlag;
    }
    public void setPushFlag(String aPushFlag) {
        PushFlag = aPushFlag;
    }

    /**
    * 使用另外一个 LDSchemeSchema 对象给 Schema 赋值
    * @param: aLDSchemeSchema LDSchemeSchema
    **/
    public void setSchema(LDSchemeSchema aLDSchemeSchema) {
        this.SelNO = aLDSchemeSchema.getSelNO();
        this.ContSchemeCode = aLDSchemeSchema.getContSchemeCode();
        this.ContSchemeName = aLDSchemeSchema.getContSchemeName();
        this.SchemeType = aLDSchemeSchema.getSchemeType();
        this.SchemeRule = aLDSchemeSchema.getSchemeRule();
        this.SchemeSQL = aLDSchemeSchema.getSchemeSQL();
        this.Remark = aLDSchemeSchema.getRemark();
        this.Operator = aLDSchemeSchema.getOperator();
        this.MakeDate = fDate.getDate( aLDSchemeSchema.getMakeDate());
        this.MakeTime = aLDSchemeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLDSchemeSchema.getModifyDate());
        this.ModifyTime = aLDSchemeSchema.getModifyTime();
        this.Peoples3 = aLDSchemeSchema.getPeoples3();
        this.Remark2 = aLDSchemeSchema.getRemark2();
        this.ManageCom = aLDSchemeSchema.getManageCom();
        this.Salechnl = aLDSchemeSchema.getSalechnl();
        this.StartDate = fDate.getDate( aLDSchemeSchema.getStartDate());
        this.EndDate = fDate.getDate( aLDSchemeSchema.getEndDate());
        this.State = aLDSchemeSchema.getState();
        this.SchemeKind2 = aLDSchemeSchema.getSchemeKind2();
        this.SchemeKind3 = aLDSchemeSchema.getSchemeKind3();
        this.SchemeKind1 = aLDSchemeSchema.getSchemeKind1();
        this.PortfolioFlag = aLDSchemeSchema.getPortfolioFlag();
        this.MedicalFlag = aLDSchemeSchema.getMedicalFlag();
        this.PushFlag = aLDSchemeSchema.getPushFlag();
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("SelNO") == null )
                this.SelNO = null;
            else
                this.SelNO = rs.getString("SelNO").trim();

            if( rs.getString("ContSchemeCode") == null )
                this.ContSchemeCode = null;
            else
                this.ContSchemeCode = rs.getString("ContSchemeCode").trim();

            if( rs.getString("ContSchemeName") == null )
                this.ContSchemeName = null;
            else
                this.ContSchemeName = rs.getString("ContSchemeName").trim();

            if( rs.getString("SchemeType") == null )
                this.SchemeType = null;
            else
                this.SchemeType = rs.getString("SchemeType").trim();

            if( rs.getString("SchemeRule") == null )
                this.SchemeRule = null;
            else
                this.SchemeRule = rs.getString("SchemeRule").trim();

            if( rs.getString("SchemeSQL") == null )
                this.SchemeSQL = null;
            else
                this.SchemeSQL = rs.getString("SchemeSQL").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            this.Peoples3 = rs.getInt("Peoples3");
            if( rs.getString("Remark2") == null )
                this.Remark2 = null;
            else
                this.Remark2 = rs.getString("Remark2").trim();

            if( rs.getString("ManageCom") == null )
                this.ManageCom = null;
            else
                this.ManageCom = rs.getString("ManageCom").trim();

            if( rs.getString("Salechnl") == null )
                this.Salechnl = null;
            else
                this.Salechnl = rs.getString("Salechnl").trim();

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("SchemeKind2") == null )
                this.SchemeKind2 = null;
            else
                this.SchemeKind2 = rs.getString("SchemeKind2").trim();

            if( rs.getString("SchemeKind3") == null )
                this.SchemeKind3 = null;
            else
                this.SchemeKind3 = rs.getString("SchemeKind3").trim();

            if( rs.getString("SchemeKind1") == null )
                this.SchemeKind1 = null;
            else
                this.SchemeKind1 = rs.getString("SchemeKind1").trim();

            if( rs.getString("PortfolioFlag") == null )
                this.PortfolioFlag = null;
            else
                this.PortfolioFlag = rs.getString("PortfolioFlag").trim();

            if( rs.getString("MedicalFlag") == null )
                this.MedicalFlag = null;
            else
                this.MedicalFlag = rs.getString("MedicalFlag").trim();

            if( rs.getString("PushFlag") == null )
                this.PushFlag = null;
            else
                this.PushFlag = rs.getString("PushFlag").trim();

        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSchemeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDSchemeSchema getSchema() {
        LDSchemeSchema aLDSchemeSchema = new LDSchemeSchema();
        aLDSchemeSchema.setSchema(this);
        return aLDSchemeSchema;
    }

    public LDSchemeDB getDB() {
        LDSchemeDB aDBOper = new LDSchemeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDScheme描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SelNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContSchemeCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContSchemeName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SchemeType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SchemeRule)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SchemeSQL)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples3));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Salechnl)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SchemeKind2)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SchemeKind3)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SchemeKind1)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PortfolioFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MedicalFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PushFlag));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDScheme>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            SelNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            ContSchemeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            ContSchemeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            SchemeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            SchemeRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            SchemeSQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            Peoples3 = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).intValue();
            Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            Salechnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            SchemeKind2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
            SchemeKind3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            SchemeKind1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            PortfolioFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            MedicalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
            PushFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSchemeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("SelNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelNO));
        }
        if (FCode.equalsIgnoreCase("ContSchemeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContSchemeCode));
        }
        if (FCode.equalsIgnoreCase("ContSchemeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContSchemeName));
        }
        if (FCode.equalsIgnoreCase("SchemeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeType));
        }
        if (FCode.equalsIgnoreCase("SchemeRule")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeRule));
        }
        if (FCode.equalsIgnoreCase("SchemeSQL")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeSQL));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("Salechnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Salechnl));
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("SchemeKind2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeKind2));
        }
        if (FCode.equalsIgnoreCase("SchemeKind3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeKind3));
        }
        if (FCode.equalsIgnoreCase("SchemeKind1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SchemeKind1));
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PortfolioFlag));
        }
        if (FCode.equalsIgnoreCase("MedicalFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicalFlag));
        }
        if (FCode.equalsIgnoreCase("PushFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PushFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SelNO);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ContSchemeCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ContSchemeName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SchemeType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SchemeRule);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(SchemeSQL);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 12:
                strFieldValue = String.valueOf(Peoples3);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Remark2);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Salechnl);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(SchemeKind2);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(SchemeKind3);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(SchemeKind1);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(PortfolioFlag);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MedicalFlag);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(PushFlag);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("SelNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                SelNO = FValue.trim();
            }
            else
                SelNO = null;
        }
        if (FCode.equalsIgnoreCase("ContSchemeCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContSchemeCode = FValue.trim();
            }
            else
                ContSchemeCode = null;
        }
        if (FCode.equalsIgnoreCase("ContSchemeName")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContSchemeName = FValue.trim();
            }
            else
                ContSchemeName = null;
        }
        if (FCode.equalsIgnoreCase("SchemeType")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeType = FValue.trim();
            }
            else
                SchemeType = null;
        }
        if (FCode.equalsIgnoreCase("SchemeRule")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeRule = FValue.trim();
            }
            else
                SchemeRule = null;
        }
        if (FCode.equalsIgnoreCase("SchemeSQL")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeSQL = FValue.trim();
            }
            else
                SchemeSQL = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("Peoples3")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples3 = i;
            }
        }
        if (FCode.equalsIgnoreCase("Remark2")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
                Remark2 = null;
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if( FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
                ManageCom = null;
        }
        if (FCode.equalsIgnoreCase("Salechnl")) {
            if( FValue != null && !FValue.equals(""))
            {
                Salechnl = FValue.trim();
            }
            else
                Salechnl = null;
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if(FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate( FValue );
            }
            else
                StartDate = null;
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if(FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate( FValue );
            }
            else
                EndDate = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("SchemeKind2")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeKind2 = FValue.trim();
            }
            else
                SchemeKind2 = null;
        }
        if (FCode.equalsIgnoreCase("SchemeKind3")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeKind3 = FValue.trim();
            }
            else
                SchemeKind3 = null;
        }
        if (FCode.equalsIgnoreCase("SchemeKind1")) {
            if( FValue != null && !FValue.equals(""))
            {
                SchemeKind1 = FValue.trim();
            }
            else
                SchemeKind1 = null;
        }
        if (FCode.equalsIgnoreCase("PortfolioFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PortfolioFlag = FValue.trim();
            }
            else
                PortfolioFlag = null;
        }
        if (FCode.equalsIgnoreCase("MedicalFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                MedicalFlag = FValue.trim();
            }
            else
                MedicalFlag = null;
        }
        if (FCode.equalsIgnoreCase("PushFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                PushFlag = FValue.trim();
            }
            else
                PushFlag = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDSchemeSchema other = (LDSchemeSchema)otherObject;
        return
            SelNO.equals(other.getSelNO())
            && ContSchemeCode.equals(other.getContSchemeCode())
            && ContSchemeName.equals(other.getContSchemeName())
            && SchemeType.equals(other.getSchemeType())
            && SchemeRule.equals(other.getSchemeRule())
            && SchemeSQL.equals(other.getSchemeSQL())
            && Remark.equals(other.getRemark())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && Peoples3 == other.getPeoples3()
            && Remark2.equals(other.getRemark2())
            && ManageCom.equals(other.getManageCom())
            && Salechnl.equals(other.getSalechnl())
            && fDate.getString(StartDate).equals(other.getStartDate())
            && fDate.getString(EndDate).equals(other.getEndDate())
            && State.equals(other.getState())
            && SchemeKind2.equals(other.getSchemeKind2())
            && SchemeKind3.equals(other.getSchemeKind3())
            && SchemeKind1.equals(other.getSchemeKind1())
            && PortfolioFlag.equals(other.getPortfolioFlag())
            && MedicalFlag.equals(other.getMedicalFlag())
            && PushFlag.equals(other.getPushFlag());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("SelNO") ) {
            return 0;
        }
        if( strFieldName.equals("ContSchemeCode") ) {
            return 1;
        }
        if( strFieldName.equals("ContSchemeName") ) {
            return 2;
        }
        if( strFieldName.equals("SchemeType") ) {
            return 3;
        }
        if( strFieldName.equals("SchemeRule") ) {
            return 4;
        }
        if( strFieldName.equals("SchemeSQL") ) {
            return 5;
        }
        if( strFieldName.equals("Remark") ) {
            return 6;
        }
        if( strFieldName.equals("Operator") ) {
            return 7;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 8;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 9;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 10;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 11;
        }
        if( strFieldName.equals("Peoples3") ) {
            return 12;
        }
        if( strFieldName.equals("Remark2") ) {
            return 13;
        }
        if( strFieldName.equals("ManageCom") ) {
            return 14;
        }
        if( strFieldName.equals("Salechnl") ) {
            return 15;
        }
        if( strFieldName.equals("StartDate") ) {
            return 16;
        }
        if( strFieldName.equals("EndDate") ) {
            return 17;
        }
        if( strFieldName.equals("State") ) {
            return 18;
        }
        if( strFieldName.equals("SchemeKind2") ) {
            return 19;
        }
        if( strFieldName.equals("SchemeKind3") ) {
            return 20;
        }
        if( strFieldName.equals("SchemeKind1") ) {
            return 21;
        }
        if( strFieldName.equals("PortfolioFlag") ) {
            return 22;
        }
        if( strFieldName.equals("MedicalFlag") ) {
            return 23;
        }
        if( strFieldName.equals("PushFlag") ) {
            return 24;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "SelNO";
                break;
            case 1:
                strFieldName = "ContSchemeCode";
                break;
            case 2:
                strFieldName = "ContSchemeName";
                break;
            case 3:
                strFieldName = "SchemeType";
                break;
            case 4:
                strFieldName = "SchemeRule";
                break;
            case 5:
                strFieldName = "SchemeSQL";
                break;
            case 6:
                strFieldName = "Remark";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "ModifyDate";
                break;
            case 11:
                strFieldName = "ModifyTime";
                break;
            case 12:
                strFieldName = "Peoples3";
                break;
            case 13:
                strFieldName = "Remark2";
                break;
            case 14:
                strFieldName = "ManageCom";
                break;
            case 15:
                strFieldName = "Salechnl";
                break;
            case 16:
                strFieldName = "StartDate";
                break;
            case 17:
                strFieldName = "EndDate";
                break;
            case 18:
                strFieldName = "State";
                break;
            case 19:
                strFieldName = "SchemeKind2";
                break;
            case 20:
                strFieldName = "SchemeKind3";
                break;
            case 21:
                strFieldName = "SchemeKind1";
                break;
            case 22:
                strFieldName = "PortfolioFlag";
                break;
            case 23:
                strFieldName = "MedicalFlag";
                break;
            case 24:
                strFieldName = "PushFlag";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "SELNO":
                return Schema.TYPE_STRING;
            case "CONTSCHEMECODE":
                return Schema.TYPE_STRING;
            case "CONTSCHEMENAME":
                return Schema.TYPE_STRING;
            case "SCHEMETYPE":
                return Schema.TYPE_STRING;
            case "SCHEMERULE":
                return Schema.TYPE_STRING;
            case "SCHEMESQL":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "PEOPLES3":
                return Schema.TYPE_INT;
            case "REMARK2":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "SALECHNL":
                return Schema.TYPE_STRING;
            case "STARTDATE":
                return Schema.TYPE_DATE;
            case "ENDDATE":
                return Schema.TYPE_DATE;
            case "STATE":
                return Schema.TYPE_STRING;
            case "SCHEMEKIND2":
                return Schema.TYPE_STRING;
            case "SCHEMEKIND3":
                return Schema.TYPE_STRING;
            case "SCHEMEKIND1":
                return Schema.TYPE_STRING;
            case "PORTFOLIOFLAG":
                return Schema.TYPE_STRING;
            case "MEDICALFLAG":
                return Schema.TYPE_STRING;
            case "PUSHFLAG":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_STRING;
            case 6:
                return Schema.TYPE_STRING;
            case 7:
                return Schema.TYPE_STRING;
            case 8:
                return Schema.TYPE_DATE;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_INT;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_STRING;
            case 16:
                return Schema.TYPE_DATE;
            case 17:
                return Schema.TYPE_DATE;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
