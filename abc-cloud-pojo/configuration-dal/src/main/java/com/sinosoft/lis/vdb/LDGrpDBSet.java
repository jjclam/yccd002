/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.vschema.LDGrpSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LDGrpDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LDGrpDBSet extends LDGrpSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LDGrpDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LDGrp");
        mflag = true;
    }

    public LDGrpDBSet() {
        db = new DBOper( "LDGrp" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LDGrp WHERE  1=1  AND CustomerNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCustomerNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LDGrp SET  CustomerNo = ? , Password = ? , GrpName = ? , BusinessType = ? , GrpNature = ? , Peoples = ? , OnWorkPeoples = ? , InsurePeoples = ? , ComDocIDType = ? , ComDocIDNo = ? , BusliceDate = ? , PostCode = ? , GrpAddress = ? , Fax = ? , UnitRegisteredAddress = ? , UnitDuration = ? , Principal = ? , PrincipalIDType = ? , PrincipalIDNo = ? , PrincipalValidate = ? , Telephone = ? , Mobile = ? , EMail = ? , AnnuityReceiveDate = ? , Managecom = ? , HeadShip = ? , GetFlag = ? , DisputedFlag = ? , BankCode = ? , BankAccNo = ? , AccName = ? , BankProivnce = ? , BankCity = ? , AccType = ? , BankLocations = ? , TaxPayerIDType = ? , TaxPayerIDNO = ? , TaxBureauAddr = ? , TaxBureauTel = ? , TaxBureauBankCode = ? , TaxBureauBankAccNo = ? , BillingType = ? , RgtMoney = ? , Asset = ? , NetProfitRate = ? , MainBussiness = ? , Corporation = ? , ComAera = ? , Phone = ? , Satrap = ? , FoundDate = ? , GrpGroupNo = ? , BlacklistFlag = ? , State = ? , Remark = ? , VIPValue = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , SubCompanyFlag = ? , SupCustoemrNo = ? , LevelCode = ? , OffWorkPeoples = ? , OtherPeoples = ? , BusinessBigType = ? , SocialInsuNo = ? , BlackListReason = ? , ComNo = ? , ComWebsite = ? , BusRegNum = ? , CorporationIDNo = ? , ComCorporationNo = ? , ContrShar = ? , HodingIDType = ? , HodingIDNo = ? , HodingValidate = ? , CorporationIDType = ? , CorporationValidate = ? WHERE  1=1  AND CustomerNo = ?");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCustomerNo());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getPassword());
            }
            if(this.get(i).getGrpName() == null || this.get(i).getGrpName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGrpName());
            }
            if(this.get(i).getBusinessType() == null || this.get(i).getBusinessType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBusinessType());
            }
            if(this.get(i).getGrpNature() == null || this.get(i).getGrpNature().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getGrpNature());
            }
            pstmt.setInt(6, this.get(i).getPeoples());
            pstmt.setInt(7, this.get(i).getOnWorkPeoples());
            pstmt.setInt(8, this.get(i).getInsurePeoples());
            if(this.get(i).getComDocIDType() == null || this.get(i).getComDocIDType().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getComDocIDType());
            }
            if(this.get(i).getComDocIDNo() == null || this.get(i).getComDocIDNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getComDocIDNo());
            }
            if(this.get(i).getBusliceDate() == null || this.get(i).getBusliceDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getBusliceDate()));
            }
            if(this.get(i).getPostCode() == null || this.get(i).getPostCode().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getPostCode());
            }
            if(this.get(i).getGrpAddress() == null || this.get(i).getGrpAddress().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getGrpAddress());
            }
            if(this.get(i).getFax() == null || this.get(i).getFax().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getFax());
            }
            if(this.get(i).getUnitRegisteredAddress() == null || this.get(i).getUnitRegisteredAddress().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getUnitRegisteredAddress());
            }
            pstmt.setDouble(16, this.get(i).getUnitDuration());
            if(this.get(i).getPrincipal() == null || this.get(i).getPrincipal().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getPrincipal());
            }
            if(this.get(i).getPrincipalIDType() == null || this.get(i).getPrincipalIDType().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getPrincipalIDType());
            }
            if(this.get(i).getPrincipalIDNo() == null || this.get(i).getPrincipalIDNo().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getPrincipalIDNo());
            }
            if(this.get(i).getPrincipalValidate() == null || this.get(i).getPrincipalValidate().equals("null")) {
                pstmt.setDate(20,null);
            } else {
                pstmt.setDate(20, Date.valueOf(this.get(i).getPrincipalValidate()));
            }
            if(this.get(i).getTelephone() == null || this.get(i).getTelephone().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getTelephone());
            }
            if(this.get(i).getMobile() == null || this.get(i).getMobile().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMobile());
            }
            if(this.get(i).getEMail() == null || this.get(i).getEMail().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getEMail());
            }
            if(this.get(i).getAnnuityReceiveDate() == null || this.get(i).getAnnuityReceiveDate().equals("null")) {
                pstmt.setDate(24,null);
            } else {
                pstmt.setDate(24, Date.valueOf(this.get(i).getAnnuityReceiveDate()));
            }
            if(this.get(i).getManagecom() == null || this.get(i).getManagecom().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getManagecom());
            }
            if(this.get(i).getHeadShip() == null || this.get(i).getHeadShip().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getHeadShip());
            }
            if(this.get(i).getGetFlag() == null || this.get(i).getGetFlag().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getGetFlag());
            }
            if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getDisputedFlag());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getBankCode());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getBankAccNo());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getAccName());
            }
            if(this.get(i).getBankProivnce() == null || this.get(i).getBankProivnce().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getBankProivnce());
            }
            if(this.get(i).getBankCity() == null || this.get(i).getBankCity().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getBankCity());
            }
            if(this.get(i).getAccType() == null || this.get(i).getAccType().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getAccType());
            }
            if(this.get(i).getBankLocations() == null || this.get(i).getBankLocations().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getBankLocations());
            }
            if(this.get(i).getTaxPayerIDType() == null || this.get(i).getTaxPayerIDType().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getTaxPayerIDType());
            }
            if(this.get(i).getTaxPayerIDNO() == null || this.get(i).getTaxPayerIDNO().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getTaxPayerIDNO());
            }
            if(this.get(i).getTaxBureauAddr() == null || this.get(i).getTaxBureauAddr().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getTaxBureauAddr());
            }
            if(this.get(i).getTaxBureauTel() == null || this.get(i).getTaxBureauTel().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getTaxBureauTel());
            }
            if(this.get(i).getTaxBureauBankCode() == null || this.get(i).getTaxBureauBankCode().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getTaxBureauBankCode());
            }
            if(this.get(i).getTaxBureauBankAccNo() == null || this.get(i).getTaxBureauBankAccNo().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getTaxBureauBankAccNo());
            }
            if(this.get(i).getBillingType() == null || this.get(i).getBillingType().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getBillingType());
            }
            pstmt.setDouble(43, this.get(i).getRgtMoney());
            pstmt.setDouble(44, this.get(i).getAsset());
            pstmt.setDouble(45, this.get(i).getNetProfitRate());
            if(this.get(i).getMainBussiness() == null || this.get(i).getMainBussiness().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getMainBussiness());
            }
            if(this.get(i).getCorporation() == null || this.get(i).getCorporation().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getCorporation());
            }
            if(this.get(i).getComAera() == null || this.get(i).getComAera().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getComAera());
            }
            if(this.get(i).getPhone() == null || this.get(i).getPhone().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getPhone());
            }
            if(this.get(i).getSatrap() == null || this.get(i).getSatrap().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getSatrap());
            }
            if(this.get(i).getFoundDate() == null || this.get(i).getFoundDate().equals("null")) {
                pstmt.setDate(51,null);
            } else {
                pstmt.setDate(51, Date.valueOf(this.get(i).getFoundDate()));
            }
            if(this.get(i).getGrpGroupNo() == null || this.get(i).getGrpGroupNo().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getGrpGroupNo());
            }
            if(this.get(i).getBlacklistFlag() == null || this.get(i).getBlacklistFlag().equals("null")) {
            	pstmt.setString(53,null);
            } else {
            	pstmt.setString(53, this.get(i).getBlacklistFlag());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getState());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getRemark());
            }
            if(this.get(i).getVIPValue() == null || this.get(i).getVIPValue().equals("null")) {
            	pstmt.setString(56,null);
            } else {
            	pstmt.setString(56, this.get(i).getVIPValue());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(57,null);
            } else {
            	pstmt.setString(57, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(58,null);
            } else {
                pstmt.setDate(58, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(60,null);
            } else {
                pstmt.setDate(60, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getModifyTime());
            }
            if(this.get(i).getSubCompanyFlag() == null || this.get(i).getSubCompanyFlag().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getSubCompanyFlag());
            }
            if(this.get(i).getSupCustoemrNo() == null || this.get(i).getSupCustoemrNo().equals("null")) {
            	pstmt.setString(63,null);
            } else {
            	pstmt.setString(63, this.get(i).getSupCustoemrNo());
            }
            if(this.get(i).getLevelCode() == null || this.get(i).getLevelCode().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getLevelCode());
            }
            pstmt.setInt(65, this.get(i).getOffWorkPeoples());
            pstmt.setInt(66, this.get(i).getOtherPeoples());
            if(this.get(i).getBusinessBigType() == null || this.get(i).getBusinessBigType().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getBusinessBigType());
            }
            if(this.get(i).getSocialInsuNo() == null || this.get(i).getSocialInsuNo().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getSocialInsuNo());
            }
            if(this.get(i).getBlackListReason() == null || this.get(i).getBlackListReason().equals("null")) {
            	pstmt.setString(69,null);
            } else {
            	pstmt.setString(69, this.get(i).getBlackListReason());
            }
            if(this.get(i).getComNo() == null || this.get(i).getComNo().equals("null")) {
            	pstmt.setString(70,null);
            } else {
            	pstmt.setString(70, this.get(i).getComNo());
            }
            if(this.get(i).getComWebsite() == null || this.get(i).getComWebsite().equals("null")) {
            	pstmt.setString(71,null);
            } else {
            	pstmt.setString(71, this.get(i).getComWebsite());
            }
            if(this.get(i).getBusRegNum() == null || this.get(i).getBusRegNum().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getBusRegNum());
            }
            if(this.get(i).getCorporationIDNo() == null || this.get(i).getCorporationIDNo().equals("null")) {
            	pstmt.setString(73,null);
            } else {
            	pstmt.setString(73, this.get(i).getCorporationIDNo());
            }
            if(this.get(i).getComCorporationNo() == null || this.get(i).getComCorporationNo().equals("null")) {
            	pstmt.setString(74,null);
            } else {
            	pstmt.setString(74, this.get(i).getComCorporationNo());
            }
            if(this.get(i).getContrShar() == null || this.get(i).getContrShar().equals("null")) {
            	pstmt.setString(75,null);
            } else {
            	pstmt.setString(75, this.get(i).getContrShar());
            }
            if(this.get(i).getHodingIDType() == null || this.get(i).getHodingIDType().equals("null")) {
            	pstmt.setString(76,null);
            } else {
            	pstmt.setString(76, this.get(i).getHodingIDType());
            }
            if(this.get(i).getHodingIDNo() == null || this.get(i).getHodingIDNo().equals("null")) {
            	pstmt.setString(77,null);
            } else {
            	pstmt.setString(77, this.get(i).getHodingIDNo());
            }
            if(this.get(i).getHodingValidate() == null || this.get(i).getHodingValidate().equals("null")) {
                pstmt.setDate(78,null);
            } else {
                pstmt.setDate(78, Date.valueOf(this.get(i).getHodingValidate()));
            }
            if(this.get(i).getCorporationIDType() == null || this.get(i).getCorporationIDType().equals("null")) {
            	pstmt.setString(79,null);
            } else {
            	pstmt.setString(79, this.get(i).getCorporationIDType());
            }
            if(this.get(i).getCorporationValidate() == null || this.get(i).getCorporationValidate().equals("null")) {
                pstmt.setDate(80,null);
            } else {
                pstmt.setDate(80, Date.valueOf(this.get(i).getCorporationValidate()));
            }
            // set where condition
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(81,null);
            } else {
            	pstmt.setString(81, this.get(i).getCustomerNo());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LDGrp VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            if(this.get(i).getCustomerNo() == null || this.get(i).getCustomerNo().equals("null")) {
            	pstmt.setString(1,null);
            } else {
            	pstmt.setString(1, this.get(i).getCustomerNo());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getPassword());
            }
            if(this.get(i).getGrpName() == null || this.get(i).getGrpName().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGrpName());
            }
            if(this.get(i).getBusinessType() == null || this.get(i).getBusinessType().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getBusinessType());
            }
            if(this.get(i).getGrpNature() == null || this.get(i).getGrpNature().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getGrpNature());
            }
            pstmt.setInt(6, this.get(i).getPeoples());
            pstmt.setInt(7, this.get(i).getOnWorkPeoples());
            pstmt.setInt(8, this.get(i).getInsurePeoples());
            if(this.get(i).getComDocIDType() == null || this.get(i).getComDocIDType().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getComDocIDType());
            }
            if(this.get(i).getComDocIDNo() == null || this.get(i).getComDocIDNo().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getComDocIDNo());
            }
            if(this.get(i).getBusliceDate() == null || this.get(i).getBusliceDate().equals("null")) {
                pstmt.setDate(11,null);
            } else {
                pstmt.setDate(11, Date.valueOf(this.get(i).getBusliceDate()));
            }
            if(this.get(i).getPostCode() == null || this.get(i).getPostCode().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getPostCode());
            }
            if(this.get(i).getGrpAddress() == null || this.get(i).getGrpAddress().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getGrpAddress());
            }
            if(this.get(i).getFax() == null || this.get(i).getFax().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getFax());
            }
            if(this.get(i).getUnitRegisteredAddress() == null || this.get(i).getUnitRegisteredAddress().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getUnitRegisteredAddress());
            }
            pstmt.setDouble(16, this.get(i).getUnitDuration());
            if(this.get(i).getPrincipal() == null || this.get(i).getPrincipal().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getPrincipal());
            }
            if(this.get(i).getPrincipalIDType() == null || this.get(i).getPrincipalIDType().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getPrincipalIDType());
            }
            if(this.get(i).getPrincipalIDNo() == null || this.get(i).getPrincipalIDNo().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getPrincipalIDNo());
            }
            if(this.get(i).getPrincipalValidate() == null || this.get(i).getPrincipalValidate().equals("null")) {
                pstmt.setDate(20,null);
            } else {
                pstmt.setDate(20, Date.valueOf(this.get(i).getPrincipalValidate()));
            }
            if(this.get(i).getTelephone() == null || this.get(i).getTelephone().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getTelephone());
            }
            if(this.get(i).getMobile() == null || this.get(i).getMobile().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getMobile());
            }
            if(this.get(i).getEMail() == null || this.get(i).getEMail().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getEMail());
            }
            if(this.get(i).getAnnuityReceiveDate() == null || this.get(i).getAnnuityReceiveDate().equals("null")) {
                pstmt.setDate(24,null);
            } else {
                pstmt.setDate(24, Date.valueOf(this.get(i).getAnnuityReceiveDate()));
            }
            if(this.get(i).getManagecom() == null || this.get(i).getManagecom().equals("null")) {
            	pstmt.setString(25,null);
            } else {
            	pstmt.setString(25, this.get(i).getManagecom());
            }
            if(this.get(i).getHeadShip() == null || this.get(i).getHeadShip().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getHeadShip());
            }
            if(this.get(i).getGetFlag() == null || this.get(i).getGetFlag().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getGetFlag());
            }
            if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getDisputedFlag());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getBankCode());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getBankAccNo());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(31,null);
            } else {
            	pstmt.setString(31, this.get(i).getAccName());
            }
            if(this.get(i).getBankProivnce() == null || this.get(i).getBankProivnce().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getBankProivnce());
            }
            if(this.get(i).getBankCity() == null || this.get(i).getBankCity().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getBankCity());
            }
            if(this.get(i).getAccType() == null || this.get(i).getAccType().equals("null")) {
            	pstmt.setString(34,null);
            } else {
            	pstmt.setString(34, this.get(i).getAccType());
            }
            if(this.get(i).getBankLocations() == null || this.get(i).getBankLocations().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getBankLocations());
            }
            if(this.get(i).getTaxPayerIDType() == null || this.get(i).getTaxPayerIDType().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getTaxPayerIDType());
            }
            if(this.get(i).getTaxPayerIDNO() == null || this.get(i).getTaxPayerIDNO().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getTaxPayerIDNO());
            }
            if(this.get(i).getTaxBureauAddr() == null || this.get(i).getTaxBureauAddr().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getTaxBureauAddr());
            }
            if(this.get(i).getTaxBureauTel() == null || this.get(i).getTaxBureauTel().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getTaxBureauTel());
            }
            if(this.get(i).getTaxBureauBankCode() == null || this.get(i).getTaxBureauBankCode().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getTaxBureauBankCode());
            }
            if(this.get(i).getTaxBureauBankAccNo() == null || this.get(i).getTaxBureauBankAccNo().equals("null")) {
            	pstmt.setString(41,null);
            } else {
            	pstmt.setString(41, this.get(i).getTaxBureauBankAccNo());
            }
            if(this.get(i).getBillingType() == null || this.get(i).getBillingType().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getBillingType());
            }
            pstmt.setDouble(43, this.get(i).getRgtMoney());
            pstmt.setDouble(44, this.get(i).getAsset());
            pstmt.setDouble(45, this.get(i).getNetProfitRate());
            if(this.get(i).getMainBussiness() == null || this.get(i).getMainBussiness().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getMainBussiness());
            }
            if(this.get(i).getCorporation() == null || this.get(i).getCorporation().equals("null")) {
            	pstmt.setString(47,null);
            } else {
            	pstmt.setString(47, this.get(i).getCorporation());
            }
            if(this.get(i).getComAera() == null || this.get(i).getComAera().equals("null")) {
            	pstmt.setString(48,null);
            } else {
            	pstmt.setString(48, this.get(i).getComAera());
            }
            if(this.get(i).getPhone() == null || this.get(i).getPhone().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getPhone());
            }
            if(this.get(i).getSatrap() == null || this.get(i).getSatrap().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getSatrap());
            }
            if(this.get(i).getFoundDate() == null || this.get(i).getFoundDate().equals("null")) {
                pstmt.setDate(51,null);
            } else {
                pstmt.setDate(51, Date.valueOf(this.get(i).getFoundDate()));
            }
            if(this.get(i).getGrpGroupNo() == null || this.get(i).getGrpGroupNo().equals("null")) {
            	pstmt.setString(52,null);
            } else {
            	pstmt.setString(52, this.get(i).getGrpGroupNo());
            }
            if(this.get(i).getBlacklistFlag() == null || this.get(i).getBlacklistFlag().equals("null")) {
            	pstmt.setString(53,null);
            } else {
            	pstmt.setString(53, this.get(i).getBlacklistFlag());
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(54,null);
            } else {
            	pstmt.setString(54, this.get(i).getState());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(55,null);
            } else {
            	pstmt.setString(55, this.get(i).getRemark());
            }
            if(this.get(i).getVIPValue() == null || this.get(i).getVIPValue().equals("null")) {
            	pstmt.setString(56,null);
            } else {
            	pstmt.setString(56, this.get(i).getVIPValue());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(57,null);
            } else {
            	pstmt.setString(57, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(58,null);
            } else {
                pstmt.setDate(58, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(59,null);
            } else {
            	pstmt.setString(59, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(60,null);
            } else {
                pstmt.setDate(60, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getModifyTime());
            }
            if(this.get(i).getSubCompanyFlag() == null || this.get(i).getSubCompanyFlag().equals("null")) {
            	pstmt.setString(62,null);
            } else {
            	pstmt.setString(62, this.get(i).getSubCompanyFlag());
            }
            if(this.get(i).getSupCustoemrNo() == null || this.get(i).getSupCustoemrNo().equals("null")) {
            	pstmt.setString(63,null);
            } else {
            	pstmt.setString(63, this.get(i).getSupCustoemrNo());
            }
            if(this.get(i).getLevelCode() == null || this.get(i).getLevelCode().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getLevelCode());
            }
            pstmt.setInt(65, this.get(i).getOffWorkPeoples());
            pstmt.setInt(66, this.get(i).getOtherPeoples());
            if(this.get(i).getBusinessBigType() == null || this.get(i).getBusinessBigType().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getBusinessBigType());
            }
            if(this.get(i).getSocialInsuNo() == null || this.get(i).getSocialInsuNo().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getSocialInsuNo());
            }
            if(this.get(i).getBlackListReason() == null || this.get(i).getBlackListReason().equals("null")) {
            	pstmt.setString(69,null);
            } else {
            	pstmt.setString(69, this.get(i).getBlackListReason());
            }
            if(this.get(i).getComNo() == null || this.get(i).getComNo().equals("null")) {
            	pstmt.setString(70,null);
            } else {
            	pstmt.setString(70, this.get(i).getComNo());
            }
            if(this.get(i).getComWebsite() == null || this.get(i).getComWebsite().equals("null")) {
            	pstmt.setString(71,null);
            } else {
            	pstmt.setString(71, this.get(i).getComWebsite());
            }
            if(this.get(i).getBusRegNum() == null || this.get(i).getBusRegNum().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getBusRegNum());
            }
            if(this.get(i).getCorporationIDNo() == null || this.get(i).getCorporationIDNo().equals("null")) {
            	pstmt.setString(73,null);
            } else {
            	pstmt.setString(73, this.get(i).getCorporationIDNo());
            }
            if(this.get(i).getComCorporationNo() == null || this.get(i).getComCorporationNo().equals("null")) {
            	pstmt.setString(74,null);
            } else {
            	pstmt.setString(74, this.get(i).getComCorporationNo());
            }
            if(this.get(i).getContrShar() == null || this.get(i).getContrShar().equals("null")) {
            	pstmt.setString(75,null);
            } else {
            	pstmt.setString(75, this.get(i).getContrShar());
            }
            if(this.get(i).getHodingIDType() == null || this.get(i).getHodingIDType().equals("null")) {
            	pstmt.setString(76,null);
            } else {
            	pstmt.setString(76, this.get(i).getHodingIDType());
            }
            if(this.get(i).getHodingIDNo() == null || this.get(i).getHodingIDNo().equals("null")) {
            	pstmt.setString(77,null);
            } else {
            	pstmt.setString(77, this.get(i).getHodingIDNo());
            }
            if(this.get(i).getHodingValidate() == null || this.get(i).getHodingValidate().equals("null")) {
                pstmt.setDate(78,null);
            } else {
                pstmt.setDate(78, Date.valueOf(this.get(i).getHodingValidate()));
            }
            if(this.get(i).getCorporationIDType() == null || this.get(i).getCorporationIDType().equals("null")) {
            	pstmt.setString(79,null);
            } else {
            	pstmt.setString(79, this.get(i).getCorporationIDType());
            }
            if(this.get(i).getCorporationValidate() == null || this.get(i).getCorporationValidate().equals("null")) {
                pstmt.setDate(80,null);
            } else {
                pstmt.setDate(80, Date.valueOf(this.get(i).getCorporationValidate()));
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDGrpDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
