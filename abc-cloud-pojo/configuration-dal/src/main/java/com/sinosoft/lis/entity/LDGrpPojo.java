/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.entity;

import java.io.*;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.StrTool;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>ClassName: LDGrpPojo </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LDGrpPojo implements  Pojo,Serializable {
    // @Field
    /** 客户号码 */
    private String CustomerNo; 
    /** 密码 */
    private String Password; 
    /** 单位名称 */
    private String GrpName; 
    /** 行业分类 */
    private String BusinessType; 
    /** 单位性质 */
    private String GrpNature; 
    /** 总人数 */
    private int Peoples; 
    /** 在职人数 */
    private int OnWorkPeoples; 
    /** 投保人数 */
    private int InsurePeoples; 
    /** 单位证件类型 */
    private String ComDocIDType; 
    /** 单位证件号码 */
    private String ComDocIDNo; 
    /** 营业执照有效期 */
    private String  BusliceDate;
    /** 邮政编码 */
    private String PostCode; 
    /** 单位地址 */
    private String GrpAddress; 
    /** 单位传真 */
    private String Fax; 
    /** 单位注册地址 */
    private String UnitRegisteredAddress; 
    /** 单位存续时间 */
    private double UnitDuration; 
    /** 负责人姓名 */
    private String Principal; 
    /** 负责人证件类型 */
    private String PrincipalIDType; 
    /** 负责人证件号码 */
    private String PrincipalIDNo; 
    /** 负责人证件有效期限 */
    private String  PrincipalValidate;
    /** 联系电话 */
    private String Telephone; 
    /** 手机号码 */
    private String Mobile; 
    /** 公司e_mail */
    private String EMail; 
    /** 保单年金领取日 */
    private String  AnnuityReceiveDate;
    /** 所属部门 */
    private String Managecom; 
    /** 职务 */
    private String HeadShip; 
    /** 付款方式 */
    private String GetFlag; 
    /** 争议处理方式 */
    private String DisputedFlag; 
    /** 银行编码 */
    private String BankCode; 
    /** 银行帐号 */
    private String BankAccNo; 
    /** 账户名 */
    private String AccName; 
    /** 开户行所在省 */
    private String BankProivnce; 
    /** 开户行所在市 */
    private String BankCity; 
    /** 卡折类型 */
    private String AccType; 
    /** 银行网点 */
    private String BankLocations; 
    /** 纳税人身份类型 */
    private String TaxPayerIDType; 
    /** 纳税人识别号 */
    private String TaxPayerIDNO; 
    /** 税局登记地址 */
    private String TaxBureauAddr; 
    /** 税局登记电话 */
    private String TaxBureauTel; 
    /** 税局开户行 */
    private String TaxBureauBankCode; 
    /** 税局登记账号 */
    private String TaxBureauBankAccNo; 
    /** 开票类型 */
    private String BillingType; 
    /** 注册资本 */
    private double RgtMoney; 
    /** 资产总额 */
    private double Asset; 
    /** 净资产收益率 */
    private double NetProfitRate; 
    /** 主营业务 */
    private String MainBussiness; 
    /** 法人 */
    private String Corporation; 
    /** 机构分布区域 */
    private String ComAera; 
    /** 单位电话 */
    private String Phone; 
    /** 负责人 */
    private String Satrap; 
    /** 成立日期 */
    private String  FoundDate;
    /** 客户组号码 */
    private String GrpGroupNo; 
    /** 黑名单标记 */
    private String BlacklistFlag; 
    /** 状态 */
    private String State; 
    /** 备注 */
    private String Remark; 
    /** Vip值 */
    private String VIPValue; 
    /** 操作员代码 */
    private String Operator; 
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime; 
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime; 
    /** 子公司标志 */
    private String SubCompanyFlag; 
    /** 上级客户号码 */
    private String SupCustoemrNo; 
    /** 级别代码 */
    private String LevelCode; 
    /** 退休人数 */
    private int OffWorkPeoples; 
    /** 其它人员人数 */
    private int OtherPeoples; 
    /** 行业大类 */
    private String BusinessBigType; 
    /** 单位社保登记号 */
    private String SocialInsuNo; 
    /** 黑名单原因 */
    private String BlackListReason; 
    /** 组织机构代码 */
    private String ComNo; 
    /** 单位网址 */
    private String ComWebsite; 
    /** 工商注册号 */
    private String BusRegNum; 
    /** 法人代表证件号 */
    private String CorporationIDNo; 
    /** 单位法人代码 */
    private String ComCorporationNo; 
    /** 实际控制人 */
    private String ContrShar; 
    /** 实际控股人证件类型 */
    private String HodingIDType; 
    /** 实际控股人证件号码 */
    private String HodingIDNo; 
    /** 实际控股人有效期限 */
    private String  HodingValidate;
    /** 法定代表人证件类型 */
    private String CorporationIDType; 
    /** 法定代表人有效期限 */
    private String  CorporationValidate;


    public static final int FIELDNUM = 80;    // 数据库表的字段个数

    private FDate fDate = new FDate();        // 处理日期
    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getBusinessType() {
        return BusinessType;
    }
    public void setBusinessType(String aBusinessType) {
        BusinessType = aBusinessType;
    }
    public String getGrpNature() {
        return GrpNature;
    }
    public void setGrpNature(String aGrpNature) {
        GrpNature = aGrpNature;
    }
    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public int getOnWorkPeoples() {
        return OnWorkPeoples;
    }
    public void setOnWorkPeoples(int aOnWorkPeoples) {
        OnWorkPeoples = aOnWorkPeoples;
    }
    public void setOnWorkPeoples(String aOnWorkPeoples) {
        if (aOnWorkPeoples != null && !aOnWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOnWorkPeoples);
            int i = tInteger.intValue();
            OnWorkPeoples = i;
        }
    }

    public int getInsurePeoples() {
        return InsurePeoples;
    }
    public void setInsurePeoples(int aInsurePeoples) {
        InsurePeoples = aInsurePeoples;
    }
    public void setInsurePeoples(String aInsurePeoples) {
        if (aInsurePeoples != null && !aInsurePeoples.equals("")) {
            Integer tInteger = new Integer(aInsurePeoples);
            int i = tInteger.intValue();
            InsurePeoples = i;
        }
    }

    public String getComDocIDType() {
        return ComDocIDType;
    }
    public void setComDocIDType(String aComDocIDType) {
        ComDocIDType = aComDocIDType;
    }
    public String getComDocIDNo() {
        return ComDocIDNo;
    }
    public void setComDocIDNo(String aComDocIDNo) {
        ComDocIDNo = aComDocIDNo;
    }
    public String getBusliceDate() {
        return BusliceDate;
    }
    public void setBusliceDate(String aBusliceDate) {
        BusliceDate = aBusliceDate;
    }
    public String getPostCode() {
        return PostCode;
    }
    public void setPostCode(String aPostCode) {
        PostCode = aPostCode;
    }
    public String getGrpAddress() {
        return GrpAddress;
    }
    public void setGrpAddress(String aGrpAddress) {
        GrpAddress = aGrpAddress;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getUnitRegisteredAddress() {
        return UnitRegisteredAddress;
    }
    public void setUnitRegisteredAddress(String aUnitRegisteredAddress) {
        UnitRegisteredAddress = aUnitRegisteredAddress;
    }
    public double getUnitDuration() {
        return UnitDuration;
    }
    public void setUnitDuration(double aUnitDuration) {
        UnitDuration = aUnitDuration;
    }
    public void setUnitDuration(String aUnitDuration) {
        if (aUnitDuration != null && !aUnitDuration.equals("")) {
            Double tDouble = new Double(aUnitDuration);
            double d = tDouble.doubleValue();
            UnitDuration = d;
        }
    }

    public String getPrincipal() {
        return Principal;
    }
    public void setPrincipal(String aPrincipal) {
        Principal = aPrincipal;
    }
    public String getPrincipalIDType() {
        return PrincipalIDType;
    }
    public void setPrincipalIDType(String aPrincipalIDType) {
        PrincipalIDType = aPrincipalIDType;
    }
    public String getPrincipalIDNo() {
        return PrincipalIDNo;
    }
    public void setPrincipalIDNo(String aPrincipalIDNo) {
        PrincipalIDNo = aPrincipalIDNo;
    }
    public String getPrincipalValidate() {
        return PrincipalValidate;
    }
    public void setPrincipalValidate(String aPrincipalValidate) {
        PrincipalValidate = aPrincipalValidate;
    }
    public String getTelephone() {
        return Telephone;
    }
    public void setTelephone(String aTelephone) {
        Telephone = aTelephone;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getAnnuityReceiveDate() {
        return AnnuityReceiveDate;
    }
    public void setAnnuityReceiveDate(String aAnnuityReceiveDate) {
        AnnuityReceiveDate = aAnnuityReceiveDate;
    }
    public String getManagecom() {
        return Managecom;
    }
    public void setManagecom(String aManagecom) {
        Managecom = aManagecom;
    }
    public String getHeadShip() {
        return HeadShip;
    }
    public void setHeadShip(String aHeadShip) {
        HeadShip = aHeadShip;
    }
    public String getGetFlag() {
        return GetFlag;
    }
    public void setGetFlag(String aGetFlag) {
        GetFlag = aGetFlag;
    }
    public String getDisputedFlag() {
        return DisputedFlag;
    }
    public void setDisputedFlag(String aDisputedFlag) {
        DisputedFlag = aDisputedFlag;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getBankProivnce() {
        return BankProivnce;
    }
    public void setBankProivnce(String aBankProivnce) {
        BankProivnce = aBankProivnce;
    }
    public String getBankCity() {
        return BankCity;
    }
    public void setBankCity(String aBankCity) {
        BankCity = aBankCity;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getBankLocations() {
        return BankLocations;
    }
    public void setBankLocations(String aBankLocations) {
        BankLocations = aBankLocations;
    }
    public String getTaxPayerIDType() {
        return TaxPayerIDType;
    }
    public void setTaxPayerIDType(String aTaxPayerIDType) {
        TaxPayerIDType = aTaxPayerIDType;
    }
    public String getTaxPayerIDNO() {
        return TaxPayerIDNO;
    }
    public void setTaxPayerIDNO(String aTaxPayerIDNO) {
        TaxPayerIDNO = aTaxPayerIDNO;
    }
    public String getTaxBureauAddr() {
        return TaxBureauAddr;
    }
    public void setTaxBureauAddr(String aTaxBureauAddr) {
        TaxBureauAddr = aTaxBureauAddr;
    }
    public String getTaxBureauTel() {
        return TaxBureauTel;
    }
    public void setTaxBureauTel(String aTaxBureauTel) {
        TaxBureauTel = aTaxBureauTel;
    }
    public String getTaxBureauBankCode() {
        return TaxBureauBankCode;
    }
    public void setTaxBureauBankCode(String aTaxBureauBankCode) {
        TaxBureauBankCode = aTaxBureauBankCode;
    }
    public String getTaxBureauBankAccNo() {
        return TaxBureauBankAccNo;
    }
    public void setTaxBureauBankAccNo(String aTaxBureauBankAccNo) {
        TaxBureauBankAccNo = aTaxBureauBankAccNo;
    }
    public String getBillingType() {
        return BillingType;
    }
    public void setBillingType(String aBillingType) {
        BillingType = aBillingType;
    }
    public double getRgtMoney() {
        return RgtMoney;
    }
    public void setRgtMoney(double aRgtMoney) {
        RgtMoney = aRgtMoney;
    }
    public void setRgtMoney(String aRgtMoney) {
        if (aRgtMoney != null && !aRgtMoney.equals("")) {
            Double tDouble = new Double(aRgtMoney);
            double d = tDouble.doubleValue();
            RgtMoney = d;
        }
    }

    public double getAsset() {
        return Asset;
    }
    public void setAsset(double aAsset) {
        Asset = aAsset;
    }
    public void setAsset(String aAsset) {
        if (aAsset != null && !aAsset.equals("")) {
            Double tDouble = new Double(aAsset);
            double d = tDouble.doubleValue();
            Asset = d;
        }
    }

    public double getNetProfitRate() {
        return NetProfitRate;
    }
    public void setNetProfitRate(double aNetProfitRate) {
        NetProfitRate = aNetProfitRate;
    }
    public void setNetProfitRate(String aNetProfitRate) {
        if (aNetProfitRate != null && !aNetProfitRate.equals("")) {
            Double tDouble = new Double(aNetProfitRate);
            double d = tDouble.doubleValue();
            NetProfitRate = d;
        }
    }

    public String getMainBussiness() {
        return MainBussiness;
    }
    public void setMainBussiness(String aMainBussiness) {
        MainBussiness = aMainBussiness;
    }
    public String getCorporation() {
        return Corporation;
    }
    public void setCorporation(String aCorporation) {
        Corporation = aCorporation;
    }
    public String getComAera() {
        return ComAera;
    }
    public void setComAera(String aComAera) {
        ComAera = aComAera;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getSatrap() {
        return Satrap;
    }
    public void setSatrap(String aSatrap) {
        Satrap = aSatrap;
    }
    public String getFoundDate() {
        return FoundDate;
    }
    public void setFoundDate(String aFoundDate) {
        FoundDate = aFoundDate;
    }
    public String getGrpGroupNo() {
        return GrpGroupNo;
    }
    public void setGrpGroupNo(String aGrpGroupNo) {
        GrpGroupNo = aGrpGroupNo;
    }
    public String getBlacklistFlag() {
        return BlacklistFlag;
    }
    public void setBlacklistFlag(String aBlacklistFlag) {
        BlacklistFlag = aBlacklistFlag;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getVIPValue() {
        return VIPValue;
    }
    public void setVIPValue(String aVIPValue) {
        VIPValue = aVIPValue;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        return MakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        MakeDate = aMakeDate;
    }
    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        return ModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getSubCompanyFlag() {
        return SubCompanyFlag;
    }
    public void setSubCompanyFlag(String aSubCompanyFlag) {
        SubCompanyFlag = aSubCompanyFlag;
    }
    public String getSupCustoemrNo() {
        return SupCustoemrNo;
    }
    public void setSupCustoemrNo(String aSupCustoemrNo) {
        SupCustoemrNo = aSupCustoemrNo;
    }
    public String getLevelCode() {
        return LevelCode;
    }
    public void setLevelCode(String aLevelCode) {
        LevelCode = aLevelCode;
    }
    public int getOffWorkPeoples() {
        return OffWorkPeoples;
    }
    public void setOffWorkPeoples(int aOffWorkPeoples) {
        OffWorkPeoples = aOffWorkPeoples;
    }
    public void setOffWorkPeoples(String aOffWorkPeoples) {
        if (aOffWorkPeoples != null && !aOffWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOffWorkPeoples);
            int i = tInteger.intValue();
            OffWorkPeoples = i;
        }
    }

    public int getOtherPeoples() {
        return OtherPeoples;
    }
    public void setOtherPeoples(int aOtherPeoples) {
        OtherPeoples = aOtherPeoples;
    }
    public void setOtherPeoples(String aOtherPeoples) {
        if (aOtherPeoples != null && !aOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aOtherPeoples);
            int i = tInteger.intValue();
            OtherPeoples = i;
        }
    }

    public String getBusinessBigType() {
        return BusinessBigType;
    }
    public void setBusinessBigType(String aBusinessBigType) {
        BusinessBigType = aBusinessBigType;
    }
    public String getSocialInsuNo() {
        return SocialInsuNo;
    }
    public void setSocialInsuNo(String aSocialInsuNo) {
        SocialInsuNo = aSocialInsuNo;
    }
    public String getBlackListReason() {
        return BlackListReason;
    }
    public void setBlackListReason(String aBlackListReason) {
        BlackListReason = aBlackListReason;
    }
    public String getComNo() {
        return ComNo;
    }
    public void setComNo(String aComNo) {
        ComNo = aComNo;
    }
    public String getComWebsite() {
        return ComWebsite;
    }
    public void setComWebsite(String aComWebsite) {
        ComWebsite = aComWebsite;
    }
    public String getBusRegNum() {
        return BusRegNum;
    }
    public void setBusRegNum(String aBusRegNum) {
        BusRegNum = aBusRegNum;
    }
    public String getCorporationIDNo() {
        return CorporationIDNo;
    }
    public void setCorporationIDNo(String aCorporationIDNo) {
        CorporationIDNo = aCorporationIDNo;
    }
    public String getComCorporationNo() {
        return ComCorporationNo;
    }
    public void setComCorporationNo(String aComCorporationNo) {
        ComCorporationNo = aComCorporationNo;
    }
    public String getContrShar() {
        return ContrShar;
    }
    public void setContrShar(String aContrShar) {
        ContrShar = aContrShar;
    }
    public String getHodingIDType() {
        return HodingIDType;
    }
    public void setHodingIDType(String aHodingIDType) {
        HodingIDType = aHodingIDType;
    }
    public String getHodingIDNo() {
        return HodingIDNo;
    }
    public void setHodingIDNo(String aHodingIDNo) {
        HodingIDNo = aHodingIDNo;
    }
    public String getHodingValidate() {
        return HodingValidate;
    }
    public void setHodingValidate(String aHodingValidate) {
        HodingValidate = aHodingValidate;
    }
    public String getCorporationIDType() {
        return CorporationIDType;
    }
    public void setCorporationIDType(String aCorporationIDType) {
        CorporationIDType = aCorporationIDType;
    }
    public String getCorporationValidate() {
        return CorporationValidate;
    }
    public void setCorporationValidate(String aCorporationValidate) {
        CorporationValidate = aCorporationValidate;
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CustomerNo") ) {
            return 0;
        }
        if( strFieldName.equals("Password") ) {
            return 1;
        }
        if( strFieldName.equals("GrpName") ) {
            return 2;
        }
        if( strFieldName.equals("BusinessType") ) {
            return 3;
        }
        if( strFieldName.equals("GrpNature") ) {
            return 4;
        }
        if( strFieldName.equals("Peoples") ) {
            return 5;
        }
        if( strFieldName.equals("OnWorkPeoples") ) {
            return 6;
        }
        if( strFieldName.equals("InsurePeoples") ) {
            return 7;
        }
        if( strFieldName.equals("ComDocIDType") ) {
            return 8;
        }
        if( strFieldName.equals("ComDocIDNo") ) {
            return 9;
        }
        if( strFieldName.equals("BusliceDate") ) {
            return 10;
        }
        if( strFieldName.equals("PostCode") ) {
            return 11;
        }
        if( strFieldName.equals("GrpAddress") ) {
            return 12;
        }
        if( strFieldName.equals("Fax") ) {
            return 13;
        }
        if( strFieldName.equals("UnitRegisteredAddress") ) {
            return 14;
        }
        if( strFieldName.equals("UnitDuration") ) {
            return 15;
        }
        if( strFieldName.equals("Principal") ) {
            return 16;
        }
        if( strFieldName.equals("PrincipalIDType") ) {
            return 17;
        }
        if( strFieldName.equals("PrincipalIDNo") ) {
            return 18;
        }
        if( strFieldName.equals("PrincipalValidate") ) {
            return 19;
        }
        if( strFieldName.equals("Telephone") ) {
            return 20;
        }
        if( strFieldName.equals("Mobile") ) {
            return 21;
        }
        if( strFieldName.equals("EMail") ) {
            return 22;
        }
        if( strFieldName.equals("AnnuityReceiveDate") ) {
            return 23;
        }
        if( strFieldName.equals("Managecom") ) {
            return 24;
        }
        if( strFieldName.equals("HeadShip") ) {
            return 25;
        }
        if( strFieldName.equals("GetFlag") ) {
            return 26;
        }
        if( strFieldName.equals("DisputedFlag") ) {
            return 27;
        }
        if( strFieldName.equals("BankCode") ) {
            return 28;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 29;
        }
        if( strFieldName.equals("AccName") ) {
            return 30;
        }
        if( strFieldName.equals("BankProivnce") ) {
            return 31;
        }
        if( strFieldName.equals("BankCity") ) {
            return 32;
        }
        if( strFieldName.equals("AccType") ) {
            return 33;
        }
        if( strFieldName.equals("BankLocations") ) {
            return 34;
        }
        if( strFieldName.equals("TaxPayerIDType") ) {
            return 35;
        }
        if( strFieldName.equals("TaxPayerIDNO") ) {
            return 36;
        }
        if( strFieldName.equals("TaxBureauAddr") ) {
            return 37;
        }
        if( strFieldName.equals("TaxBureauTel") ) {
            return 38;
        }
        if( strFieldName.equals("TaxBureauBankCode") ) {
            return 39;
        }
        if( strFieldName.equals("TaxBureauBankAccNo") ) {
            return 40;
        }
        if( strFieldName.equals("BillingType") ) {
            return 41;
        }
        if( strFieldName.equals("RgtMoney") ) {
            return 42;
        }
        if( strFieldName.equals("Asset") ) {
            return 43;
        }
        if( strFieldName.equals("NetProfitRate") ) {
            return 44;
        }
        if( strFieldName.equals("MainBussiness") ) {
            return 45;
        }
        if( strFieldName.equals("Corporation") ) {
            return 46;
        }
        if( strFieldName.equals("ComAera") ) {
            return 47;
        }
        if( strFieldName.equals("Phone") ) {
            return 48;
        }
        if( strFieldName.equals("Satrap") ) {
            return 49;
        }
        if( strFieldName.equals("FoundDate") ) {
            return 50;
        }
        if( strFieldName.equals("GrpGroupNo") ) {
            return 51;
        }
        if( strFieldName.equals("BlacklistFlag") ) {
            return 52;
        }
        if( strFieldName.equals("State") ) {
            return 53;
        }
        if( strFieldName.equals("Remark") ) {
            return 54;
        }
        if( strFieldName.equals("VIPValue") ) {
            return 55;
        }
        if( strFieldName.equals("Operator") ) {
            return 56;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 57;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 58;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 59;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 60;
        }
        if( strFieldName.equals("SubCompanyFlag") ) {
            return 61;
        }
        if( strFieldName.equals("SupCustoemrNo") ) {
            return 62;
        }
        if( strFieldName.equals("LevelCode") ) {
            return 63;
        }
        if( strFieldName.equals("OffWorkPeoples") ) {
            return 64;
        }
        if( strFieldName.equals("OtherPeoples") ) {
            return 65;
        }
        if( strFieldName.equals("BusinessBigType") ) {
            return 66;
        }
        if( strFieldName.equals("SocialInsuNo") ) {
            return 67;
        }
        if( strFieldName.equals("BlackListReason") ) {
            return 68;
        }
        if( strFieldName.equals("ComNo") ) {
            return 69;
        }
        if( strFieldName.equals("ComWebsite") ) {
            return 70;
        }
        if( strFieldName.equals("BusRegNum") ) {
            return 71;
        }
        if( strFieldName.equals("CorporationIDNo") ) {
            return 72;
        }
        if( strFieldName.equals("ComCorporationNo") ) {
            return 73;
        }
        if( strFieldName.equals("ContrShar") ) {
            return 74;
        }
        if( strFieldName.equals("HodingIDType") ) {
            return 75;
        }
        if( strFieldName.equals("HodingIDNo") ) {
            return 76;
        }
        if( strFieldName.equals("HodingValidate") ) {
            return 77;
        }
        if( strFieldName.equals("CorporationIDType") ) {
            return 78;
        }
        if( strFieldName.equals("CorporationValidate") ) {
            return 79;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CustomerNo";
                break;
            case 1:
                strFieldName = "Password";
                break;
            case 2:
                strFieldName = "GrpName";
                break;
            case 3:
                strFieldName = "BusinessType";
                break;
            case 4:
                strFieldName = "GrpNature";
                break;
            case 5:
                strFieldName = "Peoples";
                break;
            case 6:
                strFieldName = "OnWorkPeoples";
                break;
            case 7:
                strFieldName = "InsurePeoples";
                break;
            case 8:
                strFieldName = "ComDocIDType";
                break;
            case 9:
                strFieldName = "ComDocIDNo";
                break;
            case 10:
                strFieldName = "BusliceDate";
                break;
            case 11:
                strFieldName = "PostCode";
                break;
            case 12:
                strFieldName = "GrpAddress";
                break;
            case 13:
                strFieldName = "Fax";
                break;
            case 14:
                strFieldName = "UnitRegisteredAddress";
                break;
            case 15:
                strFieldName = "UnitDuration";
                break;
            case 16:
                strFieldName = "Principal";
                break;
            case 17:
                strFieldName = "PrincipalIDType";
                break;
            case 18:
                strFieldName = "PrincipalIDNo";
                break;
            case 19:
                strFieldName = "PrincipalValidate";
                break;
            case 20:
                strFieldName = "Telephone";
                break;
            case 21:
                strFieldName = "Mobile";
                break;
            case 22:
                strFieldName = "EMail";
                break;
            case 23:
                strFieldName = "AnnuityReceiveDate";
                break;
            case 24:
                strFieldName = "Managecom";
                break;
            case 25:
                strFieldName = "HeadShip";
                break;
            case 26:
                strFieldName = "GetFlag";
                break;
            case 27:
                strFieldName = "DisputedFlag";
                break;
            case 28:
                strFieldName = "BankCode";
                break;
            case 29:
                strFieldName = "BankAccNo";
                break;
            case 30:
                strFieldName = "AccName";
                break;
            case 31:
                strFieldName = "BankProivnce";
                break;
            case 32:
                strFieldName = "BankCity";
                break;
            case 33:
                strFieldName = "AccType";
                break;
            case 34:
                strFieldName = "BankLocations";
                break;
            case 35:
                strFieldName = "TaxPayerIDType";
                break;
            case 36:
                strFieldName = "TaxPayerIDNO";
                break;
            case 37:
                strFieldName = "TaxBureauAddr";
                break;
            case 38:
                strFieldName = "TaxBureauTel";
                break;
            case 39:
                strFieldName = "TaxBureauBankCode";
                break;
            case 40:
                strFieldName = "TaxBureauBankAccNo";
                break;
            case 41:
                strFieldName = "BillingType";
                break;
            case 42:
                strFieldName = "RgtMoney";
                break;
            case 43:
                strFieldName = "Asset";
                break;
            case 44:
                strFieldName = "NetProfitRate";
                break;
            case 45:
                strFieldName = "MainBussiness";
                break;
            case 46:
                strFieldName = "Corporation";
                break;
            case 47:
                strFieldName = "ComAera";
                break;
            case 48:
                strFieldName = "Phone";
                break;
            case 49:
                strFieldName = "Satrap";
                break;
            case 50:
                strFieldName = "FoundDate";
                break;
            case 51:
                strFieldName = "GrpGroupNo";
                break;
            case 52:
                strFieldName = "BlacklistFlag";
                break;
            case 53:
                strFieldName = "State";
                break;
            case 54:
                strFieldName = "Remark";
                break;
            case 55:
                strFieldName = "VIPValue";
                break;
            case 56:
                strFieldName = "Operator";
                break;
            case 57:
                strFieldName = "MakeDate";
                break;
            case 58:
                strFieldName = "MakeTime";
                break;
            case 59:
                strFieldName = "ModifyDate";
                break;
            case 60:
                strFieldName = "ModifyTime";
                break;
            case 61:
                strFieldName = "SubCompanyFlag";
                break;
            case 62:
                strFieldName = "SupCustoemrNo";
                break;
            case 63:
                strFieldName = "LevelCode";
                break;
            case 64:
                strFieldName = "OffWorkPeoples";
                break;
            case 65:
                strFieldName = "OtherPeoples";
                break;
            case 66:
                strFieldName = "BusinessBigType";
                break;
            case 67:
                strFieldName = "SocialInsuNo";
                break;
            case 68:
                strFieldName = "BlackListReason";
                break;
            case 69:
                strFieldName = "ComNo";
                break;
            case 70:
                strFieldName = "ComWebsite";
                break;
            case 71:
                strFieldName = "BusRegNum";
                break;
            case 72:
                strFieldName = "CorporationIDNo";
                break;
            case 73:
                strFieldName = "ComCorporationNo";
                break;
            case 74:
                strFieldName = "ContrShar";
                break;
            case 75:
                strFieldName = "HodingIDType";
                break;
            case 76:
                strFieldName = "HodingIDNo";
                break;
            case 77:
                strFieldName = "HodingValidate";
                break;
            case 78:
                strFieldName = "CorporationIDType";
                break;
            case 79:
                strFieldName = "CorporationValidate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "BUSINESSTYPE":
                return Schema.TYPE_STRING;
            case "GRPNATURE":
                return Schema.TYPE_STRING;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "ONWORKPEOPLES":
                return Schema.TYPE_INT;
            case "INSUREPEOPLES":
                return Schema.TYPE_INT;
            case "COMDOCIDTYPE":
                return Schema.TYPE_STRING;
            case "COMDOCIDNO":
                return Schema.TYPE_STRING;
            case "BUSLICEDATE":
                return Schema.TYPE_STRING;
            case "POSTCODE":
                return Schema.TYPE_STRING;
            case "GRPADDRESS":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "UNITREGISTEREDADDRESS":
                return Schema.TYPE_STRING;
            case "UNITDURATION":
                return Schema.TYPE_DOUBLE;
            case "PRINCIPAL":
                return Schema.TYPE_STRING;
            case "PRINCIPALIDTYPE":
                return Schema.TYPE_STRING;
            case "PRINCIPALIDNO":
                return Schema.TYPE_STRING;
            case "PRINCIPALVALIDATE":
                return Schema.TYPE_STRING;
            case "TELEPHONE":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "ANNUITYRECEIVEDATE":
                return Schema.TYPE_STRING;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "HEADSHIP":
                return Schema.TYPE_STRING;
            case "GETFLAG":
                return Schema.TYPE_STRING;
            case "DISPUTEDFLAG":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "BANKPROIVNCE":
                return Schema.TYPE_STRING;
            case "BANKCITY":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "BANKLOCATIONS":
                return Schema.TYPE_STRING;
            case "TAXPAYERIDTYPE":
                return Schema.TYPE_STRING;
            case "TAXPAYERIDNO":
                return Schema.TYPE_STRING;
            case "TAXBUREAUADDR":
                return Schema.TYPE_STRING;
            case "TAXBUREAUTEL":
                return Schema.TYPE_STRING;
            case "TAXBUREAUBANKCODE":
                return Schema.TYPE_STRING;
            case "TAXBUREAUBANKACCNO":
                return Schema.TYPE_STRING;
            case "BILLINGTYPE":
                return Schema.TYPE_STRING;
            case "RGTMONEY":
                return Schema.TYPE_DOUBLE;
            case "ASSET":
                return Schema.TYPE_DOUBLE;
            case "NETPROFITRATE":
                return Schema.TYPE_DOUBLE;
            case "MAINBUSSINESS":
                return Schema.TYPE_STRING;
            case "CORPORATION":
                return Schema.TYPE_STRING;
            case "COMAERA":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "SATRAP":
                return Schema.TYPE_STRING;
            case "FOUNDDATE":
                return Schema.TYPE_STRING;
            case "GRPGROUPNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTFLAG":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "VIPVALUE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_STRING;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_STRING;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "SUBCOMPANYFLAG":
                return Schema.TYPE_STRING;
            case "SUPCUSTOEMRNO":
                return Schema.TYPE_STRING;
            case "LEVELCODE":
                return Schema.TYPE_STRING;
            case "OFFWORKPEOPLES":
                return Schema.TYPE_INT;
            case "OTHERPEOPLES":
                return Schema.TYPE_INT;
            case "BUSINESSBIGTYPE":
                return Schema.TYPE_STRING;
            case "SOCIALINSUNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTREASON":
                return Schema.TYPE_STRING;
            case "COMNO":
                return Schema.TYPE_STRING;
            case "COMWEBSITE":
                return Schema.TYPE_STRING;
            case "BUSREGNUM":
                return Schema.TYPE_STRING;
            case "CORPORATIONIDNO":
                return Schema.TYPE_STRING;
            case "COMCORPORATIONNO":
                return Schema.TYPE_STRING;
            case "CONTRSHAR":
                return Schema.TYPE_STRING;
            case "HODINGIDTYPE":
                return Schema.TYPE_STRING;
            case "HODINGIDNO":
                return Schema.TYPE_STRING;
            case "HODINGVALIDATE":
                return Schema.TYPE_STRING;
            case "CORPORATIONIDTYPE":
                return Schema.TYPE_STRING;
            case "CORPORATIONVALIDATE":
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_INT;
            case 6:
                return Schema.TYPE_INT;
            case 7:
                return Schema.TYPE_INT;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_STRING;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_STRING;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_STRING;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_DOUBLE;
            case 43:
                return Schema.TYPE_DOUBLE;
            case 44:
                return Schema.TYPE_DOUBLE;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_STRING;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_STRING;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_STRING;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_INT;
            case 65:
                return Schema.TYPE_INT;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_STRING;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_STRING;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_STRING;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("BusinessType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
        }
        if (FCode.equalsIgnoreCase("InsurePeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurePeoples));
        }
        if (FCode.equalsIgnoreCase("ComDocIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComDocIDType));
        }
        if (FCode.equalsIgnoreCase("ComDocIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComDocIDNo));
        }
        if (FCode.equalsIgnoreCase("BusliceDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusliceDate));
        }
        if (FCode.equalsIgnoreCase("PostCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostCode));
        }
        if (FCode.equalsIgnoreCase("GrpAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAddress));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("UnitRegisteredAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitRegisteredAddress));
        }
        if (FCode.equalsIgnoreCase("UnitDuration")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitDuration));
        }
        if (FCode.equalsIgnoreCase("Principal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Principal));
        }
        if (FCode.equalsIgnoreCase("PrincipalIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrincipalIDType));
        }
        if (FCode.equalsIgnoreCase("PrincipalIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrincipalIDNo));
        }
        if (FCode.equalsIgnoreCase("PrincipalValidate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrincipalValidate));
        }
        if (FCode.equalsIgnoreCase("Telephone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Telephone));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("AnnuityReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AnnuityReceiveDate));
        }
        if (FCode.equalsIgnoreCase("Managecom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
        }
        if (FCode.equalsIgnoreCase("HeadShip")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadShip));
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetFlag));
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("BankProivnce")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankProivnce));
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCity));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("BankLocations")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankLocations));
        }
        if (FCode.equalsIgnoreCase("TaxPayerIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxPayerIDType));
        }
        if (FCode.equalsIgnoreCase("TaxPayerIDNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxPayerIDNO));
        }
        if (FCode.equalsIgnoreCase("TaxBureauAddr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBureauAddr));
        }
        if (FCode.equalsIgnoreCase("TaxBureauTel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBureauTel));
        }
        if (FCode.equalsIgnoreCase("TaxBureauBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBureauBankCode));
        }
        if (FCode.equalsIgnoreCase("TaxBureauBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBureauBankAccNo));
        }
        if (FCode.equalsIgnoreCase("BillingType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BillingType));
        }
        if (FCode.equalsIgnoreCase("RgtMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtMoney));
        }
        if (FCode.equalsIgnoreCase("Asset")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Asset));
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NetProfitRate));
        }
        if (FCode.equalsIgnoreCase("MainBussiness")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainBussiness));
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Corporation));
        }
        if (FCode.equalsIgnoreCase("ComAera")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComAera));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Satrap));
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FoundDate));
        }
        if (FCode.equalsIgnoreCase("GrpGroupNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpGroupNo));
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistFlag));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIPValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("SubCompanyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubCompanyFlag));
        }
        if (FCode.equalsIgnoreCase("SupCustoemrNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SupCustoemrNo));
        }
        if (FCode.equalsIgnoreCase("LevelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LevelCode));
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
        }
        if (FCode.equalsIgnoreCase("BusinessBigType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessBigType));
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuNo));
        }
        if (FCode.equalsIgnoreCase("BlackListReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListReason));
        }
        if (FCode.equalsIgnoreCase("ComNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComNo));
        }
        if (FCode.equalsIgnoreCase("ComWebsite")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComWebsite));
        }
        if (FCode.equalsIgnoreCase("BusRegNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusRegNum));
        }
        if (FCode.equalsIgnoreCase("CorporationIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorporationIDNo));
        }
        if (FCode.equalsIgnoreCase("ComCorporationNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCorporationNo));
        }
        if (FCode.equalsIgnoreCase("ContrShar")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContrShar));
        }
        if (FCode.equalsIgnoreCase("HodingIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HodingIDType));
        }
        if (FCode.equalsIgnoreCase("HodingIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HodingIDNo));
        }
        if (FCode.equalsIgnoreCase("HodingValidate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HodingValidate));
        }
        if (FCode.equalsIgnoreCase("CorporationIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorporationIDType));
        }
        if (FCode.equalsIgnoreCase("CorporationValidate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorporationValidate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = String.valueOf(CustomerNo);
                break;
            case 1:
                strFieldValue = String.valueOf(Password);
                break;
            case 2:
                strFieldValue = String.valueOf(GrpName);
                break;
            case 3:
                strFieldValue = String.valueOf(BusinessType);
                break;
            case 4:
                strFieldValue = String.valueOf(GrpNature);
                break;
            case 5:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 6:
                strFieldValue = String.valueOf(OnWorkPeoples);
                break;
            case 7:
                strFieldValue = String.valueOf(InsurePeoples);
                break;
            case 8:
                strFieldValue = String.valueOf(ComDocIDType);
                break;
            case 9:
                strFieldValue = String.valueOf(ComDocIDNo);
                break;
            case 10:
                strFieldValue = String.valueOf(BusliceDate);
                break;
            case 11:
                strFieldValue = String.valueOf(PostCode);
                break;
            case 12:
                strFieldValue = String.valueOf(GrpAddress);
                break;
            case 13:
                strFieldValue = String.valueOf(Fax);
                break;
            case 14:
                strFieldValue = String.valueOf(UnitRegisteredAddress);
                break;
            case 15:
                strFieldValue = String.valueOf(UnitDuration);
                break;
            case 16:
                strFieldValue = String.valueOf(Principal);
                break;
            case 17:
                strFieldValue = String.valueOf(PrincipalIDType);
                break;
            case 18:
                strFieldValue = String.valueOf(PrincipalIDNo);
                break;
            case 19:
                strFieldValue = String.valueOf(PrincipalValidate);
                break;
            case 20:
                strFieldValue = String.valueOf(Telephone);
                break;
            case 21:
                strFieldValue = String.valueOf(Mobile);
                break;
            case 22:
                strFieldValue = String.valueOf(EMail);
                break;
            case 23:
                strFieldValue = String.valueOf(AnnuityReceiveDate);
                break;
            case 24:
                strFieldValue = String.valueOf(Managecom);
                break;
            case 25:
                strFieldValue = String.valueOf(HeadShip);
                break;
            case 26:
                strFieldValue = String.valueOf(GetFlag);
                break;
            case 27:
                strFieldValue = String.valueOf(DisputedFlag);
                break;
            case 28:
                strFieldValue = String.valueOf(BankCode);
                break;
            case 29:
                strFieldValue = String.valueOf(BankAccNo);
                break;
            case 30:
                strFieldValue = String.valueOf(AccName);
                break;
            case 31:
                strFieldValue = String.valueOf(BankProivnce);
                break;
            case 32:
                strFieldValue = String.valueOf(BankCity);
                break;
            case 33:
                strFieldValue = String.valueOf(AccType);
                break;
            case 34:
                strFieldValue = String.valueOf(BankLocations);
                break;
            case 35:
                strFieldValue = String.valueOf(TaxPayerIDType);
                break;
            case 36:
                strFieldValue = String.valueOf(TaxPayerIDNO);
                break;
            case 37:
                strFieldValue = String.valueOf(TaxBureauAddr);
                break;
            case 38:
                strFieldValue = String.valueOf(TaxBureauTel);
                break;
            case 39:
                strFieldValue = String.valueOf(TaxBureauBankCode);
                break;
            case 40:
                strFieldValue = String.valueOf(TaxBureauBankAccNo);
                break;
            case 41:
                strFieldValue = String.valueOf(BillingType);
                break;
            case 42:
                strFieldValue = String.valueOf(RgtMoney);
                break;
            case 43:
                strFieldValue = String.valueOf(Asset);
                break;
            case 44:
                strFieldValue = String.valueOf(NetProfitRate);
                break;
            case 45:
                strFieldValue = String.valueOf(MainBussiness);
                break;
            case 46:
                strFieldValue = String.valueOf(Corporation);
                break;
            case 47:
                strFieldValue = String.valueOf(ComAera);
                break;
            case 48:
                strFieldValue = String.valueOf(Phone);
                break;
            case 49:
                strFieldValue = String.valueOf(Satrap);
                break;
            case 50:
                strFieldValue = String.valueOf(FoundDate);
                break;
            case 51:
                strFieldValue = String.valueOf(GrpGroupNo);
                break;
            case 52:
                strFieldValue = String.valueOf(BlacklistFlag);
                break;
            case 53:
                strFieldValue = String.valueOf(State);
                break;
            case 54:
                strFieldValue = String.valueOf(Remark);
                break;
            case 55:
                strFieldValue = String.valueOf(VIPValue);
                break;
            case 56:
                strFieldValue = String.valueOf(Operator);
                break;
            case 57:
                strFieldValue = String.valueOf(MakeDate);
                break;
            case 58:
                strFieldValue = String.valueOf(MakeTime);
                break;
            case 59:
                strFieldValue = String.valueOf(ModifyDate);
                break;
            case 60:
                strFieldValue = String.valueOf(ModifyTime);
                break;
            case 61:
                strFieldValue = String.valueOf(SubCompanyFlag);
                break;
            case 62:
                strFieldValue = String.valueOf(SupCustoemrNo);
                break;
            case 63:
                strFieldValue = String.valueOf(LevelCode);
                break;
            case 64:
                strFieldValue = String.valueOf(OffWorkPeoples);
                break;
            case 65:
                strFieldValue = String.valueOf(OtherPeoples);
                break;
            case 66:
                strFieldValue = String.valueOf(BusinessBigType);
                break;
            case 67:
                strFieldValue = String.valueOf(SocialInsuNo);
                break;
            case 68:
                strFieldValue = String.valueOf(BlackListReason);
                break;
            case 69:
                strFieldValue = String.valueOf(ComNo);
                break;
            case 70:
                strFieldValue = String.valueOf(ComWebsite);
                break;
            case 71:
                strFieldValue = String.valueOf(BusRegNum);
                break;
            case 72:
                strFieldValue = String.valueOf(CorporationIDNo);
                break;
            case 73:
                strFieldValue = String.valueOf(ComCorporationNo);
                break;
            case 74:
                strFieldValue = String.valueOf(ContrShar);
                break;
            case 75:
                strFieldValue = String.valueOf(HodingIDType);
                break;
            case 76:
                strFieldValue = String.valueOf(HodingIDNo);
                break;
            case 77:
                strFieldValue = String.valueOf(HodingValidate);
                break;
            case 78:
                strFieldValue = String.valueOf(CorporationIDType);
                break;
            case 79:
                strFieldValue = String.valueOf(CorporationValidate);
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("BusinessType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusinessType = FValue.trim();
            }
            else
                BusinessType = null;
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNature = FValue.trim();
            }
            else
                GrpNature = null;
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OnWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsurePeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsurePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("ComDocIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComDocIDType = FValue.trim();
            }
            else
                ComDocIDType = null;
        }
        if (FCode.equalsIgnoreCase("ComDocIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComDocIDNo = FValue.trim();
            }
            else
                ComDocIDNo = null;
        }
        if (FCode.equalsIgnoreCase("BusliceDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusliceDate = FValue.trim();
            }
            else
                BusliceDate = null;
        }
        if (FCode.equalsIgnoreCase("PostCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostCode = FValue.trim();
            }
            else
                PostCode = null;
        }
        if (FCode.equalsIgnoreCase("GrpAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAddress = FValue.trim();
            }
            else
                GrpAddress = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("UnitRegisteredAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                UnitRegisteredAddress = FValue.trim();
            }
            else
                UnitRegisteredAddress = null;
        }
        if (FCode.equalsIgnoreCase("UnitDuration")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                UnitDuration = d;
            }
        }
        if (FCode.equalsIgnoreCase("Principal")) {
            if( FValue != null && !FValue.equals(""))
            {
                Principal = FValue.trim();
            }
            else
                Principal = null;
        }
        if (FCode.equalsIgnoreCase("PrincipalIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrincipalIDType = FValue.trim();
            }
            else
                PrincipalIDType = null;
        }
        if (FCode.equalsIgnoreCase("PrincipalIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrincipalIDNo = FValue.trim();
            }
            else
                PrincipalIDNo = null;
        }
        if (FCode.equalsIgnoreCase("PrincipalValidate")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrincipalValidate = FValue.trim();
            }
            else
                PrincipalValidate = null;
        }
        if (FCode.equalsIgnoreCase("Telephone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Telephone = FValue.trim();
            }
            else
                Telephone = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("AnnuityReceiveDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                AnnuityReceiveDate = FValue.trim();
            }
            else
                AnnuityReceiveDate = null;
        }
        if (FCode.equalsIgnoreCase("Managecom")) {
            if( FValue != null && !FValue.equals(""))
            {
                Managecom = FValue.trim();
            }
            else
                Managecom = null;
        }
        if (FCode.equalsIgnoreCase("HeadShip")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadShip = FValue.trim();
            }
            else
                HeadShip = null;
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetFlag = FValue.trim();
            }
            else
                GetFlag = null;
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisputedFlag = FValue.trim();
            }
            else
                DisputedFlag = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("BankProivnce")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankProivnce = FValue.trim();
            }
            else
                BankProivnce = null;
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCity = FValue.trim();
            }
            else
                BankCity = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("BankLocations")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankLocations = FValue.trim();
            }
            else
                BankLocations = null;
        }
        if (FCode.equalsIgnoreCase("TaxPayerIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxPayerIDType = FValue.trim();
            }
            else
                TaxPayerIDType = null;
        }
        if (FCode.equalsIgnoreCase("TaxPayerIDNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxPayerIDNO = FValue.trim();
            }
            else
                TaxPayerIDNO = null;
        }
        if (FCode.equalsIgnoreCase("TaxBureauAddr")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxBureauAddr = FValue.trim();
            }
            else
                TaxBureauAddr = null;
        }
        if (FCode.equalsIgnoreCase("TaxBureauTel")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxBureauTel = FValue.trim();
            }
            else
                TaxBureauTel = null;
        }
        if (FCode.equalsIgnoreCase("TaxBureauBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxBureauBankCode = FValue.trim();
            }
            else
                TaxBureauBankCode = null;
        }
        if (FCode.equalsIgnoreCase("TaxBureauBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxBureauBankAccNo = FValue.trim();
            }
            else
                TaxBureauBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("BillingType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BillingType = FValue.trim();
            }
            else
                BillingType = null;
        }
        if (FCode.equalsIgnoreCase("RgtMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RgtMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("Asset")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Asset = d;
            }
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NetProfitRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("MainBussiness")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainBussiness = FValue.trim();
            }
            else
                MainBussiness = null;
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Corporation = FValue.trim();
            }
            else
                Corporation = null;
        }
        if (FCode.equalsIgnoreCase("ComAera")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComAera = FValue.trim();
            }
            else
                ComAera = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            if( FValue != null && !FValue.equals(""))
            {
                Satrap = FValue.trim();
            }
            else
                Satrap = null;
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                FoundDate = FValue.trim();
            }
            else
                FoundDate = null;
        }
        if (FCode.equalsIgnoreCase("GrpGroupNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpGroupNo = FValue.trim();
            }
            else
                GrpGroupNo = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistFlag = FValue.trim();
            }
            else
                BlacklistFlag = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIPValue = FValue.trim();
            }
            else
                VIPValue = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeDate = FValue.trim();
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyDate = FValue.trim();
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("SubCompanyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubCompanyFlag = FValue.trim();
            }
            else
                SubCompanyFlag = null;
        }
        if (FCode.equalsIgnoreCase("SupCustoemrNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SupCustoemrNo = FValue.trim();
            }
            else
                SupCustoemrNo = null;
        }
        if (FCode.equalsIgnoreCase("LevelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                LevelCode = FValue.trim();
            }
            else
                LevelCode = null;
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OffWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OtherPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("BusinessBigType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusinessBigType = FValue.trim();
            }
            else
                BusinessBigType = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuNo = FValue.trim();
            }
            else
                SocialInsuNo = null;
        }
        if (FCode.equalsIgnoreCase("BlackListReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackListReason = FValue.trim();
            }
            else
                BlackListReason = null;
        }
        if (FCode.equalsIgnoreCase("ComNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComNo = FValue.trim();
            }
            else
                ComNo = null;
        }
        if (FCode.equalsIgnoreCase("ComWebsite")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComWebsite = FValue.trim();
            }
            else
                ComWebsite = null;
        }
        if (FCode.equalsIgnoreCase("BusRegNum")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusRegNum = FValue.trim();
            }
            else
                BusRegNum = null;
        }
        if (FCode.equalsIgnoreCase("CorporationIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorporationIDNo = FValue.trim();
            }
            else
                CorporationIDNo = null;
        }
        if (FCode.equalsIgnoreCase("ComCorporationNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCorporationNo = FValue.trim();
            }
            else
                ComCorporationNo = null;
        }
        if (FCode.equalsIgnoreCase("ContrShar")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContrShar = FValue.trim();
            }
            else
                ContrShar = null;
        }
        if (FCode.equalsIgnoreCase("HodingIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                HodingIDType = FValue.trim();
            }
            else
                HodingIDType = null;
        }
        if (FCode.equalsIgnoreCase("HodingIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                HodingIDNo = FValue.trim();
            }
            else
                HodingIDNo = null;
        }
        if (FCode.equalsIgnoreCase("HodingValidate")) {
            if( FValue != null && !FValue.equals(""))
            {
                HodingValidate = FValue.trim();
            }
            else
                HodingValidate = null;
        }
        if (FCode.equalsIgnoreCase("CorporationIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorporationIDType = FValue.trim();
            }
            else
                CorporationIDType = null;
        }
        if (FCode.equalsIgnoreCase("CorporationValidate")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorporationValidate = FValue.trim();
            }
            else
                CorporationValidate = null;
        }
        return true;
    }


    public String toString() {
    return "LDGrpPojo [" +
            "CustomerNo="+CustomerNo +
            ", Password="+Password +
            ", GrpName="+GrpName +
            ", BusinessType="+BusinessType +
            ", GrpNature="+GrpNature +
            ", Peoples="+Peoples +
            ", OnWorkPeoples="+OnWorkPeoples +
            ", InsurePeoples="+InsurePeoples +
            ", ComDocIDType="+ComDocIDType +
            ", ComDocIDNo="+ComDocIDNo +
            ", BusliceDate="+BusliceDate +
            ", PostCode="+PostCode +
            ", GrpAddress="+GrpAddress +
            ", Fax="+Fax +
            ", UnitRegisteredAddress="+UnitRegisteredAddress +
            ", UnitDuration="+UnitDuration +
            ", Principal="+Principal +
            ", PrincipalIDType="+PrincipalIDType +
            ", PrincipalIDNo="+PrincipalIDNo +
            ", PrincipalValidate="+PrincipalValidate +
            ", Telephone="+Telephone +
            ", Mobile="+Mobile +
            ", EMail="+EMail +
            ", AnnuityReceiveDate="+AnnuityReceiveDate +
            ", Managecom="+Managecom +
            ", HeadShip="+HeadShip +
            ", GetFlag="+GetFlag +
            ", DisputedFlag="+DisputedFlag +
            ", BankCode="+BankCode +
            ", BankAccNo="+BankAccNo +
            ", AccName="+AccName +
            ", BankProivnce="+BankProivnce +
            ", BankCity="+BankCity +
            ", AccType="+AccType +
            ", BankLocations="+BankLocations +
            ", TaxPayerIDType="+TaxPayerIDType +
            ", TaxPayerIDNO="+TaxPayerIDNO +
            ", TaxBureauAddr="+TaxBureauAddr +
            ", TaxBureauTel="+TaxBureauTel +
            ", TaxBureauBankCode="+TaxBureauBankCode +
            ", TaxBureauBankAccNo="+TaxBureauBankAccNo +
            ", BillingType="+BillingType +
            ", RgtMoney="+RgtMoney +
            ", Asset="+Asset +
            ", NetProfitRate="+NetProfitRate +
            ", MainBussiness="+MainBussiness +
            ", Corporation="+Corporation +
            ", ComAera="+ComAera +
            ", Phone="+Phone +
            ", Satrap="+Satrap +
            ", FoundDate="+FoundDate +
            ", GrpGroupNo="+GrpGroupNo +
            ", BlacklistFlag="+BlacklistFlag +
            ", State="+State +
            ", Remark="+Remark +
            ", VIPValue="+VIPValue +
            ", Operator="+Operator +
            ", MakeDate="+MakeDate +
            ", MakeTime="+MakeTime +
            ", ModifyDate="+ModifyDate +
            ", ModifyTime="+ModifyTime +
            ", SubCompanyFlag="+SubCompanyFlag +
            ", SupCustoemrNo="+SupCustoemrNo +
            ", LevelCode="+LevelCode +
            ", OffWorkPeoples="+OffWorkPeoples +
            ", OtherPeoples="+OtherPeoples +
            ", BusinessBigType="+BusinessBigType +
            ", SocialInsuNo="+SocialInsuNo +
            ", BlackListReason="+BlackListReason +
            ", ComNo="+ComNo +
            ", ComWebsite="+ComWebsite +
            ", BusRegNum="+BusRegNum +
            ", CorporationIDNo="+CorporationIDNo +
            ", ComCorporationNo="+ComCorporationNo +
            ", ContrShar="+ContrShar +
            ", HodingIDType="+HodingIDType +
            ", HodingIDNo="+HodingIDNo +
            ", HodingValidate="+HodingValidate +
            ", CorporationIDType="+CorporationIDType +
            ", CorporationValidate="+CorporationValidate +"]";
    }
}
