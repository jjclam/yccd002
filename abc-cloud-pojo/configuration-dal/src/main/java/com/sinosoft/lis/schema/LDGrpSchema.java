/**
 * Copyright (c) 2020 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDGrpDB;

/**
 * <p>ClassName: LDGrpSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2020</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2020-01-14
 */
public class LDGrpSchema implements Schema, Cloneable {
    // @Field
    /** 客户号码 */
    private String CustomerNo;
    /** 密码 */
    private String Password;
    /** 单位名称 */
    private String GrpName;
    /** 行业分类 */
    private String BusinessType;
    /** 单位性质 */
    private String GrpNature;
    /** 总人数 */
    private int Peoples;
    /** 在职人数 */
    private int OnWorkPeoples;
    /** 投保人数 */
    private int InsurePeoples;
    /** 单位证件类型 */
    private String ComDocIDType;
    /** 单位证件号码 */
    private String ComDocIDNo;
    /** 营业执照有效期 */
    private Date BusliceDate;
    /** 邮政编码 */
    private String PostCode;
    /** 单位地址 */
    private String GrpAddress;
    /** 单位传真 */
    private String Fax;
    /** 单位注册地址 */
    private String UnitRegisteredAddress;
    /** 单位存续时间 */
    private double UnitDuration;
    /** 负责人姓名 */
    private String Principal;
    /** 负责人证件类型 */
    private String PrincipalIDType;
    /** 负责人证件号码 */
    private String PrincipalIDNo;
    /** 负责人证件有效期限 */
    private Date PrincipalValidate;
    /** 联系电话 */
    private String Telephone;
    /** 手机号码 */
    private String Mobile;
    /** 公司e_mail */
    private String EMail;
    /** 保单年金领取日 */
    private Date AnnuityReceiveDate;
    /** 所属部门 */
    private String Managecom;
    /** 职务 */
    private String HeadShip;
    /** 付款方式 */
    private String GetFlag;
    /** 争议处理方式 */
    private String DisputedFlag;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 账户名 */
    private String AccName;
    /** 开户行所在省 */
    private String BankProivnce;
    /** 开户行所在市 */
    private String BankCity;
    /** 卡折类型 */
    private String AccType;
    /** 银行网点 */
    private String BankLocations;
    /** 纳税人身份类型 */
    private String TaxPayerIDType;
    /** 纳税人识别号 */
    private String TaxPayerIDNO;
    /** 税局登记地址 */
    private String TaxBureauAddr;
    /** 税局登记电话 */
    private String TaxBureauTel;
    /** 税局开户行 */
    private String TaxBureauBankCode;
    /** 税局登记账号 */
    private String TaxBureauBankAccNo;
    /** 开票类型 */
    private String BillingType;
    /** 注册资本 */
    private double RgtMoney;
    /** 资产总额 */
    private double Asset;
    /** 净资产收益率 */
    private double NetProfitRate;
    /** 主营业务 */
    private String MainBussiness;
    /** 法人 */
    private String Corporation;
    /** 机构分布区域 */
    private String ComAera;
    /** 单位电话 */
    private String Phone;
    /** 负责人 */
    private String Satrap;
    /** 成立日期 */
    private Date FoundDate;
    /** 客户组号码 */
    private String GrpGroupNo;
    /** 黑名单标记 */
    private String BlacklistFlag;
    /** 状态 */
    private String State;
    /** 备注 */
    private String Remark;
    /** Vip值 */
    private String VIPValue;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 子公司标志 */
    private String SubCompanyFlag;
    /** 上级客户号码 */
    private String SupCustoemrNo;
    /** 级别代码 */
    private String LevelCode;
    /** 退休人数 */
    private int OffWorkPeoples;
    /** 其它人员人数 */
    private int OtherPeoples;
    /** 行业大类 */
    private String BusinessBigType;
    /** 单位社保登记号 */
    private String SocialInsuNo;
    /** 黑名单原因 */
    private String BlackListReason;
    /** 组织机构代码 */
    private String ComNo;
    /** 单位网址 */
    private String ComWebsite;
    /** 工商注册号 */
    private String BusRegNum;
    /** 法人代表证件号 */
    private String CorporationIDNo;
    /** 单位法人代码 */
    private String ComCorporationNo;
    /** 实际控制人 */
    private String ContrShar;
    /** 实际控股人证件类型 */
    private String HodingIDType;
    /** 实际控股人证件号码 */
    private String HodingIDNo;
    /** 实际控股人有效期限 */
    private Date HodingValidate;
    /** 法定代表人证件类型 */
    private String CorporationIDType;
    /** 法定代表人有效期限 */
    private Date CorporationValidate;

    public static final int FIELDNUM = 80;    // 数据库表的字段个数

    private static String[] PK;                // 主键

    private FDate fDate = new FDate();        // 处理日期

    public CErrors mErrors;            // 错误信息

    // @Constructor
    public LDGrpSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "CustomerNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDGrpSchema cloned = (LDGrpSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }
    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String aPassword) {
        Password = aPassword;
    }
    public String getGrpName() {
        return GrpName;
    }
    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }
    public String getBusinessType() {
        return BusinessType;
    }
    public void setBusinessType(String aBusinessType) {
        BusinessType = aBusinessType;
    }
    public String getGrpNature() {
        return GrpNature;
    }
    public void setGrpNature(String aGrpNature) {
        GrpNature = aGrpNature;
    }
    public int getPeoples() {
        return Peoples;
    }
    public void setPeoples(int aPeoples) {
        Peoples = aPeoples;
    }
    public void setPeoples(String aPeoples) {
        if (aPeoples != null && !aPeoples.equals("")) {
            Integer tInteger = new Integer(aPeoples);
            int i = tInteger.intValue();
            Peoples = i;
        }
    }

    public int getOnWorkPeoples() {
        return OnWorkPeoples;
    }
    public void setOnWorkPeoples(int aOnWorkPeoples) {
        OnWorkPeoples = aOnWorkPeoples;
    }
    public void setOnWorkPeoples(String aOnWorkPeoples) {
        if (aOnWorkPeoples != null && !aOnWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOnWorkPeoples);
            int i = tInteger.intValue();
            OnWorkPeoples = i;
        }
    }

    public int getInsurePeoples() {
        return InsurePeoples;
    }
    public void setInsurePeoples(int aInsurePeoples) {
        InsurePeoples = aInsurePeoples;
    }
    public void setInsurePeoples(String aInsurePeoples) {
        if (aInsurePeoples != null && !aInsurePeoples.equals("")) {
            Integer tInteger = new Integer(aInsurePeoples);
            int i = tInteger.intValue();
            InsurePeoples = i;
        }
    }

    public String getComDocIDType() {
        return ComDocIDType;
    }
    public void setComDocIDType(String aComDocIDType) {
        ComDocIDType = aComDocIDType;
    }
    public String getComDocIDNo() {
        return ComDocIDNo;
    }
    public void setComDocIDNo(String aComDocIDNo) {
        ComDocIDNo = aComDocIDNo;
    }
    public String getBusliceDate() {
        if(BusliceDate != null) {
            return fDate.getString(BusliceDate);
        } else {
            return null;
        }
    }
    public void setBusliceDate(Date aBusliceDate) {
        BusliceDate = aBusliceDate;
    }
    public void setBusliceDate(String aBusliceDate) {
        if (aBusliceDate != null && !aBusliceDate.equals("")) {
            BusliceDate = fDate.getDate(aBusliceDate);
        } else
            BusliceDate = null;
    }

    public String getPostCode() {
        return PostCode;
    }
    public void setPostCode(String aPostCode) {
        PostCode = aPostCode;
    }
    public String getGrpAddress() {
        return GrpAddress;
    }
    public void setGrpAddress(String aGrpAddress) {
        GrpAddress = aGrpAddress;
    }
    public String getFax() {
        return Fax;
    }
    public void setFax(String aFax) {
        Fax = aFax;
    }
    public String getUnitRegisteredAddress() {
        return UnitRegisteredAddress;
    }
    public void setUnitRegisteredAddress(String aUnitRegisteredAddress) {
        UnitRegisteredAddress = aUnitRegisteredAddress;
    }
    public double getUnitDuration() {
        return UnitDuration;
    }
    public void setUnitDuration(double aUnitDuration) {
        UnitDuration = aUnitDuration;
    }
    public void setUnitDuration(String aUnitDuration) {
        if (aUnitDuration != null && !aUnitDuration.equals("")) {
            Double tDouble = new Double(aUnitDuration);
            double d = tDouble.doubleValue();
            UnitDuration = d;
        }
    }

    public String getPrincipal() {
        return Principal;
    }
    public void setPrincipal(String aPrincipal) {
        Principal = aPrincipal;
    }
    public String getPrincipalIDType() {
        return PrincipalIDType;
    }
    public void setPrincipalIDType(String aPrincipalIDType) {
        PrincipalIDType = aPrincipalIDType;
    }
    public String getPrincipalIDNo() {
        return PrincipalIDNo;
    }
    public void setPrincipalIDNo(String aPrincipalIDNo) {
        PrincipalIDNo = aPrincipalIDNo;
    }
    public String getPrincipalValidate() {
        if(PrincipalValidate != null) {
            return fDate.getString(PrincipalValidate);
        } else {
            return null;
        }
    }
    public void setPrincipalValidate(Date aPrincipalValidate) {
        PrincipalValidate = aPrincipalValidate;
    }
    public void setPrincipalValidate(String aPrincipalValidate) {
        if (aPrincipalValidate != null && !aPrincipalValidate.equals("")) {
            PrincipalValidate = fDate.getDate(aPrincipalValidate);
        } else
            PrincipalValidate = null;
    }

    public String getTelephone() {
        return Telephone;
    }
    public void setTelephone(String aTelephone) {
        Telephone = aTelephone;
    }
    public String getMobile() {
        return Mobile;
    }
    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }
    public String getEMail() {
        return EMail;
    }
    public void setEMail(String aEMail) {
        EMail = aEMail;
    }
    public String getAnnuityReceiveDate() {
        if(AnnuityReceiveDate != null) {
            return fDate.getString(AnnuityReceiveDate);
        } else {
            return null;
        }
    }
    public void setAnnuityReceiveDate(Date aAnnuityReceiveDate) {
        AnnuityReceiveDate = aAnnuityReceiveDate;
    }
    public void setAnnuityReceiveDate(String aAnnuityReceiveDate) {
        if (aAnnuityReceiveDate != null && !aAnnuityReceiveDate.equals("")) {
            AnnuityReceiveDate = fDate.getDate(aAnnuityReceiveDate);
        } else
            AnnuityReceiveDate = null;
    }

    public String getManagecom() {
        return Managecom;
    }
    public void setManagecom(String aManagecom) {
        Managecom = aManagecom;
    }
    public String getHeadShip() {
        return HeadShip;
    }
    public void setHeadShip(String aHeadShip) {
        HeadShip = aHeadShip;
    }
    public String getGetFlag() {
        return GetFlag;
    }
    public void setGetFlag(String aGetFlag) {
        GetFlag = aGetFlag;
    }
    public String getDisputedFlag() {
        return DisputedFlag;
    }
    public void setDisputedFlag(String aDisputedFlag) {
        DisputedFlag = aDisputedFlag;
    }
    public String getBankCode() {
        return BankCode;
    }
    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }
    public String getBankAccNo() {
        return BankAccNo;
    }
    public void setBankAccNo(String aBankAccNo) {
        BankAccNo = aBankAccNo;
    }
    public String getAccName() {
        return AccName;
    }
    public void setAccName(String aAccName) {
        AccName = aAccName;
    }
    public String getBankProivnce() {
        return BankProivnce;
    }
    public void setBankProivnce(String aBankProivnce) {
        BankProivnce = aBankProivnce;
    }
    public String getBankCity() {
        return BankCity;
    }
    public void setBankCity(String aBankCity) {
        BankCity = aBankCity;
    }
    public String getAccType() {
        return AccType;
    }
    public void setAccType(String aAccType) {
        AccType = aAccType;
    }
    public String getBankLocations() {
        return BankLocations;
    }
    public void setBankLocations(String aBankLocations) {
        BankLocations = aBankLocations;
    }
    public String getTaxPayerIDType() {
        return TaxPayerIDType;
    }
    public void setTaxPayerIDType(String aTaxPayerIDType) {
        TaxPayerIDType = aTaxPayerIDType;
    }
    public String getTaxPayerIDNO() {
        return TaxPayerIDNO;
    }
    public void setTaxPayerIDNO(String aTaxPayerIDNO) {
        TaxPayerIDNO = aTaxPayerIDNO;
    }
    public String getTaxBureauAddr() {
        return TaxBureauAddr;
    }
    public void setTaxBureauAddr(String aTaxBureauAddr) {
        TaxBureauAddr = aTaxBureauAddr;
    }
    public String getTaxBureauTel() {
        return TaxBureauTel;
    }
    public void setTaxBureauTel(String aTaxBureauTel) {
        TaxBureauTel = aTaxBureauTel;
    }
    public String getTaxBureauBankCode() {
        return TaxBureauBankCode;
    }
    public void setTaxBureauBankCode(String aTaxBureauBankCode) {
        TaxBureauBankCode = aTaxBureauBankCode;
    }
    public String getTaxBureauBankAccNo() {
        return TaxBureauBankAccNo;
    }
    public void setTaxBureauBankAccNo(String aTaxBureauBankAccNo) {
        TaxBureauBankAccNo = aTaxBureauBankAccNo;
    }
    public String getBillingType() {
        return BillingType;
    }
    public void setBillingType(String aBillingType) {
        BillingType = aBillingType;
    }
    public double getRgtMoney() {
        return RgtMoney;
    }
    public void setRgtMoney(double aRgtMoney) {
        RgtMoney = aRgtMoney;
    }
    public void setRgtMoney(String aRgtMoney) {
        if (aRgtMoney != null && !aRgtMoney.equals("")) {
            Double tDouble = new Double(aRgtMoney);
            double d = tDouble.doubleValue();
            RgtMoney = d;
        }
    }

    public double getAsset() {
        return Asset;
    }
    public void setAsset(double aAsset) {
        Asset = aAsset;
    }
    public void setAsset(String aAsset) {
        if (aAsset != null && !aAsset.equals("")) {
            Double tDouble = new Double(aAsset);
            double d = tDouble.doubleValue();
            Asset = d;
        }
    }

    public double getNetProfitRate() {
        return NetProfitRate;
    }
    public void setNetProfitRate(double aNetProfitRate) {
        NetProfitRate = aNetProfitRate;
    }
    public void setNetProfitRate(String aNetProfitRate) {
        if (aNetProfitRate != null && !aNetProfitRate.equals("")) {
            Double tDouble = new Double(aNetProfitRate);
            double d = tDouble.doubleValue();
            NetProfitRate = d;
        }
    }

    public String getMainBussiness() {
        return MainBussiness;
    }
    public void setMainBussiness(String aMainBussiness) {
        MainBussiness = aMainBussiness;
    }
    public String getCorporation() {
        return Corporation;
    }
    public void setCorporation(String aCorporation) {
        Corporation = aCorporation;
    }
    public String getComAera() {
        return ComAera;
    }
    public void setComAera(String aComAera) {
        ComAera = aComAera;
    }
    public String getPhone() {
        return Phone;
    }
    public void setPhone(String aPhone) {
        Phone = aPhone;
    }
    public String getSatrap() {
        return Satrap;
    }
    public void setSatrap(String aSatrap) {
        Satrap = aSatrap;
    }
    public String getFoundDate() {
        if(FoundDate != null) {
            return fDate.getString(FoundDate);
        } else {
            return null;
        }
    }
    public void setFoundDate(Date aFoundDate) {
        FoundDate = aFoundDate;
    }
    public void setFoundDate(String aFoundDate) {
        if (aFoundDate != null && !aFoundDate.equals("")) {
            FoundDate = fDate.getDate(aFoundDate);
        } else
            FoundDate = null;
    }

    public String getGrpGroupNo() {
        return GrpGroupNo;
    }
    public void setGrpGroupNo(String aGrpGroupNo) {
        GrpGroupNo = aGrpGroupNo;
    }
    public String getBlacklistFlag() {
        return BlacklistFlag;
    }
    public void setBlacklistFlag(String aBlacklistFlag) {
        BlacklistFlag = aBlacklistFlag;
    }
    public String getState() {
        return State;
    }
    public void setState(String aState) {
        State = aState;
    }
    public String getRemark() {
        return Remark;
    }
    public void setRemark(String aRemark) {
        Remark = aRemark;
    }
    public String getVIPValue() {
        return VIPValue;
    }
    public void setVIPValue(String aVIPValue) {
        VIPValue = aVIPValue;
    }
    public String getOperator() {
        return Operator;
    }
    public void setOperator(String aOperator) {
        Operator = aOperator;
    }
    public String getMakeDate() {
        if(MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }
    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }
    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else
            MakeDate = null;
    }

    public String getMakeTime() {
        return MakeTime;
    }
    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }
    public String getModifyDate() {
        if(ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }
    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }
    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else
            ModifyDate = null;
    }

    public String getModifyTime() {
        return ModifyTime;
    }
    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }
    public String getSubCompanyFlag() {
        return SubCompanyFlag;
    }
    public void setSubCompanyFlag(String aSubCompanyFlag) {
        SubCompanyFlag = aSubCompanyFlag;
    }
    public String getSupCustoemrNo() {
        return SupCustoemrNo;
    }
    public void setSupCustoemrNo(String aSupCustoemrNo) {
        SupCustoemrNo = aSupCustoemrNo;
    }
    public String getLevelCode() {
        return LevelCode;
    }
    public void setLevelCode(String aLevelCode) {
        LevelCode = aLevelCode;
    }
    public int getOffWorkPeoples() {
        return OffWorkPeoples;
    }
    public void setOffWorkPeoples(int aOffWorkPeoples) {
        OffWorkPeoples = aOffWorkPeoples;
    }
    public void setOffWorkPeoples(String aOffWorkPeoples) {
        if (aOffWorkPeoples != null && !aOffWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOffWorkPeoples);
            int i = tInteger.intValue();
            OffWorkPeoples = i;
        }
    }

    public int getOtherPeoples() {
        return OtherPeoples;
    }
    public void setOtherPeoples(int aOtherPeoples) {
        OtherPeoples = aOtherPeoples;
    }
    public void setOtherPeoples(String aOtherPeoples) {
        if (aOtherPeoples != null && !aOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aOtherPeoples);
            int i = tInteger.intValue();
            OtherPeoples = i;
        }
    }

    public String getBusinessBigType() {
        return BusinessBigType;
    }
    public void setBusinessBigType(String aBusinessBigType) {
        BusinessBigType = aBusinessBigType;
    }
    public String getSocialInsuNo() {
        return SocialInsuNo;
    }
    public void setSocialInsuNo(String aSocialInsuNo) {
        SocialInsuNo = aSocialInsuNo;
    }
    public String getBlackListReason() {
        return BlackListReason;
    }
    public void setBlackListReason(String aBlackListReason) {
        BlackListReason = aBlackListReason;
    }
    public String getComNo() {
        return ComNo;
    }
    public void setComNo(String aComNo) {
        ComNo = aComNo;
    }
    public String getComWebsite() {
        return ComWebsite;
    }
    public void setComWebsite(String aComWebsite) {
        ComWebsite = aComWebsite;
    }
    public String getBusRegNum() {
        return BusRegNum;
    }
    public void setBusRegNum(String aBusRegNum) {
        BusRegNum = aBusRegNum;
    }
    public String getCorporationIDNo() {
        return CorporationIDNo;
    }
    public void setCorporationIDNo(String aCorporationIDNo) {
        CorporationIDNo = aCorporationIDNo;
    }
    public String getComCorporationNo() {
        return ComCorporationNo;
    }
    public void setComCorporationNo(String aComCorporationNo) {
        ComCorporationNo = aComCorporationNo;
    }
    public String getContrShar() {
        return ContrShar;
    }
    public void setContrShar(String aContrShar) {
        ContrShar = aContrShar;
    }
    public String getHodingIDType() {
        return HodingIDType;
    }
    public void setHodingIDType(String aHodingIDType) {
        HodingIDType = aHodingIDType;
    }
    public String getHodingIDNo() {
        return HodingIDNo;
    }
    public void setHodingIDNo(String aHodingIDNo) {
        HodingIDNo = aHodingIDNo;
    }
    public String getHodingValidate() {
        if(HodingValidate != null) {
            return fDate.getString(HodingValidate);
        } else {
            return null;
        }
    }
    public void setHodingValidate(Date aHodingValidate) {
        HodingValidate = aHodingValidate;
    }
    public void setHodingValidate(String aHodingValidate) {
        if (aHodingValidate != null && !aHodingValidate.equals("")) {
            HodingValidate = fDate.getDate(aHodingValidate);
        } else
            HodingValidate = null;
    }

    public String getCorporationIDType() {
        return CorporationIDType;
    }
    public void setCorporationIDType(String aCorporationIDType) {
        CorporationIDType = aCorporationIDType;
    }
    public String getCorporationValidate() {
        if(CorporationValidate != null) {
            return fDate.getString(CorporationValidate);
        } else {
            return null;
        }
    }
    public void setCorporationValidate(Date aCorporationValidate) {
        CorporationValidate = aCorporationValidate;
    }
    public void setCorporationValidate(String aCorporationValidate) {
        if (aCorporationValidate != null && !aCorporationValidate.equals("")) {
            CorporationValidate = fDate.getDate(aCorporationValidate);
        } else
            CorporationValidate = null;
    }


    /**
    * 使用另外一个 LDGrpSchema 对象给 Schema 赋值
    * @param: aLDGrpSchema LDGrpSchema
    **/
    public void setSchema(LDGrpSchema aLDGrpSchema) {
        this.CustomerNo = aLDGrpSchema.getCustomerNo();
        this.Password = aLDGrpSchema.getPassword();
        this.GrpName = aLDGrpSchema.getGrpName();
        this.BusinessType = aLDGrpSchema.getBusinessType();
        this.GrpNature = aLDGrpSchema.getGrpNature();
        this.Peoples = aLDGrpSchema.getPeoples();
        this.OnWorkPeoples = aLDGrpSchema.getOnWorkPeoples();
        this.InsurePeoples = aLDGrpSchema.getInsurePeoples();
        this.ComDocIDType = aLDGrpSchema.getComDocIDType();
        this.ComDocIDNo = aLDGrpSchema.getComDocIDNo();
        this.BusliceDate = fDate.getDate( aLDGrpSchema.getBusliceDate());
        this.PostCode = aLDGrpSchema.getPostCode();
        this.GrpAddress = aLDGrpSchema.getGrpAddress();
        this.Fax = aLDGrpSchema.getFax();
        this.UnitRegisteredAddress = aLDGrpSchema.getUnitRegisteredAddress();
        this.UnitDuration = aLDGrpSchema.getUnitDuration();
        this.Principal = aLDGrpSchema.getPrincipal();
        this.PrincipalIDType = aLDGrpSchema.getPrincipalIDType();
        this.PrincipalIDNo = aLDGrpSchema.getPrincipalIDNo();
        this.PrincipalValidate = fDate.getDate( aLDGrpSchema.getPrincipalValidate());
        this.Telephone = aLDGrpSchema.getTelephone();
        this.Mobile = aLDGrpSchema.getMobile();
        this.EMail = aLDGrpSchema.getEMail();
        this.AnnuityReceiveDate = fDate.getDate( aLDGrpSchema.getAnnuityReceiveDate());
        this.Managecom = aLDGrpSchema.getManagecom();
        this.HeadShip = aLDGrpSchema.getHeadShip();
        this.GetFlag = aLDGrpSchema.getGetFlag();
        this.DisputedFlag = aLDGrpSchema.getDisputedFlag();
        this.BankCode = aLDGrpSchema.getBankCode();
        this.BankAccNo = aLDGrpSchema.getBankAccNo();
        this.AccName = aLDGrpSchema.getAccName();
        this.BankProivnce = aLDGrpSchema.getBankProivnce();
        this.BankCity = aLDGrpSchema.getBankCity();
        this.AccType = aLDGrpSchema.getAccType();
        this.BankLocations = aLDGrpSchema.getBankLocations();
        this.TaxPayerIDType = aLDGrpSchema.getTaxPayerIDType();
        this.TaxPayerIDNO = aLDGrpSchema.getTaxPayerIDNO();
        this.TaxBureauAddr = aLDGrpSchema.getTaxBureauAddr();
        this.TaxBureauTel = aLDGrpSchema.getTaxBureauTel();
        this.TaxBureauBankCode = aLDGrpSchema.getTaxBureauBankCode();
        this.TaxBureauBankAccNo = aLDGrpSchema.getTaxBureauBankAccNo();
        this.BillingType = aLDGrpSchema.getBillingType();
        this.RgtMoney = aLDGrpSchema.getRgtMoney();
        this.Asset = aLDGrpSchema.getAsset();
        this.NetProfitRate = aLDGrpSchema.getNetProfitRate();
        this.MainBussiness = aLDGrpSchema.getMainBussiness();
        this.Corporation = aLDGrpSchema.getCorporation();
        this.ComAera = aLDGrpSchema.getComAera();
        this.Phone = aLDGrpSchema.getPhone();
        this.Satrap = aLDGrpSchema.getSatrap();
        this.FoundDate = fDate.getDate( aLDGrpSchema.getFoundDate());
        this.GrpGroupNo = aLDGrpSchema.getGrpGroupNo();
        this.BlacklistFlag = aLDGrpSchema.getBlacklistFlag();
        this.State = aLDGrpSchema.getState();
        this.Remark = aLDGrpSchema.getRemark();
        this.VIPValue = aLDGrpSchema.getVIPValue();
        this.Operator = aLDGrpSchema.getOperator();
        this.MakeDate = fDate.getDate( aLDGrpSchema.getMakeDate());
        this.MakeTime = aLDGrpSchema.getMakeTime();
        this.ModifyDate = fDate.getDate( aLDGrpSchema.getModifyDate());
        this.ModifyTime = aLDGrpSchema.getModifyTime();
        this.SubCompanyFlag = aLDGrpSchema.getSubCompanyFlag();
        this.SupCustoemrNo = aLDGrpSchema.getSupCustoemrNo();
        this.LevelCode = aLDGrpSchema.getLevelCode();
        this.OffWorkPeoples = aLDGrpSchema.getOffWorkPeoples();
        this.OtherPeoples = aLDGrpSchema.getOtherPeoples();
        this.BusinessBigType = aLDGrpSchema.getBusinessBigType();
        this.SocialInsuNo = aLDGrpSchema.getSocialInsuNo();
        this.BlackListReason = aLDGrpSchema.getBlackListReason();
        this.ComNo = aLDGrpSchema.getComNo();
        this.ComWebsite = aLDGrpSchema.getComWebsite();
        this.BusRegNum = aLDGrpSchema.getBusRegNum();
        this.CorporationIDNo = aLDGrpSchema.getCorporationIDNo();
        this.ComCorporationNo = aLDGrpSchema.getComCorporationNo();
        this.ContrShar = aLDGrpSchema.getContrShar();
        this.HodingIDType = aLDGrpSchema.getHodingIDType();
        this.HodingIDNo = aLDGrpSchema.getHodingIDNo();
        this.HodingValidate = fDate.getDate( aLDGrpSchema.getHodingValidate());
        this.CorporationIDType = aLDGrpSchema.getCorporationIDType();
        this.CorporationValidate = fDate.getDate( aLDGrpSchema.getCorporationValidate());
    }

    /**
    * 使用 ResultSet 中的第 i 行给 Schema 赋值
    * @param: rs ResultSet
    * @param: i int
    * @return: boolean
    **/
    public boolean setSchema(ResultSet rs,int i) {
        try {
            //rs.absolute(i);        // 非滚动游标
            if( rs.getString("CustomerNo") == null )
                this.CustomerNo = null;
            else
                this.CustomerNo = rs.getString("CustomerNo").trim();

            if( rs.getString("Password") == null )
                this.Password = null;
            else
                this.Password = rs.getString("Password").trim();

            if( rs.getString("GrpName") == null )
                this.GrpName = null;
            else
                this.GrpName = rs.getString("GrpName").trim();

            if( rs.getString("BusinessType") == null )
                this.BusinessType = null;
            else
                this.BusinessType = rs.getString("BusinessType").trim();

            if( rs.getString("GrpNature") == null )
                this.GrpNature = null;
            else
                this.GrpNature = rs.getString("GrpNature").trim();

            this.Peoples = rs.getInt("Peoples");
            this.OnWorkPeoples = rs.getInt("OnWorkPeoples");
            this.InsurePeoples = rs.getInt("InsurePeoples");
            if( rs.getString("ComDocIDType") == null )
                this.ComDocIDType = null;
            else
                this.ComDocIDType = rs.getString("ComDocIDType").trim();

            if( rs.getString("ComDocIDNo") == null )
                this.ComDocIDNo = null;
            else
                this.ComDocIDNo = rs.getString("ComDocIDNo").trim();

            this.BusliceDate = rs.getDate("BusliceDate");
            if( rs.getString("PostCode") == null )
                this.PostCode = null;
            else
                this.PostCode = rs.getString("PostCode").trim();

            if( rs.getString("GrpAddress") == null )
                this.GrpAddress = null;
            else
                this.GrpAddress = rs.getString("GrpAddress").trim();

            if( rs.getString("Fax") == null )
                this.Fax = null;
            else
                this.Fax = rs.getString("Fax").trim();

            if( rs.getString("UnitRegisteredAddress") == null )
                this.UnitRegisteredAddress = null;
            else
                this.UnitRegisteredAddress = rs.getString("UnitRegisteredAddress").trim();

            this.UnitDuration = rs.getDouble("UnitDuration");
            if( rs.getString("Principal") == null )
                this.Principal = null;
            else
                this.Principal = rs.getString("Principal").trim();

            if( rs.getString("PrincipalIDType") == null )
                this.PrincipalIDType = null;
            else
                this.PrincipalIDType = rs.getString("PrincipalIDType").trim();

            if( rs.getString("PrincipalIDNo") == null )
                this.PrincipalIDNo = null;
            else
                this.PrincipalIDNo = rs.getString("PrincipalIDNo").trim();

            this.PrincipalValidate = rs.getDate("PrincipalValidate");
            if( rs.getString("Telephone") == null )
                this.Telephone = null;
            else
                this.Telephone = rs.getString("Telephone").trim();

            if( rs.getString("Mobile") == null )
                this.Mobile = null;
            else
                this.Mobile = rs.getString("Mobile").trim();

            if( rs.getString("EMail") == null )
                this.EMail = null;
            else
                this.EMail = rs.getString("EMail").trim();

            this.AnnuityReceiveDate = rs.getDate("AnnuityReceiveDate");
            if( rs.getString("Managecom") == null )
                this.Managecom = null;
            else
                this.Managecom = rs.getString("Managecom").trim();

            if( rs.getString("HeadShip") == null )
                this.HeadShip = null;
            else
                this.HeadShip = rs.getString("HeadShip").trim();

            if( rs.getString("GetFlag") == null )
                this.GetFlag = null;
            else
                this.GetFlag = rs.getString("GetFlag").trim();

            if( rs.getString("DisputedFlag") == null )
                this.DisputedFlag = null;
            else
                this.DisputedFlag = rs.getString("DisputedFlag").trim();

            if( rs.getString("BankCode") == null )
                this.BankCode = null;
            else
                this.BankCode = rs.getString("BankCode").trim();

            if( rs.getString("BankAccNo") == null )
                this.BankAccNo = null;
            else
                this.BankAccNo = rs.getString("BankAccNo").trim();

            if( rs.getString("AccName") == null )
                this.AccName = null;
            else
                this.AccName = rs.getString("AccName").trim();

            if( rs.getString("BankProivnce") == null )
                this.BankProivnce = null;
            else
                this.BankProivnce = rs.getString("BankProivnce").trim();

            if( rs.getString("BankCity") == null )
                this.BankCity = null;
            else
                this.BankCity = rs.getString("BankCity").trim();

            if( rs.getString("AccType") == null )
                this.AccType = null;
            else
                this.AccType = rs.getString("AccType").trim();

            if( rs.getString("BankLocations") == null )
                this.BankLocations = null;
            else
                this.BankLocations = rs.getString("BankLocations").trim();

            if( rs.getString("TaxPayerIDType") == null )
                this.TaxPayerIDType = null;
            else
                this.TaxPayerIDType = rs.getString("TaxPayerIDType").trim();

            if( rs.getString("TaxPayerIDNO") == null )
                this.TaxPayerIDNO = null;
            else
                this.TaxPayerIDNO = rs.getString("TaxPayerIDNO").trim();

            if( rs.getString("TaxBureauAddr") == null )
                this.TaxBureauAddr = null;
            else
                this.TaxBureauAddr = rs.getString("TaxBureauAddr").trim();

            if( rs.getString("TaxBureauTel") == null )
                this.TaxBureauTel = null;
            else
                this.TaxBureauTel = rs.getString("TaxBureauTel").trim();

            if( rs.getString("TaxBureauBankCode") == null )
                this.TaxBureauBankCode = null;
            else
                this.TaxBureauBankCode = rs.getString("TaxBureauBankCode").trim();

            if( rs.getString("TaxBureauBankAccNo") == null )
                this.TaxBureauBankAccNo = null;
            else
                this.TaxBureauBankAccNo = rs.getString("TaxBureauBankAccNo").trim();

            if( rs.getString("BillingType") == null )
                this.BillingType = null;
            else
                this.BillingType = rs.getString("BillingType").trim();

            this.RgtMoney = rs.getDouble("RgtMoney");
            this.Asset = rs.getDouble("Asset");
            this.NetProfitRate = rs.getDouble("NetProfitRate");
            if( rs.getString("MainBussiness") == null )
                this.MainBussiness = null;
            else
                this.MainBussiness = rs.getString("MainBussiness").trim();

            if( rs.getString("Corporation") == null )
                this.Corporation = null;
            else
                this.Corporation = rs.getString("Corporation").trim();

            if( rs.getString("ComAera") == null )
                this.ComAera = null;
            else
                this.ComAera = rs.getString("ComAera").trim();

            if( rs.getString("Phone") == null )
                this.Phone = null;
            else
                this.Phone = rs.getString("Phone").trim();

            if( rs.getString("Satrap") == null )
                this.Satrap = null;
            else
                this.Satrap = rs.getString("Satrap").trim();

            this.FoundDate = rs.getDate("FoundDate");
            if( rs.getString("GrpGroupNo") == null )
                this.GrpGroupNo = null;
            else
                this.GrpGroupNo = rs.getString("GrpGroupNo").trim();

            if( rs.getString("BlacklistFlag") == null )
                this.BlacklistFlag = null;
            else
                this.BlacklistFlag = rs.getString("BlacklistFlag").trim();

            if( rs.getString("State") == null )
                this.State = null;
            else
                this.State = rs.getString("State").trim();

            if( rs.getString("Remark") == null )
                this.Remark = null;
            else
                this.Remark = rs.getString("Remark").trim();

            if( rs.getString("VIPValue") == null )
                this.VIPValue = null;
            else
                this.VIPValue = rs.getString("VIPValue").trim();

            if( rs.getString("Operator") == null )
                this.Operator = null;
            else
                this.Operator = rs.getString("Operator").trim();

            this.MakeDate = rs.getDate("MakeDate");
            if( rs.getString("MakeTime") == null )
                this.MakeTime = null;
            else
                this.MakeTime = rs.getString("MakeTime").trim();

            this.ModifyDate = rs.getDate("ModifyDate");
            if( rs.getString("ModifyTime") == null )
                this.ModifyTime = null;
            else
                this.ModifyTime = rs.getString("ModifyTime").trim();

            if( rs.getString("SubCompanyFlag") == null )
                this.SubCompanyFlag = null;
            else
                this.SubCompanyFlag = rs.getString("SubCompanyFlag").trim();

            if( rs.getString("SupCustoemrNo") == null )
                this.SupCustoemrNo = null;
            else
                this.SupCustoemrNo = rs.getString("SupCustoemrNo").trim();

            if( rs.getString("LevelCode") == null )
                this.LevelCode = null;
            else
                this.LevelCode = rs.getString("LevelCode").trim();

            this.OffWorkPeoples = rs.getInt("OffWorkPeoples");
            this.OtherPeoples = rs.getInt("OtherPeoples");
            if( rs.getString("BusinessBigType") == null )
                this.BusinessBigType = null;
            else
                this.BusinessBigType = rs.getString("BusinessBigType").trim();

            if( rs.getString("SocialInsuNo") == null )
                this.SocialInsuNo = null;
            else
                this.SocialInsuNo = rs.getString("SocialInsuNo").trim();

            if( rs.getString("BlackListReason") == null )
                this.BlackListReason = null;
            else
                this.BlackListReason = rs.getString("BlackListReason").trim();

            if( rs.getString("ComNo") == null )
                this.ComNo = null;
            else
                this.ComNo = rs.getString("ComNo").trim();

            if( rs.getString("ComWebsite") == null )
                this.ComWebsite = null;
            else
                this.ComWebsite = rs.getString("ComWebsite").trim();

            if( rs.getString("BusRegNum") == null )
                this.BusRegNum = null;
            else
                this.BusRegNum = rs.getString("BusRegNum").trim();

            if( rs.getString("CorporationIDNo") == null )
                this.CorporationIDNo = null;
            else
                this.CorporationIDNo = rs.getString("CorporationIDNo").trim();

            if( rs.getString("ComCorporationNo") == null )
                this.ComCorporationNo = null;
            else
                this.ComCorporationNo = rs.getString("ComCorporationNo").trim();

            if( rs.getString("ContrShar") == null )
                this.ContrShar = null;
            else
                this.ContrShar = rs.getString("ContrShar").trim();

            if( rs.getString("HodingIDType") == null )
                this.HodingIDType = null;
            else
                this.HodingIDType = rs.getString("HodingIDType").trim();

            if( rs.getString("HodingIDNo") == null )
                this.HodingIDNo = null;
            else
                this.HodingIDNo = rs.getString("HodingIDNo").trim();

            this.HodingValidate = rs.getDate("HodingValidate");
            if( rs.getString("CorporationIDType") == null )
                this.CorporationIDType = null;
            else
                this.CorporationIDType = rs.getString("CorporationIDType").trim();

            this.CorporationValidate = rs.getDate("CorporationValidate");
        }
        catch(SQLException sqle) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = ExceptionUtils.exceptionToString(sqle);
            this.mErrors .addOneError(tError);
            return false;
        }
        return true;
    }

    public LDGrpSchema getSchema() {
        LDGrpSchema aLDGrpSchema = new LDGrpSchema();
        aLDGrpSchema.setSchema(this);
        return aLDGrpSchema;
    }

    public LDGrpDB getDB() {
        LDGrpDB aDBOper = new LDGrpDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
    * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDGrp描述/A>表字段
    * @return: String 返回打包后字符串
    **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BusinessType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpNature)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OnWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(InsurePeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComDocIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComDocIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( BusliceDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PostCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UnitRegisteredAddress)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(UnitDuration));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Principal)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrincipalIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrincipalIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( PrincipalValidate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Telephone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( AnnuityReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HeadShip)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DisputedFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankProivnce)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCity)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankLocations)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxPayerIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxPayerIDNO)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxBureauAddr)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxBureauTel)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxBureauBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaxBureauBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BillingType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RgtMoney));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Asset));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(NetProfitRate));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainBussiness)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Corporation)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComAera)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Satrap)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( FoundDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpGroupNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlacklistFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(VIPValue)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubCompanyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SupCustoemrNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LevelCode)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OffWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OtherPeoples));strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BusinessBigType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SocialInsuNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BlackListReason)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComWebsite)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BusRegNum)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CorporationIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCorporationNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContrShar)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HodingIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HodingIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( HodingValidate ))); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CorporationIDType)); strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString( CorporationValidate )));
        return strReturn.toString();
    }

    /**
    * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDGrp>历史记账凭证主表信息</A>表字段
    * @param: strMessage String 包含一条纪录数据的字符串
    * @return: boolean
    **/
    public boolean decode(String strMessage) {
        try {
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
            BusinessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
            GrpNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
            Peoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
            OnWorkPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
            InsurePeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).intValue();
            ComDocIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
            ComDocIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
            BusliceDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
            PostCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
            GrpAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
            UnitRegisteredAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
            UnitDuration = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
            Principal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
            PrincipalIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
            PrincipalIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
            PrincipalValidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
            Telephone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
            Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
            AnnuityReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
            Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
            HeadShip = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
            GetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
            DisputedFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
            BankProivnce = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
            BankCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
            BankLocations = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
            TaxPayerIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
            TaxPayerIDNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
            TaxBureauAddr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
            TaxBureauTel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
            TaxBureauBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
            TaxBureauBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
            BillingType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
            RgtMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,43,SysConst.PACKAGESPILTER))).doubleValue();
            Asset = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,44,SysConst.PACKAGESPILTER))).doubleValue();
            NetProfitRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).doubleValue();
            MainBussiness = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
            Corporation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
            ComAera = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
            Satrap = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
            FoundDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51,SysConst.PACKAGESPILTER));
            GrpGroupNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
            BlacklistFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
            VIPValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58,SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60,SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
            SubCompanyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
            SupCustoemrNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
            LevelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
            OffWorkPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,65,SysConst.PACKAGESPILTER))).intValue();
            OtherPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,66,SysConst.PACKAGESPILTER))).intValue();
            BusinessBigType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
            SocialInsuNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
            BlackListReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
            ComNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
            ComWebsite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
            BusRegNum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
            CorporationIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
            ComCorporationNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74, SysConst.PACKAGESPILTER );
            ContrShar = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
            HodingIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER );
            HodingIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
            HodingValidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78,SysConst.PACKAGESPILTER));
            CorporationIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79, SysConst.PACKAGESPILTER );
            CorporationValidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80,SysConst.PACKAGESPILTER));
        }
        catch(NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpSchema";
            tError.functionName = "decode";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            return false;
        }
        return true;
    }

    /**
    * 取得对应传入参数的String形式的字段值
    * @param: FCode String 希望取得的字段名
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equalsIgnoreCase("Password")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equalsIgnoreCase("BusinessType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
        }
        if (FCode.equalsIgnoreCase("InsurePeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsurePeoples));
        }
        if (FCode.equalsIgnoreCase("ComDocIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComDocIDType));
        }
        if (FCode.equalsIgnoreCase("ComDocIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComDocIDNo));
        }
        if (FCode.equalsIgnoreCase("BusliceDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBusliceDate()));
        }
        if (FCode.equalsIgnoreCase("PostCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostCode));
        }
        if (FCode.equalsIgnoreCase("GrpAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAddress));
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equalsIgnoreCase("UnitRegisteredAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitRegisteredAddress));
        }
        if (FCode.equalsIgnoreCase("UnitDuration")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UnitDuration));
        }
        if (FCode.equalsIgnoreCase("Principal")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Principal));
        }
        if (FCode.equalsIgnoreCase("PrincipalIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrincipalIDType));
        }
        if (FCode.equalsIgnoreCase("PrincipalIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrincipalIDNo));
        }
        if (FCode.equalsIgnoreCase("PrincipalValidate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPrincipalValidate()));
        }
        if (FCode.equalsIgnoreCase("Telephone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Telephone));
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
        }
        if (FCode.equalsIgnoreCase("AnnuityReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAnnuityReceiveDate()));
        }
        if (FCode.equalsIgnoreCase("Managecom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
        }
        if (FCode.equalsIgnoreCase("HeadShip")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HeadShip));
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetFlag));
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equalsIgnoreCase("BankProivnce")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankProivnce));
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCity));
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
        }
        if (FCode.equalsIgnoreCase("BankLocations")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankLocations));
        }
        if (FCode.equalsIgnoreCase("TaxPayerIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxPayerIDType));
        }
        if (FCode.equalsIgnoreCase("TaxPayerIDNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxPayerIDNO));
        }
        if (FCode.equalsIgnoreCase("TaxBureauAddr")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBureauAddr));
        }
        if (FCode.equalsIgnoreCase("TaxBureauTel")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBureauTel));
        }
        if (FCode.equalsIgnoreCase("TaxBureauBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBureauBankCode));
        }
        if (FCode.equalsIgnoreCase("TaxBureauBankAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxBureauBankAccNo));
        }
        if (FCode.equalsIgnoreCase("BillingType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BillingType));
        }
        if (FCode.equalsIgnoreCase("RgtMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtMoney));
        }
        if (FCode.equalsIgnoreCase("Asset")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Asset));
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NetProfitRate));
        }
        if (FCode.equalsIgnoreCase("MainBussiness")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainBussiness));
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Corporation));
        }
        if (FCode.equalsIgnoreCase("ComAera")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComAera));
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Satrap));
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFoundDate()));
        }
        if (FCode.equalsIgnoreCase("GrpGroupNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpGroupNo));
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistFlag));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(VIPValue));
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equalsIgnoreCase("SubCompanyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubCompanyFlag));
        }
        if (FCode.equalsIgnoreCase("SupCustoemrNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SupCustoemrNo));
        }
        if (FCode.equalsIgnoreCase("LevelCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LevelCode));
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
        }
        if (FCode.equalsIgnoreCase("BusinessBigType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessBigType));
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialInsuNo));
        }
        if (FCode.equalsIgnoreCase("BlackListReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlackListReason));
        }
        if (FCode.equalsIgnoreCase("ComNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComNo));
        }
        if (FCode.equalsIgnoreCase("ComWebsite")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComWebsite));
        }
        if (FCode.equalsIgnoreCase("BusRegNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusRegNum));
        }
        if (FCode.equalsIgnoreCase("CorporationIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorporationIDNo));
        }
        if (FCode.equalsIgnoreCase("ComCorporationNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCorporationNo));
        }
        if (FCode.equalsIgnoreCase("ContrShar")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContrShar));
        }
        if (FCode.equalsIgnoreCase("HodingIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HodingIDType));
        }
        if (FCode.equalsIgnoreCase("HodingIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HodingIDNo));
        }
        if (FCode.equalsIgnoreCase("HodingValidate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHodingValidate()));
        }
        if (FCode.equalsIgnoreCase("CorporationIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CorporationIDType));
        }
        if (FCode.equalsIgnoreCase("CorporationValidate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCorporationValidate()));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
    * 取得Schema中指定索引值所对应的字段值
    * @param: nFieldIndex int 指定的字段索引值
    * @return: String
    * 如果没有对应的字段，返回""
    * 如果字段值为空，返回"null"
    **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch(nFieldIndex) {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BusinessType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpNature);
                break;
            case 5:
                strFieldValue = String.valueOf(Peoples);
                break;
            case 6:
                strFieldValue = String.valueOf(OnWorkPeoples);
                break;
            case 7:
                strFieldValue = String.valueOf(InsurePeoples);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ComDocIDType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ComDocIDNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBusliceDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PostCode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(GrpAddress);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Fax);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(UnitRegisteredAddress);
                break;
            case 15:
                strFieldValue = String.valueOf(UnitDuration);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Principal);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(PrincipalIDType);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(PrincipalIDNo);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPrincipalValidate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(Telephone);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Mobile);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAnnuityReceiveDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Managecom);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(HeadShip);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(GetFlag);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(DisputedFlag);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(BankProivnce);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(BankCity);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(BankLocations);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(TaxPayerIDType);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(TaxPayerIDNO);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(TaxBureauAddr);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(TaxBureauTel);
                break;
            case 39:
                strFieldValue = StrTool.GBKToUnicode(TaxBureauBankCode);
                break;
            case 40:
                strFieldValue = StrTool.GBKToUnicode(TaxBureauBankAccNo);
                break;
            case 41:
                strFieldValue = StrTool.GBKToUnicode(BillingType);
                break;
            case 42:
                strFieldValue = String.valueOf(RgtMoney);
                break;
            case 43:
                strFieldValue = String.valueOf(Asset);
                break;
            case 44:
                strFieldValue = String.valueOf(NetProfitRate);
                break;
            case 45:
                strFieldValue = StrTool.GBKToUnicode(MainBussiness);
                break;
            case 46:
                strFieldValue = StrTool.GBKToUnicode(Corporation);
                break;
            case 47:
                strFieldValue = StrTool.GBKToUnicode(ComAera);
                break;
            case 48:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 49:
                strFieldValue = StrTool.GBKToUnicode(Satrap);
                break;
            case 50:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFoundDate()));
                break;
            case 51:
                strFieldValue = StrTool.GBKToUnicode(GrpGroupNo);
                break;
            case 52:
                strFieldValue = StrTool.GBKToUnicode(BlacklistFlag);
                break;
            case 53:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 54:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 55:
                strFieldValue = StrTool.GBKToUnicode(VIPValue);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 57:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 61:
                strFieldValue = StrTool.GBKToUnicode(SubCompanyFlag);
                break;
            case 62:
                strFieldValue = StrTool.GBKToUnicode(SupCustoemrNo);
                break;
            case 63:
                strFieldValue = StrTool.GBKToUnicode(LevelCode);
                break;
            case 64:
                strFieldValue = String.valueOf(OffWorkPeoples);
                break;
            case 65:
                strFieldValue = String.valueOf(OtherPeoples);
                break;
            case 66:
                strFieldValue = StrTool.GBKToUnicode(BusinessBigType);
                break;
            case 67:
                strFieldValue = StrTool.GBKToUnicode(SocialInsuNo);
                break;
            case 68:
                strFieldValue = StrTool.GBKToUnicode(BlackListReason);
                break;
            case 69:
                strFieldValue = StrTool.GBKToUnicode(ComNo);
                break;
            case 70:
                strFieldValue = StrTool.GBKToUnicode(ComWebsite);
                break;
            case 71:
                strFieldValue = StrTool.GBKToUnicode(BusRegNum);
                break;
            case 72:
                strFieldValue = StrTool.GBKToUnicode(CorporationIDNo);
                break;
            case 73:
                strFieldValue = StrTool.GBKToUnicode(ComCorporationNo);
                break;
            case 74:
                strFieldValue = StrTool.GBKToUnicode(ContrShar);
                break;
            case 75:
                strFieldValue = StrTool.GBKToUnicode(HodingIDType);
                break;
            case 76:
                strFieldValue = StrTool.GBKToUnicode(HodingIDNo);
                break;
            case 77:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHodingValidate()));
                break;
            case 78:
                strFieldValue = StrTool.GBKToUnicode(CorporationIDType);
                break;
            case 79:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCorporationValidate()));
                break;
            default:
                strFieldValue = "";
        }
        if( strFieldValue.equals("") ) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
    * 设置对应传入参数的String形式的字段值
    * @param: FCode String 需要赋值的对象
    * @param: FValue String 要赋的值
    * @return: boolean
    **/
    public boolean setV(String FCode ,String FValue) {
        if(StrTool.cTrim(FCode).equals(""))
            return false;

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
                CustomerNo = null;
        }
        if (FCode.equalsIgnoreCase("Password")) {
            if( FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
                Password = null;
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpName = FValue.trim();
            }
            else
                GrpName = null;
        }
        if (FCode.equalsIgnoreCase("BusinessType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusinessType = FValue.trim();
            }
            else
                BusinessType = null;
        }
        if (FCode.equalsIgnoreCase("GrpNature")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpNature = FValue.trim();
            }
            else
                GrpNature = null;
        }
        if (FCode.equalsIgnoreCase("Peoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                Peoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OnWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("InsurePeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                InsurePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("ComDocIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComDocIDType = FValue.trim();
            }
            else
                ComDocIDType = null;
        }
        if (FCode.equalsIgnoreCase("ComDocIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComDocIDNo = FValue.trim();
            }
            else
                ComDocIDNo = null;
        }
        if (FCode.equalsIgnoreCase("BusliceDate")) {
            if(FValue != null && !FValue.equals("")) {
                BusliceDate = fDate.getDate( FValue );
            }
            else
                BusliceDate = null;
        }
        if (FCode.equalsIgnoreCase("PostCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                PostCode = FValue.trim();
            }
            else
                PostCode = null;
        }
        if (FCode.equalsIgnoreCase("GrpAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpAddress = FValue.trim();
            }
            else
                GrpAddress = null;
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if( FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
                Fax = null;
        }
        if (FCode.equalsIgnoreCase("UnitRegisteredAddress")) {
            if( FValue != null && !FValue.equals(""))
            {
                UnitRegisteredAddress = FValue.trim();
            }
            else
                UnitRegisteredAddress = null;
        }
        if (FCode.equalsIgnoreCase("UnitDuration")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                UnitDuration = d;
            }
        }
        if (FCode.equalsIgnoreCase("Principal")) {
            if( FValue != null && !FValue.equals(""))
            {
                Principal = FValue.trim();
            }
            else
                Principal = null;
        }
        if (FCode.equalsIgnoreCase("PrincipalIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrincipalIDType = FValue.trim();
            }
            else
                PrincipalIDType = null;
        }
        if (FCode.equalsIgnoreCase("PrincipalIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                PrincipalIDNo = FValue.trim();
            }
            else
                PrincipalIDNo = null;
        }
        if (FCode.equalsIgnoreCase("PrincipalValidate")) {
            if(FValue != null && !FValue.equals("")) {
                PrincipalValidate = fDate.getDate( FValue );
            }
            else
                PrincipalValidate = null;
        }
        if (FCode.equalsIgnoreCase("Telephone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Telephone = FValue.trim();
            }
            else
                Telephone = null;
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if( FValue != null && !FValue.equals(""))
            {
                Mobile = FValue.trim();
            }
            else
                Mobile = null;
        }
        if (FCode.equalsIgnoreCase("EMail")) {
            if( FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
                EMail = null;
        }
        if (FCode.equalsIgnoreCase("AnnuityReceiveDate")) {
            if(FValue != null && !FValue.equals("")) {
                AnnuityReceiveDate = fDate.getDate( FValue );
            }
            else
                AnnuityReceiveDate = null;
        }
        if (FCode.equalsIgnoreCase("Managecom")) {
            if( FValue != null && !FValue.equals(""))
            {
                Managecom = FValue.trim();
            }
            else
                Managecom = null;
        }
        if (FCode.equalsIgnoreCase("HeadShip")) {
            if( FValue != null && !FValue.equals(""))
            {
                HeadShip = FValue.trim();
            }
            else
                HeadShip = null;
        }
        if (FCode.equalsIgnoreCase("GetFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                GetFlag = FValue.trim();
            }
            else
                GetFlag = null;
        }
        if (FCode.equalsIgnoreCase("DisputedFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                DisputedFlag = FValue.trim();
            }
            else
                DisputedFlag = null;
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
                BankCode = null;
        }
        if (FCode.equalsIgnoreCase("BankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
                BankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
                AccName = null;
        }
        if (FCode.equalsIgnoreCase("BankProivnce")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankProivnce = FValue.trim();
            }
            else
                BankProivnce = null;
        }
        if (FCode.equalsIgnoreCase("BankCity")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankCity = FValue.trim();
            }
            else
                BankCity = null;
        }
        if (FCode.equalsIgnoreCase("AccType")) {
            if( FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
                AccType = null;
        }
        if (FCode.equalsIgnoreCase("BankLocations")) {
            if( FValue != null && !FValue.equals(""))
            {
                BankLocations = FValue.trim();
            }
            else
                BankLocations = null;
        }
        if (FCode.equalsIgnoreCase("TaxPayerIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxPayerIDType = FValue.trim();
            }
            else
                TaxPayerIDType = null;
        }
        if (FCode.equalsIgnoreCase("TaxPayerIDNO")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxPayerIDNO = FValue.trim();
            }
            else
                TaxPayerIDNO = null;
        }
        if (FCode.equalsIgnoreCase("TaxBureauAddr")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxBureauAddr = FValue.trim();
            }
            else
                TaxBureauAddr = null;
        }
        if (FCode.equalsIgnoreCase("TaxBureauTel")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxBureauTel = FValue.trim();
            }
            else
                TaxBureauTel = null;
        }
        if (FCode.equalsIgnoreCase("TaxBureauBankCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxBureauBankCode = FValue.trim();
            }
            else
                TaxBureauBankCode = null;
        }
        if (FCode.equalsIgnoreCase("TaxBureauBankAccNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                TaxBureauBankAccNo = FValue.trim();
            }
            else
                TaxBureauBankAccNo = null;
        }
        if (FCode.equalsIgnoreCase("BillingType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BillingType = FValue.trim();
            }
            else
                BillingType = null;
        }
        if (FCode.equalsIgnoreCase("RgtMoney")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                RgtMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("Asset")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                Asset = d;
            }
        }
        if (FCode.equalsIgnoreCase("NetProfitRate")) {
            if( FValue != null && !FValue.equals("")) {
                Double tDouble = new Double( FValue );
                double d = tDouble.doubleValue();
                NetProfitRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("MainBussiness")) {
            if( FValue != null && !FValue.equals(""))
            {
                MainBussiness = FValue.trim();
            }
            else
                MainBussiness = null;
        }
        if (FCode.equalsIgnoreCase("Corporation")) {
            if( FValue != null && !FValue.equals(""))
            {
                Corporation = FValue.trim();
            }
            else
                Corporation = null;
        }
        if (FCode.equalsIgnoreCase("ComAera")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComAera = FValue.trim();
            }
            else
                ComAera = null;
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if( FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
                Phone = null;
        }
        if (FCode.equalsIgnoreCase("Satrap")) {
            if( FValue != null && !FValue.equals(""))
            {
                Satrap = FValue.trim();
            }
            else
                Satrap = null;
        }
        if (FCode.equalsIgnoreCase("FoundDate")) {
            if(FValue != null && !FValue.equals("")) {
                FoundDate = fDate.getDate( FValue );
            }
            else
                FoundDate = null;
        }
        if (FCode.equalsIgnoreCase("GrpGroupNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                GrpGroupNo = FValue.trim();
            }
            else
                GrpGroupNo = null;
        }
        if (FCode.equalsIgnoreCase("BlacklistFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlacklistFlag = FValue.trim();
            }
            else
                BlacklistFlag = null;
        }
        if (FCode.equalsIgnoreCase("State")) {
            if( FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
                State = null;
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if( FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
                Remark = null;
        }
        if (FCode.equalsIgnoreCase("VIPValue")) {
            if( FValue != null && !FValue.equals(""))
            {
                VIPValue = FValue.trim();
            }
            else
                VIPValue = null;
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if( FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
                Operator = null;
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if(FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate( FValue );
            }
            else
                MakeDate = null;
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
                MakeTime = null;
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if(FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate( FValue );
            }
            else
                ModifyDate = null;
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if( FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
                ModifyTime = null;
        }
        if (FCode.equalsIgnoreCase("SubCompanyFlag")) {
            if( FValue != null && !FValue.equals(""))
            {
                SubCompanyFlag = FValue.trim();
            }
            else
                SubCompanyFlag = null;
        }
        if (FCode.equalsIgnoreCase("SupCustoemrNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SupCustoemrNo = FValue.trim();
            }
            else
                SupCustoemrNo = null;
        }
        if (FCode.equalsIgnoreCase("LevelCode")) {
            if( FValue != null && !FValue.equals(""))
            {
                LevelCode = FValue.trim();
            }
            else
                LevelCode = null;
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OffWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            if( FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer( FValue );
                int i = tInteger.intValue();
                OtherPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("BusinessBigType")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusinessBigType = FValue.trim();
            }
            else
                BusinessBigType = null;
        }
        if (FCode.equalsIgnoreCase("SocialInsuNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                SocialInsuNo = FValue.trim();
            }
            else
                SocialInsuNo = null;
        }
        if (FCode.equalsIgnoreCase("BlackListReason")) {
            if( FValue != null && !FValue.equals(""))
            {
                BlackListReason = FValue.trim();
            }
            else
                BlackListReason = null;
        }
        if (FCode.equalsIgnoreCase("ComNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComNo = FValue.trim();
            }
            else
                ComNo = null;
        }
        if (FCode.equalsIgnoreCase("ComWebsite")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComWebsite = FValue.trim();
            }
            else
                ComWebsite = null;
        }
        if (FCode.equalsIgnoreCase("BusRegNum")) {
            if( FValue != null && !FValue.equals(""))
            {
                BusRegNum = FValue.trim();
            }
            else
                BusRegNum = null;
        }
        if (FCode.equalsIgnoreCase("CorporationIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorporationIDNo = FValue.trim();
            }
            else
                CorporationIDNo = null;
        }
        if (FCode.equalsIgnoreCase("ComCorporationNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                ComCorporationNo = FValue.trim();
            }
            else
                ComCorporationNo = null;
        }
        if (FCode.equalsIgnoreCase("ContrShar")) {
            if( FValue != null && !FValue.equals(""))
            {
                ContrShar = FValue.trim();
            }
            else
                ContrShar = null;
        }
        if (FCode.equalsIgnoreCase("HodingIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                HodingIDType = FValue.trim();
            }
            else
                HodingIDType = null;
        }
        if (FCode.equalsIgnoreCase("HodingIDNo")) {
            if( FValue != null && !FValue.equals(""))
            {
                HodingIDNo = FValue.trim();
            }
            else
                HodingIDNo = null;
        }
        if (FCode.equalsIgnoreCase("HodingValidate")) {
            if(FValue != null && !FValue.equals("")) {
                HodingValidate = fDate.getDate( FValue );
            }
            else
                HodingValidate = null;
        }
        if (FCode.equalsIgnoreCase("CorporationIDType")) {
            if( FValue != null && !FValue.equals(""))
            {
                CorporationIDType = FValue.trim();
            }
            else
                CorporationIDType = null;
        }
        if (FCode.equalsIgnoreCase("CorporationValidate")) {
            if(FValue != null && !FValue.equals("")) {
                CorporationValidate = fDate.getDate( FValue );
            }
            else
                CorporationValidate = null;
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        LDGrpSchema other = (LDGrpSchema)otherObject;
        return
            CustomerNo.equals(other.getCustomerNo())
            && Password.equals(other.getPassword())
            && GrpName.equals(other.getGrpName())
            && BusinessType.equals(other.getBusinessType())
            && GrpNature.equals(other.getGrpNature())
            && Peoples == other.getPeoples()
            && OnWorkPeoples == other.getOnWorkPeoples()
            && InsurePeoples == other.getInsurePeoples()
            && ComDocIDType.equals(other.getComDocIDType())
            && ComDocIDNo.equals(other.getComDocIDNo())
            && fDate.getString(BusliceDate).equals(other.getBusliceDate())
            && PostCode.equals(other.getPostCode())
            && GrpAddress.equals(other.getGrpAddress())
            && Fax.equals(other.getFax())
            && UnitRegisteredAddress.equals(other.getUnitRegisteredAddress())
            && UnitDuration == other.getUnitDuration()
            && Principal.equals(other.getPrincipal())
            && PrincipalIDType.equals(other.getPrincipalIDType())
            && PrincipalIDNo.equals(other.getPrincipalIDNo())
            && fDate.getString(PrincipalValidate).equals(other.getPrincipalValidate())
            && Telephone.equals(other.getTelephone())
            && Mobile.equals(other.getMobile())
            && EMail.equals(other.getEMail())
            && fDate.getString(AnnuityReceiveDate).equals(other.getAnnuityReceiveDate())
            && Managecom.equals(other.getManagecom())
            && HeadShip.equals(other.getHeadShip())
            && GetFlag.equals(other.getGetFlag())
            && DisputedFlag.equals(other.getDisputedFlag())
            && BankCode.equals(other.getBankCode())
            && BankAccNo.equals(other.getBankAccNo())
            && AccName.equals(other.getAccName())
            && BankProivnce.equals(other.getBankProivnce())
            && BankCity.equals(other.getBankCity())
            && AccType.equals(other.getAccType())
            && BankLocations.equals(other.getBankLocations())
            && TaxPayerIDType.equals(other.getTaxPayerIDType())
            && TaxPayerIDNO.equals(other.getTaxPayerIDNO())
            && TaxBureauAddr.equals(other.getTaxBureauAddr())
            && TaxBureauTel.equals(other.getTaxBureauTel())
            && TaxBureauBankCode.equals(other.getTaxBureauBankCode())
            && TaxBureauBankAccNo.equals(other.getTaxBureauBankAccNo())
            && BillingType.equals(other.getBillingType())
            && RgtMoney == other.getRgtMoney()
            && Asset == other.getAsset()
            && NetProfitRate == other.getNetProfitRate()
            && MainBussiness.equals(other.getMainBussiness())
            && Corporation.equals(other.getCorporation())
            && ComAera.equals(other.getComAera())
            && Phone.equals(other.getPhone())
            && Satrap.equals(other.getSatrap())
            && fDate.getString(FoundDate).equals(other.getFoundDate())
            && GrpGroupNo.equals(other.getGrpGroupNo())
            && BlacklistFlag.equals(other.getBlacklistFlag())
            && State.equals(other.getState())
            && Remark.equals(other.getRemark())
            && VIPValue.equals(other.getVIPValue())
            && Operator.equals(other.getOperator())
            && fDate.getString(MakeDate).equals(other.getMakeDate())
            && MakeTime.equals(other.getMakeTime())
            && fDate.getString(ModifyDate).equals(other.getModifyDate())
            && ModifyTime.equals(other.getModifyTime())
            && SubCompanyFlag.equals(other.getSubCompanyFlag())
            && SupCustoemrNo.equals(other.getSupCustoemrNo())
            && LevelCode.equals(other.getLevelCode())
            && OffWorkPeoples == other.getOffWorkPeoples()
            && OtherPeoples == other.getOtherPeoples()
            && BusinessBigType.equals(other.getBusinessBigType())
            && SocialInsuNo.equals(other.getSocialInsuNo())
            && BlackListReason.equals(other.getBlackListReason())
            && ComNo.equals(other.getComNo())
            && ComWebsite.equals(other.getComWebsite())
            && BusRegNum.equals(other.getBusRegNum())
            && CorporationIDNo.equals(other.getCorporationIDNo())
            && ComCorporationNo.equals(other.getComCorporationNo())
            && ContrShar.equals(other.getContrShar())
            && HodingIDType.equals(other.getHodingIDType())
            && HodingIDNo.equals(other.getHodingIDNo())
            && fDate.getString(HodingValidate).equals(other.getHodingValidate())
            && CorporationIDType.equals(other.getCorporationIDType())
            && fDate.getString(CorporationValidate).equals(other.getCorporationValidate());
    }

    /**
    * 取得Schema拥有字段的数量
       * @return: int
    **/
    public int getFieldCount()
    {
         return FIELDNUM;
    }

    /**
    * 取得Schema中指定字段名所对应的索引值
    * 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldIndex(String strFieldName)
    {
        if( strFieldName.equals("CustomerNo") ) {
            return 0;
        }
        if( strFieldName.equals("Password") ) {
            return 1;
        }
        if( strFieldName.equals("GrpName") ) {
            return 2;
        }
        if( strFieldName.equals("BusinessType") ) {
            return 3;
        }
        if( strFieldName.equals("GrpNature") ) {
            return 4;
        }
        if( strFieldName.equals("Peoples") ) {
            return 5;
        }
        if( strFieldName.equals("OnWorkPeoples") ) {
            return 6;
        }
        if( strFieldName.equals("InsurePeoples") ) {
            return 7;
        }
        if( strFieldName.equals("ComDocIDType") ) {
            return 8;
        }
        if( strFieldName.equals("ComDocIDNo") ) {
            return 9;
        }
        if( strFieldName.equals("BusliceDate") ) {
            return 10;
        }
        if( strFieldName.equals("PostCode") ) {
            return 11;
        }
        if( strFieldName.equals("GrpAddress") ) {
            return 12;
        }
        if( strFieldName.equals("Fax") ) {
            return 13;
        }
        if( strFieldName.equals("UnitRegisteredAddress") ) {
            return 14;
        }
        if( strFieldName.equals("UnitDuration") ) {
            return 15;
        }
        if( strFieldName.equals("Principal") ) {
            return 16;
        }
        if( strFieldName.equals("PrincipalIDType") ) {
            return 17;
        }
        if( strFieldName.equals("PrincipalIDNo") ) {
            return 18;
        }
        if( strFieldName.equals("PrincipalValidate") ) {
            return 19;
        }
        if( strFieldName.equals("Telephone") ) {
            return 20;
        }
        if( strFieldName.equals("Mobile") ) {
            return 21;
        }
        if( strFieldName.equals("EMail") ) {
            return 22;
        }
        if( strFieldName.equals("AnnuityReceiveDate") ) {
            return 23;
        }
        if( strFieldName.equals("Managecom") ) {
            return 24;
        }
        if( strFieldName.equals("HeadShip") ) {
            return 25;
        }
        if( strFieldName.equals("GetFlag") ) {
            return 26;
        }
        if( strFieldName.equals("DisputedFlag") ) {
            return 27;
        }
        if( strFieldName.equals("BankCode") ) {
            return 28;
        }
        if( strFieldName.equals("BankAccNo") ) {
            return 29;
        }
        if( strFieldName.equals("AccName") ) {
            return 30;
        }
        if( strFieldName.equals("BankProivnce") ) {
            return 31;
        }
        if( strFieldName.equals("BankCity") ) {
            return 32;
        }
        if( strFieldName.equals("AccType") ) {
            return 33;
        }
        if( strFieldName.equals("BankLocations") ) {
            return 34;
        }
        if( strFieldName.equals("TaxPayerIDType") ) {
            return 35;
        }
        if( strFieldName.equals("TaxPayerIDNO") ) {
            return 36;
        }
        if( strFieldName.equals("TaxBureauAddr") ) {
            return 37;
        }
        if( strFieldName.equals("TaxBureauTel") ) {
            return 38;
        }
        if( strFieldName.equals("TaxBureauBankCode") ) {
            return 39;
        }
        if( strFieldName.equals("TaxBureauBankAccNo") ) {
            return 40;
        }
        if( strFieldName.equals("BillingType") ) {
            return 41;
        }
        if( strFieldName.equals("RgtMoney") ) {
            return 42;
        }
        if( strFieldName.equals("Asset") ) {
            return 43;
        }
        if( strFieldName.equals("NetProfitRate") ) {
            return 44;
        }
        if( strFieldName.equals("MainBussiness") ) {
            return 45;
        }
        if( strFieldName.equals("Corporation") ) {
            return 46;
        }
        if( strFieldName.equals("ComAera") ) {
            return 47;
        }
        if( strFieldName.equals("Phone") ) {
            return 48;
        }
        if( strFieldName.equals("Satrap") ) {
            return 49;
        }
        if( strFieldName.equals("FoundDate") ) {
            return 50;
        }
        if( strFieldName.equals("GrpGroupNo") ) {
            return 51;
        }
        if( strFieldName.equals("BlacklistFlag") ) {
            return 52;
        }
        if( strFieldName.equals("State") ) {
            return 53;
        }
        if( strFieldName.equals("Remark") ) {
            return 54;
        }
        if( strFieldName.equals("VIPValue") ) {
            return 55;
        }
        if( strFieldName.equals("Operator") ) {
            return 56;
        }
        if( strFieldName.equals("MakeDate") ) {
            return 57;
        }
        if( strFieldName.equals("MakeTime") ) {
            return 58;
        }
        if( strFieldName.equals("ModifyDate") ) {
            return 59;
        }
        if( strFieldName.equals("ModifyTime") ) {
            return 60;
        }
        if( strFieldName.equals("SubCompanyFlag") ) {
            return 61;
        }
        if( strFieldName.equals("SupCustoemrNo") ) {
            return 62;
        }
        if( strFieldName.equals("LevelCode") ) {
            return 63;
        }
        if( strFieldName.equals("OffWorkPeoples") ) {
            return 64;
        }
        if( strFieldName.equals("OtherPeoples") ) {
            return 65;
        }
        if( strFieldName.equals("BusinessBigType") ) {
            return 66;
        }
        if( strFieldName.equals("SocialInsuNo") ) {
            return 67;
        }
        if( strFieldName.equals("BlackListReason") ) {
            return 68;
        }
        if( strFieldName.equals("ComNo") ) {
            return 69;
        }
        if( strFieldName.equals("ComWebsite") ) {
            return 70;
        }
        if( strFieldName.equals("BusRegNum") ) {
            return 71;
        }
        if( strFieldName.equals("CorporationIDNo") ) {
            return 72;
        }
        if( strFieldName.equals("ComCorporationNo") ) {
            return 73;
        }
        if( strFieldName.equals("ContrShar") ) {
            return 74;
        }
        if( strFieldName.equals("HodingIDType") ) {
            return 75;
        }
        if( strFieldName.equals("HodingIDNo") ) {
            return 76;
        }
        if( strFieldName.equals("HodingValidate") ) {
            return 77;
        }
        if( strFieldName.equals("CorporationIDType") ) {
            return 78;
        }
        if( strFieldName.equals("CorporationValidate") ) {
            return 79;
        }
        return -1;
    }

    /**
    * 取得Schema中指定索引值所对应的字段名
    * 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
    **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch(nFieldIndex) {
            case 0:
                strFieldName = "CustomerNo";
                break;
            case 1:
                strFieldName = "Password";
                break;
            case 2:
                strFieldName = "GrpName";
                break;
            case 3:
                strFieldName = "BusinessType";
                break;
            case 4:
                strFieldName = "GrpNature";
                break;
            case 5:
                strFieldName = "Peoples";
                break;
            case 6:
                strFieldName = "OnWorkPeoples";
                break;
            case 7:
                strFieldName = "InsurePeoples";
                break;
            case 8:
                strFieldName = "ComDocIDType";
                break;
            case 9:
                strFieldName = "ComDocIDNo";
                break;
            case 10:
                strFieldName = "BusliceDate";
                break;
            case 11:
                strFieldName = "PostCode";
                break;
            case 12:
                strFieldName = "GrpAddress";
                break;
            case 13:
                strFieldName = "Fax";
                break;
            case 14:
                strFieldName = "UnitRegisteredAddress";
                break;
            case 15:
                strFieldName = "UnitDuration";
                break;
            case 16:
                strFieldName = "Principal";
                break;
            case 17:
                strFieldName = "PrincipalIDType";
                break;
            case 18:
                strFieldName = "PrincipalIDNo";
                break;
            case 19:
                strFieldName = "PrincipalValidate";
                break;
            case 20:
                strFieldName = "Telephone";
                break;
            case 21:
                strFieldName = "Mobile";
                break;
            case 22:
                strFieldName = "EMail";
                break;
            case 23:
                strFieldName = "AnnuityReceiveDate";
                break;
            case 24:
                strFieldName = "Managecom";
                break;
            case 25:
                strFieldName = "HeadShip";
                break;
            case 26:
                strFieldName = "GetFlag";
                break;
            case 27:
                strFieldName = "DisputedFlag";
                break;
            case 28:
                strFieldName = "BankCode";
                break;
            case 29:
                strFieldName = "BankAccNo";
                break;
            case 30:
                strFieldName = "AccName";
                break;
            case 31:
                strFieldName = "BankProivnce";
                break;
            case 32:
                strFieldName = "BankCity";
                break;
            case 33:
                strFieldName = "AccType";
                break;
            case 34:
                strFieldName = "BankLocations";
                break;
            case 35:
                strFieldName = "TaxPayerIDType";
                break;
            case 36:
                strFieldName = "TaxPayerIDNO";
                break;
            case 37:
                strFieldName = "TaxBureauAddr";
                break;
            case 38:
                strFieldName = "TaxBureauTel";
                break;
            case 39:
                strFieldName = "TaxBureauBankCode";
                break;
            case 40:
                strFieldName = "TaxBureauBankAccNo";
                break;
            case 41:
                strFieldName = "BillingType";
                break;
            case 42:
                strFieldName = "RgtMoney";
                break;
            case 43:
                strFieldName = "Asset";
                break;
            case 44:
                strFieldName = "NetProfitRate";
                break;
            case 45:
                strFieldName = "MainBussiness";
                break;
            case 46:
                strFieldName = "Corporation";
                break;
            case 47:
                strFieldName = "ComAera";
                break;
            case 48:
                strFieldName = "Phone";
                break;
            case 49:
                strFieldName = "Satrap";
                break;
            case 50:
                strFieldName = "FoundDate";
                break;
            case 51:
                strFieldName = "GrpGroupNo";
                break;
            case 52:
                strFieldName = "BlacklistFlag";
                break;
            case 53:
                strFieldName = "State";
                break;
            case 54:
                strFieldName = "Remark";
                break;
            case 55:
                strFieldName = "VIPValue";
                break;
            case 56:
                strFieldName = "Operator";
                break;
            case 57:
                strFieldName = "MakeDate";
                break;
            case 58:
                strFieldName = "MakeTime";
                break;
            case 59:
                strFieldName = "ModifyDate";
                break;
            case 60:
                strFieldName = "ModifyTime";
                break;
            case 61:
                strFieldName = "SubCompanyFlag";
                break;
            case 62:
                strFieldName = "SupCustoemrNo";
                break;
            case 63:
                strFieldName = "LevelCode";
                break;
            case 64:
                strFieldName = "OffWorkPeoples";
                break;
            case 65:
                strFieldName = "OtherPeoples";
                break;
            case 66:
                strFieldName = "BusinessBigType";
                break;
            case 67:
                strFieldName = "SocialInsuNo";
                break;
            case 68:
                strFieldName = "BlackListReason";
                break;
            case 69:
                strFieldName = "ComNo";
                break;
            case 70:
                strFieldName = "ComWebsite";
                break;
            case 71:
                strFieldName = "BusRegNum";
                break;
            case 72:
                strFieldName = "CorporationIDNo";
                break;
            case 73:
                strFieldName = "ComCorporationNo";
                break;
            case 74:
                strFieldName = "ContrShar";
                break;
            case 75:
                strFieldName = "HodingIDType";
                break;
            case 76:
                strFieldName = "HodingIDNo";
                break;
            case 77:
                strFieldName = "HodingValidate";
                break;
            case 78:
                strFieldName = "CorporationIDType";
                break;
            case 79:
                strFieldName = "CorporationValidate";
                break;
            default:
                strFieldName = "";
        };
        return strFieldName;
    }

    /**
    * 取得Schema中指定字段名所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
    **/
    public int getFieldType(String strFieldName) {
        switch (strFieldName.toUpperCase()) {
            case "CUSTOMERNO":
                return Schema.TYPE_STRING;
            case "PASSWORD":
                return Schema.TYPE_STRING;
            case "GRPNAME":
                return Schema.TYPE_STRING;
            case "BUSINESSTYPE":
                return Schema.TYPE_STRING;
            case "GRPNATURE":
                return Schema.TYPE_STRING;
            case "PEOPLES":
                return Schema.TYPE_INT;
            case "ONWORKPEOPLES":
                return Schema.TYPE_INT;
            case "INSUREPEOPLES":
                return Schema.TYPE_INT;
            case "COMDOCIDTYPE":
                return Schema.TYPE_STRING;
            case "COMDOCIDNO":
                return Schema.TYPE_STRING;
            case "BUSLICEDATE":
                return Schema.TYPE_DATE;
            case "POSTCODE":
                return Schema.TYPE_STRING;
            case "GRPADDRESS":
                return Schema.TYPE_STRING;
            case "FAX":
                return Schema.TYPE_STRING;
            case "UNITREGISTEREDADDRESS":
                return Schema.TYPE_STRING;
            case "UNITDURATION":
                return Schema.TYPE_DOUBLE;
            case "PRINCIPAL":
                return Schema.TYPE_STRING;
            case "PRINCIPALIDTYPE":
                return Schema.TYPE_STRING;
            case "PRINCIPALIDNO":
                return Schema.TYPE_STRING;
            case "PRINCIPALVALIDATE":
                return Schema.TYPE_DATE;
            case "TELEPHONE":
                return Schema.TYPE_STRING;
            case "MOBILE":
                return Schema.TYPE_STRING;
            case "EMAIL":
                return Schema.TYPE_STRING;
            case "ANNUITYRECEIVEDATE":
                return Schema.TYPE_DATE;
            case "MANAGECOM":
                return Schema.TYPE_STRING;
            case "HEADSHIP":
                return Schema.TYPE_STRING;
            case "GETFLAG":
                return Schema.TYPE_STRING;
            case "DISPUTEDFLAG":
                return Schema.TYPE_STRING;
            case "BANKCODE":
                return Schema.TYPE_STRING;
            case "BANKACCNO":
                return Schema.TYPE_STRING;
            case "ACCNAME":
                return Schema.TYPE_STRING;
            case "BANKPROIVNCE":
                return Schema.TYPE_STRING;
            case "BANKCITY":
                return Schema.TYPE_STRING;
            case "ACCTYPE":
                return Schema.TYPE_STRING;
            case "BANKLOCATIONS":
                return Schema.TYPE_STRING;
            case "TAXPAYERIDTYPE":
                return Schema.TYPE_STRING;
            case "TAXPAYERIDNO":
                return Schema.TYPE_STRING;
            case "TAXBUREAUADDR":
                return Schema.TYPE_STRING;
            case "TAXBUREAUTEL":
                return Schema.TYPE_STRING;
            case "TAXBUREAUBANKCODE":
                return Schema.TYPE_STRING;
            case "TAXBUREAUBANKACCNO":
                return Schema.TYPE_STRING;
            case "BILLINGTYPE":
                return Schema.TYPE_STRING;
            case "RGTMONEY":
                return Schema.TYPE_DOUBLE;
            case "ASSET":
                return Schema.TYPE_DOUBLE;
            case "NETPROFITRATE":
                return Schema.TYPE_DOUBLE;
            case "MAINBUSSINESS":
                return Schema.TYPE_STRING;
            case "CORPORATION":
                return Schema.TYPE_STRING;
            case "COMAERA":
                return Schema.TYPE_STRING;
            case "PHONE":
                return Schema.TYPE_STRING;
            case "SATRAP":
                return Schema.TYPE_STRING;
            case "FOUNDDATE":
                return Schema.TYPE_DATE;
            case "GRPGROUPNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTFLAG":
                return Schema.TYPE_STRING;
            case "STATE":
                return Schema.TYPE_STRING;
            case "REMARK":
                return Schema.TYPE_STRING;
            case "VIPVALUE":
                return Schema.TYPE_STRING;
            case "OPERATOR":
                return Schema.TYPE_STRING;
            case "MAKEDATE":
                return Schema.TYPE_DATE;
            case "MAKETIME":
                return Schema.TYPE_STRING;
            case "MODIFYDATE":
                return Schema.TYPE_DATE;
            case "MODIFYTIME":
                return Schema.TYPE_STRING;
            case "SUBCOMPANYFLAG":
                return Schema.TYPE_STRING;
            case "SUPCUSTOEMRNO":
                return Schema.TYPE_STRING;
            case "LEVELCODE":
                return Schema.TYPE_STRING;
            case "OFFWORKPEOPLES":
                return Schema.TYPE_INT;
            case "OTHERPEOPLES":
                return Schema.TYPE_INT;
            case "BUSINESSBIGTYPE":
                return Schema.TYPE_STRING;
            case "SOCIALINSUNO":
                return Schema.TYPE_STRING;
            case "BLACKLISTREASON":
                return Schema.TYPE_STRING;
            case "COMNO":
                return Schema.TYPE_STRING;
            case "COMWEBSITE":
                return Schema.TYPE_STRING;
            case "BUSREGNUM":
                return Schema.TYPE_STRING;
            case "CORPORATIONIDNO":
                return Schema.TYPE_STRING;
            case "COMCORPORATIONNO":
                return Schema.TYPE_STRING;
            case "CONTRSHAR":
                return Schema.TYPE_STRING;
            case "HODINGIDTYPE":
                return Schema.TYPE_STRING;
            case "HODINGIDNO":
                return Schema.TYPE_STRING;
            case "HODINGVALIDATE":
                return Schema.TYPE_DATE;
            case "CORPORATIONIDTYPE":
                return Schema.TYPE_STRING;
            case "CORPORATIONVALIDATE":
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }

    /**
    * 取得Schema中指定索引值所对应的字段类型
    * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
    **/
    public int getFieldType(int nFieldIndex) {
        switch (nFieldIndex) {
            case 0:
                return Schema.TYPE_STRING;
            case 1:
                return Schema.TYPE_STRING;
            case 2:
                return Schema.TYPE_STRING;
            case 3:
                return Schema.TYPE_STRING;
            case 4:
                return Schema.TYPE_STRING;
            case 5:
                return Schema.TYPE_INT;
            case 6:
                return Schema.TYPE_INT;
            case 7:
                return Schema.TYPE_INT;
            case 8:
                return Schema.TYPE_STRING;
            case 9:
                return Schema.TYPE_STRING;
            case 10:
                return Schema.TYPE_DATE;
            case 11:
                return Schema.TYPE_STRING;
            case 12:
                return Schema.TYPE_STRING;
            case 13:
                return Schema.TYPE_STRING;
            case 14:
                return Schema.TYPE_STRING;
            case 15:
                return Schema.TYPE_DOUBLE;
            case 16:
                return Schema.TYPE_STRING;
            case 17:
                return Schema.TYPE_STRING;
            case 18:
                return Schema.TYPE_STRING;
            case 19:
                return Schema.TYPE_DATE;
            case 20:
                return Schema.TYPE_STRING;
            case 21:
                return Schema.TYPE_STRING;
            case 22:
                return Schema.TYPE_STRING;
            case 23:
                return Schema.TYPE_DATE;
            case 24:
                return Schema.TYPE_STRING;
            case 25:
                return Schema.TYPE_STRING;
            case 26:
                return Schema.TYPE_STRING;
            case 27:
                return Schema.TYPE_STRING;
            case 28:
                return Schema.TYPE_STRING;
            case 29:
                return Schema.TYPE_STRING;
            case 30:
                return Schema.TYPE_STRING;
            case 31:
                return Schema.TYPE_STRING;
            case 32:
                return Schema.TYPE_STRING;
            case 33:
                return Schema.TYPE_STRING;
            case 34:
                return Schema.TYPE_STRING;
            case 35:
                return Schema.TYPE_STRING;
            case 36:
                return Schema.TYPE_STRING;
            case 37:
                return Schema.TYPE_STRING;
            case 38:
                return Schema.TYPE_STRING;
            case 39:
                return Schema.TYPE_STRING;
            case 40:
                return Schema.TYPE_STRING;
            case 41:
                return Schema.TYPE_STRING;
            case 42:
                return Schema.TYPE_DOUBLE;
            case 43:
                return Schema.TYPE_DOUBLE;
            case 44:
                return Schema.TYPE_DOUBLE;
            case 45:
                return Schema.TYPE_STRING;
            case 46:
                return Schema.TYPE_STRING;
            case 47:
                return Schema.TYPE_STRING;
            case 48:
                return Schema.TYPE_STRING;
            case 49:
                return Schema.TYPE_STRING;
            case 50:
                return Schema.TYPE_DATE;
            case 51:
                return Schema.TYPE_STRING;
            case 52:
                return Schema.TYPE_STRING;
            case 53:
                return Schema.TYPE_STRING;
            case 54:
                return Schema.TYPE_STRING;
            case 55:
                return Schema.TYPE_STRING;
            case 56:
                return Schema.TYPE_STRING;
            case 57:
                return Schema.TYPE_DATE;
            case 58:
                return Schema.TYPE_STRING;
            case 59:
                return Schema.TYPE_DATE;
            case 60:
                return Schema.TYPE_STRING;
            case 61:
                return Schema.TYPE_STRING;
            case 62:
                return Schema.TYPE_STRING;
            case 63:
                return Schema.TYPE_STRING;
            case 64:
                return Schema.TYPE_INT;
            case 65:
                return Schema.TYPE_INT;
            case 66:
                return Schema.TYPE_STRING;
            case 67:
                return Schema.TYPE_STRING;
            case 68:
                return Schema.TYPE_STRING;
            case 69:
                return Schema.TYPE_STRING;
            case 70:
                return Schema.TYPE_STRING;
            case 71:
                return Schema.TYPE_STRING;
            case 72:
                return Schema.TYPE_STRING;
            case 73:
                return Schema.TYPE_STRING;
            case 74:
                return Schema.TYPE_STRING;
            case 75:
                return Schema.TYPE_STRING;
            case 76:
                return Schema.TYPE_STRING;
            case 77:
                return Schema.TYPE_DATE;
            case 78:
                return Schema.TYPE_STRING;
            case 79:
                return Schema.TYPE_DATE;
            default:
                return Schema.TYPE_NOFOUND;
        }
    }
}
