/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.vdb;

import java.sql.*;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/**
 * <p>ClassName: LCContDBSet </p>
 * <p>Description: DB层多记录数据库操作类文件 </p>
 * <p>Copyright: Copyright (c) 2019</p>
 * <p>Company: sinosoft </p>
 * @CreateDate：2019-09-07
 */
public class LCContDBSet extends LCContSet {
    // @Field
    private Connection con;
    private DBOper db;
    /**
    * flag = true: 传入Connection
    * flag = false: 不传入Connection
    **/
    private boolean mflag = false;

    // @Constructor
    public LCContDBSet(Connection tConnection) {
        con = tConnection;
        db = new DBOper(con,"LCCont");
        mflag = true;
    }

    public LCContDBSet() {
        db = new DBOper( "LCCont" );
    }
    // @Method
    public boolean deleteSQL() {
        if (db.deleteSQL(this)) {
            return true;
        } else {
            // @@错误处理
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCContDBSet";
            tError.functionName = "deleteSQL";
            tError.errorMessage = "操作失败!";
            this.mErrors .addOneError(tError);
            return false;
        }
    }

    /**
     * 删除操作
     * 删除条件：主键
     * @return boolean
     */
    public boolean delete() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("DELETE FROM LCCont WHERE  1=1  AND ContID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getContID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCContDBSet";
            tError.functionName = "delete()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (!mflag) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 更新操作
     * 更新条件：主键
     * @return boolean
     */
    public boolean update() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try {
            int tCount = this.size();
            pstmt = con.prepareStatement("UPDATE LCCont SET  ContID = ? , ShardingID = ? , GrpContNo = ? , ContNo = ? , ProposalContNo = ? , PrtNo = ? , ContType = ? , FamilyType = ? , FamilyID = ? , PolType = ? , CardFlag = ? , ManageCom = ? , ExecuteCom = ? , AgentCom = ? , AgentCode = ? , AgentGroup = ? , AgentCode1 = ? , AgentType = ? , SaleChnl = ? , Handler = ? , Password = ? , AppntNo = ? , AppntName = ? , AppntSex = ? , AppntBirthday = ? , AppntIDType = ? , AppntIDNo = ? , InsuredNo = ? , InsuredName = ? , InsuredSex = ? , InsuredBirthday = ? , InsuredIDType = ? , InsuredIDNo = ? , PayIntv = ? , PayMode = ? , PayLocation = ? , DisputedFlag = ? , OutPayFlag = ? , GetPolMode = ? , SignCom = ? , SignDate = ? , SignTime = ? , ConsignNo = ? , BankCode = ? , BankAccNo = ? , AccName = ? , PrintCount = ? , LostTimes = ? , Lang = ? , Currency = ? , Remark = ? , Peoples = ? , Mult = ? , Prem = ? , Amnt = ? , SumPrem = ? , Dif = ? , PaytoDate = ? , FirstPayDate = ? , CValiDate = ? , InputOperator = ? , InputDate = ? , InputTime = ? , ApproveFlag = ? , ApproveCode = ? , ApproveDate = ? , ApproveTime = ? , UWFlag = ? , UWOperator = ? , UWDate = ? , UWTime = ? , AppFlag = ? , PolApplyDate = ? , GetPolDate = ? , GetPolTime = ? , CustomGetPolDate = ? , State = ? , Operator = ? , MakeDate = ? , MakeTime = ? , ModifyDate = ? , ModifyTime = ? , FirstTrialOperator = ? , FirstTrialDate = ? , FirstTrialTime = ? , ReceiveOperator = ? , ReceiveDate = ? , ReceiveTime = ? , TempFeeNo = ? , SellType = ? , ForceUWFlag = ? , ForceUWReason = ? , NewBankCode = ? , NewBankAccNo = ? , NewAccName = ? , NewPayMode = ? , AgentBankCode = ? , BankAgent = ? , BankAgentName = ? , BankAgentTel = ? , ProdSetCode = ? , PolicyNo = ? , BillPressNo = ? , CardTypeCode = ? , VisitDate = ? , VisitTime = ? , SaleCom = ? , PrintFlag = ? , InvoicePrtFlag = ? , NewReinsureFlag = ? , RenewPayFlag = ? , AppntFirstName = ? , AppntLastName = ? , InsuredFirstName = ? , InsuredLastName = ? , AuthorFlag = ? , GreenChnl = ? , TBType = ? , EAuto = ? , SlipForm = ? , AutoPayFlag = ? , RnewFlag = ? , FamilyContNo = ? , BussFlag = ? , SignName = ? , OrganizeDate = ? , OrganizeTime = ? , NewAutoSendBankFlag = ? , AgentCodeOper = ? , AgentCodeAssi = ? , DelayReasonCode = ? , DelayReasonDesc = ? , XQremindflag = ? , OrganComCode = ? , BankProivnce = ? , NewBankProivnce = ? , ArbitrationCom = ? , OtherPrtno = ? , DestAccountDate = ? , PrePayCount = ? , NewBankCity = ? , InsuredBankCode = ? , InsuredBankAccNo = ? , InsuredAccName = ? , InsuredBankProvince = ? , InsuredBankCity = ? , NewAccType = ? , AccType = ? , BankCity = ? , EstimateMoney = ? , InsuredAccType = ? , IsAllowedCharges = ? , WTEdorAcceptNo = ? , Recording = ? , SaleChannels = ? , ZJAgentCom = ? , ZJAgentComName = ? , RecordFlag = ? , PlanCode = ? , ReceiptNo = ? , ReissueCont = ? , DomainCode = ? , DomainName = ? , ReferralNo = ? , ThirdPartyOrderId = ? , UserId = ? WHERE  1=1  AND ContID = ?");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getContID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGrpContNo());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getContNo());
            }
            if(this.get(i).getProposalContNo() == null || this.get(i).getProposalContNo().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getProposalContNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getPrtNo());
            }
            if(this.get(i).getContType() == null || this.get(i).getContType().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getContType());
            }
            if(this.get(i).getFamilyType() == null || this.get(i).getFamilyType().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getFamilyType());
            }
            if(this.get(i).getFamilyID() == null || this.get(i).getFamilyID().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getFamilyID());
            }
            if(this.get(i).getPolType() == null || this.get(i).getPolType().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getPolType());
            }
            if(this.get(i).getCardFlag() == null || this.get(i).getCardFlag().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getCardFlag());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getManageCom());
            }
            if(this.get(i).getExecuteCom() == null || this.get(i).getExecuteCom().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getExecuteCom());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getAgentCom());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getAgentGroup());
            }
            if(this.get(i).getAgentCode1() == null || this.get(i).getAgentCode1().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getAgentCode1());
            }
            if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getAgentType());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getSaleChnl());
            }
            if(this.get(i).getHandler() == null || this.get(i).getHandler().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getHandler());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getPassword());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getAppntNo());
            }
            if(this.get(i).getAppntName() == null || this.get(i).getAppntName().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getAppntName());
            }
            if(this.get(i).getAppntSex() == null || this.get(i).getAppntSex().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getAppntSex());
            }
            if(this.get(i).getAppntBirthday() == null || this.get(i).getAppntBirthday().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getAppntBirthday()));
            }
            if(this.get(i).getAppntIDType() == null || this.get(i).getAppntIDType().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getAppntIDType());
            }
            if(this.get(i).getAppntIDNo() == null || this.get(i).getAppntIDNo().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getAppntIDNo());
            }
            if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getInsuredNo());
            }
            if(this.get(i).getInsuredName() == null || this.get(i).getInsuredName().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getInsuredName());
            }
            if(this.get(i).getInsuredSex() == null || this.get(i).getInsuredSex().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getInsuredSex());
            }
            if(this.get(i).getInsuredBirthday() == null || this.get(i).getInsuredBirthday().equals("null")) {
                pstmt.setDate(31,null);
            } else {
                pstmt.setDate(31, Date.valueOf(this.get(i).getInsuredBirthday()));
            }
            if(this.get(i).getInsuredIDType() == null || this.get(i).getInsuredIDType().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getInsuredIDType());
            }
            if(this.get(i).getInsuredIDNo() == null || this.get(i).getInsuredIDNo().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getInsuredIDNo());
            }
            pstmt.setInt(34, this.get(i).getPayIntv());
            if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getPayMode());
            }
            if(this.get(i).getPayLocation() == null || this.get(i).getPayLocation().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getPayLocation());
            }
            if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getDisputedFlag());
            }
            if(this.get(i).getOutPayFlag() == null || this.get(i).getOutPayFlag().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getOutPayFlag());
            }
            if(this.get(i).getGetPolMode() == null || this.get(i).getGetPolMode().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getGetPolMode());
            }
            if(this.get(i).getSignCom() == null || this.get(i).getSignCom().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getSignCom());
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(41,null);
            } else {
                pstmt.setDate(41, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getSignTime());
            }
            if(this.get(i).getConsignNo() == null || this.get(i).getConsignNo().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getConsignNo());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getBankCode());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getBankAccNo());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getAccName());
            }
            pstmt.setInt(47, this.get(i).getPrintCount());
            pstmt.setInt(48, this.get(i).getLostTimes());
            if(this.get(i).getLang() == null || this.get(i).getLang().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getLang());
            }
            if(this.get(i).getCurrency() == null || this.get(i).getCurrency().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getCurrency());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getRemark());
            }
            pstmt.setInt(52, this.get(i).getPeoples());
            pstmt.setDouble(53, this.get(i).getMult());
            pstmt.setDouble(54, this.get(i).getPrem());
            pstmt.setDouble(55, this.get(i).getAmnt());
            pstmt.setDouble(56, this.get(i).getSumPrem());
            pstmt.setDouble(57, this.get(i).getDif());
            if(this.get(i).getPaytoDate() == null || this.get(i).getPaytoDate().equals("null")) {
                pstmt.setDate(58,null);
            } else {
                pstmt.setDate(58, Date.valueOf(this.get(i).getPaytoDate()));
            }
            if(this.get(i).getFirstPayDate() == null || this.get(i).getFirstPayDate().equals("null")) {
                pstmt.setDate(59,null);
            } else {
                pstmt.setDate(59, Date.valueOf(this.get(i).getFirstPayDate()));
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(60,null);
            } else {
                pstmt.setDate(60, Date.valueOf(this.get(i).getCValiDate()));
            }
            if(this.get(i).getInputOperator() == null || this.get(i).getInputOperator().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getInputOperator());
            }
            if(this.get(i).getInputDate() == null || this.get(i).getInputDate().equals("null")) {
                pstmt.setDate(62,null);
            } else {
                pstmt.setDate(62, Date.valueOf(this.get(i).getInputDate()));
            }
            if(this.get(i).getInputTime() == null || this.get(i).getInputTime().equals("null")) {
            	pstmt.setString(63,null);
            } else {
            	pstmt.setString(63, this.get(i).getInputTime());
            }
            if(this.get(i).getApproveFlag() == null || this.get(i).getApproveFlag().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getApproveFlag());
            }
            if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
            	pstmt.setString(65,null);
            } else {
            	pstmt.setString(65, this.get(i).getApproveCode());
            }
            if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                pstmt.setDate(66,null);
            } else {
                pstmt.setDate(66, Date.valueOf(this.get(i).getApproveDate()));
            }
            if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getApproveTime());
            }
            if(this.get(i).getUWFlag() == null || this.get(i).getUWFlag().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getUWFlag());
            }
            if(this.get(i).getUWOperator() == null || this.get(i).getUWOperator().equals("null")) {
            	pstmt.setString(69,null);
            } else {
            	pstmt.setString(69, this.get(i).getUWOperator());
            }
            if(this.get(i).getUWDate() == null || this.get(i).getUWDate().equals("null")) {
                pstmt.setDate(70,null);
            } else {
                pstmt.setDate(70, Date.valueOf(this.get(i).getUWDate()));
            }
            if(this.get(i).getUWTime() == null || this.get(i).getUWTime().equals("null")) {
            	pstmt.setString(71,null);
            } else {
            	pstmt.setString(71, this.get(i).getUWTime());
            }
            if(this.get(i).getAppFlag() == null || this.get(i).getAppFlag().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getAppFlag());
            }
            if(this.get(i).getPolApplyDate() == null || this.get(i).getPolApplyDate().equals("null")) {
                pstmt.setDate(73,null);
            } else {
                pstmt.setDate(73, Date.valueOf(this.get(i).getPolApplyDate()));
            }
            if(this.get(i).getGetPolDate() == null || this.get(i).getGetPolDate().equals("null")) {
                pstmt.setDate(74,null);
            } else {
                pstmt.setDate(74, Date.valueOf(this.get(i).getGetPolDate()));
            }
            if(this.get(i).getGetPolTime() == null || this.get(i).getGetPolTime().equals("null")) {
            	pstmt.setString(75,null);
            } else {
            	pstmt.setString(75, this.get(i).getGetPolTime());
            }
            if(this.get(i).getCustomGetPolDate() == null || this.get(i).getCustomGetPolDate().equals("null")) {
                pstmt.setDate(76,null);
            } else {
                pstmt.setDate(76, Date.valueOf(this.get(i).getCustomGetPolDate()));
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(77,null);
            } else {
            	pstmt.setString(77, this.get(i).getState());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(78,null);
            } else {
            	pstmt.setString(78, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(79,null);
            } else {
                pstmt.setDate(79, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(80,null);
            } else {
            	pstmt.setString(80, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(81,null);
            } else {
                pstmt.setDate(81, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(82,null);
            } else {
            	pstmt.setString(82, this.get(i).getModifyTime());
            }
            if(this.get(i).getFirstTrialOperator() == null || this.get(i).getFirstTrialOperator().equals("null")) {
            	pstmt.setString(83,null);
            } else {
            	pstmt.setString(83, this.get(i).getFirstTrialOperator());
            }
            if(this.get(i).getFirstTrialDate() == null || this.get(i).getFirstTrialDate().equals("null")) {
                pstmt.setDate(84,null);
            } else {
                pstmt.setDate(84, Date.valueOf(this.get(i).getFirstTrialDate()));
            }
            if(this.get(i).getFirstTrialTime() == null || this.get(i).getFirstTrialTime().equals("null")) {
            	pstmt.setString(85,null);
            } else {
            	pstmt.setString(85, this.get(i).getFirstTrialTime());
            }
            if(this.get(i).getReceiveOperator() == null || this.get(i).getReceiveOperator().equals("null")) {
            	pstmt.setString(86,null);
            } else {
            	pstmt.setString(86, this.get(i).getReceiveOperator());
            }
            if(this.get(i).getReceiveDate() == null || this.get(i).getReceiveDate().equals("null")) {
                pstmt.setDate(87,null);
            } else {
                pstmt.setDate(87, Date.valueOf(this.get(i).getReceiveDate()));
            }
            if(this.get(i).getReceiveTime() == null || this.get(i).getReceiveTime().equals("null")) {
            	pstmt.setString(88,null);
            } else {
            	pstmt.setString(88, this.get(i).getReceiveTime());
            }
            if(this.get(i).getTempFeeNo() == null || this.get(i).getTempFeeNo().equals("null")) {
            	pstmt.setString(89,null);
            } else {
            	pstmt.setString(89, this.get(i).getTempFeeNo());
            }
            if(this.get(i).getSellType() == null || this.get(i).getSellType().equals("null")) {
            	pstmt.setString(90,null);
            } else {
            	pstmt.setString(90, this.get(i).getSellType());
            }
            if(this.get(i).getForceUWFlag() == null || this.get(i).getForceUWFlag().equals("null")) {
            	pstmt.setString(91,null);
            } else {
            	pstmt.setString(91, this.get(i).getForceUWFlag());
            }
            if(this.get(i).getForceUWReason() == null || this.get(i).getForceUWReason().equals("null")) {
            	pstmt.setString(92,null);
            } else {
            	pstmt.setString(92, this.get(i).getForceUWReason());
            }
            if(this.get(i).getNewBankCode() == null || this.get(i).getNewBankCode().equals("null")) {
            	pstmt.setString(93,null);
            } else {
            	pstmt.setString(93, this.get(i).getNewBankCode());
            }
            if(this.get(i).getNewBankAccNo() == null || this.get(i).getNewBankAccNo().equals("null")) {
            	pstmt.setString(94,null);
            } else {
            	pstmt.setString(94, this.get(i).getNewBankAccNo());
            }
            if(this.get(i).getNewAccName() == null || this.get(i).getNewAccName().equals("null")) {
            	pstmt.setString(95,null);
            } else {
            	pstmt.setString(95, this.get(i).getNewAccName());
            }
            if(this.get(i).getNewPayMode() == null || this.get(i).getNewPayMode().equals("null")) {
            	pstmt.setString(96,null);
            } else {
            	pstmt.setString(96, this.get(i).getNewPayMode());
            }
            if(this.get(i).getAgentBankCode() == null || this.get(i).getAgentBankCode().equals("null")) {
            	pstmt.setString(97,null);
            } else {
            	pstmt.setString(97, this.get(i).getAgentBankCode());
            }
            if(this.get(i).getBankAgent() == null || this.get(i).getBankAgent().equals("null")) {
            	pstmt.setString(98,null);
            } else {
            	pstmt.setString(98, this.get(i).getBankAgent());
            }
            if(this.get(i).getBankAgentName() == null || this.get(i).getBankAgentName().equals("null")) {
            	pstmt.setString(99,null);
            } else {
            	pstmt.setString(99, this.get(i).getBankAgentName());
            }
            if(this.get(i).getBankAgentTel() == null || this.get(i).getBankAgentTel().equals("null")) {
            	pstmt.setString(100,null);
            } else {
            	pstmt.setString(100, this.get(i).getBankAgentTel());
            }
            if(this.get(i).getProdSetCode() == null || this.get(i).getProdSetCode().equals("null")) {
            	pstmt.setString(101,null);
            } else {
            	pstmt.setString(101, this.get(i).getProdSetCode());
            }
            if(this.get(i).getPolicyNo() == null || this.get(i).getPolicyNo().equals("null")) {
            	pstmt.setString(102,null);
            } else {
            	pstmt.setString(102, this.get(i).getPolicyNo());
            }
            if(this.get(i).getBillPressNo() == null || this.get(i).getBillPressNo().equals("null")) {
            	pstmt.setString(103,null);
            } else {
            	pstmt.setString(103, this.get(i).getBillPressNo());
            }
            if(this.get(i).getCardTypeCode() == null || this.get(i).getCardTypeCode().equals("null")) {
            	pstmt.setString(104,null);
            } else {
            	pstmt.setString(104, this.get(i).getCardTypeCode());
            }
            if(this.get(i).getVisitDate() == null || this.get(i).getVisitDate().equals("null")) {
                pstmt.setDate(105,null);
            } else {
                pstmt.setDate(105, Date.valueOf(this.get(i).getVisitDate()));
            }
            if(this.get(i).getVisitTime() == null || this.get(i).getVisitTime().equals("null")) {
            	pstmt.setString(106,null);
            } else {
            	pstmt.setString(106, this.get(i).getVisitTime());
            }
            if(this.get(i).getSaleCom() == null || this.get(i).getSaleCom().equals("null")) {
            	pstmt.setString(107,null);
            } else {
            	pstmt.setString(107, this.get(i).getSaleCom());
            }
            if(this.get(i).getPrintFlag() == null || this.get(i).getPrintFlag().equals("null")) {
            	pstmt.setString(108,null);
            } else {
            	pstmt.setString(108, this.get(i).getPrintFlag());
            }
            if(this.get(i).getInvoicePrtFlag() == null || this.get(i).getInvoicePrtFlag().equals("null")) {
            	pstmt.setString(109,null);
            } else {
            	pstmt.setString(109, this.get(i).getInvoicePrtFlag());
            }
            if(this.get(i).getNewReinsureFlag() == null || this.get(i).getNewReinsureFlag().equals("null")) {
            	pstmt.setString(110,null);
            } else {
            	pstmt.setString(110, this.get(i).getNewReinsureFlag());
            }
            if(this.get(i).getRenewPayFlag() == null || this.get(i).getRenewPayFlag().equals("null")) {
            	pstmt.setString(111,null);
            } else {
            	pstmt.setString(111, this.get(i).getRenewPayFlag());
            }
            if(this.get(i).getAppntFirstName() == null || this.get(i).getAppntFirstName().equals("null")) {
            	pstmt.setString(112,null);
            } else {
            	pstmt.setString(112, this.get(i).getAppntFirstName());
            }
            if(this.get(i).getAppntLastName() == null || this.get(i).getAppntLastName().equals("null")) {
            	pstmt.setString(113,null);
            } else {
            	pstmt.setString(113, this.get(i).getAppntLastName());
            }
            if(this.get(i).getInsuredFirstName() == null || this.get(i).getInsuredFirstName().equals("null")) {
            	pstmt.setString(114,null);
            } else {
            	pstmt.setString(114, this.get(i).getInsuredFirstName());
            }
            if(this.get(i).getInsuredLastName() == null || this.get(i).getInsuredLastName().equals("null")) {
            	pstmt.setString(115,null);
            } else {
            	pstmt.setString(115, this.get(i).getInsuredLastName());
            }
            if(this.get(i).getAuthorFlag() == null || this.get(i).getAuthorFlag().equals("null")) {
            	pstmt.setString(116,null);
            } else {
            	pstmt.setString(116, this.get(i).getAuthorFlag());
            }
            pstmt.setInt(117, this.get(i).getGreenChnl());
            if(this.get(i).getTBType() == null || this.get(i).getTBType().equals("null")) {
            	pstmt.setString(118,null);
            } else {
            	pstmt.setString(118, this.get(i).getTBType());
            }
            if(this.get(i).getEAuto() == null || this.get(i).getEAuto().equals("null")) {
            	pstmt.setString(119,null);
            } else {
            	pstmt.setString(119, this.get(i).getEAuto());
            }
            if(this.get(i).getSlipForm() == null || this.get(i).getSlipForm().equals("null")) {
            	pstmt.setString(120,null);
            } else {
            	pstmt.setString(120, this.get(i).getSlipForm());
            }
            if(this.get(i).getAutoPayFlag() == null || this.get(i).getAutoPayFlag().equals("null")) {
            	pstmt.setString(121,null);
            } else {
            	pstmt.setString(121, this.get(i).getAutoPayFlag());
            }
            pstmt.setInt(122, this.get(i).getRnewFlag());
            if(this.get(i).getFamilyContNo() == null || this.get(i).getFamilyContNo().equals("null")) {
            	pstmt.setString(123,null);
            } else {
            	pstmt.setString(123, this.get(i).getFamilyContNo());
            }
            if(this.get(i).getBussFlag() == null || this.get(i).getBussFlag().equals("null")) {
            	pstmt.setString(124,null);
            } else {
            	pstmt.setString(124, this.get(i).getBussFlag());
            }
            if(this.get(i).getSignName() == null || this.get(i).getSignName().equals("null")) {
            	pstmt.setString(125,null);
            } else {
            	pstmt.setString(125, this.get(i).getSignName());
            }
            if(this.get(i).getOrganizeDate() == null || this.get(i).getOrganizeDate().equals("null")) {
                pstmt.setDate(126,null);
            } else {
                pstmt.setDate(126, Date.valueOf(this.get(i).getOrganizeDate()));
            }
            if(this.get(i).getOrganizeTime() == null || this.get(i).getOrganizeTime().equals("null")) {
            	pstmt.setString(127,null);
            } else {
            	pstmt.setString(127, this.get(i).getOrganizeTime());
            }
            if(this.get(i).getNewAutoSendBankFlag() == null || this.get(i).getNewAutoSendBankFlag().equals("null")) {
            	pstmt.setString(128,null);
            } else {
            	pstmt.setString(128, this.get(i).getNewAutoSendBankFlag());
            }
            if(this.get(i).getAgentCodeOper() == null || this.get(i).getAgentCodeOper().equals("null")) {
            	pstmt.setString(129,null);
            } else {
            	pstmt.setString(129, this.get(i).getAgentCodeOper());
            }
            if(this.get(i).getAgentCodeAssi() == null || this.get(i).getAgentCodeAssi().equals("null")) {
            	pstmt.setString(130,null);
            } else {
            	pstmt.setString(130, this.get(i).getAgentCodeAssi());
            }
            if(this.get(i).getDelayReasonCode() == null || this.get(i).getDelayReasonCode().equals("null")) {
            	pstmt.setString(131,null);
            } else {
            	pstmt.setString(131, this.get(i).getDelayReasonCode());
            }
            if(this.get(i).getDelayReasonDesc() == null || this.get(i).getDelayReasonDesc().equals("null")) {
            	pstmt.setString(132,null);
            } else {
            	pstmt.setString(132, this.get(i).getDelayReasonDesc());
            }
            if(this.get(i).getXQremindflag() == null || this.get(i).getXQremindflag().equals("null")) {
            	pstmt.setString(133,null);
            } else {
            	pstmt.setString(133, this.get(i).getXQremindflag());
            }
            if(this.get(i).getOrganComCode() == null || this.get(i).getOrganComCode().equals("null")) {
            	pstmt.setString(134,null);
            } else {
            	pstmt.setString(134, this.get(i).getOrganComCode());
            }
            if(this.get(i).getBankProivnce() == null || this.get(i).getBankProivnce().equals("null")) {
            	pstmt.setString(135,null);
            } else {
            	pstmt.setString(135, this.get(i).getBankProivnce());
            }
            if(this.get(i).getNewBankProivnce() == null || this.get(i).getNewBankProivnce().equals("null")) {
            	pstmt.setString(136,null);
            } else {
            	pstmt.setString(136, this.get(i).getNewBankProivnce());
            }
            if(this.get(i).getArbitrationCom() == null || this.get(i).getArbitrationCom().equals("null")) {
            	pstmt.setString(137,null);
            } else {
            	pstmt.setString(137, this.get(i).getArbitrationCom());
            }
            if(this.get(i).getOtherPrtno() == null || this.get(i).getOtherPrtno().equals("null")) {
            	pstmt.setString(138,null);
            } else {
            	pstmt.setString(138, this.get(i).getOtherPrtno());
            }
            if(this.get(i).getDestAccountDate() == null || this.get(i).getDestAccountDate().equals("null")) {
                pstmt.setDate(139,null);
            } else {
                pstmt.setDate(139, Date.valueOf(this.get(i).getDestAccountDate()));
            }
            if(this.get(i).getPrePayCount() == null || this.get(i).getPrePayCount().equals("null")) {
            	pstmt.setString(140,null);
            } else {
            	pstmt.setString(140, this.get(i).getPrePayCount());
            }
            if(this.get(i).getNewBankCity() == null || this.get(i).getNewBankCity().equals("null")) {
            	pstmt.setString(141,null);
            } else {
            	pstmt.setString(141, this.get(i).getNewBankCity());
            }
            if(this.get(i).getInsuredBankCode() == null || this.get(i).getInsuredBankCode().equals("null")) {
            	pstmt.setString(142,null);
            } else {
            	pstmt.setString(142, this.get(i).getInsuredBankCode());
            }
            if(this.get(i).getInsuredBankAccNo() == null || this.get(i).getInsuredBankAccNo().equals("null")) {
            	pstmt.setString(143,null);
            } else {
            	pstmt.setString(143, this.get(i).getInsuredBankAccNo());
            }
            if(this.get(i).getInsuredAccName() == null || this.get(i).getInsuredAccName().equals("null")) {
            	pstmt.setString(144,null);
            } else {
            	pstmt.setString(144, this.get(i).getInsuredAccName());
            }
            if(this.get(i).getInsuredBankProvince() == null || this.get(i).getInsuredBankProvince().equals("null")) {
            	pstmt.setString(145,null);
            } else {
            	pstmt.setString(145, this.get(i).getInsuredBankProvince());
            }
            if(this.get(i).getInsuredBankCity() == null || this.get(i).getInsuredBankCity().equals("null")) {
            	pstmt.setString(146,null);
            } else {
            	pstmt.setString(146, this.get(i).getInsuredBankCity());
            }
            if(this.get(i).getNewAccType() == null || this.get(i).getNewAccType().equals("null")) {
            	pstmt.setString(147,null);
            } else {
            	pstmt.setString(147, this.get(i).getNewAccType());
            }
            if(this.get(i).getAccType() == null || this.get(i).getAccType().equals("null")) {
            	pstmt.setString(148,null);
            } else {
            	pstmt.setString(148, this.get(i).getAccType());
            }
            if(this.get(i).getBankCity() == null || this.get(i).getBankCity().equals("null")) {
            	pstmt.setString(149,null);
            } else {
            	pstmt.setString(149, this.get(i).getBankCity());
            }
            pstmt.setDouble(150, this.get(i).getEstimateMoney());
            if(this.get(i).getInsuredAccType() == null || this.get(i).getInsuredAccType().equals("null")) {
            	pstmt.setString(151,null);
            } else {
            	pstmt.setString(151, this.get(i).getInsuredAccType());
            }
            if(this.get(i).getIsAllowedCharges() == null || this.get(i).getIsAllowedCharges().equals("null")) {
            	pstmt.setString(152,null);
            } else {
            	pstmt.setString(152, this.get(i).getIsAllowedCharges());
            }
            if(this.get(i).getWTEdorAcceptNo() == null || this.get(i).getWTEdorAcceptNo().equals("null")) {
            	pstmt.setString(153,null);
            } else {
            	pstmt.setString(153, this.get(i).getWTEdorAcceptNo());
            }
            if(this.get(i).getRecording() == null || this.get(i).getRecording().equals("null")) {
            	pstmt.setString(154,null);
            } else {
            	pstmt.setString(154, this.get(i).getRecording());
            }
            if(this.get(i).getSaleChannels() == null || this.get(i).getSaleChannels().equals("null")) {
            	pstmt.setString(155,null);
            } else {
            	pstmt.setString(155, this.get(i).getSaleChannels());
            }
            if(this.get(i).getZJAgentCom() == null || this.get(i).getZJAgentCom().equals("null")) {
            	pstmt.setString(156,null);
            } else {
            	pstmt.setString(156, this.get(i).getZJAgentCom());
            }
            if(this.get(i).getZJAgentComName() == null || this.get(i).getZJAgentComName().equals("null")) {
            	pstmt.setString(157,null);
            } else {
            	pstmt.setString(157, this.get(i).getZJAgentComName());
            }
            if(this.get(i).getRecordFlag() == null || this.get(i).getRecordFlag().equals("null")) {
            	pstmt.setString(158,null);
            } else {
            	pstmt.setString(158, this.get(i).getRecordFlag());
            }
            if(this.get(i).getPlanCode() == null || this.get(i).getPlanCode().equals("null")) {
            	pstmt.setString(159,null);
            } else {
            	pstmt.setString(159, this.get(i).getPlanCode());
            }
            if(this.get(i).getReceiptNo() == null || this.get(i).getReceiptNo().equals("null")) {
            	pstmt.setString(160,null);
            } else {
            	pstmt.setString(160, this.get(i).getReceiptNo());
            }
            if(this.get(i).getReissueCont() == null || this.get(i).getReissueCont().equals("null")) {
            	pstmt.setString(161,null);
            } else {
            	pstmt.setString(161, this.get(i).getReissueCont());
            }
            if(this.get(i).getDomainCode() == null || this.get(i).getDomainCode().equals("null")) {
            	pstmt.setString(162,null);
            } else {
            	pstmt.setString(162, this.get(i).getDomainCode());
            }
            if(this.get(i).getDomainName() == null || this.get(i).getDomainName().equals("null")) {
            	pstmt.setString(163,null);
            } else {
            	pstmt.setString(163, this.get(i).getDomainName());
            }
            if(this.get(i).getReferralNo() == null || this.get(i).getReferralNo().equals("null")) {
            	pstmt.setString(164,null);
            } else {
            	pstmt.setString(164, this.get(i).getReferralNo());
            }
            if(this.get(i).getThirdPartyOrderId() == null || this.get(i).getThirdPartyOrderId().equals("null")) {
            	pstmt.setString(165,null);
            } else {
            	pstmt.setString(165, this.get(i).getThirdPartyOrderId());
            }
            if(this.get(i).getUserId() == null || this.get(i).getUserId().equals("null")) {
            	pstmt.setString(166,null);
            } else {
            	pstmt.setString(166, this.get(i).getUserId());
            }
            // set where condition
            pstmt.setLong(167, this.get(i).getContID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCContDBSet";
            tError.functionName = "update()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }

    /**
     * 新增操作
     * @return boolean
     */
    public boolean insert() {
        PreparedStatement pstmt = null;

        if( !mflag ) {
            con = DBConnPool.getConnection();
        }

        try
        {
            int tCount = this.size();
            pstmt = con.prepareStatement("INSERT INTO LCCont VALUES( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");
            for (int i = 1; i <= tCount; i++) {
            pstmt.setLong(1, this.get(i).getContID());
            if(this.get(i).getShardingID() == null || this.get(i).getShardingID().equals("null")) {
            	pstmt.setString(2,null);
            } else {
            	pstmt.setString(2, this.get(i).getShardingID());
            }
            if(this.get(i).getGrpContNo() == null || this.get(i).getGrpContNo().equals("null")) {
            	pstmt.setString(3,null);
            } else {
            	pstmt.setString(3, this.get(i).getGrpContNo());
            }
            if(this.get(i).getContNo() == null || this.get(i).getContNo().equals("null")) {
            	pstmt.setString(4,null);
            } else {
            	pstmt.setString(4, this.get(i).getContNo());
            }
            if(this.get(i).getProposalContNo() == null || this.get(i).getProposalContNo().equals("null")) {
            	pstmt.setString(5,null);
            } else {
            	pstmt.setString(5, this.get(i).getProposalContNo());
            }
            if(this.get(i).getPrtNo() == null || this.get(i).getPrtNo().equals("null")) {
            	pstmt.setString(6,null);
            } else {
            	pstmt.setString(6, this.get(i).getPrtNo());
            }
            if(this.get(i).getContType() == null || this.get(i).getContType().equals("null")) {
            	pstmt.setString(7,null);
            } else {
            	pstmt.setString(7, this.get(i).getContType());
            }
            if(this.get(i).getFamilyType() == null || this.get(i).getFamilyType().equals("null")) {
            	pstmt.setString(8,null);
            } else {
            	pstmt.setString(8, this.get(i).getFamilyType());
            }
            if(this.get(i).getFamilyID() == null || this.get(i).getFamilyID().equals("null")) {
            	pstmt.setString(9,null);
            } else {
            	pstmt.setString(9, this.get(i).getFamilyID());
            }
            if(this.get(i).getPolType() == null || this.get(i).getPolType().equals("null")) {
            	pstmt.setString(10,null);
            } else {
            	pstmt.setString(10, this.get(i).getPolType());
            }
            if(this.get(i).getCardFlag() == null || this.get(i).getCardFlag().equals("null")) {
            	pstmt.setString(11,null);
            } else {
            	pstmt.setString(11, this.get(i).getCardFlag());
            }
            if(this.get(i).getManageCom() == null || this.get(i).getManageCom().equals("null")) {
            	pstmt.setString(12,null);
            } else {
            	pstmt.setString(12, this.get(i).getManageCom());
            }
            if(this.get(i).getExecuteCom() == null || this.get(i).getExecuteCom().equals("null")) {
            	pstmt.setString(13,null);
            } else {
            	pstmt.setString(13, this.get(i).getExecuteCom());
            }
            if(this.get(i).getAgentCom() == null || this.get(i).getAgentCom().equals("null")) {
            	pstmt.setString(14,null);
            } else {
            	pstmt.setString(14, this.get(i).getAgentCom());
            }
            if(this.get(i).getAgentCode() == null || this.get(i).getAgentCode().equals("null")) {
            	pstmt.setString(15,null);
            } else {
            	pstmt.setString(15, this.get(i).getAgentCode());
            }
            if(this.get(i).getAgentGroup() == null || this.get(i).getAgentGroup().equals("null")) {
            	pstmt.setString(16,null);
            } else {
            	pstmt.setString(16, this.get(i).getAgentGroup());
            }
            if(this.get(i).getAgentCode1() == null || this.get(i).getAgentCode1().equals("null")) {
            	pstmt.setString(17,null);
            } else {
            	pstmt.setString(17, this.get(i).getAgentCode1());
            }
            if(this.get(i).getAgentType() == null || this.get(i).getAgentType().equals("null")) {
            	pstmt.setString(18,null);
            } else {
            	pstmt.setString(18, this.get(i).getAgentType());
            }
            if(this.get(i).getSaleChnl() == null || this.get(i).getSaleChnl().equals("null")) {
            	pstmt.setString(19,null);
            } else {
            	pstmt.setString(19, this.get(i).getSaleChnl());
            }
            if(this.get(i).getHandler() == null || this.get(i).getHandler().equals("null")) {
            	pstmt.setString(20,null);
            } else {
            	pstmt.setString(20, this.get(i).getHandler());
            }
            if(this.get(i).getPassword() == null || this.get(i).getPassword().equals("null")) {
            	pstmt.setString(21,null);
            } else {
            	pstmt.setString(21, this.get(i).getPassword());
            }
            if(this.get(i).getAppntNo() == null || this.get(i).getAppntNo().equals("null")) {
            	pstmt.setString(22,null);
            } else {
            	pstmt.setString(22, this.get(i).getAppntNo());
            }
            if(this.get(i).getAppntName() == null || this.get(i).getAppntName().equals("null")) {
            	pstmt.setString(23,null);
            } else {
            	pstmt.setString(23, this.get(i).getAppntName());
            }
            if(this.get(i).getAppntSex() == null || this.get(i).getAppntSex().equals("null")) {
            	pstmt.setString(24,null);
            } else {
            	pstmt.setString(24, this.get(i).getAppntSex());
            }
            if(this.get(i).getAppntBirthday() == null || this.get(i).getAppntBirthday().equals("null")) {
                pstmt.setDate(25,null);
            } else {
                pstmt.setDate(25, Date.valueOf(this.get(i).getAppntBirthday()));
            }
            if(this.get(i).getAppntIDType() == null || this.get(i).getAppntIDType().equals("null")) {
            	pstmt.setString(26,null);
            } else {
            	pstmt.setString(26, this.get(i).getAppntIDType());
            }
            if(this.get(i).getAppntIDNo() == null || this.get(i).getAppntIDNo().equals("null")) {
            	pstmt.setString(27,null);
            } else {
            	pstmt.setString(27, this.get(i).getAppntIDNo());
            }
            if(this.get(i).getInsuredNo() == null || this.get(i).getInsuredNo().equals("null")) {
            	pstmt.setString(28,null);
            } else {
            	pstmt.setString(28, this.get(i).getInsuredNo());
            }
            if(this.get(i).getInsuredName() == null || this.get(i).getInsuredName().equals("null")) {
            	pstmt.setString(29,null);
            } else {
            	pstmt.setString(29, this.get(i).getInsuredName());
            }
            if(this.get(i).getInsuredSex() == null || this.get(i).getInsuredSex().equals("null")) {
            	pstmt.setString(30,null);
            } else {
            	pstmt.setString(30, this.get(i).getInsuredSex());
            }
            if(this.get(i).getInsuredBirthday() == null || this.get(i).getInsuredBirthday().equals("null")) {
                pstmt.setDate(31,null);
            } else {
                pstmt.setDate(31, Date.valueOf(this.get(i).getInsuredBirthday()));
            }
            if(this.get(i).getInsuredIDType() == null || this.get(i).getInsuredIDType().equals("null")) {
            	pstmt.setString(32,null);
            } else {
            	pstmt.setString(32, this.get(i).getInsuredIDType());
            }
            if(this.get(i).getInsuredIDNo() == null || this.get(i).getInsuredIDNo().equals("null")) {
            	pstmt.setString(33,null);
            } else {
            	pstmt.setString(33, this.get(i).getInsuredIDNo());
            }
            pstmt.setInt(34, this.get(i).getPayIntv());
            if(this.get(i).getPayMode() == null || this.get(i).getPayMode().equals("null")) {
            	pstmt.setString(35,null);
            } else {
            	pstmt.setString(35, this.get(i).getPayMode());
            }
            if(this.get(i).getPayLocation() == null || this.get(i).getPayLocation().equals("null")) {
            	pstmt.setString(36,null);
            } else {
            	pstmt.setString(36, this.get(i).getPayLocation());
            }
            if(this.get(i).getDisputedFlag() == null || this.get(i).getDisputedFlag().equals("null")) {
            	pstmt.setString(37,null);
            } else {
            	pstmt.setString(37, this.get(i).getDisputedFlag());
            }
            if(this.get(i).getOutPayFlag() == null || this.get(i).getOutPayFlag().equals("null")) {
            	pstmt.setString(38,null);
            } else {
            	pstmt.setString(38, this.get(i).getOutPayFlag());
            }
            if(this.get(i).getGetPolMode() == null || this.get(i).getGetPolMode().equals("null")) {
            	pstmt.setString(39,null);
            } else {
            	pstmt.setString(39, this.get(i).getGetPolMode());
            }
            if(this.get(i).getSignCom() == null || this.get(i).getSignCom().equals("null")) {
            	pstmt.setString(40,null);
            } else {
            	pstmt.setString(40, this.get(i).getSignCom());
            }
            if(this.get(i).getSignDate() == null || this.get(i).getSignDate().equals("null")) {
                pstmt.setDate(41,null);
            } else {
                pstmt.setDate(41, Date.valueOf(this.get(i).getSignDate()));
            }
            if(this.get(i).getSignTime() == null || this.get(i).getSignTime().equals("null")) {
            	pstmt.setString(42,null);
            } else {
            	pstmt.setString(42, this.get(i).getSignTime());
            }
            if(this.get(i).getConsignNo() == null || this.get(i).getConsignNo().equals("null")) {
            	pstmt.setString(43,null);
            } else {
            	pstmt.setString(43, this.get(i).getConsignNo());
            }
            if(this.get(i).getBankCode() == null || this.get(i).getBankCode().equals("null")) {
            	pstmt.setString(44,null);
            } else {
            	pstmt.setString(44, this.get(i).getBankCode());
            }
            if(this.get(i).getBankAccNo() == null || this.get(i).getBankAccNo().equals("null")) {
            	pstmt.setString(45,null);
            } else {
            	pstmt.setString(45, this.get(i).getBankAccNo());
            }
            if(this.get(i).getAccName() == null || this.get(i).getAccName().equals("null")) {
            	pstmt.setString(46,null);
            } else {
            	pstmt.setString(46, this.get(i).getAccName());
            }
            pstmt.setInt(47, this.get(i).getPrintCount());
            pstmt.setInt(48, this.get(i).getLostTimes());
            if(this.get(i).getLang() == null || this.get(i).getLang().equals("null")) {
            	pstmt.setString(49,null);
            } else {
            	pstmt.setString(49, this.get(i).getLang());
            }
            if(this.get(i).getCurrency() == null || this.get(i).getCurrency().equals("null")) {
            	pstmt.setString(50,null);
            } else {
            	pstmt.setString(50, this.get(i).getCurrency());
            }
            if(this.get(i).getRemark() == null || this.get(i).getRemark().equals("null")) {
            	pstmt.setString(51,null);
            } else {
            	pstmt.setString(51, this.get(i).getRemark());
            }
            pstmt.setInt(52, this.get(i).getPeoples());
            pstmt.setDouble(53, this.get(i).getMult());
            pstmt.setDouble(54, this.get(i).getPrem());
            pstmt.setDouble(55, this.get(i).getAmnt());
            pstmt.setDouble(56, this.get(i).getSumPrem());
            pstmt.setDouble(57, this.get(i).getDif());
            if(this.get(i).getPaytoDate() == null || this.get(i).getPaytoDate().equals("null")) {
                pstmt.setDate(58,null);
            } else {
                pstmt.setDate(58, Date.valueOf(this.get(i).getPaytoDate()));
            }
            if(this.get(i).getFirstPayDate() == null || this.get(i).getFirstPayDate().equals("null")) {
                pstmt.setDate(59,null);
            } else {
                pstmt.setDate(59, Date.valueOf(this.get(i).getFirstPayDate()));
            }
            if(this.get(i).getCValiDate() == null || this.get(i).getCValiDate().equals("null")) {
                pstmt.setDate(60,null);
            } else {
                pstmt.setDate(60, Date.valueOf(this.get(i).getCValiDate()));
            }
            if(this.get(i).getInputOperator() == null || this.get(i).getInputOperator().equals("null")) {
            	pstmt.setString(61,null);
            } else {
            	pstmt.setString(61, this.get(i).getInputOperator());
            }
            if(this.get(i).getInputDate() == null || this.get(i).getInputDate().equals("null")) {
                pstmt.setDate(62,null);
            } else {
                pstmt.setDate(62, Date.valueOf(this.get(i).getInputDate()));
            }
            if(this.get(i).getInputTime() == null || this.get(i).getInputTime().equals("null")) {
            	pstmt.setString(63,null);
            } else {
            	pstmt.setString(63, this.get(i).getInputTime());
            }
            if(this.get(i).getApproveFlag() == null || this.get(i).getApproveFlag().equals("null")) {
            	pstmt.setString(64,null);
            } else {
            	pstmt.setString(64, this.get(i).getApproveFlag());
            }
            if(this.get(i).getApproveCode() == null || this.get(i).getApproveCode().equals("null")) {
            	pstmt.setString(65,null);
            } else {
            	pstmt.setString(65, this.get(i).getApproveCode());
            }
            if(this.get(i).getApproveDate() == null || this.get(i).getApproveDate().equals("null")) {
                pstmt.setDate(66,null);
            } else {
                pstmt.setDate(66, Date.valueOf(this.get(i).getApproveDate()));
            }
            if(this.get(i).getApproveTime() == null || this.get(i).getApproveTime().equals("null")) {
            	pstmt.setString(67,null);
            } else {
            	pstmt.setString(67, this.get(i).getApproveTime());
            }
            if(this.get(i).getUWFlag() == null || this.get(i).getUWFlag().equals("null")) {
            	pstmt.setString(68,null);
            } else {
            	pstmt.setString(68, this.get(i).getUWFlag());
            }
            if(this.get(i).getUWOperator() == null || this.get(i).getUWOperator().equals("null")) {
            	pstmt.setString(69,null);
            } else {
            	pstmt.setString(69, this.get(i).getUWOperator());
            }
            if(this.get(i).getUWDate() == null || this.get(i).getUWDate().equals("null")) {
                pstmt.setDate(70,null);
            } else {
                pstmt.setDate(70, Date.valueOf(this.get(i).getUWDate()));
            }
            if(this.get(i).getUWTime() == null || this.get(i).getUWTime().equals("null")) {
            	pstmt.setString(71,null);
            } else {
            	pstmt.setString(71, this.get(i).getUWTime());
            }
            if(this.get(i).getAppFlag() == null || this.get(i).getAppFlag().equals("null")) {
            	pstmt.setString(72,null);
            } else {
            	pstmt.setString(72, this.get(i).getAppFlag());
            }
            if(this.get(i).getPolApplyDate() == null || this.get(i).getPolApplyDate().equals("null")) {
                pstmt.setDate(73,null);
            } else {
                pstmt.setDate(73, Date.valueOf(this.get(i).getPolApplyDate()));
            }
            if(this.get(i).getGetPolDate() == null || this.get(i).getGetPolDate().equals("null")) {
                pstmt.setDate(74,null);
            } else {
                pstmt.setDate(74, Date.valueOf(this.get(i).getGetPolDate()));
            }
            if(this.get(i).getGetPolTime() == null || this.get(i).getGetPolTime().equals("null")) {
            	pstmt.setString(75,null);
            } else {
            	pstmt.setString(75, this.get(i).getGetPolTime());
            }
            if(this.get(i).getCustomGetPolDate() == null || this.get(i).getCustomGetPolDate().equals("null")) {
                pstmt.setDate(76,null);
            } else {
                pstmt.setDate(76, Date.valueOf(this.get(i).getCustomGetPolDate()));
            }
            if(this.get(i).getState() == null || this.get(i).getState().equals("null")) {
            	pstmt.setString(77,null);
            } else {
            	pstmt.setString(77, this.get(i).getState());
            }
            if(this.get(i).getOperator() == null || this.get(i).getOperator().equals("null")) {
            	pstmt.setString(78,null);
            } else {
            	pstmt.setString(78, this.get(i).getOperator());
            }
            if(this.get(i).getMakeDate() == null || this.get(i).getMakeDate().equals("null")) {
                pstmt.setDate(79,null);
            } else {
                pstmt.setDate(79, Date.valueOf(this.get(i).getMakeDate()));
            }
            if(this.get(i).getMakeTime() == null || this.get(i).getMakeTime().equals("null")) {
            	pstmt.setString(80,null);
            } else {
            	pstmt.setString(80, this.get(i).getMakeTime());
            }
            if(this.get(i).getModifyDate() == null || this.get(i).getModifyDate().equals("null")) {
                pstmt.setDate(81,null);
            } else {
                pstmt.setDate(81, Date.valueOf(this.get(i).getModifyDate()));
            }
            if(this.get(i).getModifyTime() == null || this.get(i).getModifyTime().equals("null")) {
            	pstmt.setString(82,null);
            } else {
            	pstmt.setString(82, this.get(i).getModifyTime());
            }
            if(this.get(i).getFirstTrialOperator() == null || this.get(i).getFirstTrialOperator().equals("null")) {
            	pstmt.setString(83,null);
            } else {
            	pstmt.setString(83, this.get(i).getFirstTrialOperator());
            }
            if(this.get(i).getFirstTrialDate() == null || this.get(i).getFirstTrialDate().equals("null")) {
                pstmt.setDate(84,null);
            } else {
                pstmt.setDate(84, Date.valueOf(this.get(i).getFirstTrialDate()));
            }
            if(this.get(i).getFirstTrialTime() == null || this.get(i).getFirstTrialTime().equals("null")) {
            	pstmt.setString(85,null);
            } else {
            	pstmt.setString(85, this.get(i).getFirstTrialTime());
            }
            if(this.get(i).getReceiveOperator() == null || this.get(i).getReceiveOperator().equals("null")) {
            	pstmt.setString(86,null);
            } else {
            	pstmt.setString(86, this.get(i).getReceiveOperator());
            }
            if(this.get(i).getReceiveDate() == null || this.get(i).getReceiveDate().equals("null")) {
                pstmt.setDate(87,null);
            } else {
                pstmt.setDate(87, Date.valueOf(this.get(i).getReceiveDate()));
            }
            if(this.get(i).getReceiveTime() == null || this.get(i).getReceiveTime().equals("null")) {
            	pstmt.setString(88,null);
            } else {
            	pstmt.setString(88, this.get(i).getReceiveTime());
            }
            if(this.get(i).getTempFeeNo() == null || this.get(i).getTempFeeNo().equals("null")) {
            	pstmt.setString(89,null);
            } else {
            	pstmt.setString(89, this.get(i).getTempFeeNo());
            }
            if(this.get(i).getSellType() == null || this.get(i).getSellType().equals("null")) {
            	pstmt.setString(90,null);
            } else {
            	pstmt.setString(90, this.get(i).getSellType());
            }
            if(this.get(i).getForceUWFlag() == null || this.get(i).getForceUWFlag().equals("null")) {
            	pstmt.setString(91,null);
            } else {
            	pstmt.setString(91, this.get(i).getForceUWFlag());
            }
            if(this.get(i).getForceUWReason() == null || this.get(i).getForceUWReason().equals("null")) {
            	pstmt.setString(92,null);
            } else {
            	pstmt.setString(92, this.get(i).getForceUWReason());
            }
            if(this.get(i).getNewBankCode() == null || this.get(i).getNewBankCode().equals("null")) {
            	pstmt.setString(93,null);
            } else {
            	pstmt.setString(93, this.get(i).getNewBankCode());
            }
            if(this.get(i).getNewBankAccNo() == null || this.get(i).getNewBankAccNo().equals("null")) {
            	pstmt.setString(94,null);
            } else {
            	pstmt.setString(94, this.get(i).getNewBankAccNo());
            }
            if(this.get(i).getNewAccName() == null || this.get(i).getNewAccName().equals("null")) {
            	pstmt.setString(95,null);
            } else {
            	pstmt.setString(95, this.get(i).getNewAccName());
            }
            if(this.get(i).getNewPayMode() == null || this.get(i).getNewPayMode().equals("null")) {
            	pstmt.setString(96,null);
            } else {
            	pstmt.setString(96, this.get(i).getNewPayMode());
            }
            if(this.get(i).getAgentBankCode() == null || this.get(i).getAgentBankCode().equals("null")) {
            	pstmt.setString(97,null);
            } else {
            	pstmt.setString(97, this.get(i).getAgentBankCode());
            }
            if(this.get(i).getBankAgent() == null || this.get(i).getBankAgent().equals("null")) {
            	pstmt.setString(98,null);
            } else {
            	pstmt.setString(98, this.get(i).getBankAgent());
            }
            if(this.get(i).getBankAgentName() == null || this.get(i).getBankAgentName().equals("null")) {
            	pstmt.setString(99,null);
            } else {
            	pstmt.setString(99, this.get(i).getBankAgentName());
            }
            if(this.get(i).getBankAgentTel() == null || this.get(i).getBankAgentTel().equals("null")) {
            	pstmt.setString(100,null);
            } else {
            	pstmt.setString(100, this.get(i).getBankAgentTel());
            }
            if(this.get(i).getProdSetCode() == null || this.get(i).getProdSetCode().equals("null")) {
            	pstmt.setString(101,null);
            } else {
            	pstmt.setString(101, this.get(i).getProdSetCode());
            }
            if(this.get(i).getPolicyNo() == null || this.get(i).getPolicyNo().equals("null")) {
            	pstmt.setString(102,null);
            } else {
            	pstmt.setString(102, this.get(i).getPolicyNo());
            }
            if(this.get(i).getBillPressNo() == null || this.get(i).getBillPressNo().equals("null")) {
            	pstmt.setString(103,null);
            } else {
            	pstmt.setString(103, this.get(i).getBillPressNo());
            }
            if(this.get(i).getCardTypeCode() == null || this.get(i).getCardTypeCode().equals("null")) {
            	pstmt.setString(104,null);
            } else {
            	pstmt.setString(104, this.get(i).getCardTypeCode());
            }
            if(this.get(i).getVisitDate() == null || this.get(i).getVisitDate().equals("null")) {
                pstmt.setDate(105,null);
            } else {
                pstmt.setDate(105, Date.valueOf(this.get(i).getVisitDate()));
            }
            if(this.get(i).getVisitTime() == null || this.get(i).getVisitTime().equals("null")) {
            	pstmt.setString(106,null);
            } else {
            	pstmt.setString(106, this.get(i).getVisitTime());
            }
            if(this.get(i).getSaleCom() == null || this.get(i).getSaleCom().equals("null")) {
            	pstmt.setString(107,null);
            } else {
            	pstmt.setString(107, this.get(i).getSaleCom());
            }
            if(this.get(i).getPrintFlag() == null || this.get(i).getPrintFlag().equals("null")) {
            	pstmt.setString(108,null);
            } else {
            	pstmt.setString(108, this.get(i).getPrintFlag());
            }
            if(this.get(i).getInvoicePrtFlag() == null || this.get(i).getInvoicePrtFlag().equals("null")) {
            	pstmt.setString(109,null);
            } else {
            	pstmt.setString(109, this.get(i).getInvoicePrtFlag());
            }
            if(this.get(i).getNewReinsureFlag() == null || this.get(i).getNewReinsureFlag().equals("null")) {
            	pstmt.setString(110,null);
            } else {
            	pstmt.setString(110, this.get(i).getNewReinsureFlag());
            }
            if(this.get(i).getRenewPayFlag() == null || this.get(i).getRenewPayFlag().equals("null")) {
            	pstmt.setString(111,null);
            } else {
            	pstmt.setString(111, this.get(i).getRenewPayFlag());
            }
            if(this.get(i).getAppntFirstName() == null || this.get(i).getAppntFirstName().equals("null")) {
            	pstmt.setString(112,null);
            } else {
            	pstmt.setString(112, this.get(i).getAppntFirstName());
            }
            if(this.get(i).getAppntLastName() == null || this.get(i).getAppntLastName().equals("null")) {
            	pstmt.setString(113,null);
            } else {
            	pstmt.setString(113, this.get(i).getAppntLastName());
            }
            if(this.get(i).getInsuredFirstName() == null || this.get(i).getInsuredFirstName().equals("null")) {
            	pstmt.setString(114,null);
            } else {
            	pstmt.setString(114, this.get(i).getInsuredFirstName());
            }
            if(this.get(i).getInsuredLastName() == null || this.get(i).getInsuredLastName().equals("null")) {
            	pstmt.setString(115,null);
            } else {
            	pstmt.setString(115, this.get(i).getInsuredLastName());
            }
            if(this.get(i).getAuthorFlag() == null || this.get(i).getAuthorFlag().equals("null")) {
            	pstmt.setString(116,null);
            } else {
            	pstmt.setString(116, this.get(i).getAuthorFlag());
            }
            pstmt.setInt(117, this.get(i).getGreenChnl());
            if(this.get(i).getTBType() == null || this.get(i).getTBType().equals("null")) {
            	pstmt.setString(118,null);
            } else {
            	pstmt.setString(118, this.get(i).getTBType());
            }
            if(this.get(i).getEAuto() == null || this.get(i).getEAuto().equals("null")) {
            	pstmt.setString(119,null);
            } else {
            	pstmt.setString(119, this.get(i).getEAuto());
            }
            if(this.get(i).getSlipForm() == null || this.get(i).getSlipForm().equals("null")) {
            	pstmt.setString(120,null);
            } else {
            	pstmt.setString(120, this.get(i).getSlipForm());
            }
            if(this.get(i).getAutoPayFlag() == null || this.get(i).getAutoPayFlag().equals("null")) {
            	pstmt.setString(121,null);
            } else {
            	pstmt.setString(121, this.get(i).getAutoPayFlag());
            }
            pstmt.setInt(122, this.get(i).getRnewFlag());
            if(this.get(i).getFamilyContNo() == null || this.get(i).getFamilyContNo().equals("null")) {
            	pstmt.setString(123,null);
            } else {
            	pstmt.setString(123, this.get(i).getFamilyContNo());
            }
            if(this.get(i).getBussFlag() == null || this.get(i).getBussFlag().equals("null")) {
            	pstmt.setString(124,null);
            } else {
            	pstmt.setString(124, this.get(i).getBussFlag());
            }
            if(this.get(i).getSignName() == null || this.get(i).getSignName().equals("null")) {
            	pstmt.setString(125,null);
            } else {
            	pstmt.setString(125, this.get(i).getSignName());
            }
            if(this.get(i).getOrganizeDate() == null || this.get(i).getOrganizeDate().equals("null")) {
                pstmt.setDate(126,null);
            } else {
                pstmt.setDate(126, Date.valueOf(this.get(i).getOrganizeDate()));
            }
            if(this.get(i).getOrganizeTime() == null || this.get(i).getOrganizeTime().equals("null")) {
            	pstmt.setString(127,null);
            } else {
            	pstmt.setString(127, this.get(i).getOrganizeTime());
            }
            if(this.get(i).getNewAutoSendBankFlag() == null || this.get(i).getNewAutoSendBankFlag().equals("null")) {
            	pstmt.setString(128,null);
            } else {
            	pstmt.setString(128, this.get(i).getNewAutoSendBankFlag());
            }
            if(this.get(i).getAgentCodeOper() == null || this.get(i).getAgentCodeOper().equals("null")) {
            	pstmt.setString(129,null);
            } else {
            	pstmt.setString(129, this.get(i).getAgentCodeOper());
            }
            if(this.get(i).getAgentCodeAssi() == null || this.get(i).getAgentCodeAssi().equals("null")) {
            	pstmt.setString(130,null);
            } else {
            	pstmt.setString(130, this.get(i).getAgentCodeAssi());
            }
            if(this.get(i).getDelayReasonCode() == null || this.get(i).getDelayReasonCode().equals("null")) {
            	pstmt.setString(131,null);
            } else {
            	pstmt.setString(131, this.get(i).getDelayReasonCode());
            }
            if(this.get(i).getDelayReasonDesc() == null || this.get(i).getDelayReasonDesc().equals("null")) {
            	pstmt.setString(132,null);
            } else {
            	pstmt.setString(132, this.get(i).getDelayReasonDesc());
            }
            if(this.get(i).getXQremindflag() == null || this.get(i).getXQremindflag().equals("null")) {
            	pstmt.setString(133,null);
            } else {
            	pstmt.setString(133, this.get(i).getXQremindflag());
            }
            if(this.get(i).getOrganComCode() == null || this.get(i).getOrganComCode().equals("null")) {
            	pstmt.setString(134,null);
            } else {
            	pstmt.setString(134, this.get(i).getOrganComCode());
            }
            if(this.get(i).getBankProivnce() == null || this.get(i).getBankProivnce().equals("null")) {
            	pstmt.setString(135,null);
            } else {
            	pstmt.setString(135, this.get(i).getBankProivnce());
            }
            if(this.get(i).getNewBankProivnce() == null || this.get(i).getNewBankProivnce().equals("null")) {
            	pstmt.setString(136,null);
            } else {
            	pstmt.setString(136, this.get(i).getNewBankProivnce());
            }
            if(this.get(i).getArbitrationCom() == null || this.get(i).getArbitrationCom().equals("null")) {
            	pstmt.setString(137,null);
            } else {
            	pstmt.setString(137, this.get(i).getArbitrationCom());
            }
            if(this.get(i).getOtherPrtno() == null || this.get(i).getOtherPrtno().equals("null")) {
            	pstmt.setString(138,null);
            } else {
            	pstmt.setString(138, this.get(i).getOtherPrtno());
            }
            if(this.get(i).getDestAccountDate() == null || this.get(i).getDestAccountDate().equals("null")) {
                pstmt.setDate(139,null);
            } else {
                pstmt.setDate(139, Date.valueOf(this.get(i).getDestAccountDate()));
            }
            if(this.get(i).getPrePayCount() == null || this.get(i).getPrePayCount().equals("null")) {
            	pstmt.setString(140,null);
            } else {
            	pstmt.setString(140, this.get(i).getPrePayCount());
            }
            if(this.get(i).getNewBankCity() == null || this.get(i).getNewBankCity().equals("null")) {
            	pstmt.setString(141,null);
            } else {
            	pstmt.setString(141, this.get(i).getNewBankCity());
            }
            if(this.get(i).getInsuredBankCode() == null || this.get(i).getInsuredBankCode().equals("null")) {
            	pstmt.setString(142,null);
            } else {
            	pstmt.setString(142, this.get(i).getInsuredBankCode());
            }
            if(this.get(i).getInsuredBankAccNo() == null || this.get(i).getInsuredBankAccNo().equals("null")) {
            	pstmt.setString(143,null);
            } else {
            	pstmt.setString(143, this.get(i).getInsuredBankAccNo());
            }
            if(this.get(i).getInsuredAccName() == null || this.get(i).getInsuredAccName().equals("null")) {
            	pstmt.setString(144,null);
            } else {
            	pstmt.setString(144, this.get(i).getInsuredAccName());
            }
            if(this.get(i).getInsuredBankProvince() == null || this.get(i).getInsuredBankProvince().equals("null")) {
            	pstmt.setString(145,null);
            } else {
            	pstmt.setString(145, this.get(i).getInsuredBankProvince());
            }
            if(this.get(i).getInsuredBankCity() == null || this.get(i).getInsuredBankCity().equals("null")) {
            	pstmt.setString(146,null);
            } else {
            	pstmt.setString(146, this.get(i).getInsuredBankCity());
            }
            if(this.get(i).getNewAccType() == null || this.get(i).getNewAccType().equals("null")) {
            	pstmt.setString(147,null);
            } else {
            	pstmt.setString(147, this.get(i).getNewAccType());
            }
            if(this.get(i).getAccType() == null || this.get(i).getAccType().equals("null")) {
            	pstmt.setString(148,null);
            } else {
            	pstmt.setString(148, this.get(i).getAccType());
            }
            if(this.get(i).getBankCity() == null || this.get(i).getBankCity().equals("null")) {
            	pstmt.setString(149,null);
            } else {
            	pstmt.setString(149, this.get(i).getBankCity());
            }
            pstmt.setDouble(150, this.get(i).getEstimateMoney());
            if(this.get(i).getInsuredAccType() == null || this.get(i).getInsuredAccType().equals("null")) {
            	pstmt.setString(151,null);
            } else {
            	pstmt.setString(151, this.get(i).getInsuredAccType());
            }
            if(this.get(i).getIsAllowedCharges() == null || this.get(i).getIsAllowedCharges().equals("null")) {
            	pstmt.setString(152,null);
            } else {
            	pstmt.setString(152, this.get(i).getIsAllowedCharges());
            }
            if(this.get(i).getWTEdorAcceptNo() == null || this.get(i).getWTEdorAcceptNo().equals("null")) {
            	pstmt.setString(153,null);
            } else {
            	pstmt.setString(153, this.get(i).getWTEdorAcceptNo());
            }
            if(this.get(i).getRecording() == null || this.get(i).getRecording().equals("null")) {
            	pstmt.setString(154,null);
            } else {
            	pstmt.setString(154, this.get(i).getRecording());
            }
            if(this.get(i).getSaleChannels() == null || this.get(i).getSaleChannels().equals("null")) {
            	pstmt.setString(155,null);
            } else {
            	pstmt.setString(155, this.get(i).getSaleChannels());
            }
            if(this.get(i).getZJAgentCom() == null || this.get(i).getZJAgentCom().equals("null")) {
            	pstmt.setString(156,null);
            } else {
            	pstmt.setString(156, this.get(i).getZJAgentCom());
            }
            if(this.get(i).getZJAgentComName() == null || this.get(i).getZJAgentComName().equals("null")) {
            	pstmt.setString(157,null);
            } else {
            	pstmt.setString(157, this.get(i).getZJAgentComName());
            }
            if(this.get(i).getRecordFlag() == null || this.get(i).getRecordFlag().equals("null")) {
            	pstmt.setString(158,null);
            } else {
            	pstmt.setString(158, this.get(i).getRecordFlag());
            }
            if(this.get(i).getPlanCode() == null || this.get(i).getPlanCode().equals("null")) {
            	pstmt.setString(159,null);
            } else {
            	pstmt.setString(159, this.get(i).getPlanCode());
            }
            if(this.get(i).getReceiptNo() == null || this.get(i).getReceiptNo().equals("null")) {
            	pstmt.setString(160,null);
            } else {
            	pstmt.setString(160, this.get(i).getReceiptNo());
            }
            if(this.get(i).getReissueCont() == null || this.get(i).getReissueCont().equals("null")) {
            	pstmt.setString(161,null);
            } else {
            	pstmt.setString(161, this.get(i).getReissueCont());
            }
            if(this.get(i).getDomainCode() == null || this.get(i).getDomainCode().equals("null")) {
            	pstmt.setString(162,null);
            } else {
            	pstmt.setString(162, this.get(i).getDomainCode());
            }
            if(this.get(i).getDomainName() == null || this.get(i).getDomainName().equals("null")) {
            	pstmt.setString(163,null);
            } else {
            	pstmt.setString(163, this.get(i).getDomainName());
            }
            if(this.get(i).getReferralNo() == null || this.get(i).getReferralNo().equals("null")) {
            	pstmt.setString(164,null);
            } else {
            	pstmt.setString(164, this.get(i).getReferralNo());
            }
            if(this.get(i).getThirdPartyOrderId() == null || this.get(i).getThirdPartyOrderId().equals("null")) {
            	pstmt.setString(165,null);
            } else {
            	pstmt.setString(165, this.get(i).getThirdPartyOrderId());
            }
            if(this.get(i).getUserId() == null || this.get(i).getUserId().equals("null")) {
            	pstmt.setString(166,null);
            } else {
            	pstmt.setString(166, this.get(i).getUserId());
            }
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception ex) {
            // @@错误处理
            ex.printStackTrace();
            this.mErrors.copyAllErrors(db.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCContDBSet";
            tError.functionName = "insert()";
            tError.errorMessage = ExceptionUtils.exceptionToString(ex);
            this.mErrors .addOneError(tError);

            try {
                pstmt.close();
            } catch (Exception e){e.printStackTrace();}

            if( !mflag ) {
                try {
                    con.close();
                } catch (Exception e){e.printStackTrace();}
            }

            return false;
        }

        if( !mflag ) {
            try {
                con.close();
            } catch (Exception e){e.printStackTrace();}
        }

        return true;
    }
}
