package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class AgentComInfo implements Serializable {

    private String agentCom;

    private String agentName;

    private String address;

    private String operator;

    private String operatorName;

    public String getAgentCom() {
        return agentCom;
    }

    public void setAgentCom(String agentCom) {
        this.agentCom = agentCom;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    @Override
    public String toString() {
        return "AgentComInfo{" +
                "agentCom='" + agentCom + '\'' +
                ", agentName='" + agentName + '\'' +
                ", address='" + address + '\'' +
                ", operator='" + operator + '\'' +
                ", operatorName='" + operatorName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AgentComInfo that = (AgentComInfo) o;
        return Objects.equals(agentCom, that.agentCom) &&
                Objects.equals(agentName, that.agentName) &&
                Objects.equals(address, that.address) &&
                Objects.equals(operator, that.operator) &&
                Objects.equals(operatorName, that.operatorName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agentCom, agentName, address, operator, operatorName);
    }
}
