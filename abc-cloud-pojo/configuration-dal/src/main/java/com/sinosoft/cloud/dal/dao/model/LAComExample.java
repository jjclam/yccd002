package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class LAComExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public LAComExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andAGENTCOMIsNull() {
            addCriterion("AGENTCOM is null");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMIsNotNull() {
            addCriterion("AGENTCOM is not null");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMEqualTo(String value) {
            addCriterion("AGENTCOM =", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMNotEqualTo(String value) {
            addCriterion("AGENTCOM <>", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMGreaterThan(String value) {
            addCriterion("AGENTCOM >", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTCOM >=", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMLessThan(String value) {
            addCriterion("AGENTCOM <", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMLessThanOrEqualTo(String value) {
            addCriterion("AGENTCOM <=", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMLike(String value) {
            addCriterion("AGENTCOM like", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMNotLike(String value) {
            addCriterion("AGENTCOM not like", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMIn(List<String> values) {
            addCriterion("AGENTCOM in", values, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMNotIn(List<String> values) {
            addCriterion("AGENTCOM not in", values, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMBetween(String value1, String value2) {
            addCriterion("AGENTCOM between", value1, value2, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMNotBetween(String value1, String value2) {
            addCriterion("AGENTCOM not between", value1, value2, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMIsNull() {
            addCriterion("MANAGECOM is null");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMIsNotNull() {
            addCriterion("MANAGECOM is not null");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMEqualTo(String value) {
            addCriterion("MANAGECOM =", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMNotEqualTo(String value) {
            addCriterion("MANAGECOM <>", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMGreaterThan(String value) {
            addCriterion("MANAGECOM >", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMGreaterThanOrEqualTo(String value) {
            addCriterion("MANAGECOM >=", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMLessThan(String value) {
            addCriterion("MANAGECOM <", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMLessThanOrEqualTo(String value) {
            addCriterion("MANAGECOM <=", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMLike(String value) {
            addCriterion("MANAGECOM like", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMNotLike(String value) {
            addCriterion("MANAGECOM not like", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMIn(List<String> values) {
            addCriterion("MANAGECOM in", values, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMNotIn(List<String> values) {
            addCriterion("MANAGECOM not in", values, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMBetween(String value1, String value2) {
            addCriterion("MANAGECOM between", value1, value2, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMNotBetween(String value1, String value2) {
            addCriterion("MANAGECOM not between", value1, value2, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andAREATYPEIsNull() {
            addCriterion("AREATYPE is null");
            return (Criteria) this;
        }

        public Criteria andAREATYPEIsNotNull() {
            addCriterion("AREATYPE is not null");
            return (Criteria) this;
        }

        public Criteria andAREATYPEEqualTo(String value) {
            addCriterion("AREATYPE =", value, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPENotEqualTo(String value) {
            addCriterion("AREATYPE <>", value, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPEGreaterThan(String value) {
            addCriterion("AREATYPE >", value, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPEGreaterThanOrEqualTo(String value) {
            addCriterion("AREATYPE >=", value, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPELessThan(String value) {
            addCriterion("AREATYPE <", value, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPELessThanOrEqualTo(String value) {
            addCriterion("AREATYPE <=", value, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPELike(String value) {
            addCriterion("AREATYPE like", value, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPENotLike(String value) {
            addCriterion("AREATYPE not like", value, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPEIn(List<String> values) {
            addCriterion("AREATYPE in", values, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPENotIn(List<String> values) {
            addCriterion("AREATYPE not in", values, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPEBetween(String value1, String value2) {
            addCriterion("AREATYPE between", value1, value2, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andAREATYPENotBetween(String value1, String value2) {
            addCriterion("AREATYPE not between", value1, value2, "AREATYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPEIsNull() {
            addCriterion("CHANNELTYPE is null");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPEIsNotNull() {
            addCriterion("CHANNELTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPEEqualTo(String value) {
            addCriterion("CHANNELTYPE =", value, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPENotEqualTo(String value) {
            addCriterion("CHANNELTYPE <>", value, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPEGreaterThan(String value) {
            addCriterion("CHANNELTYPE >", value, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("CHANNELTYPE >=", value, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPELessThan(String value) {
            addCriterion("CHANNELTYPE <", value, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPELessThanOrEqualTo(String value) {
            addCriterion("CHANNELTYPE <=", value, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPELike(String value) {
            addCriterion("CHANNELTYPE like", value, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPENotLike(String value) {
            addCriterion("CHANNELTYPE not like", value, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPEIn(List<String> values) {
            addCriterion("CHANNELTYPE in", values, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPENotIn(List<String> values) {
            addCriterion("CHANNELTYPE not in", values, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPEBetween(String value1, String value2) {
            addCriterion("CHANNELTYPE between", value1, value2, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPENotBetween(String value1, String value2) {
            addCriterion("CHANNELTYPE not between", value1, value2, "CHANNELTYPE");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMIsNull() {
            addCriterion("UPAGENTCOM is null");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMIsNotNull() {
            addCriterion("UPAGENTCOM is not null");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMEqualTo(String value) {
            addCriterion("UPAGENTCOM =", value, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMNotEqualTo(String value) {
            addCriterion("UPAGENTCOM <>", value, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMGreaterThan(String value) {
            addCriterion("UPAGENTCOM >", value, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMGreaterThanOrEqualTo(String value) {
            addCriterion("UPAGENTCOM >=", value, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMLessThan(String value) {
            addCriterion("UPAGENTCOM <", value, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMLessThanOrEqualTo(String value) {
            addCriterion("UPAGENTCOM <=", value, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMLike(String value) {
            addCriterion("UPAGENTCOM like", value, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMNotLike(String value) {
            addCriterion("UPAGENTCOM not like", value, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMIn(List<String> values) {
            addCriterion("UPAGENTCOM in", values, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMNotIn(List<String> values) {
            addCriterion("UPAGENTCOM not in", values, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMBetween(String value1, String value2) {
            addCriterion("UPAGENTCOM between", value1, value2, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andUPAGENTCOMNotBetween(String value1, String value2) {
            addCriterion("UPAGENTCOM not between", value1, value2, "UPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andNAMEIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNAMEIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNAMEEqualTo(String value) {
            addCriterion("NAME =", value, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMENotEqualTo(String value) {
            addCriterion("NAME <>", value, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMEGreaterThan(String value) {
            addCriterion("NAME >", value, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMELessThan(String value) {
            addCriterion("NAME <", value, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMELessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMELike(String value) {
            addCriterion("NAME like", value, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMENotLike(String value) {
            addCriterion("NAME not like", value, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMEIn(List<String> values) {
            addCriterion("NAME in", values, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMENotIn(List<String> values) {
            addCriterion("NAME not in", values, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMEBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "NAME");
            return (Criteria) this;
        }

        public Criteria andNAMENotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "NAME");
            return (Criteria) this;
        }

        public Criteria andADDRESSIsNull() {
            addCriterion("ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andADDRESSIsNotNull() {
            addCriterion("ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andADDRESSEqualTo(String value) {
            addCriterion("ADDRESS =", value, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSNotEqualTo(String value) {
            addCriterion("ADDRESS <>", value, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSGreaterThan(String value) {
            addCriterion("ADDRESS >", value, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSGreaterThanOrEqualTo(String value) {
            addCriterion("ADDRESS >=", value, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSLessThan(String value) {
            addCriterion("ADDRESS <", value, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSLessThanOrEqualTo(String value) {
            addCriterion("ADDRESS <=", value, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSLike(String value) {
            addCriterion("ADDRESS like", value, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSNotLike(String value) {
            addCriterion("ADDRESS not like", value, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSIn(List<String> values) {
            addCriterion("ADDRESS in", values, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSNotIn(List<String> values) {
            addCriterion("ADDRESS not in", values, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSBetween(String value1, String value2) {
            addCriterion("ADDRESS between", value1, value2, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andADDRESSNotBetween(String value1, String value2) {
            addCriterion("ADDRESS not between", value1, value2, "ADDRESS");
            return (Criteria) this;
        }

        public Criteria andZIPCODEIsNull() {
            addCriterion("ZIPCODE is null");
            return (Criteria) this;
        }

        public Criteria andZIPCODEIsNotNull() {
            addCriterion("ZIPCODE is not null");
            return (Criteria) this;
        }

        public Criteria andZIPCODEEqualTo(String value) {
            addCriterion("ZIPCODE =", value, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODENotEqualTo(String value) {
            addCriterion("ZIPCODE <>", value, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODEGreaterThan(String value) {
            addCriterion("ZIPCODE >", value, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODEGreaterThanOrEqualTo(String value) {
            addCriterion("ZIPCODE >=", value, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODELessThan(String value) {
            addCriterion("ZIPCODE <", value, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODELessThanOrEqualTo(String value) {
            addCriterion("ZIPCODE <=", value, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODELike(String value) {
            addCriterion("ZIPCODE like", value, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODENotLike(String value) {
            addCriterion("ZIPCODE not like", value, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODEIn(List<String> values) {
            addCriterion("ZIPCODE in", values, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODENotIn(List<String> values) {
            addCriterion("ZIPCODE not in", values, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODEBetween(String value1, String value2) {
            addCriterion("ZIPCODE between", value1, value2, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andZIPCODENotBetween(String value1, String value2) {
            addCriterion("ZIPCODE not between", value1, value2, "ZIPCODE");
            return (Criteria) this;
        }

        public Criteria andPHONEIsNull() {
            addCriterion("PHONE is null");
            return (Criteria) this;
        }

        public Criteria andPHONEIsNotNull() {
            addCriterion("PHONE is not null");
            return (Criteria) this;
        }

        public Criteria andPHONEEqualTo(String value) {
            addCriterion("PHONE =", value, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONENotEqualTo(String value) {
            addCriterion("PHONE <>", value, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONEGreaterThan(String value) {
            addCriterion("PHONE >", value, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONEGreaterThanOrEqualTo(String value) {
            addCriterion("PHONE >=", value, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONELessThan(String value) {
            addCriterion("PHONE <", value, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONELessThanOrEqualTo(String value) {
            addCriterion("PHONE <=", value, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONELike(String value) {
            addCriterion("PHONE like", value, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONENotLike(String value) {
            addCriterion("PHONE not like", value, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONEIn(List<String> values) {
            addCriterion("PHONE in", values, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONENotIn(List<String> values) {
            addCriterion("PHONE not in", values, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONEBetween(String value1, String value2) {
            addCriterion("PHONE between", value1, value2, "PHONE");
            return (Criteria) this;
        }

        public Criteria andPHONENotBetween(String value1, String value2) {
            addCriterion("PHONE not between", value1, value2, "PHONE");
            return (Criteria) this;
        }

        public Criteria andFAXIsNull() {
            addCriterion("FAX is null");
            return (Criteria) this;
        }

        public Criteria andFAXIsNotNull() {
            addCriterion("FAX is not null");
            return (Criteria) this;
        }

        public Criteria andFAXEqualTo(String value) {
            addCriterion("FAX =", value, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXNotEqualTo(String value) {
            addCriterion("FAX <>", value, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXGreaterThan(String value) {
            addCriterion("FAX >", value, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXGreaterThanOrEqualTo(String value) {
            addCriterion("FAX >=", value, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXLessThan(String value) {
            addCriterion("FAX <", value, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXLessThanOrEqualTo(String value) {
            addCriterion("FAX <=", value, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXLike(String value) {
            addCriterion("FAX like", value, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXNotLike(String value) {
            addCriterion("FAX not like", value, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXIn(List<String> values) {
            addCriterion("FAX in", values, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXNotIn(List<String> values) {
            addCriterion("FAX not in", values, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXBetween(String value1, String value2) {
            addCriterion("FAX between", value1, value2, "FAX");
            return (Criteria) this;
        }

        public Criteria andFAXNotBetween(String value1, String value2) {
            addCriterion("FAX not between", value1, value2, "FAX");
            return (Criteria) this;
        }

        public Criteria andEMAILIsNull() {
            addCriterion("EMAIL is null");
            return (Criteria) this;
        }

        public Criteria andEMAILIsNotNull() {
            addCriterion("EMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andEMAILEqualTo(String value) {
            addCriterion("EMAIL =", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILNotEqualTo(String value) {
            addCriterion("EMAIL <>", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILGreaterThan(String value) {
            addCriterion("EMAIL >", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL >=", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILLessThan(String value) {
            addCriterion("EMAIL <", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILLessThanOrEqualTo(String value) {
            addCriterion("EMAIL <=", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILLike(String value) {
            addCriterion("EMAIL like", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILNotLike(String value) {
            addCriterion("EMAIL not like", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILIn(List<String> values) {
            addCriterion("EMAIL in", values, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILNotIn(List<String> values) {
            addCriterion("EMAIL not in", values, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILBetween(String value1, String value2) {
            addCriterion("EMAIL between", value1, value2, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILNotBetween(String value1, String value2) {
            addCriterion("EMAIL not between", value1, value2, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSIsNull() {
            addCriterion("WEBADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSIsNotNull() {
            addCriterion("WEBADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSEqualTo(String value) {
            addCriterion("WEBADDRESS =", value, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSNotEqualTo(String value) {
            addCriterion("WEBADDRESS <>", value, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSGreaterThan(String value) {
            addCriterion("WEBADDRESS >", value, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSGreaterThanOrEqualTo(String value) {
            addCriterion("WEBADDRESS >=", value, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSLessThan(String value) {
            addCriterion("WEBADDRESS <", value, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSLessThanOrEqualTo(String value) {
            addCriterion("WEBADDRESS <=", value, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSLike(String value) {
            addCriterion("WEBADDRESS like", value, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSNotLike(String value) {
            addCriterion("WEBADDRESS not like", value, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSIn(List<String> values) {
            addCriterion("WEBADDRESS in", values, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSNotIn(List<String> values) {
            addCriterion("WEBADDRESS not in", values, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSBetween(String value1, String value2) {
            addCriterion("WEBADDRESS between", value1, value2, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andWEBADDRESSNotBetween(String value1, String value2) {
            addCriterion("WEBADDRESS not between", value1, value2, "WEBADDRESS");
            return (Criteria) this;
        }

        public Criteria andLINKMANIsNull() {
            addCriterion("LINKMAN is null");
            return (Criteria) this;
        }

        public Criteria andLINKMANIsNotNull() {
            addCriterion("LINKMAN is not null");
            return (Criteria) this;
        }

        public Criteria andLINKMANEqualTo(String value) {
            addCriterion("LINKMAN =", value, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANNotEqualTo(String value) {
            addCriterion("LINKMAN <>", value, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANGreaterThan(String value) {
            addCriterion("LINKMAN >", value, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANGreaterThanOrEqualTo(String value) {
            addCriterion("LINKMAN >=", value, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANLessThan(String value) {
            addCriterion("LINKMAN <", value, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANLessThanOrEqualTo(String value) {
            addCriterion("LINKMAN <=", value, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANLike(String value) {
            addCriterion("LINKMAN like", value, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANNotLike(String value) {
            addCriterion("LINKMAN not like", value, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANIn(List<String> values) {
            addCriterion("LINKMAN in", values, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANNotIn(List<String> values) {
            addCriterion("LINKMAN not in", values, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANBetween(String value1, String value2) {
            addCriterion("LINKMAN between", value1, value2, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andLINKMANNotBetween(String value1, String value2) {
            addCriterion("LINKMAN not between", value1, value2, "LINKMAN");
            return (Criteria) this;
        }

        public Criteria andPASSWORDIsNull() {
            addCriterion("PASSWORD is null");
            return (Criteria) this;
        }

        public Criteria andPASSWORDIsNotNull() {
            addCriterion("PASSWORD is not null");
            return (Criteria) this;
        }

        public Criteria andPASSWORDEqualTo(String value) {
            addCriterion("PASSWORD =", value, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDNotEqualTo(String value) {
            addCriterion("PASSWORD <>", value, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDGreaterThan(String value) {
            addCriterion("PASSWORD >", value, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDGreaterThanOrEqualTo(String value) {
            addCriterion("PASSWORD >=", value, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDLessThan(String value) {
            addCriterion("PASSWORD <", value, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDLessThanOrEqualTo(String value) {
            addCriterion("PASSWORD <=", value, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDLike(String value) {
            addCriterion("PASSWORD like", value, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDNotLike(String value) {
            addCriterion("PASSWORD not like", value, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDIn(List<String> values) {
            addCriterion("PASSWORD in", values, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDNotIn(List<String> values) {
            addCriterion("PASSWORD not in", values, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDBetween(String value1, String value2) {
            addCriterion("PASSWORD between", value1, value2, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andPASSWORDNotBetween(String value1, String value2) {
            addCriterion("PASSWORD not between", value1, value2, "PASSWORD");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONIsNull() {
            addCriterion("CORPORATION is null");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONIsNotNull() {
            addCriterion("CORPORATION is not null");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONEqualTo(String value) {
            addCriterion("CORPORATION =", value, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONNotEqualTo(String value) {
            addCriterion("CORPORATION <>", value, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONGreaterThan(String value) {
            addCriterion("CORPORATION >", value, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONGreaterThanOrEqualTo(String value) {
            addCriterion("CORPORATION >=", value, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONLessThan(String value) {
            addCriterion("CORPORATION <", value, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONLessThanOrEqualTo(String value) {
            addCriterion("CORPORATION <=", value, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONLike(String value) {
            addCriterion("CORPORATION like", value, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONNotLike(String value) {
            addCriterion("CORPORATION not like", value, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONIn(List<String> values) {
            addCriterion("CORPORATION in", values, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONNotIn(List<String> values) {
            addCriterion("CORPORATION not in", values, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONBetween(String value1, String value2) {
            addCriterion("CORPORATION between", value1, value2, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andCORPORATIONNotBetween(String value1, String value2) {
            addCriterion("CORPORATION not between", value1, value2, "CORPORATION");
            return (Criteria) this;
        }

        public Criteria andBANKCODEIsNull() {
            addCriterion("BANKCODE is null");
            return (Criteria) this;
        }

        public Criteria andBANKCODEIsNotNull() {
            addCriterion("BANKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andBANKCODEEqualTo(String value) {
            addCriterion("BANKCODE =", value, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODENotEqualTo(String value) {
            addCriterion("BANKCODE <>", value, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODEGreaterThan(String value) {
            addCriterion("BANKCODE >", value, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODEGreaterThanOrEqualTo(String value) {
            addCriterion("BANKCODE >=", value, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODELessThan(String value) {
            addCriterion("BANKCODE <", value, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODELessThanOrEqualTo(String value) {
            addCriterion("BANKCODE <=", value, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODELike(String value) {
            addCriterion("BANKCODE like", value, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODENotLike(String value) {
            addCriterion("BANKCODE not like", value, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODEIn(List<String> values) {
            addCriterion("BANKCODE in", values, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODENotIn(List<String> values) {
            addCriterion("BANKCODE not in", values, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODEBetween(String value1, String value2) {
            addCriterion("BANKCODE between", value1, value2, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKCODENotBetween(String value1, String value2) {
            addCriterion("BANKCODE not between", value1, value2, "BANKCODE");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOIsNull() {
            addCriterion("BANKACCNO is null");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOIsNotNull() {
            addCriterion("BANKACCNO is not null");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOEqualTo(String value) {
            addCriterion("BANKACCNO =", value, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNONotEqualTo(String value) {
            addCriterion("BANKACCNO <>", value, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOGreaterThan(String value) {
            addCriterion("BANKACCNO >", value, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOGreaterThanOrEqualTo(String value) {
            addCriterion("BANKACCNO >=", value, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOLessThan(String value) {
            addCriterion("BANKACCNO <", value, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOLessThanOrEqualTo(String value) {
            addCriterion("BANKACCNO <=", value, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOLike(String value) {
            addCriterion("BANKACCNO like", value, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNONotLike(String value) {
            addCriterion("BANKACCNO not like", value, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOIn(List<String> values) {
            addCriterion("BANKACCNO in", values, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNONotIn(List<String> values) {
            addCriterion("BANKACCNO not in", values, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNOBetween(String value1, String value2) {
            addCriterion("BANKACCNO between", value1, value2, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBANKACCNONotBetween(String value1, String value2) {
            addCriterion("BANKACCNO not between", value1, value2, "BANKACCNO");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPEIsNull() {
            addCriterion("BUSINESSTYPE is null");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPEIsNotNull() {
            addCriterion("BUSINESSTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPEEqualTo(String value) {
            addCriterion("BUSINESSTYPE =", value, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPENotEqualTo(String value) {
            addCriterion("BUSINESSTYPE <>", value, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPEGreaterThan(String value) {
            addCriterion("BUSINESSTYPE >", value, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("BUSINESSTYPE >=", value, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPELessThan(String value) {
            addCriterion("BUSINESSTYPE <", value, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPELessThanOrEqualTo(String value) {
            addCriterion("BUSINESSTYPE <=", value, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPELike(String value) {
            addCriterion("BUSINESSTYPE like", value, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPENotLike(String value) {
            addCriterion("BUSINESSTYPE not like", value, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPEIn(List<String> values) {
            addCriterion("BUSINESSTYPE in", values, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPENotIn(List<String> values) {
            addCriterion("BUSINESSTYPE not in", values, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPEBetween(String value1, String value2) {
            addCriterion("BUSINESSTYPE between", value1, value2, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSTYPENotBetween(String value1, String value2) {
            addCriterion("BUSINESSTYPE not between", value1, value2, "BUSINESSTYPE");
            return (Criteria) this;
        }

        public Criteria andGRPNATUREIsNull() {
            addCriterion("GRPNATURE is null");
            return (Criteria) this;
        }

        public Criteria andGRPNATUREIsNotNull() {
            addCriterion("GRPNATURE is not null");
            return (Criteria) this;
        }

        public Criteria andGRPNATUREEqualTo(String value) {
            addCriterion("GRPNATURE =", value, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATURENotEqualTo(String value) {
            addCriterion("GRPNATURE <>", value, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATUREGreaterThan(String value) {
            addCriterion("GRPNATURE >", value, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATUREGreaterThanOrEqualTo(String value) {
            addCriterion("GRPNATURE >=", value, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATURELessThan(String value) {
            addCriterion("GRPNATURE <", value, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATURELessThanOrEqualTo(String value) {
            addCriterion("GRPNATURE <=", value, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATURELike(String value) {
            addCriterion("GRPNATURE like", value, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATURENotLike(String value) {
            addCriterion("GRPNATURE not like", value, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATUREIn(List<String> values) {
            addCriterion("GRPNATURE in", values, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATURENotIn(List<String> values) {
            addCriterion("GRPNATURE not in", values, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATUREBetween(String value1, String value2) {
            addCriterion("GRPNATURE between", value1, value2, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andGRPNATURENotBetween(String value1, String value2) {
            addCriterion("GRPNATURE not between", value1, value2, "GRPNATURE");
            return (Criteria) this;
        }

        public Criteria andACTYPEIsNull() {
            addCriterion("ACTYPE is null");
            return (Criteria) this;
        }

        public Criteria andACTYPEIsNotNull() {
            addCriterion("ACTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andACTYPEEqualTo(String value) {
            addCriterion("ACTYPE =", value, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPENotEqualTo(String value) {
            addCriterion("ACTYPE <>", value, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPEGreaterThan(String value) {
            addCriterion("ACTYPE >", value, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("ACTYPE >=", value, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPELessThan(String value) {
            addCriterion("ACTYPE <", value, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPELessThanOrEqualTo(String value) {
            addCriterion("ACTYPE <=", value, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPELike(String value) {
            addCriterion("ACTYPE like", value, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPENotLike(String value) {
            addCriterion("ACTYPE not like", value, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPEIn(List<String> values) {
            addCriterion("ACTYPE in", values, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPENotIn(List<String> values) {
            addCriterion("ACTYPE not in", values, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPEBetween(String value1, String value2) {
            addCriterion("ACTYPE between", value1, value2, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andACTYPENotBetween(String value1, String value2) {
            addCriterion("ACTYPE not between", value1, value2, "ACTYPE");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGIsNull() {
            addCriterion("SELLFLAG is null");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGIsNotNull() {
            addCriterion("SELLFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGEqualTo(String value) {
            addCriterion("SELLFLAG =", value, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGNotEqualTo(String value) {
            addCriterion("SELLFLAG <>", value, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGGreaterThan(String value) {
            addCriterion("SELLFLAG >", value, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("SELLFLAG >=", value, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGLessThan(String value) {
            addCriterion("SELLFLAG <", value, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGLessThanOrEqualTo(String value) {
            addCriterion("SELLFLAG <=", value, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGLike(String value) {
            addCriterion("SELLFLAG like", value, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGNotLike(String value) {
            addCriterion("SELLFLAG not like", value, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGIn(List<String> values) {
            addCriterion("SELLFLAG in", values, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGNotIn(List<String> values) {
            addCriterion("SELLFLAG not in", values, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGBetween(String value1, String value2) {
            addCriterion("SELLFLAG between", value1, value2, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andSELLFLAGNotBetween(String value1, String value2) {
            addCriterion("SELLFLAG not between", value1, value2, "SELLFLAG");
            return (Criteria) this;
        }

        public Criteria andOPERATORIsNull() {
            addCriterion("OPERATOR is null");
            return (Criteria) this;
        }

        public Criteria andOPERATORIsNotNull() {
            addCriterion("OPERATOR is not null");
            return (Criteria) this;
        }

        public Criteria andOPERATOREqualTo(String value) {
            addCriterion("OPERATOR =", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotEqualTo(String value) {
            addCriterion("OPERATOR <>", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORGreaterThan(String value) {
            addCriterion("OPERATOR >", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATOR >=", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLessThan(String value) {
            addCriterion("OPERATOR <", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLessThanOrEqualTo(String value) {
            addCriterion("OPERATOR <=", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLike(String value) {
            addCriterion("OPERATOR like", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotLike(String value) {
            addCriterion("OPERATOR not like", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORIn(List<String> values) {
            addCriterion("OPERATOR in", values, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotIn(List<String> values) {
            addCriterion("OPERATOR not in", values, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORBetween(String value1, String value2) {
            addCriterion("OPERATOR between", value1, value2, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotBetween(String value1, String value2) {
            addCriterion("OPERATOR not between", value1, value2, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIsNull() {
            addCriterion("MAKEDATE is null");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIsNotNull() {
            addCriterion("MAKEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE =", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE <>", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("MAKEDATE >", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE >=", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATELessThan(Date value) {
            addCriterionForJDBCDate("MAKEDATE <", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE <=", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIn(List<Date> values) {
            addCriterionForJDBCDate("MAKEDATE in", values, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("MAKEDATE not in", values, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MAKEDATE between", value1, value2, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MAKEDATE not between", value1, value2, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIsNull() {
            addCriterion("MAKETIME is null");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIsNotNull() {
            addCriterion("MAKETIME is not null");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEEqualTo(String value) {
            addCriterion("MAKETIME =", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotEqualTo(String value) {
            addCriterion("MAKETIME <>", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEGreaterThan(String value) {
            addCriterion("MAKETIME >", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEGreaterThanOrEqualTo(String value) {
            addCriterion("MAKETIME >=", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELessThan(String value) {
            addCriterion("MAKETIME <", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELessThanOrEqualTo(String value) {
            addCriterion("MAKETIME <=", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELike(String value) {
            addCriterion("MAKETIME like", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotLike(String value) {
            addCriterion("MAKETIME not like", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIn(List<String> values) {
            addCriterion("MAKETIME in", values, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotIn(List<String> values) {
            addCriterion("MAKETIME not in", values, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEBetween(String value1, String value2) {
            addCriterion("MAKETIME between", value1, value2, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotBetween(String value1, String value2) {
            addCriterion("MAKETIME not between", value1, value2, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIsNull() {
            addCriterion("MODIFYDATE is null");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIsNotNull() {
            addCriterion("MODIFYDATE is not null");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE =", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <>", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("MODIFYDATE >", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE >=", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATELessThan(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <=", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIn(List<Date> values) {
            addCriterionForJDBCDate("MODIFYDATE in", values, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("MODIFYDATE not in", values, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MODIFYDATE between", value1, value2, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MODIFYDATE not between", value1, value2, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIsNull() {
            addCriterion("MODIFYTIME is null");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIsNotNull() {
            addCriterion("MODIFYTIME is not null");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEEqualTo(String value) {
            addCriterion("MODIFYTIME =", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotEqualTo(String value) {
            addCriterion("MODIFYTIME <>", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEGreaterThan(String value) {
            addCriterion("MODIFYTIME >", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFYTIME >=", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELessThan(String value) {
            addCriterion("MODIFYTIME <", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELessThanOrEqualTo(String value) {
            addCriterion("MODIFYTIME <=", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELike(String value) {
            addCriterion("MODIFYTIME like", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotLike(String value) {
            addCriterion("MODIFYTIME not like", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIn(List<String> values) {
            addCriterion("MODIFYTIME in", values, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotIn(List<String> values) {
            addCriterion("MODIFYTIME not in", values, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEBetween(String value1, String value2) {
            addCriterion("MODIFYTIME between", value1, value2, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotBetween(String value1, String value2) {
            addCriterion("MODIFYTIME not between", value1, value2, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andBANKTYPEIsNull() {
            addCriterion("BANKTYPE is null");
            return (Criteria) this;
        }

        public Criteria andBANKTYPEIsNotNull() {
            addCriterion("BANKTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andBANKTYPEEqualTo(String value) {
            addCriterion("BANKTYPE =", value, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPENotEqualTo(String value) {
            addCriterion("BANKTYPE <>", value, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPEGreaterThan(String value) {
            addCriterion("BANKTYPE >", value, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("BANKTYPE >=", value, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPELessThan(String value) {
            addCriterion("BANKTYPE <", value, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPELessThanOrEqualTo(String value) {
            addCriterion("BANKTYPE <=", value, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPELike(String value) {
            addCriterion("BANKTYPE like", value, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPENotLike(String value) {
            addCriterion("BANKTYPE not like", value, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPEIn(List<String> values) {
            addCriterion("BANKTYPE in", values, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPENotIn(List<String> values) {
            addCriterion("BANKTYPE not in", values, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPEBetween(String value1, String value2) {
            addCriterion("BANKTYPE between", value1, value2, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andBANKTYPENotBetween(String value1, String value2) {
            addCriterion("BANKTYPE not between", value1, value2, "BANKTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFLAGIsNull() {
            addCriterion("CALFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCALFLAGIsNotNull() {
            addCriterion("CALFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCALFLAGEqualTo(String value) {
            addCriterion("CALFLAG =", value, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGNotEqualTo(String value) {
            addCriterion("CALFLAG <>", value, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGGreaterThan(String value) {
            addCriterion("CALFLAG >", value, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("CALFLAG >=", value, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGLessThan(String value) {
            addCriterion("CALFLAG <", value, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGLessThanOrEqualTo(String value) {
            addCriterion("CALFLAG <=", value, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGLike(String value) {
            addCriterion("CALFLAG like", value, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGNotLike(String value) {
            addCriterion("CALFLAG not like", value, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGIn(List<String> values) {
            addCriterion("CALFLAG in", values, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGNotIn(List<String> values) {
            addCriterion("CALFLAG not in", values, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGBetween(String value1, String value2) {
            addCriterion("CALFLAG between", value1, value2, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andCALFLAGNotBetween(String value1, String value2) {
            addCriterion("CALFLAG not between", value1, value2, "CALFLAG");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODEIsNull() {
            addCriterion("BUSILICENSECODE is null");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODEIsNotNull() {
            addCriterion("BUSILICENSECODE is not null");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODEEqualTo(String value) {
            addCriterion("BUSILICENSECODE =", value, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODENotEqualTo(String value) {
            addCriterion("BUSILICENSECODE <>", value, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODEGreaterThan(String value) {
            addCriterion("BUSILICENSECODE >", value, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODEGreaterThanOrEqualTo(String value) {
            addCriterion("BUSILICENSECODE >=", value, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODELessThan(String value) {
            addCriterion("BUSILICENSECODE <", value, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODELessThanOrEqualTo(String value) {
            addCriterion("BUSILICENSECODE <=", value, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODELike(String value) {
            addCriterion("BUSILICENSECODE like", value, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODENotLike(String value) {
            addCriterion("BUSILICENSECODE not like", value, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODEIn(List<String> values) {
            addCriterion("BUSILICENSECODE in", values, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODENotIn(List<String> values) {
            addCriterion("BUSILICENSECODE not in", values, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODEBetween(String value1, String value2) {
            addCriterion("BUSILICENSECODE between", value1, value2, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andBUSILICENSECODENotBetween(String value1, String value2) {
            addCriterion("BUSILICENSECODE not between", value1, value2, "BUSILICENSECODE");
            return (Criteria) this;
        }

        public Criteria andINSUREIDIsNull() {
            addCriterion("INSUREID is null");
            return (Criteria) this;
        }

        public Criteria andINSUREIDIsNotNull() {
            addCriterion("INSUREID is not null");
            return (Criteria) this;
        }

        public Criteria andINSUREIDEqualTo(String value) {
            addCriterion("INSUREID =", value, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDNotEqualTo(String value) {
            addCriterion("INSUREID <>", value, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDGreaterThan(String value) {
            addCriterion("INSUREID >", value, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDGreaterThanOrEqualTo(String value) {
            addCriterion("INSUREID >=", value, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDLessThan(String value) {
            addCriterion("INSUREID <", value, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDLessThanOrEqualTo(String value) {
            addCriterion("INSUREID <=", value, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDLike(String value) {
            addCriterion("INSUREID like", value, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDNotLike(String value) {
            addCriterion("INSUREID not like", value, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDIn(List<String> values) {
            addCriterion("INSUREID in", values, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDNotIn(List<String> values) {
            addCriterion("INSUREID not in", values, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDBetween(String value1, String value2) {
            addCriterion("INSUREID between", value1, value2, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREIDNotBetween(String value1, String value2) {
            addCriterion("INSUREID not between", value1, value2, "INSUREID");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALIsNull() {
            addCriterion("INSUREPRINCIPAL is null");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALIsNotNull() {
            addCriterion("INSUREPRINCIPAL is not null");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALEqualTo(String value) {
            addCriterion("INSUREPRINCIPAL =", value, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALNotEqualTo(String value) {
            addCriterion("INSUREPRINCIPAL <>", value, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALGreaterThan(String value) {
            addCriterion("INSUREPRINCIPAL >", value, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALGreaterThanOrEqualTo(String value) {
            addCriterion("INSUREPRINCIPAL >=", value, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALLessThan(String value) {
            addCriterion("INSUREPRINCIPAL <", value, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALLessThanOrEqualTo(String value) {
            addCriterion("INSUREPRINCIPAL <=", value, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALLike(String value) {
            addCriterion("INSUREPRINCIPAL like", value, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALNotLike(String value) {
            addCriterion("INSUREPRINCIPAL not like", value, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALIn(List<String> values) {
            addCriterion("INSUREPRINCIPAL in", values, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALNotIn(List<String> values) {
            addCriterion("INSUREPRINCIPAL not in", values, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALBetween(String value1, String value2) {
            addCriterion("INSUREPRINCIPAL between", value1, value2, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andINSUREPRINCIPALNotBetween(String value1, String value2) {
            addCriterion("INSUREPRINCIPAL not between", value1, value2, "INSUREPRINCIPAL");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSIsNull() {
            addCriterion("CHIEFBUSINESS is null");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSIsNotNull() {
            addCriterion("CHIEFBUSINESS is not null");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSEqualTo(String value) {
            addCriterion("CHIEFBUSINESS =", value, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSNotEqualTo(String value) {
            addCriterion("CHIEFBUSINESS <>", value, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSGreaterThan(String value) {
            addCriterion("CHIEFBUSINESS >", value, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSGreaterThanOrEqualTo(String value) {
            addCriterion("CHIEFBUSINESS >=", value, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSLessThan(String value) {
            addCriterion("CHIEFBUSINESS <", value, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSLessThanOrEqualTo(String value) {
            addCriterion("CHIEFBUSINESS <=", value, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSLike(String value) {
            addCriterion("CHIEFBUSINESS like", value, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSNotLike(String value) {
            addCriterion("CHIEFBUSINESS not like", value, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSIn(List<String> values) {
            addCriterion("CHIEFBUSINESS in", values, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSNotIn(List<String> values) {
            addCriterion("CHIEFBUSINESS not in", values, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSBetween(String value1, String value2) {
            addCriterion("CHIEFBUSINESS between", value1, value2, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andCHIEFBUSINESSNotBetween(String value1, String value2) {
            addCriterion("CHIEFBUSINESS not between", value1, value2, "CHIEFBUSINESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSIsNull() {
            addCriterion("BUSIADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSIsNotNull() {
            addCriterion("BUSIADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSEqualTo(String value) {
            addCriterion("BUSIADDRESS =", value, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSNotEqualTo(String value) {
            addCriterion("BUSIADDRESS <>", value, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSGreaterThan(String value) {
            addCriterion("BUSIADDRESS >", value, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSGreaterThanOrEqualTo(String value) {
            addCriterion("BUSIADDRESS >=", value, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSLessThan(String value) {
            addCriterion("BUSIADDRESS <", value, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSLessThanOrEqualTo(String value) {
            addCriterion("BUSIADDRESS <=", value, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSLike(String value) {
            addCriterion("BUSIADDRESS like", value, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSNotLike(String value) {
            addCriterion("BUSIADDRESS not like", value, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSIn(List<String> values) {
            addCriterion("BUSIADDRESS in", values, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSNotIn(List<String> values) {
            addCriterion("BUSIADDRESS not in", values, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSBetween(String value1, String value2) {
            addCriterion("BUSIADDRESS between", value1, value2, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andBUSIADDRESSNotBetween(String value1, String value2) {
            addCriterion("BUSIADDRESS not between", value1, value2, "BUSIADDRESS");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANIsNull() {
            addCriterion("SUBSCRIBEMAN is null");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANIsNotNull() {
            addCriterion("SUBSCRIBEMAN is not null");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANEqualTo(String value) {
            addCriterion("SUBSCRIBEMAN =", value, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANNotEqualTo(String value) {
            addCriterion("SUBSCRIBEMAN <>", value, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANGreaterThan(String value) {
            addCriterion("SUBSCRIBEMAN >", value, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANGreaterThanOrEqualTo(String value) {
            addCriterion("SUBSCRIBEMAN >=", value, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANLessThan(String value) {
            addCriterion("SUBSCRIBEMAN <", value, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANLessThanOrEqualTo(String value) {
            addCriterion("SUBSCRIBEMAN <=", value, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANLike(String value) {
            addCriterion("SUBSCRIBEMAN like", value, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANNotLike(String value) {
            addCriterion("SUBSCRIBEMAN not like", value, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANIn(List<String> values) {
            addCriterion("SUBSCRIBEMAN in", values, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANNotIn(List<String> values) {
            addCriterion("SUBSCRIBEMAN not in", values, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANBetween(String value1, String value2) {
            addCriterion("SUBSCRIBEMAN between", value1, value2, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANNotBetween(String value1, String value2) {
            addCriterion("SUBSCRIBEMAN not between", value1, value2, "SUBSCRIBEMAN");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYIsNull() {
            addCriterion("SUBSCRIBEMANDUTY is null");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYIsNotNull() {
            addCriterion("SUBSCRIBEMANDUTY is not null");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYEqualTo(String value) {
            addCriterion("SUBSCRIBEMANDUTY =", value, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYNotEqualTo(String value) {
            addCriterion("SUBSCRIBEMANDUTY <>", value, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYGreaterThan(String value) {
            addCriterion("SUBSCRIBEMANDUTY >", value, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYGreaterThanOrEqualTo(String value) {
            addCriterion("SUBSCRIBEMANDUTY >=", value, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYLessThan(String value) {
            addCriterion("SUBSCRIBEMANDUTY <", value, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYLessThanOrEqualTo(String value) {
            addCriterion("SUBSCRIBEMANDUTY <=", value, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYLike(String value) {
            addCriterion("SUBSCRIBEMANDUTY like", value, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYNotLike(String value) {
            addCriterion("SUBSCRIBEMANDUTY not like", value, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYIn(List<String> values) {
            addCriterion("SUBSCRIBEMANDUTY in", values, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYNotIn(List<String> values) {
            addCriterion("SUBSCRIBEMANDUTY not in", values, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYBetween(String value1, String value2) {
            addCriterion("SUBSCRIBEMANDUTY between", value1, value2, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andSUBSCRIBEMANDUTYNotBetween(String value1, String value2) {
            addCriterion("SUBSCRIBEMANDUTY not between", value1, value2, "SUBSCRIBEMANDUTY");
            return (Criteria) this;
        }

        public Criteria andLICENSENOIsNull() {
            addCriterion("LICENSENO is null");
            return (Criteria) this;
        }

        public Criteria andLICENSENOIsNotNull() {
            addCriterion("LICENSENO is not null");
            return (Criteria) this;
        }

        public Criteria andLICENSENOEqualTo(String value) {
            addCriterion("LICENSENO =", value, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENONotEqualTo(String value) {
            addCriterion("LICENSENO <>", value, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENOGreaterThan(String value) {
            addCriterion("LICENSENO >", value, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENOGreaterThanOrEqualTo(String value) {
            addCriterion("LICENSENO >=", value, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENOLessThan(String value) {
            addCriterion("LICENSENO <", value, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENOLessThanOrEqualTo(String value) {
            addCriterion("LICENSENO <=", value, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENOLike(String value) {
            addCriterion("LICENSENO like", value, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENONotLike(String value) {
            addCriterion("LICENSENO not like", value, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENOIn(List<String> values) {
            addCriterion("LICENSENO in", values, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENONotIn(List<String> values) {
            addCriterion("LICENSENO not in", values, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENOBetween(String value1, String value2) {
            addCriterion("LICENSENO between", value1, value2, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andLICENSENONotBetween(String value1, String value2) {
            addCriterion("LICENSENO not between", value1, value2, "LICENSENO");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODEIsNull() {
            addCriterion("REGIONALISMCODE is null");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODEIsNotNull() {
            addCriterion("REGIONALISMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODEEqualTo(String value) {
            addCriterion("REGIONALISMCODE =", value, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODENotEqualTo(String value) {
            addCriterion("REGIONALISMCODE <>", value, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODEGreaterThan(String value) {
            addCriterion("REGIONALISMCODE >", value, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODEGreaterThanOrEqualTo(String value) {
            addCriterion("REGIONALISMCODE >=", value, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODELessThan(String value) {
            addCriterion("REGIONALISMCODE <", value, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODELessThanOrEqualTo(String value) {
            addCriterion("REGIONALISMCODE <=", value, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODELike(String value) {
            addCriterion("REGIONALISMCODE like", value, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODENotLike(String value) {
            addCriterion("REGIONALISMCODE not like", value, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODEIn(List<String> values) {
            addCriterion("REGIONALISMCODE in", values, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODENotIn(List<String> values) {
            addCriterion("REGIONALISMCODE not in", values, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODEBetween(String value1, String value2) {
            addCriterion("REGIONALISMCODE between", value1, value2, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andREGIONALISMCODENotBetween(String value1, String value2) {
            addCriterion("REGIONALISMCODE not between", value1, value2, "REGIONALISMCODE");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMIsNull() {
            addCriterion("APPAGENTCOM is null");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMIsNotNull() {
            addCriterion("APPAGENTCOM is not null");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMEqualTo(String value) {
            addCriterion("APPAGENTCOM =", value, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMNotEqualTo(String value) {
            addCriterion("APPAGENTCOM <>", value, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMGreaterThan(String value) {
            addCriterion("APPAGENTCOM >", value, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMGreaterThanOrEqualTo(String value) {
            addCriterion("APPAGENTCOM >=", value, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMLessThan(String value) {
            addCriterion("APPAGENTCOM <", value, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMLessThanOrEqualTo(String value) {
            addCriterion("APPAGENTCOM <=", value, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMLike(String value) {
            addCriterion("APPAGENTCOM like", value, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMNotLike(String value) {
            addCriterion("APPAGENTCOM not like", value, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMIn(List<String> values) {
            addCriterion("APPAGENTCOM in", values, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMNotIn(List<String> values) {
            addCriterion("APPAGENTCOM not in", values, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMBetween(String value1, String value2) {
            addCriterion("APPAGENTCOM between", value1, value2, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAPPAGENTCOMNotBetween(String value1, String value2) {
            addCriterion("APPAGENTCOM not between", value1, value2, "APPAGENTCOM");
            return (Criteria) this;
        }

        public Criteria andSTATEIsNull() {
            addCriterion("STATE is null");
            return (Criteria) this;
        }

        public Criteria andSTATEIsNotNull() {
            addCriterion("STATE is not null");
            return (Criteria) this;
        }

        public Criteria andSTATEEqualTo(String value) {
            addCriterion("STATE =", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATENotEqualTo(String value) {
            addCriterion("STATE <>", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATEGreaterThan(String value) {
            addCriterion("STATE >", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATEGreaterThanOrEqualTo(String value) {
            addCriterion("STATE >=", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATELessThan(String value) {
            addCriterion("STATE <", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATELessThanOrEqualTo(String value) {
            addCriterion("STATE <=", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATELike(String value) {
            addCriterion("STATE like", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATENotLike(String value) {
            addCriterion("STATE not like", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATEIn(List<String> values) {
            addCriterion("STATE in", values, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATENotIn(List<String> values) {
            addCriterion("STATE not in", values, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATEBetween(String value1, String value2) {
            addCriterion("STATE between", value1, value2, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATENotBetween(String value1, String value2) {
            addCriterion("STATE not between", value1, value2, "STATE");
            return (Criteria) this;
        }

        public Criteria andNOTIIsNull() {
            addCriterion("NOTI is null");
            return (Criteria) this;
        }

        public Criteria andNOTIIsNotNull() {
            addCriterion("NOTI is not null");
            return (Criteria) this;
        }

        public Criteria andNOTIEqualTo(String value) {
            addCriterion("NOTI =", value, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTINotEqualTo(String value) {
            addCriterion("NOTI <>", value, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTIGreaterThan(String value) {
            addCriterion("NOTI >", value, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTIGreaterThanOrEqualTo(String value) {
            addCriterion("NOTI >=", value, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTILessThan(String value) {
            addCriterion("NOTI <", value, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTILessThanOrEqualTo(String value) {
            addCriterion("NOTI <=", value, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTILike(String value) {
            addCriterion("NOTI like", value, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTINotLike(String value) {
            addCriterion("NOTI not like", value, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTIIn(List<String> values) {
            addCriterion("NOTI in", values, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTINotIn(List<String> values) {
            addCriterion("NOTI not in", values, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTIBetween(String value1, String value2) {
            addCriterion("NOTI between", value1, value2, "NOTI");
            return (Criteria) this;
        }

        public Criteria andNOTINotBetween(String value1, String value2) {
            addCriterion("NOTI not between", value1, value2, "NOTI");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODEIsNull() {
            addCriterion("BUSINESSCODE is null");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODEIsNotNull() {
            addCriterion("BUSINESSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODEEqualTo(String value) {
            addCriterion("BUSINESSCODE =", value, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODENotEqualTo(String value) {
            addCriterion("BUSINESSCODE <>", value, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODEGreaterThan(String value) {
            addCriterion("BUSINESSCODE >", value, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODEGreaterThanOrEqualTo(String value) {
            addCriterion("BUSINESSCODE >=", value, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODELessThan(String value) {
            addCriterion("BUSINESSCODE <", value, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODELessThanOrEqualTo(String value) {
            addCriterion("BUSINESSCODE <=", value, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODELike(String value) {
            addCriterion("BUSINESSCODE like", value, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODENotLike(String value) {
            addCriterion("BUSINESSCODE not like", value, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODEIn(List<String> values) {
            addCriterion("BUSINESSCODE in", values, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODENotIn(List<String> values) {
            addCriterion("BUSINESSCODE not in", values, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODEBetween(String value1, String value2) {
            addCriterion("BUSINESSCODE between", value1, value2, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andBUSINESSCODENotBetween(String value1, String value2) {
            addCriterion("BUSINESSCODE not between", value1, value2, "BUSINESSCODE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATEIsNull() {
            addCriterion("LICENSESTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATEIsNotNull() {
            addCriterion("LICENSESTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATEEqualTo(Date value) {
            addCriterionForJDBCDate("LICENSESTARTDATE =", value, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("LICENSESTARTDATE <>", value, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("LICENSESTARTDATE >", value, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("LICENSESTARTDATE >=", value, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATELessThan(Date value) {
            addCriterionForJDBCDate("LICENSESTARTDATE <", value, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("LICENSESTARTDATE <=", value, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATEIn(List<Date> values) {
            addCriterionForJDBCDate("LICENSESTARTDATE in", values, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("LICENSESTARTDATE not in", values, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("LICENSESTARTDATE between", value1, value2, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSESTARTDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("LICENSESTARTDATE not between", value1, value2, "LICENSESTARTDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATEIsNull() {
            addCriterion("LICENSEENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATEIsNotNull() {
            addCriterion("LICENSEENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATEEqualTo(Date value) {
            addCriterionForJDBCDate("LICENSEENDDATE =", value, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("LICENSEENDDATE <>", value, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("LICENSEENDDATE >", value, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("LICENSEENDDATE >=", value, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATELessThan(Date value) {
            addCriterionForJDBCDate("LICENSEENDDATE <", value, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("LICENSEENDDATE <=", value, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATEIn(List<Date> values) {
            addCriterionForJDBCDate("LICENSEENDDATE in", values, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("LICENSEENDDATE not in", values, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("LICENSEENDDATE between", value1, value2, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andLICENSEENDDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("LICENSEENDDATE not between", value1, value2, "LICENSEENDDATE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPEIsNull() {
            addCriterion("BRANCHTYPE is null");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPEIsNotNull() {
            addCriterion("BRANCHTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPEEqualTo(String value) {
            addCriterion("BRANCHTYPE =", value, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPENotEqualTo(String value) {
            addCriterion("BRANCHTYPE <>", value, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPEGreaterThan(String value) {
            addCriterion("BRANCHTYPE >", value, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("BRANCHTYPE >=", value, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPELessThan(String value) {
            addCriterion("BRANCHTYPE <", value, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPELessThanOrEqualTo(String value) {
            addCriterion("BRANCHTYPE <=", value, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPELike(String value) {
            addCriterion("BRANCHTYPE like", value, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPENotLike(String value) {
            addCriterion("BRANCHTYPE not like", value, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPEIn(List<String> values) {
            addCriterion("BRANCHTYPE in", values, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPENotIn(List<String> values) {
            addCriterion("BRANCHTYPE not in", values, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPEBetween(String value1, String value2) {
            addCriterion("BRANCHTYPE between", value1, value2, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPENotBetween(String value1, String value2) {
            addCriterion("BRANCHTYPE not between", value1, value2, "BRANCHTYPE");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2IsNull() {
            addCriterion("BRANCHTYPE2 is null");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2IsNotNull() {
            addCriterion("BRANCHTYPE2 is not null");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2EqualTo(String value) {
            addCriterion("BRANCHTYPE2 =", value, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2NotEqualTo(String value) {
            addCriterion("BRANCHTYPE2 <>", value, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2GreaterThan(String value) {
            addCriterion("BRANCHTYPE2 >", value, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2GreaterThanOrEqualTo(String value) {
            addCriterion("BRANCHTYPE2 >=", value, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2LessThan(String value) {
            addCriterion("BRANCHTYPE2 <", value, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2LessThanOrEqualTo(String value) {
            addCriterion("BRANCHTYPE2 <=", value, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2Like(String value) {
            addCriterion("BRANCHTYPE2 like", value, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2NotLike(String value) {
            addCriterion("BRANCHTYPE2 not like", value, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2In(List<String> values) {
            addCriterion("BRANCHTYPE2 in", values, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2NotIn(List<String> values) {
            addCriterion("BRANCHTYPE2 not in", values, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2Between(String value1, String value2) {
            addCriterion("BRANCHTYPE2 between", value1, value2, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andBRANCHTYPE2NotBetween(String value1, String value2) {
            addCriterion("BRANCHTYPE2 not between", value1, value2, "BRANCHTYPE2");
            return (Criteria) this;
        }

        public Criteria andASSETSIsNull() {
            addCriterion("ASSETS is null");
            return (Criteria) this;
        }

        public Criteria andASSETSIsNotNull() {
            addCriterion("ASSETS is not null");
            return (Criteria) this;
        }

        public Criteria andASSETSEqualTo(Float value) {
            addCriterion("ASSETS =", value, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSNotEqualTo(Float value) {
            addCriterion("ASSETS <>", value, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSGreaterThan(Float value) {
            addCriterion("ASSETS >", value, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSGreaterThanOrEqualTo(Float value) {
            addCriterion("ASSETS >=", value, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSLessThan(Float value) {
            addCriterion("ASSETS <", value, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSLessThanOrEqualTo(Float value) {
            addCriterion("ASSETS <=", value, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSIn(List<Float> values) {
            addCriterion("ASSETS in", values, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSNotIn(List<Float> values) {
            addCriterion("ASSETS not in", values, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSBetween(Float value1, Float value2) {
            addCriterion("ASSETS between", value1, value2, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andASSETSNotBetween(Float value1, Float value2) {
            addCriterion("ASSETS not between", value1, value2, "ASSETS");
            return (Criteria) this;
        }

        public Criteria andINCOMEIsNull() {
            addCriterion("INCOME is null");
            return (Criteria) this;
        }

        public Criteria andINCOMEIsNotNull() {
            addCriterion("INCOME is not null");
            return (Criteria) this;
        }

        public Criteria andINCOMEEqualTo(Float value) {
            addCriterion("INCOME =", value, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMENotEqualTo(Float value) {
            addCriterion("INCOME <>", value, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMEGreaterThan(Float value) {
            addCriterion("INCOME >", value, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMEGreaterThanOrEqualTo(Float value) {
            addCriterion("INCOME >=", value, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMELessThan(Float value) {
            addCriterion("INCOME <", value, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMELessThanOrEqualTo(Float value) {
            addCriterion("INCOME <=", value, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMEIn(List<Float> values) {
            addCriterion("INCOME in", values, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMENotIn(List<Float> values) {
            addCriterion("INCOME not in", values, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMEBetween(Float value1, Float value2) {
            addCriterion("INCOME between", value1, value2, "INCOME");
            return (Criteria) this;
        }

        public Criteria andINCOMENotBetween(Float value1, Float value2) {
            addCriterion("INCOME not between", value1, value2, "INCOME");
            return (Criteria) this;
        }

        public Criteria andPROFITSIsNull() {
            addCriterion("PROFITS is null");
            return (Criteria) this;
        }

        public Criteria andPROFITSIsNotNull() {
            addCriterion("PROFITS is not null");
            return (Criteria) this;
        }

        public Criteria andPROFITSEqualTo(Float value) {
            addCriterion("PROFITS =", value, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSNotEqualTo(Float value) {
            addCriterion("PROFITS <>", value, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSGreaterThan(Float value) {
            addCriterion("PROFITS >", value, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSGreaterThanOrEqualTo(Float value) {
            addCriterion("PROFITS >=", value, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSLessThan(Float value) {
            addCriterion("PROFITS <", value, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSLessThanOrEqualTo(Float value) {
            addCriterion("PROFITS <=", value, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSIn(List<Float> values) {
            addCriterion("PROFITS in", values, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSNotIn(List<Float> values) {
            addCriterion("PROFITS not in", values, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSBetween(Float value1, Float value2) {
            addCriterion("PROFITS between", value1, value2, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPROFITSNotBetween(Float value1, Float value2) {
            addCriterion("PROFITS not between", value1, value2, "PROFITS");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMIsNull() {
            addCriterion("PERSONNALSUM is null");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMIsNotNull() {
            addCriterion("PERSONNALSUM is not null");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMEqualTo(Float value) {
            addCriterion("PERSONNALSUM =", value, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMNotEqualTo(Float value) {
            addCriterion("PERSONNALSUM <>", value, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMGreaterThan(Float value) {
            addCriterion("PERSONNALSUM >", value, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMGreaterThanOrEqualTo(Float value) {
            addCriterion("PERSONNALSUM >=", value, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMLessThan(Float value) {
            addCriterion("PERSONNALSUM <", value, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMLessThanOrEqualTo(Float value) {
            addCriterion("PERSONNALSUM <=", value, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMIn(List<Float> values) {
            addCriterion("PERSONNALSUM in", values, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMNotIn(List<Float> values) {
            addCriterion("PERSONNALSUM not in", values, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMBetween(Float value1, Float value2) {
            addCriterion("PERSONNALSUM between", value1, value2, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPERSONNALSUMNotBetween(Float value1, Float value2) {
            addCriterion("PERSONNALSUM not between", value1, value2, "PERSONNALSUM");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOIsNull() {
            addCriterion("PROTOCALNO is null");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOIsNotNull() {
            addCriterion("PROTOCALNO is not null");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOEqualTo(String value) {
            addCriterion("PROTOCALNO =", value, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNONotEqualTo(String value) {
            addCriterion("PROTOCALNO <>", value, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOGreaterThan(String value) {
            addCriterion("PROTOCALNO >", value, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOGreaterThanOrEqualTo(String value) {
            addCriterion("PROTOCALNO >=", value, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOLessThan(String value) {
            addCriterion("PROTOCALNO <", value, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOLessThanOrEqualTo(String value) {
            addCriterion("PROTOCALNO <=", value, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOLike(String value) {
            addCriterion("PROTOCALNO like", value, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNONotLike(String value) {
            addCriterion("PROTOCALNO not like", value, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOIn(List<String> values) {
            addCriterion("PROTOCALNO in", values, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNONotIn(List<String> values) {
            addCriterion("PROTOCALNO not in", values, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNOBetween(String value1, String value2) {
            addCriterion("PROTOCALNO between", value1, value2, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andPROTOCALNONotBetween(String value1, String value2) {
            addCriterion("PROTOCALNO not between", value1, value2, "PROTOCALNO");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICEIsNull() {
            addCriterion("HEADOFFICE is null");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICEIsNotNull() {
            addCriterion("HEADOFFICE is not null");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICEEqualTo(String value) {
            addCriterion("HEADOFFICE =", value, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICENotEqualTo(String value) {
            addCriterion("HEADOFFICE <>", value, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICEGreaterThan(String value) {
            addCriterion("HEADOFFICE >", value, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICEGreaterThanOrEqualTo(String value) {
            addCriterion("HEADOFFICE >=", value, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICELessThan(String value) {
            addCriterion("HEADOFFICE <", value, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICELessThanOrEqualTo(String value) {
            addCriterion("HEADOFFICE <=", value, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICELike(String value) {
            addCriterion("HEADOFFICE like", value, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICENotLike(String value) {
            addCriterion("HEADOFFICE not like", value, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICEIn(List<String> values) {
            addCriterion("HEADOFFICE in", values, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICENotIn(List<String> values) {
            addCriterion("HEADOFFICE not in", values, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICEBetween(String value1, String value2) {
            addCriterion("HEADOFFICE between", value1, value2, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andHEADOFFICENotBetween(String value1, String value2) {
            addCriterion("HEADOFFICE not between", value1, value2, "HEADOFFICE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATEIsNull() {
            addCriterion("FOUNDDATE is null");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATEIsNotNull() {
            addCriterion("FOUNDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATEEqualTo(Date value) {
            addCriterionForJDBCDate("FOUNDDATE =", value, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("FOUNDDATE <>", value, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("FOUNDDATE >", value, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("FOUNDDATE >=", value, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATELessThan(Date value) {
            addCriterionForJDBCDate("FOUNDDATE <", value, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("FOUNDDATE <=", value, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATEIn(List<Date> values) {
            addCriterionForJDBCDate("FOUNDDATE in", values, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("FOUNDDATE not in", values, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("FOUNDDATE between", value1, value2, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andFOUNDDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("FOUNDDATE not between", value1, value2, "FOUNDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andENDDATEIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andENDDATEEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE =", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE <>", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("ENDDATE >", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE >=", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATELessThan(Date value) {
            addCriterionForJDBCDate("ENDDATE <", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE <=", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEIn(List<Date> values) {
            addCriterionForJDBCDate("ENDDATE in", values, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("ENDDATE not in", values, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ENDDATE between", value1, value2, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ENDDATE not between", value1, value2, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andBANKIsNull() {
            addCriterion("BANK is null");
            return (Criteria) this;
        }

        public Criteria andBANKIsNotNull() {
            addCriterion("BANK is not null");
            return (Criteria) this;
        }

        public Criteria andBANKEqualTo(String value) {
            addCriterion("BANK =", value, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKNotEqualTo(String value) {
            addCriterion("BANK <>", value, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKGreaterThan(String value) {
            addCriterion("BANK >", value, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKGreaterThanOrEqualTo(String value) {
            addCriterion("BANK >=", value, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKLessThan(String value) {
            addCriterion("BANK <", value, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKLessThanOrEqualTo(String value) {
            addCriterion("BANK <=", value, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKLike(String value) {
            addCriterion("BANK like", value, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKNotLike(String value) {
            addCriterion("BANK not like", value, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKIn(List<String> values) {
            addCriterion("BANK in", values, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKNotIn(List<String> values) {
            addCriterion("BANK not in", values, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKBetween(String value1, String value2) {
            addCriterion("BANK between", value1, value2, "BANK");
            return (Criteria) this;
        }

        public Criteria andBANKNotBetween(String value1, String value2) {
            addCriterion("BANK not between", value1, value2, "BANK");
            return (Criteria) this;
        }

        public Criteria andACCNAMEIsNull() {
            addCriterion("ACCNAME is null");
            return (Criteria) this;
        }

        public Criteria andACCNAMEIsNotNull() {
            addCriterion("ACCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andACCNAMEEqualTo(String value) {
            addCriterion("ACCNAME =", value, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMENotEqualTo(String value) {
            addCriterion("ACCNAME <>", value, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMEGreaterThan(String value) {
            addCriterion("ACCNAME >", value, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("ACCNAME >=", value, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMELessThan(String value) {
            addCriterion("ACCNAME <", value, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMELessThanOrEqualTo(String value) {
            addCriterion("ACCNAME <=", value, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMELike(String value) {
            addCriterion("ACCNAME like", value, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMENotLike(String value) {
            addCriterion("ACCNAME not like", value, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMEIn(List<String> values) {
            addCriterion("ACCNAME in", values, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMENotIn(List<String> values) {
            addCriterion("ACCNAME not in", values, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMEBetween(String value1, String value2) {
            addCriterion("ACCNAME between", value1, value2, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andACCNAMENotBetween(String value1, String value2) {
            addCriterion("ACCNAME not between", value1, value2, "ACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERIsNull() {
            addCriterion("DRAWER is null");
            return (Criteria) this;
        }

        public Criteria andDRAWERIsNotNull() {
            addCriterion("DRAWER is not null");
            return (Criteria) this;
        }

        public Criteria andDRAWEREqualTo(String value) {
            addCriterion("DRAWER =", value, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERNotEqualTo(String value) {
            addCriterion("DRAWER <>", value, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERGreaterThan(String value) {
            addCriterion("DRAWER >", value, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERGreaterThanOrEqualTo(String value) {
            addCriterion("DRAWER >=", value, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERLessThan(String value) {
            addCriterion("DRAWER <", value, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERLessThanOrEqualTo(String value) {
            addCriterion("DRAWER <=", value, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERLike(String value) {
            addCriterion("DRAWER like", value, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERNotLike(String value) {
            addCriterion("DRAWER not like", value, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERIn(List<String> values) {
            addCriterion("DRAWER in", values, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERNotIn(List<String> values) {
            addCriterion("DRAWER not in", values, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERBetween(String value1, String value2) {
            addCriterion("DRAWER between", value1, value2, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERNotBetween(String value1, String value2) {
            addCriterion("DRAWER not between", value1, value2, "DRAWER");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOIsNull() {
            addCriterion("DRAWERACCNO is null");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOIsNotNull() {
            addCriterion("DRAWERACCNO is not null");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOEqualTo(String value) {
            addCriterion("DRAWERACCNO =", value, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNONotEqualTo(String value) {
            addCriterion("DRAWERACCNO <>", value, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOGreaterThan(String value) {
            addCriterion("DRAWERACCNO >", value, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOGreaterThanOrEqualTo(String value) {
            addCriterion("DRAWERACCNO >=", value, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOLessThan(String value) {
            addCriterion("DRAWERACCNO <", value, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOLessThanOrEqualTo(String value) {
            addCriterion("DRAWERACCNO <=", value, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOLike(String value) {
            addCriterion("DRAWERACCNO like", value, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNONotLike(String value) {
            addCriterion("DRAWERACCNO not like", value, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOIn(List<String> values) {
            addCriterion("DRAWERACCNO in", values, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNONotIn(List<String> values) {
            addCriterion("DRAWERACCNO not in", values, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNOBetween(String value1, String value2) {
            addCriterion("DRAWERACCNO between", value1, value2, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNONotBetween(String value1, String value2) {
            addCriterion("DRAWERACCNO not between", value1, value2, "DRAWERACCNO");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMIsNull() {
            addCriterion("REPMANAGECOM is null");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMIsNotNull() {
            addCriterion("REPMANAGECOM is not null");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMEqualTo(String value) {
            addCriterion("REPMANAGECOM =", value, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMNotEqualTo(String value) {
            addCriterion("REPMANAGECOM <>", value, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMGreaterThan(String value) {
            addCriterion("REPMANAGECOM >", value, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMGreaterThanOrEqualTo(String value) {
            addCriterion("REPMANAGECOM >=", value, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMLessThan(String value) {
            addCriterion("REPMANAGECOM <", value, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMLessThanOrEqualTo(String value) {
            addCriterion("REPMANAGECOM <=", value, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMLike(String value) {
            addCriterion("REPMANAGECOM like", value, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMNotLike(String value) {
            addCriterion("REPMANAGECOM not like", value, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMIn(List<String> values) {
            addCriterion("REPMANAGECOM in", values, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMNotIn(List<String> values) {
            addCriterion("REPMANAGECOM not in", values, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMBetween(String value1, String value2) {
            addCriterion("REPMANAGECOM between", value1, value2, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andREPMANAGECOMNotBetween(String value1, String value2) {
            addCriterion("REPMANAGECOM not between", value1, value2, "REPMANAGECOM");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODEIsNull() {
            addCriterion("DRAWERACCCODE is null");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODEIsNotNull() {
            addCriterion("DRAWERACCCODE is not null");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODEEqualTo(String value) {
            addCriterion("DRAWERACCCODE =", value, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODENotEqualTo(String value) {
            addCriterion("DRAWERACCCODE <>", value, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODEGreaterThan(String value) {
            addCriterion("DRAWERACCCODE >", value, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODEGreaterThanOrEqualTo(String value) {
            addCriterion("DRAWERACCCODE >=", value, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODELessThan(String value) {
            addCriterion("DRAWERACCCODE <", value, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODELessThanOrEqualTo(String value) {
            addCriterion("DRAWERACCCODE <=", value, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODELike(String value) {
            addCriterion("DRAWERACCCODE like", value, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODENotLike(String value) {
            addCriterion("DRAWERACCCODE not like", value, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODEIn(List<String> values) {
            addCriterion("DRAWERACCCODE in", values, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODENotIn(List<String> values) {
            addCriterion("DRAWERACCCODE not in", values, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODEBetween(String value1, String value2) {
            addCriterion("DRAWERACCCODE between", value1, value2, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCCODENotBetween(String value1, String value2) {
            addCriterion("DRAWERACCCODE not between", value1, value2, "DRAWERACCCODE");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMEIsNull() {
            addCriterion("DRAWERACCNAME is null");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMEIsNotNull() {
            addCriterion("DRAWERACCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMEEqualTo(String value) {
            addCriterion("DRAWERACCNAME =", value, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMENotEqualTo(String value) {
            addCriterion("DRAWERACCNAME <>", value, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMEGreaterThan(String value) {
            addCriterion("DRAWERACCNAME >", value, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("DRAWERACCNAME >=", value, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMELessThan(String value) {
            addCriterion("DRAWERACCNAME <", value, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMELessThanOrEqualTo(String value) {
            addCriterion("DRAWERACCNAME <=", value, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMELike(String value) {
            addCriterion("DRAWERACCNAME like", value, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMENotLike(String value) {
            addCriterion("DRAWERACCNAME not like", value, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMEIn(List<String> values) {
            addCriterion("DRAWERACCNAME in", values, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMENotIn(List<String> values) {
            addCriterion("DRAWERACCNAME not in", values, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMEBetween(String value1, String value2) {
            addCriterion("DRAWERACCNAME between", value1, value2, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andDRAWERACCNAMENotBetween(String value1, String value2) {
            addCriterion("DRAWERACCNAME not between", value1, value2, "DRAWERACCNAME");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2IsNull() {
            addCriterion("CHANNELTYPE2 is null");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2IsNotNull() {
            addCriterion("CHANNELTYPE2 is not null");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2EqualTo(String value) {
            addCriterion("CHANNELTYPE2 =", value, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2NotEqualTo(String value) {
            addCriterion("CHANNELTYPE2 <>", value, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2GreaterThan(String value) {
            addCriterion("CHANNELTYPE2 >", value, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2GreaterThanOrEqualTo(String value) {
            addCriterion("CHANNELTYPE2 >=", value, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2LessThan(String value) {
            addCriterion("CHANNELTYPE2 <", value, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2LessThanOrEqualTo(String value) {
            addCriterion("CHANNELTYPE2 <=", value, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2Like(String value) {
            addCriterion("CHANNELTYPE2 like", value, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2NotLike(String value) {
            addCriterion("CHANNELTYPE2 not like", value, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2In(List<String> values) {
            addCriterion("CHANNELTYPE2 in", values, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2NotIn(List<String> values) {
            addCriterion("CHANNELTYPE2 not in", values, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2Between(String value1, String value2) {
            addCriterion("CHANNELTYPE2 between", value1, value2, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andCHANNELTYPE2NotBetween(String value1, String value2) {
            addCriterion("CHANNELTYPE2 not between", value1, value2, "CHANNELTYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2IsNull() {
            addCriterion("AREATYPE2 is null");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2IsNotNull() {
            addCriterion("AREATYPE2 is not null");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2EqualTo(String value) {
            addCriterion("AREATYPE2 =", value, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2NotEqualTo(String value) {
            addCriterion("AREATYPE2 <>", value, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2GreaterThan(String value) {
            addCriterion("AREATYPE2 >", value, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2GreaterThanOrEqualTo(String value) {
            addCriterion("AREATYPE2 >=", value, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2LessThan(String value) {
            addCriterion("AREATYPE2 <", value, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2LessThanOrEqualTo(String value) {
            addCriterion("AREATYPE2 <=", value, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2Like(String value) {
            addCriterion("AREATYPE2 like", value, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2NotLike(String value) {
            addCriterion("AREATYPE2 not like", value, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2In(List<String> values) {
            addCriterion("AREATYPE2 in", values, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2NotIn(List<String> values) {
            addCriterion("AREATYPE2 not in", values, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2Between(String value1, String value2) {
            addCriterion("AREATYPE2 between", value1, value2, "AREATYPE2");
            return (Criteria) this;
        }

        public Criteria andAREATYPE2NotBetween(String value1, String value2) {
            addCriterion("AREATYPE2 not between", value1, value2, "AREATYPE2");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}