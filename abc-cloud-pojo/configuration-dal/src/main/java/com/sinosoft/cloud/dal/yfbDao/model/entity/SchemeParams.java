package com.sinosoft.cloud.dal.yfbDao.model.entity;

/**
 * 方案表要素值
 */
public class SchemeParams {
    private String id;

    private String contschemecode;

    private String contschemename;

    private String riskcode;

    private String dutyCode;

    /** 保额*/
    private String Amnt;

    /** 计算规则*/
    private String CalRule;

    /** 每日限额*/
    private String DailyLimit;

    /** 费率/折扣*/
    private String FloatRate;

    /** 免赔额*/
    private String GetLimit;

    /** 赔付比例*/
    private String GetLimitType;

    /** 免赔额类型*/
    private String GetRate;

    /** 保险期间*/
    private String InsuYear;

    /** 保险期间单位*/
    private String InsuYearFlag;

    /** 免赔天数*/
    private String NoGetDay;

    /** 给付天数(最多180天)*/
    private String PayDay;

    /** 计划名称*/
    private String PlanName;

    /** 保费*/
    private String Prem;

    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContschemecode() {
        return contschemecode;
    }

    public void setContschemecode(String contschemecode) {
        this.contschemecode = contschemecode;
    }

    public String getContschemename() {
        return contschemename;
    }

    public void setContschemename(String contschemename) {
        this.contschemename = contschemename;
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode;
    }

    public String getDutyCode() {
        return dutyCode;
    }

    public void setDutyCode(String dutyCode) {
        this.dutyCode = dutyCode;
    }

    public String getAmnt() {
        return Amnt;
    }

    public void setAmnt(String amnt) {
        Amnt = amnt;
    }

    public String getCalRule() {
        return CalRule;
    }

    public void setCalRule(String calRule) {
        CalRule = calRule;
    }

    public String getDailyLimit() {
        return DailyLimit;
    }

    public void setDailyLimit(String dailyLimit) {
        DailyLimit = dailyLimit;
    }

    public String getFloatRate() {
        return FloatRate;
    }

    public void setFloatRate(String floatRate) {
        FloatRate = floatRate;
    }

    public String getGetLimit() {
        return GetLimit;
    }

    public void setGetLimit(String getLimit) {
        GetLimit = getLimit;
    }

    public String getGetLimitType() {
        return GetLimitType;
    }

    public void setGetLimitType(String getLimitType) {
        GetLimitType = getLimitType;
    }

    public String getGetRate() {
        return GetRate;
    }

    public void setGetRate(String getRate) {
        GetRate = getRate;
    }

    public String getInsuYear() {
        return InsuYear;
    }

    public void setInsuYear(String insuYear) {
        InsuYear = insuYear;
    }

    public String getInsuYearFlag() {
        return InsuYearFlag;
    }

    public void setInsuYearFlag(String insuYearFlag) {
        InsuYearFlag = insuYearFlag;
    }

    public String getNoGetDay() {
        return NoGetDay;
    }

    public void setNoGetDay(String noGetDay) {
        NoGetDay = noGetDay;
    }

    public String getPayDay() {
        return PayDay;
    }

    public void setPayDay(String payDay) {
        PayDay = payDay;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "SchemeParams{" +
                "id='" + id + '\'' +
                ", contschemecode='" + contschemecode + '\'' +
                ", contschemename='" + contschemename + '\'' +
                ", riskcode='" + riskcode + '\'' +
                ", dutyCode='" + dutyCode + '\'' +
                ", Amnt='" + Amnt + '\'' +
                ", CalRule='" + CalRule + '\'' +
                ", DailyLimit='" + DailyLimit + '\'' +
                ", FloatRate='" + FloatRate + '\'' +
                ", GetLimit='" + GetLimit + '\'' +
                ", GetLimitType='" + GetLimitType + '\'' +
                ", GetRate='" + GetRate + '\'' +
                ", InsuYear='" + InsuYear + '\'' +
                ", InsuYearFlag='" + InsuYearFlag + '\'' +
                ", NoGetDay='" + NoGetDay + '\'' +
                ", PayDay='" + PayDay + '\'' +
                ", PlanName='" + PlanName + '\'' +
                ", Prem='" + Prem + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SchemeParams that = (SchemeParams) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (contschemecode != null ? !contschemecode.equals(that.contschemecode) : that.contschemecode != null)
            return false;
        if (contschemename != null ? !contschemename.equals(that.contschemename) : that.contschemename != null)
            return false;
        if (riskcode != null ? !riskcode.equals(that.riskcode) : that.riskcode != null) return false;
        if (dutyCode != null ? !dutyCode.equals(that.dutyCode) : that.dutyCode != null) return false;
        if (Amnt != null ? !Amnt.equals(that.Amnt) : that.Amnt != null) return false;
        if (CalRule != null ? !CalRule.equals(that.CalRule) : that.CalRule != null) return false;
        if (DailyLimit != null ? !DailyLimit.equals(that.DailyLimit) : that.DailyLimit != null) return false;
        if (FloatRate != null ? !FloatRate.equals(that.FloatRate) : that.FloatRate != null) return false;
        if (GetLimit != null ? !GetLimit.equals(that.GetLimit) : that.GetLimit != null) return false;
        if (GetLimitType != null ? !GetLimitType.equals(that.GetLimitType) : that.GetLimitType != null) return false;
        if (GetRate != null ? !GetRate.equals(that.GetRate) : that.GetRate != null) return false;
        if (InsuYear != null ? !InsuYear.equals(that.InsuYear) : that.InsuYear != null) return false;
        if (InsuYearFlag != null ? !InsuYearFlag.equals(that.InsuYearFlag) : that.InsuYearFlag != null) return false;
        if (NoGetDay != null ? !NoGetDay.equals(that.NoGetDay) : that.NoGetDay != null) return false;
        if (PayDay != null ? !PayDay.equals(that.PayDay) : that.PayDay != null) return false;
        if (PlanName != null ? !PlanName.equals(that.PlanName) : that.PlanName != null) return false;
        if (Prem != null ? !Prem.equals(that.Prem) : that.Prem != null) return false;
        return remark != null ? remark.equals(that.remark) : that.remark == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (contschemecode != null ? contschemecode.hashCode() : 0);
        result = 31 * result + (contschemename != null ? contschemename.hashCode() : 0);
        result = 31 * result + (riskcode != null ? riskcode.hashCode() : 0);
        result = 31 * result + (dutyCode != null ? dutyCode.hashCode() : 0);
        result = 31 * result + (Amnt != null ? Amnt.hashCode() : 0);
        result = 31 * result + (CalRule != null ? CalRule.hashCode() : 0);
        result = 31 * result + (DailyLimit != null ? DailyLimit.hashCode() : 0);
        result = 31 * result + (FloatRate != null ? FloatRate.hashCode() : 0);
        result = 31 * result + (GetLimit != null ? GetLimit.hashCode() : 0);
        result = 31 * result + (GetLimitType != null ? GetLimitType.hashCode() : 0);
        result = 31 * result + (GetRate != null ? GetRate.hashCode() : 0);
        result = 31 * result + (InsuYear != null ? InsuYear.hashCode() : 0);
        result = 31 * result + (InsuYearFlag != null ? InsuYearFlag.hashCode() : 0);
        result = 31 * result + (NoGetDay != null ? NoGetDay.hashCode() : 0);
        result = 31 * result + (PayDay != null ? PayDay.hashCode() : 0);
        result = 31 * result + (PlanName != null ? PlanName.hashCode() : 0);
        result = 31 * result + (Prem != null ? Prem.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }
}