package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class ManageInfo implements Serializable {

    private String manageCom;

    private String manageName;

    public String getManageCom() {
        return manageCom;
    }

    public void setManageCom(String manageCom) {
        this.manageCom = manageCom;
    }

    public String getManageName() {
        return manageName;
    }

    public void setManageName(String manageName) {
        this.manageName = manageName;
    }

    @Override
    public String toString() {
        return "ManageInfo{" +
                "manageCom='" + manageCom + '\'' +
                ", manageName='" + manageName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ManageInfo that = (ManageInfo) o;
        return Objects.equals(manageCom, that.manageCom) &&
                Objects.equals(manageName, that.manageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manageCom, manageName);
    }
}
