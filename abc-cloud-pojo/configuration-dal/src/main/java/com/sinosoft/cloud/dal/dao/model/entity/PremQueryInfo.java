package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class PremQueryInfo implements Serializable {

    private String agentName;

    private String productCode;

    private String productName;

    private int num;

    private double sumPrem;

    private String polapplydate;

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getSumPrem() {
        return sumPrem;
    }

    public void setSumPrem(double sumPrem) {
        this.sumPrem = sumPrem;
    }

    public String getPolapplydate() {
        return polapplydate;
    }

    public void setPolapplydate(String polapplydate) {
        this.polapplydate = polapplydate;
    }

    @Override
    public String toString() {
        return "PremQueryInfo{" +
                "agentName='" + agentName + '\'' +
                ", productCode='" + productCode + '\'' +
                ", productName='" + productName + '\'' +
                ", num=" + num +
                ", sumPrem=" + sumPrem +
                ", polapplydate='" + polapplydate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PremQueryInfo that = (PremQueryInfo) o;
        return num == that.num &&
                Double.compare(that.sumPrem, sumPrem) == 0 &&
                Objects.equals(agentName, that.agentName) &&
                Objects.equals(productCode, that.productCode) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(polapplydate, that.polapplydate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agentName, productCode, productName, num, sumPrem, polapplydate);
    }
}
