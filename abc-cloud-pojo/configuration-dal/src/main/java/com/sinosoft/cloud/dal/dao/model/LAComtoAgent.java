package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;

public class LAComtoAgent implements Serializable {
    /**
     * 代理机构
     *
     * @mbggenerated
     */
    private String AGENTCOM;

    /**
     * 关联类型
     *
     * @mbggenerated
     */
    private String RELATYPE;

    /**
     * 代理人编码
     *
     * @mbggenerated
     */
    private String AGENTCODE;

    /**
     * 代理人组别
     *
     * @mbggenerated
     */
    private String AGENTGROUP;

    /**
     * 操作员代码
     *
     * @mbggenerated
     */
    private String OPERATOR;

    /**
     * 入机日期
     *
     * @mbggenerated
     */
    private Date MAKEDATE;

    /**
     * 入机时间
     *
     * @mbggenerated
     */
    private String MAKETIME;

    /**
     * 最后一次修改日期
     *
     * @mbggenerated
     */
    private Date MODIFYDATE;

    /**
     * 最后一次修改时间
     *
     * @mbggenerated
     */
    private String MODIFYTIME;

    /**
     * 起始日期
     *
     * @mbggenerated
     */
    private Date STARTDATE;

    /**
     * 终止日期
     *
     * @mbggenerated
     */
    private Date ENDDATE;

    /**
     * 分配比例
     *
     * @mbggenerated
     */
    private Float RATE;

    private static final long serialVersionUID = 1L;

    public String getAGENTCOM() {
        return AGENTCOM;
    }

    public void setAGENTCOM(String AGENTCOM) {
        this.AGENTCOM = AGENTCOM;
    }

    public String getRELATYPE() {
        return RELATYPE;
    }

    public void setRELATYPE(String RELATYPE) {
        this.RELATYPE = RELATYPE;
    }

    public String getAGENTCODE() {
        return AGENTCODE;
    }

    public void setAGENTCODE(String AGENTCODE) {
        this.AGENTCODE = AGENTCODE;
    }

    public String getAGENTGROUP() {
        return AGENTGROUP;
    }

    public void setAGENTGROUP(String AGENTGROUP) {
        this.AGENTGROUP = AGENTGROUP;
    }

    public String getOPERATOR() {
        return OPERATOR;
    }

    public void setOPERATOR(String OPERATOR) {
        this.OPERATOR = OPERATOR;
    }

    public Date getMAKEDATE() {
        return MAKEDATE;
    }

    public void setMAKEDATE(Date MAKEDATE) {
        this.MAKEDATE = MAKEDATE;
    }

    public String getMAKETIME() {
        return MAKETIME;
    }

    public void setMAKETIME(String MAKETIME) {
        this.MAKETIME = MAKETIME;
    }

    public Date getMODIFYDATE() {
        return MODIFYDATE;
    }

    public void setMODIFYDATE(Date MODIFYDATE) {
        this.MODIFYDATE = MODIFYDATE;
    }

    public String getMODIFYTIME() {
        return MODIFYTIME;
    }

    public void setMODIFYTIME(String MODIFYTIME) {
        this.MODIFYTIME = MODIFYTIME;
    }

    public Date getSTARTDATE() {
        return STARTDATE;
    }

    public void setSTARTDATE(Date STARTDATE) {
        this.STARTDATE = STARTDATE;
    }

    public Date getENDDATE() {
        return ENDDATE;
    }

    public void setENDDATE(Date ENDDATE) {
        this.ENDDATE = ENDDATE;
    }

    public Float getRATE() {
        return RATE;
    }

    public void setRATE(Float RATE) {
        this.RATE = RATE;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", AGENTCOM=").append(AGENTCOM);
        sb.append(", RELATYPE=").append(RELATYPE);
        sb.append(", AGENTCODE=").append(AGENTCODE);
        sb.append(", AGENTGROUP=").append(AGENTGROUP);
        sb.append(", OPERATOR=").append(OPERATOR);
        sb.append(", MAKEDATE=").append(MAKEDATE);
        sb.append(", MAKETIME=").append(MAKETIME);
        sb.append(", MODIFYDATE=").append(MODIFYDATE);
        sb.append(", MODIFYTIME=").append(MODIFYTIME);
        sb.append(", STARTDATE=").append(STARTDATE);
        sb.append(", ENDDATE=").append(ENDDATE);
        sb.append(", RATE=").append(RATE);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LAComtoAgent other = (LAComtoAgent) that;
        return (this.getAGENTCOM() == null ? other.getAGENTCOM() == null : this.getAGENTCOM().equals(other.getAGENTCOM()))
            && (this.getRELATYPE() == null ? other.getRELATYPE() == null : this.getRELATYPE().equals(other.getRELATYPE()))
            && (this.getAGENTCODE() == null ? other.getAGENTCODE() == null : this.getAGENTCODE().equals(other.getAGENTCODE()))
            && (this.getAGENTGROUP() == null ? other.getAGENTGROUP() == null : this.getAGENTGROUP().equals(other.getAGENTGROUP()))
            && (this.getOPERATOR() == null ? other.getOPERATOR() == null : this.getOPERATOR().equals(other.getOPERATOR()))
            && (this.getMAKEDATE() == null ? other.getMAKEDATE() == null : this.getMAKEDATE().equals(other.getMAKEDATE()))
            && (this.getMAKETIME() == null ? other.getMAKETIME() == null : this.getMAKETIME().equals(other.getMAKETIME()))
            && (this.getMODIFYDATE() == null ? other.getMODIFYDATE() == null : this.getMODIFYDATE().equals(other.getMODIFYDATE()))
            && (this.getMODIFYTIME() == null ? other.getMODIFYTIME() == null : this.getMODIFYTIME().equals(other.getMODIFYTIME()))
            && (this.getSTARTDATE() == null ? other.getSTARTDATE() == null : this.getSTARTDATE().equals(other.getSTARTDATE()))
            && (this.getENDDATE() == null ? other.getENDDATE() == null : this.getENDDATE().equals(other.getENDDATE()))
            && (this.getRATE() == null ? other.getRATE() == null : this.getRATE().equals(other.getRATE()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAGENTCOM() == null) ? 0 : getAGENTCOM().hashCode());
        result = prime * result + ((getRELATYPE() == null) ? 0 : getRELATYPE().hashCode());
        result = prime * result + ((getAGENTCODE() == null) ? 0 : getAGENTCODE().hashCode());
        result = prime * result + ((getAGENTGROUP() == null) ? 0 : getAGENTGROUP().hashCode());
        result = prime * result + ((getOPERATOR() == null) ? 0 : getOPERATOR().hashCode());
        result = prime * result + ((getMAKEDATE() == null) ? 0 : getMAKEDATE().hashCode());
        result = prime * result + ((getMAKETIME() == null) ? 0 : getMAKETIME().hashCode());
        result = prime * result + ((getMODIFYDATE() == null) ? 0 : getMODIFYDATE().hashCode());
        result = prime * result + ((getMODIFYTIME() == null) ? 0 : getMODIFYTIME().hashCode());
        result = prime * result + ((getSTARTDATE() == null) ? 0 : getSTARTDATE().hashCode());
        result = prime * result + ((getENDDATE() == null) ? 0 : getENDDATE().hashCode());
        result = prime * result + ((getRATE() == null) ? 0 : getRATE().hashCode());
        return result;
    }
}