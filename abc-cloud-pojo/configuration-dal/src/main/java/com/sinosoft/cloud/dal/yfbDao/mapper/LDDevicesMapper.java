package com.sinosoft.cloud.dal.yfbDao.mapper;


import com.sinosoft.cloud.dal.dao.model.LDDevices;
import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.LcGrpCont;
import com.sinosoft.cloud.dal.yfbDao.model.entity.ScheduleInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

//import com.sinosoft.cloud.dal.yfbDao.model.entity.PremSumInfo;

public interface LDDevicesMapper {
    //
    int countScheduleList(Map<String, Object> map);
    //
    List<LDDevices> getScheduleList(Map<String, Object> map);
    //
//    @Delete("delete from lddevices where  deviceNo = #{deviceNo}")
    Integer deleteCodeMaintains(@Param(value = "deviceNo") String deviceNo);
    //
    Integer insertDevices(Map<String,Object> map);
}