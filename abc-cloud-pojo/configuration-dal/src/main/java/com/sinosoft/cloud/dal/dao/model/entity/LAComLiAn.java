/**
 * Copyright (c) 2019 sinosoft Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.cloud.dal.dao.model.entity;

import java.util.Date;

/**
xcc lian
 */
public class LAComLiAn {

    /**
     * 序列
     */
    private String proId;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }
    // @Field
    /** 代理机构 */
    private String AgentCom;
    /** 管理机构 */
    private String ManageCom;
    /** 地区类型 */
    private String AreaType;
    /** 渠道类型 */
    private String ChannelType;
    /** 上级代理机构 */
    private String UpAgentCom;
    /** 机构名称 */
    private String Name;
    /** 机构注册地址 */
    private String Address;
    /** 机构邮编 */
    private String ZipCode;
    /** 机构电话 */
    private String Phone;
    /** 机构传真 */
    private String Fax;
    /** Email */
    private String EMail;
    /** 网址 */
    private String WebAddress;
    /** 负责人 */
    private String LinkMan;
    /** 密码 */
    private String Password;
    /** 法人 */
    private String Corporation;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 行业分类 */
    private String BusinessType;
    /** 单位性质 */
    private String GrpNature;
    /** 中介机构类别 */
    private String ACType;
    /** 销售资格 */
    private String SellFlag;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 银行级别 */
    private String BankType;
    /** 是否统计网点合格率 */
    private String CalFlag;
    /** 工商执照编码 */
    private String BusiLicenseCode;
    /** 保险公司id */
    private String InsureID;
    /** 保险公司负责人 */
    private String InsurePrincipal;
    /** 主营业务 */
    private String ChiefBusiness;
    /** 营业地址 */
    private String BusiAddress;
    /** 签署人 */
    private String SubscribeMan;
    /** 签署人职务 */
    private String SubscribeManDuty;
    /** 许可证号码 */
    private String LicenseNo;
    /** 行政区划代码 */
    private String RegionalismCode;
    /** 上报代码 */
    private String AppAgentCom;
    /** 机构状态 */
    private String State;
    /** 相关说明 */
    private String Noti;
    /** 行业代码 */
    private String BusinessCode;
    /** 许可证登记日期 */
    private Date LicenseStartDate;
    /** 许可证截至日期 */
    private Date LicenseEndDate;
    /** 展业类型 */
    private String BranchType;
    /** 渠道 */
    private String BranchType2;
    /** 资产 */
    private double Assets;
    /** 营业收入 */
    private double Income;
    /** 营业利润 */
    private double Profits;
    /** 机构人数 */
    private int PersonnalSum;
    /** 合同编码 */
    private String ProtocalNo;
    /** 所属总行 */
    private String HeadOffice;
    /** 成立日期 */
    private Date FoundDate;
    /** 停业日期 */
    private Date EndDate;
    /** 开户银行 */
    private String Bank;
    /** 账户名称 */
    private String AccName;
    /** 授权领取人姓名 */
    private String DraWer;
    /** 授权领取人银行账号 */
    private String DraWerAccNo;
    /** 上级管理机构 */
    private String RepManageCom;
    /** 授权领取人银行编码 */
    private String DraWerAccCode;
    /** 授权领取人银行名称 */
    private String DraWerAccName;
    /** 中介机构渠道类别 */
    private String ChannelType2;
    /** 区域类型 */
    private String AreaType2;
    /** 机构人员关系有效标识 */
    private String ComToAgentflag;
    /** 代理人编码 */
    private String AgentCode;
    /** 机构编码 */
    private String AgentComCode;
    /** 中介合作银行 */
    private String CooperateBank;
    /** 中介合作协议终止日期 */
    private Date CooperationEndDate;
    /** 中介合作协议起始日期 */
    private Date CooperationStartDate;
    /** 代理机构类别 */
    private String AgentType;
    /** 新银行代码 */
    private String NewBankCode;
    /** 银行所在城市 */
    private String BankCity;


    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String agentCom) {
        AgentCom = agentCom;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getAreaType() {
        return AreaType;
    }

    public void setAreaType(String areaType) {
        AreaType = areaType;
    }

    public String getChannelType() {
        return ChannelType;
    }

    public void setChannelType(String channelType) {
        ChannelType = channelType;
    }

    public String getUpAgentCom() {
        return UpAgentCom;
    }

    public void setUpAgentCom(String upAgentCom) {
        UpAgentCom = upAgentCom;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public String getWebAddress() {
        return WebAddress;
    }

    public void setWebAddress(String webAddress) {
        WebAddress = webAddress;
    }

    public String getLinkMan() {
        return LinkMan;
    }

    public void setLinkMan(String linkMan) {
        LinkMan = linkMan;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getCorporation() {
        return Corporation;
    }

    public void setCorporation(String corporation) {
        Corporation = corporation;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBankAccNo() {
        return BankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        BankAccNo = bankAccNo;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getGrpNature() {
        return GrpNature;
    }

    public void setGrpNature(String grpNature) {
        GrpNature = grpNature;
    }

    public String getACType() {
        return ACType;
    }

    public void setACType(String ACType) {
        this.ACType = ACType;
    }

    public String getSellFlag() {
        return SellFlag;
    }

    public void setSellFlag(String sellFlag) {
        SellFlag = sellFlag;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public Date getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(Date makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public Date getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getBankType() {
        return BankType;
    }

    public void setBankType(String bankType) {
        BankType = bankType;
    }

    public String getCalFlag() {
        return CalFlag;
    }

    public void setCalFlag(String calFlag) {
        CalFlag = calFlag;
    }

    public String getBusiLicenseCode() {
        return BusiLicenseCode;
    }

    public void setBusiLicenseCode(String busiLicenseCode) {
        BusiLicenseCode = busiLicenseCode;
    }

    public String getInsureID() {
        return InsureID;
    }

    public void setInsureID(String insureID) {
        InsureID = insureID;
    }

    public String getInsurePrincipal() {
        return InsurePrincipal;
    }

    public void setInsurePrincipal(String insurePrincipal) {
        InsurePrincipal = insurePrincipal;
    }

    public String getChiefBusiness() {
        return ChiefBusiness;
    }

    public void setChiefBusiness(String chiefBusiness) {
        ChiefBusiness = chiefBusiness;
    }

    public String getBusiAddress() {
        return BusiAddress;
    }

    public void setBusiAddress(String busiAddress) {
        BusiAddress = busiAddress;
    }

    public String getSubscribeMan() {
        return SubscribeMan;
    }

    public void setSubscribeMan(String subscribeMan) {
        SubscribeMan = subscribeMan;
    }

    public String getSubscribeManDuty() {
        return SubscribeManDuty;
    }

    public void setSubscribeManDuty(String subscribeManDuty) {
        SubscribeManDuty = subscribeManDuty;
    }

    public String getLicenseNo() {
        return LicenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        LicenseNo = licenseNo;
    }

    public String getRegionalismCode() {
        return RegionalismCode;
    }

    public void setRegionalismCode(String regionalismCode) {
        RegionalismCode = regionalismCode;
    }

    public String getAppAgentCom() {
        return AppAgentCom;
    }

    public void setAppAgentCom(String appAgentCom) {
        AppAgentCom = appAgentCom;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getNoti() {
        return Noti;
    }

    public void setNoti(String noti) {
        Noti = noti;
    }

    public String getBusinessCode() {
        return BusinessCode;
    }

    public void setBusinessCode(String businessCode) {
        BusinessCode = businessCode;
    }

    public Date getLicenseStartDate() {
        return LicenseStartDate;
    }

    public void setLicenseStartDate(Date licenseStartDate) {
        LicenseStartDate = licenseStartDate;
    }

    public Date getLicenseEndDate() {
        return LicenseEndDate;
    }

    public void setLicenseEndDate(Date licenseEndDate) {
        LicenseEndDate = licenseEndDate;
    }

    public String getBranchType() {
        return BranchType;
    }

    public void setBranchType(String branchType) {
        BranchType = branchType;
    }

    public String getBranchType2() {
        return BranchType2;
    }

    public void setBranchType2(String branchType2) {
        BranchType2 = branchType2;
    }

    public double getAssets() {
        return Assets;
    }

    public void setAssets(double assets) {
        Assets = assets;
    }

    public double getIncome() {
        return Income;
    }

    public void setIncome(double income) {
        Income = income;
    }

    public double getProfits() {
        return Profits;
    }

    public void setProfits(double profits) {
        Profits = profits;
    }

    public int getPersonnalSum() {
        return PersonnalSum;
    }

    public void setPersonnalSum(int personnalSum) {
        PersonnalSum = personnalSum;
    }

    public String getProtocalNo() {
        return ProtocalNo;
    }

    public void setProtocalNo(String protocalNo) {
        ProtocalNo = protocalNo;
    }

    public String getHeadOffice() {
        return HeadOffice;
    }

    public void setHeadOffice(String headOffice) {
        HeadOffice = headOffice;
    }

    public Date getFoundDate() {
        return FoundDate;
    }

    public void setFoundDate(Date foundDate) {
        FoundDate = foundDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date endDate) {
        EndDate = endDate;
    }

    public String getBank() {
        return Bank;
    }

    public void setBank(String bank) {
        Bank = bank;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String accName) {
        AccName = accName;
    }

    public String getDraWer() {
        return DraWer;
    }

    public void setDraWer(String draWer) {
        DraWer = draWer;
    }

    public String getDraWerAccNo() {
        return DraWerAccNo;
    }

    public void setDraWerAccNo(String draWerAccNo) {
        DraWerAccNo = draWerAccNo;
    }

    public String getRepManageCom() {
        return RepManageCom;
    }

    public void setRepManageCom(String repManageCom) {
        RepManageCom = repManageCom;
    }

    public String getDraWerAccCode() {
        return DraWerAccCode;
    }

    public void setDraWerAccCode(String draWerAccCode) {
        DraWerAccCode = draWerAccCode;
    }

    public String getDraWerAccName() {
        return DraWerAccName;
    }

    public void setDraWerAccName(String draWerAccName) {
        DraWerAccName = draWerAccName;
    }

    public String getChannelType2() {
        return ChannelType2;
    }

    public void setChannelType2(String channelType2) {
        ChannelType2 = channelType2;
    }

    public String getAreaType2() {
        return AreaType2;
    }

    public void setAreaType2(String areaType2) {
        AreaType2 = areaType2;
    }

    public String getComToAgentflag() {
        return ComToAgentflag;
    }

    public void setComToAgentflag(String comToAgentflag) {
        ComToAgentflag = comToAgentflag;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String agentCode) {
        AgentCode = agentCode;
    }

    public String getAgentComCode() {
        return AgentComCode;
    }

    public void setAgentComCode(String agentComCode) {
        AgentComCode = agentComCode;
    }

    public String getCooperateBank() {
        return CooperateBank;
    }

    public void setCooperateBank(String cooperateBank) {
        CooperateBank = cooperateBank;
    }

    public Date getCooperationEndDate() {
        return CooperationEndDate;
    }

    public void setCooperationEndDate(Date cooperationEndDate) {
        CooperationEndDate = cooperationEndDate;
    }

    public Date getCooperationStartDate() {
        return CooperationStartDate;
    }

    public void setCooperationStartDate(Date cooperationStartDate) {
        CooperationStartDate = cooperationStartDate;
    }

    public String getAgentType() {
        return AgentType;
    }

    public void setAgentType(String agentType) {
        AgentType = agentType;
    }

    public String getNewBankCode() {
        return NewBankCode;
    }

    public void setNewBankCode(String newBankCode) {
        NewBankCode = newBankCode;
    }

    public String getBankCity() {
        return BankCity;
    }

    public void setBankCity(String bankCity) {
        BankCity = bankCity;
    }

    public LAComLiAn() {
    }

    public LAComLiAn(String proId, String agentCom, String manageCom, String areaType, String channelType, String upAgentCom, String name, String address, String zipCode, String phone, String fax, String EMail, String webAddress, String linkMan, String password, String corporation, String bankCode, String bankAccNo, String businessType, String grpNature, String ACType, String sellFlag, String operator, Date makeDate, String makeTime, Date modifyDate, String modifyTime, String bankType, String calFlag, String busiLicenseCode, String insureID, String insurePrincipal, String chiefBusiness, String busiAddress, String subscribeMan, String subscribeManDuty, String licenseNo, String regionalismCode, String appAgentCom, String state, String noti, String businessCode, Date licenseStartDate, Date licenseEndDate, String branchType, String branchType2, double assets, double income, double profits, int personnalSum, String protocalNo, String headOffice, Date foundDate, Date endDate, String bank, String accName, String draWer, String draWerAccNo, String repManageCom, String draWerAccCode, String draWerAccName, String channelType2, String areaType2, String comToAgentflag, String agentCode, String agentComCode, String cooperateBank, Date cooperationEndDate, Date cooperationStartDate, String agentType, String newBankCode, String bankCity) {
        this.proId = proId;
        AgentCom = agentCom;
        ManageCom = manageCom;
        AreaType = areaType;
        ChannelType = channelType;
        UpAgentCom = upAgentCom;
        Name = name;
        Address = address;
        ZipCode = zipCode;
        Phone = phone;
        Fax = fax;
        this.EMail = EMail;
        WebAddress = webAddress;
        LinkMan = linkMan;
        Password = password;
        Corporation = corporation;
        BankCode = bankCode;
        BankAccNo = bankAccNo;
        BusinessType = businessType;
        GrpNature = grpNature;
        this.ACType = ACType;
        SellFlag = sellFlag;
        Operator = operator;
        MakeDate = makeDate;
        MakeTime = makeTime;
        ModifyDate = modifyDate;
        ModifyTime = modifyTime;
        BankType = bankType;
        CalFlag = calFlag;
        BusiLicenseCode = busiLicenseCode;
        InsureID = insureID;
        InsurePrincipal = insurePrincipal;
        ChiefBusiness = chiefBusiness;
        BusiAddress = busiAddress;
        SubscribeMan = subscribeMan;
        SubscribeManDuty = subscribeManDuty;
        LicenseNo = licenseNo;
        RegionalismCode = regionalismCode;
        AppAgentCom = appAgentCom;
        State = state;
        Noti = noti;
        BusinessCode = businessCode;
        LicenseStartDate = licenseStartDate;
        LicenseEndDate = licenseEndDate;
        BranchType = branchType;
        BranchType2 = branchType2;
        Assets = assets;
        Income = income;
        Profits = profits;
        PersonnalSum = personnalSum;
        ProtocalNo = protocalNo;
        HeadOffice = headOffice;
        FoundDate = foundDate;
        EndDate = endDate;
        Bank = bank;
        AccName = accName;
        DraWer = draWer;
        DraWerAccNo = draWerAccNo;
        RepManageCom = repManageCom;
        DraWerAccCode = draWerAccCode;
        DraWerAccName = draWerAccName;
        ChannelType2 = channelType2;
        AreaType2 = areaType2;
        ComToAgentflag = comToAgentflag;
        AgentCode = agentCode;
        AgentComCode = agentComCode;
        CooperateBank = cooperateBank;
        CooperationEndDate = cooperationEndDate;
        CooperationStartDate = cooperationStartDate;
        AgentType = agentType;
        NewBankCode = newBankCode;
        BankCity = bankCity;
    }
}
