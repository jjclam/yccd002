package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class LAChannelProductExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public LAChannelProductExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andChannelCodeIsNull() {
            addCriterion("ChannelCode is null");
            return (Criteria) this;
        }

        public Criteria andChannelCodeIsNotNull() {
            addCriterion("ChannelCode is not null");
            return (Criteria) this;
        }

        public Criteria andChannelCodeEqualTo(String value) {
            addCriterion("ChannelCode =", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeNotEqualTo(String value) {
            addCriterion("ChannelCode <>", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeGreaterThan(String value) {
            addCriterion("ChannelCode >", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("ChannelCode >=", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeLessThan(String value) {
            addCriterion("ChannelCode <", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeLessThanOrEqualTo(String value) {
            addCriterion("ChannelCode <=", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeLike(String value) {
            addCriterion("ChannelCode like", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeNotLike(String value) {
            addCriterion("ChannelCode not like", value, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeIn(List<String> values) {
            addCriterion("ChannelCode in", values, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeNotIn(List<String> values) {
            addCriterion("ChannelCode not in", values, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeBetween(String value1, String value2) {
            addCriterion("ChannelCode between", value1, value2, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelCodeNotBetween(String value1, String value2) {
            addCriterion("ChannelCode not between", value1, value2, "channelCode");
            return (Criteria) this;
        }

        public Criteria andChannelNameIsNull() {
            addCriterion("ChannelName is null");
            return (Criteria) this;
        }

        public Criteria andChannelNameIsNotNull() {
            addCriterion("ChannelName is not null");
            return (Criteria) this;
        }

        public Criteria andChannelNameEqualTo(String value) {
            addCriterion("ChannelName =", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameNotEqualTo(String value) {
            addCriterion("ChannelName <>", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameGreaterThan(String value) {
            addCriterion("ChannelName >", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameGreaterThanOrEqualTo(String value) {
            addCriterion("ChannelName >=", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameLessThan(String value) {
            addCriterion("ChannelName <", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameLessThanOrEqualTo(String value) {
            addCriterion("ChannelName <=", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameLike(String value) {
            addCriterion("ChannelName like", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameNotLike(String value) {
            addCriterion("ChannelName not like", value, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameIn(List<String> values) {
            addCriterion("ChannelName in", values, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameNotIn(List<String> values) {
            addCriterion("ChannelName not in", values, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameBetween(String value1, String value2) {
            addCriterion("ChannelName between", value1, value2, "channelName");
            return (Criteria) this;
        }

        public Criteria andChannelNameNotBetween(String value1, String value2) {
            addCriterion("ChannelName not between", value1, value2, "channelName");
            return (Criteria) this;
        }

        public Criteria andRiskCodeIsNull() {
            addCriterion("RiskCode is null");
            return (Criteria) this;
        }

        public Criteria andRiskCodeIsNotNull() {
            addCriterion("RiskCode is not null");
            return (Criteria) this;
        }

        public Criteria andRiskCodeEqualTo(String value) {
            addCriterion("RiskCode =", value, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeNotEqualTo(String value) {
            addCriterion("RiskCode <>", value, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeGreaterThan(String value) {
            addCriterion("RiskCode >", value, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeGreaterThanOrEqualTo(String value) {
            addCriterion("RiskCode >=", value, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeLessThan(String value) {
            addCriterion("RiskCode <", value, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeLessThanOrEqualTo(String value) {
            addCriterion("RiskCode <=", value, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeLike(String value) {
            addCriterion("RiskCode like", value, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeNotLike(String value) {
            addCriterion("RiskCode not like", value, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeIn(List<String> values) {
            addCriterion("RiskCode in", values, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeNotIn(List<String> values) {
            addCriterion("RiskCode not in", values, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeBetween(String value1, String value2) {
            addCriterion("RiskCode between", value1, value2, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskCodeNotBetween(String value1, String value2) {
            addCriterion("RiskCode not between", value1, value2, "riskCode");
            return (Criteria) this;
        }

        public Criteria andRiskNameIsNull() {
            addCriterion("RiskName is null");
            return (Criteria) this;
        }

        public Criteria andRiskNameIsNotNull() {
            addCriterion("RiskName is not null");
            return (Criteria) this;
        }

        public Criteria andRiskNameEqualTo(String value) {
            addCriterion("RiskName =", value, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameNotEqualTo(String value) {
            addCriterion("RiskName <>", value, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameGreaterThan(String value) {
            addCriterion("RiskName >", value, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameGreaterThanOrEqualTo(String value) {
            addCriterion("RiskName >=", value, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameLessThan(String value) {
            addCriterion("RiskName <", value, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameLessThanOrEqualTo(String value) {
            addCriterion("RiskName <=", value, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameLike(String value) {
            addCriterion("RiskName like", value, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameNotLike(String value) {
            addCriterion("RiskName not like", value, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameIn(List<String> values) {
            addCriterion("RiskName in", values, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameNotIn(List<String> values) {
            addCriterion("RiskName not in", values, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameBetween(String value1, String value2) {
            addCriterion("RiskName between", value1, value2, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskNameNotBetween(String value1, String value2) {
            addCriterion("RiskName not between", value1, value2, "riskName");
            return (Criteria) this;
        }

        public Criteria andRiskTypeIsNull() {
            addCriterion("RiskType is null");
            return (Criteria) this;
        }

        public Criteria andRiskTypeIsNotNull() {
            addCriterion("RiskType is not null");
            return (Criteria) this;
        }

        public Criteria andRiskTypeEqualTo(String value) {
            addCriterion("RiskType =", value, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeNotEqualTo(String value) {
            addCriterion("RiskType <>", value, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeGreaterThan(String value) {
            addCriterion("RiskType >", value, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeGreaterThanOrEqualTo(String value) {
            addCriterion("RiskType >=", value, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeLessThan(String value) {
            addCriterion("RiskType <", value, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeLessThanOrEqualTo(String value) {
            addCriterion("RiskType <=", value, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeLike(String value) {
            addCriterion("RiskType like", value, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeNotLike(String value) {
            addCriterion("RiskType not like", value, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeIn(List<String> values) {
            addCriterion("RiskType in", values, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeNotIn(List<String> values) {
            addCriterion("RiskType not in", values, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeBetween(String value1, String value2) {
            addCriterion("RiskType between", value1, value2, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskTypeNotBetween(String value1, String value2) {
            addCriterion("RiskType not between", value1, value2, "riskType");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateIsNull() {
            addCriterion("RiskStartSellDate is null");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateIsNotNull() {
            addCriterion("RiskStartSellDate is not null");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateEqualTo(Date value) {
            addCriterionForJDBCDate("RiskStartSellDate =", value, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("RiskStartSellDate <>", value, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateGreaterThan(Date value) {
            addCriterionForJDBCDate("RiskStartSellDate >", value, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("RiskStartSellDate >=", value, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateLessThan(Date value) {
            addCriterionForJDBCDate("RiskStartSellDate <", value, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("RiskStartSellDate <=", value, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateIn(List<Date> values) {
            addCriterionForJDBCDate("RiskStartSellDate in", values, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("RiskStartSellDate not in", values, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("RiskStartSellDate between", value1, value2, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskStartSellDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("RiskStartSellDate not between", value1, value2, "riskStartSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateIsNull() {
            addCriterion("RiskEndSellDate is null");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateIsNotNull() {
            addCriterion("RiskEndSellDate is not null");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateEqualTo(Date value) {
            addCriterionForJDBCDate("RiskEndSellDate =", value, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("RiskEndSellDate <>", value, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateGreaterThan(Date value) {
            addCriterionForJDBCDate("RiskEndSellDate >", value, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("RiskEndSellDate >=", value, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateLessThan(Date value) {
            addCriterionForJDBCDate("RiskEndSellDate <", value, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("RiskEndSellDate <=", value, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateIn(List<Date> values) {
            addCriterionForJDBCDate("RiskEndSellDate in", values, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("RiskEndSellDate not in", values, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("RiskEndSellDate between", value1, value2, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andRiskEndSellDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("RiskEndSellDate not between", value1, value2, "riskEndSellDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("CreateDate is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("CreateDate is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterionForJDBCDate("CreateDate =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("CreateDate <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterionForJDBCDate("CreateDate >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("CreateDate >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterionForJDBCDate("CreateDate <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("CreateDate <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterionForJDBCDate("CreateDate in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("CreateDate not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("CreateDate between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("CreateDate not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateIsNull() {
            addCriterion("MakeDate is null");
            return (Criteria) this;
        }

        public Criteria andMakeDateIsNotNull() {
            addCriterion("MakeDate is not null");
            return (Criteria) this;
        }

        public Criteria andMakeDateEqualTo(Date value) {
            addCriterionForJDBCDate("MakeDate =", value, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("MakeDate <>", value, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateGreaterThan(Date value) {
            addCriterionForJDBCDate("MakeDate >", value, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MakeDate >=", value, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateLessThan(Date value) {
            addCriterionForJDBCDate("MakeDate <", value, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MakeDate <=", value, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateIn(List<Date> values) {
            addCriterionForJDBCDate("MakeDate in", values, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("MakeDate not in", values, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MakeDate between", value1, value2, "makeDate");
            return (Criteria) this;
        }

        public Criteria andMakeDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MakeDate not between", value1, value2, "makeDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNull() {
            addCriterion("ModifyDate is null");
            return (Criteria) this;
        }

        public Criteria andModifyDateIsNotNull() {
            addCriterion("ModifyDate is not null");
            return (Criteria) this;
        }

        public Criteria andModifyDateEqualTo(Date value) {
            addCriterionForJDBCDate("ModifyDate =", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("ModifyDate <>", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThan(Date value) {
            addCriterionForJDBCDate("ModifyDate >", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ModifyDate >=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThan(Date value) {
            addCriterionForJDBCDate("ModifyDate <", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ModifyDate <=", value, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateIn(List<Date> values) {
            addCriterionForJDBCDate("ModifyDate in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("ModifyDate not in", values, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ModifyDate between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andModifyDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ModifyDate not between", value1, value2, "modifyDate");
            return (Criteria) this;
        }

        public Criteria andH5hrefIsNull() {
            addCriterion("H5href is null");
            return (Criteria) this;
        }

        public Criteria andH5hrefIsNotNull() {
            addCriterion("H5href is not null");
            return (Criteria) this;
        }

        public Criteria andH5hrefEqualTo(String value) {
            addCriterion("H5href =", value, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefNotEqualTo(String value) {
            addCriterion("H5href <>", value, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefGreaterThan(String value) {
            addCriterion("H5href >", value, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefGreaterThanOrEqualTo(String value) {
            addCriterion("H5href >=", value, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefLessThan(String value) {
            addCriterion("H5href <", value, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefLessThanOrEqualTo(String value) {
            addCriterion("H5href <=", value, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefLike(String value) {
            addCriterion("H5href like", value, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefNotLike(String value) {
            addCriterion("H5href not like", value, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefIn(List<String> values) {
            addCriterion("H5href in", values, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefNotIn(List<String> values) {
            addCriterion("H5href not in", values, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefBetween(String value1, String value2) {
            addCriterion("H5href between", value1, value2, "h5href");
            return (Criteria) this;
        }

        public Criteria andH5hrefNotBetween(String value1, String value2) {
            addCriterion("H5href not between", value1, value2, "h5href");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("Status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("Status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagIsNull() {
            addCriterion("RnewageFlag is null");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagIsNotNull() {
            addCriterion("RnewageFlag is not null");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagEqualTo(String value) {
            addCriterion("RnewageFlag =", value, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagNotEqualTo(String value) {
            addCriterion("RnewageFlag <>", value, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagGreaterThan(String value) {
            addCriterion("RnewageFlag >", value, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagGreaterThanOrEqualTo(String value) {
            addCriterion("RnewageFlag >=", value, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagLessThan(String value) {
            addCriterion("RnewageFlag <", value, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagLessThanOrEqualTo(String value) {
            addCriterion("RnewageFlag <=", value, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagLike(String value) {
            addCriterion("RnewageFlag like", value, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagNotLike(String value) {
            addCriterion("RnewageFlag not like", value, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagIn(List<String> values) {
            addCriterion("RnewageFlag in", values, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagNotIn(List<String> values) {
            addCriterion("RnewageFlag not in", values, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagBetween(String value1, String value2) {
            addCriterion("RnewageFlag between", value1, value2, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andRnewageFlagNotBetween(String value1, String value2) {
            addCriterion("RnewageFlag not between", value1, value2, "rnewageFlag");
            return (Criteria) this;
        }

        public Criteria andIsConfigIsNull() {
            addCriterion("IsConfig is null");
            return (Criteria) this;
        }

        public Criteria andIsConfigIsNotNull() {
            addCriterion("IsConfig is not null");
            return (Criteria) this;
        }

        public Criteria andIsConfigEqualTo(String value) {
            addCriterion("IsConfig =", value, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigNotEqualTo(String value) {
            addCriterion("IsConfig <>", value, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigGreaterThan(String value) {
            addCriterion("IsConfig >", value, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigGreaterThanOrEqualTo(String value) {
            addCriterion("IsConfig >=", value, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigLessThan(String value) {
            addCriterion("IsConfig <", value, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigLessThanOrEqualTo(String value) {
            addCriterion("IsConfig <=", value, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigLike(String value) {
            addCriterion("IsConfig like", value, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigNotLike(String value) {
            addCriterion("IsConfig not like", value, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigIn(List<String> values) {
            addCriterion("IsConfig in", values, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigNotIn(List<String> values) {
            addCriterion("IsConfig not in", values, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigBetween(String value1, String value2) {
            addCriterion("IsConfig between", value1, value2, "isConfig");
            return (Criteria) this;
        }

        public Criteria andIsConfigNotBetween(String value1, String value2) {
            addCriterion("IsConfig not between", value1, value2, "isConfig");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeIsNull() {
            addCriterion("UpDownType is null");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeIsNotNull() {
            addCriterion("UpDownType is not null");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeEqualTo(String value) {
            addCriterion("UpDownType =", value, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeNotEqualTo(String value) {
            addCriterion("UpDownType <>", value, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeGreaterThan(String value) {
            addCriterion("UpDownType >", value, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeGreaterThanOrEqualTo(String value) {
            addCriterion("UpDownType >=", value, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeLessThan(String value) {
            addCriterion("UpDownType <", value, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeLessThanOrEqualTo(String value) {
            addCriterion("UpDownType <=", value, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeLike(String value) {
            addCriterion("UpDownType like", value, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeNotLike(String value) {
            addCriterion("UpDownType not like", value, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeIn(List<String> values) {
            addCriterion("UpDownType in", values, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeNotIn(List<String> values) {
            addCriterion("UpDownType not in", values, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeBetween(String value1, String value2) {
            addCriterion("UpDownType between", value1, value2, "upDownType");
            return (Criteria) this;
        }

        public Criteria andUpDownTypeNotBetween(String value1, String value2) {
            addCriterion("UpDownType not between", value1, value2, "upDownType");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("Remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("Remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("Remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("Remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("Remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("Remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("Remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("Remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("Remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("Remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("Remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("Remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("Remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("Remark not between", value1, value2, "remark");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}