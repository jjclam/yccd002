package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.SmsChlPro;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SmsChlProMapper {
    List<SmsChlPro> selectForSmsParamsList(Map<String,Object> map);

    Integer countForSmsParamsList(Map<String,Object> map);

    Integer verifySmsChlPro(@Param(value = "channelCode") String channelCode, @Param(value = "riskCode") String proCode);

    Integer addSmsMaintain(Map<String,Object> map);

    Integer updateSmsMaintain(Map<String,Object> map);

    Integer deleteSmsMaintain(Map<String,Object> params);

    SmsChlPro getSmsEditInfo(@Param(value = "channelCode") String channelCode, @Param(value = "riskCode") String proCode);
}