package com.sinosoft.cloud.dal.dao.mapper;


import com.sinosoft.cloud.dal.dao.model.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author J
 * 2019-12-09 0:00
 */

public interface SysUserMapper {
    SysUser CheckUser(@Param("userName") String username, @Param("password") String password);

    int createUser(Map<String,Object> map);

    int verifyUser(@Param("userName") String userName);

    Integer addUser(Map<String,Object> map);

    int countUserByName(Map<String, Object> map);

    List<SysUser> getUserInfoList(Map<String,Object> map);

    Integer deleteUser(@Param("userName") String userName);

    SysUser getUserInfoForEdit(@Param("userName") String userNames);

    Integer updateUser(Map<String,Object> map);

    List<SysUser> getUserPosition(@Param("field") String field);

    SysUser selectUserIdForUserPosition(@Param("userName") String userName);

    Integer insertUserPosition(@Param("userId") String userId, @Param("positionId") String positionId);

    Integer updateUserPosition(@Param("userId") String roleId, @Param("positionId") String positionId);

    Integer deleteUserPosition(@Param("userId") String roleId, @Param("positionId") String positionId);

    Integer addUserRole(Map<String,Object> map);

    Integer deleteUserRole(@Param("userName") String userName);

    List<SysUser> getUserInfoList1(Map<String, Object> userMap);
}
