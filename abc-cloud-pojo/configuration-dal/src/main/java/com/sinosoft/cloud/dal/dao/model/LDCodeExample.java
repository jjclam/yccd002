package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LDCodeExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public LDCodeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCODETYPEIsNull() {
            addCriterion("CODETYPE is null");
            return (Criteria) this;
        }

        public Criteria andCODETYPEIsNotNull() {
            addCriterion("CODETYPE is not null");
            return (Criteria) this;
        }

        public Criteria andCODETYPEEqualTo(String value) {
            addCriterion("CODETYPE =", value, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPENotEqualTo(String value) {
            addCriterion("CODETYPE <>", value, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPEGreaterThan(String value) {
            addCriterion("CODETYPE >", value, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPEGreaterThanOrEqualTo(String value) {
            addCriterion("CODETYPE >=", value, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPELessThan(String value) {
            addCriterion("CODETYPE <", value, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPELessThanOrEqualTo(String value) {
            addCriterion("CODETYPE <=", value, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPELike(String value) {
            addCriterion("CODETYPE like", value, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPENotLike(String value) {
            addCriterion("CODETYPE not like", value, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPEIn(List<String> values) {
            addCriterion("CODETYPE in", values, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPENotIn(List<String> values) {
            addCriterion("CODETYPE not in", values, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPEBetween(String value1, String value2) {
            addCriterion("CODETYPE between", value1, value2, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODETYPENotBetween(String value1, String value2) {
            addCriterion("CODETYPE not between", value1, value2, "CODETYPE");
            return (Criteria) this;
        }

        public Criteria andCODEIsNull() {
            addCriterion("CODE is null");
            return (Criteria) this;
        }

        public Criteria andCODEIsNotNull() {
            addCriterion("CODE is not null");
            return (Criteria) this;
        }

        public Criteria andCODEEqualTo(String value) {
            addCriterion("CODE =", value, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODENotEqualTo(String value) {
            addCriterion("CODE <>", value, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODEGreaterThan(String value) {
            addCriterion("CODE >", value, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODEGreaterThanOrEqualTo(String value) {
            addCriterion("CODE >=", value, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODELessThan(String value) {
            addCriterion("CODE <", value, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODELessThanOrEqualTo(String value) {
            addCriterion("CODE <=", value, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODELike(String value) {
            addCriterion("CODE like", value, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODENotLike(String value) {
            addCriterion("CODE not like", value, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODEIn(List<String> values) {
            addCriterion("CODE in", values, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODENotIn(List<String> values) {
            addCriterion("CODE not in", values, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODEBetween(String value1, String value2) {
            addCriterion("CODE between", value1, value2, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODENotBetween(String value1, String value2) {
            addCriterion("CODE not between", value1, value2, "CODE");
            return (Criteria) this;
        }

        public Criteria andCODE1IsNull() {
            addCriterion("CODE1 is null");
            return (Criteria) this;
        }

        public Criteria andCODE1IsNotNull() {
            addCriterion("CODE1 is not null");
            return (Criteria) this;
        }

        public Criteria andCODE1EqualTo(String value) {
            addCriterion("CODE1 =", value, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1NotEqualTo(String value) {
            addCriterion("CODE1 <>", value, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1GreaterThan(String value) {
            addCriterion("CODE1 >", value, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1GreaterThanOrEqualTo(String value) {
            addCriterion("CODE1 >=", value, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1LessThan(String value) {
            addCriterion("CODE1 <", value, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1LessThanOrEqualTo(String value) {
            addCriterion("CODE1 <=", value, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1Like(String value) {
            addCriterion("CODE1 like", value, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1NotLike(String value) {
            addCriterion("CODE1 not like", value, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1In(List<String> values) {
            addCriterion("CODE1 in", values, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1NotIn(List<String> values) {
            addCriterion("CODE1 not in", values, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1Between(String value1, String value2) {
            addCriterion("CODE1 between", value1, value2, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODE1NotBetween(String value1, String value2) {
            addCriterion("CODE1 not between", value1, value2, "CODE1");
            return (Criteria) this;
        }

        public Criteria andCODENAMEIsNull() {
            addCriterion("CODENAME is null");
            return (Criteria) this;
        }

        public Criteria andCODENAMEIsNotNull() {
            addCriterion("CODENAME is not null");
            return (Criteria) this;
        }

        public Criteria andCODENAMEEqualTo(String value) {
            addCriterion("CODENAME =", value, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMENotEqualTo(String value) {
            addCriterion("CODENAME <>", value, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMEGreaterThan(String value) {
            addCriterion("CODENAME >", value, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMEGreaterThanOrEqualTo(String value) {
            addCriterion("CODENAME >=", value, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMELessThan(String value) {
            addCriterion("CODENAME <", value, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMELessThanOrEqualTo(String value) {
            addCriterion("CODENAME <=", value, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMELike(String value) {
            addCriterion("CODENAME like", value, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMENotLike(String value) {
            addCriterion("CODENAME not like", value, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMEIn(List<String> values) {
            addCriterion("CODENAME in", values, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMENotIn(List<String> values) {
            addCriterion("CODENAME not in", values, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMEBetween(String value1, String value2) {
            addCriterion("CODENAME between", value1, value2, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODENAMENotBetween(String value1, String value2) {
            addCriterion("CODENAME not between", value1, value2, "CODENAME");
            return (Criteria) this;
        }

        public Criteria andCODEALIASIsNull() {
            addCriterion("CODEALIAS is null");
            return (Criteria) this;
        }

        public Criteria andCODEALIASIsNotNull() {
            addCriterion("CODEALIAS is not null");
            return (Criteria) this;
        }

        public Criteria andCODEALIASEqualTo(String value) {
            addCriterion("CODEALIAS =", value, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASNotEqualTo(String value) {
            addCriterion("CODEALIAS <>", value, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASGreaterThan(String value) {
            addCriterion("CODEALIAS >", value, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASGreaterThanOrEqualTo(String value) {
            addCriterion("CODEALIAS >=", value, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASLessThan(String value) {
            addCriterion("CODEALIAS <", value, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASLessThanOrEqualTo(String value) {
            addCriterion("CODEALIAS <=", value, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASLike(String value) {
            addCriterion("CODEALIAS like", value, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASNotLike(String value) {
            addCriterion("CODEALIAS not like", value, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASIn(List<String> values) {
            addCriterion("CODEALIAS in", values, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASNotIn(List<String> values) {
            addCriterion("CODEALIAS not in", values, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASBetween(String value1, String value2) {
            addCriterion("CODEALIAS between", value1, value2, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCODEALIASNotBetween(String value1, String value2) {
            addCriterion("CODEALIAS not between", value1, value2, "CODEALIAS");
            return (Criteria) this;
        }

        public Criteria andCOMCODEIsNull() {
            addCriterion("COMCODE is null");
            return (Criteria) this;
        }

        public Criteria andCOMCODEIsNotNull() {
            addCriterion("COMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCOMCODEEqualTo(String value) {
            addCriterion("COMCODE =", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODENotEqualTo(String value) {
            addCriterion("COMCODE <>", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODEGreaterThan(String value) {
            addCriterion("COMCODE >", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODEGreaterThanOrEqualTo(String value) {
            addCriterion("COMCODE >=", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODELessThan(String value) {
            addCriterion("COMCODE <", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODELessThanOrEqualTo(String value) {
            addCriterion("COMCODE <=", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODELike(String value) {
            addCriterion("COMCODE like", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODENotLike(String value) {
            addCriterion("COMCODE not like", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODEIn(List<String> values) {
            addCriterion("COMCODE in", values, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODENotIn(List<String> values) {
            addCriterion("COMCODE not in", values, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODEBetween(String value1, String value2) {
            addCriterion("COMCODE between", value1, value2, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODENotBetween(String value1, String value2) {
            addCriterion("COMCODE not between", value1, value2, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNIsNull() {
            addCriterion("OTHERSIGN is null");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNIsNotNull() {
            addCriterion("OTHERSIGN is not null");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNEqualTo(String value) {
            addCriterion("OTHERSIGN =", value, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNNotEqualTo(String value) {
            addCriterion("OTHERSIGN <>", value, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNGreaterThan(String value) {
            addCriterion("OTHERSIGN >", value, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNGreaterThanOrEqualTo(String value) {
            addCriterion("OTHERSIGN >=", value, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNLessThan(String value) {
            addCriterion("OTHERSIGN <", value, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNLessThanOrEqualTo(String value) {
            addCriterion("OTHERSIGN <=", value, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNLike(String value) {
            addCriterion("OTHERSIGN like", value, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNNotLike(String value) {
            addCriterion("OTHERSIGN not like", value, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNIn(List<String> values) {
            addCriterion("OTHERSIGN in", values, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNNotIn(List<String> values) {
            addCriterion("OTHERSIGN not in", values, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNBetween(String value1, String value2) {
            addCriterion("OTHERSIGN between", value1, value2, "OTHERSIGN");
            return (Criteria) this;
        }

        public Criteria andOTHERSIGNNotBetween(String value1, String value2) {
            addCriterion("OTHERSIGN not between", value1, value2, "OTHERSIGN");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}