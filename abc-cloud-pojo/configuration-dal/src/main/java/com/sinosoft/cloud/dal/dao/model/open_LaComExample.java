package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class open_LaComExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public open_LaComExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andCOMCODEIsNull() {
            addCriterion("COMCODE is null");
            return (Criteria) this;
        }

        public Criteria andCOMCODEIsNotNull() {
            addCriterion("COMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCOMCODEEqualTo(String value) {
            addCriterion("COMCODE =", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODENotEqualTo(String value) {
            addCriterion("COMCODE <>", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODEGreaterThan(String value) {
            addCriterion("COMCODE >", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODEGreaterThanOrEqualTo(String value) {
            addCriterion("COMCODE >=", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODELessThan(String value) {
            addCriterion("COMCODE <", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODELessThanOrEqualTo(String value) {
            addCriterion("COMCODE <=", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODELike(String value) {
            addCriterion("COMCODE like", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODENotLike(String value) {
            addCriterion("COMCODE not like", value, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODEIn(List<String> values) {
            addCriterion("COMCODE in", values, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODENotIn(List<String> values) {
            addCriterion("COMCODE not in", values, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODEBetween(String value1, String value2) {
            addCriterion("COMCODE between", value1, value2, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andCOMCODENotBetween(String value1, String value2) {
            addCriterion("COMCODE not between", value1, value2, "COMCODE");
            return (Criteria) this;
        }

        public Criteria andUSERNAMEIsNull() {
            addCriterion("USERNAME is null");
            return (Criteria) this;
        }

        public Criteria andUSERNAMEIsNotNull() {
            addCriterion("USERNAME is not null");
            return (Criteria) this;
        }

        public Criteria andUSERNAMEEqualTo(String value) {
            addCriterion("USERNAME =", value, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMENotEqualTo(String value) {
            addCriterion("USERNAME <>", value, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMEGreaterThan(String value) {
            addCriterion("USERNAME >", value, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("USERNAME >=", value, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMELessThan(String value) {
            addCriterion("USERNAME <", value, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMELessThanOrEqualTo(String value) {
            addCriterion("USERNAME <=", value, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMELike(String value) {
            addCriterion("USERNAME like", value, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMENotLike(String value) {
            addCriterion("USERNAME not like", value, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMEIn(List<String> values) {
            addCriterion("USERNAME in", values, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMENotIn(List<String> values) {
            addCriterion("USERNAME not in", values, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMEBetween(String value1, String value2) {
            addCriterion("USERNAME between", value1, value2, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andUSERNAMENotBetween(String value1, String value2) {
            addCriterion("USERNAME not between", value1, value2, "USERNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMEIsNull() {
            addCriterion("COMPANYNAME is null");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMEIsNotNull() {
            addCriterion("COMPANYNAME is not null");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMEEqualTo(String value) {
            addCriterion("COMPANYNAME =", value, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMENotEqualTo(String value) {
            addCriterion("COMPANYNAME <>", value, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMEGreaterThan(String value) {
            addCriterion("COMPANYNAME >", value, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("COMPANYNAME >=", value, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMELessThan(String value) {
            addCriterion("COMPANYNAME <", value, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMELessThanOrEqualTo(String value) {
            addCriterion("COMPANYNAME <=", value, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMELike(String value) {
            addCriterion("COMPANYNAME like", value, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMENotLike(String value) {
            addCriterion("COMPANYNAME not like", value, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMEIn(List<String> values) {
            addCriterion("COMPANYNAME in", values, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMENotIn(List<String> values) {
            addCriterion("COMPANYNAME not in", values, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMEBetween(String value1, String value2) {
            addCriterion("COMPANYNAME between", value1, value2, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andCOMPANYNAMENotBetween(String value1, String value2) {
            addCriterion("COMPANYNAME not between", value1, value2, "COMPANYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATEIsNull() {
            addCriterion("VERIFYSTATE is null");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATEIsNotNull() {
            addCriterion("VERIFYSTATE is not null");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATEEqualTo(String value) {
            addCriterion("VERIFYSTATE =", value, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATENotEqualTo(String value) {
            addCriterion("VERIFYSTATE <>", value, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATEGreaterThan(String value) {
            addCriterion("VERIFYSTATE >", value, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATEGreaterThanOrEqualTo(String value) {
            addCriterion("VERIFYSTATE >=", value, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATELessThan(String value) {
            addCriterion("VERIFYSTATE <", value, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATELessThanOrEqualTo(String value) {
            addCriterion("VERIFYSTATE <=", value, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATELike(String value) {
            addCriterion("VERIFYSTATE like", value, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATENotLike(String value) {
            addCriterion("VERIFYSTATE not like", value, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATEIn(List<String> values) {
            addCriterion("VERIFYSTATE in", values, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATENotIn(List<String> values) {
            addCriterion("VERIFYSTATE not in", values, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATEBetween(String value1, String value2) {
            addCriterion("VERIFYSTATE between", value1, value2, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYSTATENotBetween(String value1, String value2) {
            addCriterion("VERIFYSTATE not between", value1, value2, "VERIFYSTATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONIsNull() {
            addCriterion("VERIFYOPINION is null");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONIsNotNull() {
            addCriterion("VERIFYOPINION is not null");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONEqualTo(String value) {
            addCriterion("VERIFYOPINION =", value, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONNotEqualTo(String value) {
            addCriterion("VERIFYOPINION <>", value, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONGreaterThan(String value) {
            addCriterion("VERIFYOPINION >", value, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONGreaterThanOrEqualTo(String value) {
            addCriterion("VERIFYOPINION >=", value, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONLessThan(String value) {
            addCriterion("VERIFYOPINION <", value, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONLessThanOrEqualTo(String value) {
            addCriterion("VERIFYOPINION <=", value, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONLike(String value) {
            addCriterion("VERIFYOPINION like", value, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONNotLike(String value) {
            addCriterion("VERIFYOPINION not like", value, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONIn(List<String> values) {
            addCriterion("VERIFYOPINION in", values, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONNotIn(List<String> values) {
            addCriterion("VERIFYOPINION not in", values, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONBetween(String value1, String value2) {
            addCriterion("VERIFYOPINION between", value1, value2, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYOPINIONNotBetween(String value1, String value2) {
            addCriterion("VERIFYOPINION not between", value1, value2, "VERIFYOPINION");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMEIsNull() {
            addCriterion("VERIFYNAME is null");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMEIsNotNull() {
            addCriterion("VERIFYNAME is not null");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMEEqualTo(String value) {
            addCriterion("VERIFYNAME =", value, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMENotEqualTo(String value) {
            addCriterion("VERIFYNAME <>", value, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMEGreaterThan(String value) {
            addCriterion("VERIFYNAME >", value, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("VERIFYNAME >=", value, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMELessThan(String value) {
            addCriterion("VERIFYNAME <", value, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMELessThanOrEqualTo(String value) {
            addCriterion("VERIFYNAME <=", value, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMELike(String value) {
            addCriterion("VERIFYNAME like", value, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMENotLike(String value) {
            addCriterion("VERIFYNAME not like", value, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMEIn(List<String> values) {
            addCriterion("VERIFYNAME in", values, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMENotIn(List<String> values) {
            addCriterion("VERIFYNAME not in", values, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMEBetween(String value1, String value2) {
            addCriterion("VERIFYNAME between", value1, value2, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYNAMENotBetween(String value1, String value2) {
            addCriterion("VERIFYNAME not between", value1, value2, "VERIFYNAME");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATEIsNull() {
            addCriterion("VERIFYDATE is null");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATEIsNotNull() {
            addCriterion("VERIFYDATE is not null");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATEEqualTo(Date value) {
            addCriterionForJDBCDate("VERIFYDATE =", value, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("VERIFYDATE <>", value, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("VERIFYDATE >", value, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("VERIFYDATE >=", value, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATELessThan(Date value) {
            addCriterionForJDBCDate("VERIFYDATE <", value, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("VERIFYDATE <=", value, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATEIn(List<Date> values) {
            addCriterionForJDBCDate("VERIFYDATE in", values, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("VERIFYDATE not in", values, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("VERIFYDATE between", value1, value2, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("VERIFYDATE not between", value1, value2, "VERIFYDATE");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMEIsNull() {
            addCriterion("VERIFYTIME is null");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMEIsNotNull() {
            addCriterion("VERIFYTIME is not null");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMEEqualTo(String value) {
            addCriterion("VERIFYTIME =", value, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMENotEqualTo(String value) {
            addCriterion("VERIFYTIME <>", value, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMEGreaterThan(String value) {
            addCriterion("VERIFYTIME >", value, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMEGreaterThanOrEqualTo(String value) {
            addCriterion("VERIFYTIME >=", value, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMELessThan(String value) {
            addCriterion("VERIFYTIME <", value, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMELessThanOrEqualTo(String value) {
            addCriterion("VERIFYTIME <=", value, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMELike(String value) {
            addCriterion("VERIFYTIME like", value, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMENotLike(String value) {
            addCriterion("VERIFYTIME not like", value, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMEIn(List<String> values) {
            addCriterion("VERIFYTIME in", values, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMENotIn(List<String> values) {
            addCriterion("VERIFYTIME not in", values, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMEBetween(String value1, String value2) {
            addCriterion("VERIFYTIME between", value1, value2, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andVERIFYTIMENotBetween(String value1, String value2) {
            addCriterion("VERIFYTIME not between", value1, value2, "VERIFYTIME");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSIsNull() {
            addCriterion("COMADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSIsNotNull() {
            addCriterion("COMADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSEqualTo(String value) {
            addCriterion("COMADDRESS =", value, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSNotEqualTo(String value) {
            addCriterion("COMADDRESS <>", value, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSGreaterThan(String value) {
            addCriterion("COMADDRESS >", value, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSGreaterThanOrEqualTo(String value) {
            addCriterion("COMADDRESS >=", value, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSLessThan(String value) {
            addCriterion("COMADDRESS <", value, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSLessThanOrEqualTo(String value) {
            addCriterion("COMADDRESS <=", value, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSLike(String value) {
            addCriterion("COMADDRESS like", value, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSNotLike(String value) {
            addCriterion("COMADDRESS not like", value, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSIn(List<String> values) {
            addCriterion("COMADDRESS in", values, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSNotIn(List<String> values) {
            addCriterion("COMADDRESS not in", values, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSBetween(String value1, String value2) {
            addCriterion("COMADDRESS between", value1, value2, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andCOMADDRESSNotBetween(String value1, String value2) {
            addCriterion("COMADDRESS not between", value1, value2, "COMADDRESS");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONIsNull() {
            addCriterion("LEGALPERSON is null");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONIsNotNull() {
            addCriterion("LEGALPERSON is not null");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONEqualTo(String value) {
            addCriterion("LEGALPERSON =", value, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONNotEqualTo(String value) {
            addCriterion("LEGALPERSON <>", value, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONGreaterThan(String value) {
            addCriterion("LEGALPERSON >", value, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONGreaterThanOrEqualTo(String value) {
            addCriterion("LEGALPERSON >=", value, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONLessThan(String value) {
            addCriterion("LEGALPERSON <", value, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONLessThanOrEqualTo(String value) {
            addCriterion("LEGALPERSON <=", value, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONLike(String value) {
            addCriterion("LEGALPERSON like", value, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONNotLike(String value) {
            addCriterion("LEGALPERSON not like", value, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONIn(List<String> values) {
            addCriterion("LEGALPERSON in", values, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONNotIn(List<String> values) {
            addCriterion("LEGALPERSON not in", values, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONBetween(String value1, String value2) {
            addCriterion("LEGALPERSON between", value1, value2, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andLEGALPERSONNotBetween(String value1, String value2) {
            addCriterion("LEGALPERSON not between", value1, value2, "LEGALPERSON");
            return (Criteria) this;
        }

        public Criteria andIDTYPEIsNull() {
            addCriterion("IDTYPE is null");
            return (Criteria) this;
        }

        public Criteria andIDTYPEIsNotNull() {
            addCriterion("IDTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andIDTYPEEqualTo(String value) {
            addCriterion("IDTYPE =", value, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPENotEqualTo(String value) {
            addCriterion("IDTYPE <>", value, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPEGreaterThan(String value) {
            addCriterion("IDTYPE >", value, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("IDTYPE >=", value, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPELessThan(String value) {
            addCriterion("IDTYPE <", value, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPELessThanOrEqualTo(String value) {
            addCriterion("IDTYPE <=", value, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPELike(String value) {
            addCriterion("IDTYPE like", value, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPENotLike(String value) {
            addCriterion("IDTYPE not like", value, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPEIn(List<String> values) {
            addCriterion("IDTYPE in", values, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPENotIn(List<String> values) {
            addCriterion("IDTYPE not in", values, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPEBetween(String value1, String value2) {
            addCriterion("IDTYPE between", value1, value2, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDTYPENotBetween(String value1, String value2) {
            addCriterion("IDTYPE not between", value1, value2, "IDTYPE");
            return (Criteria) this;
        }

        public Criteria andIDNOIsNull() {
            addCriterion("IDNO is null");
            return (Criteria) this;
        }

        public Criteria andIDNOIsNotNull() {
            addCriterion("IDNO is not null");
            return (Criteria) this;
        }

        public Criteria andIDNOEqualTo(String value) {
            addCriterion("IDNO =", value, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNONotEqualTo(String value) {
            addCriterion("IDNO <>", value, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNOGreaterThan(String value) {
            addCriterion("IDNO >", value, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNOGreaterThanOrEqualTo(String value) {
            addCriterion("IDNO >=", value, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNOLessThan(String value) {
            addCriterion("IDNO <", value, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNOLessThanOrEqualTo(String value) {
            addCriterion("IDNO <=", value, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNOLike(String value) {
            addCriterion("IDNO like", value, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNONotLike(String value) {
            addCriterion("IDNO not like", value, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNOIn(List<String> values) {
            addCriterion("IDNO in", values, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNONotIn(List<String> values) {
            addCriterion("IDNO not in", values, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNOBetween(String value1, String value2) {
            addCriterion("IDNO between", value1, value2, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDNONotBetween(String value1, String value2) {
            addCriterion("IDNO not between", value1, value2, "IDNO");
            return (Criteria) this;
        }

        public Criteria andIDSTARTIsNull() {
            addCriterion("IDSTART is null");
            return (Criteria) this;
        }

        public Criteria andIDSTARTIsNotNull() {
            addCriterion("IDSTART is not null");
            return (Criteria) this;
        }

        public Criteria andIDSTARTEqualTo(Date value) {
            addCriterionForJDBCDate("IDSTART =", value, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTNotEqualTo(Date value) {
            addCriterionForJDBCDate("IDSTART <>", value, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTGreaterThan(Date value) {
            addCriterionForJDBCDate("IDSTART >", value, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("IDSTART >=", value, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTLessThan(Date value) {
            addCriterionForJDBCDate("IDSTART <", value, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("IDSTART <=", value, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTIn(List<Date> values) {
            addCriterionForJDBCDate("IDSTART in", values, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTNotIn(List<Date> values) {
            addCriterionForJDBCDate("IDSTART not in", values, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("IDSTART between", value1, value2, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDSTARTNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("IDSTART not between", value1, value2, "IDSTART");
            return (Criteria) this;
        }

        public Criteria andIDENDIsNull() {
            addCriterion("IDEND is null");
            return (Criteria) this;
        }

        public Criteria andIDENDIsNotNull() {
            addCriterion("IDEND is not null");
            return (Criteria) this;
        }

        public Criteria andIDENDEqualTo(Date value) {
            addCriterionForJDBCDate("IDEND =", value, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDNotEqualTo(Date value) {
            addCriterionForJDBCDate("IDEND <>", value, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDGreaterThan(Date value) {
            addCriterionForJDBCDate("IDEND >", value, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("IDEND >=", value, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDLessThan(Date value) {
            addCriterionForJDBCDate("IDEND <", value, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("IDEND <=", value, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDIn(List<Date> values) {
            addCriterionForJDBCDate("IDEND in", values, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDNotIn(List<Date> values) {
            addCriterionForJDBCDate("IDEND not in", values, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("IDEND between", value1, value2, "IDEND");
            return (Criteria) this;
        }

        public Criteria andIDENDNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("IDEND not between", value1, value2, "IDEND");
            return (Criteria) this;
        }

        public Criteria andLINKNAMEIsNull() {
            addCriterion("LINKNAME is null");
            return (Criteria) this;
        }

        public Criteria andLINKNAMEIsNotNull() {
            addCriterion("LINKNAME is not null");
            return (Criteria) this;
        }

        public Criteria andLINKNAMEEqualTo(String value) {
            addCriterion("LINKNAME =", value, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMENotEqualTo(String value) {
            addCriterion("LINKNAME <>", value, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMEGreaterThan(String value) {
            addCriterion("LINKNAME >", value, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("LINKNAME >=", value, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMELessThan(String value) {
            addCriterion("LINKNAME <", value, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMELessThanOrEqualTo(String value) {
            addCriterion("LINKNAME <=", value, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMELike(String value) {
            addCriterion("LINKNAME like", value, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMENotLike(String value) {
            addCriterion("LINKNAME not like", value, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMEIn(List<String> values) {
            addCriterion("LINKNAME in", values, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMENotIn(List<String> values) {
            addCriterion("LINKNAME not in", values, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMEBetween(String value1, String value2) {
            addCriterion("LINKNAME between", value1, value2, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKNAMENotBetween(String value1, String value2) {
            addCriterion("LINKNAME not between", value1, value2, "LINKNAME");
            return (Criteria) this;
        }

        public Criteria andLINKPHONEIsNull() {
            addCriterion("LINKPHONE is null");
            return (Criteria) this;
        }

        public Criteria andLINKPHONEIsNotNull() {
            addCriterion("LINKPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andLINKPHONEEqualTo(String value) {
            addCriterion("LINKPHONE =", value, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONENotEqualTo(String value) {
            addCriterion("LINKPHONE <>", value, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONEGreaterThan(String value) {
            addCriterion("LINKPHONE >", value, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONEGreaterThanOrEqualTo(String value) {
            addCriterion("LINKPHONE >=", value, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONELessThan(String value) {
            addCriterion("LINKPHONE <", value, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONELessThanOrEqualTo(String value) {
            addCriterion("LINKPHONE <=", value, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONELike(String value) {
            addCriterion("LINKPHONE like", value, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONENotLike(String value) {
            addCriterion("LINKPHONE not like", value, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONEIn(List<String> values) {
            addCriterion("LINKPHONE in", values, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONENotIn(List<String> values) {
            addCriterion("LINKPHONE not in", values, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONEBetween(String value1, String value2) {
            addCriterion("LINKPHONE between", value1, value2, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKPHONENotBetween(String value1, String value2) {
            addCriterion("LINKPHONE not between", value1, value2, "LINKPHONE");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILIsNull() {
            addCriterion("LINKEMAIL is null");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILIsNotNull() {
            addCriterion("LINKEMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILEqualTo(String value) {
            addCriterion("LINKEMAIL =", value, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILNotEqualTo(String value) {
            addCriterion("LINKEMAIL <>", value, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILGreaterThan(String value) {
            addCriterion("LINKEMAIL >", value, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILGreaterThanOrEqualTo(String value) {
            addCriterion("LINKEMAIL >=", value, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILLessThan(String value) {
            addCriterion("LINKEMAIL <", value, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILLessThanOrEqualTo(String value) {
            addCriterion("LINKEMAIL <=", value, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILLike(String value) {
            addCriterion("LINKEMAIL like", value, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILNotLike(String value) {
            addCriterion("LINKEMAIL not like", value, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILIn(List<String> values) {
            addCriterion("LINKEMAIL in", values, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILNotIn(List<String> values) {
            addCriterion("LINKEMAIL not in", values, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILBetween(String value1, String value2) {
            addCriterion("LINKEMAIL between", value1, value2, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andLINKEMAILNotBetween(String value1, String value2) {
            addCriterion("LINKEMAIL not between", value1, value2, "LINKEMAIL");
            return (Criteria) this;
        }

        public Criteria andZIPNOIsNull() {
            addCriterion("ZIPNO is null");
            return (Criteria) this;
        }

        public Criteria andZIPNOIsNotNull() {
            addCriterion("ZIPNO is not null");
            return (Criteria) this;
        }

        public Criteria andZIPNOEqualTo(String value) {
            addCriterion("ZIPNO =", value, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNONotEqualTo(String value) {
            addCriterion("ZIPNO <>", value, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNOGreaterThan(String value) {
            addCriterion("ZIPNO >", value, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNOGreaterThanOrEqualTo(String value) {
            addCriterion("ZIPNO >=", value, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNOLessThan(String value) {
            addCriterion("ZIPNO <", value, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNOLessThanOrEqualTo(String value) {
            addCriterion("ZIPNO <=", value, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNOLike(String value) {
            addCriterion("ZIPNO like", value, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNONotLike(String value) {
            addCriterion("ZIPNO not like", value, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNOIn(List<String> values) {
            addCriterion("ZIPNO in", values, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNONotIn(List<String> values) {
            addCriterion("ZIPNO not in", values, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNOBetween(String value1, String value2) {
            addCriterion("ZIPNO between", value1, value2, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andZIPNONotBetween(String value1, String value2) {
            addCriterion("ZIPNO not between", value1, value2, "ZIPNO");
            return (Criteria) this;
        }

        public Criteria andCOMTYPEIsNull() {
            addCriterion("COMTYPE is null");
            return (Criteria) this;
        }

        public Criteria andCOMTYPEIsNotNull() {
            addCriterion("COMTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andCOMTYPEEqualTo(String value) {
            addCriterion("COMTYPE =", value, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPENotEqualTo(String value) {
            addCriterion("COMTYPE <>", value, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPEGreaterThan(String value) {
            addCriterion("COMTYPE >", value, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("COMTYPE >=", value, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPELessThan(String value) {
            addCriterion("COMTYPE <", value, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPELessThanOrEqualTo(String value) {
            addCriterion("COMTYPE <=", value, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPELike(String value) {
            addCriterion("COMTYPE like", value, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPENotLike(String value) {
            addCriterion("COMTYPE not like", value, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPEIn(List<String> values) {
            addCriterion("COMTYPE in", values, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPENotIn(List<String> values) {
            addCriterion("COMTYPE not in", values, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPEBetween(String value1, String value2) {
            addCriterion("COMTYPE between", value1, value2, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMTYPENotBetween(String value1, String value2) {
            addCriterion("COMTYPE not between", value1, value2, "COMTYPE");
            return (Criteria) this;
        }

        public Criteria andISNETIsNull() {
            addCriterion("ISNET is null");
            return (Criteria) this;
        }

        public Criteria andISNETIsNotNull() {
            addCriterion("ISNET is not null");
            return (Criteria) this;
        }

        public Criteria andISNETEqualTo(String value) {
            addCriterion("ISNET =", value, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETNotEqualTo(String value) {
            addCriterion("ISNET <>", value, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETGreaterThan(String value) {
            addCriterion("ISNET >", value, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETGreaterThanOrEqualTo(String value) {
            addCriterion("ISNET >=", value, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETLessThan(String value) {
            addCriterion("ISNET <", value, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETLessThanOrEqualTo(String value) {
            addCriterion("ISNET <=", value, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETLike(String value) {
            addCriterion("ISNET like", value, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETNotLike(String value) {
            addCriterion("ISNET not like", value, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETIn(List<String> values) {
            addCriterion("ISNET in", values, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETNotIn(List<String> values) {
            addCriterion("ISNET not in", values, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETBetween(String value1, String value2) {
            addCriterion("ISNET between", value1, value2, "ISNET");
            return (Criteria) this;
        }

        public Criteria andISNETNotBetween(String value1, String value2) {
            addCriterion("ISNET not between", value1, value2, "ISNET");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPEIsNull() {
            addCriterion("COMIDTYPE is null");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPEIsNotNull() {
            addCriterion("COMIDTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPEEqualTo(String value) {
            addCriterion("COMIDTYPE =", value, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPENotEqualTo(String value) {
            addCriterion("COMIDTYPE <>", value, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPEGreaterThan(String value) {
            addCriterion("COMIDTYPE >", value, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("COMIDTYPE >=", value, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPELessThan(String value) {
            addCriterion("COMIDTYPE <", value, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPELessThanOrEqualTo(String value) {
            addCriterion("COMIDTYPE <=", value, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPELike(String value) {
            addCriterion("COMIDTYPE like", value, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPENotLike(String value) {
            addCriterion("COMIDTYPE not like", value, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPEIn(List<String> values) {
            addCriterion("COMIDTYPE in", values, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPENotIn(List<String> values) {
            addCriterion("COMIDTYPE not in", values, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPEBetween(String value1, String value2) {
            addCriterion("COMIDTYPE between", value1, value2, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDTYPENotBetween(String value1, String value2) {
            addCriterion("COMIDTYPE not between", value1, value2, "COMIDTYPE");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOIsNull() {
            addCriterion("COMIDNO is null");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOIsNotNull() {
            addCriterion("COMIDNO is not null");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOEqualTo(String value) {
            addCriterion("COMIDNO =", value, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNONotEqualTo(String value) {
            addCriterion("COMIDNO <>", value, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOGreaterThan(String value) {
            addCriterion("COMIDNO >", value, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOGreaterThanOrEqualTo(String value) {
            addCriterion("COMIDNO >=", value, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOLessThan(String value) {
            addCriterion("COMIDNO <", value, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOLessThanOrEqualTo(String value) {
            addCriterion("COMIDNO <=", value, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOLike(String value) {
            addCriterion("COMIDNO like", value, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNONotLike(String value) {
            addCriterion("COMIDNO not like", value, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOIn(List<String> values) {
            addCriterion("COMIDNO in", values, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNONotIn(List<String> values) {
            addCriterion("COMIDNO not in", values, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNOBetween(String value1, String value2) {
            addCriterion("COMIDNO between", value1, value2, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDNONotBetween(String value1, String value2) {
            addCriterion("COMIDNO not between", value1, value2, "COMIDNO");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTIsNull() {
            addCriterion("COMIDSTART is null");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTIsNotNull() {
            addCriterion("COMIDSTART is not null");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTEqualTo(Date value) {
            addCriterionForJDBCDate("COMIDSTART =", value, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTNotEqualTo(Date value) {
            addCriterionForJDBCDate("COMIDSTART <>", value, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTGreaterThan(Date value) {
            addCriterionForJDBCDate("COMIDSTART >", value, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("COMIDSTART >=", value, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTLessThan(Date value) {
            addCriterionForJDBCDate("COMIDSTART <", value, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("COMIDSTART <=", value, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTIn(List<Date> values) {
            addCriterionForJDBCDate("COMIDSTART in", values, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTNotIn(List<Date> values) {
            addCriterionForJDBCDate("COMIDSTART not in", values, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("COMIDSTART between", value1, value2, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDSTARTNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("COMIDSTART not between", value1, value2, "COMIDSTART");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDIsNull() {
            addCriterion("COMIDEND is null");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDIsNotNull() {
            addCriterion("COMIDEND is not null");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDEqualTo(Date value) {
            addCriterionForJDBCDate("COMIDEND =", value, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDNotEqualTo(Date value) {
            addCriterionForJDBCDate("COMIDEND <>", value, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDGreaterThan(Date value) {
            addCriterionForJDBCDate("COMIDEND >", value, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("COMIDEND >=", value, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDLessThan(Date value) {
            addCriterionForJDBCDate("COMIDEND <", value, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("COMIDEND <=", value, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDIn(List<Date> values) {
            addCriterionForJDBCDate("COMIDEND in", values, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDNotIn(List<Date> values) {
            addCriterionForJDBCDate("COMIDEND not in", values, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("COMIDEND between", value1, value2, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andCOMIDENDNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("COMIDEND not between", value1, value2, "COMIDEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOIsNull() {
            addCriterion("AGENTBUSINO is null");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOIsNotNull() {
            addCriterion("AGENTBUSINO is not null");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOEqualTo(String value) {
            addCriterion("AGENTBUSINO =", value, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINONotEqualTo(String value) {
            addCriterion("AGENTBUSINO <>", value, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOGreaterThan(String value) {
            addCriterion("AGENTBUSINO >", value, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTBUSINO >=", value, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOLessThan(String value) {
            addCriterion("AGENTBUSINO <", value, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOLessThanOrEqualTo(String value) {
            addCriterion("AGENTBUSINO <=", value, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOLike(String value) {
            addCriterion("AGENTBUSINO like", value, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINONotLike(String value) {
            addCriterion("AGENTBUSINO not like", value, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOIn(List<String> values) {
            addCriterion("AGENTBUSINO in", values, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINONotIn(List<String> values) {
            addCriterion("AGENTBUSINO not in", values, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINOBetween(String value1, String value2) {
            addCriterion("AGENTBUSINO between", value1, value2, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSINONotBetween(String value1, String value2) {
            addCriterion("AGENTBUSINO not between", value1, value2, "AGENTBUSINO");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTIsNull() {
            addCriterion("AGENTBUSISTART is null");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTIsNotNull() {
            addCriterion("AGENTBUSISTART is not null");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTEqualTo(Date value) {
            addCriterionForJDBCDate("AGENTBUSISTART =", value, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTNotEqualTo(Date value) {
            addCriterionForJDBCDate("AGENTBUSISTART <>", value, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTGreaterThan(Date value) {
            addCriterionForJDBCDate("AGENTBUSISTART >", value, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("AGENTBUSISTART >=", value, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTLessThan(Date value) {
            addCriterionForJDBCDate("AGENTBUSISTART <", value, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("AGENTBUSISTART <=", value, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTIn(List<Date> values) {
            addCriterionForJDBCDate("AGENTBUSISTART in", values, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTNotIn(List<Date> values) {
            addCriterionForJDBCDate("AGENTBUSISTART not in", values, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("AGENTBUSISTART between", value1, value2, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSISTARTNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("AGENTBUSISTART not between", value1, value2, "AGENTBUSISTART");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDIsNull() {
            addCriterion("AGENTBUSIEND is null");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDIsNotNull() {
            addCriterion("AGENTBUSIEND is not null");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDEqualTo(Date value) {
            addCriterionForJDBCDate("AGENTBUSIEND =", value, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDNotEqualTo(Date value) {
            addCriterionForJDBCDate("AGENTBUSIEND <>", value, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDGreaterThan(Date value) {
            addCriterionForJDBCDate("AGENTBUSIEND >", value, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("AGENTBUSIEND >=", value, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDLessThan(Date value) {
            addCriterionForJDBCDate("AGENTBUSIEND <", value, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("AGENTBUSIEND <=", value, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDIn(List<Date> values) {
            addCriterionForJDBCDate("AGENTBUSIEND in", values, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDNotIn(List<Date> values) {
            addCriterionForJDBCDate("AGENTBUSIEND not in", values, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("AGENTBUSIEND between", value1, value2, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andAGENTBUSIENDNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("AGENTBUSIEND not between", value1, value2, "AGENTBUSIEND");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPEIsNull() {
            addCriterion("FEEBANKTYPE is null");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPEIsNotNull() {
            addCriterion("FEEBANKTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPEEqualTo(String value) {
            addCriterion("FEEBANKTYPE =", value, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPENotEqualTo(String value) {
            addCriterion("FEEBANKTYPE <>", value, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPEGreaterThan(String value) {
            addCriterion("FEEBANKTYPE >", value, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("FEEBANKTYPE >=", value, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPELessThan(String value) {
            addCriterion("FEEBANKTYPE <", value, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPELessThanOrEqualTo(String value) {
            addCriterion("FEEBANKTYPE <=", value, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPELike(String value) {
            addCriterion("FEEBANKTYPE like", value, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPENotLike(String value) {
            addCriterion("FEEBANKTYPE not like", value, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPEIn(List<String> values) {
            addCriterion("FEEBANKTYPE in", values, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPENotIn(List<String> values) {
            addCriterion("FEEBANKTYPE not in", values, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPEBetween(String value1, String value2) {
            addCriterion("FEEBANKTYPE between", value1, value2, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKTYPENotBetween(String value1, String value2) {
            addCriterion("FEEBANKTYPE not between", value1, value2, "FEEBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMEIsNull() {
            addCriterion("FEEBANKNAME is null");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMEIsNotNull() {
            addCriterion("FEEBANKNAME is not null");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMEEqualTo(String value) {
            addCriterion("FEEBANKNAME =", value, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMENotEqualTo(String value) {
            addCriterion("FEEBANKNAME <>", value, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMEGreaterThan(String value) {
            addCriterion("FEEBANKNAME >", value, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("FEEBANKNAME >=", value, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMELessThan(String value) {
            addCriterion("FEEBANKNAME <", value, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMELessThanOrEqualTo(String value) {
            addCriterion("FEEBANKNAME <=", value, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMELike(String value) {
            addCriterion("FEEBANKNAME like", value, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMENotLike(String value) {
            addCriterion("FEEBANKNAME not like", value, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMEIn(List<String> values) {
            addCriterion("FEEBANKNAME in", values, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMENotIn(List<String> values) {
            addCriterion("FEEBANKNAME not in", values, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMEBetween(String value1, String value2) {
            addCriterion("FEEBANKNAME between", value1, value2, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNAMENotBetween(String value1, String value2) {
            addCriterion("FEEBANKNAME not between", value1, value2, "FEEBANKNAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMEIsNull() {
            addCriterion("FEENAME is null");
            return (Criteria) this;
        }

        public Criteria andFEENAMEIsNotNull() {
            addCriterion("FEENAME is not null");
            return (Criteria) this;
        }

        public Criteria andFEENAMEEqualTo(String value) {
            addCriterion("FEENAME =", value, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMENotEqualTo(String value) {
            addCriterion("FEENAME <>", value, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMEGreaterThan(String value) {
            addCriterion("FEENAME >", value, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMEGreaterThanOrEqualTo(String value) {
            addCriterion("FEENAME >=", value, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMELessThan(String value) {
            addCriterion("FEENAME <", value, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMELessThanOrEqualTo(String value) {
            addCriterion("FEENAME <=", value, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMELike(String value) {
            addCriterion("FEENAME like", value, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMENotLike(String value) {
            addCriterion("FEENAME not like", value, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMEIn(List<String> values) {
            addCriterion("FEENAME in", values, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMENotIn(List<String> values) {
            addCriterion("FEENAME not in", values, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMEBetween(String value1, String value2) {
            addCriterion("FEENAME between", value1, value2, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEENAMENotBetween(String value1, String value2) {
            addCriterion("FEENAME not between", value1, value2, "FEENAME");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOIsNull() {
            addCriterion("FEEBANKNO is null");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOIsNotNull() {
            addCriterion("FEEBANKNO is not null");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOEqualTo(String value) {
            addCriterion("FEEBANKNO =", value, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNONotEqualTo(String value) {
            addCriterion("FEEBANKNO <>", value, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOGreaterThan(String value) {
            addCriterion("FEEBANKNO >", value, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOGreaterThanOrEqualTo(String value) {
            addCriterion("FEEBANKNO >=", value, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOLessThan(String value) {
            addCriterion("FEEBANKNO <", value, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOLessThanOrEqualTo(String value) {
            addCriterion("FEEBANKNO <=", value, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOLike(String value) {
            addCriterion("FEEBANKNO like", value, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNONotLike(String value) {
            addCriterion("FEEBANKNO not like", value, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOIn(List<String> values) {
            addCriterion("FEEBANKNO in", values, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNONotIn(List<String> values) {
            addCriterion("FEEBANKNO not in", values, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNOBetween(String value1, String value2) {
            addCriterion("FEEBANKNO between", value1, value2, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andFEEBANKNONotBetween(String value1, String value2) {
            addCriterion("FEEBANKNO not between", value1, value2, "FEEBANKNO");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONIsNull() {
            addCriterion("ISCOLLECTION is null");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONIsNotNull() {
            addCriterion("ISCOLLECTION is not null");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONEqualTo(String value) {
            addCriterion("ISCOLLECTION =", value, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONNotEqualTo(String value) {
            addCriterion("ISCOLLECTION <>", value, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONGreaterThan(String value) {
            addCriterion("ISCOLLECTION >", value, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONGreaterThanOrEqualTo(String value) {
            addCriterion("ISCOLLECTION >=", value, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONLessThan(String value) {
            addCriterion("ISCOLLECTION <", value, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONLessThanOrEqualTo(String value) {
            addCriterion("ISCOLLECTION <=", value, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONLike(String value) {
            addCriterion("ISCOLLECTION like", value, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONNotLike(String value) {
            addCriterion("ISCOLLECTION not like", value, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONIn(List<String> values) {
            addCriterion("ISCOLLECTION in", values, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONNotIn(List<String> values) {
            addCriterion("ISCOLLECTION not in", values, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONBetween(String value1, String value2) {
            addCriterion("ISCOLLECTION between", value1, value2, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andISCOLLECTIONNotBetween(String value1, String value2) {
            addCriterion("ISCOLLECTION not between", value1, value2, "ISCOLLECTION");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPEIsNull() {
            addCriterion("PREMBANKTYPE is null");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPEIsNotNull() {
            addCriterion("PREMBANKTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPEEqualTo(String value) {
            addCriterion("PREMBANKTYPE =", value, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPENotEqualTo(String value) {
            addCriterion("PREMBANKTYPE <>", value, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPEGreaterThan(String value) {
            addCriterion("PREMBANKTYPE >", value, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("PREMBANKTYPE >=", value, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPELessThan(String value) {
            addCriterion("PREMBANKTYPE <", value, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPELessThanOrEqualTo(String value) {
            addCriterion("PREMBANKTYPE <=", value, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPELike(String value) {
            addCriterion("PREMBANKTYPE like", value, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPENotLike(String value) {
            addCriterion("PREMBANKTYPE not like", value, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPEIn(List<String> values) {
            addCriterion("PREMBANKTYPE in", values, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPENotIn(List<String> values) {
            addCriterion("PREMBANKTYPE not in", values, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPEBetween(String value1, String value2) {
            addCriterion("PREMBANKTYPE between", value1, value2, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKTYPENotBetween(String value1, String value2) {
            addCriterion("PREMBANKTYPE not between", value1, value2, "PREMBANKTYPE");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMEIsNull() {
            addCriterion("PREMBANKNAME is null");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMEIsNotNull() {
            addCriterion("PREMBANKNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMEEqualTo(String value) {
            addCriterion("PREMBANKNAME =", value, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMENotEqualTo(String value) {
            addCriterion("PREMBANKNAME <>", value, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMEGreaterThan(String value) {
            addCriterion("PREMBANKNAME >", value, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("PREMBANKNAME >=", value, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMELessThan(String value) {
            addCriterion("PREMBANKNAME <", value, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMELessThanOrEqualTo(String value) {
            addCriterion("PREMBANKNAME <=", value, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMELike(String value) {
            addCriterion("PREMBANKNAME like", value, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMENotLike(String value) {
            addCriterion("PREMBANKNAME not like", value, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMEIn(List<String> values) {
            addCriterion("PREMBANKNAME in", values, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMENotIn(List<String> values) {
            addCriterion("PREMBANKNAME not in", values, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMEBetween(String value1, String value2) {
            addCriterion("PREMBANKNAME between", value1, value2, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMBANKNAMENotBetween(String value1, String value2) {
            addCriterion("PREMBANKNAME not between", value1, value2, "PREMBANKNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMEIsNull() {
            addCriterion("PREMNAME is null");
            return (Criteria) this;
        }

        public Criteria andPREMNAMEIsNotNull() {
            addCriterion("PREMNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPREMNAMEEqualTo(String value) {
            addCriterion("PREMNAME =", value, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMENotEqualTo(String value) {
            addCriterion("PREMNAME <>", value, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMEGreaterThan(String value) {
            addCriterion("PREMNAME >", value, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("PREMNAME >=", value, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMELessThan(String value) {
            addCriterion("PREMNAME <", value, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMELessThanOrEqualTo(String value) {
            addCriterion("PREMNAME <=", value, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMELike(String value) {
            addCriterion("PREMNAME like", value, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMENotLike(String value) {
            addCriterion("PREMNAME not like", value, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMEIn(List<String> values) {
            addCriterion("PREMNAME in", values, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMENotIn(List<String> values) {
            addCriterion("PREMNAME not in", values, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMEBetween(String value1, String value2) {
            addCriterion("PREMNAME between", value1, value2, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNAMENotBetween(String value1, String value2) {
            addCriterion("PREMNAME not between", value1, value2, "PREMNAME");
            return (Criteria) this;
        }

        public Criteria andPREMNOIsNull() {
            addCriterion("PREMNO is null");
            return (Criteria) this;
        }

        public Criteria andPREMNOIsNotNull() {
            addCriterion("PREMNO is not null");
            return (Criteria) this;
        }

        public Criteria andPREMNOEqualTo(String value) {
            addCriterion("PREMNO =", value, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNONotEqualTo(String value) {
            addCriterion("PREMNO <>", value, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNOGreaterThan(String value) {
            addCriterion("PREMNO >", value, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNOGreaterThanOrEqualTo(String value) {
            addCriterion("PREMNO >=", value, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNOLessThan(String value) {
            addCriterion("PREMNO <", value, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNOLessThanOrEqualTo(String value) {
            addCriterion("PREMNO <=", value, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNOLike(String value) {
            addCriterion("PREMNO like", value, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNONotLike(String value) {
            addCriterion("PREMNO not like", value, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNOIn(List<String> values) {
            addCriterion("PREMNO in", values, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNONotIn(List<String> values) {
            addCriterion("PREMNO not in", values, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNOBetween(String value1, String value2) {
            addCriterion("PREMNO between", value1, value2, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andPREMNONotBetween(String value1, String value2) {
            addCriterion("PREMNO not between", value1, value2, "PREMNO");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIsNull() {
            addCriterion("MODIFYDATE is null");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIsNotNull() {
            addCriterion("MODIFYDATE is not null");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE =", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <>", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("MODIFYDATE >", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE >=", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATELessThan(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <=", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIn(List<Date> values) {
            addCriterionForJDBCDate("MODIFYDATE in", values, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("MODIFYDATE not in", values, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MODIFYDATE between", value1, value2, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MODIFYDATE not between", value1, value2, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIsNull() {
            addCriterion("MODIFYTIME is null");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIsNotNull() {
            addCriterion("MODIFYTIME is not null");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEEqualTo(String value) {
            addCriterion("MODIFYTIME =", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotEqualTo(String value) {
            addCriterion("MODIFYTIME <>", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEGreaterThan(String value) {
            addCriterion("MODIFYTIME >", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFYTIME >=", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELessThan(String value) {
            addCriterion("MODIFYTIME <", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELessThanOrEqualTo(String value) {
            addCriterion("MODIFYTIME <=", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELike(String value) {
            addCriterion("MODIFYTIME like", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotLike(String value) {
            addCriterion("MODIFYTIME not like", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIn(List<String> values) {
            addCriterion("MODIFYTIME in", values, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotIn(List<String> values) {
            addCriterion("MODIFYTIME not in", values, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEBetween(String value1, String value2) {
            addCriterion("MODIFYTIME between", value1, value2, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotBetween(String value1, String value2) {
            addCriterion("MODIFYTIME not between", value1, value2, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIsNull() {
            addCriterion("MAKEDATE is null");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIsNotNull() {
            addCriterion("MAKEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE =", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE <>", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("MAKEDATE >", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE >=", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATELessThan(Date value) {
            addCriterionForJDBCDate("MAKEDATE <", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE <=", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIn(List<Date> values) {
            addCriterionForJDBCDate("MAKEDATE in", values, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("MAKEDATE not in", values, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MAKEDATE between", value1, value2, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MAKEDATE not between", value1, value2, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIsNull() {
            addCriterion("MAKETIME is null");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIsNotNull() {
            addCriterion("MAKETIME is not null");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEEqualTo(String value) {
            addCriterion("MAKETIME =", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotEqualTo(String value) {
            addCriterion("MAKETIME <>", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEGreaterThan(String value) {
            addCriterion("MAKETIME >", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEGreaterThanOrEqualTo(String value) {
            addCriterion("MAKETIME >=", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELessThan(String value) {
            addCriterion("MAKETIME <", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELessThanOrEqualTo(String value) {
            addCriterion("MAKETIME <=", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELike(String value) {
            addCriterion("MAKETIME like", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotLike(String value) {
            addCriterion("MAKETIME not like", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIn(List<String> values) {
            addCriterion("MAKETIME in", values, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotIn(List<String> values) {
            addCriterion("MAKETIME not in", values, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEBetween(String value1, String value2) {
            addCriterion("MAKETIME between", value1, value2, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotBetween(String value1, String value2) {
            addCriterion("MAKETIME not between", value1, value2, "MAKETIME");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}