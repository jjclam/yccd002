package com.sinosoft.cloud.dal.dao.model;

import java.util.Date;

public class LDPLANRISK extends LDPLANRISKKey {
    private String mainriskversion;

    private String riskversion;

    private String contplanname;

    private String remark;

    private String operator;

    private Date makedate;

    private String maketime;

    private Date modifydate;

    private String modifytime;

    public String getMainriskversion() {
        return mainriskversion;
    }

    public void setMainriskversion(String mainriskversion) {
        this.mainriskversion = mainriskversion == null ? null : mainriskversion.trim();
    }

    public String getRiskversion() {
        return riskversion;
    }

    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion == null ? null : riskversion.trim();
    }

    public String getContplanname() {
        return contplanname;
    }

    public void setContplanname(String contplanname) {
        this.contplanname = contplanname == null ? null : contplanname.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public Date getMakedate() {
        return makedate;
    }

    public void setMakedate(Date makedate) {
        this.makedate = makedate;
    }

    public String getMaketime() {
        return maketime;
    }

    public void setMaketime(String maketime) {
        this.maketime = maketime == null ? null : maketime.trim();
    }

    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    public String getModifytime() {
        return modifytime;
    }

    public void setModifytime(String modifytime) {
        this.modifytime = modifytime == null ? null : modifytime.trim();
    }
}