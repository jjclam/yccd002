package com.sinosoft.cloud.dal.yfbDao.model;

import java.math.BigDecimal;
import java.util.Date;

public class LDGrp {
    private String customerno;

    private String password;

    private String grpname;

    private String businesstype;

    private String grpnature;

    private Short peoples;

    private Short onworkpeoples;

    private Short insurepeoples;

    private String comdocidtype;

    private String comdocidno;

    private Date buslicedate;

    private String postcode;

    private String grpaddress;

    private String fax;

    private String unitregisteredaddress;

    private BigDecimal unitduration;

    private String principal;

    private String principalidtype;

    private String principalidno;

    private Date principalvalidate;

    private String telephone;

    private String mobile;

    private String email;

    private Date annuityreceivedate;

    private String managecom;

    private String headship;

    private String getflag;

    private String disputedflag;

    private String bankcode;

    private String bankaccno;

    private String accname;

    private String bankproivnce;

    private String bankcity;

    private String acctype;

    private String banklocations;

    private String taxpayeridtype;

    private String taxpayeridno;

    private String taxbureauaddr;

    private String taxbureautel;

    private String taxbureaubankcode;

    private String taxbureaubankaccno;

    private String billingtype;

    private BigDecimal rgtmoney;

    private BigDecimal asset;

    private BigDecimal netprofitrate;

    private String mainbussiness;

    private String corporation;

    private String comaera;

    private String phone;

    private String satrap;

    private Date founddate;

    private String grpgroupno;

    private String blacklistflag;

    private String state;

    private String remark;

    private String vipvalue;

    private String operator;

    private Date makedate;

    private String maketime;

    private Date modifydate;

    private String modifytime;

    private String subcompanyflag;

    private String supcustoemrno;

    private String levelcode;

    private Short offworkpeoples;

    private Short otherpeoples;

    private String businessbigtype;

    private String socialinsuno;

    private String blacklistreason;

    private String comno;

    private String comwebsite;

    private String busregnum;

    private String corporationidno;

    private String comcorporationno;

    private String contrshar;

    private String hodingidtype;

    private String hodingidno;

    private Date hodingvalidate;

    private String corporationidtype;

    private Date corporationvalidate;


    public String getComdocidno() {
        return comdocidno;
    }

    public void setComdocidno(String comdocidno) {
        this.comdocidno = comdocidno;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getGrpaddress() {
        return grpaddress;
    }

    public void setGrpaddress(String grpaddress) {
        this.grpaddress = grpaddress;
    }

    public String getUnitregisteredaddress() {
        return unitregisteredaddress;
    }

    public void setUnitregisteredaddress(String unitregisteredaddress) {
        this.unitregisteredaddress = unitregisteredaddress;
    }

    public BigDecimal getUnitduration() {
        return unitduration;
    }

    public void setUnitduration(BigDecimal unitduration) {
        this.unitduration = unitduration;
    }

    public String getPrincipalidtype() {
        return principalidtype;
    }

    public void setPrincipalidtype(String principalidtype) {
        this.principalidtype = principalidtype;
    }

    public String getPrincipalidno() {
        return principalidno;
    }

    public void setPrincipalidno(String principalidno) {
        this.principalidno = principalidno;
    }

    public Date getPrincipalvalidate() {
        return principalvalidate;
    }

    public void setPrincipalvalidate(Date principalvalidate) {
        this.principalvalidate = principalvalidate;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getAnnuityreceivedate() {
        return annuityreceivedate;
    }

    public void setAnnuityreceivedate(Date annuityreceivedate) {
        this.annuityreceivedate = annuityreceivedate;
    }

    public String getHeadship() {
        return headship;
    }

    public void setHeadship(String headship) {
        this.headship = headship;
    }

    public String getDisputedflag() {
        return disputedflag;
    }

    public void setDisputedflag(String disputedflag) {
        this.disputedflag = disputedflag;
    }

    public String getAccname() {
        return accname;
    }

    public void setAccname(String accname) {
        this.accname = accname;
    }

    public String getBankproivnce() {
        return bankproivnce;
    }

    public void setBankproivnce(String bankproivnce) {
        this.bankproivnce = bankproivnce;
    }

    public String getBankcity() {
        return bankcity;
    }

    public void setBankcity(String bankcity) {
        this.bankcity = bankcity;
    }

    public String getAcctype() {
        return acctype;
    }

    public void setAcctype(String acctype) {
        this.acctype = acctype;
    }

    public String getBanklocations() {
        return banklocations;
    }

    public void setBanklocations(String banklocations) {
        this.banklocations = banklocations;
    }

    public String getTaxpayeridtype() {
        return taxpayeridtype;
    }

    public void setTaxpayeridtype(String taxpayeridtype) {
        this.taxpayeridtype = taxpayeridtype;
    }

    public String getTaxpayeridno() {
        return taxpayeridno;
    }

    public void setTaxpayeridno(String taxpayeridno) {
        this.taxpayeridno = taxpayeridno;
    }

    public String getTaxbureauaddr() {
        return taxbureauaddr;
    }

    public void setTaxbureauaddr(String taxbureauaddr) {
        this.taxbureauaddr = taxbureauaddr;
    }

    public String getTaxbureautel() {
        return taxbureautel;
    }

    public void setTaxbureautel(String taxbureautel) {
        this.taxbureautel = taxbureautel;
    }

    public String getTaxbureaubankcode() {
        return taxbureaubankcode;
    }

    public void setTaxbureaubankcode(String taxbureaubankcode) {
        this.taxbureaubankcode = taxbureaubankcode;
    }

    public String getTaxbureaubankaccno() {
        return taxbureaubankaccno;
    }

    public void setTaxbureaubankaccno(String taxbureaubankaccno) {
        this.taxbureaubankaccno = taxbureaubankaccno;
    }

    public String getBillingtype() {
        return billingtype;
    }

    public void setBillingtype(String billingtype) {
        this.billingtype = billingtype;
    }

    public String getBlacklistreason() {
        return blacklistreason;
    }

    public void setBlacklistreason(String blacklistreason) {
        this.blacklistreason = blacklistreason;
    }

    public String getComno() {
        return comno;
    }

    public void setComno(String comno) {
        this.comno = comno;
    }

    public String getComwebsite() {
        return comwebsite;
    }

    public void setComwebsite(String comwebsite) {
        this.comwebsite = comwebsite;
    }

    public String getBusregnum() {
        return busregnum;
    }

    public void setBusregnum(String busregnum) {
        this.busregnum = busregnum;
    }

    public String getCorporationidno() {
        return corporationidno;
    }

    public void setCorporationidno(String corporationidno) {
        this.corporationidno = corporationidno;
    }

    public String getComcorporationno() {
        return comcorporationno;
    }

    public void setComcorporationno(String comcorporationno) {
        this.comcorporationno = comcorporationno;
    }

    public String getContrshar() {
        return contrshar;
    }

    public void setContrshar(String contrshar) {
        this.contrshar = contrshar;
    }

    public String getHodingidtype() {
        return hodingidtype;
    }

    public void setHodingidtype(String hodingidtype) {
        this.hodingidtype = hodingidtype;
    }

    public String getHodingidno() {
        return hodingidno;
    }

    public void setHodingidno(String hodingidno) {
        this.hodingidno = hodingidno;
    }

    public Date getHodingvalidate() {
        return hodingvalidate;
    }

    public void setHodingvalidate(Date hodingvalidate) {
        this.hodingvalidate = hodingvalidate;
    }

    public String getCorporationidtype() {
        return corporationidtype;
    }

    public void setCorporationidtype(String corporationidtype) {
        this.corporationidtype = corporationidtype;
    }

    public Date getCorporationvalidate() {
        return corporationvalidate;
    }

    public void setCorporationvalidate(Date corporationvalidate) {
        this.corporationvalidate = corporationvalidate;
    }

    public Date getBuslicedate() {
        return buslicedate;
    }

    public void setBuslicedate(Date buslicedate) {
        this.buslicedate = buslicedate;
    }

    public String getComdocidtype() {
        return comdocidtype;
    }

    public void setComdocidtype(String comdocidtype) {
        this.comdocidtype = comdocidtype;
    }

    public Short getInsurepeoples() {
        return insurepeoples;
    }

    public void setInsurepeoples(Short insurepeoples) {
        this.insurepeoples = insurepeoples;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getCustomerno() {
        return customerno;
    }

    public void setCustomerno(String customerno) {
        this.customerno = customerno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGrpname() {
        return grpname;
    }

    public void setGrpname(String grpname) {
        this.grpname = grpname;
    }

    public String getBusinesstype() {
        return businesstype;
    }

    public void setBusinesstype(String businesstype) {
        this.businesstype = businesstype;
    }

    public String getGrpnature() {
        return grpnature;
    }

    public void setGrpnature(String grpnature) {
        this.grpnature = grpnature;
    }

    public Short getPeoples() {
        return peoples;
    }

    public void setPeoples(Short peoples) {
        this.peoples = peoples;
    }

    public BigDecimal getRgtmoney() {
        return rgtmoney;
    }

    public void setRgtmoney(BigDecimal rgtmoney) {
        this.rgtmoney = rgtmoney;
    }

    public BigDecimal getAsset() {
        return asset;
    }

    public void setAsset(BigDecimal asset) {
        this.asset = asset;
    }

    public BigDecimal getNetprofitrate() {
        return netprofitrate;
    }

    public void setNetprofitrate(BigDecimal netprofitrate) {
        this.netprofitrate = netprofitrate;
    }

    public String getMainbussiness() {
        return mainbussiness;
    }

    public void setMainbussiness(String mainbussiness) {
        this.mainbussiness = mainbussiness;
    }

    public String getCorporation() {
        return corporation;
    }

    public void setCorporation(String corporation) {
        this.corporation = corporation;
    }

    public String getComaera() {
        return comaera;
    }

    public void setComaera(String comaera) {
        this.comaera = comaera;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGetflag() {
        return getflag;
    }

    public void setGetflag(String getflag) {
        this.getflag = getflag;
    }

    public String getSatrap() {
        return satrap;
    }

    public void setSatrap(String satrap) {
        this.satrap = satrap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFounddate() {
        return founddate;
    }

    public void setFounddate(Date founddate) {
        this.founddate = founddate;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankaccno() {
        return bankaccno;
    }

    public void setBankaccno(String bankaccno) {
        this.bankaccno = bankaccno;
    }

    public String getGrpgroupno() {
        return grpgroupno;
    }

    public void setGrpgroupno(String grpgroupno) {
        this.grpgroupno = grpgroupno;
    }

    public String getBlacklistflag() {
        return blacklistflag;
    }

    public void setBlacklistflag(String blacklistflag) {
        this.blacklistflag = blacklistflag;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getVipvalue() {
        return vipvalue;
    }

    public void setVipvalue(String vipvalue) {
        this.vipvalue = vipvalue;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getMakedate() {
        return makedate;
    }

    public void setMakedate(Date makedate) {
        this.makedate = makedate;
    }

    public String getMaketime() {
        return maketime;
    }

    public void setMaketime(String maketime) {
        this.maketime = maketime;
    }

    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    public String getModifytime() {
        return modifytime;
    }

    public void setModifytime(String modifytime) {
        this.modifytime = modifytime;
    }

    public String getSubcompanyflag() {
        return subcompanyflag;
    }

    public void setSubcompanyflag(String subcompanyflag) {
        this.subcompanyflag = subcompanyflag;
    }

    public String getSupcustoemrno() {
        return supcustoemrno;
    }

    public void setSupcustoemrno(String supcustoemrno) {
        this.supcustoemrno = supcustoemrno;
    }

    public String getLevelcode() {
        return levelcode;
    }

    public void setLevelcode(String levelcode) {
        this.levelcode = levelcode;
    }

    public Short getOnworkpeoples() {
        return onworkpeoples;
    }

    public void setOnworkpeoples(Short onworkpeoples) {
        this.onworkpeoples = onworkpeoples;
    }

    public Short getOffworkpeoples() {
        return offworkpeoples;
    }

    public void setOffworkpeoples(Short offworkpeoples) {
        this.offworkpeoples = offworkpeoples;
    }

    public Short getOtherpeoples() {
        return otherpeoples;
    }

    public void setOtherpeoples(Short otherpeoples) {
        this.otherpeoples = otherpeoples;
    }

    public String getBusinessbigtype() {
        return businessbigtype;
    }

    public void setBusinessbigtype(String businessbigtype) {
        this.businessbigtype = businessbigtype;
    }

    public String getSocialinsuno() {
        return socialinsuno;
    }

    public void setSocialinsuno(String socialinsuno) {
        this.socialinsuno = socialinsuno;
    }

    public String getManagecom() {
        return managecom;
    }

    public void setManagecom(String managecom) {
        this.managecom = managecom;
    }


}