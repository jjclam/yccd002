package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;

public class LCCont implements Serializable {
    private Integer contID;

    private String shardingID;

    private String grpContNo;

    private String contNo;

    private String proposalContNo;

    private String prtNo;

    private String contType;

    private String familyType;

    private String familyID;

    private String polType;

    private String cardFlag;

    private String manageCom;

    private String executeCom;

    private String agentCom;

    private String agentCode;

    private String agentGroup;

    private String agentCode1;

    private String agentType;

    private String saleChnl;

    private String handler;

    private String password;

    private String appntNo;

    private String appntName;

    private String appntSex;

    private Date appntBirthday;

    private String appntIDType;

    private String appntIDNo;

    private String insuredNo;

    private String insuredName;

    private String insuredSex;

    private Date insuredBirthday;

    private String insuredIDType;

    private String insuredIDNo;

    private Integer payIntv;

    private String payMode;

    private String payLocation;

    private String disputedFlag;

    private String outPayFlag;

    private String getPolMode;

    private String signCom;

    private Date signDate;

    private String signTime;

    private String consignNo;

    private String bankCode;

    private String bankAccNo;

    private String accName;

    private Integer printCount;

    private Integer lostTimes;

    private String lang;

    private String currency;

    private String remark;

    private Integer peoples;

    private Float mult;

    private Float prem;

    private Float amnt;

    private Float sumPrem;

    private Float dif;

    private Date paytoDate;

    private Date firstPayDate;

    private Date cvaliDate;

    private String inputOperator;

    private Date inputDate;

    private String inputTime;

    private String approveFlag;

    private String approveCode;

    private Date approveDate;

    private String approveTime;

    private String UWFlag;

    private String UWOperator;

    private Date UWDate;

    private String UWTime;

    private String appFlag;

    private Date polApplyDate;

    private Date getPolDate;

    private String getPolTime;

    private Date customGetPolDate;

    private String state;

    private String operator;

    private Date makeDate;

    private String makeTime;

    private Date modifyDate;

    private String modifyTime;

    private String firstTrialOperator;

    private Date firstTrialDate;

    private String firstTrialTime;

    private String receiveOperator;

    private Date receiveDate;

    private String receiveTime;

    private String tempFeeNo;

    private String sellType;

    private String forceUWFlag;

    private String forceUWReason;

    private String newBankCode;

    private String newBankAccNo;

    private String newAccName;

    private String newPayMode;

    private String agentBankCode;

    private String bankAgent;

    private String bankAgentName;

    private String bankAgentTel;

    private String prodSetCode;

    private String policyNo;

    private String billPressNo;

    private String cardTypeCode;

    private Date visitDate;

    private String visitTime;

    private String saleCom;

    private String printFlag;

    private String invoicePrtFlag;

    private String newReinsureFlag;

    private String renewPayFlag;

    private String appntFirstName;

    private String appntLastName;

    private String insuredFirstName;

    private String insuredLastName;

    private String authorFlag;

    private Integer greenChnl;

    private String TBType;

    private String EAuto;

    private String slipForm;

    private String autoPayFlag;

    private Integer rnewFlag;

    private String familyContNo;

    private String bussFlag;

    private String signName;

    private Date organizeDate;

    private String organizeTime;

    private String newAutoSendBankFlag;

    private String agentCodeOper;

    private String agentCodeAssi;

    private String delayReasonCode;

    private String delayReasonDesc;

    private String XQremindflag;

    private String organComCode;

    private String bankProivnce;

    private String newBankProivnce;

    private String arbitrationCom;

    private String otherPrtno;

    private Date destAccountDate;

    private String prePayCount;

    private String newBankCity;

    private String insuredBankCode;

    private String insuredBankAccNo;

    private String insuredAccName;

    private String insuredBankProvince;

    private String insuredBankCity;

    private String newAccType;

    private String accType;

    private String bankCity;

    private Float estimateMoney;

    private String insuredAccType;

    private String isAllowedCharges;

    private String WTEdorAcceptNo;

    private String recording;

    private String saleChannels;

    private String ZJAgentCom;

    private String ZJAgentComName;

    private String recordFlag;

    private String planCode;

    private String receiptNo;

    private String reissueCont;

    private String domainCode;

    private String domainName;

    private String referralNo;

    private String thirdPartyOrderId;

    private String userId;

    private static final long serialVersionUID = 1L;

    public Integer getContID() {
        return contID;
    }

    public void setContID(Integer contID) {
        this.contID = contID;
    }

    public String getShardingID() {
        return shardingID;
    }

    public void setShardingID(String shardingID) {
        this.shardingID = shardingID;
    }

    public String getGrpContNo() {
        return grpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        this.grpContNo = grpContNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getProposalContNo() {
        return proposalContNo;
    }

    public void setProposalContNo(String proposalContNo) {
        this.proposalContNo = proposalContNo;
    }

    public String getPrtNo() {
        return prtNo;
    }

    public void setPrtNo(String prtNo) {
        this.prtNo = prtNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getFamilyType() {
        return familyType;
    }

    public void setFamilyType(String familyType) {
        this.familyType = familyType;
    }

    public String getFamilyID() {
        return familyID;
    }

    public void setFamilyID(String familyID) {
        this.familyID = familyID;
    }

    public String getPolType() {
        return polType;
    }

    public void setPolType(String polType) {
        this.polType = polType;
    }

    public String getCardFlag() {
        return cardFlag;
    }

    public void setCardFlag(String cardFlag) {
        this.cardFlag = cardFlag;
    }

    public String getManageCom() {
        return manageCom;
    }

    public void setManageCom(String manageCom) {
        this.manageCom = manageCom;
    }

    public String getExecuteCom() {
        return executeCom;
    }

    public void setExecuteCom(String executeCom) {
        this.executeCom = executeCom;
    }

    public String getAgentCom() {
        return agentCom;
    }

    public void setAgentCom(String agentCom) {
        this.agentCom = agentCom;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentGroup() {
        return agentGroup;
    }

    public void setAgentGroup(String agentGroup) {
        this.agentGroup = agentGroup;
    }

    public String getAgentCode1() {
        return agentCode1;
    }

    public void setAgentCode1(String agentCode1) {
        this.agentCode1 = agentCode1;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public String getSaleChnl() {
        return saleChnl;
    }

    public void setSaleChnl(String saleChnl) {
        this.saleChnl = saleChnl;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAppntNo() {
        return appntNo;
    }

    public void setAppntNo(String appntNo) {
        this.appntNo = appntNo;
    }

    public String getAppntName() {
        return appntName;
    }

    public void setAppntName(String appntName) {
        this.appntName = appntName;
    }

    public String getAppntSex() {
        return appntSex;
    }

    public void setAppntSex(String appntSex) {
        this.appntSex = appntSex;
    }

    public Date getAppntBirthday() {
        return appntBirthday;
    }

    public void setAppntBirthday(Date appntBirthday) {
        this.appntBirthday = appntBirthday;
    }

    public String getAppntIDType() {
        return appntIDType;
    }

    public void setAppntIDType(String appntIDType) {
        this.appntIDType = appntIDType;
    }

    public String getAppntIDNo() {
        return appntIDNo;
    }

    public void setAppntIDNo(String appntIDNo) {
        this.appntIDNo = appntIDNo;
    }

    public String getInsuredNo() {
        return insuredNo;
    }

    public void setInsuredNo(String insuredNo) {
        this.insuredNo = insuredNo;
    }

    public String getInsuredName() {
        return insuredName;
    }

    public void setInsuredName(String insuredName) {
        this.insuredName = insuredName;
    }

    public String getInsuredSex() {
        return insuredSex;
    }

    public void setInsuredSex(String insuredSex) {
        this.insuredSex = insuredSex;
    }

    public Date getInsuredBirthday() {
        return insuredBirthday;
    }

    public void setInsuredBirthday(Date insuredBirthday) {
        this.insuredBirthday = insuredBirthday;
    }

    public String getInsuredIDType() {
        return insuredIDType;
    }

    public void setInsuredIDType(String insuredIDType) {
        this.insuredIDType = insuredIDType;
    }

    public String getInsuredIDNo() {
        return insuredIDNo;
    }

    public void setInsuredIDNo(String insuredIDNo) {
        this.insuredIDNo = insuredIDNo;
    }

    public Integer getPayIntv() {
        return payIntv;
    }

    public void setPayIntv(Integer payIntv) {
        this.payIntv = payIntv;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getPayLocation() {
        return payLocation;
    }

    public void setPayLocation(String payLocation) {
        this.payLocation = payLocation;
    }

    public String getDisputedFlag() {
        return disputedFlag;
    }

    public void setDisputedFlag(String disputedFlag) {
        this.disputedFlag = disputedFlag;
    }

    public String getOutPayFlag() {
        return outPayFlag;
    }

    public void setOutPayFlag(String outPayFlag) {
        this.outPayFlag = outPayFlag;
    }

    public String getGetPolMode() {
        return getPolMode;
    }

    public void setGetPolMode(String getPolMode) {
        this.getPolMode = getPolMode;
    }

    public String getSignCom() {
        return signCom;
    }

    public void setSignCom(String signCom) {
        this.signCom = signCom;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getSignTime() {
        return signTime;
    }

    public void setSignTime(String signTime) {
        this.signTime = signTime;
    }

    public String getConsignNo() {
        return consignNo;
    }

    public void setConsignNo(String consignNo) {
        this.consignNo = consignNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public Integer getPrintCount() {
        return printCount;
    }

    public void setPrintCount(Integer printCount) {
        this.printCount = printCount;
    }

    public Integer getLostTimes() {
        return lostTimes;
    }

    public void setLostTimes(Integer lostTimes) {
        this.lostTimes = lostTimes;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getPeoples() {
        return peoples;
    }

    public void setPeoples(Integer peoples) {
        this.peoples = peoples;
    }

    public Float getMult() {
        return mult;
    }

    public void setMult(Float mult) {
        this.mult = mult;
    }

    public Float getPrem() {
        return prem;
    }

    public void setPrem(Float prem) {
        this.prem = prem;
    }

    public Float getAmnt() {
        return amnt;
    }

    public void setAmnt(Float amnt) {
        this.amnt = amnt;
    }

    public Float getSumPrem() {
        return sumPrem;
    }

    public void setSumPrem(Float sumPrem) {
        this.sumPrem = sumPrem;
    }

    public Float getDif() {
        return dif;
    }

    public void setDif(Float dif) {
        this.dif = dif;
    }

    public Date getPaytoDate() {
        return paytoDate;
    }

    public void setPaytoDate(Date paytoDate) {
        this.paytoDate = paytoDate;
    }

    public Date getFirstPayDate() {
        return firstPayDate;
    }

    public void setFirstPayDate(Date firstPayDate) {
        this.firstPayDate = firstPayDate;
    }

    public Date getcvaliDate() {
        return cvaliDate;
    }

    public void setcvaliDate(Date cvaliDate) {
        this.cvaliDate = cvaliDate;
    }

    public String getInputOperator() {
        return inputOperator;
    }

    public void setInputOperator(String inputOperator) {
        this.inputOperator = inputOperator;
    }

    public Date getInputDate() {
        return inputDate;
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }

    public String getInputTime() {
        return inputTime;
    }

    public void setInputTime(String inputTime) {
        this.inputTime = inputTime;
    }

    public String getApproveFlag() {
        return approveFlag;
    }

    public void setApproveFlag(String approveFlag) {
        this.approveFlag = approveFlag;
    }

    public String getApproveCode() {
        return approveCode;
    }

    public void setApproveCode(String approveCode) {
        this.approveCode = approveCode;
    }

    public Date getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(Date approveDate) {
        this.approveDate = approveDate;
    }

    public String getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime;
    }

    public String getUWFlag() {
        return UWFlag;
    }

    public void setUWFlag(String UWFlag) {
        this.UWFlag = UWFlag;
    }

    public String getUWOperator() {
        return UWOperator;
    }

    public void setUWOperator(String UWOperator) {
        this.UWOperator = UWOperator;
    }

    public Date getUWDate() {
        return UWDate;
    }

    public void setUWDate(Date UWDate) {
        this.UWDate = UWDate;
    }

    public String getUWTime() {
        return UWTime;
    }

    public void setUWTime(String UWTime) {
        this.UWTime = UWTime;
    }

    public String getAppFlag() {
        return appFlag;
    }

    public void setAppFlag(String appFlag) {
        this.appFlag = appFlag;
    }

    public Date getPolApplyDate() {
        return polApplyDate;
    }

    public void setPolApplyDate(Date polApplyDate) {
        this.polApplyDate = polApplyDate;
    }

    public Date getGetPolDate() {
        return getPolDate;
    }

    public void setGetPolDate(Date getPolDate) {
        this.getPolDate = getPolDate;
    }

    public String getGetPolTime() {
        return getPolTime;
    }

    public void setGetPolTime(String getPolTime) {
        this.getPolTime = getPolTime;
    }

    public Date getCustomGetPolDate() {
        return customGetPolDate;
    }

    public void setCustomGetPolDate(Date customGetPolDate) {
        this.customGetPolDate = customGetPolDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(Date makeDate) {
        this.makeDate = makeDate;
    }

    public String getMakeTime() {
        return makeTime;
    }

    public void setMakeTime(String makeTime) {
        this.makeTime = makeTime;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getFirstTrialOperator() {
        return firstTrialOperator;
    }

    public void setFirstTrialOperator(String firstTrialOperator) {
        this.firstTrialOperator = firstTrialOperator;
    }

    public Date getFirstTrialDate() {
        return firstTrialDate;
    }

    public void setFirstTrialDate(Date firstTrialDate) {
        this.firstTrialDate = firstTrialDate;
    }

    public String getFirstTrialTime() {
        return firstTrialTime;
    }

    public void setFirstTrialTime(String firstTrialTime) {
        this.firstTrialTime = firstTrialTime;
    }

    public String getReceiveOperator() {
        return receiveOperator;
    }

    public void setReceiveOperator(String receiveOperator) {
        this.receiveOperator = receiveOperator;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getTempFeeNo() {
        return tempFeeNo;
    }

    public void setTempFeeNo(String tempFeeNo) {
        this.tempFeeNo = tempFeeNo;
    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public String getForceUWFlag() {
        return forceUWFlag;
    }

    public void setForceUWFlag(String forceUWFlag) {
        this.forceUWFlag = forceUWFlag;
    }

    public String getForceUWReason() {
        return forceUWReason;
    }

    public void setForceUWReason(String forceUWReason) {
        this.forceUWReason = forceUWReason;
    }

    public String getNewBankCode() {
        return newBankCode;
    }

    public void setNewBankCode(String newBankCode) {
        this.newBankCode = newBankCode;
    }

    public String getNewBankAccNo() {
        return newBankAccNo;
    }

    public void setNewBankAccNo(String newBankAccNo) {
        this.newBankAccNo = newBankAccNo;
    }

    public String getNewAccName() {
        return newAccName;
    }

    public void setNewAccName(String newAccName) {
        this.newAccName = newAccName;
    }

    public String getNewPayMode() {
        return newPayMode;
    }

    public void setNewPayMode(String newPayMode) {
        this.newPayMode = newPayMode;
    }

    public String getAgentBankCode() {
        return agentBankCode;
    }

    public void setAgentBankCode(String agentBankCode) {
        this.agentBankCode = agentBankCode;
    }

    public String getBankAgent() {
        return bankAgent;
    }

    public void setBankAgent(String bankAgent) {
        this.bankAgent = bankAgent;
    }

    public String getBankAgentName() {
        return bankAgentName;
    }

    public void setBankAgentName(String bankAgentName) {
        this.bankAgentName = bankAgentName;
    }

    public String getBankAgentTel() {
        return bankAgentTel;
    }

    public void setBankAgentTel(String bankAgentTel) {
        this.bankAgentTel = bankAgentTel;
    }

    public String getProdSetCode() {
        return prodSetCode;
    }

    public void setProdSetCode(String prodSetCode) {
        this.prodSetCode = prodSetCode;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getBillPressNo() {
        return billPressNo;
    }

    public void setBillPressNo(String billPressNo) {
        this.billPressNo = billPressNo;
    }

    public String getCardTypeCode() {
        return cardTypeCode;
    }

    public void setCardTypeCode(String cardTypeCode) {
        this.cardTypeCode = cardTypeCode;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(String visitTime) {
        this.visitTime = visitTime;
    }

    public String getSaleCom() {
        return saleCom;
    }

    public void setSaleCom(String saleCom) {
        this.saleCom = saleCom;
    }

    public String getPrintFlag() {
        return printFlag;
    }

    public void setPrintFlag(String printFlag) {
        this.printFlag = printFlag;
    }

    public String getInvoicePrtFlag() {
        return invoicePrtFlag;
    }

    public void setInvoicePrtFlag(String invoicePrtFlag) {
        this.invoicePrtFlag = invoicePrtFlag;
    }

    public String getNewReinsureFlag() {
        return newReinsureFlag;
    }

    public void setNewReinsureFlag(String newReinsureFlag) {
        this.newReinsureFlag = newReinsureFlag;
    }

    public String getRenewPayFlag() {
        return renewPayFlag;
    }

    public void setRenewPayFlag(String renewPayFlag) {
        this.renewPayFlag = renewPayFlag;
    }

    public String getAppntFirstName() {
        return appntFirstName;
    }

    public void setAppntFirstName(String appntFirstName) {
        this.appntFirstName = appntFirstName;
    }

    public String getAppntLastName() {
        return appntLastName;
    }

    public void setAppntLastName(String appntLastName) {
        this.appntLastName = appntLastName;
    }

    public String getInsuredFirstName() {
        return insuredFirstName;
    }

    public void setInsuredFirstName(String insuredFirstName) {
        this.insuredFirstName = insuredFirstName;
    }

    public String getInsuredLastName() {
        return insuredLastName;
    }

    public void setInsuredLastName(String insuredLastName) {
        this.insuredLastName = insuredLastName;
    }

    public String getAuthorFlag() {
        return authorFlag;
    }

    public void setAuthorFlag(String authorFlag) {
        this.authorFlag = authorFlag;
    }

    public Integer getGreenChnl() {
        return greenChnl;
    }

    public void setGreenChnl(Integer greenChnl) {
        this.greenChnl = greenChnl;
    }

    public String getTBType() {
        return TBType;
    }

    public void setTBType(String TBType) {
        this.TBType = TBType;
    }

    public String getEAuto() {
        return EAuto;
    }

    public void setEAuto(String EAuto) {
        this.EAuto = EAuto;
    }

    public String getSlipForm() {
        return slipForm;
    }

    public void setSlipForm(String slipForm) {
        this.slipForm = slipForm;
    }

    public String getAutoPayFlag() {
        return autoPayFlag;
    }

    public void setAutoPayFlag(String autoPayFlag) {
        this.autoPayFlag = autoPayFlag;
    }

    public Integer getRnewFlag() {
        return rnewFlag;
    }

    public void setRnewFlag(Integer rnewFlag) {
        this.rnewFlag = rnewFlag;
    }

    public String getFamilyContNo() {
        return familyContNo;
    }

    public void setFamilyContNo(String familyContNo) {
        this.familyContNo = familyContNo;
    }

    public String getBussFlag() {
        return bussFlag;
    }

    public void setBussFlag(String bussFlag) {
        this.bussFlag = bussFlag;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public Date getOrganizeDate() {
        return organizeDate;
    }

    public void setOrganizeDate(Date organizeDate) {
        this.organizeDate = organizeDate;
    }

    public String getOrganizeTime() {
        return organizeTime;
    }

    public void setOrganizeTime(String organizeTime) {
        this.organizeTime = organizeTime;
    }

    public String getNewAutoSendBankFlag() {
        return newAutoSendBankFlag;
    }

    public void setNewAutoSendBankFlag(String newAutoSendBankFlag) {
        this.newAutoSendBankFlag = newAutoSendBankFlag;
    }

    public String getAgentCodeOper() {
        return agentCodeOper;
    }

    public void setAgentCodeOper(String agentCodeOper) {
        this.agentCodeOper = agentCodeOper;
    }

    public String getAgentCodeAssi() {
        return agentCodeAssi;
    }

    public void setAgentCodeAssi(String agentCodeAssi) {
        this.agentCodeAssi = agentCodeAssi;
    }

    public String getDelayReasonCode() {
        return delayReasonCode;
    }

    public void setDelayReasonCode(String delayReasonCode) {
        this.delayReasonCode = delayReasonCode;
    }

    public String getDelayReasonDesc() {
        return delayReasonDesc;
    }

    public void setDelayReasonDesc(String delayReasonDesc) {
        this.delayReasonDesc = delayReasonDesc;
    }

    public String getXQremindflag() {
        return XQremindflag;
    }

    public void setXQremindflag(String XQremindflag) {
        this.XQremindflag = XQremindflag;
    }

    public String getOrganComCode() {
        return organComCode;
    }

    public void setOrganComCode(String organComCode) {
        this.organComCode = organComCode;
    }

    public String getBankProivnce() {
        return bankProivnce;
    }

    public void setBankProivnce(String bankProivnce) {
        this.bankProivnce = bankProivnce;
    }

    public String getNewBankProivnce() {
        return newBankProivnce;
    }

    public void setNewBankProivnce(String newBankProivnce) {
        this.newBankProivnce = newBankProivnce;
    }

    public String getArbitrationCom() {
        return arbitrationCom;
    }

    public void setArbitrationCom(String arbitrationCom) {
        this.arbitrationCom = arbitrationCom;
    }

    public String getOtherPrtno() {
        return otherPrtno;
    }

    public void setOtherPrtno(String otherPrtno) {
        this.otherPrtno = otherPrtno;
    }

    public Date getDestAccountDate() {
        return destAccountDate;
    }

    public void setDestAccountDate(Date destAccountDate) {
        this.destAccountDate = destAccountDate;
    }

    public String getPrePayCount() {
        return prePayCount;
    }

    public void setPrePayCount(String prePayCount) {
        this.prePayCount = prePayCount;
    }

    public String getNewBankCity() {
        return newBankCity;
    }

    public void setNewBankCity(String newBankCity) {
        this.newBankCity = newBankCity;
    }

    public String getInsuredBankCode() {
        return insuredBankCode;
    }

    public void setInsuredBankCode(String insuredBankCode) {
        this.insuredBankCode = insuredBankCode;
    }

    public String getInsuredBankAccNo() {
        return insuredBankAccNo;
    }

    public void setInsuredBankAccNo(String insuredBankAccNo) {
        this.insuredBankAccNo = insuredBankAccNo;
    }

    public String getInsuredAccName() {
        return insuredAccName;
    }

    public void setInsuredAccName(String insuredAccName) {
        this.insuredAccName = insuredAccName;
    }

    public String getInsuredBankProvince() {
        return insuredBankProvince;
    }

    public void setInsuredBankProvince(String insuredBankProvince) {
        this.insuredBankProvince = insuredBankProvince;
    }

    public String getInsuredBankCity() {
        return insuredBankCity;
    }

    public void setInsuredBankCity(String insuredBankCity) {
        this.insuredBankCity = insuredBankCity;
    }

    public String getNewAccType() {
        return newAccType;
    }

    public void setNewAccType(String newAccType) {
        this.newAccType = newAccType;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getBankCity() {
        return bankCity;
    }

    public void setBankCity(String bankCity) {
        this.bankCity = bankCity;
    }

    public Float getEstimateMoney() {
        return estimateMoney;
    }

    public void setEstimateMoney(Float estimateMoney) {
        this.estimateMoney = estimateMoney;
    }

    public String getInsuredAccType() {
        return insuredAccType;
    }

    public void setInsuredAccType(String insuredAccType) {
        this.insuredAccType = insuredAccType;
    }

    public String getIsAllowedCharges() {
        return isAllowedCharges;
    }

    public void setIsAllowedCharges(String isAllowedCharges) {
        this.isAllowedCharges = isAllowedCharges;
    }

    public String getWTEdorAcceptNo() {
        return WTEdorAcceptNo;
    }

    public void setWTEdorAcceptNo(String WTEdorAcceptNo) {
        this.WTEdorAcceptNo = WTEdorAcceptNo;
    }

    public String getRecording() {
        return recording;
    }

    public void setRecording(String recording) {
        this.recording = recording;
    }

    public String getSaleChannels() {
        return saleChannels;
    }

    public void setSaleChannels(String saleChannels) {
        this.saleChannels = saleChannels;
    }

    public String getZJAgentCom() {
        return ZJAgentCom;
    }

    public void setZJAgentCom(String ZJAgentCom) {
        this.ZJAgentCom = ZJAgentCom;
    }

    public String getZJAgentComName() {
        return ZJAgentComName;
    }

    public void setZJAgentComName(String ZJAgentComName) {
        this.ZJAgentComName = ZJAgentComName;
    }

    public String getRecordFlag() {
        return recordFlag;
    }

    public void setRecordFlag(String recordFlag) {
        this.recordFlag = recordFlag;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getReissueCont() {
        return reissueCont;
    }

    public void setReissueCont(String reissueCont) {
        this.reissueCont = reissueCont;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getReferralNo() {
        return referralNo;
    }

    public void setReferralNo(String referralNo) {
        this.referralNo = referralNo;
    }

    public String getThirdPartyOrderId() {
        return thirdPartyOrderId;
    }

    public void setThirdPartyOrderId(String thirdPartyOrderId) {
        this.thirdPartyOrderId = thirdPartyOrderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", contID=").append(contID);
        sb.append(", shardingID=").append(shardingID);
        sb.append(", grpContNo=").append(grpContNo);
        sb.append(", contNo=").append(contNo);
        sb.append(", proposalContNo=").append(proposalContNo);
        sb.append(", prtNo=").append(prtNo);
        sb.append(", contType=").append(contType);
        sb.append(", familyType=").append(familyType);
        sb.append(", familyID=").append(familyID);
        sb.append(", polType=").append(polType);
        sb.append(", cardFlag=").append(cardFlag);
        sb.append(", manageCom=").append(manageCom);
        sb.append(", executeCom=").append(executeCom);
        sb.append(", agentCom=").append(agentCom);
        sb.append(", agentCode=").append(agentCode);
        sb.append(", agentGroup=").append(agentGroup);
        sb.append(", agentCode1=").append(agentCode1);
        sb.append(", agentType=").append(agentType);
        sb.append(", saleChnl=").append(saleChnl);
        sb.append(", handler=").append(handler);
        sb.append(", password=").append(password);
        sb.append(", appntNo=").append(appntNo);
        sb.append(", appntName=").append(appntName);
        sb.append(", appntSex=").append(appntSex);
        sb.append(", appntBirthday=").append(appntBirthday);
        sb.append(", appntIDType=").append(appntIDType);
        sb.append(", appntIDNo=").append(appntIDNo);
        sb.append(", insuredNo=").append(insuredNo);
        sb.append(", insuredName=").append(insuredName);
        sb.append(", insuredSex=").append(insuredSex);
        sb.append(", insuredBirthday=").append(insuredBirthday);
        sb.append(", insuredIDType=").append(insuredIDType);
        sb.append(", insuredIDNo=").append(insuredIDNo);
        sb.append(", payIntv=").append(payIntv);
        sb.append(", payMode=").append(payMode);
        sb.append(", payLocation=").append(payLocation);
        sb.append(", disputedFlag=").append(disputedFlag);
        sb.append(", outPayFlag=").append(outPayFlag);
        sb.append(", getPolMode=").append(getPolMode);
        sb.append(", signCom=").append(signCom);
        sb.append(", signDate=").append(signDate);
        sb.append(", signTime=").append(signTime);
        sb.append(", consignNo=").append(consignNo);
        sb.append(", bankCode=").append(bankCode);
        sb.append(", bankAccNo=").append(bankAccNo);
        sb.append(", accName=").append(accName);
        sb.append(", printCount=").append(printCount);
        sb.append(", lostTimes=").append(lostTimes);
        sb.append(", lang=").append(lang);
        sb.append(", currency=").append(currency);
        sb.append(", remark=").append(remark);
        sb.append(", peoples=").append(peoples);
        sb.append(", mult=").append(mult);
        sb.append(", prem=").append(prem);
        sb.append(", amnt=").append(amnt);
        sb.append(", sumPrem=").append(sumPrem);
        sb.append(", dif=").append(dif);
        sb.append(", paytoDate=").append(paytoDate);
        sb.append(", firstPayDate=").append(firstPayDate);
        sb.append(", cvaliDate=").append(cvaliDate);
        sb.append(", inputOperator=").append(inputOperator);
        sb.append(", inputDate=").append(inputDate);
        sb.append(", inputTime=").append(inputTime);
        sb.append(", approveFlag=").append(approveFlag);
        sb.append(", approveCode=").append(approveCode);
        sb.append(", approveDate=").append(approveDate);
        sb.append(", approveTime=").append(approveTime);
        sb.append(", UWFlag=").append(UWFlag);
        sb.append(", UWOperator=").append(UWOperator);
        sb.append(", UWDate=").append(UWDate);
        sb.append(", UWTime=").append(UWTime);
        sb.append(", appFlag=").append(appFlag);
        sb.append(", polApplyDate=").append(polApplyDate);
        sb.append(", getPolDate=").append(getPolDate);
        sb.append(", getPolTime=").append(getPolTime);
        sb.append(", customGetPolDate=").append(customGetPolDate);
        sb.append(", state=").append(state);
        sb.append(", operator=").append(operator);
        sb.append(", makeDate=").append(makeDate);
        sb.append(", makeTime=").append(makeTime);
        sb.append(", modifyDate=").append(modifyDate);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", firstTrialOperator=").append(firstTrialOperator);
        sb.append(", firstTrialDate=").append(firstTrialDate);
        sb.append(", firstTrialTime=").append(firstTrialTime);
        sb.append(", receiveOperator=").append(receiveOperator);
        sb.append(", receiveDate=").append(receiveDate);
        sb.append(", receiveTime=").append(receiveTime);
        sb.append(", tempFeeNo=").append(tempFeeNo);
        sb.append(", sellType=").append(sellType);
        sb.append(", forceUWFlag=").append(forceUWFlag);
        sb.append(", forceUWReason=").append(forceUWReason);
        sb.append(", newBankCode=").append(newBankCode);
        sb.append(", newBankAccNo=").append(newBankAccNo);
        sb.append(", newAccName=").append(newAccName);
        sb.append(", newPayMode=").append(newPayMode);
        sb.append(", agentBankCode=").append(agentBankCode);
        sb.append(", bankAgent=").append(bankAgent);
        sb.append(", bankAgentName=").append(bankAgentName);
        sb.append(", bankAgentTel=").append(bankAgentTel);
        sb.append(", prodSetCode=").append(prodSetCode);
        sb.append(", policyNo=").append(policyNo);
        sb.append(", billPressNo=").append(billPressNo);
        sb.append(", cardTypeCode=").append(cardTypeCode);
        sb.append(", visitDate=").append(visitDate);
        sb.append(", visitTime=").append(visitTime);
        sb.append(", saleCom=").append(saleCom);
        sb.append(", printFlag=").append(printFlag);
        sb.append(", invoicePrtFlag=").append(invoicePrtFlag);
        sb.append(", newReinsureFlag=").append(newReinsureFlag);
        sb.append(", renewPayFlag=").append(renewPayFlag);
        sb.append(", appntFirstName=").append(appntFirstName);
        sb.append(", appntLastName=").append(appntLastName);
        sb.append(", insuredFirstName=").append(insuredFirstName);
        sb.append(", insuredLastName=").append(insuredLastName);
        sb.append(", authorFlag=").append(authorFlag);
        sb.append(", greenChnl=").append(greenChnl);
        sb.append(", TBType=").append(TBType);
        sb.append(", EAuto=").append(EAuto);
        sb.append(", slipForm=").append(slipForm);
        sb.append(", autoPayFlag=").append(autoPayFlag);
        sb.append(", rnewFlag=").append(rnewFlag);
        sb.append(", familyContNo=").append(familyContNo);
        sb.append(", bussFlag=").append(bussFlag);
        sb.append(", signName=").append(signName);
        sb.append(", organizeDate=").append(organizeDate);
        sb.append(", organizeTime=").append(organizeTime);
        sb.append(", newAutoSendBankFlag=").append(newAutoSendBankFlag);
        sb.append(", agentCodeOper=").append(agentCodeOper);
        sb.append(", agentCodeAssi=").append(agentCodeAssi);
        sb.append(", delayReasonCode=").append(delayReasonCode);
        sb.append(", delayReasonDesc=").append(delayReasonDesc);
        sb.append(", XQremindflag=").append(XQremindflag);
        sb.append(", organComCode=").append(organComCode);
        sb.append(", bankProivnce=").append(bankProivnce);
        sb.append(", newBankProivnce=").append(newBankProivnce);
        sb.append(", arbitrationCom=").append(arbitrationCom);
        sb.append(", otherPrtno=").append(otherPrtno);
        sb.append(", destAccountDate=").append(destAccountDate);
        sb.append(", prePayCount=").append(prePayCount);
        sb.append(", newBankCity=").append(newBankCity);
        sb.append(", insuredBankCode=").append(insuredBankCode);
        sb.append(", insuredBankAccNo=").append(insuredBankAccNo);
        sb.append(", insuredAccName=").append(insuredAccName);
        sb.append(", insuredBankProvince=").append(insuredBankProvince);
        sb.append(", insuredBankCity=").append(insuredBankCity);
        sb.append(", newAccType=").append(newAccType);
        sb.append(", accType=").append(accType);
        sb.append(", bankCity=").append(bankCity);
        sb.append(", estimateMoney=").append(estimateMoney);
        sb.append(", insuredAccType=").append(insuredAccType);
        sb.append(", isAllowedCharges=").append(isAllowedCharges);
        sb.append(", WTEdorAcceptNo=").append(WTEdorAcceptNo);
        sb.append(", recording=").append(recording);
        sb.append(", saleChannels=").append(saleChannels);
        sb.append(", ZJAgentCom=").append(ZJAgentCom);
        sb.append(", ZJAgentComName=").append(ZJAgentComName);
        sb.append(", recordFlag=").append(recordFlag);
        sb.append(", planCode=").append(planCode);
        sb.append(", receiptNo=").append(receiptNo);
        sb.append(", reissueCont=").append(reissueCont);
        sb.append(", domainCode=").append(domainCode);
        sb.append(", domainName=").append(domainName);
        sb.append(", referralNo=").append(referralNo);
        sb.append(", thirdPartyOrderId=").append(thirdPartyOrderId);
        sb.append(", userId=").append(userId);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LCCont other = (LCCont) that;
        return (this.getContID() == null ? other.getContID() == null : this.getContID().equals(other.getContID()))
            && (this.getShardingID() == null ? other.getShardingID() == null : this.getShardingID().equals(other.getShardingID()))
            && (this.getGrpContNo() == null ? other.getGrpContNo() == null : this.getGrpContNo().equals(other.getGrpContNo()))
            && (this.getContNo() == null ? other.getContNo() == null : this.getContNo().equals(other.getContNo()))
            && (this.getProposalContNo() == null ? other.getProposalContNo() == null : this.getProposalContNo().equals(other.getProposalContNo()))
            && (this.getPrtNo() == null ? other.getPrtNo() == null : this.getPrtNo().equals(other.getPrtNo()))
            && (this.getContType() == null ? other.getContType() == null : this.getContType().equals(other.getContType()))
            && (this.getFamilyType() == null ? other.getFamilyType() == null : this.getFamilyType().equals(other.getFamilyType()))
            && (this.getFamilyID() == null ? other.getFamilyID() == null : this.getFamilyID().equals(other.getFamilyID()))
            && (this.getPolType() == null ? other.getPolType() == null : this.getPolType().equals(other.getPolType()))
            && (this.getCardFlag() == null ? other.getCardFlag() == null : this.getCardFlag().equals(other.getCardFlag()))
            && (this.getManageCom() == null ? other.getManageCom() == null : this.getManageCom().equals(other.getManageCom()))
            && (this.getExecuteCom() == null ? other.getExecuteCom() == null : this.getExecuteCom().equals(other.getExecuteCom()))
            && (this.getAgentCom() == null ? other.getAgentCom() == null : this.getAgentCom().equals(other.getAgentCom()))
            && (this.getAgentCode() == null ? other.getAgentCode() == null : this.getAgentCode().equals(other.getAgentCode()))
            && (this.getAgentGroup() == null ? other.getAgentGroup() == null : this.getAgentGroup().equals(other.getAgentGroup()))
            && (this.getAgentCode1() == null ? other.getAgentCode1() == null : this.getAgentCode1().equals(other.getAgentCode1()))
            && (this.getAgentType() == null ? other.getAgentType() == null : this.getAgentType().equals(other.getAgentType()))
            && (this.getSaleChnl() == null ? other.getSaleChnl() == null : this.getSaleChnl().equals(other.getSaleChnl()))
            && (this.getHandler() == null ? other.getHandler() == null : this.getHandler().equals(other.getHandler()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getAppntNo() == null ? other.getAppntNo() == null : this.getAppntNo().equals(other.getAppntNo()))
            && (this.getAppntName() == null ? other.getAppntName() == null : this.getAppntName().equals(other.getAppntName()))
            && (this.getAppntSex() == null ? other.getAppntSex() == null : this.getAppntSex().equals(other.getAppntSex()))
            && (this.getAppntBirthday() == null ? other.getAppntBirthday() == null : this.getAppntBirthday().equals(other.getAppntBirthday()))
            && (this.getAppntIDType() == null ? other.getAppntIDType() == null : this.getAppntIDType().equals(other.getAppntIDType()))
            && (this.getAppntIDNo() == null ? other.getAppntIDNo() == null : this.getAppntIDNo().equals(other.getAppntIDNo()))
            && (this.getInsuredNo() == null ? other.getInsuredNo() == null : this.getInsuredNo().equals(other.getInsuredNo()))
            && (this.getInsuredName() == null ? other.getInsuredName() == null : this.getInsuredName().equals(other.getInsuredName()))
            && (this.getInsuredSex() == null ? other.getInsuredSex() == null : this.getInsuredSex().equals(other.getInsuredSex()))
            && (this.getInsuredBirthday() == null ? other.getInsuredBirthday() == null : this.getInsuredBirthday().equals(other.getInsuredBirthday()))
            && (this.getInsuredIDType() == null ? other.getInsuredIDType() == null : this.getInsuredIDType().equals(other.getInsuredIDType()))
            && (this.getInsuredIDNo() == null ? other.getInsuredIDNo() == null : this.getInsuredIDNo().equals(other.getInsuredIDNo()))
            && (this.getPayIntv() == null ? other.getPayIntv() == null : this.getPayIntv().equals(other.getPayIntv()))
            && (this.getPayMode() == null ? other.getPayMode() == null : this.getPayMode().equals(other.getPayMode()))
            && (this.getPayLocation() == null ? other.getPayLocation() == null : this.getPayLocation().equals(other.getPayLocation()))
            && (this.getDisputedFlag() == null ? other.getDisputedFlag() == null : this.getDisputedFlag().equals(other.getDisputedFlag()))
            && (this.getOutPayFlag() == null ? other.getOutPayFlag() == null : this.getOutPayFlag().equals(other.getOutPayFlag()))
            && (this.getGetPolMode() == null ? other.getGetPolMode() == null : this.getGetPolMode().equals(other.getGetPolMode()))
            && (this.getSignCom() == null ? other.getSignCom() == null : this.getSignCom().equals(other.getSignCom()))
            && (this.getSignDate() == null ? other.getSignDate() == null : this.getSignDate().equals(other.getSignDate()))
            && (this.getSignTime() == null ? other.getSignTime() == null : this.getSignTime().equals(other.getSignTime()))
            && (this.getConsignNo() == null ? other.getConsignNo() == null : this.getConsignNo().equals(other.getConsignNo()))
            && (this.getBankCode() == null ? other.getBankCode() == null : this.getBankCode().equals(other.getBankCode()))
            && (this.getBankAccNo() == null ? other.getBankAccNo() == null : this.getBankAccNo().equals(other.getBankAccNo()))
            && (this.getAccName() == null ? other.getAccName() == null : this.getAccName().equals(other.getAccName()))
            && (this.getPrintCount() == null ? other.getPrintCount() == null : this.getPrintCount().equals(other.getPrintCount()))
            && (this.getLostTimes() == null ? other.getLostTimes() == null : this.getLostTimes().equals(other.getLostTimes()))
            && (this.getLang() == null ? other.getLang() == null : this.getLang().equals(other.getLang()))
            && (this.getCurrency() == null ? other.getCurrency() == null : this.getCurrency().equals(other.getCurrency()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
            && (this.getPeoples() == null ? other.getPeoples() == null : this.getPeoples().equals(other.getPeoples()))
            && (this.getMult() == null ? other.getMult() == null : this.getMult().equals(other.getMult()))
            && (this.getPrem() == null ? other.getPrem() == null : this.getPrem().equals(other.getPrem()))
            && (this.getAmnt() == null ? other.getAmnt() == null : this.getAmnt().equals(other.getAmnt()))
            && (this.getSumPrem() == null ? other.getSumPrem() == null : this.getSumPrem().equals(other.getSumPrem()))
            && (this.getDif() == null ? other.getDif() == null : this.getDif().equals(other.getDif()))
            && (this.getPaytoDate() == null ? other.getPaytoDate() == null : this.getPaytoDate().equals(other.getPaytoDate()))
            && (this.getFirstPayDate() == null ? other.getFirstPayDate() == null : this.getFirstPayDate().equals(other.getFirstPayDate()))
            && (this.getcvaliDate() == null ? other.getcvaliDate() == null : this.getcvaliDate().equals(other.getcvaliDate()))
            && (this.getInputOperator() == null ? other.getInputOperator() == null : this.getInputOperator().equals(other.getInputOperator()))
            && (this.getInputDate() == null ? other.getInputDate() == null : this.getInputDate().equals(other.getInputDate()))
            && (this.getInputTime() == null ? other.getInputTime() == null : this.getInputTime().equals(other.getInputTime()))
            && (this.getApproveFlag() == null ? other.getApproveFlag() == null : this.getApproveFlag().equals(other.getApproveFlag()))
            && (this.getApproveCode() == null ? other.getApproveCode() == null : this.getApproveCode().equals(other.getApproveCode()))
            && (this.getApproveDate() == null ? other.getApproveDate() == null : this.getApproveDate().equals(other.getApproveDate()))
            && (this.getApproveTime() == null ? other.getApproveTime() == null : this.getApproveTime().equals(other.getApproveTime()))
            && (this.getUWFlag() == null ? other.getUWFlag() == null : this.getUWFlag().equals(other.getUWFlag()))
            && (this.getUWOperator() == null ? other.getUWOperator() == null : this.getUWOperator().equals(other.getUWOperator()))
            && (this.getUWDate() == null ? other.getUWDate() == null : this.getUWDate().equals(other.getUWDate()))
            && (this.getUWTime() == null ? other.getUWTime() == null : this.getUWTime().equals(other.getUWTime()))
            && (this.getAppFlag() == null ? other.getAppFlag() == null : this.getAppFlag().equals(other.getAppFlag()))
            && (this.getPolApplyDate() == null ? other.getPolApplyDate() == null : this.getPolApplyDate().equals(other.getPolApplyDate()))
            && (this.getGetPolDate() == null ? other.getGetPolDate() == null : this.getGetPolDate().equals(other.getGetPolDate()))
            && (this.getGetPolTime() == null ? other.getGetPolTime() == null : this.getGetPolTime().equals(other.getGetPolTime()))
            && (this.getCustomGetPolDate() == null ? other.getCustomGetPolDate() == null : this.getCustomGetPolDate().equals(other.getCustomGetPolDate()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
            && (this.getOperator() == null ? other.getOperator() == null : this.getOperator().equals(other.getOperator()))
            && (this.getMakeDate() == null ? other.getMakeDate() == null : this.getMakeDate().equals(other.getMakeDate()))
            && (this.getMakeTime() == null ? other.getMakeTime() == null : this.getMakeTime().equals(other.getMakeTime()))
            && (this.getModifyDate() == null ? other.getModifyDate() == null : this.getModifyDate().equals(other.getModifyDate()))
            && (this.getModifyTime() == null ? other.getModifyTime() == null : this.getModifyTime().equals(other.getModifyTime()))
            && (this.getFirstTrialOperator() == null ? other.getFirstTrialOperator() == null : this.getFirstTrialOperator().equals(other.getFirstTrialOperator()))
            && (this.getFirstTrialDate() == null ? other.getFirstTrialDate() == null : this.getFirstTrialDate().equals(other.getFirstTrialDate()))
            && (this.getFirstTrialTime() == null ? other.getFirstTrialTime() == null : this.getFirstTrialTime().equals(other.getFirstTrialTime()))
            && (this.getReceiveOperator() == null ? other.getReceiveOperator() == null : this.getReceiveOperator().equals(other.getReceiveOperator()))
            && (this.getReceiveDate() == null ? other.getReceiveDate() == null : this.getReceiveDate().equals(other.getReceiveDate()))
            && (this.getReceiveTime() == null ? other.getReceiveTime() == null : this.getReceiveTime().equals(other.getReceiveTime()))
            && (this.getTempFeeNo() == null ? other.getTempFeeNo() == null : this.getTempFeeNo().equals(other.getTempFeeNo()))
            && (this.getSellType() == null ? other.getSellType() == null : this.getSellType().equals(other.getSellType()))
            && (this.getForceUWFlag() == null ? other.getForceUWFlag() == null : this.getForceUWFlag().equals(other.getForceUWFlag()))
            && (this.getForceUWReason() == null ? other.getForceUWReason() == null : this.getForceUWReason().equals(other.getForceUWReason()))
            && (this.getNewBankCode() == null ? other.getNewBankCode() == null : this.getNewBankCode().equals(other.getNewBankCode()))
            && (this.getNewBankAccNo() == null ? other.getNewBankAccNo() == null : this.getNewBankAccNo().equals(other.getNewBankAccNo()))
            && (this.getNewAccName() == null ? other.getNewAccName() == null : this.getNewAccName().equals(other.getNewAccName()))
            && (this.getNewPayMode() == null ? other.getNewPayMode() == null : this.getNewPayMode().equals(other.getNewPayMode()))
            && (this.getAgentBankCode() == null ? other.getAgentBankCode() == null : this.getAgentBankCode().equals(other.getAgentBankCode()))
            && (this.getBankAgent() == null ? other.getBankAgent() == null : this.getBankAgent().equals(other.getBankAgent()))
            && (this.getBankAgentName() == null ? other.getBankAgentName() == null : this.getBankAgentName().equals(other.getBankAgentName()))
            && (this.getBankAgentTel() == null ? other.getBankAgentTel() == null : this.getBankAgentTel().equals(other.getBankAgentTel()))
            && (this.getProdSetCode() == null ? other.getProdSetCode() == null : this.getProdSetCode().equals(other.getProdSetCode()))
            && (this.getPolicyNo() == null ? other.getPolicyNo() == null : this.getPolicyNo().equals(other.getPolicyNo()))
            && (this.getBillPressNo() == null ? other.getBillPressNo() == null : this.getBillPressNo().equals(other.getBillPressNo()))
            && (this.getCardTypeCode() == null ? other.getCardTypeCode() == null : this.getCardTypeCode().equals(other.getCardTypeCode()))
            && (this.getVisitDate() == null ? other.getVisitDate() == null : this.getVisitDate().equals(other.getVisitDate()))
            && (this.getVisitTime() == null ? other.getVisitTime() == null : this.getVisitTime().equals(other.getVisitTime()))
            && (this.getSaleCom() == null ? other.getSaleCom() == null : this.getSaleCom().equals(other.getSaleCom()))
            && (this.getPrintFlag() == null ? other.getPrintFlag() == null : this.getPrintFlag().equals(other.getPrintFlag()))
            && (this.getInvoicePrtFlag() == null ? other.getInvoicePrtFlag() == null : this.getInvoicePrtFlag().equals(other.getInvoicePrtFlag()))
            && (this.getNewReinsureFlag() == null ? other.getNewReinsureFlag() == null : this.getNewReinsureFlag().equals(other.getNewReinsureFlag()))
            && (this.getRenewPayFlag() == null ? other.getRenewPayFlag() == null : this.getRenewPayFlag().equals(other.getRenewPayFlag()))
            && (this.getAppntFirstName() == null ? other.getAppntFirstName() == null : this.getAppntFirstName().equals(other.getAppntFirstName()))
            && (this.getAppntLastName() == null ? other.getAppntLastName() == null : this.getAppntLastName().equals(other.getAppntLastName()))
            && (this.getInsuredFirstName() == null ? other.getInsuredFirstName() == null : this.getInsuredFirstName().equals(other.getInsuredFirstName()))
            && (this.getInsuredLastName() == null ? other.getInsuredLastName() == null : this.getInsuredLastName().equals(other.getInsuredLastName()))
            && (this.getAuthorFlag() == null ? other.getAuthorFlag() == null : this.getAuthorFlag().equals(other.getAuthorFlag()))
            && (this.getGreenChnl() == null ? other.getGreenChnl() == null : this.getGreenChnl().equals(other.getGreenChnl()))
            && (this.getTBType() == null ? other.getTBType() == null : this.getTBType().equals(other.getTBType()))
            && (this.getEAuto() == null ? other.getEAuto() == null : this.getEAuto().equals(other.getEAuto()))
            && (this.getSlipForm() == null ? other.getSlipForm() == null : this.getSlipForm().equals(other.getSlipForm()))
            && (this.getAutoPayFlag() == null ? other.getAutoPayFlag() == null : this.getAutoPayFlag().equals(other.getAutoPayFlag()))
            && (this.getRnewFlag() == null ? other.getRnewFlag() == null : this.getRnewFlag().equals(other.getRnewFlag()))
            && (this.getFamilyContNo() == null ? other.getFamilyContNo() == null : this.getFamilyContNo().equals(other.getFamilyContNo()))
            && (this.getBussFlag() == null ? other.getBussFlag() == null : this.getBussFlag().equals(other.getBussFlag()))
            && (this.getSignName() == null ? other.getSignName() == null : this.getSignName().equals(other.getSignName()))
            && (this.getOrganizeDate() == null ? other.getOrganizeDate() == null : this.getOrganizeDate().equals(other.getOrganizeDate()))
            && (this.getOrganizeTime() == null ? other.getOrganizeTime() == null : this.getOrganizeTime().equals(other.getOrganizeTime()))
            && (this.getNewAutoSendBankFlag() == null ? other.getNewAutoSendBankFlag() == null : this.getNewAutoSendBankFlag().equals(other.getNewAutoSendBankFlag()))
            && (this.getAgentCodeOper() == null ? other.getAgentCodeOper() == null : this.getAgentCodeOper().equals(other.getAgentCodeOper()))
            && (this.getAgentCodeAssi() == null ? other.getAgentCodeAssi() == null : this.getAgentCodeAssi().equals(other.getAgentCodeAssi()))
            && (this.getDelayReasonCode() == null ? other.getDelayReasonCode() == null : this.getDelayReasonCode().equals(other.getDelayReasonCode()))
            && (this.getDelayReasonDesc() == null ? other.getDelayReasonDesc() == null : this.getDelayReasonDesc().equals(other.getDelayReasonDesc()))
            && (this.getXQremindflag() == null ? other.getXQremindflag() == null : this.getXQremindflag().equals(other.getXQremindflag()))
            && (this.getOrganComCode() == null ? other.getOrganComCode() == null : this.getOrganComCode().equals(other.getOrganComCode()))
            && (this.getBankProivnce() == null ? other.getBankProivnce() == null : this.getBankProivnce().equals(other.getBankProivnce()))
            && (this.getNewBankProivnce() == null ? other.getNewBankProivnce() == null : this.getNewBankProivnce().equals(other.getNewBankProivnce()))
            && (this.getArbitrationCom() == null ? other.getArbitrationCom() == null : this.getArbitrationCom().equals(other.getArbitrationCom()))
            && (this.getOtherPrtno() == null ? other.getOtherPrtno() == null : this.getOtherPrtno().equals(other.getOtherPrtno()))
            && (this.getDestAccountDate() == null ? other.getDestAccountDate() == null : this.getDestAccountDate().equals(other.getDestAccountDate()))
            && (this.getPrePayCount() == null ? other.getPrePayCount() == null : this.getPrePayCount().equals(other.getPrePayCount()))
            && (this.getNewBankCity() == null ? other.getNewBankCity() == null : this.getNewBankCity().equals(other.getNewBankCity()))
            && (this.getInsuredBankCode() == null ? other.getInsuredBankCode() == null : this.getInsuredBankCode().equals(other.getInsuredBankCode()))
            && (this.getInsuredBankAccNo() == null ? other.getInsuredBankAccNo() == null : this.getInsuredBankAccNo().equals(other.getInsuredBankAccNo()))
            && (this.getInsuredAccName() == null ? other.getInsuredAccName() == null : this.getInsuredAccName().equals(other.getInsuredAccName()))
            && (this.getInsuredBankProvince() == null ? other.getInsuredBankProvince() == null : this.getInsuredBankProvince().equals(other.getInsuredBankProvince()))
            && (this.getInsuredBankCity() == null ? other.getInsuredBankCity() == null : this.getInsuredBankCity().equals(other.getInsuredBankCity()))
            && (this.getNewAccType() == null ? other.getNewAccType() == null : this.getNewAccType().equals(other.getNewAccType()))
            && (this.getAccType() == null ? other.getAccType() == null : this.getAccType().equals(other.getAccType()))
            && (this.getBankCity() == null ? other.getBankCity() == null : this.getBankCity().equals(other.getBankCity()))
            && (this.getEstimateMoney() == null ? other.getEstimateMoney() == null : this.getEstimateMoney().equals(other.getEstimateMoney()))
            && (this.getInsuredAccType() == null ? other.getInsuredAccType() == null : this.getInsuredAccType().equals(other.getInsuredAccType()))
            && (this.getIsAllowedCharges() == null ? other.getIsAllowedCharges() == null : this.getIsAllowedCharges().equals(other.getIsAllowedCharges()))
            && (this.getWTEdorAcceptNo() == null ? other.getWTEdorAcceptNo() == null : this.getWTEdorAcceptNo().equals(other.getWTEdorAcceptNo()))
            && (this.getRecording() == null ? other.getRecording() == null : this.getRecording().equals(other.getRecording()))
            && (this.getSaleChannels() == null ? other.getSaleChannels() == null : this.getSaleChannels().equals(other.getSaleChannels()))
            && (this.getZJAgentCom() == null ? other.getZJAgentCom() == null : this.getZJAgentCom().equals(other.getZJAgentCom()))
            && (this.getZJAgentComName() == null ? other.getZJAgentComName() == null : this.getZJAgentComName().equals(other.getZJAgentComName()))
            && (this.getRecordFlag() == null ? other.getRecordFlag() == null : this.getRecordFlag().equals(other.getRecordFlag()))
            && (this.getPlanCode() == null ? other.getPlanCode() == null : this.getPlanCode().equals(other.getPlanCode()))
            && (this.getReceiptNo() == null ? other.getReceiptNo() == null : this.getReceiptNo().equals(other.getReceiptNo()))
            && (this.getReissueCont() == null ? other.getReissueCont() == null : this.getReissueCont().equals(other.getReissueCont()))
            && (this.getDomainCode() == null ? other.getDomainCode() == null : this.getDomainCode().equals(other.getDomainCode()))
            && (this.getDomainName() == null ? other.getDomainName() == null : this.getDomainName().equals(other.getDomainName()))
            && (this.getReferralNo() == null ? other.getReferralNo() == null : this.getReferralNo().equals(other.getReferralNo()))
            && (this.getThirdPartyOrderId() == null ? other.getThirdPartyOrderId() == null : this.getThirdPartyOrderId().equals(other.getThirdPartyOrderId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getContID() == null) ? 0 : getContID().hashCode());
        result = prime * result + ((getShardingID() == null) ? 0 : getShardingID().hashCode());
        result = prime * result + ((getGrpContNo() == null) ? 0 : getGrpContNo().hashCode());
        result = prime * result + ((getContNo() == null) ? 0 : getContNo().hashCode());
        result = prime * result + ((getProposalContNo() == null) ? 0 : getProposalContNo().hashCode());
        result = prime * result + ((getPrtNo() == null) ? 0 : getPrtNo().hashCode());
        result = prime * result + ((getContType() == null) ? 0 : getContType().hashCode());
        result = prime * result + ((getFamilyType() == null) ? 0 : getFamilyType().hashCode());
        result = prime * result + ((getFamilyID() == null) ? 0 : getFamilyID().hashCode());
        result = prime * result + ((getPolType() == null) ? 0 : getPolType().hashCode());
        result = prime * result + ((getCardFlag() == null) ? 0 : getCardFlag().hashCode());
        result = prime * result + ((getManageCom() == null) ? 0 : getManageCom().hashCode());
        result = prime * result + ((getExecuteCom() == null) ? 0 : getExecuteCom().hashCode());
        result = prime * result + ((getAgentCom() == null) ? 0 : getAgentCom().hashCode());
        result = prime * result + ((getAgentCode() == null) ? 0 : getAgentCode().hashCode());
        result = prime * result + ((getAgentGroup() == null) ? 0 : getAgentGroup().hashCode());
        result = prime * result + ((getAgentCode1() == null) ? 0 : getAgentCode1().hashCode());
        result = prime * result + ((getAgentType() == null) ? 0 : getAgentType().hashCode());
        result = prime * result + ((getSaleChnl() == null) ? 0 : getSaleChnl().hashCode());
        result = prime * result + ((getHandler() == null) ? 0 : getHandler().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getAppntNo() == null) ? 0 : getAppntNo().hashCode());
        result = prime * result + ((getAppntName() == null) ? 0 : getAppntName().hashCode());
        result = prime * result + ((getAppntSex() == null) ? 0 : getAppntSex().hashCode());
        result = prime * result + ((getAppntBirthday() == null) ? 0 : getAppntBirthday().hashCode());
        result = prime * result + ((getAppntIDType() == null) ? 0 : getAppntIDType().hashCode());
        result = prime * result + ((getAppntIDNo() == null) ? 0 : getAppntIDNo().hashCode());
        result = prime * result + ((getInsuredNo() == null) ? 0 : getInsuredNo().hashCode());
        result = prime * result + ((getInsuredName() == null) ? 0 : getInsuredName().hashCode());
        result = prime * result + ((getInsuredSex() == null) ? 0 : getInsuredSex().hashCode());
        result = prime * result + ((getInsuredBirthday() == null) ? 0 : getInsuredBirthday().hashCode());
        result = prime * result + ((getInsuredIDType() == null) ? 0 : getInsuredIDType().hashCode());
        result = prime * result + ((getInsuredIDNo() == null) ? 0 : getInsuredIDNo().hashCode());
        result = prime * result + ((getPayIntv() == null) ? 0 : getPayIntv().hashCode());
        result = prime * result + ((getPayMode() == null) ? 0 : getPayMode().hashCode());
        result = prime * result + ((getPayLocation() == null) ? 0 : getPayLocation().hashCode());
        result = prime * result + ((getDisputedFlag() == null) ? 0 : getDisputedFlag().hashCode());
        result = prime * result + ((getOutPayFlag() == null) ? 0 : getOutPayFlag().hashCode());
        result = prime * result + ((getGetPolMode() == null) ? 0 : getGetPolMode().hashCode());
        result = prime * result + ((getSignCom() == null) ? 0 : getSignCom().hashCode());
        result = prime * result + ((getSignDate() == null) ? 0 : getSignDate().hashCode());
        result = prime * result + ((getSignTime() == null) ? 0 : getSignTime().hashCode());
        result = prime * result + ((getConsignNo() == null) ? 0 : getConsignNo().hashCode());
        result = prime * result + ((getBankCode() == null) ? 0 : getBankCode().hashCode());
        result = prime * result + ((getBankAccNo() == null) ? 0 : getBankAccNo().hashCode());
        result = prime * result + ((getAccName() == null) ? 0 : getAccName().hashCode());
        result = prime * result + ((getPrintCount() == null) ? 0 : getPrintCount().hashCode());
        result = prime * result + ((getLostTimes() == null) ? 0 : getLostTimes().hashCode());
        result = prime * result + ((getLang() == null) ? 0 : getLang().hashCode());
        result = prime * result + ((getCurrency() == null) ? 0 : getCurrency().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        result = prime * result + ((getPeoples() == null) ? 0 : getPeoples().hashCode());
        result = prime * result + ((getMult() == null) ? 0 : getMult().hashCode());
        result = prime * result + ((getPrem() == null) ? 0 : getPrem().hashCode());
        result = prime * result + ((getAmnt() == null) ? 0 : getAmnt().hashCode());
        result = prime * result + ((getSumPrem() == null) ? 0 : getSumPrem().hashCode());
        result = prime * result + ((getDif() == null) ? 0 : getDif().hashCode());
        result = prime * result + ((getPaytoDate() == null) ? 0 : getPaytoDate().hashCode());
        result = prime * result + ((getFirstPayDate() == null) ? 0 : getFirstPayDate().hashCode());
        result = prime * result + ((getcvaliDate() == null) ? 0 : getcvaliDate().hashCode());
        result = prime * result + ((getInputOperator() == null) ? 0 : getInputOperator().hashCode());
        result = prime * result + ((getInputDate() == null) ? 0 : getInputDate().hashCode());
        result = prime * result + ((getInputTime() == null) ? 0 : getInputTime().hashCode());
        result = prime * result + ((getApproveFlag() == null) ? 0 : getApproveFlag().hashCode());
        result = prime * result + ((getApproveCode() == null) ? 0 : getApproveCode().hashCode());
        result = prime * result + ((getApproveDate() == null) ? 0 : getApproveDate().hashCode());
        result = prime * result + ((getApproveTime() == null) ? 0 : getApproveTime().hashCode());
        result = prime * result + ((getUWFlag() == null) ? 0 : getUWFlag().hashCode());
        result = prime * result + ((getUWOperator() == null) ? 0 : getUWOperator().hashCode());
        result = prime * result + ((getUWDate() == null) ? 0 : getUWDate().hashCode());
        result = prime * result + ((getUWTime() == null) ? 0 : getUWTime().hashCode());
        result = prime * result + ((getAppFlag() == null) ? 0 : getAppFlag().hashCode());
        result = prime * result + ((getPolApplyDate() == null) ? 0 : getPolApplyDate().hashCode());
        result = prime * result + ((getGetPolDate() == null) ? 0 : getGetPolDate().hashCode());
        result = prime * result + ((getGetPolTime() == null) ? 0 : getGetPolTime().hashCode());
        result = prime * result + ((getCustomGetPolDate() == null) ? 0 : getCustomGetPolDate().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getOperator() == null) ? 0 : getOperator().hashCode());
        result = prime * result + ((getMakeDate() == null) ? 0 : getMakeDate().hashCode());
        result = prime * result + ((getMakeTime() == null) ? 0 : getMakeTime().hashCode());
        result = prime * result + ((getModifyDate() == null) ? 0 : getModifyDate().hashCode());
        result = prime * result + ((getModifyTime() == null) ? 0 : getModifyTime().hashCode());
        result = prime * result + ((getFirstTrialOperator() == null) ? 0 : getFirstTrialOperator().hashCode());
        result = prime * result + ((getFirstTrialDate() == null) ? 0 : getFirstTrialDate().hashCode());
        result = prime * result + ((getFirstTrialTime() == null) ? 0 : getFirstTrialTime().hashCode());
        result = prime * result + ((getReceiveOperator() == null) ? 0 : getReceiveOperator().hashCode());
        result = prime * result + ((getReceiveDate() == null) ? 0 : getReceiveDate().hashCode());
        result = prime * result + ((getReceiveTime() == null) ? 0 : getReceiveTime().hashCode());
        result = prime * result + ((getTempFeeNo() == null) ? 0 : getTempFeeNo().hashCode());
        result = prime * result + ((getSellType() == null) ? 0 : getSellType().hashCode());
        result = prime * result + ((getForceUWFlag() == null) ? 0 : getForceUWFlag().hashCode());
        result = prime * result + ((getForceUWReason() == null) ? 0 : getForceUWReason().hashCode());
        result = prime * result + ((getNewBankCode() == null) ? 0 : getNewBankCode().hashCode());
        result = prime * result + ((getNewBankAccNo() == null) ? 0 : getNewBankAccNo().hashCode());
        result = prime * result + ((getNewAccName() == null) ? 0 : getNewAccName().hashCode());
        result = prime * result + ((getNewPayMode() == null) ? 0 : getNewPayMode().hashCode());
        result = prime * result + ((getAgentBankCode() == null) ? 0 : getAgentBankCode().hashCode());
        result = prime * result + ((getBankAgent() == null) ? 0 : getBankAgent().hashCode());
        result = prime * result + ((getBankAgentName() == null) ? 0 : getBankAgentName().hashCode());
        result = prime * result + ((getBankAgentTel() == null) ? 0 : getBankAgentTel().hashCode());
        result = prime * result + ((getProdSetCode() == null) ? 0 : getProdSetCode().hashCode());
        result = prime * result + ((getPolicyNo() == null) ? 0 : getPolicyNo().hashCode());
        result = prime * result + ((getBillPressNo() == null) ? 0 : getBillPressNo().hashCode());
        result = prime * result + ((getCardTypeCode() == null) ? 0 : getCardTypeCode().hashCode());
        result = prime * result + ((getVisitDate() == null) ? 0 : getVisitDate().hashCode());
        result = prime * result + ((getVisitTime() == null) ? 0 : getVisitTime().hashCode());
        result = prime * result + ((getSaleCom() == null) ? 0 : getSaleCom().hashCode());
        result = prime * result + ((getPrintFlag() == null) ? 0 : getPrintFlag().hashCode());
        result = prime * result + ((getInvoicePrtFlag() == null) ? 0 : getInvoicePrtFlag().hashCode());
        result = prime * result + ((getNewReinsureFlag() == null) ? 0 : getNewReinsureFlag().hashCode());
        result = prime * result + ((getRenewPayFlag() == null) ? 0 : getRenewPayFlag().hashCode());
        result = prime * result + ((getAppntFirstName() == null) ? 0 : getAppntFirstName().hashCode());
        result = prime * result + ((getAppntLastName() == null) ? 0 : getAppntLastName().hashCode());
        result = prime * result + ((getInsuredFirstName() == null) ? 0 : getInsuredFirstName().hashCode());
        result = prime * result + ((getInsuredLastName() == null) ? 0 : getInsuredLastName().hashCode());
        result = prime * result + ((getAuthorFlag() == null) ? 0 : getAuthorFlag().hashCode());
        result = prime * result + ((getGreenChnl() == null) ? 0 : getGreenChnl().hashCode());
        result = prime * result + ((getTBType() == null) ? 0 : getTBType().hashCode());
        result = prime * result + ((getEAuto() == null) ? 0 : getEAuto().hashCode());
        result = prime * result + ((getSlipForm() == null) ? 0 : getSlipForm().hashCode());
        result = prime * result + ((getAutoPayFlag() == null) ? 0 : getAutoPayFlag().hashCode());
        result = prime * result + ((getRnewFlag() == null) ? 0 : getRnewFlag().hashCode());
        result = prime * result + ((getFamilyContNo() == null) ? 0 : getFamilyContNo().hashCode());
        result = prime * result + ((getBussFlag() == null) ? 0 : getBussFlag().hashCode());
        result = prime * result + ((getSignName() == null) ? 0 : getSignName().hashCode());
        result = prime * result + ((getOrganizeDate() == null) ? 0 : getOrganizeDate().hashCode());
        result = prime * result + ((getOrganizeTime() == null) ? 0 : getOrganizeTime().hashCode());
        result = prime * result + ((getNewAutoSendBankFlag() == null) ? 0 : getNewAutoSendBankFlag().hashCode());
        result = prime * result + ((getAgentCodeOper() == null) ? 0 : getAgentCodeOper().hashCode());
        result = prime * result + ((getAgentCodeAssi() == null) ? 0 : getAgentCodeAssi().hashCode());
        result = prime * result + ((getDelayReasonCode() == null) ? 0 : getDelayReasonCode().hashCode());
        result = prime * result + ((getDelayReasonDesc() == null) ? 0 : getDelayReasonDesc().hashCode());
        result = prime * result + ((getXQremindflag() == null) ? 0 : getXQremindflag().hashCode());
        result = prime * result + ((getOrganComCode() == null) ? 0 : getOrganComCode().hashCode());
        result = prime * result + ((getBankProivnce() == null) ? 0 : getBankProivnce().hashCode());
        result = prime * result + ((getNewBankProivnce() == null) ? 0 : getNewBankProivnce().hashCode());
        result = prime * result + ((getArbitrationCom() == null) ? 0 : getArbitrationCom().hashCode());
        result = prime * result + ((getOtherPrtno() == null) ? 0 : getOtherPrtno().hashCode());
        result = prime * result + ((getDestAccountDate() == null) ? 0 : getDestAccountDate().hashCode());
        result = prime * result + ((getPrePayCount() == null) ? 0 : getPrePayCount().hashCode());
        result = prime * result + ((getNewBankCity() == null) ? 0 : getNewBankCity().hashCode());
        result = prime * result + ((getInsuredBankCode() == null) ? 0 : getInsuredBankCode().hashCode());
        result = prime * result + ((getInsuredBankAccNo() == null) ? 0 : getInsuredBankAccNo().hashCode());
        result = prime * result + ((getInsuredAccName() == null) ? 0 : getInsuredAccName().hashCode());
        result = prime * result + ((getInsuredBankProvince() == null) ? 0 : getInsuredBankProvince().hashCode());
        result = prime * result + ((getInsuredBankCity() == null) ? 0 : getInsuredBankCity().hashCode());
        result = prime * result + ((getNewAccType() == null) ? 0 : getNewAccType().hashCode());
        result = prime * result + ((getAccType() == null) ? 0 : getAccType().hashCode());
        result = prime * result + ((getBankCity() == null) ? 0 : getBankCity().hashCode());
        result = prime * result + ((getEstimateMoney() == null) ? 0 : getEstimateMoney().hashCode());
        result = prime * result + ((getInsuredAccType() == null) ? 0 : getInsuredAccType().hashCode());
        result = prime * result + ((getIsAllowedCharges() == null) ? 0 : getIsAllowedCharges().hashCode());
        result = prime * result + ((getWTEdorAcceptNo() == null) ? 0 : getWTEdorAcceptNo().hashCode());
        result = prime * result + ((getRecording() == null) ? 0 : getRecording().hashCode());
        result = prime * result + ((getSaleChannels() == null) ? 0 : getSaleChannels().hashCode());
        result = prime * result + ((getZJAgentCom() == null) ? 0 : getZJAgentCom().hashCode());
        result = prime * result + ((getZJAgentComName() == null) ? 0 : getZJAgentComName().hashCode());
        result = prime * result + ((getRecordFlag() == null) ? 0 : getRecordFlag().hashCode());
        result = prime * result + ((getPlanCode() == null) ? 0 : getPlanCode().hashCode());
        result = prime * result + ((getReceiptNo() == null) ? 0 : getReceiptNo().hashCode());
        result = prime * result + ((getReissueCont() == null) ? 0 : getReissueCont().hashCode());
        result = prime * result + ((getDomainCode() == null) ? 0 : getDomainCode().hashCode());
        result = prime * result + ((getDomainName() == null) ? 0 : getDomainName().hashCode());
        result = prime * result + ((getReferralNo() == null) ? 0 : getReferralNo().hashCode());
        result = prime * result + ((getThirdPartyOrderId() == null) ? 0 : getThirdPartyOrderId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        return result;
    }
}