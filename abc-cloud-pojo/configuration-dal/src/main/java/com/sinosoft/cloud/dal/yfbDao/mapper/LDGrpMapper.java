package com.sinosoft.cloud.dal.yfbDao.mapper;

import com.sinosoft.cloud.dal.dao.model.entity.LMCalModePojo;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LDGrpMapper {
    int deleteByPrimaryKey(String customerno);

    int insert(LMCalModePojo lmCalModePojo);

    int insertSelective(LDGrp record);

    LDGrp selectByPrimaryKey(String customerno);

    int updateByPrimaryKeySelective(LDGrp record);

    int updateByPrimaryKey(LDGrp record);

    List<LDGrp> getAllInstitutionsName();

    List<LDGrp> getAllInstitutionsNameByOutWorker();

    List<LDGrp> getAllInstitutionsNameByEnterprise();

    List<LMCalModePojo> selectByNoParam(@Param("lmCalModePojo") LMCalModePojo lmCalModePojo);

    int SelectGrpCount(@Param("lmCalModePojo")LMCalModePojo lmCalModePojo);

    /*List<LDGrp> selectByNoParam(@Param("ldGrp")LDGrp ldGrp);
    int SelectGrpCount(@Param("ldGrp")LDGrp ldGrp);

    void insert(@Param("ldGrp")LDGrp ldGrp);*/


}