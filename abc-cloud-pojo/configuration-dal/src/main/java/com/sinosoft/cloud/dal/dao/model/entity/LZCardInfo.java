package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class LZCardInfo implements Serializable {

    private String certifyCode;

    private String stateFlag;

    private String sendOut;

    private String receive;

    private String operator;

    private String handler;

    private String handlerDate;

    private String handlerStartDate;

    private String handlerEndDate;

    private String makeDate;

    private String makeStartDate;

    private String makeEndDate;

    private String startNo;

    private String endNo;


    public String getCertifyCode() {
        return certifyCode;
    }

    public void setCertifyCode(String certifyCode) {
        this.certifyCode = certifyCode;
    }

    public String getSendOut() {
        return sendOut;
    }

    public void setSendOut(String sendOut) {
        this.sendOut = sendOut;
    }

    public String getReceive() {
        return receive;
    }

    public void setReceive(String receive) {
        this.receive = receive;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

    public String getHandlerDate() {
        return handlerDate;
    }

    public void setHandlerDate(String handlerDate) {
        this.handlerDate = handlerDate;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getStartNo() {
        return startNo;
    }

    public void setStartNo(String startNo) {
        this.startNo = startNo;
    }

    public String getEndNo() {
        return endNo;
    }

    public void setEndNo(String endNo) {
        this.endNo = endNo;
    }

    public String getStateFlag() {
        return stateFlag;
    }

    public void setStateFlag(String stateFlag) {
        this.stateFlag = stateFlag;
    }

    public String getHandlerStartDate() {
        return handlerStartDate;
    }

    public void setHandlerStartDate(String handlerStartDate) {
        this.handlerStartDate = handlerStartDate;
    }

    public String getHandlerEndDate() {
        return handlerEndDate;
    }

    public void setHandlerEndDate(String handlerEndDate) {
        this.handlerEndDate = handlerEndDate;
    }

    public String getMakeStartDate() {
        return makeStartDate;
    }

    public void setMakeStartDate(String makeStartDate) {
        this.makeStartDate = makeStartDate;
    }

    public String getMakeEndDate() {
        return makeEndDate;
    }

    public void setMakeEndDate(String makeEndDate) {
        this.makeEndDate = makeEndDate;
    }

    @Override
    public String toString() {
        return "LZCardInfo{" +
                "certifyCode='" + certifyCode + '\'' +
                ", stateFlag='" + stateFlag + '\'' +
                ", sendOut='" + sendOut + '\'' +
                ", receive='" + receive + '\'' +
                ", operator='" + operator + '\'' +
                ", handler='" + handler + '\'' +
                ", handlerDate='" + handlerDate + '\'' +
                ", handlerStartDate='" + handlerStartDate + '\'' +
                ", handlerEndDate='" + handlerEndDate + '\'' +
                ", makeDate='" + makeDate + '\'' +
                ", makeStartDate='" + makeStartDate + '\'' +
                ", makeEndDate='" + makeEndDate + '\'' +
                ", startNo='" + startNo + '\'' +
                ", endNo='" + endNo + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LZCardInfo that = (LZCardInfo) o;
        return Objects.equals(certifyCode, that.certifyCode) &&
                Objects.equals(stateFlag, that.stateFlag) &&
                Objects.equals(sendOut, that.sendOut) &&
                Objects.equals(receive, that.receive) &&
                Objects.equals(operator, that.operator) &&
                Objects.equals(handler, that.handler) &&
                Objects.equals(handlerDate, that.handlerDate) &&
                Objects.equals(handlerStartDate, that.handlerStartDate) &&
                Objects.equals(handlerEndDate, that.handlerEndDate) &&
                Objects.equals(makeDate, that.makeDate) &&
                Objects.equals(makeStartDate, that.makeStartDate) &&
                Objects.equals(makeEndDate, that.makeEndDate) &&
                Objects.equals(startNo, that.startNo) &&
                Objects.equals(endNo, that.endNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(certifyCode, stateFlag, sendOut, receive, operator, handler, handlerDate, handlerStartDate, handlerEndDate, makeDate, makeStartDate, makeEndDate, startNo, endNo);
    }
}
