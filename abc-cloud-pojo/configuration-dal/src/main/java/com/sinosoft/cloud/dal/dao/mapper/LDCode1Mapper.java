package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LDCode1;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LDCode1Mapper {

    List<LDCode1> getChlChildTypeList();

    List<LDCode1> getAllChlChildType(@Param(value = "chlType") String chlTypeL);

    Integer countForChlMaintainList(@Param(value = "chlTypeL") String chlTypeL, @Param(value = "chlTypeC") String chlTypeC);

    List<LDCode1> getChlMaintainList(@Param(value = "chlTypeL") String chlTypeL, @Param(value = "chlTypeC") String chlTypeC);

    Integer addChlMaintain(Map<String,Object> map);

    Integer deleteChlMaintain(@Param(value = "chlTypeL") String chlTypeL, @Param(value = "chlTypeC") String chlTypeC);

    Integer countForChlCodeMaintainList(Map<String,Object> map);

    List<LDCode1> selectForChlCodeMaintainList(Map<String,Object> map);

    Integer verifychlcodetype(Map<String,Object> map);

    Integer addChlCodeMaintain(Map<String,Object> map);

//    Integer deleteChlCodeMaintain(@Param(value = "codeType") String codeType, @Param(value = "code") String codeValue);
    Integer deleteChlCodeMaintain(Map<String, Object> params);

    List<LDCode1> getAllCodeTypeInfo(@Param(value = "channelCode") String channelCode);

    Integer verifybasecodetype(@Param(value = "codeType") String codeType);

    Integer insertbasecodetype(Map<String,Object> map);

    List<LDCode1> getAllCodeValueInfo(@Param(value = "codeType") String codeType, @Param(value = "channelCode") String channelCode);

    Integer verifyChlExist(Map<String,Object> map);

    Integer verifychlcodetypeRef(Map<String,Object> map);
}