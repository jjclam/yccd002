package com.sinosoft.cloud.dal.dao.model.entity;


import java.io.Serializable;

public class ProInfo implements Serializable {
    /**
     * 序列
     */
    private String proId;

    /**
     * 产品编码
     */
    private String proCode;

    /**
     * 产品名称
     */
    private String proName;

    /**
     * 产品起售日期
     */
    private String startDate;

    /**
     * 产品停售日期
     */
    private String overDate;

    /**
     * 是否为标准产品
     */
    private String standard;

    /**
     * 产品上下架状态
     */
    private String status;

    private  String remark;
    private  String remark2;
    private  String planKind1;
    private  String planKind2;
    private  String planKind3;

    @Override
    public String toString() {
        return "ProInfo{" +
                "proId='" + proId + '\'' +
                ", proCode='" + proCode + '\'' +
                ", proName='" + proName + '\'' +
                ", startDate='" + startDate + '\'' +
                ", overDate='" + overDate + '\'' +
                ", standard='" + standard + '\'' +
                ", status='" + status + '\'' +
                ", remark='" + remark + '\'' +
                ", remark2='" + remark2 + '\'' +
                ", planKind1='" + planKind1 + '\'' +
                ", planKind2='" + planKind2 + '\'' +
                ", planKind3='" + planKind3 + '\'' +
                '}';
    }
    public ProInfo() {

    }
    public ProInfo(String proId, String proCode, String proName, String startDate, String overDate, String standard, String status, String remark, String remark2, String planKind1, String planKind2, String planKind3) {
        this.proId = proId;
        this.proCode = proCode;
        this.proName = proName;
        this.startDate = startDate;
        this.overDate = overDate;
        this.standard = standard;
        this.status = status;
        this.remark = remark;
        this.remark2 = remark2;
        this.planKind1 = planKind1;
        this.planKind2 = planKind2;
        this.planKind3 = planKind3;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getOverDate() {
        return overDate;
    }

    public void setOverDate(String overDate) {
        this.overDate = overDate;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getPlanKind1() {
        return planKind1;
    }

    public void setPlanKind1(String planKind1) {
        this.planKind1 = planKind1;
    }

    public String getPlanKind2() {
        return planKind2;
    }

    public void setPlanKind2(String planKind2) {
        this.planKind2 = planKind2;
    }

    public String getPlanKind3() {
        return planKind3;
    }

    public void setPlanKind3(String planKind3) {
        this.planKind3 = planKind3;
    }
}
