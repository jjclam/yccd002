package com.sinosoft.cloud.dal.yfbDao.model.entity;


public class Laagent {

  private String agentcode;
  private String agentgroup;
  private String managecom;
  private String password;
  private String entryno;
  private String name;
  private String sex;
  private String birthday;
  private String nativeplace;
  private String nationality;
  private String marriage;
  private String creditgrade;
  private String homeaddresscode;
  private String homeaddress;
  private String postaladdress;
  private String zipcode;
  private String phone;
  private String bp;
  private String mobile;
  private String email;
  private String marriagedate;
  private String idno;
  private String source;
  private String bloodtype;
  private String polityvisage;
  private String degree;
  private String graduateschool;
  private String speciality;
  private String posttitle;
  private String foreignlevel;
  private String workage;
  private String oldcom;
  private String oldoccupation;
  private String headship;
  private String recommendagent;
  private String business;
  private String salequaf;
  private String quafno;
  private String quafstartdate;
  private String quafenddate;
  private String devno1;
  private String devno2;
  private String retaincontno;
  private String agentkind;
  private String devgrade;
  private String insideflag;
  private String fulltimeflag;
  private String noworkflag;
  private String traindate;
  private String employdate;
  private String indueformdate;
  private String outworkdate;
  private String recommendno;
  private String cautionername;
  private String cautionersex;
  private String cautionerid;
  private String cautionerbirthday;
  private String approver;
  private String approvedate;
  private String assumoney;
  private String remark;
  private String agentstate;
  private String qualipassflag;
  private String smokeflag;
  private String rgtaddress;
  private String bankcode;
  private String bankaccno;
  private String operator;
  private String makedate;
  private String maketime;
  private String modifydate;
  private String modifytime;
  private String branchtype;
  private String trainperiods;
  private String branchcode;
  private String age;
  private String channelname;
  private String receiptno;
  private String idnotype;
  private String branchtype2;
  private String trainpassflag;
  private String emergentlink;
  private String emergentphone;
  private String retainstartdate;
  private String retainenddate;
  private String togaeflag;
  private String archievecode;
  private String affix;
  private String affixname;
  private String bankaddress;
  private String dxbranchtype2;
  private String dxsaleorg;


  public String getAgentcode() {
    return agentcode;
  }

  public void setAgentcode(String agentcode) {
    this.agentcode = agentcode;
  }


  public String getAgentgroup() {
    return agentgroup;
  }

  public void setAgentgroup(String agentgroup) {
    this.agentgroup = agentgroup;
  }


  public String getManagecom() {
    return managecom;
  }

  public void setManagecom(String managecom) {
    this.managecom = managecom;
  }


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  public String getEntryno() {
    return entryno;
  }

  public void setEntryno(String entryno) {
    this.entryno = entryno;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }


  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }


  public String getNativeplace() {
    return nativeplace;
  }

  public void setNativeplace(String nativeplace) {
    this.nativeplace = nativeplace;
  }


  public String getNationality() {
    return nationality;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }


  public String getMarriage() {
    return marriage;
  }

  public void setMarriage(String marriage) {
    this.marriage = marriage;
  }


  public String getCreditgrade() {
    return creditgrade;
  }

  public void setCreditgrade(String creditgrade) {
    this.creditgrade = creditgrade;
  }


  public String getHomeaddresscode() {
    return homeaddresscode;
  }

  public void setHomeaddresscode(String homeaddresscode) {
    this.homeaddresscode = homeaddresscode;
  }


  public String getHomeaddress() {
    return homeaddress;
  }

  public void setHomeaddress(String homeaddress) {
    this.homeaddress = homeaddress;
  }


  public String getPostaladdress() {
    return postaladdress;
  }

  public void setPostaladdress(String postaladdress) {
    this.postaladdress = postaladdress;
  }


  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  public String getBp() {
    return bp;
  }

  public void setBp(String bp) {
    this.bp = bp;
  }


  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getMarriagedate() {
    return marriagedate;
  }

  public void setMarriagedate(String marriagedate) {
    this.marriagedate = marriagedate;
  }


  public String getIdno() {
    return idno;
  }

  public void setIdno(String idno) {
    this.idno = idno;
  }


  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }


  public String getBloodtype() {
    return bloodtype;
  }

  public void setBloodtype(String bloodtype) {
    this.bloodtype = bloodtype;
  }


  public String getPolityvisage() {
    return polityvisage;
  }

  public void setPolityvisage(String polityvisage) {
    this.polityvisage = polityvisage;
  }


  public String getDegree() {
    return degree;
  }

  public void setDegree(String degree) {
    this.degree = degree;
  }


  public String getGraduateschool() {
    return graduateschool;
  }

  public void setGraduateschool(String graduateschool) {
    this.graduateschool = graduateschool;
  }


  public String getSpeciality() {
    return speciality;
  }

  public void setSpeciality(String speciality) {
    this.speciality = speciality;
  }


  public String getPosttitle() {
    return posttitle;
  }

  public void setPosttitle(String posttitle) {
    this.posttitle = posttitle;
  }


  public String getForeignlevel() {
    return foreignlevel;
  }

  public void setForeignlevel(String foreignlevel) {
    this.foreignlevel = foreignlevel;
  }


  public String getWorkage() {
    return workage;
  }

  public void setWorkage(String workage) {
    this.workage = workage;
  }


  public String getOldcom() {
    return oldcom;
  }

  public void setOldcom(String oldcom) {
    this.oldcom = oldcom;
  }


  public String getOldoccupation() {
    return oldoccupation;
  }

  public void setOldoccupation(String oldoccupation) {
    this.oldoccupation = oldoccupation;
  }


  public String getHeadship() {
    return headship;
  }

  public void setHeadship(String headship) {
    this.headship = headship;
  }


  public String getRecommendagent() {
    return recommendagent;
  }

  public void setRecommendagent(String recommendagent) {
    this.recommendagent = recommendagent;
  }


  public String getBusiness() {
    return business;
  }

  public void setBusiness(String business) {
    this.business = business;
  }


  public String getSalequaf() {
    return salequaf;
  }

  public void setSalequaf(String salequaf) {
    this.salequaf = salequaf;
  }


  public String getQuafno() {
    return quafno;
  }

  public void setQuafno(String quafno) {
    this.quafno = quafno;
  }


  public String getQuafstartdate() {
    return quafstartdate;
  }

  public void setQuafstartdate(String quafstartdate) {
    this.quafstartdate = quafstartdate;
  }


  public String getQuafenddate() {
    return quafenddate;
  }

  public void setQuafenddate(String quafenddate) {
    this.quafenddate = quafenddate;
  }


  public String getDevno1() {
    return devno1;
  }

  public void setDevno1(String devno1) {
    this.devno1 = devno1;
  }


  public String getDevno2() {
    return devno2;
  }

  public void setDevno2(String devno2) {
    this.devno2 = devno2;
  }


  public String getRetaincontno() {
    return retaincontno;
  }

  public void setRetaincontno(String retaincontno) {
    this.retaincontno = retaincontno;
  }


  public String getAgentkind() {
    return agentkind;
  }

  public void setAgentkind(String agentkind) {
    this.agentkind = agentkind;
  }


  public String getDevgrade() {
    return devgrade;
  }

  public void setDevgrade(String devgrade) {
    this.devgrade = devgrade;
  }


  public String getInsideflag() {
    return insideflag;
  }

  public void setInsideflag(String insideflag) {
    this.insideflag = insideflag;
  }


  public String getFulltimeflag() {
    return fulltimeflag;
  }

  public void setFulltimeflag(String fulltimeflag) {
    this.fulltimeflag = fulltimeflag;
  }


  public String getNoworkflag() {
    return noworkflag;
  }

  public void setNoworkflag(String noworkflag) {
    this.noworkflag = noworkflag;
  }


  public String getTraindate() {
    return traindate;
  }

  public void setTraindate(String traindate) {
    this.traindate = traindate;
  }


  public String getEmploydate() {
    return employdate;
  }

  public void setEmploydate(String employdate) {
    this.employdate = employdate;
  }


  public String getIndueformdate() {
    return indueformdate;
  }

  public void setIndueformdate(String indueformdate) {
    this.indueformdate = indueformdate;
  }


  public String getOutworkdate() {
    return outworkdate;
  }

  public void setOutworkdate(String outworkdate) {
    this.outworkdate = outworkdate;
  }


  public String getRecommendno() {
    return recommendno;
  }

  public void setRecommendno(String recommendno) {
    this.recommendno = recommendno;
  }


  public String getCautionername() {
    return cautionername;
  }

  public void setCautionername(String cautionername) {
    this.cautionername = cautionername;
  }


  public String getCautionersex() {
    return cautionersex;
  }

  public void setCautionersex(String cautionersex) {
    this.cautionersex = cautionersex;
  }


  public String getCautionerid() {
    return cautionerid;
  }

  public void setCautionerid(String cautionerid) {
    this.cautionerid = cautionerid;
  }


  public String getCautionerbirthday() {
    return cautionerbirthday;
  }

  public void setCautionerbirthday(String cautionerbirthday) {
    this.cautionerbirthday = cautionerbirthday;
  }


  public String getApprover() {
    return approver;
  }

  public void setApprover(String approver) {
    this.approver = approver;
  }


  public String getApprovedate() {
    return approvedate;
  }

  public void setApprovedate(String approvedate) {
    this.approvedate = approvedate;
  }


  public String getAssumoney() {
    return assumoney;
  }

  public void setAssumoney(String assumoney) {
    this.assumoney = assumoney;
  }


  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }


  public String getAgentstate() {
    return agentstate;
  }

  public void setAgentstate(String agentstate) {
    this.agentstate = agentstate;
  }


  public String getQualipassflag() {
    return qualipassflag;
  }

  public void setQualipassflag(String qualipassflag) {
    this.qualipassflag = qualipassflag;
  }


  public String getSmokeflag() {
    return smokeflag;
  }

  public void setSmokeflag(String smokeflag) {
    this.smokeflag = smokeflag;
  }


  public String getRgtaddress() {
    return rgtaddress;
  }

  public void setRgtaddress(String rgtaddress) {
    this.rgtaddress = rgtaddress;
  }


  public String getBankcode() {
    return bankcode;
  }

  public void setBankcode(String bankcode) {
    this.bankcode = bankcode;
  }


  public String getBankaccno() {
    return bankaccno;
  }

  public void setBankaccno(String bankaccno) {
    this.bankaccno = bankaccno;
  }


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }


  public String getMakedate() {
    return makedate;
  }

  public void setMakedate(String makedate) {
    this.makedate = makedate;
  }


  public String getMaketime() {
    return maketime;
  }

  public void setMaketime(String maketime) {
    this.maketime = maketime;
  }


  public String getModifydate() {
    return modifydate;
  }

  public void setModifydate(String modifydate) {
    this.modifydate = modifydate;
  }


  public String getModifytime() {
    return modifytime;
  }

  public void setModifytime(String modifytime) {
    this.modifytime = modifytime;
  }


  public String getBranchtype() {
    return branchtype;
  }

  public void setBranchtype(String branchtype) {
    this.branchtype = branchtype;
  }


  public String getTrainperiods() {
    return trainperiods;
  }

  public void setTrainperiods(String trainperiods) {
    this.trainperiods = trainperiods;
  }


  public String getBranchcode() {
    return branchcode;
  }

  public void setBranchcode(String branchcode) {
    this.branchcode = branchcode;
  }


  public String getAge() {
    return age;
  }

  public void setAge(String age) {
    this.age = age;
  }


  public String getChannelname() {
    return channelname;
  }

  public void setChannelname(String channelname) {
    this.channelname = channelname;
  }


  public String getReceiptno() {
    return receiptno;
  }

  public void setReceiptno(String receiptno) {
    this.receiptno = receiptno;
  }


  public String getIdnotype() {
    return idnotype;
  }

  public void setIdnotype(String idnotype) {
    this.idnotype = idnotype;
  }


  public String getBranchtype2() {
    return branchtype2;
  }

  public void setBranchtype2(String branchtype2) {
    this.branchtype2 = branchtype2;
  }


  public String getTrainpassflag() {
    return trainpassflag;
  }

  public void setTrainpassflag(String trainpassflag) {
    this.trainpassflag = trainpassflag;
  }


  public String getEmergentlink() {
    return emergentlink;
  }

  public void setEmergentlink(String emergentlink) {
    this.emergentlink = emergentlink;
  }


  public String getEmergentphone() {
    return emergentphone;
  }

  public void setEmergentphone(String emergentphone) {
    this.emergentphone = emergentphone;
  }


  public String getRetainstartdate() {
    return retainstartdate;
  }

  public void setRetainstartdate(String retainstartdate) {
    this.retainstartdate = retainstartdate;
  }


  public String getRetainenddate() {
    return retainenddate;
  }

  public void setRetainenddate(String retainenddate) {
    this.retainenddate = retainenddate;
  }


  public String getTogaeflag() {
    return togaeflag;
  }

  public void setTogaeflag(String togaeflag) {
    this.togaeflag = togaeflag;
  }


  public String getArchievecode() {
    return archievecode;
  }

  public void setArchievecode(String archievecode) {
    this.archievecode = archievecode;
  }


  public String getAffix() {
    return affix;
  }

  public void setAffix(String affix) {
    this.affix = affix;
  }


  public String getAffixname() {
    return affixname;
  }

  public void setAffixname(String affixname) {
    this.affixname = affixname;
  }


  public String getBankaddress() {
    return bankaddress;
  }

  public void setBankaddress(String bankaddress) {
    this.bankaddress = bankaddress;
  }


  public String getDxbranchtype2() {
    return dxbranchtype2;
  }

  public void setDxbranchtype2(String dxbranchtype2) {
    this.dxbranchtype2 = dxbranchtype2;
  }


  public String getDxsaleorg() {
    return dxsaleorg;
  }

  public void setDxsaleorg(String dxsaleorg) {
    this.dxsaleorg = dxsaleorg;
  }

}
