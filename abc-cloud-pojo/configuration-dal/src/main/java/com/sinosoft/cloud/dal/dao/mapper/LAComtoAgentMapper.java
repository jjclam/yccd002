package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LAComtoAgent;
import com.sinosoft.cloud.dal.dao.model.LAComtoAgentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LAComtoAgentMapper {
    int countByExample(LAComtoAgentExample example);

    int deleteByExample(LAComtoAgentExample example);

    int deleteByPrimaryKey(String AGENTCOM);

    int insert(LAComtoAgent record);

    int insertSelective(LAComtoAgent record);

    List<LAComtoAgent> selectByExample(LAComtoAgentExample example);

    LAComtoAgent selectByPrimaryKey(String AGENTCOM);

    int updateByExampleSelective(@Param("record") LAComtoAgent record, @Param("example") LAComtoAgentExample example);

    int updateByExample(@Param("record") LAComtoAgent record, @Param("example") LAComtoAgentExample example);

    int updateByPrimaryKeySelective(LAComtoAgent record);

    int updateByPrimaryKey(LAComtoAgent record);

    List<LAComtoAgent> selectByAgentCode(String opreator);

    LAComtoAgent selectByAgentCom(String agentCom);
}