package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LMRiskExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public LMRiskExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRISKCODEIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRISKCODEIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRISKCODEEqualTo(String value) {
            addCriterion("RISKCODE =", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODENotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODEGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODEGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODELessThan(String value) {
            addCriterion("RISKCODE <", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODELessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODELike(String value) {
            addCriterion("RISKCODE like", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODENotLike(String value) {
            addCriterion("RISKCODE not like", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODEIn(List<String> values) {
            addCriterion("RISKCODE in", values, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODENotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODEBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODENotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKVERIsNull() {
            addCriterion("RISKVER is null");
            return (Criteria) this;
        }

        public Criteria andRISKVERIsNotNull() {
            addCriterion("RISKVER is not null");
            return (Criteria) this;
        }

        public Criteria andRISKVEREqualTo(String value) {
            addCriterion("RISKVER =", value, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERNotEqualTo(String value) {
            addCriterion("RISKVER <>", value, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERGreaterThan(String value) {
            addCriterion("RISKVER >", value, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERGreaterThanOrEqualTo(String value) {
            addCriterion("RISKVER >=", value, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERLessThan(String value) {
            addCriterion("RISKVER <", value, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERLessThanOrEqualTo(String value) {
            addCriterion("RISKVER <=", value, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERLike(String value) {
            addCriterion("RISKVER like", value, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERNotLike(String value) {
            addCriterion("RISKVER not like", value, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERIn(List<String> values) {
            addCriterion("RISKVER in", values, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERNotIn(List<String> values) {
            addCriterion("RISKVER not in", values, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERBetween(String value1, String value2) {
            addCriterion("RISKVER between", value1, value2, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKVERNotBetween(String value1, String value2) {
            addCriterion("RISKVER not between", value1, value2, "RISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKNAMEIsNull() {
            addCriterion("RISKNAME is null");
            return (Criteria) this;
        }

        public Criteria andRISKNAMEIsNotNull() {
            addCriterion("RISKNAME is not null");
            return (Criteria) this;
        }

        public Criteria andRISKNAMEEqualTo(String value) {
            addCriterion("RISKNAME =", value, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMENotEqualTo(String value) {
            addCriterion("RISKNAME <>", value, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMEGreaterThan(String value) {
            addCriterion("RISKNAME >", value, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("RISKNAME >=", value, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMELessThan(String value) {
            addCriterion("RISKNAME <", value, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMELessThanOrEqualTo(String value) {
            addCriterion("RISKNAME <=", value, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMELike(String value) {
            addCriterion("RISKNAME like", value, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMENotLike(String value) {
            addCriterion("RISKNAME not like", value, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMEIn(List<String> values) {
            addCriterion("RISKNAME in", values, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMENotIn(List<String> values) {
            addCriterion("RISKNAME not in", values, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMEBetween(String value1, String value2) {
            addCriterion("RISKNAME between", value1, value2, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKNAMENotBetween(String value1, String value2) {
            addCriterion("RISKNAME not between", value1, value2, "RISKNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMEIsNull() {
            addCriterion("RISKSHORTNAME is null");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMEIsNotNull() {
            addCriterion("RISKSHORTNAME is not null");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMEEqualTo(String value) {
            addCriterion("RISKSHORTNAME =", value, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMENotEqualTo(String value) {
            addCriterion("RISKSHORTNAME <>", value, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMEGreaterThan(String value) {
            addCriterion("RISKSHORTNAME >", value, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("RISKSHORTNAME >=", value, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMELessThan(String value) {
            addCriterion("RISKSHORTNAME <", value, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMELessThanOrEqualTo(String value) {
            addCriterion("RISKSHORTNAME <=", value, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMELike(String value) {
            addCriterion("RISKSHORTNAME like", value, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMENotLike(String value) {
            addCriterion("RISKSHORTNAME not like", value, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMEIn(List<String> values) {
            addCriterion("RISKSHORTNAME in", values, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMENotIn(List<String> values) {
            addCriterion("RISKSHORTNAME not in", values, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMEBetween(String value1, String value2) {
            addCriterion("RISKSHORTNAME between", value1, value2, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSHORTNAMENotBetween(String value1, String value2) {
            addCriterion("RISKSHORTNAME not between", value1, value2, "RISKSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMEIsNull() {
            addCriterion("RISKENNAME is null");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMEIsNotNull() {
            addCriterion("RISKENNAME is not null");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMEEqualTo(String value) {
            addCriterion("RISKENNAME =", value, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMENotEqualTo(String value) {
            addCriterion("RISKENNAME <>", value, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMEGreaterThan(String value) {
            addCriterion("RISKENNAME >", value, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("RISKENNAME >=", value, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMELessThan(String value) {
            addCriterion("RISKENNAME <", value, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMELessThanOrEqualTo(String value) {
            addCriterion("RISKENNAME <=", value, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMELike(String value) {
            addCriterion("RISKENNAME like", value, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMENotLike(String value) {
            addCriterion("RISKENNAME not like", value, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMEIn(List<String> values) {
            addCriterion("RISKENNAME in", values, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMENotIn(List<String> values) {
            addCriterion("RISKENNAME not in", values, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMEBetween(String value1, String value2) {
            addCriterion("RISKENNAME between", value1, value2, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENNAMENotBetween(String value1, String value2) {
            addCriterion("RISKENNAME not between", value1, value2, "RISKENNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMEIsNull() {
            addCriterion("RISKENSHORTNAME is null");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMEIsNotNull() {
            addCriterion("RISKENSHORTNAME is not null");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMEEqualTo(String value) {
            addCriterion("RISKENSHORTNAME =", value, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMENotEqualTo(String value) {
            addCriterion("RISKENSHORTNAME <>", value, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMEGreaterThan(String value) {
            addCriterion("RISKENSHORTNAME >", value, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("RISKENSHORTNAME >=", value, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMELessThan(String value) {
            addCriterion("RISKENSHORTNAME <", value, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMELessThanOrEqualTo(String value) {
            addCriterion("RISKENSHORTNAME <=", value, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMELike(String value) {
            addCriterion("RISKENSHORTNAME like", value, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMENotLike(String value) {
            addCriterion("RISKENSHORTNAME not like", value, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMEIn(List<String> values) {
            addCriterion("RISKENSHORTNAME in", values, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMENotIn(List<String> values) {
            addCriterion("RISKENSHORTNAME not in", values, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMEBetween(String value1, String value2) {
            addCriterion("RISKENSHORTNAME between", value1, value2, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andRISKENSHORTNAMENotBetween(String value1, String value2) {
            addCriterion("RISKENSHORTNAME not between", value1, value2, "RISKENSHORTNAME");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGIsNull() {
            addCriterion("CHODUTYFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGIsNotNull() {
            addCriterion("CHODUTYFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGEqualTo(String value) {
            addCriterion("CHODUTYFLAG =", value, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGNotEqualTo(String value) {
            addCriterion("CHODUTYFLAG <>", value, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGGreaterThan(String value) {
            addCriterion("CHODUTYFLAG >", value, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("CHODUTYFLAG >=", value, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGLessThan(String value) {
            addCriterion("CHODUTYFLAG <", value, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGLessThanOrEqualTo(String value) {
            addCriterion("CHODUTYFLAG <=", value, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGLike(String value) {
            addCriterion("CHODUTYFLAG like", value, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGNotLike(String value) {
            addCriterion("CHODUTYFLAG not like", value, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGIn(List<String> values) {
            addCriterion("CHODUTYFLAG in", values, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGNotIn(List<String> values) {
            addCriterion("CHODUTYFLAG not in", values, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGBetween(String value1, String value2) {
            addCriterion("CHODUTYFLAG between", value1, value2, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCHODUTYFLAGNotBetween(String value1, String value2) {
            addCriterion("CHODUTYFLAG not between", value1, value2, "CHODUTYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGIsNull() {
            addCriterion("CPAYFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGIsNotNull() {
            addCriterion("CPAYFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGEqualTo(String value) {
            addCriterion("CPAYFLAG =", value, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGNotEqualTo(String value) {
            addCriterion("CPAYFLAG <>", value, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGGreaterThan(String value) {
            addCriterion("CPAYFLAG >", value, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("CPAYFLAG >=", value, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGLessThan(String value) {
            addCriterion("CPAYFLAG <", value, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGLessThanOrEqualTo(String value) {
            addCriterion("CPAYFLAG <=", value, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGLike(String value) {
            addCriterion("CPAYFLAG like", value, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGNotLike(String value) {
            addCriterion("CPAYFLAG not like", value, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGIn(List<String> values) {
            addCriterion("CPAYFLAG in", values, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGNotIn(List<String> values) {
            addCriterion("CPAYFLAG not in", values, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGBetween(String value1, String value2) {
            addCriterion("CPAYFLAG between", value1, value2, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andCPAYFLAGNotBetween(String value1, String value2) {
            addCriterion("CPAYFLAG not between", value1, value2, "CPAYFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGIsNull() {
            addCriterion("GETFLAG is null");
            return (Criteria) this;
        }

        public Criteria andGETFLAGIsNotNull() {
            addCriterion("GETFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andGETFLAGEqualTo(String value) {
            addCriterion("GETFLAG =", value, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGNotEqualTo(String value) {
            addCriterion("GETFLAG <>", value, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGGreaterThan(String value) {
            addCriterion("GETFLAG >", value, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("GETFLAG >=", value, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGLessThan(String value) {
            addCriterion("GETFLAG <", value, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGLessThanOrEqualTo(String value) {
            addCriterion("GETFLAG <=", value, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGLike(String value) {
            addCriterion("GETFLAG like", value, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGNotLike(String value) {
            addCriterion("GETFLAG not like", value, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGIn(List<String> values) {
            addCriterion("GETFLAG in", values, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGNotIn(List<String> values) {
            addCriterion("GETFLAG not in", values, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGBetween(String value1, String value2) {
            addCriterion("GETFLAG between", value1, value2, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andGETFLAGNotBetween(String value1, String value2) {
            addCriterion("GETFLAG not between", value1, value2, "GETFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGIsNull() {
            addCriterion("EDORFLAG is null");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGIsNotNull() {
            addCriterion("EDORFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGEqualTo(String value) {
            addCriterion("EDORFLAG =", value, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGNotEqualTo(String value) {
            addCriterion("EDORFLAG <>", value, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGGreaterThan(String value) {
            addCriterion("EDORFLAG >", value, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("EDORFLAG >=", value, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGLessThan(String value) {
            addCriterion("EDORFLAG <", value, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGLessThanOrEqualTo(String value) {
            addCriterion("EDORFLAG <=", value, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGLike(String value) {
            addCriterion("EDORFLAG like", value, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGNotLike(String value) {
            addCriterion("EDORFLAG not like", value, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGIn(List<String> values) {
            addCriterion("EDORFLAG in", values, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGNotIn(List<String> values) {
            addCriterion("EDORFLAG not in", values, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGBetween(String value1, String value2) {
            addCriterion("EDORFLAG between", value1, value2, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andEDORFLAGNotBetween(String value1, String value2) {
            addCriterion("EDORFLAG not between", value1, value2, "EDORFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGIsNull() {
            addCriterion("RNEWFLAG is null");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGIsNotNull() {
            addCriterion("RNEWFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGEqualTo(String value) {
            addCriterion("RNEWFLAG =", value, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGNotEqualTo(String value) {
            addCriterion("RNEWFLAG <>", value, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGGreaterThan(String value) {
            addCriterion("RNEWFLAG >", value, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("RNEWFLAG >=", value, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGLessThan(String value) {
            addCriterion("RNEWFLAG <", value, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGLessThanOrEqualTo(String value) {
            addCriterion("RNEWFLAG <=", value, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGLike(String value) {
            addCriterion("RNEWFLAG like", value, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGNotLike(String value) {
            addCriterion("RNEWFLAG not like", value, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGIn(List<String> values) {
            addCriterion("RNEWFLAG in", values, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGNotIn(List<String> values) {
            addCriterion("RNEWFLAG not in", values, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGBetween(String value1, String value2) {
            addCriterion("RNEWFLAG between", value1, value2, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andRNEWFLAGNotBetween(String value1, String value2) {
            addCriterion("RNEWFLAG not between", value1, value2, "RNEWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGIsNull() {
            addCriterion("UWFLAG is null");
            return (Criteria) this;
        }

        public Criteria andUWFLAGIsNotNull() {
            addCriterion("UWFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andUWFLAGEqualTo(String value) {
            addCriterion("UWFLAG =", value, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGNotEqualTo(String value) {
            addCriterion("UWFLAG <>", value, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGGreaterThan(String value) {
            addCriterion("UWFLAG >", value, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("UWFLAG >=", value, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGLessThan(String value) {
            addCriterion("UWFLAG <", value, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGLessThanOrEqualTo(String value) {
            addCriterion("UWFLAG <=", value, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGLike(String value) {
            addCriterion("UWFLAG like", value, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGNotLike(String value) {
            addCriterion("UWFLAG not like", value, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGIn(List<String> values) {
            addCriterion("UWFLAG in", values, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGNotIn(List<String> values) {
            addCriterion("UWFLAG not in", values, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGBetween(String value1, String value2) {
            addCriterion("UWFLAG between", value1, value2, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andUWFLAGNotBetween(String value1, String value2) {
            addCriterion("UWFLAG not between", value1, value2, "UWFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGIsNull() {
            addCriterion("RINSFLAG is null");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGIsNotNull() {
            addCriterion("RINSFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGEqualTo(String value) {
            addCriterion("RINSFLAG =", value, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGNotEqualTo(String value) {
            addCriterion("RINSFLAG <>", value, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGGreaterThan(String value) {
            addCriterion("RINSFLAG >", value, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("RINSFLAG >=", value, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGLessThan(String value) {
            addCriterion("RINSFLAG <", value, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGLessThanOrEqualTo(String value) {
            addCriterion("RINSFLAG <=", value, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGLike(String value) {
            addCriterion("RINSFLAG like", value, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGNotLike(String value) {
            addCriterion("RINSFLAG not like", value, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGIn(List<String> values) {
            addCriterion("RINSFLAG in", values, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGNotIn(List<String> values) {
            addCriterion("RINSFLAG not in", values, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGBetween(String value1, String value2) {
            addCriterion("RINSFLAG between", value1, value2, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andRINSFLAGNotBetween(String value1, String value2) {
            addCriterion("RINSFLAG not between", value1, value2, "RINSFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGIsNull() {
            addCriterion("INSUACCFLAG is null");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGIsNotNull() {
            addCriterion("INSUACCFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGEqualTo(String value) {
            addCriterion("INSUACCFLAG =", value, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGNotEqualTo(String value) {
            addCriterion("INSUACCFLAG <>", value, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGGreaterThan(String value) {
            addCriterion("INSUACCFLAG >", value, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("INSUACCFLAG >=", value, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGLessThan(String value) {
            addCriterion("INSUACCFLAG <", value, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGLessThanOrEqualTo(String value) {
            addCriterion("INSUACCFLAG <=", value, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGLike(String value) {
            addCriterion("INSUACCFLAG like", value, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGNotLike(String value) {
            addCriterion("INSUACCFLAG not like", value, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGIn(List<String> values) {
            addCriterion("INSUACCFLAG in", values, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGNotIn(List<String> values) {
            addCriterion("INSUACCFLAG not in", values, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGBetween(String value1, String value2) {
            addCriterion("INSUACCFLAG between", value1, value2, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andINSUACCFLAGNotBetween(String value1, String value2) {
            addCriterion("INSUACCFLAG not between", value1, value2, "INSUACCFLAG");
            return (Criteria) this;
        }

        public Criteria andDESTRATEIsNull() {
            addCriterion("DESTRATE is null");
            return (Criteria) this;
        }

        public Criteria andDESTRATEIsNotNull() {
            addCriterion("DESTRATE is not null");
            return (Criteria) this;
        }

        public Criteria andDESTRATEEqualTo(Float value) {
            addCriterion("DESTRATE =", value, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATENotEqualTo(Float value) {
            addCriterion("DESTRATE <>", value, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATEGreaterThan(Float value) {
            addCriterion("DESTRATE >", value, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATEGreaterThanOrEqualTo(Float value) {
            addCriterion("DESTRATE >=", value, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATELessThan(Float value) {
            addCriterion("DESTRATE <", value, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATELessThanOrEqualTo(Float value) {
            addCriterion("DESTRATE <=", value, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATEIn(List<Float> values) {
            addCriterion("DESTRATE in", values, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATENotIn(List<Float> values) {
            addCriterion("DESTRATE not in", values, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATEBetween(Float value1, Float value2) {
            addCriterion("DESTRATE between", value1, value2, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andDESTRATENotBetween(Float value1, Float value2) {
            addCriterion("DESTRATE not between", value1, value2, "DESTRATE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODEIsNull() {
            addCriterion("ORIGRISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODEIsNotNull() {
            addCriterion("ORIGRISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODEEqualTo(String value) {
            addCriterion("ORIGRISKCODE =", value, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODENotEqualTo(String value) {
            addCriterion("ORIGRISKCODE <>", value, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODEGreaterThan(String value) {
            addCriterion("ORIGRISKCODE >", value, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODEGreaterThanOrEqualTo(String value) {
            addCriterion("ORIGRISKCODE >=", value, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODELessThan(String value) {
            addCriterion("ORIGRISKCODE <", value, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODELessThanOrEqualTo(String value) {
            addCriterion("ORIGRISKCODE <=", value, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODELike(String value) {
            addCriterion("ORIGRISKCODE like", value, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODENotLike(String value) {
            addCriterion("ORIGRISKCODE not like", value, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODEIn(List<String> values) {
            addCriterion("ORIGRISKCODE in", values, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODENotIn(List<String> values) {
            addCriterion("ORIGRISKCODE not in", values, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODEBetween(String value1, String value2) {
            addCriterion("ORIGRISKCODE between", value1, value2, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andORIGRISKCODENotBetween(String value1, String value2) {
            addCriterion("ORIGRISKCODE not between", value1, value2, "ORIGRISKCODE");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERIsNull() {
            addCriterion("SUBRISKVER is null");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERIsNotNull() {
            addCriterion("SUBRISKVER is not null");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVEREqualTo(String value) {
            addCriterion("SUBRISKVER =", value, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERNotEqualTo(String value) {
            addCriterion("SUBRISKVER <>", value, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERGreaterThan(String value) {
            addCriterion("SUBRISKVER >", value, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERGreaterThanOrEqualTo(String value) {
            addCriterion("SUBRISKVER >=", value, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERLessThan(String value) {
            addCriterion("SUBRISKVER <", value, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERLessThanOrEqualTo(String value) {
            addCriterion("SUBRISKVER <=", value, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERLike(String value) {
            addCriterion("SUBRISKVER like", value, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERNotLike(String value) {
            addCriterion("SUBRISKVER not like", value, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERIn(List<String> values) {
            addCriterion("SUBRISKVER in", values, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERNotIn(List<String> values) {
            addCriterion("SUBRISKVER not in", values, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERBetween(String value1, String value2) {
            addCriterion("SUBRISKVER between", value1, value2, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andSUBRISKVERNotBetween(String value1, String value2) {
            addCriterion("SUBRISKVER not between", value1, value2, "SUBRISKVER");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMEIsNull() {
            addCriterion("RISKSTATNAME is null");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMEIsNotNull() {
            addCriterion("RISKSTATNAME is not null");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMEEqualTo(String value) {
            addCriterion("RISKSTATNAME =", value, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMENotEqualTo(String value) {
            addCriterion("RISKSTATNAME <>", value, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMEGreaterThan(String value) {
            addCriterion("RISKSTATNAME >", value, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("RISKSTATNAME >=", value, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMELessThan(String value) {
            addCriterion("RISKSTATNAME <", value, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMELessThanOrEqualTo(String value) {
            addCriterion("RISKSTATNAME <=", value, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMELike(String value) {
            addCriterion("RISKSTATNAME like", value, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMENotLike(String value) {
            addCriterion("RISKSTATNAME not like", value, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMEIn(List<String> values) {
            addCriterion("RISKSTATNAME in", values, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMENotIn(List<String> values) {
            addCriterion("RISKSTATNAME not in", values, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMEBetween(String value1, String value2) {
            addCriterion("RISKSTATNAME between", value1, value2, "RISKSTATNAME");
            return (Criteria) this;
        }

        public Criteria andRISKSTATNAMENotBetween(String value1, String value2) {
            addCriterion("RISKSTATNAME not between", value1, value2, "RISKSTATNAME");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}