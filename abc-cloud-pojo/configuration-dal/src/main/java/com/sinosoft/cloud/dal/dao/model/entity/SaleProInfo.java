package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;

public class SaleProInfo implements Serializable {
    /**
     * 序列
     */
    private String proId;

    /**
     * 产品编码
     */
    private String proCode;

    /**
     * 产品名称
     */
    private String proName;

    /**
     * 险种类型
     */
    private String riskType;

    /**
     * 最大投保年龄
     */
    private String maxAppntAge;

    /**
     * 最小投保年龄
     */
    private String minAppntAge;

    /**
     * 创建日期
     */
    private String createDate;

    /**
     * 修改日期
     */
    private String modifyDate;

    /**
     * 渠道编码
     */
    private String channelCode;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 险种类型编码
     */
    private String riskTypeCode;

    /**
     * 险种类型名称
     */
    private String riskTypeName;

    /**
     * 渠道产品可售起期
     */
    private String RiskStartSellDate;

    /**
     * 渠道产品止售起期
     */
    private String RiskEndSellDate;

    /**
     * H5页面链接
     */
    private String h5href;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public String getMaxAppntAge() {
        return maxAppntAge;
    }

    public void setMaxAppntAge(String maxAppntAge) {
        this.maxAppntAge = maxAppntAge;
    }

    public String getMinAppntAge() {
        return minAppntAge;
    }

    public void setMinAppntAge(String minAppntAge) {
        this.minAppntAge = minAppntAge;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getRiskTypeCode() {
        return riskTypeCode;
    }

    public void setRiskTypeCode(String riskTypeCode) {
        this.riskTypeCode = riskTypeCode;
    }

    public String getRiskTypeName() {
        return riskTypeName;
    }

    public void setRiskTypeName(String riskTypeName) {
        this.riskTypeName = riskTypeName;
    }

    public String getRiskStartSellDate() {
        return RiskStartSellDate;
    }

    public void setRiskStartSellDate(String riskStartSellDate) {
        RiskStartSellDate = riskStartSellDate;
    }

    public String getRiskEndSellDate() {
        return RiskEndSellDate;
    }

    public void setRiskEndSellDate(String riskEndSellDate) {
        RiskEndSellDate = riskEndSellDate;
    }

    public String getH5href() {
        return h5href;
    }

    public void setH5href(String h5href) {
        this.h5href = h5href;
    }

    @Override
    public String toString() {
        return "SaleProInfo{" +
                "proId='" + proId + '\'' +
                ", proCode='" + proCode + '\'' +
                ", proName='" + proName + '\'' +
                ", riskType='" + riskType + '\'' +
                ", maxAppntAge='" + maxAppntAge + '\'' +
                ", minAppntAge='" + minAppntAge + '\'' +
                ", createDate='" + createDate + '\'' +
                ", modifyDate='" + modifyDate + '\'' +
                ", channelCode='" + channelCode + '\'' +
                ", channelName='" + channelName + '\'' +
                ", riskTypeCode='" + riskTypeCode + '\'' +
                ", riskTypeName='" + riskTypeName + '\'' +
                ", RiskStartSellDate='" + RiskStartSellDate + '\'' +
                ", RiskEndSellDate='" + RiskEndSellDate + '\'' +
                ", h5href='" + h5href + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SaleProInfo that = (SaleProInfo) o;

        if (proId != null ? !proId.equals(that.proId) : that.proId != null) return false;
        if (proCode != null ? !proCode.equals(that.proCode) : that.proCode != null) return false;
        if (proName != null ? !proName.equals(that.proName) : that.proName != null) return false;
        if (riskType != null ? !riskType.equals(that.riskType) : that.riskType != null) return false;
        if (maxAppntAge != null ? !maxAppntAge.equals(that.maxAppntAge) : that.maxAppntAge != null) return false;
        if (minAppntAge != null ? !minAppntAge.equals(that.minAppntAge) : that.minAppntAge != null) return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null) return false;
        if (modifyDate != null ? !modifyDate.equals(that.modifyDate) : that.modifyDate != null) return false;
        if (channelCode != null ? !channelCode.equals(that.channelCode) : that.channelCode != null) return false;
        if (channelName != null ? !channelName.equals(that.channelName) : that.channelName != null) return false;
        if (riskTypeCode != null ? !riskTypeCode.equals(that.riskTypeCode) : that.riskTypeCode != null) return false;
        if (riskTypeName != null ? !riskTypeName.equals(that.riskTypeName) : that.riskTypeName != null) return false;
        if (RiskStartSellDate != null ? !RiskStartSellDate.equals(that.RiskStartSellDate) : that.RiskStartSellDate != null)
            return false;
        if (RiskEndSellDate != null ? !RiskEndSellDate.equals(that.RiskEndSellDate) : that.RiskEndSellDate != null)
            return false;
        return h5href != null ? h5href.equals(that.h5href) : that.h5href == null;
    }

    @Override
    public int hashCode() {
        int result = proId != null ? proId.hashCode() : 0;
        result = 31 * result + (proCode != null ? proCode.hashCode() : 0);
        result = 31 * result + (proName != null ? proName.hashCode() : 0);
        result = 31 * result + (riskType != null ? riskType.hashCode() : 0);
        result = 31 * result + (maxAppntAge != null ? maxAppntAge.hashCode() : 0);
        result = 31 * result + (minAppntAge != null ? minAppntAge.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);
        result = 31 * result + (channelCode != null ? channelCode.hashCode() : 0);
        result = 31 * result + (channelName != null ? channelName.hashCode() : 0);
        result = 31 * result + (riskTypeCode != null ? riskTypeCode.hashCode() : 0);
        result = 31 * result + (riskTypeName != null ? riskTypeName.hashCode() : 0);
        result = 31 * result + (RiskStartSellDate != null ? RiskStartSellDate.hashCode() : 0);
        result = 31 * result + (RiskEndSellDate != null ? RiskEndSellDate.hashCode() : 0);
        result = 31 * result + (h5href != null ? h5href.hashCode() : 0);
        return result;
    }
}
