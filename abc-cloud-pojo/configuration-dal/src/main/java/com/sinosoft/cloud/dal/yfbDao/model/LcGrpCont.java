package com.sinosoft.cloud.dal.yfbDao.model;


import java.text.SimpleDateFormat;

public class LcGrpCont {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体投保单号码 */
    private String ProposalGrpContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 销售渠道 */
    private String SaleChnl;
    /** 管理机构 */
    private String ManageCom;
    /** 保单口令 */
    private String Password;
    /** 密码 */
    private String Password2;
    /** 客户号码 */
    private String AppntNo;
    /** 投保总人数 */
    private int Peoples2;
    /** 付款方式 */
    private String GetFlag;
    /** 合同争议处理方式 */
    private String DisputedFlag;
    /** 集体特别约定 */
    private String GrpSpec;
    /** 交费方式 */
    private String PayMode;
    /** 签单机构 */
    private String SignCom;
    /** 签单日期 */
    private String  SignDate;
    /** 签单时间 */
    private String SignTime;
    /** 保单生效日期 */
    private String  CCValiDate;
    /** 交费间隔 */
    private int PayIntv;
    /** 总人数 */
    private int Peoples;
    /** 总份数 */
    private double Mult;
    /** 总保费 */
    private double Prem;
    /** 总保额 */
    private double Amnt;
    /** 总累计保费 */
    private double SumPrem;
    /** 总累计交费 */
    private double SumPay;
    /** 差额 */
    private double Dif;
    /** 备注 */
    private String Remark;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 复核状态 */
    private String ApproveFlag;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private String  ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 核保人 */
    private String UWOperator;
    /** 核保状态 */
    private String UWFlag;
    /** 核保完成日期 */
    private String  UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 投保单/保单标志 */
    private String AppFlag;
    /** 投保单申请日期 */
    private String  PolApplyDate;
    /** 保单回执客户签收日期 */
    private String  CustomGetPolDate;
    /** 保单送达日期 */
    private String  GetPolDate;
    /** 保单送达时间 */
    private String GetPolTime;
    /** 状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 初审人 */
    private String FirstTrialOperator;
    /** 初审日期 */
    private String  FirstTrialDate;
    /** 初审时间 */
    private String FirstTrialTime;
    /** 总单类型 */
    private String ContType;
    /** 年金领取日期 */
    private String  AnnuityReceiveDate;
    /** 保障终止日期 */
    private String  CCEndDate;
    /** 最初承保人数 */
    private int NativePeoples;
    /** 最初总保费 */
    private double NativePrem;
    /** 最初总保额 */
    private double NativeAmnt;
    /** 减保频率 */
    private String ReduceFre;
    /** 官网登录账号 */
    private String OffAcc;
    /** 官网登录密码 */
    private String OffPwd;
    /** 销售渠道编码 */
    private String SaleChannels;
    /** 电子投保单地址 */
    private String E_AppntID;
    /** 电子投保单生成日期 */
    private String  E_AppntDate;
    /** 电子合同地址 */
    private String E_ContID;
    /** 电子合同生成日期 */
    private String  E_ContDate;
    /** 是否为续保单 */
    private String IsRenewFlag;


    public LcGrpCont() {

    }


    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        GrpContNo = grpContNo;
    }

    public String getProposalGrpContNo() {
        return ProposalGrpContNo;
    }

    public void setProposalGrpContNo(String proposalGrpContNo) {
        ProposalGrpContNo = proposalGrpContNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String prtNo) {
        PrtNo = prtNo;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }

    public void setSaleChnl(String saleChnl) {
        SaleChnl = saleChnl;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String manageCom) {
        ManageCom = manageCom;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getPassword2() {
        return Password2;
    }

    public void setPassword2(String password2) {
        Password2 = password2;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String appntNo) {
        AppntNo = appntNo;
    }

    public int getPeoples2() {
        return Peoples2;
    }

    public void setPeoples2(int peoples2) {
        Peoples2 = peoples2;
    }

    public String getGetFlag() {
        return GetFlag;
    }

    public void setGetFlag(String getFlag) {
        GetFlag = getFlag;
    }

    public String getDisputedFlag() {
        return DisputedFlag;
    }

    public void setDisputedFlag(String disputedFlag) {
        DisputedFlag = disputedFlag;
    }

    public String getGrpSpec() {
        return GrpSpec;
    }

    public void setGrpSpec(String grpSpec) {
        GrpSpec = grpSpec;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String payMode) {
        PayMode = payMode;
    }

    public String getSignCom() {
        return SignCom;
    }

    public void setSignCom(String signCom) {
        SignCom = signCom;
    }

    public String getSignDate() {
        return SignDate;
    }

    public void setSignDate(String signDate) {
        SignDate = signDate;
    }

    public String getSignTime() {
        return SignTime;
    }

    public void setSignTime(String signTime) {
        SignTime = signTime;
    }

    public String getCCValiDate() {
        return CCValiDate;
    }

    public void setCCValiDate(String CCValiDate) {
        this.CCValiDate = CCValiDate;
    }

    public int getPayIntv() {
        return PayIntv;
    }

    public void setPayIntv(int payIntv) {
        PayIntv = payIntv;
    }

    public int getPeoples() {
        return Peoples;
    }

    public void setPeoples(int peoples) {
        Peoples = peoples;
    }

    public double getMult() {
        return Mult;
    }

    public void setMult(double mult) {
        Mult = mult;
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double prem) {
        Prem = prem;
    }

    public double getAmnt() {
        return Amnt;
    }

    public void setAmnt(double amnt) {
        Amnt = amnt;
    }

    public double getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(double sumPrem) {
        SumPrem = sumPrem;
    }

    public double getSumPay() {
        return SumPay;
    }

    public void setSumPay(double sumPay) {
        SumPay = sumPay;
    }

    public double getDif() {
        return Dif;
    }

    public void setDif(double dif) {
        Dif = dif;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }

    public void setStandbyFlag1(String standbyFlag1) {
        StandbyFlag1 = standbyFlag1;
    }

    public String getStandbyFlag2() {
        return StandbyFlag2;
    }

    public void setStandbyFlag2(String standbyFlag2) {
        StandbyFlag2 = standbyFlag2;
    }

    public String getStandbyFlag3() {
        return StandbyFlag3;
    }

    public void setStandbyFlag3(String standbyFlag3) {
        StandbyFlag3 = standbyFlag3;
    }

    public String getApproveFlag() {
        return ApproveFlag;
    }

    public void setApproveFlag(String approveFlag) {
        ApproveFlag = approveFlag;
    }

    public String getApproveCode() {
        return ApproveCode;
    }

    public void setApproveCode(String approveCode) {
        ApproveCode = approveCode;
    }

    public String getApproveDate() {
        return ApproveDate;
    }

    public void setApproveDate(String approveDate) {
        ApproveDate = approveDate;
    }

    public String getApproveTime() {
        return ApproveTime;
    }

    public void setApproveTime(String approveTime) {
        ApproveTime = approveTime;
    }

    public String getUWOperator() {
        return UWOperator;
    }

    public void setUWOperator(String UWOperator) {
        this.UWOperator = UWOperator;
    }

    public String getUWFlag() {
        return UWFlag;
    }

    public void setUWFlag(String UWFlag) {
        this.UWFlag = UWFlag;
    }

    public String getUWDate() {
        return UWDate;
    }

    public void setUWDate(String UWDate) {
        this.UWDate = UWDate;
    }

    public String getUWTime() {
        return UWTime;
    }

    public void setUWTime(String UWTime) {
        this.UWTime = UWTime;
    }

    public String getAppFlag() {
        return AppFlag;
    }

    public void setAppFlag(String appFlag) {
        AppFlag = appFlag;
    }

    public String getPolApplyDate() {
        return PolApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        PolApplyDate = polApplyDate;
    }

    public String getCustomGetPolDate() {
        return CustomGetPolDate;
    }

    public void setCustomGetPolDate(String customGetPolDate) {
        CustomGetPolDate = customGetPolDate;
    }

    public String getGetPolDate() {
        return GetPolDate;
    }

    public void setGetPolDate(String getPolDate) {
        GetPolDate = getPolDate;
    }

    public String getGetPolTime() {
        return GetPolTime;
    }

    public void setGetPolTime(String getPolTime) {
        GetPolTime = getPolTime;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getFirstTrialOperator() {
        return FirstTrialOperator;
    }

    public void setFirstTrialOperator(String firstTrialOperator) {
        FirstTrialOperator = firstTrialOperator;
    }

    public String getFirstTrialDate() {
        return FirstTrialDate;
    }

    public void setFirstTrialDate(String firstTrialDate) {
        FirstTrialDate = firstTrialDate;
    }

    public String getFirstTrialTime() {
        return FirstTrialTime;
    }

    public void setFirstTrialTime(String firstTrialTime) {
        FirstTrialTime = firstTrialTime;
    }

    public String getContType() {
        return ContType;
    }

    public void setContType(String contType) {
        ContType = contType;
    }

    public String getAnnuityReceiveDate() {
        return AnnuityReceiveDate;
    }

    public void setAnnuityReceiveDate(String annuityReceiveDate) {
        AnnuityReceiveDate = annuityReceiveDate;
    }

    public String getCCEndDate() {
        return CCEndDate;
    }

    public void setCCEndDate(String CCEndDate) {
        this.CCEndDate = CCEndDate;
    }

    public int getNativePeoples() {
        return NativePeoples;
    }

    public void setNativePeoples(int nativePeoples) {
        NativePeoples = nativePeoples;
    }

    public double getNativePrem() {
        return NativePrem;
    }

    public void setNativePrem(double nativePrem) {
        NativePrem = nativePrem;
    }

    public double getNativeAmnt() {
        return NativeAmnt;
    }

    public void setNativeAmnt(double nativeAmnt) {
        NativeAmnt = nativeAmnt;
    }

    public String getReduceFre() {
        return ReduceFre;
    }

    public void setReduceFre(String reduceFre) {
        ReduceFre = reduceFre;
    }

    public String getOffAcc() {
        return OffAcc;
    }

    public void setOffAcc(String offAcc) {
        OffAcc = offAcc;
    }

    public String getOffPwd() {
        return OffPwd;
    }

    public void setOffPwd(String offPwd) {
        OffPwd = offPwd;
    }

    public String getSaleChannels() {
        return SaleChannels;
    }

    public void setSaleChannels(String saleChannels) {
        SaleChannels = saleChannels;
    }

    public String getE_AppntID() {
        return E_AppntID;
    }

    public void setE_AppntID(String e_AppntID) {
        E_AppntID = e_AppntID;
    }

    public String getE_AppntDate() {
        return E_AppntDate;
    }

    public void setE_AppntDate(String e_AppntDate) {
        E_AppntDate = e_AppntDate;
    }

    public String getE_ContID() {
        return E_ContID;
    }

    public void setE_ContID(String e_ContID) {
        E_ContID = e_ContID;
    }

    public String getE_ContDate() {
        return E_ContDate;
    }

    public void setE_ContDate(String e_ContDate) {
        E_ContDate = e_ContDate;
    }

    public String getIsRenewFlag() {
        return IsRenewFlag;
    }

    public void setIsRenewFlag(String isRenewFlag) {
        IsRenewFlag = isRenewFlag;
    }
}