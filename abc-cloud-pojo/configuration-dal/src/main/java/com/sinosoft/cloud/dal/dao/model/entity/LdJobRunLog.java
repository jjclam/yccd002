package com.sinosoft.cloud.dal.dao.model.entity;

import java.util.Objects;

public class LdJobRunLog {
    /**
     * 序列号
     */
    private String SerialNo;
    /**
     * job名称
     */
    private String TaskName;
    /**
     * job编码
     */
    private String TaskCode;
    /**
     * 执行开始日期
     */
    private String ExecuteDate;
    /**
     * 执行开始时间
     */
    private String ExecuteTime;
    /**
     *  执行结束日期
     */
    private String FinishDate;
    /**
     * 执行结束时间
     */
    private String FinishTime;
    /**
     * 执行次数
     */
    private String ExecuteCount;
    /**
     * 执行结果状态
     */
    private String ExecuteState;
    /**
     * 执行结果
     */
    private String ExecuteResult;
    /**
     * 备用字段1
     */
    private String Remark1;
    /**
     * 备用字段2
     */
    private String Remark2;

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getTaskName() {
        return TaskName;
    }

    public void setTaskName(String taskName) {
        TaskName = taskName;
    }

    public String getTaskCode() {
        return TaskCode;
    }

    public void setTaskCode(String taskCode) {
        TaskCode = taskCode;
    }

    public String getExecuteDate() {
        return ExecuteDate;
    }

    public void setExecuteDate(String executeDate) {
        ExecuteDate = executeDate;
    }

    public String getExecuteTime() {
        return ExecuteTime;
    }

    public void setExecuteTime(String executeTime) {
        ExecuteTime = executeTime;
    }

    public String getFinishDate() {
        return FinishDate;
    }

    public void setFinishDate(String finishDate) {
        FinishDate = finishDate;
    }

    public String getFinishTime() {
        return FinishTime;
    }

    public void setFinishTime(String finishTime) {
        FinishTime = finishTime;
    }

    public String getExecuteCount() {
        return ExecuteCount;
    }

    public void setExecuteCount(String executeCount) {
        ExecuteCount = executeCount;
    }

    public String getExecuteState() {
        return ExecuteState;
    }

    public void setExecuteState(String executeState) {
        ExecuteState = executeState;
    }

    public String getExecuteResult() {
        return ExecuteResult;
    }

    public void setExecuteResult(String executeResult) {
        ExecuteResult = executeResult;
    }

    public String getRemark1() {
        return Remark1;
    }

    public void setRemark1(String remark1) {
        Remark1 = remark1;
    }

    public String getRemark2() {
        return Remark2;
    }

    public void setRemark2(String remark2) {
        Remark2 = remark2;
    }

    @Override
    public String toString() {
        return "LdJobRunLog{" +
                "SerialNo='" + SerialNo + '\'' +
                ", TaskName='" + TaskName + '\'' +
                ", TaskCode='" + TaskCode + '\'' +
                ", ExecuteDate='" + ExecuteDate + '\'' +
                ", ExecuteTime='" + ExecuteTime + '\'' +
                ", FinishDate='" + FinishDate + '\'' +
                ", FinishTime='" + FinishTime + '\'' +
                ", ExecuteCount='" + ExecuteCount + '\'' +
                ", ExecuteState='" + ExecuteState + '\'' +
                ", ExecuteResult='" + ExecuteResult + '\'' +
                ", Remark1='" + Remark1 + '\'' +
                ", Remark2='" + Remark2 + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LdJobRunLog that = (LdJobRunLog) o;
        return Objects.equals(SerialNo, that.SerialNo) &&
                Objects.equals(TaskName, that.TaskName) &&
                Objects.equals(TaskCode, that.TaskCode) &&
                Objects.equals(ExecuteDate, that.ExecuteDate) &&
                Objects.equals(ExecuteTime, that.ExecuteTime) &&
                Objects.equals(FinishDate, that.FinishDate) &&
                Objects.equals(FinishTime, that.FinishTime) &&
                Objects.equals(ExecuteCount, that.ExecuteCount) &&
                Objects.equals(ExecuteState, that.ExecuteState) &&
                Objects.equals(ExecuteResult, that.ExecuteResult) &&
                Objects.equals(Remark1, that.Remark1) &&
                Objects.equals(Remark2, that.Remark2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(SerialNo, TaskName, TaskCode, ExecuteDate, ExecuteTime, FinishDate, FinishTime, ExecuteCount, ExecuteState, ExecuteResult, Remark1, Remark2);
    }
}
