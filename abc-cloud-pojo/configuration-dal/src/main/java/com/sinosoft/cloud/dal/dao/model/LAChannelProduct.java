package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;

public class LAChannelProduct implements Serializable {
    /**
     * 渠道编码
     *
     * @mbggenerated
     */
    private String channelCode;

    /**
     * 渠道名称
     *
     * @mbggenerated
     */
    private String channelName;

    /**
     * 产品编码
     *
     * @mbggenerated
     */
    private String riskCode;

    /**
     * 产品名称
     *
     * @mbggenerated
     */
    private String riskName;

    /**
     * 产品类型 L--人寿保险R--年金保险H--健康险A--意外伤害保险S--重疾保险U--万能保险
     *
     * @mbggenerated
     */
    private String riskType;

    /**
     * 产品起售日期
     *
     * @mbggenerated
     */
    private Date riskStartSellDate;

    /**
     * 产品止售日期
     *
     * @mbggenerated
     */
    private Date riskEndSellDate;

    /**
     * 数据创建日期
     *
     * @mbggenerated
     */
    private Date createDate;

    /**
     * 数据生成日期
     *
     * @mbggenerated
     */
    private Date makeDate;

    /**
     * 最后修改日期
     *
     * @mbggenerated
     */
    private Date modifyDate;

    /**
     * H5页面链接
     *
     * @mbggenerated
     */
    private String h5href;

    /**
     * 渠道产品上下架状态 0--上架;1--下架
     *
     * @mbggenerated
     */
    private String status;

    /**
     * 续保权限 0-无效、1-有效
     *
     * @mbggenerated
     */
    private String rnewageFlag;

    /**
     * 配置状态 0-无效、1-有效
     *
     * @mbggenerated
     */
    private String isConfig;

    /**
     * 上下架方式 00_手动立即上下架，01_自动定时上下架
     *
     * @mbggenerated
     */
    private String upDownType;

    /**
     * 渠道产品说明
     *
     * @mbggenerated
     */
    private String remark;

    /**
     * 渠道产品上架时间
     *
     * @mbggenerated
     */
    private String channelProductUpDate;

    /**
     * 渠道产品下架时间
     *
     * @c
     */
    private String channelProductDownDate;

    private static final long serialVersionUID = 1L;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public Date getRiskStartSellDate() {
        return riskStartSellDate;
    }

    public void setRiskStartSellDate(Date riskStartSellDate) {
        this.riskStartSellDate = riskStartSellDate;
    }

    public Date getRiskEndSellDate() {
        return riskEndSellDate;
    }

    public void setRiskEndSellDate(Date riskEndSellDate) {
        this.riskEndSellDate = riskEndSellDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(Date makeDate) {
        this.makeDate = makeDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getH5href() {
        return h5href;
    }

    public void setH5href(String h5href) {
        this.h5href = h5href;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRnewageFlag() {
        return rnewageFlag;
    }

    public void setRnewageFlag(String rnewageFlag) {
        this.rnewageFlag = rnewageFlag;
    }

    public String getIsConfig() {
        return isConfig;
    }

    public void setIsConfig(String isConfig) {
        this.isConfig = isConfig;
    }

    public String getUpDownType() {
        return upDownType;
    }

    public void setUpDownType(String upDownType) {
        this.upDownType = upDownType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getChannelProductUpDate() {
        return channelProductUpDate;
    }

    public void setChannelProductUpDate(String channelProductUpDate) {
        this.channelProductUpDate = channelProductUpDate;
    }

    public String getChannelProductDownDate() {
        return channelProductDownDate;
    }

    public void setChannelProductDownDate(String channelProductDownDate) {
        this.channelProductDownDate = channelProductDownDate;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "LAChannelProduct{" +
                "channelCode='" + channelCode + '\'' +
                ", channelName='" + channelName + '\'' +
                ", riskCode='" + riskCode + '\'' +
                ", riskName='" + riskName + '\'' +
                ", riskType='" + riskType + '\'' +
                ", riskStartSellDate=" + riskStartSellDate +
                ", riskEndSellDate=" + riskEndSellDate +
                ", createDate=" + createDate +
                ", makeDate=" + makeDate +
                ", modifyDate=" + modifyDate +
                ", h5href='" + h5href + '\'' +
                ", status='" + status + '\'' +
                ", rnewageFlag='" + rnewageFlag + '\'' +
                ", isConfig='" + isConfig + '\'' +
                ", upDownType='" + upDownType + '\'' +
                ", remark='" + remark + '\'' +
                ", channelProductUpDate='" + channelProductUpDate + '\'' +
                ", channelProductDownDate='" + channelProductDownDate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LAChannelProduct that = (LAChannelProduct) o;

        if (channelCode != null ? !channelCode.equals(that.channelCode) : that.channelCode != null) return false;
        if (channelName != null ? !channelName.equals(that.channelName) : that.channelName != null) return false;
        if (riskCode != null ? !riskCode.equals(that.riskCode) : that.riskCode != null) return false;
        if (riskName != null ? !riskName.equals(that.riskName) : that.riskName != null) return false;
        if (riskType != null ? !riskType.equals(that.riskType) : that.riskType != null) return false;
        if (riskStartSellDate != null ? !riskStartSellDate.equals(that.riskStartSellDate) : that.riskStartSellDate != null)
            return false;
        if (riskEndSellDate != null ? !riskEndSellDate.equals(that.riskEndSellDate) : that.riskEndSellDate != null)
            return false;
        if (createDate != null ? !createDate.equals(that.createDate) : that.createDate != null) return false;
        if (makeDate != null ? !makeDate.equals(that.makeDate) : that.makeDate != null) return false;
        if (modifyDate != null ? !modifyDate.equals(that.modifyDate) : that.modifyDate != null) return false;
        if (h5href != null ? !h5href.equals(that.h5href) : that.h5href != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (rnewageFlag != null ? !rnewageFlag.equals(that.rnewageFlag) : that.rnewageFlag != null) return false;
        if (isConfig != null ? !isConfig.equals(that.isConfig) : that.isConfig != null) return false;
        if (upDownType != null ? !upDownType.equals(that.upDownType) : that.upDownType != null) return false;
        if (remark != null ? !remark.equals(that.remark) : that.remark != null) return false;
        if (channelProductUpDate != null ? !channelProductUpDate.equals(that.channelProductUpDate) : that.channelProductUpDate != null)
            return false;
        return channelProductDownDate != null ? channelProductDownDate.equals(that.channelProductDownDate) : that.channelProductDownDate == null;
    }

    @Override
    public int hashCode() {
        int result = channelCode != null ? channelCode.hashCode() : 0;
        result = 31 * result + (channelName != null ? channelName.hashCode() : 0);
        result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
        result = 31 * result + (riskName != null ? riskName.hashCode() : 0);
        result = 31 * result + (riskType != null ? riskType.hashCode() : 0);
        result = 31 * result + (riskStartSellDate != null ? riskStartSellDate.hashCode() : 0);
        result = 31 * result + (riskEndSellDate != null ? riskEndSellDate.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (makeDate != null ? makeDate.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);
        result = 31 * result + (h5href != null ? h5href.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (rnewageFlag != null ? rnewageFlag.hashCode() : 0);
        result = 31 * result + (isConfig != null ? isConfig.hashCode() : 0);
        result = 31 * result + (upDownType != null ? upDownType.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        result = 31 * result + (channelProductUpDate != null ? channelProductUpDate.hashCode() : 0);
        result = 31 * result + (channelProductDownDate != null ? channelProductDownDate.hashCode() : 0);
        return result;
    }
}