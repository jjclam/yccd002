package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;

public class ChlInfo implements Serializable {
    /**
     * 序列
     */
    private String proId;

    /**
     * 中介机构代码
     */
    private String agencyCode;

    /**
     * 中介机构名称
     */
    private String agencyName;

    /**
     * 代理人编号
     */
    private String agentCode;

    /**
     * 中介机构类别
     */
    private String agencyType;

    /**
     * 保险许可证号码
     */
    private String licenseNo;

    /**
     * 渠道编码
     */
    private String channelCode;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 产品编码
     */
    private String proCode;

    /**
     * 产品名称
     */
    private String proName;

    /**
     * 创建时间
     */
    private String createDate;

    /**
     * 创建时间起期
     */
    private String createStartDate;

    /**
     * 创建时间止期
     */
    private String createEndDate;

    /**
     * 渠道产品上下架状态
     */
    private String status;

    /**
     * 渠道产品可售起期
     */
    private String RiskStartSellDate;

    /**
     * 渠道产品止售起期
     */
    private String RiskEndSellDate;

    /**
     * 渠道产品上架时间
     */
    private String ChannelProductUpDate;

    /**
     * 渠道产品下架时间
     */
    private String ChannelProductDownDate;

    /**
     * 渠道产品上下架时间
     */
    private String upDownDate;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgencyType() {
        return agencyType;
    }

    public void setAgencyType(String agencyType) {
        this.agencyType = agencyType;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateStartDate() {
        return createStartDate;
    }

    public void setCreateStartDate(String createStartDate) {
        this.createStartDate = createStartDate;
    }

    public String getCreateEndDate() {
        return createEndDate;
    }

    public void setCreateEndDate(String createEndDate) {
        this.createEndDate = createEndDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRiskStartSellDate() {
        return RiskStartSellDate;
    }

    public void setRiskStartSellDate(String riskStartSellDate) {
        RiskStartSellDate = riskStartSellDate;
    }

    public String getRiskEndSellDate() {
        return RiskEndSellDate;
    }

    public void setRiskEndSellDate(String riskEndSellDate) {
        RiskEndSellDate = riskEndSellDate;
    }

    public String getChannelProductUpDate() {
        return ChannelProductUpDate;
    }

    public void setChannelProductUpDate(String channelProductUpDate) {
        ChannelProductUpDate = channelProductUpDate;
    }

    public String getChannelProductDownDate() {
        return ChannelProductDownDate;
    }

    public void setChannelProductDownDate(String channelProductDownDate) {
        ChannelProductDownDate = channelProductDownDate;
    }

    public String getUpDownDate() {
        return upDownDate;
    }

    public void setUpDownDate(String upDownDate) {
        this.upDownDate = upDownDate;
    }

    @Override
    public String toString() {
        return "ChlInfo{" +
                "proId='" + proId + '\'' +
                ", agencyCode='" + agencyCode + '\'' +
                ", agencyName='" + agencyName + '\'' +
                ", agentCode='" + agentCode + '\'' +
                ", agencyType='" + agencyType + '\'' +
                ", licenseNo='" + licenseNo + '\'' +
                ", channelCode='" + channelCode + '\'' +
                ", channelName='" + channelName + '\'' +
                ", proCode='" + proCode + '\'' +
                ", proName='" + proName + '\'' +
                ", createDate='" + createDate + '\'' +
                ", createStartDate='" + createStartDate + '\'' +
                ", createEndDate='" + createEndDate + '\'' +
                ", status='" + status + '\'' +
                ", RiskStartSellDate='" + RiskStartSellDate + '\'' +
                ", RiskEndSellDate='" + RiskEndSellDate + '\'' +
                ", ChannelProductUpDate='" + ChannelProductUpDate + '\'' +
                ", ChannelProductDownDate='" + ChannelProductDownDate + '\'' +
                ", upDownDate='" + upDownDate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChlInfo chlInfo = (ChlInfo) o;

        if (proId != null ? !proId.equals(chlInfo.proId) : chlInfo.proId != null) return false;
        if (agencyCode != null ? !agencyCode.equals(chlInfo.agencyCode) : chlInfo.agencyCode != null) return false;
        if (agencyName != null ? !agencyName.equals(chlInfo.agencyName) : chlInfo.agencyName != null) return false;
        if (agentCode != null ? !agentCode.equals(chlInfo.agentCode) : chlInfo.agentCode != null) return false;
        if (agencyType != null ? !agencyType.equals(chlInfo.agencyType) : chlInfo.agencyType != null) return false;
        if (licenseNo != null ? !licenseNo.equals(chlInfo.licenseNo) : chlInfo.licenseNo != null) return false;
        if (channelCode != null ? !channelCode.equals(chlInfo.channelCode) : chlInfo.channelCode != null) return false;
        if (channelName != null ? !channelName.equals(chlInfo.channelName) : chlInfo.channelName != null) return false;
        if (proCode != null ? !proCode.equals(chlInfo.proCode) : chlInfo.proCode != null) return false;
        if (proName != null ? !proName.equals(chlInfo.proName) : chlInfo.proName != null) return false;
        if (createDate != null ? !createDate.equals(chlInfo.createDate) : chlInfo.createDate != null) return false;
        if (createStartDate != null ? !createStartDate.equals(chlInfo.createStartDate) : chlInfo.createStartDate != null)
            return false;
        if (createEndDate != null ? !createEndDate.equals(chlInfo.createEndDate) : chlInfo.createEndDate != null)
            return false;
        if (status != null ? !status.equals(chlInfo.status) : chlInfo.status != null) return false;
        if (RiskStartSellDate != null ? !RiskStartSellDate.equals(chlInfo.RiskStartSellDate) : chlInfo.RiskStartSellDate != null)
            return false;
        if (RiskEndSellDate != null ? !RiskEndSellDate.equals(chlInfo.RiskEndSellDate) : chlInfo.RiskEndSellDate != null)
            return false;
        if (ChannelProductUpDate != null ? !ChannelProductUpDate.equals(chlInfo.ChannelProductUpDate) : chlInfo.ChannelProductUpDate != null)
            return false;
        if (ChannelProductDownDate != null ? !ChannelProductDownDate.equals(chlInfo.ChannelProductDownDate) : chlInfo.ChannelProductDownDate != null)
            return false;
        return upDownDate != null ? upDownDate.equals(chlInfo.upDownDate) : chlInfo.upDownDate == null;
    }

    @Override
    public int hashCode() {
        int result = proId != null ? proId.hashCode() : 0;
        result = 31 * result + (agencyCode != null ? agencyCode.hashCode() : 0);
        result = 31 * result + (agencyName != null ? agencyName.hashCode() : 0);
        result = 31 * result + (agentCode != null ? agentCode.hashCode() : 0);
        result = 31 * result + (agencyType != null ? agencyType.hashCode() : 0);
        result = 31 * result + (licenseNo != null ? licenseNo.hashCode() : 0);
        result = 31 * result + (channelCode != null ? channelCode.hashCode() : 0);
        result = 31 * result + (channelName != null ? channelName.hashCode() : 0);
        result = 31 * result + (proCode != null ? proCode.hashCode() : 0);
        result = 31 * result + (proName != null ? proName.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (createStartDate != null ? createStartDate.hashCode() : 0);
        result = 31 * result + (createEndDate != null ? createEndDate.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (RiskStartSellDate != null ? RiskStartSellDate.hashCode() : 0);
        result = 31 * result + (RiskEndSellDate != null ? RiskEndSellDate.hashCode() : 0);
        result = 31 * result + (ChannelProductUpDate != null ? ChannelProductUpDate.hashCode() : 0);
        result = 31 * result + (ChannelProductDownDate != null ? ChannelProductDownDate.hashCode() : 0);
        result = 31 * result + (upDownDate != null ? upDownDate.hashCode() : 0);
        return result;
    }
}
