package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.open_LaCom;
import com.sinosoft.cloud.dal.dao.model.open_LaComExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface open_LaComMapper {
    int countByExample(open_LaComExample example);

    int deleteByExample(open_LaComExample example);

    int deleteByPrimaryKey(String COMCODE);

    int insert(open_LaCom record);

    int insertSelective(open_LaCom record);

    List<open_LaCom> selectByExample(open_LaComExample example);

    open_LaCom selectByPrimaryKey(String COMCODE);

    int updateByExampleSelective(@Param("record") open_LaCom record, @Param("example") open_LaComExample example);

    int updateByExample(@Param("record") open_LaCom record, @Param("example") open_LaComExample example);

    int updateByPrimaryKeySelective(open_LaCom record);

    int updateByPrimaryKey(open_LaCom record);

    open_LaCom selectByaa();

    open_LaCom selectComInfoByAgentCode(@Param("agencyCode") String agencyCode);
}