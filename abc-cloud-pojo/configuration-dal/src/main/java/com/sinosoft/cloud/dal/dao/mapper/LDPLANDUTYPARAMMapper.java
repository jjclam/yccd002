package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LDPLANDUTYPARAM;
import com.sinosoft.cloud.dal.dao.model.LDPLANDUTYPARAMExample;

import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LDPLANDUTYPARAMMapper {
    int countByExample(LDPLANDUTYPARAMExample example);

    int deleteByExample(LDPLANDUTYPARAMExample example);

    int insert(LDPLANDUTYPARAM record);

    int insertSelective(LDPLANDUTYPARAM record);

    List<ProInfo> selectByExample(LDPLANDUTYPARAMExample example);

    int updateByExampleSelective(@Param("record") LDPLANDUTYPARAM record, @Param("example") LDPLANDUTYPARAMExample example);

    int updateByExample(@Param("record") LDPLANDUTYPARAM record, @Param("example") LDPLANDUTYPARAMExample example);

    List<LDPLANDUTYPARAM> selectByContPlan(LDPLANDUTYPARAM record);
}