package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;

public class SmsChlPro implements Serializable {
    private Integer id;

    private String channelCode;

    private String riskCode;

    private String smsInfo;

    private Date createDate;

    private Date updateDate;

    private String remark;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getSmsInfo() {
        return smsInfo;
    }

    public void setSmsInfo(String smsInfo) {
        this.smsInfo = smsInfo;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "SmsChlPro{" +
                "id=" + id +
                ", channelCode='" + channelCode + '\'' +
                ", riskCode='" + riskCode + '\'' +
                ", smsInfo='" + smsInfo + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", remark='" + remark + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmsChlPro smsChlPro = (SmsChlPro) o;

        if (id != null ? !id.equals(smsChlPro.id) : smsChlPro.id != null) return false;
        if (channelCode != null ? !channelCode.equals(smsChlPro.channelCode) : smsChlPro.channelCode != null)
            return false;
        if (riskCode != null ? !riskCode.equals(smsChlPro.riskCode) : smsChlPro.riskCode != null) return false;
        if (smsInfo != null ? !smsInfo.equals(smsChlPro.smsInfo) : smsChlPro.smsInfo != null) return false;
        if (createDate != null ? !createDate.equals(smsChlPro.createDate) : smsChlPro.createDate != null) return false;
        if (updateDate != null ? !updateDate.equals(smsChlPro.updateDate) : smsChlPro.updateDate != null) return false;
        return remark != null ? remark.equals(smsChlPro.remark) : smsChlPro.remark == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (channelCode != null ? channelCode.hashCode() : 0);
        result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
        result = 31 * result + (smsInfo != null ? smsInfo.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }
}