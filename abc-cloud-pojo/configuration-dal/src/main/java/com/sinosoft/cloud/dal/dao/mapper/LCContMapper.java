package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LCCont;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.dal.dao.model.entity.SettleInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LCContMapper {

    Integer countForPolSearchList(Map<String,Object> map);

    List<Operation> selectForPolSearchList(Map<String,Object> map);

    Integer countForPremCountList(Map<String,Object> map);

    List<Operation> selectForPremCountList(Map<String,Object> map);

    Operation getCancelPolicyByContNo(@Param(value = "contNo") String contNo);

    Integer updateAppFlag(Map<String,Object> map);

    Integer insertCont(Map map);

    List<SettleInfo> selectSettleExcel(SettleInfo settleInfo);

    Integer updateSettleStatus(Map<String, Object> map);

    Integer updateSynFlag(Map<String, Object> map);
}