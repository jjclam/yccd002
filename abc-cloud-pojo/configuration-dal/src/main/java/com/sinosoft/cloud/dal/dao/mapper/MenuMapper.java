package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuMapper {
    //菜单
    List<Menu> getParentMenu(@Param("parameter2")String parameter2);
    List<Menu> getById( @Param("parameter")String parameter ,@Param("parameter1") String  erecord);

      //功能
    List<Menu> selectMenu();
    List<Menu> getMenu();

    int SelectMenuCount(@Param("param1")String name);

    int SelectMenuSort(@Param("Sort")String Sort);

    int SelectMenuId();

    Menu SelectMenuEntity(@Param("menuId") String menuId);


    List<Menu> selectByNoParam(@Param("menuname")String menuname);



    int deleteByPrimaryKey(String id);

    int insert(Menu record);

    int insertSelective(Menu record);

    Menu selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Menu record);

    int updateByPrimaryKey(Menu record);

    List<Menu> getparentMenus(@Param(value = "field") String field);

    Menu getparentMenu(@Param(value = "menuId") String mchId);
}