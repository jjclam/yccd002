package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class SettleInfo implements Serializable {
    /**
     * 序列
     */
    private String proId;

    /**
     * 保单号
     */
    private String contNo;

    /**
     * 产品编码
     */
    private String productCode;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 投保日期
     */
    private String polApplyDate;

    /**
     * 保单生效日期
     */
    private String cValidate;

    /**
     * 保单终止日期
     */
    private String cEndDate;

    /**
     * 保单状态
     */
    private String appFlag;

    /**
     * 结算状态
     */
    private String settleStatus;

    /**
     * 总保费
     */
    private String sumPrem;

    /**
     * 生效起期
     */
    private String validateStartDate;

    /**
     * 生效止期
     */
    private String validateEndDate;

    /**
     * 投保起期
     */
    private String applyStartDate;

    /**
     * 投保止期
     */
    private String applyEndDate;

    /**
     * 导出标记 1-已结算 0-未结算
     */
    private String exportFlag;

    /**
     * 结算时间
     */
    private String settleDate;

    /**
     * 结算编号
     */
    private String settleNumber;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPolApplyDate() {
        return polApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        this.polApplyDate = polApplyDate;
    }

    public String getcValidate() {
        return cValidate;
    }

    public void setcValidate(String cValidate) {
        this.cValidate = cValidate;
    }

    public String getcEndDate() {
        return cEndDate;
    }

    public void setcEndDate(String cEndDate) {
        this.cEndDate = cEndDate;
    }

    public String getAppFlag() {
        return appFlag;
    }

    public void setAppFlag(String appFlag) {
        this.appFlag = appFlag;
    }

    public String getSettleStatus() {
        return settleStatus;
    }

    public void setSettleStatus(String settleStatus) {
        this.settleStatus = settleStatus;
    }

    public String getSumPrem() {
        return sumPrem;
    }

    public void setSumPrem(String sumPrem) {
        this.sumPrem = sumPrem;
    }

    public String getValidateStartDate() {
        return validateStartDate;
    }

    public void setValidateStartDate(String validateStartDate) {
        this.validateStartDate = validateStartDate;
    }

    public String getValidateEndDate() {
        return validateEndDate;
    }

    public void setValidateEndDate(String validateEndDate) {
        this.validateEndDate = validateEndDate;
    }

    public String getApplyStartDate() {
        return applyStartDate;
    }

    public void setApplyStartDate(String applyStartDate) {
        this.applyStartDate = applyStartDate;
    }

    public String getApplyEndDate() {
        return applyEndDate;
    }

    public void setApplyEndDate(String applyEndDate) {
        this.applyEndDate = applyEndDate;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(String settleDate) {
        this.settleDate = settleDate;
    }

    public String getSettleNumber() {
        return settleNumber;
    }

    public void setSettleNumber(String settleNumber) {
        this.settleNumber = settleNumber;
    }

    @Override
    public String toString() {
        return "SettleInfo{" +
                "proId='" + proId + '\'' +
                ", contNo='" + contNo + '\'' +
                ", productCode='" + productCode + '\'' +
                ", productName='" + productName + '\'' +
                ", polApplyDate='" + polApplyDate + '\'' +
                ", cValidate='" + cValidate + '\'' +
                ", cEndDate='" + cEndDate + '\'' +
                ", appFlag='" + appFlag + '\'' +
                ", settleStatus='" + settleStatus + '\'' +
                ", sumPrem='" + sumPrem + '\'' +
                ", validateStartDate='" + validateStartDate + '\'' +
                ", validateEndDate='" + validateEndDate + '\'' +
                ", applyStartDate='" + applyStartDate + '\'' +
                ", applyEndDate='" + applyEndDate + '\'' +
                ", exportFlag='" + exportFlag + '\'' +
                ", settleDate='" + settleDate + '\'' +
                ", settleNumber='" + settleNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SettleInfo that = (SettleInfo) o;
        return Objects.equals(proId, that.proId) &&
                Objects.equals(contNo, that.contNo) &&
                Objects.equals(productCode, that.productCode) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(polApplyDate, that.polApplyDate) &&
                Objects.equals(cValidate, that.cValidate) &&
                Objects.equals(cEndDate, that.cEndDate) &&
                Objects.equals(appFlag, that.appFlag) &&
                Objects.equals(settleStatus, that.settleStatus) &&
                Objects.equals(sumPrem, that.sumPrem) &&
                Objects.equals(validateStartDate, that.validateStartDate) &&
                Objects.equals(validateEndDate, that.validateEndDate) &&
                Objects.equals(applyStartDate, that.applyStartDate) &&
                Objects.equals(applyEndDate, that.applyEndDate) &&
                Objects.equals(exportFlag, that.exportFlag) &&
                Objects.equals(settleDate, that.settleDate) &&
                Objects.equals(settleNumber, that.settleNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(proId, contNo, productCode, productName, polApplyDate, cValidate, cEndDate, appFlag, settleStatus, sumPrem, validateStartDate, validateEndDate, applyStartDate, applyEndDate, exportFlag, settleDate, settleNumber);
    }
}
