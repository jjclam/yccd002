package com.sinosoft.cloud.dal.yfbDao.model;

import java.util.Date;

public class LDSchemeRisk {
    private String selno;

    private String mainriskcode;

    private String mainriskversion;

    private String riskcode;

    private String riskversion;

    private String contschemecode;

    private String contschemename;

    private String remark;

    private String operator;

    private Date makedate;

    private String maketime;

    private Date modifydate;

    private String modifytime;

    private String schemetype;

    private String portfolioflag;

    public String getSelno() {
        return selno;
    }

    public void setSelno(String selno) {
        this.selno = selno;
    }

    public String getMainriskcode() {
        return mainriskcode;
    }

    public void setMainriskcode(String mainriskcode) {
        this.mainriskcode = mainriskcode;
    }

    public String getMainriskversion() {
        return mainriskversion;
    }

    public void setMainriskversion(String mainriskversion) {
        this.mainriskversion = mainriskversion;
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode;
    }

    public String getRiskversion() {
        return riskversion;
    }

    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion;
    }

    public String getContschemecode() {
        return contschemecode;
    }

    public void setContschemecode(String contschemecode) {
        this.contschemecode = contschemecode;
    }

    public String getContschemename() {
        return contschemename;
    }

    public void setContschemename(String contschemename) {
        this.contschemename = contschemename;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getMakedate() {
        return makedate;
    }

    public void setMakedate(Date makedate) {
        this.makedate = makedate;
    }

    public String getMaketime() {
        return maketime;
    }

    public void setMaketime(String maketime) {
        this.maketime = maketime;
    }

    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    public String getModifytime() {
        return modifytime;
    }

    public void setModifytime(String modifytime) {
        this.modifytime = modifytime;
    }

    public String getSchemetype() {
        return schemetype;
    }

    public void setSchemetype(String schemetype) {
        this.schemetype = schemetype;
    }

    public String getPortfolioflag() {
        return portfolioflag;
    }

    public void setPortfolioflag(String portfolioflag) {
        this.portfolioflag = portfolioflag;
    }
}