package com.sinosoft.cloud.dal.yfbDao.mapper;

import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeRisk;

import java.util.List;
import java.util.Map;

public interface LDSchemeRiskMapper {

    Integer countSchemeInfo(Map<String,Object> map);

    List<LDSchemeRisk> getSchemeInfoList(Map<String,Object> map);

    Integer countAllSchemeAuthorize(Map<String,Object> map);

    List<LDSchemeRisk> getAllSchemeAuthorizeList(Map<String,Object> map);
}