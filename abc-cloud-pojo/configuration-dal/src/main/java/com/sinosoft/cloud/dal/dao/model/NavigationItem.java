package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class NavigationItem implements Serializable {
    private String title;
    private String icon;
    private String href;
    private boolean spread;
    private List<NavigationItem> children;

    public NavigationItem() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isSpread() {
        return spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }

    public List<NavigationItem> getChildren() {
        return children;
    }

    public void setChildren(List<NavigationItem> children) {
        this.children = children;
    }


    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}