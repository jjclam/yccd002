package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.entity.LMCalModePojo;

import java.util.List;
import java.util.Map;

public interface FunctDefMapper {
    //查询日志信息
    List<LMCalModePojo> selectByTaskName(Map<String, Object> map);
    //查询任务编码下拉
    List<LMCalModePojo> getCodes();
    //查询记录总条数
    Integer countForList(Map<String, Object> map);
}
