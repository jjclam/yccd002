package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class LDPlanExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public LDPlanExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andCONTPLANCODEIsNull() {
            addCriterion("CONTPLANCODE is null");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEIsNotNull() {
            addCriterion("CONTPLANCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEEqualTo(String value) {
            addCriterion("CONTPLANCODE =", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODENotEqualTo(String value) {
            addCriterion("CONTPLANCODE <>", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEGreaterThan(String value) {
            addCriterion("CONTPLANCODE >", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEGreaterThanOrEqualTo(String value) {
            addCriterion("CONTPLANCODE >=", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODELessThan(String value) {
            addCriterion("CONTPLANCODE <", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODELessThanOrEqualTo(String value) {
            addCriterion("CONTPLANCODE <=", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODELike(String value) {
            addCriterion("CONTPLANCODE like", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODENotLike(String value) {
            addCriterion("CONTPLANCODE not like", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEIn(List<String> values) {
            addCriterion("CONTPLANCODE in", values, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODENotIn(List<String> values) {
            addCriterion("CONTPLANCODE not in", values, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEBetween(String value1, String value2) {
            addCriterion("CONTPLANCODE between", value1, value2, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODENotBetween(String value1, String value2) {
            addCriterion("CONTPLANCODE not between", value1, value2, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEIsNull() {
            addCriterion("CONTPLANNAME is null");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEIsNotNull() {
            addCriterion("CONTPLANNAME is not null");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEEqualTo(String value) {
            addCriterion("CONTPLANNAME =", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMENotEqualTo(String value) {
            addCriterion("CONTPLANNAME <>", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEGreaterThan(String value) {
            addCriterion("CONTPLANNAME >", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("CONTPLANNAME >=", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMELessThan(String value) {
            addCriterion("CONTPLANNAME <", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMELessThanOrEqualTo(String value) {
            addCriterion("CONTPLANNAME <=", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMELike(String value) {
            addCriterion("CONTPLANNAME like", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMENotLike(String value) {
            addCriterion("CONTPLANNAME not like", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEIn(List<String> values) {
            addCriterion("CONTPLANNAME in", values, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMENotIn(List<String> values) {
            addCriterion("CONTPLANNAME not in", values, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEBetween(String value1, String value2) {
            addCriterion("CONTPLANNAME between", value1, value2, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMENotBetween(String value1, String value2) {
            addCriterion("CONTPLANNAME not between", value1, value2, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEIsNull() {
            addCriterion("PLANTYPE is null");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEIsNotNull() {
            addCriterion("PLANTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEEqualTo(String value) {
            addCriterion("PLANTYPE =", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPENotEqualTo(String value) {
            addCriterion("PLANTYPE <>", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEGreaterThan(String value) {
            addCriterion("PLANTYPE >", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("PLANTYPE >=", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPELessThan(String value) {
            addCriterion("PLANTYPE <", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPELessThanOrEqualTo(String value) {
            addCriterion("PLANTYPE <=", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPELike(String value) {
            addCriterion("PLANTYPE like", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPENotLike(String value) {
            addCriterion("PLANTYPE not like", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEIn(List<String> values) {
            addCriterion("PLANTYPE in", values, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPENotIn(List<String> values) {
            addCriterion("PLANTYPE not in", values, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEBetween(String value1, String value2) {
            addCriterion("PLANTYPE between", value1, value2, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPENotBetween(String value1, String value2) {
            addCriterion("PLANTYPE not between", value1, value2, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANRULEIsNull() {
            addCriterion("PLANRULE is null");
            return (Criteria) this;
        }

        public Criteria andPLANRULEIsNotNull() {
            addCriterion("PLANRULE is not null");
            return (Criteria) this;
        }

        public Criteria andPLANRULEEqualTo(String value) {
            addCriterion("PLANRULE =", value, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULENotEqualTo(String value) {
            addCriterion("PLANRULE <>", value, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULEGreaterThan(String value) {
            addCriterion("PLANRULE >", value, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULEGreaterThanOrEqualTo(String value) {
            addCriterion("PLANRULE >=", value, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULELessThan(String value) {
            addCriterion("PLANRULE <", value, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULELessThanOrEqualTo(String value) {
            addCriterion("PLANRULE <=", value, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULELike(String value) {
            addCriterion("PLANRULE like", value, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULENotLike(String value) {
            addCriterion("PLANRULE not like", value, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULEIn(List<String> values) {
            addCriterion("PLANRULE in", values, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULENotIn(List<String> values) {
            addCriterion("PLANRULE not in", values, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULEBetween(String value1, String value2) {
            addCriterion("PLANRULE between", value1, value2, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANRULENotBetween(String value1, String value2) {
            addCriterion("PLANRULE not between", value1, value2, "PLANRULE");
            return (Criteria) this;
        }

        public Criteria andPLANSQLIsNull() {
            addCriterion("PLANSQL is null");
            return (Criteria) this;
        }

        public Criteria andPLANSQLIsNotNull() {
            addCriterion("PLANSQL is not null");
            return (Criteria) this;
        }

        public Criteria andPLANSQLEqualTo(String value) {
            addCriterion("PLANSQL =", value, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLNotEqualTo(String value) {
            addCriterion("PLANSQL <>", value, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLGreaterThan(String value) {
            addCriterion("PLANSQL >", value, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLGreaterThanOrEqualTo(String value) {
            addCriterion("PLANSQL >=", value, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLLessThan(String value) {
            addCriterion("PLANSQL <", value, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLLessThanOrEqualTo(String value) {
            addCriterion("PLANSQL <=", value, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLLike(String value) {
            addCriterion("PLANSQL like", value, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLNotLike(String value) {
            addCriterion("PLANSQL not like", value, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLIn(List<String> values) {
            addCriterion("PLANSQL in", values, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLNotIn(List<String> values) {
            addCriterion("PLANSQL not in", values, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLBetween(String value1, String value2) {
            addCriterion("PLANSQL between", value1, value2, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andPLANSQLNotBetween(String value1, String value2) {
            addCriterion("PLANSQL not between", value1, value2, "PLANSQL");
            return (Criteria) this;
        }

        public Criteria andREMARKIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andREMARKIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andREMARKEqualTo(String value) {
            addCriterion("REMARK =", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKGreaterThan(String value) {
            addCriterion("REMARK >", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKLessThan(String value) {
            addCriterion("REMARK <", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKLike(String value) {
            addCriterion("REMARK like", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKNotLike(String value) {
            addCriterion("REMARK not like", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKIn(List<String> values) {
            addCriterion("REMARK in", values, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "REMARK");
            return (Criteria) this;
        }

        public Criteria andOPERATORIsNull() {
            addCriterion("OPERATOR is null");
            return (Criteria) this;
        }

        public Criteria andOPERATORIsNotNull() {
            addCriterion("OPERATOR is not null");
            return (Criteria) this;
        }

        public Criteria andOPERATOREqualTo(String value) {
            addCriterion("OPERATOR =", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotEqualTo(String value) {
            addCriterion("OPERATOR <>", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORGreaterThan(String value) {
            addCriterion("OPERATOR >", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATOR >=", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLessThan(String value) {
            addCriterion("OPERATOR <", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLessThanOrEqualTo(String value) {
            addCriterion("OPERATOR <=", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLike(String value) {
            addCriterion("OPERATOR like", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotLike(String value) {
            addCriterion("OPERATOR not like", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORIn(List<String> values) {
            addCriterion("OPERATOR in", values, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotIn(List<String> values) {
            addCriterion("OPERATOR not in", values, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORBetween(String value1, String value2) {
            addCriterion("OPERATOR between", value1, value2, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotBetween(String value1, String value2) {
            addCriterion("OPERATOR not between", value1, value2, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIsNull() {
            addCriterion("MAKEDATE is null");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIsNotNull() {
            addCriterion("MAKEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE =", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE <>", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("MAKEDATE >", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE >=", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATELessThan(Date value) {
            addCriterionForJDBCDate("MAKEDATE <", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE <=", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIn(List<Date> values) {
            addCriterionForJDBCDate("MAKEDATE in", values, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("MAKEDATE not in", values, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MAKEDATE between", value1, value2, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MAKEDATE not between", value1, value2, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIsNull() {
            addCriterion("MAKETIME is null");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIsNotNull() {
            addCriterion("MAKETIME is not null");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEEqualTo(String value) {
            addCriterion("MAKETIME =", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotEqualTo(String value) {
            addCriterion("MAKETIME <>", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEGreaterThan(String value) {
            addCriterion("MAKETIME >", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEGreaterThanOrEqualTo(String value) {
            addCriterion("MAKETIME >=", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELessThan(String value) {
            addCriterion("MAKETIME <", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELessThanOrEqualTo(String value) {
            addCriterion("MAKETIME <=", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELike(String value) {
            addCriterion("MAKETIME like", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotLike(String value) {
            addCriterion("MAKETIME not like", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIn(List<String> values) {
            addCriterion("MAKETIME in", values, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotIn(List<String> values) {
            addCriterion("MAKETIME not in", values, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEBetween(String value1, String value2) {
            addCriterion("MAKETIME between", value1, value2, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotBetween(String value1, String value2) {
            addCriterion("MAKETIME not between", value1, value2, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIsNull() {
            addCriterion("MODIFYDATE is null");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIsNotNull() {
            addCriterion("MODIFYDATE is not null");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE =", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <>", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("MODIFYDATE >", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE >=", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATELessThan(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <=", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIn(List<Date> values) {
            addCriterionForJDBCDate("MODIFYDATE in", values, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("MODIFYDATE not in", values, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MODIFYDATE between", value1, value2, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MODIFYDATE not between", value1, value2, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIsNull() {
            addCriterion("MODIFYTIME is null");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIsNotNull() {
            addCriterion("MODIFYTIME is not null");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEEqualTo(String value) {
            addCriterion("MODIFYTIME =", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotEqualTo(String value) {
            addCriterion("MODIFYTIME <>", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEGreaterThan(String value) {
            addCriterion("MODIFYTIME >", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFYTIME >=", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELessThan(String value) {
            addCriterion("MODIFYTIME <", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELessThanOrEqualTo(String value) {
            addCriterion("MODIFYTIME <=", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELike(String value) {
            addCriterion("MODIFYTIME like", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotLike(String value) {
            addCriterion("MODIFYTIME not like", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIn(List<String> values) {
            addCriterion("MODIFYTIME in", values, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotIn(List<String> values) {
            addCriterion("MODIFYTIME not in", values, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEBetween(String value1, String value2) {
            addCriterion("MODIFYTIME between", value1, value2, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotBetween(String value1, String value2) {
            addCriterion("MODIFYTIME not between", value1, value2, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3IsNull() {
            addCriterion("PEOPLES3 is null");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3IsNotNull() {
            addCriterion("PEOPLES3 is not null");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3EqualTo(String value) {
            addCriterion("PEOPLES3 =", value, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3NotEqualTo(String value) {
            addCriterion("PEOPLES3 <>", value, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3GreaterThan(String value) {
            addCriterion("PEOPLES3 >", value, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3GreaterThanOrEqualTo(String value) {
            addCriterion("PEOPLES3 >=", value, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3LessThan(String value) {
            addCriterion("PEOPLES3 <", value, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3LessThanOrEqualTo(String value) {
            addCriterion("PEOPLES3 <=", value, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3Like(String value) {
            addCriterion("PEOPLES3 like", value, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3NotLike(String value) {
            addCriterion("PEOPLES3 not like", value, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3In(List<String> values) {
            addCriterion("PEOPLES3 in", values, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3NotIn(List<String> values) {
            addCriterion("PEOPLES3 not in", values, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3Between(String value1, String value2) {
            addCriterion("PEOPLES3 between", value1, value2, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andPEOPLES3NotBetween(String value1, String value2) {
            addCriterion("PEOPLES3 not between", value1, value2, "PEOPLES3");
            return (Criteria) this;
        }

        public Criteria andREMARK2IsNull() {
            addCriterion("REMARK2 is null");
            return (Criteria) this;
        }

        public Criteria andREMARK2IsNotNull() {
            addCriterion("REMARK2 is not null");
            return (Criteria) this;
        }

        public Criteria andREMARK2EqualTo(String value) {
            addCriterion("REMARK2 =", value, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2NotEqualTo(String value) {
            addCriterion("REMARK2 <>", value, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2GreaterThan(String value) {
            addCriterion("REMARK2 >", value, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2GreaterThanOrEqualTo(String value) {
            addCriterion("REMARK2 >=", value, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2LessThan(String value) {
            addCriterion("REMARK2 <", value, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2LessThanOrEqualTo(String value) {
            addCriterion("REMARK2 <=", value, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2Like(String value) {
            addCriterion("REMARK2 like", value, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2NotLike(String value) {
            addCriterion("REMARK2 not like", value, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2In(List<String> values) {
            addCriterion("REMARK2 in", values, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2NotIn(List<String> values) {
            addCriterion("REMARK2 not in", values, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2Between(String value1, String value2) {
            addCriterion("REMARK2 between", value1, value2, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andREMARK2NotBetween(String value1, String value2) {
            addCriterion("REMARK2 not between", value1, value2, "REMARK2");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMIsNull() {
            addCriterion("MANAGECOM is null");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMIsNotNull() {
            addCriterion("MANAGECOM is not null");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMEqualTo(String value) {
            addCriterion("MANAGECOM =", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMNotEqualTo(String value) {
            addCriterion("MANAGECOM <>", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMGreaterThan(String value) {
            addCriterion("MANAGECOM >", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMGreaterThanOrEqualTo(String value) {
            addCriterion("MANAGECOM >=", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMLessThan(String value) {
            addCriterion("MANAGECOM <", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMLessThanOrEqualTo(String value) {
            addCriterion("MANAGECOM <=", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMLike(String value) {
            addCriterion("MANAGECOM like", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMNotLike(String value) {
            addCriterion("MANAGECOM not like", value, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMIn(List<String> values) {
            addCriterion("MANAGECOM in", values, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMNotIn(List<String> values) {
            addCriterion("MANAGECOM not in", values, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMBetween(String value1, String value2) {
            addCriterion("MANAGECOM between", value1, value2, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andMANAGECOMNotBetween(String value1, String value2) {
            addCriterion("MANAGECOM not between", value1, value2, "MANAGECOM");
            return (Criteria) this;
        }

        public Criteria andSALECHNLIsNull() {
            addCriterion("SALECHNL is null");
            return (Criteria) this;
        }

        public Criteria andSALECHNLIsNotNull() {
            addCriterion("SALECHNL is not null");
            return (Criteria) this;
        }

        public Criteria andSALECHNLEqualTo(String value) {
            addCriterion("SALECHNL =", value, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLNotEqualTo(String value) {
            addCriterion("SALECHNL <>", value, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLGreaterThan(String value) {
            addCriterion("SALECHNL >", value, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLGreaterThanOrEqualTo(String value) {
            addCriterion("SALECHNL >=", value, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLLessThan(String value) {
            addCriterion("SALECHNL <", value, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLLessThanOrEqualTo(String value) {
            addCriterion("SALECHNL <=", value, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLLike(String value) {
            addCriterion("SALECHNL like", value, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLNotLike(String value) {
            addCriterion("SALECHNL not like", value, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLIn(List<String> values) {
            addCriterion("SALECHNL in", values, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLNotIn(List<String> values) {
            addCriterion("SALECHNL not in", values, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLBetween(String value1, String value2) {
            addCriterion("SALECHNL between", value1, value2, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSALECHNLNotBetween(String value1, String value2) {
            addCriterion("SALECHNL not between", value1, value2, "SALECHNL");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEIsNull() {
            addCriterion("STARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEIsNotNull() {
            addCriterion("STARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEEqualTo(Date value) {
            addCriterionForJDBCDate("STARTDATE =", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("STARTDATE <>", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("STARTDATE >", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("STARTDATE >=", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATELessThan(Date value) {
            addCriterionForJDBCDate("STARTDATE <", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("STARTDATE <=", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEIn(List<Date> values) {
            addCriterionForJDBCDate("STARTDATE in", values, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("STARTDATE not in", values, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("STARTDATE between", value1, value2, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("STARTDATE not between", value1, value2, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andENDDATEIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andENDDATEEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE =", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE <>", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("ENDDATE >", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE >=", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATELessThan(Date value) {
            addCriterionForJDBCDate("ENDDATE <", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE <=", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEIn(List<Date> values) {
            addCriterionForJDBCDate("ENDDATE in", values, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("ENDDATE not in", values, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ENDDATE between", value1, value2, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ENDDATE not between", value1, value2, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andSTATEIsNull() {
            addCriterion("STATE is null");
            return (Criteria) this;
        }

        public Criteria andSTATEIsNotNull() {
            addCriterion("STATE is not null");
            return (Criteria) this;
        }

        public Criteria andSTATEEqualTo(String value) {
            addCriterion("STATE =", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATENotEqualTo(String value) {
            addCriterion("STATE <>", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATEGreaterThan(String value) {
            addCriterion("STATE >", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATEGreaterThanOrEqualTo(String value) {
            addCriterion("STATE >=", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATELessThan(String value) {
            addCriterion("STATE <", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATELessThanOrEqualTo(String value) {
            addCriterion("STATE <=", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATELike(String value) {
            addCriterion("STATE like", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATENotLike(String value) {
            addCriterion("STATE not like", value, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATEIn(List<String> values) {
            addCriterion("STATE in", values, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATENotIn(List<String> values) {
            addCriterion("STATE not in", values, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATEBetween(String value1, String value2) {
            addCriterion("STATE between", value1, value2, "STATE");
            return (Criteria) this;
        }

        public Criteria andSTATENotBetween(String value1, String value2) {
            addCriterion("STATE not between", value1, value2, "STATE");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1IsNull() {
            addCriterion("PLANKIND1 is null");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1IsNotNull() {
            addCriterion("PLANKIND1 is not null");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1EqualTo(String value) {
            addCriterion("PLANKIND1 =", value, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1NotEqualTo(String value) {
            addCriterion("PLANKIND1 <>", value, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1GreaterThan(String value) {
            addCriterion("PLANKIND1 >", value, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1GreaterThanOrEqualTo(String value) {
            addCriterion("PLANKIND1 >=", value, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1LessThan(String value) {
            addCriterion("PLANKIND1 <", value, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1LessThanOrEqualTo(String value) {
            addCriterion("PLANKIND1 <=", value, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1Like(String value) {
            addCriterion("PLANKIND1 like", value, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1NotLike(String value) {
            addCriterion("PLANKIND1 not like", value, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1In(List<String> values) {
            addCriterion("PLANKIND1 in", values, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1NotIn(List<String> values) {
            addCriterion("PLANKIND1 not in", values, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1Between(String value1, String value2) {
            addCriterion("PLANKIND1 between", value1, value2, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND1NotBetween(String value1, String value2) {
            addCriterion("PLANKIND1 not between", value1, value2, "PLANKIND1");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2IsNull() {
            addCriterion("PLANKIND2 is null");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2IsNotNull() {
            addCriterion("PLANKIND2 is not null");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2EqualTo(String value) {
            addCriterion("PLANKIND2 =", value, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2NotEqualTo(String value) {
            addCriterion("PLANKIND2 <>", value, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2GreaterThan(String value) {
            addCriterion("PLANKIND2 >", value, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2GreaterThanOrEqualTo(String value) {
            addCriterion("PLANKIND2 >=", value, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2LessThan(String value) {
            addCriterion("PLANKIND2 <", value, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2LessThanOrEqualTo(String value) {
            addCriterion("PLANKIND2 <=", value, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2Like(String value) {
            addCriterion("PLANKIND2 like", value, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2NotLike(String value) {
            addCriterion("PLANKIND2 not like", value, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2In(List<String> values) {
            addCriterion("PLANKIND2 in", values, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2NotIn(List<String> values) {
            addCriterion("PLANKIND2 not in", values, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2Between(String value1, String value2) {
            addCriterion("PLANKIND2 between", value1, value2, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND2NotBetween(String value1, String value2) {
            addCriterion("PLANKIND2 not between", value1, value2, "PLANKIND2");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3IsNull() {
            addCriterion("PLANKIND3 is null");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3IsNotNull() {
            addCriterion("PLANKIND3 is not null");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3EqualTo(String value) {
            addCriterion("PLANKIND3 =", value, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3NotEqualTo(String value) {
            addCriterion("PLANKIND3 <>", value, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3GreaterThan(String value) {
            addCriterion("PLANKIND3 >", value, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3GreaterThanOrEqualTo(String value) {
            addCriterion("PLANKIND3 >=", value, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3LessThan(String value) {
            addCriterion("PLANKIND3 <", value, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3LessThanOrEqualTo(String value) {
            addCriterion("PLANKIND3 <=", value, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3Like(String value) {
            addCriterion("PLANKIND3 like", value, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3NotLike(String value) {
            addCriterion("PLANKIND3 not like", value, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3In(List<String> values) {
            addCriterion("PLANKIND3 in", values, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3NotIn(List<String> values) {
            addCriterion("PLANKIND3 not in", values, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3Between(String value1, String value2) {
            addCriterion("PLANKIND3 between", value1, value2, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPLANKIND3NotBetween(String value1, String value2) {
            addCriterion("PLANKIND3 not between", value1, value2, "PLANKIND3");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGIsNull() {
            addCriterion("PORTFOLIOFLAG is null");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGIsNotNull() {
            addCriterion("PORTFOLIOFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGEqualTo(String value) {
            addCriterion("PORTFOLIOFLAG =", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGNotEqualTo(String value) {
            addCriterion("PORTFOLIOFLAG <>", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGGreaterThan(String value) {
            addCriterion("PORTFOLIOFLAG >", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("PORTFOLIOFLAG >=", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGLessThan(String value) {
            addCriterion("PORTFOLIOFLAG <", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGLessThanOrEqualTo(String value) {
            addCriterion("PORTFOLIOFLAG <=", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGLike(String value) {
            addCriterion("PORTFOLIOFLAG like", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGNotLike(String value) {
            addCriterion("PORTFOLIOFLAG not like", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGIn(List<String> values) {
            addCriterion("PORTFOLIOFLAG in", values, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGNotIn(List<String> values) {
            addCriterion("PORTFOLIOFLAG not in", values, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGBetween(String value1, String value2) {
            addCriterion("PORTFOLIOFLAG between", value1, value2, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGNotBetween(String value1, String value2) {
            addCriterion("PORTFOLIOFLAG not between", value1, value2, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGIsNull() {
            addCriterion("STANDARDFLAG is null");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGIsNotNull() {
            addCriterion("STANDARDFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGEqualTo(String value) {
            addCriterion("STANDARDFLAG =", value, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGNotEqualTo(String value) {
            addCriterion("STANDARDFLAG <>", value, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGGreaterThan(String value) {
            addCriterion("STANDARDFLAG >", value, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("STANDARDFLAG >=", value, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGLessThan(String value) {
            addCriterion("STANDARDFLAG <", value, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGLessThanOrEqualTo(String value) {
            addCriterion("STANDARDFLAG <=", value, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGLike(String value) {
            addCriterion("STANDARDFLAG like", value, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGNotLike(String value) {
            addCriterion("STANDARDFLAG not like", value, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGIn(List<String> values) {
            addCriterion("STANDARDFLAG in", values, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGNotIn(List<String> values) {
            addCriterion("STANDARDFLAG not in", values, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGBetween(String value1, String value2) {
            addCriterion("STANDARDFLAG between", value1, value2, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andSTANDARDFLAGNotBetween(String value1, String value2) {
            addCriterion("STANDARDFLAG not between", value1, value2, "STANDARDFLAG");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSIsNull() {
            addCriterion("UPDOWNSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSIsNotNull() {
            addCriterion("UPDOWNSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSEqualTo(String value) {
            addCriterion("UPDOWNSTATUS =", value, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSNotEqualTo(String value) {
            addCriterion("UPDOWNSTATUS <>", value, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSGreaterThan(String value) {
            addCriterion("UPDOWNSTATUS >", value, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSGreaterThanOrEqualTo(String value) {
            addCriterion("UPDOWNSTATUS >=", value, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSLessThan(String value) {
            addCriterion("UPDOWNSTATUS <", value, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSLessThanOrEqualTo(String value) {
            addCriterion("UPDOWNSTATUS <=", value, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSLike(String value) {
            addCriterion("UPDOWNSTATUS like", value, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSNotLike(String value) {
            addCriterion("UPDOWNSTATUS not like", value, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSIn(List<String> values) {
            addCriterion("UPDOWNSTATUS in", values, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSNotIn(List<String> values) {
            addCriterion("UPDOWNSTATUS not in", values, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSBetween(String value1, String value2) {
            addCriterion("UPDOWNSTATUS between", value1, value2, "UPDOWNSTATUS");
            return (Criteria) this;
        }

        public Criteria andUPDOWNSTATUSNotBetween(String value1, String value2) {
            addCriterion("UPDOWNSTATUS not between", value1, value2, "UPDOWNSTATUS");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}