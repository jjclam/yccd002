package com.sinosoft.cloud.dal.dao.model.entity;

import java.util.Date;

public class LZCardTrack {
    private String certifyCode;
    private String subCode;
    private String riskCode;
    private String riskVersion;
    private String startNo;
    private String endNo;
    private String sendOutCom;
    private String receiveCom;
    private Integer sumCount;
    private int prem;
    private int amnt;
    private String handler;
    private Date handleDate;
    private Date invaliDate;
    private String takeBackNo;
    private String saleChnl;
    private String stateFlag;
    private String operateFlag;
    private String payFlag;
    private String enteraccFlag;
    private String reason;
    private String state;

    private String operator;
    private String makeDate;
    private String makeTime;

    private String modifyDate;
    private String modifyTime;
    private String applyNo;

    private String agentCom;
    private Integer validDays;
    private String lostindFlag;
    private String lostindContent;
    private String backFlag;
    private String barCode;

    public String getCertifyCode() {
        return certifyCode;
    }

    public void setCertifyCode(String certifyCode) {
        this.certifyCode = certifyCode;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public String getRiskVersion() {
        return riskVersion;
    }

    public void setRiskVersion(String riskVersion) {
        this.riskVersion = riskVersion;
    }

    public String getStartNo() {
        return startNo;
    }

    public void setStartNo(String startNo) {
        this.startNo = startNo;
    }

    public String getEndNo() {
        return endNo;
    }

    public void setEndNo(String endNo) {
        this.endNo = endNo;
    }

    public String getSendOutCom() {
        return sendOutCom;
    }

    public void setSendOutCom(String sendOutCom) {
        this.sendOutCom = sendOutCom;
    }

    public String getReceiveCom() {
        return receiveCom;
    }

    public void setReceiveCom(String receiveCom) {
        this.receiveCom = receiveCom;
    }

    public Integer getSumCount() {
        return sumCount;
    }

    public void setSumCount(Integer sumCount) {
        this.sumCount = sumCount;
    }

    public int getPrem() {
        return prem;
    }

    public void setPrem(int prem) {
        this.prem = prem;
    }

    public int getAmnt() {
        return amnt;
    }

    public void setAmnt(int amnt) {
        this.amnt = amnt;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

    public Date getHandleDate() {
        return handleDate;
    }

    public void setHandleDate(Date handleDate) {
        this.handleDate = handleDate;
    }

    public Date getInvaliDate() {
        return invaliDate;
    }

    public void setInvaliDate(Date invaliDate) {
        this.invaliDate = invaliDate;
    }

    public String getTakeBackNo() {
        return takeBackNo;
    }

    public void setTakeBackNo(String takeBackNo) {
        this.takeBackNo = takeBackNo;
    }

    public String getSaleChnl() {
        return saleChnl;
    }

    public void setSaleChnl(String saleChnl) {
        this.saleChnl = saleChnl;
    }

    public String getStateFlag() {
        return stateFlag;
    }

    public void setStateFlag(String stateFlag) {
        this.stateFlag = stateFlag;
    }

    public String getOperateFlag() {
        return operateFlag;
    }

    public void setOperateFlag(String operateFlag) {
        this.operateFlag = operateFlag;
    }

    public String getPayFlag() {
        return payFlag;
    }

    public void setPayFlag(String payFlag) {
        this.payFlag = payFlag;
    }

    public String getEnteraccFlag() {
        return enteraccFlag;
    }

    public void setEnteraccFlag(String enteraccFlag) {
        this.enteraccFlag = enteraccFlag;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getMakeTime() {
        return makeTime;
    }

    public void setMakeTime(String makeTime) {
        this.makeTime = makeTime;
    }


    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getApplyNo() {
        return applyNo;
    }

    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    public String getAgentCom() {
        return agentCom;
    }

    public void setAgentCom(String agentCom) {
        this.agentCom = agentCom;
    }

    public Integer getValidDays() {
        return validDays;
    }

    public void setValidDays(Integer validDays) {
        this.validDays = validDays;
    }

    public String getLostindFlag() {
        return lostindFlag;
    }

    public void setLostindFlag(String lostindFlag) {
        this.lostindFlag = lostindFlag;
    }

    public String getLostindContent() {
        return lostindContent;
    }

    public void setLostindContent(String lostindContent) {
        this.lostindContent = lostindContent;
    }

    public String getBackFlag() {
        return backFlag;
    }

    public void setBackFlag(String backFlag) {
        this.backFlag = backFlag;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    @Override
    public String toString() {
        return "LZCardTrack{" +
                "certifyCode='" + certifyCode + '\'' +
                ", subCode='" + subCode + '\'' +
                ", riskCode='" + riskCode + '\'' +
                ", riskVersion='" + riskVersion + '\'' +
                ", startNo='" + startNo + '\'' +
                ", endNo='" + endNo + '\'' +
                ", sendOutCom='" + sendOutCom + '\'' +
                ", receiveCom='" + receiveCom + '\'' +
                ", sumCount=" + sumCount +
                ", prem=" + prem +
                ", amnt=" + amnt +
                ", handler='" + handler + '\'' +
                ", handleDate=" + handleDate +
                ", invaliDate=" + invaliDate +
                ", takeBackNo='" + takeBackNo + '\'' +
                ", saleChnl='" + saleChnl + '\'' +
                ", stateFlag='" + stateFlag + '\'' +
                ", operateFlag='" + operateFlag + '\'' +
                ", payFlag='" + payFlag + '\'' +
                ", enteraccFlag='" + enteraccFlag + '\'' +
                ", reason='" + reason + '\'' +
                ", state='" + state + '\'' +
                ", operator='" + operator + '\'' +
                ", makeDate=" + makeDate +
                ", makeTime='" + makeTime + '\'' +
                ", modifyDate=" + modifyDate +
                ", modifyTime='" + modifyTime + '\'' +
                ", applyNo='" + applyNo + '\'' +
                ", agentCom='" + agentCom + '\'' +
                ", validDays=" + validDays +
                ", lostindFlag='" + lostindFlag + '\'' +
                ", lostindContent='" + lostindContent + '\'' +
                ", backFlag='" + backFlag + '\'' +
                ", barCode='" + barCode + '\'' +
                '}';
    }
}
