package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.UserMgr;
import org.apache.ibatis.annotations.Param;

public interface UserMgrMapper {

    UserMgr CheckUser(@Param("userName") String username, @Param("password") String password);

    int insert(UserMgr record);

    int insertSelective(UserMgr record);
}