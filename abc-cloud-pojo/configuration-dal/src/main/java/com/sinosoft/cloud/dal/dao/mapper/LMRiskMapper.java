package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LMRisk;
import com.sinosoft.cloud.dal.dao.model.LMRiskExample;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SaleProInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface LMRiskMapper {
    int countByExample();

    int deleteByExample(LMRiskExample example);

    int insert(LMRisk record);

    int insertSelective(LMRisk record);

    List<LMRisk> selectByExample(LMRiskExample example);

    int updateByExampleSelective(@Param("record") LMRisk record, @Param("example") LMRiskExample example);

    int updateByExample(@Param("record") LMRisk record, @Param("example") LMRiskExample example);

    List<ProInfo> selectOnlyProCode();

    List<ProInfo> selectCombProCode();

    List<ProInfo> selectCombProName();

    List<ProInfo> selectByRiskCode(@Param(value = "proCode") String proCode);

    List<ProInfo> selectByParam(@Param(value = "proCode") String proCode, @Param(value = "proName") String proName);

    List<ProInfo> selectProCodeName();

    List<LMRisk> getProCodeName();

    List<ProInfo> selectForHEXIN(@Param(value = "proCode") String proCode, @Param(value = "proType") String proType);

    List<ProInfo> selectForTAOCAN(@Param(value = "proCode") String proCode, @Param(value = "proType") String proType);

    List<ProInfo> selectByNoParam(@Param(value = "proType") String proType);

    List<ProInfo> selectForHEXINex(@Param(value = "proCode") String proCode, @Param(value = "proType") String proType);

    List<ProInfo> selectForTAOCANex(@Param(value = "proCode") String proCode, @Param(value = "proType") String proType);

    List<ProInfo> selectByNoParamex();

    Integer upProForHEXIN(String proCode);

    Integer upProForTAOCAN(String proCode);

    Integer downProForHEXIN(String proCode);

    Integer downProForTAOCAN(String proCode);

    Integer editProForHEXIN(@Param(value = "proCode") String proCode, @Param(value = "flag") String flag);

    Integer editProForTAOCAN(@Param(value = "proCode") String proCode, @Param(value = "flag") String flag);

    SaleProInfo selectSaleProInfoByProCode(@Param(value = "proCode") String proCode, @Param(value = "channelCode") String channelCode);

    SaleProInfo selectSaleProInfoByProCodeex(@Param(value = "proCode") String proCode, @Param(value = "channelCode") String channelCode);

    Integer countByHEXIN(@Param(value = "proCode") String proCode, @Param(value = "proType") String proType);

    Integer countByTAOCAN(@Param(value = "proCode") String proCode, @Param(value = "proType") String proType);

    Integer countByNoParam(@Param(value = "proType") String proType);

    ProInfo confirmChlProHaved(Map<String,Object> map);

    ProInfo confirmChlProHavedTC(Map<String,Object> map);

    ProInfo getProStartEndDate(String riskCode);

    List<ProInfo> getAllProForStartDate(String riskCode);

    List<SaleProInfo> selectSaleProInfoByRiskType(@Param(value = "proType") String riskType);

    SaleProInfo selectRiskName(@Param(value = "proCode") String riskCode);

    ProInfo getTCProStartEndDate(String riskCode);

    Integer upProForTAOCAN2(Map<String,Object> map);
}