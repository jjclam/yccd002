package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class SubContInfo implements Serializable {
    private Integer id;

    private String contNo;

    private String appntName;

    private String appntIDType;

    private String appntIDNo;

    private String mobile;

    private Float relationship;

    private String insuredName;

    private String insuredIDType;

    private String insuredIDNo;

    private String prodSetCode;

    private String cValidate;

    private String opreator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getAppntName() {
        return appntName;
    }

    public void setAppntName(String appntName) {
        this.appntName = appntName;
    }

    public String getAppntIDType() {
        return appntIDType;
    }

    public void setAppntIDType(String appntIDType) {
        this.appntIDType = appntIDType;
    }

    public String getAppntIDNo() {
        return appntIDNo;
    }

    public void setAppntIDNo(String appntIDNo) {
        this.appntIDNo = appntIDNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Float getRelationship() {
        return relationship;
    }

    public void setRelationship(Float relationship) {
        this.relationship = relationship;
    }

    public String getInsuredName() {
        return insuredName;
    }

    public void setInsuredName(String insuredName) {
        this.insuredName = insuredName;
    }

    public String getInsuredIDType() {
        return insuredIDType;
    }

    public void setInsuredIDType(String insuredIDType) {
        this.insuredIDType = insuredIDType;
    }

    public String getInsuredIDNo() {
        return insuredIDNo;
    }

    public void setInsuredIDNo(String insuredIDNo) {
        this.insuredIDNo = insuredIDNo;
    }

    public String getProdSetCode() {
        return prodSetCode;
    }

    public void setProdSetCode(String prodSetCode) {
        this.prodSetCode = prodSetCode;
    }

    public String getcValidate() {
        return cValidate;
    }

    public void setcValidate(String cValidate) {
        this.cValidate = cValidate;
    }

    public String getOpreator() {
        return opreator;
    }

    public void setOpreator(String opreator) {
        this.opreator = opreator;
    }

    @Override
    public String toString() {
        return "SubContInfo{" +
                "id=" + id +
                ", contNo='" + contNo + '\'' +
                ", appntName='" + appntName + '\'' +
                ", appntIDType='" + appntIDType + '\'' +
                ", appntIDNo='" + appntIDNo + '\'' +
                ", mobile='" + mobile + '\'' +
                ", relationship=" + relationship +
                ", insuredName='" + insuredName + '\'' +
                ", insuredIDType='" + insuredIDType + '\'' +
                ", insuredIDNo='" + insuredIDNo + '\'' +
                ", prodSetCode='" + prodSetCode + '\'' +
                ", cValidate='" + cValidate + '\'' +
                ", opreator='" + opreator + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubContInfo that = (SubContInfo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(contNo, that.contNo) &&
                Objects.equals(appntName, that.appntName) &&
                Objects.equals(appntIDType, that.appntIDType) &&
                Objects.equals(appntIDNo, that.appntIDNo) &&
                Objects.equals(mobile, that.mobile) &&
                Objects.equals(relationship, that.relationship) &&
                Objects.equals(insuredName, that.insuredName) &&
                Objects.equals(insuredIDType, that.insuredIDType) &&
                Objects.equals(insuredIDNo, that.insuredIDNo) &&
                Objects.equals(prodSetCode, that.prodSetCode) &&
                Objects.equals(cValidate, that.cValidate) &&
                Objects.equals(opreator, that.opreator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, contNo, appntName, appntIDType, appntIDNo, mobile, relationship, insuredName, insuredIDType, insuredIDNo, prodSetCode, cValidate, opreator);
    }
}