package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;

public class LMRisk implements Serializable {
    /**
     * 险种编码
     *
     * @mbggenerated
     */
    private String RISKCODE;

    /**
     * 险种版本
     *
     * @mbggenerated
     */
    private String RISKVER;

    /**
     * 险种名称
     *
     * @mbggenerated
     */
    private String RISKNAME;

    /**
     * 险种简称
     *
     * @mbggenerated
     */
    private String RISKSHORTNAME;

    /**
     * 险种英文名称
     *
     * @mbggenerated
     */
    private String RISKENNAME;

    /**
     * 险种英文简称
     *
     * @mbggenerated
     */
    private String RISKENSHORTNAME;

    /**
     * 选择责任标记 Y--是 N--否
     *
     * @mbggenerated
     */
    private String CHODUTYFLAG;

    /**
     * 续期收费标记 Y--是 N--否
     *
     * @mbggenerated
     */
    private String CPAYFLAG;

    /**
     * 生存给付标记 Y--是 N--否
     *
     * @mbggenerated
     */
    private String GETFLAG;

    /**
     * 保全标记 Y--是、N--否
     *
     * @mbggenerated
     */
    private String EDORFLAG;

    /**
     * 续保标记 Y--是 N--否
     *
     * @mbggenerated
     */
    private String RNEWFLAG;

    /**
     * 核保标记 Y--是 N--否
     *
     * @mbggenerated
     */
    private String UWFLAG;

    /**
     * 分保标记 Y--是 N--否
     *
     * @mbggenerated
     */
    private String RINSFLAG;

    /**
     * 保险帐户标记 Y--是 N--否
     *
     * @mbggenerated
     */
    private String INSUACCFLAG;

    /**
     * 预定利率 该险种的预定利率,利差返还时用到
     *
     * @mbggenerated
     */
    private Float DESTRATE;

    /**
     * 原险种编码
     *
     * @mbggenerated
     */
    private String ORIGRISKCODE;

    /**
     * 子版本号
     *
     * @mbggenerated
     */
    private String SUBRISKVER;

    /**
     * 险种统计名称 统计时显示的名称
     *
     * @mbggenerated
     */
    private String RISKSTATNAME;

    private static final long serialVersionUID = 1L;

    public String getRISKCODE() {
        return RISKCODE;
    }

    public void setRISKCODE(String RISKCODE) {
        this.RISKCODE = RISKCODE;
    }

    public String getRISKVER() {
        return RISKVER;
    }

    public void setRISKVER(String RISKVER) {
        this.RISKVER = RISKVER;
    }

    public String getRISKNAME() {
        return RISKNAME;
    }

    public void setRISKNAME(String RISKNAME) {
        this.RISKNAME = RISKNAME;
    }

    public String getRISKSHORTNAME() {
        return RISKSHORTNAME;
    }

    public void setRISKSHORTNAME(String RISKSHORTNAME) {
        this.RISKSHORTNAME = RISKSHORTNAME;
    }

    public String getRISKENNAME() {
        return RISKENNAME;
    }

    public void setRISKENNAME(String RISKENNAME) {
        this.RISKENNAME = RISKENNAME;
    }

    public String getRISKENSHORTNAME() {
        return RISKENSHORTNAME;
    }

    public void setRISKENSHORTNAME(String RISKENSHORTNAME) {
        this.RISKENSHORTNAME = RISKENSHORTNAME;
    }

    public String getCHODUTYFLAG() {
        return CHODUTYFLAG;
    }

    public void setCHODUTYFLAG(String CHODUTYFLAG) {
        this.CHODUTYFLAG = CHODUTYFLAG;
    }

    public String getCPAYFLAG() {
        return CPAYFLAG;
    }

    public void setCPAYFLAG(String CPAYFLAG) {
        this.CPAYFLAG = CPAYFLAG;
    }

    public String getGETFLAG() {
        return GETFLAG;
    }

    public void setGETFLAG(String GETFLAG) {
        this.GETFLAG = GETFLAG;
    }

    public String getEDORFLAG() {
        return EDORFLAG;
    }

    public void setEDORFLAG(String EDORFLAG) {
        this.EDORFLAG = EDORFLAG;
    }

    public String getRNEWFLAG() {
        return RNEWFLAG;
    }

    public void setRNEWFLAG(String RNEWFLAG) {
        this.RNEWFLAG = RNEWFLAG;
    }

    public String getUWFLAG() {
        return UWFLAG;
    }

    public void setUWFLAG(String UWFLAG) {
        this.UWFLAG = UWFLAG;
    }

    public String getRINSFLAG() {
        return RINSFLAG;
    }

    public void setRINSFLAG(String RINSFLAG) {
        this.RINSFLAG = RINSFLAG;
    }

    public String getINSUACCFLAG() {
        return INSUACCFLAG;
    }

    public void setINSUACCFLAG(String INSUACCFLAG) {
        this.INSUACCFLAG = INSUACCFLAG;
    }

    public Float getDESTRATE() {
        return DESTRATE;
    }

    public void setDESTRATE(Float DESTRATE) {
        this.DESTRATE = DESTRATE;
    }

    public String getORIGRISKCODE() {
        return ORIGRISKCODE;
    }

    public void setORIGRISKCODE(String ORIGRISKCODE) {
        this.ORIGRISKCODE = ORIGRISKCODE;
    }

    public String getSUBRISKVER() {
        return SUBRISKVER;
    }

    public void setSUBRISKVER(String SUBRISKVER) {
        this.SUBRISKVER = SUBRISKVER;
    }

    public String getRISKSTATNAME() {
        return RISKSTATNAME;
    }

    public void setRISKSTATNAME(String RISKSTATNAME) {
        this.RISKSTATNAME = RISKSTATNAME;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", RISKCODE=").append(RISKCODE);
        sb.append(", RISKVER=").append(RISKVER);
        sb.append(", RISKNAME=").append(RISKNAME);
        sb.append(", RISKSHORTNAME=").append(RISKSHORTNAME);
        sb.append(", RISKENNAME=").append(RISKENNAME);
        sb.append(", RISKENSHORTNAME=").append(RISKENSHORTNAME);
        sb.append(", CHODUTYFLAG=").append(CHODUTYFLAG);
        sb.append(", CPAYFLAG=").append(CPAYFLAG);
        sb.append(", GETFLAG=").append(GETFLAG);
        sb.append(", EDORFLAG=").append(EDORFLAG);
        sb.append(", RNEWFLAG=").append(RNEWFLAG);
        sb.append(", UWFLAG=").append(UWFLAG);
        sb.append(", RINSFLAG=").append(RINSFLAG);
        sb.append(", INSUACCFLAG=").append(INSUACCFLAG);
        sb.append(", DESTRATE=").append(DESTRATE);
        sb.append(", ORIGRISKCODE=").append(ORIGRISKCODE);
        sb.append(", SUBRISKVER=").append(SUBRISKVER);
        sb.append(", RISKSTATNAME=").append(RISKSTATNAME);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LMRisk other = (LMRisk) that;
        return (this.getRISKCODE() == null ? other.getRISKCODE() == null : this.getRISKCODE().equals(other.getRISKCODE()))
            && (this.getRISKVER() == null ? other.getRISKVER() == null : this.getRISKVER().equals(other.getRISKVER()))
            && (this.getRISKNAME() == null ? other.getRISKNAME() == null : this.getRISKNAME().equals(other.getRISKNAME()))
            && (this.getRISKSHORTNAME() == null ? other.getRISKSHORTNAME() == null : this.getRISKSHORTNAME().equals(other.getRISKSHORTNAME()))
            && (this.getRISKENNAME() == null ? other.getRISKENNAME() == null : this.getRISKENNAME().equals(other.getRISKENNAME()))
            && (this.getRISKENSHORTNAME() == null ? other.getRISKENSHORTNAME() == null : this.getRISKENSHORTNAME().equals(other.getRISKENSHORTNAME()))
            && (this.getCHODUTYFLAG() == null ? other.getCHODUTYFLAG() == null : this.getCHODUTYFLAG().equals(other.getCHODUTYFLAG()))
            && (this.getCPAYFLAG() == null ? other.getCPAYFLAG() == null : this.getCPAYFLAG().equals(other.getCPAYFLAG()))
            && (this.getGETFLAG() == null ? other.getGETFLAG() == null : this.getGETFLAG().equals(other.getGETFLAG()))
            && (this.getEDORFLAG() == null ? other.getEDORFLAG() == null : this.getEDORFLAG().equals(other.getEDORFLAG()))
            && (this.getRNEWFLAG() == null ? other.getRNEWFLAG() == null : this.getRNEWFLAG().equals(other.getRNEWFLAG()))
            && (this.getUWFLAG() == null ? other.getUWFLAG() == null : this.getUWFLAG().equals(other.getUWFLAG()))
            && (this.getRINSFLAG() == null ? other.getRINSFLAG() == null : this.getRINSFLAG().equals(other.getRINSFLAG()))
            && (this.getINSUACCFLAG() == null ? other.getINSUACCFLAG() == null : this.getINSUACCFLAG().equals(other.getINSUACCFLAG()))
            && (this.getDESTRATE() == null ? other.getDESTRATE() == null : this.getDESTRATE().equals(other.getDESTRATE()))
            && (this.getORIGRISKCODE() == null ? other.getORIGRISKCODE() == null : this.getORIGRISKCODE().equals(other.getORIGRISKCODE()))
            && (this.getSUBRISKVER() == null ? other.getSUBRISKVER() == null : this.getSUBRISKVER().equals(other.getSUBRISKVER()))
            && (this.getRISKSTATNAME() == null ? other.getRISKSTATNAME() == null : this.getRISKSTATNAME().equals(other.getRISKSTATNAME()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRISKCODE() == null) ? 0 : getRISKCODE().hashCode());
        result = prime * result + ((getRISKVER() == null) ? 0 : getRISKVER().hashCode());
        result = prime * result + ((getRISKNAME() == null) ? 0 : getRISKNAME().hashCode());
        result = prime * result + ((getRISKSHORTNAME() == null) ? 0 : getRISKSHORTNAME().hashCode());
        result = prime * result + ((getRISKENNAME() == null) ? 0 : getRISKENNAME().hashCode());
        result = prime * result + ((getRISKENSHORTNAME() == null) ? 0 : getRISKENSHORTNAME().hashCode());
        result = prime * result + ((getCHODUTYFLAG() == null) ? 0 : getCHODUTYFLAG().hashCode());
        result = prime * result + ((getCPAYFLAG() == null) ? 0 : getCPAYFLAG().hashCode());
        result = prime * result + ((getGETFLAG() == null) ? 0 : getGETFLAG().hashCode());
        result = prime * result + ((getEDORFLAG() == null) ? 0 : getEDORFLAG().hashCode());
        result = prime * result + ((getRNEWFLAG() == null) ? 0 : getRNEWFLAG().hashCode());
        result = prime * result + ((getUWFLAG() == null) ? 0 : getUWFLAG().hashCode());
        result = prime * result + ((getRINSFLAG() == null) ? 0 : getRINSFLAG().hashCode());
        result = prime * result + ((getINSUACCFLAG() == null) ? 0 : getINSUACCFLAG().hashCode());
        result = prime * result + ((getDESTRATE() == null) ? 0 : getDESTRATE().hashCode());
        result = prime * result + ((getORIGRISKCODE() == null) ? 0 : getORIGRISKCODE().hashCode());
        result = prime * result + ((getSUBRISKVER() == null) ? 0 : getSUBRISKVER().hashCode());
        result = prime * result + ((getRISKSTATNAME() == null) ? 0 : getRISKSTATNAME().hashCode());
        return result;
    }
}