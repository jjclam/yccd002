package com.sinosoft.cloud.dal.yfbDao.mapper;


import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.entity.Laagent;
import org.apache.ibatis.annotations.Select;

import java.util.List;

//import com.sinosoft.cloud.dal.yfbDao.model.entity.PremSumInfo;

public interface LianLifeMapper {


    @Select("select distinct agentcom|| '-' ||name as name from  LACOM")
    public List<Lacom> selectwangBitSelect();

    @Select("select comcode|| '-' ||name as name from ldcom")
    public List<Lacom> selectManAgeComSelect();

    @Select("select distinct agentcom name from  LACOM")
    public List<Lacom> selectManagecom();
    @Select("select agentcode|| '-' ||name as name from laagent")
    List<Laagent> selectAgentSelect();
}