package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;

public class LDCode1 implements Serializable {


    private String id;

    private String codeType;

    private String codeTypeName;

    private String code;

    private String code1;

    private String codeName;

    private String codeAlias;

    private String comCode;

    private String otherSign;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getCodeTypeName() {
        return codeTypeName;
    }

    public void setCodeTypeName(String codeTypeName) {
        this.codeTypeName = codeTypeName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode1() {
        return code1;
    }

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCodeAlias() {
        return codeAlias;
    }

    public void setCodeAlias(String codeAlias) {
        this.codeAlias = codeAlias;
    }

    public String getComCode() {
        return comCode;
    }

    public void setComCode(String comCode) {
        this.comCode = comCode;
    }

    public String getOtherSign() {
        return otherSign;
    }

    public void setOtherSign(String otherSign) {
        this.otherSign = otherSign;
    }

    @Override
    public String toString() {
        return "LDCode{" +
                "id='" + id + '\'' +
                ", codeType='" + codeType + '\'' +
                ", codeTypeName='" + codeTypeName + '\'' +
                ", code='" + code + '\'' +
                ", code1='" + code1 + '\'' +
                ", codeName='" + codeName + '\'' +
                ", codeAlias='" + codeAlias + '\'' +
                ", comCode='" + comCode + '\'' +
                ", otherSign='" + otherSign + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LDCode1 ldCode = (LDCode1) o;

        if (id != null ? !id.equals(ldCode.id) : ldCode.id != null) return false;
        if (codeType != null ? !codeType.equals(ldCode.codeType) : ldCode.codeType != null) return false;
        if (codeTypeName != null ? !codeTypeName.equals(ldCode.codeTypeName) : ldCode.codeTypeName != null)
            return false;
        if (code != null ? !code.equals(ldCode.code) : ldCode.code != null) return false;
        if (code1 != null ? !code1.equals(ldCode.code1) : ldCode.code1 != null) return false;
        if (codeName != null ? !codeName.equals(ldCode.codeName) : ldCode.codeName != null) return false;
        if (codeAlias != null ? !codeAlias.equals(ldCode.codeAlias) : ldCode.codeAlias != null) return false;
        if (comCode != null ? !comCode.equals(ldCode.comCode) : ldCode.comCode != null) return false;
        return otherSign != null ? otherSign.equals(ldCode.otherSign) : ldCode.otherSign == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (codeType != null ? codeType.hashCode() : 0);
        result = 31 * result + (codeTypeName != null ? codeTypeName.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (code1 != null ? code1.hashCode() : 0);
        result = 31 * result + (codeName != null ? codeName.hashCode() : 0);
        result = 31 * result + (codeAlias != null ? codeAlias.hashCode() : 0);
        result = 31 * result + (comCode != null ? comCode.hashCode() : 0);
        result = 31 * result + (otherSign != null ? otherSign.hashCode() : 0);
        return result;
    }
}