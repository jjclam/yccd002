package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;

public class LDPlan implements Serializable {
    /**
     * 保险计划编码
     *
     * @mbggenerated
     */
    private String CONTPLANCODE;

    /**
     * 计划类别
     *
     * @mbggenerated
     */
    private String CONTPLANNAME;

    /**
     * 保险计划名称
     *
     * @mbggenerated
     */
    private String PLANTYPE;

    /**
     * 计划规则
     *
     * @mbggenerated
     */
    private String PLANRULE;

    /**
     * 计划规则sql
     *
     * @mbggenerated
     */
    private String PLANSQL;

    /**
     * 备注
     *
     * @mbggenerated
     */
    private String REMARK;

    /**
     * 操作员
     *
     * @mbggenerated
     */
    private String OPERATOR;

    /**
     * 入机日期
     *
     * @mbggenerated
     */
    private Date MAKEDATE;

    /**
     * 入机时间
     *
     * @mbggenerated
     */
    private String MAKETIME;

    /**
     * 最后一次修改日期
     *
     * @mbggenerated
     */
    private Date MODIFYDATE;

    /**
     * 最后一次修改时间
     *
     * @mbggenerated
     */
    private String MODIFYTIME;

    /**
     * 可投保人数
     *
     * @mbggenerated
     */
    private String PEOPLES3;

    /**
     * 备注2
     *
     * @mbggenerated
     */
    private String REMARK2;

    /**
     * 管理机构
     *
     * @mbggenerated
     */
    private String MANAGECOM;

    /**
     * 销售渠道
     *
     * @mbggenerated
     */
    private String SALECHNL;

    /**
     * 销售起期
     *
     * @mbggenerated
     */
    private Date STARTDATE;

    /**
     * 销售止期
     *
     * @mbggenerated
     */
    private Date ENDDATE;

    /**
     * 状态
     *
     * @mbggenerated
     */
    private String STATE;

    /**
     * 计划层级1
     *
     * @mbggenerated
     */
    private String PLANKIND1;

    /**
     * 计划层级2
     *
     * @mbggenerated
     */
    private String PLANKIND2;

    /**
     * 计划层级3
     *
     * @mbggenerated
     */
    private String PLANKIND3;

    /**
     * 产品组合标记
     *
     * @mbggenerated
     */
    private String PORTFOLIOFLAG;

    /**
     * 是否为标准产品
     *
     * @mbggenerated
     */
    private String STANDARDFLAG;

    /**
     * 产品上下架状态
     *
     * @mbggenerated
     */
    private String UPDOWNSTATUS;

    private static final long serialVersionUID = 1L;

    public String getCONTPLANCODE() {
        return CONTPLANCODE;
    }

    public void setCONTPLANCODE(String CONTPLANCODE) {
        this.CONTPLANCODE = CONTPLANCODE;
    }

    public String getCONTPLANNAME() {
        return CONTPLANNAME;
    }

    public void setCONTPLANNAME(String CONTPLANNAME) {
        this.CONTPLANNAME = CONTPLANNAME;
    }

    public String getPLANTYPE() {
        return PLANTYPE;
    }

    public void setPLANTYPE(String PLANTYPE) {
        this.PLANTYPE = PLANTYPE;
    }

    public String getPLANRULE() {
        return PLANRULE;
    }

    public void setPLANRULE(String PLANRULE) {
        this.PLANRULE = PLANRULE;
    }

    public String getPLANSQL() {
        return PLANSQL;
    }

    public void setPLANSQL(String PLANSQL) {
        this.PLANSQL = PLANSQL;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public String getOPERATOR() {
        return OPERATOR;
    }

    public void setOPERATOR(String OPERATOR) {
        this.OPERATOR = OPERATOR;
    }

    public Date getMAKEDATE() {
        return MAKEDATE;
    }

    public void setMAKEDATE(Date MAKEDATE) {
        this.MAKEDATE = MAKEDATE;
    }

    public String getMAKETIME() {
        return MAKETIME;
    }

    public void setMAKETIME(String MAKETIME) {
        this.MAKETIME = MAKETIME;
    }

    public Date getMODIFYDATE() {
        return MODIFYDATE;
    }

    public void setMODIFYDATE(Date MODIFYDATE) {
        this.MODIFYDATE = MODIFYDATE;
    }

    public String getMODIFYTIME() {
        return MODIFYTIME;
    }

    public void setMODIFYTIME(String MODIFYTIME) {
        this.MODIFYTIME = MODIFYTIME;
    }

    public String getPEOPLES3() {
        return PEOPLES3;
    }

    public void setPEOPLES3(String PEOPLES3) {
        this.PEOPLES3 = PEOPLES3;
    }

    public String getREMARK2() {
        return REMARK2;
    }

    public void setREMARK2(String REMARK2) {
        this.REMARK2 = REMARK2;
    }

    public String getMANAGECOM() {
        return MANAGECOM;
    }

    public void setMANAGECOM(String MANAGECOM) {
        this.MANAGECOM = MANAGECOM;
    }

    public String getSALECHNL() {
        return SALECHNL;
    }

    public void setSALECHNL(String SALECHNL) {
        this.SALECHNL = SALECHNL;
    }

    public Date getSTARTDATE() {
        return STARTDATE;
    }

    public void setSTARTDATE(Date STARTDATE) {
        this.STARTDATE = STARTDATE;
    }

    public Date getENDDATE() {
        return ENDDATE;
    }

    public void setENDDATE(Date ENDDATE) {
        this.ENDDATE = ENDDATE;
    }

    public String getSTATE() {
        return STATE;
    }

    public void setSTATE(String STATE) {
        this.STATE = STATE;
    }

    public String getPLANKIND1() {
        return PLANKIND1;
    }

    public void setPLANKIND1(String PLANKIND1) {
        this.PLANKIND1 = PLANKIND1;
    }

    public String getPLANKIND2() {
        return PLANKIND2;
    }

    public void setPLANKIND2(String PLANKIND2) {
        this.PLANKIND2 = PLANKIND2;
    }

    public String getPLANKIND3() {
        return PLANKIND3;
    }

    public void setPLANKIND3(String PLANKIND3) {
        this.PLANKIND3 = PLANKIND3;
    }

    public String getPORTFOLIOFLAG() {
        return PORTFOLIOFLAG;
    }

    public void setPORTFOLIOFLAG(String PORTFOLIOFLAG) {
        this.PORTFOLIOFLAG = PORTFOLIOFLAG;
    }

    public String getSTANDARDFLAG() {
        return STANDARDFLAG;
    }

    public void setSTANDARDFLAG(String STANDARDFLAG) {
        this.STANDARDFLAG = STANDARDFLAG;
    }

    public String getUPDOWNSTATUS() {
        return UPDOWNSTATUS;
    }

    public void setUPDOWNSTATUS(String UPDOWNSTATUS) {
        this.UPDOWNSTATUS = UPDOWNSTATUS;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", CONTPLANCODE=").append(CONTPLANCODE);
        sb.append(", CONTPLANNAME=").append(CONTPLANNAME);
        sb.append(", PLANTYPE=").append(PLANTYPE);
        sb.append(", PLANRULE=").append(PLANRULE);
        sb.append(", PLANSQL=").append(PLANSQL);
        sb.append(", REMARK=").append(REMARK);
        sb.append(", OPERATOR=").append(OPERATOR);
        sb.append(", MAKEDATE=").append(MAKEDATE);
        sb.append(", MAKETIME=").append(MAKETIME);
        sb.append(", MODIFYDATE=").append(MODIFYDATE);
        sb.append(", MODIFYTIME=").append(MODIFYTIME);
        sb.append(", PEOPLES3=").append(PEOPLES3);
        sb.append(", REMARK2=").append(REMARK2);
        sb.append(", MANAGECOM=").append(MANAGECOM);
        sb.append(", SALECHNL=").append(SALECHNL);
        sb.append(", STARTDATE=").append(STARTDATE);
        sb.append(", ENDDATE=").append(ENDDATE);
        sb.append(", STATE=").append(STATE);
        sb.append(", PLANKIND1=").append(PLANKIND1);
        sb.append(", PLANKIND2=").append(PLANKIND2);
        sb.append(", PLANKIND3=").append(PLANKIND3);
        sb.append(", PORTFOLIOFLAG=").append(PORTFOLIOFLAG);
        sb.append(", STANDARDFLAG=").append(STANDARDFLAG);
        sb.append(", UPDOWNSTATUS=").append(UPDOWNSTATUS);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LDPlan other = (LDPlan) that;
        return (this.getCONTPLANCODE() == null ? other.getCONTPLANCODE() == null : this.getCONTPLANCODE().equals(other.getCONTPLANCODE()))
            && (this.getCONTPLANNAME() == null ? other.getCONTPLANNAME() == null : this.getCONTPLANNAME().equals(other.getCONTPLANNAME()))
            && (this.getPLANTYPE() == null ? other.getPLANTYPE() == null : this.getPLANTYPE().equals(other.getPLANTYPE()))
            && (this.getPLANRULE() == null ? other.getPLANRULE() == null : this.getPLANRULE().equals(other.getPLANRULE()))
            && (this.getPLANSQL() == null ? other.getPLANSQL() == null : this.getPLANSQL().equals(other.getPLANSQL()))
            && (this.getREMARK() == null ? other.getREMARK() == null : this.getREMARK().equals(other.getREMARK()))
            && (this.getOPERATOR() == null ? other.getOPERATOR() == null : this.getOPERATOR().equals(other.getOPERATOR()))
            && (this.getMAKEDATE() == null ? other.getMAKEDATE() == null : this.getMAKEDATE().equals(other.getMAKEDATE()))
            && (this.getMAKETIME() == null ? other.getMAKETIME() == null : this.getMAKETIME().equals(other.getMAKETIME()))
            && (this.getMODIFYDATE() == null ? other.getMODIFYDATE() == null : this.getMODIFYDATE().equals(other.getMODIFYDATE()))
            && (this.getMODIFYTIME() == null ? other.getMODIFYTIME() == null : this.getMODIFYTIME().equals(other.getMODIFYTIME()))
            && (this.getPEOPLES3() == null ? other.getPEOPLES3() == null : this.getPEOPLES3().equals(other.getPEOPLES3()))
            && (this.getREMARK2() == null ? other.getREMARK2() == null : this.getREMARK2().equals(other.getREMARK2()))
            && (this.getMANAGECOM() == null ? other.getMANAGECOM() == null : this.getMANAGECOM().equals(other.getMANAGECOM()))
            && (this.getSALECHNL() == null ? other.getSALECHNL() == null : this.getSALECHNL().equals(other.getSALECHNL()))
            && (this.getSTARTDATE() == null ? other.getSTARTDATE() == null : this.getSTARTDATE().equals(other.getSTARTDATE()))
            && (this.getENDDATE() == null ? other.getENDDATE() == null : this.getENDDATE().equals(other.getENDDATE()))
            && (this.getSTATE() == null ? other.getSTATE() == null : this.getSTATE().equals(other.getSTATE()))
            && (this.getPLANKIND1() == null ? other.getPLANKIND1() == null : this.getPLANKIND1().equals(other.getPLANKIND1()))
            && (this.getPLANKIND2() == null ? other.getPLANKIND2() == null : this.getPLANKIND2().equals(other.getPLANKIND2()))
            && (this.getPLANKIND3() == null ? other.getPLANKIND3() == null : this.getPLANKIND3().equals(other.getPLANKIND3()))
            && (this.getPORTFOLIOFLAG() == null ? other.getPORTFOLIOFLAG() == null : this.getPORTFOLIOFLAG().equals(other.getPORTFOLIOFLAG()))
            && (this.getSTANDARDFLAG() == null ? other.getSTANDARDFLAG() == null : this.getSTANDARDFLAG().equals(other.getSTANDARDFLAG()))
            && (this.getUPDOWNSTATUS() == null ? other.getUPDOWNSTATUS() == null : this.getUPDOWNSTATUS().equals(other.getUPDOWNSTATUS()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCONTPLANCODE() == null) ? 0 : getCONTPLANCODE().hashCode());
        result = prime * result + ((getCONTPLANNAME() == null) ? 0 : getCONTPLANNAME().hashCode());
        result = prime * result + ((getPLANTYPE() == null) ? 0 : getPLANTYPE().hashCode());
        result = prime * result + ((getPLANRULE() == null) ? 0 : getPLANRULE().hashCode());
        result = prime * result + ((getPLANSQL() == null) ? 0 : getPLANSQL().hashCode());
        result = prime * result + ((getREMARK() == null) ? 0 : getREMARK().hashCode());
        result = prime * result + ((getOPERATOR() == null) ? 0 : getOPERATOR().hashCode());
        result = prime * result + ((getMAKEDATE() == null) ? 0 : getMAKEDATE().hashCode());
        result = prime * result + ((getMAKETIME() == null) ? 0 : getMAKETIME().hashCode());
        result = prime * result + ((getMODIFYDATE() == null) ? 0 : getMODIFYDATE().hashCode());
        result = prime * result + ((getMODIFYTIME() == null) ? 0 : getMODIFYTIME().hashCode());
        result = prime * result + ((getPEOPLES3() == null) ? 0 : getPEOPLES3().hashCode());
        result = prime * result + ((getREMARK2() == null) ? 0 : getREMARK2().hashCode());
        result = prime * result + ((getMANAGECOM() == null) ? 0 : getMANAGECOM().hashCode());
        result = prime * result + ((getSALECHNL() == null) ? 0 : getSALECHNL().hashCode());
        result = prime * result + ((getSTARTDATE() == null) ? 0 : getSTARTDATE().hashCode());
        result = prime * result + ((getENDDATE() == null) ? 0 : getENDDATE().hashCode());
        result = prime * result + ((getSTATE() == null) ? 0 : getSTATE().hashCode());
        result = prime * result + ((getPLANKIND1() == null) ? 0 : getPLANKIND1().hashCode());
        result = prime * result + ((getPLANKIND2() == null) ? 0 : getPLANKIND2().hashCode());
        result = prime * result + ((getPLANKIND3() == null) ? 0 : getPLANKIND3().hashCode());
        result = prime * result + ((getPORTFOLIOFLAG() == null) ? 0 : getPORTFOLIOFLAG().hashCode());
        result = prime * result + ((getSTANDARDFLAG() == null) ? 0 : getSTANDARDFLAG().hashCode());
        result = prime * result + ((getUPDOWNSTATUS() == null) ? 0 : getUPDOWNSTATUS().hashCode());
        return result;
    }
}