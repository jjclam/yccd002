package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;

public class LDDevices implements Serializable {
    private String serialNo;

    private String deviceNo;

    private String address;

    private String useFlag;

    private String mangeCom;

    private String regDate;

    private String manageComName;

    private String agentCom;

    public LDDevices() {
    }

    public LDDevices(String serialNo, String deviceNo, String address, String useFlag, String mangeCom, String regDate, String manageComName, String agentCom) {
        this.serialNo = serialNo;
        this.deviceNo = deviceNo;
        this.address = address;
        this.useFlag = useFlag;
        this.mangeCom = mangeCom;
        this.regDate = regDate;
        this.manageComName = manageComName;
        this.agentCom = agentCom;
    }

    @Override
    public String toString() {
        return "LDDevices{" +
                "serialNo='" + serialNo + '\'' +
                ", deviceNo='" + deviceNo + '\'' +
                ", address='" + address + '\'' +
                ", useFlag='" + useFlag + '\'' +
                ", mangeCom='" + mangeCom + '\'' +
                ", regDate='" + regDate + '\'' +
                ", manageComName='" + manageComName + '\'' +
                ", agentCom='" + agentCom + '\'' +
                '}';
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUseFlag() {
        return useFlag;
    }

    public void setUseFlag(String useFlag) {
        this.useFlag = useFlag;
    }

    public String getMangeCom() {
        return mangeCom;
    }

    public void setMangeCom(String mangeCom) {
        this.mangeCom = mangeCom;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getManageComName() {
        return manageComName;
    }

    public void setManageComName(String manageComName) {
        this.manageComName = manageComName;
    }

    public String getAgentCom() {
        return agentCom;
    }

    public void setAgentCom(String agentCom) {
        this.agentCom = agentCom;
    }
}