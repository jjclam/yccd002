package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;

public class AgentInfo implements Serializable {
    /**
     * 序列
     */
    private String proId;

    /**
     * 中介机构代码
     */
    private String agencyCode;

    /**
     * 中介机构名称
     */
    private String agencyName;

    /**
     * 中介机构类别
     */
    private String agencyType;

    /**
     * 保险许可证号码
     */
    private String licenseNo;

    /**
     * 机构注册地址
     */
    private String address;

    /**
     * 联系人
     */
    private String linkMan;

    /**
     * 机构联系电话
     */
    private String phone;

    /**
     * 成立日期
     */
    private String foundDate;

    /**
     * 停业标志
     */
    private String endFlag;

    /**
     * 停业日期
     *
     */
    private String endDate;

    /**
     * 渠道编码
     */
    private String channelCode;

    /**
     * 渠道名称
     */
    private String channelName;

    /**
     * 代理人编号
     */
    private String agentCode;

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAgencyType() {
        return agencyType;
    }

    public void setAgencyType(String agencyType) {
        this.agencyType = agencyType;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(String foundDate) {
        this.foundDate = foundDate;
    }

    public String getEndFlag() {
        return endFlag;
    }

    public void setEndFlag(String endFlag) {
        this.endFlag = endFlag;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    @Override
    public String toString() {
        return "AgentInfo{" +
                "proId='" + proId + '\'' +
                ", agencyCode='" + agencyCode + '\'' +
                ", agencyName='" + agencyName + '\'' +
                ", agencyType='" + agencyType + '\'' +
                ", licenseNo='" + licenseNo + '\'' +
                ", address='" + address + '\'' +
                ", linkMan='" + linkMan + '\'' +
                ", phone='" + phone + '\'' +
                ", foundDate=" + foundDate +
                ", endFlag=" + endFlag +
                ", endDate=" + endDate +
                ", channelCode='" + channelCode + '\'' +
                ", channelName='" + channelName + '\'' +
                ", agentCode='" + agentCode + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgentInfo agentInfo = (AgentInfo) o;

        if (proId != null ? !proId.equals(agentInfo.proId) : agentInfo.proId != null) return false;
        if (agencyCode != null ? !agencyCode.equals(agentInfo.agencyCode) : agentInfo.agencyCode != null) return false;
        if (agencyName != null ? !agencyName.equals(agentInfo.agencyName) : agentInfo.agencyName != null) return false;
        if (agencyType != null ? !agencyType.equals(agentInfo.agencyType) : agentInfo.agencyType != null) return false;
        if (licenseNo != null ? !licenseNo.equals(agentInfo.licenseNo) : agentInfo.licenseNo != null) return false;
        if (address != null ? !address.equals(agentInfo.address) : agentInfo.address != null) return false;
        if (linkMan != null ? !linkMan.equals(agentInfo.linkMan) : agentInfo.linkMan != null) return false;
        if (phone != null ? !phone.equals(agentInfo.phone) : agentInfo.phone != null) return false;
        if (foundDate != null ? !foundDate.equals(agentInfo.foundDate) : agentInfo.foundDate != null) return false;
        if (endFlag != null ? !endFlag.equals(agentInfo.endFlag) : agentInfo.endFlag != null) return false;
        if (endDate != null ? !endDate.equals(agentInfo.endDate) : agentInfo.endDate != null) return false;
        if (channelCode != null ? !channelCode.equals(agentInfo.channelCode) : agentInfo.channelCode != null)
            return false;
        if (channelName != null ? !channelName.equals(agentInfo.channelName) : agentInfo.channelName != null)
            return false;
        return agentCode != null ? agentCode.equals(agentInfo.agentCode) : agentInfo.agentCode == null;
    }

    @Override
    public int hashCode() {
        int result = proId != null ? proId.hashCode() : 0;
        result = 31 * result + (agencyCode != null ? agencyCode.hashCode() : 0);
        result = 31 * result + (agencyName != null ? agencyName.hashCode() : 0);
        result = 31 * result + (agencyType != null ? agencyType.hashCode() : 0);
        result = 31 * result + (licenseNo != null ? licenseNo.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (linkMan != null ? linkMan.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (foundDate != null ? foundDate.hashCode() : 0);
        result = 31 * result + (endFlag != null ? endFlag.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (channelCode != null ? channelCode.hashCode() : 0);
        result = 31 * result + (channelName != null ? channelName.hashCode() : 0);
        result = 31 * result + (agentCode != null ? agentCode.hashCode() : 0);
        return result;
    }
}
