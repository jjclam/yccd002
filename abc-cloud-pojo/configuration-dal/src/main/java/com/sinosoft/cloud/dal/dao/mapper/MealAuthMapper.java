package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.LDPlan;

import java.util.List;
import java.util.Map;

public interface MealAuthMapper {
    //查询日志信息
    List<LDPlan> selectByTaskName(Map<String, Object> map);
    //查询任务编码下拉
    List<LDPlan> getCodes();
    //查询记录总条数
    Integer countForList(Map<String, Object> map);
    //查询机构编码下拉
    List<LACom> getMngCode();
}
