package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;

public class LMCalModePojo implements Serializable {
    private String CalCode;
    /** 险种编码 */
    private String RiskCode;
    /** 算法类型 */
    private String Type;
    /** 算法内容 */
    private String CalSQL;
    /** 算法描述 */
    private String Remark;
    /** 算法版本 */
    private int Version;

    public String getCalCode() {
        return CalCode;
    }

    public void setCalCode(String calCode) {
        CalCode = calCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String riskCode) {
        RiskCode = riskCode;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getCalSQL() {
        return CalSQL;
    }

    public void setCalSQL(String calSQL) {
        CalSQL = calSQL;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public int getVersion() {
        return Version;
    }

    public void setVersion(int version) {
        Version = version;
    }

    @Override
    public String toString() {
        return "LMCalModePojo{" +
                "CalCode='" + CalCode + '\'' +
                ", RiskCode='" + RiskCode + '\'' +
                ", Type='" + Type + '\'' +
                ", CalSQL='" + CalSQL + '\'' +
                ", Remark='" + Remark + '\'' +
                ", Version=" + Version +
                '}';
    }
}
