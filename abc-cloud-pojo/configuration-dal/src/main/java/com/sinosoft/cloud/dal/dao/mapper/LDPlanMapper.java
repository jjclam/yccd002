package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LDPlan;
import com.sinosoft.cloud.dal.dao.model.LDPlanExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LDPlanMapper {
    int countByExample(LDPlanExample example);

    int deleteByExample(LDPlanExample example);

    int deleteByPrimaryKey(String CONTPLANCODE);

    int insert(LDPlan record);

    int insertSelective(LDPlan record);

    List<LDPlan> selectByExample(LDPlanExample example);

    LDPlan selectByPrimaryKey(String CONTPLANCODE);

    int updateByExampleSelective(@Param("record") LDPlan record, @Param("example") LDPlanExample example);

    int updateByExample(@Param("record") LDPlan record, @Param("example") LDPlanExample example);

    int updateByPrimaryKeySelective(LDPlan record);

    int updateByPrimaryKey(LDPlan record);
}