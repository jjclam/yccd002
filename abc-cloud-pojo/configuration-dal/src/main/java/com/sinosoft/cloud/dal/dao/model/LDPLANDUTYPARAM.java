package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;

public class LDPLANDUTYPARAM implements Serializable {
    private String MAINRISKCODE;

    private String MAINRISKVERSION;

    private String RISKCODE;

    private String RISKVERSION;

    private String CONTPLANCODE;

    private String CONTPLANNAME;

    private String DUTYCODE;

    private String CALFACTOR;

    private String CALFACTORTYPE;

    private String CALFACTORVALUE;

    private String REMARK;

    private String PLANTYPE;

    private String PORTFOLIOFLAG;

    private String FIELDCODE;

    private static final long serialVersionUID = 1L;

    public String getMAINRISKCODE() {
        return MAINRISKCODE;
    }

    public void setMAINRISKCODE(String MAINRISKCODE) {
        this.MAINRISKCODE = MAINRISKCODE;
    }

    public String getMAINRISKVERSION() {
        return MAINRISKVERSION;
    }

    public void setMAINRISKVERSION(String MAINRISKVERSION) {
        this.MAINRISKVERSION = MAINRISKVERSION;
    }

    public String getRISKCODE() {
        return RISKCODE;
    }

    public void setRISKCODE(String RISKCODE) {
        this.RISKCODE = RISKCODE;
    }

    public String getRISKVERSION() {
        return RISKVERSION;
    }

    public void setRISKVERSION(String RISKVERSION) {
        this.RISKVERSION = RISKVERSION;
    }

    public String getCONTPLANCODE() {
        return CONTPLANCODE;
    }

    public void setCONTPLANCODE(String CONTPLANCODE) {
        this.CONTPLANCODE = CONTPLANCODE;
    }

    public String getCONTPLANNAME() {
        return CONTPLANNAME;
    }

    public void setCONTPLANNAME(String CONTPLANNAME) {
        this.CONTPLANNAME = CONTPLANNAME;
    }

    public String getDUTYCODE() {
        return DUTYCODE;
    }

    public void setDUTYCODE(String DUTYCODE) {
        this.DUTYCODE = DUTYCODE;
    }

    public String getCALFACTOR() {
        return CALFACTOR;
    }

    public void setCALFACTOR(String CALFACTOR) {
        this.CALFACTOR = CALFACTOR;
    }

    public String getCALFACTORTYPE() {
        return CALFACTORTYPE;
    }

    public void setCALFACTORTYPE(String CALFACTORTYPE) {
        this.CALFACTORTYPE = CALFACTORTYPE;
    }

    public String getCALFACTORVALUE() {
        return CALFACTORVALUE;
    }

    public void setCALFACTORVALUE(String CALFACTORVALUE) {
        this.CALFACTORVALUE = CALFACTORVALUE;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public String getPLANTYPE() {
        return PLANTYPE;
    }

    public void setPLANTYPE(String PLANTYPE) {
        this.PLANTYPE = PLANTYPE;
    }

    public String getPORTFOLIOFLAG() {
        return PORTFOLIOFLAG;
    }

    public void setPORTFOLIOFLAG(String PORTFOLIOFLAG) {
        this.PORTFOLIOFLAG = PORTFOLIOFLAG;
    }

    public String getFIELDCODE() {
        return FIELDCODE;
    }

    public void setFIELDCODE(String FIELDCODE) {
        this.FIELDCODE = FIELDCODE;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", MAINRISKCODE=").append(MAINRISKCODE);
        sb.append(", MAINRISKVERSION=").append(MAINRISKVERSION);
        sb.append(", RISKCODE=").append(RISKCODE);
        sb.append(", RISKVERSION=").append(RISKVERSION);
        sb.append(", CONTPLANCODE=").append(CONTPLANCODE);
        sb.append(", CONTPLANNAME=").append(CONTPLANNAME);
        sb.append(", DUTYCODE=").append(DUTYCODE);
        sb.append(", CALFACTOR=").append(CALFACTOR);
        sb.append(", CALFACTORTYPE=").append(CALFACTORTYPE);
        sb.append(", CALFACTORVALUE=").append(CALFACTORVALUE);
        sb.append(", REMARK=").append(REMARK);
        sb.append(", PLANTYPE=").append(PLANTYPE);
        sb.append(", PORTFOLIOFLAG=").append(PORTFOLIOFLAG);
        sb.append(", FIELDCODE=").append(FIELDCODE);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LDPLANDUTYPARAM other = (LDPLANDUTYPARAM) that;
        return (this.getMAINRISKCODE() == null ? other.getMAINRISKCODE() == null : this.getMAINRISKCODE().equals(other.getMAINRISKCODE()))
            && (this.getMAINRISKVERSION() == null ? other.getMAINRISKVERSION() == null : this.getMAINRISKVERSION().equals(other.getMAINRISKVERSION()))
            && (this.getRISKCODE() == null ? other.getRISKCODE() == null : this.getRISKCODE().equals(other.getRISKCODE()))
            && (this.getRISKVERSION() == null ? other.getRISKVERSION() == null : this.getRISKVERSION().equals(other.getRISKVERSION()))
            && (this.getCONTPLANCODE() == null ? other.getCONTPLANCODE() == null : this.getCONTPLANCODE().equals(other.getCONTPLANCODE()))
            && (this.getCONTPLANNAME() == null ? other.getCONTPLANNAME() == null : this.getCONTPLANNAME().equals(other.getCONTPLANNAME()))
            && (this.getDUTYCODE() == null ? other.getDUTYCODE() == null : this.getDUTYCODE().equals(other.getDUTYCODE()))
            && (this.getCALFACTOR() == null ? other.getCALFACTOR() == null : this.getCALFACTOR().equals(other.getCALFACTOR()))
            && (this.getCALFACTORTYPE() == null ? other.getCALFACTORTYPE() == null : this.getCALFACTORTYPE().equals(other.getCALFACTORTYPE()))
            && (this.getCALFACTORVALUE() == null ? other.getCALFACTORVALUE() == null : this.getCALFACTORVALUE().equals(other.getCALFACTORVALUE()))
            && (this.getREMARK() == null ? other.getREMARK() == null : this.getREMARK().equals(other.getREMARK()))
            && (this.getPLANTYPE() == null ? other.getPLANTYPE() == null : this.getPLANTYPE().equals(other.getPLANTYPE()))
            && (this.getPORTFOLIOFLAG() == null ? other.getPORTFOLIOFLAG() == null : this.getPORTFOLIOFLAG().equals(other.getPORTFOLIOFLAG()))
            && (this.getFIELDCODE() == null ? other.getFIELDCODE() == null : this.getFIELDCODE().equals(other.getFIELDCODE()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getMAINRISKCODE() == null) ? 0 : getMAINRISKCODE().hashCode());
        result = prime * result + ((getMAINRISKVERSION() == null) ? 0 : getMAINRISKVERSION().hashCode());
        result = prime * result + ((getRISKCODE() == null) ? 0 : getRISKCODE().hashCode());
        result = prime * result + ((getRISKVERSION() == null) ? 0 : getRISKVERSION().hashCode());
        result = prime * result + ((getCONTPLANCODE() == null) ? 0 : getCONTPLANCODE().hashCode());
        result = prime * result + ((getCONTPLANNAME() == null) ? 0 : getCONTPLANNAME().hashCode());
        result = prime * result + ((getDUTYCODE() == null) ? 0 : getDUTYCODE().hashCode());
        result = prime * result + ((getCALFACTOR() == null) ? 0 : getCALFACTOR().hashCode());
        result = prime * result + ((getCALFACTORTYPE() == null) ? 0 : getCALFACTORTYPE().hashCode());
        result = prime * result + ((getCALFACTORVALUE() == null) ? 0 : getCALFACTORVALUE().hashCode());
        result = prime * result + ((getREMARK() == null) ? 0 : getREMARK().hashCode());
        result = prime * result + ((getPLANTYPE() == null) ? 0 : getPLANTYPE().hashCode());
        result = prime * result + ((getPORTFOLIOFLAG() == null) ? 0 : getPORTFOLIOFLAG().hashCode());
        result = prime * result + ((getFIELDCODE() == null) ? 0 : getFIELDCODE().hashCode());
        return result;
    }
}