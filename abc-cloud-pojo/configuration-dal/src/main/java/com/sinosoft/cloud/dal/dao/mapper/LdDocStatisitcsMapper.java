package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LZCard;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;

import java.util.List;
import java.util.Map;

public interface LdDocStatisitcsMapper {
    //查询日志信息
    List<LZCard> selectByTaskName(Map<String, Object> map);
    //查询编码下拉
    List<LDCode> getCodes();
    //查询状态下拉
    List<LZCard> getStates();
    //查询记录总条数
    Integer countForList(Map<String, Object> map);
}
