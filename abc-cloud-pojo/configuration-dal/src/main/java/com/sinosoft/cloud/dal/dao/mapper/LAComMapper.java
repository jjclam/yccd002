package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LAChannelProduct;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.LAComExample;
import com.sinosoft.cloud.dal.dao.model.entity.AgentInfo;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SaleProInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LAComMapper {
    int countByExample();

    int deleteByExample(LAComExample example);

    int deleteByPrimaryKey(String AGENTCOM);

    int insert(LACom record);

    int insertSelective(LACom record);

    List<LACom> selectByExample(LAComExample example);

    LACom selectByPrimaryKey(String AGENTCOM);

    LACom selectByAgentCom(String AGENTCOM);

    int updateByExampleSelective(@Param("record") LACom record, @Param("example") LAComExample example);

    int updateByExample(@Param("record") LACom record, @Param("example") LAComExample example);

    int updateByPrimaryKeySelective(LACom record);

    int updateByPrimaryKey(LACom record);

    List<ChlInfo> selectForChlInfo(Map<String,Object> map);

    List<ChlInfo> selectChlCodeName();

    List<LAChannelProduct> selectForChlProInfo(Map<String,Object> map);

    AgentInfo selectAgentInfoByAgentCode(@Param("agencyCode") String agencyCode);

    List<SaleProInfo> selectRiskTypeCodeName();

    List<SaleProInfo> selectRiskTypeCodeNameEX(@Param("riskType") String riskType);

    Integer countByChlInfo(Map<String,Object> map);

    List<ChlInfo> selectChlName(@Param("channelCode") String channelCode);

    SaleProInfo selectRiskTypeName(@Param("riskTypeCode") String riskTypeCode);

    Integer countChlForUpDown(@Param("channelCode") String channelCode);

    List<ChlInfo> selectChlAgencyType();

    List<ChlInfo> selectForAgencyInfo();
}