package com.sinosoft.cloud.dal.yfbDao.mapper;


import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.LcGrpCont;
//import com.sinosoft.cloud.dal.yfbDao.model.entity.PremSumInfo;
import com.sinosoft.cloud.dal.yfbDao.model.entity.ScheduleInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LcGrpContMapper {
    int deleteByPrimaryKey(String grpcontno);

    int insert(LcGrpCont record);

    int insertSelective(LcGrpCont record);

    LcGrpCont selectByPrimaryKey(String grpcontno);

    int updateByPrimaryKeySelective(LcGrpCont record);

    int updateByPrimaryKey(LcGrpCont record);

    List<LcGrpCont> selectGrpLccontInf(@Param("mLCGrpContSchema") LcGrpCont mLCGrpContSchema);

    public Integer findCount(@Param("mLCGrpContSchema") LcGrpCont mLCGrpContSchema);
    /**   Add By Xcc     */
    int countScheduleList(Map<String,Object> map);

    List<ScheduleInfo> getScheduleList(Map<String,Object> map);

    List<Lacom> selectLacom(@Param("lacom") Lacom lacom);

    Integer selectLacomCount(@Param("lacom") Lacom lacom);

    public List<Lacom> selectLacomSelect();

    List<ScheduleInfo> getScheduleViewList(Map<String,Object> map);

//    List<PremSumInfo> getPremSummaryList(Map<String,Object> map);

//    List<PremSumInfo> getPremSumViewList(Map<String,Object> map);
}