package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;

public class Operation implements Serializable {
    private Integer id;

    private String contNo;

    private String appntName;

    private String insuredName;

    private String sellType;

    private String riskCode;

    private Float prem;

    private String cvaliDate;

    private String endDate;

    private String appFlag;

    private String countDate;

    private String countPol;

    private String thirdPartyOrderId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getAppntName() {
        return appntName;
    }

    public void setAppntName(String appntName) {
        this.appntName = appntName;
    }

    public String getInsuredName() {
        return insuredName;
    }

    public void setInsuredName(String insuredName) {
        this.insuredName = insuredName;
    }

    public String getSellType() {
        return sellType;
    }

    public void setSellType(String sellType) {
        this.sellType = sellType;
    }

    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public Float getPrem() {
        return prem;
    }

    public void setPrem(Float prem) {
        this.prem = prem;
    }

    public String getCvaliDate() {
        return cvaliDate;
    }

    public void setCvaliDate(String cvaliDate) {
        this.cvaliDate = cvaliDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAppFlag() {
        return appFlag;
    }

    public void setAppFlag(String appFlag) {
        this.appFlag = appFlag;
    }

    public String getCountDate() {
        return countDate;
    }

    public void setCountDate(String countDate) {
        this.countDate = countDate;
    }

    public String getCountPol() {
        return countPol;
    }

    public void setCountPol(String countPol) {
        this.countPol = countPol;
    }

    public String getThirdPartyOrderId() {
        return thirdPartyOrderId;
    }

    public void setThirdPartyOrderId(String thirdPartyOrderId) {
        this.thirdPartyOrderId = thirdPartyOrderId;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "id=" + id +
                ", contNo='" + contNo + '\'' +
                ", appntName='" + appntName + '\'' +
                ", insuredName='" + insuredName + '\'' +
                ", sellType='" + sellType + '\'' +
                ", riskCode='" + riskCode + '\'' +
                ", prem=" + prem +
                ", cvaliDate='" + cvaliDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", appFlag='" + appFlag + '\'' +
                ", countDate='" + countDate + '\'' +
                ", countPol='" + countPol + '\'' +
                ", thirdPartyOrderId='" + thirdPartyOrderId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Operation operation = (Operation) o;

        if (id != null ? !id.equals(operation.id) : operation.id != null) return false;
        if (contNo != null ? !contNo.equals(operation.contNo) : operation.contNo != null) return false;
        if (appntName != null ? !appntName.equals(operation.appntName) : operation.appntName != null) return false;
        if (insuredName != null ? !insuredName.equals(operation.insuredName) : operation.insuredName != null)
            return false;
        if (sellType != null ? !sellType.equals(operation.sellType) : operation.sellType != null) return false;
        if (riskCode != null ? !riskCode.equals(operation.riskCode) : operation.riskCode != null) return false;
        if (prem != null ? !prem.equals(operation.prem) : operation.prem != null) return false;
        if (cvaliDate != null ? !cvaliDate.equals(operation.cvaliDate) : operation.cvaliDate != null) return false;
        if (endDate != null ? !endDate.equals(operation.endDate) : operation.endDate != null) return false;
        if (appFlag != null ? !appFlag.equals(operation.appFlag) : operation.appFlag != null) return false;
        if (countDate != null ? !countDate.equals(operation.countDate) : operation.countDate != null) return false;
        if (countPol != null ? !countPol.equals(operation.countPol) : operation.countPol != null) return false;
        return thirdPartyOrderId != null ? thirdPartyOrderId.equals(operation.thirdPartyOrderId) : operation.thirdPartyOrderId == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (contNo != null ? contNo.hashCode() : 0);
        result = 31 * result + (appntName != null ? appntName.hashCode() : 0);
        result = 31 * result + (insuredName != null ? insuredName.hashCode() : 0);
        result = 31 * result + (sellType != null ? sellType.hashCode() : 0);
        result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
        result = 31 * result + (prem != null ? prem.hashCode() : 0);
        result = 31 * result + (cvaliDate != null ? cvaliDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (appFlag != null ? appFlag.hashCode() : 0);
        result = 31 * result + (countDate != null ? countDate.hashCode() : 0);
        result = 31 * result + (countPol != null ? countPol.hashCode() : 0);
        result = 31 * result + (thirdPartyOrderId != null ? thirdPartyOrderId.hashCode() : 0);
        return result;
    }
}