package com.sinosoft.cloud.dal.yfbDao.model;


public class LDGrpToPlan {
    /** 客户号码 */
    private String CustomerNo;
    /** 方案编码 */
    private String PlanCode;
    /** 方案名称 */
    private String PlanName;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private String  MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private String  ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 开办日期 */
    private String  StartDate;
    /** 停办日期 */
    private String  EndDate;
    /** 销售保额上限 */
    private double MAXAmnt;
    /** 销售保额下限 */
    private double MINAmnt;
    /** 销售份数上限 */
    private int MAXMult;
    /** 销售份数下限 */
    private int MINMult;
    /** 销售保费上限 */
    private double MAXPrem;
    /** 销售保费下限 */
    private double MINPrem;
    /** 手续费率 */
    private double FeeRate;

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public String getPlanCode() {
        return PlanCode;
    }

    public void setPlanCode(String planCode) {
        PlanCode = planCode;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getMakeDate() {
        return MakeDate;
    }

    public void setMakeDate(String makeDate) {
        MakeDate = makeDate;
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String makeTime) {
        MakeTime = makeTime;
    }

    public String getModifyDate() {
        return ModifyDate;
    }

    public void setModifyDate(String modifyDate) {
        ModifyDate = modifyDate;
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String modifyTime) {
        ModifyTime = modifyTime;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public double getMAXAmnt() {
        return MAXAmnt;
    }

    public void setMAXAmnt(double MAXAmnt) {
        this.MAXAmnt = MAXAmnt;
    }

    public double getMINAmnt() {
        return MINAmnt;
    }

    public void setMINAmnt(double MINAmnt) {
        this.MINAmnt = MINAmnt;
    }

    public int getMAXMult() {
        return MAXMult;
    }

    public void setMAXMult(int MAXMult) {
        this.MAXMult = MAXMult;
    }

    public int getMINMult() {
        return MINMult;
    }

    public void setMINMult(int MINMult) {
        this.MINMult = MINMult;
    }

    public double getMAXPrem() {
        return MAXPrem;
    }

    public void setMAXPrem(double MAXPrem) {
        this.MAXPrem = MAXPrem;
    }

    public double getMINPrem() {
        return MINPrem;
    }

    public void setMINPrem(double MINPrem) {
        this.MINPrem = MINPrem;
    }

    public double getFeeRate() {
        return FeeRate;
    }

    public void setFeeRate(double feeRate) {
        FeeRate = feeRate;
    }
}