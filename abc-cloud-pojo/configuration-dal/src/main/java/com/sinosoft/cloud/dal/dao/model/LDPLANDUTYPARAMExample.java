package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LDPLANDUTYPARAMExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public LDPLANDUTYPARAMExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMAINRISKCODEIsNull() {
            addCriterion("MAINRISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODEIsNotNull() {
            addCriterion("MAINRISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODEEqualTo(String value) {
            addCriterion("MAINRISKCODE =", value, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODENotEqualTo(String value) {
            addCriterion("MAINRISKCODE <>", value, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODEGreaterThan(String value) {
            addCriterion("MAINRISKCODE >", value, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODEGreaterThanOrEqualTo(String value) {
            addCriterion("MAINRISKCODE >=", value, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODELessThan(String value) {
            addCriterion("MAINRISKCODE <", value, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODELessThanOrEqualTo(String value) {
            addCriterion("MAINRISKCODE <=", value, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODELike(String value) {
            addCriterion("MAINRISKCODE like", value, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODENotLike(String value) {
            addCriterion("MAINRISKCODE not like", value, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODEIn(List<String> values) {
            addCriterion("MAINRISKCODE in", values, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODENotIn(List<String> values) {
            addCriterion("MAINRISKCODE not in", values, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODEBetween(String value1, String value2) {
            addCriterion("MAINRISKCODE between", value1, value2, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKCODENotBetween(String value1, String value2) {
            addCriterion("MAINRISKCODE not between", value1, value2, "MAINRISKCODE");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONIsNull() {
            addCriterion("MAINRISKVERSION is null");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONIsNotNull() {
            addCriterion("MAINRISKVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONEqualTo(String value) {
            addCriterion("MAINRISKVERSION =", value, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONNotEqualTo(String value) {
            addCriterion("MAINRISKVERSION <>", value, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONGreaterThan(String value) {
            addCriterion("MAINRISKVERSION >", value, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONGreaterThanOrEqualTo(String value) {
            addCriterion("MAINRISKVERSION >=", value, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONLessThan(String value) {
            addCriterion("MAINRISKVERSION <", value, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONLessThanOrEqualTo(String value) {
            addCriterion("MAINRISKVERSION <=", value, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONLike(String value) {
            addCriterion("MAINRISKVERSION like", value, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONNotLike(String value) {
            addCriterion("MAINRISKVERSION not like", value, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONIn(List<String> values) {
            addCriterion("MAINRISKVERSION in", values, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONNotIn(List<String> values) {
            addCriterion("MAINRISKVERSION not in", values, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONBetween(String value1, String value2) {
            addCriterion("MAINRISKVERSION between", value1, value2, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andMAINRISKVERSIONNotBetween(String value1, String value2) {
            addCriterion("MAINRISKVERSION not between", value1, value2, "MAINRISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKCODEIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRISKCODEIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRISKCODEEqualTo(String value) {
            addCriterion("RISKCODE =", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODENotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODEGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODEGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODELessThan(String value) {
            addCriterion("RISKCODE <", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODELessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODELike(String value) {
            addCriterion("RISKCODE like", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODENotLike(String value) {
            addCriterion("RISKCODE not like", value, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODEIn(List<String> values) {
            addCriterion("RISKCODE in", values, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODENotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODEBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKCODENotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "RISKCODE");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONIsNull() {
            addCriterion("RISKVERSION is null");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONIsNotNull() {
            addCriterion("RISKVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONEqualTo(String value) {
            addCriterion("RISKVERSION =", value, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONNotEqualTo(String value) {
            addCriterion("RISKVERSION <>", value, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONGreaterThan(String value) {
            addCriterion("RISKVERSION >", value, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONGreaterThanOrEqualTo(String value) {
            addCriterion("RISKVERSION >=", value, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONLessThan(String value) {
            addCriterion("RISKVERSION <", value, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONLessThanOrEqualTo(String value) {
            addCriterion("RISKVERSION <=", value, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONLike(String value) {
            addCriterion("RISKVERSION like", value, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONNotLike(String value) {
            addCriterion("RISKVERSION not like", value, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONIn(List<String> values) {
            addCriterion("RISKVERSION in", values, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONNotIn(List<String> values) {
            addCriterion("RISKVERSION not in", values, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONBetween(String value1, String value2) {
            addCriterion("RISKVERSION between", value1, value2, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andRISKVERSIONNotBetween(String value1, String value2) {
            addCriterion("RISKVERSION not between", value1, value2, "RISKVERSION");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEIsNull() {
            addCriterion("CONTPLANCODE is null");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEIsNotNull() {
            addCriterion("CONTPLANCODE is not null");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEEqualTo(String value) {
            addCriterion("CONTPLANCODE =", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODENotEqualTo(String value) {
            addCriterion("CONTPLANCODE <>", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEGreaterThan(String value) {
            addCriterion("CONTPLANCODE >", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEGreaterThanOrEqualTo(String value) {
            addCriterion("CONTPLANCODE >=", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODELessThan(String value) {
            addCriterion("CONTPLANCODE <", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODELessThanOrEqualTo(String value) {
            addCriterion("CONTPLANCODE <=", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODELike(String value) {
            addCriterion("CONTPLANCODE like", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODENotLike(String value) {
            addCriterion("CONTPLANCODE not like", value, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEIn(List<String> values) {
            addCriterion("CONTPLANCODE in", values, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODENotIn(List<String> values) {
            addCriterion("CONTPLANCODE not in", values, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODEBetween(String value1, String value2) {
            addCriterion("CONTPLANCODE between", value1, value2, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANCODENotBetween(String value1, String value2) {
            addCriterion("CONTPLANCODE not between", value1, value2, "CONTPLANCODE");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEIsNull() {
            addCriterion("CONTPLANNAME is null");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEIsNotNull() {
            addCriterion("CONTPLANNAME is not null");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEEqualTo(String value) {
            addCriterion("CONTPLANNAME =", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMENotEqualTo(String value) {
            addCriterion("CONTPLANNAME <>", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEGreaterThan(String value) {
            addCriterion("CONTPLANNAME >", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEGreaterThanOrEqualTo(String value) {
            addCriterion("CONTPLANNAME >=", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMELessThan(String value) {
            addCriterion("CONTPLANNAME <", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMELessThanOrEqualTo(String value) {
            addCriterion("CONTPLANNAME <=", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMELike(String value) {
            addCriterion("CONTPLANNAME like", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMENotLike(String value) {
            addCriterion("CONTPLANNAME not like", value, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEIn(List<String> values) {
            addCriterion("CONTPLANNAME in", values, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMENotIn(List<String> values) {
            addCriterion("CONTPLANNAME not in", values, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMEBetween(String value1, String value2) {
            addCriterion("CONTPLANNAME between", value1, value2, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andCONTPLANNAMENotBetween(String value1, String value2) {
            addCriterion("CONTPLANNAME not between", value1, value2, "CONTPLANNAME");
            return (Criteria) this;
        }

        public Criteria andDUTYCODEIsNull() {
            addCriterion("DUTYCODE is null");
            return (Criteria) this;
        }

        public Criteria andDUTYCODEIsNotNull() {
            addCriterion("DUTYCODE is not null");
            return (Criteria) this;
        }

        public Criteria andDUTYCODEEqualTo(String value) {
            addCriterion("DUTYCODE =", value, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODENotEqualTo(String value) {
            addCriterion("DUTYCODE <>", value, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODEGreaterThan(String value) {
            addCriterion("DUTYCODE >", value, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODEGreaterThanOrEqualTo(String value) {
            addCriterion("DUTYCODE >=", value, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODELessThan(String value) {
            addCriterion("DUTYCODE <", value, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODELessThanOrEqualTo(String value) {
            addCriterion("DUTYCODE <=", value, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODELike(String value) {
            addCriterion("DUTYCODE like", value, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODENotLike(String value) {
            addCriterion("DUTYCODE not like", value, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODEIn(List<String> values) {
            addCriterion("DUTYCODE in", values, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODENotIn(List<String> values) {
            addCriterion("DUTYCODE not in", values, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODEBetween(String value1, String value2) {
            addCriterion("DUTYCODE between", value1, value2, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andDUTYCODENotBetween(String value1, String value2) {
            addCriterion("DUTYCODE not between", value1, value2, "DUTYCODE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORIsNull() {
            addCriterion("CALFACTOR is null");
            return (Criteria) this;
        }

        public Criteria andCALFACTORIsNotNull() {
            addCriterion("CALFACTOR is not null");
            return (Criteria) this;
        }

        public Criteria andCALFACTOREqualTo(String value) {
            addCriterion("CALFACTOR =", value, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORNotEqualTo(String value) {
            addCriterion("CALFACTOR <>", value, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORGreaterThan(String value) {
            addCriterion("CALFACTOR >", value, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORGreaterThanOrEqualTo(String value) {
            addCriterion("CALFACTOR >=", value, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORLessThan(String value) {
            addCriterion("CALFACTOR <", value, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORLessThanOrEqualTo(String value) {
            addCriterion("CALFACTOR <=", value, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORLike(String value) {
            addCriterion("CALFACTOR like", value, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORNotLike(String value) {
            addCriterion("CALFACTOR not like", value, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORIn(List<String> values) {
            addCriterion("CALFACTOR in", values, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORNotIn(List<String> values) {
            addCriterion("CALFACTOR not in", values, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORBetween(String value1, String value2) {
            addCriterion("CALFACTOR between", value1, value2, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORNotBetween(String value1, String value2) {
            addCriterion("CALFACTOR not between", value1, value2, "CALFACTOR");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPEIsNull() {
            addCriterion("CALFACTORTYPE is null");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPEIsNotNull() {
            addCriterion("CALFACTORTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPEEqualTo(String value) {
            addCriterion("CALFACTORTYPE =", value, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPENotEqualTo(String value) {
            addCriterion("CALFACTORTYPE <>", value, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPEGreaterThan(String value) {
            addCriterion("CALFACTORTYPE >", value, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("CALFACTORTYPE >=", value, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPELessThan(String value) {
            addCriterion("CALFACTORTYPE <", value, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPELessThanOrEqualTo(String value) {
            addCriterion("CALFACTORTYPE <=", value, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPELike(String value) {
            addCriterion("CALFACTORTYPE like", value, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPENotLike(String value) {
            addCriterion("CALFACTORTYPE not like", value, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPEIn(List<String> values) {
            addCriterion("CALFACTORTYPE in", values, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPENotIn(List<String> values) {
            addCriterion("CALFACTORTYPE not in", values, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPEBetween(String value1, String value2) {
            addCriterion("CALFACTORTYPE between", value1, value2, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORTYPENotBetween(String value1, String value2) {
            addCriterion("CALFACTORTYPE not between", value1, value2, "CALFACTORTYPE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUEIsNull() {
            addCriterion("CALFACTORVALUE is null");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUEIsNotNull() {
            addCriterion("CALFACTORVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUEEqualTo(String value) {
            addCriterion("CALFACTORVALUE =", value, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUENotEqualTo(String value) {
            addCriterion("CALFACTORVALUE <>", value, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUEGreaterThan(String value) {
            addCriterion("CALFACTORVALUE >", value, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUEGreaterThanOrEqualTo(String value) {
            addCriterion("CALFACTORVALUE >=", value, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUELessThan(String value) {
            addCriterion("CALFACTORVALUE <", value, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUELessThanOrEqualTo(String value) {
            addCriterion("CALFACTORVALUE <=", value, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUELike(String value) {
            addCriterion("CALFACTORVALUE like", value, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUENotLike(String value) {
            addCriterion("CALFACTORVALUE not like", value, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUEIn(List<String> values) {
            addCriterion("CALFACTORVALUE in", values, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUENotIn(List<String> values) {
            addCriterion("CALFACTORVALUE not in", values, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUEBetween(String value1, String value2) {
            addCriterion("CALFACTORVALUE between", value1, value2, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andCALFACTORVALUENotBetween(String value1, String value2) {
            addCriterion("CALFACTORVALUE not between", value1, value2, "CALFACTORVALUE");
            return (Criteria) this;
        }

        public Criteria andREMARKIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andREMARKIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andREMARKEqualTo(String value) {
            addCriterion("REMARK =", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKGreaterThan(String value) {
            addCriterion("REMARK >", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKLessThan(String value) {
            addCriterion("REMARK <", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKLike(String value) {
            addCriterion("REMARK like", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKNotLike(String value) {
            addCriterion("REMARK not like", value, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKIn(List<String> values) {
            addCriterion("REMARK in", values, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "REMARK");
            return (Criteria) this;
        }

        public Criteria andREMARKNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "REMARK");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEIsNull() {
            addCriterion("PLANTYPE is null");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEIsNotNull() {
            addCriterion("PLANTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEEqualTo(String value) {
            addCriterion("PLANTYPE =", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPENotEqualTo(String value) {
            addCriterion("PLANTYPE <>", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEGreaterThan(String value) {
            addCriterion("PLANTYPE >", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEGreaterThanOrEqualTo(String value) {
            addCriterion("PLANTYPE >=", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPELessThan(String value) {
            addCriterion("PLANTYPE <", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPELessThanOrEqualTo(String value) {
            addCriterion("PLANTYPE <=", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPELike(String value) {
            addCriterion("PLANTYPE like", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPENotLike(String value) {
            addCriterion("PLANTYPE not like", value, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEIn(List<String> values) {
            addCriterion("PLANTYPE in", values, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPENotIn(List<String> values) {
            addCriterion("PLANTYPE not in", values, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPEBetween(String value1, String value2) {
            addCriterion("PLANTYPE between", value1, value2, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPLANTYPENotBetween(String value1, String value2) {
            addCriterion("PLANTYPE not between", value1, value2, "PLANTYPE");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGIsNull() {
            addCriterion("PORTFOLIOFLAG is null");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGIsNotNull() {
            addCriterion("PORTFOLIOFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGEqualTo(String value) {
            addCriterion("PORTFOLIOFLAG =", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGNotEqualTo(String value) {
            addCriterion("PORTFOLIOFLAG <>", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGGreaterThan(String value) {
            addCriterion("PORTFOLIOFLAG >", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGGreaterThanOrEqualTo(String value) {
            addCriterion("PORTFOLIOFLAG >=", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGLessThan(String value) {
            addCriterion("PORTFOLIOFLAG <", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGLessThanOrEqualTo(String value) {
            addCriterion("PORTFOLIOFLAG <=", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGLike(String value) {
            addCriterion("PORTFOLIOFLAG like", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGNotLike(String value) {
            addCriterion("PORTFOLIOFLAG not like", value, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGIn(List<String> values) {
            addCriterion("PORTFOLIOFLAG in", values, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGNotIn(List<String> values) {
            addCriterion("PORTFOLIOFLAG not in", values, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGBetween(String value1, String value2) {
            addCriterion("PORTFOLIOFLAG between", value1, value2, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andPORTFOLIOFLAGNotBetween(String value1, String value2) {
            addCriterion("PORTFOLIOFLAG not between", value1, value2, "PORTFOLIOFLAG");
            return (Criteria) this;
        }

        public Criteria andFIELDCODEIsNull() {
            addCriterion("FIELDCODE is null");
            return (Criteria) this;
        }

        public Criteria andFIELDCODEIsNotNull() {
            addCriterion("FIELDCODE is not null");
            return (Criteria) this;
        }

        public Criteria andFIELDCODEEqualTo(String value) {
            addCriterion("FIELDCODE =", value, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODENotEqualTo(String value) {
            addCriterion("FIELDCODE <>", value, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODEGreaterThan(String value) {
            addCriterion("FIELDCODE >", value, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODEGreaterThanOrEqualTo(String value) {
            addCriterion("FIELDCODE >=", value, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODELessThan(String value) {
            addCriterion("FIELDCODE <", value, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODELessThanOrEqualTo(String value) {
            addCriterion("FIELDCODE <=", value, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODELike(String value) {
            addCriterion("FIELDCODE like", value, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODENotLike(String value) {
            addCriterion("FIELDCODE not like", value, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODEIn(List<String> values) {
            addCriterion("FIELDCODE in", values, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODENotIn(List<String> values) {
            addCriterion("FIELDCODE not in", values, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODEBetween(String value1, String value2) {
            addCriterion("FIELDCODE between", value1, value2, "FIELDCODE");
            return (Criteria) this;
        }

        public Criteria andFIELDCODENotBetween(String value1, String value2) {
            addCriterion("FIELDCODE not between", value1, value2, "FIELDCODE");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}