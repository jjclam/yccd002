package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LAChannelAgent;
import com.sinosoft.cloud.dal.dao.model.LAChannelAgentExample;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.entity.AgentInfo;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.LAComLiAn;
import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface LAChannelAgentMapper {
    int countByExample(LAChannelAgentExample example);

    int deleteByExample(LAChannelAgentExample example);

    int deleteByPrimaryKey(String channelCode);

    int insert(LAChannelAgent record);

    @Insert("INSERT INTO lacom(AGENTCOM, MANAGECOM, AREATYPE, CHANNELTYPE, NAME, PHONE, EMAIL,OPERATOR,BRANCHTYPE2) values( #{AGENTCOM}, #{MANAGECOM}, 'LA', '0',#{NAME}, #{PHONE}, #{EMAIL}, 'LIAN' ,#{BRANCHTYPE2})")
    int addWangBit2(LACom mLacom);

    @Select("select AGENTCOM from lacom where AGENTCOM = #{AGENTCOM} ")
    String  selectWangbit2(LACom mLacom);

    int insertSelective(LAChannelAgent record);

    List<LAChannelAgent> selectByExample(LAChannelAgentExample example);

    LAChannelAgent selectByPrimaryKey(String channelCode);

    int updateByExampleSelective(@Param("record") LAChannelAgent record, @Param("example") LAChannelAgentExample example);

    int updateByExample(@Param("record") LAChannelAgent record, @Param("example") LAChannelAgentExample example);

    int updateByPrimaryKeySelective(LAChannelAgent record);

    int updateByPrimaryKey(LAChannelAgent record);

    List<ChlInfo> selectForChlInfo(Map<String,Object> map);

    Integer countByChlInfo(Map<String,Object> map);

    Integer countForChlAgencyMaintainList(@Param("agencyCode") String agencyCode, @Param("agencyName") String agencyName,@Param("chlTypeL")String chlTypeL);

    List<LAComLiAn> selectChlAgencyMaintainList(@Param("agencyCode") String agencyCode, @Param("agencyName") String agencyName, @Param("chlTypeL")String chlTypeL);

    Integer verifyChannelAgent(Map<String,Object> map);

    Integer insertChlAgencyMaintain(Map<String,Object> map);

    Integer deleteChlAgencyMaintain(@Param("agencyCode") String agencyName, @Param("channelCode") String channelName);



}