package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;

public class open_LaCom implements Serializable {
    private String COMCODE;

    private String USERNAME;

    private String COMPANYNAME;

    private String VERIFYSTATE;

    private String VERIFYOPINION;

    private String VERIFYNAME;

    private Date VERIFYDATE;

    private String VERIFYTIME;

    private String COMADDRESS;

    private String LEGALPERSON;

    private String IDTYPE;

    private String IDNO;

    private Date IDSTART;

    private Date IDEND;

    private String LINKNAME;

    private String LINKPHONE;

    private String LINKEMAIL;

    private String ZIPNO;

    private String COMTYPE;

    private String ISNET;

    private String COMIDTYPE;

    private String COMIDNO;

    private Date COMIDSTART;

    private Date COMIDEND;

    private String AGENTBUSINO;

    private Date AGENTBUSISTART;

    private Date AGENTBUSIEND;

    private String FEEBANKTYPE;

    private String FEEBANKNAME;

    private String FEENAME;

    private String FEEBANKNO;

    private String ISCOLLECTION;

    private String PREMBANKTYPE;

    private String PREMBANKNAME;

    private String PREMNAME;

    private String PREMNO;

    private Date MODIFYDATE;

    private String MODIFYTIME;

    private Date MAKEDATE;

    private String MAKETIME;

    private static final long serialVersionUID = 1L;

    public String getCOMCODE() {
        return COMCODE;
    }

    public void setCOMCODE(String COMCODE) {
        this.COMCODE = COMCODE;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getCOMPANYNAME() {
        return COMPANYNAME;
    }

    public void setCOMPANYNAME(String COMPANYNAME) {
        this.COMPANYNAME = COMPANYNAME;
    }

    public String getVERIFYSTATE() {
        return VERIFYSTATE;
    }

    public void setVERIFYSTATE(String VERIFYSTATE) {
        this.VERIFYSTATE = VERIFYSTATE;
    }

    public String getVERIFYOPINION() {
        return VERIFYOPINION;
    }

    public void setVERIFYOPINION(String VERIFYOPINION) {
        this.VERIFYOPINION = VERIFYOPINION;
    }

    public String getVERIFYNAME() {
        return VERIFYNAME;
    }

    public void setVERIFYNAME(String VERIFYNAME) {
        this.VERIFYNAME = VERIFYNAME;
    }

    public Date getVERIFYDATE() {
        return VERIFYDATE;
    }

    public void setVERIFYDATE(Date VERIFYDATE) {
        this.VERIFYDATE = VERIFYDATE;
    }

    public String getVERIFYTIME() {
        return VERIFYTIME;
    }

    public void setVERIFYTIME(String VERIFYTIME) {
        this.VERIFYTIME = VERIFYTIME;
    }

    public String getCOMADDRESS() {
        return COMADDRESS;
    }

    public void setCOMADDRESS(String COMADDRESS) {
        this.COMADDRESS = COMADDRESS;
    }

    public String getLEGALPERSON() {
        return LEGALPERSON;
    }

    public void setLEGALPERSON(String LEGALPERSON) {
        this.LEGALPERSON = LEGALPERSON;
    }

    public String getIDTYPE() {
        return IDTYPE;
    }

    public void setIDTYPE(String IDTYPE) {
        this.IDTYPE = IDTYPE;
    }

    public String getIDNO() {
        return IDNO;
    }

    public void setIDNO(String IDNO) {
        this.IDNO = IDNO;
    }

    public Date getIDSTART() {
        return IDSTART;
    }

    public void setIDSTART(Date IDSTART) {
        this.IDSTART = IDSTART;
    }

    public Date getIDEND() {
        return IDEND;
    }

    public void setIDEND(Date IDEND) {
        this.IDEND = IDEND;
    }

    public String getLINKNAME() {
        return LINKNAME;
    }

    public void setLINKNAME(String LINKNAME) {
        this.LINKNAME = LINKNAME;
    }

    public String getLINKPHONE() {
        return LINKPHONE;
    }

    public void setLINKPHONE(String LINKPHONE) {
        this.LINKPHONE = LINKPHONE;
    }

    public String getLINKEMAIL() {
        return LINKEMAIL;
    }

    public void setLINKEMAIL(String LINKEMAIL) {
        this.LINKEMAIL = LINKEMAIL;
    }

    public String getZIPNO() {
        return ZIPNO;
    }

    public void setZIPNO(String ZIPNO) {
        this.ZIPNO = ZIPNO;
    }

    public String getCOMTYPE() {
        return COMTYPE;
    }

    public void setCOMTYPE(String COMTYPE) {
        this.COMTYPE = COMTYPE;
    }

    public String getISNET() {
        return ISNET;
    }

    public void setISNET(String ISNET) {
        this.ISNET = ISNET;
    }

    public String getCOMIDTYPE() {
        return COMIDTYPE;
    }

    public void setCOMIDTYPE(String COMIDTYPE) {
        this.COMIDTYPE = COMIDTYPE;
    }

    public String getCOMIDNO() {
        return COMIDNO;
    }

    public void setCOMIDNO(String COMIDNO) {
        this.COMIDNO = COMIDNO;
    }

    public Date getCOMIDSTART() {
        return COMIDSTART;
    }

    public void setCOMIDSTART(Date COMIDSTART) {
        this.COMIDSTART = COMIDSTART;
    }

    public Date getCOMIDEND() {
        return COMIDEND;
    }

    public void setCOMIDEND(Date COMIDEND) {
        this.COMIDEND = COMIDEND;
    }

    public String getAGENTBUSINO() {
        return AGENTBUSINO;
    }

    public void setAGENTBUSINO(String AGENTBUSINO) {
        this.AGENTBUSINO = AGENTBUSINO;
    }

    public Date getAGENTBUSISTART() {
        return AGENTBUSISTART;
    }

    public void setAGENTBUSISTART(Date AGENTBUSISTART) {
        this.AGENTBUSISTART = AGENTBUSISTART;
    }

    public Date getAGENTBUSIEND() {
        return AGENTBUSIEND;
    }

    public void setAGENTBUSIEND(Date AGENTBUSIEND) {
        this.AGENTBUSIEND = AGENTBUSIEND;
    }

    public String getFEEBANKTYPE() {
        return FEEBANKTYPE;
    }

    public void setFEEBANKTYPE(String FEEBANKTYPE) {
        this.FEEBANKTYPE = FEEBANKTYPE;
    }

    public String getFEEBANKNAME() {
        return FEEBANKNAME;
    }

    public void setFEEBANKNAME(String FEEBANKNAME) {
        this.FEEBANKNAME = FEEBANKNAME;
    }

    public String getFEENAME() {
        return FEENAME;
    }

    public void setFEENAME(String FEENAME) {
        this.FEENAME = FEENAME;
    }

    public String getFEEBANKNO() {
        return FEEBANKNO;
    }

    public void setFEEBANKNO(String FEEBANKNO) {
        this.FEEBANKNO = FEEBANKNO;
    }

    public String getISCOLLECTION() {
        return ISCOLLECTION;
    }

    public void setISCOLLECTION(String ISCOLLECTION) {
        this.ISCOLLECTION = ISCOLLECTION;
    }

    public String getPREMBANKTYPE() {
        return PREMBANKTYPE;
    }

    public void setPREMBANKTYPE(String PREMBANKTYPE) {
        this.PREMBANKTYPE = PREMBANKTYPE;
    }

    public String getPREMBANKNAME() {
        return PREMBANKNAME;
    }

    public void setPREMBANKNAME(String PREMBANKNAME) {
        this.PREMBANKNAME = PREMBANKNAME;
    }

    public String getPREMNAME() {
        return PREMNAME;
    }

    public void setPREMNAME(String PREMNAME) {
        this.PREMNAME = PREMNAME;
    }

    public String getPREMNO() {
        return PREMNO;
    }

    public void setPREMNO(String PREMNO) {
        this.PREMNO = PREMNO;
    }

    public Date getMODIFYDATE() {
        return MODIFYDATE;
    }

    public void setMODIFYDATE(Date MODIFYDATE) {
        this.MODIFYDATE = MODIFYDATE;
    }

    public String getMODIFYTIME() {
        return MODIFYTIME;
    }

    public void setMODIFYTIME(String MODIFYTIME) {
        this.MODIFYTIME = MODIFYTIME;
    }

    public Date getMAKEDATE() {
        return MAKEDATE;
    }

    public void setMAKEDATE(Date MAKEDATE) {
        this.MAKEDATE = MAKEDATE;
    }

    public String getMAKETIME() {
        return MAKETIME;
    }

    public void setMAKETIME(String MAKETIME) {
        this.MAKETIME = MAKETIME;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", COMCODE=").append(COMCODE);
        sb.append(", USERNAME=").append(USERNAME);
        sb.append(", COMPANYNAME=").append(COMPANYNAME);
        sb.append(", VERIFYSTATE=").append(VERIFYSTATE);
        sb.append(", VERIFYOPINION=").append(VERIFYOPINION);
        sb.append(", VERIFYNAME=").append(VERIFYNAME);
        sb.append(", VERIFYDATE=").append(VERIFYDATE);
        sb.append(", VERIFYTIME=").append(VERIFYTIME);
        sb.append(", COMADDRESS=").append(COMADDRESS);
        sb.append(", LEGALPERSON=").append(LEGALPERSON);
        sb.append(", IDTYPE=").append(IDTYPE);
        sb.append(", IDNO=").append(IDNO);
        sb.append(", IDSTART=").append(IDSTART);
        sb.append(", IDEND=").append(IDEND);
        sb.append(", LINKNAME=").append(LINKNAME);
        sb.append(", LINKPHONE=").append(LINKPHONE);
        sb.append(", LINKEMAIL=").append(LINKEMAIL);
        sb.append(", ZIPNO=").append(ZIPNO);
        sb.append(", COMTYPE=").append(COMTYPE);
        sb.append(", ISNET=").append(ISNET);
        sb.append(", COMIDTYPE=").append(COMIDTYPE);
        sb.append(", COMIDNO=").append(COMIDNO);
        sb.append(", COMIDSTART=").append(COMIDSTART);
        sb.append(", COMIDEND=").append(COMIDEND);
        sb.append(", AGENTBUSINO=").append(AGENTBUSINO);
        sb.append(", AGENTBUSISTART=").append(AGENTBUSISTART);
        sb.append(", AGENTBUSIEND=").append(AGENTBUSIEND);
        sb.append(", FEEBANKTYPE=").append(FEEBANKTYPE);
        sb.append(", FEEBANKNAME=").append(FEEBANKNAME);
        sb.append(", FEENAME=").append(FEENAME);
        sb.append(", FEEBANKNO=").append(FEEBANKNO);
        sb.append(", ISCOLLECTION=").append(ISCOLLECTION);
        sb.append(", PREMBANKTYPE=").append(PREMBANKTYPE);
        sb.append(", PREMBANKNAME=").append(PREMBANKNAME);
        sb.append(", PREMNAME=").append(PREMNAME);
        sb.append(", PREMNO=").append(PREMNO);
        sb.append(", MODIFYDATE=").append(MODIFYDATE);
        sb.append(", MODIFYTIME=").append(MODIFYTIME);
        sb.append(", MAKEDATE=").append(MAKEDATE);
        sb.append(", MAKETIME=").append(MAKETIME);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        open_LaCom other = (open_LaCom) that;
        return (this.getCOMCODE() == null ? other.getCOMCODE() == null : this.getCOMCODE().equals(other.getCOMCODE()))
            && (this.getUSERNAME() == null ? other.getUSERNAME() == null : this.getUSERNAME().equals(other.getUSERNAME()))
            && (this.getCOMPANYNAME() == null ? other.getCOMPANYNAME() == null : this.getCOMPANYNAME().equals(other.getCOMPANYNAME()))
            && (this.getVERIFYSTATE() == null ? other.getVERIFYSTATE() == null : this.getVERIFYSTATE().equals(other.getVERIFYSTATE()))
            && (this.getVERIFYOPINION() == null ? other.getVERIFYOPINION() == null : this.getVERIFYOPINION().equals(other.getVERIFYOPINION()))
            && (this.getVERIFYNAME() == null ? other.getVERIFYNAME() == null : this.getVERIFYNAME().equals(other.getVERIFYNAME()))
            && (this.getVERIFYDATE() == null ? other.getVERIFYDATE() == null : this.getVERIFYDATE().equals(other.getVERIFYDATE()))
            && (this.getVERIFYTIME() == null ? other.getVERIFYTIME() == null : this.getVERIFYTIME().equals(other.getVERIFYTIME()))
            && (this.getCOMADDRESS() == null ? other.getCOMADDRESS() == null : this.getCOMADDRESS().equals(other.getCOMADDRESS()))
            && (this.getLEGALPERSON() == null ? other.getLEGALPERSON() == null : this.getLEGALPERSON().equals(other.getLEGALPERSON()))
            && (this.getIDTYPE() == null ? other.getIDTYPE() == null : this.getIDTYPE().equals(other.getIDTYPE()))
            && (this.getIDNO() == null ? other.getIDNO() == null : this.getIDNO().equals(other.getIDNO()))
            && (this.getIDSTART() == null ? other.getIDSTART() == null : this.getIDSTART().equals(other.getIDSTART()))
            && (this.getIDEND() == null ? other.getIDEND() == null : this.getIDEND().equals(other.getIDEND()))
            && (this.getLINKNAME() == null ? other.getLINKNAME() == null : this.getLINKNAME().equals(other.getLINKNAME()))
            && (this.getLINKPHONE() == null ? other.getLINKPHONE() == null : this.getLINKPHONE().equals(other.getLINKPHONE()))
            && (this.getLINKEMAIL() == null ? other.getLINKEMAIL() == null : this.getLINKEMAIL().equals(other.getLINKEMAIL()))
            && (this.getZIPNO() == null ? other.getZIPNO() == null : this.getZIPNO().equals(other.getZIPNO()))
            && (this.getCOMTYPE() == null ? other.getCOMTYPE() == null : this.getCOMTYPE().equals(other.getCOMTYPE()))
            && (this.getISNET() == null ? other.getISNET() == null : this.getISNET().equals(other.getISNET()))
            && (this.getCOMIDTYPE() == null ? other.getCOMIDTYPE() == null : this.getCOMIDTYPE().equals(other.getCOMIDTYPE()))
            && (this.getCOMIDNO() == null ? other.getCOMIDNO() == null : this.getCOMIDNO().equals(other.getCOMIDNO()))
            && (this.getCOMIDSTART() == null ? other.getCOMIDSTART() == null : this.getCOMIDSTART().equals(other.getCOMIDSTART()))
            && (this.getCOMIDEND() == null ? other.getCOMIDEND() == null : this.getCOMIDEND().equals(other.getCOMIDEND()))
            && (this.getAGENTBUSINO() == null ? other.getAGENTBUSINO() == null : this.getAGENTBUSINO().equals(other.getAGENTBUSINO()))
            && (this.getAGENTBUSISTART() == null ? other.getAGENTBUSISTART() == null : this.getAGENTBUSISTART().equals(other.getAGENTBUSISTART()))
            && (this.getAGENTBUSIEND() == null ? other.getAGENTBUSIEND() == null : this.getAGENTBUSIEND().equals(other.getAGENTBUSIEND()))
            && (this.getFEEBANKTYPE() == null ? other.getFEEBANKTYPE() == null : this.getFEEBANKTYPE().equals(other.getFEEBANKTYPE()))
            && (this.getFEEBANKNAME() == null ? other.getFEEBANKNAME() == null : this.getFEEBANKNAME().equals(other.getFEEBANKNAME()))
            && (this.getFEENAME() == null ? other.getFEENAME() == null : this.getFEENAME().equals(other.getFEENAME()))
            && (this.getFEEBANKNO() == null ? other.getFEEBANKNO() == null : this.getFEEBANKNO().equals(other.getFEEBANKNO()))
            && (this.getISCOLLECTION() == null ? other.getISCOLLECTION() == null : this.getISCOLLECTION().equals(other.getISCOLLECTION()))
            && (this.getPREMBANKTYPE() == null ? other.getPREMBANKTYPE() == null : this.getPREMBANKTYPE().equals(other.getPREMBANKTYPE()))
            && (this.getPREMBANKNAME() == null ? other.getPREMBANKNAME() == null : this.getPREMBANKNAME().equals(other.getPREMBANKNAME()))
            && (this.getPREMNAME() == null ? other.getPREMNAME() == null : this.getPREMNAME().equals(other.getPREMNAME()))
            && (this.getPREMNO() == null ? other.getPREMNO() == null : this.getPREMNO().equals(other.getPREMNO()))
            && (this.getMODIFYDATE() == null ? other.getMODIFYDATE() == null : this.getMODIFYDATE().equals(other.getMODIFYDATE()))
            && (this.getMODIFYTIME() == null ? other.getMODIFYTIME() == null : this.getMODIFYTIME().equals(other.getMODIFYTIME()))
            && (this.getMAKEDATE() == null ? other.getMAKEDATE() == null : this.getMAKEDATE().equals(other.getMAKEDATE()))
            && (this.getMAKETIME() == null ? other.getMAKETIME() == null : this.getMAKETIME().equals(other.getMAKETIME()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCOMCODE() == null) ? 0 : getCOMCODE().hashCode());
        result = prime * result + ((getUSERNAME() == null) ? 0 : getUSERNAME().hashCode());
        result = prime * result + ((getCOMPANYNAME() == null) ? 0 : getCOMPANYNAME().hashCode());
        result = prime * result + ((getVERIFYSTATE() == null) ? 0 : getVERIFYSTATE().hashCode());
        result = prime * result + ((getVERIFYOPINION() == null) ? 0 : getVERIFYOPINION().hashCode());
        result = prime * result + ((getVERIFYNAME() == null) ? 0 : getVERIFYNAME().hashCode());
        result = prime * result + ((getVERIFYDATE() == null) ? 0 : getVERIFYDATE().hashCode());
        result = prime * result + ((getVERIFYTIME() == null) ? 0 : getVERIFYTIME().hashCode());
        result = prime * result + ((getCOMADDRESS() == null) ? 0 : getCOMADDRESS().hashCode());
        result = prime * result + ((getLEGALPERSON() == null) ? 0 : getLEGALPERSON().hashCode());
        result = prime * result + ((getIDTYPE() == null) ? 0 : getIDTYPE().hashCode());
        result = prime * result + ((getIDNO() == null) ? 0 : getIDNO().hashCode());
        result = prime * result + ((getIDSTART() == null) ? 0 : getIDSTART().hashCode());
        result = prime * result + ((getIDEND() == null) ? 0 : getIDEND().hashCode());
        result = prime * result + ((getLINKNAME() == null) ? 0 : getLINKNAME().hashCode());
        result = prime * result + ((getLINKPHONE() == null) ? 0 : getLINKPHONE().hashCode());
        result = prime * result + ((getLINKEMAIL() == null) ? 0 : getLINKEMAIL().hashCode());
        result = prime * result + ((getZIPNO() == null) ? 0 : getZIPNO().hashCode());
        result = prime * result + ((getCOMTYPE() == null) ? 0 : getCOMTYPE().hashCode());
        result = prime * result + ((getISNET() == null) ? 0 : getISNET().hashCode());
        result = prime * result + ((getCOMIDTYPE() == null) ? 0 : getCOMIDTYPE().hashCode());
        result = prime * result + ((getCOMIDNO() == null) ? 0 : getCOMIDNO().hashCode());
        result = prime * result + ((getCOMIDSTART() == null) ? 0 : getCOMIDSTART().hashCode());
        result = prime * result + ((getCOMIDEND() == null) ? 0 : getCOMIDEND().hashCode());
        result = prime * result + ((getAGENTBUSINO() == null) ? 0 : getAGENTBUSINO().hashCode());
        result = prime * result + ((getAGENTBUSISTART() == null) ? 0 : getAGENTBUSISTART().hashCode());
        result = prime * result + ((getAGENTBUSIEND() == null) ? 0 : getAGENTBUSIEND().hashCode());
        result = prime * result + ((getFEEBANKTYPE() == null) ? 0 : getFEEBANKTYPE().hashCode());
        result = prime * result + ((getFEEBANKNAME() == null) ? 0 : getFEEBANKNAME().hashCode());
        result = prime * result + ((getFEENAME() == null) ? 0 : getFEENAME().hashCode());
        result = prime * result + ((getFEEBANKNO() == null) ? 0 : getFEEBANKNO().hashCode());
        result = prime * result + ((getISCOLLECTION() == null) ? 0 : getISCOLLECTION().hashCode());
        result = prime * result + ((getPREMBANKTYPE() == null) ? 0 : getPREMBANKTYPE().hashCode());
        result = prime * result + ((getPREMBANKNAME() == null) ? 0 : getPREMBANKNAME().hashCode());
        result = prime * result + ((getPREMNAME() == null) ? 0 : getPREMNAME().hashCode());
        result = prime * result + ((getPREMNO() == null) ? 0 : getPREMNO().hashCode());
        result = prime * result + ((getMODIFYDATE() == null) ? 0 : getMODIFYDATE().hashCode());
        result = prime * result + ((getMODIFYTIME() == null) ? 0 : getMODIFYTIME().hashCode());
        result = prime * result + ((getMAKEDATE() == null) ? 0 : getMAKEDATE().hashCode());
        result = prime * result + ((getMAKETIME() == null) ? 0 : getMAKETIME().hashCode());
        return result;
    }
}