package com.sinosoft.cloud.dal.yfbDao.model.entity;

import java.util.Objects;

public class ScheduleInfo {
    private String id;

    private String proposalGrpContNo;

    private String grpContNo;

    private String customerNo;

    private String grpName;

    private String polApplyDate;

    private String NativePeoples;

    private String Peoples;

    private String Prem;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProposalGrpContNo() {
        return proposalGrpContNo;
    }

    public void setProposalGrpContNo(String proposalGrpContNo) {
        this.proposalGrpContNo = proposalGrpContNo;
    }

    public String getGrpContNo() {
        return grpContNo;
    }

    public void setGrpContNo(String grpContNo) {
        this.grpContNo = grpContNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public String getPolApplyDate() {
        return polApplyDate;
    }

    public void setPolApplyDate(String polApplyDate) {
        this.polApplyDate = polApplyDate;
    }

    public String getNativePeoples() {
        return NativePeoples;
    }

    public void setNativePeoples(String nativePeoples) {
        NativePeoples = nativePeoples;
    }

    public String getPeoples() {
        return Peoples;
    }

    public void setPeoples(String peoples) {
        Peoples = peoples;
    }

    public String getPrem() {
        return Prem;
    }

    public void setPrem(String prem) {
        Prem = prem;
    }

    @Override
    public String toString() {
        return "ScheduleInfo{" +
                "id='" + id + '\'' +
                ", proposalGrpContNo='" + proposalGrpContNo + '\'' +
                ", grpContNo='" + grpContNo + '\'' +
                ", customerNo='" + customerNo + '\'' +
                ", grpName='" + grpName + '\'' +
                ", polApplyDate='" + polApplyDate + '\'' +
                ", NativePeoples='" + NativePeoples + '\'' +
                ", Peoples='" + Peoples + '\'' +
                ", Prem='" + Prem + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduleInfo that = (ScheduleInfo) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(proposalGrpContNo, that.proposalGrpContNo) &&
                Objects.equals(grpContNo, that.grpContNo) &&
                Objects.equals(customerNo, that.customerNo) &&
                Objects.equals(grpName, that.grpName) &&
                Objects.equals(polApplyDate, that.polApplyDate) &&
                Objects.equals(NativePeoples, that.NativePeoples) &&
                Objects.equals(Peoples, that.Peoples) &&
                Objects.equals(Prem, that.Prem);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, proposalGrpContNo, grpContNo, customerNo, grpName, polApplyDate, NativePeoples, Peoples, Prem);
    }
}
