package com.sinosoft.cloud.dal.dao.model;

public class LDPLANRISKKey {
    private String mainriskcode;

    private String riskcode;

    private String contplancode;

    private String plantype;

    public String getMainriskcode() {
        return mainriskcode;
    }

    public void setMainriskcode(String mainriskcode) {
        this.mainriskcode = mainriskcode == null ? null : mainriskcode.trim();
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public String getContplancode() {
        return contplancode;
    }

    public void setContplancode(String contplancode) {
        this.contplancode = contplancode == null ? null : contplancode.trim();
    }

    public String getPlantype() {
        return plantype;
    }

    public void setPlantype(String plantype) {
        this.plantype = plantype == null ? null : plantype.trim();
    }
}