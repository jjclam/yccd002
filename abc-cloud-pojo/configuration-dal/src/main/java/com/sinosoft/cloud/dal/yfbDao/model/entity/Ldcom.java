package com.sinosoft.cloud.dal.yfbDao.model.entity;


public class Ldcom {

  private String comcode;
  private String outcomcode;
  private String name;
  private String shortname;
  private String address;
  private String zipcode;
  private String phone;
  private String fax;
  private String email;
  private String webaddress;
  private String satrapname;
  private String insumonitorcode;
  private String insureid;
  private String signid;
  private String regionalismcode;
  private String comnature;
  private String validcode;
  private String sign;
  private String comcitysize;
  private String servicename;
  private String serviceno;
  private String servicephone;
  private String servicepostaddress;
  private String comgrade;
  private String comareatype;
  private String upcomcode;
  private String isdirunder;
  private String comareatype1;
  private String province;
  private String city;
  private String county;
  private String findb;
  private String letterservicename;
  private String comstate;
  private String compreparedate;
  private String supmanagedate;
  private String supapprovaldate;
  private String theopeningdate;
  private String comreplycanceldate;
  private String supreplycanceldate;
  private String buspapercanceldate;


  public String getComcode() {
    return comcode;
  }

  public void setComcode(String comcode) {
    this.comcode = comcode;
  }


  public String getOutcomcode() {
    return outcomcode;
  }

  public void setOutcomcode(String outcomcode) {
    this.outcomcode = outcomcode;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getShortname() {
    return shortname;
  }

  public void setShortname(String shortname) {
    this.shortname = shortname;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getWebaddress() {
    return webaddress;
  }

  public void setWebaddress(String webaddress) {
    this.webaddress = webaddress;
  }


  public String getSatrapname() {
    return satrapname;
  }

  public void setSatrapname(String satrapname) {
    this.satrapname = satrapname;
  }


  public String getInsumonitorcode() {
    return insumonitorcode;
  }

  public void setInsumonitorcode(String insumonitorcode) {
    this.insumonitorcode = insumonitorcode;
  }


  public String getInsureid() {
    return insureid;
  }

  public void setInsureid(String insureid) {
    this.insureid = insureid;
  }


  public String getSignid() {
    return signid;
  }

  public void setSignid(String signid) {
    this.signid = signid;
  }


  public String getRegionalismcode() {
    return regionalismcode;
  }

  public void setRegionalismcode(String regionalismcode) {
    this.regionalismcode = regionalismcode;
  }


  public String getComnature() {
    return comnature;
  }

  public void setComnature(String comnature) {
    this.comnature = comnature;
  }


  public String getValidcode() {
    return validcode;
  }

  public void setValidcode(String validcode) {
    this.validcode = validcode;
  }


  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }


  public String getComcitysize() {
    return comcitysize;
  }

  public void setComcitysize(String comcitysize) {
    this.comcitysize = comcitysize;
  }


  public String getServicename() {
    return servicename;
  }

  public void setServicename(String servicename) {
    this.servicename = servicename;
  }


  public String getServiceno() {
    return serviceno;
  }

  public void setServiceno(String serviceno) {
    this.serviceno = serviceno;
  }


  public String getServicephone() {
    return servicephone;
  }

  public void setServicephone(String servicephone) {
    this.servicephone = servicephone;
  }


  public String getServicepostaddress() {
    return servicepostaddress;
  }

  public void setServicepostaddress(String servicepostaddress) {
    this.servicepostaddress = servicepostaddress;
  }


  public String getComgrade() {
    return comgrade;
  }

  public void setComgrade(String comgrade) {
    this.comgrade = comgrade;
  }


  public String getComareatype() {
    return comareatype;
  }

  public void setComareatype(String comareatype) {
    this.comareatype = comareatype;
  }


  public String getUpcomcode() {
    return upcomcode;
  }

  public void setUpcomcode(String upcomcode) {
    this.upcomcode = upcomcode;
  }


  public String getIsdirunder() {
    return isdirunder;
  }

  public void setIsdirunder(String isdirunder) {
    this.isdirunder = isdirunder;
  }


  public String getComareatype1() {
    return comareatype1;
  }

  public void setComareatype1(String comareatype1) {
    this.comareatype1 = comareatype1;
  }


  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }


  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }


  public String getFindb() {
    return findb;
  }

  public void setFindb(String findb) {
    this.findb = findb;
  }


  public String getLetterservicename() {
    return letterservicename;
  }

  public void setLetterservicename(String letterservicename) {
    this.letterservicename = letterservicename;
  }


  public String getComstate() {
    return comstate;
  }

  public void setComstate(String comstate) {
    this.comstate = comstate;
  }


  public String getCompreparedate() {
    return compreparedate;
  }

  public void setCompreparedate(String compreparedate) {
    this.compreparedate = compreparedate;
  }


  public String getSupmanagedate() {
    return supmanagedate;
  }

  public void setSupmanagedate(String supmanagedate) {
    this.supmanagedate = supmanagedate;
  }


  public String getSupapprovaldate() {
    return supapprovaldate;
  }

  public void setSupapprovaldate(String supapprovaldate) {
    this.supapprovaldate = supapprovaldate;
  }


  public String getTheopeningdate() {
    return theopeningdate;
  }

  public void setTheopeningdate(String theopeningdate) {
    this.theopeningdate = theopeningdate;
  }


  public String getComreplycanceldate() {
    return comreplycanceldate;
  }

  public void setComreplycanceldate(String comreplycanceldate) {
    this.comreplycanceldate = comreplycanceldate;
  }


  public String getSupreplycanceldate() {
    return supreplycanceldate;
  }

  public void setSupreplycanceldate(String supreplycanceldate) {
    this.supreplycanceldate = supreplycanceldate;
  }


  public String getBuspapercanceldate() {
    return buspapercanceldate;
  }

  public void setBuspapercanceldate(String buspapercanceldate) {
    this.buspapercanceldate = buspapercanceldate;
  }

}
