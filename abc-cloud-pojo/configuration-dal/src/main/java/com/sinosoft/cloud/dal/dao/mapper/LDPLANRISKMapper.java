package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LDPLANRISK;
import com.sinosoft.cloud.dal.dao.model.LDPLANRISKKey;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SettleInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LDPLANRISKMapper {
    int deleteByPrimaryKey(LDPLANRISKKey key);

    int insert(LDPLANRISK record);

    int insertSelective(LDPLANRISK record);

    LDPLANRISK selectByPrimaryKey(String key);

    int updateByPrimaryKeySelective(LDPLANRISK record);

    int updateByPrimaryKey(LDPLANRISK record);

    LDPLANRISK selectssss();

    LDPLANRISK selectByParam(@Param(value = "mainriskcode") String mainriskcode, @Param(value = "countplanname") String countplanname);

    List<ProInfo> selectOnlyProName();

    List<ProInfo> selectOnlyProCode();

    List<ProInfo> selectByProCode(@Param(value = "proCode") String proCode);

    List<ProInfo> selectNameAndCode();

    List<ProInfo> selectCodeByUser(@Param(value = "admin") String admin);
}