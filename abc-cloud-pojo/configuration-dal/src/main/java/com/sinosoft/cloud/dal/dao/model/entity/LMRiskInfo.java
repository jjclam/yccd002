package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class LMRiskInfo implements Serializable {
    private String contplancode;

    private String contplanname;

    private String riskcode;

    private String riskname;

    private String dutycode;

    private String dutyname;

    private String insuyear;


    public String getContplancode() {
        return contplancode;
    }

    public void setContplancode(String contplancode) {
        this.contplancode = contplancode;
    }

    public String getContplanname() {
        return contplanname;
    }

    public void setContplanname(String contplanname) {
        this.contplanname = contplanname;
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode;
    }

    public String getRiskname() {
        return riskname;
    }

    public void setRiskname(String riskname) {
        this.riskname = riskname;
    }

    public String getDutycode() {
        return dutycode;
    }

    public void setDutycode(String dutycode) {
        this.dutycode = dutycode;
    }

    public String getDutyname() {
        return dutyname;
    }

    public void setDutyname(String dutyname) {
        this.dutyname = dutyname;
    }

    public String getInsuyear() {
        return insuyear;
    }

    public void setInsuyear(String insuyear) {
        this.insuyear = insuyear;
    }

    @Override
    public String toString() {
        return "LMRiskInfo{" +
                "contplancode='" + contplancode + '\'' +
                ", contplanname='" + contplanname + '\'' +
                ", riskcode='" + riskcode + '\'' +
                ", riskname='" + riskname + '\'' +
                ", dutycode='" + dutycode + '\'' +
                ", dutyname='" + dutyname + '\'' +
                ", insuyear='" + insuyear + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LMRiskInfo that = (LMRiskInfo) o;
        return Objects.equals(contplancode, that.contplancode) &&
                Objects.equals(contplanname, that.contplanname) &&
                Objects.equals(riskcode, that.riskcode) &&
                Objects.equals(riskname, that.riskname) &&
                Objects.equals(dutycode, that.dutycode) &&
                Objects.equals(dutyname, that.dutyname) &&
                Objects.equals(insuyear, that.insuyear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contplancode, contplanname, riskcode, riskname, dutycode, dutyname, insuyear);
    }
}

