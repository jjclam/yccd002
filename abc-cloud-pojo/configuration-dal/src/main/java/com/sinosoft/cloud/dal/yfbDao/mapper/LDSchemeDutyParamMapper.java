package com.sinosoft.cloud.dal.yfbDao.mapper;

import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeDutyParam;
import com.sinosoft.cloud.dal.yfbDao.model.entity.SchemeParams;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LDSchemeDutyParamMapper {
    Integer countSchemeInfoForView(@Param(value = "schemecode") String schemecode);

    List<LDSchemeDutyParam> getSchemeInfoListForView(@Param(value = "schemecode") String schemecode);

    List<LDSchemeDutyParam> getSchemeInfoListForRiskCode(@Param(value = "schemecode") String schemecode);

    List<LDSchemeDutyParam> getSchemeInfoListForDutycode(@Param(value = "schemecode") String schemecode,
                                                       @Param(value = "riskcode") String riskcode);

    List<LDSchemeDutyParam> getSchemeInfoListForViewParams(@Param(value = "schemecode") String schemecode,
                                                         @Param(value = "riskcode") String riskcode,
                                                         @Param(value = "dutycode") String dutycode);


    SchemeParams getSchemeInfoListForParams(@Param(value = "schemecode") String schemecode);
}