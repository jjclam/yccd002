package com.sinosoft.cloud.dal.yfbDao.model;

public class Lacom {

    private String  agentcom;

    private String  managecom;

    private String  name;

    public Lacom() {
    }

    public Lacom(String agentcom, String managecom, String name) {
        this.agentcom = agentcom;
        this.managecom = managecom;
        this.name = name;
    }

    public String getAgentcom() {
        return agentcom;
    }

    public void setAgentcom(String agentcom) {
        this.agentcom = agentcom;
    }

    public String getManagecom() {
        return managecom;
    }

    public void setManagecom(String managecom) {
        this.managecom = managecom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
