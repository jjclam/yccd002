package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;

public class UserMgr implements Serializable {
    /**
     * 用户ID
     *
     * @mbggenerated
     */
    private String id;
    /**
     * 用户账号
     *
     * @mbggenerated
     */
    private String userName;
    /**
     * 密码
     *
     * @mbggenerated
     */
    private String password;
    /**
     * 用户昵称
     *
     * @mbggenerated
     */
    private String nickName;
    /**
     * 用户类型（00系统用户）
     *
     * @mbggenerated
     */
    private String userType;
    /**
     * 邮箱
     *
     * @mbggenerated
     */
    private String email;
    /**
     * 用户手机
     *
     * @mbggenerated
     */
    private String telephone;
    /**
     * 用户电话
     *
     * @mbggenerated
     */
    private String phonenumber;
    /**
     * '用户性别（0男 1女 2未知）
     *
     * @mbggenerated
     */
    private String sex;
    /**
     * 头像地址
     *
     * @mbggenerated
     */
    private String avatar;
    /**
     * 机构
     *
     * @mbggenerated
     */
    private String institutions;
    /**
     * 外勤（0外勤 1企业HR，2中台）
     *
     * @mbggenerated
     */
    private String field;
    /**
     * 帐号状态（0正常 1停用';
     *
     * @mbggenerated
     */
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     *
     * @mbggenerated
     */
    private String delFlag;
    /**
     * 最后登陆时间
     *
     * @mbggenerated
     */
    private Date loginDate;
    /**
     * 创建者
     *
     * @mbggenerated
     */
    private String createBy;
    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createDate;
    /**
     * 修改者
     *
     * @mbggenerated
     */
    private String updateBy;
    /**
     * 修改时间
     *
     * @mbggenerated
     */
    private Date updateDate;
    /**
     * 备注
     *
     * @mbggenerated
     */
    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getInstitutions() {
        return institutions;
    }

    public void setInstitutions(String institutions) {
        this.institutions = institutions;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}