package com.sinosoft.cloud.dal.yfbDao.model;

public class LDSchemeDutyParam {
    private String selno;

    private String mainriskcode;

    private String mainriskversion;

    private String riskcode;

    private String riskversion;

    private String contschemecode;

    private String contschemename;

    private String dutycode;

    private String calfactor;

    private String calfactortype;

    private String calfactorvalue;

    private String remark;

    private String schemetype;

    private String portfolioflag;

    public String getSelno() {
        return selno;
    }

    public void setSelno(String selno) {
        this.selno = selno;
    }

    public String getMainriskcode() {
        return mainriskcode;
    }

    public void setMainriskcode(String mainriskcode) {
        this.mainriskcode = mainriskcode;
    }

    public String getMainriskversion() {
        return mainriskversion;
    }

    public void setMainriskversion(String mainriskversion) {
        this.mainriskversion = mainriskversion;
    }

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode;
    }

    public String getRiskversion() {
        return riskversion;
    }

    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion;
    }

    public String getContschemecode() {
        return contschemecode;
    }

    public void setContschemecode(String contschemecode) {
        this.contschemecode = contschemecode;
    }

    public String getContschemename() {
        return contschemename;
    }

    public void setContschemename(String contschemename) {
        this.contschemename = contschemename;
    }

    public String getDutycode() {
        return dutycode;
    }

    public void setDutycode(String dutycode) {
        this.dutycode = dutycode;
    }

    public String getCalfactor() {
        return calfactor;
    }

    public void setCalfactor(String calfactor) {
        this.calfactor = calfactor;
    }

    public String getCalfactortype() {
        return calfactortype;
    }

    public void setCalfactortype(String calfactortype) {
        this.calfactortype = calfactortype;
    }

    public String getCalfactorvalue() {
        return calfactorvalue;
    }

    public void setCalfactorvalue(String calfactorvalue) {
        this.calfactorvalue = calfactorvalue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSchemetype() {
        return schemetype;
    }

    public void setSchemetype(String schemetype) {
        this.schemetype = schemetype;
    }

    public String getPortfolioflag() {
        return portfolioflag;
    }

    public void setPortfolioflag(String portfolioflag) {
        this.portfolioflag = portfolioflag;
    }
}