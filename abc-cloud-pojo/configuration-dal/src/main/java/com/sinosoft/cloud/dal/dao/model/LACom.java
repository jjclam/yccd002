package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.Date;

public class LACom implements Serializable {
    /**
     * 代理机构
     *
     * @mbggenerated
     */
    private String AGENTCOM;

    /**
     * 管理机构
     *
     * @mbggenerated
     */
    private String MANAGECOM;

    /**
     * 地区类型
     *
     * @mbggenerated
     */
    private String AREATYPE;

    /**
     * 渠道类型
     *
     * @mbggenerated
     */
    private String CHANNELTYPE;

    /**
     * 上级代理机构
     *
     * @mbggenerated
     */
    private String UPAGENTCOM;

    /**
     * 机构名称
     *
     * @mbggenerated
     */
    private String NAME;

    /**
     * 机构注册地址
     *
     * @mbggenerated
     */
    private String ADDRESS;

    /**
     * 机构邮编
     *
     * @mbggenerated
     */
    private String ZIPCODE;

    /**
     * 机构电话
     *
     * @mbggenerated
     */
    private String PHONE;

    /**
     * 机构传真
     *
     * @mbggenerated
     */
    private String FAX;

    /**
     * EMail
     *
     * @mbggenerated
     */
    private String EMAIL;

    /**
     * 网址
     *
     * @mbggenerated
     */
    private String WEBADDRESS;

    /**
     * 负责人
     *
     * @mbggenerated
     */
    private String LINKMAN;

    /**
     * 密码
     *
     * @mbggenerated
     */
    private String PASSWORD;

    /**
     * 法人
     *
     * @mbggenerated
     */
    private String CORPORATION;

    /**
     * 银行编码
     *
     * @mbggenerated
     */
    private String BANKCODE;

    /**
     * 银行帐号
     *
     * @mbggenerated
     */
    private String BANKACCNO;

    /**
     * 行业分类
     *
     * @mbggenerated
     */
    private String BUSINESSTYPE;

    /**
     * 单位性质
     *
     * @mbggenerated
     */
    private String GRPNATURE;

    /**
     * 中介机构类别
     *
     * @mbggenerated
     */
    private String ACTYPE;

    /**
     * 销售资格
     *
     * @mbggenerated
     */
    private String SELLFLAG;

    /**
     * 操作员代码
     *
     * @mbggenerated
     */
    private String OPERATOR;

    /**
     * 入机日期
     *
     * @mbggenerated
     */
    private Date MAKEDATE;

    /**
     * 入机时间
     *
     * @mbggenerated
     */
    private String MAKETIME;

    /**
     * 最后一次修改日期
     *
     * @mbggenerated
     */
    private Date MODIFYDATE;

    /**
     * 最后一次修改时间
     *
     * @mbggenerated
     */
    private String MODIFYTIME;

    /**
     * 银行级别
     *
     * @mbggenerated
     */
    private String BANKTYPE;

    /**
     * 是否统计网点合格率
     *
     * @mbggenerated
     */
    private String CALFLAG;

    /**
     * 工商执照编码
     *
     * @mbggenerated
     */
    private String BUSILICENSECODE;

    /**
     * 保险公司ID
     *
     * @mbggenerated
     */
    private String INSUREID;

    /**
     * 保险公司负责人
     *
     * @mbggenerated
     */
    private String INSUREPRINCIPAL;

    /**
     * 主营业务
     *
     * @mbggenerated
     */
    private String CHIEFBUSINESS;

    /**
     * 营业地址
     *
     * @mbggenerated
     */
    private String BUSIADDRESS;

    /**
     * 签署人
     *
     * @mbggenerated
     */
    private String SUBSCRIBEMAN;

    /**
     * 签署人职务
     *
     * @mbggenerated
     */
    private String SUBSCRIBEMANDUTY;

    /**
     * 许可证号码
     *
     * @mbggenerated
     */
    private String LICENSENO;

    /**
     * 行政区划代码
     *
     * @mbggenerated
     */
    private String REGIONALISMCODE;

    /**
     * 上报代码
     *
     * @mbggenerated
     */
    private String APPAGENTCOM;

    /**
     * 机构状态
     *
     * @mbggenerated
     */
    private String STATE;

    /**
     * 相关说明
     *
     * @mbggenerated
     */
    private String NOTI;

    /**
     * 行业代码
     *
     * @mbggenerated
     */
    private String BUSINESSCODE;

    /**
     * 许可证登记日期
     *
     * @mbggenerated
     */
    private Date LICENSESTARTDATE;

    /**
     * 许可证截至日期
     *
     * @mbggenerated
     */
    private Date LICENSEENDDATE;

    /**
     * 展业类型
     *
     * @mbggenerated
     */
    private String BRANCHTYPE;

    /**
     * 渠道
     *
     * @mbggenerated
     */
    private String BRANCHTYPE2;

    /**
     * 资产
     *
     * @mbggenerated
     */
    private Float ASSETS;

    /**
     * 营业收入
     *
     * @mbggenerated
     */
    private Float INCOME;

    /**
     * 营业利润
     *
     * @mbggenerated
     */
    private Float PROFITS;

    /**
     * 机构人数
     *
     * @mbggenerated
     */
    private Float PERSONNALSUM;

    /**
     * 合同编码
     *
     * @mbggenerated
     */
    private String PROTOCALNO;

    /**
     * 所属总行
     *
     * @mbggenerated
     */
    private String HEADOFFICE;

    /**
     * 成立日期
     *
     * @mbggenerated
     */
    private Date FOUNDDATE;

    /**
     * 停业日期
     *
     * @mbggenerated
     */
    private Date ENDDATE;

    /**
     * 开户银行
     *
     * @mbggenerated
     */
    private String BANK;

    /**
     * 账户名称
     *
     * @mbggenerated
     */
    private String ACCNAME;

    /**
     * 授权领取人姓名
     *
     * @mbggenerated
     */
    private String DRAWER;

    /**
     * 授权领取人银行账号
     *
     * @mbggenerated
     */
    private String DRAWERACCNO;

    /**
     * 上级管理机构
     *
     * @mbggenerated
     */
    private String REPMANAGECOM;

    /**
     * 授权领取人银行编码
     *
     * @mbggenerated
     */
    private String DRAWERACCCODE;

    /**
     * 授权领取人银行名称
     *
     * @mbggenerated
     */
    private String DRAWERACCNAME;

    /**
     * 中介机构渠道类别
     *
     * @mbggenerated
     */
    private String CHANNELTYPE2;

    /**
     * 区域类型
     *
     * @mbggenerated
     */
    private String AREATYPE2;

    private static final long serialVersionUID = 1L;

    public String getAGENTCOM() {
        return AGENTCOM;
    }

    public void setAGENTCOM(String AGENTCOM) {
        this.AGENTCOM = AGENTCOM;
    }

    public String getMANAGECOM() {
        return MANAGECOM;
    }

    public void setMANAGECOM(String MANAGECOM) {
        this.MANAGECOM = MANAGECOM;
    }

    public String getAREATYPE() {
        return AREATYPE;
    }

    public void setAREATYPE(String AREATYPE) {
        this.AREATYPE = AREATYPE;
    }

    public String getCHANNELTYPE() {
        return CHANNELTYPE;
    }

    public void setCHANNELTYPE(String CHANNELTYPE) {
        this.CHANNELTYPE = CHANNELTYPE;
    }

    public String getUPAGENTCOM() {
        return UPAGENTCOM;
    }

    public void setUPAGENTCOM(String UPAGENTCOM) {
        this.UPAGENTCOM = UPAGENTCOM;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getZIPCODE() {
        return ZIPCODE;
    }

    public void setZIPCODE(String ZIPCODE) {
        this.ZIPCODE = ZIPCODE;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public String getFAX() {
        return FAX;
    }

    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getWEBADDRESS() {
        return WEBADDRESS;
    }

    public void setWEBADDRESS(String WEBADDRESS) {
        this.WEBADDRESS = WEBADDRESS;
    }

    public String getLINKMAN() {
        return LINKMAN;
    }

    public void setLINKMAN(String LINKMAN) {
        this.LINKMAN = LINKMAN;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getCORPORATION() {
        return CORPORATION;
    }

    public void setCORPORATION(String CORPORATION) {
        this.CORPORATION = CORPORATION;
    }

    public String getBANKCODE() {
        return BANKCODE;
    }

    public void setBANKCODE(String BANKCODE) {
        this.BANKCODE = BANKCODE;
    }

    public String getBANKACCNO() {
        return BANKACCNO;
    }

    public void setBANKACCNO(String BANKACCNO) {
        this.BANKACCNO = BANKACCNO;
    }

    public String getBUSINESSTYPE() {
        return BUSINESSTYPE;
    }

    public void setBUSINESSTYPE(String BUSINESSTYPE) {
        this.BUSINESSTYPE = BUSINESSTYPE;
    }

    public String getGRPNATURE() {
        return GRPNATURE;
    }

    public void setGRPNATURE(String GRPNATURE) {
        this.GRPNATURE = GRPNATURE;
    }

    public String getACTYPE() {
        return ACTYPE;
    }

    public void setACTYPE(String ACTYPE) {
        this.ACTYPE = ACTYPE;
    }

    public String getSELLFLAG() {
        return SELLFLAG;
    }

    public void setSELLFLAG(String SELLFLAG) {
        this.SELLFLAG = SELLFLAG;
    }

    public String getOPERATOR() {
        return OPERATOR;
    }

    public void setOPERATOR(String OPERATOR) {
        this.OPERATOR = OPERATOR;
    }

    public Date getMAKEDATE() {
        return MAKEDATE;
    }

    public void setMAKEDATE(Date MAKEDATE) {
        this.MAKEDATE = MAKEDATE;
    }

    public String getMAKETIME() {
        return MAKETIME;
    }

    public void setMAKETIME(String MAKETIME) {
        this.MAKETIME = MAKETIME;
    }

    public Date getMODIFYDATE() {
        return MODIFYDATE;
    }

    public void setMODIFYDATE(Date MODIFYDATE) {
        this.MODIFYDATE = MODIFYDATE;
    }

    public String getMODIFYTIME() {
        return MODIFYTIME;
    }

    public void setMODIFYTIME(String MODIFYTIME) {
        this.MODIFYTIME = MODIFYTIME;
    }

    public String getBANKTYPE() {
        return BANKTYPE;
    }

    public void setBANKTYPE(String BANKTYPE) {
        this.BANKTYPE = BANKTYPE;
    }

    public String getCALFLAG() {
        return CALFLAG;
    }

    public void setCALFLAG(String CALFLAG) {
        this.CALFLAG = CALFLAG;
    }

    public String getBUSILICENSECODE() {
        return BUSILICENSECODE;
    }

    public void setBUSILICENSECODE(String BUSILICENSECODE) {
        this.BUSILICENSECODE = BUSILICENSECODE;
    }

    public String getINSUREID() {
        return INSUREID;
    }

    public void setINSUREID(String INSUREID) {
        this.INSUREID = INSUREID;
    }

    public String getINSUREPRINCIPAL() {
        return INSUREPRINCIPAL;
    }

    public void setINSUREPRINCIPAL(String INSUREPRINCIPAL) {
        this.INSUREPRINCIPAL = INSUREPRINCIPAL;
    }

    public String getCHIEFBUSINESS() {
        return CHIEFBUSINESS;
    }

    public void setCHIEFBUSINESS(String CHIEFBUSINESS) {
        this.CHIEFBUSINESS = CHIEFBUSINESS;
    }

    public String getBUSIADDRESS() {
        return BUSIADDRESS;
    }

    public void setBUSIADDRESS(String BUSIADDRESS) {
        this.BUSIADDRESS = BUSIADDRESS;
    }

    public String getSUBSCRIBEMAN() {
        return SUBSCRIBEMAN;
    }

    public void setSUBSCRIBEMAN(String SUBSCRIBEMAN) {
        this.SUBSCRIBEMAN = SUBSCRIBEMAN;
    }

    public String getSUBSCRIBEMANDUTY() {
        return SUBSCRIBEMANDUTY;
    }

    public void setSUBSCRIBEMANDUTY(String SUBSCRIBEMANDUTY) {
        this.SUBSCRIBEMANDUTY = SUBSCRIBEMANDUTY;
    }

    public String getLICENSENO() {
        return LICENSENO;
    }

    public void setLICENSENO(String LICENSENO) {
        this.LICENSENO = LICENSENO;
    }

    public String getREGIONALISMCODE() {
        return REGIONALISMCODE;
    }

    public void setREGIONALISMCODE(String REGIONALISMCODE) {
        this.REGIONALISMCODE = REGIONALISMCODE;
    }

    public String getAPPAGENTCOM() {
        return APPAGENTCOM;
    }

    public void setAPPAGENTCOM(String APPAGENTCOM) {
        this.APPAGENTCOM = APPAGENTCOM;
    }

    public String getSTATE() {
        return STATE;
    }

    public void setSTATE(String STATE) {
        this.STATE = STATE;
    }

    public String getNOTI() {
        return NOTI;
    }

    public void setNOTI(String NOTI) {
        this.NOTI = NOTI;
    }

    public String getBUSINESSCODE() {
        return BUSINESSCODE;
    }

    public void setBUSINESSCODE(String BUSINESSCODE) {
        this.BUSINESSCODE = BUSINESSCODE;
    }

    public Date getLICENSESTARTDATE() {
        return LICENSESTARTDATE;
    }

    public void setLICENSESTARTDATE(Date LICENSESTARTDATE) {
        this.LICENSESTARTDATE = LICENSESTARTDATE;
    }

    public Date getLICENSEENDDATE() {
        return LICENSEENDDATE;
    }

    public void setLICENSEENDDATE(Date LICENSEENDDATE) {
        this.LICENSEENDDATE = LICENSEENDDATE;
    }

    public String getBRANCHTYPE() {
        return BRANCHTYPE;
    }

    public void setBRANCHTYPE(String BRANCHTYPE) {
        this.BRANCHTYPE = BRANCHTYPE;
    }

    public String getBRANCHTYPE2() {
        return BRANCHTYPE2;
    }

    public void setBRANCHTYPE2(String BRANCHTYPE2) {
        this.BRANCHTYPE2 = BRANCHTYPE2;
    }

    public Float getASSETS() {
        return ASSETS;
    }

    public void setASSETS(Float ASSETS) {
        this.ASSETS = ASSETS;
    }

    public Float getINCOME() {
        return INCOME;
    }

    public void setINCOME(Float INCOME) {
        this.INCOME = INCOME;
    }

    public Float getPROFITS() {
        return PROFITS;
    }

    public void setPROFITS(Float PROFITS) {
        this.PROFITS = PROFITS;
    }

    public Float getPERSONNALSUM() {
        return PERSONNALSUM;
    }

    public void setPERSONNALSUM(Float PERSONNALSUM) {
        this.PERSONNALSUM = PERSONNALSUM;
    }

    public String getPROTOCALNO() {
        return PROTOCALNO;
    }

    public void setPROTOCALNO(String PROTOCALNO) {
        this.PROTOCALNO = PROTOCALNO;
    }

    public String getHEADOFFICE() {
        return HEADOFFICE;
    }

    public void setHEADOFFICE(String HEADOFFICE) {
        this.HEADOFFICE = HEADOFFICE;
    }

    public Date getFOUNDDATE() {
        return FOUNDDATE;
    }

    public void setFOUNDDATE(Date FOUNDDATE) {
        this.FOUNDDATE = FOUNDDATE;
    }

    public Date getENDDATE() {
        return ENDDATE;
    }

    public void setENDDATE(Date ENDDATE) {
        this.ENDDATE = ENDDATE;
    }

    public String getBANK() {
        return BANK;
    }

    public void setBANK(String BANK) {
        this.BANK = BANK;
    }

    public String getACCNAME() {
        return ACCNAME;
    }

    public void setACCNAME(String ACCNAME) {
        this.ACCNAME = ACCNAME;
    }

    public String getDRAWER() {
        return DRAWER;
    }

    public void setDRAWER(String DRAWER) {
        this.DRAWER = DRAWER;
    }

    public String getDRAWERACCNO() {
        return DRAWERACCNO;
    }

    public void setDRAWERACCNO(String DRAWERACCNO) {
        this.DRAWERACCNO = DRAWERACCNO;
    }

    public String getREPMANAGECOM() {
        return REPMANAGECOM;
    }

    public void setREPMANAGECOM(String REPMANAGECOM) {
        this.REPMANAGECOM = REPMANAGECOM;
    }

    public String getDRAWERACCCODE() {
        return DRAWERACCCODE;
    }

    public void setDRAWERACCCODE(String DRAWERACCCODE) {
        this.DRAWERACCCODE = DRAWERACCCODE;
    }

    public String getDRAWERACCNAME() {
        return DRAWERACCNAME;
    }

    public void setDRAWERACCNAME(String DRAWERACCNAME) {
        this.DRAWERACCNAME = DRAWERACCNAME;
    }

    public String getCHANNELTYPE2() {
        return CHANNELTYPE2;
    }

    public void setCHANNELTYPE2(String CHANNELTYPE2) {
        this.CHANNELTYPE2 = CHANNELTYPE2;
    }

    public String getAREATYPE2() {
        return AREATYPE2;
    }

    public void setAREATYPE2(String AREATYPE2) {
        this.AREATYPE2 = AREATYPE2;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", AGENTCOM=").append(AGENTCOM);
        sb.append(", MANAGECOM=").append(MANAGECOM);
        sb.append(", AREATYPE=").append(AREATYPE);
        sb.append(", CHANNELTYPE=").append(CHANNELTYPE);
        sb.append(", UPAGENTCOM=").append(UPAGENTCOM);
        sb.append(", NAME=").append(NAME);
        sb.append(", ADDRESS=").append(ADDRESS);
        sb.append(", ZIPCODE=").append(ZIPCODE);
        sb.append(", PHONE=").append(PHONE);
        sb.append(", FAX=").append(FAX);
        sb.append(", EMAIL=").append(EMAIL);
        sb.append(", WEBADDRESS=").append(WEBADDRESS);
        sb.append(", LINKMAN=").append(LINKMAN);
        sb.append(", PASSWORD=").append(PASSWORD);
        sb.append(", CORPORATION=").append(CORPORATION);
        sb.append(", BANKCODE=").append(BANKCODE);
        sb.append(", BANKACCNO=").append(BANKACCNO);
        sb.append(", BUSINESSTYPE=").append(BUSINESSTYPE);
        sb.append(", GRPNATURE=").append(GRPNATURE);
        sb.append(", ACTYPE=").append(ACTYPE);
        sb.append(", SELLFLAG=").append(SELLFLAG);
        sb.append(", OPERATOR=").append(OPERATOR);
        sb.append(", MAKEDATE=").append(MAKEDATE);
        sb.append(", MAKETIME=").append(MAKETIME);
        sb.append(", MODIFYDATE=").append(MODIFYDATE);
        sb.append(", MODIFYTIME=").append(MODIFYTIME);
        sb.append(", BANKTYPE=").append(BANKTYPE);
        sb.append(", CALFLAG=").append(CALFLAG);
        sb.append(", BUSILICENSECODE=").append(BUSILICENSECODE);
        sb.append(", INSUREID=").append(INSUREID);
        sb.append(", INSUREPRINCIPAL=").append(INSUREPRINCIPAL);
        sb.append(", CHIEFBUSINESS=").append(CHIEFBUSINESS);
        sb.append(", BUSIADDRESS=").append(BUSIADDRESS);
        sb.append(", SUBSCRIBEMAN=").append(SUBSCRIBEMAN);
        sb.append(", SUBSCRIBEMANDUTY=").append(SUBSCRIBEMANDUTY);
        sb.append(", LICENSENO=").append(LICENSENO);
        sb.append(", REGIONALISMCODE=").append(REGIONALISMCODE);
        sb.append(", APPAGENTCOM=").append(APPAGENTCOM);
        sb.append(", STATE=").append(STATE);
        sb.append(", NOTI=").append(NOTI);
        sb.append(", BUSINESSCODE=").append(BUSINESSCODE);
        sb.append(", LICENSESTARTDATE=").append(LICENSESTARTDATE);
        sb.append(", LICENSEENDDATE=").append(LICENSEENDDATE);
        sb.append(", BRANCHTYPE=").append(BRANCHTYPE);
        sb.append(", BRANCHTYPE2=").append(BRANCHTYPE2);
        sb.append(", ASSETS=").append(ASSETS);
        sb.append(", INCOME=").append(INCOME);
        sb.append(", PROFITS=").append(PROFITS);
        sb.append(", PERSONNALSUM=").append(PERSONNALSUM);
        sb.append(", PROTOCALNO=").append(PROTOCALNO);
        sb.append(", HEADOFFICE=").append(HEADOFFICE);
        sb.append(", FOUNDDATE=").append(FOUNDDATE);
        sb.append(", ENDDATE=").append(ENDDATE);
        sb.append(", BANK=").append(BANK);
        sb.append(", ACCNAME=").append(ACCNAME);
        sb.append(", DRAWER=").append(DRAWER);
        sb.append(", DRAWERACCNO=").append(DRAWERACCNO);
        sb.append(", REPMANAGECOM=").append(REPMANAGECOM);
        sb.append(", DRAWERACCCODE=").append(DRAWERACCCODE);
        sb.append(", DRAWERACCNAME=").append(DRAWERACCNAME);
        sb.append(", CHANNELTYPE2=").append(CHANNELTYPE2);
        sb.append(", AREATYPE2=").append(AREATYPE2);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LACom other = (LACom) that;
        return (this.getAGENTCOM() == null ? other.getAGENTCOM() == null : this.getAGENTCOM().equals(other.getAGENTCOM()))
            && (this.getMANAGECOM() == null ? other.getMANAGECOM() == null : this.getMANAGECOM().equals(other.getMANAGECOM()))
            && (this.getAREATYPE() == null ? other.getAREATYPE() == null : this.getAREATYPE().equals(other.getAREATYPE()))
            && (this.getCHANNELTYPE() == null ? other.getCHANNELTYPE() == null : this.getCHANNELTYPE().equals(other.getCHANNELTYPE()))
            && (this.getUPAGENTCOM() == null ? other.getUPAGENTCOM() == null : this.getUPAGENTCOM().equals(other.getUPAGENTCOM()))
            && (this.getNAME() == null ? other.getNAME() == null : this.getNAME().equals(other.getNAME()))
            && (this.getADDRESS() == null ? other.getADDRESS() == null : this.getADDRESS().equals(other.getADDRESS()))
            && (this.getZIPCODE() == null ? other.getZIPCODE() == null : this.getZIPCODE().equals(other.getZIPCODE()))
            && (this.getPHONE() == null ? other.getPHONE() == null : this.getPHONE().equals(other.getPHONE()))
            && (this.getFAX() == null ? other.getFAX() == null : this.getFAX().equals(other.getFAX()))
            && (this.getEMAIL() == null ? other.getEMAIL() == null : this.getEMAIL().equals(other.getEMAIL()))
            && (this.getWEBADDRESS() == null ? other.getWEBADDRESS() == null : this.getWEBADDRESS().equals(other.getWEBADDRESS()))
            && (this.getLINKMAN() == null ? other.getLINKMAN() == null : this.getLINKMAN().equals(other.getLINKMAN()))
            && (this.getPASSWORD() == null ? other.getPASSWORD() == null : this.getPASSWORD().equals(other.getPASSWORD()))
            && (this.getCORPORATION() == null ? other.getCORPORATION() == null : this.getCORPORATION().equals(other.getCORPORATION()))
            && (this.getBANKCODE() == null ? other.getBANKCODE() == null : this.getBANKCODE().equals(other.getBANKCODE()))
            && (this.getBANKACCNO() == null ? other.getBANKACCNO() == null : this.getBANKACCNO().equals(other.getBANKACCNO()))
            && (this.getBUSINESSTYPE() == null ? other.getBUSINESSTYPE() == null : this.getBUSINESSTYPE().equals(other.getBUSINESSTYPE()))
            && (this.getGRPNATURE() == null ? other.getGRPNATURE() == null : this.getGRPNATURE().equals(other.getGRPNATURE()))
            && (this.getACTYPE() == null ? other.getACTYPE() == null : this.getACTYPE().equals(other.getACTYPE()))
            && (this.getSELLFLAG() == null ? other.getSELLFLAG() == null : this.getSELLFLAG().equals(other.getSELLFLAG()))
            && (this.getOPERATOR() == null ? other.getOPERATOR() == null : this.getOPERATOR().equals(other.getOPERATOR()))
            && (this.getMAKEDATE() == null ? other.getMAKEDATE() == null : this.getMAKEDATE().equals(other.getMAKEDATE()))
            && (this.getMAKETIME() == null ? other.getMAKETIME() == null : this.getMAKETIME().equals(other.getMAKETIME()))
            && (this.getMODIFYDATE() == null ? other.getMODIFYDATE() == null : this.getMODIFYDATE().equals(other.getMODIFYDATE()))
            && (this.getMODIFYTIME() == null ? other.getMODIFYTIME() == null : this.getMODIFYTIME().equals(other.getMODIFYTIME()))
            && (this.getBANKTYPE() == null ? other.getBANKTYPE() == null : this.getBANKTYPE().equals(other.getBANKTYPE()))
            && (this.getCALFLAG() == null ? other.getCALFLAG() == null : this.getCALFLAG().equals(other.getCALFLAG()))
            && (this.getBUSILICENSECODE() == null ? other.getBUSILICENSECODE() == null : this.getBUSILICENSECODE().equals(other.getBUSILICENSECODE()))
            && (this.getINSUREID() == null ? other.getINSUREID() == null : this.getINSUREID().equals(other.getINSUREID()))
            && (this.getINSUREPRINCIPAL() == null ? other.getINSUREPRINCIPAL() == null : this.getINSUREPRINCIPAL().equals(other.getINSUREPRINCIPAL()))
            && (this.getCHIEFBUSINESS() == null ? other.getCHIEFBUSINESS() == null : this.getCHIEFBUSINESS().equals(other.getCHIEFBUSINESS()))
            && (this.getBUSIADDRESS() == null ? other.getBUSIADDRESS() == null : this.getBUSIADDRESS().equals(other.getBUSIADDRESS()))
            && (this.getSUBSCRIBEMAN() == null ? other.getSUBSCRIBEMAN() == null : this.getSUBSCRIBEMAN().equals(other.getSUBSCRIBEMAN()))
            && (this.getSUBSCRIBEMANDUTY() == null ? other.getSUBSCRIBEMANDUTY() == null : this.getSUBSCRIBEMANDUTY().equals(other.getSUBSCRIBEMANDUTY()))
            && (this.getLICENSENO() == null ? other.getLICENSENO() == null : this.getLICENSENO().equals(other.getLICENSENO()))
            && (this.getREGIONALISMCODE() == null ? other.getREGIONALISMCODE() == null : this.getREGIONALISMCODE().equals(other.getREGIONALISMCODE()))
            && (this.getAPPAGENTCOM() == null ? other.getAPPAGENTCOM() == null : this.getAPPAGENTCOM().equals(other.getAPPAGENTCOM()))
            && (this.getSTATE() == null ? other.getSTATE() == null : this.getSTATE().equals(other.getSTATE()))
            && (this.getNOTI() == null ? other.getNOTI() == null : this.getNOTI().equals(other.getNOTI()))
            && (this.getBUSINESSCODE() == null ? other.getBUSINESSCODE() == null : this.getBUSINESSCODE().equals(other.getBUSINESSCODE()))
            && (this.getLICENSESTARTDATE() == null ? other.getLICENSESTARTDATE() == null : this.getLICENSESTARTDATE().equals(other.getLICENSESTARTDATE()))
            && (this.getLICENSEENDDATE() == null ? other.getLICENSEENDDATE() == null : this.getLICENSEENDDATE().equals(other.getLICENSEENDDATE()))
            && (this.getBRANCHTYPE() == null ? other.getBRANCHTYPE() == null : this.getBRANCHTYPE().equals(other.getBRANCHTYPE()))
            && (this.getBRANCHTYPE2() == null ? other.getBRANCHTYPE2() == null : this.getBRANCHTYPE2().equals(other.getBRANCHTYPE2()))
            && (this.getASSETS() == null ? other.getASSETS() == null : this.getASSETS().equals(other.getASSETS()))
            && (this.getINCOME() == null ? other.getINCOME() == null : this.getINCOME().equals(other.getINCOME()))
            && (this.getPROFITS() == null ? other.getPROFITS() == null : this.getPROFITS().equals(other.getPROFITS()))
            && (this.getPERSONNALSUM() == null ? other.getPERSONNALSUM() == null : this.getPERSONNALSUM().equals(other.getPERSONNALSUM()))
            && (this.getPROTOCALNO() == null ? other.getPROTOCALNO() == null : this.getPROTOCALNO().equals(other.getPROTOCALNO()))
            && (this.getHEADOFFICE() == null ? other.getHEADOFFICE() == null : this.getHEADOFFICE().equals(other.getHEADOFFICE()))
            && (this.getFOUNDDATE() == null ? other.getFOUNDDATE() == null : this.getFOUNDDATE().equals(other.getFOUNDDATE()))
            && (this.getENDDATE() == null ? other.getENDDATE() == null : this.getENDDATE().equals(other.getENDDATE()))
            && (this.getBANK() == null ? other.getBANK() == null : this.getBANK().equals(other.getBANK()))
            && (this.getACCNAME() == null ? other.getACCNAME() == null : this.getACCNAME().equals(other.getACCNAME()))
            && (this.getDRAWER() == null ? other.getDRAWER() == null : this.getDRAWER().equals(other.getDRAWER()))
            && (this.getDRAWERACCNO() == null ? other.getDRAWERACCNO() == null : this.getDRAWERACCNO().equals(other.getDRAWERACCNO()))
            && (this.getREPMANAGECOM() == null ? other.getREPMANAGECOM() == null : this.getREPMANAGECOM().equals(other.getREPMANAGECOM()))
            && (this.getDRAWERACCCODE() == null ? other.getDRAWERACCCODE() == null : this.getDRAWERACCCODE().equals(other.getDRAWERACCCODE()))
            && (this.getDRAWERACCNAME() == null ? other.getDRAWERACCNAME() == null : this.getDRAWERACCNAME().equals(other.getDRAWERACCNAME()))
            && (this.getCHANNELTYPE2() == null ? other.getCHANNELTYPE2() == null : this.getCHANNELTYPE2().equals(other.getCHANNELTYPE2()))
            && (this.getAREATYPE2() == null ? other.getAREATYPE2() == null : this.getAREATYPE2().equals(other.getAREATYPE2()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAGENTCOM() == null) ? 0 : getAGENTCOM().hashCode());
        result = prime * result + ((getMANAGECOM() == null) ? 0 : getMANAGECOM().hashCode());
        result = prime * result + ((getAREATYPE() == null) ? 0 : getAREATYPE().hashCode());
        result = prime * result + ((getCHANNELTYPE() == null) ? 0 : getCHANNELTYPE().hashCode());
        result = prime * result + ((getUPAGENTCOM() == null) ? 0 : getUPAGENTCOM().hashCode());
        result = prime * result + ((getNAME() == null) ? 0 : getNAME().hashCode());
        result = prime * result + ((getADDRESS() == null) ? 0 : getADDRESS().hashCode());
        result = prime * result + ((getZIPCODE() == null) ? 0 : getZIPCODE().hashCode());
        result = prime * result + ((getPHONE() == null) ? 0 : getPHONE().hashCode());
        result = prime * result + ((getFAX() == null) ? 0 : getFAX().hashCode());
        result = prime * result + ((getEMAIL() == null) ? 0 : getEMAIL().hashCode());
        result = prime * result + ((getWEBADDRESS() == null) ? 0 : getWEBADDRESS().hashCode());
        result = prime * result + ((getLINKMAN() == null) ? 0 : getLINKMAN().hashCode());
        result = prime * result + ((getPASSWORD() == null) ? 0 : getPASSWORD().hashCode());
        result = prime * result + ((getCORPORATION() == null) ? 0 : getCORPORATION().hashCode());
        result = prime * result + ((getBANKCODE() == null) ? 0 : getBANKCODE().hashCode());
        result = prime * result + ((getBANKACCNO() == null) ? 0 : getBANKACCNO().hashCode());
        result = prime * result + ((getBUSINESSTYPE() == null) ? 0 : getBUSINESSTYPE().hashCode());
        result = prime * result + ((getGRPNATURE() == null) ? 0 : getGRPNATURE().hashCode());
        result = prime * result + ((getACTYPE() == null) ? 0 : getACTYPE().hashCode());
        result = prime * result + ((getSELLFLAG() == null) ? 0 : getSELLFLAG().hashCode());
        result = prime * result + ((getOPERATOR() == null) ? 0 : getOPERATOR().hashCode());
        result = prime * result + ((getMAKEDATE() == null) ? 0 : getMAKEDATE().hashCode());
        result = prime * result + ((getMAKETIME() == null) ? 0 : getMAKETIME().hashCode());
        result = prime * result + ((getMODIFYDATE() == null) ? 0 : getMODIFYDATE().hashCode());
        result = prime * result + ((getMODIFYTIME() == null) ? 0 : getMODIFYTIME().hashCode());
        result = prime * result + ((getBANKTYPE() == null) ? 0 : getBANKTYPE().hashCode());
        result = prime * result + ((getCALFLAG() == null) ? 0 : getCALFLAG().hashCode());
        result = prime * result + ((getBUSILICENSECODE() == null) ? 0 : getBUSILICENSECODE().hashCode());
        result = prime * result + ((getINSUREID() == null) ? 0 : getINSUREID().hashCode());
        result = prime * result + ((getINSUREPRINCIPAL() == null) ? 0 : getINSUREPRINCIPAL().hashCode());
        result = prime * result + ((getCHIEFBUSINESS() == null) ? 0 : getCHIEFBUSINESS().hashCode());
        result = prime * result + ((getBUSIADDRESS() == null) ? 0 : getBUSIADDRESS().hashCode());
        result = prime * result + ((getSUBSCRIBEMAN() == null) ? 0 : getSUBSCRIBEMAN().hashCode());
        result = prime * result + ((getSUBSCRIBEMANDUTY() == null) ? 0 : getSUBSCRIBEMANDUTY().hashCode());
        result = prime * result + ((getLICENSENO() == null) ? 0 : getLICENSENO().hashCode());
        result = prime * result + ((getREGIONALISMCODE() == null) ? 0 : getREGIONALISMCODE().hashCode());
        result = prime * result + ((getAPPAGENTCOM() == null) ? 0 : getAPPAGENTCOM().hashCode());
        result = prime * result + ((getSTATE() == null) ? 0 : getSTATE().hashCode());
        result = prime * result + ((getNOTI() == null) ? 0 : getNOTI().hashCode());
        result = prime * result + ((getBUSINESSCODE() == null) ? 0 : getBUSINESSCODE().hashCode());
        result = prime * result + ((getLICENSESTARTDATE() == null) ? 0 : getLICENSESTARTDATE().hashCode());
        result = prime * result + ((getLICENSEENDDATE() == null) ? 0 : getLICENSEENDDATE().hashCode());
        result = prime * result + ((getBRANCHTYPE() == null) ? 0 : getBRANCHTYPE().hashCode());
        result = prime * result + ((getBRANCHTYPE2() == null) ? 0 : getBRANCHTYPE2().hashCode());
        result = prime * result + ((getASSETS() == null) ? 0 : getASSETS().hashCode());
        result = prime * result + ((getINCOME() == null) ? 0 : getINCOME().hashCode());
        result = prime * result + ((getPROFITS() == null) ? 0 : getPROFITS().hashCode());
        result = prime * result + ((getPERSONNALSUM() == null) ? 0 : getPERSONNALSUM().hashCode());
        result = prime * result + ((getPROTOCALNO() == null) ? 0 : getPROTOCALNO().hashCode());
        result = prime * result + ((getHEADOFFICE() == null) ? 0 : getHEADOFFICE().hashCode());
        result = prime * result + ((getFOUNDDATE() == null) ? 0 : getFOUNDDATE().hashCode());
        result = prime * result + ((getENDDATE() == null) ? 0 : getENDDATE().hashCode());
        result = prime * result + ((getBANK() == null) ? 0 : getBANK().hashCode());
        result = prime * result + ((getACCNAME() == null) ? 0 : getACCNAME().hashCode());
        result = prime * result + ((getDRAWER() == null) ? 0 : getDRAWER().hashCode());
        result = prime * result + ((getDRAWERACCNO() == null) ? 0 : getDRAWERACCNO().hashCode());
        result = prime * result + ((getREPMANAGECOM() == null) ? 0 : getREPMANAGECOM().hashCode());
        result = prime * result + ((getDRAWERACCCODE() == null) ? 0 : getDRAWERACCCODE().hashCode());
        result = prime * result + ((getDRAWERACCNAME() == null) ? 0 : getDRAWERACCNAME().hashCode());
        result = prime * result + ((getCHANNELTYPE2() == null) ? 0 : getCHANNELTYPE2().hashCode());
        result = prime * result + ((getAREATYPE2() == null) ? 0 : getAREATYPE2().hashCode());
        return result;
    }
}