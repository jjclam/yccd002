package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface LDCodeMapper {

    List<LDCode> getCodeTypeList();

    List<LDCode> getCodeValueList();

    List<LDCode> getAllCodeValueInfo(@Param(value = "codeType") String codeType);

    Integer countForCodeMaintainList(Map<String,Object> map);

    List<LDCode> selectForCodeMaintainList(Map<String,Object> map);

    Integer verifybasecodetype(String codeType);

    Integer insertbasecodetype(Map<String,Object> map);

    Integer addCodeMaintain(Map<String,Object> map);

    Integer deleteCodeMaintain(@Param(value = "codeType") String codeType, @Param(value = "code") String codeValue);

    List<LDCode> getChlLargeTypeList();

//    Integer verifycodetype(@Param(value = "chlTypeL") String chlTypeL);
    Integer verifycodetype(Map<String,Object> map);

    Integer insertcodetype(Map<String,Object> map);

    List<LDCode> getSmsparamsList(@Param(value = "codeType") String codeType);

    Integer verifycodetypeName(Map<String,Object> map);

    @Select("select CODE || '-' || CODENAME as channelName from ldcode where CODETYPE = 'sell_cirl'")
    public List<ChlInfo> selectCirlBySe();
}