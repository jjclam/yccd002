package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.entity.*;

import java.util.List;
import java.util.Map;

public interface IntegratedQueryMapper {

    Integer contSearchList(Map<String, Object> map);

    List<ContQueryInfo> getContSearchList(Map<String, Object> map);

    List<ManageInfo> getManage();

    List<AgentComInfo> getAgentCom();

    Integer settleSearchList(Map<String, Object> map);

    List<ContQueryInfo> getSettleSearchList(Map<String, Object> map);

    List<ProInfo> getProCodeName();

    Integer productSearchList(Map<String, Object> map);

    List<ProInfo> getProductSearchList(Map<String, Object> map);

    List<ProInfo> getPlanCodeName();

    Integer planSearchList(Map<String, Object> map);

    List<ProInfo> getPlanSearchList(Map<String, Object> map);

    Integer outletSearchList(Map<String, Object> map);

    List<AgentComInfo> getOutletSearchList(Map<String, Object> map);

    Integer premSearchList(Map<String, Object> map);

    List<PremQueryInfo> getPremSearchList(Map<String, Object> map);

    Integer documentSearchList(Map<String, Object> map);

    List<LZCardInfo> getDocumentSearchList(Map<String, Object> map);

    List<LMRiskInfo> getLmRiskInfo(Map<String, Object> map);
}