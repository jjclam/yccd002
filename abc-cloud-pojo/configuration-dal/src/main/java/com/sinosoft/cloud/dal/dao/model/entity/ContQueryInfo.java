package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class ContQueryInfo implements Serializable {
    private String documentNo;

    private String channelName;

    private String contno;

    private String signState;

    private String managecom;

    private String operator;

    private String appntname;

    private String polapplydate;

    private String polapplyStartDate;

    private String polapplyEndDate;

    private String insuredname;

    private String insuredidno;

    private String settlementStatus;

    private String settlementNumber;

    private String settlementDate;

    private String agentCom;

    private String productcode;

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    public String getSignState() {
        return signState;
    }

    public void setSignState(String signState) {
        this.signState = signState;
    }

    public String getManagecom() {
        return managecom;
    }

    public void setManagecom(String managecom) {
        this.managecom = managecom;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAppntname() {
        return appntname;
    }

    public void setAppntname(String appntname) {
        this.appntname = appntname;
    }

    public String getPolapplydate() {
        return polapplydate;
    }

    public void setPolapplydate(String polapplydate) {
        this.polapplydate = polapplydate;
    }

    public String getPolapplyStartDate() {
        return polapplyStartDate;
    }

    public void setPolapplyStartDate(String polapplyStartDate) {
        this.polapplyStartDate = polapplyStartDate;
    }

    public String getPolapplyEndDate() {
        return polapplyEndDate;
    }

    public void setPolapplyEndDate(String polapplyEndDate) {
        this.polapplyEndDate = polapplyEndDate;
    }

    public String getInsuredname() {
        return insuredname;
    }

    public void setInsuredname(String insuredname) {
        this.insuredname = insuredname;
    }

    public String getInsuredidno() {
        return insuredidno;
    }

    public void setInsuredidno(String insuredidno) {
        this.insuredidno = insuredidno;
    }

    public String getSettlementStatus() {
        return settlementStatus;
    }

    public void setSettlementStatus(String settlementStatus) {
        this.settlementStatus = settlementStatus;
    }

    public String getSettlementNumber() {
        return settlementNumber;
    }

    public void setSettlementNumber(String settlementNumber) {
        this.settlementNumber = settlementNumber;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getAgentCom() {
        return agentCom;
    }

    public void setAgentCom(String agentCom) {
        this.agentCom = agentCom;
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    @Override
    public String toString() {
        return "ContQueryInfo{" +
                "documentNo='" + documentNo + '\'' +
                ", channelName='" + channelName + '\'' +
                ", contno='" + contno + '\'' +
                ", signState='" + signState + '\'' +
                ", managecom='" + managecom + '\'' +
                ", operator='" + operator + '\'' +
                ", appntname='" + appntname + '\'' +
                ", polapplydate='" + polapplydate + '\'' +
                ", polapplyStartDate='" + polapplyStartDate + '\'' +
                ", polapplyEndDate='" + polapplyEndDate + '\'' +
                ", insuredname='" + insuredname + '\'' +
                ", insuredidno='" + insuredidno + '\'' +
                ", settlementStatus='" + settlementStatus + '\'' +
                ", settlementNumber='" + settlementNumber + '\'' +
                ", settlementDate='" + settlementDate + '\'' +
                ", agentCom='" + agentCom + '\'' +
                ", productcode='" + productcode + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContQueryInfo that = (ContQueryInfo) o;
        return Objects.equals(documentNo, that.documentNo) &&
                Objects.equals(channelName, that.channelName) &&
                Objects.equals(contno, that.contno) &&
                Objects.equals(signState, that.signState) &&
                Objects.equals(managecom, that.managecom) &&
                Objects.equals(operator, that.operator) &&
                Objects.equals(appntname, that.appntname) &&
                Objects.equals(polapplydate, that.polapplydate) &&
                Objects.equals(polapplyStartDate, that.polapplyStartDate) &&
                Objects.equals(polapplyEndDate, that.polapplyEndDate) &&
                Objects.equals(insuredname, that.insuredname) &&
                Objects.equals(insuredidno, that.insuredidno) &&
                Objects.equals(settlementStatus, that.settlementStatus) &&
                Objects.equals(settlementNumber, that.settlementNumber) &&
                Objects.equals(settlementDate, that.settlementDate) &&
                Objects.equals(agentCom, that.agentCom) &&
                Objects.equals(productcode, that.productcode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentNo, channelName, contno, signState, managecom, operator, appntname, polapplydate, polapplyStartDate, polapplyEndDate, insuredname, insuredidno, settlementStatus, settlementNumber, settlementDate, agentCom, productcode);
    }
}