package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;

import java.util.List;
import java.util.Map;

public interface LdJobRunLogMapper {
    //查询日志信息
    List<LdJobRunLog> selectByTaskName(Map<String,Object> map);
    //查询任务编码下拉
    List<LDCode> getCodes();
    //查询状态下拉
    List<LDCode> getStates();
    //查询记录总条数
    Integer countForList(Map<String,Object> map);

    int updateDocInfo(Map<String,Object> map);

    int insertDocTrackInfo(Map<String,Object> map);

    int updateDocSaleInfo(Map<String,Object> map);

    int insertDocTrackSaleInfo(Map<String,Object> map);

}
