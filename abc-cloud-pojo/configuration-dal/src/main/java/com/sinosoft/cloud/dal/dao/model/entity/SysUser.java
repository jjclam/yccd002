package com.sinosoft.cloud.dal.dao.model.entity;

import java.io.Serializable;
import java.util.Date;

public class SysUser implements Serializable {
    private String id;

    /**必录项*/
    private String userName;

    /**必录项*/
    private String password;

    /**必录项*/
    private String realName;

    private String email;

    private String positionId;
    /**必录项*/
    private String position;

    private String telephone;

    private String userType;

    private String institutions;

    private String field;

    private String status;

    private String linkedPhone;

    private Date createDate;

    private Date updateDate;

    private String remark;

    private String agentcom;

    private String agentcode;

    private String validstartdate;

    private String validenddate;

    private String ukeyflag;


    public String getAgentcom() {
        return agentcom;
    }

    public void setAgentcom(String agentcom) {
        this.agentcom = agentcom;
    }

    public String getAgentcode() {
        return agentcode;
    }

    public void setAgentcode(String agentcode) {
        this.agentcode = agentcode;
    }

    public String getValidstartdate() {
        return validstartdate;
    }

    public void setValidstartdate(String validstartdate) {
        this.validstartdate = validstartdate;
    }

    public String getValidenddate() {
        return validenddate;
    }

    public void setValidenddate(String validenddate) {
        this.validenddate = validenddate;
    }

    public String getUkeyflag() {
        return ukeyflag;
    }

    public void setUkeyflag(String ukeyflag) {
        this.ukeyflag = ukeyflag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getInstitutions() {
        return institutions;
    }

    public void setInstitutions(String institutions) {
        this.institutions = institutions;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLinkedPhone() {
        return linkedPhone;
    }

    public void setLinkedPhone(String linkedPhone) {
        this.linkedPhone = linkedPhone;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "id='" + id + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", realName='" + realName + '\'' +
                ", email='" + email + '\'' +
                ", positionId='" + positionId + '\'' +
                ", position='" + position + '\'' +
                ", telephone='" + telephone + '\'' +
                ", userType='" + userType + '\'' +
                ", institutions='" + institutions + '\'' +
                ", field='" + field + '\'' +
                ", status='" + status + '\'' +
                ", linkedPhone='" + linkedPhone + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", remark='" + remark + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUser sysUser = (SysUser) o;

        if (id != null ? !id.equals(sysUser.id) : sysUser.id != null) return false;
        if (userName != null ? !userName.equals(sysUser.userName) : sysUser.userName != null) return false;
        if (password != null ? !password.equals(sysUser.password) : sysUser.password != null) return false;
        if (realName != null ? !realName.equals(sysUser.realName) : sysUser.realName != null) return false;
        if (email != null ? !email.equals(sysUser.email) : sysUser.email != null) return false;
        if (positionId != null ? !positionId.equals(sysUser.positionId) : sysUser.positionId != null) return false;
        if (position != null ? !position.equals(sysUser.position) : sysUser.position != null) return false;
        if (telephone != null ? !telephone.equals(sysUser.telephone) : sysUser.telephone != null) return false;
        if (userType != null ? !userType.equals(sysUser.userType) : sysUser.userType != null) return false;
        if (institutions != null ? !institutions.equals(sysUser.institutions) : sysUser.institutions != null)
            return false;
        if (field != null ? !field.equals(sysUser.field) : sysUser.field != null) return false;
        if (status != null ? !status.equals(sysUser.status) : sysUser.status != null) return false;
        if (linkedPhone != null ? !linkedPhone.equals(sysUser.linkedPhone) : sysUser.linkedPhone != null) return false;
        if (createDate != null ? !createDate.equals(sysUser.createDate) : sysUser.createDate != null) return false;
        if (updateDate != null ? !updateDate.equals(sysUser.updateDate) : sysUser.updateDate != null) return false;
        return remark != null ? remark.equals(sysUser.remark) : sysUser.remark == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (realName != null ? realName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (positionId != null ? positionId.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (userType != null ? userType.hashCode() : 0);
        result = 31 * result + (institutions != null ? institutions.hashCode() : 0);
        result = 31 * result + (field != null ? field.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (linkedPhone != null ? linkedPhone.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }
}