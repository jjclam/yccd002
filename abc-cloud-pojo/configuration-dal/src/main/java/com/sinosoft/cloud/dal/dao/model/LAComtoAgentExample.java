package com.sinosoft.cloud.dal.dao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class LAComtoAgentExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public LAComtoAgentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andAGENTCOMIsNull() {
            addCriterion("AGENTCOM is null");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMIsNotNull() {
            addCriterion("AGENTCOM is not null");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMEqualTo(String value) {
            addCriterion("AGENTCOM =", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMNotEqualTo(String value) {
            addCriterion("AGENTCOM <>", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMGreaterThan(String value) {
            addCriterion("AGENTCOM >", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTCOM >=", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMLessThan(String value) {
            addCriterion("AGENTCOM <", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMLessThanOrEqualTo(String value) {
            addCriterion("AGENTCOM <=", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMLike(String value) {
            addCriterion("AGENTCOM like", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMNotLike(String value) {
            addCriterion("AGENTCOM not like", value, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMIn(List<String> values) {
            addCriterion("AGENTCOM in", values, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMNotIn(List<String> values) {
            addCriterion("AGENTCOM not in", values, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMBetween(String value1, String value2) {
            addCriterion("AGENTCOM between", value1, value2, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andAGENTCOMNotBetween(String value1, String value2) {
            addCriterion("AGENTCOM not between", value1, value2, "AGENTCOM");
            return (Criteria) this;
        }

        public Criteria andRELATYPEIsNull() {
            addCriterion("RELATYPE is null");
            return (Criteria) this;
        }

        public Criteria andRELATYPEIsNotNull() {
            addCriterion("RELATYPE is not null");
            return (Criteria) this;
        }

        public Criteria andRELATYPEEqualTo(String value) {
            addCriterion("RELATYPE =", value, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPENotEqualTo(String value) {
            addCriterion("RELATYPE <>", value, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPEGreaterThan(String value) {
            addCriterion("RELATYPE >", value, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPEGreaterThanOrEqualTo(String value) {
            addCriterion("RELATYPE >=", value, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPELessThan(String value) {
            addCriterion("RELATYPE <", value, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPELessThanOrEqualTo(String value) {
            addCriterion("RELATYPE <=", value, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPELike(String value) {
            addCriterion("RELATYPE like", value, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPENotLike(String value) {
            addCriterion("RELATYPE not like", value, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPEIn(List<String> values) {
            addCriterion("RELATYPE in", values, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPENotIn(List<String> values) {
            addCriterion("RELATYPE not in", values, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPEBetween(String value1, String value2) {
            addCriterion("RELATYPE between", value1, value2, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andRELATYPENotBetween(String value1, String value2) {
            addCriterion("RELATYPE not between", value1, value2, "RELATYPE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODEIsNull() {
            addCriterion("AGENTCODE is null");
            return (Criteria) this;
        }

        public Criteria andAGENTCODEIsNotNull() {
            addCriterion("AGENTCODE is not null");
            return (Criteria) this;
        }

        public Criteria andAGENTCODEEqualTo(String value) {
            addCriterion("AGENTCODE =", value, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODENotEqualTo(String value) {
            addCriterion("AGENTCODE <>", value, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODEGreaterThan(String value) {
            addCriterion("AGENTCODE >", value, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODEGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTCODE >=", value, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODELessThan(String value) {
            addCriterion("AGENTCODE <", value, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODELessThanOrEqualTo(String value) {
            addCriterion("AGENTCODE <=", value, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODELike(String value) {
            addCriterion("AGENTCODE like", value, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODENotLike(String value) {
            addCriterion("AGENTCODE not like", value, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODEIn(List<String> values) {
            addCriterion("AGENTCODE in", values, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODENotIn(List<String> values) {
            addCriterion("AGENTCODE not in", values, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODEBetween(String value1, String value2) {
            addCriterion("AGENTCODE between", value1, value2, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTCODENotBetween(String value1, String value2) {
            addCriterion("AGENTCODE not between", value1, value2, "AGENTCODE");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPIsNull() {
            addCriterion("AGENTGROUP is null");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPIsNotNull() {
            addCriterion("AGENTGROUP is not null");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPEqualTo(String value) {
            addCriterion("AGENTGROUP =", value, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPNotEqualTo(String value) {
            addCriterion("AGENTGROUP <>", value, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPGreaterThan(String value) {
            addCriterion("AGENTGROUP >", value, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPGreaterThanOrEqualTo(String value) {
            addCriterion("AGENTGROUP >=", value, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPLessThan(String value) {
            addCriterion("AGENTGROUP <", value, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPLessThanOrEqualTo(String value) {
            addCriterion("AGENTGROUP <=", value, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPLike(String value) {
            addCriterion("AGENTGROUP like", value, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPNotLike(String value) {
            addCriterion("AGENTGROUP not like", value, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPIn(List<String> values) {
            addCriterion("AGENTGROUP in", values, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPNotIn(List<String> values) {
            addCriterion("AGENTGROUP not in", values, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPBetween(String value1, String value2) {
            addCriterion("AGENTGROUP between", value1, value2, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andAGENTGROUPNotBetween(String value1, String value2) {
            addCriterion("AGENTGROUP not between", value1, value2, "AGENTGROUP");
            return (Criteria) this;
        }

        public Criteria andOPERATORIsNull() {
            addCriterion("OPERATOR is null");
            return (Criteria) this;
        }

        public Criteria andOPERATORIsNotNull() {
            addCriterion("OPERATOR is not null");
            return (Criteria) this;
        }

        public Criteria andOPERATOREqualTo(String value) {
            addCriterion("OPERATOR =", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotEqualTo(String value) {
            addCriterion("OPERATOR <>", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORGreaterThan(String value) {
            addCriterion("OPERATOR >", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATOR >=", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLessThan(String value) {
            addCriterion("OPERATOR <", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLessThanOrEqualTo(String value) {
            addCriterion("OPERATOR <=", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORLike(String value) {
            addCriterion("OPERATOR like", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotLike(String value) {
            addCriterion("OPERATOR not like", value, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORIn(List<String> values) {
            addCriterion("OPERATOR in", values, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotIn(List<String> values) {
            addCriterion("OPERATOR not in", values, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORBetween(String value1, String value2) {
            addCriterion("OPERATOR between", value1, value2, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andOPERATORNotBetween(String value1, String value2) {
            addCriterion("OPERATOR not between", value1, value2, "OPERATOR");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIsNull() {
            addCriterion("MAKEDATE is null");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIsNotNull() {
            addCriterion("MAKEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE =", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE <>", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("MAKEDATE >", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE >=", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATELessThan(Date value) {
            addCriterionForJDBCDate("MAKEDATE <", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MAKEDATE <=", value, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEIn(List<Date> values) {
            addCriterionForJDBCDate("MAKEDATE in", values, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("MAKEDATE not in", values, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MAKEDATE between", value1, value2, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKEDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MAKEDATE not between", value1, value2, "MAKEDATE");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIsNull() {
            addCriterion("MAKETIME is null");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIsNotNull() {
            addCriterion("MAKETIME is not null");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEEqualTo(String value) {
            addCriterion("MAKETIME =", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotEqualTo(String value) {
            addCriterion("MAKETIME <>", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEGreaterThan(String value) {
            addCriterion("MAKETIME >", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEGreaterThanOrEqualTo(String value) {
            addCriterion("MAKETIME >=", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELessThan(String value) {
            addCriterion("MAKETIME <", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELessThanOrEqualTo(String value) {
            addCriterion("MAKETIME <=", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMELike(String value) {
            addCriterion("MAKETIME like", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotLike(String value) {
            addCriterion("MAKETIME not like", value, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEIn(List<String> values) {
            addCriterion("MAKETIME in", values, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotIn(List<String> values) {
            addCriterion("MAKETIME not in", values, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMEBetween(String value1, String value2) {
            addCriterion("MAKETIME between", value1, value2, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMAKETIMENotBetween(String value1, String value2) {
            addCriterion("MAKETIME not between", value1, value2, "MAKETIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIsNull() {
            addCriterion("MODIFYDATE is null");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIsNotNull() {
            addCriterion("MODIFYDATE is not null");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE =", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <>", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("MODIFYDATE >", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE >=", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATELessThan(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("MODIFYDATE <=", value, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEIn(List<Date> values) {
            addCriterionForJDBCDate("MODIFYDATE in", values, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("MODIFYDATE not in", values, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MODIFYDATE between", value1, value2, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("MODIFYDATE not between", value1, value2, "MODIFYDATE");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIsNull() {
            addCriterion("MODIFYTIME is null");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIsNotNull() {
            addCriterion("MODIFYTIME is not null");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEEqualTo(String value) {
            addCriterion("MODIFYTIME =", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotEqualTo(String value) {
            addCriterion("MODIFYTIME <>", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEGreaterThan(String value) {
            addCriterion("MODIFYTIME >", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFYTIME >=", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELessThan(String value) {
            addCriterion("MODIFYTIME <", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELessThanOrEqualTo(String value) {
            addCriterion("MODIFYTIME <=", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMELike(String value) {
            addCriterion("MODIFYTIME like", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotLike(String value) {
            addCriterion("MODIFYTIME not like", value, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEIn(List<String> values) {
            addCriterion("MODIFYTIME in", values, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotIn(List<String> values) {
            addCriterion("MODIFYTIME not in", values, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMEBetween(String value1, String value2) {
            addCriterion("MODIFYTIME between", value1, value2, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andMODIFYTIMENotBetween(String value1, String value2) {
            addCriterion("MODIFYTIME not between", value1, value2, "MODIFYTIME");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEIsNull() {
            addCriterion("STARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEIsNotNull() {
            addCriterion("STARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEEqualTo(Date value) {
            addCriterionForJDBCDate("STARTDATE =", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("STARTDATE <>", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("STARTDATE >", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("STARTDATE >=", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATELessThan(Date value) {
            addCriterionForJDBCDate("STARTDATE <", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("STARTDATE <=", value, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEIn(List<Date> values) {
            addCriterionForJDBCDate("STARTDATE in", values, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("STARTDATE not in", values, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("STARTDATE between", value1, value2, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andSTARTDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("STARTDATE not between", value1, value2, "STARTDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andENDDATEIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andENDDATEEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE =", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE <>", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEGreaterThan(Date value) {
            addCriterionForJDBCDate("ENDDATE >", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE >=", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATELessThan(Date value) {
            addCriterionForJDBCDate("ENDDATE <", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATELessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("ENDDATE <=", value, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEIn(List<Date> values) {
            addCriterionForJDBCDate("ENDDATE in", values, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotIn(List<Date> values) {
            addCriterionForJDBCDate("ENDDATE not in", values, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATEBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ENDDATE between", value1, value2, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andENDDATENotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("ENDDATE not between", value1, value2, "ENDDATE");
            return (Criteria) this;
        }

        public Criteria andRATEIsNull() {
            addCriterion("RATE is null");
            return (Criteria) this;
        }

        public Criteria andRATEIsNotNull() {
            addCriterion("RATE is not null");
            return (Criteria) this;
        }

        public Criteria andRATEEqualTo(Float value) {
            addCriterion("RATE =", value, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATENotEqualTo(Float value) {
            addCriterion("RATE <>", value, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATEGreaterThan(Float value) {
            addCriterion("RATE >", value, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATEGreaterThanOrEqualTo(Float value) {
            addCriterion("RATE >=", value, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATELessThan(Float value) {
            addCriterion("RATE <", value, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATELessThanOrEqualTo(Float value) {
            addCriterion("RATE <=", value, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATEIn(List<Float> values) {
            addCriterion("RATE in", values, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATENotIn(List<Float> values) {
            addCriterion("RATE not in", values, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATEBetween(Float value1, Float value2) {
            addCriterion("RATE between", value1, value2, "RATE");
            return (Criteria) this;
        }

        public Criteria andRATENotBetween(Float value1, Float value2) {
            addCriterion("RATE not between", value1, value2, "RATE");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}