package com.sinosoft.cloud.dal.dao.mapper;

import com.sinosoft.cloud.dal.dao.model.LAChannelProduct;
import com.sinosoft.cloud.dal.dao.model.LAChannelProductExample;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface LAChannelProductMapper {
    int countByExample(LAChannelProductExample example);

    int deleteByExample(LAChannelProductExample example);

    int deleteByPrimaryKey(String channelCode);

    int insert(LAChannelProduct record);

    int insertSelective(LAChannelProduct record);

    List<LAChannelProduct> selectByExample(LAChannelProductExample example);

    LAChannelProduct selectByPrimaryKey(@Param("channelCode") String channelCode);

    int updateByExampleSelective(@Param("record") LAChannelProduct record, @Param("example") LAChannelProductExample example);

    int updateByExample(@Param("record") LAChannelProduct record, @Param("example") LAChannelProductExample example);

    int updateByPrimaryKeySelective(LAChannelProduct record);

    int updateByPrimaryKey(LAChannelProduct record);

    List<ChlInfo> selectForChlProInfo(Map<String,Object> map);

    List<LAChannelProduct> selectSaleProInfoByChlCode(@Param("riskType") String riskType);
//    List<LAChannelProduct> selectSaleProInfoByChlCode();

    LAChannelProduct confirmChlProHaved(Map<String,Object> map);

    int updateForEdit(HashMap<String,Object> map);

    int putAwayChlPro(Map<String,Object> map);

    int putdownChlPro(HashMap<String,Object> map);

    List<ChlInfo> selectForChlInfo(Map<String,Object> map);

    List<ChlInfo> selectForChlProExcel(Map<String,Object> map);

    LAChannelProduct selectForChlProEdit(Map<String,Object> map);

    Integer countChlProInfoList(Map<String,Object> map);

    LAChannelProduct selectSaleProInfoByProChlCode(@Param("riskCode") String proCode, @Param("channelCode") String channelCode);

    int verifyChlProExist(Map<String,Object> map);

    int insertPutAwayChlPro(Map<String,Object> map);

    LAChannelProduct getChlProUpDate(@Param("channelCode") String channelCode, @Param("riskCode") String riskCode);

    List<LAChannelProduct> verifyChlProUpStatus(@Param("riskCode") String proCode);

    LAChannelProduct getHttpUrl(@Param("httpUrl") String httpUrl);

    List<ChlInfo> verifyChlExist(@Param("channelCode") String channelCode);
}