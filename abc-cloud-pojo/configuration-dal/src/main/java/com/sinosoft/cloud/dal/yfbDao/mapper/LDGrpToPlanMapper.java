package com.sinosoft.cloud.dal.yfbDao.mapper;

import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrpToPlan;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LDGrpToPlanMapper {

    int countSchemeAuthorize(Map<String,Object> map);

    List<LACom> getSchemeAuthorizeList(Map<String,Object> map);
   // @Select ("select * from LDGRPTOPLAN a where a.customerno= #{customerno}")
    List<LDGrpToPlan> gethavenAuthorize(@Param(value = "customerno") String customerno);

    int authorizeSchemes(Map<String,Object> map);


    int deleteAuthorizedScheme(@Param(value = "customerno") String customerno, @Param(value = "plancode")  String plancode);
}