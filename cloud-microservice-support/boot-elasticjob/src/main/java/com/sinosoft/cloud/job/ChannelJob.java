package com.sinosoft.cloud.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.sinosoft.cloud.service.ChannelJobTaskService;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LdJobRunLogSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
     * Created by  on 2018/8/24.
     */

/**
 * Created by  on 2018/9/13.
 */
@RestController
public class ChannelJob implements SimpleJob {
    private ChannelJobTaskService channelJobTaskService = new ChannelJobTaskService();
    private Logger logger = LoggerFactory.getLogger(ChannelJob.class);
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String tCurrentDate = PubFun.getCurrentDate();
    private String tCurrentTime = PubFun.getCurrentTime();
    private static String SUCCESS = "success";
    private static String FAIL = "fail";
    @Override
    public synchronized void execute(ShardingContext shardingContext) {
        jobBeginLog(shardingContext);//启动日志
        //业务处理
        dealData("");
        jobEndLog(shardingContext);//结束日志
    }

   @RequestMapping("dealData")
    public String dealData(String channelCode) {
        String mResult = channelJobTaskService.deal(channelCode);
        if ("SUCCESS".equals(mResult)){
            tCurrentDate = PubFun.getCurrentDate();
            tCurrentTime = PubFun.getCurrentTime();
            backlog(SUCCESS);
            return mResult;
        } else {
            tCurrentDate = PubFun.getCurrentDate();
            tCurrentTime = PubFun.getCurrentTime();
            backlog(FAIL);
            return mResult;
        }
    }

    private void backlog(String tResult) {
        //记录任务调度日志表 LdJobRunLog
        MMap tMMap = new MMap();
        VData tVData = new VData();
        String serialno = PubFun.CreateSeq("seq_batchno_20191116",10);

        ExeSQL mExeSQL = new  ExeSQL();
        String codeSql = "select code from ldcode where codetype ='jobtype' and codealias = 'ChannelJobTaskService'";
        String taskCode = mExeSQL.getOneValue(codeSql);

        LdJobRunLogSchema tLdJobRunLogSchema = new LdJobRunLogSchema();
        tLdJobRunLogSchema.setSerialNo(serialno);
        tLdJobRunLogSchema.setTaskCode(taskCode);
        tLdJobRunLogSchema.setTaskName("ChannelJobTaskService");
        tLdJobRunLogSchema.setExecuteDate(mCurrentDate);
        tLdJobRunLogSchema.setExecuteTime(mCurrentTime);
        tLdJobRunLogSchema.setFinishDate(tCurrentDate);
        tLdJobRunLogSchema.setFinishTime(tCurrentTime);
        if ("success".equals(tResult)){
            tLdJobRunLogSchema.setExecuteState("1");
        }else if("fail".equals(tResult)){
            tLdJobRunLogSchema.setExecuteState("0");
        }
        tLdJobRunLogSchema.setExecuteResult("同步渠道机构信息批处理执行" + tResult +";");
        tMMap.put(tLdJobRunLogSchema,"DELETE&INSERT");

        tVData.add(tMMap);

        //数据落库
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(tVData);
    }

    /**
     * job 分片启动日志
     * @param shardingContext
     */
    public void jobBeginLog(ShardingContext shardingContext){
        logger.info(String.format("渠道机构信息同步断点回传批处理任务开始：Thread ID: %s, 任务总片数: %s, " +
                        "当前分片项: %s.当前参数: %s,"+
                        "当前任务名称: %s.当前任务参数: %s",Thread.currentThread().getId(),
                shardingContext.getShardingTotalCount(),
                shardingContext.getShardingItem(),
                shardingContext.getShardingParameter(),
                shardingContext.getJobName(),
                shardingContext.getJobParameter()));
    }

    /**
     * job 分片结束日志
     * @param shardingContext
     */
    public void jobEndLog(ShardingContext shardingContext){
        logger.info(String.format("渠道机构信息同步断点回传批处理任务结束：Thread ID: %s, 任务总片数: %s, " +
                        "当前分片项: %s.当前参数: %s,"+
                        "当前任务名称: %s.当前任务参数: %s",Thread.currentThread().getId(),
                shardingContext.getShardingTotalCount(),
                shardingContext.getShardingItem(),
                shardingContext.getShardingParameter(),
                shardingContext.getJobName(),
                shardingContext.getJobParameter()));
    }
}

