package com.sinosoft.cloud.service;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Connection;
import java.sql.SQLException;

@Service("ChannelJobTaskService")
public class ChannelJobTaskService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private MMap tMMap;
    private VData tVData = new VData();
    private ExeSQL tExeSQL;
    private Reflections tReflections = new Reflections();
    private String tResult = "";
    @RequestMapping("deal")
    public String deal(String tChannelCode){
        logger.info("++++++++++++ChannelJobTaskService Start++++++++++++");
        Connection tConnection = DBConnPool.getConnection("lbasedataSource");//核心数据源
        try {
            StringBuffer tSql = new StringBuffer();
            if (!"".equals(tChannelCode)){
                tSql.append("select code1,codename,comcode,code from ldcode1 where codetype = 'selltype' and code = ('05','02') and code1 = '"+tChannelCode+"'");
            }else {
                tSql.append("select code1,codename,comcode,code from ldcode1 where codetype = 'selltype' and code in  ('05','02')");
            }
            LASaleInfoDB mLASaleInfoDB = new LASaleInfoDB();
            LASaleInfoSet mLASaleInfoSet = new LASaleInfoSet();
            tExeSQL = new ExeSQL(tConnection);//建立核心连接
            SSRS tSSRS = tExeSQL.execSQL(tSql.toString());
            tMMap = new MMap();
                if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                    for (int m = 1; m <= tSSRS.getMaxRow(); m++) {
                        mLASaleInfoDB.setChannelCode(tSSRS.GetText(m, 1));
                        mLASaleInfoDB.setSaleChnl(tSSRS.GetText(m, 4));
                        //判断中台是否已经有数据，如果有了就不用添加了
                        mLASaleInfoDB.query();
                        if (!mLASaleInfoDB.getInfo()) {
                            LASaleInfoSchema mLASaleInfoSchema = new LASaleInfoSchema();
                            mLASaleInfoSchema.setChannelCode(tSSRS.GetText(m, 1));
                            mLASaleInfoSchema.setChannelName(tSSRS.GetText(m, 2));
                            mLASaleInfoSchema.setComCode(tSSRS.GetText(m, 3));
                            mLASaleInfoSchema.setSaleChnl(tSSRS.GetText(m, 4));
                            mLASaleInfoSet.add(mLASaleInfoSchema);
                            tResult = "SUCCESS";
                        }else {
                            logger.info("渠道编码" + tChannelCode + "已经同步到中台，不需要再次同步！");
                            tResult = "渠道编码" + tChannelCode + "已经同步到中台，不需要再次同步！";
                        }
                    }
                    tMMap.put(mLASaleInfoSet, "DELETE&INSERT");
                    if (mLASaleInfoSet.size()>0){
                        tResult = "SUCCESS";
//                        tVData.clear();
                        tVData.add(tMMap);
                    }
                }else {
                    logger.info("渠道编码" + tChannelCode + "没有需要同步的数据，请确认编码是否有误！");
                    tResult = "渠道编码" + tChannelCode + "没有需要同步的数据，请确认编码是否有误！";
                }
            //查询核心数据lacom，然后循环插入到schema中，提交到中台库
            LAComDB mLAComDB = new LAComDB(tConnection);
            String lacomSql = "select * from lacom where branchtype = '5'";
            LAComSet mLAComSet= mLAComDB.executeQuery(lacomSql);
            LAComSet saveLAComSet = new LAComSet();
            tExeSQL = new ExeSQL();
            int tCount =  Integer.parseInt(tExeSQL.getOneValue("select count(1) from lacom where branchtype = '5'"));
            if (tCount != mLAComSet.size()) {
                if (mLAComSet != null && mLAComSet.size() > 0) {
                    for (int i = 1; i <= mLAComSet.size(); i++) {
                        LAComSchema mLAComSchema = new LAComSchema();
                        tReflections.transFields(mLAComSchema,mLAComSet.get(i));
                        mLAComSchema.setAssets("");
                        mLAComSchema.setIncome("");
                        mLAComSchema.setProfits("");
                        mLAComSchema.setPersonnalSum("");
                        mLAComSchema.setProtocalNo("");
                        mLAComSchema.setHeadOffice("");
                        mLAComSchema.setBank("");
                        mLAComSchema.setAccName("");
                        mLAComSchema.setDraWer("");
                        mLAComSchema.setDraWerAccNo("");
                        mLAComSchema.setRepManageCom("");
                        mLAComSchema.setDraWerAccCode("");
                        mLAComSchema.setDraWerAccName("");
                        mLAComSchema.setChannelType2("");
                        mLAComSchema.setAreaType2("");
                        mLAComSchema.setAgentCode("");
                        saveLAComSet.add(mLAComSchema);
                        tResult = "SUCCESS";
                    }
                    tMMap.put(saveLAComSet,"DELETE&INSERT");
                    if (saveLAComSet.size()>0){
                        tVData.add(tMMap);
                    }
                }
            }
            //modify by gxw for 渠道信息同步数据新增三张表信息 laagent,lacomtoagent,latree on 2020-1-2
            //laagent
            LAAgentDB mlaAgentDB = new LAAgentDB(tConnection);
            LAAgentSet mLAAgentSet = mlaAgentDB.executeQuery("select * from laagent t1 where t1.branchtype=5");
            LAAgentSet saveLAAgentSet = new LAAgentSet();
            tExeSQL = new ExeSQL();
            int tlaAgentCount =  Integer.parseInt(tExeSQL.getOneValue("select count(1) from laagent where branchtype = '5' "));
            if (tlaAgentCount != mLAAgentSet.size()) {
                if (mLAAgentSet.size() > 0 && mLAAgentSet != null){
                    for (int j = 1; j <= mLAAgentSet.size(); j++) {
                        LAAgentSchema mLAAgentSchema = new LAAgentSchema();
                        String tflag = tExeSQL.getOneValue("select 1 from laagent t1 " +
                                " where t1.branchtype=5 and t1.agentcode = '"+mLAAgentSet.get(j).getAgentCode()+"'" +
                                " and t1.modifydate = '"+mLAAgentSet.get(j).getModifyDate()+"'" +
                                " and t1.modifytime = '"+mLAAgentSet.get(j).getModifyTime()+"'");
                        if ("".equals(tflag)){
                            tReflections.transFields(mLAAgentSchema,mLAAgentSet.get(j));
                            //other cols
                            mLAAgentSchema.setTrainPassFlag("");
                            mLAAgentSchema.setEmergentLink("");
                            mLAAgentSchema.setEmergentPhone("");
                            mLAAgentSchema.setRetainStartDate("");
                            mLAAgentSchema.setRetainEndDate("");
                            mLAAgentSchema.setTogaeFlag("");
                            mLAAgentSchema.setArchieveCode("");
                            mLAAgentSchema.setAffix("");
                            mLAAgentSchema.setAffixName("");
                            mLAAgentSchema.setBankAddress("");
                            mLAAgentSchema.setDXBranchtype2("");
                            mLAAgentSchema.setDXSaleOrg("");
                            saveLAAgentSet.add(mLAAgentSchema);
                            tResult = "SUCCESS";
                        }
                    }
                    tMMap.put(saveLAAgentSet,"DELETE&INSERT");
                    if (saveLAAgentSet.size()>0){
                        tVData.add(tMMap);
                    }
                }
            }
            //lacomtoagent
            LAComToAgentDB mLAComToAgentDB = new LAComToAgentDB(tConnection);
            LAComToAgentSet mLAComToAgentSet = mLAComToAgentDB.executeQuery("select t3.agentcom,t3.relatype,t3.agentcode,t3.agentgroup,t3.operator,t3.makedate,t3.maketime,t3.modifydate,t3.modifytime,t3.startdate,t3.enddate,t3.rate from lacomtoagent t3 where t3.agentcom in(select agentcom from lacom t2 where t2.branchtype = 5)");
            LAComToAgentSet saveLAComToAgentSet = new LAComToAgentSet();
            tExeSQL = new ExeSQL();
            int tLAComToAgentCount =  Integer.parseInt(tExeSQL.getOneValue("select count(*) from lacomtoagent t3 where t3.agentcom in(select agentcom from lacom t2 where t2.branchtype = 5)"));
            if (tLAComToAgentCount != mLAComToAgentSet.size()) {
                if (mLAComToAgentSet.size() > 0 && mLAComToAgentSet != null){
                    for (int k = 1; k <= mLAComToAgentSet.size(); k++) {
                        LAComToAgentSchema mLAComToAgentSchema = new LAComToAgentSchema();
                        tReflections.transFields(mLAComToAgentSchema,mLAComToAgentSet.get(k));
                        tResult = "SUCCESS";
                        saveLAComToAgentSet.add(mLAComToAgentSchema);
                    }
                    tMMap.put(saveLAComToAgentSet,"DELETE&INSERT");
                    if (saveLAComToAgentSet.size()>0){
                        tVData.add(tMMap);
                    }
                }
            }
            //latree -- new create table
            LATreeDB mLATreeDB = new LATreeDB(tConnection);
            LATreeSet mLATreeSet = mLATreeDB.executeQuery("select * from latree t3 where t3.branchtype =5");
            LATreeSet saveLATreeSet = new LATreeSet();
            tExeSQL = new ExeSQL();
            int tLATreeCount = Integer.parseInt(tExeSQL.getOneValue("select count(*) from latree t3 where t3.branchtype =5"));
            if (tLATreeCount != mLATreeSet.size()){
                if ( mLATreeSet.size() > 0 &&  mLATreeSet != null) {
                    for (int l = 1; l <= mLATreeSet.size(); l++) {
                        LATreeSchema mLATreeSchema = new LATreeSchema();
                        tReflections.transFields(mLATreeSchema,mLATreeSet.get(l));
                        tResult = "SUCCESS";
                        saveLATreeSet.add(mLATreeSchema);
                    }
                    tMMap.put(saveLATreeSet,"DELETE&INSERT");
                    if (saveLATreeSet.size()>0){
                        tVData.add(tMMap);
                    }
                }
            }
            //labranchgroup
            LABranchGroupDB mLABranchGroupDB = new LABranchGroupDB(tConnection);
            LABranchGroupSet mLABranchGroupSet = mLABranchGroupDB.executeQuery("select * from labranchgroup where agentgroup in （select agentgroup from laagent t1 where t1.branchtype in （'5','2'))");
            LABranchGroupSet saveLABranchGroupSet = new LABranchGroupSet();
            tExeSQL = new ExeSQL();
            int tLABranchGroupCount = Integer.parseInt(tExeSQL.getOneValue("select count(1) from labranchgroup where agentgroup in （select agentgroup from laagent t1 where t1.branchtype in （'5','2'))"));
            if (tLABranchGroupCount != mLABranchGroupSet.size()) {
                if (mLABranchGroupSet.size() > 0) {
                    for (int n = 1; n <= mLABranchGroupSet.size(); n++) {
                        LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
                        tReflections.transFields(mLABranchGroupSchema,mLABranchGroupSet.get(n));
                        tResult = "SUCCESS";
                        saveLABranchGroupSet.add(mLABranchGroupSchema);
                    }
                    tMMap.put(saveLABranchGroupSet,"DELETE&INSERT");
                    if (saveLABranchGroupSet.size()>0){
                        tVData.add(tMMap);
                    }
                }
            }


            /**   Add By Xcc  增加方案信息同步表 LDSchemeRisk   */
            LDSchemeRiskDB mLDSchemeRiskDB = new LDSchemeRiskDB(tConnection);
            LDSchemeRiskSet mLDSchemeRiskSet =  mLDSchemeRiskDB.executeQuery("select  * from  LDSchemeRisk");

            LDSchemeRiskSet saveLDSchemeRiskSet = new LDSchemeRiskSet();
            tExeSQL = new ExeSQL();
            int tLDSchemeRiskCount = Integer.parseInt(tExeSQL.getOneValue("select count(*) from LDSchemeRisk "));
            if (tLDSchemeRiskCount != mLDSchemeRiskSet.size()){
                if ( mLDSchemeRiskSet.size() > 0 &&  mLDSchemeRiskSet != null) {
                    for (int l = 1; l <= mLDSchemeRiskSet.size(); l++) {
                        LDSchemeRiskSchema mLDSchemeRiskSchema = new LDSchemeRiskSchema();
                        tReflections.transFields(mLDSchemeRiskSchema,mLDSchemeRiskSet.get(l));
                        tResult = "SUCCESS";
                        saveLDSchemeRiskSet.add(mLDSchemeRiskSchema);
                    }
                    tMMap.put(saveLDSchemeRiskSet,"DELETE&INSERT");
                    if (saveLDSchemeRiskSet.size()>0){
                        tVData.add(tMMap);
                    }
                }
            }

            /**   Add By Xcc  增加方案信息同步表 LDSchemeDutyParam   */
            LDSchemeDutyParamDB mLDSchemeDutyParamDB = new LDSchemeDutyParamDB(tConnection);
            LDSchemeDutyParamSet mLDSchemeDutyParamSet =  mLDSchemeDutyParamDB.executeQuery("select  * from  LDSchemeDutyParam");

            LDSchemeDutyParamSet saveLDSchemeDutyParamSet = new LDSchemeDutyParamSet();
            tExeSQL = new ExeSQL();
            int tLDSchemeDutyParamCount = Integer.parseInt(tExeSQL.getOneValue("select count(*) from LDSchemeDutyParam "));
            if (tLDSchemeDutyParamCount != mLDSchemeDutyParamSet.size()){
                if ( mLDSchemeDutyParamSet.size() > 0 &&  mLDSchemeDutyParamSet != null) {
                    for (int l = 1; l <= mLDSchemeDutyParamSet.size(); l++) {
                        LDSchemeDutyParamSchema mLDSchemeDutyParamSchema = new LDSchemeDutyParamSchema();
                        tReflections.transFields(mLDSchemeDutyParamSchema,mLDSchemeDutyParamSet.get(l));
                        tResult = "SUCCESS";
                        saveLDSchemeDutyParamSet.add(mLDSchemeDutyParamSchema);
                    }
                    tMMap.put(saveLDSchemeDutyParamSet,"DELETE&INSERT");
                    if (saveLDSchemeDutyParamSet.size()>0){
                        tVData.add(tMMap);
                    }
                }
            }

            /**   Add By Xcc 增加方案信息同步表 LDScheme    */
            LDSchemeDB mLDSchemeDB = new LDSchemeDB(tConnection);
            LDSchemeSet mLDSchemeSet =  mLDSchemeDB.executeQuery("select  * from  LDScheme");

            LDSchemeSet saveLDSchemeSet = new LDSchemeSet();
            tExeSQL = new ExeSQL();
            int tLDSchemeCount = Integer.parseInt(tExeSQL.getOneValue("select count(*) from LDScheme "));
            if (tLDSchemeCount != mLDSchemeSet.size()){
                if ( mLDSchemeSet.size() > 0 &&  mLDSchemeSet != null) {
                    for (int l = 1; l <= mLDSchemeSet.size(); l++) {
                        LDSchemeSchema mLDSchemeSchema = new LDSchemeSchema();
                        tReflections.transFields(mLDSchemeSchema,mLDSchemeSet.get(l));
                        tResult = "SUCCESS";
                        saveLDSchemeSet.add(mLDSchemeSchema);
                    }
                    tMMap.put(saveLDSchemeSet,"DELETE&INSERT");
                    if (saveLDSchemeSet.size()>0){
                        tVData.add(tMMap);
                    }
                }
            }



            //提交
            PubSubmit tPubSubmit = new PubSubmit();
            if(!tPubSubmit.submitData(tVData)){
                logger.info("渠道和机构信息插入失败！");
                return "渠道和机构信息插入失败！";
            }
            logger.info("++++++++++++ChannelJobTaskService End++++++++++++");
        }catch (Exception e){
            logger.info("影像上传服务异常:"+e.getMessage());
            return "影像上传服务异常:"+e.getMessage();
        }finally {
            if (tConnection != null) {
                try {
                    tConnection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    logger.error(ExceptionUtils.exceptionToString(e));
                }
            }
            return tResult;
        }
    }
}
