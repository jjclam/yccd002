package com.sinosoft.cloud.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created b on 2018/11/13.
 */
public class DateUtil {

    /**
     * 获取日期时间的前一天（24小时之前）
     * @param date
     * @return
     */
    public static Date getYestoday(Date date){
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(Calendar.DAY_OF_MONTH, -1);
        Date startDat = instance.getTime();
        return startDat;
    }

}
