package com.sinosoft.cloud.config;

import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.event.JobEventConfiguration;
import com.dangdang.ddframe.job.event.rdb.JobEventRdbConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.lite.spring.api.SpringJobScheduler;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.sinosoft.cloud.job.ChannelJob;
import com.sinosoft.cloud.listener.ElasticJobListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * job配置管理类
 **/
@Configuration
public class ElasticJobConfig {
    /*-----------------------------------公共配置开始------------------------------------*/
    @Resource
    private ZookeeperRegistryCenter regCenter;

    @Resource
    private DataSource dataSource;
    /*-----------------------------------公共配置结束------------------------------------*/
    /*-----------------------------------job注入开始------------------------------------*/

    @Resource
    ChannelJob channelJob;

    /*-----------------------------------job注入结束------------------------------------*/

//同步核心机构和渠道数据--lacom,lasaleinfo
    @Bean(initMethod = "init")
    public JobScheduler simpleJobScheduler_channelJob(@Value("${channelJob.cron}") final String cron, @Value("${channelJob.shardingTotalCount}") final int shardingTotalCount, @Value("${channelJob.shardingItemParameters}") final String shardingItemParameters) {
        return new SpringJobScheduler(channelJob, regCenter, getLiteJobConfiguration(channelJob.getClass(), cron, shardingTotalCount, shardingItemParameters), jobEventConfiguration());
    }

    /**
     * 将作业运行的痕迹(日志)进行持久化到DB
     *
     * @return
     */
    @Bean
    public JobEventConfiguration jobEventConfiguration() {
        return new JobEventRdbConfiguration(dataSource);
    }

    @Bean
    public ElasticJobListener elasticJobListener() {
        return new ElasticJobListener(100, 100);
    }


    /**
     * 必要的简单配置
     *
     * @Description 任务（job）配置类,job初始化时使用，获取 LiteJobConfiguration 对象
     */
    private LiteJobConfiguration getLiteJobConfiguration(final Class<? extends SimpleJob> jobClass,
                                                         final String cron,
                                                         final int shardingTotalCount,
                                                         final String shardingItemParameters) {


        return LiteJobConfiguration
                .newBuilder(
                        new SimpleJobConfiguration(
                                JobCoreConfiguration.newBuilder(
                                        jobClass.getName(), cron, shardingTotalCount)
                                        .shardingItemParameters(shardingItemParameters)
                                        .build()
                                , jobClass.getCanonicalName()
                        )
                )
                .overwrite(true)
                .build();

    }

    /**
     * 完整的配置
     *
     * @param jobClass               job.class
     * @param cron                   cron表达式  （定时）
     * @param shardingTotalCount     总的分片数
     * @param shardingItemParameters 分片的序列号和参数 0=A,1=B
     * @param jobParameter           自定义参数
     * @param failover               是否开启任务失效转移(默认false)
     * @param misfire                是否开启错过任务重新执行(默认true)
     * @param monitorExecution       监控作业运行状态（建议运行时间或者间隔时间较长的开启）
     * @return
     */
    private LiteJobConfiguration getLiteJobConfiguration1(final Class<? extends SimpleJob> jobClass,
                                                          final String cron,
                                                          final int shardingTotalCount,
                                                          final String shardingItemParameters,
                                                          final String jobParameter, final boolean failover, final boolean misfire, final boolean monitorExecution) {
        return LiteJobConfiguration
                .newBuilder(
                        new SimpleJobConfiguration(
                                JobCoreConfiguration.newBuilder(jobClass.getName(), cron, shardingTotalCount)
                                        .shardingItemParameters(shardingItemParameters)
                                        .jobParameter(jobParameter)
                                        .failover(failover)
                                        .misfire(misfire)
                                        .build()
                                , jobClass.getCanonicalName()
                        )
                )
                .monitorExecution(monitorExecution)
                .overwrite(true)
                .build();

    }
}
