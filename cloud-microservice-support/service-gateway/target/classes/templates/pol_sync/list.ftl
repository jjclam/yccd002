﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>保单作废</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 50px">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="">
                <label class="layui-form-label">保单号</label>
                <div class="layui-input-inline">
                    <input type="text" name="contNo" id="contNo" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item" style="margin-top: 2px">
                <label class="layui-form-label">投保人姓名</label>
                <div class="layui-input-inline">
                    <input type="text" name="appntName" id="appntName" autocomplete="off" class="layui-input">
                </div>
                <label class="layui-form-label">投保日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期" name="countDate" id="countDate"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                </div>
            </div>
            <div class="layui-form" style="margin-top: 2px;">
                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 2px">
                    <button id="search" lay-filter="search" class="layui-btn" lay-submit >
                        <i class="fa fa-search" aria-hidden="true"></i> 查询
                    </button>
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 2px">
                    <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                        <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                    </button>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>查询结果</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>保单号</th>
                    <th>投保人</th>
                    <th>被保人</th>
                    <th>险种</th>
                    <th>保费</th>
                    <th>生效日期</th>
                    <th>终止日期</th>
                    <th>保单状态</th>
                    <th>同步状态</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
            <div class="admin-table-page">
                <div id="paged" class="page">
                </div>
            </div>
        </div>

    </fieldset>

    <div class="layui-form" style="margin-left: 30px;">
        <button id="syncPol"  class="layui-btn" >
            <i class="button" aria-hidden="true"></i> 确认同步
        </button>
    </div>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.id == undefined ?"":item.id }}</td>
        <td>{{ item.contNo == undefined ?"":item.contNo }}</td>
        <td>{{ item.appntName == undefined ?"":item.appntName }}</td>
        <td>{{ item.insuredName == undefined ?"":item.insuredName }}</td>
        <td>{{ item.riskCode == undefined ?"":item.riskCode }}</td>
        <td>{{ item.prem == undefined ?"":item.prem }}</td>
        <td>{{ item.cvaliDate == undefined ?"":item.cvaliDate }}</td>
        <td>{{ item.endDate == undefined ?"":item.endDate }}</td>
        <#--<td>{{ item.appFlag == undefined ?"":item.appFlag }}</td>-->
        <td>
            {{# if(item.appFlag === '0'){ }} <span style="">未签单</span> {{# } }}
            {{# if(item.appFlag === '1'){ }} <span style="">已签单</span> {{# } }}
            {{# if(item.appFlag === 'c'){ }} <span style="">签单后作废</span> {{# } }}
            {{# if(item.appFlag === 'd'){ }} <span style="">未签单作废</span> {{# } }}
        </td>
        <td>
            {{# if(item.countPol === '0'){ }} <span style="">未同步</span> {{# } }}
            {{# if(item.countPol === '1'){ }} <span style="">已同步</span> {{# } }}
        </td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/pol_search/list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                    location.reload();
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'), $(this).data('idx'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });

        $("#reset").click(function () {
            $("#contNo").val("");
            $("#appntName").val("");
            $("#countDate").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var contNo = $("#contNo").val();
            var appntName = $("#appntName").val();
            var countDate = $("#countDate").val();

            paging.get({
                "contNo": contNo,
                "appntName": appntName,
                "countDate": countDate,
                "v": new Date().getTime()
            });

        });

        $('#syncPol').on('click', function () {
            var contNo = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    // contNo += n + ',';
                    contNo += n ;
                    var j = $that.children('td').eq(5).text();
                    // contNo += n + ',';
                    record++;
                }
            });
            if (record == '') {
                alert("请先选择一条信息!");
                return false;
            }
            /*if (record > 1) {
                alert("一次只能选择一条信息作废! ");
                return false;
            }*/

            $.ajax({
                type: "POST",
                url: '/pol_sync/syncPolicy',
                data: "params=" + JSON.stringify({
                    contNo: contNo
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('同步成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('同步失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });


    });
</script>
</body>

</html>