<head>
    <meta charset="UTF-8">
    <title>操作轨迹</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 250px">

    <div class=layui-form" style="display: none">
        <div class="layui-input-inline">
            <input id="proposalGrpContNo" value="${item.proposalGrpContNo}">
        </div>
    </div>
    <div class="layui-form" style="display: none">
        <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
            <button id="search1" type="button" class="layui-btn" style="margin-left: 30px;">
                <i class="fa fa-search" aria-hidden="true"></i> 查询
            </button>
        </div>
    </div>

    <fieldset class="layui-elem-field">
        <div class="layui-field-box layui-form" >
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>任务节点</th>
                    <th>操作员</th>
                    <th>任务流转时间</th>
                    <th>任务完成时间</th>
                    <th>说明</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.id == undefined ?"":item.id }}</td>
        <td>{{ item.activityid == undefined ?"":item.activityid }}</td>
        <td>{{ item.operator == undefined ?"":item.operator }}</td>
        <td>{{ item.maketime == undefined ?"":item.maketime }}</td>
        <td>{{ item.endtime == undefined ?"":item.endtime }}</td>
        <td>{{ item.rmack == undefined ?"":item.rmack }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/schedule/childViewList', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'), $(this).data('idx'));
                    });
                });

            },
        });

        $('#search').on('click', function () {
            var proposalGrpContNo = $("#proposalGrpContNo").val();

            paging.get({
                "proposalGrpContNo": proposalGrpContNo,
                "v": new Date().getTime()
            });
        });


        function viewForm(proposalGrpContNo) {
            //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
            layer.open({
                type: 2,
                title: '操作轨迹2',
                content: '/schedule/childView.html?proposalGrpContNo=' + proposalGrpContNo,
                shade: false,
                btn: '返回',
                offset: 'auto',
                area: ['100%', '100%'],
                zIndex: 19991231,
                id: 'lay_schedule_view',
                maxmin: false,
                yes: function () {
                    layer.closeAll();
                }
            });
        };


        (()=>{
            // 兼容IE
            if(document.all) {
                document.getElementById("search").click();
            }
            // 兼容其它浏览器
            else {
                var e = document.createEvent("MouseEvents");
                e.initEvent("click", true, true);
                document.getElementById("search").dispatchEvent(e);
                // onchangeParams();
            }
        })();

    });

</script>
</body>
