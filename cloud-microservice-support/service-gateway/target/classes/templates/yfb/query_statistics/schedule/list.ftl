﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>设备查询</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 50px">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">

                    <label class="layui-form-label">终端设备编号*</label>
                    <div class="layui-input-inline">
                        <input type="text" name="deviceNo" ay-verify="required" placeholder="请输入编号" id="deviceNo" autocomplete="off" class="layui-input">
                    </div>

                    <label class="layui-form-label">安装地址</label>
                    <div class="layui-input-inline">
                        <input type="text" name="address" id="address" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">是否启用</label>
                    <div class="layui-input-inline" style="display:flex">*
                        <select name="useFlag" class="form-control" id="useFlag" lay-search>
                            <option value="">请选择</option>
                            <option value="0">0-不启用</option>
                            <option value="1">1-启用</option>
                        </select>
                    </div>
                    <label class="layui-form-label">管理机构</label>
                    <div class="layui-input-inline" style="display:flex">
                        <select name="mangeCom" id="mangeCom" class="form-control" lay-filter="funChange" lay-search>
                            <option value="">请选择</option>
                            <option value="86">86-上海人寿保险股份有限公司</option>
                            <option value="86">8601-上海人寿保险股份有限公司</option>
                            <option value="86">860101-上海人寿上海分公司本部（虚拟）</option>
                            <option value="86">86010101-利安人寿保险股份有限公司直属业务部</option>
                            <option value="86">860101010-上海人寿保险股份有限公司直属业务部</option>
                            <option value="86">863111111-上海人寿保险股份有限公司上海分公司</option>
                            <option value="86">863301-上海人寿上海分公司陆家嘴中心支公司</option>
                            <option value="86">86330101-上海人寿上海分公司陆家嘴中心支公司营销服务一部</option>
                        </select>
                    </div>
            </div>
                <p>&nbsp;</p>
                <div class="layui-inline">
                    <label class="layui-form-label">注册日期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="regDate" id="regDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                               value="${item.regDate?if_exists }">
                    </div>

                    <label class="layui-form-label">网点</label>
                    <div class="layui-input-inline">
                        <select name="manageComName" id="manageComName" class="form-control" lay-filter="funChange" lay-search>
                            <option value="">请选择</option>
                            <option value="上海农村商业银行长江西路分理处">050101040122-上海农村商业银行长江西路分理处</option>
                            <option value="上海农村商业银行长江南路分理处">050101040123-上海农村商业银行长江南路分理处</option>
                            <option value="上海农村商业银行淞青路分理处">050101040125-上海农村商业银行淞青路分理处</option>
                            <option value="上海农村商业银行华和路支行">050101040126-上海农村商业银行华和路支行</option>
                            <option value="上海农村商业银行吉浦路支行">050101040127-上海农村商业银行吉浦路支行</option>
                            <option value="上海农村商业银行东方国贸商城支行">050101040128-上海农村商业银行东方国贸商城支行</option>
                            <option value="上海农村商业银行杨泰路分理处">050101040129-上海农村商业银行杨泰路分理处</option>
                            <option value="上海农村商业银行菊盛路分理处">050101040130-上海农村商业银行菊盛路分理处</option>
                            <option value="上海农村商业银行南汇支行营业部">050101050101-上海农村商业银行南汇支行营业部</option>
                            <option value="上海农村商业银行惠南支行">050101050102-上海农村商业银行惠南支行</option>
                            <option value="上海农村商业银行黄路支行">050101050103-上海农村商业银行黄路支行</option>
                            <option value="上海农村商业银行金汇支行">050101060116-上海农村商业银行金汇支行</option>
                            <option value="上海农村商业银行市一分理处">110101040118-上海农村商业银行市一分理处</option>

                        </select>
                    </div>
                </div>

            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
                <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;margin-left: 5px;">
                <button id="add" type="button" class="layui-btn" >
                    <i class="fa fa-plus" aria-hidden="true"></i> 注册
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
                <button id="delete" class="layui-btn">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> 删除
                </button>
            </div>

        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>查询结果</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <#--<th>序号</th>-->
                    <th>终端设备编号</th>
                    <th>安装地址</th>
                    <th>管理机构</th>
                    <th>是否启用</th>
                    <th>注册日期</th>
                    <th>代理网点</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
            <div class="admin-table-page">
                <div id="paged" class="page">
                </div>
            </div>
        </div>

    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <#--<td>{{ item.serialNo == undefined ?"":item.serialNo }}</td>-->
        <td>{{ item.deviceNo == undefined ?"":item.deviceNo }}</td>
        <#--<td>{{ item.deviceNo == undefined ?"":item.deviceNo }}</td>-->
        <td>{{ item.address == undefined ?"":item.address }}</td>
        <td>{{ item.mangeCom == undefined ?"":item.mangeCom }}</td>
        <td>{{ item.useFlag == undefined ?"":item.useFlag }}</td>
        <td>{{ item.regDate == undefined ?"":item.regDate }}</td>
        <td>{{ item.manageComName == undefined ?"":item.manageComName }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/schedule/list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('该查询条件下，没有符合查询条件的结果！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'), $(this).data('idx'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                            layer.msg($(this).data('id'));
                    });
                });

            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            // layer.msg('你选择的名称有：' + names);
        });

        $('#search').on('click', function () {
            var serialNo = $("#serialNo").val();
            var deviceNo = $("#deviceNo").val();
            var address = $("#address").val();
            var mangeCom = $("#mangeCom").val();
            var useFlag = $("#useFlag").val();
            var regDate = $("#regDate").val();
            var manageComName = $("#manageComName").val();
            paging.get({
                "serialNo": serialNo,
                "deviceNo": deviceNo,
                "address": address,
                "mangeCom": mangeCom,
                "useFlag": useFlag,
                "regDate": regDate,
                "manageComName": manageComName,
                "v": new Date().getTime()
            });

        });

        $('#delete').on('click', function () {
            var deviceNo = '';
            // var useFlag = '';
            var record = '';
            // $('#content').children('tr').each(function () {
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(1).text();
                    deviceNo += n + ',';
                    record++;
                }
            });


            // $('#content').children('tr').each(function () {
            //     var $that = $(this);
            //     var $sbx = $that.children('td').eq(0).text();
            //     if ($sbx == "") {
            //         alert('请先进行查询!');
            //     }
            //     var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
            //     if ($cbx) {
            //         var n = $that.children('td').eq(1).text();
            //         deviceNo += n + ',';
            //         var j = $that.children('td').eq(4).text();
            //         useFlag += j + ',';
            //         record++;
            //     }
            // });
            if (record == '') {
                alert("请先选择一条信息!");
                return false;
            }

            $.ajax({
                type: "POST",
                url: '/schedule/deleteCodeMaintain',
                data: "params=" + JSON.stringify({
                    deviceNo: deviceNo
                    , useFlag: useFlag
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('删除成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('删除失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
          });

        $("#add").click(function () {
            var deviceNo = $("#deviceNo").val();
            var useFlag = $("#useFlag").val();
            var address = $("#address").val();
            var mangeCom = $("#mangeCom").val();
            var regDate = $("#regDate").val();
            var manageComName = $("#manageComName").val();
            // alert(manageComName);
            $.ajax({
                type: "POST",
                url:"/schedule/addCodeMaintain/",
                data: "params=" + JSON.stringify({
                     deviceNo: deviceNo
                    , useFlag: useFlag
                    , address: address
                    , mangeCom: mangeCom
                    , regDate: regDate
                    ,manageComName:manageComName
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '1') {
                        alert('成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });

        // $('#add').on('click', function () {
        //                     $.get('/schedule/addCodeMaintain.html', function(form) {
        //                         layer.open({
        //                             type: 1,
        //                             title: '增加',
        //                             content: form,
        //                             btn: ['提交', '返回'],
        //                             shade: false,
        //                             offset: 'auto',
        //                             area : ['450px', '480px'],
        //                             zIndex: 19991231,
        //                             id: 'lay_schedule_add',
        //                             maxmin: false,
        //                             yes: function(index) {
        //                                 //触发表单的提交事件
        //                                 $('form.layui-form-item').find('button[lay-filter=edit]').click();
        //                             },
        //                             success: function(layero, index) {
        //                                 var form = layui.form();
        //                                 form.render();
        //                                 form.on('submit(edit)', function(data) {
        //                                     var deviceNo = $("#deviceNo").val();
        //                                     var useFlag = $("#useFlag").val();
        //                                     var address = $("#address").val();
        //                                     var manageCom = $("#manageCom").val();
        //                                     var regDate = $("#regDate").val();
        //
        //                                     alert(deviceNo);
        //                                     $.ajax({
        //                                         type: "POST",
        //                                         url: "/schedule/addCodeMaintain",
        //                                         data: "params=" + JSON.stringify({
        //                                             deviceNo: deviceNo
        //                                             , useFlag: useFlag
        //                                             , address: address
        //                                             , manageCom: manageCom
        //                                             , regDate: regDate
        //                                         }),
        //                                         dataType: 'json',
        //                                         success: function(data){
        //                                             if(data.msg == '0000') {
        //                                                 alert('添加成功');
        //                                                 layer.closeAll();
        //                                                 $('#search').click();//刷新页面
        //                                             }else {
        //                                                 alert('添加失败, ' + data.msg);
        //                                                 console.log(data.msg);
        //                                             }
        //                                         }
        //                                     });
        //                                     return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        //                 });
        //             },
        //         });
        //     });
        // });

    });


    // });
</script>
</body>

</html>