<head>
    <meta charset="UTF-8">
    <title>方案详情</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 250px">

    <fieldset class="layui-elem-field">
        <legend>${params.contschemename}-方案</legend>
        <div class="layui-form" style="display: none">
            <div class="layui-input-inline">
                <input id="contschemecode" value="${params.contschemecode}">
            </div>
        </div>
        <div class="layui-form" style="display: none">
            <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 2px">
                <button id="search" lay-filter="onchangeParams" class="layui-btn" lay-submit >
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
        </div>

        <div class="layui-form" >
            <div class="layui-input-inline" style="font-size: 16px; margin-left: 26px; margin-top: 10px;">
                <a id="insuYear" >保险期间: ${params.insuYear}</a>
            </div>
        </div>
        <#--<div class="layui-form" >-->
            <#--<div class="layui-input-inline" style="font-size: 16px; margin-left: 26px; margin-top: 10px;">-->
                <#--<a id="insuYear" >职业类别: ${params.insuYear!}</a>-->
            <#--</div>-->
        <#--</div>-->

        <div class="layui-field-box layui-form" style="width: 800px">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>险种名称</th>
                    <th>险种责任</th>
                    <th>保额</th>
                    <th>免赔额</th>
                    <th>免赔天数</th>
                    <th>赔付比例</th>
                    <th>备注</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

        <div class="layui-form" style="font-size: 16px; margin-left: 26px; margin-bottom: 10px;">
            <a id="Prem" >方案合计保费: ${params.prem}</a>
        </div>
    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.id == undefined ?"":item.id }}</td>
        <td>{{ item.riskcode == undefined ?"":item.riskcode }}</td>
        <td>{{ item.dutyCode == undefined ?"":item.dutyCode }}</td>
        <td>{{ item.amnt == undefined ?"":item.amnt }}</td>
        <td>{{ item.getLimit == undefined ?"":item.getLimit }}</td>
        <td>{{ item.noGetDay == undefined ?"":item.noGetDay }}</td>
        <td>{{ item.getLimitType == undefined ?"":item.getLimitType }}</td>
        <td>{{ item.remark == undefined ?"":item.remark }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/scheme_info/viewList', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });


            },
        });

        $('#search').on('click', function () {
            var contschemecode = $("#contschemecode").val();

            // alert(contschemecode);
            paging.get({
                "contschemecode": contschemecode,
                "v": new Date().getTime()
            });
        });

        function onchangeParams(data) {
            var contschemecode = $("#contschemecode").val();
            alert(contschemecode);

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/scheme_info/linkedSchemeParams',
                data:{contschemecode: contschemecode},
                success: function(data){
                    alert(data.Prems);
                    var prems = data.Prems;
                    var Prem = $('#Prem');
                    Prem.val(prems);
                },
                error:function(data){
                    alert('系统错误');
                }
            });

        };


        (()=>{
            // 兼容IE
            if(document.all) {
                document.getElementById("search").click();
            }
            // 兼容其它浏览器
            else {
                var e = document.createEvent("MouseEvents");
                e.initEvent("click", true, true);
                document.getElementById("search").dispatchEvent(e);
                // onchangeParams();
            }
        })();

    });

</script>
</body>
