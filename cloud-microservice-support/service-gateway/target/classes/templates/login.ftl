<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>管理系统</title>
<#--<link rel="icon" type="image/x-icon" href="images/favicon.ico">-->
    <link rel="stylesheet" href="plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="css/login.css"/>

</head>

<body class="beg-login-bg">
<div class="beg-login-bg1">
<#--<a class="logo" style="margin-left: 10px">
    <img src="images/title.png" style="margin-left: 6px; margin-top: 15px;">
</a>-->
<#--<a>
    <h1 style="margin-left: 307px; margin-top: -53px; font-size: 30px">上海人寿中台管理系统</h1>
</a>-->
</div>
<div class="beg-login-box">
<#--<header>-->
<#--<h1>上海人寿中台管理系统</h1>-->
<#--</header>-->

    <div id="windows1" class="beg-login-main">
        <form action="/login" method="post" class="layui-form"><input name="__RequestVerificationToken"
                                                                      type="hidden"
                                                                      value="fkfh8D89BFqTdrE2iiSdG_L781RSRtdWOH411poVUWhxzA5MzI8es07g6KPYQh9Log-xf84pIR2RIAEkOokZL3Ee3UKmX0Jc8bW8jOdhqo81"/>
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe612;</i>
                </label>
                <input type="text" name="userName" id="userName" lay-verify="userName" autocomplete="off" placeholder="请输入用户名"
                       class="layui-input" value="admin" style="margin-top: 18px;">
            </div>
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe642;</i>
                </label>
                <input type="password" name="password" id="passWord" lay-verify="password" autocomplete="off" placeholder="请输入密码"
                       class="layui-input" value="123456">
            </div>
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe638;</i>
                </label>
                <input type="vercode" name="verCode" id="verCode" lay-verify="verCode" autocomplete="off"
                       placeholder="请输入验证码" class="layui-input" style="width: 140px">
                <div class="layui-input-block" style="display:flex; margin-top: -38px; margin-right: -38px; left: 40px;">
                    <canvas id="canvas" width="120px" height="38px"></canvas>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="beg-pull" type="center">
                    <button class="layui-btn layui-btn-primary" type="submit"  id="login" style="width: 100%">
                        <i class="layui-icon"></i> 登录
                    </button>
                </div>
                <div class="beg-clear"></div>
            </div>
        </form>

    </div>

</div>

<div class="beg-login-bg2">
<#-- <a style="text-align: center; ">
        <h1 style="font-size: 15px; margin-top: 22px;">上海人寿保险有限公司</h1>
    </a>
    <a style="text-align: center; ">
        <h1 style="font-size: 15px">地址: 上海市浦东新区世纪大道201号8楼、12楼</h1>
    </a>-->
</div>

<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script>
    layui.use(['layer', 'form'], function () {
        var layer = layui.layer,
                $ = layui.jquery,
                form = layui.form();

    <#--var msg = '${message}';-->
    <#--if (msg != null && msg.trim() != "") {-->
    <#--layer.msg(msg);-->
    <#--return false;-->
    <#--}-->

        // form.on('submit(login)', function (data) {
        //     alert(data);
        //     var username = data.form.userName.value;
        //     var password = data.form.password.value;
        //     var verCode = data.form.verCode.value;
        //     // var username  = $("#userName").val();
        //     // var password = $("#passWord").val();
        //     alert(password+username+verCode);
        //     if (username.trim() == '') {
        //         alert('用户名不能为空');
        //         return false;
        //     }
        //     if (password.trim() == '') {
        //         alert("密码不能为空");
        //         return false;
        //     }
        //     var check = showVerCode.trim();
        //     if (verCode.trim() == '') {
        //         alert("验证码不能为空");
        //         return false;
        //     } else if (check != '' && verCode.trim() != check) {
        //         alert("验证码输入错误! ");
        //         return false;
        //     }
        // });


        $('#login').on('click', function () {
            var username  = $("#userName").val();
            var password = $("#passWord").val();
            var verCode = $("#verCode").val();

            if (username.trim() == '') {
                alert('用户名不能为空');
                return false;
            }
            if (password.trim() == '') {
                alert("密码不能为空");
                return false;
            }
            // alert(verCode);
            var check = showVerCode.join("").trim();
            if (verCode.trim() == '') {
                alert("验证码不能为空");
                $('#canvas').click();
                return false;
            } else if (check != '' && verCode.trim() != check) {
                alert("验证码输入错误! ");
                $('#canvas').click();
                return false;
            }
        });

        //验证码
        var showVerCode = [];
        /**生成一个随机数**/
        function randomNum(min,max){
            return Math.floor( Math.random()*(max-min)+min);
        }
        /**生成一个随机色**/
        function randomColor(min,max){
            var r = randomNum(min,max);
            var g = randomNum(min,max);
            var b = randomNum(min,max);
            return "rgb("+r+","+g+","+b+")";
        }
        drawPic(showVerCode);//

        $('#canvas').on('click', function(e) {
            e.preventDefault();
            drawPic(showVerCode);
        });

        /**绘制验证码图片**/
        function drawPic(showVerCode){
            var canvas=document.getElementById("canvas");
            var width=canvas.width;
            var height=canvas.height;
            var ctx = canvas.getContext('2d');
            ctx.textBaseline = 'bottom';

            /**绘制背景色**/
            ctx.fillStyle = randomColor(180,240); //颜色若太深可能导致看不清
            ctx.fillRect(0,0,width,height);
            /**绘制文字**/
                    // var str = 'ABCEFGHJKLMNPQRSTWXY123456789';
            var str = '0123456789';
            for(var i=0; i<4; i++){
                var txt = str[randomNum(0,str.length)];
                showVerCode[i] = txt;
                ctx.fillStyle = randomColor(50,160);  //随机生成字体颜色
                ctx.font = randomNum(15,40)+'px SimHei'; //随机生成字体大小
                var x = 10+i*25;
                var y = randomNum(25,45);
                var deg = randomNum(-45, 45);
                //修改坐标原点和旋转角度
                ctx.translate(x,y);
                ctx.rotate(deg*Math.PI/180);
                ctx.fillText(txt, 0,0);
                //恢复坐标原点和旋转角度
                ctx.rotate(-deg*Math.PI/180);
                ctx.translate(-x,-y);
            }
            /**绘制干扰线**/
            for(var i=0; i<6; i++){
                ctx.strokeStyle = randomColor(40,180);
                ctx.beginPath();
                ctx.moveTo( randomNum(0,width), randomNum(0,height) );
                ctx.lineTo( randomNum(0,width), randomNum(0,height) );
                ctx.stroke();
            }
        };

    });
</script>
</body>

</html>