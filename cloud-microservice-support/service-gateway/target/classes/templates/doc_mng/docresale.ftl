﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>回销单证信息</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
            <div class="layui-form" style="float:left;">
                <div class="layui-form-item" style="margin-top: 5px;">
                    <label class="layui-form-label">机构</label>
                    <div class="layui-input-inline">
                    <#--<input type="text" name="executeState" id="executeState" autocomplete="off" class="layui-input"> 1--成功 0--失败-->
                        <select name="executeState" id="executeState" class="form-control" lay-search>
                            <option value="0">请选择</option>
                            <option value="86">86</option>
                            <option value="86">8601</option>
                            <option value="86">860101</option>
                            <option value="86">86010101</option>
                            <option value="86">860101010</option>
                            <option value="86">8611</option>
                            <option value="86">861100</option>
                            <option value="86">86110000</option>
                            <option value="86">86110001</option>
                            <option value="86">86110002</option>
                            <option value="86">86110003</option>
                            <option value="86">86110004</option>
                            <option value="86">86110005</option>
                            <option value="86">86110006</option>
                            <option value="86">86110007</option>
                            <option value="86">86110099</option>
                            <option value="86">861102</option>
                            <option value="86">86110200</option>
                            <option value="86">86110200</option>
                            <option value="86">86110299</option>
                            <option value="86">861103</option>
                            <option value="86">86110300</option>
                            <option value="86">86110300</option>
                            <option value="86">86</option>
                            <option value="86">8601</option>
                            <option value="86">860101</option>
                            <option value="86">86010101</option>
                            <option value="86">860101010</option>
                            <option value="86">8611</option>
                            <option value="86">861100</option>
                            <option value="86">86110000</option>
                            <option value="86">86110001</option>
                            <option value="86">86110002</option>
                            <option value="86">86110003</option>
                            <option value="86">86110004</option>
                            <option value="86">86110005</option>
                            <option value="86">86110006</option>
                            <option value="86">86110007</option>
                            <option value="86">86110099</option>
                            <option value="86">861102</option>
                            <option value="86">86110200</option>
                            <option value="86">86110200</option>
                            <option value="86">86110299</option>
                            <option value="86">861103</option>
                            <option value="86">86110300</option>
                            <option value="86">86110300</option>
                        <#--<#list stateCode as LDCode>-->
                        <#--<option value="${(LDCode.code)!}">${(LDCode.code)!}—${(LDCode.codeName)!}</option>-->
                        <#--</#list>-->
                        </select>
                    </div>

                    <label class="layui-form-label">单证编码</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input"  id="certifycode" value="001">
                    </div>

                    <p>&nbsp;</p>

                    <div class="layui-form-item">
                        <label class="layui-form-label">单证起始号</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input"  value="" id="startno">
                        </div>
                        <label class="layui-form-label">单证终止号</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input"  value="" id="endno">
                        </div>
                    </div>

                    <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 5px;">
                    <button id="search11" lay-filter="search" class="layui-btn" lay-submit>
                        <i class="fa fa-search" aria-hidden="true"></i> 单证校验
                    </button>
                    </div>

                    <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 5px;">
                        <button id="resale" lay-filter="search" class="layui-btn" lay-submit>
                            <i class="fa fa-search" aria-hidden="true"></i> 单证回销
                        </button>
                    </div>
                </div>
            </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>操作说明</legend>
        <div class="layui-field-box layui-form">
            <ul type="disc">
                <li>    *  1、回退的单证必须是未使用的单证。</li>
                <li>    *  2、回销机构是指从单证系统下发到远程出单时的发起者管理机构，代码规则：“A”+“管理机构代码”。</li>
            </ul>
        </div>
    </fieldset>
    <#--<div class="admin-table-page">-->
        <#--<div id="paged" class="page">-->
        <#--</div>-->
    <#--</div>-->


</div>

<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();


            $("#resale").click(function () {
                alert("单证回销success！");

                var executeState = $("#executeState").val();
                var certifycode = $("#certifycode").val();
                var startno = $("#startno").val();
                var endno = $("#endno").val();
                if(executeState == 0){
                    alert("机构不能为空！");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url:"/doc_mng/documentBackSale/",
                    data: "params=" + JSON.stringify({
                        executeState: executeState,
                        certifycode:certifycode,
                        startno:startno,
                        endno:endno
                    }),
                    dataType: 'json',
                    cache: false,
                    success: function(data){
                        if(data.msg == '1') {
                            alert('回销成功');
                            $('#search').click();//刷新页面
                        }else {
                            alert('回销失败, ' + data.msg);
                            console.log(data.msg);
                        }
                    }
                });
            });

            $('#search11').on('click', function () {
                alert("单证校验通过！");

            });
    });
</script>
</body>

</html>