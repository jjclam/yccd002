﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>渠道中介机构配置</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
    <fieldset class="layui-elem-field">
        <legend>查询中介机构</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 10px;">
                <label class="layui-form-label">网点编号</label>
                <div class="layui-input-inline">
                    <input type="text" name="agencyCode" id="agencyCode" autocomplete="off" class="layui-input">
                </div>
                <label class="layui-form-label">网点名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="agencyName" id="agencyName" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-inline" style="margin-top: 15px;">
                    <label class="layui-form-label">渠道大类</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="chlTypeL" id="chlTypeL" class="form-control" lay-filter="funChange" lay-search>
                            <option value="">请选择渠道类型</option>
                        <#list laChlLargeType as LDCode>
                            <option value="${(LDCode.code)!}">${(LDCode.code)!}-${(LDCode.codeName)!}</option>
                        </#list>
                        </select>
                    </div>
                </div>
            </div>

            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
                <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" >
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="display:flex; margin-top: -2px;margin-left: 28px;">
                <button id="add" type="button" class="layui-btn">
                    <i class="fa fa-plus" aria-hidden="true"></i> 增加
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
                <button id="delete" class="layui-btn">
                    <i class="fa fa-times" aria-hidden="true"></i> 删除
                </button>
            </div>
        </div>

    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>配置信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>网点代码</th>
                    <th>管理机构</th>
                    <th>网点名称</th>
                    <th>渠道大类</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>


    <div class="layui-form" style="float:left; display: contents">

    </div>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.proId == undefined ?"":item.proId }}</td>
        <td>{{ item.agentCom == undefined ?"":item.agentCom }}</td>
        <td>{{ item.manageCom == undefined ?"":item.manageCom }}</td>
        <td>{{ item.name == undefined ?"":item.name }}</td>
        <td>{{ item.branchType2 == undefined ?"":item.branchType2 }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/chl_edit/chlAgencyMaintain_list',

            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });

        $("#reset").click(function () {
            $("#agencyCode").val("");
            $("#agencyName").val("");
            $("#channelName").val("");
            $("#agencyName2").val("");
            $("#chlTypeL").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var agencyCode = $("#agencyCode").val();
            var agencyName = $("#agencyName").val();
            var chlTypeL = $("#chlTypeL").val();

            // if (agencyCode != "" & agencyName != "") {
            //     alert("不能同时输入编码和名称!");
            //     return;
            // }

            paging.get({
                "agencyCode": agencyCode,
                "agencyName": agencyName,
                "chlTypeL": chlTypeL,
                "v": new Date().getTime()
            });
        });

        $('#add').on('click', function () {

            $.get('/chl_edit/addLacom.html', function(form) {
                layer.open({
                    type: 1,
                    title: '网点增加',
                    content: form,
                    btn: ['提交', '返回'],
                    shade: false,
                    offset: 'auto',
                    area : ['950px', '480px'],
                    zIndex: 19991231,
                    id: 'lay_addUser',
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form-item').find('button[lay-filter=edit]').click();
                    },
                    success: function(layero, index) {
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var userName = $("#agentcom").val();
                            // alert(userName);
                            var agentName = $("#agentName").val();
                            var rePassword = $("#rePassword5").val();
                            var realName = $("#realName5").val();
                            // var userType = $("#userType5").val();
                            var institutions = $("#institutions5").val();
                            var field = $("#field5").val();
                            var email = $("#email5").val();
                            var position = $("#position5").val();
                            var telephone = $("#telephone5").val();
                            var linkedPhone = $("#linkedPhone5").val();
                            var wangbit = $("#wangbit").val();
                            var chlTypeLer = $("#chlTypeLer").val();

                            if (userName == '') {
                                alert("请输入出单网点编码!");
                                return false;
                            }
                            if (agentName == '') {
                                alert("请输入出单网点名称!");
                                return false;
                            }
                            if (wangbit == '') {
                                alert("请选择管理机构！");
                                return false;
                            }
                            if (field == '') {
                                alert("请输入所属系统!");
                                return false;
                            }
                            if (email != '') {
                                // alert(email);
                                var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
                                if (!reg.test(email)) {
                                    alert("邮箱格式不正确!");
                                    return false;
                                }
                            }
                            if (telephone != '') {
                                // alert(email);
                                var reg = /^1[3|4|5|8][0-9]\d{4,8}$/;
                                if (!reg.test(telephone)) {
                                    alert("手机号格式不正确!");
                                    return false;
                                }
                            }
                            if(chlTypeLer==''){
                                alert("请选择渠道大类!");
                                return false;
                            }

                            $.ajax({
                                type: "POST",
                                url: "/chl_edit/addUser",
                                data: "params=" + JSON.stringify({
                                    agentcom: userName
                                    , agentName: agentName
                                    , realName: realName
                                    , institutions: institutions
                                    , field: field
                                    , email: email
                                    , position: position
                                    , telephone: telephone
                                    , linkedPhone: linkedPhone
                                    , wangbit: wangbit
                                    , chlTypeLer:chlTypeLer
                                }),
                                dataType: 'json',
                                success: function(data){
                                    if(data.msg == '0000') {
                                        alert('添加成功');
                                        // location.reload(); //刷新
                                        layer.closeAll();
                                        $('#search').click();//刷新页面
                                    }else {
                                        alert('添加失败, ' + "该网点已配置");
                                        console.log(data.msg);
                                        // location.reload(); //刷新
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                });
            });
        });

        $('#delete').on('click', function () {
            var agencyNames = '';
            var channelNames = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    agencyNames += n + ',';
                    var j = $that.children('td').eq(4).text();
                    channelNames += j + ',';
                    record++;
                }
            });
            if (record == '') {
                alert("请先选择一条信息!");
                return false;
            }
            $.ajax({
                type: "POST",
                url: '/chl_edit/deleteChlAgencyMaintain',
                data: "params=" + JSON.stringify({
                    agencyNames: agencyNames
                    , channelNames: channelNames
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('删除成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('删除失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });


    });
</script>
</body>

</html>