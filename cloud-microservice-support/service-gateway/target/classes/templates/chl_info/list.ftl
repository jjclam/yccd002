﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>渠道信息查询</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 50px">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="">
                <label class="layui-form-label">中介机构代码</label>
                <div class="layui-input-inline">
                    <input type="text" name="agencyCode" id="agencyCode" autocomplete="off" class="layui-input">
                </div>
                <label class="layui-form-label">中介机构名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="agencyName" id="agencyName" autocomplete="off" class="layui-input">
                </div>
                <#--<label class="layui-form-label">代理人编号</label>-->
                <#--<div class="layui-input-inline">-->
                    <#--<input type="text" name="agentCode" id="agentCode" autocomplete="off" class="layui-input">-->
                <#--</div>-->
            </div>
            <div class="layui-form-item" style="margin-top: 2px">
                <label class="layui-form-label">中介机构类别</label>
                <div class="layui-input-inline">
                    <select name="agencyType" id="agencyType" lay-search>
                            <option value="">请选择中介机构类别</option>
                    <#list laChlAgencyType as ChlInfo>
                        <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                    </#list>
                    </select>
                </div>
                <label class="layui-form-label">保险许可证号码</label>
                <div class="layui-input-inline layui-unselect">
                    <input type="text" name="licenseNo" id="licenseNo" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form" style="margin-top: 2px;">
                <#--<div class="layui-form-item" style="margin:0;">-->
                    <#--<label class="layui-form-label">渠道编码</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="channelCode" id="channelCode" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                    <#--<label class="layui-form-label">渠道名称</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input type="text" name="channelName" id="channelName" autocomplete="off" class="layui-input">-->
                    <#--</div>-->
                <#--</div>-->
                    <#--<div class="layui-form-item" style="margin:0;">-->
                        <#--<label class="layui-form-label">渠道名称</label>-->
                        <#--<div class="layui-input-inline layui-unselect">-->
                            <#--<select name="channelCode" id="channelCode" lay-search>-->
                                <#--<option value="">请选择渠道</option>-->
                    <#--<#list laChlInfo as ChlInfo>-->
                        <#--<option value="${(ChlInfo.channelCode)!}">${(ChlInfo.channelCode)!}—${(ChlInfo.channelName)!}</option>-->
                    <#--</#list>-->
                            <#--</select>-->
                        <#--</div>-->
                    <#--</div>-->
                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 2px">
                    <button id="search11" lay-filter="search" class="layui-btn" lay-submit >
                        <i class="fa fa-search" aria-hidden="true"></i> 查询
                    </button>
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 2px">
                    <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                        <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                    </button>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>查询结果</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>中介机构代码</th>
                    <th>中介机构名称</th>
                    <th>保险许可证号码</th>
                    <#--<th>渠道编码</th>-->
                    <#--<th>渠道名称</th>-->
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
            <div class="admin-table-page">
                <div id="paged" class="page">
                </div>
            </div>
        </div>


    </fieldset>


</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.proId == undefined ?"":item.proId }}</td>
        <td>{{ item.agencyCode == undefined ?"":item.agencyCode }}</td>
        <td>{{ item.agencyName == undefined ?"":item.agencyName }}</td>
        <td>{{ item.licenseNo == undefined ?"":item.licenseNo }}</td>
        <#--<td>{{ item.channelCode == undefined ?"":item.channelCode }}</td>-->
        <#--<td>{{ item.channelName == undefined ?"":item.channelName }}</td>-->
        <td>
            <a href="javascript:;" data-id="{{ item.agencyCode }}" data-idx="{{ item.channelCode }}" data-opt="view" class="layui-btn layui-btn-normal layui-btn-mini">详情</a>
            <#--<a href="javascript:;" data-id="{{ item.mchId }}" data-opt="notify" class="layui-btn layui-btn-mini">编辑</a>-->
            <!--<a href="javascript:;" data-id="{{ item.mchId }}" data-opt="del" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>-->
        </td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/chl_edit/list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'), $(this).data('idx'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            // layer.msg('你选择的名称有：' + names);
        });

        $("#reset").click(function () {
            $("#agencyCode").val("");
            // alert(agencyCode);
            $("#agencyName").val("");
            $("#agentCode").val("");
            $("#agencyType").val("");
            $("#licenseNo").val("");
            $("#channelCode").val("");
            $("#channelName").val("");
            form.render();
        });

        $('#search11').on('click', function () {
            var agencyCode = $("#agencyCode").val();
            // alert(agencyCode);
            var agencyName = $("#agencyName").val();
            var agentCode = $("#agentCode").val();
            var agencyType = $("#agencyType").val();
            var licenseNo = $("#licenseNo").val();
            var channelCode = $("#channelCode").val();

            paging.get({
                "agencyCode": agencyCode,
                "agencyName": agencyName,
                "agentCode": agentCode,
                "agencyType": agencyType,
                "licenseNo": licenseNo,
                "channelCode": channelCode,
                "v": new Date().getTime()
            });

        });


        var addBoxIndex = -1;
        $('#add').on('click', function () {
            if (addBoxIndex !== -1)
                return;
            editForm();
        });

        $('#import').on('click', function () {
            var that = this;
            var index = layer.tips('只想提示地精准些', that, {tips: [1, 'white']});
            $('#layui-layer' + index).children('div.layui-layer-content').css('color', '#000000');
        });

        function viewForm(agencyCode, channelCode) {
            //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
            $.get('/chl_edit/view.html?agencyCode=' + agencyCode + '&channelCode=' + channelCode, null, function(form) {
                addBoxIndex = layer.open({
                    type: 1,
                    title: '中介机构信息',
                    content: form,

                    shade: false,
                    offset: ['100px', '30%'],
                    area: ['600px', '550px'],
                    zIndex: 19991231,
                    id: 'lay_agent',
                    maxmin: false,

                    full: function(elem) {
                        var win = window.top === window.self ? window : parent.window;
                        $(win).on('resize', function() {
                            var $this = $(this);
                            elem.width($this.width()).height($this.height()).css({
                                top: 0,
                                left: 0
                            });
                            elem.children('div.layui-layer-content').height($this.height() - 95);
                        });
                    },
                    end: function() {
                        addBoxIndex = -1;
                    }
                });
                layer.full(addBoxIndex);
            });
        }

    });
</script>
</body>

</html>