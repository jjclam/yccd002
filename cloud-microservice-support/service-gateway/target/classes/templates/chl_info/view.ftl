<div style="margin: 15px;">
	<form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline" >
                    <label class="layui-form-label">中介机构代码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.agencyCode?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">中介机构名称</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.agencyName?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">中介机构类别</label>
                <div class="layui-input-block" style="display:flex" value="${item.agencyType?if_exists }">
                    <#--<input type="text" class="layui-input" disabled="disabled" value="${item.agencyType?if_exists }">-->
                    <#if item.agencyType = "01">
                <input type="text" disabled="disabled" class="layui-input" value="专业代理" >
                    <#elseif item.agencyType = "02">
                <input type="text" disabled="disabled" class="layui-input" value="兼业代理" >
                    <#elseif item.agencyType = "03">
                <input type="text" disabled="disabled" class="layui-input" value="经纪公司" >
                    <#elseif item.agencyType = "04">
                <input type="text" disabled="disabled" class="layui-input" value="专业技术" >
                    <#else>
                <input type="text" disabled="disabled" class="layui-input" value="" >
                    </#if>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">保险许可证号码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.licenseNo?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">地址</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.address?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">联系人</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.linkMan?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">联系方式</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.phone?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">成立日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.foundDate?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">停业标志</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.endFlag?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">停业日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.endDate?if_exists }">
                    <#--<input type="text" class="layui-input" disabled
                    ="disabled" >-->
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-hide">
                <label class="layui-form-label">渠道编码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.channelCode?if_exists }">
                </div>
            </div>
            <div class="layui-hide">
                <label class="layui-form-label">渠道名称</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.channelName?if_exists }">
                </div>
            </div>
        </div>
		<div class="layui-form-item">
            <div class="layui-hide">
                <label class="layui-form-label">代理人编号</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.agentCode?if_exists }">
                </div>
            </div>
		</div>

		<button lay-filter="edit" lay-submit style="display: none;"></button>
	</form>
</div>

<blockquote class="layui-elem-quote">企业基本信息</blockquote>
<div style="margin: 15px;">
    <form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline" >
                <label class="layui-form-label">企业全称</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.COMPANYNAME?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">公司地址</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.COMADDRESS?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">法定代表人</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.LEGALPERSON?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">法定代表人身份证号</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.IDNO?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">联系人姓名</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.LINKNAME?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">联系人电话</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.LINKPHONE?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">联系邮箱</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.LINKEMAIL?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">邮政编码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.ZIPNO?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">企业类型</label>
                <div class="layui-input-block" style="display:flex" value="${comItem.COMTYPE?if_exists }">
                    <#--<input type="text" class="layui-input" disabled="disabled" value="${comItem.COMTYPE?if_exists }">-->
                    <#if comItem.COMTYPE = "1">
                <input type="text" disabled="disabled" class="layui-input" value="保险经纪" >
                    <#elseif comItem.COMTYPE = "2">
                <input type="text" disabled="disabled" class="layui-input" value="保险代理" >
                    <#elseif comItem.COMTYPE = "3">
                <input type="text" disabled="disabled" class="layui-input" value="兼业代理" >
                    <#elseif comItem.COMTYPE = "4">
                <input type="text" disabled="disabled" class="layui-input" value="第三方平台" >
                    <#else>
                    <input type="text" disabled="disabled" class="layui-input" value="">
                    </#if>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">是否有网销资质</label>
                <div class="layui-input-block" style="display:flex" value="${comItem.ISNET?if_exists }">
                    <#--<input type="text" class="layui-input" disabled="disabled" value="${comItem.ISNET?if_exists }">-->
                    <#if comItem.COMTYPE = "0">
                <input type="text" disabled="disabled" class="layui-input" value="否" >
                    <#elseif comItem.COMTYPE = "1">
                <input type="text" disabled="disabled" class="layui-input" value="是" >
                    <#else>
                    <input type="text" disabled="disabled" class="layui-input" value="">
                    </#if>
                </div>
            </div>
        </div>

    </form>
</div>

<blockquote class="layui-elem-quote">企业证件信息</blockquote>
<div style="margin: 15px;">
    <form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline" >
                <label class="layui-form-label">企业证件类型</label>
                <div class="layui-input-block" style="display:flex" value="${comItem.COMIDTYPE?if_exists }">
                    <#--<input type="text" class="layui-input" disabled="disabled" value="${comItem.COMIDTYPE?if_exists }">-->
                    <#if comItem.COMTYPE = "1">
                <input type="text" disabled="disabled" class="layui-input" value="营业执照" >
                    <#else>
                    <input type="text" disabled="disabled" class="layui-input" value="">
                    </#if>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">统一社会信用代码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.COMIDNO?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">企业证件有效起始日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${(comItem.COMIDSTART?string("yyyy-MM-dd")?if_exists)!}">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">企业证件有效终止日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${(comItem.COMIDEND?string("yyyy-MM-dd")?if_exists)!}">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline" style="width: 400px;">
                <label class="layui-form-label">《经营保险经纪/代理业务许可证》编码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${comItem.AGENTBUSINO?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline" style="width: 400px;">
                <label class="layui-form-label">《经营保险经纪/代理业务许可证》有效起始日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${(comItem.AGENTBUSISTART?string("yyyy-MM-dd")?if_exists)!}">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline" style="width: 400px;">
                <label class="layui-form-label">《经营保险经纪/代理业务许可证》有效终止日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${(comItem.AGENTBUSIEND?string("yyyy-MM-dd")?if_exists)!}">
                </div>
            </div>
        </div>
    </form>
</div>

