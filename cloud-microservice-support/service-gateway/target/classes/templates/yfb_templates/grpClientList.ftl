﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>团体客户管理</title>
<#-- <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
 <link rel="stylesheet" href="../css/global.css" media="all">
 <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
 <link rel="stylesheet" href="../css/table.css"/>-->

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/plugins/YFB/layui/css/layui.css" media="all" />
    <style>
        .layui-form-item .layui-inline{
            width:300px;
        }
        .layui-table-header{
            width:820px;
        }
        .layui-form-label{
            width:100px;
        }
    </style>
</head>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
        <legend></legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="layui-form-label" style="width: 90px;">团体客户名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="name" id="name" autocomplete="off" class="layui-input">
                </div>
                <button id="searchInfo" type="button" data-type="reload" class="layui-btn" style="margin-left:65px;">
                    <i class="fa fa-plus" aria-hidden="true"></i> 查询
                </button>
            </div>
    </fieldset>

    <table class="layui-hide" id="test" lay-filter="test"></table>

    <div style="margin: 15px;">
        <legend>企业信息</legend>
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">单位名称</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" name="GrpName" maxlength="20" id="grpName" lay-verify="required" lay-reqtext="单位名称不能为空！" placeholder="请输入" autocomplete="off" class="layui-input">
                        <span style="color: red;">*</span>
                    </div>

                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">单位属性</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="GrpNature" id="grpNature" lay-verify="required" lay-search>
                            <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                        </select>
                       <span style="color: red;">*</span>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">行业类别</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="BusinessType" id="businessType" lay-verify="required" lay-search>
                            <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                        </select><span style="color: red;">*</span>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">单位员工总数</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="peoples" name="Peoples" lay-verify="peoplesS" ><span style="color: red;">*</span>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">在职人数</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="onWorkPeoples" name="OnWorkPeoples" lay-verify="peoplesS" ><span style="color: red;">*</span>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">投保人数</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="insurePeoples" name="InsurePeoples" lay-verify="peoplesS" ><span style="color: red;">*</span>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">单位证件类型</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="ComDocIDType" id="comdocidtype" lay-verify="required" lay-search>
                            <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                        </select><span style="color: red;">*</span>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">单位证件号码</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="comdocidNo" maxlength="10" name="ComDocIDNo" lay-verify="required" ><span style="color: red;">*</span>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">单位证件有效期</label>
                    <div class="layui-input-block" style="display:flex">
                        <#--<input type="text" class="layui-input" id="unitPapersLimit" lay-verify="required" value="${item.modifyDate?if_exists }">-->
                        <input type="text" name="BusliceDate" id="busliceDate" lay-verify="required|date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
                        <span style="color: red;">*</span>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">邮政编码</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input"  id="postCode" name="PostCode" lay-verify="isZipCode" ><span style="color: red;">*</span>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline" style="width:600px;">
                    <label class="layui-form-label">单位地址</label>
                    <div class="layui-input-block" style="display:flex">
                        <input id="grpAddress" class="layui-input" maxlength="40" name="GrpAddress" lay-verify="required" ><span style="color: red;">*</span>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">单位传真</label>
                    <div class="layui-input-block" style="display:flex">
                        <input id="Fax" class="layui-input" name="Fax" maxlength="6" lay-verify="" >
                    </div>
                </div>
            </div>
        <div class="layui-form-item">
            <div class="layui-inline" style="width:600px;">
                <label class="layui-form-label">单位注册地址</label>
                <div class="layui-input-block" style="display:flex">
                    <input  class="layui-input" lay-verify="required" maxlength="40" id="unitRegisteredAddress" name="UnitRegisteredAddress"> <span style="color: red;">*</span>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label" style="width:150px;">单位存续时间（年）</label>
                <div class="layui-input-block" style="display:flex">
                    <input  class="layui-input" lay-verify="peoplesS" maxlength="3" id="unitDuration" name="UnitDuration"> <span style="color: red;">*</span>
                </div>
            </div>
        </div>
        <legend>保险联系人</legend>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">姓名</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input"  id="principal" name="Principal" lay-verify="checkTrueName"><span style="color: red;">*</span>
                </div>

            </div>
            <div class="layui-inline">
                <label class="layui-form-label">证件类型</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="PrincipalIDType" id="principalIDType" lay-verify="required" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select><span style="color: red;">*</span>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">证件号码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" id="principalIDNo" name="PrincipalIDNo" lay-verify="required|identity"><span style="color: red;">*</span>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">证件有效期至</label>
                <div class="layui-input-block" style="display:flex">
                        <input type="text" name="PrincipalValidate" id="principalValidate" lay-verify="required|date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input"><span style="color: red;">*</span>
                </div>

            </div>
            <div class="layui-inline">
                <label class="layui-form-label">联系电话</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" id="telephone" name="Telephone" lay-verify="checkPhone">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">手机号码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" id="mobile" name="Mobile" lay-verify="checkMobile">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">E-MAIL</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" id="eMail" name="EMail" lay-verify="checkEmail">
                </div>

            </div>
            <div class="layui-inline">
                <label class="layui-form-label">保单年金领取日</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" name="AnnuityReceiveDate" id="annuityReceiveDate" lay-verify="required|date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input"><span style="color: red;">*</span>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline" style="width:600px;">
                <label class="layui-form-label">所属部门</label>
                <div class="layui-input-block" style="display:flex">
                    <input  class="layui-input" id="managecom" maxlength="6" name="Managecom" lay-verify="" >
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">职务</label>
                <div class="layui-input-block" style="display:flex">
                    <input  class="layui-input" placeholder="" maxlength="30" id="headShip" name="HeadShip" lay-verify="">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">收款方式</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="GetFlag" id="getFlag" lay-verify="required" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select><span style="color: red;">*</span>
                </div>

            </div>
            <div class="layui-inline">
                <label class="layui-form-label">争议处理方式</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="DisputedFlag" id="disputedFlag" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">开户银行</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="BankCode" id="bankCode" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>

            </div>
            <div class="layui-inline">
                <label class="layui-form-label">账号</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" id="bankAccNo" name="BankAccNo" maxlength="20" lay-verify="">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">账户名</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" id="accName" name="AccName" maxlength="30" lay-verify="">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">开户银行所在省</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="BankProivnce" id="bankProivnce" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>

            </div>
            <div class="layui-inline">
                <label class="layui-form-label">开户行所在市</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="BankCity" id="bankCity" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">卡折类型</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="AccType" id="accType" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">银行网点</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="BankLocations" id="bankLocations" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>

            </div>
            <div class="layui-inline">
                <label class="layui-form-label">纳税人身份类型</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="TaxPayerIDType" id="taxPayerIDType" lay-verify="required" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select><span style="color: red;">*</span>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">纳税人识别号</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="TaxPayerIDNO" id="taxPayerIDNO" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline" style="width:600px;">
                <label class="layui-form-label">税局登记地址</label>
                <div class="layui-input-block" style="display:flex">
                    <input class="layui-input"  id="taxBureauAddr" name="TaxBureauAddr" maxlength="80" >
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">税局登记电话</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" id="taxBureauTel" name="TaxBureauTel" lay-verify="checkPhone">
                </div>

            </div>
            <div class="layui-inline">
                <label class="layui-form-label">税局登记开户行</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="TaxBureauBankCode" id="taxBureauBankCode" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">税局登记账号</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" id="taxBureauBankAccNo" name="TaxBureauBankAccNo" maxlength="20" lay-verify="">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">开票类型</label>
                <div class="layui-input-block" style="display:flex">
                    <select name="BillingType" id="billingType" lay-verify="" lay-search>
                        <option value=""></option>
                                <#list laChlAgencyType as ChlInfo>
                                    <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                                </#list>
                    </select>
                </div>
            </div>
        </div>

        <#--<legend>所属上级客户</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTableTwo" style="width:50%" >
                <thead>
                <tr>
                    <th>团体客户号</th>
                    <th>团体客户名称</th>
                    <th>地址</th>
                </tr>
                </thead>
                <tbody id="contentTwo">
                <tr>
                    <td>1112222333</td>
                    <td>杰克东</td>
                    <td>京东杭州</td>
                </tr>
                </tbody>
            </table>
        </div>
        <legend>附属业务员信息</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTableThree" style="width:50%" >
                <thead>
                <tr>
                    <th>序号</th>
                    <th>业务员编码</th>
                    <th>业务员姓名</th>
                    <th>职位</th>
                </tr>
                </thead>
                <tbody id="contentThree">
                </tbody>
            </table>
        </div>-->


            <button id="addOne" type="button" class="layui-btn" >
                <i class="fa fa-plus" aria-hidden="true"></i> 上级客户关联
            </button>
            <button id="addTwo" type="button" class="layui-btn" >
                <i class="fa fa-plus" aria-hidden="true"></i> 业务员信息录入
            </button>
            <button type="button" class="layui-btn site-demo-active" lay-submit lay-filter="demo1" >
                保存
            </button>
        <#--<button lay-filter="edit" lay-submit >上级客户关联</button>
        <button lay-filter="edit" lay-submit >业务员信息录入</button>
        <button lay-filter="edit" lay-submit >保存</button>-->
        </form>
    </div>
</div>
<!--模板-->
<!--模板-->
<script src="/plugins/YFB/layui/layui.js"></script>
<script>
    layui.use(['table','layer'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var index = layer.load(1);

        table.render({
            elem: '#test'
            ,method:'post'
            ,loading:true  //翻页加loading
            ,url:'/GrpClient/listData/' //数据接口
            ,cols: [[    //表头
                {type:'radio'}
                ,{type:'numbers',title:'序号',sort: true,width:80}
                ,{field:'customerno', title: '团体客户号',width:160}
                ,{field:'grpname',  title: '团体客户名称',width:160}
                ,{field:'grpaddress', title: '地址',width:300}
                ,{field:'principal',  title: '联系人',width:100}
            ]]
            ,page: true
            ,done:function (res) {
                layer.close(index);   //返回数据关闭loading
            }
        });

        //根据条件查询数据
        $('#searchInfo').on('click', function(){
            var table = layui.table;
            var name = $('#name').val();
            table.reload('test', {
                method: 'post'
                , where: {
                    'grpname': name
                }
                , page: {
                    curr: 1
                }
            });
        });

        //监听行单击事件（双击事件为：rowDouble）
        table.on('radio(test)', function(obj){
            var data = obj.data;
            console.log(data);
            var grpname = data.grpname;
            $("#grpName").val(grpname);
            $("#grpNature").val(data.grpnature);
            /*layer.alert(JSON.stringify(data), {
                title: '当前行数据：'
            });*/
            //标注选中样式
            obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        });
    });
</script>


<script type="text/html" id="rank">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<script>
    //表单监听提交
    layui.use(['form','laydate'], function() {
        var form = layui.form
                ,layer = layui.layer
                ,laydate = layui.laydate;
        //单位证件有效期
        laydate.render({
            elem:'#busliceDate',
            /*value:new Date(),
            done:function(value,obj){

            }*/
        });
        laydate.render({
            elem:'#principalValidate',
            /*value:new Date(),
            done:function(value,obj){
             annuityReceiveDate
            }*/
        });
        laydate.render({
            elem:'#annuityReceiveDate',
            /*value:new Date(),
            done:function(value,obj){

            }*/
        });
        //表单校验
        form.verify({
            //非空的非负整数校验
            peoplesS: function (value, item) {
                var re = new RegExp("^(0|[1-9][0-9]*)$");
                if (!value) {
                    return '必填项不能为空';
                }
                if (!re.test(value)){
                    return '应为非负整数';
                }
            },
            //校验邮编
            isZipCode:function (value, item) {
                var re = new RegExp("^[0-9]{6}$");
                if (!value) {
                    return '必填项不能为空';
                }
                if (!re.test(value)){
                    return '请正确填写您的邮政编码';
                }
            },
            //校验姓名
            checkTrueName:function (value, item) {
                var re = new RegExp("^(([a-zA-Z+\\.?\\·?a-zA-Z+]{2,30}$)|([\u4e00-\u9fa5+\\·?\u4e00-\u9fa5+]{2,30}$))");
                if (!value) {
                    return '必填项不能为空';
                }
                if (!re.test(value)){
                    return '请正确填写您的姓名';
                }
            },
            //校验联系电话
            checkPhone:function (value, item) {
                var re = new RegExp("^0\\d{2,3}-?\\d{7,8}$");
                if (value) {
                    if (!re.test(value)){
                        return '请正确填写您的联系电话';
                    }
                }

            },
            //校验手机号
            checkMobile:function (value, item) {
                var re = new RegExp("^1[3|4|5|7|8]\\d{9}$");
                if (value) {
                    if (!re.test(value)){
                        return '请正确填写您的手机号码';
                    }
                }
            },
            //校验邮箱
            checkEmail:function (value, item) {
                var re = new RegExp("^[a-z0-9._%-]+@([a-z0-9-]+\\.)+[a-z]{2,4}$|^1[3|4|5|7|8]\\d{9}$");
                if (value) {
                    if (!re.test(value)){
                        return '请正确填写您的邮箱';
                    }
                }
            }
        });

        //监听提交
        form.on('submit(demo1)', function(data){
            //防止重复提交
            $('.site-demo-active').attr("disabled","disabled");
            var index = layer.load(1);
            /*layer.alert(JSON.stringify(data.field), {
                title: '最终的提交信息'
            });*/
            $.ajax({
                url:"/GrpClient/saveData/",
                async:true,
                type:"post",
                /*dataType:"text",*/
                data:data.field,
                success:function (data) {

                    layer.msg(data.msg);
                    layer.close(index);
                    $('.site-demo-active').removeAttr('disabled');
                }
            });

            return false;
        });
    });

</script>

<script>
    $(window).on('load',function(){});
</script>
<script>

    //添加上级客户
    $('#addOne').on('click', function() {
        //触发事件
        layer.open({
            type: 2,
            skin: 'layui-layer-demo', //样式类名
            title: '上级客户关联',
            closeBtn: false, //不显示关闭按钮
            btn: '关闭',
            anim: 2,
            area: ['1000px', '450px'],
            shadeClose: true, //开启遮罩关闭
            content: '/GrpClient/addClient/'
        });
        /*$.get('/GrpClient/addClient', null, function(form) {*/
    });

    //添加业务员
    $('#addTwo').on('click', function() {
        //触发事件
        layer.open({
            type: 2,
            skin: 'layui-layer-demo', //样式类名
            title: '业务员信息录入',
            closeBtn: false, //不显示关闭按钮
            btn: '关闭',
            anim: 2,
            area: ['1000px', '450px'],
            shadeClose: true, //开启遮罩关闭
            content: '/GrpClient/addBM/'
        });
    });
</script>
</body>

</html>