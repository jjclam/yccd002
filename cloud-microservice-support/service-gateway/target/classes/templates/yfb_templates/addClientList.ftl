﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>团体客户管理</title>

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/plugins/YFB/layui/css/layui.css" media="all" />
    <style>
        .layui-table-header{
            width:794px;
        }
    </style>
</head>
<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
        <legend></legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="layui-form-label" style="width: 90px;">团体客户名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="name" id="name" autocomplete="off" class="layui-input">
                </div>
                <button id="searchInfo" type="button" data-type="reload" class="layui-btn" style="margin-left:65px;">
                    <i class="fa fa-plus" aria-hidden="true"></i> 查询
                </button>
            </div>
    </fieldset>

    <table class="layui-hide" id="test" style="width: 80%;" lay-filter="test"></table>
    <div class="layui-form-item" style="margin:0;">

        <button id="confirmClient" type="button" data-type="reload" class="layui-btn" style="margin-left:65px;">
            <i class="fa fa-plus" aria-hidden="true"></i> 确定
        </button>
    </div>

</div>
<!--模板-->
<!--模板-->
<script src="/plugins/YFB/layui/layui.js"></script>
<script>
    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#test'
            ,url:'/GrpClient/listData/' //数据接口
            ,cols: [[
                {type:'radio'}
                ,{type:'numbers',title:'序号',sort: true,width:80}
                ,{field:'id',  title: '投保单号',hide:true}
                ,{field:'parentId', title: '团体客户号',width:160}
                ,{field:'name',  title: '团体客户名称',width:160}
                ,{field:'sort', title: '地址',width:300}
                ,{field:'path',  title: '联系人',width:80}
            ]]
            ,page: true
        });

        $('#searchInfo').on('click', function(){
            var table = layui.table;
            var name = $('#name').val();
            table.reload('test', {
                method: 'post'
                , where: {
                    'name': name,
                }
                , page: {
                    curr: 1
                }
            });
        });

        //监听行单击事件（双击事件为：rowDouble）
        table.on('radio(test)', function(obj){
            var data = obj.data;
            var name = data.name;

            alert("333");

            /*layer.alert(JSON.stringify(data), {
                title: '当前行数据：'
            });*/
            //标注选中样式
            obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        });
    });
</script>


<script type="text/html" id="rank">
    {{d.LAY_TABLE_INDEX+1}}
</script>


<script>


</script>
</body>

</html>