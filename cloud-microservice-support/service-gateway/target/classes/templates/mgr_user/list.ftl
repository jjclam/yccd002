﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>用户管理配置</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
    <fieldset class="layui-elem-field">
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 10px;">
                <label class="layui-form-label">用户账号</label>
                <div class="layui-input-inline">
                    <input type="text" name="userName" id="userName" autocomplete="off" class="layui-input">
                </div>
                <label class="layui-form-label">用户姓名</label>
                <div class="layui-input-inline">
                    <input type="text" name="realName" id="realName" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item" style="">
                <label class="layui-form-label">所属系统</label>
                <div class="layui-input-inline">
                    <select id="field" lay-filter="onchangePosition">
                        <option value="">请选择所属系统</option>
                        <option value="0">0-外勤</option>
                        <option value="1">1-企业HR</option>
                        <option value="2">2-中台</option>
                    </select>
                </div>
                <label class="layui-form-label">岗位</label>
                <div class="layui-input-inline">
                <#--<input type="text" name="position" id="position" autocomplete="off" class="layui-input">-->
                    <select name="position" id="position" lay-search>
                        <option value="">请选择岗位</option>
                    <#--<#list laposition as SysUser>-->
                    <#--<option value="${(SysUser.position)!}">${(SysUser.position)!}</option>-->
                    <#--</#list>-->
                    <#--联动-->
                    </select>
                </div>

                <div class="layui-inline" style="margin-top: 15px;">
                    <label class="layui-form-label">网点</label>
                    <div class="layui-input-inline">
                        <select name="wangbit" id="wangbit" lay-verify="required" lay-search>
                            <option value=""></option>
                        <#list lacoms as lacomss>
                            <option value="${(lacomss.name)!}">${(lacomss.name)!}</option>
                        </#list>
                        </select>
                    </div>
                </div>
            </div>


            <div class="layui-form-mid layui-word-aux" style="margin-top: 5px;">
                <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 用户查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 5px;">
                <button id="add" type="button" class="layui-btn" style="margin-left: 5px;">
                    <i class="fa fa-plus" aria-hidden="true"></i> 用户增加
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 5px; margin-left: 5px">
                <button id="delete" class="layui-btn">
                    <i class="fa fa-times" aria-hidden="true"></i> 用户删除
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 5px; margin-left: 5px">
                <button id="update" class="layui-btn">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> 用户更新
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" >
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>用户信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>用户账号</th>
                    <th>用户姓名</th>
                    <th>网点</th>
                    <th>企业</th>
                    <th>所属系统</th>
                    <th>岗位</th>
                    <th>用户状态</th>
                    <th>邮箱</th>
                    <th>手机号</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.id == undefined ?"":item.id }}</td>
        <td>{{ item.userName == undefined ?"":item.userName }}</td>
        <td>{{ item.realName == undefined ?"":item.realName }}</td>
        <td>{{ item.institutions == undefined ?"":item.institutions }}</td>
        <td class="layui-hide">{{ item.institutions == undefined ?"":item.institutions }}</td>
        <td>{{ item.remark == undefined ?"":item.remark }}</td>
        <td>
            {{# if(item.field === '0'){ }} <span style="">外勤</span> {{# } }}
            {{# if(item.field === '1'){ }} <span style="">企业HR</span> {{# } }}
            {{# if(item.field === '2'){ }} <span style="">中台</span> {{# } }}
        </td>
        <td>{{ item.position == undefined ?"":item.position }}</td>
        <td>
            {{# if(item.status === '0'){ }} <span style="color: green">正常</span> {{# } }}
            {{# if(item.status === '1'){ }} <span style="color: red">停用</span> {{# } }}
        </td>
        <td>{{ item.email == undefined ?"":item.email }}</td>
        <td>{{ item.telephone == undefined ?"":item.telephone }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/mgr_user/list',
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                   alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });

        $("#reset").click(function () {
            $("#userName").val("");
            $("#realName").val("");
            $("#field").val("");
            $("#wangbit").val("");
            $("#position").empty().append("<option value=\"\">请先选择所属系统</option>");
            form.render();
        });

        $('#search').on('click', function () {
            var userName = $("#userName").val();
            var realName = $("#realName").val();
            var field = $("#field").val();
            var institutions = $("#position").val();
            var wangbit = $("#wangbit").val();

            paging.get({
                "userName": userName,
                "realName": realName,
                "field": field,
                "position": institutions,
                "institutions": wangbit,
                "v": new Date().getTime()
            });
        });

        $('#add').on('click', function () {

            $.get('/mgr_user/add.html', function(form) {
                layer.open({
                    type: 1,
                    title: '用户增加',
                    content: form,
                    btn: ['提交', '返回'],
                    shade: false,
                    offset: 'auto',
                    area : ['950px', '480px'],
                    zIndex: 19991231,
                    id: 'lay_addUser',
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form-item').find('button[lay-filter=edit]').click();
                    },
                    success: function(layero, index) {
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var userName = $("#userName5").val();
                            // alert(userName);
                            var password = $("#password5").val();
                            var rePassword = $("#rePassword5").val();
                            var realName = $("#realName5").val();
                            // var userType = $("#userType5").val();
                            var institutions = $("#institutions5").val();
                            var field = $("#field5").val();
                            var email = $("#email5").val();
                            var position = $("#position5").val();
                            var telephone = $("#telephone5").val();
                            var linkedPhone = $("#linkedPhone5").val();
                            var wangbit = $("#wangbit").val();
                            if (userName == '') {
                                alert("请输入用户账号!");
                                return false;
                            }
                            if (realName == '') {
                                alert("请输入用户姓名!");
                                return false;
                            }
                            if (wangbit == '') {
                                alert("请选择网点！");
                                return false;
                            }
                            // if (institutions == '') {
                            //     alert("请输入机构!");
                            //     return false;
                            // }
                            if (field == '') {
                                alert("请输入所属系统!");
                                return false;
                            }
                            // if (position == '') {
                            //     alert("请输入岗位!");
                            //     return false;
                            // }
                            if (email != '') {
                                // alert(email);
                                var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
                                if (!reg.test(email)) {
                                    alert("邮箱格式不正确!");
                                    return false;
                                }
                            }
                            if (telephone != '') {
                                // alert(email);
                                var reg = /^1[3|4|5|8][0-9]\d{4,8}$/;
                                if (!reg.test(telephone)) {
                                    alert("手机号格式不正确!");
                                    return false;
                                }
                            }
                            if (password == '') {
                                alert("请输入密码!");
                                return false;
                            }
                            if (rePassword == '') {
                                alert("请再次输入密码!");
                                return false;
                            }
                            if (password != rePassword) {
                                alert("两次输入的密码不相同, 请重新输入! ");
                                return false;
                            }

                            $.ajax({
                                type: "POST",
                                url: "/mgr_user/addUser",
                                data: "params=" + JSON.stringify({
                                    userName: userName
                                    , password: password
                                    , realName: realName
                                    , institutions: institutions
                                    , field: field
                                    , email: email
                                    , position: position
                                    , telephone: telephone
                                    , linkedPhone: linkedPhone
                                    , wangbit: wangbit
                                }),
                                dataType: 'json',
                                success: function(data){
                                    if(data.msg == '0000') {
                                        alert('添加成功');
                                        // location.reload(); //刷新
                                        layer.closeAll();
                                        $('#search').click();//刷新页面
                                    }else {
                                        alert('添加失败, ' + data.msg);
                                        console.log(data.msg);
                                        // location.reload(); //刷新
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                });
            });
        });

        $('#update').on('click', function () {
            var userNames = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    userNames += n + ',';
                    record++;
                }
            });
            if (record == '') {
                alert("请先选择一条用户! ");
                return false;
            }
            if (record > 1) {
                alert("一次只能选择一条用户修改! ");
                return false;
            }

            $.get('/mgr_user/edit.html?userNames=' + userNames, function(form) {
                layer.open({
                    type: 1,
                    title: '用户更新',
                    content: form,
                    btn: ['提交', '返回'],
                    shade: false,
                    offset: 'auto',
                    area : ['850px', '400px'],
                    zIndex: 19991231,
                    id: 'lay_editUser',
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form-item').find('button[lay-filter=edit]').click();
                    },
                    success: function(layero, index) {
                        var form = layui.form();
                        form.render('select');
                        form.on('submit(edit)', function(data) {
                            var userName = $("#userName6").val();
                            var oldUserName = $("#userName0").val();
                            // alert(userName);
                            var password = $("#password6").val();
                            var rePassword = $("#rePassword6").val();
                            var realName = $("#realName6").val();
                            // var userType = $("#userType6").val();
                            var institutions = $("#wangbit2").val();
                            var field = $("#field6").val();
                            var status = $("#status6").val();
                            var email = $("#email6").val();
                            var position = $("#position6").val();
                            var telephone = $("#telephone6").val();
                            var linkedPhone = $("#linkedPhone6").val();
                            var userId = $("#userId6").val();
                            if (userName == '') {
                                alert("请输入用户账号!");
                                return false;
                            }
                            if (realName == '') {
                                alert("请输入用户姓名!");
                                return false;
                            }
                            // if (userType == '') {
                            //     alert("请输入用户类型!");
                            //     return false;
                            // }
                            if (field == '') {
                                alert("请输入所属系统!");
                                return false;
                            }
                            // alert(field);
                            if (field == '1' && institutions == '') {
                                alert("请输入企业!");
                                return false;
                            }
                            if (email != '') {
                                // alert(email);
                                var reg = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
                                if (!reg.test(email)) {
                                    alert("邮箱格式不正确!");
                                    return false;
                                }
                            }
                            if (telephone != '') {
                                // alert(email);
                                var reg = /^1[3|4|5|8][0-9]\d{4,8}$/;
                                if (!reg.test(telephone)) {
                                    alert("手机号格式不正确!");
                                    return false;
                                }
                            }
                            if (password == '') {
                                alert("请输入密码!");
                                return false;
                            }
                            if (rePassword == '') {
                                alert("请再次输入密码!");
                                return false;
                            }
                            if (password != rePassword) {
                                alert("两次输入的密码不相同, 请重新输入! ");
                                return false;
                            }

                            $.ajax({
                                type: "POST",
                                url: "/mgr_user/updateUser",
                                data: "params=" + JSON.stringify({
                                    userName: userName
                                    , oldUserName: oldUserName
                                    , password: password
                                    , realName: realName
                                    // , userType: userType
                                    , institutions: institutions
                                    , field: field
                                    , status: status
                                    , email: email
                                    , position: position
                                    , telephone: telephone
                                    , linkedPhone: linkedPhone
                                    , userId: userId
                                }),
                                dataType: 'json',
                                success: function(data){
                                    if(data.msg == '0000') {
                                        alert('修改成功');
                                        // location.reload(); //刷新
                                        layer.closeAll();
                                        $('#search').click();//刷新页面
                                    }else {
                                        alert('修改失败, ' + data.msg);
                                        console.log(data.msg);
                                        // location.reload(); //刷新
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                });
            });
        });

        $('#delete').on('click', function () {
            var userNames = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    userNames += n + ',';
                    record++;
                }
            });
            if (record == '') {
                alert("请先选择一条用户!");
                return false;
            }
            $.ajax({
                type: "POST",
                url: '/mgr_user/deleteUser',
                data: {userNames: userNames},
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('删除成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('删除失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });

        form.on('select(onchangePosition)', function (data) {
            form.render('select');
            var field = $("#field").val();

            if (field == "") {
                var select = document.getElementById("position");
                var newOption = document.createElement("option");
                newOption.text = "请先选择所属系统!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/mgr_user/linkedPositions',
                data:{field: field},
                success: function(data){
                    var dataList = data.linkedPositions;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#position").empty().append("<option value=\"\">请先选择所属系统</option>");
                        form.render('select');
                        return;
                    }
                    if(dataList!=null){
                        $("#position").empty();
                        var select = document.getElementById("position");
                        //联动框非强制选;默认空串
                        var newOption = document.createElement("option");
                        newOption.text = "请选择岗位";
                        newOption.value = "" ;
                        select.options.add(newOption);
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            // var code = categoryObj.customerno;
                            var codeName = categoryObj.position;
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            // newOption.text = code + '-' + codeName;
                            // newOption.value = code + '-' + codeName;
                            newOption.text = codeName;
                            newOption.value = codeName;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }
                },error:function(data){
                    alert('系统错误');
                }
            });

            // if (field5 != 1) {
            //     $("#institutions5").empty().append("<option value=\"\">企业HR需要选择企业</option>");
            //     form.render('select');
            //     return;//只有企业HR才需要录入企业信息
            // }

            // $.ajax({
            //     type:'post',
            //     dataType:'json',
            //     url:'/mgr_user/linkedInstitutions',
            //     data:{field: field5},
            //     success: function(data){
            //         var dataList = data.linkedInstitutions;
            //         var error = data.msg;
            //         // alert(dataList);
            //         if (error) {
            //             $("#institutions5").empty().append("<option value=\"\">请先选择所属系统</option>");
            //             form.render();
            //             return;
            //         }
            //         if(dataList!=null){
            //             $("#institutions5").empty();
            //             var select = document.getElementById("institutions5");
            //             for(var i = 0; i < dataList.length; i++) { //遍历
            //                 var categoryObj = dataList[i];
            //                 var code = categoryObj.customerno;
            //                 var codeName = categoryObj.grpname;
            //                 //进行添加到标签里
            //                 var newOption = document.createElement("option");
            //                 newOption.text = code + '-' + codeName;
            //                 newOption.value = code;
            //                 // newOption.value = code + '-' + codeName;
            //                 select.options.add(newOption);
            //             }
            //             form.render();
            //             // form.render('select');
            //         }
            //
            //     },error:function(data){
            //         alert('系统错误');
            //     }
            // });

        });


    });
</script>
</body>

</html>