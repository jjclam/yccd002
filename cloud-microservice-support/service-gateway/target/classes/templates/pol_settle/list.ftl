﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>保费统计</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 50px">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="">
                <label class="layui-form-label">产品编码<font color=red>*</font></label>
                <div class="layui-input-inline layui-unselect">
                    <select name="productCode" id="productCode" class="form-control" lay-search>
                        <option value="">请选择产品编码</option>
                    <#list proInfo as ProInfo>
                        <option value="${(ProInfo.proCode)!}">${(ProInfo.proCode)!}-${(ProInfo.proName)!}</option>
                    </#list>
                    </select>
                </div>
            </div>
            <div class="layui-form-item" style="margin-top: 2px">
                <div class="layui-inline">
                    <label class="layui-form-label">保单生效起期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="validateStartDate" id="validateStartDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">保单生效止期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="validateEndDate" id="validateEndDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="margin-top: 2px">
                <div class="layui-inline">
                    <label class="layui-form-label">保单投保起期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="applyStartDate" id="applyStartDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">保单投保止期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="applyEndDate" id="applyEndDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    </div>
                </div>
            </div>
            <div class="layui-form" style="margin-top: 2px;">
                <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 10px;">
                    <button type="button" class="layui-btn " id="settleExcel" name="settleExcel">
                        <i class="fa fa-file-excel-o"></i> 导出已结算保单
                    </button>
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 10px;">
                    <button type="button" class="layui-btn " id="unSettleExcel" name="unSettleExcel">
                        <i class="fa fa-file-excel-o"></i> 导出未结算保单
                    </button>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="layui-elem-field">
        <legend>保单结算</legend>
        <div class="layui-form-item" >
            <label class="layui-form-label">文件上传：</label>
            <div class="layui-upload">
                <input type="file" name="file" class="layui-upload-file" lay-ext="xls|xlsx|xlsm|xlt|xltx|xltm" lay-title="请上传Excel文件">
            </div>
        </div>
    </fieldset>
</div>

<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        $('#settleExcel').on('click', function () {
            var productCode = $("#productCode").val();
            var validateStartDate = $("#validateStartDate").val();
            var validateEndDate = $("#validateEndDate").val();
            var applyStartDate = $("#applyStartDate").val();
            var applyEndDate = $("#applyEndDate").val();

            window.location.href='/pol_settle/settleExcel?exportFlag=1&productCode=' + productCode + '&validateStartDate=' + validateStartDate + '&validateEndDate=' + validateEndDate + '&applyStartDate=' + applyStartDate + '&applyEndDate=' + applyEndDate;
        });

        $('#unSettleExcel').on('click', function () {
            var productCode = $("#productCode").val();
            var validateStartDate = $("#validateStartDate").val();
            var validateEndDate = $("#validateEndDate").val();
            var applyStartDate = $("#applyStartDate").val();
            var applyEndDate = $("#applyEndDate").val();

            window.location.href='/pol_settle/settleExcel?exportFlag=0&productCode=' + productCode + '&validateStartDate=' + validateStartDate + '&validateEndDate=' + validateEndDate + '&applyStartDate=' + applyStartDate + '&applyEndDate=' + applyEndDate;
        });

    });
</script>


<script type="text/javascript">
    layui.use(['form','upload'],function(){
        layui.upload({
            url: '/pol_settle/importData',
            accept: "file",
            before: function(input){
                //返回的参数item，即为当前的input DOM对象
                console.log('文件上传中');
            }
            ,success: function(res){
                console.log('上传完毕');
                alert('上传成功！');
            }
        });
    });

</script>
</body>

</html>