﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>渠道信息维护</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">渠道大类</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="chlTypeL" id="chlTypeL" class="form-control" lay-filter="funChange" lay-search>
                            <option value="">请选择渠道类型</option>
                        <#list laChlLargeType as LDCode>
                            <option value="${(LDCode.code)!}">${(LDCode.code)!}-${(LDCode.codeName)!}</option>
                        </#list>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">子渠道</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="chlTypeC" id="chlTypeC" class="form-control"  lay-search>
                            <option value="">请选择子渠道类型</option>
                        <#--<#list laChlChildType as LDCode1>-->
                            <#--<option value="${(LDCode1.code1)!}">${(LDCode1.code1)!}-${(LDCode1.codeName)!}</option>-->
                        <#--</#list>-->
                        </select>
                    </div>
                </div>
            </div>

            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
                <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;margin-left: 5px;">
                <button id="add" type="button" class="layui-btn" >
                    <i class="fa fa-plus" aria-hidden="true"></i> 增加
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
                <button id="delete" class="layui-btn">
                    <i class="fa fa-times" aria-hidden="true"></i> 删除
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" >
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>配置信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>渠道代码</th>
                    <#--<th>渠道名称</th>-->
                    <th>子渠道代码</th>
                    <th>子渠道名称</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.id == undefined ?"":item.id }}</td>
        <td>{{ item.code == undefined ?"":item.code }}</td>
        <td>{{ item.code1 == undefined ?"":item.code1 }}</td>
        <td>{{ item.codeName == undefined ?"":item.codeName }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/chl_edit/chlMaintain_list',

            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });

        $("#reset").click(function () {
            $("#chlTypeL").val("");
            $("#chlTypeC").empty().append("<option value=\"\">请选择子渠道</option>");
            $("#chlTypeC").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var chlTypeL = $("#chlTypeL").val();
            var chlTypeC = $("#chlTypeC").val();

            paging.get({
                "chlTypeC": chlTypeC,
                "chlTypeL": chlTypeL,
                "v": new Date().getTime()
            });
        });

        $('#add').on('click', function () {

            $.get('/chl_edit/chlMaintain_add.html', function(form) {
                layer.open({
                    type: 1,
                    title: '渠道增加',
                    content: form,
                    btn: ['提交', '返回'],
                    shade: false,
                    offset: 'auto',
                    area : ['450px', '480px'],
                    zIndex: 19991231,
                    id: 'lay_chlMaintain_add',
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form-item').find('button[lay-filter=edit]').click();
                    },
                    success: function(layero, index) {
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var chlTypeL = $("#chlTypeL1").val();
                            // var chlTypeLName = $("#chlTypeLName1").val();
                            var chlTypeC = $("#chlTypeC1").val();
                            var chlTypeCName = $("#chlTypeCName1").val();
                            if (chlTypeL == '') {
                                alert("请输入渠道大类!");
                                return false;
                            }
                            // if (chlTypeLName == '') {
                            //     alert("请输入渠道类型名称!");
                            //     return false;
                            // }
                            if (chlTypeC == '') {
                                alert("请输入子渠道代码!");
                                return false;
                            }
                            if (chlTypeCName == '') {
                                alert("请输入子渠道名称!");
                                return false;
                            }

                            $.ajax({
                                type: "POST",
                                url: "/chl_edit/addChlMaintain",
                                data: "params=" + JSON.stringify({
                                    chlTypeL: chlTypeL
                                    // , chlTypeLName: chlTypeLName
                                    , chlTypeC: chlTypeC
                                    , chlTypeCName: chlTypeCName
                                }),
                                dataType: 'json',
                                success: function(data){
                                    if(data.msg == '0000') {
                                        alert('添加成功');
                                        // location.reload(); //刷新
                                        layer.closeAll();
                                        $('#search').click();//刷新页面
                                    }else {
                                        alert('添加失败, ' + data.msg);
                                        console.log(data.msg);
                                        // location.reload(); //刷新
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                });
            });
        });

        $('#delete').on('click', function () {
            var chlTypeLs = '';
            var chlTypeCs = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    chlTypeLs += n + ',';
                    var n = $that.children('td').eq(3).text();
                    chlTypeCs += n + ',';
                    record++;
                }
            });
            if (record == '') {
                alert("请先选择一条信息!");
                return false;
            }
            $.ajax({
                type: "POST",
                url: '/chl_edit/deleteChlMaintain',
                data: "params=" + JSON.stringify({
                    chlTypeLs: chlTypeLs
                    , chlTypeCs: chlTypeCs
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('删除成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('删除失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });

        form.on('select(funChange)', function (data) {
            form.render('select');
            var chlTypeL = $("#chlTypeL").val();

            if (chlTypeL == "") {
                var select = document.getElementById("chlTypeC");
                var newOption = document.createElement("option");
                newOption.text = "请先选择渠道大类!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/chl_edit/linkChlChildType',
                data:{chlTypeL: chlTypeL},
                success: function(data){
                    var dataList = data.linkChlChildType;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#chlTypeC").empty().append("<option value=\"\">请选择子渠道</option>");
                        $("#chlTypeC").val("");
                        form.render();
                        return;
                    }
                    if(dataList!=null){
                        $("#chlTypeC").empty();
                        var select = document.getElementById("chlTypeC");
                        //联动框非强制选;默认空串
                        var newOption = document.createElement("option");
                        newOption.text = "请选择子渠道";
                        newOption.value = "" ;
                        select.options.add(newOption);
                        for(var i=0;i<dataList.length;i++){ //遍历
                            var categoryObj = dataList[i];
                            // alert(categoryObj);
                            var code = categoryObj.code1;
                            var codeName = categoryObj.codeName;
                            // alert(proName);
                            //进行添加到标签里
                            newOption = document.createElement("option");
                            newOption.text = code + '-' + codeName;
                            newOption.value = code;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                },error:function(data){
                    alert('系统错误');
                }
            });

        });

    });
</script>
</body>

</html>