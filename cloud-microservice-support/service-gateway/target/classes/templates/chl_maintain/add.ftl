<body>
<div class="layui-form" style="">
    <form class="layui-form-item">
        <div class="layui-inline" style="margin-top: 5px;">
            <label class="layui-form-label">出单网点编码<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" id="agentcom"  name="agentcom" lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入出单网点编码">
            </div>
        </div>

        <div class="layui-inline" style="margin-top: 5px;">
            <label class="layui-form-label">出单网点名称<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" id="agentName" name="agentName" lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入出单网点名称">
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">管理机构</label>
            <div class="layui-input-inline">
                <select name="wangbit" id="wangbit" lay-verify="required" lay-search>
                    <option value=""></option>
                        <#list lacoms as lacomss>
                            <option value="${(lacomss.name)!}">${(lacomss.name)!}</option>
                        </#list>
                </select>
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 15px;">
            <label class="layui-form-label">渠道大类</label>
            <div class="layui-input-block" style="display:flex">
                <select name="chlTypeLer" id="chlTypeLer" class="form-control" lay-filter="funChange" lay-search>
                    <option value="">请选择渠道类型</option>
                        <#list laChlLargeType as LDCode>
                            <option value="${(LDCode.code)!}">${(LDCode.code)!}-${(LDCode.codeName)!}</option>
                        </#list>
                </select>
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-inline">
                <input name="email" style="display:none"/>
                <input type="text" name="email" lay-verify="required" id="email5" autocomplete="off" class="layui-input" placeholder="请输入邮箱"
            >
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">手机号</label>
            <div class="layui-input-inline">
                <input type="text" name="telephone" style="display:none"/>
                <input type="text" name="telephone" lay-verify="required" id="telephone5" autocomplete="off" class="layui-input" placeholder="请输入手机"
                       >
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">联系电话</label>
            <div class="layui-input-inline">
                <input type="text" name="linkedPhone0" style="display:none"/>
                <input type="text" name="linkedPhone0" lay-verify="required" id="linkedPhone5" autocomplete="off" class="layui-input" placeholder="请输入联系电话">
            </div>
        </div>
        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>
</div>

<script type="text/javascript">
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        form.on('select(onchangeGrpPos)', function (data) {
            form.render('select');
            var field5 = $("#field5").val();
            // alert(field5);

            if (field5 == "") {
                var select = document.getElementById("institutions5");
                var newOption = document.createElement("option");
                newOption.text = "请先选择所属系统!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/mgr_user/linkedPositions',
                data:{field: field5},
                success: function(data){
                    var dataList = data.linkedPositions;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#position5").empty().append("<option value=\"\">请先选择岗位</option>");
                        form.render('select');
                        return;
                    }
                    if(dataList!=null){
                        $("#position5").empty();
                        var select = document.getElementById("position5");
                        //联动框非强制选;默认空串
                        var newOption = document.createElement("option");
                        newOption.text = "请选择岗位";
                        newOption.value = "" ;
                        select.options.add(newOption);
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            var code = categoryObj.positionId;
                            var codeName = categoryObj.position;
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            // newOption.text = code + '-' + codeName;
                            newOption.value = code + '-' + codeName;
                            newOption.text = codeName;
                            // newOption.value = codeName;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                },error:function(data){
                    alert('系统错误');
                }
            });

            if (field5 == 2) {
                $("#institutions5").empty().append("<option value=\"\">请选择企业</option>");
                form.render('select');
                return;//只有企业HR才需要录入企业信息 + 外勤laagent
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/mgr_user/linkedInstitutions',
                data:{field: field5},
                success: function(data){
                    var dataList = data.linkedInstitutions;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#institutions5").empty().append("<option value=\"\">请先选择所属系统</option>");
                        form.render();
                        return;
                    }
                    if(dataList!=null){
                        $("#institutions5").empty();
                        var select = document.getElementById("institutions5");
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            var code = categoryObj.customerno;
                            var codeName = categoryObj.grpname;
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            newOption.text = code + '-' + codeName;
                            newOption.value = code;
                            // newOption.value = codeName;
                            // newOption.value = code + '-' + codeName;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                },error:function(data){
                    alert('系统错误');
                }
            });

        });

    });

</script>
</body>