<body>
<div class="layui-form" style="">
    <form class="layui-form-item">
        <div class="layui-inline" style="margin-top: 5px;">
            <label class="layui-form-label">渠道大类<font color=red>*</font></label>
            <div class="layui-input-block" style="display:flex">
                <select name="chlTypeL" id="chlTypeL1" class="form-control" lay-filter="funChange" lay-search>
                    <option value="">请选择渠道类型</option>
                        <#list laChlLargeType as LDCode>
                            <option value="${(LDCode.code)!}">${(LDCode.code)!}-${(LDCode.codeName)!}</option>
                        </#list>
                </select>
            </div>
        </div>
        <#--<div class="layui-inline" style="margin-top: 5px;">-->
            <#--<label class="layui-form-label">渠道类型<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="chlTypeL1" autocomplete="off" class="layui-input" placeholder="请输入渠道类型">-->
            <#--</div>-->
        <#--</div>-->
        <#--<div class="layui-inline" style="margin-top: 2px;">-->
            <#--<label class="layui-form-label">渠道类型名称<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="chlTypeLName1" autocomplete="off" class="layui-input" placeholder="请输入渠道类型名称">-->
            <#--</div>-->
        <#--</div>-->
        <div class="layui-inline" style="margin-top: 1px;">
            <label class="layui-form-label">子渠道代码<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" id="chlTypeC1" autocomplete="off" class="layui-input" placeholder="请输入子渠道代码">
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 1px;">
            <label class="layui-form-label">子渠道名称<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" id="chlTypeCName1" autocomplete="off" class="layui-input" placeholder="请输入子渠道名称">
            </div>
        </div>

        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>
</div>
</body>