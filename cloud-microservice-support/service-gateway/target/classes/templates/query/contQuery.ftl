﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/plugins/YFB/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="../plugins/layui/css/query.css"/>
</head>
<body>
<div class="">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="">
            <#--<label class="query-form-label">单证流水号</label>
            <div class="layui-input-inline">
                <input type="text" name="documentNo" id="documentNo" autocomplete="off" class="layui-input">
            </div>-->

                <label class="query-form-label">出单网点</label>
                <div class="layui-input-inline layui-unselect">
                    <select name="channelCode" id="channelCode" class="form-control" lay-search>
                        <option value="">请选择网点</option>
                        <#list agentComInfo as aInfo>
                            <option value="${(aInfo.agentCom)!}">${(aInfo.agentCom)!}—${(aInfo.agentName)!}</option>
                        </#list>
                    </select>
                </div>
                <label class="query-form-label">保单号</label>
                <div class="layui-input-inline">
                    <input type="text" name="contNo" id="contNo" autocomplete="off" class="layui-input">
                </div>


                <label class="query-form-label">保单状态</label>
                <div class="layui-input-inline">
                    <select id="contState" lay-filter="onchangePosition">
                        <option value="">请选择保单状态</option>
                        <option value="0">0-未签发</option>
                        <option value="1">1-已签发</option>
                    </select>
                </div>
            </div>

            <div class="layui-form-item" style="">
                <label class="query-form-label">管理机构</label>
                <div class="layui-input-inline layui-unselect">
                    <select name="managecom" id="managecom" class="form-control" lay-search>
                        <option value="">请选择管理机构</option>
                        <#list manageInfo as mInfo>
                            <option value="${(mInfo.manageCom)!}">${(mInfo.manageCom)!}—${(mInfo.manageName)!}</option>
                        </#list>
                    </select>
                </div>

            <#--<label class="query-form-label">套餐</label>
            <div class="layui-input-inline">
                <input type="text" name="plancode" id="plancode" autocomplete="off" class="layui-input">
            </div>-->

                <label class="query-form-label">操作员代码</label>
                <div class="layui-input-inline">
                    <input type="text" name="operatorCode" id="operatorCode" autocomplete="off" class="layui-input">
                </div>

                <label class="query-form-label">投保人姓名</label>
                <div class="layui-input-inline layui-unselect">
                    <input type="text" name="appntName" id="appntName" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item" style="margin-top: 2px">


                <label class="query-form-label">投保起始日</label>
                <div class="layui-input-inline" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期"  name="insuranceStartDate" id="insuranceStartDate"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                           value="${item.insuranceStartDate?if_exists }">
                </div>

                <label class="query-form-label">投保终止日</label>
                <div class="layui-input-inline" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期" name="insuranceEndDate" id="insuranceEndDate"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                           value="${item.insuranceEndDate?if_exists }">
                </div>

                <label class="query-form-label">被投保人姓名</label>
                <div class="layui-input-inline layui-unselect">
                    <input type="text" name="insuredName" id="insuredName" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item" style="margin-top: 2px">


                <label class="query-form-label">被保险人证件号码</label>
                <div class="layui-input-inline">
                    <input type="text" name="insuranceIDNo" id="insuranceIDNo" autocomplete="off" class="layui-input">
                </div>

                <label class="query-form-label">结算状态</label>
                <div class="layui-input-inline">
                    <select id="settlementStatus" lay-filter="onchangePosition">
                        <option value="">请选择结算状态</option>
                        <option value="0">0-未结算</option>
                        <option value="1">1-已结算</option>
                    </select>
                </div>

                <label class="query-form-label">结算编号</label>
                <div class="layui-input-inline">
                    <input type="text" name="settlementNumber" id="settlementNumber" autocomplete="off" class="layui-input">
                </div>

            <#--<label class="query-form-label">代理机构</label>-->
            <#--<div class="layui-input-inline">-->
            <#--<input type="text" name="agentCom" id="agentCom" autocomplete="off" class="layui-input">-->
            <#--</div>-->
            </div>

            <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 2px">
                <button id="search" lay-filter="search" class="layui-btn" lay-submit >
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 2px">
                <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
        </div>
    </fieldset>
</div>
<table class="layui-hide" id="test" lay-filter="test"></table>



<#--<script type="text/html" id="toolbarDemo">-->
    <#--<div class="layui-btn-container">-->
        <#--<button class="layui-btn layui-btn-sm" lay-event="getCheckData">获取选中行数据</button>-->
    <#--</div>-->
<#--</script>-->


<!--模板-->
<script src="/plugins/YFB/layui/layui.js"></script>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>

<script>
    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#test'
            ,url:'/query/contQueryList/' //数据接口
            ,cellMinWidth: 100 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,cols: [[
                {type:'checkbox'}
                ,{type:'numbers',title:'序号'}
                ,{field:'agentCom',  title: '出单网点'}
                ,{field:'contno', title: '保单号'}
                ,{field:'signState',  title: '保单状态'}
                ,{field:'managecom', title: '管理机构', sort: true}
                ,{field:'operator',  title: '操作人编码'}
                ,{field:'appntname',  title: '投保人姓名'}
                ,{field:'polapplydate', title: '投保日期'}
                ,{field:'insuredname', title: '被投保人姓名'}
                ,{field:'insuredidno', title: '被保险人证件号码'}
                ,{field:'settlementStatus', title: '结算状态'}
                ,{field:'settlementNumber', title: '结算编号'}
            ]]
            ,page: true
        });

        $("#reset").click(function () {
            $("#channelCode").val("");
            $("#contNo").val("");
            $("#contState").val("");
            $("#managecom").val("");
            $("#operatorCode").val("");
            $("#appntName").val("");
            $("#insuranceStartDate").val("");
            $("#insuranceEndDate").val("");
            $("#insuredName").val("");
            $("#insuranceIDNo").val("");
            $("#settlementStatus").val("");
            $("#settlementNumber").val("");
            form.render();
        });

        // 执行搜索，表格重载
        $('#search').on('click', function () {
            var channelCode = $("#channelCode").val();
            var contNo = $("#contNo").val();
            var contState = $("#contState").val();
            var managecom = $("#managecom").val();
            var operatorCode = $("#operatorCode").val();
            var appntName = $("#appntName").val();
            var insuranceStartDate = $("#insuranceStartDate").val();
            var insuranceEndDate = $("#insuranceEndDate").val();
            var insuredName = $("#insuredName").val();
            var insuranceIDNo = $("#insuranceIDNo").val();
            var settlementStatus = $("#settlementStatus").val();
            var settlementNumber = $("#settlementNumber").val();

            table.reload('test', {
                method: 'post'
                , where: {
                    'agentCom': channelCode,
                    'contno': contNo,
                    'signState': contState,
                    'managecom': managecom,
                    'operator': operatorCode,
                    'appntname': appntName,
                    'polapplyStartDate': insuranceStartDate,
                    'polapplyEndDate': insuranceEndDate,
                    'insuredname': insuredName,
                    'insuredidno': insuranceIDNo,
                    'settlementStatus': settlementStatus,
                    'settlementNumber': settlementNumber,
                }
                , page: {
                    curr: 1
                }
            });
        });

    });
</script>




<script type="text/html" id="rank">
    {{d.LAY_TABLE_INDEX+1}}
</script>

</body>
</html>