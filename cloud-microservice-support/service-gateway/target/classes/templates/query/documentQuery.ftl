﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>单证查询</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../plugins/layui/css/query.css">
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 50px">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
                <div class="layui-form-item" style="margin-top: 2px">
                  <label class="query-form-label">单证编码</label>
                  <div class="layui-input-inline">
                      <input type="text" name="certifyCode" id="certifyCode" autocomplete="off" class="layui-input">
                  </div>

                  <label class="query-form-label">单证状态</label>
                  <div class="layui-input-inline">
                      <select id="stateFlag" name="stateFlag" lay-filter="onchangePosition">
                          <option value="">请选择单证状态</option>
                          <option value="1">1-发放</option>
                          <option value="2">2-回退</option>
                          <option value="3">3-回销</option>
                          <option value="4">4-使用</option>
                      </select>
                  </div>
                  <label class="query-form-label">发放者</label>
                  <div class="layui-input-inline">
                      <input type="text" name="sendOut" id="sendOut" autocomplete="off" class="layui-input">
                  </div>



                </div>
                <div class="layui-form-item" style="margin-top: 2px">
                  <label class="query-form-label">接收者</label>
                  <div class="layui-input-inline">
                      <input type="text" name="receive" id="receive" autocomplete="off" class="layui-input">
                  </div>

                  <label class="query-form-label">操作员</label>
                  <div class="layui-input-inline">
                      <input type="text" name="operator" id="operator" autocomplete="off" class="layui-input">
                  </div>

                  <label class="query-form-label">经办人</label>
                  <div class="layui-input-inline">
                      <input type="text" name="handler" id="handler" autocomplete="off" class="layui-input">
                  </div>
                </div>
                <div class="layui-form-item" style="margin-top: 2px">
                  <label class="query-form-label">经办起期</label>
                  <div class="layui-input-inline" style="display:flex">
                  <input class="layui-input" placeholder="请选择经办起期"  name="handlerStartDate" id="handlerStartDate"
                         onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                         value="${item.insuranceStartDate?if_exists }">
                  </div>

                  <label class="query-form-label">经办止期</label>
                  <div class="layui-input-inline" style="display:flex">
                      <input class="layui-input" placeholder="请选择经办止期"  name="handlerEndDate" id="handlerEndDate"
                             onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                             value="${item.insuranceStartDate?if_exists }">
                  </div>

                  <label class="query-form-label">入机起期</label>
                  <div class="layui-input-inline" style="display:flex">
                      <input class="layui-input" placeholder="请选择入机日期"  name="makeStartDate" id="makeStartDate"
                             onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                             value="${item.insuranceStartDate?if_exists }">
                  </div>
                </div>
                <div class="layui-form-item" style="margin-top: 2px">
                    <label class="query-form-label">入机止期</label>
                    <div class="layui-input-inline" style="display:flex">
                        <input class="layui-input" placeholder="请选择入机止期"  name="makeEndDate" id="makeEndDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                               value="${item.insuranceStartDate?if_exists }">
                    </div>

                    <label class="query-form-label">单证起始号</label>
                    <div class="layui-input-inline" style="display:flex">
                        <input class="layui-input" placeholder="请选择单证起始号"  name="startNo" id="startNo"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                               value="${item.insuranceStartDate?if_exists }">
                    </div>

                    <label class="query-form-label">单证终止号</label>
                    <div class="layui-input-inline" style="display:flex">
                        <input class="layui-input" placeholder="请选择单证终止号"  name="endNo" id="endNo"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                               value="${item.insuranceStartDate?if_exists }">
                    </div>
                </div>
              <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 2px">
                  <button id="search" lay-filter="search" class="layui-btn" lay-submit >
                      <i class="fa fa-search" aria-hidden="true"></i> 查询
                  </button>
              </div>
              <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 2px">
                  <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                      <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                  </button>
              </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>单证信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>单证编码</th>
                    <th>单证状态</th>
                    <th>发放者</th>

                    <th>接受者</th>
                    <th>操作员</th>
                    <th>经办人</th>

                    <th>经办日期</th>
                    <th>入机日期</th>
                    <th>单证起始号</th>

                    <th>单证终止号</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
            <div class="admin-table-page">
                <div id="paged" class="page">
                </div>
            </div>
        </div>


    </fieldset>


</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.certifyCode == undefined ?"":item.certifyCode }}</td>
        <td>
            {{# if(item.stateFlag === '3'){ }} <span>回销</span> {{# } }}
            {{# if(item.stateFlag === '2'){ }} <span>回退</span> {{# } }}
            {{# if(item.stateFlag === '1'){ }} <span>发放</span> {{# } }}
            {{# if(item.stateFlag === '4'){ }} <span>使用</span> {{# } }}
        </td>
        <td>{{ item.sendOut == undefined ?"":item.sendOut }}</td>
        <td>{{ item.receive == undefined ?"":item.receive }}</td>
        <td>{{ item.operator == undefined ?"":item.operator }}</td>
        <td>{{ item.handler == undefined ?"":item.handler }}</td>
        <td>{{ item.handlerDate == undefined ?"":item.handlerDate }}</td>
        <td>{{ item.makeDate == undefined ?"":item.makeDate }}</td>
        <td>{{ item.startNo == undefined ?"":item.startNo }}</td>
        <td>{{ item.endNo == undefined ?"":item.endNo }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/query/documentQueryList', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                    // location.reload();
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });


            },
        });

        $("#reset").click(function () {
            $("#certifyCode").val("");
            $("#stateFlag").val("");
            $("#sendOut").val("");
            $("#receive").val("");
            $("#operator").val("");
            $("#handler").val("");
            $("#handlerStartDate").val("");
            $("#handlerEndDate").val("");
            $("#makeStartDate").val("");
            $("#makeEndDate").val("");
            $("#startNo").val("");
            $("#endNo").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var certifyCode = $("#certifyCode").val();
            var stateFlag = $("#stateFlag").val();
            var sendOut = $("#sendOut").val();
            var receive = $("#receive").val();
            var operator = $("#operator").val();
            var handler = $("#handler").val();
            var handlerStartDate = $("#handlerStartDate").val();
            var handlerEndDate = $("#handlerEndDate").val();
            var makeStartDate = $("#makeStartDate").val();
            var makeEndDate = $("#makeEndDate").val();
            var startNo = $("#startNo").val();
            var endNo = $("#endNo").val();

            paging.get({
                "certifyCode": certifyCode,
                "stateFlag": stateFlag,
                "sendOut": sendOut,
                "receive": receive,
                "operator": operator,
                "handler": handler,
                "handlerStartDate": handlerStartDate,
                "handlerEndDate": handlerEndDate,
                "makeStartDate": makeStartDate,
                "makeEndDate": makeEndDate,
                "startNo": startNo,
                "endNo": endNo,
                "v": new Date().getTime()
            });
        });
    });
</script>
</body>

</html>