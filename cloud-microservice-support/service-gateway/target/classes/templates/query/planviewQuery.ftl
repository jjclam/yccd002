<head>
    <meta charset="UTF-8">
    <title>方案详情</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
    <link rel="stylesheet" href="../plugins/layui/css/query.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 250px">

    <fieldset class="layui-elem-field">
        <legend>套餐名称：${proCode}</legend>
        <div class="layui-form" style="display: none">
            <div class="layui-input-inline">
                <input id="proCode" value="${proCode}">
            </div>
        </div>
        <div class="layui-form" style="display: none">
            <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 2px">
                <button id="search" lay-filter="onchangeParams" class="layui-btn" lay-submit >
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
        </div>

        <div class="layui-field-box layui-form" >
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>套餐编码</th>
                    <th>套餐名称</th>
                    <th>险种编码</th>
                    <th>险种名称</th>
                    <th>责任编码</th>
                    <th>责任名称</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.contplancode == undefined ?"":item.contplancode }}</td>
        <td>{{ item.contplanname == undefined ?"":item.contplanname }}</td>
        <td>{{ item.riskcode == undefined ?"":item.riskcode }}</td>
        <td>{{ item.riskname == undefined ?"":item.riskname }}</td>
        <td>{{ item.dutycode == undefined ?"":item.dutycode }}</td>
        <td>{{ item.dutyname == undefined ?"":item.dutyname }}</td>
        <#--<td>{{ item.insuyear == undefined ?"":item.insuyear }}</td>-->

    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();
        var proCode = $("#proCode").val();
        paging.init({

            openWait: true,
            url: '/query/planviewQueryList?proCode='+proCode, //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });


            },
        });
/*
        $('#search').on('click', function () {
            var proCode = $("#proCode").val();

            // alert(contschemecode);
            paging.get({
                "proCode": proCode,
                "v": new Date().getTime()
            });
        });

        (()=>{
            // 兼容IE
            if(document.all) {
                document.getElementById("search").click();
            }
            // 兼容其它浏览器
            else {
                var e = document.createEvent("MouseEvents");
                e.initEvent("click", true, true);
                document.getElementById("search").dispatchEvent(e);
                // onchangeParams();
            }
        })();*/

    });

</script>
</body>
