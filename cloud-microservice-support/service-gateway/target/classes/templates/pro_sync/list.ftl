﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>产品信息同步</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
        <legend>核心产品信息查询</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="layui-form-label">产品编码</label>
                <div class="layui-input-inline">
                <#--<input type="text" name="proCode" id="proCode" autocomplete="off" class="layui-input">-->
                    <select name="proCode" id="proCode" class="form-control" lay-search>
                        <option value="">请选择产品</option>
                    <#list lmRiskSale as ProInfo>
                        <option value="${(ProInfo.proCode)!}">${(ProInfo.proCode)!} —${(ProInfo.proName)!}</option>
                    </#list>
                    </select>
                </div>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 10px;">
                <button id="search11" lay-filter="search" class="layui-btn" lay-submit>
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 10px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>核心产品信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th contenteditable="true">产品编码</th>
                    <th contenteditable="true">产品名称</th>
                    <th contenteditable="true">产品起售日期</th>
                    <th contenteditable="true">产品停售日期</th>
                    <th contenteditable="true">算法编码</th>
                    <th contenteditable="true">投保人年龄起期</th>
                    <th contenteditable="true">投保人年龄止期</th>
                    <th contenteditable="true">被保人年龄起期</th>
                    <th contenteditable="true">被保人年龄止期</th>
                    <th contenteditable="true">产品上下架状态</th>

                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
    </fieldset>

    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>
    <div style="display: flex">
        <div class="layui-form" >
            <button id="upstatus" lay-filter="save" class="layui-btn" lay-submit style="margin-left: 30px;style="margin-top: 10px;"">
                <i class="fa fa-level-up" aria-hidden="true"></i> 上架
            </button>
        </div>
        <div class="layui-form" >
            <button id="downstatus" lay-filter="save" class="layui-btn" lay-submit style="margin-left: 5px;style="margin-top: 10px;"">
                <i class="fa fa-level-down" aria-hidden="true"></i> 下架
            </button>
        </div>
    </div>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td contenteditable="true">{{ item.proId == undefined ?"":item.proId }}</td>
        <td contenteditable="true">{{ item.proCode == undefined ?"":item.proCode }}</td>
        <td contenteditable="true">{{ item.proName == undefined ?"":item.proName }}</td>
        <td contenteditable="true">{{ item.startDate == undefined ?"":item.startDate }}</td>
        <td contenteditable="true">{{ item.overDate == undefined ?"":item.overDate }}</td>

        <td contenteditable="true">{{ item.remark == undefined ?"":item.remark }}</td>
        <td contenteditable="true">{{ item.remark2 == undefined ?"":item.remark2 }}</td>
        <td contenteditable="true">{{ item.planKind1 == undefined ?"":item.planKind1 }}</td>
        <td contenteditable="true">{{ item.planKind2 == undefined ?"":item.planKind2 }}</td>
        <td contenteditable="true">{{ item.planKind3 == undefined ?"":item.planKind3 }}</td>
        <td>
            {{# if(item.status === '0'){ }} <span style="color: green">上架</span> {{# } }}
            {{# if(item.status === '1'){ }} <span style="color: red">下架</span> {{# } }}
        </td>

    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/pro_sync/list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            layer.msg('你选择的名称有：' + names);
        });

        $("#reset").click(function () {
            $("#proCode").val("");
            form.render();
        });

        $('#search11').on('click', function () {
            var proCode = $("#proCode").val();
            // alert(proCode);
            var proType = $("#proType").val();

            paging.get({
                "proCode": proCode,
                "proType": proType,
                "v": new Date().getTime()
            });

        });

        var addBoxIndex = -1;
        $('#add').on('click', function () {
            if (addBoxIndex !== -1)
                return;
            editForm();
        });

        $('#import').on('click', function () {
            var that = this;
            var index = layer.tips('只想提示地精准些', that, {tips: [1, 'white']});
            $('#layui-layer' + index).children('div.layui-layer-content').css('color', '#000000');
        });

        $('#upstatus').on('click', function () {
            var proCodes = '';

            var remark = '';
            var remark2 = '';
            var planKind1 = '';
            var planKind2 = '';
            var planKind3 = '';
           // var planKind3 = $("#planKind3").val();
            var j = '';
            var flag = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    proCodes += n ;

                    var n1 = $that.children('td').eq(6).text();
                    remark += n1 ;
                    var n2 = $that.children('td').eq(7).text();
                    remark2 += n2 ;
                    var n3 = $that.children('td').eq(8).text();
                    planKind1 += n3 ;
                    var n4 = $that.children('td').eq(9).text();
                    planKind2 += n4 ;
                    var n5 = $that.children('td').eq(10).text();
                    planKind3 += n5 ;
                    // alert(n);
                    // alert(n1);
                    j = $that.children('td').eq(11).text().trim();
                    // alert(n);
                    if (j == '上架') { flag = 1; }
                }
            });

            if (proCodes == '') {
                alert('请先进行选择产品!');
                return false;
            };

            // alert(j);
            if (flag == 1) {
                alert('所选产品中存在已上架的产品,请重新选择!');
                return false;
            };
            // layer.msg('你选择的名称有：' + proCodes);
            $.ajax({
                type: "POST",
                url: '/pro_sync/upPro',
                data: {proCodes: proCodes,
                    remark:remark,
                    remark2:remark2,
                    planKind1:planKind1,
                    planKind2:planKind2,
                    planKind3:planKind3
                },
                dataType: 'json',
                cache: false,
                success: function (data) {
                    console.log(data);
                    if (data.msg == '0000') {
                        alert('上架成功');
                        // layerTips.close(index);
                        // location.reload(); //刷新
                        // $('.layui-laypage-btn').click();
                        $('#search11').click();//刷新页面
                    } else {
                        alert('上架失败');
                        // layerTips.close(index);
                        console.log(data.msg);
                        // location.reload(); //刷新
                    }
                }
            });
        });

        $('#downstatus').on('click', function () {
            var proCodes = '';
            var j = '';
            var flag = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    proCodes += n + ',';
                    j = $that.children('td').eq(6).text().trim();
                    // alert(n);
                    if (j == '下架') { flag = 1; }
                }
            });

            if (proCodes == '') {
                alert('请先进行选择产品!');
                return false;
            };

            // alert(j);
            if (flag == 1) {
                alert('所选产品中存在已下架的产品,请重新选择!');
                return false;
            };

            // layer.msg('你选择的名称有：' + proCodes);
            $.ajax({
                type: "POST",
                url: '/pro_sync/downPro',
                data: {proCodes: proCodes},
                dataType: 'json',
                cache: false,
                success: function (data) {
                    console.log(data);
                    if (data.msg == '0000') {
                        alert('下架成功');
                        // layerTips.close(index);
                        // location.reload(); //刷新
                        $('#search11').click();
                    } else {
                        alert('下架失败' + data.msg);
                        // layerTips.close(index);
                        // console.log(data.msg);
                        // location.reload(); //刷新
                    }
                }
            });
        });

    });
</script>
</body>

</html>