﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>产品信息同步</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">
<#--<blockquote class="layui-elem-quote">-->

<#--</blockquote>-->
    <fieldset class="layui-elem-field">
        <legend>核心产品同步</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="layui-form-label">核心险种编码</label>
                <div class="layui-input-inline layui-unselect">
                    <input type="text" name="riskCode" id="riskCode" autocomplete="off" class="layui-input">
                </div>
                <label class="layui-form-label">套餐编码</label>
                <div class="layui-input-inline">
                    <input type="text" name="combCode" id="combCode" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-left: 30px;margin-top: 10px;">
                <button id="syncPro" lay-filter="funProduct" class="layui-btn" lay-submit>
                    <i class="fa fa-arrows-h" aria-hidden="true"></i> 产品同步
                </button>
            </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>核心产品信息查询</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="layui-form-label">产品编码</label>
                <div class="layui-input-inline">
                <#--<input type="text" name="proCode" id="proCode" autocomplete="off" class="layui-input">-->
                    <select name="proCode" id="proCode" class="form-control" lay-search>
                        <option value="">请选择产品</option>
                    <#list lmRiskSale as ProInfo>
                        <option value="${(ProInfo.proCode)!}">${(ProInfo.proCode)!} —${(ProInfo.proName)!}</option>
                    </#list>
                    </select>
                </div>
                <#--<label class="layui-form-label">产品名称</label>-->
                <#--<div class="layui-input-inline layui-unselect">-->
                    <#--<input type="text" name="proCode" id="proCode" autocomplete="off" class="layui-input">-->
                <#--</div>-->
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 10px;">
                <button id="search11" lay-filter="search" class="layui-btn" lay-submit>
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 10px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>核心产品信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>产品编码</th>
                    <th>产品名称</th>
                    <th>产品起售日期</th>
                    <th>产品停售日期</th>
                    <th>产品上下架状态</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
    </fieldset>
    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.proId == undefined ?"":item.proId }}</td>
        <td>{{ item.proCode == undefined ?"":item.proCode }}</td>
        <td>{{ item.proName == undefined ?"":item.proName }}</td>
        <td>{{ item.startDate == undefined ?"":item.startDate }}</td>
        <td>{{ item.overDate == undefined ?"":item.overDate }}</td>
        <td>
            {{# if(item.status === '0'){ }} <span style="color: green">上架</span> {{# } }}
            {{# if(item.status === '1'){ }} <span style="color: red">下架</span> {{# } }}
        </td>

    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/pro_sync/list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            layer.msg('你选择的名称有：' + names);
        });

        $("#reset").click(function () {
            $("#proCode").val("");
            form.render();
        });

        $('#search11').on('click', function () {
            var proCode = $("#proCode").val();
            // alert(proCode);
            var proType = $("#proType").val();

            paging.get({
                "proCode": proCode,
                "proType": proType,
                "v": new Date().getTime()
            });

        });

        var addBoxIndex = -1;
        $('#add').on('click', function () {
            if (addBoxIndex !== -1)
                return;
            editForm();
        });

        $('#import').on('click', function () {
            var that = this;
            var index = layer.tips('只想提示地精准些', that, {tips: [1, 'white']});
            $('#layui-layer' + index).children('div.layui-layer-content').css('color', '#000000');
        });


        $('#syncPro').on('click', function (data) {
            var riskCode = $("#riskCode").val();
            var combCode = $("#combCode").val();
            var index = 0;
            if (riskCode != "" & combCode != "") {
                alert("不能同时选产品和套餐!");
                return;
            }
            // if (riskCode == "" & combCode == "") {
            //     alert("同步所有产品需要大量时间,是否继续? ");
            //     return;
            // }
            if (riskCode == "" & combCode == "") {
                if(confirm('同步所有产品需要较长时间, 是否继续? ')){
                    layer.close(index);
                }else {
                    layer.close(index);
                    return;
                };
            }
            // alert(riskCode + "," + combCode);
            $.ajax({
                type: "POST",
                url: '/pro_sync/syncPro',
                data: {riskCode: riskCode, combCode: combCode},
                dataType: 'json',
                cache: false,
                beforeSend: function() {
                    index = layer.load(0, {
                        shade: [0.5, 'gray'], //0.5透明度的灰色背景
                        content: '数据同步中, 请耐心等待...',
                        success: function (layero) {
                            layero.find('.layui-layer-content').css({
                                'padding-top': '39px',
                                'width': '160px'
                            });
                        }
                    }, {time: 20*1000});
                },
                success: function (data) {
                    // console.log(data);
                    if (data.msg == '0000') {
                        alert('同步成功');
                        // layerTips.close(index);
                        location.reload(); //刷新
                        // layer.close(index);
                        // $('#search11').click();//刷新页面
                    } else {
                        // alert('同步失败: ' + data.msg);
                        // alert('同步失败');
                        alert('核心无满足查询条件的数据');
                        // layerTips.close(index);
                        // layer.close(index);
                        console.log(data.msg);
                        // location.reload(); //刷新
                    }
                },
                complete: function () {
                    layer.close(index);
                    // $('#search11').click();
                }
            });
        });


    });
</script>
</body>

</html>