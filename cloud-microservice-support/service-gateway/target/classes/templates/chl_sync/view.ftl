<div style="margin: 15px;">
	<form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">产品名称</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.proName?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">产品编码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.proCode?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">险种类型</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.riskTypeName?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">最大投保年龄</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.maxAppntAge?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">最小投保年龄</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" value="${item.minAppntAge?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">创建日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" lay-verify="required" value="${item.createDate?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">修改日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" disabled="disabled" lay-verify="required" value="${item.modifyDate?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">产品起售日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input disabled="disabled" class="layui-input" placeholder="请选择日期" name="riskStartSellDate"
                           value="${item.riskStartSellDate?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">产品止售日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input disabled="disabled" class="layui-input" placeholder="请选择日期" name="riskEndSellDate"
                           value="${item.riskEndSellDate?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">H5页面链接</label>
                <div class="layui-input-block" style="display:flex">
                    <input disabled="disabled" class="layui-input" name="h5href" value="${item.h5href?if_exists }">
                </div>
            </div>
        </div>

		<button lay-filter="edit" lay-submit style="display: none;"></button>
	</form>
</div>