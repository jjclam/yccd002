<div style="margin: 15px;">
    <form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">渠道</label>
                <div class="layui-input-block" style="display:flex" >
                    <input type="text" class="layui-input" name="channelCode" disabled="disabled" value="${item.channelCode?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">险种类型</label>
                <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" name="riskType" disabled="disabled" value="${item.riskType?if_exists }">
                <#--<select name="riskType" id="riskType" class="form-control" onchange="getProName(this, channelCode)"-->
                            <#--value="${item.riskType?if_exists }">-->
                        <#--<option >请选择险种类型</option>-->
                        <#--<option value="A">意外伤害保险</option>-->
                        <#--<option value="H">健康险</option>-->
                        <#--<option value="L">人寿保险</option>-->
                        <#--<option value="R">年金保险</option>-->
                        <#--<option value="S">重疾保险</option>-->
                        <#--<option value="U">万能保险</option>-->

                    <#--&lt;#&ndash;<#list laRiskTypeInfo as SaleProInfo>&ndash;&gt;-->
                        <#--&lt;#&ndash;<option value="${(SaleProInfo.riskTypeCode)!}">${(SaleProInfo.riskTypeCode)!}—${(SaleProInfo.riskTypeName)!}</option>&ndash;&gt;-->
                    <#--&lt;#&ndash;</#list>&ndash;&gt;-->
                    <#--</select>-->
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">产品名称</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" name="riskName" disabled="disabled" value="${item.riskName?if_exists }">
                </div>
            </div>
            <div class="layui-hide">
                <label class="layui-form-label">产品编码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" class="layui-input" name="riskCode" disabled="disabled" value="${item.riskCode?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">产品起售日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期"  name="riskStartSellDate" id="riskStartSellDate3"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                           value="${item.riskStartSellDate?string("yyyy-MM-dd HH:mm:ss")?if_exists }">
                </div>
            </div>
            <#--<div class="layui-inline">-->
                <#--<label class="layui-form-label">产品起售时间</label>-->
                <#--<div class="layui-input-block" style="display:flex">-->
                    <#--<input type="text" class="layui-input" disabled="disabled" value="${item.riskStartSellDate?string("yyyy-MM-dd")?if_exists }">-->
                <#--</div>-->
            <#--</div>-->
        <#--</div>-->
        <#--<div class="layui-form-item">-->
            <#--<div class="layui-inline">-->
                <#--<label class="layui-form-label">产品止售时间</label>-->
                <#--<div class="layui-input-block" style="display:flex">-->
                    <#--<input type="text" class="layui-input" disabled="disabled" value="${item.riskEndSellEnd?string("yyyy-MM-dd")?if_exists }">-->
                <#--</div>-->
            <#--</div>-->
            <div class="layui-inline">
                <label class="layui-form-label">产品止售日期</label>
                <div class="layui-input-block" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期"  name="riskEndSellDate" id="riskEndSellDate3"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                           value="${item.riskEndSellDate?string("yyyy-MM-dd HH:mm:ss")?if_exists }">
                </div>
            </div>
        </div>
		<div class="layui-form-item">
            <label class="layui-form-label">H5页面链接</label>
            <div class="layui-input-inline" style="display:flex">
                <input type="text" class="layui-input" name="h5href" autocomplete="off"
                       value="${item.h5href?if_exists }" style="">
            </div>
        </div>

        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>
</div>

<script>
    function getProName(riskType, channelCode) {
        alert(riskType);
        alert(channelCode);
        return;
    }

</script>















