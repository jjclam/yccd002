<body>
<div style="margin: 15px;">
    <form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">渠道<font color=red>*</font></label>
                <div class="layui-input-inline layui-unselect">
                    <select id="channelCode" name="channelCode"  class="form-control"  lay-search>
                        <#--业务更改;不能删-->
                        <#--<option value="${item.channelCode?if_exists }">${item.channelCode?if_exists}</option>-->
                        <option value="">请选择渠道</option>
                    <#list laChlInfo as ChlInfo>
                        <option value="${(ChlInfo.channelCode)!}-${(ChlInfo.channelName)!}">${(ChlInfo.channelCode)!}-${(ChlInfo.channelName)!}</option>
                    </#list>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">险种类型<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="riskType" id="riskType" class="form-control" lay-filter="funChange" lay-search>
                    value="${item.riskType?if_exists }">
                        <#--业务更改;不能删-->
                        <#--<option value="${item.riskType?if_exists }">${item.riskType?if_exists }</option>-->
                        <option value="">请选择险种类型</option>
                        <#--<option value="A" <c:if test="${item.riskType?if_exists == 'A'}">selected</c:if>意外伤害保险</option>-->
                        <#--<option value="H" <c:if test="${item.riskType?if_exists == 'H'}">selected</c:if>健康险</option>-->
                        <#--<option value="L" <c:if test="${item.riskType?if_exists == 'L'}">selected</c:if>人寿保险</option>-->
                        <#--<option value="A" <c:if test="${item.riskType?if_exists == 'R'}">selected</c:if>年金保险</option>-->
                        <#--<option value="S" <c:if test="${item.riskType?if_exists == 'S'}">selected</c:if>重疾保险</option>-->
                        <#--<option value="U" <c:if test="${item.riskType?if_exists == 'U'}">selected</c:if>万能保险</option>-->

                        <#--<option value="A">A-意外伤害保险</option>-->
                        <#--<option value="H">H-健康险</option>-->
                        <#--<option value="L">L-人寿保险</option>-->
                        <#--<option value="R">R-年金保险</option>-->
                        <#--<option value="S">S-重疾保险</option>-->
                        <#--<option value="U">U-万能保险</option>-->
                        <#list laRiskTypeInfo as SaleProInfo>
                        <option value="${(SaleProInfo.riskTypeCode)!}">${(SaleProInfo.riskTypeCode)!}-${(SaleProInfo.riskTypeName)!}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">产品名称<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <#--<option type="text" id="riskCode" selected="" value="${item.agencyName?if_exists }"></option>-->
                        <#--便于执行效率;此处产品名称用编码值交互-->
                    <select name="riskCode" class="form-control" id="riskCode" lay-search>
                    <#--业务更改;不能删-->
                    <#--<option value="${item.proCode?if_exists}">${item.proName?if_exists}</option>-->
                        <option value="">请选择产品名称</option>
                    <#--<#list laAddProInfo as SaleProInfo>-->
                        <#--<option value="${(SaleProInfo.proCode)!}">${(SaleProInfo.proCode)!}-${(SaleProInfo.proName)!}</option>-->
                    <#--</#list>-->
                    </select>
                </div>
            </div>

        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">产品起售日期<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期" name="riskStartSellDate" id="riskStartSellDate4"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                           value="${item.riskStartSellDate?if_exists }">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">产品止售日期<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期" name="riskEndSellDate" id="riskEndSellDate4"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})"
                           value="${item.riskEndSellDate?if_exists }">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">H5页面链接</label>
            <div class="layui-input-inline" style="display:flex">
                <input type="text" class="layui-input" name="h5href" autocomplete="off"
                       value="${item.h5href?if_exists }">
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-hide">
                <label class="layui-form-label">续保权限<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="rnewageFlag"  lay-verify="">
                        <option value="">请选择产品的续保权限</option>
                        <option value="01">关闭</option>
                        <option value="02">打开</option>
                    </select>
                </div>
            </div>
            <div class="layui-hide">
                <label class="layui-form-label">配置状态<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="isConfig"  lay-verify="">
                        <option value="">请选择该产品的配置状态</option>
                        <option value="0">无效</option>
                        <option value="1">有效</option>
                    </select>
                </div>
            </div>
            <div class="layui-hide">
                <label class="layui-form-label">上下架方式<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="upDownType"  lay-verify="">
                        <option value="">请选择该产品上下架的方式</option>
                        <option value="00">手动立即上下架</option>
                        <option value="01">自动定时上下架</option>
                    </select>
                </div>
            </div>
        </div>


        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>
</div>
<#--<script src="/templates/chl_sync/position.js"></script>-->
<#--<script type="text/javascript" src="../plugins/layui/layui.js"></script>-->
<#--<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>-->
<#--<script type="text/javascript" src="../js/jquery.js"></script>-->
<#--<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>-->
<#--<script type="text/javascript" src="../plugins/layui/layui.js"></script>-->
<#--<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>-->
<script type="text/javascript">
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        form.on('select(funChange)', function (data) {
            form.render('select');
            var riskType = $("#riskType").val();

            if (riskType == "") {
                var select = document.getElementById("riskCode");
                var newOption = document.createElement("option");
                newOption.text = "请先选择险种!";
                newOption.value = "";
                select.options.add(newOption);
            }
            // $("#riskCode").val("");
            // form.render();
            // alert(proName);
            //写ajax
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/chl_sync/linkProduct',
                data: {riskType: riskType},
                success: function (data) {
                    var dataList = data.linkPro;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#riskCode").empty().append("<option value=\"\">请选择产品名称</option>");
                        $("#riskCode").val("");
                        form.render();
                        return;
                    }
                    if (dataList != null) {
                        $("#riskCode").empty();
                        var select = document.getElementById("riskCode");
                        for (var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            // alert(categoryObj);
                            var proCode = categoryObj.proCode;
                            var proName = categoryObj.proName;
                            // alert(proName);
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            newOption.text = proCode + '-' + proName;
                            newOption.value = proCode + '-' + proName;//为添加上架产品新增拼接符
                            // document.getElementById("riskCode").options.add(newOption);
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                }, error: function (data) {
                    alert('系统错误');
                }
            });

        });

    });

</script>
</body>