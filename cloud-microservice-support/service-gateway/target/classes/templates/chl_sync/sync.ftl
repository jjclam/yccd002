﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>渠道产品管理</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>

</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">
<#--<blockquote class="layui-elem-quote">-->

<#--</blockquote>-->
    <fieldset class="layui-elem-field">
        <legend>渠道同步</legend>
        <div class="layui-form" style="float:left;">

                <div class="layui-form-item" style="margin:0;">
                    <label class="layui-form-label">渠道名称</label>
                    <div class="layui-input-inline layui-unselect">
                        <input type="text" name="channelName" id="channelName" autocomplete="off" class="layui-input">
                    <#--<select name="channelName" id="channelName" class="form-control">-->
                            <#--<option value="">请选择渠道</option>-->
                    <#--<#list laChlInfo as ChlInfo>-->
                        <#--<option value="${(ChlInfo.channelCode)!}">${(ChlInfo.channelCode)!}—${(ChlInfo.channelName)!}</option>-->
                    <#--</#list>-->
                        <#--</select>-->
                    </div>
                    <#--<label class="layui-form-label">创建起期</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input class="layui-input" placeholder="请选择日期" id="riskStartSellDate1"-->
                               <#--onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">-->
                    <#--</div>-->
                    <#--<label class="layui-form-label">创建止期</label>-->
                    <#--<div class="layui-input-inline">-->
                        <#--<input class="layui-input" placeholder="请选择日期" id="riskEndSellDate1"-->
                               <#--onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">-->
                    <#--</div>-->
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px;">
                    <button id="chlSysc" lay-filter="search" class="layui-btn" lay-submit>
                        <i class="fa fa-arrows-h" aria-hidden="true"></i> 渠道同步
                    </button>
                </div>

        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>渠道产品查询</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="layui-form-label">渠道名称</label>
                <div class="layui-input-inline layui-unselect">
                    <select name="channelName2" id="channelName2" class="form-control" lay-search>
                        <option value="">请选择渠道</option>
                    <#list laChlInfo as ChlInfo>
                        <option value="${(ChlInfo.channelCode)!}">${(ChlInfo.channelCode)!}—${(ChlInfo.channelName)!}</option>
                    </#list>
                    </select>
                </div>
                <#--<label class="layui-form-label">创建起期</label>-->
                <#--<div class="layui-input-inline">-->
                    <#--<input class="layui-input" placeholder="请选择日期" id="riskStartSellDate2"-->
                           <#--onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">-->
                <#--</div>-->
                <#--<label class="layui-form-label">创建止期</label>-->
                <#--<div class="layui-input-inline">-->
                    <#--<input class="layui-input" placeholder="请选择日期" id="riskEndSellDate2"-->
                           <#--onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">-->
                <#--</div>-->
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 10px;">
                <button id="search1" lay-filter="search" class="layui-btn" lay-submit>
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 10px;">
                <button type="button" class="layui-btn " id="exportExcel" name="exportExcel">
                    <i class="fa fa-file-excel-o"></i> 导出
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 10px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
        </div>
    </fieldset>


    <fieldset class="layui-elem-field">
        <legend>渠道产品信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>渠道编码</th>
                    <th>渠道名称</th>
                    <th>产品编码</th>
                    <th>产品名称</th>
                    <th>渠道产品上下架时间</th>
                    <th>渠道产品上下架状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
    </fieldset>
    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>


</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.proId == undefined ?"":item.proId }}</td>
        <td>{{ item.channelCode == undefined ?"":item.channelCode }}</td>
        <td>{{ item.channelName == undefined ?"":item.channelName }}</td>
        <td>{{ item.proCode == undefined ?"":item.proCode }}</td>
        <td>{{ item.proName == undefined ?"":item.proName }}</td>
        <td>{{ item.upDownDate == undefined ?"":item.upDownDate }}</td>
        <td>
            {{# if(item.status === '0'){ }} <span style="color: green">上架</span> {{# } }}
            {{# if(item.status === '1'){ }} <span style="color: red">下架</span> {{# } }}
        </td>
        <td>
            <a href="javascript:;" data-id="{{ item.proCode }}" data-idx="{{ item.channelCode }}" data-opt="view" class="layui-btn layui-btn-normal layui-btn-mini">详情</a>
            <#--<a href="javascript:;" data-id="{{ item.channelCode }}" data-idx="{{ item.proCode }}" data-ids="{{ item.status }}" data-opt="edit" class="layui-btn layui-btn-mini">编辑</a>-->
            <!--<a href="javascript:;" data-id="{{ item.mchId }}" data-opt="del" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>-->
        </td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/chl_sync/list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'), $(this).data('idx'), $(this).data('ids'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'), $(this).data('idx'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

                //绑定所有上架按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=up]').on('click', function () {
                        addForm($(this).data('id'));
                    });
                });

                //绑定所有下架按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=down]').on('click', function () {
                        addForm($(this).data('id'));
                    });
                });

            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            // layer.msg('你选择的名称有：' + names);
        });

        $("#reset").click(function () {
            $("#channelName2").val("");
            $("#riskStartSellDate2").val("");
            $("#riskEndSellDate2").val("");
            // form.render('checkbox');
            form.render();
        });

        $('#search1').on('click', function () {
            var channelCode = $("#channelName2").val();
            var startDate = $("#riskStartSellDate2").val();
            var endDate = $("#riskEndSellDate2").val();
            // alert(channelCode);
            if (startDate > endDate) {
                alert("截止日期必须在起始日期之后!");
                return false;
            };

            paging.get({
                "channelCode": channelCode,
                "createStartDate": startDate,
                "createEndDate": endDate,
                "v": new Date().getTime()
            });

        });


        var addBoxIndex = -1;
        $('#add').on('click', function () {
            if (addBoxIndex !== -1)
                return;
            editForm();
            addForm();
        });

        $('#import').on('click', function () {
            var that = this;
            var index = layer.tips('只想提示地精准些', that, {tips: [1, 'white']});
            $('#layui-layer' + index).children('div.layui-layer-content').css('color', '#000000');
        });

        function viewForm(proCode, channelCode) {

            //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
            $.get('/chl_sync/view.html?proCode=' + proCode + "&channelCode=" + channelCode, null, function(form) {
                // alert(proCode);
                addBoxIndex = layer.open({
                    type: 1,
                    title: '销售产品信息详情',
                    content: form,
                    btn: ['返回'],
                    shade: false,
                    offset: ['100px', '30%'],
                    area: ['600px', '550px'],
                    zIndex: 19991231,
                    id: 'lay_view', //设定一个id，防止重复弹出
                    maxmin: false,

                    full: function(elem) {
                        var win = window.top === window.self ? window : parent.window;
                        $(win).on('resize', function() {
                            var $this = $(this);
                            elem.width($this.width()).height($this.height()).css({
                                top: 0,
                                left: 0
                            });
                            elem.children('div.layui-layer-content').height($this.height() - 95);
                        });
                    },
                    end: function() {
                        addBoxIndex = -1;
                    }
                });
                layer.full(addBoxIndex);
            });
        }

        function editForm(channelCode, proCode, status) {
            // alert(channelCode);
            // alert(proCode);
            if (status == 0) {
                alert("产品上架状态不可进行编辑！");
                return false;
            }
            //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
            $.get('/chl_sync/edit.html?channelCode=' + channelCode + '&riskCode=' + proCode, null, function(form) {
                addBoxIndex = layer.open({
                    type: 1,
                    title: '渠道产品修改',
                    content: form,
                    btn: ['保存', '返回'],
                    shade: false,
                    offset: ['100px', '30%'],
                    area: ['600px', '450px'],
                    zIndex: 19991231,
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form').find('button[lay-filter=edit]').click();
                    },
                    full: function(elem) {
                        var win = window.top === window.self ? window : parent.window;
                        $(win).on('resize', function() {
                            var $this = $(this);
                            elem.width($this.width()).height($this.height()).css({
                                top: 0,
                                left: 0
                            });
                            elem.children('div.layui-layer-content').height($this.height() - 95);
                        });
                    },
                    success: function(layero, index) {
                        //弹出窗口成功后渲染表单
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var startDate = $("#riskStartSellDate3").val();
                            var endDate = $("#riskEndSellDate3").val();
                            if (startDate > endDate) {
                                alert("截止日期必须在起始日期之后!");
                                return false;
                            };
                            //这里可以写ajax方法提交表单
                            $.ajax({
                                type: "POST",
                                url: "/chl_sync/save",
                                data: "params=" + JSON.stringify(data.field),
                                success: function(data){
                                    console.log(data);
                                    // alert(msg);
                                    if(data.msg == '0000') {
                                        alert('保存成功');
                                        layerTips.close(index);
                                        location.reload(); //刷新
                                    }else {
                                        alert('保存失败, ' + data.msg);
                                        layerTips.close(index);
                                        // location.reload(); //刷新
                                        console.log(data.msg);
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                    end: function() {
                        addBoxIndex = -1;
                    }
                });
                layer.full(addBoxIndex);
            });
        }


        $('#exportExcel').on('click', function () {
            var channelCode = $("#channelName2").val();
            var startDate = $("#riskStartSellDate2").val();
            var endDate = $("#riskEndSellDate2").val();

            window.location.href='/chl_sync/excel?channelCode=' + channelCode + '&startDate=' + startDate + '&endDate=' + endDate;
        });

        $('#chlSysc').on('click', function (data) {
            var channelName = $("#channelName").val();
            var index = 0;
            if (channelName == "") {
                if(confirm('同步所有渠道需要较长的时间, 是否继续? ')){
                    layer.close(index);
                }else {
                    layer.close(index);
                    return;
                };
            }
            // alert(riskCode + "," + combCode);
            $.ajax({
                type: "POST",
                url: '/chl_sync/chlSysc',
                data: {channelName: channelName},
                dataType: 'json',
                cache: false,
                beforeSend: function() {
                    index = layer.load(0, {
                        shade: [0.5, 'gray'], //0.5透明度的灰色背景
                        content: '数据同步中, 请耐心等待...',
                        success: function (layero) {
                            layero.find('.layui-layer-content').css({
                                'padding-top': '39px',
                                'width': '160px'
                            });
                        }
                    }, {time: 20*1000});
                },
                success: function (data) {
                    // console.log(data);
                    if (data.msg == '0000') {
                        alert('同步成功');
                        // location.reload(); //刷新
                        $('#search11').click();//刷新页面
                    } else {
                        alert('同步失败: ' + data.msg);
                        // alert('核心无满足查询条件的数据');
                        console.log(data.msg);
                        // location.reload(); //刷新
                    }
                },
                complete: function () {
                    layer.close(index);
                }
            });

        });
    });
</script>
</body>

</html>