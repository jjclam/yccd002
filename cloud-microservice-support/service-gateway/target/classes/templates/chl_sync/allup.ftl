<head>
    <meta charset="UTF-8">
    <title>渠道产品一键上架</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 180px">
    <fieldset class="layui-elem-field">
        <legend>渠道查询</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form" style="float:left;">
                <div class="layui-form-item" style="margin:0;">
                    <label class="layui-form-label">渠道名称</label>
                    <div class="layui-input-inline layui-unselect" >
                        <select name="channelCode" id="channelCode" class="form-control" lay-search>
                            <option value="">请选择渠道</option>
                    <#list laChlInfo as ChlInfo>
                        <option value="${(ChlInfo.channelCode)!}">${(ChlInfo.channelCode)!}-${(ChlInfo.channelName)!}</option>
                    </#list>
                        </select>
                    </div>
                <#--<label class="layui-form-label">创建起期</label>-->
                <#--<div class="layui-input-inline">-->
                <#--<input class="layui-input" placeholder="请选择日期" id="createStartDate"-->
                <#--onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">-->
                <#--</div>-->
                <#--<label class="layui-form-label">创建止期</label>-->
                <#--<div class="layui-input-inline">-->
                <#--<input class="layui-input" placeholder="请选择日期" id="createEndDate"-->
                <#--onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">-->
                <#--</div>-->
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 10px;">
                    <button id="search" lay-filter="search" class="layui-btn" lay-submit>
                        <i class="fa fa-search" aria-hidden="true"></i> 查询
                    </button>
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-top: 10px; margin-left: 5px">
                    <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                        <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                    </button>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>渠道列表</legend>

        <div class="layui-field-box layui-form" style="width: 800px">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>渠道名称</th>
                <#--<th>创建时间</th>-->
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>


    <form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">险种类型<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="riskType" id="riskType" class="form-control" lay-filter="funChange" lay-search>
                        <option value="">请选择险种类型</option>
                        <#list laRiskTypeInfo as SaleProInfo>
                            <option value="${(SaleProInfo.riskTypeCode)!}">${(SaleProInfo.riskTypeCode)!}-${(SaleProInfo.riskTypeName)!}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">产品名称<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="riskCode" class="form-control" id="riskCode" lay-search>
                        <option value="">请选择产品名称</option>
                    <#--<#list laAddProInfo as SaleProInfo>-->
                        <#--<option value="${(SaleProInfo.proCode)!}">${(SaleProInfo.proCode)!}-${(SaleProInfo.proName)!}</option>-->
                    <#--</#list>-->
                    </select>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">产品起售日期<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期" name="riskStartSellDate" id="riskStartSellDate4"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">产品止售日期<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期" name="riskEndSellDate" id="riskEndSellDate4"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">H5页面链接</label>
            <div class="layui-input-inline" style="display:flex">
                <input type="text" class="layui-input" name="h5href" id="h5href" autocomplete="off"
                       value="${item.h5href?if_exists }">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-hide">
                <label class="layui-form-label">续保权限<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="proType" id="proType" lay-verify="">
                        <option value="">请选择产品的续保权限</option>
                        <option value="01">关闭</option>
                        <option value="02">打开</option>
                    </select>
                </div>
            </div>
            <div class="layui-hide">
                <label class="layui-form-label">配置状态<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="proType" id="proType" lay-verify="">
                        <option value="">请选择该产品的配置状态</option>
                        <option value="0">无效</option>
                        <option value="1">有效</option>
                    </select>
                </div>
            </div>
            <div class="layui-hide">
                <label class="layui-form-label">上下架方式<font color=red>*</font></label>
                <div class="layui-input-block" style="display:flex">
                    <select name="proType" id="proType" lay-verify="">
                        <option value="">请选择该产品上下架的方式</option>
                        <option value="00">手动立即上下架</option>
                        <option value="01">自动定时上下架</option>
                    </select>
                </div>
            </div>
        </div>

    </form>

    <div class="layui-form-item" style="display: flex">
        <div class="layui-form" style="margin-left: 30px;">
            <button id="upstatus" lay-filter="save" class="layui-btn" lay-submit>
                <i class="fa fa-level-up" aria-hidden="true"></i> 上架
            </button>
        </div>

    </div>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.proId == undefined ?"":item.proId }}</td>
        <td>{{ item.channelName == undefined ?"":item.channelName }}</td>
        <td style="display:none">{{ item.channelCode == undefined ?"":item.channelCode }}</td>
    <#--<td>{{ item.createDate == undefined ?"":item.createDate }}</td>-->
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/chl_sync/up_list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            layer.msg('你选择的名称有：' + names);
        });


        $("#reset").click(function () {
            $("#channelCode").val("");
            $("#riskType").val("");
            $("#riskCode").empty().append("<option value=\"\">请选择产品名称</option>");
            $("#riskCode").val("");
            // form.render('checkbox');
            form.render();
        });

        $('#import').on('click', function () {
            var that = this;
            var index = layer.tips('只想提示地精准些', that, {tips: [1, 'white']});
            $('#layui-layer' + index).children('div.layui-layer-content').css('color', '#000000');
        });

        $('#search').on('click', function () {
            var channelCode = $("#channelCode").val();
            var createStartDate = $("#createStartDate").val();
            var createEndDate = $("#createEndDate").val();
            // alert(channelCode);
            paging.get({
                "channelCode": channelCode,
                "createStartDate": createStartDate,
                "createEndDate": createEndDate,
                "v": new Date().getTime()
            });
        });

        $('#upstatus').on('click', function () {
            var riskType = $("#riskType").val();
            var riskCode = $("#riskCode").val();
            var riskStartSellDate = $("#riskStartSellDate4").val();
            var riskEndSellDate = $("#riskEndSellDate4").val();
            var h5href = $("#h5href").val();
            var channelCodes = '';
            var proCodes = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    var j = $that.children('td').eq(3).text();
                    // channelCodes += n + ',';
                    channelCodes += j + '-' + n + ',';
                    record ++;
                    // alert(n);
                }
            });
            if (record == '') {
                alert("请先选择一条上架渠道!");
                return false;
            }
            if (riskType == '') {
                alert("请先选择险种类型!");
                return false;
            }
            if (riskCode == '') {
                alert("请先选择产品!");
                return false;
            }
            if (riskStartSellDate == '') {
                alert("请录入起售日期!");
                return false;
            }
            if (riskEndSellDate == '') {
                alert("请录入止售日期!");
                return false;
            }
            if (riskStartSellDate > riskEndSellDate) {
                alert("销售止期需在销售起期之后！");
                return false;
            };

            //layer.msg('你选择的名称有：' + channelCodes + "-" + proCodes);
            $.ajax({
                type: "POST",
                url: '/chl_sync/allupChlPro',
                // data: {channelCodes: channelCodes, riskType: riskType, riskCode: riskCode},
                data: "params=" + JSON.stringify({
                    channelCodes: channelCodes
                    , riskType: riskType
                    , riskCode: riskCode
                    , riskStartSellDate: riskStartSellDate
                    , riskEndSellDate: riskEndSellDate
                    , h5href: h5href
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('上架成功');
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }else {
                        alert('上架失败, ' + data.msg);
                        console.log(data.msg);
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }
                }
            });
        });

        $('#back').on('click', function () {
            // alert(11111);
            // layer.closeAll('loading'); //关闭加载层
            // layer.close(layer.index);
            // layer.closeAll();

        });

        form.on('select(funChange)', function (data) {
            form.render('select');
            var riskType = $("#riskType").val();

            if (riskType == "") {
                var select = document.getElementById("riskCode");
                var newOption = document.createElement("option");
                newOption.text = "请先选择险种!";
                newOption.value = "";
                select.options.add(newOption);
            }
            // $("#riskCode").val("");
            // form.render();
            // alert(proName);
            //写ajax
            $.ajax({
                type:'post',
                dataType:'json',
                url:'/chl_sync/linkProduct',
                data:{riskType:riskType},
                success: function(data){
                    var dataList = data.linkPro;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#riskCode").empty().append("<option value=\"\">请选择产品名称</option>");
                        $("#riskCode").val("");
                        form.render();
                        return;
                    }
                    if(dataList!=null){
                        $("#riskCode").empty();
                        var select = document.getElementById("riskCode");
                        for(var i=0;i<dataList.length;i++){ //遍历
                            var categoryObj = dataList[i];
                            // alert(categoryObj);
                            var proCode = categoryObj.proCode;
                            var proName = categoryObj.proName;
                            // alert(proName);
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            newOption.text = proCode + '-' + proName;
                            newOption.value = proCode + '-' + proName;//为添加上架产品新增拼接符;
                            // document.getElementById("riskCode").options.add(newOption);
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                },error:function(data){
                    alert('系统错误');
                }
            });

        });

    });
</script>
</body>
