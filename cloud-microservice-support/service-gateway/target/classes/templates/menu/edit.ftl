<body>
<div style="margin: 15px;">

    <form class="layui-form">

        <div class="layui-form-item">
            <label class="layui-form-label">菜单名称<font color=red>*</font></label>
            <div class="layui-input-block">
                <input type="hidden" name="id"  value="${item.id?if_exists }">

                <input type="text" name="name" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input" value="${item.name?if_exists }">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">系统<font color=red>*</font></label>
            <div class="layui-input-block" >
                <#--<select name="field" id="field" lay-filter="onchangePos">-->
                <select name="field" id="field" lay-filter="onchangeParentMenu">
                    <option value="0" <#if (item.field!"") == "0">selected="selected"</#if>>外勤</option>
                    <option value="1" <#if (item.field!"") == "1">selected="selected"</#if>>企业HR</option>
                    <option value="2" <#if (item.field!"") == "2">selected="selected"</#if>>中台</option>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">上一级</label>
            <div class="layui-input-block">

                <select id="parentId" name="parentId"  class="form-control"  lay-search>
                    <option value="">请选择</option>
					<#--<#list parentMenu as getparentMenu>-->
                        <#--<option value="${(getparentMenu.id)!}" <#if (item.parentId!"") == "${(getparentMenu.id)!}">selected="selected"</#if>>${(getparentMenu.id)!}-${(getparentMenu.name)!}（${(getparentMenu.remarks1)!}）</option>-->
                    <#--</#list>-->
                <option value="${(parentMenu.id)!}" <#if (item.parentId!"") == "${(parentMenu.id)!}">selected="selected"</#if>>${(parentMenu.name)!}</option>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">链接</label>
            <div class="layui-input-block">
                <input type="text" name="href"   placeholder="请输入链接" autocomplete="off" class="layui-input" value="${item.href?if_exists }">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">路径</label>
            <div class="layui-input-block">
                <input type="text" name="path"   placeholder="中台不输入" autocomplete="off" class="layui-input" value="${item.path?if_exists }">
            </div>
        </div>


    <#--<div class="layui-form-item" >-->
    <#--<label class="layui-form-label">图标</label>-->
    <#--<div class="layui-input-block">-->
    <#--<input type="text" name="icon" lay-verify="required" placeholder="请输入图标" autocomplete="off" class="layui-input" value="${item.icon?if_exists }">-->
    <#--</div>-->
    <#--</div>-->


    <#--<#if item.id??>-->
    <#--&lt;#&ndash;val是有值的&ndash;&gt;-->
    <#--<#else>-->
        <#--<div class="layui-form-item">-->
            <#--<label class="layui-form-label">岗位</label>-->
            <#--<div class="layui-input-block">-->

                <#--<select id="position" name="role"  class="form-control"  lay-search>-->
                    <#--<option value="">请选择</option>-->
                <#--&lt;#&ndash;<#list role as getrole>&ndash;&gt;-->
                <#--&lt;#&ndash;<option value="${(getrole.id)!}">${(getrole.id)!}-${(getrole.name)!}</option>&ndash;&gt;-->
                <#--&lt;#&ndash;</#list>&ndash;&gt;-->
                <#--</select>-->
            <#--</div>-->
        <#--</div>-->
    <#--</#if>-->
        <div class="layui-form-item">
            <#--<label class="layui-form-label">是否启用<font color=red>*</font></label>-->
            <label class="layui-form-label">状态<font color=red>*</font></label>
            <div class="layui-input-block" >
                <select name="permission" id="permission"  class="form-control"  lay-search>
                    <option value="1" <#if (item.permission!"") == "1">selected="selected"</#if>>启用</option>
                    <option value="2" <#if (item.permission!"") == "2">selected="selected"</#if>>停用</option>
                </select>
            </div>
        </div>

        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>

</div>
<script type="text/javascript">
     layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        form.on('select(onchangePos)', function (data) {
            form.render('select');
            var field = $("#field").val();
            // alert(field);

            if (field == "") {
                var select = document.getElementById("position");
                var newOption = document.createElement("option");
                newOption.text = "请先选择所属系统!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/mgr_user/linkedPositions',
                data:{field: field},
                success: function(data){
                    var dataList = data.linkedPositions;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#position").empty().append("<option value=\"\">请先选择所属系统</option>");
                        form.render('select');
                        return;
                    }
                    if(dataList!=null){
                        $("#position").empty();
                        var select = document.getElementById("position");
                        //联动框非强制选;默认空串
                        var newOption = document.createElement("option");
                        newOption.text = "请选择岗位";
                        newOption.value = "" ;
                        select.options.add(newOption);
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            var code = categoryObj.positionId;
                            var codeName = categoryObj.position;
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            // newOption.text = code + '-' + codeName;
                            // newOption.value = code + '-' + codeName;
                            newOption.text = codeName;
                            newOption.value = code;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }
                },error:function(data){
                    alert('系统错误');
                }
            });

        });

        form.on('select(onchangeParentMenu)', function (data) {
            form.render('select');
            var field = $("#field").val();
            // alert(field);

            if (field == "") {
                var select = document.getElementById("position");
                var newOption = document.createElement("option");
                newOption.text = "请先选择所属系统!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/menu/linkedParentMenus',
                data:{field: field},
                success: function(data){
                    var dataList = data.linkedParentMenus;
                    var error = data.msg;
                    if (error) {
                        $("#parentId").empty().append("<option value=\"\">请先选择所属系统</option>");
                        form.render('select');
                        return;
                    }
                    if(dataList!=null){
                        $("#parentId").empty();
                        var select = document.getElementById("parentId");
                        //联动框非强制选;默认空串
                        var newOption = document.createElement("option");
                        newOption.text = "请选择菜单";
                        newOption.value = "" ;
                        select.options.add(newOption);
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            var code = categoryObj.id;
                            var codeName = categoryObj.name;
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            newOption.text = codeName;
                            newOption.value = code;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }
                },error:function(data){
                    alert('系统错误');
                }
            });

        });

    });

</script>
</body>