﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>短信模板配置</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
    <fieldset class="layui-elem-field">
        <legend>参数查询</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
                <button id="search1" type="button" class="layui-btn" style="margin-left: 30px;" >
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>参数信息列表</legend>
        <div class="layui-field-box layui-form">
        <table class="layui-table admin-table" id="dataTable2">
            <thead>
            <tr>
                <th>序号</th>
                <th>参数类型</th>
                <th>参数名称</th>
            </tr>
            </thead>
            <#--<tbody id="content1" class="layui-hide" >-->
            <tbody id="content1">
            <#--<tr>-->
                <#--<td>1</td>-->
                <#--<td>&lt;appntName></td>-->
                <#--<td>投保人姓名</td>-->
            <#--</tr>-->
            <#--<tr>-->
                <#--<td>2</td>-->
                <#--<td>&lt;productName></td>-->
                <#--<td>投保人姓名</td>-->
            <#--</tr>-->
            <#--<tr>-->
                <#--<td>3</td>-->
                <#--<td>&lt;appntName></td>-->
                <#--<td>险种名称</td>-->
            <#--</tr>-->
            <#--<tr>-->
                <#--<td>4</td>-->
                <#--<td>&lt;contNo></td>-->
                <#--<td>保单号码</td>-->
            <#--</tr>-->
            <#--<tr>-->
                <#--<td>5</td>-->
                <#--<td>&lt;prem></td>-->
                <#--<td>保费</td>-->
            <#--</tr>-->
            <#--<tr>-->
                <#--<td>6</td>-->
                <#--<td>&lt;cValiDate></td>-->
                <#--<td>生效日</td>-->
            <#--</tr>-->

            </tbody>
        </table>
        </div>

    </fieldset>

<fieldset class="layui-elem-field">
    <legend>查询条件</legend>
    <div class="layui-form" style="float:left; width: 100%;">
        <div class="layui-inline">
            <label class="layui-form-label">渠道代码</label>
            <div class="layui-input-block" style="display:flex">
                <select name="channelCode" id="channelCode" class="form-control" lay-filter="channelChange" lay-search>
                    <option value="">请选择渠道</option>
                    <#list laChlInfo as ChlInfo>
                        <option value="${(ChlInfo.channelCode)!}">${(ChlInfo.channelCode)!}-${(ChlInfo.channelName)!}</option>
                    </#list>
                </select>
            </div>
        </div>
        <div class="layui-inline" style="">
            <label class="layui-form-label">产品代码</label>
            <div class="layui-input-inline">
                <input type="text" name="proCode" id="proCode" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form" style="margin-top: 47px;">
        <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
            <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                <i class="fa fa-search" aria-hidden="true"></i> 查询
            </button>
        </div>
        <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;margin-left: 5px;">
            <button id="add" type="button" class="layui-btn">
                <i class="fa fa-plus" aria-hidden="true"></i> 增加
            </button>
        </div>
        <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
            <button id="delete" class="layui-btn">
                <i class="fa fa-times" aria-hidden="true"></i> 删除
            </button>
        </div>
        <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
            <button id="update" class="layui-btn">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> 更新
            </button>
        </div>
        <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
            <button id="reset" lay-filter="reset" class="layui-btn">
                <i class="fa fa-refresh" aria-hidden="true"></i> 重置
            </button>
        </div>
    </div>
</fieldset>


<fieldset class="layui-elem-field">
    <legend>短信配置信息列表</legend>
    <div class="layui-field-box layui-form">
        <table class="layui-table admin-table" id="dataTable">
            <thead>
            <tr>
                <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                <th style="width: 30px;">序号</th>
                <th style="width: 60px;">渠道代码</th>
                <th style="width: 90px;">产品代码</th>
                <th>短信内容</th>
            </tr>
            </thead>
            <tbody id="content">
            </tbody>
        </table>
    </div>

    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>

</fieldset>

</div>

<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, sms){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ sms.id == undefined ?"":sms.id }}</td>
        <td>{{ sms.channelCode == undefined ?"":sms.channelCode }}</td>
        <td>{{ sms.riskCode == undefined ?"":sms.riskCode }}</td>
        <td>{{ sms.smsInfo == undefined ?"":sms.smsInfo.replace(/</g, '&lt;') }}</td>//去标签化
        <#--<td>{{ sms.smsInfo == undefined ?"":sms.smsInfo }}</td>-->
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/sms_maintain/list',
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                   alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });

        $("#reset").click(function () {
            $("#channelCode").val("");
            $("#proCode").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var channelCode = $("#channelCode").val();
            var proCode = $("#proCode").val();

            paging.get({
                "channelCode": channelCode,
                "riskCode": proCode,
                "v": new Date().getTime()
            });
        });

        $('#add').on('click', function () {

            $.get('/sms_maintain/sms_add.html', function(form) {
                layer.open({
                    type: 1,
                    title: '短信模板增加',
                    content: form,
                    btn: ['提交', '返回'],
                    shade: false,
                    offset: 'auto',
                    area : ['850px', '480px'],
                    zIndex: 19991231,
                    id: 'lay_sms_add',
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form-item').find('button[lay-filter=edit]').click();
                    },
                    success: function(layero, index) {
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var channelCode = $("#channelCode2").val();
                            var proCode = $("#proCode2").val();
                            var smsInfo = $("#smsInfo2").val();
                            if (channelCode == '') {
                                alert("请输入渠道!");
                                return false;
                            }
                            if (proCode == '') {
                                alert("请输入产品代码!");
                                return false;
                            }
                            if (smsInfo == '') {
                                alert("请输入短信内容!");
                                return false;
                            }

                            $.ajax({
                                type: "POST",
                                url: "/sms_maintain/addSmsMaintain",
                                data: "params=" + JSON.stringify({
                                    channelCode: channelCode
                                    , proCode: proCode
                                    , smsInfo: smsInfo
                                }),
                                dataType: 'json',
                                success: function(data){
                                    if(data.msg == '0000') {
                                        alert('添加成功');
                                        // location.reload(); //刷新
                                        layer.closeAll();
                                        $('#search').click();//刷新页面
                                    }else {
                                        alert('添加失败, ' + data.msg);
                                        console.log(data.msg);
                                        // location.reload(); //刷新
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                });
            });
        });

        $('#update').on('click', function () {
            var channelCode = '';
            var proCode = '';
            // var smsInfo = '';
            var smsArray = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    channelCode += n;
                    var j = $that.children('td').eq(3).text();
                    proCode += j;
                    var smsInfo = $that.children('td').eq(4).text();
                    // smsInfo += k;
                    // smsArray = channelCode + '||' + proCode + '||' + smsInfo;
                    record++;
                }
            });
            if (record == '') {
                alert("请先选择一条信息! ");
                return false;
            }
            if (record > 1) {
                alert("一次只能选择一条进行修改! ");
                return false;
            }

            // smsArray.replace(/\\n\\r/g, '');
            // alert(smsArray);
            // $.get('/sms_maintain/sms_edit.html?smsArray=' + smsArray, function(form) {
            $.get('/sms_maintain/sms_edit.html?channelCode=' + channelCode + '&proCode=' + proCode, function(form) {
                layer.open({
                    type: 1,
                    title: '短信模板修改',
                    content: form,
                    // content: $('#sms_edit').html(),
                    // content: 'sms_edit.html',
                    // data: "params=" + JSON.stringify({
                    //     channelCode: channelCode
                    //     , proCode: proCode
                    //     , smsInfo: smsInfo
                    // }),
                    btn: ['提交', '返回'],
                    shade: false,
                    offset: 'auto',
                    area : ['850px', '480px'],
                    zIndex: 19991231,
                    id: 'lay_editSMS',
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form-item').find('button[lay-filter=edit]').click();
                    },
                    success: function(layero, index) {
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var channelCode = $("#channelCode3").val();
                            // alert(userName);
                            var proCode = $("#proCode3").val();
                            var smsInfo = $("#smsInfo3").val();
                            if (channelCode == '') {
                                alert("请输入渠道!");
                                return false;
                            }
                            if (proCode == '') {
                                alert("请输入产品!");
                                return false;
                            }
                            if (smsInfo == '') {
                                alert("请输入短信内容!");
                                return false;
                            }

                            $.ajax({
                                type: "POST",
                                url: "/sms_maintain/updateSmsMaintain",
                                data: "params=" + JSON.stringify({
                                    channelCode: channelCode
                                    , proCode: proCode
                                    , smsInfo: smsInfo
                                }),
                                dataType: 'json',
                                success: function(data){
                                    if(data.msg == '0000') {
                                        alert('修改成功');
                                        // location.reload(); //刷新
                                        layer.closeAll();
                                        $('#search').click();//刷新页面
                                    }else {
                                        alert('修改失败, ' + data.msg);
                                        console.log(data.msg);
                                        // location.reload(); //刷新
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                });
            });
        });

        $('#delete').on('click', function () {
            var deleteArray= new Array();
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    var j = $that.children('td').eq(3).text();
                    deleteArray.push({channelCode: n, riskCode: j});
                    record++;
                }
            });
            if (record == '') {
                alert("请先选择一条信息!");
                return false;
            }
            $.ajax({
                type: "POST",
                url: '/sms_maintain/deleteSmsMaintain',
                data: JSON.stringify(deleteArray),
                dataType: 'json',
                contentType : 'application/json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('删除成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('删除失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });

        $('#search2').on('click', function () {
            $("#content1").removeClass('layui-hide');
        });

        $('#search1').on('click', function () {

            $.ajax({
                type:'post',
                data: {codeType: 'smsparam'},
                dataType:'json',
                url:'/sms_maintain/smsParams',
                success: function(data){
                    var dataList = data.smsParam;
                    var error = data.msg;
                    if (error) {
                        $('#content1').html('');
                        return;
                    }
                    if(dataList!=null){
                        var tbody = document.getElementById("content1");
                        $('#content1').html('');
                        //联动框非强制选;默认空串
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var smsParamsObj = dataList[i];
                            // alert(categoryObj);
                            var id = smsParamsObj.id;
                            var code = smsParamsObj.code;
                            var codeName = smsParamsObj.codeName;
                            // alert(proName);
                            //进行添加到标签里
                            var row  = document.createElement("tr");
                            var idCell = document.createElement("td");
                            idCell.innerHTML = id;
                            row.appendChild(idCell);

                            var codeCell = document.createElement("td");
                            codeCell.innerHTML = code;
                            row.appendChild(codeCell);

                            var codeNameCell = document.createElement("td");
                            codeNameCell.innerHTML = codeName;
                            row.appendChild(codeNameCell);

                            tbody.appendChild(row);
                        }
                        form.render();
                    }

                },error:function(data){
                    alert('系统错误');
                }
            });

        });

    });
</script>
</body>

</html>