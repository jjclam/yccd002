// var navs = [{
//     "title": "产品中心",
//     "icon": "fa-cubes",
//     "spread": true,
//     "children": [{
//         "title": "产品信息同步",
//         "icon": "&#xe641;",
//         "href": "/pro_sync/list.html"
//     }, {
//         "title": "产品上下架配置",
//         "icon": "&#xe63c;",
//         "href": "/pro_sync/upDown_list.html"
//     }, {
//         "title": "产品标准配置",
//         "icon": "&#xe63c;",
//         "href": "/pro_center/standard_list.html"
//     }]
// }, {
//     "title": "渠道管理",
//     // "icon": "&#x1002;",
//     "icon": "fa-cubes",
//     "spread": true,
//     "children": [{
//         "title": "渠道信息同步",
//         "icon": "&#xe63c;",
//         "href": "/chl_sync/sync.html"
//     }, {
//         "title": "渠道产品管理",
//         "icon": "&#xe641;",
//         "href": "/chl_sync/list.html"
//     }, {
//         "title": "渠道信息查询",
//         "icon": "&#xe641;",
//         "href": "/chl_edit/list.html"
//     }, {
//         "title": "渠道中介机构配置",
//         "icon": "&#xe641;",
//         "href": "/chl_edit/chl_agencyMaintain.html"
//     }, {
//         "title": "渠道信息维护",
//         "icon": "&#xe641;",
//         "href": "/chl_edit/info_maintain.html"
//     }]
// }, {
//     "title": "运营管理",
//     // "icon": "&#x1002;",
//     "icon": "fa-cubes",
//     "spread": true,
//     "children": [{
//         "title": "保单查询",
//         "icon": "&#xe63c;",
//         "href": "/pol_search/list.html"
//     }, {
//         "title": "人工撤单",
//         "icon": "&#xe641;",
//         "href": "/remove_pol/list.html"
//     }, {
//         "title": "保费统计",
//         "icon": "&#xe641;",
//         "href": "/prem_count/list.html"
//     }]
// }, {
//     "title": "代码维护",
//     // "icon": "&#x1002;",
//     "icon": "fa-cubes",
//     "spread": true,
//     "children": [{
//         "title": "标准代码维护",
//         "icon": "&#xe63c;",
//         "href": "/code_maintain/core_list.html"
//     }, {
//         "title": "渠道代码映射",
//         "icon": "&#xe641;",
//         "href": "/chl_maintain/chl_list.html"
//     }]
// }, {
//     "title": "用户管理",
//     // "icon": "&#x1002;",
//     "icon": "fa-cubes",
//     "spread": true,
//     "children": [{
//         "title": "用户信息",
//         "icon": "&#xe63c;",
//         "href": "/mgr_user/list.html"
//     }]
//
// }];
var navs = [
    {
    "title": "单证管理",
    "icon": "fa-cubes",
    "spread": true,
    "children": [{
        "title": "单证回退",
        "icon": "&#xe62a;",
        "href": "/doc_mng/docback.html"
    }, {
        "title": "单证回销",
        "icon": "&#xe62a;",
        "href": "/doc_mng/docresale.html"
    }, {
        "title": "单证统计",
        "icon": "&#xe62a;",
        "href": "/doc_mng/docsatistics.html"
    }]
},
    {
        "title": "产品管理",
        "icon": "fa-cubes",
        "spread": true,
        "children": [{
            "title": "算法定义",
            "icon": "&#xe62a;",
            "href": "/pro_mng/functdef.html"
        }, {
            "title": "产品定义",
            "icon": "&#xe62a;",
            "href": "/pro_mng/prodef.html"
        }, {
            "title": "套餐定义",
            "icon": "&#xe62a;",
            "href": "/pro_mng/mealdef.html"
        },{
            "title": "套餐授权",
            "icon": "&#xe62a;",
            "href": "/pro_mng/mealauth.html"
        }]
    }
];