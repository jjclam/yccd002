﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>批量出单</title>
 <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
 <link rel="stylesheet" href="../css/global.css" media="all">
 <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
 <link rel="stylesheet" href="../css/table.css"/>

</head>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>

<body>
<div class="admin-main" style="padding-bottom: 50px">
    <div class="layui-form-item" >
        <label class="layui-form-label">批量出单</label>
        <div class="layui-upload">
            <input type="file" name="file" class="layui-upload-file" lay-ext="xls|xlsx|xlsm|xlt|xltx|xltm" lay-title="请上传Excel文件">
        </div>
    </div>
    <div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 10px;">
        <button type="button" class="layui-btn " id="settleExcel" name="settleExcel">
            <i class="fa fa-file-excel-o"></i> 下载Excel模版
        </button>
    </div>

</div>
<!--模板-->
<!--模板-->
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script type="text/javascript">
    layui.config({
        base: '../plugins/layui/layui_exts/'
    }).extend({
        excel: 'excel'
    });
</script>

<script type="text/javascript">
    layui.use(['form','upload'],function(){
        layui.upload({
            url: '/contBatch/importData',
            accept: "file",
            before: function(input){
                //返回的参数item，即为当前的input DOM对象
                console.log('文件上传中');
            }
            ,success: function(res){
                console.log('上传完毕');
                alert('上传完毕，批量出单成功');
            }
        });
    });

</script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        $('#settleExcel').on('click', function () {
            window.location.href='/contBatch/exportExcel?exportFlag=1';
        });
    });
</script>

<script>
    $(window).on('load',function(){});
</script>
</body>

</html>