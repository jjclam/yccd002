﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>套餐管理</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>

</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">
    <fieldset class="layui-elem-field">
        <legend>套餐查询</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="layui-form-label">套餐名称</label>
                <div class="layui-input-inline layui-unselect">
                    <select name="channelName2" id="channelName2" class="form-control" lay-search>
                        <option value="">请选择套餐</option>
                    <#--<#list laChlInfo as ChlInfo>-->
                        <#--<option value="${(ChlInfo.channelCode)!}">${(ChlInfo.channelCode)!}—${(ChlInfo.channelName)!}</option>-->
                    <option value="J001-航空意外综合套餐">J001-航空意外综合套餐</option>
                    <option value="L001-旅游意外综合套餐">L001-旅游意外综合套餐</option>
                    <option value="S001-学生意外综合套餐">S001-学生意外综合套餐</option>
                    <#--</#list>-->
                    </select>
                </div>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 10px;">
                <button id="search1" lay-filter="search" class="layui-btn" lay-submit>
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <#--<div class="layui-form-mid layui-word-aux" style="margin-left: 5px; margin-top: 10px;">-->
                <#--<button type="button" class="layui-btn " id="exportExcel" name="exportExcel">-->
                    <#--<i class="fa fa-file-excel-o"></i> 导出-->
                <#--</button>-->
            <#--</div>-->
            <div class="layui-form-mid layui-word-aux" style="margin-top: 10px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
        </div>
    </fieldset>


    <fieldset class="layui-elem-field">
        <legend>套餐信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>套餐编码</th>
                    <th>套餐名称</th>
                    <th>产品编码</th>
                    <th>产品名称</th>
                    <th>套餐产品上下架时间</th>
                    <th>套餐产品上下架状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
    </fieldset>
    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>

    <#--<div class="layui-form" style="float:left;">-->
        <#--<button id="upstatus" lay-filter="save" class="layui-btn" lay-submit>-->
            <#--<i class="fa fa-save" aria-hidden="true"></i> 详情-->
        <#--</button>-->
    <#--</div>-->
    <div style="display: flex">
        <div class="layui-form" style="margin-left: 30px;">
            <button id="upstatus" lay-filter="save" class="layui-btn" lay-submit>
                <i class="fa fa-level-up" aria-hidden="true"></i> 上架
            </button>
        </div>
        <div class="layui-form" style="margin-left: 5px;">
            <button id="downstatus" lay-filter="save" class="layui-btn" lay-submit>
                <i class="fa fa-level-down" aria-hidden="true"></i> 下架
            </button>
        </div>

    </div>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.proId == undefined ?"":item.proId }}</td>
        <td>{{ item.channelCode == undefined ?"":item.channelCode }}</td>
        <td>{{ item.channelName == undefined ?"":item.channelName }}</td>
        <td>{{ item.proCode == undefined ?"":item.proCode }}</td>
        <td>{{ item.proName == undefined ?"":item.proName }}</td>
        <td>{{ item.upDownDate == undefined ?"":item.upDownDate }}</td>
        <td>
            {{# if(item.status === '0'){ }} <span style="color: green">上架</span> {{# } }}
            {{# if(item.status === '1'){ }} <span style="color: red">下架</span> {{# } }}
        </td>
        <td>
            <a href="javascript:;" data-id="{{ item.proCode }}" data-idx="{{ item.channelCode }}" data-opt="view" class="layui-btn layui-btn-normal layui-btn-mini">详情</a>
            <#--<a href="javascript:;" data-id="{{ item.channelCode }}" data-idx="{{ item.proCode }}" data-ids="{{ item.status }}" data-opt="edit" class="layui-btn layui-btn-mini">编辑</a>-->
            <!--<a href="javascript:;" data-id="{{ item.mchId }}" data-opt="del" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>-->
        </td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/chl_sync/list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'), $(this).data('idx'), $(this).data('ids'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'), $(this).data('idx'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

                //绑定所有上架按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=up]').on('click', function () {
                        addForm($(this).data('id'));
                    });
                });

                //绑定所有下架按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=down]').on('click', function () {
                        addForm($(this).data('id'));
                    });
                });

            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            // layer.msg('你选择的名称有：' + names);
        });

        $("#reset").click(function () {
            $("#channelName2").val("");
            $("#riskStartSellDate2").val("");
            $("#riskEndSellDate2").val("");
            // form.render('checkbox');
            form.render();
        });

        $('#search1').on('click', function () {
            var channelCode = $("#channelName2").val();
            var startDate = $("#riskStartSellDate2").val();
            var endDate = $("#riskEndSellDate2").val();
            // alert(channelCode);
            if (startDate > endDate) {
                alert("截止日期必须在起始日期之后!");
                return false;
            };

            paging.get({
                "channelCode": channelCode,
                "createStartDate": startDate,
                "createEndDate": endDate,
                "v": new Date().getTime()
            });

        });


        var addBoxIndex = -1;
        $('#add').on('click', function () {
            if (addBoxIndex !== -1)
                return;
            editForm();
            addForm();
        });

        $('#import').on('click', function () {
            var that = this;
            var index = layer.tips('只想提示地精准些', that, {tips: [1, 'white']});
            $('#layui-layer' + index).children('div.layui-layer-content').css('color', '#000000');
        });

        function viewForm(proCode, channelCode) {

            //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
            $.get('/chl_sync/view.html?proCode=' + proCode + "&channelCode=" + channelCode, null, function(form) {
                // alert(proCode);
                addBoxIndex = layer.open({
                    type: 1,
                    title: '销售产品信息详情',
                    content: form,
                    btn: ['返回'],
                    shade: false,
                    offset: ['100px', '30%'],
                    area: ['600px', '550px'],
                    zIndex: 19991231,
                    id: 'lay_view', //设定一个id，防止重复弹出
                    maxmin: false,

                    full: function(elem) {
                        var win = window.top === window.self ? window : parent.window;
                        $(win).on('resize', function() {
                            var $this = $(this);
                            elem.width($this.width()).height($this.height()).css({
                                top: 0,
                                left: 0
                            });
                            elem.children('div.layui-layer-content').height($this.height() - 95);
                        });
                    },
                    end: function() {
                        addBoxIndex = -1;
                    }
                });
                layer.full(addBoxIndex);
            });
        }

        function editForm(channelCode, proCode, status) {
            // alert(channelCode);
            // alert(proCode);
            if (status == 0) {
                alert("产品上架状态不可进行编辑！");
                return false;
            }
            //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
            $.get('/chl_sync/edit.html?channelCode=' + channelCode + '&riskCode=' + proCode, null, function(form) {
                addBoxIndex = layer.open({
                    type: 1,
                    title: '渠道产品修改',
                    content: form,
                    btn: ['保存', '返回'],
                    shade: false,
                    offset: ['100px', '30%'],
                    area: ['600px', '450px'],
                    zIndex: 19991231,
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form').find('button[lay-filter=edit]').click();
                    },
                    full: function(elem) {
                        var win = window.top === window.self ? window : parent.window;
                        $(win).on('resize', function() {
                            var $this = $(this);
                            elem.width($this.width()).height($this.height()).css({
                                top: 0,
                                left: 0
                            });
                            elem.children('div.layui-layer-content').height($this.height() - 95);
                        });
                    },
                    success: function(layero, index) {
                        //弹出窗口成功后渲染表单
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var startDate = $("#riskStartSellDate3").val();
                            var endDate = $("#riskEndSellDate3").val();
                            if (startDate > endDate) {
                                alert("截止日期必须在起始日期之后!");
                                return false;
                            };
                            //这里可以写ajax方法提交表单
                            $.ajax({
                                type: "POST",
                                url: "/chl_sync/save",
                                data: "params=" + JSON.stringify(data.field),
                                success: function(data){
                                    console.log(data);
                                    // alert(msg);
                                    if(data.msg == '0000') {
                                        alert('保存成功');
                                        layerTips.close(index);
                                        location.reload(); //刷新
                                    }else {
                                        alert('保存失败, ' + data.msg);
                                        layerTips.close(index);
                                        // location.reload(); //刷新
                                        console.log(data.msg);
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                    end: function() {
                        addBoxIndex = -1;
                    }
                });
                layer.full(addBoxIndex);
            });
        }

        $('#upstatus').on('click', function () {
            var channelCodes = '';
            var proCodes = '';
            var statuss = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "该信息没有录入, 请重新选择!") { //渠道查询不到数据,上架按钮可使用
                    $sbx = "";
                }
                if ($sbx != "") {
                    var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                    if ($cbx) {
                        var n = $that.children('td').eq(2).text();
                        channelCodes += n + ',';
                        var j = $that.children('td').eq(4).text();
                        proCodes += j + ',';
                        var k = $that.children('td').eq(7).text().trim();
                        if (k == '上架') {
                            statuss += k + ',';
                        }
                        // alert(n);
                        record++;
                    }
                }
            });

            //业务更改,当勾选上架产品的时候阻断
            if (statuss.split(",")[0] == '上架') {
                alert("该产品已经上架! ");
                return false;
            }

            // if (record > 1) {
            //     alert("一次只能选择一条产品上架!");
            //     return false;
            // }
            // if (statuss == '上架,') {
            //     alert("该产品已经上架,请重新选择!");
            //     return false;
            // }
            //未选择则作为渠道新增
            // else if (record == '') {
            //     alert("请先选择一条产品上架!");
            //     return false;
            // }

            //业务更改,上架不携带页面勾选数据
            // $.get('/chl_sync/add.html?proCode=' + proCodes + '&channelCode=' + channelCodes , null, function(form) {
            $.get('/chl_sync/add.html' , null, function(form) {
                addBoxIndex = layer.open({
                    type: 1,
                    title: '渠道产品增加',
                    content: form,
                    btn: ['保存', '返回'],
                    shade: false,
                    offset: 'auto',
                    area : ['100%', '100%'],
                    zIndex: 19991231,
                    id: 'lay_ups', //设定一个id，防止重复弹出
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form').find('button[lay-filter=edit]').click();
                    },
                    full: function(elem) {
                        var win = window.top === window.self ? window : parent.window;
                        $(win).on('resize', function() {
                            var $this = $(this);
                            elem.width($this.width()).height($this.height()).css({
                                top: 0,
                                left: 0
                            });
                            elem.children('div.layui-layer-content').height($this.height() - 95);
                        });
                    },
                    success: function(layero, index) {
                        //弹出窗口成功后渲染表单
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var channelCode = $("#channelCode").val();
                            var riskType = $("#riskType").val();
                            var riskCode = $("#riskCode").val();
                            var startDate = $("#riskStartSellDate4").val();
                            var endDate = $("#riskEndSellDate4").val();
                            if (channelCode == '') {
                                // alert("请先选择一条上架渠道!");
                                alert("请先选择渠道!");
                                return false;
                            }
                            if (riskType == '') {
                                alert("请先选择险种类型!");
                                return false;
                            }
                            if (riskCode == '') {
                                alert("请先选择产品!");
                                return false;
                            }
                            if (startDate == '') {
                                alert("请录入起售日期!");
                                return false;
                            }
                            if (endDate == '') {
                                alert("请录入止售日期!");
                                return false;
                            }
                            if (startDate > endDate) {
                                alert("截止日期必须在起始日期之后!");
                                return false;
                            };
                            //这里可以写ajax方法提交表单
                            $.ajax({
                                type: "POST",
                                url: "/chl_sync/addSave",
                                data: "params=" + JSON.stringify(data.field),
                                success: function(data){
                                    if(data.msg == '0000') {
                                        alert('保存成功');
                                        // layerTips.close(index);
                                        // location.reload(); //刷新
                                        layer.closeAll();
                                        $('#search1').click();//刷新页面

                                    }else {
                                        alert('保存失败, ' + data.msg);
                                        console.log(data.msg);
                                        // layerTips.close(index);
                                        // location.reload(); //刷新
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                    end: function() {
                        addBoxIndex = -1;
                    }
                });
                layer.full(addBoxIndex);
            });
        });

        $('#downstatus').on('click', function () {
            var channelCodes = '';
            var proCodes = '';
            var statuss = '';
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                // alert($sbx);
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;

                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    channelCodes += n + ',';
                    var j = $that.children('td').eq(4).text();
                    proCodes += j + ',';
                    var k = $that.children('td').eq(7).text().trim();
                    if (k == '下架') {
                        statuss += k + ',';
                    }
                    record ++;
                    // alert(n);
                }
            });
            if (record == '') {
                alert("请先选择一条产品下架!");
                return false;
            }
            if (statuss.split(",")[0] == '下架') {
                alert("所选产品已经下架,请重新选择! ");
                return false;
            }
            //layer.msg('你选择的名称有：' + channelCodes + "-" + proCodes);
            $.ajax({
                type: "POST",
                url: '/chl_sync/downChlPro',
                data: {proCodes: proCodes, channelCodes: channelCodes},
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('下架成功');
                        // layerTips.close(index);
                        // location.reload(); //刷新
                        $('#search1').click();//刷新页面
                    }else {
                        alert('下架失败, ' + data.msg);
                        console.log(data.msg);
                        // layerTips.close(index);
                        // location.reload(); //刷新
                    }
                }
            });
        });

        $('#allup').on('click', function () {
            layer.open({
                type: 2,   //1.text 2.href
                content: '/chl_sync/allup.html',
                btn: '返回',
                offset: 'auto',
                area : ['100%', '100%'],
                // btnAlign: 'c', //按钮居中
                shade: 0, //不显示遮罩
                zIndex: 19991231,
                id: 'lay_allup', //设定一个id，防止重复弹出
                maxmin: false,
                yes: function(){
                    layer.closeAll();
                },
                end: function() {
                    $('#search1').click();//刷新页面
                }
            });

        });

        $('#alldown').on('click', function () {
            layer.open({
                type: 2,   //1.text 2.href
                content: '/chl_sync/alldown.html',
                btn: '返回',
                offset: 'auto',
                area : ['100%', '100%'],
                // btnAlign: 'c', //按钮居中
                shade: 0, //不显示遮罩
                zIndex: 19991231,
                id: 'lay_alldown', //设定一个id，防止重复弹出
                maxmin: false,
                yes: function(){
                    layer.closeAll();
                },
                end: function() {
                    $('#search1').click();//刷新页面
                }
            });
        });

        $('#exportExcel').on('click', function () {
            var channelCode = $("#channelName2").val();
            var startDate = $("#riskStartSellDate2").val();
            var endDate = $("#riskEndSellDate2").val();

            window.location.href='/chl_sync/excel?channelCode=' + channelCode + '&startDate=' + startDate + '&endDate=' + endDate;
        });

        $('#chlSysc').on('click', function (data) {
            var channelName = $("#channelName").val();
            var index = 0;
            if (channelName == "") {
                if(confirm('同步所有渠道需要较长时间, 是否继续? ')){
                    layer.close(index);
                }else {
                    layer.close(index);
                    return;
                };
            }
            // alert(riskCode + "," + combCode);
            $.ajax({
                type: "POST",
                url: '/chl_sync/chlSysc',
                data: {channelName: channelName},
                dataType: 'json',
                cache: false,
                beforeSend: function() {
                    index = layer.load(0, {
                        shade: [0.5, 'gray'], //0.5透明度的灰色背景
                        content: '数据同步中, 请耐心等待...',
                        success: function (layero) {
                            layero.find('.layui-layer-content').css({
                                'padding-top': '39px',
                                'width': '160px'
                            });
                        }
                    }, {time: 20*1000});
                },
                success: function (data) {
                    // console.log(data);
                    if (data.msg == '0000') {
                        alert('同步成功');
                        // layerTips.close(index);
                        // location.reload(); //刷新
                        $('#search11').click();//刷新页面
                    } else {
                        // alert('同步失败: ' + data.msg);
                        // alert('同步失败');
                        alert('核心无满足查询条件的数据');
                        // layerTips.close(index);
                        console.log(data.msg);
                        // location.reload(); //刷新
                    }
                },
                complete: function () {
                    layer.close(index);
                }
            });

        });
    });
</script>
</body>

</html>