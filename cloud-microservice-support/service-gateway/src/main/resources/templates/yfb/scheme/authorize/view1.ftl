<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>套餐授权</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" >
    <div class=layui-form" style="display: none">
        <div class="layui-input-inline">
            <input id="customerno" value="${customerno}">
        </div>
    </div>

    <fieldset class="layui-elem-field">
        <legend>新增套餐</legend>
        <div class="layui-form" style="float:left; width: 100%;">
            <div class="layui-inline">
                <label class="layui-form-label">套餐编码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" name="schemeCode" id="schemeCode" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline" style="">
                <label class="layui-form-label">套餐名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="schemeName" id="schemeName" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form" style="margin-top: 47px;">
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
                <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>套餐信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>套餐编码</th>
                    <th>套餐名称</th>
                    <th>授权起期</th>
                    <th>授权止期</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>

    <div class="layui-form-item" style="display: flex">
        <div class="layui-form" style="margin-left: 30px;">
            <button id="save" lay-filter="save" class="layui-btn" lay-submit="">
                <i class="" aria-hidden="true"></i> 授权
            </button>
        </div>
    </div>

</div>

<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.selno == undefined ?"":item.selno }}</td>
        <td>{{ item.contschemecode == undefined ?"":item.contschemecode }}</td>
        <td>{{ item.contschemename == undefined ?"":item.contschemename }}</td>
        <td><input class="layui-input" placeholder="请选择日期" name="validateStartDate" id="startDate"
                   onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"></td>
        <td><input class="layui-input" placeholder="请选择日期" name="validateStartDate" id="endDate"
                   onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"></td>

    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/scheme_authorize/schemeList',
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('该查询条件下，没有符合查询条件的结果！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

            },
        });

        $("#reset").click(function () {
            $("#schemeCode").val("");
            $("#schemeName").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var schemeCode = $("#schemeCode").val();
            var schemeName = $("#schemeName").val();

            paging.get({
                "contschemecode": schemeCode,
                "contschemename": schemeName,
                "v": new Date().getTime()
            });
        });


        function cancel(plancode) {
            var customerno = $("#customerno").val();
            // layer.msg(plancode);
            $.ajax({
                type: "POST",
                url: '/scheme_authorize/delete',
                data: "params=" + JSON.stringify({
                    customerno: customerno
                    , plancode: plancode
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('取消成功');
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }else {
                        alert('取消失败, ' + data.msg);
                        console.log(data.msg);
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }
                }
            });
        }

        $('#save').on('click', function () {
            var customerno = $("#customerno").val();
            //alert("----"+customerno);

            var saveArray= new Array();
            var record = '';
            $('#content').children('tr').each(function () {
                var n="";
                var j="";
                var o="";
                var p ="";
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                     n = $that.children('td').eq(2).text();
                    //alert("---"+n);
                     j = $that.children('td').eq(3).text();

                     o =  $that.children('td').eq(4).find('#startDate').val();
                    if (o == "") {
                        alert('请填写授权起期!');
                        return false;
                    }
                    var p =  $that.children('td').eq(5).find('#endDate').val();
                    if (o == "") {
                        alert('请填写授权止期!');
                        return false;
                    }

                    saveArray.push({customerNo: customerno, planCode: n, planName: j,startDate: o,endDate: p});
                    record++;
                }
            });

            if (record == '') {
                alert("请先选择一条信息!");
                return false;
            }
            //alert('3233');
            $.ajax({
                type: "POST",
                url: '/scheme_authorize/bacthsave',
                data: JSON.stringify(saveArray),
                dataType: 'json',
                contentType : 'application/json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('授权成功');
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }else {
                        alert('授权失败, ' + data.msg);
                        console.log(data.msg);
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }
                }
            });
        });



        // (()=>{
        //     // 兼容IE
        //     if(document.all) {
        //         document.getElementById("search1").click();
        //     }
        //     // 兼容其它浏览器
        //     else {
        //         var e = document.createEvent("MouseEvents");
        //         e.initEvent("click", true, true);
        //         document.getElementById("search1").dispatchEvent(e);
        //     }
        // })();


    });
</script>
</body>

</html>