<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>套餐授权</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" >
    <div class=layui-form" style="display: none">
        <div class="layui-input-inline">
            <input id="customerno" value="${item.customerno}">
        </div>
    </div>
    <div class="layui-form" style="display: none">
        <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
            <button id="search1" type="button" class="layui-btn" style="margin-left: 30px;">
                <i class="fa fa-search" aria-hidden="true"></i> 查询
            </button>
        </div>
    </div>
    <fieldset class="layui-elem-field">
        <legend>已授权套餐</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable2">
                <thead>
                <tr>
                    <th>序号</th>
                    <th>套餐编码</th>
                    <th>套餐名称</th>
                    <#--                    <th>授权日期</th>-->
                    <th>授权起期</th>
                    <th>授权止期</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="content1">
                </tbody>
            </table>
        </div>

    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>新增套餐</legend>
        <div class="layui-form" style="float:left; width: 100%;">
            <div class="layui-inline">
                <label class="layui-form-label">套餐编码</label>
                <div class="layui-input-block" style="display:flex">
                    <input type="text" name="schemeCode" id="schemeCode" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline" style="">
                <label class="layui-form-label">套餐名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="schemeName" id="schemeName" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form" style="margin-top: 47px;">
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
                <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>套餐信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>套餐编码</th>
                    <th>套餐名称</th>
                    <th>授权起期</th>
                    <th>授权止期</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>

    <div class="layui-form-item" style="display: flex">
        <div class="layui-form" style="margin-left: 30px;">
            <button id="save" lay-filter="save" class="layui-btn" lay-submit="">
                <i class="" aria-hidden="true"></i> 授权
            </button>
        </div>
    </div>

</div>

<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.selno == undefined ?"":item.selno }}</td>
        <td>{{ item.contschemecode == undefined ?"":item.contschemecode }}</td>
        <td>{{ item.contschemename == undefined ?"":item.contschemename }}</td>
        <td><input class="layui-input" placeholder="请选择日期" name="validateStartDate" id="startDate1"
                   onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"></td>
        <td><input class="layui-input" placeholder="请选择日期" name="validateStartDate" id="endDate1"
                   onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"></td>

    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
            paging = layui.paging(),
            layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
            layer = layui.layer, //获取当前窗口的layer对象
            form = layui.form();

        paging.init({
            openWait: true,
            url: '/scheme_authorize/schemeList',
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('该查询条件下，没有符合查询条件的结果！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

            },
        });

        $("#reset").click(function () {
            $("#schemeCode").val("");
            $("#schemeName").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var schemeCode = $("#schemeCode").val();
            var schemeName = $("#schemeName").val();

            paging.get({
                "contschemecode": schemeCode,
                "contschemename": schemeName,
                "v": new Date().getTime()
            });
        });

        $('#search1').on('click', function (data) {
            var customerno = $("#customerno").val();
            // alert(customerno);

            $.ajax({
                type:'post',
                data: {customerno: customerno},
                dataType:'json',
                url:'/scheme_authorize/haven',
                success: function(data){
                    var dataList = data.havenAuthorize;
                    var error = data.msg;
                    if (error) {
                        $('#content1').html('');
                        return;
                    }
                    if(dataList!=null){
                        var tbody = document.getElementById("content1");
                        $('#content1').html('');
                        //联动框非强制选;默认空串
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var schemeObj = dataList[i];
                            // alert(categoryObj);
                            var id = schemeObj.customerNo;
                            var plancode = schemeObj.planCode;
                            var planname = schemeObj.planName;
                            var startDate = schemeObj.startDate;
                            var endDate = schemeObj.endDate;
                            // var makedate = schemeObj.makedate;
                            var makedate = schemeObj.makeTime;
                            // alert(proName);
                            //进行添加到标签里
                            var row  = document.createElement("tr");
                            var idCell = document.createElement("td");
                            idCell.innerHTML = id;
                            row.appendChild(idCell);

                            var codeCell = document.createElement("td");
                            codeCell.innerHTML = plancode;
                            row.appendChild(codeCell);

                            var codeNameCell = document.createElement("td");
                            codeNameCell.innerHTML = planname;
                            row.appendChild(codeNameCell);

                            // var makedateCell = document.createElement("td");
                            // makedateCell.innerHTML = makedate;
                            // row.appendChild(makedateCell);
                            var startDateCell = document.createElement("td");
                            startDateCell.innerHTML = startDate;
                            row.appendChild(startDateCell);
                            var endDateCell = document.createElement("td");
                            endDateCell.innerHTML = endDate;
                            row.appendChild(endDateCell);

                            //操作
                            var operate = document.createElement("td");
                            operate.innerHTML = "<a href=\"javascript:;\" data-id=" + plancode + " data-opt=\"cancel\" class=\"layui-btn layui-btn-normal layui-btn-mini\">取消</a>";
                            row.appendChild(operate);

                            tbody.appendChild(row);
                        }
                        form.render();
                    }
                },
                error:function(data){
                    alert('系统错误');
                },
                complete: function () { //完成的回调
                    //绑定所有删除按钮事件
                    $('#content1').children('tr').each(function () {
                        var $that = $(this);
                        $that.children('td:last-child').children('a[data-opt=cancel]').on('click', function () {
                            // layer.msg($(this).data('id'));
                            cancel($(this).data('id'));
                        });
                    });
                }
            });

        });

        function cancel(plancode) {
            var customerno = $("#customerno").val();
            // layer.msg(plancode);
            $.ajax({
                type: "POST",
                url: '/scheme_authorize/delete',
                data: "params=" + JSON.stringify({
                    customerno: customerno
                    , plancode: plancode
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('取消成功');
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }else {
                        alert('取消失败, ' + data.msg);
                        console.log(data.msg);
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }
                }
            });
        }

        $('#save').on('click', function () {
            var customerno = $("#customerno").val();
            //alert("----"+customerno);

            var saveArray= new Array();
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                  //alert("---"+n);
                    var j = $that.children('td').eq(3).text();
                    var o =  $that.children('td').eq(4).find('#startDate1').val();
                    // if (o == "") {
                    //     alert('请填写授权起期!');
                    //     return false;
                    // }
                    var p =  $that.children('td').eq(5).find('#endDate1').val();
                    // if (o == "") {
                    //     alert('请填写授权止期!');
                    //     return false;
                    // }

                    saveArray.push({customerNo: customerno, planCode: n, planName: j,startDate: o,endDate: p});
                    record++;
                }
            });
           // alert("2222");
            if (record == '') {
                alert("请先选择一条信息!");
                return false;
            }
            // alert('33333');
            $.ajax({
                type: "POST",
                url: '/scheme_authorize/save',
                data: JSON.stringify(saveArray),
                dataType: 'json',
                contentType : 'application/json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('授权成功');
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }else {
                        alert('授权失败, ' + data.msg);
                        console.log(data.msg);
                        // layerTips.close(index);
                        location.reload(); //刷新
                    }
                }
            });
        });



        (()=>{
            // 兼容IE
            if(document.all) {
                document.getElementById("search1").click();
            }
            // 兼容其它浏览器
            else {
                var e = document.createEvent("MouseEvents");
                e.initEvent("click", true, true);
                document.getElementById("search1").dispatchEvent(e);
            }
        })();


    });
</script>
</body>

</html>