<body>
<div class="layui-form" style="">
    <form class="layui-form-item">
        <div class="layui-inline" style="margin-top: 5px;">
            <label class="layui-form-label">代码类型<font color=red>*</font></label>
            <div class="layui-input-block" style="display:flex">
                <select name="codeType" id="codeType1" class="form-control" lay-filter="funChange" lay-search>
                    <option value="">请选择代码类型</option>
                        <#list laCodeType as LDCode>
                            <option value="${(LDCode.code)!}">${(LDCode.code)!}-${(LDCode.codeName)!}</option>
                        </#list>
                </select>
            </div>
        </div>
        <#--<div class="layui-inline" style="margin-top: 5px;">-->
            <#--<label class="layui-form-label">代码类型<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="codeType1" autocomplete="off" class="layui-input" placeholder="请输入代码类型">-->
            <#--</div>-->
        <#--</div>-->
        <#--<div class="layui-inline" style="margin-top: 2px;">-->
            <#--<label class="layui-form-label">代码类型名称<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="codeTypeName1" autocomplete="off" class="layui-input" placeholder="请输入代码类型名称">-->
            <#--</div>-->
        <#--</div>-->
        <div class="layui-inline" style="margin-top: 1px;">
            <label class="layui-form-label">代码值<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" id="codeValue1" autocomplete="off" class="layui-input" placeholder="请输入代码值">
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 1px;">
            <label class="layui-form-label">代码值名称<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" id="codeValueName1" autocomplete="off" class="layui-input" placeholder="请输入代码值名称">
            </div>
        </div>

        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>
</div>
</body>