<body>
<div class="layui-form" style="">
    <form class="layui-form-item">
        <div class="layui-inline" style="margin-top: 5px;">
            <label class="layui-form-label">渠道代码<font color=red>*</font></label>
            <div class="layui-input-block" style="display:flex">
                <select name="channelCode" id="channelCode2" class="form-control" lay-filter="channelChange" lay-search>
                    <option value="">请选择渠道</option>
                    <#list laChlInfo as ChlInfo>
                        <option value="${(ChlInfo.channelCode)!}">${(ChlInfo.channelCode)!}-${(ChlInfo.channelName)!}</option>
                    </#list>
                </select>
            </div>
        </div>
        <#--<div class="layui-inline" style="margin-top: 5px;">-->
            <#--<label class="layui-form-label">渠道编码<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="channelCode2" autocomplete="off" class="layui-input" placeholder="请输入渠道编码">-->
            <#--</div>-->
        <#--</div>-->
        <div class="layui-inline" style="margin-top: 1px;">
            <label class="layui-form-label">代码类型<font color=red>*</font></label>
            <div class="layui-input-block" style="display:flex">
                <select name="codeType" id="codeType2" class="form-control" lay-filter="codeTypeChange2" lay-search>
                    <option value="">请选择代码类型</option>
                <#list laCodeType as LDCode>
                <option value="${(LDCode.code)!}">${(LDCode.code)!}-${(LDCode.codeName)!}</option>
                </#list>
                </select>
            </div>
        </div>
        <#--<div class="layui-inline" style="margin-top: 5px;">-->
            <#--<label class="layui-form-label">代码类型<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="codeType2" autocomplete="off" class="layui-input" placeholder="请输入代码类型">-->
            <#--</div>-->
        <#--</div>-->
        <#--<div class="layui-inline" style="margin-top: 5px;">-->
            <#--<label class="layui-form-label">代码类型名称<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="codeTypeName2" autocomplete="off" class="layui-input" placeholder="请输入代码类型名称">-->
            <#--</div>-->
        <#--</div>-->
        <div class="layui-inline" style="margin-top: 1px;">
            <label class="layui-form-label">代码值<font color=red>*</font></label>
            <div class="layui-input-block" style="display:flex">
                <select name="codeValue" id="codeValue2" class="form-control" lay-search>
                    <option value="">请选择代码值</option>
                <#--联动-->
                </select>
            </div>
        </div>
        <#--<div class="layui-inline" style="margin-top: 2px;">-->
            <#--<label class="layui-form-label">代码值<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="codeValue2" autocomplete="off" class="layui-input" placeholder="请输入代码值">-->
            <#--</div>-->
        <#--</div>-->
        <#--<div class="layui-inline" style="margin-top: 2px;">-->
            <#--<label class="layui-form-label">代码值名称<font color=red>*</font></label>-->
            <#--<div class="layui-input-inline">-->
                <#--<input type="text" id="codeValueName2" autocomplete="off" class="layui-input" placeholder="请输入代码值名称">-->
            <#--</div>-->
        <#--</div>-->
        <div class="layui-inline" style="margin-top: 1px;">
            <label class="layui-form-label">渠道代码值<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" id="chlCodeValue2" autocomplete="off" class="layui-input" placeholder="请输入渠道代码值">
            </div>
        </div>

        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>
</div>

<script type="text/javascript">
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        form.on('select(codeTypeChange2)', function (data) {
            form.render('select');
            var codeType2 = $("#codeType2").val();

            if (codeType2 == "") {
                var select = document.getElementById("codeValue2");
                var newOption = document.createElement("option");
                newOption.text = "请先选择代码类型!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/code_maintain/linkCodeValue',
                data:{codeType: codeType2},
                success: function(data){
                    var dataList = data.linkCodeValue;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#codeValue2").empty().append("<option value=\"\">请选择代码值</option>");
                        // $("#codeValue2").val("");
                        form.render();
                        return;
                    }
                    if(dataList!=null){
                        $("#codeValue2").empty();
                        var select = document.getElementById("codeValue2");
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            // alert(categoryObj);
                            var code = categoryObj.code;
                            var codeName = categoryObj.codeName;
                            // alert(proName);
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            newOption.text = code + '-' + codeName;
                            // newOption.value = code;
                            newOption.value = code + '-' + codeName;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                },error:function(data){
                    alert('系统错误');
                }
            });

        });

    });

</script>
</body>