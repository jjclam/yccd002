<body>
<div class="layui-form" style="">
    <form class="layui-form-item">
        <div class="layui-inline" style="margin-top: 5px;">
            <label class="layui-form-label">渠道代码<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" name="channelCode" id="channelCode3" disabled="disabled" class="layui-input"
                       value="${item.channelCode?if_exists }">
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">产品代码<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" name="proCode" id="proCode3" disabled="disabled" class="layui-input"
                       value="${item.proCode?if_exists }">
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">短信内容<font color=red>*</font></label>
            <div class="layui-input-inline">
                <#--<input type="textarea" name="smsInfo" id="smsInfo2" autocomplete="off" class="layui-input"-->
                       <#--value="${item.smsInfo?if_exists }" style="width: 584px">-->
                    <textarea id="smsInfo3" class="layui-textarea"
                              style="width: 584px; height: 200px;" value="${item.smsInfo?if_exists }">${item.smsInfo?if_exists}</textarea>
            </div>
        </div>


        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>
</div>
</body>