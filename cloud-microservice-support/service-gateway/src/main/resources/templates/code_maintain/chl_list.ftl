﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>标准代码映射</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">渠道代码</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="channelCode" id="channelCode" class="form-control" lay-filter="channelChange" lay-search>
                            <option value="">请选择渠道</option>
                    <#list laChlInfo as ChlInfo>
                        <option value="${(ChlInfo.channelCode)!}">${(ChlInfo.channelCode)!}-${(ChlInfo.channelName)!}</option>
                    </#list>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">代码类型</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="codeType" id="codeType" class="form-control" lay-filter="codeTypeChange" lay-search>
                            <option value="">请选择代码类型</option>
                        <#--联动-->
                        <#--<#list laCodeType as LDCode>-->
                            <#--<option value="${(LDCode.code)!}">${(LDCode.code)!}-${(LDCode.codeName)!}</option>-->
                        <#--</#list>-->
                        </select>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="margin-top: 2px;">
                <div class="layui-inline">
                    <label class="layui-form-label">代码值</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="codeValue" class="form-control" id="codeValue" lay-search>
                            <option value="">请选择代码值</option>
                        <#--联动-->
                        </select>
                    </div>
                </div>
            </div>

            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
                <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;margin-left: 5px;">
                <button id="add" type="button" class="layui-btn" >
                    <i class="fa fa-plus" aria-hidden="true"></i> 增加
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
                <button id="delete" class="layui-btn">
                    <i class="fa fa-times" aria-hidden="true"></i> 删除
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" >
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>配置信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>渠道</th>
                    <th>代码类型</th>
                    <th>代码值</th>
                    <th>代码值名称</th>
                    <th>渠道代码值</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.id == undefined ?"":item.id }}</td>
        <td>{{ item.otherSign == undefined ?"":item.otherSign }}</td>
        <td>{{ item.codeType == undefined ?"":item.codeType }}</td>
        <td>{{ item.code == undefined ?"":item.code }}</td>
        <td>{{ item.codeName == undefined ?"":item.codeName }}</td>
        <td>{{ item.code1 == undefined ?"":item.code1 }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/chl_maintain/chl_list',

            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });

        $("#reset").click(function () {
            $("#channelCode").val("");
            $("#codeType").empty().append("<option value=\"\">请选择代码类型</option>");
            // $("#codeType").val("");
            $("#codeValue").empty().append("<option value=\"\">请选择代码值</option>");
            // $("#codeValue").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var channelCode = $("#channelCode").val();
            var codeType = $("#codeType").val();
            var codeValue = $("#codeValue").val();

            if (channelCode != "" & codeType == "") {
                alert("请选择代码类型");
                return;
            }
            //暂时注销可查
            // if (codeType != "" & codeValue == "") {
            //     alert("请选择代码值");
            //     return;
            // }

            paging.get({
                "otherSign": channelCode,
                "codeType": codeType,
                "code": codeValue,
                "v": new Date().getTime()
            });
        });

        $('#add').on('click', function () {

            $.get('/chl_maintain/chl_add.html', function(form) {
                layer.open({
                    type: 1,
                    title: '渠道代码映射',
                    content: form,
                    btn: ['提交', '返回'],
                    shade: false,
                    offset: 'auto',
                    area : ['450px', '480px'],
                    zIndex: 19991231,
                    id: 'lay_chl_add',
                    maxmin: false,
                    yes: function(index) {
                        //触发表单的提交事件
                        $('form.layui-form-item').find('button[lay-filter=edit]').click();
                    },
                    success: function(layero, index) {
                        var form = layui.form();
                        form.render();
                        form.on('submit(edit)', function(data) {
                            var channelCode = $("#channelCode2").val();
                            var codeType = $("#codeType2").val();
                            // var codeTypeName = $("#codeTypeName2").val();
                            var codeValue = $("#codeValue2").val();
                            // var codeValueName = $("#codeValueName2").val();
                            var chlCodeValue = $("#chlCodeValue2").val();
                            if (channelCode == '') {
                                alert("请输入渠道代码!");
                                return false;
                            }
                            if (codeType == '') {
                                alert("请输入代码类型!");
                                return false;
                            }
                            // if (codeTypeName == '') {
                            //     alert("请输入代码类型名称!");
                            //     return false;
                            // }
                            if (codeValue == '') {
                                alert("请输入代码值!");
                                return false;
                            }
                            // if (codeValueName == '') {
                            //     alert("请输入代码值名称!");
                            //     return false;
                            // }
                            if (chlCodeValue == '') {
                                alert("请输入渠道代码值!");
                                return false;
                            }

                            $.ajax({
                                type: "POST",
                                url: "/chl_maintain/addChlMaintain",
                                data: "params=" + JSON.stringify({
                                    codeType: codeType
                                    , channelCode: channelCode
                                    // , codeTypeName: codeTypeName
                                    , codeValue: codeValue
                                    // , codeValueName: codeValueName
                                    , chlCodeValue: chlCodeValue
                                }),
                                dataType: 'json',
                                success: function(data){
                                    if(data.msg == '0000') {
                                        alert('添加成功');
                                        // location.reload(); //刷新
                                        layer.closeAll();
                                        $('#search').click();//刷新页面
                                    }else {
                                        alert('添加失败, ' + data.msg);
                                        console.log(data.msg);
                                        // location.reload(); //刷新
                                    }
                                }
                            });
                            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
                        });
                    },
                });
            });
        });

        $('#delete').on('click', function () {
            var codeTypes = '';
            var codeValues = '';
            var deleteObject = '';
            var ChlCodeValues = '';
            var deleteArray= new Array();
            var record = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $sbx = $that.children('td').eq(0).text();
                if ($sbx == "") {
                    alert('请先进行查询!');
                }
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td').eq(2).text();
                    // ChlCodeValues += n + ',';
                    var j = $that.children('td').eq(3).text();
                    // codeTypes += j + ',';
                    var k = $that.children('td').eq(4).text();
                    // codeValues += k + ',';
                    // deleteObject = "channelCode:" + n + ',' + "codeType:" + j + "codeValue:" + k;
                    // deleteObject += n + ',' + j + ',' + k;
                    deleteArray.push({otherSign: n, codeType: j, code: k});
                    record++;
                }
            });
            if (record == '') {
                alert("请先选择一条信息!");
                return false;
            }
            $.ajax({
                type: "POST",
                url: '/chl_maintain/deleteChlMaintain',
                data: JSON.stringify(deleteArray),
                // data: "params=" + JSON.stringify({
                    // record: record,
                    // deleteObject: deleteObject
                    // codeTypes: codeTypes
                    // , codeValues: codeValues
                    // , ChlCodeValues: ChlCodeValues
                // }),
                dataType: 'json',
                contentType : 'application/json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('删除成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('删除失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });

        form.on('select(channelChange)', function (data) {
            form.render('select');
            var channelCode = $("#channelCode").val();

            if (channelCode == "") {
                var select = document.getElementById("channelCode");
                var newOption = document.createElement("option");
                newOption.text = "请先选择渠道";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/chl_maintain/linkCodeType',
                data:{channelCode: channelCode},
                success: function(data){
                    var dataList = data.linkCodeType;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#codeType").empty().append("<option value=\"\">请选择代码类型</option>");
                        $("#codeValue").empty().append("<option value=\"\">请选择代码值</option>");
                        form.render();
                        return;
                    }
                    if(dataList!=null){
                        $("#codeType").empty();
                        var select = document.getElementById("codeType");
                        for(var i=0;i<dataList.length;i++){ //遍历
                            var categoryObj = dataList[i];
                            // alert(categoryObj);
                            var code = categoryObj.code;
                            var codeName = categoryObj.codeName;
                            // alert(proName);
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            newOption.text = code + '-' + codeName;
                            newOption.value = code;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                },error:function(data){
                    alert('系统错误');
                }
            });

        });

        form.on('select(codeTypeChange)', function (data) {
            form.render('select');
            var codeType = $("#codeType").val();
            var channelCode = $("#channelCode").val();

            if (codeType == "") {
                var select = document.getElementById("codeValue");
                var newOption = document.createElement("option");
                newOption.text = "请先选择代码类型!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                // url:'/code_maintain/linkCodeValue',
                url:'/chl_maintain/chlLinkCodeValue',
                data:{codeType: codeType, channelCode: channelCode},
                success: function(data){
                    var dataList = data.chlLinkCodeValue;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#codeValue").empty().append("<option value=\"\">请选择代码值</option>");
                        form.render();
                        return;
                    }
                    if(dataList!=null){
                        $("#codeValue").empty();
                        var select = document.getElementById("codeValue");
                        for(var i=0;i<dataList.length;i++){ //遍历
                            var categoryObj = dataList[i];
                            // alert(categoryObj);
                            var code = categoryObj.code;
                            var codeName = categoryObj.codeName;
                            // alert(proName);
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            newOption.text = code + '-' + codeName;
                            newOption.value = code;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                },error:function(data){
                    alert('系统错误');
                }
            });

        });

    });
</script>
</body>

</html>