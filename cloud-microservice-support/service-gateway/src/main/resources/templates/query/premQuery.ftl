﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>保费统计</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../plugins/layui/css/query.css"/>
    <link rel="stylesheet" href="../css/global.css" media="all"/>
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
    <fieldset class="layui-elem-field">
        <legend>保费统计</legend>
        <div class="layui-form" style="float:left;">

            <div class="layui-form-item" style="margin-top: 2px">
                <label class="query-form-label">套餐编码</label>
                <div class="layui-input-inline layui-unselect">
                <#--<input type="text" name="proCode" id="proCode" autocomplete="off" class="layui-input">-->
                    <select name="proCode" id="proCode" class="form-control" lay-search>
                        <option value="">请选择套餐</option>
                        <option value=""></option>
                    <#list lmRiskSale as ProInfo>
                        <option value="${(ProInfo.proCode)!}">${(ProInfo.proCode)!} —${(ProInfo.proName)!}</option>
                    </#list>
                    </select>
                </div>

                <label class="query-form-label">管理机构</label>
                <div class="layui-input-inline">
                    <select name="managecom" id="managecom" class="form-control" lay-search>
                        <option value="">请选择管理机构</option>
                        <#list manageInfo as mInfo>
                            <option value="${(mInfo.manageCom)!}">${(mInfo.manageCom)!}—${(mInfo.manageName)!}</option>
                        </#list>
                    </select>
                </div>

                <label class="query-form-label">出单网点</label>
                <div class="layui-input-inline">
                    <select name="agentCom" id="agentCom" class="form-control" lay-search>
                        <option value="">请选择出单网点</option>
                        <#list agentComInfo as aInfo>
                            <option value="${(aInfo.agentCom)!}">${(aInfo.agentCom)!}—${(aInfo.agentName)!}</option>
                        </#list>
                    </select>
                </div>

            </div>

            <div class="layui-form-item" style="margin-top: 2px;">
                <label class="query-form-label">保单状态</label>
                <div class="layui-input-inline">
                    <select id="contState" lay-filter="onchangePosition">
                        <option value="">请选择保单状态</option>
                        <option value="0">0-未签发</option>
                        <option value="1">1-已签发</option>
                    </select>
                </div>

                <label class="query-form-label">结算状态</label>
                <div class="layui-input-inline">
                    <select id="settlementStatus" lay-filter="onchangePosition">
                        <option value="">请选择结算状态</option>
                        <option value="0">0-未结算</option>
                        <option value="1">1-已结算</option>
                    </select>
                </div>
                <label class="query-form-label">操作员代码</label>
                <div class="layui-input-inline">
                    <input type="text" name="operatorCode" id="operatorCode" autocomplete="off" class="layui-input">
                </div>

            </div>

            <div class="layui-form-item" style="margin-top: 2px;">
                <label class="query-form-label">投保起始日</label>
                <div class="layui-input-inline" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期"  name="insuranceStartDate" id="insuranceStartDate"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                           value="${item.insuranceStartDate?if_exists }">
                </div>

                <label class="query-form-label">投保终止日</label>
                <div class="layui-input-inline" style="display:flex">
                    <input class="layui-input" placeholder="请选择日期" name="insuranceEndDate" id="insuranceEndDate"
                           onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                           value="${item.insuranceEndDate?if_exists }">
                </div>
            </div>


            <div class="layui-form-mid layui-word-aux" style="margin-top: 10px;">
                <button id="search11" lay-filter="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 10px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>保费统计信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>网点名称</th>
                    <th>套餐代码</th>
                    <th>套餐名称</th>
                    <th>投保日</th>
                    <th>保单张数</th>
                    <th>保费合计</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>
    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.agentName == undefined ?"":item.agentName }}</td>
        <td>{{ item.productCode == undefined ?"":item.productCode }}</td>
        <td>{{ item.productName == undefined ?"":item.productName }}</td>
        <td>{{ item.polapplydate == undefined ?"":item.polapplydate }}</td>
        <td>{{ item.num == undefined ?"":item.num }}</td>
        <td>{{ item.sumPrem == undefined ?"":item.sumPrem }}</td>
    </tr>
    {{# }); }}
</script>

<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/query/premQueryList', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                    // location.reload();
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });


            },
        });

        $("#reset").click(function () {
            $("#proCode").val("");
            $("#managecom").val("");
            $("#agentCom").val("");
            $("#contState").val("");
            $("#settlementStatus").val("");
            $("#operatorCode").val("");
            $("#insuranceStartDate").val("");
            $("#insuranceEndDate").val("");
            form.render();
        });

        $('#search11').on('click', function () {
            var proCode = $("#proCode").val();
            var managecom = $("#managecom").val();
            var agentCom = $("#agentCom").val();
            var contState = $("#contState").val();
            var settlementStatus = $("#settlementStatus").val();
            var operatorCode = $("#operatorCode").val();
            var insuranceStartDate = $("#insuranceStartDate").val();
            var insuranceEndDate = $("#insuranceEndDate").val();

            paging.get({
                "productcode": proCode,
                "managecom": managecom,
                "agentCom": agentCom,
                "signState": contState,
                "settlementStatus": settlementStatus,
                "operator": operatorCode,
                "polapplyStartDate": insuranceStartDate,
                "polapplyEndDate": insuranceEndDate,
                "v": new Date().getTime()
            });
        });


    });
</script>
</body>

</html>