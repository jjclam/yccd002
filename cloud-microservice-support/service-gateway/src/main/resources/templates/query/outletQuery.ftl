﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>出单网点查询</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../plugins/layui/css/query.css">
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
    <fieldset class="layui-elem-field">
        <legend>查询条件</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="query-form-label">出单网点编码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="code" id="code" autocomplete="off" class="layui-input">
                    </div>

                    <label class="query-form-label">出单网点名称</label>
                    <div class="layui-input-inline">
                        <input type="text" name="codeName" id="codeName" autocomplete="off" class="layui-input">
                    </div>

                    <label class="query-form-label">出单网点地址</label>
                    <div class="layui-input-inline">
                        <input type="text" name="address" id="address" autocomplete="off" class="layui-input">
                    </div>
                </div>

                <div class="layui-inline">
                    <label class="query-form-label">出单人编码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="operator" id="operator" autocomplete="off" class="layui-input">
                    </div>

                    <label class="query-form-label">出单人姓名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="operatorName" id="operatorName" autocomplete="off" class="layui-input">
                    </div>
                </div>

                <#--<div class="layui-inline">
                    <label class="query-form-label">联系电话</label>
                    <div class="layui-input-inline">
                        <input type="text" name="phone" id="phone" autocomplete="off" class="layui-input">
                    </div>

                    <label class="query-form-label">联系人</label>
                    <div class="layui-input-inline">
                        <input type="text" name="linkman" id="linkman" autocomplete="off" class="layui-input">
                    </div>
                </div>-->
            </div>

            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px;">
                <button id="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>

            <div class="layui-form-mid layui-word-aux" style="margin-top: -2px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" >
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
        </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>出单网点信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>出单网点编码</th>
                    <th>出单网点名称</th>
                    <th>出单网点地址</th>
                    <th>出单人编码</th>
                    <th>出单人姓名</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>

    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.agentCom == undefined ?"":item.agentCom }}</td>
        <td>{{ item.agentName == undefined ?"":item.agentName }}</td>
        <td>{{ item.address == undefined ?"":item.address }}</td>
        <td>{{ item.operator == undefined ?"":item.operator }}</td>
        <td>{{ item.operatorName == undefined ?"":item.operatorName }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/query/outletQueryList', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                    // location.reload();
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });


            },
        });

        $("#reset").click(function () {
            $("#code").val("");
            $("#codeName").val("");
            $("#address").val("");
            $("#operator").val("");
            $("#operatorName").val("");
            form.render();
        });

        $('#search').on('click', function () {
            var code = $("#code").val();
            var codeName = $("#codeName").val();
            var address = $("#address").val();
            var operator = $("#operator").val();
            var operatorName = $("#operatorName").val();

            paging.get({
                "agentCom": code,
                "agentName": codeName,
                "address": address,
                "operator": operator,
                "operatorName": operatorName,
                "v": new Date().getTime()
            });
        });


    });
</script>
</body>

</html>