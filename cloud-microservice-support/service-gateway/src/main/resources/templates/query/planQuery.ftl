﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>套餐查询</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
    <link rel="stylesheet" href="../plugins/layui/css/query.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
<#--<blockquote class="layui-elem-quote">-->

<#--</blockquote>-->
    <fieldset class="layui-elem-field">
        <legend>套餐查询</legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="query-form-label">套餐编码</label>
                <div class="layui-input-inline layui-unselect">
                <#--<input type="text" name="proCode" id="proCode" autocomplete="off" class="layui-input">-->
                    <select name="proCode" id="proCode" class="form-control" lay-search>
                        <option value="">请选择套餐</option>
                        <option value=""></option>
                    <#list lmRiskSale as ProInfo>
                        <option value="${(ProInfo.proCode)!}">${(ProInfo.proCode)!} —${(ProInfo.proName)!}</option>
                    </#list>
                    </select>
                </div>

                <label class="query-form-label">套餐名称</label>
                <div class="layui-input-inline">
                    <input type="text" name="proName" id="proName" autocomplete="off" class="layui-input">
                </div>

                <label class="query-form-label">是否为标准产品</label>
                <div class="layui-input-inline">
                    <select name="proType1" id="proType1" lay-verify="">
                        <option value="">请选择是否为标准产品</option>
                        <option value="0">是</option>
                        <option value="1">否</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 10px;">
                <button id="search11" lay-filter="search" type="button" class="layui-btn" style="margin-left: 30px;">
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
            </div>
            <div class="layui-form-mid layui-word-aux" style="margin-top: 10px; margin-left: 5px">
                <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                    <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                </button>
            </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>套餐信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>套餐编码</th>
                    <th>套餐名称</th>
                    <th>套餐起售日期</th>
                    <th>套餐停售日期</th>
                    <th>是否为标准产品</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>

        <div class="admin-table-page">
            <div id="paged" class="page">
            </div>
        </div>
    </fieldset>

</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.proCode == undefined ?"":item.proCode }}</td>
        <td>{{ item.proName == undefined ?"":item.proName }}</td>
        <td>{{ item.startDate == undefined ?"":item.startDate }}</td>
        <td>{{ item.overDate == undefined ?"":item.overDate }}</td>
        <td>
            {{# if(item.standard === '0'){ }} <span style="color: green">是</span> {{# } }}
            {{# if(item.standard === '1'){ }} <span style="color: red">否</span> {{# } }}
        </td>
        <td>
            <a href="javascript:;" data-id="{{ item.proCode }}" data-opt="view" class="layui-btn layui-btn-normal layui-btn-mini">详情</a>
        </td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            url: '/query/planQueryList', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function (data) { //获取数据失败的回调
                //alert('获取数据失败')
                if (data.msg == '无满足查询条件的数据！') {
                    alert('该查询条件下，没有符合查询条件的结果！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有编辑按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=edit]').on('click', function () {
                        editForm($(this).data('id'));
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'), $(this).data('idx'));
                    });
                });

                //绑定所有删除按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=del]').on('click', function () {
                        layer.msg($(this).data('id'));
                    });
                });

            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            // layer.msg('你选择的名称有：' + names);
        });

        $("#reset").click(function () {
            $("#proCode").val("");
            $("#proName").val("");
            $("#proType1").val("");
            form.render();
        });

        $('#search11').on('click', function () {
            var proCode = $("#proCode").val();
            var proName = $("#proName").val();
            var proType1 = $("#proType1").val();

            paging.get({
                "proCode": proCode,
                "proName": proName,
                "standard": proType1,
                "v": new Date().getTime()
            });

        });

        $('#import').on('click', function () {
            var that = this;
            var index = layer.tips('只想提示地精准些', that, {tips: [1, 'white']});
            $('#layui-layer' + index).children('div.layui-layer-content').css('color', '#000000');
        });

        function viewForm(proCode) {
            //本表单通过ajax加载 --以模板的形式，当然你也可以直接写在页面上读取
            layer.open({
                type: 2,
                title: '套餐信息',
                content: '/query/planview.html?proCode=' + proCode,
                shade: false,
                btn: '返回',
                offset: 'auto',
                area: ['100%', '100%'],
                zIndex: 19991231,
                id: 'lay_scheme_info',
                maxmin: false,
                yes: function () {
                    layer.closeAll();
                },
                end: function () {
                    $('#search').click();//刷新页面
                }
            });
        };




    });
</script>
</body>

</html>