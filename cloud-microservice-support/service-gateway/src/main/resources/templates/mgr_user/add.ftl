<body>
<div class="layui-form" style="">
    <form class="layui-form-item">
        <div class="layui-inline" style="margin-top: 5px;">
            <label class="layui-form-label">用户账号<font color=red>*</font></label>
            <div class="layui-input-inline">
                <#--                <input type="text" name="userName0" style="display:none"/>-->
                <input type="text" id="userName5" lay-verify="required" autocomplete="off" class="layui-input"
                       placeholder="请输入用户账号">
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">用户姓名<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="text" id="realName5" lay-verify="required" autocomplete="off" class="layui-input"
                       placeholder="请输入用户姓名">
            </div>
        </div>
        <#--<div class="layui-inline" style="margin-top: 2px;">-->
        <#--<label class="layui-form-label">用户类型<font color=red>*</font></label>-->
        <#--<div class="layui-input-inline">-->
        <#--<input type="text" id="userType5" autocomplete="off" class="layui-input" placeholder="请输入用户类型">-->
        <#--</div>-->
        <#--</div>-->
        <#--        <div class="layui-inline" style="margin-top: 2px;">-->
        <#--            <label class="layui-form-label">所属系统<font color=red>*</font></label>-->
        <#--            <div class="layui-input-inline">-->
        <input type="hidden" id="field5" value="2" lay-verify="required" autocomplete="off" class="layui-input"
               placeholder="请输入用户账号">
        <#--                <select id="field5" lay-filter="onchangeGrpPos" lay-verify="required">-->
        <#--                    <option value="2">请输入所属系统</option>-->
        <#--                    <option value="0">0-外勤</option>-->
        <#--                    <option value="1">1-企业HR</option>-->
        <#--                    <option value="2">2-中台</option>-->
        <#--                </select>-->
        <#--            </div>-->
        <#--        </div>-->
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">企业</label>
            <div class="layui-input-inline">
                <#--<input type="text" id="institutions5" autocomplete="off" class="layui-input" placeholder="请输入企业">-->
                <select name="institutions" id="institutions5" class="form-control" lay-search>
                    <option value="">请选择企业</option>
                    <#--                    后期增加下拉框-->
                    <option value="0">0-利安人寿</option>
                    -->
                    <#--联动-->
                </select>
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">网点</label>
            <div class="layui-input-inline">
                <select name="wangbit5" id="wangbit5" lay-verify="required" lay-search>
                    <option value=""></option>
                    <#list lacoms as lacomss>
                        <option value="${(lacomss.name)!}">${(lacomss.name)!}</option>
                    </#list>
                </select>
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <#--<label class="layui-form-label">岗位<font color=red>*</font></label>-->
            <label class="layui-form-label">岗位</label>
            <div class="layui-input-inline">
                <input name="position" style="display:none"/>
                <select name="position" id="position5" lay-search>
                    <option value="">请选择岗位</option>
                    <#list roles as roless>
                        <option value="${(roless.id)!}-${(roless.name)!}">${(roless.id)!}-${(roless.name)!}</option>
                    </#list>
                </select>
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">机构编码</label>
            <div class="layui-input-inline">
                <select name="ldcom5" id="ldcom5" lay-verify="required" lay-search>
                    <option value=""></option>
                    <#list ldcoms as ladomss>
                        <option value="${(ladomss.name)!}">${(ladomss.name)!}</option>
                    </#list>
                </select>
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">指定业务员</label>
            <div class="layui-input-inline">
                <select name="laagent5" id="laagent5" lay-verify="required" lay-search>
                    <option value=""></option>
                    <#list laagents as laagentss>
                        <option value="${(laagentss.name)!}">${(laagentss.name)!}</option>
                    </#list>
                </select>
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">是否使用UKEY</label>
            <div class="layui-input-inline">
                <input name="ukey5" style="display:none"/>
                <input type="text" name="ukey5" id="ukey5" autocomplete="off" class="layui-input"
                       placeholder="请输入是否使用UKEY"
                >

            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">用户描述</label>
            <div class="layui-input-inline">
                <input name="describe" style="display:none"/>
                <input type="text" name="describe5" id="describe5" autocomplete="off" class="layui-input"
                       placeholder="请输入描述"
                >

            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">有效开始日期</label>
            <div class="layui-input-inline">
                <input class="layui-input" placeholder="请选择日期" name="validateStartDate" id="startDate5"
                       onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">

            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">有效结束日期</label>
            <div class="layui-input-inline">
                <input class="layui-input" placeholder="请选择日期" name="validateEndDate" id="endDate5"
                       onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">

            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-inline">
                <input name="email" style="display:none"/>
                <input type="text" name="email" lay-verify="required" id="email5" autocomplete="off" class="layui-input"
                       placeholder="请输入邮箱"
                >
                <#--lay-verify="email">-->
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">手机号</label>
            <div class="layui-input-inline">
                <input type="text" name="telephone" style="display:none"/>
                <input type="text" name="telephone" lay-verify="required" id="telephone5" autocomplete="off"
                       class="layui-input" placeholder="请输入手机"
                >
                <#--lay-verify="phone">-->
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">联系电话</label>
            <div class="layui-input-inline">
                <#--                <input type="text" name="linkedPhone0" style="display:none"/>-->
                <input type="text" name="linkedPhone5" lay-verify="required" id="linkedPhone5" autocomplete="off"
                       class="layui-input" placeholder="请输入联系电话">
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">密码<font color=red>*</font></label>
            <div class="layui-input-inline">
                <#--                <input type="text" name="password0" style="display:none"/>-->
                <input type="password" name="password5" id="password5" lay-verify="required" autocomplete="off"
                       class="layui-input" placeholder="请输入密码">
            </div>
        </div>
        <div class="layui-inline" style="margin-top: 2px;">
            <label class="layui-form-label">确认密码<font color=red>*</font></label>
            <div class="layui-input-inline">
                <input type="password" id="rePassword5" lay-verify="required" autocomplete="off" class="layui-input"
                       placeholder="请再次输入密码">
            </div>
        </div>
        <button lay-filter="edit" lay-submit style="display: none;"></button>
    </form>
</div>

<script type="text/javascript">
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
            paging = layui.paging(),
            layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
            layer = layui.layer, //获取当前窗口的layer对象
            form = layui.form();

        form.on('select(onchangeGrpPos)', function (data) {
            form.render('select');
            var field5 = $("#field5").val();
            // alert(field5);

            if (field5 == "") {
                var select = document.getElementById("institutions5");
                var newOption = document.createElement("option");
                newOption.text = "请先选择所属系统!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/mgr_user/linkedPositions',
                data: {field: field5},
                success: function (data) {
                    var dataList = data.linkedPositions;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#position5").empty().append("<option value=\"\">请先选择岗位</option>");
                        form.render('select');
                        return;
                    }
                    if (dataList != null) {
                        $("#position5").empty();
                        var select = document.getElementById("position5");
                        //联动框非强制选;默认空串
                        var newOption = document.createElement("option");
                        newOption.text = "请选择岗位";
                        newOption.value = "";
                        select.options.add(newOption);
                        for (var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            var code = categoryObj.positionId;
                            var codeName = categoryObj.position;
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            // newOption.text = code + '-' + codeName;
                            newOption.value = code + '-' + codeName;
                            newOption.text = codeName;
                            // newOption.value = codeName;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                }, error: function (data) {
                    alert('系统错误');
                }
            });

            if (field5 == 2) {
                $("#institutions5").empty().append("<option value=\"\">请选择企业</option>");
                form.render('select');
                return;//只有企业HR才需要录入企业信息 + 外勤laagent
            }

            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/mgr_user/linkedInstitutions',
                data: {field: field5},
                success: function (data) {
                    var dataList = data.linkedInstitutions;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#institutions5").empty().append("<option value=\"\">请先选择所属系统</option>");
                        form.render();
                        return;
                    }
                    if (dataList != null) {
                        $("#institutions5").empty();
                        var select = document.getElementById("institutions5");
                        for (var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            var code = categoryObj.customerno;
                            var codeName = categoryObj.grpname;
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            newOption.text = code + '-' + codeName;
                            newOption.value = code;
                            // newOption.value = codeName;
                            // newOption.value = code + '-' + codeName;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }

                }, error: function (data) {
                    alert('系统错误');
                }
            });

        });

    });

</script>
</body>