﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>任务日志查询</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
        <#--<legend>job日志信息查询</legend>-->
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 5px;">
                <label class="layui-form-label">任务编码</label>
                <div class="layui-input-inline">
                    <select name="taskCode" id="taskCode" class="form-control" lay-search>
                        <option value="">请选择</option>
                        <#list jobCode as LDCode>
                            <option value="${(LDCode.code)!}">${(LDCode.code)!}—${(LDCode.codeName)!}</option>
                        </#list>
                    </select>
                </div>

                <label class="layui-form-label">执行状态</label>
                <div class="layui-input-inline">
                    <#--<input type="text" name="executeState" id="executeState" autocomplete="off" class="layui-input"> 1--成功 0--失败-->
                        <select name="executeState" id="executeState" class="form-control" lay-search>
                            <option value="">请选择</option>
                        <#list stateCode as LDCode>
                            <option value="${(LDCode.code)!}">${(LDCode.code)!}—${(LDCode.codeName)!}</option>
                        </#list>
                        </select>
                </div>
              <p>&nbsp;</p>

            <#--<div class="layui-inline">-->
                <div class="layui-form-item" style=" margin-top: 5px;">
                    <div class="layui-inline" >
                    <label class="layui-form-label">任务开始日期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期"  name="executeDate" id="executeDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                               value="${item.executeDate?if_exists }">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">任务结束日期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="finishDate" id="finishDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                               value="${item.finishDate?if_exists }">
                    </div>
                </div>
                </div>



                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 5px;">
                <button id="search11" lay-filter="search" class="layui-btn" lay-submit>
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-top: 5px; margin-left: 5px">
                    <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                        <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                    </button>
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 5px;">
                    <button id="links" class="layui-btn" lay-submit>
                        <a href="http://10.52.200.40:8899/" target="_blank">
                            <i class="fa fa-search" aria-hidden="true">链接日志管理系统</i>
                        </a>
                    </button>
                </div>

    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>日志信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>编码</th>
                    <th>名称</th>
                    <th>开始日期</th>
                    <th>开始时间</th>
                    <th>结束日期</th>
                    <th>结束时间</th>
                    <th>状态</th>
                    <th>结果</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
    </fieldset>
    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>


</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.serialNo == undefined ?"":item.serialNo }}</td>
        <td>{{ item.taskCode == undefined ?"":item.taskCode }}</td>
        <td>{{ item.taskName == undefined ?"":item.taskName }}</td>
        <td>{{ item.executeDate == undefined ?"":item.executeDate }}</td>
        <td>{{ item.executeTime == undefined ?"":item.executeTime }}</td>
        <td>{{ item.finishDate == undefined ?"":item.finishDate }}</td>
        <td>{{ item.finishTime == undefined ?"":item.finishTime }}</td>
        <td>{{ item.executeState == undefined ?"":item.executeState }}</td>
        <td>{{ item.executeResult == undefined ?"":item.executeResult }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/taskinfo/list', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                // alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败');
                if (msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });
            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            layer.msg('你选择的名称有：' + names);
        });

        $("#reset").click(function () {
            $("#serialNo").val("");
            $("#taskCode").val("");
            $("#taskName").val("");
            $("#executeDate").val("");
            $("#executeTime").val("");
            $("#finishDate").val("");
            $("#finishTime").val("");
            $("#executeState").val("");
            $("#executeResult").val("");
            form.render();
        });

        $('#search11').on('click', function () {
            var serialNo = $("#serialNo").val();
            var taskCode = $("#taskCode").val();
            var taskName = $("#taskName").val();
            var executeDate = $("#executeDate").val();
            var executeTime = $("#executeTime").val();
            var finishDate = $("#finishDate").val();
            var finishTime = $("#finishTime").val();
            var executeState = $("#executeState").val();
            var executeResult = $("#executeResult").val();

            paging.get({
                "serialNo": serialNo,
                "taskCode": taskCode,
                "taskName": taskName,
                "executeDate": executeDate,
                "executeTime": executeTime,
                "finishDate": finishDate,
                "finishTime": finishTime,
                "executeState": executeState,
                "executeResult": executeResult,
                "v": new Date().getTime()
            });

        });
    });
</script>
</body>

</html>