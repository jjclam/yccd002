﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/plugins/YFB/layui/css/layui.css" media="all" />
</head>
<body>
<div class="">
    <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
        <legend style="margin-bottom:15px;">投保审核</legend>
    <div class="layui-form-item" id="lccont">
        <div class="layui-inline">
            <label class="layui-form-label">投保单号</label>
            <div class="layui-input-inline">
                <input type="text" id="pcontno" name="number" lay-verify="required|number" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">团体客户号</label>
            <div class="layui-input-inline">
                <input type="text" id="grpCom" name="number" lay-verify="required|number" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label" style="width: 100px">团体客户名称</label>
            <div class="layui-input-inline">
                <input type="text" id="grpComName" name="number" lay-verify="" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-row" style="margin-left: 20px;">
                <button type="button" id="reload_btn" class="layui-btn layui-inline" data-type="reload">查询</button>
        </div>
    </div>


    </fieldset>
</div>
<table class="layui-hide" id="test" lay-filter="test"></table>



<#--<script type="text/html" id="toolbarDemo">-->
    <#--<div class="layui-btn-container">-->
        <#--<button class="layui-btn layui-btn-sm" lay-event="getCheckData">获取选中行数据</button>-->
    <#--</div>-->
<#--</script>-->


<!--模板-->
<script src="/plugins/YFB/layui/layui.js"></script>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>

<script>
    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#test'
            ,url:'/GrpAppentMan/showAppTable2/' //数据接口
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,cols: [[
                {type:'radio'}
                ,{type:'numbers',title:'序号'}
                ,{field:'proposalGrpContNo',  title: '投保单号'}
                ,{field:'appntNo', title: '团体客户号'}
                ,{field:'standbyFlag1',  title: '团体客户名称'}
                ,{field:'polApplyDate', title: '申请日期', sort: true}
                ,{field:'peoples2',  title: '投保人数'}
                ,{field:'nativePrem',  title: '保费'}
                ,{field:'makeDate', title: '保障开始时间'}
                ,{field:'modifyDate', title: '保障结束时间'}
            ]]
            ,page: true
        });

        // 执行搜索，表格重载
        $('#reload_btn').on('click', function () {
            var pcontno = $('#pcontno').val();
            var grpCom = $('#grpCom').val();
            var grpComName = $('#grpComName').val();
            table.reload('test', {
                method: 'post'
                , where: {
                    'proposalGrpContNo': pcontno,
                    'appntNo': grpCom,
                    'standbyFlag1': grpComName,
                }
                , page: {
                    curr: 1
                }
            });
        });
        //监听行单击事件（双击事件为：rowDouble）
        table.on('radio(test)', function(obj){
            var data = obj.data;
            var name = data.proposalGrpContNo;
            //触发事件
            layer.open({
                type: 2,
                skin: 'layui-layer-demo', //样式类名
                title: '人员清单明细',
                closeBtn: 0, //不显示关闭按钮
                anim: 2,
                area: ['1050px', '470px'],
                shadeClose: true, //开启遮罩关闭
                content: '/GrpAppentMan/showAppentTail/'+'?name='+name
            });
            // layer.alert(JSON.stringify(data), {
            //     title: '人员清单信息：'
            // });
            //标注选中样式
            obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        });


    });
</script>




<script type="text/html" id="rank">
    {{d.LAY_TABLE_INDEX+1}}
</script>

</body>
</html>