﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>人员清单信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/plugins/YFB/layui/css/layui.css" media="all" />
</head>
<body>
<div class="">
    <form class="layui-form" action="/GrpAppentMan/savePerpose">
        <input type="text" value="${nameer}" id="nameer">
    <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
        <legend style="margin-bottom:15px;">人员清单信息</legend>
        <table class="layui-hide" id="test" lay-filter="test"></table>
    </fieldset>

    <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
        <legend style="margin-bottom:15px;">投保审核结论</legend>
        <div class="layui-form-item">
            <label class="layui-form-label">审核结论</label>
            <div class="layui-input-block">
                <select name="bannerType" lay-filter="bannerType" id="bannerType" lay-verify="required">
                    <option value="0" >通过</option>
                    <option value="1" selected="">不通过</option>
                </select>
            </div>
        </div>
        <div>
                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">审核意见</label>
                    <div class="layui-input-block">
                        <textarea placeholder="请输入审核意见" class="layui-textarea" name="desc"></textarea>
                    </div>
                </div>
                <div class="layui-form-item layui-form-text" id="teyue" style="display: none">
                    <label class="layui-form-label">特约</label>
                    <div class="layui-input-block">
                        <textarea placeholder="请输入特约内容" class="layui-textarea" name="desc"></textarea>
                    </div>
                </div>
        </div>
    </fieldset>


    <fieldset class="layui-elem-field site-demo-button"  id="jiti" style="margin-top: 30px;  display: none">
        <legend style="margin-bottom:15px;">集体保单信息</legend>
        <div class="layui-form-item" style="margin-top: 2px">
            <label class="layui-form-label" style="width: 100px;padding-bottom: 25px;">管理机构</label>
            <div class="layui-input-inline">
                <select name="agencyType" id="agencyType" lay-search>
                    <option value=""></option>
                    <#list laChlAgencyType as ChlInfo>
                        <option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>
                    </#list>
                </select>
            </div>
            <label class="layui-form-label" style="width: 180px;padding-bottom: 5px;">销售渠道</label>
            <div class="layui-input-inline">
                <select name="agencyType" id="agencyType" lay-search>
                    <option value=""></option>
                    <#list cirlBySe as cirlBySee>
                        <option value="${(cirlBySee.channelName)!}">${(cirlBySee.channelName)!}</option>
                    </#list>
                </select>
            </div>

            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label" style="width: 100px;padding-bottom: 5px;">中介机构编码</label>
                    <div class="layui-input-block" style="display:flex" id="zhongjCode">
                        <input type="text" class="layui-input" id="dname" lay-verify="required" >
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label" style="width: 100px;padding-bottom: 5px;">中介机构名称</label>
                    <div class="layui-input-block" style="display:flex" id="zhongjName">
                        <input type="text" class="layui-input" id="zjjgmc" lay-verify="required">
                    </div>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-inline">&nbsp;&nbsp;&nbsp;
                    <button id="addOne" type="button" class="layui-btn" >
                        <i class="fa fa-plus" aria-hidden="true"></i> 详情
                    </button>&nbsp;&nbsp;&nbsp;
                    <button type="button" class="layui-btn site-demo-active"  lay-submit lay-filter="demo1" id="tijiao">
                        确定
                    </button>&nbsp;&nbsp;&nbsp;
                    <button id="addTwo" type="button" class="layui-btn" >
                        <i class="fa fa-plus" aria-hidden="true"></i> 返回
                    </button>
                </div>
            </div>

        </div>
    </fieldset>


        <#--隐藏时的-->
        <div id="hidden1" lay-filter="hidden1" style="display: none" >
                <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
                    <legend style="margin-bottom:15px;">机构查询条件</legend>
                    <div class="layui-form-item" id="lccont">
                        <div class="layui-inline">
                            <label class="layui-form-label">机构编码</label>
                            <div class="layui-input-inline" style="width: 120px">
                                <input type="text" id="pcontno" name="number" lay-verify="" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 100px;">管理机构</label>
                            <div class="layui-input-inline">
                                <select name="agencyTypelacomss" id="agencyTypelacomss" lay-search>
                                    <option value=""></option>
                                    <#list lacoms as lacomss>
                                        <option value="${(lacomss.name)!}">${(lacomss.name)!}</option>
                                    </#list>
                                </select>
                            </div>
                        </div>
                        <div class="layui-inline">
                            <label class="layui-form-label">机构名称</label>
                            <div class="layui-input-inline" style="width: 120px">
                                <input type="text" id="grpCom" name="number" lay-verify="" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-row" style="margin-left: 20px;">
                            <button type="button" id="reload_btn" class="layui-btn layui-inline" data-type="reload">查询</button>
                        </div>
                    </div>
                </fieldset>
            <table id="department_result" lay-filter="department_result"></table>
        </div>
    </form>
</div>

<!--模板-->
<script src="/plugins/YFB/layui/layui.js"></script>
<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>

<#-- 添加 弹出部门表格的 tool（里面的按钮）-->
<script type="text/html" id="hidden1-table-tool">
  <a class="layui-btn layui-btn-xs" lay-event="select">选择</a>
</script>

<script>
    var nameer = $("#nameer").val();
    form = layui.form;
    layui.config({
        base: '../js/'
    });

    layui.use('table', function(){
        var table = layui.table;
        table.render({
            elem: '#test'
            ,url:'/GrpAppentMan/showAppTable3'+'?nameer='+nameer //数据接口
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,cols: [[
                {type:'radio'}
                ,{field:'peoples2',  title: '投保人数'}
                ,{field:'nativePrem', title: '保费'}
                ,{field:'makeDate',  title: '保障开始时间'}
                ,{field:'modifyDate', title: '保障结束时间'}
            ]]
            ,page: false
        });
    });

    form.on('submit(formDemo)', function (data) {
        alert(1231231);
        console.log(data.field);
        return false;
    });
</script>

<script type="text/javascript">
    // 定义全局变量. employee.js 中要用到
    var $, table, layer, laydate, form;
    var context_path = '/layui-curd';

    layui.config({
        base: context_path + '/layuiadmin/'
    }).extend({
        index : 'lib/index'
    }).use([ 'table', 'layer', 'laydate', 'form' ], function() {

        // part 1: 为全局变量赋值
        $ = layui.$,
                // admin = layui.admin,
                table = layui.table,
                layer = layui.layer,
                laydate = layui.laydate,
                form = layui.form;

        // part 2: 让layui渲染页面
        table.render(department_result_table_options); // 渲染页面上的table. table中 的数据是通过 ajax 请求从后台获取。

        // part 3: 让 layui 为页面的元素绑定事件处理函数
        table.on('tool(department_result)', department_tool_event_handler); // 为页面上的table上的tool绑定事件处理函数

        // part 4: 为输入框绑定光标聚焦事件的触发该函数，
        $('#dname').focus(depart_input_focus_handler);

        //渲染table
        //页面上隐藏的用于弹层的所有客户的列表的相关设置
        var department_result_table_options = {
            elem: '#department_result'
            ,url: '/GrpAppentMan/department'
            ,method : 'post'
            ,title : '部门列表'
            ,cols : [ [
                {field:'agentcom',  title: '机构编码'}
                ,{field:'managecom', title: '管理机构'}
                ,{field:'name',  title: '机构名称'}
                ,{ fixed : 'right', title : '操作', toolbar : '#hidden1-table-tool', width : 80}
                ] ]
            ,page: true
        };


        // 执行搜索，表格重载
        $('#reload_btn').on('click', function () {
            var pcontno = $('#pcontno').val();
            var grpCom = $('#grpCom').val();
            var grpComName = $('select[name="agencyTypelacomss"] option:selected').text();
            table.reload('department_result', {
                method: 'post'
                , where: {
                    'agentcom': pcontno,
                    'name': grpCom,
                    'managecom': grpComName,
                }
                , page: {
                    curr: 1
                }
            });
        });


        // “客户”输入框的光标聚焦事件的触发函数， 弹出弹层，弹层上显示所有的客户，以供选择。
        function depart_input_focus_handler() {
            layer.open({
                type : 1
                , area : [ '950px', '380px' ]
                , content : $('#hidden1')
                , success : function () {
                    // 重新加载表格中的数据
                    table.render(department_result_table_options);
                    $('#hidden1').css('display', 'block');
                }
            });
        }


    });
</script>

<script type="text/javascript">

</script>

<script type="text/javascript">
    //点击弹出的“客户信息”弹层上的表格中“选中”按钮的触发函数
    function department_tool_event_handler(obj) {
        console.info(obj);
        var data = obj.data;
        console.log(data);
        switch (obj.event) {
            case 'select':
                $('#dname').val(data.agentcom);
                $('#zjjgmc').val(data.name);
                layer.close(layer.index);
                break;
        }
    }
</script>

<script type="text/javascript">
    layui.use(['element', 'form', 'jquery'], function () {
//加载element、form、jquery模块
        var form = layui.form  //获取form模块
                , element = layui.element
                , $ = layui.$;
        form.on('select(bannerType)', function (data) {
            //使用layui的form.on绑定select选中事件
            if (data.value == 0) {
                $("#jiti").show();
                $("#teyue").show();
            } else {
                $("#jiti").hide();
                $("#teyue").hide();
            }
        });
    });
</script>


</body>
</html>