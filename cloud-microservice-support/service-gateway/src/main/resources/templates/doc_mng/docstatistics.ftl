﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>单证统计</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 5px;">
                <label class="layui-form-label">单证编码</label>
                <div class="layui-input-inline">
                    <#--<input type="text" name="certifyCode" id="certifyCode" autocomplete="off" class="layui-input">-->
                    <select name="certifyCodes" id="certifyCodes" class="form-control" lay-search>
                    <option value="">请选择</option>
                    <#list certifyCodes as LDCode>
                    <option value="${(LDCode.code)!}">${(LDCode.code)!}</option>
                    </#list>

                    </select>
                </div>



                <label class="layui-form-label">发放者</label>
                <div class="layui-input-inline">
                    <input type="text" name="sendOutCom" id="sendOutCom" autocomplete="off" class="layui-input">
                </div>
                <p>&nbsp;</p>
                <label class="layui-form-label">操作员</label>
                <div class="layui-input-inline">
                    <input type="text" name="operator" id="operator" autocomplete="off" class="layui-input">
                </div>

                <label class="layui-form-label">起始流水号</label>
                <div class="layui-input-inline">
                    <input type="text" name="startNo" id="startNo" autocomplete="off" class="layui-input">
                </div>

                <p>&nbsp;</p>

                <div class="layui-form-item" style=" margin-top: 5px;">
                    <div class="layui-inline" >
                        <label class="layui-form-label">经办日期（开始）</label>
                        <div class="layui-input-block" style="display:flex">
                            <input class="layui-input" placeholder="请选择日期"  name="handleDate" id="handleDate"
                                   onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                                   value="${item.handleDate?if_exists }">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">入机日期（开始）</label>
                        <div class="layui-input-block" style="display:flex">
                            <input class="layui-input" placeholder="请选择日期" name="makeDate" id="makeDate"
                                   onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})"
                                   value="${item.makeDate?if_exists }">
                        </div>
                    </div>
                </div>

                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 5px;">
                    <button id="search11" lay-filter="search" class="layui-btn" lay-submit>
                        <i class="fa fa-search" aria-hidden="true"></i> 查询
                    </button>
                </div>

    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>单证信息</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <#--<th>序号</th>-->
                    <th>单证编码</th>
                    <th>发放者</th>
                    <th>接收者</th>
                    <th>起始流水号</th>
                    <th>终止流水号</th>
                    <th>数量</th>
                    <th>操作员</th>
                    <th>状态</th>
                    <th>入机日期</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
    </fieldset>
    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>


</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <#--<td>{{ item.code == undefined ?"":item.code }}</td>-->
        <td>{{ item.certifyCode == undefined ?"":item.certifyCode }}</td>
        <td>{{ item.sendOutCom == undefined ?"":item.sendOutCom }}</td>
        <td>{{ item.receiveCom == undefined ?"":item.receiveCom }}</td>
        <td>{{ item.startNo == undefined ?"":item.startNo }}</td>
        <td>{{ item.endNo == undefined ?"":item.endNo }}</td>
        <td>{{ item.sumCount == undefined ?"":item.sumCount }}</td>
        <td>{{ item.operator == undefined ?"":item.operator }}</td>
        <td>{{ item.state == undefined ?"":item.state }}</td>
        <td>{{ item.makeDate == undefined ?"":item.makeDate }}</td>

    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/doc_mng/docstatistics', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                    // alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败');
                if (msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });
            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            layer.msg('你选择的名称有：' + names);
        });

        // $("#reset").click(function () {
        //     $("#serialNo").val("");
        //     $("#idNo").val("");
        //     $("#sendUser").val("");
        //     $("#sendRecvr").val("");
        //     $("#startNo").val("");
        //     $("#endNo").val("");
        //     $("#summary").val("");
        //     $("#operator").val("");
        //     $("#enterDate").val("");
        //     form.render();
        // });

        $('#search11').on('click', function () {

            // var serialNo = $("#serialNo").val();
            var certifyCode = $("#certifyCodes").val();
            var sendOutCom = $("#sendOutCom").val();
            var operator = $("#operator").val();
            var startNo = $("#startNo").val();
            // var endNo = $("#endNo").val();
            // var summary = $("#summary").val();
            // var operator = $("#operator").val();
            var makeDate = $("#makeDate").val();
            // alert(certifyCodes);
            // alert(operator);
            paging.get({
                // "serialNo": serialNo,
                "certifyCode": certifyCode,
                "sendOutCom": sendOutCom,
                "operator": operator,
                "startNo": startNo,

                "operator": operator,
                "makeDate": makeDate,
                "v": new Date().getTime()
            });

        });
    });
</script>
</body>

</html>