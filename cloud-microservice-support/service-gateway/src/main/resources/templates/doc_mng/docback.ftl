﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>发放回退信息</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
        <#--<legend>job日志信息查询</legend>-->
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 5px;">
                <label class="layui-form-label">机构</label>
                <div class="layui-input-inline">
                    <select name="executeState" id="executeState" class="form-control" lay-search>
                        <option value="0">请选择</option>
                        <option value="86">86</option>
                        <option value="86">8601</option>
                        <option value="86">860101</option>
                        <option value="86">86010101</option>
                        <option value="86">860101010</option>
                        <option value="86">8611</option>
                        <option value="86">861100</option>
                        <option value="86">86110000</option>
                        <option value="86">86110001</option>
                        <option value="86">86110002</option>
                        <option value="86">86110003</option>
                        <option value="86">86110004</option>
                        <option value="86">86110005</option>
                        <option value="86">86110006</option>
                        <option value="86">86110007</option>
                        <option value="86">86110099</option>
                        <option value="86">861102</option>
                        <option value="86">86110200</option>
                        <option value="86">86110200</option>
                        <option value="86">86110299</option>
                        <option value="86">861103</option>
                        <option value="86">86110300</option>
                        <option value="86">86110300</option>
                        <option value="86">86</option>
                        <option value="86">8601</option>
                        <option value="86">860101</option>
                        <option value="86">86010101</option>
                        <option value="86">860101010</option>
                        <option value="86">8611</option>
                        <option value="86">861100</option>
                        <option value="86">86110000</option>
                        <option value="86">86110001</option>
                        <option value="86">86110002</option>
                        <option value="86">86110003</option>
                        <option value="86">86110004</option>
                        <option value="86">86110005</option>
                        <option value="86">86110006</option>
                        <option value="86">86110007</option>
                        <option value="86">86110099</option>
                        <option value="86">861102</option>
                        <option value="86">86110200</option>
                        <option value="86">86110200</option>
                        <option value="86">86110299</option>
                        <option value="86">861103</option>
                        <option value="86">86110300</option>
                        <option value="86">86110300</option>
                    </select>
                </div>

                <label class="layui-form-label">单证编码</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input"   id="certifycode" value="001">
                </div>

                <p>&nbsp;</p>

                <div class="layui-form-item">
                    <label class="layui-form-label">单证起始号</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input"  value="" id="startno">
                    </div>
                    <label class="layui-form-label">单证终止号</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input"  value="" id="endno">
                    </div>
                </div>


                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 5px;">
                <button id="search11" lay-filter="search" class="layui-btn" lay-submit>
                    <i class="fa fa-search" aria-hidden="true"></i> 单证校验
                </button>
                </div>

                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 5px;">
                    <button id="sendback" lay-filter="search" class="layui-btn" lay-submit>
                        <i class="fa fa-search" aria-hidden="true"></i> 发放回退
                    </button>
                </div>

    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>操作说明</legend>
        <div class="layui-field-box layui-form">
            <ul type="disc">
                <li> &nbsp;&nbsp;&nbsp;&nbsp;* &nbsp;&nbsp; 1、回退的单证必须是未使用的单证</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;* &nbsp;&nbsp;  2、单证回退的操作只能按发放轨迹逆向进行</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;* &nbsp;&nbsp;  3、单证从哪个机构回退，哪个机构就是回退发起者；单子回退到那个机构，那个机构就时回退接收者。例如：需要从A机构回退到B机构，那么A就是回退发起者，B则是回退接收者。发放者接收者信息需要按输入规则录入。</li>
                <li> &nbsp;&nbsp;&nbsp;&nbsp;* &nbsp;&nbsp; 4、回退发起者和回退接收者输入规则：“类型代码”+“机构（用户）代码”</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 类型代码规则如下：</li>
                <li> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A-管理机构  D-代理人</li>
                <li> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;E-代理机构  H-出单用户</li>
            </ul>
        </div>
    </fieldset>
    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>


</div>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();


        $("#sendback").click(function () {

            var executeState = $("#executeState").val();
            var certifycode = $("#certifycode").val();
            var startno = $("#startno").val();
            var endno = $("#endno").val();
            if(executeState == 0){
                alert("机构不能为空！");
                return false;
            }
            $.ajax({
                type: "POST",
                url:"/doc_mng/documentBack/",
                data: "params=" + JSON.stringify({
                    executeState: executeState,
                    certifycode:certifycode,
                    startno:startno,
                    endno:endno
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    console.log(data.msg);
                    if(data.msg == '1') {
                        alert('回退成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('回退失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });

        $('#search11').on('click', function () {
            alert("单证校验通过");
        });
    });
</script>
</body>

</html>