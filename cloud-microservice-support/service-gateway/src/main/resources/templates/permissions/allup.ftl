<head>
    <meta charset="UTF-8">
    <title>渠道产品一键上架</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="margin-bottom: 180px">

    <fieldset class="layui-elem-field">
        <legend>菜单列表</legend>

            <div   style="text-align:center">
                <select id="leftSelect" multiple="multiple" style="height: 250px;width: 250px;">
                <#list menu as SaleProInfo>
                    <option value="${(SaleProInfo.id)!}">${(SaleProInfo.parent)!}——${(SaleProInfo.name)!}</option>
                </#list>
                </select>
                <input type="button" id="right" value=" > " />
                <input type="button" id="rightAll" value=" >>> " />
                <input type="button" id="left" value=" < " />
                <input type="button" id="leftAll" value=" <<< " />
                <select id="rightSelect" multiple="multiple" style="height: 250px;width: 250px;">

                </select>
            </div>

    </fieldset>

    <form class="layui-form">

<#--        <div class="layui-form-item">-->
<#--            <label class="layui-form-label">系统<font color=red>*</font></label>-->
<#--            <div class="layui-input-block" >-->
<#--                <select name="field" id="field" lay-filter="onchangePos">-->
<#--                    <option value="0" <#if (item.field!"") == "0">selected="selected"</#if>>外勤</option>-->
<#--                    <option value="1" <#if (item.field!"") == "1">selected="selected"</#if>>企业HR</option>-->
<#--                    <option value="2" <#if (item.field!"") == "2">selected="selected"</#if>>中台</option>-->
<#--                </select>-->
<#--            </div>-->
<#--        </div>-->
        <input type="hidden"  id="field" name="field"  value="2">
        <div class="layui-form-item">
            <label class="layui-form-label">岗位名称<font color=red>*</font></label>
            <div class="layui-input-block">
                <#--<input type="hidden"  id="id" name="id"  value="${item.id?if_exists }">-->
                <#--<input id="name"  type="text" name="name" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input" value="${item.name?if_exists }">-->

                <#--<input type="hidden"  id="name" name="name"  value="${item.id?if_exists }">-->

                    <select id="position"  class="form-control" lay-search>
                        <option value="">请选择</option>
                        <option value="${(item.id)!}-${(item.name)!}" <#if (item.id!"") >selected="selected"</#if>>${(item.name)!}</option>
                    </select>

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">英文名称<font color=red>*</font></label>
            <div class="layui-input-block">
                <input  id="enname"  type="enname" name="enname" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input" value="${item.enname?if_exists }">
            </div>
        </div>

        <div class="layui-form-item">
                 <#--<label class="layui-form-label">是否启用<font color=red>*</font></label>-->
                 <label class="layui-form-label">状态<font color=red>*</font></label>
                <div class="layui-input-block">
                    <select name="useable" id="useable" lay-verify="">
                        <option value="01" <#if (item.useable!"") == "1">selected="selected"</#if>>启用</option>
                        <option value="02" <#if (item.useable!"") == "2">selected="selected"</#if>>停用</option>
                    </select>
                </div>
        <#--${item.enname?if_exists }-->
        </div>

    </form>

    <div class="layui-form-item" style="display: flex">
        <div class="layui-form" style="margin-left: 30px;">
            <button id="upstatus" lay-filter="save" class="layui-btn" lay-submit>
                <i class="fa fa-level-up" aria-hidden="true"></i> 保存
            </button>
        </div>

    </div>

</div>

<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>

<script>
    layui.config({
        base: '../js/'
    });
    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        // paging.init({
        //     openWait: true,
        //     // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
        //     url: '/permissions/list?v=' + new Date().getTime(), //地址
        //     elem: '#content', //内容容器
        //     params: { //发送到服务端的参数
        //     },
        //     type: 'GET',
        //     tempElem: '#tpl', //模块容器
        //     pageConfig: { //分页参数配置
        //         elem: '#paged', //分页容器
        //         pageSize: 10 //分页大小
        //     },
        //     success: function () { //渲染成功的回调
        //         //alert('渲染成功');
        //     },
        //     fail: function (msg) { //获取数据失败的回调
        //         //alert('获取数据失败')
        //     },
        //     complate: function () { //完成的回调
        //         //alert('处理完成');
        //         //重新渲染复选框
        //         form.render('checkbox');
        //         form.on('checkbox(allselector)', function (data) {
        //             var elem = data.elem;
        //
        //             $('#content').children('tr').each(function () {
        //                 var $that = $(this);
        //                 //全选或反选
        //                 $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
        //                 form.render('checkbox');
        //             });
        //         });
        //
        //     },
        // });
        //获取所有选择的列
        // $('#getSelected').on('click', function () {
        //     var names = '';
        //     $('#content').children('tr').each(function () {
        //         var $that = $(this);
        //         var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
        //         if ($cbx) {
        //             var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
        //             names += n + ',';
        //         }
        //     });
        //     layer.msg('你选择的名称有：' + names);
        // });


        //
        //
        // $('#import').on('click', function () {
        //     var that = this;
        //     var index = layer.tips('只想提示地精准些', that, {tips: [1, 'white']});
        //     $('#layui-layer' + index).children('div.layui-layer-content').css('color', '#000000');
        // });
        //
        // $('#search').on('click', function () {
        //     var channelCode = $("#channelCode").val();
        //     var createStartDate = $("#createStartDate").val();
        //     var createEndDate = $("#createEndDate").val();
        //     // alert(channelCode);
        //     paging.get({
        //         "channelCode": channelCode,
        //         "createStartDate": createStartDate,
        //         "createEndDate": createEndDate,
        //         "v": new Date().getTime()
        //     });
        // });


        form.on('select(onchangePos)', function (data) {
            form.render('select');
            var field = $("#field").val();
            // alert(field);

            if (field == "") {
                var select = document.getElementById("position");
                var newOption = document.createElement("option");
                newOption.text = "请先选择所属系统!";
                newOption.value = "";
                select.options.add(newOption);
            }

            $.ajax({
                type:'post',
                dataType:'json',
                url:'/mgr_user/linkedPositions',
                data:{field: field},
                success: function(data){
                    var dataList = data.linkedPositions;
                    var error = data.msg;
                    // alert(dataList);
                    if (error) {
                        $("#position").empty().append("<option value=\"\">请先选择所属系统</option>");
                        form.render('select');
                        return;
                    }
                    if(dataList!=null){
                        $("#position").empty();
                        var select = document.getElementById("position");
                        //联动框非强制选;默认空串
                        var newOption = document.createElement("option");
                        newOption.text = "请选择岗位";
                        newOption.value = "" ;
                        select.options.add(newOption);
                        for(var i = 0; i < dataList.length; i++) { //遍历
                            var categoryObj = dataList[i];
                            var code = categoryObj.positionId;
                            var codeName = categoryObj.position;
                            //进行添加到标签里
                            var newOption = document.createElement("option");
                            // newOption.text = code + '-' + codeName;
                            newOption.value = code + '-' + codeName;
                            newOption.text = codeName;
                            // newOption.value = code;
                            select.options.add(newOption);
                        }
                        form.render();
                        // form.render('select');
                    }
                },error:function(data){
                    alert('系统错误');
                }
            });

        });

    });

    $('#upstatus').on('click', function () {
        //  alert("你没有选ddd！！");
        var selectedValues = "";
        var rightSelectObject = document.getElementById("rightSelect");
        for(var i=0; i<rightSelectObject.options.length; i++){
            selectedValues = rightSelectObject.options[i].value + ',' + selectedValues;
        }
        selectedValues = selectedValues.substring(0, selectedValues.lastIndexOf(','));
//        if(selectedValues == ""){
//            alert("你没有选择任何选项！！！");
//        }else{
//            alert("你选中的值为：" + selectedValues);
//        }
//         var id = $("#id").val();
        var id = $("#position").val().split("-")[0];
        var name = $("#position").val().split("-")[1];
        // alert(id+name);
        var enname = $("#enname").val();
        // var name = $("#name").val();
        var useable = $("#useable").val();
        var field = $("#field").val();
        if (enname == '') {
            alert("请输入英文名称!");
            return false;
        }
        if (name == '') {
            alert("请输入中文名称!");
            return false;
        }
        if (useable == '') {
            alert("请选择状态!");
            return false;
        }
        if (field == '') {
            alert("请选择系统!");
            return false;
        }
        $.ajax({
            type: "POST",
            url: '/permissions/save',
            data: "params=" + JSON.stringify({
                enname: enname
                , id: id
                , name: name
                , field: field
                , useable: useable
                , selectedValues: selectedValues
                ,type:"edit"
            }),
            dataType: 'json',
            cache: false,
            success: function(data){
                if(data.msg == '0000') {
                    alert('添加成功');
                    // layerTips.close(index);
                    // location.reload(); //刷新
                    layer.closeAll();
                    $('#search').click();
                }else {
                    alert('添加失败, ' + data.msg);
                    console.log(data.msg);
                    // layerTips.close(index);
                    location.reload(); //刷新
                }
            }
        });
    });





    //$(document).ready(function(){
    // 将左边数据移动到右边
    $("#right").click(function(){
       // alert("right");
        //将左边option中选中的值赋给vSelect变量
        var vSelect=$("#leftSelect option:selected");
        //将数据添加到rightSelect中
        vSelect.clone().appendTo("#rightSelect");
        //同时删除leftSelect中的数据
        vSelect.remove();
    });
    //将右边数据移动到左边
    //        $("#left").click(function(){
    $('#left').on('click', function (){
      //  alert("right1");
        var vSelect=$("#rightSelect option:selected");
        //将右边的数据追加到左边列表中
        vSelect.clone().appendTo("#leftSelect");
        vSelect.remove();
    });
    //将左边全部数据移到右边
    $("#rightAll").click(function(){
        $("#rightSelect").append($("#leftSelect>option"));
        $("#leftSelect>option").remove();
    });
    //将右边数据全部移到左边
    $("#leftAll").click(function(){
        $("#leftSelect").append($("#rightSelect>option"));
        $("#rightSelect>option").remove();
    });
    //});

</script>
</body>
