<div style="margin: 15px;">
	<form class="layui-form">

		<div class="layui-form-item">
			<label class="layui-form-label">名称</label>
			<div class="layui-input-block">
				<input type="text" name="name" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input" value="${item.name?if_exists }">
			</div>
		</div>
        <div class="layui-form-item">
                <label class="layui-form-label">链接</label>
                <div class="layui-input-block">
                    <input type="text" name="name" lay-verify="required" placeholder="请输入链接" autocomplete="off" class="layui-input" value="${item.name?if_exists }">
                </div>
         </div>

        <div class="layui-form-item">
                <label class="layui-form-label">图标</label>
                <div class="layui-input-block">
                    <input type="text" name="name" lay-verify="required" placeholder="请输入图标" autocomplete="off" class="layui-input" value="${item.name?if_exists }">
                </div>
       </div>
            <div class="layui-form-item">
                <label class="layui-form-label">上一级</label>
                <div class="layui-input-block">

                    <select id="channelCode" name="channelCode"  class="form-control"  lay-search>
                        <option value="">请选择</option>
					<#list parentMenu as getparentMenu>
                        <option value="${(getparentMenu.id)!}">${(getparentMenu.id)!}-${(getparentMenu.name)!}</option>
					</#list>
                    </select>
                </div>
            </div>
		<div class="layui-form-item">
			<label class="layui-form-label">岗位</label>
			<div class="layui-input-block">

                <select id="channelCode" name="channelCode"  class="form-control"  lay-search>
			       <option value="">请选择</option>
				    <#list role as getrole>
                    <option value="${(getrole.id)!}">${(getrole.id)!}-${(getrole.name)!}</option>
			     	</#list>
                  </select>
			</div>
		</div>


		<button lay-filter="edit" lay-submit style="display: none;"></button>
	</form>
</div>