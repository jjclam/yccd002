﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>套餐定义</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
        <#--<legend>job日志信息查询</legend>-->
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin-top: 5px;">
                <label class="layui-form-label">套餐编码</label>
                <div class="layui-input-inline">
                    <select name="taskCode" id="taskCode" class="form-control" lay-search>
                        <option value="">请选择</option>
                        <#list jobCode as LDCode>
                            <option value="${(LDCode.code)!}">${(LDCode.code)!}—${(LDCode.codeName)!}</option>
                        </#list>
                    </select>
                </div>
                <p>&nbsp;</p>

                <div class="layui-form-mid layui-word-aux" style="margin-left: 30px; margin-top: 5px;">
                <button id="search11" lay-filter="search" class="layui-btn" lay-submit>
                    <i class="fa fa-search" aria-hidden="true"></i> 查询
                </button>
                </div>
                <div class="layui-form-mid layui-word-aux" style="margin-top: 5px; margin-left: 5px">
                    <button id="reset" lay-filter="reset" class="layui-btn" lay-submit>
                        <i class="fa fa-refresh" aria-hidden="true"></i> 重置
                    </button>
                </div>
    </fieldset>

    <fieldset class="layui-elem-field">
        <legend>套餐信息列表</legend>
        <div class="layui-field-box layui-form">
            <table class="layui-table admin-table" id="dataTable">
                <thead>
                <tr>
                    <th style="width: 30px;"><input type="checkbox" lay-filter="allselector" lay-skin="primary"></th>
                    <th>序号</th>
                    <th>套餐编码</th>
                    <th>套餐名称</th>
                </tr>
                </thead>
                <tbody id="content">
                </tbody>
            </table>
        </div>
    </fieldset>
    <div class="admin-table-page">
        <div id="paged" class="page">
        </div>
    </div>


</div>
<!--模板-->
<script type="text/html" id="tpl">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td><input type="checkbox" lay-skin="primary"></td>
        <td>{{ item.serialNo == undefined ?"":item.serialNo }}</td>
        <td>{{ item.taskCode == undefined ?"":item.taskCode }}</td>
        <td>{{ item.taskName == undefined ?"":item.taskName }}</td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>

<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                paging = layui.paging(),
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();

        paging.init({
            openWait: true,
            // url: '/pro_sync/test?v=' + new Date().getTime(), //地址
            url: '/pro_mng/mealdef', //地址
            elem: '#content', //内容容器
            params: { //发送到服务端的参数
            },
            type: 'GET',
            tempElem: '#tpl', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 10 //分页大小
            },
            success: function () { //渲染成功的回调
                // alert('渲染成功');
            },
            fail: function (msg) { //获取数据失败的回调
                //alert('获取数据失败');
                if (msg == '无满足查询条件的数据！') {
                    alert('无满足查询条件的数据！');
                };
            },
            complate: function () { //完成的回调
                //alert('处理完成');
                //重新渲染复选框
                form.render('checkbox');
                form.on('checkbox(allselector)', function (data) {
                    var elem = data.elem;

                    $('#content').children('tr').each(function () {
                        var $that = $(this);
                        //全选或反选
                        $that.children('td').eq(0).children('input[type=checkbox]')[0].checked = elem.checked;
                        form.render('checkbox');
                    });
                });

                //绑定所有预览按钮事件
                $('#content').children('tr').each(function () {
                    var $that = $(this);
                    $that.children('td:last-child').children('a[data-opt=view]').on('click', function () {
                        viewForm($(this).data('id'));
                    });
                });
            },
        });
        //获取所有选择的列
        $('#getSelected').on('click', function () {
            var names = '';
            $('#content').children('tr').each(function () {
                var $that = $(this);
                var $cbx = $that.children('td').eq(0).children('input[type=checkbox]')[0].checked;
                if ($cbx) {
                    var n = $that.children('td:last-child').children('a[data-opt=edit]').data('name');
                    names += n + ',';
                }
            });
            layer.msg('你选择的名称有：' + names);
        });

        $("#reset").click(function () {
            $("#serialNo").val("");
            $("#taskCode").val("");
            $("#taskName").val("");
            $("#executeDate").val("");
            $("#executeTime").val("");
            $("#finishDate").val("");
            $("#finishTime").val("");
            $("#executeState").val("");
            $("#executeResult").val("");
            form.render();
        });

        $('#search11').on('click', function () {
            var serialNo = $("#serialNo").val();
            var taskCode = $("#taskCode").val();
            var taskName = $("#taskName").val();
            var executeDate = $("#executeDate").val();
            var executeTime = $("#executeTime").val();
            var finishDate = $("#finishDate").val();
            var finishTime = $("#finishTime").val();
            var executeState = $("#executeState").val();
            var executeResult = $("#executeResult").val();

            paging.get({
                "serialNo": serialNo,
                "taskCode": taskCode,
                "taskName": taskName,
                "executeDate": executeDate,
                "executeTime": executeTime,
                "finishDate": finishDate,
                "finishTime": finishTime,
                "executeState": executeState,
                "executeResult": executeResult,
                "v": new Date().getTime()
            });

        });
    });
</script>
</body>

</html>