﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>算法定义</title>
<#-- <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
 <link rel="stylesheet" href="../css/global.css" media="all">
 <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
 <link rel="stylesheet" href="../css/table.css"/>-->

    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/plugins/YFB/layui/css/layui.css" media="all" />
    <style>
        .layui-form-item .layui-inline{
            width:300px;
        }
        .layui-table-header{
            width:820px;
        }
        .layui-form-label{
            width:100px;
        }
    </style>
</head>
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>

<body>
<div class="admin-main" style="padding-bottom: 50px">

    <fieldset class="layui-elem-field">
        <legend></legend>
        <div class="layui-form" style="float:left;">
            <div class="layui-form-item" style="margin:0;">
                <label class="layui-form-label" style="width: 90px;">算法编码</label>
                <div class="layui-input-inline">
                    <input type="text" name="calcode" id="calcode" autocomplete="off" class="layui-input">
                </div>
                <button id="searchInfo" type="button" data-type="reload" class="layui-btn" style="margin-left:65px;">
                    <i class="fa fa-plus" aria-hidden="true"></i> 查询
                </button>
            </div>
    </fieldset>

    <table class="layui-hide" id="test" lay-filter="test"></table>

    <div style="margin: 15px;">
        <legend>算法信息</legend>
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">算法编码</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" name="calcode1" maxlength="20" id="calcode1" lay-verify="required" lay-reqtext="算法编码不能为空！" placeholder="请输入" autocomplete="off" class="layui-input">
                        <span style="color: red;">*</span>
                    </div>

                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">算法类型</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" name="calType" maxlength="20" id="calType"  class="layui-input">
                        <span style="color: red;">*</span>
                    </div>
                    <#--<div class="layui-input-block" style="display:flex">-->
                        <#--<select name="calType" id="calType" lay-verify="required" lay-search>-->
                            <#--<option value=""></option>-->
                                <#--<#list laChlAgencyType as ChlInfo>-->
                                    <#--<option value="${(ChlInfo.agencyType)!}">${(ChlInfo.agencyType)!}</option>-->
                                <#--</#list>-->
                        <#--</select>-->
                        <#--<span style="color: red;">*</span>-->
                    <#--</div>-->
                </div>

            </div>

            <div class="layui-form-item">
                <div class="layui-inline" style="width:600px;">
                    <label class="layui-form-label">算法内容</label>
                    <div class="layui-input-block" style="display:flex">
                        <input id="calsql" class="layui-input" maxlength="40" name="calsql" lay-verify="required" ><span style="color: red;">*</span>
                    </div>
                </div>

            </div>
            <div class="layui-form-item">
                <div class="layui-inline" style="width:600px;">
                    <label class="layui-form-label">算法描述</label>
                    <div class="layui-input-block" style="display:flex">
                        <input  class="layui-input" lay-verify="required" maxlength="40" id="remark" name="remark"> <span style="color: red;">*</span>
                    </div>
                </div>

            </div>

            <#--<button id="addOne" type="button" class="layui-btn" >-->
                <#--<i class="fa fa-plus" aria-hidden="true"></i> 上级客户关联-->
            <#--</button>-->
            <#--<button id="addTwo" type="button" class="layui-btn" >-->
                <#--<i class="fa fa-plus" aria-hidden="true"></i> 业务员信息录入-->
            <#--</button>-->
            <button type="button" class="layui-btn site-demo-active" lay-submit lay-filter="demo1" >
                保存
            </button>
        <#--<button lay-filter="edit" lay-submit >上级客户关联</button>
        <button lay-filter="edit" lay-submit >业务员信息录入</button>
        <button lay-filter="edit" lay-submit >保存</button>-->
        </form>
    </div>
</div>
<!--模板-->
<!--模板-->
<script src="/plugins/YFB/layui/layui.js"></script>
<script>
    layui.use(['table','layer'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var index = layer.load(1);

        table.render({
            elem: '#test'
            ,method:'post'
            ,loading:true  //翻页加loading
            ,url:'/GrpClient/listData/' //数据接口
            ,cols: [[    //表头
                {type:'radio'}
                ,{type:'numbers',title:'序号',sort: true,width:80}
                ,{field:'type', title: '算法类型',width:160}
                ,{field:'calCode',  title: '算法编码',width:160}
                ,{field:'calSQL', title: '算法内容',width:200}
                ,{field:'remark',  title: '算法描述',width:200}
            ]]
            ,page: true
            ,done:function (res) {
                layer.close(index);   //返回数据关闭loading
            }
        });

        //根据条件查询数据
        $('#searchInfo').on('click', function(){
            var table = layui.table;
            var calcode = $('#calcode').val();
            // table.reload('test', {
            //     method: 'post'
            //     , where: {
            //         'calcode': calcode,
            //     }
            //     , page: {
            //         curr: 1
            //     }
            // });
            table.reload('test', {
                method: 'post'
                , where: {
                    'CalCode': calcode,
                }
                , page: {
                    curr: 1
                }
            });
        });

        //监听行单击事件（双击事件为：rowDouble）
        table.on('radio(test)', function(obj){
            var data = obj.data;
            console.log(data);
            var calcode1 = data.calCode;
            $("#calcode1").val(calcode1);
            $("#calType").val(data.type);
            $("#calsql").val(data.calSQL);
            $("#remark").val(data.remark);
            /*layer.alert(JSON.stringify(data), {
                title: '当前行数据：'
            });*/
            //标注选中样式
            obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        });
    });
</script>


<script type="text/html" id="rank">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<script>
    //表单监听提交
    layui.use(['form','laydate'], function() {
        var form = layui.form
                ,layer = layui.layer
                ,laydate = layui.laydate;
        //单位证件有效期
        laydate.render({
            elem:'#busliceDate',
            /*value:new Date(),
            done:function(value,obj){

            }*/
        });
        laydate.render({
            elem:'#principalValidate',
            /*value:new Date(),
            done:function(value,obj){
             annuityReceiveDate
            }*/
        });
        laydate.render({
            elem:'#annuityReceiveDate',
            /*value:new Date(),
            done:function(value,obj){

            }*/
        });
        //表单校验
        form.verify({

        });

        //监听提交
        form.on('submit(demo1)', function(data){
            //防止重复提交
            $('.site-demo-active').attr("disabled","disabled");
            var index = layer.load(1);
            /*layer.alert(JSON.stringify(data.field), {
                title: '最终的提交信息'
            });*/
            $.ajax({
                url:"/GrpClient/saveData/",
                async:true,
                type:"post",
                /*dataType:"text",*/
                data:data.field,
                success:function (data) {

                    layer.msg(data.msg);
                    layer.close(index);
                    $('.site-demo-active').removeAttr('disabled');
                }
            });

            return false;
        });
    });

</script>

<script>
    $(window).on('load',function(){});
</script>
<script>

    // //添加上级客户
    // $('#addOne').on('click', function() {
    //     //触发事件
    //     layer.open({
    //         type: 2,
    //         skin: 'layui-layer-demo', //样式类名
    //         title: '上级客户关联',
    //         closeBtn: false, //不显示关闭按钮
    //         btn: '关闭',
    //         anim: 2,
    //         area: ['1000px', '450px'],
    //         shadeClose: true, //开启遮罩关闭
    //         content: '/GrpClient/addClient/'
    //     });
    //     /*$.get('/GrpClient/addClient', null, function(form) {*/
    // });

    // //添加业务员
    // $('#addTwo').on('click', function() {
    //     //触发事件
    //     layer.open({
    //         type: 2,
    //         skin: 'layui-layer-demo', //样式类名
    //         title: '业务员信息录入',
    //         closeBtn: false, //不显示关闭按钮
    //         btn: '关闭',
    //         anim: 2,
    //         area: ['1000px', '450px'],
    //         shadeClose: true, //开启遮罩关闭
    //         content: '/GrpClient/addBM/'
    //     });
    // });
</script>
</body>

</html>