﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>在线出单</title>
    <link rel="stylesheet" href="../plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="../css/global.css" media="all">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/table.css"/>
</head>

<body>
<div class="admin-main" style="padding-bottom: 150px">
    <fieldset class="layui-elem-field">
        <div class="layui-form" style="float:left;">
            <legend>投保人信息</legend>
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">投保人姓名</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input"  id="appntName" name="AppntName" lay-verify="checkName">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">投保人证件类型</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="AppntIDType" id="appntIDType" lay-verify="required" lay-search>
                            <option value=""></option>
                                    <#list idTypeInfo as IDTypeInfo>
                                        <option value="${(IDTypeInfo.code)!}">${(IDTypeInfo.code)!}-${(IDTypeInfo.codeName)!}</option>
                                    </#list>
                        </select>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">投保人证件号码</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="appntIDNo" name="AppntIDNo" lay-verify="required|identity"><span style="color: red;">*</span>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">手机号码</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="mobile" name="Mobile" lay-verify="checkMobile">
                    </div>
                </div>
            </div>
            <legend>被保人信息</legend>
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">与投保人关系</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="Relationship" id="relationship" class="form-control" lay-filter="funChange" lay-search>
                            <option value=""></option>
                                    <#list relationInfo as RelationInfo>
                                        <option value="${(RelationInfo.code)!}">${(RelationInfo.code)!}-${(RelationInfo.codeName)!}</option>
                                    </#list>
                        </select><span style="color: red;">*</span>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" id = "insured" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">被保人姓名</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input"  id="insuredName" name="InsuredName" lay-verify="checkName"><span style="color: red;">*</span>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">被保人证件类型</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="InsuredIDType" id="insuredIDType" lay-verify="required" lay-search>
                            <option value=""></option>
                                    <#list idTypeInfo as IDTypeInfo>
                                        <option value="${(IDTypeInfo.code)!}">${(IDTypeInfo.code)!}-${(IDTypeInfo.codeName)!}</option>
                                    </#list>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">被保人证件号码</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="insuredIDNo" name="InsuredIDNo" lay-verify="required|identity"><span style="color: red;">*</span>
                    </div>
                </div>
            </div>
            <legend>套餐信息</legend>
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">套餐名称</label>
                    <div class="layui-input-block" style="display:flex">
                        <select name="ProdSetCode" id="prodSetCode" class="form-control" lay-filter="productCode" lay-search>
                            <option value=""></option>
                                    <#list proInfo as ProInfo>
                                        <option value="${(ProInfo.proCode)!}">${(ProInfo.proCode)!}-${(ProInfo.proName)!}</option>
                                    </#list>
                        </select>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">单证号</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="prtNo" name="prtNo" lay-verify="required">
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">保单生效日期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="cValidate" id="cValidate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    </div>
                </div>
            </div>
            <div class="layui-form-item" id="travel" style="display: none;" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">旅游日期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="travelDate" id="travelDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">旅游景点</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="travelPlace" name="travelPlace" lay-verify="required">
                    </div>
                </div>
            </div>
            <div class="layui-form-item" id="airplane" style="display: none;" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">航班日期</label>
                    <div class="layui-input-block" style="display:flex">
                        <input class="layui-input" placeholder="请选择日期" name="airDate" id="airDate"
                               onclick="layui.laydate({elem: this, istime: true, format: 'YYYY-MM-DD'})">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">航班时间</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="airTime" name="airTime" lay-verify="required">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">航空公司</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="airCompany" name="airCompany" lay-verify="required">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">航班号</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="airNo" name="airNo" lay-verify="required">
                    </div>
                </div>
            </div>
            <div class="layui-form-item" id="student" style="display: none;" style="margin-top: 10px;">
                <div class="layui-inline">
                    <label class="layui-form-label">学校</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="school" name="school" lay-verify="required">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">班级</label>
                    <div class="layui-input-block" style="display:flex">
                        <input type="text" class="layui-input" id="classroom" name="classroom" lay-verify="required">
                    </div>
                </div>
            </div>
            <div class="layui-form" style="margin-left: 30px;">
                <button id="submitCont"  class="layui-btn" >
                    <i class="button" aria-hidden="true"></i> 确认出单
                </button>
            </div>
        </div>
    </fieldset>
</div>

<script type="text/javascript" src="../plugins/layui/layui.js"></script>
<script type="text/javascript" src="../plugins/layui/lay/dest/layui.all.js"></script>
<script>
    layui.config({
        base: '../js/'
    });

    layui.use(['paging', 'form'], function () {
        var $ = layui.jquery,
                layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
                layer = layui.layer, //获取当前窗口的layer对象
                form = layui.form();


        $('#submitCont').on('click', function () {
            var appntName = $("#appntName").val();
            var appntIDType = $("#appntIDType").val();
            var appntIDNo = $("#appntIDNo").val();
            var mobile = $("#mobile").val();
            var relationship = $("#relationship").val();
            var insuredName = $("#insuredName").val();
            var insuredIDType = $("#insuredIDType").val();
            var insuredIDNo = $("#insuredIDNo").val();
            var prodSetCode = $("#prodSetCode").val();
            var cValidate = $("#cValidate").val();
            var prtNo = $("#prtNo").val();

            if (appntName == '') {
                alert("请录入投保人姓名!");
                return false;
            }
            if (appntIDType == '') {
                alert("请录入投保人证件类型");
                return false;
            }
            if (appntIDNo == '') {
                alert("请录入投保人证件号码");
                return false;
            }
            if (relationship == '') {
                alert("请录入投被保人关系");
                return false;
            }
            if (relationship != '00' && insuredName == '') {
                alert("请录入被保人姓名");
                return false;
            }
            if (relationship != '00' && insuredIDType == '') {
                alert("请录入被保人证件类型");
                return false;
            }
            if (relationship != '00' && insuredIDNo == '') {
                alert("请录入被保人证件号码");
                return false;
            }if (prodSetCode == '') {
                alert("请录入产品信息");
                return false;
            }if (cValidate == '') {
                alert("请录入保单生效日期");
                return false;
            }

            $.ajax({
                type: "POST",
                url: '/newBusiness/saveData/',
                data: "params=" + JSON.stringify({
                    appntName: appntName,
                    appntIDType: appntIDType,
                    appntIDNo: appntIDNo,
                    mobile: mobile,
                    relationship: relationship,
                    insuredName: insuredName,
                    insuredIDType: insuredIDType,
                    insuredIDNo: insuredIDNo,
                    prodSetCode: prodSetCode,
                    cValidate: cValidate,
                    prtNo: prtNo
                }),
                dataType: 'json',
                cache: false,
                success: function(data){
                    if(data.msg == '0000') {
                        alert('出单成功');
                        $('#search').click();//刷新页面
                    }else {
                        alert('出单失败, ' + data.msg);
                        console.log(data.msg);
                    }
                }
            });
        });


        form.on('select(funChange)', function (data) {
            // form.render('select');
            var relationship = $("#relationship").val();
            if(relationship == '00'){
                $("#insured").hide();
            }else{
                $("#insured").show();
            }
        });
        form.on('select(productCode)', function (data) {
            // form.render('select');
            var prodSetCode = $("#prodSetCode").val();
            if(prodSetCode == 'L001'){
                $("#travel").show();
                $("#airplane").hide();
                $("#student").hide();
            }else if(prodSetCode == 'J001'){
                $("#travel").hide();
                $("#airplane").show();
                $("#student").hide();
            }else if(prodSetCode == 'S001'){
                $("#travel").hide();
                $("#airplane").hide();
                $("#student").show();
            }
        });

    });
</script>
</body>

</html>