package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.sinosoft.cloud.dal.dao.model.LCCont;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SubContInfo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.NewBusinessService;
import com.sinosoft.cloud.mgr.service.SettleExcelService;
import com.sinosoft.lis.pubfun.PubFun;
import com.sun.org.apache.bcel.internal.generic.LDC;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

/**
 * 运营管理
 * 保单查询
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/newBusiness")
public class NewBusinessController {
    private final static Log logger = LogFactory.getLog(NewBusinessController.class);
    @Autowired
    private ChannelProService channelProService;
    @Autowired
    private NewBusinessService newBusinessService;
    @Autowired
    private SettleExcelService tSettleExcelService;

    @RequestMapping("/list.html")
    public String core_list(ModelMap model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String admin = "";
        if(session.getAttribute("user") != null){
            admin = (String) session.getAttribute("user");
        }else{
            admin = "test";
        }
        List<ProInfo> proInfo = tSettleExcelService.getProductCode1(admin);
        model.addAttribute("proInfo", proInfo);

        List<LDCode> idTypeInfo = newBusinessService.getIDtype();
        model.addAttribute("idTypeInfo", idTypeInfo);

        List<LDCode> relationInfo = newBusinessService.getRelation();
        model.addAttribute("relationInfo", relationInfo);
        return "/new_business/list";
    }

    @RequestMapping("/saveData")
    @ResponseBody
//    public String saveData(@ModelAttribute SubContInfo subContInfo) {
    public Map<String, Object> saveData(@RequestParam(value = "params") String params, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String opreator = "";
        if(session.getAttribute("user") != null){
            opreator = (String) session.getAttribute("user");
        }else{
            opreator = "test";
        }


        Map<String, Object> map = new HashMap<>();
        String result = newBusinessService.saveCont(params,opreator);
        if (result == SUCCESS.getCode()) {
            map.put("msg", SUCCESS.getCode());
            logger.info("---------出单成功---------");
        } else {
            map.put("msg", result);
            logger.error("---------出单失败---------" + result);
        }
        return map;
    }

}
