package com.sinosoft.cloud.mgr.service.impl;

import com.sinosoft.cloud.dal.dao.mapper.*;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.LAComtoAgent;
import com.sinosoft.cloud.dal.dao.model.LDPLANDUTYPARAM;
import com.sinosoft.cloud.dal.dao.model.entity.SysUser;
import com.sinosoft.cloud.mgr.service.ContBatchService;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.Chl_NESPARAM_NOT_FOUND;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_FAIL;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

@Service
public class ContBatchServiceImpl implements ContBatchService {
    private final static Log logger = LogFactory.getLog(ContBatchServiceImpl.class);
    @Autowired
    private LCContMapper tLCContMapper;
    @Autowired
    private LAComtoAgentMapper tLAComtoAgentMapper;
    @Autowired
    private LDPLANDUTYPARAMMapper tLDPLANDUTYPARAMMapper;
    @Autowired
    private SysUserMapper tSysUserMapper;
    @Autowired
    private LAComMapper tLAComMapper;

    @Override
    public Map<String, Object> importExcel(MultipartFile file, String opreator) {
        try {
            InputStream inputStream = file.getInputStream();
            logger.info("============fileName=============="+file.getOriginalFilename());
            List list = this.readExcel(inputStream, file.getOriginalFilename());
            inputStream.close();
            for (int i = 0; i < list.size(); i++) {
                List<Object> newCont = (ArrayList) list.get(i);
                logger.info("============object=============="+newCont.toString());
                String result = insertCont(newCont,opreator);

                if (result == SUCCESS.getCode()) {
                    logger.info("---------出单成功---------" + newCont.toString());
                } else {
                    logger.error(result + "---------出单失败---------" + newCont.toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, Object> map = new HashMap<>();
        map.put("msg", SUCCESS.getCode());
        logger.info("---------出单成功---------");

        return map;
    }

    private String insertCont(List<Object> newCont,String opreator) {
        Map<String, Object> map = new HashMap<>();
        String appntIDNo = newCont.get(7).toString();
        String appntSex = "";
        String appntBirthDay = "";
        if(appntIDNo == null || "".equals(appntIDNo)){
            logger.info("投保人证件号码为空，请检查");
            return Chl_NESPARAM_NOT_FOUND.getMessage();
        }
        appntSex = PubFun.getSexFromId(appntIDNo);
        appntBirthDay = PubFun.getBirthFromIdNo(appntIDNo);

        String appntName = newCont.get(5).toString();
        String appntIDType = newCont.get(6).toString();
        map.put("APPNTNAME",appntName);
        map.put("APPNTIDTYPE",appntIDType);
        map.put("APPNTIDNO",appntIDNo);
        map.put("APPNTBIRTHDAY",appntBirthDay);
        map.put("APPNTSEX",appntSex);
        map.put("APPNTNO",PubFun.CreateSeq("seq_notify",20));

        String insuredIDNo = newCont.get(12).toString();
        String insuredSex = "";
        String insuredBirthDay = "";
        if(insuredIDNo == null || "".equals(insuredIDNo)){
            logger.info("被保人证件号码为空，请检查");
            return Chl_NESPARAM_NOT_FOUND.getMessage();
        }
        insuredSex = PubFun.getSexFromId(insuredIDNo);
        insuredBirthDay = PubFun.getBirthFromIdNo(insuredIDNo);
        String insuredName = newCont.get(10).toString();
        String insuredIDType = newCont.get(11).toString();

        map.put("INSUREDNAME",insuredName);
        map.put("INSUREDIDTYPE",insuredIDType);
        map.put("INSUREDIDNO",insuredIDNo);
        map.put("INSUREDBIRTHDAY",insuredBirthDay);
        map.put("INSUREDSEX",insuredSex);
        map.put("INSUREDNO",PubFun.CreateSeq("seq_notify",20));

        String productCode = newCont.get(1).toString();
        map.put("PRODSETCODE",productCode);
        map.put("PRODUCTCODE",productCode);

        //根据产品编码从ldplandutyparam中查询名称
        LDPLANDUTYPARAM tLDPLANDUTYPARAM = new LDPLANDUTYPARAM();
        tLDPLANDUTYPARAM.setCONTPLANCODE(productCode);
        List<LDPLANDUTYPARAM> tLDPLANList = tLDPLANDUTYPARAMMapper.selectByContPlan(tLDPLANDUTYPARAM);
        if(tLDPLANList.size()>0){
            map.put("REMARK",tLDPLANList.get(0).getCONTPLANNAME());
        }

        if(opreator == null || "".equals(opreator)){
            opreator = "admin";
        }
        map.put("APPFLAG","1");
        map.put("UWFLAG","9");
        map.put("OPERATOR",opreator);

        map.put("CONTID",PubFun.CreateSeq("seq_sys_logininfor"));
        String contNo = PubFun.CreateSeq("seq_sys_continfo",20);
        map.put("CONTNO",contNo);
//        String prtNo = (String) newCont.get(1);
        map.put("PRTNO",contNo);


        map.put("PROPOSALCONTNO",PubFun.CreateSeq("seq_proposalno_11",12));
        map.put("GRPCONTNO","00000000000000000000");

        //查询usermgr,得到操作用户对应的agentcom
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("userName",opreator);
        List<SysUser> userList = tSysUserMapper.getUserInfoList1(userMap);
        String agentCom = "";
        if(userList.size()>0){
            agentCom = userList.get(0).getRemark();
            map.put("AGENTCOM",agentCom);
        }else{
            agentCom = "000000001";
            map.put("AGENTCOM",agentCom);
        }

        LAComtoAgent tLAComtoAgent = tLAComtoAgentMapper.selectByAgentCom(agentCom);
        if(tLAComtoAgent != null){
            map.put("AGENTCODE",tLAComtoAgent.getAGENTCODE());
        }else{
            map.put("AGENTCODE","3000000001");
        }

        //根据agentcom从lacom里查机构信息
        LACom tLACom = tLAComMapper.selectByAgentCom(agentCom);
        if(tLACom != null){
            map.put("MANAGECOM",tLACom.getMANAGECOM());
            map.put("SIGNCOM",tLACom.getMANAGECOM());
        }else{
            map.put("MANAGECOM","860101");
            map.put("SIGNCOM","860101");
        }

        //查询ldplandutyparam，得到保额保费
        tLDPLANDUTYPARAM.setCALFACTOR("Prem");
        List<LDPLANDUTYPARAM> tPremList = tLDPLANDUTYPARAMMapper.selectByContPlan(tLDPLANDUTYPARAM);
        if(tPremList.size()>0){
            map.put("PREM",tPremList.get(0).getCALFACTORVALUE());
            map.put("SUMPREM",tPremList.get(0).getCALFACTORVALUE());

        }else{
            map.put("PREM","100");
            map.put("SUMPREM","100");
        }

        tLDPLANDUTYPARAM.setCALFACTOR("Amnt");
        List<LDPLANDUTYPARAM> tAmntList = tLDPLANDUTYPARAMMapper.selectByContPlan(tLDPLANDUTYPARAM);
        if(tAmntList.size()>0){
            map.put("AMNT",tAmntList.get(0).getCALFACTORVALUE());
        }else{
            map.put("AMNT","10000");
        }
        String polApplyDate = newCont.get(3).toString();

        map.put("APPROVEDATE",PubFun.getCurrentDate());
        map.put("POLAPPLYDATE",polApplyDate);
        map.put("SIGNDATE",PubFun.getCurrentDate());
        map.put("SIGNTIME",PubFun.getCurrentTime());
        map.put("MAKEDATE",PubFun.getCurrentDate());
        map.put("MODIFYDATE",PubFun.getCurrentDate());
        map.put("MAKETIME",PubFun.getCurrentTime());
        map.put("MODIFYTIME",PubFun.getCurrentTime());

        String cValidate = newCont.get(4).toString();

        map.put("CVALIDATE",cValidate);
        map.put("SETTLEMENTSTATUS","0");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try{
            Date transDate = sdf.parse(cValidate);

            //查询insuredyear和insuredyearflag
            int insuredYear = 1;
            String insuredYearFlag = "D";

            tLDPLANDUTYPARAM.setCALFACTOR("InsuYear");
            List<LDPLANDUTYPARAM> tInsuYearList = tLDPLANDUTYPARAMMapper.selectByContPlan(tLDPLANDUTYPARAM);
            if(tInsuYearList.size()>0){
                insuredYear = Integer.parseInt(tInsuYearList.get(0).getCALFACTORVALUE());
            }
            tLDPLANDUTYPARAM.setCALFACTOR("InsuYearFlag");
            List<LDPLANDUTYPARAM> tInsuYearFlagList = tLDPLANDUTYPARAMMapper.selectByContPlan(tLDPLANDUTYPARAM);
            if(tInsuYearFlagList.size()>0){
                insuredYearFlag = tInsuYearFlagList.get(0).getCALFACTORVALUE();
            }

            Date endDate = PubFun.calDate(transDate,insuredYear,insuredYearFlag);
            map.put("ENDDATE",endDate);
        }catch (Exception e){

        }
        String result = "";
        int insertFlag = tLCContMapper.insertCont(map);
        if (insertFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("出单成功: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("出单失败: " + DB_FAIL.getMessage());
        }

        return result;
    }

    private static List readExcel(InputStream in, String fileName) throws Exception {
        List list = new ArrayList<>();
        //创建Excel工作薄
        Workbook work = getWorkbook(in, fileName);
        if (null == work) {
            throw new Exception("创建Excel工作薄为空！");
        }
        Row row = null;
        Cell cell = null;

        Sheet sheet = work.getSheetAt(0);
        // 获取第一个实际行的下标
        logger.info("======sheet.getFirstRowNum()========="+sheet.getFirstRowNum());
        // 获取最后一个实际行的下标
        logger.info("======sheet.getLastRowNum()========="+sheet.getLastRowNum());
        for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            // 获取在某行第一个单元格的下标
            logger.info("======row.getFirstCellNum()========="+row.getFirstCellNum());
            // 获取在某行的列数
            logger.info("======row.getLastCellNum()========="+row.getLastCellNum());
            if (row == null || row.getFirstCellNum() == i) {
                continue;
            }
            List<Object> li = new ArrayList<>();
            for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
                cell = row.getCell(j);
                li.add(cell);
            }
            list.add(li);
        }
        work.close();//这行报错的话  看下导入jar包的版本
        return list;
    }

    /**
     * 判断文件格式
     *
     * @param inStr
     * @param fileName
     * @return
     * @throws Exception
     */
    private static Workbook getWorkbook(InputStream inStr, String fileName) throws Exception {
        Workbook workbook = null;
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        logger.info("==========fileType========"+fileType);
        if (".xls".equals(fileType)) {
            workbook = new HSSFWorkbook(inStr);
        }
//        else if (".xlsx".equals(fileType)) {
//            workbook = new XSSFWorkbook(inStr);
//        }
        else {
            throw new Exception("请上传excel文件！");
        }
        return workbook;
    }

}
