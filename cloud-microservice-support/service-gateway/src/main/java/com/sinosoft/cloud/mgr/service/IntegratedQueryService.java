package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.entity.*;

import java.util.List;

public interface IntegratedQueryService {
    int contSearchList(ContQueryInfo contInfo);

    List<ContQueryInfo> getContSearchList(ContQueryInfo contInfo, Integer pageIndex, Integer pageSize);

    List<ManageInfo> getManage();


    List<AgentComInfo> getAgentCom();


    int settleSearchList(ContQueryInfo contInfo);


    List<ContQueryInfo> getSettleSearchList(ContQueryInfo contInfo, Integer pageIndex, Integer pageSize);

    List<ProInfo> getProCodeName();

    int productSearchList(ProInfo proInfo);

    List<ProInfo> getProductSearchList(ProInfo proInfo, Integer pageIndex, Integer pageSize);

    List<ProInfo> getPlanCodeName();

    int planSearchList(ProInfo proInfo);

    List<ProInfo> getPlanSearchList(ProInfo proInfo, Integer pageIndex, Integer pageSize);

    int outletSearchList(AgentComInfo lacom);

    List<AgentComInfo> getOutletSearchList(AgentComInfo lacom, Integer pageIndex, Integer pageSize);

    int premSearchList(ContQueryInfo contQueryInfo);

    List<PremQueryInfo> getPremSearchList(ContQueryInfo contQueryInfo, Integer pageIndex, Integer pageSize);

    int documentSearchList(LZCardInfo lzCardInfo);

    List<LZCardInfo> getDocumentSearchList(LZCardInfo lzCardInfo, Integer pageIndex, Integer pageSize);

    List<LMRiskInfo> getLmRiskInfo(String proCode);

}
