package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LdJobRunLogMapper;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LZCardInfo;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;
import com.sinosoft.cloud.mgr.service.DocManageService;
import com.sinosoft.cloud.mgr.service.ElasticJobService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DocManageServiceImpl implements DocManageService {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    LdJobRunLogMapper ldJobRunLogMapper;
            ;
    @Override
    public List<LdJobRunLog> getResultByTaskName(LdJobRunLog ldJobRunLog, Integer pageIndex, Integer pageSize) {
        logger.info("--pageIndex："+pageIndex+"--pageSize："+pageSize);
        logger.info("TaskCode"+ldJobRunLog.getTaskCode());
        logger.info("ExecuteState"+ldJobRunLog.getExecuteState());
        logger.info("ExecuteDate"+ldJobRunLog.getExecuteDate());
        logger.info("FinishDate"+ldJobRunLog.getFinishDate());
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("taskCode", ldJobRunLog.getTaskCode());
        map.put("executeState", ldJobRunLog.getExecuteState());
        map.put("executeDate", ldJobRunLog.getExecuteDate());
        map.put("finishDate", ldJobRunLog.getFinishDate());
        return ldJobRunLogMapper.selectByTaskName(map);
    }

    @Override
    public List<LDCode> getCodes() {

        return ldJobRunLogMapper.getCodes();
    }
    @Override
    public List<LDCode> getStates() {

        return ldJobRunLogMapper.getStates();
    }
    @Override
    public Integer countForList(LdJobRunLog ldJobRunLog) {
        Map<String, Object> map = new HashMap<>();
        map.put("taskCode", ldJobRunLog.getTaskCode());
        map.put("executeState", ldJobRunLog.getExecuteState());
        map.put("executeDate", ldJobRunLog.getExecuteDate());
        map.put("finishDate", ldJobRunLog.getFinishDate());
        return ldJobRunLogMapper.countForList(map);
    }

    @Override
    public String documentBack(String params) {
        String succFlag = "";
        JSONObject po = JSONObject.parseObject(params);
        String executeState = po.getString("executeState");
        String certifycode = po.getString("certifycode");
        String startno = po.getString("startno");
        String endno = po.getString("endno");
        Map<String, Object> map = new HashMap<>();
        map.put("executeState", executeState);
        map.put("certifycode", certifycode);
        map.put("startno", startno);
        map.put("endno", endno);
        try {
            //更新
            int i = ldJobRunLogMapper.updateDocInfo(map);
            logger.info(i);
            //插入
            ldJobRunLogMapper.insertDocTrackInfo(map);
            succFlag = "1";//成功
        }catch (Exception e) {
            succFlag = "0";//失败
            e.printStackTrace();
        }
        return succFlag;
    }

    @Override
    public String documentBackSale(String params) {
        String succFlag = "";
        JSONObject po = JSONObject.parseObject(params);
        String executeState = po.getString("executeState");
        String certifycode = po.getString("certifycode");
        String startno = po.getString("startno");
        String endno = po.getString("endno");
        Map<String, Object> map = new HashMap<>();
        map.put("executeState", executeState);
        map.put("certifycode", certifycode);
        map.put("startno", startno);
        map.put("endno", endno);
        try {
            //更新
            ldJobRunLogMapper.updateDocSaleInfo(map);
            //插入
            ldJobRunLogMapper.insertDocTrackSaleInfo(map);
            succFlag = "1";//成功
        }catch (Exception e) {
            succFlag = "0";//失败
            e.printStackTrace();
        }
        return succFlag;
    }
}
