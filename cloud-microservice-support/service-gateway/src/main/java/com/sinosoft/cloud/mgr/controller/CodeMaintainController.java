package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.CodeMaintainService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;

/**
 * 代码维护
 * 标准代码维护
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/code_maintain")
public class CodeMaintainController {
    private final static Log logger = LogFactory.getLog(CodeMaintainController.class);
    @Autowired
    private CodeMaintainService codeMaintainService;

    @RequestMapping("/core_list.html")
    public String core_list(ModelMap model) {
        List<LDCode> laCodeType = codeMaintainService.getCodeTypeList();
        model.addAttribute("laCodeType", laCodeType);
        return "code_maintain/core_list";
    }

    @RequestMapping("/core_add.html")
    public String core_add(ModelMap model) {
        List<LDCode> laCodeType = codeMaintainService.getCodeTypeList();
        model.addAttribute("laCodeType", laCodeType);
        return "code_maintain/core_add";
    }

    @RequestMapping("/core_list")
    @ResponseBody
    public String coreList(@ModelAttribute LDCode ldCode, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        if (ldCode.getCodeType() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = codeMaintainService.countForCodeMaintainList(ldCode);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }
        List<LDCode> result = codeMaintainService.getCodeMaintainList(ldCode, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LDCode pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping(value = "/addCodeMaintain")
    @ResponseBody
    public Map<String, Object> addCodeMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        try {
            String result = codeMaintainService.addCodeMaintain(po);
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping(value = "/deleteCodeMaintain")
    @ResponseBody
    public Map<String, Object> deleteCodeMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String result = "";
        String codeTypes = po.getString("codeTypes");
        String codeValues = po.getString("codeValues");
        if (StringUtils.isBlank(codeTypes)) {
            logger.error(codeTypes + "没有");
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        } else {
            try {
                result = codeMaintainService.deleteCodeMaintain(codeTypes, codeValues);
                map.put("msg", result);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("系统异常");
                map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
                return map;
            }
            return map;
        }
    }

    @RequestMapping("/linkCodeValue")
    @ResponseBody
    public Map<String, Object> linkCodeValue(@RequestParam String codeType) {
        Map<String, Object> map = new HashMap<>();
        List<LDCode> list = codeMaintainService.getAllCodeValueInfo(codeType);
        logger.info(list);
        //如果代码类型没有对应的产品
        if (list.size() == 0) {
            map.put("msg", CM_LINKCODEVALUE_NOT_EXIST.getMessage());
        }
        map.put("linkCodeValue", list);
        return map;
    }

}
