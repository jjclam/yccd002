package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LDCodeMapper;
import com.sinosoft.cloud.dal.dao.mapper.LdDocStatisitcsMapper;
import com.sinosoft.cloud.dal.dao.mapper.LdJobRunLogMapper;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LZCard;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;
import com.sinosoft.cloud.mgr.service.DocManageService;
import com.sinosoft.cloud.mgr.service.DocStatisticsService;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DocStatisticsServiceImpl implements DocStatisticsService {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    LdDocStatisitcsMapper ldDocStatisitcsMapper;
            ;
    @Override
    public List<LZCard> getResultByTaskName(LZCard lzCard, Integer pageIndex, Integer pageSize) {
        logger.info("--pageIndex："+pageIndex+"--pageSize："+pageSize);

        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("certifyCode",lzCard.getCertifyCode());
        map.put("startNo",lzCard.getStartNo());
        map.put("endNo", lzCard.getEndNo());
        map.put("operator", lzCard.getOperator());
        map.put("sendOutCom", lzCard.getSendOutCom());
        return ldDocStatisitcsMapper.selectByTaskName(map);
    }

    @Override
    public List<LDCode> getCodes() {

        return ldDocStatisitcsMapper.getCodes();
    }
    @Override
    public List<LZCard> getStates() {

        return ldDocStatisitcsMapper.getStates();
    }
    @Override
    public Integer countForList(LZCard lzCard) {
        Map<String, Object> map = new HashMap<>();
        map.put("certifyCode",lzCard.getCertifyCode());
        map.put("startNo",lzCard.getStartNo());
        map.put("endNo", lzCard.getEndNo());
        map.put("operator", lzCard.getOperator());

        return ldDocStatisitcsMapper.countForList(map);
    }
}
