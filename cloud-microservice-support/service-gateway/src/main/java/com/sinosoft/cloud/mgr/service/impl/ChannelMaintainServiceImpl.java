package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LDCode1Mapper;
import com.sinosoft.cloud.dal.dao.model.LDCode1;
import com.sinosoft.cloud.mgr.service.ChannelMaintainService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;

@Service
public class ChannelMaintainServiceImpl implements ChannelMaintainService {
    private final static Log logger = LogFactory.getLog(ChannelMaintainServiceImpl.class);
    @Autowired
    private LDCode1Mapper ldCode1Mapper;

    @Override
    public int countForChlMaintainList(LDCode1 ldCode1) {
        Map<String, Object> map = new HashMap<>();
        map.put("codeType", ldCode1.getCodeType());
        map.put("otherSign", ldCode1.getOtherSign());
        map.put("code", ldCode1.getCode());
        return ldCode1Mapper.countForChlCodeMaintainList(map);
    }

    @Override
    public List<LDCode1> getChlMaintainList(LDCode1 ldCode1, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("codeType", ldCode1.getCodeType());
        map.put("otherSign", ldCode1.getOtherSign());
        map.put("code", ldCode1.getCode());
        return ldCode1Mapper.selectForChlCodeMaintainList(map);
    }

    @Override
    public String addChlMaintain(JSONObject po) {
        String channelCode = po.getString("channelCode");
        String codeType = po.getString("codeType");
        String codeTypeName = po.getString("codeTypeName");
        String chlCodeValue = po.getString("chlCodeValue");
//        String codeValue = po.getString("codeValue");
        String codeValue = po.getString("codeValue").split("-")[0];
//        String codeValueName = po.getString("codeValueName");
        String codeValueName = po.getString("codeValue").split("-")[1];

        if (channelCode == "" || codeType == "" || chlCodeValue == "" || codeValue == "" || codeValueName == "") {
            logger.error(PARAM_NOT_FOUND.getMessage());
            return PARAM_NOT_FOUND.getMessage();
        }

        int addbasecodetypeOk = 0;
        String result = "";
        //判断中台是否配置过渠道基础代码解释; 若没有配置 ,先配置
//        int firstFlag = ldCode1Mapper.verifybasecodetype(codeType);
//        if (firstFlag == 0) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("code", codeType);
//            map.put("codeName", codeTypeName);
//            addbasecodetypeOk = ldCode1Mapper.insertbasecodetype(map);
//            if (addbasecodetypeOk == 1) {
//                logger.info("chlbasecodetypeSuccessfully: " + SUCCESS.getMessage());
//            } else {
//                result = DB_FAIL.getMessage();
//                logger.info("chlbasecodetypeUnsuccessfully: " + DB_FAIL.getMessage());
//                return result;
//            }
//        }

        Map<String, Object> map = new HashMap<>();
        map.put("otherSign", channelCode);
        map.put("codeType", codeType);
        map.put("code", codeValue);
        //判断中台是否配置过渠道代码映射;
//        String result = "";
        int flag = ldCode1Mapper.verifychlcodetype(map);
        if (flag == 1) {
            result = CM_CHLCODELINKED_EXIST.getMessage();
            logger.info("addChlCodeMaintain: " + CM_CHLCODELINKED_EXIST.getMessage());
            return result;
        }

        //判断中台; 该渠道下 该代码类型下的代码值映射是否已经使用过
        map.put("code1", chlCodeValue);
        int refFlag = ldCode1Mapper.verifychlcodetypeRef(map);
        if (refFlag >= 1) {
            result = CM_CHLCODELINKEDNAME_EXIST.getMessage();
            logger.info("addChlCodeMaintain: " + CM_CHLCODELINKEDNAME_EXIST.getMessage());
            return result;
        }

        map.put("codeName", codeValueName);
        logger.info("addChlCodeMaintain(map):" + map);
        int addchlcodetypeOk = ldCode1Mapper.addChlCodeMaintain(map);
        if (addchlcodetypeOk == 1) {
            result = SUCCESS.getCode();
            logger.info("addChlCodeMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("addChlCodeMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    public String deleteChlMaintain(List<LDCode1> deleteArray) {
        String result = "";
        Map<String, Object> params = new HashMap<>();
        for (LDCode1 pc : deleteArray) {
            params.put("channelCode", pc.getOtherSign());
            params.put("codeType", pc.getCodeType());
            params.put("codeValue", pc.getCode());
            int flag = ldCode1Mapper.deleteChlCodeMaintain(params);
            if (flag == 1) {
                result = SUCCESS.getCode();
                logger.info("deleteChlCodeMaintain: " + SUCCESS.getMessage());
            } else {
                result = DB_FAIL.getMessage();
                logger.info("deleteChlCodeMaintain: " + DB_FAIL.getMessage());
            }
        }
        return result;
    }

    @Override
    public List<LDCode1> getAllCodeTypeInfo(String channelCode) {
        return ldCode1Mapper.getAllCodeTypeInfo(channelCode);
    }

    @Override
    public List<LDCode1> getAllCodeValueInfo(String codeType, String channelCode) {
        return ldCode1Mapper.getAllCodeValueInfo(codeType, channelCode);
    }
}
