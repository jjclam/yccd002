package com.sinosoft.cloud.liAnlife;

import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.entity.Laagent;

import java.util.List;

public interface LianLifeService {

    public List<Lacom> selectwangBitSelect();

    public List<Lacom> selectManagecom();

    public List<Lacom> selectManAgeComSelect();

    List<Laagent> selectAgentSelect();
}
