package com.sinosoft.cloud.mgr.common;


import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class XMLStringTwo {

    public static List<LDGrpDemo> xmlElements(String xmlDoc){
        //创建一个新的字符串
        StringReader read = new StringReader(xmlDoc);
        //创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入
        InputSource source = new InputSource(read);
        //创建一个新的SAXBuilder
        SAXBuilder sb = new SAXBuilder();
        List<LDGrpDemo> ldGrps = new ArrayList<>();

        try {
            Document doc = sb.build(source);
            //取的根元素
            Element root = doc.getRootElement();
            System.out.println(root.getName());//输出根元素的名称（测试）
            //得到根元素所有子元素的集合
            List jiedian = root.getChildren();
            Element element = null;
            Element elementTwo = null;

                element = (Element)jiedian.get(0);
                LDGrpDemo ldGrpDemo = new LDGrpDemo();
                ldGrpDemo.setFlag(element.getChild("Flag").getText());
                ldGrpDemo.setDesc(element.getChild("Desc").getText());
                ldGrps.add(ldGrpDemo);

            elementTwo = (Element)jiedian.get(1);
            LDGrpDemo ldGrpDemoTwo = new LDGrpDemo();
            ldGrpDemoTwo.setCustomerno(elementTwo.getChild("CustomerNo").getText());
            ldGrpDemoTwo.setGrpname(elementTwo.getChild("GrpName").getText());
            ldGrpDemoTwo.setGrpnature(elementTwo.getChild("GrpNature").getText());
            ldGrpDemoTwo.setBusinesstype(elementTwo.getChild("BusinessType").getText());
            ldGrps.add(ldGrpDemoTwo);


        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ldGrps;
    }


    public static void main(String[] args){
        String resp = "<?xml version=\"1.0\" encoding=\"GBK\"?>" +
                "<TranData>" +
                "  <Head>" +
                "    <Flag>0</Flag>" +
                "    <Desc>交易成功！</Desc>" +
                "  </Head>" +
                "  <Body>" +
                "  <CustomerNo>0000001948</CustomerNo>  <!--投保单位客户号 -->" +
                "  <GrpName>团体0000001948</GrpName>   <!--单位名称 -->" +
                "  <GrpNature>300</GrpNature>    <!--单位属性 -->" +
                "  <BusinessType>83</BusinessType>   <!--行业类别 -->" +
                "  </Body>" +
                "</TranData>";

        List<LDGrpDemo> ldGrps = xmlElements(resp);
        for (LDGrpDemo ldGrp:ldGrps){
            System.out.println(ldGrp.getCustomerno());
        }

    }

}
