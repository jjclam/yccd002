package com.sinosoft.cloud.mgr.controller;

import com.sinosoft.cloud.dal.dao.model.entity.SettleInfo;
import com.sinosoft.cloud.mgr.common.ObjectExcelView;
import com.sinosoft.cloud.mgr.service.ContBatchService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 运营管理
 * 保单查询
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/contBatch")
public class ContBatchController {
    private final static Log logger = LogFactory.getLog(ContBatchController.class);
    @Autowired
    private ContBatchService contBatchService;


    @RequestMapping("/list.html")
    public String core_list(ModelMap model, HttpServletRequest request) {

        return "/cont_batch/list";
    }

    @RequestMapping("/importData")
    @ResponseBody
    public Map<String, Object> importData(@RequestPart(value = "file") MultipartFile file, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String opreator = "";
        if(session.getAttribute("user") != null){
            opreator = (String) session.getAttribute("user");
        }else{
            opreator = "test";
        }


        return contBatchService.importExcel(file,opreator);
    }

    /**导出到excel
     * @param
     * @throws Exception
     */
    @RequestMapping(value="/exportExcel")
    public ModelAndView exportExcel(@ModelAttribute SettleInfo settleInfo) throws Exception{
        ModelAndView mv = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<>();
        List<String> titles = new ArrayList<>();
        titles.add("序号"); //1
        titles.add("套餐编码"); //2
        titles.add("套餐名称"); //3
        titles.add("投保日期"); //4
        titles.add("保单生效日期"); //5
        titles.add("投保人姓名"); //6
        titles.add("投保人证件类型"); //7
        titles.add("投保人证件号码"); //8
        titles.add("投保人电话"); //9
        titles.add("投被保人关系"); //10
        titles.add("被保人姓名"); //11
        titles.add("被保人证件类型"); //12
        titles.add("被保人证件号码"); //13
        dataMap.put("titles", titles);

        List<Map> varList = new ArrayList<>();
        Map<String, Object> vpd = new HashMap();
        vpd.put("var1", "");        //1
        vpd.put("var2", "");    //2
        vpd.put("var3", ""); //3
        vpd.put("var4", ""); //4
        vpd.put("var5", ""); //5
        vpd.put("var6", ""); //6
        vpd.put("var7", ""); //7
        vpd.put("var8", ""); //8
        vpd.put("var9", ""); //9
        vpd.put("var10", ""); //10
        vpd.put("var11", ""); //11
        vpd.put("var11", ""); //12
        vpd.put("var11", ""); //13

        varList.add(vpd);
        dataMap.put("varList", varList);

        ObjectExcelView erv = new ObjectExcelView();
        mv = new ModelAndView(erv, dataMap);
        logger.info("---------EXCEL_OVER---------");
        return mv;
    }

}
