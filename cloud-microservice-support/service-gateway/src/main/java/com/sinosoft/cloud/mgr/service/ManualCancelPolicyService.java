package com.sinosoft.cloud.mgr.service;

public interface ManualCancelPolicyService {
    String cancelPolicy(String params);
}
