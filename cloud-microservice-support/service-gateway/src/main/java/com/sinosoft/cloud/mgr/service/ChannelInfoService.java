package com.sinosoft.cloud.mgr.service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.LDCode1;
import com.sinosoft.cloud.dal.dao.model.entity.AgentInfo;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.LAComLiAn;
import com.sinosoft.cloud.dal.dao.model.open_LaCom;

import java.util.List;

public interface ChannelInfoService {
    List<ChlInfo> getChannelInfoList(ChlInfo chlInfo, Integer pageIndex, Integer pageSize);

    Integer count();

    AgentInfo getDetailByAgencyCode(String agencyCode);

    Integer countChlInfoInfo(ChlInfo chlInfo);

    open_LaCom getComDetailByAgencyCode(String agencyCode);

    Integer countForChlMaintainList(String chlTypeL, String chlTypeC);

    List<LDCode> getChlLargeType();

    List<LDCode1> getChlChildType();

    List<LDCode1> getAllChlChildType(String chlTypeL);

    List<LDCode1> getChlMaintainList(String chlTypeL, String chlTypeC, Integer pageIndex, Integer pageSize);

    String addChlMaintain(JSONObject po);

    String addwangBit(JSONObject po);

    String deleteChlMaintain(String chlTypeL, String chlTypeC);

    List<ChlInfo> getAgencyInfo();

    Integer countForChlAgencyMaintainList(String agencyCode, String agencyName,String chlTypeL);

    List<LAComLiAn> getChlAgencyMaintainList(String agencyCode, String agencyName, String chlTypeL, Integer pageIndex, Integer pageSize);

    String addChlAgencyMaintain(JSONObject po);

    String deleteChlAgencyMaintain(String agencyName, String channelName);
}
