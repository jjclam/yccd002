package com.sinosoft.cloud.mgr.service;


import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;

import java.util.List;

public interface ProSyncService {

	Integer count(ProInfo proInfo);

	List<ProInfo> getproSyncList(ProInfo proInfo, Integer pageIndex, Integer pageSize);

    String upPro(String proCode);

    String upPro(String proCode,String str);

	String downPro(String proCode);


}
