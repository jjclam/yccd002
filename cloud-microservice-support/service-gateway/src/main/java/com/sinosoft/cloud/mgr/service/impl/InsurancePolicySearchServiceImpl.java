package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LCContMapper;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.mgr.service.InsurancePolicySearchService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InsurancePolicySearchServiceImpl implements InsurancePolicySearchService {
    private final static Log logger = LogFactory.getLog(InsurancePolicySearchServiceImpl.class);
    @Autowired
    private LCContMapper lcContMapper;
    
    @Override
    public int countForPolSearchList(Operation lcCont) {
        Map<String, Object> map = new HashMap<>();
        map.put("sellType", lcCont.getSellType());
        map.put("contNo", lcCont.getContNo());
        map.put("appntName", lcCont.getAppntName());
        map.put("insuredName", lcCont.getInsuredName());
        map.put("appFlag", lcCont.getAppFlag());
        return lcContMapper.countForPolSearchList(map);
    }

    @Override
    public List<Operation> getPolSearchList(Operation lcCont, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
//        map.put("sellType", lcCont.getSellType());
        map.put("contNo", lcCont.getContNo());
        map.put("appntName", lcCont.getAppntName());
        map.put("insuredName", lcCont.getInsuredName());
        map.put("countDate", lcCont.getCountDate());
        logger.info("getPolSearchList: " + map);
        return lcContMapper.selectForPolSearchList(map);
    }
}
