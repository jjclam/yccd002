package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.LDCode1;
import com.sinosoft.cloud.dal.dao.model.entity.AgentInfo;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.LAComLiAn;
import com.sinosoft.cloud.dal.dao.model.open_LaCom;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.liAnlife.LianLifeService;
import com.sinosoft.cloud.mgr.service.ChannelInfoService;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;

/**
 * 渠道管理
 *
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/chl_edit")
public class ChannelInfoController {
    private final static Log logger = LogFactory.getLog(ChannelInfoController.class);
    @Autowired
    private ChannelInfoService channelInfoService;
    @Autowired
    private ChannelProService channelProService;
    @Autowired
    private LianLifeService mLianLifeService;

    @RequestMapping("/list.html")
    public String listInput(ModelMap model) {
//        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
//        model.addAttribute("laChlInfo", laChlInfo);
        List<ChlInfo> laChlAgencyType = channelProService.getChlAgencyType();
        model.addAttribute("laChlAgencyType", laChlAgencyType);
        return "chl_info/list";
    }

    @RequestMapping("/info_maintain.html")
    public String listChlMaintain(ModelMap model) {
        List<LDCode> laChlLargeType = channelInfoService.getChlLargeType();
        model.addAttribute("laChlLargeType", laChlLargeType);
        List<LDCode1> laChlChildType = channelInfoService.getChlChildType();
        model.addAttribute("laChlChildType", laChlChildType);
        return "chl_maintain/chl_list";
    }

    @RequestMapping("/chl_agencyMaintain.html")
    public String listAgencyMaintain(ModelMap model) {
        List<LDCode> laChlLargeType = channelInfoService.getChlLargeType();
        model.addAttribute("laChlLargeType", laChlLargeType);
        return "chl_agencyMaintain/chl_list";
    }

    @RequestMapping("/addLacom.html")
    public String add(ModelMap model) {
        List<Lacom> lacoms = mLianLifeService.selectManAgeComSelect();
        model.addAttribute("lacoms", lacoms);
        List<LDCode> laChlLargeType = channelInfoService.getChlLargeType();
        model.addAttribute("laChlLargeType", laChlLargeType);
        return "chl_maintain/add";
    }

    /**   Add By Xcc  出单网点新增   */
    @RequestMapping("/addUser")
    @ResponseBody
    public Map<String, Object> addWangBit(@RequestParam String params){
            Map<String, Object> map = new HashMap<>();
            JSONObject po = JSONObject.parseObject(params);
            try {
                String result = channelInfoService.addwangBit(po);
                map.put("msg", result);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("系统异常");
                map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
                return map;
            }
            return map;
    }





    @RequestMapping("/chlMaintain_add.html")
    public String core_add(ModelMap model) {
        List<LDCode> laChlLargeType = channelInfoService.getChlLargeType();
        model.addAttribute("laChlLargeType", laChlLargeType);
        return "chl_maintain/chl_add";
    }

    @RequestMapping("/view.html")
    public String viewInput(String agencyCode, String channelCode, ModelMap model) {
        AgentInfo item = null;
        if (StringUtils.isNotBlank(agencyCode)) {
            item = channelInfoService.getDetailByAgencyCode(agencyCode);
            if (item.getEndFlag() != null) {
                //核心停业标志Y为无效;N为有效;
                if (item.getEndFlag().equals("Y")) item.setEndFlag("否");
                else item.setEndFlag("是");
            }
        }
        if (item == null) item = new AgentInfo();
        model.put("item", item);
        //企业信息
        open_LaCom comItem = null;
        comItem = channelInfoService.getComDetailByAgencyCode(agencyCode);
        if (comItem == null) comItem = new open_LaCom();
        model.put("comItem", comItem);
//        logger.info("????????????????????????????item: " + item);
        return "chl_info/view";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute ChlInfo chlInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
//        int count = channelInfoService.count();
//        if(count <= 0) return JSON.toJSONString(pageModel);
        //首次页面不查找数据
        if (chlInfo.getAgencyCode() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = channelInfoService.countChlInfoInfo(chlInfo);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }

        List<ChlInfo> result = channelInfoService.getChannelInfoList(chlInfo, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (ChlInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
            logger.info(array.toJSONString());
        }
        pageModel.setCount(count);
        logger.info(result.toString());
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/chlMaintain_list")
    @ResponseBody
    public String chlMaintainList(String chlTypeL, String chlTypeC, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        if (chlTypeL == null || chlTypeC == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = channelInfoService.countForChlMaintainList(chlTypeL, chlTypeC);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }
        List<LDCode1> result = channelInfoService.getChlMaintainList(chlTypeL, chlTypeC, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LDCode1 pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/chlAgencyMaintain_list")
    @ResponseBody
    public String chlAgencyMaintainList(String agencyCode, String agencyName, Integer pageIndex,String chlTypeL,Integer pageSize) {
        if (chlTypeL!=null&&(!("".equals(chlTypeL)))){
            String[] split = chlTypeL.split("-");
            chlTypeL = split[0];
        }
        PageModel pageModel = new PageModel();
        if (agencyCode == null || agencyName == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = channelInfoService.countForChlAgencyMaintainList(agencyCode, agencyName,chlTypeL);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }
        List<LAComLiAn> result = channelInfoService.getChlAgencyMaintainList(agencyCode, agencyName,chlTypeL,pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LAComLiAn pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping(value = "/addChlMaintain")
    @ResponseBody
    public Map<String, Object> addChlMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        try {
            String result = channelInfoService.addChlMaintain(po);
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping(value = "/addChlAgencyMaintain")
    @ResponseBody
    public Map<String, Object> addChlAgencyMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        try {
            String result = channelInfoService.addChlAgencyMaintain(po);
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping(value = "/deleteChlMaintain")
    @ResponseBody
    public Map<String, Object> deleteChlMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String chlTypeLs = po.getString("chlTypeLs");
        String chlTypeCs = po.getString("chlTypeCs");
        if (StringUtils.isBlank(chlTypeLs)) {
            logger.error(PARAM_NOT_FOUND.getMessage());
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        }
        String result = "";
        String chlTypeL = "";
        String chlTypeC = "";
        String[] strArrayL = chlTypeLs.split(",");
        String[] strArrayP = chlTypeCs.split(",");
        try {
            for (int i = 0; i < strArrayL.length; i++) {
                chlTypeL = strArrayL[i];
                for (int n = 0; n < strArrayP.length; n++) {
                    chlTypeC = strArrayP[n];
                    result = channelInfoService.deleteChlMaintain(chlTypeL, chlTypeC);
                }
            }
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(Chl_UNKNOWN_ERROR.getMessage());
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping(value = "/deleteChlAgencyMaintain")
    @ResponseBody
    public Map<String, Object> deleteChlAgencyMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String agencyNames = po.getString("agencyNames");
        String channelNames = po.getString("channelNames");
        if (StringUtils.isBlank(agencyNames)) {
            logger.error(PARAM_NOT_FOUND.getMessage());
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        }
        String result = "";
        String agencyName = "";
        String channelName = "";
        String[] strArrayU = agencyNames.split(",");
        String[] strArrayP = channelNames.split(",");
        try {
            for (int i = 0; i < strArrayU.length; i++) {
                agencyName = strArrayU[i];
                for (int n = i; n < strArrayP.length; n++) {
                    channelName = strArrayP[n];
                }
                result = channelInfoService.deleteChlAgencyMaintain(agencyName, channelName);
            }
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping("/linkChlChildType")
    @ResponseBody
    public Map<String, Object> linkChlChildType(@RequestParam String chlTypeL) {
        Map<String, Object> map = new HashMap<>();
        List<LDCode1> list = channelInfoService.getAllChlChildType(chlTypeL);
        //如果渠道大类没有录入对应的子渠道
        if (list.size() == 0) {
            map.put("msg", CM_LINKCHLCHILDTYPE_NOT_EXIST.getMessage());
        }
        map.put("linkChlChildType", list);
        return map;
    }
}
