package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LZCardInfo;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.DocManageService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/doc_mng")
//@Import({DocManageService.class})
public class DocManageController {
    private final Log logger = LogFactory.getLog(this.getClass());
@Autowired
    private DocManageService docManageService;

//    @Value("${spring.elasticjoblite.url}")
//    private String joburl ;

    @RequestMapping("/docback.html")
    public String docBack(ModelMap model) {
        List<LDCode> jobCode = docManageService.getCodes();
//        List<LDCode> stateCode = docManageService.getStates();
//        model.addAttribute("jobCode", jobCode);
//        model.addAttribute("stateCode", stateCode);
        return "doc_mng/docBack";
    }

    @RequestMapping("/docresale.html")
    public String docResale(ModelMap model) {
        List<LDCode> jobCode = docManageService.getCodes();
//        List<LDCode> stateCode = docManageService.getStates();
//        model.addAttribute("jobCode", jobCode);
//        model.addAttribute("stateCode", stateCode);
        return "doc_mng/docresale";
    }
//
//    @RequestMapping("/docsatistics.html")
//    public String docSatistics(ModelMap model) {
//        System.out.println("joburl"+joburl);
//        List<LDCode> jobCode = docManageService.getCodes();
////        List<LDCode> stateCode = docManageService.getStates();
////        model.addAttribute("jobCode", jobCode);
////        model.addAttribute("stateCode", stateCode);
//        return "doc_mng/docsatistics";
//    }
//    @RequestMapping("/docSatistics.html")
//    public String docSatistics(ModelMap model) {
//        System.out.println("joburl"+joburl);
////        List<LDCode> jobCode = docManageService.getCodes();
////        List<LDCode> stateCode = docManageService.getStates();
////        model.addAttribute("jobCode", jobCode);
////        model.addAttribute("stateCode", stateCode);
//        return "doc_mng/docsatistics";
//    }
@RequestMapping("/lists")
@ResponseBody
public String lists(@ModelAttribute LdJobRunLog ldJobRunLog, Integer pageIndex, Integer pageSize) {
    PageModel pageModel = new PageModel();
    String taskCode = ldJobRunLog.getTaskCode();

    if (taskCode == null){
        pageModel.setMsg("");
        return JSON.toJSONString(pageModel);
    }
    //分页计数
    int count = docManageService.countForList(ldJobRunLog);
    if(count <= 0) {
        pageModel.setMsg("该信息没有录入, 请重新选择!");
        return JSON.toJSONString(pageModel);
    }

    List<LdJobRunLog> result = (List<LdJobRunLog>) docManageService.getResultByTaskName(ldJobRunLog,pageIndex,pageSize);
    if (!CollectionUtils.isEmpty(result)) {
        JSONArray array = new JSONArray();
        for (LdJobRunLog dJobRunLog : result) {
            if (dJobRunLog.getTaskName() != null) {}
            JSONObject object = (JSONObject) JSONObject.toJSON(dJobRunLog);
            array.add(object);
        }
        logger.info("结果返回："+result.toString());
        pageModel.setList(array);
    }
    pageModel.setCount(count);
    pageModel.setMsg("ok");
    pageModel.setRel(true);
    return JSON.toJSONString(pageModel);
}


    @RequestMapping("/documentBack")
    @ResponseBody
    public Map<String, Object> documentBack(@RequestParam(value = "params") String params, HttpServletRequest request) {
        logger.info("===params"+params);
        Map<String, Object> map = new HashMap<>();
        String flag = docManageService.documentBack(params);
        map.put("msg",flag);
        return map;
    }


    /**
     * 回销
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/documentBackSale")
    @ResponseBody
    public Map<String, Object> documentBackSale(@RequestParam(value = "params") String params, HttpServletRequest request) {
        logger.info("===params"+params);
        Map<String, Object> map = new HashMap<>();
        String flag = docManageService.documentBackSale(params);
        map.put("msg",flag);
        return map;
    }

}
