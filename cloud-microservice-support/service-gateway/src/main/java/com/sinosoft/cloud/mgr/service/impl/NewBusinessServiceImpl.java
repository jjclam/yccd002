package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.mapper.*;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.mgr.service.NewBusinessService;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.SLCXmlSchema;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.Chl_NESPARAM_NOT_FOUND;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

@Service
public class NewBusinessServiceImpl implements NewBusinessService {
    private final static Log logger = LogFactory.getLog (NewBusinessServiceImpl.class);
    @Autowired
    private LCContMapper tLCContMapper;
    @Autowired
    private LDCodeMapper tLDCodeMapper;
    @Autowired
    private LAComtoAgentMapper tLAComtoAgentMapper;
    @Autowired
    private LDPLANDUTYPARAMMapper tLDPLANDUTYPARAMMapper;
    @Autowired
    private SysUserMapper tSysUserMapper;
    @Autowired
    private LAComMapper tLAComMapper;


    //保单查询核心uat接口地址
    @Value("${cloud.nb.servlet.url}")
    private String url;
    @Value("service")
    private String method;


    private String createXml(String params, String opreator) {
        JSONObject po = JSONObject.parseObject (params);

        //创建Document 对象，代表整个 xml文档
        Document document = DocumentHelper.createDocument ( );
        //创建根节点
        Element Package = document.addElement ("Package");
        //创建子节点Head
        Element Head = Package.addElement ("Head");
        //TransDate
        Element TransDate = Head.addElement ("TransDate");
        String nowDate = PubFun.getCurrentDate ( );
        TransDate.setText (nowDate);
        //Transtime
        Element TransTime = Head.addElement ("TransTime");
        String nowTime = PubFun.getCurrentTime ( );
        TransTime.setText (nowTime);
        //ChannelCode 测试渠道信息
        Element ChannelCode = Head.addElement ("ChannelCode");
        ChannelCode.setText ("43");
        //TransType
        Element TransType = Head.addElement ("TransType");
        TransType.setText ("A0021");
        //SerialNo
        Element SerialNo = Head.addElement ("SerialNo");
        String seq = PubFun.CreateSeq ("seq_serialno", 6);
        String serialNo = nowDate.replace ("-", "") + nowTime.replace (":", "") + seq;
        SerialNo.setText (serialNo);

        //创建子节点RequestNodes
        Element RequestNodes = Package.addElement ("RequestNodes");
        //创建子节点RequestNode
        Element RequestNode = RequestNodes.addElement ("RequestNode");
        //ProductCode
        Element ProductCode = RequestNode.addElement ("ProductCode");
        ProductCode.setText (po.getString ("prodSetCode"));
        //ProductName
        Element ProductName = RequestNode.addElement ("ProductName");
        ProductName.setText (po.getString ("prodSetCode"));
        //OrderNo
        Element OrderNo = RequestNode.addElement ("OrderNo");
        OrderNo.setText (serialNo);
        //proposalNo
        Element proposalNo = RequestNode.addElement ("proposalNo");
        proposalNo.setText (PubFun.CreateSeq ("seq_proposalno_11", 12));

        //创建子节点Applicant
        Element Applicant = RequestNode.addElement ("Applicant");
        //AppName
        Element AppName = Applicant.addElement ("AppName");
        AppName.setText (po.getString ("appntName"));

        String appntIDNo = po.getString ("appntIDNo");
        String appntSex = "";
        String insuredSex = "";
        String appntBirthDay = "";
        String insuredBirthDay = "";
        String insuredName = "";
        String insuredIDType="";
        String insuredIDNo="";
        if (appntIDNo == null || "".equals (appntIDNo)) {
            logger.info ("投保人证件号码为空，请检查");
            return Chl_NESPARAM_NOT_FOUND.getMessage ( );
        }
        appntSex = PubFun.getSexFromId (appntIDNo);
        appntBirthDay = PubFun.getBirthFromIdNo (appntIDNo);

        //AppSex
        Element AppSex = Applicant.addElement ("AppSex");
        AppSex.setText (appntSex);
        //AppBirthday
        Element AppBirthday = Applicant.addElement ("AppBirthday");
        AppBirthday.setText (appntBirthDay);
        //AppIDType
        Element AppIDType = Applicant.addElement ("AppIDType");
        AppIDType.setText (po.getString ("appntIDType"));
        //AppIDNo
        Element AppIDNo = Applicant.addElement ("AppIDNo");
        AppIDNo.setText (appntIDNo);
        //IdexpDate
        Element IdexpDate = Applicant.addElement ("IdexpDate");
//        IdexpDate.setText(po.getString("idexpDate"));
        //AppMobile
        Element AppMobile = Applicant.addElement ("AppMobile");
        if (po.getString ("mobile") != null) {
            AppMobile.setText (po.getString ("mobile"));
        }


        //创建子节点Insured
        Element Insured = RequestNode.addElement ("Insured");

        String relationship = po.getString ("relationship");
        if ("00".equals (relationship)) {
            //InsuName
            Element InsuName = Insured.addElement ("InsuName");
            insuredName = po.getString ("appntName");
            InsuName.setText (po.getString ("appntName"));
            //InsuSex
            Element InsuSex = Insured.addElement ("InsuSex");
            insuredSex = appntSex;
            InsuSex.setText (appntSex);
            //InsuBirthday
            Element InsuBirthday = Insured.addElement ("InsuBirthday");
            InsuBirthday.setText (appntBirthDay);
            insuredBirthDay = appntBirthDay;
            //InsuIDType
            Element InsuIDType = Insured.addElement ("InsuIDType");
            insuredIDType = po.getString ("appntIDType");
            InsuIDType.setText (insuredIDType);

            //InsuIDNo
            Element InsuIDNo = Insured.addElement ("InsuIDNo");
            insuredIDNo = appntIDNo;
            InsuIDNo.setText (insuredIDNo);
        } else {
            //InsuName
            Element InsuName = Insured.addElement ("InsuName");
            InsuName.setText (po.getString ("insuredName"));
            insuredName = po.getString ("insuredName");

           insuredIDNo = po.getString ("insuredIDNo");


            if (insuredIDNo == null || "".equals (insuredIDNo)) {
                logger.info ("被保人证件号码为空，请检查");
                return Chl_NESPARAM_NOT_FOUND.getMessage ( );
            }
            insuredSex = PubFun.getSexFromId (insuredIDNo);
            insuredBirthDay = PubFun.getBirthFromIdNo (insuredIDNo);
            //InsuSex
            Element InsuSex = Insured.addElement ("InsuSex");
            InsuSex.setText (insuredSex);
            //InsuBirthday
            Element InsuBirthday = Insured.addElement ("InsuBirthday");
            InsuBirthday.setText (insuredBirthDay);
            //InsuIDType
            Element InsuIDType = Insured.addElement ("InsuIDType");
            insuredIDType = po.getString ("insuredIDType");
            InsuIDType.setText (insuredIDType);
            //InsuIDNo
            Element InsuIDNo = Insured.addElement ("InsuIDNo");
            InsuIDNo.setText (insuredIDNo);
        }


        //Relationship
        Element Relationship = Insured.addElement ("Relationship");
        Relationship.setText (po.getString ("relationship"));
        // TODO: 2020/4/24 增加 SLCXml
        SLCXmlSchema tSLCXmlSchema = new SLCXmlSchema ( );
        Element SLCXml = RequestNode.addElement ("SLCXml");
        tSLCXmlSchema.setMult (1);
        tSLCXmlSchema.setInsuredName (insuredName); //被保人姓名
        tSLCXmlSchema.setInsuredIDType (insuredIDType); //被保人证件类型
        tSLCXmlSchema.setInsuredIDNo (insuredIDNo); //被保人证件号码
        tSLCXmlSchema.setInsuredSex (insuredSex);  //被保人性别
        tSLCXmlSchema.setInsuredBirthday (new FDate ( ).getDate (insuredBirthDay));
        tSLCXmlSchema.setInsuYearFlag ("");
        tSLCXmlSchema.setInsuYearFlag ("");
        tSLCXmlSchema.setFlightNo ("G12120");
        tSLCXmlSchema.setAppBirthday (new FDate ( ).getDate (appntBirthDay));

        tSLCXmlSchema.setRiskCode ("1");//产品编码
        tSLCXmlSchema.setInterCode ("1"); //款式
        tSLCXmlSchema.setPolApplyDate (new Date ( )); //投保日期
        //tSLCXmlSchema.setIsBooking("0"); //是否预约生效，不预约，取后台时间
        tSLCXmlSchema.setIsBooking ("111");
        tSLCXmlSchema.setCValiDate (new Date ( )); //生效日期
        //tSLCXmlSchema.setCValiTime("00:00:00");  //生效时间

        tSLCXmlSchema.setOccupationType ("1");
        tSLCXmlSchema.setInsureOccupation ("111111");


        //受益人信息
        tSLCXmlSchema.setBnfFlag ("111"); //受益人标记 0-法定；1-指定
        tSLCXmlSchema.setBnfName ("111"); //受益人姓名
        tSLCXmlSchema.setBnfRelation ("111"); //受益人与被保人关系
        String tsLcjson = JSON.toJSONString (tSLCXmlSchema);

        SLCXml.setText (tsLcjson);
        logger.info ("tSLCXmlSchema的值" + tsLcjson);


        //IdexpDate
        Element InIdexpDate = Insured.addElement ("IdexpDate");
//        InIdexpDate.setText(po.getString("inIDexpDate"));


        document.setXMLEncoding ("GBK");
        String text = document.asXML ( );
        return text;
    }


    @Override
    public String saveCont(String params, String opreator) {
        String xmlStr = createXml (params, opreator);
        System.out.println ("请求报文:" + xmlStr);

        String response = null;
        try {
            response = post (url, xmlStr);
        } catch (Exception e) {
            e.printStackTrace ( );
        }
        System.out.println ("返回报文:" + response);

        //解析返回报文
        String result = "";
        try {
            result = getReturnXml (response);
        } catch (Exception e) {
            e.printStackTrace ( );
        }


        return result;
    }


    @Override
    public List<LDCode> getIDtype() {
        return tLDCodeMapper.getAllCodeValueInfo ("idtype");
    }

    @Override
    public List<LDCode> getRelation() {
        return tLDCodeMapper.getAllCodeValueInfo ("relation");
    }


    private String getReturnXml(String response) throws Exception {
        String result = "";

        Document document = DocumentHelper.parseText (response);
        // 通过document对象获取根元素的信息
        Element Package = document.getRootElement ( );

        Element ResponseNodes = Package.element ("ResponseNodes");
        Element ResponseNode = ResponseNodes.element ("ResponseNode");
        Element BusinessObject = ResponseNode.element ("BusinessObject");
        Element isSuccess = BusinessObject.element ("isSuccess");
        String successFlag = isSuccess.getText ( );

        if ("1".equals (successFlag)) {
            result = SUCCESS.getCode ( );
            logger.info ("出单成功: " + SUCCESS.getMessage ( ));
        } else {
            Element Result = ResponseNode.element ("Result");
            Element ResultInfoDesc = Result.element ("ResultInfoDesc");
            result = ResultInfoDesc.getText ( );
        }

        return result;
    }

    /**
     * 发送xml数据请求到server端
     *
     * @param url        xml请求数据地址
     * @param requestXml 发送的xml数据流
     */
    public String post(String url, String requestXml) {
        //关闭
        System.setProperty ("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        System.setProperty ("org.apache.commons.logging.simplelog.showdatetime", "true");
        System.setProperty ("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "stdout");

        //创建httpclient工具对象
        HttpClient client = new HttpClient ( );
        //创建post请求方法
        PostMethod myPost = new PostMethod (url);
        //设置请求超时时间
        client.setConnectionTimeout (300 * 1000);
        String responseString = null;
        try {
            myPost.setRequestHeader ("Content-Type", "application/xml;charset=GBK");
          //  myPost.setRequestHeader ("charset", "utf-8");
            myPost.setRequestBody (requestXml);

            int statusCode = client.executeMethod (myPost);
            if (statusCode == HttpStatus.SC_OK) {
                BufferedInputStream bis = new BufferedInputStream (myPost.getResponseBodyAsStream ( ));
                byte[] bytes = new byte[1024];
                ByteArrayOutputStream bos = new ByteArrayOutputStream ( );
                int count = 0;
                while ((count = bis.read (bytes)) != -1) {
                    bos.write (bytes, 0, count);
                }
                byte[] strByte = bos.toByteArray ( );
                responseString = new String (strByte, 0, strByte.length, "GBK");
                bos.close ( );
                bis.close ( );
            }
        } catch (Exception e) {
            e.printStackTrace ( );
        }
        myPost.releaseConnection ( );
        return responseString;
    }


}
