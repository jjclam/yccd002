package com.sinosoft.cloud.mgr.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface ContBatchService {

    Map<String, Object> importExcel(MultipartFile file, String opreator);
}
