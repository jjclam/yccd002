package com.sinosoft.cloud.mgr.service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.Role;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Caden
 * @Description:
 * @Date: on 2019/12/18.
 * @Modified By:
 */
@Service
public interface RoleService {
    Role selectRole(String roleId);
    int roleCount(Role role);
    List<Role> getRoleList(Role role, Integer pageIndex, Integer pageSize);
    String adduRole(JSONObject po);
}
