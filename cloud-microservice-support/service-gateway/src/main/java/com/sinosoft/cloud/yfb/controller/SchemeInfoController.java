package com.sinosoft.cloud.yfb.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeRisk;
import com.sinosoft.cloud.dal.yfbDao.model.entity.SchemeParams;
import com.sinosoft.cloud.yfb.service.SchemeInfoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_NOT_FOUND;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.PARAM_NOT_FOUND;

/**
 * 方案查询
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/scheme_info")
public class SchemeInfoController {
    private final static Log logger = LogFactory.getLog(SchemeInfoController.class);
    @Autowired
    private SchemeInfoService schemeInfoService;

    @RequestMapping("/list.html")
    public String list(ModelMap model) {
        return "yfb/scheme/info/list";
    }

    @RequestMapping("/view.html")
    public String view(LDSchemeRisk contschemecode, ModelMap model) {
        String schemecode = contschemecode.getContschemecode();
        SchemeParams result = schemeInfoService.getSchemeInfoListForParams(schemecode);
        model.put("params", result);
        return "yfb/scheme/info/view";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute LDSchemeRisk scheme, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        //首次页面不查找数据
        if (scheme.getContschemecode() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = schemeInfoService.countSchemeInfo(scheme);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<LDSchemeRisk> result = schemeInfoService.getSchemeInfoList(scheme, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LDSchemeRisk pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/viewList")
    @ResponseBody
    public String viewList(@ModelAttribute SchemeParams scheme, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        int count = schemeInfoService.countSchemeInfoForView(scheme);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<SchemeParams> result = schemeInfoService.getSchemeInfoListForView(scheme, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (SchemeParams pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/linkedSchemeParams")
    @ResponseBody
    public Map<String, Object> linkedSchemeParams(SchemeParams scheme, ModelMap model) {
        HashMap<String, Object> map = new HashMap<>();
        String schemecode = scheme.getContschemecode();
        if (StringUtils.isBlank(schemecode)){
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        }
        SchemeParams result = schemeInfoService.getSchemeInfoListForParams(schemecode);
        map.put("Prems", result.getPrem());
        logger.info(result.getPrem());
        return map;
    }

}
