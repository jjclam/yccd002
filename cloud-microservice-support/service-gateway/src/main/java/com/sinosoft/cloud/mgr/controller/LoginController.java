package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.sinosoft.cloud.dal.dao.model.Menu;
import com.sinosoft.cloud.dal.dao.model.NavigationItem;
import com.sinosoft.cloud.dal.dao.model.UserMgr;
import com.sinosoft.cloud.dal.dao.model.entity.SysUser;
import com.sinosoft.cloud.mgr.service.SysUserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.Chl_UNKNOWN_ERROR;


@Controller
public class LoginController {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private SysUserService sysUserService;
    String user1;
    @RequestMapping(value = {"/"})
    public String index( Model model) {
        try {
            logger.info("主页");
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("用户 id： ="+user1);
       model.addAttribute("navList", JSON.toJSONString(this.getNavList()));
    return "/index";
    }
    private List<NavigationItem> getNavList(){
        List navList = new ArrayList<>();
        List<Menu> parentMenu = sysUserService.parentMenu(user1);

        for(int i=0;i<parentMenu.size();i++){
            String name = parentMenu.get(i).getName();
            String icon1 = parentMenu.get(i).getIcon();
            NavigationItem nav = new NavigationItem();
            nav.setTitle(name);
            nav.setIcon(icon1);
            nav.setSpread(false);
            List navChildren = new ArrayList<NavigationItem>();
            String id2 = parentMenu.get(i).getId();
//            Integer id = parentMenu.get(i).getId();
//             String id1 = id.toString();
            List<Menu> Menu = sysUserService.getMenu(user1,id2);
            for(int j=0;j<Menu.size();j++){
                String Menuname = Menu.get(j).getName();
                String href = Menu.get(j).getHref();
                String icon = Menu.get(j).getIcon();
                NavigationItem child =new NavigationItem();
                child.setTitle(Menuname);
                child.setIcon(icon);
                child.setHref(href);
                navChildren.add(child);
            }
            nav.setChildren(navChildren);
            navList.add(nav);
        }
       // logger.info("====="+JSON.toJSONString(navList, true));
        return navList;
    }
    @RequestMapping(value = "/logout")
    public String Loginout(ServletRequest servletRequest, ServletResponse servletResponse) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        try {
            logger.info("注销");
            HttpSession session = request.getSession(false);
            session.removeAttribute("user");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/";
    }


    @RequestMapping(value = "/login")
    public String CheckUser(Model model, HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> map = new HashMap<String, String>();
        String msg = "";
        try {
            logger.info("验证客户");
            HttpSession session = request.getSession();
            String userName = request.getParameter("userName");
            String password = request.getParameter("password");
            logger.info("userName=" + userName + ":password=" + password);
            if (userName != null && password != null) {
                UserMgr SysUser = sysUserService.checkUser(userName, password);
                if (SysUser != null) {
                    session.setAttribute("user", userName);
                    session.setAttribute("name", SysUser.getNickName());
                     user1 = SysUser.getId();
                     logger.info("开始用户获取+"+ user1);
                    return  "redirect:/";
                } else {
                    msg = "用户名或密码错误!";

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            msg = "系统异常!";
        }

        model.addAttribute("message", msg);
        return "/login";
    }

    @RequestMapping(value = "/register")
    @ResponseBody
    public Map<String, Object> CreateUser(HttpServletRequest request, HttpServletResponse response) {
//    public Map<String, Object> CreateUser(@RequestParam(value = "userName") String userName, @RequestParam(value = "passWord") String passWord) {
        Map<String, Object> map = new HashMap<>();
        String msg = "";
        try {
            String userName = request.getParameter("userName");
            String passWord = request.getParameter("passWord");
            logger.info("userName=" + userName + ":passWord=" + passWord);
            if (userName != null && passWord != null) {
                msg = sysUserService.createUser(userName, passWord);
                map.put("msg", msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

}
