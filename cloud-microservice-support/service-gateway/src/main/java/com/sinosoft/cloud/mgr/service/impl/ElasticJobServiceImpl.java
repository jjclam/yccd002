package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LdJobRunLogMapper;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;
import com.sinosoft.cloud.mgr.service.ElasticJobService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ElasticJobServiceImpl implements ElasticJobService {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    LdJobRunLogMapper ldJobRunLogMapper;
            ;
    @Override
    public List<LdJobRunLog> getResultByTaskName(LdJobRunLog ldJobRunLog, Integer pageIndex, Integer pageSize) {
        logger.info("--pageIndex："+pageIndex+"--pageSize："+pageSize);
        logger.info("TaskCode"+ldJobRunLog.getTaskCode());
        logger.info("ExecuteState"+ldJobRunLog.getExecuteState());
        logger.info("ExecuteDate"+ldJobRunLog.getExecuteDate());
        logger.info("FinishDate"+ldJobRunLog.getFinishDate());
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("taskCode", ldJobRunLog.getTaskCode());
        map.put("executeState", ldJobRunLog.getExecuteState());
        map.put("executeDate", ldJobRunLog.getExecuteDate());
        map.put("finishDate", ldJobRunLog.getFinishDate());
        return ldJobRunLogMapper.selectByTaskName(map);
    }

    @Override
    public List<LDCode> getCodes() {

        return ldJobRunLogMapper.getCodes();
    }
    @Override
    public List<LDCode> getStates() {

        return ldJobRunLogMapper.getStates();
    }
    @Override
    public Integer countForList(LdJobRunLog ldJobRunLog) {
        Map<String, Object> map = new HashMap<>();
        map.put("taskCode", ldJobRunLog.getTaskCode());
        map.put("executeState", ldJobRunLog.getExecuteState());
        map.put("executeDate", ldJobRunLog.getExecuteDate());
        map.put("finishDate", ldJobRunLog.getFinishDate());
        return ldJobRunLogMapper.countForList(map);
    }
}
