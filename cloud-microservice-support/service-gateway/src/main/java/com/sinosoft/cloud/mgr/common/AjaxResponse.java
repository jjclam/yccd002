package com.sinosoft.cloud.mgr.common;

public class AjaxResponse {
    private String  statusName;
    private String  statusCode;
    private String  msgName;
    private String  countName;
    private Object dataName;   //返回对象

    public AjaxResponse() {
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsgName() {
        return msgName;
    }

    public void setMsgName(String msgName) {
        this.msgName = msgName;
    }

    public String getCountName() {
        return countName;
    }

    public void setCountName(String countName) {
        this.countName = countName;
    }

    public Object getDataName() {
        return dataName;
    }

    public void setDataName(Object dataName) {
        this.dataName = dataName;
    }

    public AjaxResponse(String statusName, String statusCode, String msgName, String countName, Object dataName) {
        this.statusName = statusName;
        this.statusCode = statusCode;
        this.msgName = msgName;
        this.countName = countName;
        this.dataName = dataName;
    }
}
