package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LDPLANRISKMapper;
import com.sinosoft.cloud.dal.dao.mapper.LMRiskMapper;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.mgr.service.ProInfoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_FAIL;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;
import static com.sinosoft.cloud.common.util.RegUtil.regMatch;

@Service
public class ProInfoServiceImpl implements ProInfoService {
    private final static Log logger = LogFactory.getLog(ProInfoServiceImpl.class);
    @Autowired
    private LDPLANRISKMapper tLDPLANRISKMapper;
    @Autowired
    private LMRiskMapper tLMRiskMapper;

    public List<ProInfo> getProCode() {
        return tLMRiskMapper.selectCombProCode();
    }

    public List<ProInfo> getProName() {
        return tLDPLANRISKMapper.selectOnlyProName();
    }

    @Override
    public List<ProInfo> getProCodeName() {
        return tLMRiskMapper.selectProCodeName();
    }

    public Integer count() {
        return tLMRiskMapper.countByExample();
    }

    @Override
    public Integer countProInfo(ProInfo proInfo) {
        String proCode = proInfo.getProCode();
        String proType = proInfo.getStandard();
//        if (!proCode.equals("") || proCode != null) {页面伪造
        if (!proCode.equals("")) {
            if (regMatch(proCode)) {
                return tLMRiskMapper.countByHEXIN(proCode, proType);
            } else {
                return tLMRiskMapper.countByTAOCAN(proCode, proType);
            }
        } else {
            return tLMRiskMapper.countByNoParam(proType);
        }
    }

    @Override
    public List<ProInfo> getproInfoList(ProInfo proInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        //区分核心与套餐
        String proCode = proInfo.getProCode();
        String proType = proInfo.getStandard();
//        if (!proCode.equals("") || proCode != null) {页面伪造
        if (!proCode.equals("")) {
            if (regMatch(proCode)) {
                return tLMRiskMapper.selectForHEXIN(proCode, proType);
            } else {
                return tLMRiskMapper.selectForTAOCAN(proCode, proType);
            }
        } else {
            return tLMRiskMapper.selectByNoParam(proType);
        }
    }

    @Override
    public String savePro(String proCode, String flag) {
        int updateFlag = 0;
        String result = "";
        if (regMatch(proCode)) {
            updateFlag = tLMRiskMapper.editProForHEXIN(proCode, flag);
        } else {
            updateFlag = tLMRiskMapper.editProForTAOCAN(proCode, flag);
        }
        if (updateFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("standardPro: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("standardPro: " + DB_FAIL.getMessage());
        }
        return result;
    }

}
