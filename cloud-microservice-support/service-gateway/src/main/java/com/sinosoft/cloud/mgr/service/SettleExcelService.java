package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SettleInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SettleExcelService {
    List<SettleInfo> getSettleExcel(SettleInfo settleInfo);

    List<ProInfo> getProductName();

    List<ProInfo> getProductCode1(String admin);

    String importExcel(MultipartFile file);
}
