package com.sinosoft.cloud.mgr.service;


import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;

import java.util.List;

public interface ProInfoService {

    List<ProInfo> getProCode();

    List<ProInfo> getProName();

    List<ProInfo> getProCodeName();

    Integer count();

    List<ProInfo> getproInfoList(ProInfo proInfo, Integer pageIndex, Integer pageSize);

    String savePro(String proCode, String flag);

    Integer countProInfo(ProInfo proInfo);
}
