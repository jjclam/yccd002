package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LAChannelProduct;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SaleProInfo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.common.ObjectExcelView;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.*;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.DateUtils.getTimeStrDefault;
import static com.sinosoft.cloud.common.util.HttpClientUtil.InputStreamTOString;
import static com.sinosoft.cloud.common.util.HttpClientUtil.getHttpURLConnection;
import static com.sinosoft.cloud.common.util.RegUtil.regMatch;

/**
 * 渠道管理
 *
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/chl_sync")
public class ChannelProController {
    private final static Log logger = LogFactory.getLog(ChannelProController.class);
    @Autowired
    private ChannelProService channelProService;

    @RequestMapping("/list.html")
    public String listInput(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        return "chl_sync/list";
    }

    @RequestMapping("/sync.html")
    public String listInputJ(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        return "chl_sync/sync";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute ChlInfo chlInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        //首次页面不查找数据
        if (chlInfo.getChannelCode() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = channelProService.countChlProInfoList(chlInfo);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }
        List<ChlInfo> result = channelProService.getChlProInfoList(chlInfo, pageIndex, pageSize);
//        List<LAChannelProduct> result = channelProService.getChlProInfoList((pageIndex-1)*pageSize, pageSize, laChlPro);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (ChlInfo pc : result) {
                if (pc.getChannelProductUpDate() != null & pc.getStatus().equals("0")) {
                    pc.setUpDownDate(pc.getChannelProductUpDate());
                } else if (pc.getChannelProductDownDate() != null & pc.getStatus().equals("1")) {
                    pc.setUpDownDate(pc.getChannelProductDownDate());
                } else {
                    pc.setUpDownDate("");//没有录入渠道产品上下架时间显示为空
                }
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
//                logger.info(object.toString());
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        ObjectExcelView erv = new ObjectExcelView();
        logger.info(result.toString());
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/view.html")
    public String viewInput(String proCode, String channelCode, ModelMap model) {
        SaleProInfo item = null;
        if (StringUtils.isNotBlank(proCode)) {
            item = channelProService.getDetailByProcode(proCode, channelCode);
            //险种类型显示为中文
            if (item != null & regMatch(item.getProCode())) {
                String riskTypeCode = item.getRiskType();
                SaleProInfo riskTypeName = channelProService.getRiskTypeName(riskTypeCode);
                item.setRiskTypeName(riskTypeName.getRiskTypeName());
            }
        }
        if (item == null) item = new SaleProInfo();
        model.put("item", item);

//        List<SaleProInfo> laRiskTypeInfo = channelProService.getRiskTypeCodeName();
//        model.addAttribute("laRiskTypeInfo", laRiskTypeInfo);
//
        return "chl_sync/view";
    }

    @RequestMapping("/edit.html")
    public String editInput(@ModelAttribute LAChannelProduct laChlPro, ModelMap model) {
        LAChannelProduct item = null;
        if (StringUtils.isNotBlank(laChlPro.getRiskCode()) & StringUtils.isNotBlank(laChlPro.getChannelCode())) {
            item = channelProService.getEditByProcode(laChlPro);
            String riskTypeCode = item.getRiskType();
            SaleProInfo riskTypeName = channelProService.getRiskTypeName(riskTypeCode);
            item.setRiskType(riskTypeName.getRiskTypeName());
        }

        if (item == null) item = new LAChannelProduct();
        if (item.getChannelCode() != null & item.getChannelName() != null) {
            item.setChannelCode(item.getChannelCode() + "-" + item.getChannelName());
        }
        model.put("item", item);
        logger.info("item: " + item);
        //
//        List<SaleProInfo> laRiskTypeInfo = channelProService.getRiskTypeCodeName();
//        model.addAttribute("laRiskTypeInfo", laRiskTypeInfo);
        return "chl_sync/edit";
    }

    @RequestMapping("/add.html")
    public String addInput(String proCode, String channelCode, ModelMap model) {
        LAChannelProduct item = null;
        if (StringUtils.isNotBlank(proCode) & StringUtils.isNotBlank(channelCode)) {
            proCode = proCode.split(",")[0];
            channelCode = channelCode.split(",")[0];
//            Map<String, Object> map = new HashMap<>();
            SaleProInfo pretend = new SaleProInfo();
            item = channelProService.getDetailByProChlcode(proCode, channelCode);
//            (String) item.setRiskEndSellDate(item.getRiskStartSellDate());
            pretend.setChannelCode(item.getChannelCode() + "-" + item.getChannelName());
            pretend.setRiskType(item.getRiskType());
            pretend.setProCode(item.getRiskCode());
            pretend.setProName(item.getRiskName());
            pretend.setRiskStartSellDate(getTimeStrDefault(item.getRiskStartSellDate()));
            pretend.setRiskEndSellDate(getTimeStrDefault(item.getRiskEndSellDate()));
            pretend.setH5href(item.getH5href());
            model.put("item", pretend);
        }
        if (item == null) {
            item = new LAChannelProduct();
            model.put("item", item);
        }
//
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        //险种类型
        List<SaleProInfo> laRiskTypeInfo = channelProService.getRiskTypeCodeName();
        model.addAttribute("laRiskTypeInfo", laRiskTypeInfo);
//        List<SaleProInfo> laAddProInfo = channelProService.getAllProInfo("");
//        model.addAttribute("laAddProInfo", laAddProInfo);
        return "chl_sync/add";
    }

    @RequestMapping("/allup.html")
    public String allupInput(ModelMap model, String riskType) {
//        ChlInfo item = null;
//        if (StringUtils.isNotBlank(proCode)) {
//            item = channelProService.getDetailByProcode(proCode);
//        }
//        if (item == null) item = new ChlInfo();
//        model.put("item", item);

        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        //险种类型
        List<SaleProInfo> laRiskTypeInfo = channelProService.getRiskTypeCodeName();
        model.addAttribute("laRiskTypeInfo", laRiskTypeInfo);
//        List<SaleProInfo> laAddProInfo = channelProService.getAllProInfo("");
//        model.addAttribute("laAddProInfo", laAddProInfo);
        return "chl_sync/allup";
    }

    @RequestMapping("/alldown.html")
    public String alldownInput(ModelMap model) {
//        ChlInfo item = null;
//        if (StringUtils.isNotBlank(proCode)) {
//            item = channelProService.getDetailByProcode(proCode);
//        }
//        if (item == null) item = new ChlInfo();
//        model.put("item", item);
//        List<ProInfo> lmRiskSale = proInfoService.getProCodeName();
//        model.addAttribute("lmRiskSale", lmRiskSale);
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        //险种类型
        List<SaleProInfo> laRiskTypeInfo = channelProService.getRiskTypeCodeName();
        model.addAttribute("laRiskTypeInfo", laRiskTypeInfo);
//        List<SaleProInfo> laAddProInfo = channelProService.getAllProInfo("");
//        model.addAttribute("laAddProInfo", laAddProInfo);
        return "chl_sync/alldown";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> save(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
//        map.put("msg", Chl_UNKNOWN_ERROR.getCode());
        JSONObject po = JSONObject.parseObject(params);
        String result = channelProService.updateChlProInfo(po);
        map.put("msg", result);
        return map;
    }

    @RequestMapping(value = "/addSave", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addSave(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String result = channelProService.addChlProInfo(po);
        map.put("msg", result);
        return map;
    }

    @RequestMapping("/downChlPro")
    @ResponseBody
    public Map<String, Object> downChlPro(@RequestParam(value = "channelCodes") String channelCodes, @RequestParam(value = "proCodes") String proCodes) {
        Map<String, Object> map = new HashMap<>();
        //截取获得字符串数组
        String result = "0";
        if (StringUtils.isBlank(proCodes) || StringUtils.isBlank(channelCodes)) {
            logger.error(proCodes + channelCodes + "没有");
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        } else {
            String[] strArrayC = channelCodes.split(",");
            String[] strArrayP = proCodes.split(",");
            String channelCode = "";
            String proCode = "";
            for (int i = 0; i < strArrayC.length; i++) {
                channelCode = strArrayC[i];
                for (int n = i; n < i + 1; n++) {
                    proCode = strArrayP[n];
                    result = channelProService.downChlPro(channelCode, proCode);
                }
            }
            map.put("msg", result);
            return map;
        }
    }

    @RequestMapping(value = "/allupChlPro", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> allupChlPro(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        //截取获得字符串数组
//        String result = "0";
        JSONObject po = JSONObject.parseObject(params);
//        String result = channelProService.addChlProInfo(po);
//        String channelCodes = po.getString("channelCodes");
//        String[] strArrayC = channelCodes.split(",");
//        String channelCode = "";
        String result = "";
//        for (int i = 0; i < strArrayC.length; i++) {
//            channelCode = strArrayC[i];
//            po.put("channelCode", channelCode);
//            logger.info(po);
//            result = channelProService.addupChlProInfo(po);
//        }
        result = channelProService.addupChlProInfo(po);
        map.put("msg", result);
        return map;

    }

    @RequestMapping(value = "/alldownChlPro", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> alldownChlPro(@RequestParam(value = "channelCodes") String channelCodes, @RequestParam(value = "riskType") String riskType,  @RequestParam(value = "riskCode") String proCode) {
        Map<String, Object> map = new HashMap<>();
        //截取获得字符串数组
        String result = "0";
        if (StringUtils.isBlank(proCode) || StringUtils.isBlank(channelCodes)) {
            logger.error(proCode + channelCodes + "没有");
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        } else {
//            String[] strArrayC = channelCodes.split(",");
//            String channelCode = "";
//            for (int i = 0; i < strArrayC.length; i++) {
//                channelCode = strArrayC[i];
//                result = channelProService.alldownChlPro(channelCode, proCode, riskType);
//            }
            result = channelProService.alldownChlPro(channelCodes, proCode, riskType);
            map.put("msg", result);
            return map;
        }
    }

    @RequestMapping("/up_list")
    @ResponseBody
    public String upallList(@ModelAttribute ChlInfo chlInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        int count = channelProService.countChlForUpDown(chlInfo.getChannelCode());
        if (count <= 0) return JSON.toJSONString(pageModel);
        List<ChlInfo> result = channelProService.getChlInfoList(chlInfo, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (ChlInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        logger.info(result.toString());
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

//    @RequestMapping("/down_list")
//    @ResponseBody
//    public String downallList(@ModelAttribute ChlInfo chlInfo, Integer pageIndex, Integer pageSize) {
//        PageModel pageModel = new PageModel();
//        int count = channelProService.countChlForUpDown(chlInfo.getChannelCode());
//        if (count <= 0) return JSON.toJSONString(pageModel);
//        List<ChlInfo> result = channelProService.getChlAllUpList(chlInfo, pageIndex, pageSize);
//        if (!CollectionUtils.isEmpty(result)) {
//            JSONArray array = new JSONArray();
//            for (ChlInfo pc : result) {
//                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
//                array.add(object);
//            }
//            pageModel.setList(array);
//        }
//        pageModel.setCount(count);
//        pageModel.setMsg("ok");
//        pageModel.setRel(true);
//        return JSON.toJSONString(pageModel);
//    }

    @RequestMapping("/linkProduct")
    @ResponseBody
    public Map<String, Object> linkProduct(@RequestParam String riskType) {
        Map<String, Object> map = new HashMap<>();
        List<SaleProInfo> list = new LinkedList<>();
        list = channelProService.getAllProInfo(riskType);
        logger.info(list);
        //如果险种没有对应的产品
        if (list.size() == 0) {
            map.put("msg", Chl_LINKPRO_NOT_EXIST.getMessage());
        }
        map.put("linkPro", list);
        return map;
    }

    @RequestMapping("/chlSysc")
    @ResponseBody
    public Map<String, Object> syncPro(@RequestParam(value = "channelName") String channelName) {
        Map<String, Object> map = new HashMap<>();
        map.put("msg", Chl_UNKNOWN_ERROR.getCode());
        //xinzeng仅供DAT
        String channelCode = channelName;
        List<ChlInfo> chlName = channelProService.getChlXXX(channelCode);
        if (chlName.size() == 0) {
            logger.error(channelName);
            map.put("msg", "请输入正确的渠道编码! ");
            return map;
        }

        try {
            //url配置到数据库中
//            String strUrl = "http://10.52.200.40:8569/dealData?channelCode=06";
//            String environment = environments.trim();
//            logger.info("运行环境: " + environment);
            String httpUrl = "channelsync";//对应ldcode1表中codetype
            String strUrl = channelProService.getHttpUrl(httpUrl);
            if (channelName != "") {
                if (regMatch(channelName)) {
                    strUrl += "?channelCode=" + channelName;
                } else {
                    logger.error(channelName);
                    map.put("msg", "请输入正确的渠道编码! ");
                    return map;
                }
            }
            logger.info("---------渠道开始同步---------");
            logger.info("channelsync: " + strUrl);
            HttpURLConnection back = getHttpURLConnection(strUrl);
            int status = back.getResponseCode();
            logger.info(status);
            String result = InputStreamTOString(back.getInputStream(), "UTF-8");
            logger.info("渠道同步返回结果集: " + result);
            //渠道和产品不同同一个人开发;返回值不同;此处特殊处理!!!
            if (result.contains("已经同步到中台") || result.contains("SUCCESS") || result.contains("渠道编码null")) {
                result = "success";
            }
            if (status == 200 & result.equals("success")) {
                map.put("msg", SUCCESS.getCode());
                logger.info("---------渠道同步成功---------");
            } else {
                map.put("msg", result);
                logger.error("---------渠道同步失败---------" + result);
            }
        } catch (IOException e) {
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            logger.error("---------渠道同步异常-------" + Chl_UNKNOWN_ERROR.getCode() + " :Connection refused");
            e.printStackTrace();
        }
        return map;
    }

    /**导出到excel
     * @param
     * @throws Exception
     */
    @RequestMapping(value="/excel")
    public ModelAndView exportExcel(@ModelAttribute ChlInfo chlInfo) throws Exception{
        ModelAndView mv = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<>();
        List<String> titles = new ArrayList<>();
        titles.add("序号"); //1
        titles.add("渠道编码"); //2
        titles.add("渠道名称"); //3
        titles.add("产品编码"); //4
        titles.add("产品名称"); //5
        titles.add("渠道产品上下架时间"); //6
        titles.add("渠道产品上下架状态"); //7
        dataMap.put("titles", titles);
        List<ChlInfo> varOList = channelProService.getChlProInfoExcel(chlInfo);
        List<Map> varList = new ArrayList<>();
        for(int i=0,j=1;i<varOList.size();i++){
            Map<String, Object> vpd = new HashMap();
            vpd.put("var1", String.valueOf(j++));        //1
            vpd.put("var2", varOList.get(i).getChannelCode());    //2
            vpd.put("var3", varOList.get(i).getChannelName()); //3
            vpd.put("var4", varOList.get(i).getProCode()); //4
            vpd.put("var5", varOList.get(i).getProName()); //5
            //创建时间改成上下架时间
//            vpd.put("var6", varOList.get(i).getCreateDate()); //6
//            logger.info("上下架状态 :"+varOList.get(i).getStatus());
            String flag = varOList.get(i).getStatus();//7
            if (flag.equals("1")) {
                vpd.put("var7", "下架");
                vpd.put("var6", varOList.get(i).getChannelProductDownDate());
            } else if (flag.equals("0")) {
                vpd.put("var7", "上架");
                vpd.put("var6", varOList.get(i).getChannelProductUpDate());
            }  else {
                vpd.put("var7", "");
                vpd.put("var6", "");
            }
            varList.add(vpd);
        }

        dataMap.put("varList", varList);
        ObjectExcelView erv = new ObjectExcelView();
        mv = new ModelAndView(erv, dataMap);
        logger.info("---------EXCEL_OVER---------");
        return mv;
    }

}
