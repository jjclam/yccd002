package com.sinosoft.cloud.yfb.service;

import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrpToPlan;
import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeRisk;


import java.util.List;

public interface SchemeAuthorizeService {

    int countSchemeAuthorize(LACom laCom);

    List<LACom> getSchemeAuthorizeList(LACom laCom, Integer pageIndex, Integer pageSize);

    List<LDGrpToPlan> gethavenAuthorize(String customerno);

    int countAllSchemeAuthorize(LDSchemeRisk ldschemerisk);

    List<LDSchemeRisk> getAllSchemeAuthorizeList(LDSchemeRisk ldschemerisk, Integer pageIndex, Integer pageSize);

    String authorizeSchemes(List<LDGrpToPlan> saveArray, String operator);


    String deleteAuthorizedScheme(String customerno, String plancode);
}
