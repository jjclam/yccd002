package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.FunctDefMapper;
import com.sinosoft.cloud.dal.dao.mapper.LdJobRunLogMapper;
import com.sinosoft.cloud.dal.dao.model.entity.LMCalModePojo;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;
import com.sinosoft.cloud.mgr.service.FunctDefService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FunctDefServiceImpl implements FunctDefService {
    private final Log logger = LogFactory.getLog(this.getClass());
//    @Autowired
//    LdJobRunLogMapper ldJobRunLogMapper;
    @Autowired
    FunctDefMapper functDefMapper;
            ;
    @Override
    public List<LMCalModePojo> getResultByTaskName(LMCalModePojo lMCalModePojo, Integer pageIndex, Integer pageSize) {
        logger.info("--pageIndex："+pageIndex+"--pageSize："+pageSize);
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
//        map.put("serialNo", lMCalModePojo.getCalCode());
        map.put("calCode", lMCalModePojo.getCalCode());
        map.put("type", lMCalModePojo.getType());
        map.put("remark", lMCalModePojo.getRemark());
        map.put("calSql", lMCalModePojo.getCalSQL());
        return functDefMapper.selectByTaskName(map);
    }

    @Override
    public List<LMCalModePojo> getCodes() {
        return functDefMapper.getCodes();
    }
    @Override
    public Integer countForList(LMCalModePojo lMCalModePojo) {
        Map<String, Object> map = new HashMap<>();
        map.put("calCode", lMCalModePojo.getCalCode());
        map.put("type", lMCalModePojo.getType());
        map.put("remark", lMCalModePojo.getRemark());
        map.put("calSql", lMCalModePojo.getCalSQL());
        return functDefMapper.countForList(map);
    }
}
