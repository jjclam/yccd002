package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.*;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.LDCode1;
import com.sinosoft.cloud.dal.dao.model.entity.AgentInfo;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.LAComLiAn;
import com.sinosoft.cloud.dal.dao.model.open_LaCom;
import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.mgr.service.ChannelInfoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;

@Service
public class ChannelInfoServiceImpl implements ChannelInfoService {
    private final static Log logger = LogFactory.getLog(ChannelInfoServiceImpl.class);
    @Autowired
    private LAComMapper laComMapper;
    @Autowired
    private LAChannelAgentMapper laChannelAgentMapper;
    @Autowired
    private open_LaComMapper open_laComMapper;
    @Autowired
    private LDCodeMapper ldCodeMapper;
    @Autowired
    private LDCode1Mapper ldCode1Mapper;

    @Override
    public Integer count() {
        return laComMapper.countByExample();
    }

    @Override
    public Integer countChlInfoInfo(ChlInfo chlInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put("agencyCode", chlInfo.getAgencyCode());
        map.put("agencyName", chlInfo.getAgencyName());
        map.put("agentCode", chlInfo.getAgentCode());
        String agencyType = chlInfo.getAgencyType().split("-")[0];
        map.put("agencyType", agencyType);
        map.put("licenseNo", chlInfo.getLicenseNo());
//        String channelCode = chlInfo.getChannelCode().split("-")[0];
//        map.put("channelCode", channelCode);
//        map.put("channelName", chlInfo.getChannelName());
        logger.info("List<ChlInfo>: " + map);
//        return laComMapper.countByChlInfo(map);
        return laChannelAgentMapper.countByChlInfo(map);
    }

    @Override
    public List<ChlInfo> getChannelInfoList(ChlInfo chlInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("agencyCode", chlInfo.getAgencyCode());
        map.put("agencyName", chlInfo.getAgencyName());
        map.put("agentCode", chlInfo.getAgentCode());
        String agencyType = chlInfo.getAgencyType().split("-")[0];
        map.put("agencyType", agencyType);
        map.put("licenseNo", chlInfo.getLicenseNo());
//        String[] channelCode = chlInfo.getChannelCode().split("-");
//        map.put("channelCode", channelCode[0]);
        logger.info("List<ChlInfo>: " + map);
        return laChannelAgentMapper.selectForChlInfo(map);
    }

    @Override
    public AgentInfo getDetailByAgencyCode(String agencyCode) {
        return laComMapper.selectAgentInfoByAgentCode(agencyCode);
    }

    @Override
    public open_LaCom getComDetailByAgencyCode(String agencyCode) {
        return open_laComMapper.selectComInfoByAgentCode(agencyCode);
    }

    @Override
    public List<LDCode> getChlLargeType() {
        return ldCodeMapper.getChlLargeTypeList();
    }

    @Override
    public List<LDCode1> getChlChildType() {
        return ldCode1Mapper.getChlChildTypeList();
    }

    @Override
    public List<LDCode1> getAllChlChildType(String chlTypeL) {
        return ldCode1Mapper.getAllChlChildType(chlTypeL);
    }

    @Override
    public List<ChlInfo> getAgencyInfo() {
        return laComMapper.selectForAgencyInfo();
    }

    @Override
    public Integer countForChlMaintainList(String chlTypeL, String chlTypeC) {
        return ldCode1Mapper.countForChlMaintainList(chlTypeL, chlTypeC);
    }

    @Override
    public List<LDCode1> getChlMaintainList(String chlTypeL, String chlTypeC, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        return ldCode1Mapper.getChlMaintainList(chlTypeL, chlTypeC);
    }

    @Override
    public String addChlMaintain(JSONObject po) {
        String chlTypeL = po.getString("chlTypeL");
//        String chlTypeLName = po.getString("chlTypeLName");
        String chlTypeC = po.getString("chlTypeC");
        String chlTypeCName = po.getString("chlTypeCName");

        if (chlTypeL == "" || chlTypeC == "" || chlTypeCName == "") {
            logger.error(PARAM_NOT_FOUND.getMessage());
            return PARAM_NOT_FOUND.getMessage();
        }

        int addcodetypeOk = 0;
        String result = "";
        Map<String, Object> map = new HashMap<>();
        map.put("code", chlTypeL);
        map.put("code1", chlTypeC);
        //判断中台是否配置过渠道;
        int flag = ldCode1Mapper.verifyChlExist(map);
        if (flag == 1) {
            result = CM_CHLCODE_EXIST.getMessage();
            logger.info("addChlMaintain: " + CM_CHLCODE_EXIST.getMessage());
            return result;
        }

        map.put("codeName", chlTypeCName);
        logger.info("addChlMaintain(map):" + map);
        int addFlag = ldCode1Mapper.addChlMaintain(map);
        if (addFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("addChlMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("addChlMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }

    //AGENTCOM, MANAGECOM, AREATYPE, CHANNELTYPE, NAME, PHONE, EMAIL,OPERATOR
    //{"agentcom":"123123","agentName":"12312312","email":"12312321@qq.com","telephone":"15698726517","linkedPhone":"12312312","wangbit":"86-利安人寿保险股份有限公司"}
    @Transactional(propagation= Propagation.SUPPORTS,readOnly=true)
    @Override
    public String addwangBit(JSONObject po){
        Map<String, Object> map = new HashMap<>();
        String result = "";
        LACom mLacom = new LACom();
        String wangbit = po.getString("wangbit");
        mLacom.setAGENTCOM(po.getString("agentcom"));
        if (wangbit!=null&&(!("".equals(wangbit)))){
            String[] split = wangbit.split("-");
            mLacom.setMANAGECOM((split[0]));
        }
        mLacom.setEMAIL(po.getString("email"));
        mLacom.setPHONE(po.getString("telephone"));
        mLacom.setNAME(po.getString("agentName"));
        String chlTypeLer = po.getString("chlTypeLer");
        if (chlTypeLer!=null&&(!("".equals(chlTypeLer)))){
            String[] splitChl = chlTypeLer.split("-");
            mLacom.setBRANCHTYPE2(splitChl[0]);
        }
        map.put("AGENTCOM",mLacom.getAGENTCOM());
        map.put("MANAGECOM",mLacom.getMANAGECOM());
        map.put("NAME",mLacom.getNAME());
        map.put("PHONE",mLacom.getPHONE());
        map.put("AGENTCOM",mLacom.getEMAIL());

        String chelo = laChannelAgentMapper.selectWangbit2(mLacom);
        if (chelo!=null||(!("".equals(chelo)))){
            result = "该网点信息已配置！";
            logger.info("addChlMaintain: "+result);
        }
        int addFlag = laChannelAgentMapper.addWangBit2(mLacom);
        if (addFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("addChlMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("addChlMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }


    @Override
    public String deleteChlMaintain(String chlTypeL, String chlTypeC) {
        String result = "";
        int flag = ldCode1Mapper.deleteChlMaintain(chlTypeL, chlTypeC);
        if (flag == 1) {
            result = SUCCESS.getCode();
            logger.info("deleteChlMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("deleteChlMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    public Integer countForChlAgencyMaintainList(String agencyCode, String agencyName,String chlTypeL) {
        return laChannelAgentMapper.countForChlAgencyMaintainList(agencyCode, agencyName,chlTypeL);
    }

    @Override
    public List<LAComLiAn> getChlAgencyMaintainList(String agencyCode, String agencyName, String chlTypeL, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        return laChannelAgentMapper.selectChlAgencyMaintainList(agencyCode, agencyName,chlTypeL);
    }

    @Override
    public String addChlAgencyMaintain(JSONObject po) {
        String channelName = po.getString("channelName");
        String agencyName = po.getString("agencyName");
        if (StringUtils.isBlank(channelName) || StringUtils.isBlank(agencyName)) {
            logger.error(PARAM_NOT_FOUND.getMessage());
            return PARAM_NOT_FOUND.getMessage();
        }
        Map<String, Object> map = new HashMap<>();
        String channelCode = channelName.split("-")[0];
        String channelNameJ = channelName.split("-")[1];
        map.put("channelCode", channelCode);
        map.put("channelName", channelNameJ);
        String agencyCode = agencyName.split("-")[0];
        map.put("agencyCode", agencyCode);

        //判断中台是否配置过渠道中介关系;
        String result = "";
        int flag = laChannelAgentMapper.verifyChannelAgent(map);
        if (flag == 1) {
            result = CM_CHLAGENCYLINKED_EXIST.getMessage();
            logger.info("addChlAgencyMaintain: " + CM_CHLAGENCYLINKED_EXIST.getMessage());
            return result;
        }

        String createDate = getCurrentDefaultTimeStr();
        map.put("createDate", createDate);
        int addFlag = laChannelAgentMapper.insertChlAgencyMaintain(map);
        if (addFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("addChlAgencyMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("addChlAgencyMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    public String deleteChlAgencyMaintain(String agencyName, String channelName) {
        String result = "";
        int flag = laChannelAgentMapper.deleteChlAgencyMaintain(agencyName, channelName);
        if (flag == 1) {
            result = SUCCESS.getCode();
            logger.info("deleteChlAgencyMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("deleteChlAgencyMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }
}
