package com.sinosoft.cloud.yfb.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.yfbDao.mapper.LDGrpMapper;
import com.sinosoft.cloud.dal.yfbDao.mapper.LDGrpToPlanMapper;
import com.sinosoft.cloud.dal.yfbDao.mapper.LDSchemeRiskMapper;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrpToPlan;
import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeRisk;
import com.sinosoft.cloud.yfb.service.SchemeAuthorizeService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;

@Service
public class SchemeAuthorizeServiceImpl implements SchemeAuthorizeService {
    private final static Log logger = LogFactory.getLog(SchemeAuthorizeServiceImpl.class);
    @Autowired
    private LDGrpMapper ldGrpMapper;
    @Autowired
    private LDGrpToPlanMapper ldGrpToPlanMapper;
    @Autowired
    private LDSchemeRiskMapper ldSchemeRiskMapper;

    @Override
    public int countSchemeAuthorize(LACom laCom) {
        Map<String, Object> map = new HashMap<>();
        String NAME = laCom.getNAME();
        map.put("NAME", NAME);
        return ldGrpToPlanMapper.countSchemeAuthorize(map);
    }

    @Override
    public List<LACom> getSchemeAuthorizeList(LACom laCom, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        String NAME = laCom.getNAME();
        map.put("NAME", NAME);
        return ldGrpToPlanMapper.getSchemeAuthorizeList(map);
    }

    @Override
    public List<LDGrpToPlan> gethavenAuthorize(String customerno) {
        List<LDGrpToPlan> ldGrpToPlans = ldGrpToPlanMapper.gethavenAuthorize(customerno);
//        for (LDGrpToPlan map : ldGrpToPlans) {
//            map.setMaketime(map.getMakedate().toString());
//        }
        return ldGrpToPlans;
    }

    @Override
    public int countAllSchemeAuthorize(LDSchemeRisk ldschemerisk) {
        Map<String, Object> map = new HashMap<>();
        String schemecode = ldschemerisk.getContschemecode();
        String schemename = ldschemerisk.getContschemename();
        map.put("schemecode", schemecode);
        map.put("schemename", schemename);
        return ldSchemeRiskMapper.countAllSchemeAuthorize(map);
    }

    @Override
    public List<LDSchemeRisk> getAllSchemeAuthorizeList(LDSchemeRisk ldschemerisk, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        String schemecode = ldschemerisk.getContschemecode();
        String schemename = ldschemerisk.getContschemename();
        map.put("schemecode", schemecode);
        map.put("schemename", schemename);
        return ldSchemeRiskMapper.getAllSchemeAuthorizeList(map);
    }

    @Override
    @Transactional
    public String authorizeSchemes(List<LDGrpToPlan> saveArray, String operator) {
        String result = "";
        int saveFlag = 1;
        String createDate = getCurrentDefaultTimeStr();
        String date = createDate.split(" ")[0];
        String time = createDate.split(" ")[1];
        for (LDGrpToPlan schemes : saveArray) {
            Map<String, Object> map = new HashMap<>();
            System.out.println("plancode:"+schemes.getPlanCode ());
            System.out.println("planName:"+schemes.getPlanName());
            map.put("customerno", schemes.getCustomerNo());
            map.put("plancode", schemes.getPlanCode());
            map.put("planname", schemes.getPlanName());
            map.put("operator", operator);
            map.put("date", date);
            map.put("time", time);
            if("".equals (schemes.getStartDate ())||schemes.getStartDate ()==null||"".equals (schemes.getEndDate ())||schemes.getEndDate ()==null){
                result= Chl_DATE_NOT_EXIST.getMessage ( );
                return result;
            }
            map.put("startdate", schemes.getStartDate ());
            map.put("enddate", schemes.getEndDate ());
            logger.info("authorizeSchemes(map): " + map);
            ldGrpToPlanMapper.deleteAuthorizedScheme (schemes.getCustomerNo (),schemes.getPlanCode ());
            saveFlag = ldGrpToPlanMapper.authorizeSchemes(map);
            if (saveFlag == 1) {
                result = SUCCESS.getCode();
                logger.info("authorizeSchemes: " + SUCCESS.getMessage());
            } else {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                result = DB_FAIL.getMessage();
                logger.info("authorizeSchemes: " + map + DB_FAIL.getMessage());
                return result;
            }
        }
        return result;
    }

    @Override
    public String deleteAuthorizedScheme(String customerno, String plancode) {
        String result = "";
        int deleteFlag = ldGrpToPlanMapper.deleteAuthorizedScheme(customerno, plancode);
        if (deleteFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("deleteAuthorizedScheme: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("deleteAuthorizedScheme: " + DB_FAIL.getMessage());
            return result;
        }
        return result;
    }
}
