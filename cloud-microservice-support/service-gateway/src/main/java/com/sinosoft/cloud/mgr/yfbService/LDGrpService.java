package com.sinosoft.cloud.mgr.yfbService;

import com.sinosoft.cloud.dal.dao.model.entity.LMCalModePojo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LDGrpService {

    List<LMCalModePojo> selectByNoParam(@Param("LMCalModePojo") LMCalModePojo lmCalModePojo, Integer pageIndex, Integer pageSize);
    int SelectGrpCount(@Param("lmCalModePojo") LMCalModePojo lmCalModePojo);

    void insert(@Param("lmCalModePojo")LMCalModePojo lmCalModePojo);
}
