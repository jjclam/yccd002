package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LAChannelProductMapper;
import com.sinosoft.cloud.dal.dao.mapper.LAComMapper;
import com.sinosoft.cloud.dal.dao.mapper.LDCodeMapper;
import com.sinosoft.cloud.dal.dao.mapper.LMRiskMapper;
import com.sinosoft.cloud.dal.dao.model.LAChannelProduct;
import com.sinosoft.cloud.dal.dao.model.LAChannelProductExample;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SaleProInfo;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.DateUtil.*;
import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;
import static com.sinosoft.cloud.common.util.RegUtil.regMatch;

@Service
public class LcgrpContServiceImpl {
    private final static Log logger = LogFactory.getLog(LcgrpContServiceImpl.class);
    @Autowired
    private LAComMapper laComMapper;
    @Autowired
    private LMRiskMapper tLMRiskMapper;
    @Autowired
    private LAChannelProductMapper laChannelProductMapper;
    @Autowired
    private LDCodeMapper mLDCodeMapper;

    public List<ChlInfo> getChlXXX(String channelCode) {
        return laChannelProductMapper.verifyChlExist(channelCode);
    }

//    @Override
//    public List<ChlInfo> findCirlBySe() {
//        List<ChlInfo> chlInfos = mLDCodeMapper.selectCirlBySe();
//        return chlInfos;
//    }


}
