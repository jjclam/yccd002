package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.LDCode;

import java.util.List;

public interface NewBusinessService {
    String saveCont(String params,String opreator);


    List<LDCode> getIDtype();

    List<LDCode> getRelation();

}
