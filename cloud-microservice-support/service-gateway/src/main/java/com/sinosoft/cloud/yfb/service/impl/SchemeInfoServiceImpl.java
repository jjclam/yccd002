package com.sinosoft.cloud.yfb.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.yfbDao.mapper.LDSchemeDutyParamMapper;
import com.sinosoft.cloud.dal.yfbDao.mapper.LDSchemeRiskMapper;
import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeDutyParam;
import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeRisk;
import com.sinosoft.cloud.dal.yfbDao.model.entity.SchemeParams;
import com.sinosoft.cloud.yfb.service.SchemeInfoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SchemeInfoServiceImpl implements SchemeInfoService {
    private final static Log logger = LogFactory.getLog(SchemeInfoServiceImpl.class);
    @Autowired
    private LDSchemeRiskMapper ldSchemeRiskMapper;
    @Autowired
    private LDSchemeDutyParamMapper ldSchemeDutyParamMapper;

    @Override
    public int countSchemeInfo(LDSchemeRisk scheme) {
        Map<String, Object> map = new HashMap<>();
        String schemecode = scheme.getContschemecode();
        String schemename = scheme.getContschemename();
        map.put("schemecode", schemecode);
        map.put("schemename", schemename);
        return ldSchemeRiskMapper.countSchemeInfo(map);
    }

    @Override
    public List<LDSchemeRisk> getSchemeInfoList(LDSchemeRisk scheme, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        String schemecode = scheme.getContschemecode();
        String schemename = scheme.getContschemename();
        map.put("schemecode", schemecode);
        map.put("schemename", schemename);
        logger.info(map);
        return ldSchemeRiskMapper.getSchemeInfoList(map);
    }

    @Override
    public int countSchemeInfoForView(SchemeParams scheme) {
        String schemecode = scheme.getContschemecode();
        return ldSchemeDutyParamMapper.countSchemeInfoForView(schemecode);
    }

    @Override
    public List<SchemeParams> getSchemeInfoListForView(SchemeParams scheme, Integer pageIndex, Integer pageSize) {
        String schemecode = scheme.getContschemecode();
        logger.info(scheme);
        ArrayList<SchemeParams> list = new ArrayList<>();
        int count = 0;
        Double Prems = 0.0;
        List<LDSchemeDutyParam> riskCodes = ldSchemeDutyParamMapper.getSchemeInfoListForRiskCode(schemecode);
        for (LDSchemeDutyParam riskCode : riskCodes) {
            List<LDSchemeDutyParam> dutycodes = ldSchemeDutyParamMapper.getSchemeInfoListForDutycode(schemecode, riskCode.getRiskcode());
            for (LDSchemeDutyParam dutyCode : dutycodes) {
                SchemeParams schemeParams = new SchemeParams();
                List<LDSchemeDutyParam> params = ldSchemeDutyParamMapper.getSchemeInfoListForViewParams(schemecode, riskCode.getRiskcode(), dutyCode.getDutycode());
                for (LDSchemeDutyParam param : params) {
                    String factor = param.getCalfactor();
                    String value = param.getCalfactorvalue();
                    if (factor.equals("Amnt")) schemeParams.setAmnt(value);
                    if (factor.equals("GetLimit")) schemeParams.setGetLimit(value);
                    if (factor.equals("NoGetDay")) schemeParams.setNoGetDay(value);
                    if (factor.equals("GetLimitType")) schemeParams.setGetLimitType(value);
                    if (factor.equals("InsuYear")) schemeParams.setInsuYear(value);
                    if (factor.equals("Prem") && !value.equals(null)) {
                        schemeParams.setPrem(value);
                        Prems = Double.valueOf(schemeParams.getPrem()) + Prems;
                    }

                }
                schemeParams.setRiskcode(riskCode.getRiskcode());
                schemeParams.setDutyCode(dutyCode.getDutycode());
                schemeParams.setRemark(dutyCode.getDutycode());
                count++;
                schemeParams.setId(String.valueOf(count));
                schemeParams.setPrem(Prems.toString());
                logger.info(schemeParams);
                list.add(schemeParams);
            }
//            count++;
//            schemeParams.setId(String.valueOf(count));
//            schemeParams.setPrem(Prems.toString());
//            logger.info(schemeParams);
//            list.add(schemeParams);
        }
        return list;
    }

    @Override
    public SchemeParams getSchemeInfoListForParams(String schemecode) {
        return ldSchemeDutyParamMapper.getSchemeInfoListForParams(schemecode);
    }


}
