package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.common.util.RpcSignUtils;
import com.sinosoft.cloud.dal.dao.mapper.SysUserMapper;
import com.sinosoft.cloud.dal.dao.model.entity.SysUser;
import com.sinosoft.cloud.dal.yfbDao.mapper.LDGrpMapper;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrp;
import com.sinosoft.cloud.mgr.Filter.SecurityUtils;
import com.sinosoft.cloud.mgr.service.UserInfoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;

@Service
public class UserInfoServiceImpl implements UserInfoService {
    private final static Log logger = LogFactory.getLog(UserInfoServiceImpl.class);
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private LDGrpMapper ldGrpMapper;

    @Override
    public List<SysUser> getUserPosition(String field) {
        return sysUserMapper.getUserPosition(field);
    }

    @Override
    public int countUserInfo(SysUser sysUser) {
        Map<String, Object> map = new HashMap<>();
        String userName = sysUser.getUserName();
        String realName = sysUser.getRealName();
        String field = sysUser.getField();
        String position = sysUser.getPosition();
        String institutions = sysUser.getInstitutions();
        map.put("userName", userName);
        map.put("realName", realName);
        map.put("field", field);
        map.put("position", position);
        map.put("institutions", institutions);
        return sysUserMapper.countUserByName(map);
    }

    @Override
    public List<SysUser> getUserInfoList(SysUser sysUser, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        String userName = sysUser.getUserName();
        String realName = sysUser.getRealName();
        String position = sysUser.getPosition();
        String institutions = sysUser.getInstitutions();
        String field = sysUser.getField();
        map.put("userName", userName);
        map.put("realName", realName);
        map.put("field", field);
        map.put("position", position);
        map.put("institutions", institutions);

        return sysUserMapper.getUserInfoList(map);
    }

    @Override
    public SysUser getDetailByuserName(String userNames) {
        return sysUserMapper.getUserInfoForEdit(userNames);
    }

    @Override
    @Transactional
    public String addUser(JSONObject po) {
        String userName = po.getString("userName");
        String password = po.getString("password");
        String realName = po.getString("realName");
        String userType = po.getString("userType");
        String institutions = po.getString("institutions");
        String field = po.getString("field");
        String email = po.getString("email");
        String operator = po.getString("operator");
        String position = po.getString("position");
        String wangbit = po.getString("wangbit");
            //利安新增属性
        String remark = po.getString("describe");
        String agentcode = po.getString("laagent");
        String  agentcom = po.getString("ldcom");
        String validstartdate = po.getString("startDate");
        String validenddate = po.getString("endDate");
        String ukeyflag = po.getString("ukey");
        if (wangbit!=null&&(!("".equals(wangbit)))){
            String[] split = wangbit.split("-");
            wangbit = split[0];
        }
        if (agentcom!=null&&(!("".equals(agentcom)))){
            String[] split = agentcom.split("-");
            agentcom = split[0];
        }
        if (agentcode!=null&&(!("".equals(agentcode)))){
            String[] split = agentcode.split("-");
            agentcode = split[0];
        }


        /**   Add By Xcc  暂存usertype   */
        String positionId = "";
        if (!position.equals("")) {
            positionId = position.split("-")[0];
            position = position.split("-")[1];
        }
        String telephone = po.getString("telephone");
        String linkedPhone = po.getString("linkedPhone");

        if (userName == "" || password == "" || realName == "" ) {//position改为必录//改为非必录去掉|| position == ""
            logger.error(PARAM_NOT_FOUND.getMessage());
            return PARAM_NOT_FOUND.getMessage();
        }

        //判断用户名是否已存在
        int flag = sysUserMapper.verifyUser(userName);
        if (flag == 1) {
            logger.error("addUser: " + DB_USER_EXIST.getMessage());
            return DB_USER_EXIST.getMessage();
        }

        Map<String, Object> map = new HashMap<>();
        map.put("userName", userName);
        //中台用户和外勤加密方式不同
        if (field.equals("0") || field.equals("1")) {
            password = SecurityUtils.encryptPassword(password);
        } else if (field.equals("2")) {
            password = RpcSignUtils.sha1(password);
        }
        map.put("password", password);
        map.put("realName", realName);
        map.put("userType", userType);
        map.put("institutions", wangbit);
        map.put("field", field);
        map.put("email", email);
        map.put("telephone", telephone);
        map.put("linkedPhone", linkedPhone);
        map.put("operator", operator);
        String createDate = getCurrentDefaultTimeStr();
        map.put("createDate", createDate);

        map.put("remark", remark);
        map.put("agentcode", agentcode);
        map.put("agentcom", agentcom);
        map.put("validstartdate", validstartdate);
        map.put("validenddate", validenddate);
        map.put("ukeyflag", ukeyflag);

        logger.info("addUser(map):" + map);
        int addFlag = sysUserMapper.addUser(map);
        String result = "";
        if (addFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("addUser: " + SUCCESS.getMessage());
        } else {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            result = DB_FAIL.getMessage();
            logger.info("addUser: " + DB_FAIL.getMessage());
            return result;
        }

        //为权限管理配置岗位关系
        if (!position.equals("")) {
            SysUser sysUser = new SysUser();
            sysUser = sysUserMapper.selectUserIdForUserPosition(userName);
            String userId = sysUser.getId();
            int addUserPositionFlag = sysUserMapper.insertUserPosition(userId, positionId);
            if (addUserPositionFlag != 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                result = DB_FAIL.getMessage();
                logger.info("addUserIdForUserPosition: " + DB_FAIL.getMessage());
                return result;
            }
        }
        return result;
    }

    @Override
    @Transactional
    public String updateUser(JSONObject po) {
        String userName = po.getString("userName");
        String oldUserName = po.getString("oldUserName");
        String password = po.getString("password");
        String realName = po.getString("realName");
        String userType = po.getString("userType");
        String institutions = po.getString("institutions");
        if (institutions!=null&&(!("".equals(institutions)))){
            int i = institutions.indexOf("-");
            if(i!=-1){
                String[] split = institutions.split("-");
                institutions=split[0];
            }
        }
        String field = po.getString("field");
        String status = po.getString("status");
        String email = po.getString("email");
        String operator = po.getString("operator");
        String position = po.getString("position");
        String positionId = "";
        if (!position.equals("")) {
            positionId = position.split("-")[0];
            position = position.split("-")[1];
        }
//        if (!field.equals("")) {
//            field = field.split("-")[0];
//        }
        String telephone = po.getString("telephone");
        String linkedPhone = po.getString("linkedPhone");

        if (userName == "" || password == "" || realName == "") {//|| position == ""
            logger.error(PARAM_NOT_FOUND.getMessage());
            return PARAM_NOT_FOUND.getMessage();
        }

        //利安新增属性
        String remark = po.getString("describe");
        String agentcode = po.getString("laagent");
        String  agentcom = po.getString("ldcom");
        String validstartdate = po.getString("startDate");
        String validenddate = po.getString("endDate");
        String ukeyflag = po.getString("ukey");

        if (agentcom!=null&&(!("".equals(agentcom)))){
            String[] split = agentcom.split("-");
            agentcom = split[0];
        }
        if (agentcode!=null&&(!("".equals(agentcode)))){
            String[] split = agentcode.split("-");
            agentcode = split[0];
        }

        Map<String, Object> map = new HashMap<>();
        map.put("userName", userName);
        String userId = po.getString("userId");
        map.put("userId", userId);
        //中台用户和外勤加密方式不同
        if (field.equals("0") || field.equals("1")) {
            password = SecurityUtils.encryptPassword(password);
        } else if (field.equals("2")) {
            password = RpcSignUtils.sha1(password);
        }
        map.put("password", password);

        map.put("realName", realName);
        map.put("userType", userType);
        map.put("institutions", institutions);
        map.put("field", field);
        map.put("status", status);
        map.put("email", email);
        map.put("operator", operator);
        map.put("position", position);
        map.put("telephone", telephone);
        map.put("linkedPhone", linkedPhone);
        String updateDate = getCurrentDefaultTimeStr();
        map.put("updateDate", updateDate);

        map.put("remark", remark);
        map.put("agentcode", agentcode);
        map.put("agentcom", agentcom);
        map.put("validstartdate", validstartdate);
        map.put("validenddate", validenddate);
        map.put("ukeyflag", ukeyflag);
        logger.info("updateUser(map):" + map);
        String result = "";

        //判断用户名是否已存在
        if (!oldUserName.equals(userName)) { //username修改
            int flag = sysUserMapper.verifyUser(userName);
            if (flag == 1) {
                logger.error("updateUser: " + DB_USER_EXIST.getMessage());
                return DB_USER_EXIST.getMessage();
            }
        }

        int updateFlag = sysUserMapper.updateUser(map);
        if (updateFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("updateUser: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("updateUser: " + DB_FAIL.getMessage());
        }

        //为权限管理配置岗位关系
        SysUser sysUser = sysUserMapper.selectUserIdForUserPosition(userName);
        String roleId = sysUser.getId();
        sysUserMapper.deleteUserPosition(roleId, positionId);
        if (!position.equals("")) {
            //---
            int addUserPositionFlag = sysUserMapper.insertUserPosition(userId, positionId);
            if (addUserPositionFlag != 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                result = DB_FAIL.getMessage();
                logger.info("updateUserPosition: " + DB_FAIL.getMessage());
                return result;
            }
        }
        return result;
    }

    @Override
    @Transactional
    public String deleteUser(String userNames) {
        String[] strArrayU = userNames.split(",");
        String result = "";
        String userName = "";
        for (int i = 0; i < strArrayU.length; i++) {
            userName = strArrayU[i];
            //用户表
            int flag = sysUserMapper.deleteUser(userName);
            if (flag == 1) {
                result = SUCCESS.getCode();
                logger.info("deleteUser: " + SUCCESS.getMessage());
            } else {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                result = DB_FAIL.getMessage();
                logger.info("deleteUser: " + DB_FAIL.getMessage());
                return result;
            }
            //删除用户角色关系
            int roleFlag = sysUserMapper.deleteUserRole(userName);
//            if (roleFlag == 1) {
//                result = SUCCESS.getCode();
//                logger.info("deleteUserRole: " + SUCCESS.getMessage());
//            } else {
//                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//                result = DB_FAIL.getMessage();
//                logger.info("deleteUserRole: " + DB_FAIL.getMessage());
//                return result;
//            }
        }
        return result;
    }

    @Override
    public List<LDGrp> getAllInstitutions(String field) {
        List<LDGrp> institutions = new ArrayList<>();
        if (field.equals("0")) { //外勤
            institutions = ldGrpMapper.getAllInstitutionsNameByOutWorker();
        } else if (field.equals("1")) { //企业
            institutions = ldGrpMapper.getAllInstitutionsNameByEnterprise();
        }
        return institutions;
    }
}
