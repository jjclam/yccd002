package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.LDPlan;

import java.util.List;

public interface MealAuthService {
    List<LDPlan> getResultByTaskName(LDPlan lDPlan, Integer pageIndex, Integer pageSize);
    List<LDPlan> getCodes();
    Integer countForList(LDPlan lDPlan);
    List<LACom> getMngCode();

}
