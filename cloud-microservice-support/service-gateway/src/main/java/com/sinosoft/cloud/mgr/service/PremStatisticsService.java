package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.entity.Operation;

import java.util.List;

public interface PremStatisticsService {
    int countForPremCountList(Operation lcCont);

    List<Operation> getPremCountList(Operation lcCont, Integer pageIndex, Integer pageSize);
}
