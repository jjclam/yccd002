package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.LDPlan;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.MealAuthService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/pro_mng")
public class MealAuthController {
    private final Log logger = LogFactory.getLog(this.getClass());

@Autowired
private MealAuthService mealAuthService;

    @RequestMapping("/mealauth.html")
    public String listInput(ModelMap model) {
        List<LDPlan> cal = mealAuthService.getCodes();
        List<LACom> MngCode = mealAuthService.getMngCode();

        model.addAttribute("cal", cal);
        model.addAttribute("MngCode", MngCode);
        return "pro_mng/mealauth";
    }

    @RequestMapping("/mealauth")
    @ResponseBody
    public String list(@ModelAttribute LDPlan lDPlan, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
//        String calCode = lDPlan.getCONTPLANCODE();

//        if (calCode == null){
//            pageModel.setMsg("");
//            return JSON.toJSONString(pageModel);
//        }
        //分页计数
        int count = mealAuthService.countForList(lDPlan);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }

        List<LDPlan> result =  mealAuthService.getResultByTaskName(lDPlan,pageIndex,pageSize);
        pageModel.setList(result);
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }
}
