package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LZCardInfo;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;

import java.util.List;

public interface DocManageService {
    List<LdJobRunLog> getResultByTaskName(LdJobRunLog ldJobRunLog, Integer pageIndex, Integer pageSize);
    List<LDCode> getCodes();
    List<LDCode> getStates();
    Integer countForList(LdJobRunLog ldJobRunLog);

    String documentBack(String params);

    String documentBackSale(String params);
}
