package com.sinosoft.cloud.mgr.common;

public class GrpClient {
    private String unitName;
    private String unitAttr;
    private String unitType;
    private String unitCount;
    private String jobCount;
    private String appntCount;
    private String unitPapersType;
    private String unitPapersNumber;
    private String unitPapersLimit;

    public GrpClient(){}

    public GrpClient(String unitName, String unitAttr, String unitType, String unitCount, String jobCount, String appntCount, String unitPapersType, String unitPapersNumber, String unitPapersLimit) {
        this.unitName = unitName;
        this.unitAttr = unitAttr;
        this.unitType = unitType;
        this.unitCount = unitCount;
        this.jobCount = jobCount;
        this.appntCount = appntCount;
        this.unitPapersType = unitPapersType;
        this.unitPapersNumber = unitPapersNumber;
        this.unitPapersLimit = unitPapersLimit;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitAttr() {
        return unitAttr;
    }

    public void setUnitAttr(String unitAttr) {
        this.unitAttr = unitAttr;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getUnitCount() {
        return unitCount;
    }

    public void setUnitCount(String unitCount) {
        this.unitCount = unitCount;
    }

    public String getJobCount() {
        return jobCount;
    }

    public void setJobCount(String jobCount) {
        this.jobCount = jobCount;
    }

    public String getAppntCount() {
        return appntCount;
    }

    public void setAppntCount(String appntCount) {
        this.appntCount = appntCount;
    }

    public String getUnitPapersType() {
        return unitPapersType;
    }

    public void setUnitPapersType(String unitPapersType) {
        this.unitPapersType = unitPapersType;
    }

    public String getUnitPapersNumber() {
        return unitPapersNumber;
    }

    public void setUnitPapersNumber(String unitPapersNumber) {
        this.unitPapersNumber = unitPapersNumber;
    }

    public String getUnitPapersLimit() {
        return unitPapersLimit;
    }

    public void setUnitPapersLimit(String unitPapersLimit) {
        this.unitPapersLimit = unitPapersLimit;
    }
}
