package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.LDCode1;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ChannelMaintainService;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.CodeMaintainService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.CM_LINKCODETYPE_NOT_EXIST;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.Chl_UNKNOWN_ERROR;

/**
 * 代码维护
 * 渠道
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/chl_maintain")
public class ChannelMaintainController {
    private final static Log logger = LogFactory.getLog(ChannelMaintainController.class);
    @Autowired
    private ChannelMaintainService channelMaintainService;
    @Autowired
    private CodeMaintainService codeMaintainService;
    @Autowired
    private ChannelProService channelProService;

    @RequestMapping("/chl_list.html")
    public String chl_list(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
//        List<LDCode> laCodeType = codeMaintainService.getCodeTypeList();
//        model.addAttribute("laCodeType", laCodeType);
        return "code_maintain/chl_list";
    }

    @RequestMapping("/chl_add.html")
    public String chl_add(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        List<LDCode> laCodeType = codeMaintainService.getCodeTypeList();
        model.addAttribute("laCodeType", laCodeType);
        return "code_maintain/chl_add";
    }

//    public Map<String, Object> deleteChlMaintain(@RequestParam String params) {

    @RequestMapping(value = "/deleteChlMaintain")
    @ResponseBody
    public Map<String, Object> deleteChlMaintain(@RequestBody List<LDCode1> deleteArray) {
        String result = "";
        Map<String, Object> map = new HashMap<>();
        try {
            if (!CollectionUtils.isEmpty(deleteArray)) {
                result = channelMaintainService.deleteChlMaintain(deleteArray);
            }
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping("/chl_list")
    @ResponseBody
    public String chlList(@ModelAttribute LDCode1 ldCode1, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        if (ldCode1.getCodeType() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = channelMaintainService.countForChlMaintainList(ldCode1);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }
        List<LDCode1> result = channelMaintainService.getChlMaintainList(ldCode1, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LDCode1 pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping(value = "/addChlMaintain")
    @ResponseBody
    public Map<String, Object> addChlMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        try {
            String result = channelMaintainService.addChlMaintain(po);
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("addChlMaintain: 系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping("/linkCodeType")
    @ResponseBody
    public Map<String, Object> linkCodeType(@RequestParam String channelCode) {
        Map<String, Object> map = new HashMap<>();
        List<LDCode1> list = channelMaintainService.getAllCodeTypeInfo(channelCode);
        logger.info(list);
        //如果代码类型没有对应的产品
        if (list.size() == 0) {
            map.put("msg", CM_LINKCODETYPE_NOT_EXIST.getMessage());
        }
        map.put("linkCodeType", list);
        return map;
    }

    @RequestMapping("/chlLinkCodeValue")
    @ResponseBody
    public Map<String, Object> chlLinkCodeValue(@RequestParam String codeType, @RequestParam String channelCode) {
        Map<String, Object> map = new HashMap<>();
        List<LDCode1> list = channelMaintainService.getAllCodeValueInfo(codeType, channelCode);
        logger.info(list);
        //如果代码类型在渠道映射中没有对应的产品
        if (list.size() == 0) {
            map.put("msg", CM_LINKCODETYPE_NOT_EXIST.getMessage());
        }
        map.put("chlLinkCodeValue", list);
        return map;
    }

}
