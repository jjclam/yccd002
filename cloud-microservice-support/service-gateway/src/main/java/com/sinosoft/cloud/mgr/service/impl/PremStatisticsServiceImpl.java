package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LCContMapper;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.mgr.service.PremStatisticsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PremStatisticsServiceImpl implements PremStatisticsService {
    private final static Log logger = LogFactory.getLog(PremStatisticsServiceImpl.class);
    @Autowired
    private LCContMapper lcContMapper;

    @Override
    public int countForPremCountList(Operation lcCont) {
        Map<String, Object> map = new HashMap<>();
        map.put("channelCode", lcCont.getSellType());
        map.put("appFlag", lcCont.getAppFlag());
        map.put("startDate", lcCont.getCvaliDate());
        map.put("endDate", lcCont.getEndDate());
        return lcContMapper.countForPremCountList(map);
    }

    @Override
    public List<Operation> getPremCountList(Operation lcCont, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("channelCode", lcCont.getSellType());
        map.put("appFlag", lcCont.getAppFlag());
        map.put("startDate", lcCont.getCvaliDate());
        map.put("endDate", lcCont.getEndDate());
        logger.info("getPolSearchList: " + map);
        return lcContMapper.selectForPremCountList(map);
    }
}
