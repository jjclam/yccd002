package com.sinosoft.cloud.mgr.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.XML_WRITE_ERROR;

/**
 * XML报文解析类
 * 目前只针对运营管理内的撤单
 *
 * @author J
 * @version 1.0
 */

public class XMLWriteRead {
    private final static Log logger = LogFactory.getLog(XMLWriteRead.class);

    public static String cancelPolicyByXML(Map<String, Object> params) {
        //存放报文
        StringBuffer xmlData = new StringBuffer();
        xmlData.append("<?xml version=\"1.0\" encoding=\"GBK\" standalone=\"yes\"?>");
        xmlData.append("<Package>");
        xmlData.append("<Head>");

        xmlData.append("<TransTime>");
        xmlData.append(params.get("TransTime"));
        xmlData.append("</TransTime>");
        xmlData.append("<ThirdPartyCode>");
        xmlData.append(params.get("ThirdPartyCode"));
        xmlData.append("</ThirdPartyCode>");
        xmlData.append("<TransNo>");
        xmlData.append(params.get("TransNo"));
        xmlData.append("</TransNo>");
        xmlData.append("<TransType>");
        xmlData.append("A0026");
        xmlData.append("</TransType>");
        xmlData.append("<Coworker>");
        xmlData.append("01");
        xmlData.append("</Coworker>");
        xmlData.append("</Head>");

        xmlData.append("<RequestNodes>");
        xmlData.append("<RequestNode>");
        xmlData.append("<ThirdPartyOrderId>");
        xmlData.append(params.get("ThirdPartyOrderId"));
        xmlData.append("</ThirdPartyOrderId>");
        xmlData.append("<ContNo>");
//        xmlData.append("2019092000001398");
        xmlData.append(params.get("ContNo"));
        xmlData.append("</ContNo>");
        xmlData.append("<ApplyDate>");
//        xmlData.append("2019-09-20");
        xmlData.append(params.get("ApplyDate"));
        xmlData.append("</ApplyDate>");
        xmlData.append("<ApplyTime>");
        xmlData.append("00:00:00");
        xmlData.append("</ApplyTime>");
        xmlData.append("<ApplyFlag>");
        xmlData.append(params.get("ApplyFlag"));
        xmlData.append("</ApplyFlag>");

        xmlData.append("</RequestNode>");
        xmlData.append("</RequestNodes>");

        xmlData.append("</Package>");

        //返回报文
        return xmlData.toString();
    }

    public static Map<String, Object> parseXMLDataFromPost(HttpURLConnection httpResponse) {
        Map<String, Object> map = new HashMap<>();
        try {
            //存放返回的报文
            StringBuffer recieveData = new StringBuffer();
            BufferedReader in = null;
            //BufferedReader读取返回报文
            InputStream inputStream = httpResponse.getInputStream();
//            String recieveData = InputStreamTOString(inputStream, "GBK");
//            logger.info(recieveData);
            in = new BufferedReader(new InputStreamReader(inputStream, "GBK"));
            for (String inputLine = null; (inputLine = in.readLine()) != null;){
                recieveData.append(inputLine);
            }
//            //打印接收到的报文
            logger.info("recieveData:"+recieveData.toString());
            inputStream.close();

            String xmlData = URLDecoder.decode(recieveData.toString(),"GBK");
            Element root = null;
            //SAXReader解析报文
            SAXReader saxReader = new SAXReader();
            root = saxReader.read(new StringReader(xmlData)).getRootElement();
            //获取报文中的ResponseNode节点
//            Element Package = root.element("Package");
            Element ResponseNodes = root.element("ResponseNodes");
            Element ResponseNode = ResponseNodes.element("ResponseNode");
            //获取报文中ResultInfoDesc节点中的子节点值
            Element Result = ResponseNode.element("Result");
            String resultInfoDesc = Result.elementTextTrim("ResultInfoDesc");
            map.put("resultInfoDesc", resultInfoDesc);
            //获取报文中ResultDesc节点中的子节点值
            Element BusinessObject = ResponseNode.element("BusinessObject");
            String resultDesc = BusinessObject.elementTextTrim("ResultDesc");
            map.put("resultDesc", resultDesc);
            logger.info(map);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        return map;
    }

//    public static void main(String[] args) throws Exception{
    public static String CancelPolicyRequest(Map<String, Object> params) {
        //1.创建一个Document对象
        Document doc = DocumentHelper.createDocument();

        //创建根对象
        Element root = doc.addElement("Package");

        //Head
        Element head = root.addElement("Head");
        Element transTime = head.addElement("TransTime");
//        transTime.setText("741909200124830096-1");
        transTime.setText("");
        Element thirdPartyCode = head.addElement("ThirdPartyCode");
        thirdPartyCode.setText("14");
        Element transNo = head.addElement("TransNo");
        transNo.setText("20190920032524844_9f16bbdc96014a819018322b84cc9a46");
        Element transType = head.addElement("TransType");
        transType.setText("A0026");
        Element coworker = head.addElement("Coworker");
        coworker.setText("01");

        //RequestNodes
        Element requestNodes = root.addElement("RequestNodes");
        Element requestNode = requestNodes.addElement("RequestNode");
        Element thirdPartyOrderId = requestNode.addElement("ThirdPartyOrderId");
        thirdPartyOrderId.setText("741909200124830096-1");
        Element contNo = requestNode.addElement("ContNo");
        contNo.setText("2019092000001398");
        Element applyDate = requestNode.addElement("ApplyDate");
        applyDate.setText("2019-09-20");
        requestNode.addElement("ApplyTime");
        Element applyFlag = requestNode.addElement("ApplyFlag");
        applyFlag.setText("0");

        try {
            //6.设置输出流来生成一个xml文件
            OutputStream os = new FileOutputStream("etoak4.xml");
            //Format格式输出格式刷
            OutputFormat format = OutputFormat.createPrettyPrint();
            //设置xml编码
            format.setEncoding("GBK");

            //写：传递两个参数一个为输出流表示生成xml文件在哪里
            //另一个参数表示设置xml的格式
            XMLWriter xw = new XMLWriter(os, format);
            //将组合好的xml封装到已经创建好的document对象中，写出真实存在的xml文件中
            xw.write(doc);
            //清空缓存关闭资源
            xw.flush();
            xw.close();
        } catch (Exception e){
            params.put("msg", XML_WRITE_ERROR.getMessage());
            logger.error("---------XML异常-------" + XML_WRITE_ERROR.getMessage() );
            e.printStackTrace();
        }

        return null;
    }

}
