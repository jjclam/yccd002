package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.Role;
import com.sinosoft.cloud.dal.dao.model.RoleExample;
import com.sinosoft.cloud.dal.dao.model.entity.SysUser;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrp;
import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.entity.Laagent;
import com.sinosoft.cloud.liAnlife.LianLifeService;
import com.sinosoft.cloud.mgr.service.MenuService;
import com.sinosoft.cloud.mgr.service.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;

/**
 * 用户管理
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/mgr_user")
public class UserController {
    private final static Log logger = LogFactory.getLog(UserController.class);
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private LianLifeService mLianLifeService;
    @Autowired
    private MenuService menuService;

    @RequestMapping("/list.html")
    public String list(ModelMap model) {
        List<Lacom> lacoms = mLianLifeService.selectwangBitSelect();
        List<Role> roles = menuService.getrole(new RoleExample ());
        model.addAttribute("lacoms", lacoms);
        model.addAttribute("roles", roles);

        return "mgr_user/list";
    }

    @RequestMapping("/add.html")
    public String add(ModelMap model) {
        List<Lacom> lacoms = mLianLifeService.selectwangBitSelect();
        List<Lacom> ldcoms = mLianLifeService.selectManAgeComSelect ();
        List<Laagent> laagents = mLianLifeService.selectAgentSelect ();
        List<Role> roles = menuService.getrole(new RoleExample ());
        model.addAttribute("roles", roles);
        model.addAttribute("lacoms", lacoms);
        model.addAttribute("ldcoms", ldcoms);
        model.addAttribute("lacoms", lacoms);
        model.addAttribute ("laagents",laagents);

        return "mgr_user/add";
    }

    @RequestMapping("/edit.html")
    public String edit(String userNames, ModelMap model) {
        SysUser item = null;
        if (StringUtils.isNotBlank(userNames)) {
            String userName = userNames.split(",")[0];
            item = userInfoService.getDetailByuserName(userName);
        }
        if (item == null) {
            item = new SysUser();
        }

        model.put("item", item);
        List<Lacom> lacoms = mLianLifeService.selectwangBitSelect();
        model.addAttribute("lacoms", lacoms);
        List<Role> roles = menuService.getrole(new RoleExample ());
        model.addAttribute("roles", roles);

        List<Lacom> ldcoms = mLianLifeService.selectManAgeComSelect ();
        List<Laagent> laagents = mLianLifeService.selectAgentSelect ();
        model.addAttribute("ldcoms", ldcoms);
        model.addAttribute ("laagents",laagents);
        return "mgr_user/edit";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute SysUser sysUser, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        //首次页面不查找数据
        if (sysUser.getUserName() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        if (sysUser!=null){
                if (sysUser.getInstitutions()!=null&&(!("".equals(sysUser.getInstitutions())))){
                    String[] split = sysUser.getInstitutions().split("-");
                    sysUser.setInstitutions(split[1]);
                }
        }
        int count = userInfoService.countUserInfo(sysUser);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<SysUser> result = userInfoService.getUserInfoList(sysUser, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (SysUser pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping(value = "/addUser")
    @ResponseBody
    public Map<String, Object> addUser(@RequestParam String params, HttpSession session) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String operator = (String) session.getAttribute("user");
        po.put("operator", operator);
        logger.info("操作员: " + operator);
        try {
            String result = userInfoService.addUser(po);
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping(value = "/updateUser")
    @ResponseBody
    public Map<String, Object> updateUser(@RequestParam String params, HttpSession session) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String operator = (String) session.getAttribute("user");
        po.put("operator", operator);
        logger.info("操作员: " + operator);
        try {
            String result = userInfoService.updateUser(po);
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }

    @RequestMapping(value = "/deleteUser")
    @ResponseBody
    public Map<String, Object> deleteUser(@RequestParam(value = "userNames")  String userNames) {
        Map<String, Object> map = new HashMap<>();
        String result = "";
        if (StringUtils.isBlank(userNames)) {
            logger.error(userNames + "没有");
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        } else {
            try {
                result = userInfoService.deleteUser(userNames);
                map.put("msg", result);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("系统异常");
                map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
                return map;
            }
            return map;
        }
    }

    @RequestMapping("/linkedInstitutions")
    @ResponseBody
    public Map<String, Object> linkedInstitutions(@RequestParam String field) {
        Map<String, Object> map = new HashMap<>();
        List<LDGrp> list = userInfoService.getAllInstitutions(field);
        logger.info(list);
        //如果没有企业
        if (list.size() == 0) {
            map.put("msg", DB_NOT_FOUND.getMessage());
        }
        map.put("linkedInstitutions", list);
        return map;
    }

    @RequestMapping("/linkedPositions")
    @ResponseBody
    public Map<String, Object> linkedPositions(@RequestParam String field) {
        Map<String, Object> map = new HashMap<>();
        List<SysUser> list = userInfoService.getUserPosition(field);
        logger.info(list);
        //如果没有岗位
        if (list.size() == 0) {
            map.put("msg", DB_NOT_FOUND.getMessage());
        }
        map.put("linkedPositions", list);
        return map;
    }

}
