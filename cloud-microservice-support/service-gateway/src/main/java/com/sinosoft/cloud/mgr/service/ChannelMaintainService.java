package com.sinosoft.cloud.mgr.service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode1;

import java.util.List;

public interface ChannelMaintainService {

    int countForChlMaintainList(LDCode1 ldCode1);

    List<LDCode1> getChlMaintainList(LDCode1 ldCode1, Integer pageIndex, Integer pageSize);

    String addChlMaintain(JSONObject po);

    String deleteChlMaintain(List<LDCode1> deleteArray);
//    String deleteChlMaintain(Map<String, Object> params);

    List<LDCode1> getAllCodeTypeInfo(String channelCode);

    List<LDCode1> getAllCodeValueInfo(String codeType, String channelCode);
}
