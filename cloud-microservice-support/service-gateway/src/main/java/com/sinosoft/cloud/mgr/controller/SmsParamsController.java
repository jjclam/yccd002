package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.SmsChlPro;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.SmsParamsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;

/**
 * 运营管理
 * 短信模板配置
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/sms_maintain")
public class SmsParamsController {
    private final static Log logger = LogFactory.getLog(SmsParamsController.class);
    @Autowired
    private SmsParamsService smsParamsService;
    @Autowired
    private ChannelProService channelProService;

    @RequestMapping("/list.html")
    public String list(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        return "code_maintain/sms_list";
    }

    @RequestMapping("/sms_add.html")
    public String add(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        return "code_maintain/sms_add";
    }

    @RequestMapping("/sms_edit.html")
    public String edit(ModelMap model, String channelCode, String proCode) {
//        String channelCode = smsArray.split("||")[0];
//        String proCode = smsArray.split("||")[1];
//        String smsInfo = smsArray.split("||")[2];
//        JSONObject po = JSONObject.parseObject(params);
        SmsChlPro smsChlPro = smsParamsService.getSmsEditInfo(channelCode, proCode);
        Map<String, Object> map = new HashMap<>();
        map.put("channelCode", channelCode);
        map.put("proCode", proCode);
        map.put("smsInfo", smsChlPro.getSmsInfo());
//        map.put("proCode", po.getString("proCode"));
//        map.put("smsInfo", po.getString("smsInfo"));
        model.put("item", map);

        return "code_maintain/sms_edit";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute SmsChlPro smsChlPro, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        if (smsChlPro.getChannelCode() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = smsParamsService.countForSmsParamsList(smsChlPro);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<SmsChlPro> result = smsParamsService.getSmsParamsList(smsChlPro, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (SmsChlPro sms : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(sms);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping(value = "/addSmsMaintain")
    @ResponseBody
    public Map<String, Object> addSmsMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        try {
            String result = smsParamsService.addSmsMaintain(po);
            map.put("msg", result);
        } catch (Exception e) {
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            e.printStackTrace();
            logger.error("系统异常");
        }
        return map;
    }

    @RequestMapping(value = "/updateSmsMaintain")
    @ResponseBody
    public Map<String, Object> updateSmsMaintain(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        try {
            String result = smsParamsService.updateSmsMaintain(po);
            map.put("msg", result);
        } catch (Exception e) {
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            e.printStackTrace();
            logger.error("系统异常");
        }
        return map;
    }

    @RequestMapping(value = "/deleteSmsMaintain")
    @ResponseBody
    public Map<String, Object> deleteSmsMaintain(@RequestBody List<SmsChlPro> deleteArray) {
        Map<String, Object> map = new HashMap<>();
        String result = "";
        if (CollectionUtils.isEmpty(deleteArray)) {
            logger.error("deleteSmsMaintain: " + PARAM_NOT_FOUND.getMessage());
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        } else {
            result = smsParamsService.deleteSmsMaintain(deleteArray);
            map.put("msg", result);
        }
        return map;
    }

    @RequestMapping(value = "/smsParams")
    @ResponseBody
    public Map<String, Object> smsParamsLinked(@RequestParam(value = "codeType") String codeType) {
        Map<String, Object> map = new HashMap<>();
        List<LDCode> list = smsParamsService.getSmsParamsLinkded(codeType);
        logger.info(list);
        //如果短信模板参数ldcode没有配置
        if (list.size() == 0) {
            map.put("msg", CM_SMSPARAM_NOT_EXIST.getMessage());
        }
        map.put("smsParam", list);
        return map;
    }
}
