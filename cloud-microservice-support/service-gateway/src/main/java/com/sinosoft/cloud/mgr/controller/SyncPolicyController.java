package com.sinosoft.cloud.mgr.controller;

import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.ManualCancelPolicyService;
import com.sinosoft.cloud.mgr.service.SynPolicyService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

/**
 * 出单管理
 * 保单同步
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/pol_sync")
public class SyncPolicyController {
    private final static Log logger = LogFactory.getLog(SyncPolicyController.class);
    @Autowired
    private ChannelProService channelProService;
    @Autowired
    private SynPolicyService synPolicyService;


    @RequestMapping("/list.html")
    public String core_list(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        return "pol_sync/list";
    }

    @RequestMapping("/syncPolicy")
    @ResponseBody
    public Map<String, Object> syncPolicy(@RequestParam(value = "params") String params) {
        Map<String, Object> map = new HashMap<>();
        String result = synPolicyService.saveCont(params);
        if (result == SUCCESS.getCode()) {
            map.put("msg", SUCCESS.getCode());
            logger.info("---------同步成功---------");
        } else {
            map.put("msg", result);
            logger.error("---------同步失败---------" + result);
        }
        return map;
    }

}
