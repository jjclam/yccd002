package com.sinosoft.cloud.mgr.controller;

import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.ManualCancelPolicyService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

/**
 * 运营管理
 * 人工撤单
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/remove_pol")
public class ManualCancelPolicyController {
    private final static Log logger = LogFactory.getLog(ManualCancelPolicyController.class);
    @Autowired
    private ChannelProService channelProService;
    @Autowired
    private ManualCancelPolicyService manualCancelPolicyService;

    @RequestMapping("/list.html")
    public String core_list(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        return "operation/remove_pol/list";
    }

    @RequestMapping("/removePolicy")
    @ResponseBody
    public Map<String, Object> cancelPolicy(@RequestParam(value = "params") String params) {
        Map<String, Object> map = new HashMap<>();
        String result = "";
        result = manualCancelPolicyService.cancelPolicy(params);
        if (result == SUCCESS.getCode()) {
            map.put("msg", SUCCESS.getCode());
            logger.info("---------撤单成功---------");
        } else {
            map.put("msg", result);
            logger.error("---------撤单失败---------" + result);
        }
        return map;
    }

}
