package com.sinosoft.cloud.mgr.yfbController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.LMCalModePojo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.common.AjaxJson;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.MenuService;
import com.sinosoft.cloud.mgr.yfbService.LDGrpService;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.VData;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_NOT_FOUND;

@Controller
@RequestMapping("/GrpClient")
public class GrpClientController {
    @Autowired
    private MenuService menuService;
    @Autowired
    private ChannelProService channelProService;
    @Autowired
    private LDGrpService ldGrpService;


    @RequestMapping("/list")
    public String clientList(ModelMap model) {

        /*List<ChlInfo> laChlAgencyType = channelProService.getChlAgencyType();
        model.addAttribute("laChlAgencyType", laChlAgencyType);*/
        List<ChlInfo> laChlAgencyType = channelProService.getChlAgencyType();
        model.addAttribute("laChlAgencyType", laChlAgencyType);
        return "yfb_templates/grpClientList";
    }

    /**
     * 团体客户管理列表数据
     * add by ppf
     */
    @RequestMapping("/listData")
    @ResponseBody
    public AjaxJson listData(@ModelAttribute LMCalModePojo lmCalModePojo, Integer page, Integer limit) {
        AjaxJson aAjaxJson = new AjaxJson();
        PageModel pageModel = new PageModel();
        String calCode = lmCalModePojo.getCalCode();
//        if (calCode != null) {                    //带参数查询团体客户
            int count = ldGrpService.SelectGrpCount(lmCalModePojo);
            if (count > 0) {                                 //在中台查到了团体客户
                List<LMCalModePojo> lmCalModePojoList = ldGrpService.selectByNoParam(lmCalModePojo, page, limit);
                aAjaxJson.setData(lmCalModePojoList);
                pageModel.setCount(count);
                pageModel.setMsg("ok");
                pageModel.setRel(true);
                aAjaxJson.setCode("0");
                aAjaxJson.setCount(count);
                pageModel.setRel(true);
            }
//        }
        return aAjaxJson;
    }


    /**
     * 保存团体客户信息
     */
    @RequestMapping("/saveData")
    @ResponseBody
    public AjaxJson saveData(LMCalModePojo lMCalModePojo) {
        System.out.println("_____------------______");
        MMap tMMap = new MMap();
        VData tVData = new VData();
        AjaxJson ajaxJson = new AjaxJson();
        try {
            tMMap.put(lMCalModePojo, "INSERT");
            tVData.add(tMMap);
            //数据落库
            PubSubmit tPubSubmit = new PubSubmit();
            tPubSubmit.submitData(tVData);
            ajaxJson.setMsg("保存成功");
        } catch (Exception e) {
            System.out.println("增加报错！");
            ajaxJson.setMsg("保存失败");
        }
        return ajaxJson;
    }



}


