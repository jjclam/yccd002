package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LAChannelProductMapper;
import com.sinosoft.cloud.dal.dao.mapper.LAComMapper;
import com.sinosoft.cloud.dal.dao.mapper.LDCodeMapper;
import com.sinosoft.cloud.dal.dao.mapper.LMRiskMapper;
import com.sinosoft.cloud.dal.dao.model.LAChannelProduct;
import com.sinosoft.cloud.dal.dao.model.LAChannelProductExample;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SaleProInfo;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.DateUtil.*;
import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;
import static com.sinosoft.cloud.common.util.RegUtil.regMatch;

@Service
public class ChannelProServiceImpl implements ChannelProService {
    private final static Log logger = LogFactory.getLog(ChannelProServiceImpl.class);
    @Autowired
    private LAComMapper laComMapper;
    @Autowired
    private LMRiskMapper tLMRiskMapper;
    @Autowired
    private LAChannelProductMapper laChannelProductMapper;
    @Autowired
    private LDCodeMapper mLDCodeMapper;

    @Override
    public List<ChlInfo> getChlXXX(String channelCode) {
        return laChannelProductMapper.verifyChlExist(channelCode);
    }

    @Override
    public List<ChlInfo> findCirlBySe() {
        List<ChlInfo> chlInfos = mLDCodeMapper.selectCirlBySe();
        return chlInfos;
    }

    @Override
    public SaleProInfo getDetailByProcode(String proCode, String channelCode) {
        SaleProInfo saleProInfo = new SaleProInfo();
        if (regMatch(proCode)) {
            saleProInfo = tLMRiskMapper.selectSaleProInfoByProCode(proCode, channelCode);
        } else {
            saleProInfo = tLMRiskMapper.selectSaleProInfoByProCodeex(proCode, channelCode);
        }
        logger.info(saleProInfo);
        return saleProInfo;
    }

    @Override
    public LAChannelProduct getDetailByProChlcode(String proCode, String channelCode) {
        return laChannelProductMapper.selectSaleProInfoByProChlCode(proCode, channelCode);
    }

    @Override
    public LAChannelProduct getEditByProcode(LAChannelProduct laChlPro) {
        Map<String, Object> map = new HashMap<>();
        map.put("riskCode", laChlPro.getRiskCode());
        map.put("channelCode", laChlPro.getChannelCode());
        return laChannelProductMapper.selectForChlProEdit(map);
    }

    @Override
    public String addChlProInfo(JSONObject po) {
        String oldChannelCode = po.getString("channelCode");
        String channelCode = oldChannelCode.split("-")[0];//如果取数有连接符需截取
        String channelName = oldChannelCode.split("-")[1];//如果取数有连接符需截取
        String riskType = po.getString("riskType");
        String riskCode = po.getString("riskCode").split("-")[0];
        String riskName = po.getString("riskCode").split("-")[1];
        String riskStartSellDate = po.getString("riskStartSellDate");
        String riskEndSellDate = po.getString("riskEndSellDate");
        String url = po.getString("h5href");

        if (channelCode == null || riskType == null || riskCode == null || channelCode.equals("") || riskType.equals("") || riskCode.equals("")) {
            return PARAM_NOT_FOUND.getCode();
        }

        Map<String, Object> map = new HashMap<>();
        map.put("riskCode", riskCode);
        map.put("riskType", riskType);

        //默认该产品在核心没有下架
        String flag = "0";
        ProInfo info = null;
//        LAChannelProduct laChlPro = laChannelProductMapper.confirmChlProHaved(map);
        if (regMatch(riskCode)) {
            info = tLMRiskMapper.confirmChlProHaved(map);
        } else {
            info = tLMRiskMapper.confirmChlProHavedTC(map);
        }
//        flag = laChannelProductMapper.confirmChlProHaved(map);

        if (info == null) {
            logger.info("putAwayChlPro: " + Chl_STATUS_NOT_EXIST.getMessage());
            return Chl_STATUS_NOT_EXIST.getMessage();
        }
        flag = info.getStatus();
        if (flag.equals("1")) {
            logger.info("putAwayChlPro: " + Chl_PUTDOWN_EXIST.getMessage());
            return Chl_PUTDOWN_EXIST.getMessage();
        }

        if (riskStartSellDate.isEmpty() || riskStartSellDate.equals("") || riskEndSellDate.isEmpty() || riskEndSellDate.equals("")) {
            return Chl_SALETIME_NOT_FOUND.getCode();
        } else {
            if (compareDate(riskStartSellDate, riskEndSellDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == 1) {
                return Chl_SALESTARTTIME_OVER_END.getCode();
            }
        }

        map.put("channelCode", channelCode);
        map.put("riskStartSellDate", riskStartSellDate);
        map.put("riskEndSellDate", riskEndSellDate);
        String modifyDate = getCurrentTimeStr("");
        map.put("modifyDate", modifyDate);
        String channelProductUpDate = getCurrentDefaultTimeStr();
        map.put("channelProductUpDate", channelProductUpDate);
        map.put("h5href", url);
        map.put("rnewageFlag", po.getString("rnewageFlag"));
        map.put("isConfig", po.getString("isConfig"));
        map.put("upDownType", po.getString("upDownType"));
        logger.info("ChlProAdd(map): " + map);

        //获取核心产品起售日期
//        List<ProInfo> proInfoDates = new LinkedList<>();
        ProInfo proInfoDates = new ProInfo();
        String riskStartDate = "";
        String riskEndDate = "";
        if (regMatch(riskCode)) {
            proInfoDates = tLMRiskMapper.getProStartEndDate(riskCode);
        } else {
            proInfoDates = tLMRiskMapper.getTCProStartEndDate(riskCode);
        }
        if (proInfoDates != null) {
            riskStartDate = proInfoDates.getStartDate();
            riskEndDate = proInfoDates.getOverDate();

            if (riskStartDate != "" & compareDate(riskStartSellDate, riskStartDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == -1) {
                return Chl_SALESTARTTIME_ERROR.getMessage();
            }
            if (riskEndDate != "" & compareDate(riskEndSellDate, riskEndDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == 1) {
                return Chl_SALEENDTIME_ERROR.getMessage();
            }
        }

        //判断渠道产品表中是否已经录入该产品,若没有录入,则在渠道产品表中新增
        int exist = 0;
        int putawayOk = 0;
        String result = "";
        exist = laChannelProductMapper.verifyChlProExist(map);
//        exist = exists.getStatus();
        if (exist == 1) {
            //业务更改--判断是否已经上架,若已经上架则提示;若下架,则更新上下架状态
            LAChannelProduct updownstatus = laChannelProductMapper.confirmChlProHaved(map);
            if (updownstatus.getStatus().equals("0")) {
                logger.info("putAwayChlPro: " + Chl_PUTAWAY_EXIST.getMessage());
                return Chl_PUTAWAY_EXIST.getMessage();
            }
            //判断产品渠道起售时间在产品上架时间的合理范围内;即起售时间要大于上架时间
//            LAChannelProduct chlProUpDate = laChannelProductMapper.getChlProUpDate(channelCode, riskCode);
//            if (compareDate(riskStartSellDate, chlProUpDate.getChannelProductUpDate(), FORMAT_YYYY_MM_DD_HH_MM_SS) == -1){
//                return Chl_SALESTARTTIME_UPERROR.getMessage();
//            }

            logger.info("updateForAdd(map): " + map);
            putawayOk = laChannelProductMapper.putAwayChlPro(map);
            logger.info("putAwayChlProSuccessfully: " + putawayOk);
        } else {
//            List<ChlInfo> chlName = laComMapper.selectChlName(channelCode);
//            map.put("channelName", chlName.get(0).getChannelName());
//            SaleProInfo proName = tLMRiskMapper.selectRiskName(riskCode);
//            map.put("riskName", po.getString(""));
            map.put("channelName", channelName);
            map.put("riskName", riskName);
            map.put("status", "0");//上架
            String createDate = getCurrentDefaultTimeStr();
            map.put("createDate", createDate);
            logger.info("updateForAdd(map): " + map);
            putawayOk = laChannelProductMapper.insertPutAwayChlPro(map);
            logger.info("putAwayChlProSuccessfully: " + putawayOk);
        }
        if (putawayOk == 1) {
            result = SUCCESS.getCode();
            logger.info("putAwayChlPro: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("putAwayChlPro: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    public String updateChlProInfo(JSONObject po) {
        String channelCode = po.getString("channelCode").substring(0,2);
        String riskType = po.getString("riskType");
        String riskCode = po.getString("riskCode");
        String riskStartSellDate = po.getString("riskStartSellDate");
        String riskEndSellDate = po.getString("riskEndSellDate");
        String url = po.getString("h5href");

        if (channelCode == null || riskType == null || riskCode == null || riskStartSellDate == null || riskEndSellDate == null) {
            return PARAM_NOT_FOUND.getCode();
        }

//        Integer flag = 1; //默认渠道产品不存在
//        flag = laChannelProductMapper.confirmChlProHaved(map);
//        if (flag == 0) {
//            model.put("meg", "产品已存在，上架失败！");
//        } else {
//
//        }

        if (riskStartSellDate.isEmpty() || riskStartSellDate.equals("") || riskEndSellDate.isEmpty() || riskEndSellDate.equals("")) {
//            model.put("meg","产品起售时间/产品止售时间不能为空!");
            return Chl_SALETIME_NOT_FOUND.getCode();
        } else {
            if (compareDate(riskStartSellDate, riskEndSellDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == 1) {
//                model.put("meg","产品起售时间不能大于产品止售时间");
                return Chl_SALESTARTTIME_OVER_END.getCode();
            }
            //获取核心产品起售日期
            ProInfo proInfoDates = new ProInfo();
            proInfoDates = tLMRiskMapper.getProStartEndDate(riskCode);
            String riskStartDate = proInfoDates.getStartDate();
            String riskEndDate = proInfoDates.getOverDate();
            if (riskStartDate != "" & compareDate(riskStartSellDate, riskStartDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == -1) {
                return Chl_SALESTARTTIME_ERROR.getMessage();
            }
            if (riskEndDate != "" & riskEndDate != null & compareDate(riskEndSellDate, riskEndDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == 1) {
                return Chl_SALEENDTIME_ERROR.getMessage();
            }
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("channelCode", channelCode);
        map.put("riskCode", riskCode);
        map.put("riskType", riskType);
        map.put("riskStartSellDate", riskStartSellDate);
        map.put("riskEndSellDate", riskEndSellDate);
        String modifyDate = getCurrentDefaultTimeStr();
        map.put("modifyDate", modifyDate);
        map.put("url", url);
//        map.put("rnewageFlag", po.getString("rnewageFlag"));
//        map.put("isConfig", po.getString("isConfig"));
//        map.put("upDownType", po.getString("upDownType"));
        logger.info("ChlProEdit(map): " + map);
        String result = "";
        int flag = laChannelProductMapper.updateForEdit(map);
        logger.info("updateForEditSuccessfully: " + result);
        if (flag == 0) {
            result = DB_FAIL.getMessage();
            logger.info("updateForEdit: " + DB_FAIL.getMessage());
        } else {
            result = SUCCESS.getCode();
            logger.info("updateForEdit: " + SUCCESS.getMessage());
        }
        return result;
    }

    @Override
    public List<ChlInfo> getChlCodeName() {
        return laComMapper.selectChlCodeName();
    }

    @Override
    public List<ChlInfo> getChlAgencyType() {
        return laComMapper.selectChlAgencyType();
    }

    @Override
    public Integer count() {
        return laComMapper.countByExample();
    }

    @Override
    public Integer count(LAChannelProduct laChlPro) {
        LAChannelProductExample example = new LAChannelProductExample();
        LAChannelProductExample.Criteria criteria = example.createCriteria();
        setCriteria(criteria, laChlPro);
        return laChannelProductMapper.countByExample(example);
    }

    @Override
    public Integer countChlForUpDown(String channelCode) {
        return laComMapper.countChlForUpDown(channelCode);
    }

    @Override
    public Integer countChlProInfoList(ChlInfo chlInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put("channelCode", chlInfo.getChannelCode());
        map.put("startDate", chlInfo.getCreateStartDate());
        map.put("endDate", chlInfo.getCreateEndDate());
        return laChannelProductMapper.countChlProInfoList(map);
    }

    @Override
    public List<ChlInfo> getChlProInfoList(ChlInfo chlInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("channelCode", chlInfo.getChannelCode());
//        map.put("channelName", chlInfo.getChannelName());
        map.put("startDate", chlInfo.getCreateStartDate());
        map.put("endDate", chlInfo.getCreateEndDate());
        logger.info("List<laChlPro>: " + map);
//        return laComMapper.selectForChlProInfo(map);
        return laChannelProductMapper.selectForChlProInfo(map);
    }

    @Override
    public List<ChlInfo> getChlInfoList(ChlInfo chlInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
//        map.put("channelName", chlInfo.getChannelName());
        map.put("channelCode", chlInfo.getChannelCode());
        map.put("startDate", chlInfo.getCreateStartDate());
        map.put("endDate", chlInfo.getCreateEndDate());
        logger.info("List<laChlPro>: " + map);
        return laChannelProductMapper.selectForChlInfo(map);
    }

    //    @Override
    public List<LAChannelProduct> getChlProInfoList(int offset, int limit, LAChannelProduct laChlPro) {
        LAChannelProductExample example = new LAChannelProductExample();
        example.setOrderByClause("createTime DESC");
        example.setOffset(offset);
        example.setLimit(limit);
        LAChannelProductExample.Criteria criteria = example.createCriteria();
        setCriteria(criteria, laChlPro);
        return laChannelProductMapper.selectByExample(example);
    }

    @Override
    public List<SaleProInfo> getRiskTypeCodeName() {
        return laComMapper.selectRiskTypeCodeName();
    }

    @Override
    public SaleProInfo getRiskTypeName(String riskTypeCode) {
        return laComMapper.selectRiskTypeName(riskTypeCode);
    }

    @Override
    public List<LAChannelProduct> getSaleProInfo(String riskType) {
        return laChannelProductMapper.selectSaleProInfoByChlCode(riskType);
    }

    @Override
    public List<SaleProInfo> getAllProInfo(String riskType) {
        return tLMRiskMapper.selectSaleProInfoByRiskType(riskType);
    }

    @Override
    public String downChlPro(String channelCode, String proCode) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("channelCode", channelCode);
        map.put("riskCode", proCode);
//        map.put("h5href", "");
        String modifyDate = getCurrentDefaultTimeStr();
        map.put("modifyDate", modifyDate);
        String channelProductDownDate = getCurrentDefaultTimeStr();
        map.put("channelProductDownDate", channelProductDownDate);
        int back = laChannelProductMapper.putdownChlPro(map);
        String result = "";
        logger.info("putDownChlPro: " + back);
        if (back == 1) {
            result = SUCCESS.getCode();
            logger.info("putDownChlPro: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("putDownChlPro: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    @Transactional
    public String addupChlProInfo(JSONObject po) {
        String result = "";
        String channelCodes = po.getString("channelCodes");
        String[] strArrayC = channelCodes.split(",");
        String oldChannelCode = "";
        for (int i = 0; i < strArrayC.length; i++) {
            oldChannelCode = strArrayC[i];
//        }
//        String oldChannelCode = po.getString("channelCode");
            String channelCode = oldChannelCode.split("-")[0];//如果取数有连接符需截取
            String channelName = oldChannelCode.split("-")[1];
            String riskType = po.getString("riskType");
            String riskCode = po.getString("riskCode").split("-")[0];
            String riskName = po.getString("riskCode").split("-")[1];
            String riskStartSellDate = po.getString("riskStartSellDate");
            String riskEndSellDate = po.getString("riskEndSellDate");
            String url = po.getString("h5href");

            if (channelCode == null || riskType == null || riskCode == null || channelCode.equals("") || riskType.equals("") || riskCode.equals("")) {
                return PARAM_NOT_FOUND.getCode();
            }

            Map<String, Object> map = new HashMap<>();
            map.put("riskCode", riskCode);
            map.put("riskType", riskType);

            //默认该产品在核心没有下架
            String flag = "0";
            ProInfo info = null;
            if (regMatch(riskCode)) {
                info = tLMRiskMapper.confirmChlProHaved(map);
            } else {
                info = tLMRiskMapper.confirmChlProHavedTC(map);
            }

            if (info == null) {
                return Chl_STATUS_NOT_EXIST.getMessage();
            }
            flag = info.getStatus();
            if (flag.equals("1")) {
                return Chl_PUTDOWN_EXIST.getMessage();
            }


            if (riskStartSellDate.isEmpty() || riskStartSellDate.equals("") || riskEndSellDate.isEmpty() || riskEndSellDate.equals("")) {
                return Chl_SALETIME_NOT_FOUND.getCode();
            } else {
                if (compareDate(riskStartSellDate, riskEndSellDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == 1) {
                    return Chl_SALESTARTTIME_OVER_END.getCode();
                }
            }

            map.put("channelCode", channelCode);
            map.put("riskStartSellDate", riskStartSellDate);
            map.put("riskEndSellDate", riskEndSellDate);
            String modifyDate = getCurrentDefaultTimeStr();
            map.put("modifyDate", modifyDate);
            String channelProductUpDate = getCurrentDefaultTimeStr();
            map.put("channelProductUpDate", channelProductUpDate);
            map.put("url", url);
            map.put("rnewageFlag", po.getString("rnewageFlag"));
            map.put("isConfig", po.getString("isConfig"));
            map.put("upDownType", po.getString("upDownType"));
            logger.info("ChlProAdd(map): " + map);

            //获取核心产品起售日期
//        List<ProInfo> proInfoDates = new LinkedList<>();
            ProInfo proInfoDates = new ProInfo();
            String riskStartDate = "";
            String riskEndDate = "";
            if (regMatch(riskCode)) {
                proInfoDates = tLMRiskMapper.getProStartEndDate(riskCode);
            } else {
                proInfoDates = tLMRiskMapper.getTCProStartEndDate(riskCode);
            }
            if (proInfoDates != null) {
                riskStartDate = proInfoDates.getStartDate();
                riskEndDate = proInfoDates.getOverDate();

                if (riskStartDate != "" & riskStartDate != null & compareDate(riskStartSellDate, riskStartDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == -1) {
                    return Chl_SALESTARTTIME_ERROR.getMessage();
                }
                if (riskEndDate != "" & riskEndDate != null & compareDate(riskEndSellDate, riskEndDate, FORMAT_YYYY_MM_DD_HH_MM_SS) == 1) {
                    return Chl_SALEENDTIME_ERROR.getMessage();
                }
            }

            //判断渠道产品表中是否已经录入该产品,若没有录入,则在渠道产品表中新增
            int exist = 0;
            int putawayOk = 0;
//            String result = "";
            exist = laChannelProductMapper.verifyChlProExist(map);
//        exist = exists.getStatus();
            if (exist == 1) {
                //业务更改--判断是否已经上架,若已经上架则提示;若下架,则更新上下架状态
                LAChannelProduct updownstatus = laChannelProductMapper.confirmChlProHaved(map);
                if (updownstatus.getStatus().equals("0")) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return Chl_PUTAWAY_ALL_EXIST.getMessage();
                }
                putawayOk = laChannelProductMapper.putAwayChlPro(map);
            } else {
//                List<ChlInfo> chlName = laComMapper.selectChlName(channelCode);
//                map.put("channelName", chlName.get(0).getChannelName());
//            SaleProInfo proName = tLMRiskMapper.selectRiskName(riskCode);
//            map.put("riskName", po.getString(""));
                map.put("channelName", channelName);
                map.put("riskName", riskName);
                map.put("status", "0");//上架
                String createDate = getCurrentDefaultTimeStr();
                map.put("createDate", createDate);
                putawayOk = laChannelProductMapper.insertPutAwayChlPro(map);
            }
            if (putawayOk == 1) {
                result = SUCCESS.getCode();
                logger.info("putAwayAllChlProSuccessfully: " + SUCCESS.getMessage());
            } else {
                result = DB_FAIL.getMessage();
                logger.info("putAwayAllChlProUnsuccessfully: " + DB_FAIL.getMessage());
            }
        }
        return result;
    }

    @Override
    @Transactional
    public String alldownChlPro(String channelCodes, String proCode, String riskType) {
        String result = "";
        String[] strArrayC = channelCodes.split(",");
        String channelCode = "";
        int back = 0;
        for (int i = 0; i < strArrayC.length; i++) {
            channelCode = strArrayC[i];
//        }
            HashMap<String, Object> map = new HashMap<>();
            map.put("channelCode", channelCode);
            String riskCode = proCode.split("-")[0];
            map.put("riskCode", riskCode);
            map.put("riskType", riskType);
            String modifyDate = getCurrentDefaultTimeStr();
            map.put("modifyDate", modifyDate);
            String channelProductDownDate = getCurrentDefaultTimeStr();
            map.put("channelProductDownDate", channelProductDownDate);
            //业务更改--判断是否已经存在下架
            LAChannelProduct updownstatus = laChannelProductMapper.confirmChlProHaved(map);
            //渠道没有配置过下架,一键下架中让其下架
            //业务更改,渠道没有配置过下架,一键下架中抛异常
            if (updownstatus == null) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                logger.info("putDownAllChlProUnsuccessfully: " + Chl_PUTAWAY_NOT_EXIST.getMessage());
                return Chl_PUTAWAY_NOT_EXIST.getMessage();
//                back = laChannelProductMapper.putdownChlPro(map);
            }
            if (updownstatus != null & updownstatus.getStatus().equals("1")) {
//                throw new RuntimeException(Chl_PUTDOWN_ALL_EXIST.getMessage());
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                logger.info("putDownAllChlProUnsuccessfully: " + Chl_PUTDOWN_ALL_EXIST.getMessage());
                return Chl_PUTDOWN_ALL_EXIST.getMessage();
            }
            back = laChannelProductMapper.putdownChlPro(map);
//            String result = "";
//            logger.info("putDownAllChlPro: " + back);
        }
        if (back == 1) {
            result = SUCCESS.getCode();
            logger.info("putDownAllChlProSuccessfully: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("putDownAllChlProUnsuccessfully: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    public List<ChlInfo> getChlProInfoExcel(ChlInfo chlInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put("channelCode", chlInfo.getChannelCode());
        map.put("channelName", chlInfo.getChannelName());
        map.put("startDate", chlInfo.getRiskStartSellDate());
        map.put("endDate", chlInfo.getRiskEndSellDate());
        logger.info("List<laChlPro>: " + map);
//        return laComMapper.selectForChlProInfo(map);
        return laChannelProductMapper.selectForChlProExcel(map);
    }

    @Override
    public List<SaleProInfo> getRiskTypeCodeNameEX(String riskType) {
        return laComMapper.selectRiskTypeCodeNameEX(riskType);
    }

    @Override
    public List<ChlInfo> getChlAllUpList(ChlInfo chlInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        String channelCode = chlInfo.getChannelCode();
        return laComMapper.selectChlName(channelCode);
    }

    @Override
    public String getHttpUrl(String httpUrl) {
        LAChannelProduct url = laChannelProductMapper.getHttpUrl(httpUrl);
        return url.getH5href();
    }

    void setCriteria(LAChannelProductExample.Criteria criteria, LAChannelProduct laChlPro) {
        if(laChlPro != null) {
            if(StringUtils.isNotBlank(laChlPro.getChannelCode())) criteria.andChannelCodeEqualTo(laChlPro.getChannelCode());
        }
    }
}
