package com.sinosoft.cloud.yfb.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.model.LDDevices;
import com.sinosoft.cloud.dal.yfbDao.mapper.LDDevicesMapper;
import com.sinosoft.cloud.yfb.service.ScheduleQueryService;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_FAIL;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

@Service
public class ScheduleQueryServiceImpl implements ScheduleQueryService {
    private final static Log logger = LogFactory.getLog(SchemeInfoServiceImpl.class);
    @Autowired
    private LDDevicesMapper ldDevicesMapper;

    @Override
    public int countScheduleList(LDDevices ldDevices) {
        Map<String, Object> map = new HashMap<>();
        String serialNo = ldDevices.getSerialNo();
        String deviceNo = ldDevices.getDeviceNo();
        String useFlag = ldDevices.getUseFlag();
        String address = ldDevices.getAddress();
        String mangeCom = ldDevices.getMangeCom();
        map.put("serialNo", serialNo);
        map.put("deviceNo", deviceNo);
        map.put("useFlag", useFlag);
        map.put("address", address);
        map.put("mangeCom", mangeCom);
        return ldDevicesMapper.countScheduleList(map);
    }

    @Override
    public List<LDDevices> getScheduleList(LDDevices ldDevices, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);

        Map<String, Object> map = new HashMap<>();
        String serialNo = ldDevices.getSerialNo();
        String deviceNo = ldDevices.getDeviceNo();
        String useFlag = ldDevices.getUseFlag();
        String address = ldDevices.getAddress();
        String mangeCom = ldDevices.getMangeCom();
        map.put("serialNo", serialNo);
        map.put("deviceNo", deviceNo);
        map.put("useFlag", useFlag);
        map.put("address", address);
        map.put("mangeCom", mangeCom);
        return ldDevicesMapper.getScheduleList(map);
    }



    @Override
    public String deleteCodeMaintain(String deviceNos) {
        String result = "";
        String deviceNo = "";
//        String useFlag = "";
        String[] strArrayU = deviceNos.split(",");
//        String[] strArrayP = useFlags.split(",");
        for (int i = 0; i < strArrayU.length; i++) {
            deviceNo = strArrayU[i];
            for (int n = i; n < i + 1; n++) {
//                useFlags = strArrayP[n];
                int flag = ldDevicesMapper.deleteCodeMaintains(deviceNo);
                if (flag == 1) {
                    result = SUCCESS.getCode();
                    logger.info("deleteCodeMaintain: " + SUCCESS.getMessage());
                } else {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//                    result = DB_FAIL.getMessage();
//                    logger.info("deleteCodeMaintain: " + DB_FAIL.getMessage());
                    return result;
                }
            }
        }
        return result;
    }

    @Override
    public String addCodeMaintain(JSONObject po) {
        System.out.println("xulie "+PubFun.CreateSeq("SEQ_DEV_ID",10));
        String serialNo = PubFun.CreateSeq("SEQ_DEV_ID",10);
        String deviceNo = po.getString("deviceNo");
        String useFlag = po.getString("useFlag");
        String address = po.getString("address");
        String mangeCom = po.getString("mangeCom");
        String regDate = po.getString("regDate");
        String manageComName = po.getString("manageComName");
//        String agentCom = po.getString("agentCom");
        String result = "";

        Map<String, Object> map = new HashMap<>();
        map.put("serialNo",serialNo);
        map.put("deviceNo", deviceNo);
        map.put("useFlag", useFlag);
        map.put("address", address);
        map.put("mangeCom", mangeCom);
        map.put("regDate", regDate);
        map.put("manageComName", manageComName);
//        map.put("agentCom",agentCom);
        //判断中台是否配置过标准代码类型;
        int flag = ldDevicesMapper.insertDevices(map);
        if (flag != 1) {
            result = CM_CODETYPE_EXIST.getMessage();
            logger.info("addCodeMaintain: " + CM_CODETYPE_EXIST.getMessage());
            return result;
        }
        result = Integer.toString(flag);
        return result;
    }
}
