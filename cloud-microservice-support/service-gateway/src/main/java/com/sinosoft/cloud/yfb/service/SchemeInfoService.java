package com.sinosoft.cloud.yfb.service;

import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeRisk;
import com.sinosoft.cloud.dal.yfbDao.model.entity.SchemeParams;

import java.util.List;

public interface SchemeInfoService {
    int countSchemeInfo(LDSchemeRisk scheme);

    List<LDSchemeRisk> getSchemeInfoList(LDSchemeRisk scheme, Integer pageIndex, Integer pageSize);

    int countSchemeInfoForView(SchemeParams scheme);

    List<SchemeParams> getSchemeInfoListForView(SchemeParams scheme, Integer pageIndex, Integer pageSize);

    SchemeParams getSchemeInfoListForParams(String schemecode);
}
