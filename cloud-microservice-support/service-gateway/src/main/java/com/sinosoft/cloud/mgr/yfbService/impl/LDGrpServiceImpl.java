package com.sinosoft.cloud.mgr.yfbService.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.model.entity.LMCalModePojo;
import com.sinosoft.cloud.dal.yfbDao.mapper.LDGrpMapper;
import com.sinosoft.cloud.mgr.yfbService.LDGrpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LDGrpServiceImpl implements LDGrpService {

    @Autowired
    private LDGrpMapper ldGrpMapper;


    @Override
    public List<LMCalModePojo> selectByNoParam(LMCalModePojo lmCalModePojo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        return ldGrpMapper.selectByNoParam(lmCalModePojo);
    }

    @Override
    public int SelectGrpCount(LMCalModePojo lmCalModePojo) {
        return ldGrpMapper.SelectGrpCount(lmCalModePojo);
    }


    @Override
    public void insert(LMCalModePojo lmCalModePojo) {
        ldGrpMapper.insert(lmCalModePojo);
    }
}
