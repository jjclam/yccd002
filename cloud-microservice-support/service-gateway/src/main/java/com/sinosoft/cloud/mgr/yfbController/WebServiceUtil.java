package com.sinosoft.cloud.mgr.yfbController;

import com.sinosoft.lis.schema.LDGrpSchema;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.util.StringUtils;


public class WebServiceUtil {
    public static String sendService(String url,String method,String requestxml) throws Exception{
        //创建服务
        Service service=new Service();
        Call call=(Call)service.createCall();
        call.setTargetEndpointAddress(url);
        call.setOperationName(method);
        //call.setReturnType(XMLType.XSD_STRING);
        String request=(String)call.invoke(new Object[]{requestxml});
        return request;
    }



    //将核心返回的xml转化成pojo

    /*public LDGrpSchema xml2Pojo(String message) throws DocumentException {
        LDGrpSchema mLDGrpSchema = new LDGrpSchema();
        Document document = DocumentHelper.parseText(message);
        // 通过document对象获取根元素的信息
        Element Package = document.getRootElement();

        Element ResponseNodes = Package.element("ResponseNodes");
        Element ResponseNode= ResponseNodes.element("ResponseNode");
        Element BusinessObject=ResponseNode.element("BusinessObject");
        Element Result=ResponseNode.element("Result");
        Element ResultStatus=Result.element("ResultStatus");
        if("0".equals(ResultStatus.getText())){
            Element ResultInfoDesc=Result.element("ResultInfoDesc");
        }else{
            Element StateReason=BusinessObject.element("StateReason");
            String stateReason=StateReason.getText();
        }
        return mLDGrpSchema;
    }*/
}
