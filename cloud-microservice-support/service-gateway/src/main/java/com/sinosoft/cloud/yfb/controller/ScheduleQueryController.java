package com.sinosoft.cloud.yfb.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.LDDevices;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.dal.yfbDao.model.LWTrace;
import com.sinosoft.cloud.dal.yfbDao.model.entity.ScheduleInfo;
import com.sinosoft.cloud.yfb.service.ScheduleQueryService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.Chl_UNKNOWN_ERROR;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_NOT_FOUND;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.PARAM_NOT_FOUND;

/**
 * 进度查询
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/schedule")
public class ScheduleQueryController {
    private final static Log logger = LogFactory.getLog(ScheduleQueryController.class);
    @Autowired
    private ScheduleQueryService scheduleQueryService;

    @RequestMapping("/list.html")
    public String list(ModelMap model) {
        return "yfb/query_statistics/schedule/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String scheduleList(@ModelAttribute LDDevices ldDevices, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        int count = scheduleQueryService.countScheduleList(ldDevices);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<LDDevices> result = scheduleQueryService.getScheduleList(ldDevices, pageIndex, pageSize);
        pageModel.setList(result);
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/deleteCodeMaintain")
    @ResponseBody
//    String params
    public Map<String, Object> deleteCodeMaintain(@RequestParam(value = "params",required = false)String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String result = "";
        String deviceNo = po.getString("deviceNo");
        String useFlag = po.getString("useFlag");
            try {
                result = scheduleQueryService.deleteCodeMaintain(deviceNo);
                map.put("msg", result);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("系统异常");
                map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
                return map;
            }
            return map;
//        }
    }

/**
 * 添加
 * */
    @RequestMapping("/addCodeMaintain")
    @ResponseBody
    public Map<String, Object> addCodeMaintain(@RequestParam(value = "params",required = false)String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        try {
            String result = scheduleQueryService.addCodeMaintain(po);
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }


}
