package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ElasticJobService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/pro_mng")
public class ProManageController {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private ElasticJobService elasticJobService;

    @Value("${spring.elasticjoblite.url}")
    private String joburl ;

//    @RequestMapping("/functdef.html")
//    public String functDef(ModelMap model) {
//        System.out.println("joburl"+joburl);
//        List<LDCode> jobCode = elasticJobService.getCodes();
////        List<LDCode> stateCode = elasticJobService.getStates();
////        model.addAttribute("jobCode", jobCode);
////        model.addAttribute("stateCode", stateCode);
//        return "pro_mng/functdef";
//    }

    @RequestMapping("/prodef.html")
    public String proDef(ModelMap model) {
        System.out.println("joburl"+joburl);
        List<LDCode> jobCode = elasticJobService.getCodes();
//        List<LDCode> stateCode = elasticJobService.getStates();
//        model.addAttribute("jobCode", jobCode);
//        model.addAttribute("stateCode", stateCode);
        return "/pro_mng/prodef";
    }
    @RequestMapping("/mealdef.html")
    public String mealDef(ModelMap model) {
        System.out.println("joburl"+joburl);
        List<LDCode> jobCode = elasticJobService.getCodes();
//        List<LDCode> stateCode = elasticJobService.getStates();
//        model.addAttribute("jobCode", jobCode);
//        model.addAttribute("stateCode", stateCode);
        return "/pro_mng/mealdef";
    }

//    @RequestMapping("/mealauth.html")
//    public String mealAuth(ModelMap model) {
//        System.out.println("joburl"+joburl);
//        List<LDCode> jobCode = elasticJobService.getCodes();
////        List<LDCode> stateCode = elasticJobService.getStates();
////        model.addAttribute("jobCode", jobCode);
////        model.addAttribute("stateCode", stateCode);
//        return "/pro_mng/mealauth";
//    }
    @RequestMapping("/list1")
    @ResponseBody
    public String list(@ModelAttribute LdJobRunLog ldJobRunLog, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        String taskCode = ldJobRunLog.getTaskCode();

        if (taskCode == null){
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        //分页计数
        int count = elasticJobService.countForList(ldJobRunLog);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }

        List<LdJobRunLog> result = (List<LdJobRunLog>) elasticJobService.getResultByTaskName(ldJobRunLog,pageIndex,pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LdJobRunLog dJobRunLog : result) {
                if (dJobRunLog.getTaskName() != null) {}
                JSONObject object = (JSONObject) JSONObject.toJSON(dJobRunLog);
                array.add(object);
            }
            logger.info("结果返回："+result.toString());
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }
}
