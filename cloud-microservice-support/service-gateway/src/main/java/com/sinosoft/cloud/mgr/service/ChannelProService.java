package com.sinosoft.cloud.mgr.service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LAChannelProduct;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SaleProInfo;

import java.util.List;

public interface ChannelProService {
    SaleProInfo getDetailByProcode(String proCode, String channelCode);

    LAChannelProduct getEditByProcode(LAChannelProduct laChlPro);

    String addChlProInfo(JSONObject po);

    String updateChlProInfo(JSONObject po);

    List<ChlInfo> getChlCodeName();

    Integer count();

    Integer count(LAChannelProduct laChlPro);

    List<ChlInfo> getChlProInfoList(ChlInfo chlInfo, Integer pageIndex, Integer pageSize);
//    List<LAChannelProduct> getChlProInfoList(int offset, int limit, LAChannelProduct laChlPro);

    List<SaleProInfo> getRiskTypeCodeName();

    List<LAChannelProduct> getSaleProInfo(String riskType);
//    List<LAChannelProduct> getSaleProInfo();

    List<SaleProInfo> getAllProInfo(String riskType);

    String downChlPro(String channelCode, String proCode);

    List<ChlInfo> getChlInfoList(ChlInfo chlInfo, Integer pageIndex, Integer pageSize);

    List<ChlInfo> getChlProInfoExcel(ChlInfo chlInfo);

    List<SaleProInfo> getRiskTypeCodeNameEX(String riskType);

    Integer countChlProInfoList(ChlInfo chlInfo);

    LAChannelProduct getDetailByProChlcode(String proCode, String channelCode);

    SaleProInfo getRiskTypeName(String riskTypeCode);

    Integer countChlForUpDown(String channelCode);

    List<ChlInfo> getChlAllUpList(ChlInfo chlInfo, Integer pageIndex, Integer pageSize);

    String alldownChlPro(String channelCodes, String proCode, String riskType);

    String addupChlProInfo(JSONObject po);

    String getHttpUrl(String httpUrl);

    List<ChlInfo> getChlAgencyType();

    List<ChlInfo> getChlXXX(String channelCode);

    List<ChlInfo> findCirlBySe();

}
