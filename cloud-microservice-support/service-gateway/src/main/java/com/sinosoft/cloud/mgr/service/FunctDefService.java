package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.entity.LMCalModePojo;

import java.util.List;

public interface FunctDefService {
    List<LMCalModePojo> getResultByTaskName(LMCalModePojo lMCalModePojo, Integer pageIndex, Integer pageSize);
    List<LMCalModePojo> getCodes();
    Integer countForList(LMCalModePojo lMCalModePojo);
}
