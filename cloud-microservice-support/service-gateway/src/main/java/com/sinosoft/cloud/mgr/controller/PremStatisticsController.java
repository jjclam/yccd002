package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.PremStatisticsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 运营管理
 * 保费统计
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/prem_count")
public class PremStatisticsController {
    private final static Log logger = LogFactory.getLog(PremStatisticsController.class);
    @Autowired
    private ChannelProService channelProService;
    @Autowired
    private PremStatisticsService premStatisticsService;

    @RequestMapping("/list.html")
    public String core_list(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        return "operation/prem_count/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute Operation lcCont, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        if (lcCont.getSellType() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = premStatisticsService.countForPremCountList(lcCont);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }
        List<Operation> result = premStatisticsService.getPremCountList(lcCont, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (Operation pc : result) {
                if (pc.getCountDate() != null) pc.setCountDate(pc.getCountDate().substring(0,10));
                //appFlag
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }


}
