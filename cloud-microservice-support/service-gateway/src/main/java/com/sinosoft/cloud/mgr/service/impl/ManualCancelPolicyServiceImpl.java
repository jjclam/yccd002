package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.mapper.LAChannelProductMapper;
import com.sinosoft.cloud.dal.dao.mapper.LCContMapper;
import com.sinosoft.cloud.dal.dao.model.LAChannelProduct;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.mgr.service.ManualCancelPolicyService;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.Chl_UNKNOWN_ERROR;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_FAIL;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;
import static com.sinosoft.cloud.common.util.DateUtil.getSeqString;
import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;
import static com.sinosoft.cloud.common.util.HttpClientUtil.sendXMLDataByPost;
import static com.sinosoft.cloud.mgr.common.XMLWriteRead.cancelPolicyByXML;
import static com.sinosoft.cloud.mgr.common.XMLWriteRead.parseXMLDataFromPost;

@Service
public class ManualCancelPolicyServiceImpl implements ManualCancelPolicyService {
    private final static Log logger = LogFactory.getLog(ManualCancelPolicyServiceImpl.class);
    @Autowired
    private LCContMapper lcContMapper;
    @Autowired
    private LAChannelProductMapper laChannelProductMapper;

    @Override
    public String cancelPolicy(String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String contNo = po.getString("contNo");
        map.put("contNo", contNo);
        Operation lccont = lcContMapper.getCancelPolicyByContNo(contNo);
        String applyFlag = lccont.getAppFlag();
        if(applyFlag != null &&"0".equals(applyFlag)){
            map.put("appFlag", "d");
        }else {
            map.put("appFlag", "c");
        }
        String transDate = PubFun.getCurrentDate();
        map.put("transDate", transDate);
        map.put("endDate", transDate);
        map.put("state", "1");
        String transTime = PubFun.getCurrentTime();
        map.put("transTime", transTime);

        int modifyFlag = lcContMapper.updateAppFlag(map);
        String result = "";
        if (modifyFlag >= 1) {
            result = SUCCESS.getCode();
            logger.info("保单作废成功: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("保单作废失败: " + DB_FAIL.getMessage());
        }
        return result;
    }
}
