package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LCContMapper;
import com.sinosoft.cloud.dal.dao.mapper.LDPLANRISKMapper;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SettleInfo;
import com.sinosoft.cloud.mgr.service.PremStatisticsService;
import com.sinosoft.cloud.mgr.service.SettleExcelService;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_FAIL;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

@Service
public class SettleExcelServiceImpl implements SettleExcelService {
    private final static Log logger = LogFactory.getLog(SettleExcelServiceImpl.class);
    @Autowired
    private LCContMapper lcContMapper;
    @Autowired
    private LDPLANRISKMapper tLDPLANRISKMapper;

    @Override
    public List<SettleInfo> getSettleExcel(SettleInfo settleInfo) {

        return lcContMapper.selectSettleExcel(settleInfo);
    }

    @Override
    public List<ProInfo> getProductName() {
        return tLDPLANRISKMapper.selectNameAndCode();
    }

    @Override
    public List<ProInfo> getProductCode1(String admin) {
        return tLDPLANRISKMapper.selectCodeByUser(admin);
    }

    @Override
    public String importExcel(MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            List list = this.readExcel(inputStream, file.getOriginalFilename());
            inputStream.close();
            for (int i = 0; i < list.size(); i++) {
                List<Object> newCont = (ArrayList) list.get(i);
                logger.info("============object=============="+newCont.toString());
                String contNo = newCont.get(1).toString();
                String settlementDate = PubFun.getCurrentDate();
                String settlementStatus = "1";
                String settlementNo = PubFun.CreateSeq("seq_proposalno_11",20);
                Map<String, Object> map = new HashMap<>();
                map.put("contNo",contNo);
                map.put("settlementDate",settlementDate);
                map.put("settlementStatus",settlementStatus);
                map.put("settlementNo",settlementNo);

                Integer updateFlag = lcContMapper.updateSettleStatus(map);
                if (updateFlag == 1) {
                    logger.info("保单：" +contNo +"结算成功" + SUCCESS.getMessage());
                } else {
                    logger.info("保单：" +contNo +"结算成功" + DB_FAIL.getMessage());
                    return DB_FAIL.getMessage();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return SUCCESS.getCode();
    }

    private static List readExcel(InputStream in, String fileName) throws Exception {
        List list = new ArrayList<>();
        //创建Excel工作薄
        Workbook work = getWorkbook(in, fileName);
        if (null == work) {
            throw new Exception("创建Excel工作薄为空！");
        }
        Row row = null;
        Cell cell = null;

        Sheet sheet = work.getSheetAt(0);
        // 获取第一个实际行的下标
        logger.info("======sheet.getFirstRowNum()========="+sheet.getFirstRowNum());
        // 获取最后一个实际行的下标
        logger.info("======sheet.getLastRowNum()========="+sheet.getLastRowNum());
        for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            // 获取在某行第一个单元格的下标
            logger.info("======row.getFirstCellNum()========="+row.getFirstCellNum());
            // 获取在某行的列数
            logger.info("======row.getLastCellNum()========="+row.getLastCellNum());
            if (row == null || row.getFirstCellNum() == i) {
                continue;
            }
            List<Object> li = new ArrayList<>();
            for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
                cell = row.getCell(j);
                li.add(cell);
            }
            list.add(li);
        }
        work.close();//这行报错的话  看下导入jar包的版本
        return list;
    }

    /**
     * 判断文件格式
     *
     * @param inStr
     * @param fileName
     * @return
     * @throws Exception
     */
    private static Workbook getWorkbook(InputStream inStr, String fileName) throws Exception {
        Workbook workbook = null;
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        logger.info("==========fileType========"+fileType);
        if (".xls".equals(fileType)) {
            workbook = new HSSFWorkbook(inStr);
        }
//        else if (".xlsx".equals(fileType)) {
//            workbook = new XSSFWorkbook(inStr);
//        }
        else {
            throw new Exception("请上传excel文件！");
        }
        return workbook;
    }
}
