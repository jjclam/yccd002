package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.InsurancePolicySearchService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_NOT_FOUND;

/**
 * 运营管理
 * 保单查询
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/pol_search")
public class InsurancePolicySearchController {
    private final static Log logger = LogFactory.getLog(InsurancePolicySearchController.class);
    @Autowired
    private ChannelProService channelProService;
    @Autowired
    private InsurancePolicySearchService insurancePolicySearchService;

    @RequestMapping("/list.html")
    public String core_list(ModelMap model) {
        List<ChlInfo> laChlInfo = channelProService.getChlCodeName();
        model.addAttribute("laChlInfo", laChlInfo);
        return "operation/pol_search/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String chlList(@ModelAttribute Operation lcCont, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        if (lcCont.getContNo() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = insurancePolicySearchService.countForPolSearchList(lcCont);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<Operation> result = insurancePolicySearchService.getPolSearchList(lcCont, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (Operation pc : result) {
                if (pc.getCvaliDate() != null) pc.setCvaliDate(pc.getCvaliDate().substring(0,10));
                if (pc.getEndDate() != null) pc.setEndDate(pc.getEndDate().substring(0,10));
//                if (pc.getAppFlag() != null) {
//
//                }
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

}
