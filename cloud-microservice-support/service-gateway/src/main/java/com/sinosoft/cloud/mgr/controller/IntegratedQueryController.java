package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.LMRisk;
import com.sinosoft.cloud.dal.dao.model.entity.*;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.dal.yfbDao.model.LcGrpCont;
import com.sinosoft.cloud.mgr.common.AjaxJson;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.IntegratedQueryService;
import com.sinosoft.cloud.mgr.service.ProInfoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_NOT_FOUND;

/**
 * 综合查询
 * 保单查询、结算查询、单证查询、产品查询、套餐查询、出单网点查询、保费统计
 *
 * @author LS
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/query")
public class IntegratedQueryController {
    private final static Log logger = LogFactory.getLog(IntegratedQueryController.class);
    @Autowired
    private ChannelProService channelProService;

    @Autowired
    private IntegratedQueryService integratedQueryService;

    @Autowired
    private ProInfoService proInfoService;

    /**
     *
     * @param model
     * @return 1、保单查询页面
     */
    @RequestMapping("/contQuery.html")
    public String contQuery(ModelMap model) {
        List<ManageInfo> manageInfo = integratedQueryService.getManage();
        List<AgentComInfo> agentComInfo = integratedQueryService.getAgentCom();
        model.addAttribute("manageInfo", manageInfo);
        model.addAttribute("agentComInfo", agentComInfo);
//        return "query/contQuery";
        return "query/contQuery";
    }

    /**
     *  保单查询页面查询按钮
     */
    @RequestMapping("/contQueryList")
    @ResponseBody
    public AjaxJson contQueryList(@ModelAttribute ContQueryInfo contInfo, Integer page, Integer limit) {

        logger.info("=2="+contInfo);
        PageModel pageModel = new PageModel();
        AjaxJson aAjaxJson = new AjaxJson();
        int count = integratedQueryService.contSearchList(contInfo);
        logger.info("count=2="+count);
        logger.info("page=2="+page);
        logger.info("limit=2="+limit);
        List<ContQueryInfo> result = integratedQueryService.getContSearchList(contInfo, page, limit);
        logger.info("result=="+result);
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        aAjaxJson.setCode("0");
        aAjaxJson.setData(result);
        aAjaxJson.setCount(count);
        return aAjaxJson;
    }





    /**
     * 2、结算查询页面
     */
    @RequestMapping("/settlementQuery.html")
    public String settlementQuery(ModelMap model) {
        List<ManageInfo> manageInfo = integratedQueryService.getManage();
        List<AgentComInfo> agentComInfo = integratedQueryService.getAgentCom();
        model.addAttribute("manageInfo", manageInfo);
        model.addAttribute("agentComInfo", agentComInfo);


        return "query/settlementQuery";
    }

    /**
     *  结算查询页面查询按钮
     */
    @RequestMapping("/settlementQueryList")
    @ResponseBody
    public String settlementQueryList(@ModelAttribute ContQueryInfo contInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();

        logger.info("contInfo"+contInfo.toString());
        int count = integratedQueryService.settleSearchList(contInfo);

        logger.info("数量："+count);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }

        List<ContQueryInfo> result = integratedQueryService.getSettleSearchList(contInfo, pageIndex, pageSize);


        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (ContQueryInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    /**
     * 3、单证查询页面
     */
    @RequestMapping("/documentQuery.html")
    public String documentQuery(ModelMap model) {
        return "query/documentQuery";
    }

    /**
     *  单证查询页面查询按钮
     */
    @RequestMapping("/documentQueryList")
    @ResponseBody
    public String documentQueryList(@ModelAttribute LZCardInfo lzCardInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();

        logger.info("lzCardInfo"+lzCardInfo.toString());
        int count = integratedQueryService.documentSearchList(lzCardInfo);

        logger.info("数量："+count);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }

        List<LZCardInfo> result = integratedQueryService.getDocumentSearchList(lzCardInfo, pageIndex, pageSize);


        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LZCardInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    /**
     *  4、产品查询页面
     */
    @RequestMapping("/productQuery.html")
    public String productQuery(ModelMap model) {
        List<ProInfo> lmRiskSale = integratedQueryService.getProCodeName();
        model.addAttribute("lmRiskSale", lmRiskSale);
        return "query/productQuery";
    }

    @RequestMapping("/productQueryList")
    @ResponseBody
    public String productQueryList(@ModelAttribute ProInfo proInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();

        logger.info("proInfo"+proInfo.toString());
        int count = integratedQueryService.productSearchList(proInfo);

        logger.info("数量："+count);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<ProInfo> result = integratedQueryService.getProductSearchList(proInfo, pageIndex, pageSize);


        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (ProInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }


    /**
     * 5、套餐查询页面
     */
    @RequestMapping("/planQuery.html")
    public String planQuery(ModelMap model) {
        List<ProInfo> lmRiskSale = integratedQueryService.getPlanCodeName();
        model.addAttribute("lmRiskSale", lmRiskSale);
        return "query/planQuery";
    }

    /**
     *  套餐查询页面查询按钮
     * @param proInfo
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @RequestMapping("/planQueryList")
    @ResponseBody
    public String planQueryList(@ModelAttribute ProInfo proInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();

        logger.info("proInfo"+proInfo.toString());
        int count = integratedQueryService.planSearchList(proInfo);

        logger.info("数量："+count);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<ProInfo> result = integratedQueryService.getPlanSearchList(proInfo, pageIndex, pageSize);


        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (ProInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/planview.html")
    public String planview(String proCode,ModelMap model) {
        logger.info("进来了~~~~~~~~~~~~~~~~");
        logger.info("---"+proCode);
        model.addAttribute("proCode", proCode);
        return "query/planviewQuery";
    }

    @RequestMapping("/planviewQueryList")
    @ResponseBody
    public String planviewQueryList(String proCode, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        logger.info("proCode=="+proCode);
       /* int count = integratedQueryService.planSearchList(proInfo);

        logger.info("数量："+count);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
                return JSON.toJSONString(pageModel);
        }*/
        List<LMRiskInfo> result = integratedQueryService.getLmRiskInfo(proCode);
        logger.info("result.size()=="+result.size());
        if(result.size()==0){
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }


        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LMRiskInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(10);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    /**
     * 6、出单网点查询页面
     */
    @RequestMapping("/outletQuery.html")
    public String outletQuery(ModelMap model) {
        return "query/outletQuery";
    }

    /**
     *  出单网点查询页面查询按钮
     * @param lacom
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @RequestMapping("/outletQueryList")
    @ResponseBody
    public String outletQueryList(@ModelAttribute AgentComInfo lacom, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();

        logger.info("lacom"+lacom.toString());
        int count = integratedQueryService.outletSearchList(lacom);

        logger.info("数量："+count);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<AgentComInfo> result = integratedQueryService.getOutletSearchList(lacom, pageIndex, pageSize);
        logger.info("result"+result);

        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (AgentComInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }


    /**
     * 7、保费统计查询
     */
    @RequestMapping("/premQuery.html")
    public String premQuery(ModelMap model) {
        List<ManageInfo> manageInfo = integratedQueryService.getManage();
        List<AgentComInfo> agentComInfo = integratedQueryService.getAgentCom();
        List<ProInfo> lmRiskSale = integratedQueryService.getPlanCodeName();
        model.addAttribute("lmRiskSale", lmRiskSale);
        model.addAttribute("manageInfo", manageInfo);
        model.addAttribute("agentComInfo", agentComInfo);
        return "query/premQuery";
    }

    /**
     *  出单网点查询页面查询按钮
     * @param contQueryInfo
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @RequestMapping("/premQueryList")
    @ResponseBody
    public String premQueryList(@ModelAttribute ContQueryInfo contQueryInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();

        logger.info("contQueryInfo"+contQueryInfo.toString());
        int count = integratedQueryService.premSearchList(contQueryInfo);

        logger.info("数量："+count);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<PremQueryInfo> result = integratedQueryService.getPremSearchList(contQueryInfo, pageIndex, pageSize);
        logger.info("result"+result);

        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (PremQueryInfo pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }
}
