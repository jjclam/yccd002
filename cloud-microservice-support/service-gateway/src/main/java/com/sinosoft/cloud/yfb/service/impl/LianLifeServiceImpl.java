package com.sinosoft.cloud.yfb.service.impl;

import com.sinosoft.cloud.dal.yfbDao.mapper.LianLifeMapper;
import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.entity.Laagent;
import com.sinosoft.cloud.liAnlife.LianLifeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LianLifeServiceImpl implements LianLifeService {
    @Autowired
    LianLifeMapper mLianLifeMapper;

    @Override
    public List<Lacom> selectwangBitSelect(){
        return  mLianLifeMapper.selectwangBitSelect();
    }

    @Override
    public List<Lacom> selectManagecom() {
        return mLianLifeMapper.selectManagecom();
    }

    @Override
    public List<Lacom> selectManAgeComSelect(){
        return mLianLifeMapper.selectManAgeComSelect();
    }

    @Override
    public List<Laagent> selectAgentSelect() {
        return mLianLifeMapper.selectAgentSelect();
    }
}
