package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LCContMapper;
import com.sinosoft.cloud.dal.dao.mapper.RoleMapper;
import com.sinosoft.cloud.dal.dao.mapper.RoleMenuMapper;
import com.sinosoft.cloud.dal.dao.model.Role;
import com.sinosoft.cloud.dal.dao.model.RoleMenu;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.mgr.service.RoleService;
import com.sinosoft.cloud.mgr.service.SynPolicyService;
import com.sinosoft.lis.pubfun.PubFun;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_FAIL;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

/**
 * @Author: Caden
 * @Description:
 * @Date: on 2019/12/18.
 * @Modified By:
 */
@Service
public class SynPolicyServiceImpl implements SynPolicyService {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private LCContMapper lcContMapper;

    @Override
    public String saveCont(String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String contNo = po.getString("contNo");
        map.put("contNo", contNo);
        map.put("lang", "1");


        int modifyFlag = lcContMapper.updateSynFlag(map);
        String result = "";
        if (modifyFlag >= 1) {
            result = SUCCESS.getCode();
            logger.info("保单同步成功: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("保单同步失败: " + DB_FAIL.getMessage());
        }
        return result;
    }
}
