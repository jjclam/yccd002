package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.ProInfoService;
import com.sinosoft.cloud.mgr.service.ProSyncService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.HttpClientUtil.InputStreamTOString;
import static com.sinosoft.cloud.common.util.HttpClientUtil.getHttpURLConnection;

/**
 * 产品同步
 *
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/pro_sync")
public class ProSyncController {
    private final static Log logger = LogFactory.getLog(ProSyncController.class);
    @Autowired
    private ProInfoService proInfoService;
    @Autowired
    private ProSyncService proSyncService;
    @Autowired
    private ChannelProService channelProService;

    @RequestMapping("/list.html")
    public String listInput(ModelMap model) {
        List<ProInfo> lmRiskSale = proInfoService.getProCodeName();
        model.addAttribute("lmRiskSale", lmRiskSale);
        return "pro_sync/sync";
    }

    @RequestMapping("/upDown_list.html")
    public String listInputJ(ModelMap model) {
        List<ProInfo> lmRiskSale = proInfoService.getProCodeName();
        model.addAttribute("lmRiskSale", lmRiskSale);
        return "pro_sync/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute ProInfo proInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        if (proInfo.getProCode() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);

        }
        //首次页面不查找数据
        int count = proSyncService.count(proInfo);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }

        List<ProInfo> result = proSyncService.getproSyncList(proInfo, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (ProInfo pc : result) {
                //Date.0格式化
                if (pc.getStartDate() != null) pc.setStartDate(pc.getStartDate().substring(0,10));
                if (pc.getOverDate() != null) pc.setOverDate(pc.getOverDate().substring(0,10));
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        logger.info(result.toString());
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/upPro")
    @ResponseBody
    public Map<String, Object> upPro(@RequestParam(value = "proCodes") String proCodes,
                                     @RequestParam(value = "remark") String remark,
                                     @RequestParam(value = "remark2") String remark2,
                                     @RequestParam(value = "planKind1") String planKind1,
                                     @RequestParam(value = "planKind2") String planKind2,
                                     @RequestParam(value = "planKind3") String planKind3) {
        String str = remark+";"+remark2+";"+planKind1+";"+planKind2+";"+planKind3;
        logger.info("stt="+str);
        Map<String, Object> map = new HashMap<>();
        map.put("msg", Chl_UNKNOWN_ERROR.getCode());

        //截取获得字符串数组
        String params = "";
        if (StringUtils.isBlank(proCodes)) {
            logger.error(proCodes + "没有");
            map.put("msg", PARAM_NOT_FOUND.getCode());
            return map;
        } else {
            String[] strArray = proCodes.split(",");
            for (int i = 0; i < strArray.length; i++) {
                params = strArray[i];
                proSyncService.upPro(params,str);
            }
            map.put("msg", SUCCESS.getCode());
            return map;
        }
    }

    @RequestMapping("/downPro")
    @ResponseBody
    public Map<String, Object> downPro(@RequestParam(value = "proCodes") String proCodes) {
        Map<String, Object> map = new HashMap<>();
        map.put("msg", Chl_UNKNOWN_ERROR.getCode());
        //截取获得字符串数组
        String params = "";
        String result = "9999";
        if (StringUtils.isBlank(proCodes)) {
            logger.error(proCodes + "没有");
            map.put("msg", PARAM_NOT_FOUND.getCode());
            return map;
        } else {
            String[] strArray = proCodes.split(",");
            for (int i = 0; i < strArray.length; i++) {
                params = strArray[i];
                result = proSyncService.downPro(params);
            }
            map.put("msg", result);
        }
        return map;
    }

    @RequestMapping("/syncPro")
    @ResponseBody
    public Map<String, Object> syncPro(@RequestParam(value = "riskCode") String riskCode, @RequestParam(value = "combCode") String combCode, ModelMap model) {
        Map<String, Object> map = new HashMap<>();
        map.put("msg", Chl_UNKNOWN_ERROR.getCode());
        try {
            //url配置到数据库中
//            String strUrl = "http://10.52.200.40:8571/syndata";
//            String environment = "dat";
//            String environment = environments.trim();
//            logger.info("运行环境: " + environment);
            String httpUrl = "productsync";//对应ldcode1表中codetype
            String strUrl = channelProService.getHttpUrl(httpUrl);
            if (riskCode != "" & combCode == "") strUrl += "?riskCode=" + riskCode;
            if (combCode != "" & riskCode == "") strUrl += "?contPlanCode=" + combCode;
            if (combCode != "" & riskCode != "") strUrl += "?riskCode=" + riskCode + "&contPlanCode=" + combCode;
            logger.info("---------产品开始同步---------");
            logger.info("productsync: " + strUrl);
            HttpURLConnection back = getHttpURLConnection(strUrl);
            int status = back.getResponseCode();
            logger.info(status);
            String result = InputStreamTOString(back.getInputStream(), "UTF-8");
            logger.info(result);
            if (status == 200 & result.equals("success")) {
                map.put("msg", SUCCESS.getCode());
                logger.info("---------产品同步成功---------");
            } else {
                map.put("msg", result);
                logger.error("---------产品同步失败---------" + result);

            }
        } catch (IOException e) {
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            logger.error("---------产品同步异常-------" + Chl_UNKNOWN_ERROR.getCode() + " :Connection refused");
            e.printStackTrace();
        }
        //下拉框刷新数据
        List<ProInfo> lmRiskSale = proInfoService.getProCodeName();
        model.addAttribute("lmRiskSale", lmRiskSale);
        return map;
    }

}
