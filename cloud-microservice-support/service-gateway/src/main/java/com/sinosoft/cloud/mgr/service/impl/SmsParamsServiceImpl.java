package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.mapper.LDCodeMapper;
import com.sinosoft.cloud.dal.dao.mapper.SmsChlProMapper;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.SmsChlPro;
import com.sinosoft.cloud.mgr.service.SmsParamsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;

@Service
public class SmsParamsServiceImpl implements SmsParamsService {
    private final static Log logger = LogFactory.getLog(SmsParamsServiceImpl.class);
    @Autowired
    private SmsChlProMapper smsChlProMapper;
    @Autowired
    private LDCodeMapper ldCodeMapper;

    @Override
    public Integer countForSmsParamsList(SmsChlPro smsChlPro) {
        Map<String, Object> map = new HashMap<>();
        String channelCode = smsChlPro.getChannelCode();
        String riskCode = smsChlPro.getRiskCode();
        map.put("channelCode", channelCode);
        map.put("riskCode", riskCode);
        return smsChlProMapper.countForSmsParamsList(map);
    }

    @Override
    public List<SmsChlPro> getSmsParamsList(SmsChlPro smsChlPro, Integer pageIndex, Integer pageSize) {
        Map<String, Object> map = new HashMap<>();
        String channelCode = smsChlPro.getChannelCode();
        String riskCode = smsChlPro.getRiskCode();
        map.put("channelCode", channelCode);
        map.put("riskCode", riskCode);
        return smsChlProMapper.selectForSmsParamsList(map);
    }

    @Override
    public String addSmsMaintain(JSONObject po) {
        String channelCode = po.getString("channelCode");
        String proCode = po.getString("proCode");
        String smsInfo = po.getString("smsInfo");

        if (channelCode == "" || proCode == "" || smsInfo == "" ) {
            logger.error(PARAM_NOT_FOUND.getMessage());
            return PARAM_NOT_FOUND.getMessage();
        }

        //判断短信是否已配置过
        int flag = smsChlProMapper.verifySmsChlPro(channelCode, proCode);
        if (flag == 1) {
            logger.error("addSmsMaintain: " + CM_SMSPARAM_EXIST.getMessage());
            return CM_SMSPARAM_EXIST.getMessage();
        }

        Map<String, Object> map = new HashMap<>();
        map.put("channelCode", channelCode);
        map.put("riskCode", proCode);
        map.put("smsInfo", smsInfo);
        String createDate = getCurrentDefaultTimeStr();
        map.put("createDate", createDate);
        logger.info("addSmsMaintain(map): " + map);
        int addFlag = smsChlProMapper.addSmsMaintain(map);
        String result = "";
        if (addFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("addSmsMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("addSmsMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    public String updateSmsMaintain(JSONObject po) {
        String channelCode = po.getString("channelCode");
        String proCode = po.getString("proCode");
        String smsInfo = po.getString("smsInfo");

        if (channelCode == "" || proCode == "" || smsInfo == "" ) {
            logger.error(PARAM_NOT_FOUND.getMessage());
            return PARAM_NOT_FOUND.getMessage();
        }

        Map<String, Object> map = new HashMap<>();
        map.put("channelCode", channelCode);
        map.put("riskCode", proCode);
        map.put("smsInfo", smsInfo);
        String updateDate = getCurrentDefaultTimeStr();
        map.put("updateDate", updateDate);
        logger.info("updateSmsMaintain(map): " + map);
        int updateFlag = smsChlProMapper.updateSmsMaintain(map);
        String result = "";
        if (updateFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("updateSmsMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("updateSmsMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    @Transactional
    public String deleteSmsMaintain(List<SmsChlPro> deleteArray) {
        String result = "";
        int deleteFlag = 1;
        for (SmsChlPro sms : deleteArray) {
            Map<String, Object> params = new HashMap<>();
            params.put("channelCode", sms.getChannelCode());
            params.put("riskCode", sms.getRiskCode());
            deleteFlag = smsChlProMapper.deleteSmsMaintain(params);
            if (deleteFlag == 1) {
                result = SUCCESS.getCode();
                logger.info("deleteSmsMaintain: " + SUCCESS.getMessage());
            } else {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                result = DB_FAIL.getMessage();
                logger.info("deleteSmsMaintain: " + params + DB_FAIL.getMessage());
                return result;
            }
        }
        return result;
    }

    @Override
    public SmsChlPro getSmsEditInfo(String channelCode, String proCode) {
        return smsChlProMapper.getSmsEditInfo(channelCode, proCode);
    }

    @Override
    public List<LDCode> getSmsParamsLinkded(String codeType) {
        return ldCodeMapper.getSmsparamsList(codeType);
    }
}
