package com.sinosoft.cloud.mgr.yfbController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.Menu;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.LcGrpCont;
import com.sinosoft.cloud.mgr.common.AjaxJson;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.MenuService;
import com.sinosoft.cloud.yfb.service.LCGrpContService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/GrpAppentMan")
public class GrpAppentManController {
 @Autowired
 private MenuService menuService;
 @Autowired
 private LCGrpContService mLCGrpContService;
    @Autowired
    private ChannelProService channelProService;

    @RequestMapping("/showApp")
    public String clientList(ModelMap model) {
        return "yfb_templates/grpAppentMan";
    }


    /**   Add By Xcc
     * GrpAppentMan/showAppentTail */
    @RequestMapping("/showAppentTail")
    public String showAppentTail(ModelMap model,String name) {
        System.out.println(" name: " + name );
        List<ChlInfo> laChlAgencyType = channelProService.getChlAgencyType();
        List<ChlInfo> cirlBySe = channelProService.findCirlBySe();
        List<Lacom> lacoms = mLCGrpContService.selectLacomSelect();
        model.addAttribute("laChlAgencyType", laChlAgencyType);
        model.addAttribute("cirlBySe", cirlBySe);
        model.addAttribute("nameer", name);
        model.addAttribute("lacoms", lacoms);

        return "yfb_templates/grpAppentTail";
    }

    @RequestMapping("/showAppentTail2")
    public String showAppentTail2(ModelMap model) {
        List<ChlInfo> laChlAgencyType = channelProService.getChlAgencyType();
        model.addAttribute("laChlAgencyType", laChlAgencyType);
        return "yfb_templates/grpAppentTail";
    }




    @RequestMapping("/showAppTable")
    public String showAppTable(ModelMap model) {
        return "yfb_templates/grpAppentMan";
    }


    @RequestMapping("/showAppTable3")
    @ResponseBody
    public AjaxJson list3(@ModelAttribute LcGrpCont mLcGrpCont, Integer page, Integer limit,HttpServletRequest request) {
        String nameer = request.getParameter("nameer");
        mLcGrpCont.setProposalGrpContNo(nameer);
        PageModel pageModel = new PageModel();
        AjaxJson aAjaxJson = new AjaxJson();
        List<LcGrpCont> lcGrpContSchemas = mLCGrpContService.selectGrpLccontInf(mLcGrpCont, page, limit);
        int count = mLCGrpContService.findCount(mLcGrpCont);
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        aAjaxJson.setCode("0");
        aAjaxJson.setCount(count);
        aAjaxJson.setData(lcGrpContSchemas);
        return aAjaxJson;
    }


    @RequestMapping("/department")
    @ResponseBody
    public AjaxJson department(@ModelAttribute Lacom lacom, Integer page, Integer limit , HttpServletRequest request) {
        if (lacom.getManagecom()!=null&&(!("".equals(lacom.getManagecom())))){
            String[] strings = lacom.getManagecom().split("-");
            lacom.setManagecom(strings[0]);
        }
        PageModel pageModel = new PageModel();
        AjaxJson aAjaxJson = new AjaxJson();
        int count = mLCGrpContService.selectLacomCount(lacom);
        List<Lacom> lacoms = mLCGrpContService.selectLacom(lacom, page, limit);
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        aAjaxJson.setCode("0");
        aAjaxJson.setCount(count);
        aAjaxJson.setData(lacoms);
        return aAjaxJson;
    }


         @RequestMapping("/showAppTable2")
         @ResponseBody
         public AjaxJson list(@ModelAttribute LcGrpCont mLCGrpContSchema, Integer page, Integer limit) {
              PageModel pageModel = new PageModel();
              AjaxJson aAjaxJson = new AjaxJson();
              int count = mLCGrpContService.findCount(mLCGrpContSchema);
              List<LcGrpCont> lcGrpContSchemas = mLCGrpContService.selectGrpLccontInf(mLCGrpContSchema, page, limit);
              pageModel.setCount(count);
              pageModel.setMsg("ok");
              pageModel.setRel(true);
              aAjaxJson.setCode("0");
              aAjaxJson.setCount(count);
              aAjaxJson.setData(lcGrpContSchemas);
              return aAjaxJson;
         }


}
