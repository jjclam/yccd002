package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.MealAuthMapper;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.LDPlan;
import com.sinosoft.cloud.mgr.service.MealAuthService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MealAuthServiceImpl implements MealAuthService {
    private final Log logger = LogFactory.getLog(this.getClass());
//    @Autowired
//    LdJobRunLogMapper ldJobRunLogMapper;
    @Autowired
MealAuthMapper mealAuthMapper;
            ;
    @Override
    public List<LDPlan> getResultByTaskName(LDPlan lDPlan, Integer pageIndex, Integer pageSize) {
        logger.info("--pageIndex："+pageIndex+"--pageSize："+pageSize);
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("mangeCom", lDPlan.getMANAGECOM());
        map.put("state", lDPlan.getSTATE());
//        map.put("remark", lDPlan.getREMARK());
        map.put("startDate", lDPlan.getSTARTDATE());
        map.put("endDate", lDPlan.getENDDATE());
        return mealAuthMapper.selectByTaskName(map);
    }

    @Override
    public List<LDPlan> getCodes() {
        return mealAuthMapper.getCodes();
    }
    @Override
    public Integer countForList(LDPlan lDPlan) {
        Map<String, Object> map = new HashMap<>();
        map.put("calCode", lDPlan.getCONTPLANCODE());
        map.put("type", lDPlan.getMANAGECOM());
        map.put("remark", lDPlan.getMANAGECOM());
        return mealAuthMapper.countForList(map);
    }

    @Override
    public List<LACom> getMngCode() {
        return mealAuthMapper.getMngCode();
    }
}
