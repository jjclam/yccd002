package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.MenuMapper;
import com.sinosoft.cloud.dal.dao.mapper.RoleMapper;
import com.sinosoft.cloud.dal.dao.mapper.RoleMenuMapper;
import com.sinosoft.cloud.dal.dao.model.Menu;
import com.sinosoft.cloud.dal.dao.model.Role;
import com.sinosoft.cloud.dal.dao.model.RoleExample;
import com.sinosoft.cloud.dal.dao.model.RoleMenu;
import com.sinosoft.cloud.mgr.service.MenuService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Author: Caden
 * @Description:11
 * @Date: on 2019/12/16.
 * @Modified By:
 */
@Service
public class MenuServiceImpl implements MenuService {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Override
    public List<Menu> getMenu() {
        return menuMapper.selectMenu();
    }

    @Override
    public List<Role> getrole(RoleExample example) {
        return roleMapper.selectByExample(example);
    }

    @Override
    public List<Menu> parentMenu() {
        return menuMapper.getMenu();
    }


    @Override
    public List<Menu> getMenuList(Menu menu, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        String name = menu.getName();
        return menuMapper.selectByNoParam(name);
    }


    @Override
    public int count(Menu menu) {
        String name = menu.getName();
        return menuMapper.SelectMenuCount(name);
    }

    @Override
    public Menu selectMenu(String menuId) {
        Integer i ;
        Menu menu =  new Menu();
        if (menuId != null) {
            i = Integer.valueOf(menuId);
            menu =   menuMapper.selectByPrimaryKey(menuId);
        }
        return menu;
    }

    @Override
    public int addMchInfo(Menu menu) {
        String  parent_id = menu.getParentId();
        int MenuI = menuMapper.SelectMenuId();
        menu.setId(String.valueOf(MenuI));

        menu.setDelFlag("0");
        String href = menu.getHref();
        String field = menu.getField();
        if(!"0".equals(parent_id) || parent_id !="0") {
            int  Sort= menuMapper.SelectMenuSort(String.valueOf(parent_id));
            logger.info("查看主键MenuI"+MenuI+"查看Sort"+Sort);

            menu.setSort(String.valueOf(Sort));
        }else {
            menu.setSort("10000");
        }

        if( "0".equals(parent_id) || parent_id =="0"){
            menu.setParent("主级菜单");
                    if(field=="2" || "2".equals(field)){
//                        menu.setPermission("insurance:firstinsure");
                        menu.setPermission("1");
                        menu.setType("1");
                        menu.setIcon("fa-cubes");
                        menu.setRemarks1("中台");
                    }else {
                        menu.setType("M");
                        menu.setIcon("peoples");
                        menu.setPermission("insurance:firstinsure");
                        if(field=="0" || "0".equals(field) ){
                            menu.setRemarks1("外勤");
                        }else {
                            menu.setRemarks1("企业HR");
                        }
                    }
        }else {
            menu.setParent("子菜单");
            if(field=="2"|| "2".equals(field) ){
//                menu.setPermission("insurance:firstinsure:list");
                menu.setPermission("1");
                menu.setType("2");
//                menu.setIcon("fa-xe62a");
                menu.setIcon("&#xe62a");
                menu.setRemarks1("中台");
            }else {
                menu.setType("C");
                menu.setIcon("post");
                menu.setPermission("insurance:firstinsure:list");
                if(field=="0" || "0".equals(field) ){
                    menu.setRemarks1("外勤");
                }else {
                    menu.setRemarks1("企业HR");
                }
            }
        }
        Date date = new Date();// 获取当前时间
        menu.setCreateDate(date);
        menu.setUpdateDate(date);
        String RoleID = menu.getRoleID();
        //添加中间表
        if(!"".equals(RoleID) && RoleID !=null ) {

            RoleMenu roleMenu =new RoleMenu();
            roleMenu.setMenu_id(MenuI);
            roleMenu.setRole_id(Integer.valueOf(RoleID));
            roleMenuMapper.insert(roleMenu);
        }
        return menuMapper.insert(menu);
    }

    @Override
    public int updateMchInfo(Menu menu) {
        String  parent_id = menu.getParentId();

        if(!"0".equals(parent_id) || parent_id !="0") {
            int  Sort= menuMapper.SelectMenuSort(String.valueOf(parent_id));
            menu.setSort(String.valueOf(Sort));
        }else {
            menu.setSort("10000");
        }
        String field = menu.getField();
        menu.setDelFlag("0");
        String href = menu.getHref();
        if("0".equals(parent_id) || parent_id =="0"){
            menu.setParent("主级菜单");
            if(field=="2" || "2".equals(field)){
//                menu.setPermission("insurance:firstinsure");
                menu.setPermission("1");
                menu.setType("1");
                menu.setIcon("fa-cubes");
                menu.setRemarks1("中台");
            }else {
                menu.setType("M");
                menu.setIcon("peoples");
                menu.setPermission("insurance:firstinsure");
                if(field=="0" || "0".equals(field) ){
                    menu.setRemarks1("外勤");
                }else {
                    menu.setRemarks1("企业HR");
                }

            }
        }else {
            menu.setParent("子菜单");
            if(field=="2"|| "2".equals(field) ){
//                menu.setPermission("insurance:firstinsure:list");
                menu.setPermission("1");
                menu.setType("2");
//                menu.setIcon("fa-xe62a");
                menu.setIcon("&#xe62a");
                menu.setRemarks1("中台");
            }else {
                menu.setType("C");
                menu.setIcon("post");
                menu.setPermission("insurance:firstinsure:list");
                if(field=="0" || "0".equals(field) ){
                    menu.setRemarks1("外勤");
                }else {
                    menu.setRemarks1("企业HR");
                }
            }
        }

        Date date = new Date();// 获取当前时间
        menu.setUpdateDate(date);
        return  menuMapper.updateByPrimaryKeySelective(menu);
    }

    @Override
    public List<Menu> getparentMenus(String field) {
        return menuMapper.getparentMenus(field);
    }

    @Override
    public Menu getparentMenu(String mchId) {
        return menuMapper.getparentMenu(mchId);
    }
}