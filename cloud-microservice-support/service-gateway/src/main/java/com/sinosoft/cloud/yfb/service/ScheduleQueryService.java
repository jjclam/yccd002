package com.sinosoft.cloud.yfb.service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDDevices;
import com.sinosoft.cloud.dal.yfbDao.model.LWTrace;
import com.sinosoft.cloud.dal.yfbDao.model.entity.ScheduleInfo;

import java.util.List;

public interface ScheduleQueryService {
    int countScheduleList(LDDevices ldDevices);

    List<LDDevices> getScheduleList(LDDevices ldDevices, Integer pageIndex, Integer pageSize);

    String deleteCodeMaintain(String deviceNo);

    String addCodeMaintain(JSONObject po);
}
