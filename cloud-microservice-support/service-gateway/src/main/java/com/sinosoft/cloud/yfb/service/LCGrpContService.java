package com.sinosoft.cloud.yfb.service;

import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.LcGrpCont;
import com.sinosoft.lis.schema.LCGrpContSchema;

import java.util.List;

public interface LCGrpContService {

    public List<LcGrpCont> selectGrpLccontInf(LcGrpCont mLCGrpContSchema, Integer page, Integer limit);

    public Integer findCount(LcGrpCont mLCGrpContSchema);

    public List<Lacom> selectLacom(Lacom lacom, Integer page, Integer limit);

    public Integer selectLacomCount(Lacom lacom);

    public List<Lacom> selectLacomSelect();
}
