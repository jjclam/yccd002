package com.sinosoft.cloud.mgr.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.IntegratedQueryMapper;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.model.entity.*;
import com.sinosoft.cloud.mgr.service.IntegratedQueryService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IntegratedQueryServiceImpl implements IntegratedQueryService {

    private final static Log logger = LogFactory.getLog(IntegratedQueryServiceImpl.class);

    @Autowired
    private IntegratedQueryMapper integratedQueryMapper;

    @Override
    public int contSearchList(ContQueryInfo contInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put("agentcom", contInfo.getAgentCom());
        map.put("contno", contInfo.getContno());
        map.put("signState", contInfo.getSignState());
        map.put("managecom", contInfo.getManagecom());
        map.put("operator", contInfo.getOperator());
        map.put("appntname", contInfo.getAppntname());
        map.put("polapplyStartDate", contInfo.getPolapplyStartDate());
        map.put("polapplyEndDate", contInfo.getPolapplyEndDate());
        map.put("insuredname", contInfo.getInsuredname());
        map.put("insuredidno", contInfo.getInsuredidno());
        map.put("settlementStatus", contInfo.getSettlementStatus());
        map.put("settlementNumber", contInfo.getSettlementNumber());
        return integratedQueryMapper.contSearchList(map);
    }

    @Override
    public List<ContQueryInfo> getContSearchList(ContQueryInfo contInfo, Integer pageIndex, Integer pageSize) {

        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("agentcom", contInfo.getAgentCom());
        map.put("contno", contInfo.getContno());
        map.put("signState", contInfo.getSignState());
        map.put("managecom", contInfo.getManagecom());
        map.put("operator", contInfo.getOperator());
        map.put("appntname", contInfo.getAppntname());
        map.put("polapplyStartDate", contInfo.getPolapplyStartDate());
        map.put("polapplyEndDate", contInfo.getPolapplyEndDate());
        map.put("insuredname", contInfo.getInsuredname());
        map.put("insuredidno", contInfo.getInsuredidno());
        map.put("settlementStatus", contInfo.getSettlementStatus());
        map.put("settlementNumber", contInfo.getSettlementNumber());
        logger.info("getContSearchList: " + map);
        return integratedQueryMapper.getContSearchList(map);

    }

    @Override
    public List<ManageInfo> getManage() {
        return integratedQueryMapper.getManage();
    }

    @Override
    public List<AgentComInfo> getAgentCom() {
        return integratedQueryMapper.getAgentCom();
    }

    @Override
    public int settleSearchList(ContQueryInfo contInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put("managecom", contInfo.getManagecom());
        map.put("agentcom", contInfo.getAgentCom());
        map.put("settlementStatus", contInfo.getSettlementStatus());
        map.put("settlementNumber", contInfo.getSettlementNumber());
        map.put("settlementDate",contInfo.getSettlementDate());
        map.put("contno",contInfo.getContno());

        return integratedQueryMapper.settleSearchList(map);
    }

    @Override
    public List<ContQueryInfo> getSettleSearchList(ContQueryInfo contInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("managecom", contInfo.getManagecom());
        map.put("agentcom", contInfo.getAgentCom());
        map.put("settlementStatus", contInfo.getSettlementStatus());
        map.put("settlementNumber", contInfo.getSettlementNumber());
        map.put("settlementDate",contInfo.getSettlementDate());
        map.put("contno",contInfo.getContno());

        return integratedQueryMapper.getSettleSearchList(map);
    }

    @Override
    public List<ProInfo> getProCodeName() {
        return integratedQueryMapper.getProCodeName();
    }

    @Override
    public int productSearchList(ProInfo proInfo) {

        Map<String, Object> map = new HashMap<>();
        map.put("proCode", proInfo.getProCode());
        map.put("proName", proInfo.getProName());
        map.put("standard", proInfo.getStandard() );

        return integratedQueryMapper.productSearchList(map);
    }

    @Override
    public List<ProInfo> getProductSearchList(ProInfo proInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("proCode", proInfo.getProCode());
        map.put("proName", proInfo.getProName());
        map.put("standard", proInfo.getStandard() );

        return integratedQueryMapper.getProductSearchList(map);
    }

    @Override
    public List<ProInfo> getPlanCodeName() {
        return integratedQueryMapper.getPlanCodeName();
    }

    @Override
    public int planSearchList(ProInfo proInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put("proCode", proInfo.getProCode());
        map.put("proName", proInfo.getProName());
        map.put("standard", proInfo.getStandard());
        return integratedQueryMapper.planSearchList(map);
    }

    @Override
    public List<ProInfo> getPlanSearchList(ProInfo proInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("proCode", proInfo.getProCode());
        map.put("proName", proInfo.getProName());
        map.put("standard", proInfo.getStandard());
        return integratedQueryMapper.getPlanSearchList(map);
    }

    @Override
    public int outletSearchList(AgentComInfo lacom) {
        Map<String, Object> map = new HashMap<>();
        map.put("agentcom", lacom.getAgentCom());
        map.put("name", lacom.getAgentName());
        map.put("address", lacom.getAddress());
        map.put("operator", lacom.getOperator());
        map.put("operatorName", lacom.getOperatorName());
        return integratedQueryMapper.outletSearchList(map);
    }

    @Override
    public List<AgentComInfo> getOutletSearchList(AgentComInfo lacom, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("agentcom", lacom.getAgentCom());
        map.put("name", lacom.getAgentName());
        map.put("address", lacom.getAddress());
        map.put("operator", lacom.getOperator());
        map.put("operatorName", lacom.getOperatorName());
        return integratedQueryMapper.getOutletSearchList(map);
    }

    @Override
    public int premSearchList(ContQueryInfo contInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put("productcode", contInfo.getProductcode());
        map.put("managecom", contInfo.getManagecom());
        map.put("agentcom", contInfo.getAgentCom());
        map.put("signState", contInfo.getSignState());
        map.put("settlementStatus", contInfo.getSettlementStatus());
        map.put("operator", contInfo.getOperator());
        map.put("polapplyStartDate", contInfo.getPolapplyStartDate());
        map.put("polapplyEndDate", contInfo.getPolapplyEndDate());
        return integratedQueryMapper.premSearchList(map);
    }

    @Override
    public List<PremQueryInfo> getPremSearchList(ContQueryInfo contInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("productcode", contInfo.getProductcode());
        map.put("managecom", contInfo.getManagecom());
        map.put("agentcom", contInfo.getAgentCom());
        map.put("signState", contInfo.getSignState());
        map.put("settlementStatus", contInfo.getSettlementStatus());
        map.put("operator", contInfo.getOperator());
        map.put("polapplyStartDate", contInfo.getPolapplyStartDate());
        map.put("polapplyEndDate", contInfo.getPolapplyEndDate());
        return integratedQueryMapper.getPremSearchList(map);
    }

    @Override
    public int documentSearchList(LZCardInfo lzCardInfo) {
        Map<String, Object> map = new HashMap<>();
        map.put("certifyCode", lzCardInfo.getCertifyCode());
        map.put("stateFlag", lzCardInfo.getStateFlag());
        map.put("sendOut", lzCardInfo.getSendOut());
        map.put("receive", lzCardInfo.getReceive());
        map.put("operator", lzCardInfo.getOperator());
        map.put("handler", lzCardInfo.getHandler());
        map.put("handlerStartDate", lzCardInfo.getHandlerStartDate());
        map.put("handlerEndDate", lzCardInfo.getHandlerEndDate());
        map.put("makeStartDate", lzCardInfo.getMakeStartDate());
        map.put("makeEndDate", lzCardInfo.getMakeEndDate());
        map.put("startNo", lzCardInfo.getStartNo());
        map.put("endNo", lzCardInfo.getEndNo());
        return integratedQueryMapper.documentSearchList(map);

    }

    @Override
    public List<LZCardInfo> getDocumentSearchList(LZCardInfo lzCardInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("certifyCode", lzCardInfo.getCertifyCode());
        map.put("stateFlag", lzCardInfo.getStateFlag());
        map.put("sendOut", lzCardInfo.getSendOut());
        map.put("receive", lzCardInfo.getReceive());
        map.put("operator", lzCardInfo.getOperator());
        map.put("handler", lzCardInfo.getHandler());
        map.put("handlerStartDate", lzCardInfo.getHandlerStartDate());
        map.put("handlerEndDate", lzCardInfo.getHandlerEndDate());
        map.put("makeStartDate", lzCardInfo.getMakeStartDate());
        map.put("makeEndDate", lzCardInfo.getMakeEndDate());
        map.put("startNo", lzCardInfo.getStartNo());
        map.put("endNo", lzCardInfo.getEndNo());
        return integratedQueryMapper.getDocumentSearchList(map);
    }

    @Override
    public List<LMRiskInfo> getLmRiskInfo(String proCode) {
        Map<String, Object> map = new HashMap<>();
        map.put("proCode", proCode);
        return integratedQueryMapper.getLmRiskInfo(map);
    }
}
