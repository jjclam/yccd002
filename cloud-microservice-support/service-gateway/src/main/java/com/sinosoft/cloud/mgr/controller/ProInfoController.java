package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.ProInfoService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;

/**
 * 产品中心
 *
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/pro_center")
public class ProInfoController {
    private final static Log logger = LogFactory.getLog(ProInfoController.class);
    @Autowired
    private ProInfoService proInfoService;

    @RequestMapping("/standard_list.html")
    public String listInput(ModelMap model) {
        List<ProInfo> lmRiskSale = proInfoService.getProCodeName();
        model.addAttribute("lmRiskSale", lmRiskSale);
        return "pro_info/list";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute ProInfo proInfo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
//        int count = proInfoService.count();
//        if(count <= 0) return JSON.toJSONString(pageModel);

        //首次页面不查找数据
         if (proInfo.getProCode() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = proInfoService.countProInfo(proInfo);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<ProInfo> result = proInfoService.getproInfoList(proInfo, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (ProInfo pc : result) {
                //jdbc的.0问题数据格式化
                if (pc.getStartDate() != null) pc.setStartDate(pc.getStartDate().substring(0,10));
                if (pc.getOverDate() != null) pc.setOverDate(pc.getOverDate().substring(0,10));
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
//                count += 1;
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        logger.info(result.toString());
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/save")
    @ResponseBody
    public Map<String, Object> savePro(@RequestParam(value = "proCodes") String proCodes, @RequestParam(value = "proType") String proType) {
        Map<String, Object> map = new HashMap<>();
        map.put("msg", Chl_UNKNOWN_ERROR.getCode());
        //截取获得字符串数组
        String flag = proType;
        String result = "";
        if(StringUtils.isBlank(proCodes)) {
            logger.error(proCodes + "没有");
            map.put("msg", PARAM_NOT_FOUND.getCode());
            return map;
        }else if (StringUtils.isBlank(proType)) {
            logger.error(proCodes + "没有");
            map.put("msg", PARAM_NOT_FOUND.getCode());
            return map;
        }else {
            String[] strArray = proCodes.split(",");
            for (int i = 0; i < strArray.length; i++) {
                result = strArray[i];
                proInfoService.savePro(result, flag);
            }
            map.put("msg", SUCCESS.getCode());
        }
        return map;
    }

}
