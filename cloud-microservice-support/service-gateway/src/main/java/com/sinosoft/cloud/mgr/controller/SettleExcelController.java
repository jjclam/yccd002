package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.entity.ChlInfo;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.dal.dao.model.entity.SettleInfo;
import com.sinosoft.cloud.dal.dao.model.entity.Operation;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.common.ObjectExcelView;
import com.sinosoft.cloud.mgr.service.ChannelProService;
import com.sinosoft.cloud.mgr.service.PremStatisticsService;
import com.sinosoft.cloud.mgr.service.SettleExcelService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

/**
 * 出单管理
 * 保单结算
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping(value = "/pol_settle")
public class SettleExcelController {
    private final static Log logger = LogFactory.getLog(SettleExcelController.class);
    @Autowired
    private SettleExcelService tSettleExcelService;

    @RequestMapping("/list.html")
    public String core_list(ModelMap model) {
        List<ProInfo> proInfo = tSettleExcelService.getProductName();
        model.addAttribute("proInfo", proInfo);
        return "/pol_settle/list";
    }

    /**导出到excel
     * @param
     * @throws Exception
     */
    @RequestMapping(value="/settleExcel")
    public ModelAndView settleExcel(@ModelAttribute SettleInfo settleInfo) throws Exception{
        ModelAndView mv = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<>();
        List<String> titles = new ArrayList<>();
        titles.add("序号"); //1
        titles.add("保单号"); //2
        titles.add("套餐编码"); //3
        titles.add("套餐名称"); //4
        titles.add("投保日期"); //5
        titles.add("保单生效日期"); //6
        titles.add("保单状态"); //7
        titles.add("是否结算"); //8
        titles.add("结算日期"); //9
        titles.add("结算号码"); //10
        titles.add("总保费"); //11
        dataMap.put("titles", titles);
        List<SettleInfo> varOList = tSettleExcelService.getSettleExcel(settleInfo);
        List<Map> varList = new ArrayList<>();
        for(int i=0,j=1;i<varOList.size();i++){
            Map<String, Object> vpd = new HashMap();
            vpd.put("var1", String.valueOf(j++));        //1
            vpd.put("var2", varOList.get(i).getContNo());    //2
            vpd.put("var3", varOList.get(i).getProductCode()); //3
            vpd.put("var4", varOList.get(i).getProductName()); //4
            vpd.put("var5", varOList.get(i).getPolApplyDate()); //5
            vpd.put("var6", varOList.get(i).getcValidate()); //6
            vpd.put("var7", varOList.get(i).getAppFlag()); //7
            vpd.put("var8", varOList.get(i).getSettleStatus()); //8
            vpd.put("var9", varOList.get(i).getSettleDate()); //9
            vpd.put("var10", varOList.get(i).getSettleNumber()); //10
            vpd.put("var11", varOList.get(i).getSumPrem()); //11

            varList.add(vpd);
        }

        dataMap.put("varList", varList);
        ObjectExcelView erv = new ObjectExcelView();
        mv = new ModelAndView(erv, dataMap);
        logger.info("---------EXCEL_OVER---------");
        return mv;
    }

    @RequestMapping("/importData")
    @ResponseBody
    public Map<String, Object> importData(@RequestPart(value = "file") MultipartFile file) {
        String result = tSettleExcelService.importExcel(file);
        Map<String, Object> map = new HashMap<>();
        if (result == SUCCESS.getCode()) {
            map.put("msg", SUCCESS.getCode());
            logger.info("---------结算成功---------");
        } else {
            map.put("msg", result);
            logger.error("---------结算失败---------" + result);
        }
        return map;
    }


}
