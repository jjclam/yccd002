
package com.sinosoft.cloud.mgr.service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.entity.SysUser;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrp;

import java.util.List;

public interface UserInfoService {
    String addUser(JSONObject po);

    int countUserInfo(SysUser sysUser);

    List<SysUser> getUserInfoList(SysUser sysUser, Integer pageIndex, Integer pageSize);

    String updateUser(JSONObject po);

    String deleteUser(String userNames);

    List<LDGrp> getAllInstitutions(String field);

    SysUser getDetailByuserName(String userNames);

    List<SysUser> getUserPosition(String field);
}
