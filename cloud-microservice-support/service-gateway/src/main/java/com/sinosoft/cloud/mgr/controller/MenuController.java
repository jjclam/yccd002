package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.Menu;
import com.sinosoft.cloud.dal.dao.model.RoleExample;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.MenuService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_NOT_FOUND;

/**
 * @Author: Caden
 * @Description:
 * @Date: on 2019/12/16.
 * @Modified By:
 */
@Controller
@RequestMapping("/menu")
public class MenuController {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private MenuService menuService;
    @RequestMapping("/list.html")
    public String listInput(ModelMap model) {
        List<Menu> menu = menuService.getMenu();
        model.addAttribute("menu", menu);
        return "menu/list";
    }
    @RequestMapping("/edit.html")
    public String editInput(String mchId, ModelMap model,RoleExample example) {
//        List<Role> getrole = menuService.getrole(example);
//        List<Menu> parentMenu = menuService.parentMenu();
        if (!mchId.equals("undefined")) {
            Menu parentMenu = menuService.getparentMenu(mchId);
            model.put("parentMenu", parentMenu);
        }

        if ("undefined".equals(mchId) || mchId == "undefined") {
            mchId = "";
        }
        Menu item = null;
        if(StringUtils.isNotBlank(mchId)) {
           item = menuService.selectMenu(mchId);
        }
        if(item == null) item = new Menu();
//        model.put("role", getrole);
        model.put("item", item);
        return "menu/edit";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute Menu menu, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        int count = menuService.count(menu);
        //首次页面不查找数据
        if (menu.getName() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }

        if(count <= 0) return JSON.toJSONString(pageModel);
        List<Menu> menuList = menuService.getMenuList( menu,pageIndex, pageSize);
        if(!CollectionUtils.isEmpty(menuList)) {
            JSONArray array = new JSONArray();
            for(Menu me : menuList) {
                JSONObject object = (JSONObject) JSONObject.toJSON(me);
                //  object.put("createTime", DateUtil.date2Str(mi.getCreateTime()));
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public String save(@RequestParam String params) {
        JSONObject po = JSONObject.parseObject(params);
        Menu menu = new Menu();
        //id
        String mchId = po.getString("id");
        //名称
        menu.setName(po.getString("name"));
        //链接
        menu.setHref(po.getString("href"));
        menu.setField(po.getString("field"));
        menu.setPath(po.getString("path"));
        //图标
        //图标页面隐藏手动配置,改为默认父级菜单和子集菜单,方法在service层setIcon指定默认类型
       menu.setRemarks(po.getString("permission"));
        String parentId = po.getString("parentId");
        if(!"".equals(parentId) && parentId !=null ) {
            menu.setParentId(parentId);//Integer.valueOf(parentId));

        }else {
            menu.setParentId("0");

        }
        menu.setRoleID(po.getString("role"));
        int result=1;
        if(StringUtils.isBlank(mchId)) {
            // 添加
            result = menuService.addMchInfo(menu);
        }else {
            // 修改
            menu.setId(mchId);
            result = menuService.updateMchInfo(menu);
        }
        return result+"";
    }

    @RequestMapping("/view.html")
    public String viewInput(String mchId, ModelMap model) {
        Menu item = null;
        if(StringUtils.isNotBlank(mchId)) {
        }
        model.put("item", item);
        return "menu/view";
    }

    @RequestMapping("/linkedParentMenus")
    @ResponseBody
    public Map<String, Object> linkedParentMenus(@RequestParam String field) {
        Map<String, Object> map = new HashMap<>();
        List<Menu> list = menuService.getparentMenus(field);
        logger.info(list);
        if (list.size() == 0) {
            map.put("msg", DB_NOT_FOUND.getMessage());
        }
        map.put("linkedParentMenus", list);
        return map;
    }

}
