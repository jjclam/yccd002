package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.sinosoft.cloud.dal.dao.model.entity.LMCalModePojo;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.FunctDefService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/pro_mng")
public class FunctDefController {
    private final Log logger = LogFactory.getLog(this.getClass());

@Autowired
private FunctDefService functDefService;

    @RequestMapping("/functdef.html")
    public String listInput(ModelMap model) {
        List<LMCalModePojo> cal = functDefService.getCodes();
        model.addAttribute("cal", cal);
        return "pro_mng/functdef";
    }

    @RequestMapping("/functdef")
    @ResponseBody
    public String list(@ModelAttribute LMCalModePojo lMCalModePojo, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        String calCode = lMCalModePojo.getCalCode();

        if (calCode == null){
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        //分页计数
        int count = functDefService.countForList(lMCalModePojo);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }

        List<LMCalModePojo> result =  functDefService.getResultByTaskName(lMCalModePojo,pageIndex,pageSize);
     /*   if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LdJobRunLog dJobRunLog : result) {
                if (dJobRunLog.getTaskName() != null) {}
                JSONObject object = (JSONObject) JSONObject.toJSON(dJobRunLog);
                array.add(object);
            }
            logger.info("结果返回："+result.toString());
            pageModel.setList(array);
        }*/
        pageModel.setList(result);
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }
}
