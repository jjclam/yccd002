package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.entity.Operation;

import java.util.List;

public interface InsurancePolicySearchService {
    int countForPolSearchList(Operation lcCont);

    List<Operation> getPolSearchList(Operation lcCont, Integer pageIndex, Integer pageSize);
}
