package com.sinosoft.cloud.yfb.service.impl;

import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.yfbDao.mapper.LcGrpContMapper;
import com.sinosoft.cloud.dal.yfbDao.model.Lacom;
import com.sinosoft.cloud.dal.yfbDao.model.LcGrpCont;
import com.sinosoft.cloud.yfb.service.LCGrpContService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LCGrpContServiceImpl implements LCGrpContService {
    @Autowired
    LcGrpContMapper mLcGrpContMapper;

    @Override
    public List<LcGrpCont> selectGrpLccontInf(LcGrpCont mLCGrpContSchema ,Integer page,Integer limit) {
        PageHelper.startPage(page, limit);
        List<LcGrpCont> lcGrpContSchemas = mLcGrpContMapper.selectGrpLccontInf(mLCGrpContSchema);
        return lcGrpContSchemas;
    }

    @Override
    public Integer findCount(LcGrpCont mLCGrpContSchema){
        Integer count = mLcGrpContMapper.findCount(mLCGrpContSchema);
        return count;
    }

    public List<Lacom> selectLacom(Lacom lacom,Integer page,Integer limit){
        PageHelper.startPage(page, limit);
        return mLcGrpContMapper.selectLacom(lacom);
    }

    public Integer selectLacomCount(Lacom lacom){
        return mLcGrpContMapper.selectLacomCount(lacom);
    }

    public List<Lacom> selectLacomSelect(){
        return mLcGrpContMapper.selectLacomSelect();
    }
}
