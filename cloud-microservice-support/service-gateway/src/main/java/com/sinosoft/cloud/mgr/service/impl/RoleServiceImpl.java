package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.RoleMapper;
import com.sinosoft.cloud.dal.dao.mapper.RoleMenuMapper;
import com.sinosoft.cloud.dal.dao.model.Role;
import com.sinosoft.cloud.dal.dao.model.RoleMenu;
import com.sinosoft.cloud.mgr.service.RoleService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.DB_FAIL;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;

/**
 * @Author: Caden
 * @Description:
 * @Date: on 2019/12/18.
 * @Modified By:
 */
@Service
public class RoleServiceImpl implements RoleService {
    private final Log logger = LogFactory.getLog(this.getClass());
     @Autowired
       private RoleMapper roleMapper;
     @Autowired
       private RoleMenuMapper roleMenuMapper;


    @Override
    public List<Role> getRoleList(Role role, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        String name = role.getName();
        return roleMapper.selectByNoParam(name);
    }


    @Override
    public int roleCount(Role role) {
        String name = role.getName();
        return roleMapper.SelectRoleCount(name);
    }
    @Override
    public Role selectRole(String menuId) {
        Integer i ;
        Role role =  new Role();
        if (menuId != null) {
            i = Integer.valueOf(menuId);
            role =   roleMapper.selectByPrimaryKey(menuId);
        }
        return role;
    }

    @Override
    public String adduRole(JSONObject po) {
        String result;
        int rolReId = roleMapper.SelectRoleId();
        String Id = po.getString("id");
        String type = po.getString("type");

        String enname = po.getString("enname");
        String name = po.getString("name");
        String field = po.getString("field");
        String useable = po.getString("useable");
        String selectedValues = po.getString("selectedValues");
        Role role = new Role();
        role.setName(name);
        role.setField(field);
        role.setEnname(enname);
        if("01".equals(useable) || useable =="01"){
            //打开
            byte c =1;
            role.setUseable("1");
        }else {
            //关闭
            byte b = 2;
            role.setUseable("2");
        }
        Date date = new Date();// 获取当前时间
        role.setDelFlag("0");
        if("add".equals (type)) {
            role.setCreateDate(date);
            role.setUpdateDate(date);
            role.setId(String.valueOf(rolReId));//(rolReId);
            // 添加
            int insert = roleMapper.insert(role);
            //当role添加成功添加中间表 ，进行判断，中间表存在删除掉 在进行添加
            int roleInsert = 0;
            if(insert>0 ){
                String menuid;
                String[] units = selectedValues.split(",");
                if(!"".equals(selectedValues) && selectedValues !=null){
                for(int i=1;i<units.length+1;i++){
                    menuid = selectedValues.split(",")[selectedValues.split(",").length - i];
//                    roleMenuMapper.deleteByPrimaryKey()
                    logger.info("循环"+menuid);
                    RoleMenu roleMenu =new RoleMenu();
                    roleMenu.setMenu_id(Integer.valueOf(menuid));
                    roleMenu.setRole_id(Integer.valueOf(rolReId));
                    roleInsert = roleMenuMapper.insert(roleMenu);
                    logger.info("添加中间表insert1: " + roleInsert);
                }
            }
            }
            if(!"".equals(selectedValues) && selectedValues !=null){
                if (insert>0 && roleInsert>0) {
                    result = SUCCESS.getCode();
                    logger.info("添加权限管理: " + SUCCESS.getMessage());
                } else {
                    result = DB_FAIL.getMessage();
                    logger.info("添加权限管理 : " + DB_FAIL.getMessage());
                }
            }else {
                if (insert>0) {
                    result = SUCCESS.getCode();
                    logger.info("添加权限管理: " + SUCCESS.getMessage());
                } else {
                    result = DB_FAIL.getMessage();
                    logger.info("添加权限管理 : " + DB_FAIL.getMessage());
                }
            }
        } else{
            // 修改
            role.setId(Id);
            role.setUpdateDate(date);
            // 添加

            int insert = roleMapper.updateByPrimaryKeySelective(role);
            //当role添加成功添加中间表 ，进行判断，中间表存在删除掉 在进行添加
            int roleInsert = 0;
            if(insert>0 ){
                String menuid;
                String[] units = selectedValues.split(",");
                if(!"".equals(selectedValues) && selectedValues !=null){
                    //删除中间表
                    roleMenuMapper.deleteByPrimaryKey(Integer.valueOf(Id));
                    for(int i=1;i<units.length+1;i++){
                    menuid = selectedValues.split(",")[selectedValues.split(",").length - i];
//                    roleMenuMapper.deleteByPrimaryKey()
                    logger.info("循环"+menuid);
                       RoleMenu  roleMenu =new RoleMenu();
                        roleMenu.setMenu_id(Integer.valueOf(menuid));
                        roleMenu.setRole_id(Integer.valueOf(Id));

                    roleInsert = roleMenuMapper.insert(roleMenu);
                    logger.info("添加中间表insert1: " + roleInsert);
                }
             }
            }
            if(!"".equals(selectedValues) && selectedValues !=null){
                if (insert>0 && roleInsert>0) {
                    result = SUCCESS.getCode();
                    logger.info("添加权限管理: " + SUCCESS.getMessage());
                } else {
                    result = DB_FAIL.getMessage();
                    logger.info("添加权限管理 : " + DB_FAIL.getMessage());
                }
            }else {
                if (insert>0) {
                    result = SUCCESS.getCode();
                    logger.info("添加权限管理: " + SUCCESS.getMessage());
                } else {
                    result = DB_FAIL.getMessage();
                    logger.info("添加权限管理 : " + DB_FAIL.getMessage());
                }
            }

            // result = mchInfoService.updateMchInfo(mchInfo);
        }
        return result;
    }










}
