package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;

import java.util.List;

public interface ElasticJobService {
    List<LdJobRunLog> getResultByTaskName(LdJobRunLog ldJobRunLog, Integer pageIndex, Integer pageSize);
    List<LDCode> getCodes();
    List<LDCode> getStates();
    Integer countForList(LdJobRunLog ldJobRunLog);

}
