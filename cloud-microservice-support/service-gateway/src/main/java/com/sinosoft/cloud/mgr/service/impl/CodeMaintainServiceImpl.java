package com.sinosoft.cloud.mgr.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LDCodeMapper;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.mgr.service.CodeMaintainService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;

@Service
public class CodeMaintainServiceImpl implements CodeMaintainService {
    private final static Log logger = LogFactory.getLog(CodeMaintainServiceImpl.class);
    @Autowired
    private LDCodeMapper ldCodeMapper;

    @Override
    public List<LDCode> getCodeTypeList() {
        return ldCodeMapper.getCodeTypeList();
    }

    @Override
    public List<LDCode> getCodeValueList() {
        return ldCodeMapper.getCodeValueList();
    }

    @Override
    public List<LDCode> getAllCodeValueInfo(String codeType) {
        return ldCodeMapper.getAllCodeValueInfo(codeType);
    }

    @Override
    public Integer countForCodeMaintainList(LDCode ldCode) {
        Map<String, Object> map = new HashMap<>();
        map.put("codeType", ldCode.getCodeType());
        map.put("code", ldCode.getCode());
        return ldCodeMapper.countForCodeMaintainList(map);
    }

    @Override
    public List<LDCode> getCodeMaintainList(LDCode ldCode, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        Map<String, Object> map = new HashMap<>();
        map.put("codeType", ldCode.getCodeType());
        map.put("code", ldCode.getCode());
        return ldCodeMapper.selectForCodeMaintainList(map);
    }

    @Override
    public String addCodeMaintain(JSONObject po) {
        String codeType = po.getString("codeType");
//        String codeTypeName = po.getString("codeTypeName");
        String codeValue = po.getString("codeValue");
        String codeValueName = po.getString("codeValueName");

        if (codeType == "" || codeValue == "" || codeValueName == "") {
            logger.error(PARAM_NOT_FOUND.getMessage());
            return PARAM_NOT_FOUND.getMessage();
        }

        int addbasecodetypeOk = 0;
        String result = "";
        //判断中台是否配置过基础代码解释; 若没有配置 ,先配置
        // 更改为后台在数据库直接配置
//        int flag = ldCodeMapper.verifybasecodetype(codeType);
//        if (flag == 0) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("code", codeType);
//            map.put("codeName", codeTypeName);
//            addbasecodetypeOk = ldCodeMapper.insertbasecodetype(map);
//            if (addbasecodetypeOk == 1) {
//                logger.info("basecodetypeSuccessfully: " + SUCCESS.getMessage());
//            } else {
//                result = DB_FAIL.getMessage();
//                logger.info("basecodetypeUnsuccessfully: " + DB_FAIL.getMessage());
//                return result;
//            }
//        }

        Map<String, Object> map = new HashMap<>();
        map.put("codeType", codeType);
        map.put("code", codeValue);
        //判断中台是否配置过标准代码类型;
        int flag = ldCodeMapper.verifycodetype(map);
        if (flag == 1) {
            result = CM_CODETYPE_EXIST.getMessage();
            logger.info("addCodeMaintain: " + CM_CODETYPE_EXIST.getMessage());
            return result;
        }

        //判断中台标准代码类型对应名称是否已存在;
        map.put("codeName", codeValueName);
        int typeNameFlag = ldCodeMapper.verifycodetypeName(map);
        if (typeNameFlag == 1) {
            result = CM_CODETYPENAME_EXIST.getMessage();
            logger.info("addCodeMaintain: " + CM_CODETYPENAME_EXIST.getMessage());
            return result;
        }

        logger.info("addCodeMaintain(map):" + map);
        int addFlag = ldCodeMapper.addCodeMaintain(map);
        if (addFlag == 1) {
            result = SUCCESS.getCode();
            logger.info("addCodeMaintain: " + SUCCESS.getMessage());
        } else {
            result = DB_FAIL.getMessage();
            logger.info("addCodeMaintain: " + DB_FAIL.getMessage());
        }
        return result;
    }

    @Override
    @Transactional
    public String deleteCodeMaintain(String codeTypes, String codeValues) {
        String result = "";
        String codeType = "";
        String codeValue = "";
        String[] strArrayU = codeTypes.split(",");
        String[] strArrayP = codeValues.split(",");
        for (int i = 0; i < strArrayU.length; i++) {
            codeType = strArrayU[i];
            for (int n = i; n < i + 1; n++) {
                codeValue = strArrayP[n];
                int flag = ldCodeMapper.deleteCodeMaintain(codeType, codeValue);
                if (flag == 1) {
                    result = SUCCESS.getCode();
                    logger.info("deleteCodeMaintain: " + SUCCESS.getMessage());
                } else {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    result = DB_FAIL.getMessage();
                    logger.info("deleteCodeMaintain: " + DB_FAIL.getMessage());
                    return result;
                }
            }
        }
        return result;
    }
}
