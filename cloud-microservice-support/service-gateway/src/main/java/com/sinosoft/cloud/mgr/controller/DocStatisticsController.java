package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LZCard;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.DocStatisticsService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/doc_mng")
public class DocStatisticsController {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private DocStatisticsService docStatisticsService;

    @RequestMapping("/docsatistics.html")
    public String docSatistics(ModelMap model) {
        List<LDCode> certifyCodes = docStatisticsService.getCodes();
//        List<LDCode> stateCode = docStatisticsService.getStates();
        model.addAttribute("certifyCodes", certifyCodes);
//        model.addAttribute("stateCode", stateCode);
        return "doc_mng/docstatistics";
    }

    @RequestMapping("/docstatistics")
    @ResponseBody
    public String list(@ModelAttribute LZCard lzCard, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        String certifyCode = lzCard.getCertifyCode();

//        if (idNo == null){
//            pageModel.setMsg("");
//            return JSON.toJSONString(pageModel);
//        }
        //分页计数
        int count = docStatisticsService.countForList(lzCard);
        if(count <= 0) {
            pageModel.setMsg("该信息没有录入, 请重新选择!");
            return JSON.toJSONString(pageModel);
        }

        List<LZCard> result =  docStatisticsService.getResultByTaskName(lzCard,pageIndex,pageSize);
//        if (!CollectionUtils.isEmpty(result)) {
//            JSONArray array = new JSONArray();
//            for (LDCode ldCode1 : result) {
//                if (ldCode1 != null) {}
//                JSONObject object = (JSONObject) JSONObject.toJSON(ldCode);
//                array.add(object);
//            }
        pageModel.setList(result);
//            logger.info("结果返回："+result.toString());
//            pageModel.setList(array);
//        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

}
