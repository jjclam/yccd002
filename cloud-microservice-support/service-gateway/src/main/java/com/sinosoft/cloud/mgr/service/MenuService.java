package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.Menu;
import com.sinosoft.cloud.dal.dao.model.Role;
import com.sinosoft.cloud.dal.dao.model.RoleExample;

import java.util.List;

/**
 * @Author: Caden
 * @Description:
 * @Date: on 2019/12/16.
 * @Modified By:
 */
public interface MenuService {
    List<Menu> getMenu();
     int count(Menu menu);
    List<Menu> getMenuList(Menu mchInfo, Integer pageIndex, Integer pageSize);
    List<Role> getrole(RoleExample example);
    List<Menu>  parentMenu();
    Menu selectMenu(String menuId);
    int addMchInfo(Menu menu);
    int updateMchInfo(Menu menu);

    List<Menu> getparentMenus(String field);

    Menu getparentMenu(String mchId);
}
