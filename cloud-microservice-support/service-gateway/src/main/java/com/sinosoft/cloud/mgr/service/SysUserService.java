package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.common.util.RpcSignUtils;
import com.sinosoft.cloud.dal.dao.mapper.MenuMapper;
import com.sinosoft.cloud.dal.dao.mapper.SysUserMapper;
import com.sinosoft.cloud.dal.dao.mapper.UserMgrMapper;
import com.sinosoft.cloud.dal.dao.model.Menu;
import com.sinosoft.cloud.dal.dao.model.UserMgr;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;
import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;

/**
 * @author yang@dehong
 * 2018-07-09 0:02
 */
@Service
@Component
public class SysUserService {
    private final Log logger = LogFactory.getLog(this.getClass());
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private UserMgrMapper userMgrMapper;
    @Autowired
    private MenuMapper menuMapper;




    public List<Menu> parentMenu(String _ueer){
      //  Integer  id =1;
        return menuMapper.getParentMenu(_ueer);
    }
    public List<Menu> getMenu(String id, String parent_id ){
     //   Integer  id =1;

//        Menu  menu =new Menu();
//        menu.setParent_id(parent_id);
        return menuMapper.getById(id,parent_id);
    }



    public UserMgr checkUser(String name, String password) {
        UserMgr SysUser = new UserMgr();
        password = RpcSignUtils.sha1(password);
//        password = SecurityUtils.encryptPassword(password);
        SysUser.setUserName(name);
        SysUser.setPassword(password);
        SysUser = userMgrMapper.CheckUser(name,password);

        return SysUser;
    }

    public String createUser(String userName, String passWord) {
        String msg = "";
        passWord = RpcSignUtils.sha1(passWord);
//        passWord = SecurityUtils.encryptPassword(passWord);
        int flag = sysUserMapper.verifyUser(userName);
        if (flag == 1) {
            logger.error("---------注册用户已存在---------");
            msg = DB_USER_EXIST.getMessage();
            return msg;
        }
        logger.info("---------注册用户---------");
        Map<String, Object> map = new HashMap<>();
        map.put("userName", userName);
        map.put("passWord", passWord);
        String createDate = getCurrentDefaultTimeStr();
        map.put("createDate", createDate);
        map.put("operator", userName);
        int result = sysUserMapper.createUser(map);
        if (result == 1) {
            msg = SUCCESS.getCode();
        } else {
            msg = DB_FAIL.getCode();
        }
        return msg;
    }
}
