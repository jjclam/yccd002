package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.yfbDao.model.LDGrp;

import java.util.List;

public interface LcgrpContService {
    int deleteByPrimaryKey(String customerno);

    int insert(LDGrp record);

    int insertSelective(LDGrp record);

    LDGrp selectByPrimaryKey(String customerno);

    int updateByPrimaryKeySelective(LDGrp record);

    int updateByPrimaryKey(LDGrp record);

    List<LDGrp> getAllInstitutionsName();
}
