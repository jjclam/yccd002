package com.sinosoft.cloud.mgr.service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;

import java.util.List;

public interface CodeMaintainService {
    List<LDCode> getCodeTypeList();

    List<LDCode> getCodeValueList();

    List<LDCode> getAllCodeValueInfo(String codeType);

    Integer countForCodeMaintainList(LDCode ldCode);

    List<LDCode> getCodeMaintainList(LDCode ldCode, Integer pageIndex, Integer pageSize);

    String addCodeMaintain(JSONObject po);

    String deleteCodeMaintain(String codeTypes, String codeValues);
}
