package com.sinosoft.cloud.mgr.service;

import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.entity.LZCard;
import com.sinosoft.cloud.dal.dao.model.entity.LdJobRunLog;

import java.util.List;

public interface DocStatisticsService {
    List<LZCard> getResultByTaskName(LZCard lzCard, Integer pageIndex, Integer pageSize);
    List<LDCode> getCodes();
    List<LZCard> getStates();
    Integer countForList(LZCard lzCard);

}
