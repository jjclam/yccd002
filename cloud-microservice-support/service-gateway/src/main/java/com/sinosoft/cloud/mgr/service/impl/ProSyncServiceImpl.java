package com.sinosoft.cloud.mgr.service.impl;


import com.github.pagehelper.PageHelper;
import com.sinosoft.cloud.dal.dao.mapper.LAChannelProductMapper;
import com.sinosoft.cloud.dal.dao.mapper.LMRiskMapper;
import com.sinosoft.cloud.dal.dao.model.LAChannelProduct;
import com.sinosoft.cloud.dal.dao.model.entity.ProInfo;
import com.sinosoft.cloud.mgr.service.ProSyncService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.Chl_PRO_UPSTATUS;
import static com.sinosoft.cloud.common.enumm.ResultMegEnum.SUCCESS;
import static com.sinosoft.cloud.common.util.RegUtil.regMatch;

@Service
public class ProSyncServiceImpl implements ProSyncService {
    private final static Log logger = LogFactory.getLog(ProSyncServiceImpl.class);
    @Autowired
    private LMRiskMapper tLMRiskMapper;
    @Autowired
    private LAChannelProductMapper laChannelProductMapper;

    @Override
    public Integer count(ProInfo proInfo) {
//        return tLMRiskMapper.countForProSync(proInfo);
        String proCode = proInfo.getProCode();
        String proType = "";
        if (!proCode.equals("")) {
            if (regMatch(proCode)) {
                return tLMRiskMapper.countByHEXIN(proCode, proType);
            } else {
                return tLMRiskMapper.countByTAOCAN(proCode, proType);
            }
        } else {
            return tLMRiskMapper.countByExample();
        }
    }

    @Override
    public List<ProInfo> getproSyncList(ProInfo proInfo, Integer pageIndex, Integer pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        //区分核心与套餐
        String proCode = proInfo.getProCode();
        String proType = proInfo.getStatus();
        if (!proCode.equals("")) {
            if (regMatch(proCode)) {
                return tLMRiskMapper.selectForHEXINex(proCode, proType);
            } else {
                return tLMRiskMapper.selectForTAOCANex(proCode, proType);
            }
        } else {
            return tLMRiskMapper.selectByNoParamex();
        }
    }

    @Override
    public String upPro(String proCode) {
        if (regMatch(proCode)) {
            tLMRiskMapper.upProForHEXIN(proCode);
        } else {
            tLMRiskMapper.upProForTAOCAN(proCode);
        }
        return SUCCESS.getCode();
    }

    @Override
    public String upPro(String proCode, String str) {
        String[] split = str.split(";");
        String remark = split[0];
        String remark2 = split[1];
        String planKind1 = split[2];
        String planKind2 = split[3];
        String planKind3 = split[4];
        Map<String, Object> map = new HashMap<>();
        map.put("remark", remark);
        map.put("remark2", remark2);
        map.put("planKind1", planKind1);
        map.put("planKind2", planKind2);
        map.put("planKind3", planKind3);
        map.put("proCode", proCode);
        tLMRiskMapper.upProForTAOCAN2(map);

        return SUCCESS.getCode();
    }

    @Override
    public String downPro(String proCode) {
        //渠道上架的产品,不能在核心直接下架
        List<LAChannelProduct> upStatuses = laChannelProductMapper.verifyChlProUpStatus(proCode);
        for (int i = 0; i < upStatuses.size(); i++) {
            String status = upStatuses.get(i).getStatus();
            if (status.equals("0")) {
                logger.error(Chl_PRO_UPSTATUS.getMessage());
                return Chl_PRO_UPSTATUS.getMessage();
            }
        }
        if (regMatch(proCode)) {
            tLMRiskMapper.downProForHEXIN(proCode);
        } else {
            tLMRiskMapper.downProForTAOCAN(proCode);
        }
        return SUCCESS.getCode();
    }

}
