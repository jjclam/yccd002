package com.sinosoft.cloud.mgr.service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LDCode;
import com.sinosoft.cloud.dal.dao.model.SmsChlPro;

import java.util.List;

public interface SmsParamsService {
    Integer countForSmsParamsList(SmsChlPro smsChlPro);

    List<SmsChlPro> getSmsParamsList(SmsChlPro smsChlPro, Integer pageIndex, Integer pageSize);

    String addSmsMaintain(JSONObject po);

    String updateSmsMaintain(JSONObject po);

    String deleteSmsMaintain(List<SmsChlPro> deleteArray);

    SmsChlPro getSmsEditInfo(String channelCode, String proCode);

    List<LDCode> getSmsParamsLinkded(String codeType);
}
