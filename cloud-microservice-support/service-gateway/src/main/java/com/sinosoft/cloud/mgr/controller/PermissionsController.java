package com.sinosoft.cloud.mgr.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.Menu;
import com.sinosoft.cloud.dal.dao.model.Role;
import com.sinosoft.cloud.dal.dao.model.RoleExample;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.mgr.service.MenuService;
import com.sinosoft.cloud.mgr.service.RoleService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Caden
 * @Description:
 * @Date: on 2019/12/16.
 * @Modified By:
 */
@Controller
@RequestMapping("/permissions")
public class PermissionsController {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private MenuService menuService;
    @Autowired
    private RoleService roleService;
    @RequestMapping("/list.html")
    public String listInput(ModelMap model) {

        return "permissions/list";
    }
    @RequestMapping("/edit.html")
    public String editInput(String mchId, ModelMap model,RoleExample example) {
        List<Menu> menu = menuService.getMenu();
        Role item = null;
        if("undefined".equals(mchId)||  mchId == "undefined" ){
            mchId  ="";
        }
        if(StringUtils.isNotBlank(mchId)) {
            item = roleService.selectRole(mchId);
        }
        model.put("item", item);
        model.addAttribute("menu", menu);
        return"permissions/allup";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute Role role, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        int count = roleService.roleCount(role);
        if(count <= 0) return JSON.toJSONString(pageModel);
        if (role.getName() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        List<Role> roleList = roleService.getRoleList(role,pageIndex, pageSize);
        if(!CollectionUtils.isEmpty(roleList)) {
            JSONArray array = new JSONArray();
            for(Role mi : roleList) {
                JSONObject object = (JSONObject) JSONObject.toJSON(mi);
              //  object.put("createTime", DateUtil.date2Str(mi.getCreateTime()));
                array.add(object);
            }
            pageModel.setList(array);
        }
         pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }


    @RequestMapping("/alldown.html")
    public String alldownInput(ModelMap model) {
        List<Menu> menu = menuService.getMenu();
        model.addAttribute("menu", menu);
        return "permissions/allup11";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public  Map<String, Object> save(@RequestParam String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
       String result;
        result = roleService.adduRole(po);
        map.put("msg", result);
        return map;
    }

    @RequestMapping("/view.html")
    public String viewInput(String mchId, ModelMap model) {
//        MchInfo item = null;
//        if(StringUtils.isNotBlank(mchId)) {
//            item = mchInfoService.selectMchInfo(mchId);
//        }
//        if(item == null) item = new MchInfo();
//        model.put("item", item);
        return "permissions/view";
    }



}
