package com.sinosoft.cloud.yfb.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.cloud.dal.dao.model.LACom;
import com.sinosoft.cloud.dal.dao.plugin.PageModel;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrp;
import com.sinosoft.cloud.dal.yfbDao.model.LDGrpToPlan;
import com.sinosoft.cloud.dal.yfbDao.model.LDSchemeRisk;
import com.sinosoft.cloud.yfb.service.SchemeAuthorizeService;
import com.sinosoft.cloud.yfb.service.SchemeInfoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sinosoft.cloud.common.enumm.ResultMegEnum.*;

/**
 * 方案授权
 *
 * @author J
 * @version 1.0
 */

@Controller
@RequestMapping("/scheme_authorize")
public class SchemeAuthorizeController {
    private final static Log logger = LogFactory.getLog(SchemeAuthorizeController.class);
    @Autowired
    private SchemeAuthorizeService schemeAuthorizeService;
    @Autowired
    private SchemeInfoService schemeInfoService;

    @RequestMapping("/list.html")
    public String list(ModelMap model) {
        return "yfb/scheme/authorize/list";
    }

    @RequestMapping("/view.html")
    public String view(LDGrp customerno, ModelMap model) {
        model.put("item", customerno);
        return "yfb/scheme/authorize/view";
    }

    @RequestMapping("/view1.html")
    public String bacthview( String customerno, ModelMap model) {
       model.addAttribute ("customerno",customerno);
        return "yfb/scheme/authorize/view1";
    }

    @RequestMapping("/list")
    @ResponseBody
    public String list(@ModelAttribute LACom laCom, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        //首次页面不查找数据
//        if (laCom.getNAME() == null) {
//            pageModel.setMsg("");
//            return JSON.toJSONString(pageModel);
//        }
        int count = schemeAuthorizeService.countSchemeAuthorize(laCom);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<LACom> result = schemeAuthorizeService.getSchemeAuthorizeList(laCom, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LACom pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
//        pageModel.setList(result);
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping("/schemeList")
    @ResponseBody
    public String schemeList(@ModelAttribute LDSchemeRisk ldschemerisk, Integer pageIndex, Integer pageSize) {
        PageModel pageModel = new PageModel();
        //首次页面不查找数据
        if (ldschemerisk.getContschemecode() == null) {
            pageModel.setMsg("");
            return JSON.toJSONString(pageModel);
        }
        int count = schemeAuthorizeService.countAllSchemeAuthorize(ldschemerisk);
        if(count <= 0) {
            pageModel.setMsg(DB_NOT_FOUND.getMessage());
            return JSON.toJSONString(pageModel);
        }
        List<LDSchemeRisk> result = schemeAuthorizeService.getAllSchemeAuthorizeList(ldschemerisk, pageIndex, pageSize);
        if (!CollectionUtils.isEmpty(result)) {
            JSONArray array = new JSONArray();
            for (LDSchemeRisk pc : result) {
                JSONObject object = (JSONObject) JSONObject.toJSON(pc);
                array.add(object);
            }
            pageModel.setList(array);
        }
        pageModel.setCount(count);
        pageModel.setMsg("ok");
        pageModel.setRel(true);
        return JSON.toJSONString(pageModel);
    }

    @RequestMapping(value = "/save")
    @ResponseBody
    public Map<String, Object> save(@RequestBody List<LDGrpToPlan> saveArray, HttpSession session) {
        Map<String, Object> map = new HashMap<>();
        String result = "";
        String operator = (String) session.getAttribute("user");
        if (CollectionUtils.isEmpty(saveArray)) {
            logger.error("authorizeSchemes: " + PARAM_NOT_FOUND.getMessage());
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        } else {
            result = schemeAuthorizeService.authorizeSchemes(saveArray, operator);
            map.put("msg", result);
        }
        return map;
    }

    @RequestMapping(value = "/bacthsave")
    @ResponseBody
    public Map<String, Object> bacthsave(@RequestBody List<LDGrpToPlan> saveArray, HttpSession session) {
        Map<String, Object> map = new HashMap<>();
        String result = "";
        String operator = (String) session.getAttribute("user");
        if (CollectionUtils.isEmpty(saveArray)) {
            logger.error("authorizeSchemes: " + PARAM_NOT_FOUND.getMessage());
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        } else {
           String customers = saveArray.get (0).getCustomerNo ();
           String[] customerArrays = customers.split (",");
            for (String customerno:
                    customerArrays ) {
                for (LDGrpToPlan ldGrpToPlan:
                saveArray) {
                    ldGrpToPlan.setCustomerNo (customerno);
                }
                result = schemeAuthorizeService.authorizeSchemes(saveArray, operator);
            }

            map.put("msg", result);
        }
        return map;
    }

    @RequestMapping(value = "/haven")
    @ResponseBody
    public Map<String, Object> havenAuthorize(@RequestParam(value = "customerno") String customerno) {
        Map<String, Object> map = new HashMap<>();
        List<LDGrpToPlan> list = schemeAuthorizeService.gethavenAuthorize(customerno);
        logger.info(list);
        //如果没有已授权的方案
        if (list.size() == 0) {
            map.put("msg", CM_SMSPARAM_NOT_EXIST.getMessage());
        }
        map.put("havenAuthorize", list);
        return map;
    }

    @RequestMapping(value = "/delete")
    @ResponseBody
    public Map<String, Object> deleteAuthorizedScheme(@RequestParam(value = "params") String params) {
        Map<String, Object> map = new HashMap<>();
        JSONObject po = JSONObject.parseObject(params);
        String customerno = po.getString("customerno");
        String plancode = po.getString("plancode");
        if (StringUtils.isBlank(customerno) || StringUtils.isBlank(plancode) ) {
            logger.error(PARAM_NOT_FOUND.getMessage() + "without" + plancode + customerno);
            map.put("msg", PARAM_NOT_FOUND.getMessage());
            return map;
        }
        String result = "";
        try {
            result = schemeAuthorizeService.deleteAuthorizedScheme(customerno, plancode);
            map.put("msg", result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("系统异常");
            map.put("msg", Chl_UNKNOWN_ERROR.getMessage());
            return map;
        }
        return map;
    }



}
