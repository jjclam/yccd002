import com.sinosoft.cloud.dal.dao.mapper.LAComMapper;
import com.sinosoft.cloud.dal.dao.mapper.open_LaComMapper;
import com.sinosoft.cloud.dal.dao.model.entity.AgentInfo;
import com.sinosoft.cloud.dal.dao.model.open_LaCom;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

import static com.sinosoft.cloud.common.util.DateUtils.getCurrentDefaultTimeStr;
import static com.sinosoft.cloud.mgr.common.XMLWriteRead.cancelPolicyByXML;

public class DBtest {

    @Autowired
    private  open_LaComMapper open_laComMapper;
    @Autowired
    private LAComMapper laComMapper;

    @Test
    public void open_LaComTest() {

        AgentInfo s = laComMapper.selectAgentInfoByAgentCode("102");
        System.out.println(s);


        open_LaCom map = open_laComMapper.selectByaa();
        System.out.println(map);
    }

    @Test
    public void timeSync() {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    System.out.println("start");
                    try {
                        Thread.sleep(9);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("end");
                }

            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }

    @Test
    public void timeSort() {
        Map<String, Object> map = new HashMap<>();
        map.put("ok", "ok");

        String start = getCurrentDefaultTimeStr();
        String result = cancelPolicyByXML(map);
        String end = getCurrentDefaultTimeStr();
        System.out.println(result + ": " + end + start);


    }
}
