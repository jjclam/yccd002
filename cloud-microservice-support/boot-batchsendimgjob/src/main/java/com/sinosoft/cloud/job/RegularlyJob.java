package com.sinosoft.cloud.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.sinosoft.cloud.BatchSendImageUploadTask;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LdJobRunLogSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
     * Created by  on 2018/8/24.
     */

/**
 * Created by  on 2018/9/13.
 */
@Service
public class RegularlyJob implements SimpleJob {
    @Resource
    BatchSendImageUploadTask batchSendImageUploadTask;
     private Logger logger = LoggerFactory.getLogger(RegularlyJob.class);
    private static String SUCCESS = "success";
    private static String FAIL = "fail";
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String tCurrentDate = PubFun.getCurrentDate();
    private String tCurrentTime = PubFun.getCurrentTime();
    @Override
    public synchronized void execute(ShardingContext shardingContext) {
        jobBeginLog(shardingContext);//启动日志

        //业务处理
        if(batchSendImageUploadTask.dealData()) {
            tCurrentDate = PubFun.getCurrentDate();
            tCurrentTime = PubFun.getCurrentTime();
            backlog(SUCCESS);
        }else {
            tCurrentDate = PubFun.getCurrentDate();
            tCurrentTime = PubFun.getCurrentTime();
            backlog(FAIL);
            return;
        }

         jobEndLog(shardingContext);//结束日志
    }

    private void backlog(String tResult) {
        //记录任务调度日志表 LDJOBRUNLOG
        MMap tMMap = new MMap();
        VData tVData = new VData();
        String serialno = PubFun.CreateSeq("seq_batchno_20191116",10);
        ExeSQL mExeSQL = new  ExeSQL();
        String codeSql = "select code from ldcode where codetype ='jobtype' and codealias = 'BatchSendImageUploadTask'";
        String taskCode = mExeSQL.getOneValue(codeSql);

        LdJobRunLogSchema tLdJobRunLogSchema = new LdJobRunLogSchema();
        tLdJobRunLogSchema.setSerialNo(serialno);
        tLdJobRunLogSchema.setTaskCode(taskCode);
        tLdJobRunLogSchema.setTaskName("BatchSendImageUploadTask");
        tLdJobRunLogSchema.setExecuteDate(mCurrentDate);
        tLdJobRunLogSchema.setExecuteTime(mCurrentTime);
        tLdJobRunLogSchema.setFinishDate(tCurrentDate);
        tLdJobRunLogSchema.setFinishTime(tCurrentTime);
        if ("success".equals(tResult)){
            tLdJobRunLogSchema.setExecuteState("1");
        }else if("fail".equals(tResult)){
            tLdJobRunLogSchema.setExecuteState("0");
        }
        tLdJobRunLogSchema.setExecuteResult("定时影像上传批处理执行" + tResult +";具体信息请参见LCImageUploadInfo");
        tMMap.put(tLdJobRunLogSchema,"DELETE&INSERT");

        tVData.add(tMMap);

        //数据落库
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(tVData);
    }

    /**
     * job 分片启动日志
     * @param shardingContext
     */
    public void jobBeginLog(ShardingContext shardingContext){
        logger.info(String.format("影像上传断点回传批处理任务开始：Thread ID: %s, 任务总片数: %s, " +
                        "当前分片项: %s.当前参数: %s,"+
                        "当前任务名称: %s.当前任务参数: %s",Thread.currentThread().getId(),
                shardingContext.getShardingTotalCount(),
                shardingContext.getShardingItem(),
                shardingContext.getShardingParameter(),
                shardingContext.getJobName(),
                shardingContext.getJobParameter()));
    }

    /**
     * job 分片结束日志
     * @param shardingContext
     */
    public void jobEndLog(ShardingContext shardingContext){
        logger.info(String.format("影像上传断点回传批处理任务结束：Thread ID: %s, 任务总片数: %s, " +
                        "当前分片项: %s.当前参数: %s,"+
                        "当前任务名称: %s.当前任务参数: %s",Thread.currentThread().getId(),
                shardingContext.getShardingTotalCount(),
                shardingContext.getShardingItem(),
                shardingContext.getShardingParameter(),
                shardingContext.getJobName(),
                shardingContext.getJobParameter()));
    }
}

