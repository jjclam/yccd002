package com.sinosoft.cloud;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author luoliang
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan({"com.sinosoft.cloud"})
@EnableFeignClients("com.sinosoft.cloud")
@Configuration
public class BatchSendimgJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatchSendimgJobApplication.class, args);
    }



}
