package com.sinosoft.cloud.util;

import com.dangdang.ddframe.job.api.ShardingContext;
import org.slf4j.Logger;

/**
 *
 */
public class LogUtil {
    /**
     * job 分片启动日志
     * @param shardingContext
     */
    public static void jobBeginLog(Logger logger,ShardingContext shardingContext){
        logger.info(String.format("任务开始：当前任务名称: %s.Thread ID: %s, 任务总片数: %s, " +
                        "当前分片项: %s.当前参数: %s,"+
                        "当前任务参数: %s",
                shardingContext.getJobName(),
                Thread.currentThread().getId(),
                shardingContext.getShardingTotalCount(),
                shardingContext.getShardingItem(),
                shardingContext.getShardingParameter(),
                shardingContext.getJobParameter()));
    }


    /**
     * job 分片结束日志
     * @param shardingContext
     */
    public static void jobEndLog(Logger logger,ShardingContext shardingContext){
        logger.info(String.format("任务结束：当前任务名称: %s.Thread ID: %s, 任务总片数: %s, " +
                        "当前分片项: %s.当前参数: %s,"+
                        "当前任务参数: %s",
                shardingContext.getJobName(),
                Thread.currentThread().getId(),
                shardingContext.getShardingTotalCount(),
                shardingContext.getShardingItem(),
                shardingContext.getShardingParameter(),
                shardingContext.getJobParameter()));
    }
}
