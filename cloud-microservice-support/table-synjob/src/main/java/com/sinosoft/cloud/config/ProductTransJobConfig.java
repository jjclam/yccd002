package com.sinosoft.cloud.config;

import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.event.JobEventConfiguration;
import com.dangdang.ddframe.job.event.rdb.JobEventRdbConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.lite.spring.api.SpringJobScheduler;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.sinosoft.cloud.job.ProductTransJob;
import com.sinosoft.cloud.listener.SynJobListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * job配置管理类
 **/
@Configuration
public class ProductTransJobConfig {
    /*-----------------------------------公共配置开始------------------------------------*/
    @Autowired
    private ZookeeperRegistryCenter regCenter;

    @Autowired
    //@Resource
    private DataSource dataSource;
    /*-----------------------------------公共配置结束------------------------------------*/
    /*-----------------------------------job注入开始------------------------------------*/

//    @Resource
//
//    @Autowired
    @Resource
    ProductTransJob productTransJob;

    /*-----------------------------------job注入结束------------------------------------*/




    @Bean(initMethod = "init")
    public JobScheduler simpleJobScheduler(@Value("${ProductTransJob.cron}") final String cron, @Value("${ProductTransJob.shardingTotalCount}") final int shardingTotalCount, @Value("${ProductTransJob.shardingItemParameters}") final String shardingItemParameters) {
        return new SpringJobScheduler(productTransJob, regCenter, getLiteJobConfiguration(productTransJob.getClass(), cron, shardingTotalCount, shardingItemParameters), jobEventConfiguration());
    }


    /**
     * 将作业运行的痕迹(日志)进行持久化到DB
     *
     * @return
     */
    @Bean
    public JobEventConfiguration jobEventConfiguration() {
        return new JobEventRdbConfiguration(dataSource);
    }

    @Bean
    public SynJobListener synJobListener() {
        return new SynJobListener(100, 100);
    }


    /**
     * 必要的简单配置
     *
     * @Description 任务（job）配置类,job初始化时使用，获取 LiteJobConfiguration 对象
     */
    private LiteJobConfiguration getLiteJobConfiguration(final Class<? extends SimpleJob> jobClass,
                                                         final String cron,
                                                         final int shardingTotalCount,
                                                         final String shardingItemParameters) {


        return LiteJobConfiguration
                .newBuilder(
                        new SimpleJobConfiguration(
                                JobCoreConfiguration.newBuilder(
                                        jobClass.getName(), cron, shardingTotalCount)
                                        .shardingItemParameters(shardingItemParameters)
                                        .build()
                                , jobClass.getCanonicalName()
                        )
                )
                .overwrite(true)
                .build();

    }


    /**
     * 必要的简单配置
     *
     * @Description 任务（job）配置类,job初始化时使用，获取 LiteJobConfiguration 对象
     */
    private LiteJobConfiguration getLiteJobConfiguration1(final Class<? extends SimpleJob> jobClass,
                                                         final String cron,
                                                         final int shardingTotalCount,
                                                         final String shardingItemParameters) {


        return LiteJobConfiguration
                .newBuilder(
                        new SimpleJobConfiguration(
                                JobCoreConfiguration.newBuilder(
                                        jobClass.getName(), cron, shardingTotalCount)
                                        .shardingItemParameters(shardingItemParameters)
                                        .build()
                                , jobClass.getCanonicalName()
                        )
                )
                .overwrite(true)
                .build();

    }
}
