package com.sinosoft.cloud.job;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.*;
import org.apache.ibatis.jdbc.SQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@Component("TableSynTask")
public class TableSynTask {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private MMap tMMap = new MMap();
    private MMap tMMap2 = new MMap();
    private MMap tMMap3 = new MMap();
    private MMap tMMap4 = new MMap();
    private MMap tMMap5 = new MMap();
    private VData tVData = new VData();
    private VData tVData2 = new VData();
    private VData tVData3 = new VData();
    private VData tVData4 = new VData();
    private VData tVData5 = new VData();
    private Connection mid_connection = null;
    private Connection core_connection = null;
    private ArrayList<String> mSQLArray = new ArrayList();
    private ArrayList<String> mSQLCondition = new ArrayList();
    private boolean isSYN = false;
    private String contPlanCode = "";
    private String riskCode = "";
    private boolean mflag = false;//是否需要回滚
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    private TransferData mTransferData;
    public CErrors mErrors = new CErrors();
    private String synType = "1";  //同步产品表的方式：1、全量同步（默认）；2、按照产品编码部分同步；3、按照险种编码部分同步；

    public boolean submitData(VData tVData) {
        try {
            if (!getInputData(tVData)) {
                logger.error("查询同步表数据失败！");
                return false;
            }
            if (!checkData()){
                logger.error("校验数据失败！");
                return false;
            }
            if(!dealData()){
                logger.error("同步表数据失败！");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }finally {
            try{
                if(mid_connection!=null){
                    mid_connection.close();
                }
                if(core_connection!=null){
                    core_connection.close();
                }
            }catch (SQLException e){
                logger.error(e.toString());
            }
        }
        return true;
    }



    private boolean checkData() {
        //校验传的数值是否有效
        ExeSQL tExeSQL = new ExeSQL(core_connection);
        SSRS ssrs = new SSRS();
        try {
            if(contPlanCode!=null && !"".equals(contPlanCode)){
                ssrs = tExeSQL.execSQL("select 1 from ldplan where contplancode = '" + contPlanCode + "'");
                if(ssrs!=null && ssrs.getMaxRow()==0){
                    buildError("checkData","核心没有查到对应的产品编码");
                    return false;
                }
                synType = "2";//同步产品表的方式 按照产品编码同步
            }else if(riskCode !=null && !"".equals(riskCode)){
                ssrs = tExeSQL.execSQL("select 1 from lmrisk where riskcode = '" + riskCode + "'");
                if(ssrs!=null && ssrs.getMaxRow()==0){
                    buildError("checkData","核心没有查到对应的险种编码");
                    return false;
                }
                synType = "3";//同步产品表的方式 按照险种编码同步
            }
        }catch(Exception e){
            logger.error("校验失败"+e.toString());
        }
        return true;
    }

    /**
     * 得到需要同步的表数据
     */
    private boolean getInputData(VData tVData) {
        mid_connection = DBConnPool.getConnection("dataSource");//中台
        core_connection = DBConnPool.getConnection("lbasedataSource");//核心

        mTransferData = (TransferData)tVData.getObjectByObjectName("TransferData", 0);
        contPlanCode = (String)mTransferData.getValueByName("contPlanCode");
        riskCode = (String)mTransferData.getValueByName("riskCode");

        return true;
    }

    /**
     * 处理同步表
     * @return
     */
    private synchronized boolean dealData() {

        try{
            //1)查询需要操作的产品表
            if (!deleteCoreData()) {
                logger.error("查找同步表数据失败！");
                return false;
            }
            if (!isSYN){
                logger.info("无需同步数据~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                return true;
            }

            //2）查询核心的产品表数据拼装sql
            if (!searchCoreProducts()){
                logger.error("查询核心表数据失败！");
                return false;
            }
            logger.info("删除产品表开始"+PubFun.getCurrentDate()+PubFun.getCurrentTime());
            //3）删除中台产品表数据
            PubSubmit tPubSubmit = new PubSubmit(mid_connection);
            if (!tPubSubmit.submitData(tVData)) {
                logger.error("删除中台产品表数据失败！");
                mflag = true;
                buildError("Pubsubmit","删除语句"+tPubSubmit.mErrors.getFirstError());
                return false;
            }
            logger.info("删除产品表结束"+PubFun.getCurrentDate()+PubFun.getCurrentTime());

            //4）同步数据
            logger.info("插入产品表数据开始"+PubFun.getCurrentDate()+PubFun.getCurrentTime());
            if (!tPubSubmit.submitData(tVData2)) {
                logger.error("插入中台产品表数据失败！");
                mflag = true;
                buildError("Pubsubmit","添加语句"+tPubSubmit.mErrors.getFirstError());
                return false;
            }
            logger.info("插入产品表数据结束"+PubFun.getCurrentDate()+PubFun.getCurrentTime());
            //5）更新产品表数据(当中台的产品表字段比核心多的时候才会执行以下内容)
            if(tVData3!=null && tVData3.size()>0){
                logger.info("更新产品表数据开始"+PubFun.getCurrentDate()+PubFun.getCurrentTime());
                if (!tPubSubmit.submitData(tVData3)) {
                    logger.error("插入中台产品表数据失败！");
                    mflag = true;
                    buildError("Pubsubmit","添加语句"+tPubSubmit.mErrors.getFirstError());
                    return false;
                }
                logger.info("更新产品表数据结束"+PubFun.getCurrentDate()+PubFun.getCurrentTime());
            }

        }catch (Exception e){
            logger.error("失败了~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }finally {
            try {
                if(mid_connection!=null){
                    if(mflag){
                        mid_connection.rollback();//当中间发生错误，数据进回滚
                    }
                    mid_connection.close();
                }
                if(core_connection!=null){
                    core_connection.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return true;
    }



    private boolean deleteCoreData() {
        tMMap = new MMap();
        tMMap4 = new MMap();
        tMMap5 = new MMap();
        tVData4 = new VData();
        tVData5 = new VData();
        ExeSQL exeSQL = new ExeSQL(mid_connection);
        SSRS ssrs = exeSQL.execSQL("select codename,othersign from ldcode where codetype = 'tablesyn" + synType + "'");//需要同步的表，以及条件
        //通过产品编码同步需要知道 all riskcode,all dutycode,all getdutycode,all payplancode
        //通过职工编码同步需要知道 all dutycode,all getdutycode,all payplancode
        exeSQL = new ExeSQL(core_connection);//用核心的连接
        SSRS riskCodes = new SSRS();
        SSRS dutyCodes = new SSRS();
        SSRS getDutyCodes = new SSRS();
        SSRS payPlanCodes = new SSRS();
        String risks = "";
        String dutys = "";
        String gets = "";
        String pays = "";
        try {
            if ("2".equals(synType)) {
                riskCodes = exeSQL.execSQL(" select distinct ld.riskcode from ldplanrisk ld where ld.contplancode = '" + contPlanCode + "'");
                dutyCodes = exeSQL.execSQL(" select distinct lm.dutycode from lmriskduty lm where lm.riskcode in (select ld.riskcode from ldplanrisk ld where ld.contplancode = '" + contPlanCode + "') ");
                getDutyCodes = exeSQL.execSQL(" select distinct lmd.getdutycode from LMDutyGetRela lmd where lmd.dutycode in (select distinct lm.dutycode from lmriskduty lm where lm.riskcode in (select ld.riskcode from ldplanrisk ld where ld.contplancode = '" + contPlanCode + "'))");
                payPlanCodes = exeSQL.execSQL(" select distinct lmd.payplancode from LMDutyPayRela lmd where dutycode in (select distinct lm.dutycode from lmriskduty lm where lm.riskcode in (select ld.riskcode from ldplanrisk ld where ld.contplancode = '" + contPlanCode + "'))");
            } else if ("3".equals(synType)) {
                dutyCodes = exeSQL.execSQL(" select distinct lm.dutycode from lmriskduty lm where lm.riskcode = '" + riskCode + "'");
                getDutyCodes = exeSQL.execSQL(" select lmd.getdutycode from LMDutyGetRela lmd where lmd.dutycode in (select distinct lm.dutycode from lmriskduty lm where lm.riskcode = '" + riskCode + "' )");
                payPlanCodes = exeSQL.execSQL(" select distinct lmd.payplancode from LMDutyPayRela lmd where lmd.dutycode in (select distinct lm.dutycode from lmriskduty lm where lm.riskcode = '" + riskCode + "')");
            }
        }catch (Exception e){
            logger.error("sql语句拼接有问题"+e.toString());
            buildError("deleteCoreData","查询核心产品表数据失败");
            return  false;
        }
        if("2".equals(synType) || "3".equals(synType)){
            risks = generateS(riskCodes);
            dutys = generateS(dutyCodes);
            gets = generateS(getDutyCodes);
            pays = generateS(payPlanCodes);
        }
        if(ssrs!=null && ssrs.getMaxRow()>0){
            String con = "";//存储条件
            for(int i=1;i<=ssrs.getMaxRow();i++){
                mSQLArray.add(ssrs.GetText(i,1));//存储需要操作的表数据
                if("2".equals(synType)){
                    con = ssrs.GetText(i,2).replace("?contPlanCode?", contPlanCode ).replace("?riskCode?",risks).replace("?dutyCode?",dutys).replace("?getDutyCode?",gets).replace("?payPlanCode?",pays);
                }else if("3".equals(synType)){
                    con = ssrs.GetText(i,2).replace("?riskCode?",riskCode).replace("?dutyCode?",dutys).replace("?getDutyCode?",gets).replace("?payPlanCode?",pays);
                }else{
                    con = ssrs.GetText(i,2);
                }
                mSQLCondition.add(con);
                tMMap.put("DELETE FROM " + ssrs.GetText(i,1) + " " + con, "DELETE");
            }
            isSYN = true;//是否需要同步表
        }else{
            logger.info("没有查询到需要同步的表数据");
            return true;
        }
        //对于部分同步的需要查找rt表
        String rtNames = "";
        String oneValue = "";
        if("2".equals(synType)){
            SSRS ssrsRisks = exeSQL.execSQL(" select distinct riskcode from ldplanrisk where contplancode = '" + contPlanCode + "'");
            if(ssrsRisks!=null && ssrsRisks.getMaxRow()>0){
                for (int j = 1; j <= ssrsRisks.getMaxRow(); j++) {
                    rtNames = "rt_" + ssrsRisks.GetText(j,1);
                    oneValue = exeSQL.getOneValue("select 1 from user_tables where table_name= '" + rtNames.toUpperCase() + "'");
                    if (oneValue != null
                            && "1".equals(oneValue)) {
                        mSQLArray.add(rtNames);
                        mSQLCondition.add(" where 1=1");
                        if(!dealRT(rtNames)){
                            return false;
                        }
                    }
                }
            }
        }else if("3".equals(synType)){
            rtNames = "rt_" + riskCode;
            oneValue = exeSQL.getOneValue("select 1 from user_tables where table_name= '" + rtNames.toUpperCase() + "'");
            if (oneValue != null
                    && "1".equals(oneValue)) {
                mSQLArray.add(rtNames);
                mSQLCondition.add(" where 1=1");
                if(!dealRT(rtNames)){
                    return false;
                }
            }
        }else{
            SSRS ssrsRisks = exeSQL.execSQL(" select distinct riskcode from lmrisk ");
            if(ssrsRisks!=null && ssrsRisks.getMaxRow()>0){
                for (int j = 1; j <= ssrsRisks.getMaxRow(); j++) {
                    rtNames = "rt_" + ssrsRisks.GetText(j,1);
                    oneValue = exeSQL.getOneValue("select 1 from user_tables where table_name= '" + rtNames.toUpperCase() + "'");
                    if (oneValue != null
                            && "1".equals(oneValue)) {
                        mSQLArray.add(rtNames);
                        mSQLCondition.add(" where 1=1");
                        if(!dealRT(rtNames)){
                            return false;
                        }
                    }
                }
            }
        }

        PubSubmit tPubSubmit = new PubSubmit(mid_connection);
        tVData.clear();
        tVData.add(tMMap);
        tVData4.add(tMMap4);
        if(tMMap5!=null && tMMap5.size()>0){
            tVData5.add(tMMap5);
            if (!tPubSubmit.submitData(tVData5)) {
                logger.error("删除表失败！");
                buildError("Pubsubmit","删除表失败"+tPubSubmit.mErrors.getFirstError());
                return false;
            }
        }

        logger.info("创建表开始");
        if(tMMap4!=null && tMMap4.size()>0){
            if (!tPubSubmit.submitData(tVData4)) {
                logger.error("创建表失败！");
                buildError("Pubsubmit","创建表"+tPubSubmit.mErrors.getFirstError());
                return false;
            }
        }
        logger.info("创建表结束");
        return true;
    }

    private boolean dealRT(String rtNames) {
        try{


        ExeSQL tExeSQL = new ExeSQL(mid_connection);
        ExeSQL tExeSQL2 = new ExeSQL(core_connection);
        String tSQL_CTTable = "select 1 from user_tables where table_name= '" + rtNames.toUpperCase() + "'";
        String oneValue = tExeSQL.getOneValue(tSQL_CTTable);
        if (oneValue != null
                && "1".equals(oneValue)) {
            String tDropSQL = "drop table " + rtNames;
            tMMap5.put(tDropSQL, "DROP");
        }

        String tQuery_SQL_DDL = "select  DBMS_METADATA.GET_DDL('TABLE',UPPER('"
                + rtNames + "')) FROM DUAL ";
            String tSQL_DDL = "";
        try {
            logger.debug(tExeSQL2.getOneValue(tQuery_SQL_DDL));
            tSQL_DDL = tExeSQL2.getOneValue(tQuery_SQL_DDL);
        }catch (Exception e){
            logger.error(e.toString());
            buildError("dealRT","核心不存在表"+rtNames);
            return  false;
        }

        if (tSQL_DDL.indexOf("USING INDEX") != -1) {
            tSQL_DDL = tSQL_DDL.substring(0,
                    tSQL_DDL.indexOf("USING INDEX"));
            tSQL_DDL = tSQL_DDL + " )";
        } else if (tSQL_DDL.indexOf("PCTFREE") != -1) {
            tSQL_DDL = tSQL_DDL.substring(0,
                    tSQL_DDL.indexOf("PCTFREE"));
        }

        tSQL_DDL = StrTool.replaceEx(tSQL_DDL, "\"", "");
        //获取当前的用户
        String tSQL_CurrentUser = "";
        tSQL_CurrentUser = "select SYS_CONTEXT('USERENV','CURRENT_USER') from dual ";
        SQLwithBindVariables sqlbv = new SQLwithBindVariables();
        sqlbv.sql(tSQL_CurrentUser);
        String tTableUser = tExeSQL2.getOneValue(sqlbv.sql());

        logger.info("CurrentUser:" + tTableUser);
        if (tTableUser != null && !tTableUser.equals("")) {
            tSQL_DDL = StrTool.replaceEx(tSQL_DDL,
                    tTableUser + ".", "");
        }

        logger.info("建表语句:" + tSQL_DDL);

        tMMap4.put(tSQL_DDL, "CREATE");



        }catch (Exception e){
            logger.error("报错了"+e.toString());
            buildError("dealrt",e.toString());
            return false;
        }
        return true;
    }

    private String generateS(SSRS ssrs) {
        String ss = "";
        if (ssrs!=null && ssrs.getMaxRow()>0){
            for (int m = 1; m <= ssrs.getMaxRow(); m++) {
                ss = ss + "'" + ssrs.GetText(m,1) + "'";
                if(m<ssrs.getMaxRow()){
                    ss = ss + "," ;
                }
            }
        }
        return  ss;
    }

    private boolean searchCoreProducts() {
        tMMap2 = new MMap();
        tMMap3 = new MMap();
        String sourceTable = "";
        String destTable = "";
        String sqlCondition = "";//部分同步时查询条件
        String tCols = "";
        SSRS midColsSSRS = new SSRS();
        SSRS coreColsSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        if(mSQLArray!=null && mSQLArray.size()>0){
            for (int i = 0; i < mSQLArray.size(); i++) {
                sourceTable  = mSQLArray.get(i);
                destTable = mSQLArray.get(i);
                sqlCondition = mSQLCondition.get(i);
                if("LMRISKPROCESS".equals(destTable.toUpperCase()) || "LMRISKCHANGE".equals(destTable.toUpperCase())){
                    if(!buildInsertSpecialTable(destTable,sqlCondition)){
                        logger.error("处理特殊的表失败了");
                        return false;
                    }
                    continue;
                }
                tCols = "select column_name,data_type from user_tab_cols where lower(table_name) ='"
                        + destTable.toLowerCase()
                        + "' and column_id is not null order by column_id";
                try{
                    tExeSQL = new ExeSQL(mid_connection);
                    midColsSSRS = tExeSQL.execSQL(tCols);
                    tExeSQL = new ExeSQL(core_connection);
                    coreColsSSRS = tExeSQL.execSQL(tCols);
                }catch (Exception e){
                    buildError("searchCoreProducts",e.toString());
                    logger.error("不存在此表~~~~~~~~");
                }
                if(!buildInsertData(sourceTable, destTable, midColsSSRS, coreColsSSRS,sqlCondition)){
                    logger.error("报错啦！");
                    return false;
                }
            }
        }
        tVData2.clear();
        tVData2.add(tMMap2);
        logger.info("本次插入的数据"+tMMap2.size());
        tVData3.clear();
        tVData3.add(tMMap3);
        logger.info("本次更新的数据"+tMMap3.size());
        return true;
    }



    private boolean buildInsertData(String sourceTable, String destTable, SSRS midColsSSRS, SSRS coreColsSSRS,String sqlCondition) {
        ArrayList<String> coreArray = new ArrayList();
        ArrayList<String> cols = new ArrayList();//中台和核心一致的字段
        ArrayList<String> colsType = new ArrayList();
        ArrayList<String> extraCols = new ArrayList();//中台比核心多的字段
        ArrayList<String> extraColsType = new ArrayList();
        String searchSQL = "";

        try {
            if(coreColsSSRS!=null && coreColsSSRS.getMaxRow() > 0 && midColsSSRS!=null && midColsSSRS.getMaxRow() > 0 ){
                for(int i=1;i<=coreColsSSRS.getMaxRow();i++){
                    coreArray.add(coreColsSSRS.GetText(i,1));//所有的字段
                }
            }else {
                buildError("buildInsertData","表"+destTable+"不存在");
                return false;
            }

            if(midColsSSRS!=null && midColsSSRS.getMaxRow() > 0){
                for(int j=1;j<=midColsSSRS.getMaxRow();j++){
                    if (coreArray.contains(midColsSSRS.GetText(j,1))){
                        cols.add(midColsSSRS.GetText(j,1));
                        colsType.add(midColsSSRS.GetText(j,2));
                    }else{
                        extraCols.add(midColsSSRS.GetText(j,1));
                        extraColsType.add(midColsSSRS.GetText(j,2));
                    }
                }
            }else{
                return false;
            }
            StringBuffer searchCols = new StringBuffer();//要查询的列名
            if(cols!=null && cols.size()>0){
                for (int k = 0; k < cols.size(); k++) {
                    if(k==cols.size()-1){
                        searchCols.append(cols.get(k));
                    }else{
                        searchCols.append(cols.get(k) + ",");
                    }
                }
            }else{
                return false;
            }

            //分为2步处理表 1）处理插入语句 2）针对中台多出的字段，中台要执行一次更新操作
            //1）处理插入中台的SQL语句
            searchSQL = "SELECT " + searchCols.toString() + " from " +  sourceTable + " " +  sqlCondition;

            ExeSQL tExeSQL = new ExeSQL(core_connection);
            SSRS searchResult = tExeSQL.execSQL(searchSQL);
            String tColType = "";
            String tColValue = "";
            String tCol = "";
            StringBuffer tColValueBF = new StringBuffer();
            logger.info(PubFun.getCurrentTime()+"拼装SQL开始------"+destTable);
            if(searchResult!=null && searchResult.getMaxRow() > 0){
                for(int m=1;m<=searchResult.getMaxRow();m++){
                    tColValueBF = new StringBuffer();
                    tColValueBF.append("insert into ");
                    tColValueBF.append(destTable);
                    tColValueBF.append(" (");
                    tColValueBF.append(searchCols);
                    tColValueBF.append(" ) values ( ");
                    for (int n = 0; n < colsType.size(); n++) {
                        tColType = colsType.get(n);
                        tCol = cols.get(n);
                        tColValue = searchResult.GetText(m,n+1);
                        if (!"".equals(tColValue)){
                            if (tColType.toUpperCase().equals("VARCHAR2")
                                    || tColType.toUpperCase().equals("CHAR")
                                    || tColType.toUpperCase().equals("VARCHAR")
                                    || tColType.toUpperCase().equals("DATE")) {
                                tColValueBF.append("'");
                                tColValue = tColValue.replaceAll("'","''");
                                tColValueBF.append(tColValue);
                                tColValueBF.append("'");

                            } else {
                                tColValue = tColValue.replaceAll("'","''");
                                tColValueBF.append(tColValue);
                            }
                        }else {
                            if ("LMRiskApp".toUpperCase().equals(destTable.toUpperCase()) && "STARTDATE".equals(tCol.toUpperCase())){
                                tColValueBF.append("'" + PubFun.getCurrentDate() + "'");
                            }else {
                                tColValueBF.append("''");
                            }
                        }
                        if(n!=colsType.size()-1){
                            tColValueBF.append(",");
                        }
                    }
                    tColValueBF.append(" )");
                    tMMap2.put(tColValueBF.toString(),"INSERT");
                }
            }else{
                return true;
            }
            logger.info(PubFun.getCurrentTime()+"拼装SQL结束------"+destTable);
        }catch (Exception e){
            logger.error("拼装sql出问题了"+e.toString());
            buildError("buildInsertData",e.toString());
        }

        //2）更新中台表多余的字段，通过主键更新，如果没有主键则不去考虑
        //查询中台表的主键
        try{
            ExeSQL midExeSQL = new ExeSQL(mid_connection);
            SSRS midSSRS = midExeSQL.execSQL(" select a.column_name from user_cons_columns a, user_constraints b "
                    + " where a.constraint_name = b.constraint_name and b.constraint_type = 'P' "
                    + " and lower(a.table_name) ='" + destTable.toLowerCase() + "'");//查询出表的主键
            //拼装查询sql
            StringBuffer extraSB = new StringBuffer();
            StringBuffer updateSB = new StringBuffer();
            if(extraCols!=null && extraCols.size()>0){
                if(midSSRS !=null && midSSRS.getMaxRow() > 0){
                    logger.info("需要更新的表"+destTable);
                    extraSB.append(" select ");
                    for (int p = 0; p < extraCols.size(); p++) {
                        if(p > 0){
                            extraSB.append(" ,");
                        }
                        extraSB.append(extraCols.get(p));
                    }
                    for (int q = 1; q <= midSSRS.getMaxRow(); q++) {
                        extraSB.append(",");
                        extraSB.append(midSSRS.GetText(q,1));
                    }
                    extraSB.append(" from " + destTable + " " + sqlCondition);
                }
                SSRS midUpdateResult = midExeSQL.execSQL(extraSB.toString());
                //拼装update语句
                if (midUpdateResult!=null && midUpdateResult.getMaxRow()>0){

                    String extraCol = "";
                    String extraColValue = "";
                    String extraColType = "";
                    for (int e = 1; e <= midUpdateResult.getMaxRow(); e++) {
                        updateSB = new StringBuffer();
                        updateSB.append("update " + destTable + " set ");
                        for (int f = 1; f <= midUpdateResult.getMaxCol(); f++) {
                            if(f <= extraCols.size()){
                                if (f>1) {
                                    updateSB.append(" ,");
                                }
                                extraCol  = extraCols.get(f-1);
                                extraColValue = midUpdateResult.GetText(e,f).replace("'","''");
                                extraColType =  extraColsType.get(f-1);
                                updateSB.append(extraCol + "=");
                                if (!"".equals(extraColValue)){
                                    if("VARCHAR2".equals(extraColType)
                                        || "CHAR".equals(extraColType)
                                        || "DATE".equals(extraColType)
                                        || "VARCHAR".equals(extraColType)){
                                        updateSB.append("'");
                                        updateSB.append(extraColValue);
                                        updateSB.append("'");
                                    } else {
                                        updateSB.append(extraColValue);
                                    }
                                }else {
                                    updateSB.append("''");
                                }
                            }else{
                                if(extraCols.size() + 1 == f){
                                    updateSB.append(" where ");
                                }else {
                                    updateSB.append(" and ");
                                }
                                extraColValue = midUpdateResult.GetText(e,f).replace("'","''");
                                updateSB.append(midSSRS.GetText(f-extraCols.size(),1) + "=");
                                updateSB.append("'" + extraColValue + "'");
                            }

                        }
                        //logger.info(updateSB.toString());//更新语句
                        tMMap3.put(updateSB.toString(),"UPDATE");//存储所有需要更新的数据
                    }
                }

            }
        }catch(Exception e){
            logger.error("更新表数据发生错误"+e.toString());
            buildError("buildInsertData",e.toString());
        }



        return true;
    }

    private boolean buildInsertSpecialTable(String destTable,String sqlCondition) {
        String tCols = "";
        String searchSQL = "";
        logger.info(PubFun.getCurrentDate()+PubFun.getCurrentTime()+"处理特殊表");
        if("LMRISKPROCESS".equals(destTable.toUpperCase())){
            searchSQL = "select riskcode,riskprop,'N', 'N', 'N', 'N', '01', '', '' from lmriskapp " + sqlCondition;
            tCols = " RISKCODE, RISKTYPE, VALISALEFLAG, CALLVCHFLAG, GETRATEFLAG, SPECIALAPPROVEFLAG, BAK1, BAK2, BAK3 ";
        }else if ("LMRISKCHANGE".equals(destTable.toUpperCase())){
            searchSQL = "select riskcode,riskcode,riskcode,'N' from lmriskapp " + sqlCondition;
            tCols = " INPUTRISKCODE,RISKCODE,BACKRISKCODE,QFLAG ";
        }
        try{
            ExeSQL tExeSQL = new ExeSQL(core_connection);
            SSRS tResult= tExeSQL.execSQL(searchSQL);
            StringBuffer tColValueBF = new StringBuffer();
            if(tResult!=null && tResult.getMaxRow() > 0){
                for (int i = 1; i <= tResult.getMaxRow(); i++) {
                    tColValueBF = new StringBuffer();
                    tColValueBF.append("insert into ");
                    tColValueBF.append(destTable);
                    tColValueBF.append(" (");
                    tColValueBF.append(tCols);
                    tColValueBF.append(" ) values ( ");
                    for (int j = 1; j <= tResult.getMaxCol(); j++) {
                        tColValueBF.append("'");
                        tColValueBF.append(tResult.GetText(i,j));
                        tColValueBF.append("'");
                        if(j<tResult.getMaxCol()){
                            tColValueBF.append(",");
                        }
                    }
                    tColValueBF.append(" )");
                    tMMap2.put(tColValueBF.toString(),"INSERT");
                }
            }
        }catch (Exception e){
            logger.error("处理特殊表同步失败"+e.toString());
            buildError("buildInsertSpecialTable",e.toString());
            return false;
        }
        return true;
    }


    /**
     * 构建错误类
     *
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "TableSyn";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}

