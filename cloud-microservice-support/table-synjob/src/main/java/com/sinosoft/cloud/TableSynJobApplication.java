package com.sinosoft.cloud;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author lishuai
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.sinosoft.cloud")
@Configuration
public class TableSynJobApplication {

    public static void main(String[] args) {
            SpringApplication.run(TableSynJobApplication.class, args);
    }

}
