package com.sinosoft.cloud.job;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Component("MidTableSynTask")
public class MidTableSynTask {
    private MMap tMMap = new MMap();
    private MMap tMMap2 = new MMap();
    private MMap tMMap3 = new MMap();

    VData tVData = new VData();
    private VData tVData2 = new VData();
    private VData tVData3 = new VData();
    private Connection mid_connection = null;
    private Connection open_connection = null;
    private ArrayList<String> mSQLArray = new ArrayList();
    private ArrayList<String> mSQLCondition = new ArrayList();
    private String contPlanCode = "";
    private String riskCode = "";
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    private TransferData mTransferData;
    public CErrors mErrors = new CErrors();
    private Logger logger = LoggerFactory.getLogger(MidTableSynTask.class);

    /**默认需要同步套餐数据 */
    private boolean synPlancode = true;
    /**默认需要同步险种数据 */
    private boolean synRiskcode = true;
    private StringBuffer plancodes = new StringBuffer();
    private StringBuffer riskcodes = new StringBuffer();
    private boolean mflag = false;//是否需要回滚

    public boolean submitData() {
        try {
            if (!getInputData()) {
                logger.error("查询同步表数据失败！");
                return false;
            }
            if (!checkData()){
                logger.error("校验数据失败！");
                return false;
            }
            if(!dealData()){
                logger.error("同步表数据失败！");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }finally {
            try{
                if(mid_connection!=null){
                    mid_connection.close();
                }
                if(open_connection!=null){
                    open_connection.close();
                }
            }catch (SQLException e){
                logger.error(e.toString());
            }
        }
        return true;
    }



    private boolean checkData() {

        return true;
    }

    /**
     * 得到需要同步的表数据
     */
    private boolean getInputData() throws ParseException {
        mid_connection = DBConnPool.getConnection("dataSource");
        open_connection = DBConnPool.getConnection("opendataSource");
        ExeSQL tExeSQL = new ExeSQL(open_connection);
        ExeSQL tExeSQL2 = new ExeSQL(mid_connection);

        String oneValue1 = tExeSQL.getOneValue(" select count(1) from open_LmRiskInterface");
        String oneValue2 = tExeSQL2.getOneValue(" select count(1) from ldplan");
        String oneValue3 = tExeSQL2.getOneValue(" select count(1) from lmriskapp b ");

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date dt = df.parse(PubFun.getCurrentDate());
        Calendar tCalendar = Calendar.getInstance();
        tCalendar.setTime(dt);
        tCalendar.add(Calendar.DAY_OF_MONTH,-1);
        Date dt2 = tCalendar.getTime();
        String beforeDate = df.format(dt2);

        SSRS ssrs1;
        SSRS ssrs2;
        /*if(Integer.valueOf(oneValue1)>=(Integer.valueOf(oneValue2)+Integer.valueOf(oneValue3))){
            ssrs1 = tExeSQL2.execSQL(" select contplancode from ldplan where modifydate >= date'" + beforeDate + "'");
        }else{
            ssrs1 = tExeSQL2.execSQL(" select contplancode from ldplan ");
        }*/
        ssrs1 = tExeSQL2.execSQL(" select contplancode from ldplan where standardflag in ('0','1')");

        ssrs2 = tExeSQL2.execSQL(" select riskcode from lmrisk b where standardflag in ('0','1')");
        if(ssrs2!=null && ssrs2.getMaxRow()>0){
            dealSSRS(ssrs2,"2");
            logger.info(riskcodes.toString());
        }else {
            synRiskcode = false;
        }
        if(ssrs1!=null && ssrs1.getMaxRow()>0){
            dealSSRS(ssrs1,"1");
            logger.info(plancodes.toString());
        }else {
            synPlancode = false;
        }


        return true;
    }

    public void dealSSRS(SSRS ssrs,String type){
        if("1".equals(type)){
            plancodes = new StringBuffer();
            plancodes.append("in (");
            for (int i = 1; i <= ssrs.getMaxRow() ; i++) {
                if (i>1){
                    plancodes.append(",");
                }
                plancodes.append("'");
                plancodes.append(ssrs.GetText(i,1));
                plancodes.append("'");
            }
            plancodes.append(" )");
        }else {
            riskcodes = new StringBuffer();
            riskcodes.append("in (");
            for (int i = 1; i <= ssrs.getMaxRow() ; i++) {
                if (i>1){
                    riskcodes.append(",");
                }
                riskcodes.append("'");
                riskcodes.append(ssrs.GetText(i,1));
                riskcodes.append("'");
            }
            riskcodes.append(" )");
        }
    }


    /**
     * 处理同步表
     * @return
     */
    private synchronized boolean dealData() {

        try{
            //1)删除开放平台的产品表数据
            if (!deleteMidData()) {
                logger.error("查询开放平台数据失败！");
                return false;
            }

            //2）查询中台的产品表数据拼装sql
            if (!searchCoreProducts()){
                logger.error("查询中台表数据失败！");
                return false;
            }
            if(synPlancode || synRiskcode) {
                logger.info("删除开放平台产品表开始" + PubFun.getCurrentDate() + PubFun.getCurrentTime());
                //3）删除开放平台产品表数据
                PubSubmit tPubSubmit = new PubSubmit(open_connection);
                if (!tPubSubmit.submitData(tVData)) {
                    logger.error("删除开放平台产品表数据失败！");
                    mflag = true;
                    buildError("Pubsubmit", "删除语句" + tPubSubmit.mErrors.getFirstError());
                    return false;
                }
                logger.info("删除开放平台产品表结束" + PubFun.getCurrentDate() + PubFun.getCurrentTime());

                //4）同步数据
                logger.info("插入开放平台产品表数据开始" + PubFun.getCurrentDate() + PubFun.getCurrentTime());
                if (!tPubSubmit.submitData(tVData2)) {
                    logger.error("插入开放平台产品表数据失败！");
                    mflag = true;
                    buildError("Pubsubmit", "添加语句" + tPubSubmit.mErrors.getFirstError());
                    return false;
                }
                logger.info("插入开放平台产品表数据结束" + PubFun.getCurrentDate() + PubFun.getCurrentTime());
                if (tVData3.size()>0){
                    if (!tPubSubmit.submitData(tVData3)) {
                        logger.error("更新开放平台产品表数据失败！");
                        mflag = true;
                        buildError("Pubsubmit", "更新语句" + tPubSubmit.mErrors.getFirstError());
                        return false;
                    }
                }
            }
        }catch (Exception e){
            logger.error("失败了~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            buildError("dealData","处理失败了");
            return false;
        }finally {
            try {
                if(mid_connection!=null){
                    mid_connection.close();
                }
                if(open_connection!=null){
                    open_connection.close();
                    if (mflag){
                        open_connection.rollback();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return true;
    }



    private boolean deleteMidData() {
        tMMap = new MMap();
        /*if(synPlancode){
            tMMap.put("DELETE FROM open_LmRiskInterface where productcode " +  plancodes.toString() , "DELETE");
            tMMap.put("DELETE FROM open_LmRiskItemInterface where productcode  " + plancodes.toString() , "DELETE");
        }
        if(synRiskcode) {
            tMMap.put("DELETE FROM open_LmRiskInterface where productcode " + riskcodes.toString(), "DELETE");
            tMMap.put("DELETE FROM open_LmRiskItemInterface where  productcode " + riskcodes.toString() , "DELETE");
        }*/
        tMMap.put("DELETE FROM open_LmRiskInterface ", "DELETE");
        tMMap.put("DELETE FROM open_LmRiskItemInterface ", "DELETE");
        tMMap.put("DELETE FROM LACHANNELPRODUCT " , "DELETE");
        tMMap.put("DELETE FROM open_lasaleinfo " , "DELETE");
        tVData.clear();
        tVData.add(tMMap);

        return true;
    }

    private boolean searchCoreProducts() {
        tMMap2 = new MMap();
        ExeSQL tExeSQL = new ExeSQL(mid_connection);
        String tSearchSQL = "";
        //产品编码open_LmRiskInterface
        if(synPlancode) {
            tSearchSQL = " select a.contplancode,2,0,a.contplanname,decode(standardflag,'0','1','1','0','') ,decode(a.updownstatus,'1','0','0','1','0'),0,(select sum(b.calfactorvalue) " +
                    " from ldplandutyparam b where b.contplancode = a.contplancode and b.calfactor = 'Amnt'),1 from ldplan a where standardflag in ('0','1') and contplancode "
                    + plancodes.toString();
            logger.info(tSearchSQL);
            SSRS ssrs1 = tExeSQL.execSQL(tSearchSQL);
            StringBuffer tSQL = new StringBuffer();
            String tContplancode = "";
            if (ssrs1 != null && ssrs1.getMaxRow() > 0) {
                for (int i = 1; i <= ssrs1.getMaxRow(); i++) {
                    tSQL = new StringBuffer();
                    tSQL.append(" INSERT INTO open_LmRiskInterface ");
                    tSQL.append(" (productcode,productcategory,productclass,productname,isstandard,isonline,isconfig,MinPrem,datastate,modifydate,modifytime,makedate,maketime) ");
                    tSQL.append(" values (");
                    for (int j = 1; j <= ssrs1.getMaxCol(); j++) {
                        if (j > 1) {
                            tSQL.append(",");
                        }
                        tSQL.append("'");
                        tSQL.append(ssrs1.GetText(i, j));
                        if (j == 6 && "".equals(ssrs1.GetText(i, j))) {
                            tSQL.append("0");
                        }
                        tSQL.append("'");
                    }
                    tSQL.append(",date'");
                    tSQL.append(PubFun.getCurrentDate());
                    tSQL.append("','");
                    tSQL.append(PubFun.getCurrentTime());
                    tSQL.append("',date'");
                    tSQL.append(PubFun.getCurrentDate());
                    tSQL.append("','");
                    tSQL.append(PubFun.getCurrentTime());
                    tSQL.append("' )");
                    tMMap2.put(tSQL.toString(), "INSERT");
                    tContplancode = ssrs1.GetText(i, 1);
                    dealPlancodeItem(tContplancode);
                }
            }
        }
        //险种编码open_LmRiskInterface
        if(synRiskcode) {
            tSearchSQL = " select b.riskcode,1, decode(b.kindcode,'L','4','R','2','H','3','A','1','S','6','U','7','0'), b.riskname,b.maxappntage,b.mininsuredage,b.maxinsuredage,b.minappntage," +
                    "  decode(a.standardflag,'0','1','1','0','') , decode(updownstatus,'1','0','0','1','0') , 0 , 1 from lmriskapp b,lmrisk a " +
                    " where a.riskcode = b.riskcode and a.standardflag in ('0','1') and  a.riskcode " + riskcodes.toString();
            logger.info(tSearchSQL);
            SSRS ssrs2 = tExeSQL.execSQL(tSearchSQL);
            String tRiskcode = "";
            String tRisktype = "";
            StringBuffer tSQL2 = new StringBuffer();
            if (ssrs2 != null && ssrs2.getMaxRow() > 0) {
                for (int i = 1; i <= ssrs2.getMaxRow(); i++) {
                    tSQL2 = new StringBuffer();
                    tSQL2.append(" INSERT INTO open_LmRiskInterface ");
                    tSQL2.append(" (ProductCode,ProductCategory,ProductClass,ProductName,AppntAgeEnd,InsuredAgeStart,InsuredAgeEnd,AppntAgeStart,isstandard,IsOnline,IsConfig,DataState,ModifyDate,ModifyTime,MakeDate,MakeTime )");
                    tSQL2.append(" values ( ");

                    for (int j = 1; j <= ssrs2.getMaxCol(); j++) {
                        if (j > 1) {
                            tSQL2.append(",");
                        }
                        tSQL2.append("'");
                        tSQL2.append(ssrs2.GetText(i, j));
                        if (j == 10 && "".equals(ssrs2.GetText(i, j))) {
                            tSQL2.append("0");
                        }
                        tSQL2.append("'");
                    }
                    tSQL2.append(",date'");
                    tSQL2.append(PubFun.getCurrentDate());
                    tSQL2.append("','");
                    tSQL2.append(PubFun.getCurrentTime());
                    tSQL2.append("',date'");
                    tSQL2.append(PubFun.getCurrentDate());
                    tSQL2.append("','");
                    tSQL2.append(PubFun.getCurrentTime());
                    tSQL2.append("' )");
                    tMMap2.put(tSQL2.toString(), "INSERT");
                    tRiskcode = ssrs2.GetText(i, 1);
                    tRisktype = ssrs2.GetText(i, 2);
                    dealRiskcodeItem(tRiskcode, tRisktype);

                }
            }
        }

        //同步渠道产品关系表
        tSearchSQL = " select a.channelcode,a.channelname,a.riskcode,a.riskname,a.risktype,a.riskstartselldate,a.riskendselldate, "
            + " a.createdate,a.makedate,a.modifydate,a.h5href,a.status,a.rnewageflag,a.isconfig,a.updowntype,a.remark,a.channelproductupdate,a.channelproductdowndate from LACHANNELPRODUCT a ";
        logger.info(tSearchSQL);
        SSRS ssrs3 = tExeSQL.execSQL(tSearchSQL);
        StringBuffer tSQL3 = new StringBuffer();
        if (ssrs3 != null && ssrs3.getMaxRow() > 0) {
            for (int i = 1; i <= ssrs3.getMaxRow(); i++) {
                tSQL3 = new StringBuffer();
                tSQL3.append(" INSERT INTO LACHANNELPRODUCT ");
                tSQL3.append(" (channelcode,channelname,riskcode,riskname,risktype,riskstartselldate,riskendselldate,createdate,makedate,modifydate,h5href,status,rnewageflag,isconfig,updowntype,remark,channelproductupdate,channelproductdowndate) ");
                tSQL3.append(" values ( ");
                for (int j = 1; j <= ssrs3.getMaxCol(); j++) {
                    if (j > 1) {
                        tSQL3.append(",");
                    }
                    tSQL3.append("'");
                    tSQL3.append(ssrs3.GetText(i, j));
                    tSQL3.append("'");
                }
                tSQL3.append(" )");
                tMMap2.put(tSQL3.toString(), "INSERT");
            }
        }



        //同步渠道数据
        tSearchSQL = "select channelcode,channelname,comcode,salechnl,managecom,remark1,remark2 from lasaleinfo";
        logger.info(tSearchSQL);
        SSRS ssrs4 = tExeSQL.execSQL(tSearchSQL);
        StringBuffer tSQL4 = new StringBuffer();
        if (ssrs4 != null && ssrs4.getMaxRow() > 0) {
            for (int i = 1; i <= ssrs4.getMaxRow(); i++) {
                tSQL4 = new StringBuffer();
                tSQL4.append(" INSERT INTO open_lasaleinfo ");
                tSQL4.append(" (channelcode,channelname,comcode,salechnl,managecom,remark1,remark2)");
                tSQL4.append(" values ( ");
                for (int j = 1; j <= ssrs4.getMaxCol(); j++) {
                    if (j > 1) {
                        tSQL4.append(",");
                    }
                    tSQL4.append("'");
                    tSQL4.append(ssrs4.GetText(i, j));
                    tSQL4.append("'");
                }
                tSQL4.append(" )");
                tMMap2.put(tSQL4.toString(), "INSERT");
            }
        }

        //处理开放平台是否配置字段
        tMMap3 = new MMap();
        ExeSQL openExe = new ExeSQL(open_connection);
        SSRS needsUpdate = openExe.execSQL(" select productcode,isconfig from open_lmriskinterface b where b.isconfig <> '0'");
        if(needsUpdate!=null && needsUpdate.getMaxRow() > 0){
            for (int i= 1; i<= needsUpdate.getMaxRow() ; i++){
                tMMap3.put(" update open_lmriskinterface set isconfig = '" + needsUpdate.GetText(i,2) +"' where productcode = '" + needsUpdate.GetText(i,1)  + "'", "UPDATE");
            }
            tVData3.clear();
            tVData3.add(tMMap3);
        }

        tVData2.clear();
        tVData2.add(tMMap2);
        return true;
    }

    private void dealRiskcodeItem(String tRiskcode, String tRisktype) {
        ExeSQL tExeSQL = new ExeSQL(mid_connection);
        //处理交费方式
        String generate = "";
        SSRS ssrs = tExeSQL.execSQL("  select distinct e.payintv from lmriskduty b ,lmdutypayrela c ,lmdutypay e  where b.dutycode = c.dutycode and c.payplancode = e.payplancode and b.riskcode = '" + tRiskcode + "' ");
        StringBuffer tSQL1 = new StringBuffer();
        if(ssrs!=null && ssrs.getMaxRow() > 0){
            for (int i = 1; i <= ssrs.getMaxRow() ; i++) {
                if (!"".equals(ssrs.GetText(i,1))){
                    generate = PubFun.CreateSeq("seq_batchno_20191116",19);
                    logger.info("序列号==" + generate);
                    tSQL1 = new StringBuffer();
                    tSQL1.append(" insert into open_LmRiskItemInterface (SERIALNO, PRODUCTCODE, PRODUCTCATEGORY, PRODUCTCLASS, ITEMTYPE, ITEMVALUE, ISSEE, DATASTATE, MODIFYDATE, MODIFYTIME, MAKEDATE, MAKETIME ) ");
                    tSQL1.append(" values ( '");
                    tSQL1.append(generate + "','");
                    tSQL1.append(tRiskcode);
                    tSQL1.append("','1','");
                    tSQL1.append(tRisktype);
                    tSQL1.append("','9','");
                    tSQL1.append(ssrs.GetText(i,1));
                    tSQL1.append("','1','1',date'");
                    tSQL1.append(PubFun.getCurrentDate());
                    tSQL1.append("','");
                    tSQL1.append(PubFun.getCurrentTime());
                    tSQL1.append("',date'");
                    tSQL1.append(PubFun.getCurrentDate());
                    tSQL1.append("','");
                    tSQL1.append(PubFun.getCurrentTime());
                    tSQL1.append("' )");
                    tMMap2.put(tSQL1.toString(),"INSERT");
                }
            }
        }

    }

    private void dealPlancodeItem(String tContplancode) {
        ExeSQL tExeSQL =  new ExeSQL(mid_connection);
        //处理交费方式
        String generate = "";

        SSRS ssrs1 = tExeSQL.execSQL(" select distinct e.payintv from ldplanrisk a,lmriskduty b ,lmdutypayrela c ,lmdutypay e where a.riskcode = b.riskcode and b.dutycode = c.dutycode and c.payplancode = e.payplancode and a.contplancode = '" + tContplancode + "'");
        StringBuffer tSQL1 = new StringBuffer();
        if(ssrs1!=null && ssrs1.getMaxRow()>0){
            for (int i = 1; i <= ssrs1.getMaxRow(); i++) {
                if(!"".equals(ssrs1.GetText(i,1))){
                    generate = PubFun.CreateSeq("seq_batchno_20191116",19);
                    tSQL1 = new StringBuffer();
                    tSQL1.append(" insert into open_LmRiskItemInterface (SERIALNO, PRODUCTCODE, PRODUCTCATEGORY, PRODUCTCLASS, ITEMTYPE, ITEMVALUE, ISSEE, DATASTATE, MODIFYDATE, MODIFYTIME, MAKEDATE, MAKETIME ) ");
                    tSQL1.append(" values ( '");
                    tSQL1.append(generate + "','");
                    tSQL1.append(tContplancode);
                    tSQL1.append("','1','0','9','");
                    tSQL1.append(ssrs1.GetText(i,1));
                    tSQL1.append("','1','1',date'");
                    tSQL1.append(PubFun.getCurrentDate());
                    tSQL1.append("','");
                    tSQL1.append(PubFun.getCurrentTime());
                    tSQL1.append("',date'");
                    tSQL1.append(PubFun.getCurrentDate());
                    tSQL1.append("','");
                    tSQL1.append(PubFun.getCurrentTime());
                    tSQL1.append("' )");
                    tMMap2.put(tSQL1.toString(),"INSERT");
                }
            }
        }
    }

    /**
     * 构建错误类
     *
     * @param szFunc
     * @param szErrMsg
     */
    private void buildError(String szFunc, String szErrMsg) {

        CError cError = new CError();
        cError.moduleName = "TableSyn";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}

