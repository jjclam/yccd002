package com.sinosoft.cloud.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LdJobRunLogSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lishuai  on 2019/12/02
 */
@RestController
public class MidProductTransJob implements SimpleJob {

    private Logger logger = LoggerFactory.getLogger(MidProductTransJob.class);
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String tCurrentDate = PubFun.getCurrentDate();
    private String tCurrentTime = PubFun.getCurrentTime();
    @Override
    public synchronized void execute(ShardingContext shardingContext) {
        jobBeginLog(shardingContext);//启动日志
        dealData();
        jobEndLog(shardingContext);//结束日志
    }

    @RequestMapping("/test")
    public String index(){
        dealData();
        return "结束了";
    }

    private void backlog(String tResult) {
        //记录任务调度日志表 LDJOBRUNLOG
        MMap tMMap = new MMap();
        VData tVData = new VData();
        String serialno = PubFun.CreateSeq("seq_batchno_20191116",10);
        ExeSQL mExeSQL = new  ExeSQL();
        String codeSql = "select code from ldcode where codetype ='jobtype' and codealias = 'MidTableSynTask'";
        String taskCode = mExeSQL.getOneValue(codeSql);
        LdJobRunLogSchema tLDJOBRUNLOGSchema = new LdJobRunLogSchema();
        tLDJOBRUNLOGSchema.setSerialNo(serialno);
        tLDJOBRUNLOGSchema.setTaskCode(taskCode);
        tLDJOBRUNLOGSchema.setTaskName("MidTableSynTask");
        tLDJOBRUNLOGSchema.setExecuteDate(mCurrentDate);
        tLDJOBRUNLOGSchema.setExecuteTime(mCurrentTime);
        tLDJOBRUNLOGSchema.setFinishDate(tCurrentDate);
        tLDJOBRUNLOGSchema.setFinishTime(tCurrentTime);
        if ("success".equals(tResult)){
            tLDJOBRUNLOGSchema.setExecuteState("1");
        }else{
            tLDJOBRUNLOGSchema.setExecuteState("0");
        }
        tLDJOBRUNLOGSchema.setExecuteResult("产品信息同步批处理执行" + tResult);
        tMMap.put(tLDJOBRUNLOGSchema,"DELETE&INSERT");

        tVData.add(tMMap);

        //数据落库
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(tVData);
    }

    /**
     * job 分片启动日志
     * @param shardingContext
     */
    public void jobBeginLog(ShardingContext shardingContext){
        logger.info(String.format("产品表数据同步批处理任务开始：Thread ID: %s, 任务总片数: %s, " +
                        "当前分片项: %s.当前参数: %s,"+
                        "当前任务名称: %s.当前任务参数: %s",Thread.currentThread().getId(),
                shardingContext.getShardingTotalCount(),
                shardingContext.getShardingItem(),
                shardingContext.getShardingParameter(),
                shardingContext.getJobName(),
                shardingContext.getJobParameter()));
    }

    /**
     * job 分片结束日志
     * @param shardingContext
     */
    public void jobEndLog(ShardingContext shardingContext){
        logger.info(String.format("产品表数据同步批处理任务结束：Thread ID: %s, 任务总片数: %s, " +
                        "当前分片项: %s.当前参数: %s,"+
                        "当前任务名称: %s.当前任务参数: %s",Thread.currentThread().getId(),
                shardingContext.getShardingTotalCount(),
                shardingContext.getShardingItem(),
                shardingContext.getShardingParameter(),
                shardingContext.getJobName(),
                shardingContext.getJobParameter()));
    }


    private String dealData(){


        MidTableSynTask tMidTableSynTask = new MidTableSynTask();
        //业务处理
        if(tMidTableSynTask.submitData()) {
            tCurrentDate = PubFun.getCurrentDate();
            tCurrentTime = PubFun.getCurrentTime();
            backlog("success");
            return "success";
        }else {
            tCurrentDate = PubFun.getCurrentDate();
            tCurrentTime = PubFun.getCurrentTime();
            String tErrors = "Fail";
            if (tMidTableSynTask.mErrors!=null && tMidTableSynTask.mErrors.getFirstError().length()>0){
                if(tMidTableSynTask.mErrors.getFirstError().length()>100){
                    tErrors = tErrors + tMidTableSynTask.mErrors.getFirstError().substring(0,100);
                }else {
                    tErrors = tErrors + tMidTableSynTask.mErrors.getFirstError();
                }
            }
            backlog(tErrors);
            return tErrors;
        }
    }



}
